﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Accounting.Core
{
    public class ITripleDes
    {
        private static string default_Key = "easy@accounting";

        #region Encyption and Decyption

        static TripleDES CreateTripleDes(string key)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            TripleDES tripleDes = new TripleDESCryptoServiceProvider();
            tripleDes.Key = md5.ComputeHash(Encoding.Unicode.GetBytes(key));
            tripleDes.IV = new byte[tripleDes.BlockSize / 8];
            return tripleDes;
        }

        public static string TripleDesEncryption(string key, string text)
        {
            TripleDES tripleDes = CreateTripleDes(key);
            ICryptoTransform ct = tripleDes.CreateEncryptor();
            byte[] input = Encoding.Unicode.GetBytes(text);
            byte[] output = ct.TransformFinalBlock(input, 0, input.Length);
            return Convert.ToBase64String(output);
        }

        public static string TripleDesDecryption(string key, string text)
        {
            TripleDES tripleDes = CreateTripleDes(key);
            ICryptoTransform ct = tripleDes.CreateDecryptor();
            string fixedText = text.Replace(" ", "+");
            byte[] input = Convert.FromBase64String(fixedText);
            byte[] output = ct.TransformFinalBlock(input, 0, input.Length);
            string i=Encoding.Unicode.GetString(output);
            return i;
        }

        #endregion

        #region 3DES Default key
        public static string TripleDesDefaultEncryption(string text)
        {
            return TripleDesEncryption(default_Key, text);
        }

        public static string TripleDesDefaultDecryption(string encrypted)
        {
            return TripleDesDecryption(default_Key, encrypted);
        }
        #endregion
    }
}
