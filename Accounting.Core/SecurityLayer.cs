﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Security.Principal;
using System.Threading;
using System.Collections;
using FX.Data;
using FX.Core;
using FX.Utils;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.Core;

namespace Accounting.Core
{   
    /// <summary>
    /// class này kế thừa IPrincipal để liên kết với các thread hiện tại và tương lai trong phạm vi
    /// của ứng dụng này, dựa trên thông tin bảo mật(định danh) của người dùng đã được xác thực(sau khi đăng nhập).
    /// </summary>
    public class SecurityPrinciple : IPrincipal
    {
        public static IAppUserService _IUserService { get { return IoC.Resolve<IAppUserService>(); } }

        /// <summary>
        /// Cờ báo cho biết việc người dùng này đã được xác thực bởi RBAC chưa
        /// </summary>
        public static bool IsRBACAuthenticated = false;
        /// <summary>
        /// Danh sách này chứa các vai trò của người dùng
        /// </summary>
        static Hashtable Roles;
        /// <summary>
        /// Danh sách này chứa các quyền truy cập của người dùng
        /// </summary>
        static Hashtable SecurityRights;
        /// <summary>
        /// Đối tượng thông tin nhận dạng người dùng, được gắn với class SecurityPrincipal này
        /// </summary>
        static UserIdentity TheUserIdentity;

        private SecurityPrinciple() { }
        /// <summary>
        /// chỉ cho hàm SetSecurityPrincipal khởi tạo instance của lớp SecurityPrincipal này
        /// </summary>
        /// <param name="Roles">danh sách vai trò của người dùng</param>
        /// <param name="SecurityRights">danh sách quyền truy cập của người dùng</param>
        /// <param name="UserInfo">các thông tin định dạng người dùng</param>
        private static SecurityPrinciple SetSecurityValues(Hashtable roles, Hashtable secRights, Hashtable uInfo)
        {
            Roles = roles;
            SecurityRights = secRights;
            // tạo một đối tượng kiểu IIdentity và gắn nó vào lớp này(dạng IPrincipal)
            TheUserIdentity = UserIdentity.CreateUserIdentity(uInfo);
            return new SecurityPrinciple();
        }

        /// <summary>
        /// Khởi tạo cơ chế RBAC để kiểm tra định danh tính người dùng
        /// </summary>
        /// <param name=UserName></param>
        /// <param name=Password></param>
        /// <returns>yes/ no</returns>
        public static AuthenticatedUser IsAuthenticatedUser(string userName, string password)
        {
            //bool retVal = false;
            Hashtable roles = null;
            Hashtable securityRights = null;

            AuthenticatedUser authenticatedUser = _IUserService.CheckUserAuthentication(userName, password);                
            if (authenticatedUser.UserName != null && authenticatedUser.Roles != null && authenticatedUser.Permissions != null)
            {
                roles = authenticatedUser.Roles;
                securityRights = authenticatedUser.Permissions;
                Hashtable user = new Hashtable();
                user.Add(authenticatedUser.UserName, authenticatedUser.LastName + " " + authenticatedUser.FirstName);
                IPrincipal ip = SetSecurityPrinciple(roles, securityRights, user);
                IsRBACAuthenticated = true;
                //retVal = true;
            }

            return authenticatedUser;
        }

        /// <summary>
        /// thiết lập mới thông tin security principal với các thông tin định danh, bảo mật của người dùng        
        /// </summary>
        /// <param name="roles">danh sách vai trò của người dùng</param>
        /// <param name="SecurityRights">danh sách quyền truy cập của người dùng</param>
        /// <param name="UserInfo">thông tin định danh của người dùng</param>
        /// <returns> trả lại thông tin đối tượng dạng IPrincipal hiện tại để nó có thể phục hồi lại sau khi người dùng đăng xuất; trả null nếu ko thiết lập lại thông tin security principal</returns>
        private static IPrincipal SetSecurityPrinciple(Hashtable roles, Hashtable securityRights, Hashtable user)
        {
            // thiết lập này xác định phạm vi xác thực người dùng trong phạm vi ứng dụng này, một thread sẽ có thông tin 
            // security principal gắn vào, được .NET dùng để kiểm tra tư cách vai trò người dùng(role-based security)
            // Gán luôn PrincipalPolicy cho AppDomain để các thread được khởi tạo sau đó sẽ đc tự động gán WindowsPrincipal
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            // chuyển sang security principal mới hay không, điều kiện này đặt ra để hạn chế phía sử dụng phương thức này thiết lập lại liên tục
            if (!(Thread.CurrentPrincipal is SecurityPrinciple))
            {
                // tạo instance mới của security principal
                SecurityPrinciple theSecurityPrincipal = SetSecurityValues(roles, securityRights, user);
                // tham chiếu security principal hiện tại để cung cấp cho phía sử dụng phương thức này
                IPrincipal currentSecurityPrincipal = Thread.CurrentPrincipal;
                // thiết lập security principal vừa được tạo cho thread hiện tại 
                Thread.CurrentPrincipal = theSecurityPrincipal;
                // trả ra security principal hiện tại
                return currentSecurityPrincipal;
            }
            else // nếu không thiết lập lại security principal
                return null;
        }

        /// <summary>
        /// phục hồi security principal gốc, việc này thực hiện khi người dùng đăng xuất khỏi ứng dụng
        /// </summary>
        /// <param name="originalPrincipal">truyền đối tượng gốc dạng IPrincipal nhận trả về từ hàm SetSecurityPrincipal ở trên</param>
        public static void RestoreSecurityPrincipal(IPrincipal originalPrincipal)
        {
            Thread.CurrentPrincipal = originalPrincipal;
        }

        /// <summary>
        /// truy cập vào IIdentity của người dùng cụ thể, cụ thể là các thuộc tính của bản thân đối tượng người dùng        
        /// </summary>
        public IIdentity Identity
        {
            get { return TheUserIdentity; }
        }

        /// <summary>
        /// kiểm tra xem người dùng có role này không
        /// </summary>
        /// <param name="Role"></param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            return Roles.ContainsValue(role);
        }

        /// <summary>
        /// kiểm tra xem người dùng có quyền truy cập này không
        /// </summary>
        /// <param name="Permission">quyền truy cập muốn kiểm tra</param>
        /// <returns></returns>
        public static bool HasPermission(string permission)
        {
            return SecurityRights.ContainsValue(permission);
        }
    }

    /// <summary>
    /// tạo đối tượng IIdentity sẽ được gắn với IPrincipal và cung cấp các thông tin định danh về người dùng sau khi được 
    /// hệ thống xác thực    
    /// </summary>
    public class UserIdentity : IIdentity
    {
        private Hashtable UserInfo;
        private UserIdentity(Hashtable userInfo)
        {
            UserInfo = userInfo;
        }

        /// <summary>
        /// Tạo instance của đối tượng UserIdentity để chứa các thông tin định danh của người dùng sau khi được hệ thống xác thực
        /// để gắn với đối tượng dạng IPrincipal ở trên, chỉ cho phép tạo ở đây!        
        /// </summary>
        /// <param name="userInfo">thông tin định danh người dùng mà hệ thống cung cấp sau quá trình xác thực</param>
        /// <returns>1 đối tượng dạng IIdentity</returns>
        public static UserIdentity CreateUserIdentity(Hashtable userInfo)
        {
            return new UserIdentity(userInfo);
        }

        public string Name
        {
            get { return Convert.ToString(UserInfo["UserName"], CultureInfo.InvariantCulture).Trim(); }
        }

        public string[] GetPropertyNames()
        {
            string[] PropertyNames = new string[UserInfo.Count];
            int Count = 0;

            foreach (object Key in UserInfo.Keys)
                PropertyNames[Count++] = (string)Key;

            return PropertyNames;
        }

        public string GetProperty(string PropertyName)
        {
            return Convert.ToString(UserInfo[PropertyName], CultureInfo.InvariantCulture);
        }

        public void SetProperty(string PropertyName, string PropertyValue)
        {
            UserInfo[PropertyName] = PropertyValue;
        }

        /// <summary>
        /// kiểu xác thực mà hệ thống thực hiện, ở đây trình bày ra đây để đảm bảo implement đầy đủ IIdentity        
        /// </summary>
        public string AuthenticationType
        {
            get
            {
                return "";
            }
        }

        // mục đích như trên
        public bool IsAuthenticated
        {
            get
            {
                return true;
            }
        }
    }
    
}
