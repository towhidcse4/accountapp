﻿using Accounting.Core.Common;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;
using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Accounting.Core.DAO
{
    public class ReportProcedureSDS : SqlDataProvider
    {
        private DateTime endOfDay(DateTime todate)
        {
            TimeSpan ts = new TimeSpan(23, 59, 59);
            todate = todate.Date + ts;
            return todate;
        }

        #region count voucher fix by cuongpv
        public int CountVoucher(DateTime fromDate, DateTime toDate, int typeCount)
        {
            int totalVoucher = 0;
            using (SqlCommand dbCmd = new SqlCommand("Proc_CountVoucher", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@TypeCount", typeCount));
                    totalVoucher = dbCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return totalVoucher;
        }
        #endregion

        #region[Bank_SearchByCode]
        public List<Bank> Bank_SearchByCode(string strBankCode)
        {
            List<Bank> records = new List<Bank>();
            using (SqlCommand dbCmd = new SqlCommand("sp_Bank_GetByCode", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@BankCode", strBankCode));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<Bank>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }

        public List<GeneralLedger> ReportS07DNSQL(DateTime fromDate, DateTime toDate, string currencyID, string account)
        {
            toDate = endOfDay(toDate);
            List<GeneralLedger> records = new List<GeneralLedger>();
            using (SqlCommand dbCmd = new SqlCommand("sp_SoQuyTienMat", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@p_TuNgay", fromDate));
                    dbCmd.Parameters.Add(new SqlParameter("@p_DenNgay", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@p_CurrencyID", currencyID));
                    dbCmd.Parameters.Add(new SqlParameter("@p_Account", account));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GeneralLedger>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }

        #endregion
        public List<BalanceAccountF01> GetBalanceAccountF01(DateTime fromDate, DateTime toDate, int account, bool IsBalanceBothSide)
        {
            toDate = endOfDay(toDate);
            List<BalanceAccountF01> records = new List<BalanceAccountF01>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetBalanceAccountF01", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@MaxAccountGrade", account));
                    dbCmd.Parameters.Add(new SqlParameter("@IsBalanceBothSide", IsBalanceBothSide));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<BalanceAccountF01>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }
        public List<B09BlockVI> GetB09_SecVI(DateTime fromDate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            DateTime startDateAgo = new DateTime(fromDate.Year - 1, fromDate.Month, fromDate.Day);
            List<B09BlockVI> records = new List<B09BlockVI>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetB09_SecVI", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@StartDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@EndDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@StartDateAgo", startDateAgo));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<B09BlockVI>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }
        public List<HealthFinanceEnterpriseChart> GetHealthFinanceEnterpriseChart(DateTime fromDate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            List<HealthFinanceEnterpriseChart> records = new List<HealthFinanceEnterpriseChart>();
            using (SqlCommand dbcmd = new SqlCommand("Proc_GetHealthFinanceChart", GetConnection()))
            {
                try
                {
                    dbcmd.CommandType = CommandType.StoredProcedure;
                    dbcmd.Parameters.Add(new SqlParameter("@FromDate", fromDate));
                    dbcmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    using (SqlDataReader reader = dbcmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<HealthFinanceEnterpriseChart>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<HealthFinanceEnterpriseMoney> GetHealthFinanceEnterpriseMoney(DateTime fromDate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            List<HealthFinanceEnterpriseMoney> records = new List<HealthFinanceEnterpriseMoney>();
            using (SqlCommand dbcmd = new SqlCommand("Proc_GetHealthFinanceMoney", GetConnection()))
            {
                try
                {
                    dbcmd.CommandType = CommandType.StoredProcedure;
                    dbcmd.Parameters.Add(new SqlParameter("@FromDate", fromDate));
                    dbcmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    using (SqlDataReader reader = dbcmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<HealthFinanceEnterpriseMoney>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<FU_GetCashDetailBook> GetFU_GetCashDetailBook(DateTime fromDate, DateTime toDate, string account, string BranchID, string CurrencyID)
        {
            toDate = endOfDay(toDate);
            List<FU_GetCashDetailBook> records = new List<FU_GetCashDetailBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_FU_GetCashDetailBook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", "," + account + ","));
                    if (BranchID != "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", CurrencyID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<FU_GetCashDetailBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }

        public List<SoTheoDoiThanhToanNgoaiTe> GetSoTheoDoiThanhToanNgoaiTe(DateTime fromDate, DateTime toDate, string CurrencyID, string account, string strAccountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<SoTheoDoiThanhToanNgoaiTe> records = new List<SoTheoDoiThanhToanNgoaiTe>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiThanhToanBangNgoaiTe", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@TypeMoney", CurrencyID));
                    if (account == "Tất cả")
                        dbCmd.Parameters.Add(new SqlParameter("@Account", "all"));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@Account", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + strAccountingObjectID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SoTheoDoiThanhToanNgoaiTe>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }
        public List<TongHopCongNoNhanVien> GetTongHopCongNoNhanVien(DateTime fromDate, DateTime toDate, string account, string strAccountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<TongHopCongNoNhanVien> records = new List<TongHopCongNoNhanVien>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_TongHopCongNoNhanVien", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (account == "Tất cả")
                        dbCmd.Parameters.Add(new SqlParameter("@Account", "all"));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@Account", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + strAccountingObjectID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<TongHopCongNoNhanVien>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }
        public List<BookDepositListDetail> GetBookDepositListDetail(string BranchID, bool IncludeDependentBranch, DateTime fromDate, DateTime toDate, string account, string CurrencyID, string BankAccountID, bool IsSimilarSum, bool IsSoftOrderVoucher)
        {
            toDate = endOfDay(toDate);
            List<BookDepositListDetail> records = new List<BookDepositListDetail>();
            IBankAccountDetailService _IBankAccountDetailService;
            using (SqlCommand dbCmd = new SqlCommand("Proc_BA_GetBookDepositListDetail", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID != "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", CurrencyID));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarSum", IsSimilarSum));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSoftOrderVoucher", IsSoftOrderVoucher));
                    if (BankAccountID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", DBNull.Value));
                    else
                    {
                        BankAccountDetail bankaccount = new BankAccountDetail();
                        _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
                        bankaccount = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == BankAccountID);
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", bankaccount.ID));
                    }
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<BookDepositListDetail>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }
        public List<OverBalanceBook> GetBangKeSoDuNganHang(DateTime fromDate, DateTime toDate, string account, string BranchID, string CurrencyID, string BankAccountID, bool IsWorkingWithManagementBook, bool IncludeDependentBranch)
        {
            toDate = endOfDay(toDate);
            List<OverBalanceBook> records = new List<OverBalanceBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_BA_GetOverBalanceBook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID != "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    if (BankAccountID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", new Guid(BankAccountID)));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", CurrencyID));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsWorkingWithManagementBook", IsWorkingWithManagementBook));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<OverBalanceBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return records;
        }
        public List<ListInventoryItemID> GetSoChiTietBanHang(DateTime fromDate, DateTime toDate, string InventoryItemID, string BranchID, string AccountObjectID, string OrganizationUnitID, bool IncludeDependentBranch, ref Decimal iSumGiaVon, ref List<ListInventoryItemID> list_sum_goc)
        {
            toDate = endOfDay(toDate);
            List<ListInventoryItemID> records = new List<ListInventoryItemID>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_GetSalesBookDetail", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (BranchID != "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@InventoryItemID", "," + InventoryItemID + ","));
                    if (OrganizationUnitID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@OrganizationUnitID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@OrganizationUnitID", new Guid(OrganizationUnitID)));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", AccountObjectID));
                    dbCmd.Parameters.Add(new SqlParameter("@SumGiaVon", SqlDbType.Decimal)).Direction = ParameterDirection.Output;
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<ListInventoryItemID>(reader);
                        reader.NextResult();
                        if (reader.HasRows)
                            list_sum_goc = Helpers.GetPOBaseTListFromReader<ListInventoryItemID>(reader);
                    }
                    iSumGiaVon = 0;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<SalesDiaryBook> GetNhatKyBanHang(DateTime fromDate, DateTime toDate, int IsDisplay)
        {
            toDate = endOfDay(toDate);
            List<SalesDiaryBook> records = new List<SalesDiaryBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_GetSalesDiaryBook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsDisplayNotReceiptOnly", IsDisplay));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SalesDiaryBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }


        public List<SA_GetReceivableSummary> GetTongCongNoPhaiThu(DateTime fromdate, DateTime toDate, string currencyID,
            string account, string accountingObjectID, string BranchID, bool IsShowInPeriodOnly)
        {
            toDate = endOfDay(toDate);
            List<SA_GetReceivableSummary> records = new List<SA_GetReceivableSummary>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_GetReceivableSummary", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currencyID));
                    dbCmd.Parameters.Add(new SqlParameter("@IsShowInPeriodOnly", IsShowInPeriodOnly));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_GetReceivableSummary>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<SA_ReceivableDetail> GetCHICONGNOPHAITHUKH(DateTime fromdate, DateTime toDate, string currencyID,
            string account, string accountingObjectID, bool IsSimilarSum)
        {
            toDate = endOfDay(toDate);
            List<SA_ReceivableDetail> records = new List<SA_ReceivableDetail>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_ReceivableDetail", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currencyID));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarSum", IsSimilarSum));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_ReceivableDetail>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<SA_ReceivableDetail> GetCHICONGNOPHAITHUNV(DateTime fromdate, DateTime toDate,
            string account, string accountingObjectID, bool IsSimilarSum)
        {
            toDate = endOfDay(toDate);
            List<SA_ReceivableDetail> records = new List<SA_ReceivableDetail>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoChiTietCongNoNhanVien", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (account == "Tất cả")
                        dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", "all"));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_ReceivableDetail>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<StatisticsCodeBook> GetCHITIETMATHONGKE(DateTime fromdate, DateTime toDate, string statisticscode, string account)
        {
            toDate = endOfDay(toDate);
            List<StatisticsCodeBook> records = new List<StatisticsCodeBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoTheoDoiChiTietTheoMaThongKe", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@StatisticsCodeID", statisticscode));
                    dbCmd.Parameters.Add(new SqlParameter("@Account", account));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<StatisticsCodeBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<CostSetBook> GETCCHITIETDOITUONGTHCP(DateTime fromdate, DateTime toDate, string costsetid, string account)
        {
            toDate = endOfDay(toDate);
            List<CostSetBook> records = new List<CostSetBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoTheoDoiChiTietTheoDoiTuongTHCP", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CostSetID", costsetid));
                    dbCmd.Parameters.Add(new SqlParameter("@Account", account));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<CostSetBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        //namnh
        public List<CTBook> SOTONGHOPCONGTRINH(DateTime fromdate, DateTime toDate, string costsetid)
        {
            toDate = endOfDay(toDate);
            List<CTBook> records = new List<CTBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoTongHopCongTrinh", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CostSetID", costsetid));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<CTBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<CTBook> SOCHITIETCONGTRINH(DateTime fromdate, DateTime toDate, string costsetid)
        {
            toDate = endOfDay(toDate);
            List<CTBook> records = new List<CTBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoChiTietCongTrinh", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CostSetID", costsetid));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<CTBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<AccountingObjectEmployees> GETDOANHTHUNV(DateTime fromdate, DateTime toDate, string AccountingObjectCode)
        {
            toDate = endOfDay(toDate);
            List<AccountingObjectEmployees> records = new List<AccountingObjectEmployees>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoChiTietDoanhThuTheoNhanVien", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountingObjectCode", AccountingObjectCode));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<AccountingObjectEmployees>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        //addbymran
        public List<AccountingObjectMaterial_Book> getTongHopDOANHTHUNV(DateTime fromdate, DateTime toDate, string AccountingObjectID, string MaterialGoodsID)
        {
            toDate = endOfDay(toDate);
            List<AccountingObjectMaterial_Book> records = new List<AccountingObjectMaterial_Book>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoTongHopDoanhThuTheoNhanVien", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountingObjectID", AccountingObjectID));
                    dbCmd.Parameters.Add(new SqlParameter("@MaterialGoodsID", MaterialGoodsID));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<AccountingObjectMaterial_Book>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        //namnh
        public List<AccountingObjectSalary> GETSALARYNV(int BeginMonth, int BeginYear, int EndMonth, int EndYear, string AccountingObjectCode)
        {

            List<AccountingObjectSalary> records = new List<AccountingObjectSalary>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoTongHopLuongNhanVien", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@BeginMonth", BeginMonth));
                    dbCmd.Parameters.Add(new SqlParameter("@BeginYear", BeginYear));
                    dbCmd.Parameters.Add(new SqlParameter("@EndMonth", EndMonth));
                    dbCmd.Parameters.Add(new SqlParameter("@EndYear", EndYear));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountingObjectCode", AccountingObjectCode));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<AccountingObjectSalary>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        //add by Mr An
        public List<CostSetExpense> GeTDOITUONGTHCPTheoKMCP(DateTime fromdate, DateTime toDate, string costsetid, string expenseItemid)
        {
            toDate = endOfDay(toDate);
            List<CostSetExpense> records = new List<CostSetExpense>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoTheoDoiDoiTuongTHCP", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CostSetID", costsetid));
                    dbCmd.Parameters.Add(new SqlParameter("@ExpenseItemID", expenseItemid));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<CostSetExpense>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        //end of Mr AN

        public List<F02bDNNBook> GETDATEF02bDNN(DateTime fromdate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            List<F02bDNNBook> records = new List<F02bDNNBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoDangKyChungTuGhiSoS02bDNN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<F02bDNNBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<BookNhatKySoCai> GETDATENHATKYSOCAI(DateTime fromdate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            List<BookNhatKySoCai> records = new List<BookNhatKySoCai>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_NhatKySoCai", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<BookNhatKySoCai>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<CostSetBook> GETGVOUCHERS02bDNN(DateTime fromdate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            List<CostSetBook> records = new List<CostSetBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoDangKyChungTuGhiSoS02bDNN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<CostSetBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<ChungTuGhiSoS02bDNN> CTGSS02bDNN(int typeprint)
        {

            List<ChungTuGhiSoS02bDNN> records = new List<ChungTuGhiSoS02bDNN>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_ChungTuGhiSoS02bDNN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@typeprint", typeprint));

                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<ChungTuGhiSoS02bDNN>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<SA_GetReceivableSummaryByGroup> GetTongCongNoPhaiThuTheoNhomKH(DateTime fromdate, DateTime toDate, string currencyID,
            string account, string accountingObjectID, string AccountObjectGroupID)
        {
            toDate = endOfDay(toDate);
            List<SA_GetReceivableSummaryByGroup> records = new List<SA_GetReceivableSummaryByGroup>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_GetReceivableSummaryByGroup", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currencyID));
                    if (AccountObjectGroupID == null)
                        dbCmd.Parameters.Add(new SqlParameter("@AccountObjectGroupID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@AccountObjectGroupID", AccountObjectGroupID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_GetReceivableSummaryByGroup>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<GetChiTietPhaiThuKhachHangTheoMatHang> GetChiTietPhaiThuKhachHangTheoMatHang(DateTime fromdate, DateTime toDate, string currencyID,
           string account, string accountingObjectID, string AccountObjectGroupID, string materialGoodID)
        {
            toDate = endOfDay(toDate);
            List<GetChiTietPhaiThuKhachHangTheoMatHang> records = new List<GetChiTietPhaiThuKhachHangTheoMatHang>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoChiTietPhaiThuKhachHangTheoMatHang", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@MaterialsGoodID", "," + materialGoodID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currencyID));
                    if (AccountObjectGroupID == null)
                        dbCmd.Parameters.Add(new SqlParameter("@AccountObjectGroupID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@AccountObjectGroupID", AccountObjectGroupID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GetChiTietPhaiThuKhachHangTheoMatHang>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<GetChiTietBanHangTheoNhanVien> GetChiTietBanHangTheoNhanVien(DateTime fromdate, DateTime toDate,
           string EmployeeID, string accountingObjectID, string materialGoodID)
        {
            toDate = endOfDay(toDate);
            List<GetChiTietBanHangTheoNhanVien> records = new List<GetChiTietBanHangTheoNhanVien>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SoChiTietBanHangTheoNhanVien", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@MaterialsGoodID", "," + materialGoodID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GetChiTietBanHangTheoNhanVien>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<SA_SoTheoDoiLaiLoTheoMatHang> GetSoTheoDoiLaiLoTheoMatHang(DateTime fromDate, DateTime toDate, string strMaterialGoodsID)
        {
            toDate = endOfDay(toDate);
            List<SA_SoTheoDoiLaiLoTheoMatHang> records = new List<SA_SoTheoDoiLaiLoTheoMatHang>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiLaiLoTheoMatHang", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@MaterialGoodsID", strMaterialGoodsID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_SoTheoDoiLaiLoTheoMatHang>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<S11DNN_PBCCDC> GetSoCongCuDungCu(DateTime fromDate, DateTime toDate, string strToolID)
        {
            toDate = endOfDay(toDate);
            List<S11DNN_PBCCDC> records = new List<S11DNN_PBCCDC>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoCongCuDungCu", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@ToolsIDs", "," + strToolID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<S11DNN_PBCCDC>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<SA_SoTheoDoiLaiLoTheoHoaDon> GetSoTheoDoiLaiLoTheoHoaDon(DateTime fromDate, DateTime toDate)
        {
            toDate = endOfDay(toDate);
            List<SA_SoTheoDoiLaiLoTheoHoaDon> records = new List<SA_SoTheoDoiLaiLoTheoHoaDon>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiLaiLoTheoHoaDon", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_SoTheoDoiLaiLoTheoHoaDon>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }

        public List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon> GetSoTheoDoiCongNoPhaiThuTheoHoaDon(DateTime fromDate, DateTime toDate, string strAccountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon> records = new List<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiCongNoPhaiThuTheoHoaDon", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + strAccountingObjectID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_SoTheoDoiCongNoPhaiThuTheoHoaDon>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }

        public List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang> GetSoTheoDoiCongNoPhaiThuTheoMatHang(DateTime fromDate, DateTime toDate, string strMaterialGoodsID)
        {
            toDate = endOfDay(toDate);
            List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang> records = new List<SA_SoTheoDoiCongNoPhaiThuTheoMatHang>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiCongNoPhaiThuTheoMatHang", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@MaterialGoodsID", strMaterialGoodsID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_SoTheoDoiCongNoPhaiThuTheoMatHang>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }

        public List<SA_SoTheoDoiLaiLoTheoHopDongBan> GetSoTheoDoiLaiLoTheoHopDongBan(DateTime fromDate, DateTime toDate, string strAccountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<SA_SoTheoDoiLaiLoTheoHopDongBan> records = new List<SA_SoTheoDoiLaiLoTheoHopDongBan>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiLaiLoTheoHopDongBan", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + strAccountingObjectID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_SoTheoDoiLaiLoTheoHopDongBan>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }

        public List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan> GetSoTheoDoiCongNoTheoHopDongBan(DateTime fromDate, DateTime toDate, string strAccountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan> records = new List<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromDate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + strAccountingObjectID + ","));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan>(reader);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return records;
        }

        public List<PO_PayableDetail> GetTongCongNoPhaiTraTheoNCC_bck18122018(DateTime fromdate, DateTime toDate, string currencyID,
            string account, string accountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<PO_PayableDetail> records = new List<PO_PayableDetail>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_PO_PayableDetail", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (account == null)
                        dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currencyID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<PO_PayableDetail>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<PO_PayableDetail> GetTongHopCongNoPhaiTraNCC(DateTime fromdate, DateTime toDate, string currencyID,
            string account, string accountingObjectID)
        {
            toDate = endOfDay(toDate);
            List<PO_PayableDetail> records = new List<PO_PayableDetail>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_PO_SummaryPayable", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (account == null)
                        dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", "," + accountingObjectID + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currencyID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<PO_PayableDetail>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<PO_DiaryBook> GetSoNhatKyMuaHang(DateTime fromdate, DateTime toDate, int IsNotPaid, string BranchID, string IncludeDependentBranch)
        {
            toDate = endOfDay(toDate);
            List<PO_DiaryBook> records = new List<PO_DiaryBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_PO_DiaryBook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", BranchID));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@IsNotPaid", IsNotPaid));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<PO_DiaryBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<GL_GetBookDetailPaymentByAccountNumber> GetSoChiTietCacTaiKhoan(string BranchID, string IncludeDependentBranch,
            DateTime fromdate, DateTime toDate, string AccountNumber, int GroupTheSameItem)//edit by cuongpv bo tham so (, string CurrencyId) theo issue: SDSACC-4949
        {
            toDate = endOfDay(toDate);
            List<GL_GetBookDetailPaymentByAccountNumber> records = new List<GL_GetBookDetailPaymentByAccountNumber>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GL_GetBookDetailPaymentByAccountNumber", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", BranchID));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", "," + AccountNumber + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@GroupTheSameItem", GroupTheSameItem));
                    //dbCmd.Parameters.Add(new SqlParameter("@CurrencyId", CurrencyId)); //comment by cuongpv
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GL_GetBookDetailPaymentByAccountNumber>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<GL_GetBookDetailPaymentByAccountNumber> GetSoChiTietCacTaiKhoan1(string BranchID, string IncludeDependentBranch,
           DateTime fromdate, DateTime toDate, string AccountNumber, int GroupTheSameItem)//edit by cuongpv bo tham so (, string CurrencyId) theo issue: SDSACC-4949
        {
            toDate = endOfDay(toDate);
            List<GL_GetBookDetailPaymentByAccountNumber> records = new List<GL_GetBookDetailPaymentByAccountNumber>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GL_GetBookDetailPaymentByAccountNumber1", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", BranchID));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", "," + AccountNumber + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@GroupTheSameItem", GroupTheSameItem));
                    //dbCmd.Parameters.Add(new SqlParameter("@CurrencyId", CurrencyId)); //comment by cuongpv
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GL_GetBookDetailPaymentByAccountNumber>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<S02C1DNN> GetSoCaiS02c1DNN(string BranchID, int IncludeDependentBranch,
            DateTime fromdate, DateTime toDate, string AccountNumber, int IsSimilarSum, ref List<S02C1DNN> listHeader)
        {
            toDate = endOfDay(toDate);
            List<S02C1DNN> records = new List<S02C1DNN>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_S02C1DNN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", BranchID));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@StartDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", "," + AccountNumber + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarSum", IsSimilarSum));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            listHeader = Helpers.GetPOBaseTListFromReader<S02C1DNN>(reader);
                        reader.NextResult();
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<S02C1DNN>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<GL_GetGLAccountLedgerDiaryBook> GetSoCaiS03bDN(string BranchID, int IncludeDependentBranch,
            DateTime fromdate, DateTime toDate, string AccountNumber, int IsSimilarSum, ref List<GL_GetGLAccountLedgerDiaryBook> listHeader)
        {
            toDate = endOfDay(toDate);
            List<GL_GetGLAccountLedgerDiaryBook> records = new List<GL_GetGLAccountLedgerDiaryBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GL_GetGLAccountLedgerDiaryBook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", BranchID));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@StartDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", "," + AccountNumber + ","));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarSum", IsSimilarSum));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            listHeader = Helpers.GetPOBaseTListFromReader<GL_GetGLAccountLedgerDiaryBook>(reader);
                        reader.NextResult();
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GL_GetGLAccountLedgerDiaryBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<GetB01_DN> GetB01_DN(DateTime fromdate, DateTime toDate, string BranchID, int IncludeDependentBranch, int IsSimilarBranch, int IsB01bDNN, int isPrintByYear, DateTime PrevFromDate, DateTime PrevToDate)
        {
            toDate = endOfDay(toDate);
            List<GetB01_DN> records = new List<GetB01_DN>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetB01_DN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (BranchID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", BranchID));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", DBNull.Value));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarBranch", IsSimilarBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@IsB01bDNN", IsB01bDNN));
                    dbCmd.Parameters.Add(new SqlParameter("@isPrintByYear", isPrintByYear));
                    dbCmd.Parameters.Add(new SqlParameter("@PrevFromDate", PrevFromDate));
                    dbCmd.Parameters.Add(new SqlParameter("@PrevToDate", PrevToDate));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GetB01_DN>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<GL_GeneralDiaryBook_S03a> Get_GL_GeneralDiaryBook_S03a(DateTime fromdate, DateTime toDate, int GroupTheSameItem, int IsShowAccumAmount)
        {
            toDate = endOfDay(toDate);
            List<GL_GeneralDiaryBook_S03a> records = new List<GL_GeneralDiaryBook_S03a>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GL_GeneralDiaryBook_S03a", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@GroupTheSameItem", GroupTheSameItem));
                    dbCmd.Parameters.Add(new SqlParameter("@IsShowAccumAmount", IsShowAccumAmount));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GL_GeneralDiaryBook_S03a>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<RContract> GetContract(DateTime fromdate, DateTime toDate, bool isContractSale)
        {
            toDate = endOfDay(toDate);
            List<RContract> contractsale = new List<RContract>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_EM_GetContract", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsContractSale", isContractSale));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            contractsale = Helpers.GetPOBaseTListFromReader<RContract>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return contractsale;
        }
        public List<PO_GetDetailBook> Get_SoChiTietMuaHang(DateTime fromdate, DateTime toDate, string AccountObjectID, string MaterialGoodsID)
        {
            toDate = endOfDay(toDate);
            List<PO_GetDetailBook> records = new List<PO_GetDetailBook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_PO_GetDetailBook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    if (AccountObjectID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", AccountObjectID));
                    if (MaterialGoodsID == "")
                        dbCmd.Parameters.Add(new SqlParameter("@MaterialGoods", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@MaterialGoods", MaterialGoodsID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<PO_GetDetailBook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<SA_GetDetailPayS12> Get_SoChiTietThanhToan(DateTime fromdate, DateTime toDate, string account, string currency, string AccountObjectID)
        {
            toDate = endOfDay(toDate);
            List<SA_GetDetailPayS12> records = new List<SA_GetDetailPayS12>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_SA_GetDetailPayS12", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@fromdate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@todate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@account", account));
                    dbCmd.Parameters.Add(new SqlParameter("@currency", currency));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", AccountObjectID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<SA_GetDetailPayS12>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<PO_PayDetailNCC> Get_SoChitietPhaTra_NCC(DateTime fromdate, DateTime toDate, string account, string currency, string AccountObjectID)
        {
            toDate = endOfDay(toDate);
            List<PO_PayDetailNCC> records = new List<PO_PayDetailNCC>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_PO_PayDetailNCC", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", AccountObjectID));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currency));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<PO_PayDetailNCC>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<PO_PayDetailNCC1> Get_SoChitietPhaTra_NCC1(DateTime fromdate, DateTime toDate, string account, string currency, string AccountObjectID)
        {
            toDate = endOfDay(toDate);
            List<PO_PayDetailNCC1> records = new List<PO_PayDetailNCC1>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_PO_PayDetailNCC1", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", AccountObjectID));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currency));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<PO_PayDetailNCC1>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<CACashBookInCABook> Get_SoQuyTM(DateTime fromdate, DateTime toDate, string account, string currency)
        {
            toDate = endOfDay(toDate);
            List<CACashBookInCABook> records = new List<CACashBookInCABook>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetCACashBookInCABook", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currency));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<CACashBookInCABook>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<GLR_GetS03a1_DN> Get_SoNhatKyThuTien(DateTime fromdate, DateTime toDate, string account, string currency, string BankAccountID, int IsSimilarSum)
        {
            toDate = endOfDay(toDate);
            List<GLR_GetS03a1_DN> records = new List<GLR_GetS03a1_DN>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GLR_GetS03a1_DN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currency));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    if (BankAccountID == "" || BankAccountID == "Tất cả")
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", new Guid(BankAccountID)));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarSum", IsSimilarSum));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GLR_GetS03a1_DN>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<GLR_GetS03a1_DN> Get_SoNhatKyChiTien(DateTime fromdate, DateTime toDate, string account, string currency, string BankAccountID, int IsSimilarSum)
        {
            toDate = endOfDay(toDate);
            List<GLR_GetS03a1_DN> records = new List<GLR_GetS03a1_DN>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GLR_GetS03a2_DN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CurrencyID", currency));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountNumber", account));
                    if (BankAccountID == "" || BankAccountID == "Tất cả")
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BankAccountID", new Guid(BankAccountID)));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarSum", IsSimilarSum));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GLR_GetS03a1_DN>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }

        public List<GLBookDetailMoneyBorrow> Get_SoChiTietTienVay(DateTime fromdate, DateTime toDate, string AccountID, string AccountObjectID)
        {
            toDate = endOfDay(toDate);
            List<GLBookDetailMoneyBorrow> records = new List<GLBookDetailMoneyBorrow>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetGLBookDetailMoneyBorrow", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountID", AccountID));
                    dbCmd.Parameters.Add(new SqlParameter("@AccountObjectID", AccountObjectID));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            records = Helpers.GetPOBaseTListFromReader<GLBookDetailMoneyBorrow>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return records;
        }
        public List<MBTellerPaperReport> GetAccreditative(string bankCode)
        {
            List<MBTellerPaperReport> tellerpaper = new List<MBTellerPaperReport>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_Accreditative", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@BankCode", bankCode));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<MBTellerPaperReport>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }
        public List<FAIncrementReport> GetAccreditative1(string bankCode)
        {
            List<FAIncrementReport> faIncrement = new List<FAIncrementReport>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetAccreditative", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@BankCode", bankCode));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            faIncrement = Helpers.GetPOBaseTListFromReader<FAIncrementReport>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return faIncrement;
        }
        public List<TIIncrementReport> GetAccreditative2(string bankCode)
        {
            List<TIIncrementReport> tiIncrement = new List<TIIncrementReport>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetAccreditative1", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@BankCode", bankCode));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tiIncrement = Helpers.GetPOBaseTListFromReader<TIIncrementReport>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tiIncrement;
        }
        public List<Proc_GetB02_DN_Model> GetProc_GetB02_DN(DateTime fromdate, DateTime toDate, int IncludeDependentBranch, string BranchID)
        {
            toDate = endOfDay(toDate);
            List<Proc_GetB02_DN_Model> tellerpaper = new List<Proc_GetB02_DN_Model>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetB02_DN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@PrevFromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@PrevToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    if (string.IsNullOrEmpty(BranchID))
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<Proc_GetB02_DN_Model>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }
        public List<Proc_GetB03_DN_Model> GetProc_GetB03_DN(DateTime fromdate, DateTime toDate, int IncludeDependentBranch, string BranchID)
        {
            toDate = endOfDay(toDate);
            List<Proc_GetB03_DN_Model> tellerpaper = new List<Proc_GetB03_DN_Model>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetB03_DN", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@PrevFromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@PrevToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    if (string.IsNullOrEmpty(BranchID))
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<Proc_GetB03_DN_Model>(reader);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }

        public void UpdateImport(DateTime fromdate, DateTime toDate, Guid cpperiodID)
        {
            toDate = endOfDay(toDate);
            using (SqlCommand dbCmd = new SqlCommand("UpdateImport", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CPPeriodID", cpperiodID));
                    dbCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void UpdateExport(DateTime fromdate, DateTime toDate, Guid cpperiodID)
        {
            toDate = endOfDay(toDate);
            using (SqlCommand dbCmd = new SqlCommand("UpdateExport", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@CPPeriodID", cpperiodID));
                    dbCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<BangKeMuaVao_Model> GetProc_GetBkeInv_Buy(DateTime fromdate, DateTime toDate, int IsSimilarBranch, int IncludeDependentBranch, string BranchID)
        {
            toDate = endOfDay(toDate);
            List<BangKeMuaVao_Model> tellerpaper = new List<BangKeMuaVao_Model>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetBkeInv_Buy", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (string.IsNullOrEmpty(BranchID))
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarBranch", IsSimilarBranch));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<BangKeMuaVao_Model>(reader);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }
        public List<BangKeMuaVao_Model> GetProc_GetBkeInv_BuyTax(DateTime fromdate, DateTime toDate, int IsSimilarBranch, int IncludeDependentBranch, string BranchID, Int16 isKhauTru)
        {
            toDate = endOfDay(toDate);
            List<BangKeMuaVao_Model> tellerpaper = new List<BangKeMuaVao_Model>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetBkeInv_Buy_Tax", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (string.IsNullOrEmpty(BranchID))
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarBranch", IsSimilarBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@KhauTru", isKhauTru));//add by cuongpv: them dk check khau tru thue hay ko
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<BangKeMuaVao_Model>(reader);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }

        public List<BangKeBanRa_Model> GetProc_GetBkeInv_Sale(DateTime fromdate, DateTime toDate, int IsSimilarBranch, int IncludeDependentBranch, string BranchID)
        {
            toDate = endOfDay(toDate);
            List<BangKeBanRa_Model> tellerpaper = new List<BangKeBanRa_Model>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetBkeInv_Sale", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (string.IsNullOrEmpty(BranchID))
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarBranch", IsSimilarBranch));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<BangKeBanRa_Model>(reader);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }

        public List<BangKeBanRa_Model> GetProc_GetBkeInv_Sale_QT(DateTime fromdate, DateTime toDate, int IsSimilarBranch, int IncludeDependentBranch, string BranchID)
        {
            toDate = endOfDay(toDate);
            List<BangKeBanRa_Model> tellerpaper = new List<BangKeBanRa_Model>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_GetBkeInv_Sale_QT", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    if (string.IsNullOrEmpty(BranchID))
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", DBNull.Value));
                    else
                        dbCmd.Parameters.Add(new SqlParameter("@BranchID", new Guid(BranchID)));
                    dbCmd.Parameters.Add(new SqlParameter("@IncludeDependentBranch", IncludeDependentBranch));
                    dbCmd.Parameters.Add(new SqlParameter("@FromDate", fromdate.Date));
                    dbCmd.Parameters.Add(new SqlParameter("@ToDate", toDate));
                    dbCmd.Parameters.Add(new SqlParameter("@IsSimilarBranch", IsSimilarBranch));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            tellerpaper = Helpers.GetPOBaseTListFromReader<BangKeBanRa_Model>(reader);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return tellerpaper;
        }

        public List<TaxSubmit> GetListTaxSubmit(DateTime inputDate)
        {
            inputDate = endOfDay(inputDate);
            List<TaxSubmit> taxSubmit = new List<TaxSubmit>();
            using (SqlCommand dbCmd = new SqlCommand("Proc_TAX_ListTaxSubmit", GetConnection()))
            {
                try
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbCmd.Parameters.Add(new SqlParameter("@inputDate", inputDate));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            taxSubmit = Helpers.GetPOBaseTListFromReader<TaxSubmit>(reader);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return taxSubmit;
        }
    }
}