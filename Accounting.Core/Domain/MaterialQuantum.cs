

using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialQuantum
    {
        private Guid iD;
        private string materialQuantumCode;
        private string materialQuantumName;
        private Guid? costSetID;
        private string costSetName;
        private bool isActive;
        private bool isSecurity;
        private Guid? materialGoodsID;
        private string materialGoodsName;
        private DateTime? fromDate;
        private DateTime? toDate;
        private string costSetCode;
        public virtual CostSet CostSets { get; set; }
        public virtual string CostSetCode
        {
            get
            {
                //if (CostSets != null)
                //{
                //    return CostSets.CostSetCode;
                //}
                //else
                return costSetCode;
            }
            set { costSetCode = value; }
        }
        public MaterialQuantum()
        {
            MaterialQuantumDetails = new List<MaterialQuantumDetail>();
            CostSets = new CostSet();
        }
        public virtual IList<MaterialQuantumDetail> MaterialQuantumDetails { get; set; }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string MaterialQuantumCode
        {
            get { return materialQuantumCode; }
            set { materialQuantumCode = value; }
        }

        public virtual string MaterialQuantumName
        {
            get { return materialQuantumName; }
            set { materialQuantumName = value; }
        }
        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual string CostSetName
        {
            get { return costSetName; }
            set { costSetName = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual string MaterialGoodsName
        {
            get { return materialGoodsName; }
            set { materialGoodsName = value; }
        }

        public virtual DateTime? FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }

        public virtual DateTime? ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }


    }
}
