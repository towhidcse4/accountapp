using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMDevidend
    {
        private Guid _ID;
        private DateTime _LockDate;
        private Decimal _Amount;
        private Decimal _Quantity;
        private Decimal _FaceValue;
        private Decimal _DevidendPerShare;
        private float _DevidendPerSharePercent;
        private int _TypeID;
        private string _Description;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual DateTime LockDate
        {
            get { return _LockDate; }
            set { _LockDate = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal FaceValue
        {
            get { return _FaceValue; }
            set { _FaceValue = value; }
        }

        public virtual Decimal DevidendPerShare
        {
            get { return _DevidendPerShare; }
            set { _DevidendPerShare = value; }
        }

        public virtual float DevidendPerSharePercent
        {
            get { return _DevidendPerSharePercent; }
            set { _DevidendPerSharePercent = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }


    }
}
