using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountDefault
    {
        private Guid _ID;
        private int _TypeID;
        private string _ColumnName;
        private string _ColumnCaption;
        private string _FilterAccount;
        private string _DefaultAccount;
        private string _ReduceAccount;
        private Boolean _PPType;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string ColumnName
        {
            get { return _ColumnName; }
            set { _ColumnName = value; }
        }

        public virtual string ColumnCaption
        {
            get { return _ColumnCaption; }
            set { _ColumnCaption = value; }
        }

        public virtual string FilterAccount
        {
            get { return _FilterAccount; }
            set { _FilterAccount = value; }
        }

        public virtual string DefaultAccount
        {
            get { return _DefaultAccount; }
            set { _DefaultAccount = value; }
        }

        public virtual string ReduceAccount
        {
            get { return _ReduceAccount; }
            set { _ReduceAccount = value; }
        }

        public virtual Boolean PPType
        {
            get { return _PPType; }
            set { _PPType = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
