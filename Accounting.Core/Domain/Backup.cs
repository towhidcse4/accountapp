using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Backup
    {
        private Guid _ID;
        private string _FileName;
        private string _FilePath;
        private DateTime _CreateDate;
        private Boolean _SyncState;
        private DateTime? _SyncTime;
        private string _SyncVersion;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        public virtual string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }

        public virtual DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }

        public virtual Boolean SyncState
        {
            get { return _SyncState; }
            set { _SyncState = value; }
        }

        public virtual DateTime? SyncTime
        {
            get { return _SyncTime; }
            set { _SyncTime = value; }
        }

        public virtual string SyncVersion
        {
            get { return _SyncVersion; }
            set { _SyncVersion = value; }
        }


    }
}
