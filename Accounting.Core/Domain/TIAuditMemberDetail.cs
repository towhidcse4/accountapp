using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIAuditMemberDetail
    {
        private Guid _ID;
        private Guid? _TIAuditID;
        private Guid? _DepartmentID;
        private Guid? _AccountingObjectID;
        private string _AccountingObjectName;
        private string _AccountingObjectTitle;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? TIAuditID
        {
            get { return _TIAuditID; }
            set { _TIAuditID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string AccountingObjectTitle
        {
            get { return _AccountingObjectTitle; }
            set { _AccountingObjectTitle = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }


    }
}
