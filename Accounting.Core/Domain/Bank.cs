using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Bank
    {
        private Guid iD;
        private string bankCode;
        private string bankName;
        private string bankNameRepresent;
        private string address;
        private string description;
        private int? bankTransactionType;
        private byte[] icon;
        private bool isActive;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string BankCode
        {
            get { return bankCode; }
            set { bankCode = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

      

        public virtual string BankNameRepresent
        {
            get { return bankNameRepresent; }
            set { bankNameRepresent = value; }
        }

        public virtual string Address
        {
            get { return address; }
            set { address = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual int? BankTransactionType
        {
            get { return bankTransactionType; }
            set { bankTransactionType = value; }
        }

        public virtual byte[] Icon
        {
            get { return icon; }
            set { icon = value; }
        }


        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

    }
}
