﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class StatisticsCodeBook
    {
        
        public Guid StatisticsCodeID { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
        public string StatisticsCode { get; set; }
        public string StatisticsCodeName { get; set; }
        public DateTime NgayChungTu { get; set; }
        public string SoChungTu { get; set; }
        public string DienGiai { get; set; }
        public string TK { get; set; }
        public string TKDoiUng { get; set; }
        public decimal SoTienNo { get; set; }
        public decimal SoTienCo { get; set; }
       
    }
    public class StatisticsCode_Period
    {
        public string Period { get; set; }

    }
}
