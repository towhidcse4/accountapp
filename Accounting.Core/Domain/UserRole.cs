using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class UserRole
    {
        private Guid _UserRoleId;
        private Guid _UserId;
        private Guid _RoleId;


        public virtual Guid UserRoleId
        {
            get { return _UserRoleId; }
            set { _UserRoleId = value; }
        }

        public virtual Guid UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public virtual Guid RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }


    }
}
