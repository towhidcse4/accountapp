﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class VoucherDetailForeignCurrency
    {
        public virtual Guid ID { get; set; }
        public virtual Guid GOtherVoucherDetailForeignCurrencyID { get; set; }
        public virtual Guid ReferenceID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual Decimal CreditAmount { get; set; }
        public virtual Decimal CreditAmountOriginal { get; set; }
        public virtual Decimal DebitAmount { get; set; }
        public virtual Decimal DebitAmountOriginal { get; set; }
        public virtual Decimal NewExchangeRate { get; set; }
    }
}
