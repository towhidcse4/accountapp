using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPUncomplete
    {
        private Guid _ID;
        private Guid _CPPeriodID;
        private Guid _CostSetID;
        private Decimal _DirectMaterialAmount;
        private Decimal _DirectLaborAmount;
        private Decimal _GeneralExpensesAmount;
        private Decimal _TotalCostAmount;
        private CostSet _Costset;
        private string _CostsetCode;
        private string _CostsetName;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Decimal DirectMaterialAmount
        {
            get { return _DirectMaterialAmount; }
            set { _DirectMaterialAmount = value; }
        }

        public virtual Decimal DirectLaborAmount
        {
            get { return _DirectLaborAmount; }
            set { _DirectLaborAmount = value; }
        }

        public virtual Decimal GeneralExpensesAmount
        {
            get { return _GeneralExpensesAmount; }
            set { _GeneralExpensesAmount = value; }
        }

        public virtual Decimal TotalCostAmount
        {
            get { return _TotalCostAmount; }
            set { _TotalCostAmount = value; }
        }

        public virtual CostSet Costset
        {
            get { return _Costset; }
            set { _Costset = value; }
        }

        public virtual string CostsetCode
        {
            get { return _CostsetCode; }
            set { _CostsetCode = value; }
        }

        public virtual string CostsetName
        {
            get { return _CostsetName; }
            set { _CostsetName = value; }
        }

        public virtual IList<CPUncompleteDetail> CPUncompleteDetails { get; set; }
        public CPUncomplete()
        {
            CPUncompleteDetails = new List<CPUncompleteDetail>();
        }
    }
}
