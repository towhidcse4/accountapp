﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ContractState
    {
        private int iD;

        public virtual int ID
        {
            get { return iD; }
            set { iD = value; }
        }
        private string contractStateCode;

        public virtual string ContractStateCode
        {
            get { return contractStateCode; }
            set { contractStateCode = value; }
        }
        private string contractStateName;

        public virtual string ContractStateName
        {
            get { return contractStateName; }
            set { contractStateName = value; }
        }
        private string description;

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        private bool isSecurity;

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }
    }
}
