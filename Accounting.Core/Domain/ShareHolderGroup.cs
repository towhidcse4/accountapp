using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ShareHolderGroup
    {
        private Guid iD;
        private string shareHolderGroupCode;
        private string shareHolderGroupName;
        private bool isActive;
        private bool isSecurity;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string ShareHolderGroupCode
        {
            get { return shareHolderGroupCode; }
            set { shareHolderGroupCode = value; }
        }

        public virtual string ShareHolderGroupName
        {
            get { return shareHolderGroupName; }
            set { shareHolderGroupName = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }




    }
}
