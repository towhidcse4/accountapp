using System;

namespace Accounting.Core.Domain
{
    public class TILostDetail
    {
        private Guid _ID;
        private Guid _TILostID;
        private Guid? _MaterialGoodsID;
        private string _Description;
        private Decimal _Quantity;
        private Decimal _Amount;
        private Guid? _StatisticsCodeID;
        private Guid? _TIIncrementID;
        private Guid? _DepartmentID;
        private Decimal _UnitPrice;
        private Boolean _IsIrrationalCost;
        private int? _ConfrontTypeID;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TILostID
        {
            get { return _TILostID; }
            set { _TILostID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual Guid? TIIncrementID
        {
            get { return _TIIncrementID; }
            set { _TIIncrementID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Boolean IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }

        public virtual int? ConfrontTypeID
        {
            get { return _ConfrontTypeID; }
            set { _ConfrontTypeID = value; }
        }


    }
}
