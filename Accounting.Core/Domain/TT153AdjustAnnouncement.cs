﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TT153AdjustAnnouncement
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime _Date;
        private string _No;
        private string _Description;
        private string _GetDetectionObject;
        private string _RespresentationInLaw;
        private int _Status;
        private string _PublishInvoiceNo;
        private DateTime _PublishInvoiceDate;
        private string _AttachFileName;
        private Byte[] _AttachFileContent;

        public TT153AdjustAnnouncement()
        {
            TT153AdjustAnnouncementDetailListInvoices = new List<TT153AdjustAnnouncementDetailListInvoice>();
            TT153AdjustAnnouncementDetails = new List<TT153AdjustAnnouncementDetail>();
        }

        public virtual IList<TT153AdjustAnnouncementDetailListInvoice> TT153AdjustAnnouncementDetailListInvoices { get; set; }
        public virtual IList<TT153AdjustAnnouncementDetail> TT153AdjustAnnouncementDetails { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string GetDetectionObject
        {
            get { return _GetDetectionObject; }
            set { _GetDetectionObject = value; }
        }

        public virtual string RespresentationInLaw
        {
            get { return _RespresentationInLaw; }
            set { _RespresentationInLaw = value; }
        }

        public virtual int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public virtual string PublishInvoiceNo
        {
            get { return _PublishInvoiceNo; }
            set { _PublishInvoiceNo = value; }
        }

        public virtual DateTime PublishInvoiceDate
        {
            get { return _PublishInvoiceDate; }
            set { _PublishInvoiceDate = value; }
        }

        public virtual string AttachFileName
        {
            get { return _AttachFileName; }
            set { _AttachFileName = value; }
        }

        public virtual Byte[] AttachFileContent
        {
            get { return _AttachFileContent; }
            set { _AttachFileContent = value; }
        }

        public virtual string StatusString
        {
            get
            {
                if (_Status == 0) return "Chưa có hiệu lực";
                if (_Status == 1) return "Đã có hiệu lực";
                return null;
            }
        }
    }
}
