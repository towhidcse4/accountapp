using System;

namespace Accounting.Core.Domain
{
    public class TM03TNDNDocument
    {
        private Guid _ID;
        private Guid _TM03TNDNID;
        private string _DocumentName;
        private int? _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM03TNDNID
        {
            get { return _TM03TNDNID; }
            set { _TM03TNDNID = value; }
        }

        public virtual string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
