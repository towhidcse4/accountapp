using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class BudgetItem
    {
        private Guid iD;
        private string budgetItemCode;
        private string budgetItemName;
        private int budgetItemType;
        private string budgetItemTypeVIEW;
        private int grade;
        private bool isActive;
        private string description;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string BudgetItemCode
        {
            get { return budgetItemCode; }
            set { budgetItemCode = value; }
        }

        public virtual string BudgetItemName
        {
            get { return budgetItemName; }
            set { budgetItemName = value; }
        }

        public virtual int BudgetItemType
        {
            get { return budgetItemType; }
            set { budgetItemType = value; }
        }

        public virtual string BudgetItemTypeVIEW
        {
            get { return budgetItemTypeVIEW; }
            set { budgetItemTypeVIEW = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }

    }
}
