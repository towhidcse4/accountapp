using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSTimeSheetDetail
    {
        private Guid _ID;
        private Guid _PSTimeSheetID;
        private Guid? _EmployeeID;
        public virtual string EmployeeCode { get; set; }
        private string _AccountingObjectName;
        private string _AccountingObjectTitle;
        private Guid? _DepartmentID;
        public virtual string DepartmentCode { get; set; }
        private int _FromHour;
        private int _ToHour;
        private string _Day1;
        private string _Day2;
        private string _Day3;
        private string _Day4;
        private string _Day5;
        private string _Day6;
        private string _Day7;
        private string _Day8;
        private string _Day9;
        private string _Day10;
        private string _Day11;
        private string _Day12;
        private string _Day13;
        private string _Day14;
        private string _Day15;
        private string _Day16;
        private string _Day17;
        private string _Day18;
        private string _Day19;
        private string _Day20;
        private string _Day21;
        private string _Day22;
        private string _Day23;
        private string _Day24;
        private string _Day25;
        private string _Day26;
        private string _Day27;
        private string _Day28;
        private string _Day29;
        private string _Day30;
        private string _Day31;
        private Decimal _PaidWorkingDay;
        private Decimal _PaidNonWorkingDay;
        private Decimal _InsuranceReplaceSalaryWorkingDay;
        private Decimal _InsuranceReplaceSalaryTotalRate;
        private Decimal _WorkingDay;
        private Decimal _WeekendDay;
        private Decimal _Holiday;
        private Decimal _WorkingDayNight;
        private Decimal _WeekendDayNight;
        private Decimal _HolidayNight;
        private Decimal _TotalOverTime;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid PSTimeSheetID
        {
            get { return _PSTimeSheetID; }
            set { _PSTimeSheetID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string AccountingObjectTitle
        {
            get { return _AccountingObjectTitle; }
            set { _AccountingObjectTitle = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual int FromHour
        {
            get { return _FromHour; }
            set { _FromHour = value; }
        }

        public virtual int ToHour
        {
            get { return _ToHour; }
            set { _ToHour = value; }
        }

        public virtual string Day1
        {
            get { return _Day1; }
            set { _Day1 = value; }
        }

        public virtual string Day2
        {
            get { return _Day2; }
            set { _Day2 = value; }
        }

        public virtual string Day3
        {
            get { return _Day3; }
            set { _Day3 = value; }
        }

        public virtual string Day4
        {
            get { return _Day4; }
            set { _Day4 = value; }
        }

        public virtual string Day5
        {
            get { return _Day5; }
            set { _Day5 = value; }
        }

        public virtual string Day6
        {
            get { return _Day6; }
            set { _Day6 = value; }
        }

        public virtual string Day7
        {
            get { return _Day7; }
            set { _Day7 = value; }
        }

        public virtual string Day8
        {
            get { return _Day8; }
            set { _Day8 = value; }
        }

        public virtual string Day9
        {
            get { return _Day9; }
            set { _Day9 = value; }
        }

        public virtual string Day10
        {
            get { return _Day10; }
            set { _Day10 = value; }
        }

        public virtual string Day11
        {
            get { return _Day11; }
            set { _Day11 = value; }
        }

        public virtual string Day12
        {
            get { return _Day12; }
            set { _Day12 = value; }
        }

        public virtual string Day13
        {
            get { return _Day13; }
            set { _Day13 = value; }
        }

        public virtual string Day14
        {
            get { return _Day14; }
            set { _Day14 = value; }
        }

        public virtual string Day15
        {
            get { return _Day15; }
            set { _Day15 = value; }
        }

        public virtual string Day16
        {
            get { return _Day16; }
            set { _Day16 = value; }
        }

        public virtual string Day17
        {
            get { return _Day17; }
            set { _Day17 = value; }
        }

        public virtual string Day18
        {
            get { return _Day18; }
            set { _Day18 = value; }
        }

        public virtual string Day19
        {
            get { return _Day19; }
            set { _Day19 = value; }
        }

        public virtual string Day20
        {
            get { return _Day20; }
            set { _Day20 = value; }
        }

        public virtual string Day21
        {
            get { return _Day21; }
            set { _Day21 = value; }
        }

        public virtual string Day22
        {
            get { return _Day22; }
            set { _Day22 = value; }
        }

        public virtual string Day23
        {
            get { return _Day23; }
            set { _Day23 = value; }
        }

        public virtual string Day24
        {
            get { return _Day24; }
            set { _Day24 = value; }
        }

        public virtual string Day25
        {
            get { return _Day25; }
            set { _Day25 = value; }
        }

        public virtual string Day26
        {
            get { return _Day26; }
            set { _Day26 = value; }
        }

        public virtual string Day27
        {
            get { return _Day27; }
            set { _Day27 = value; }
        }

        public virtual string Day28
        {
            get { return _Day28; }
            set { _Day28 = value; }
        }

        public virtual string Day29
        {
            get { return _Day29; }
            set { _Day29 = value; }
        }

        public virtual string Day30
        {
            get { return _Day30; }
            set { _Day30 = value; }
        }

        public virtual string Day31
        {
            get { return _Day31; }
            set { _Day31 = value; }
        }

        public virtual Decimal PaidWorkingDay
        {
            get { return _PaidWorkingDay; }
            set { _PaidWorkingDay = value; }
        }

        public virtual Decimal PaidNonWorkingDay
        {
            get { return _PaidNonWorkingDay; }
            set { _PaidNonWorkingDay = value; }
        }

        public virtual Decimal InsuranceReplaceSalaryWorkingDay
        {
            get { return _InsuranceReplaceSalaryWorkingDay; }
            set { _InsuranceReplaceSalaryWorkingDay = value; }
        }

        public virtual Decimal InsuranceReplaceSalaryTotalRate
        {
            get { return _InsuranceReplaceSalaryTotalRate; }
            set { _InsuranceReplaceSalaryTotalRate = value; }
        }

        public virtual Decimal WorkingDay
        {
            get { return _WorkingDay; }
            set { _WorkingDay = value; }
        }

        public virtual Decimal WeekendDay
        {
            get { return _WeekendDay; }
            set { _WeekendDay = value; }
        }

        public virtual Decimal Holiday
        {
            get { return _Holiday; }
            set { _Holiday = value; }
        }

        public virtual Decimal WorkingDayNight
        {
            get { return _WorkingDayNight; }
            set { _WorkingDayNight = value; }
        }

        public virtual Decimal WeekendDayNight
        {
            get { return _WeekendDayNight; }
            set { _WeekendDayNight = value; }
        }

        public virtual Decimal HolidayNight
        {
            get { return _HolidayNight; }
            set { _HolidayNight = value; }
        }

        public virtual Decimal TotalOverTime
        {
            get { return _TotalOverTime; }
            set { _TotalOverTime = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
