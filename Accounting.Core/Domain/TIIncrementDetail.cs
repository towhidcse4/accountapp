using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIIncrementDetail
    {
        private Guid _ID;
        private Guid _TIIncrementID;
        private Guid? _ToolsID;
        private string _ToolsName;
        private string _Description;
        private Guid? _DepartmentID;
        private string _DebitAccount;
        private string _CreditAccount;
        private Decimal _Quantity;
        private Decimal _UnitPrice;
        private Decimal _UnitPriceOriginal;
        private Decimal _Amount;
        private int _AllocationTimes;
        private int _AllocationType;
        private Guid? _StatisticsCodeID;
        private Guid? _CostSetID;
        private Guid? _RSInwardOutwardID;
        private Guid? _ExpenseItemID;
        private Guid? _BudgetItemID;
        private Guid? _RSInwardOutwardDetailID;
        private Boolean? _IsIrrationalCost;
        private int? _OrderPriority;
        private Decimal _AmountOriginal;
        private Decimal _DiscountRate;
        private Decimal _DiscountAmount;
        private Decimal _DiscountAmountOriginal;
        private Decimal _CustomUnitPrice;
        private Decimal _CustomUnitPriceOriginal;
        private Decimal _ImportTaxRate;
        private Decimal _ImportTaxAmount;
        private Decimal _ImportTaxAmountOriginal;
        private string _ImportTaxAccount;
        private Decimal _SpecialConsumeTaxRate;
        private Decimal _SpecialConsumeTaxAmount;
        private Decimal _SpecialConsumeTaxAmountOriginal;
        private string _SpecialConsumeTaxAccount;
        private Decimal _VATRate;
        private Decimal _VATAmount;
        private Decimal _VATAmountOriginal;
        private string _VATAccount;
        private string _DeductionDebitAccount;
        private Guid? _ContractID;
        private Guid? _AccountingObjectID;
        private Guid? _AccountingObjectTaxID;
        private string _AccountingObjectTaxName;
        private Guid? _PaymentID;
        private string _CompanyTaxCode;
        private Guid? _InvoiceTypeID;
        private Decimal _ImportTaxExpenseAmount;
        private Decimal _ImportTaxExpenseAmountOriginal;
        private Boolean _IsVATPaid;
        private int _InvoiceType;
        private DateTime? _InvoiceDate;
        private string _InvoiceNo;
        private string _InvoiceSeries;
        private Guid? _GoodsServicePurchaseID;
        private Guid? _TIAuditID;
        private Decimal _FreightAmount;
        private Decimal _FreightAmountOriginal;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private decimal inwardAmount;
        private decimal inwardAmountOriginal;
        public virtual string InvoiceTemplate { get; set; }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal CashOutVATAmount { get; set; }
        public virtual decimal CashOutDifferVATAmount { get; set; }
        public virtual decimal TaxExchangeRate { get; set; }
        public virtual decimal InwardAmount
        {
            get { return inwardAmount; }
            set { inwardAmount = value; }
        }

        public virtual decimal InwardAmountOriginal
        {
            get { return inwardAmountOriginal; }
            set { inwardAmountOriginal = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TIIncrementID
        {
            get { return _TIIncrementID; }
            set { _TIIncrementID = value; }
        }

        public virtual Guid? ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }
        public virtual Decimal UnitPriceOriginal
        {
            get { return _UnitPriceOriginal; }
            set { _UnitPriceOriginal = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual int AllocationTimes
        {
            get { return _AllocationTimes; }
            set { _AllocationTimes = value; }
        }

        public virtual int AllocationType
        {
            get { return _AllocationType; }
            set { _AllocationType = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }
        public virtual Guid? TIAuditID

        {
            get { return _TIAuditID; }
            set { _TIAuditID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? RSInwardOutwardID
        {
            get { return _RSInwardOutwardID; }
            set { _RSInwardOutwardID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return _BudgetItemID; }
            set { _BudgetItemID = value; }
        }

        public virtual Guid? RSInwardOutwardDetailID
        {
            get { return _RSInwardOutwardDetailID; }
            set { _RSInwardOutwardDetailID = value; }
        }

        public virtual Boolean? IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }

        public virtual Decimal AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual Decimal DiscountRate
        {
            get { return _DiscountRate; }
            set { _DiscountRate = value; }
        }

        public virtual Decimal DiscountAmount
        {
            get { return _DiscountAmount; }
            set { _DiscountAmount = value; }
        }

        public virtual Decimal DiscountAmountOriginal
        {
            get { return _DiscountAmountOriginal; }
            set { _DiscountAmountOriginal = value; }
        }

        public virtual Decimal CustomUnitPrice
        {
            get { return _CustomUnitPrice; }
            set { _CustomUnitPrice = value; }
        }

        public virtual Decimal CustomUnitPriceOriginal
        {
            get { return _CustomUnitPriceOriginal; }
            set { _CustomUnitPriceOriginal = value; }
        }

        public virtual Decimal ImportTaxRate
        {
            get { return _ImportTaxRate; }
            set { _ImportTaxRate = value; }
        }

        public virtual Decimal ImportTaxAmount
        {
            get { return _ImportTaxAmount; }
            set { _ImportTaxAmount = value; }
        }

        public virtual Decimal ImportTaxAmountOriginal
        {
            get { return _ImportTaxAmountOriginal; }
            set { _ImportTaxAmountOriginal = value; }
        }

        public virtual string ImportTaxAccount
        {
            get { return _ImportTaxAccount; }
            set { _ImportTaxAccount = value; }
        }

        public virtual Decimal SpecialConsumeTaxRate
        {
            get { return _SpecialConsumeTaxRate; }
            set { _SpecialConsumeTaxRate = value; }
        }

        public virtual Decimal SpecialConsumeTaxAmount
        {
            get { return _SpecialConsumeTaxAmount; }
            set { _SpecialConsumeTaxAmount = value; }
        }

        public virtual Decimal SpecialConsumeTaxAmountOriginal
        {
            get { return _SpecialConsumeTaxAmountOriginal; }
            set { _SpecialConsumeTaxAmountOriginal = value; }
        }

        public virtual string SpecialConsumeTaxAccount
        {
            get { return _SpecialConsumeTaxAccount; }
            set { _SpecialConsumeTaxAccount = value; }
        }

        public virtual Decimal VATRate
        {
            get { return _VATRate; }
            set { _VATRate = value; }
        }

        public virtual Decimal VATAmount
        {
            get { return _VATAmount; }
            set { _VATAmount = value; }
        }

        public virtual Decimal VATAmountOriginal
        {
            get { return _VATAmountOriginal; }
            set { _VATAmountOriginal = value; }
        }

        public virtual string VATAccount
        {
            get { return _VATAccount; }
            set { _VATAccount = value; }
        }

        public virtual string DeductionDebitAccount
        {
            get { return _DeductionDebitAccount; }
            set { _DeductionDebitAccount = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual Guid? AccountingObjectTaxID
        {
            get { return _AccountingObjectTaxID; }
            set { _AccountingObjectTaxID = value; }
        }

        public virtual string AccountingObjectTaxName
        {
            get { return _AccountingObjectTaxName; }
            set { _AccountingObjectTaxName = value; }
        }

        public virtual Guid? PaymentID
        {
            get { return _PaymentID; }
            set { _PaymentID = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return _CompanyTaxCode; }
            set { _CompanyTaxCode = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual Decimal ImportTaxExpenseAmount
        {
            get { return _ImportTaxExpenseAmount; }
            set { _ImportTaxExpenseAmount = value; }
        }

        public virtual Decimal ImportTaxExpenseAmountOriginal
        {
            get { return _ImportTaxExpenseAmountOriginal; }
            set { _ImportTaxExpenseAmountOriginal = value; }
        }

        public virtual Boolean IsVATPaid
        {
            get { return _IsVATPaid; }
            set { _IsVATPaid = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual Guid? GoodsServicePurchaseID
        {
            get { return _GoodsServicePurchaseID; }
            set { _GoodsServicePurchaseID = value; }
        }

        public virtual Decimal FreightAmount
        {
            get { return _FreightAmount; }
            set { _FreightAmount = value; }
        }

        public virtual Decimal FreightAmountOriginal
        {
            get { return _FreightAmountOriginal; }
            set { _FreightAmountOriginal = value; }
        }

        public virtual string ToolsName
        {
            get { return _ToolsName; }
            set { _ToolsName = value; }
        }
        public virtual string VATDescription { get; set; }

    }
}
