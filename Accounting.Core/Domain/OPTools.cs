using System;

namespace Accounting.Core.Domain
{
    public class OPTools
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _TypeID;
        private Guid _ToolsID;
        private DateTime _PostedDate;
        private string _CurrencyID;
        private Decimal _ExchangeRate;
        private Decimal? _Quantity;
        private Decimal? _UnitPrice;
        private Decimal? _Amount;
        private int? _AllocationTimes;
        private Decimal? _AllocatedAmount;
        private Decimal? _RemainAmount;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual Guid ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }

        public virtual Decimal ExchangeRate
        {
            get { return _ExchangeRate; }
            set { _ExchangeRate = value; }
        }

        public virtual Decimal? Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal? UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual int? AllocationTimes
        {
            get { return _AllocationTimes; }
            set { _AllocationTimes = value; }
        }

        public virtual Decimal? AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual Decimal? RemainAmount
        {
            get { return _RemainAmount; }
            set { _RemainAmount = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }
        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

    }
}
