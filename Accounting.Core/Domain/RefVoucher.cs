using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RefVoucher
    {
        public virtual Guid ID
        {
            get;set;
        }
        public virtual Guid RefID1
        {
            get; set;
        }
        public virtual Guid RefID2
        {
            get; set;
        }
        public virtual string No
        {
            get; set;
        }
        public virtual DateTime Date
        {
            get; set;
        }
        public virtual DateTime PostedDate
        {
            get; set;
        }
        public virtual string Reason
        {
            get; set;
        }
        public virtual bool Check { get; set; }
        public virtual int TypeVoucher { get; set; }
        public virtual int TypeID { get; set; }
    }
}