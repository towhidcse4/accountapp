﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SalePriceGroup
    {
        private Guid iD;
        private string salePriceGroupCode;
        private string salePriceGroupName;
        private string description;
        private bool isActive;
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }

        }
        public virtual string SalePriceGroupCode
        {
            get { return salePriceGroupCode; }
            set { salePriceGroupCode = value; }
        }
        public virtual string SalePriceGroupName
        {
            get { return salePriceGroupName; }
            set { salePriceGroupName = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
    

           


    }
}
