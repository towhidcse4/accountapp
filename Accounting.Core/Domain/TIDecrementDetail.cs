using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain {
    
    [Serializable]
    public partial class TIDecrementDetail {
        public virtual Guid ID { get; set; }
        public virtual Guid? TIDecrementID { get; set; }
        public virtual Guid? ToolsID { get; set; }
        public virtual string ToolsName { get; set; }
        public virtual string Description { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? DecrementQuantity { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal RemainingAmount { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? TIIncrementID { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual int? ConfrontTypeID { get; set; }
        public virtual bool? IsIrrationalCost { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual Guid? TIAuditID { get; set; }
    }
}
