using System;
using System.Text;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public partial class TIIncrement
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _TypeID;
        private DateTime _Date;
        private string _No;
        private string _Reason;
        private Decimal _TotalAmount;
        private Boolean _Recorded;
        private Boolean _Exported;
        private Guid? _TemplateID;
        private DateTime _PostedDate;
        private string _IssueBy;
        private string _CurrencyID;
        private Decimal _TotalAmountOriginal;
        private Decimal _TotalVATAmount;
        private Decimal _TotalVATAmountOriginal;
        private Decimal _TotalDiscountAmount;
        private Decimal _TotalDiscountAmountOriginal;
        private Decimal _TotalFreightAmount;
        private Decimal _TotalFreightAmountOriginal;
        private Decimal _TotalInwardAmount;
        private Decimal _TotalInwardAmountOriginal;
        private Decimal _TotalSpecialConsumeTaxAmount;
        private Decimal _TotalSpecialConsumeTaxAmountOriginal;
        private Decimal _TotalAll;
        private Decimal _TotalAllOriginal;
        private Boolean? _IsImportPurchase;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private Guid? _EmployeeID;
        private decimal? _ExchangeRate;
        private Guid? _PaymentClauseID;
        private DateTime? _DueDate;
        private Guid? _AccountingObjectBankAccount;
        private string _AccountingObjectBankName;
        private Guid? _AccountingObjectID;
        private string _AccountingObjectName;
        private string _AccountingObjectAddress;
        private string _CreditCardNumber;
        private Guid? _BankAccountDetailID;
        private string _BankName;
        private string _IdentificationNo;
        private DateTime? _IssueDate;
        private string _MContactName;
        private string _NumberAttach;
        private Boolean _IsSpecialConsumeTax;
        private Boolean _IsMatch;
        private DateTime? _MatchDate;
        private Guid? _TransportMethodID;
        private Boolean _IsVATPaid;
        private string _TaxCode;
        private Decimal _TotalImportTaxAmount;
        private Decimal _TotalImportTaxAmountOriginal;
        public virtual Guid? RefID { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string Type { get; set; }
        public virtual decimal TotalAmountFromVoucher { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual Decimal TotalAmount
        {
            get { return TotalAmountOriginal * ExchangeRate??1; }
            set { _TotalAmount = value; }
        }

        public virtual Boolean Recorded
        {
            get { return _Recorded; }
            set { _Recorded = value; }
        }

        public virtual Boolean Exported
        {
            get { return _Exported; }
            set { _Exported = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual string IssueBy
        {
            get { return _IssueBy; }
            set { _IssueBy = value; }
        }

        public virtual string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }

        public virtual Decimal TotalAmountOriginal
        {
            get { return _TotalAmountOriginal; }
            set { _TotalAmountOriginal = value; }
        }

        public virtual Decimal TotalVATAmount
        {
            get { return _TotalVATAmount; }
            set { _TotalVATAmount = value; }
        }

        public virtual Decimal TotalVATAmountOriginal
        {
            get { return _TotalVATAmountOriginal; }
            set { _TotalVATAmountOriginal = value; }
        }

        public virtual Decimal TotalDiscountAmount
        {
            get { return _TotalDiscountAmount; }
            set { _TotalDiscountAmount = value; }
        }

        public virtual Decimal TotalDiscountAmountOriginal
        {
            get { return _TotalDiscountAmountOriginal; }
            set { _TotalDiscountAmountOriginal = value; }
        }

        public virtual Decimal TotalFreightAmount
        {
            get { return _TotalFreightAmount; }
            set { _TotalFreightAmount = value; }
        }

        public virtual Decimal TotalFreightAmountOriginal
        {
            get { return _TotalFreightAmountOriginal; }
            set { _TotalFreightAmountOriginal = value; }
        }

        public virtual Decimal TotalInwardAmount
        {
            get { return _TotalInwardAmount; }
            set { _TotalInwardAmount = value; }
        }

        public virtual Decimal TotalInwardAmountOriginal
        {
            get { return _TotalInwardAmountOriginal; }
            set { _TotalInwardAmountOriginal = value; }
        }

        public virtual Boolean? IsImportPurchase
        {
            get { return _IsImportPurchase; }
            set { _IsImportPurchase = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return _ExchangeRate; }
            set { _ExchangeRate = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return _PaymentClauseID; }
            set { _PaymentClauseID = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        public virtual Guid? AccountingObjectBankAccount
        {
            get { return _AccountingObjectBankAccount; }
            set { _AccountingObjectBankAccount = value; }
        }

        public virtual string AccountingObjectBankName
        {
            get { return _AccountingObjectBankName; }
            set { _AccountingObjectBankName = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return _AccountingObjectAddress; }
            set { _AccountingObjectAddress = value; }
        }

        public virtual string CreditCardNumber
        {
            get { return _CreditCardNumber; }
            set { _CreditCardNumber = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return _BankAccountDetailID; }
            set { _BankAccountDetailID = value; }
        }

        public virtual string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        public virtual string IdentificationNo
        {
            get { return _IdentificationNo; }
            set { _IdentificationNo = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return _IssueDate; }
            set { _IssueDate = value; }
        }

        public virtual string MContactName
        {
            get { return _MContactName; }
            set { _MContactName = value; }
        }
        public virtual string Receiver
        {
            get { return _MContactName; }
        }
        public virtual string NumberAttach
        {
            get { return _NumberAttach; }
            set { _NumberAttach = value; }
        }

        public virtual Boolean IsSpecialConsumeTax
        {
            get { return _IsSpecialConsumeTax; }
            set { _IsSpecialConsumeTax = value; }
        }

        public virtual Boolean IsMatch
        {
            get { return _IsMatch; }
            set { _IsMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return _MatchDate; }
            set { _MatchDate = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return _TransportMethodID; }
            set { _TransportMethodID = value; }
        }

        public virtual Boolean IsVATPaid
        {
            get { return _IsVATPaid; }
            set { _IsVATPaid = value; }
        }
        public virtual string TaxCode
        {
            get { return _TaxCode; }
            set { _TaxCode = value; }
        }

        public virtual Decimal TotalImportTaxAmount
        {
            get { return _TotalImportTaxAmount; }
            set { _TotalImportTaxAmount = value; }
        }
        public virtual Decimal TotalImportTaxAmountOriginal
        {
            get { return _TotalImportTaxAmountOriginal; }
            set { _TotalImportTaxAmountOriginal = value; }
        }
        public virtual IList<TIIncrementDetail> TIIncrementDetails { get; set; }
        public virtual IList<PPInvoiceDetailCost> PPInvoiceDetailCosts { get; set; }

        

        public virtual Decimal TotalSpecialConsumeTaxAmount
        {
            get { return _TotalSpecialConsumeTaxAmount; }
            set { _TotalSpecialConsumeTaxAmount = value; }
        }

        public virtual Decimal TotalSpecialConsumeTaxAmountOriginal
        {
            get { return _TotalSpecialConsumeTaxAmountOriginal; }
            set { _TotalSpecialConsumeTaxAmountOriginal = value; }
        }

        public virtual Decimal TotalAll
        {
            get { return TotalAmount - TotalDiscountAmount + (!(IsImportPurchase ?? false) ? TotalVATAmount : 0); }
            set { _TotalAll = value; }
        }

        public virtual Decimal TotalAllOriginal
        {
            get { return TotalAmountOriginal - TotalDiscountAmountOriginal + (!(IsImportPurchase ?? false) ? TotalVATAmountOriginal : 0); }
            set { _TotalAllOriginal = value; }
        }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public TIIncrement()
        {
            RefVouchers = new List<RefVoucher>();
            TIIncrementDetails = new List<TIIncrementDetail>();
            PPInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
            ExchangeRate = 1;
        }
    }
}
