using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RolePermission
    {
        private Guid _RolePermissionId;
        private Guid _RoleId;
        private Guid _PermissionId;


        public virtual Guid RolePermissionId
        {
            get { return _RolePermissionId; }
            set { _RolePermissionId = value; }
        }

        public virtual Guid RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        public virtual Guid PermissionId
        {
            get { return _PermissionId; }
            set { _PermissionId = value; }
        }


    }
}
