using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIAudit
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private DateTime _PostedDate;
        private DateTime _Date;
        private string _No;
        private string _Description;
        private DateTime _InventoryDate;
        private string _Summary;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private Guid _TemplateID;

        public virtual IList<TIAuditDetail> TIAuditDetails { get; set; }
        public virtual IList<TIAuditMemberDetail> TIAuditMemberDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public TIAudit()
        {
            RefVouchers = new List<RefVoucher>();
            TIAuditDetails = new List<TIAuditDetail>();
            TIAuditMemberDetails = new List<TIAuditMemberDetail>();
        }


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual DateTime InventoryDate
        {
            get { return _InventoryDate; }
            set { _InventoryDate = value; }
        }

        public virtual string Summary
        {
            get { return _Summary; }
            set { _Summary = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual Guid TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }


    }
}
