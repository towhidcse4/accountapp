using System;

namespace Accounting.Core.Domain
{
    public class ApiService
    {
        private Guid _ID;
        private Guid _SupplierServiceID;
        private string _SupplierServiceCode;
        private string _ApiName;
        private string _ApiPath;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid SupplierServiceID
        {
            get { return _SupplierServiceID; }
            set { _SupplierServiceID = value; }
        }

        public virtual string SupplierServiceCode
        {
            get { return _SupplierServiceCode; }
            set { _SupplierServiceCode = value; }
        }

        public virtual string ApiName
        {
            get { return _ApiName; }
            set { _ApiName = value; }
        }

        public virtual string ApiPath
        {
            get { return _ApiPath; }
            set { _ApiPath = value; }
        }


    }
}
