﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPInvoiceDetail
    {
        private Guid iD;
        private Guid pPInvoiceID;
        private Guid? materialGoodsID;
        private decimal unitPrice;
        private decimal unitPriceOriginal;
        private decimal unitPriceConvert;
        private decimal unitPriceConvertOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private decimal importTaxAmount;
        private decimal importTaxAmountOriginal;
        private decimal freightAmount;
        private decimal freightAmountOriginal;
        private decimal inwardAmount;
        private decimal inwardAmountOriginal;
        private decimal vATAmount;
        private decimal vATAmountOriginal;
        private decimal unitPriceAfterTax;
        private decimal unitPriceAfterTaxOriginal;
        private decimal amountAfterTax;
        private decimal amountAfterTaxOriginal;
        private decimal importTaxExpenseAmount;
        private decimal importTaxExpenseAmountOriginal;
        private decimal? importTaxChargedAmount;
        private decimal? importTaxChargedAmountOriginal;
        private decimal discountAmountAfterTax;
        private decimal discountAmountAfterTaxOriginal;
        private decimal discountAmount;
        private decimal discountAmountOriginal;
        private decimal customUnitPrice;
        private decimal customUnitPriceOriginal;
        private decimal specialConsumeTaxAmount;
        private decimal specialConsumeTaxAmountOriginal;
        private string specialConsumeTaxAccount;
        private decimal? specialConsumeTaxRate;
        private decimal? importTaxRate;
        private bool? isVATPaid;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool? isIrrationalCost;
        private int? orderPriority;
        private Guid? pPOrderID;
        private string pPOrderNo;
        private Guid? expenseItemID;
        private string vATAccount;
        private int? invoiceType;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string invoiceTemplate;
        private string invoiceSeries;
        private Guid? goodsServicePurchaseID;
        private string deductionDebitAccount;
        private Guid? accountingObjectID;
        private Guid? budgetItemID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? statisticCodeID;
        private Guid? departmentID;
        private DateTime? expiryDate;
        private string lotNo;
        private Guid? paymentID;
        private string unitConvert;
        private DateTime? vATPostedDate;
        private string companyTaxCode;
        private Guid? accountingObjectTaxID;
        private string accountingObjectTaxName;
        private Guid? invoiceTypeID;
        private decimal? convertRate;
        private string importTaxAccount;
        private decimal? vATRate;
        private decimal? discountRate;
        private Guid? repositoryID;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private string unit;
        private decimal? quantity;
        private decimal? quantityConvert;
        private string accountingObjectTaxCode;
        private string vATDescription;
        private Guid? detailID;

        public PPInvoiceDetail()
        {
            MaterialGoods = new MaterialGoods();
        }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal CashOutVATAmount { get; set; }
        public virtual decimal CashOutDifferVATAmount { get; set; }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid PPInvoiceID
        {
            get { return pPInvoiceID; }
            set { pPInvoiceID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }
        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal UnitPriceOriginal
        {
            get { return unitPriceOriginal; }
            set { unitPriceOriginal = value; }
        }

        public virtual decimal UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value; }
        }

        public virtual decimal UnitPriceConvertOriginal
        {
            get { return unitPriceConvertOriginal; }
            set { unitPriceConvertOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal ImportTaxAmount
        {
            get { return importTaxAmount; }
            set { importTaxAmount = value; }
        }

        public virtual decimal ImportTaxAmountOriginal
        {
            get { return importTaxAmountOriginal; }
            set { importTaxAmountOriginal = value; }
        }

        public virtual decimal FreightAmount
        {
            get { return freightAmount; }
            set { freightAmount = value; }
        }

        public virtual decimal FreightAmountOriginal
        {
            get { return freightAmountOriginal; }
            set { freightAmountOriginal = value; }
        }

        public virtual decimal InwardAmount
        {
            get { return inwardAmount; }
            set { inwardAmount = value; }
        }

        public virtual decimal InwardAmountOriginal
        {
            get { return inwardAmountOriginal; }
            set { inwardAmountOriginal = value; }
        }

        public virtual decimal VATAmount
        {
            get { return vATAmount; }
            set { vATAmount = value; }
        }

        public virtual decimal VATAmountOriginal
        {
            get { return vATAmountOriginal; }
            set { vATAmountOriginal = value; }
        }

        public virtual decimal UnitPriceAfterTax
        {
            get { return unitPriceAfterTax; }
            set { unitPriceAfterTax = value; }
        }

        public virtual decimal UnitPriceAfterTaxOriginal
        {
            get { return unitPriceAfterTaxOriginal; }
            set { unitPriceAfterTaxOriginal = value; }
        }

        public virtual decimal AmountAfterTax
        {
            get { return amountAfterTax; }
            set { amountAfterTax = value; }
        }

        public virtual decimal AmountAfterTaxOriginal
        {
            get { return amountAfterTaxOriginal; }
            set { amountAfterTaxOriginal = value; }
        }

        public virtual decimal ImportTaxExpenseAmount
        {
            get { return importTaxExpenseAmount; }
            set { importTaxExpenseAmount = value; }
        }

        public virtual decimal ImportTaxExpenseAmountOriginal
        {
            get { return importTaxExpenseAmountOriginal; }
            set { importTaxExpenseAmountOriginal = value; }
        }

        public virtual decimal DiscountAmountAfterTax
        {
            get { return discountAmountAfterTax; }
            set { discountAmountAfterTax = value; }
        }

        public virtual decimal DiscountAmountAfterTaxOriginal
        {
            get { return discountAmountAfterTaxOriginal; }
            set { discountAmountAfterTaxOriginal = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return discountAmountOriginal; }
            set { discountAmountOriginal = value; }
        }

        public virtual decimal CustomUnitPrice
        {
            get { return customUnitPrice; }
            set { customUnitPrice = value; }
        }

        public virtual decimal CustomUnitPriceOriginal
        {
            get { return customUnitPriceOriginal; }
            set { customUnitPriceOriginal = value; }
        }

        public virtual decimal SpecialConsumeTaxAmount
        {
            get { return specialConsumeTaxAmount; }
            set { specialConsumeTaxAmount = value; }
        }

        public virtual decimal SpecialConsumeTaxAmountOriginal
        {
            get { return specialConsumeTaxAmountOriginal; }
            set { specialConsumeTaxAmountOriginal = value; }
        }

        public virtual string SpecialConsumeTaxAccount
        {
            get { return specialConsumeTaxAccount; }
            set { specialConsumeTaxAccount = value; }
        }

        public virtual decimal? SpecialConsumeTaxRate
        {
            get { return specialConsumeTaxRate; }
            set { specialConsumeTaxRate = value; }
        }

        public virtual decimal? ImportTaxRate
        {
            get { return importTaxRate; }
            set { importTaxRate = value; }
        }

        public virtual bool? IsVATPaid
        {
            get { return isVATPaid; }
            set { isVATPaid = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool? IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? PPOrderID
        {
            get { return pPOrderID; }
            set { pPOrderID = value; }
        }

        public virtual string PPOrderNo
        {
            get { return pPOrderNo; }
            set { pPOrderNo = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual string VATAccount
        {
            get { return vATAccount; }
            set { vATAccount = value; }
        }

        public virtual int? InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }
        public virtual string InvoiceTemplate
        {
            get { return invoiceTemplate; }
            set { invoiceTemplate = value; }
        }
        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }

        public virtual Guid? GoodsServicePurchaseID
        {
            get { return goodsServicePurchaseID; }
            set { goodsServicePurchaseID = value; }
        }

        public virtual string DeductionDebitAccount
        {
            get { return deductionDebitAccount; }
            set { deductionDebitAccount = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticCodeID; }
            set { statisticCodeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return lotNo; }
            set { lotNo = value; }
        }

        public virtual Guid? PaymentID
        {
            get { return paymentID; }
            set { paymentID = value; }
        }

        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }

        public virtual DateTime? VATPostedDate
        {
            get { return vATPostedDate; }
            set { vATPostedDate = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual Guid? AccountingObjectTaxID
        {
            get { return accountingObjectTaxID; }
            set { accountingObjectTaxID = value; }
        }

        public virtual string AccountingObjectTaxName
        {
            get { return accountingObjectTaxName; }
            set { accountingObjectTaxName = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return invoiceTypeID; }
            set { invoiceTypeID = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }

        public virtual string ImportTaxAccount
        {
            get { return importTaxAccount; }
            set { importTaxAccount = value; }
        }

        public virtual decimal? VATRate
        {
            get { return vATRate; }
            set { vATRate = value; }
        }

        public virtual decimal? DiscountRate
        {
            get { return discountRate; }
            set { discountRate = value; }
        }

        public virtual Guid? RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual decimal? QuantityConvert
        {
            get { return quantityConvert; }
            set { quantityConvert = value; }
        }

        public virtual string AccountingObjectTaxCode
        {
            get { return accountingObjectTaxCode; }
            set { accountingObjectTaxCode = value; }
        }
        public virtual string VATDescription
        {
            get { return vATDescription; }
            set { vATDescription = value; }
        }
        public virtual MaterialGoods MaterialGoods { get; set; }
        public virtual string MaterialGoodsCode
        {
            get
            {
                if (MaterialGoods != null)
                    return MaterialGoods.MaterialGoodsCode;
                else
                    return "";
            }
        }
        public virtual string MaterialGoodsName {
            get
            {
                if (MaterialGoods != null)
                    return MaterialGoods.MaterialGoodsName;
                else
                    return "";
            }
        }

        public virtual Guid? DetailID
        {
            get { return detailID; }
            set { detailID = value; }
        }

        public virtual decimal TaxExchangeRate { get; set; }

        //public virtual decimal? ImportTaxChargedAmount //phí tính thuế nhập khẩu
        //{
        //    get { return importTaxChargedAmount; }
        //    set { importTaxChargedAmount = value; }
        //}

        //public virtual decimal? ImportTaxChargedAmountOriginal //phí tính thuế nhập khẩu nguyên tệ
        //{
        //    get { return importTaxChargedAmountOriginal; }
        //    set { importTaxChargedAmountOriginal = value; }
        //}
    }
}
