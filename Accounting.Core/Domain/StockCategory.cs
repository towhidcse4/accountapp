using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class StockCategory
    {
        private Guid iD;
        private string stockCategoryCode;
        private string stockCategoryName;
        private decimal unitPrice;
        private int stockKind;
        private bool isActive;
        private bool isSecurity = false;
        private string incentives;
        private string transferCondition;
        private decimal? yearTransLimited;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string StockCategoryCode
        {
            get { return stockCategoryCode; }
            set { stockCategoryCode = value; }
        }

        public virtual string StockCategoryName
        {
            get { return stockCategoryName; }
            set { stockCategoryName = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual int StockKind
        {
            get { return stockKind; }
            set { stockKind = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual string Incentives
        {
            get { return incentives; }
            set { incentives = value; }
        }

        public virtual string TransferCondition
        {
            get { return transferCondition; }
            set { transferCondition = value; }
        }

        public virtual decimal? YearTransLimited
        {
            get { return yearTransLimited; }
            set { yearTransLimited = value; }
        }
    }
}
