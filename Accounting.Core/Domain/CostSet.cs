﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CostSet
    {
        private Guid iD;
        private string costSetCode;
        private string costSetName;
        private int grade;
        private decimal amountContract;
        private bool isActive;
        private string description;
        private Guid? accountingObjectID;
        private DateTime? startDate;
        private DateTime? finishDate;
        private int costSetType;
        string costSetTypeView;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;
        public virtual bool Select { get; set; }
        public virtual string Type { get; set; }
        public CostSet()
        {
            MaterialGoods = new List<MaterialGoods>();
            Type = "ĐT THCP";
        }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual IList<MaterialGoods> MaterialGoods { get; set; }
        public virtual string CostSetCode
        {
            get { return costSetCode; }
            set { costSetCode = value; }
        }

        public virtual string CostSetName
        {
            get { return costSetName; }
            set { costSetName = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual decimal AmountContract
        {
            get { return amountContract; }
            set { amountContract = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual DateTime? StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public virtual DateTime? FinishDate
        {
            get { return finishDate; }
            set { finishDate = value; }
        }

        public virtual int CostSetType
        {
            get { return costSetType; }
            set { costSetType = value; }
        }
        public virtual string CostSetTypeView
        {
            get { return costSetTypeView; }
            set { costSetTypeView = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }

    }
}
