﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADepreciationDetail
    {
        private Guid id;
        private Guid fADepreciationID;
        private Guid? fixAssetID;
        private Guid? objectID;
        private string currencyID;
        private decimal exchangeRate;
        private decimal orgPrice;
        private decimal orgPriceOriginal;
        private decimal eachMonthDepreciationRate;
        private decimal eachYearDepreciationAmount;
        private decimal eachYearDepreciationAmountOriginal;
        private decimal monthlyDepreciationAmount;
        private decimal monthlyDepreciationAmountOriginal;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private decimal amount;
        private decimal amountOriginal;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? accountingObjectID;
        private Guid? employeeID;
        private Guid? statisticsCodeID;
        private Guid? expenseItemID;
        private Guid? budgetItemID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int orderPriority;
        private string fixedAssetName;
        public virtual Guid? FixedAssetCategoryID { get; set; }
        public virtual decimal? AllocationRate { get; set; }
        public virtual decimal? AllocationAmount { get; set; }
        public virtual decimal? AllocationAmountOriginal { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? TotalOrgPrice { get; set; }
        public virtual decimal? TotalOrgPriceOriginal { get; set; }
        public virtual string Reason { get; set; }
        public virtual FixedAssetCategory FixedAssetCategory { get; set; }
        public virtual string FixedAssetCategoryName
        {
            get { return FixedAssetCategory != null ? FixedAssetCategory.FixedAssetCategoryName : ""; }
        }

        private bool isIrrationalCost; // chi phí không hợp lý

        public virtual bool IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }
        public virtual string FixedAssetName
        {
            get { return fixedAssetName; }
            set { fixedAssetName = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }
        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }
        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }
        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }
        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }
        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }
        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }
        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }
        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }
        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }
        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }
        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }
        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual decimal MonthlyDepreciationAmountOriginal
        {
            get { return monthlyDepreciationAmountOriginal; }
            set { monthlyDepreciationAmountOriginal = value; }
        }
        public virtual decimal MonthlyDepreciationAmount
        {
            get { return monthlyDepreciationAmount; }
            set { monthlyDepreciationAmount = value; }
        }
        public virtual decimal EachYearDepreciationAmountOriginal
        {
            get { return eachYearDepreciationAmountOriginal; }
            set { eachYearDepreciationAmountOriginal = value; }
        }
        public virtual decimal EachYearDepreciationAmount
        {
            get { return eachYearDepreciationAmount; }
            set { eachYearDepreciationAmount = value; }
        }
        public virtual decimal EachMonthDepreciationRate
        {
            get { return eachMonthDepreciationRate; }
            set { eachMonthDepreciationRate = value; }
        }
        public virtual decimal OrgPriceOriginal
        {
            get { return orgPriceOriginal; }
            set { orgPriceOriginal = value; }
        }
        public virtual decimal OrgPrice
        {
            get { return orgPrice; }
            set { orgPrice = value; }
        }
        public virtual decimal ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }
        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }
        public virtual Guid? ObjectID
        {
            get { return objectID; }
            set { objectID = value; }
        }
        public virtual Guid? FixedAssetID
        {
            get { return fixAssetID; }
            set { fixAssetID = value; }
        }
        public virtual Guid FADepreciationID
        {
            get { return fADepreciationID; }
            set { fADepreciationID = value; }
        }

        public virtual Guid ID
        {
            get { return id; }
            set { id = value; }
        }
    }
}
