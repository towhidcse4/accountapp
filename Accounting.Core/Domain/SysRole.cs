using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysRole {
        public virtual System.Guid RoleID { get; set; }
        public virtual string RoleCode { get; set; }
        public virtual string RoleName { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsSystem { get; set; }
        public virtual IList<SysUser> Users { get; private set; }
    }
}
