﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAInvoice
    {
        public virtual bool Check { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual Guid? BillRefID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string No { get; set; }
        public virtual int InvoiceType { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal TotalAmountOriginal { get; set; }
        public virtual decimal TotalDiscountAmount { get; set; }
        public virtual decimal? TotalDiscountRate { get; set; }
        public virtual decimal? TotalVATRate { get; set; }
        public virtual decimal TotalDiscountAmountOriginal { get; set; }
        public virtual decimal TotalVATAmount { get; set; }
        public virtual decimal TotalExportTaxAmount { get; set; }
        public virtual decimal TotalVATAmountOriginal { get; set; }
        public virtual decimal TotalCapitalAmount { get; set; }
        public virtual decimal TotalCapitalAmountOriginal { get; set; }
        private decimal _totalPaymentAMOriginalStand;
        public virtual decimal TotalPaymentAmountOriginal
        {
            get { return TotalAmountOriginal - TotalDiscountAmountOriginal + TotalVATAmountOriginal; }
            set { _totalPaymentAMOriginalStand = value; }
        }
        public virtual int? MaxOrderPriority
        {
            get
            {
                int? max1 = null;
                if (SAInvoiceDetails.Count > 0)
                {
                    max1 = SAInvoiceDetails.Max(k => k.OrderPriority) + 100000;
                }
                return max1;
            }
        }
        private decimal _totalPaymentAMStand;
        public virtual decimal TotalPaymentAmount
        {
            get { return TotalAmount - TotalDiscountAmount + TotalVATAmount; }
            set { _totalPaymentAMStand = value; }
        }
        public virtual decimal CommisionAmount { get; set; }
        public virtual decimal CommisionAmountOriginal { get; set; }
        public virtual Guid ID { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual string PlaceOfDelivery { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string ListNo { get; set; }
        public virtual DateTime? ListDate { get; set; }
        public virtual string ListCommonNameInventory { get; set; }
        public virtual bool? IsAttachList { get; set; }
        public virtual bool? IsDiscountByVoucher { get; set; }
        public virtual string ContainerNo { get; set; }
        public virtual string TransportCompanyName { get; set; }
        public virtual bool IsMatch { get; set; }
        public virtual DateTime? MatchDate { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual string CustomProperty4 { get; set; }
        public virtual string CustomProperty5 { get; set; }
        public virtual string CustomProperty6 { get; set; }
        public virtual int? InvoiceForm { get; set; }
        public virtual Guid? InvoiceTypeID { get; set; } // loại hóa đơn
        public virtual decimal? CommisionRate { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string InvoiceSeries { get; set; } // Ký hiệu hóa đơn
        public virtual Guid? SAQuoteID { get; set; }
        public virtual Guid? SAOrderID { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual Guid? PaymentClauseID { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual Guid? TransportMethodID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual string OutwardNo { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual DateTime? SPostedDate { get; set; }
        public virtual DateTime? ModifiedDate { get; set; }
        public virtual DateTime? SDate { get; set; }
        public virtual string SNo { get; set; }
        public virtual string SContactName { get; set; }
        public virtual string SReason { get; set; }
        public virtual DateTime? MPostedDate { get; set; }
        public virtual string MNo { get; set; }
        public virtual DateTime? MDate { get; set; }
        public virtual string MContactName { get; set; }
        public virtual string MReasonPay { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual string BankName { get; set; }
        public virtual bool? IsDeliveryVoucher { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual string BillOffLadingNo { get; set; }
        public virtual DateTime? NTTTPostedDate { get; set; }
        public virtual string NTTTNo { get; set; }
        public virtual string InvoiceTemplate { get; set; }// Mẫu số 
        public virtual DateTime? NTTTDate { get; set; }
        public virtual Guid? SABillID { get; set; }
        private DateTime? refDateTime;
        public virtual DateTime? RefDateTime
        {
            get { return refDateTime; }
            set { refDateTime = value; }
        }
        public virtual string TypicalOfDelivery { get; set; }// địa điểm nhận hàng
        public virtual SABill SABills { get; set; }
        private string invoiceNo;
        public virtual string InvoiceNoT
        {
            get
            {
                if (BillRefID != null && SABills != null && !string.IsNullOrEmpty(SABills.InvoiceNo))
                {
                    return SABills.InvoiceNo;
                }
                else
                    return InvoiceNo;
            }
            set
            {
                invoiceNo = value;
            }
        }
        public virtual string InvoiceNo { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public SAInvoice()
        {
            RefVouchers = new List<RefVoucher>();
            SABills = new SABill();
            SAInvoiceDetails = new List<SAInvoiceDetail>();
            ObjRsInwardOutwardIDs = new List<Guid>();
            ExchangeRate = 1;
            BillRefID = null;
        }

        public virtual IList<SAInvoiceDetail> SAInvoiceDetails { get; set; }
        public virtual List<Guid> ObjRsInwardOutwardIDs { get; set; }

        public virtual Guid? OutwardRefID { get; set; }

        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (SAInvoiceDetails.Count > 0)
                {
                    max = SAInvoiceDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        public virtual decimal? MaxDiscountRate
        {
            get
            {
                decimal? max = null;
                if (SAInvoiceDetails.Count > 0)
                {
                    max = SAInvoiceDetails.Max(k => k.DiscountRate);
                }
                return max;
            }
        }

        public virtual int StatusInvoice { get; set; }
        //public virtual string StatusInvoiceStr
        //{
        //    get
        //    {
        //        if (StatusInvoice == -1) return "Hoá đơn không tồn tại trong hệ thống";
        //        if (StatusInvoice == 0) return "Hoá đơn mới tạo lập";
        //        if (StatusInvoice == 1) return "Hoá đơn có chữ ký số";
        //        if (StatusInvoice == 2) return "Hoá đơn đã khai báo thuế";
        //        if (StatusInvoice == 3) return "Hoá đơn bị thay thế";
        //        if (StatusInvoice == 4) return "Hoá đơn bị điều chỉnh";
        //        if (StatusInvoice == 5) return "Hoá đơn bị huỷ";
        //        return null;
        //    }

        //}
        public virtual bool StatusConverted { get; set; }
        public virtual bool StatusSendMail { get; set; }

        public virtual bool StatusCusView { get; set; }
        public virtual DateTime? DateSendMail { get; set; }
        public virtual string Email { get; set; }
        public virtual bool IsBill { get; set; }
        public virtual bool IsAttachListBill { get; set; }
        public virtual string PaymentMethod { get; set; }

        public virtual Guid? IDReplaceInv { get; set; }
        public virtual Guid? IDAdjustInv { get; set; }
        public virtual Guid? ID_MIV { get; set; }

    }
}
