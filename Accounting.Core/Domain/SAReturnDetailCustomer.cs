using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAReturnDetailCustomer
    {
        public virtual Guid SAInvoiceID { get; set; }
        public virtual decimal ReceipableAmount { get; set; }
        public virtual decimal ReceipableAmountOriginal { get; set; }
        public virtual decimal DeptAmount { get; set; }
        public virtual decimal DeptAmountOriginal { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual int ReceipType { get; set; }
        public virtual Guid ID { get; set; }
        public virtual Guid SAReturnID { get; set; }
        public virtual decimal DiscountAmount { get; set; }
        public virtual decimal DiscountAmountOriginal { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? GenID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual Guid? DebtEmployeeID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual DateTime? VoucherDate { get; set; }
        public virtual DateTime? VoucherPostedDate { get; set; }
        public virtual string VoucherNo { get; set; }
        public virtual int? VoucherTypeID { get; set; }
    }
}
