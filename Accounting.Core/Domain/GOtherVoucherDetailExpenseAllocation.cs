using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailExpenseAllocation
    {
        private Guid _ID;
        private Guid _GOtherVoucherID;
        private Guid? _PrepaidExpenseID;
        private Decimal _AllocationAmount;
        private Guid? _AllocationObjectID;
        private int _AllocationObjectType;
        private Decimal _AllocationRate;
        private Decimal _Amount;
        private string _CostAccount;
        private Guid? _ExpenseItemID;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual int? OrderPriority { get; set; }
        public virtual Guid GOtherVoucherID
        {
            get { return _GOtherVoucherID; }
            set { _GOtherVoucherID = value; }
        }

        public virtual Guid? PrepaidExpenseID
        {
            get { return _PrepaidExpenseID; }
            set { _PrepaidExpenseID = value; }
        }
        public virtual string PrepaidExpenseName
        {
            get;
            set;
        }
        public virtual Decimal AllocationAmount
        {
            get { return _AllocationAmount; }
            set { _AllocationAmount = value; }
        }

        public virtual Guid? AllocationObjectID
        {
            get { return _AllocationObjectID; }
            set { _AllocationObjectID = value; }
        }
        public virtual string AllocationObjectName { get; set; }
        public virtual int AllocationObjectType
        {
            get { return _AllocationObjectType; }
            set { _AllocationObjectType = value; }
        }

        public virtual Decimal AllocationRate
        {
            get { return _AllocationRate; }
            set { _AllocationRate = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual string CostAccount
        {
            get { return _CostAccount; }
            set { _CostAccount = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }


    }
}
