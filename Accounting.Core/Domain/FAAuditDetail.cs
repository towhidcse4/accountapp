using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAAuditDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? FAAuditID { get; set; }
        public virtual Guid? FixedAssetID { get; set; }
        public virtual string FixedAssetName { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Decimal? OriginalPrice { get; set; }
        public virtual Decimal? AcDepreciationAmount { get; set; }
        public virtual Decimal? DepreciationAmount { get; set; }
        public virtual Decimal? RemainingAmount { get; set; }
        public virtual int ExistInStock { get; set; }
        public virtual int Recommendation { get; set; }
        public virtual string Note { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
    }
}
