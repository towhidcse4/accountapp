using System;

namespace Accounting.Core.Domain
{
    public class TMDeclaration
    {
        private Guid _ID;
        private Guid _BranchID;
        private Guid _TypeID;
        private string _DeclarationName;
        private string _DeclarationTerm;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private Boolean _IsFirstDeclaration;
        private int _AdditionTime;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual Guid TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string DeclarationName
        {
            get { return _DeclarationName; }
            set { _DeclarationName = value; }
        }

        public virtual string DeclarationTerm
        {
            get { return _DeclarationTerm; }
            set { _DeclarationTerm = value; }
        }

        public virtual DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public virtual Boolean IsFirstDeclaration
        {
            get { return _IsFirstDeclaration; }
            set { _IsFirstDeclaration = value; }
        }

        public virtual int AdditionTime
        {
            get { return _AdditionTime; }
            set { _AdditionTime = value; }
        }


    }
}
