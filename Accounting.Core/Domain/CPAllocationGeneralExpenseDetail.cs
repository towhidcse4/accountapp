using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPAllocationGeneralExpenseDetail
    {
        private Guid _ID;
        private Guid _CPAllocationGeneralExpenseID;
        private Guid _CostSetID;
        private Guid _ContractID;
        private Guid _ExpenseItemID;
        private Decimal _AllocatedRate;
        private Decimal _AllocatedAmount;
        private CostSet _Costset;
        private string _CostsetCode;
        private string _CostsetName;
        private ExpenseItem _ExpenseItem;
        private string _ExpenseItemCode;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string ContractCode { get; set; }
        public virtual DateTime? SignedDate { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual Guid CPAllocationGeneralExpenseID
        {
            get { return _CPAllocationGeneralExpenseID; }
            set { _CPAllocationGeneralExpenseID = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Guid ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Decimal AllocatedRate
        {
            get { return _AllocatedRate; }
            set { _AllocatedRate = value; }
        }

        public virtual Decimal AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual CostSet Costset
        {
            get { return _Costset; }
            set { _Costset = value; }
        }

        public virtual string CostsetCode
        {
            get { return _CostsetCode; }
            set { _CostsetCode = value; }
        }

        public virtual string CostsetName
        {
            get { return _CostsetName; }
            set { _CostsetName = value; }
        }

        public virtual ExpenseItem ExpenseItem
        {
            get { return _ExpenseItem; }
            set { _ExpenseItem = value; }
        }

        public virtual string ExpenseItemCode
        {
            get { return _ExpenseItemCode; }
            set { _ExpenseItemCode = value; }
        }
    }
}
