﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CostSetMaterialGoods
    {
        private Guid iD;
        private Guid costSetID;
        private Guid materialGoodsID;
        private string description;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }
        public virtual Guid MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
}
