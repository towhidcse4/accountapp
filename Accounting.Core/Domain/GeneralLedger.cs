using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GeneralLedger
    {
        private decimal debitAmount;
        private decimal debitAmountOriginal;
        private decimal creditAmount;
        private decimal creditAmountOriginal;
        private Guid referenceID;
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private Guid iD;
        private Guid? branchID;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string account;
        private string accountCorresponding;
        private Guid? bankAccountDetailID;
        private string currencyID;
        private decimal? exchangeRate;
        private string reason;
        private string description;
        private string vATDescription;
        private Guid? accountingObjectID;
        private Guid? employeeID;
        private Guid? budgetItemID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? statisticsCodeID;
        private string invoiceSeries;
        private string contactName;
        private Guid? detailID;
        private string refNo;
        private DateTime? refDate;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private int ? orderPriority;
        private bool? isIrrationalCost;

        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal DebitAmount
        {
            get { return debitAmount; }
            set { debitAmount = value; }
        }

        public virtual decimal DebitAmountOriginal
        {
            get { return debitAmountOriginal; }
            set { debitAmountOriginal = value; }
        }

        public virtual decimal CreditAmount
        {
            get { return creditAmount; }
            set { creditAmount = value; }
        }

        public virtual decimal CreditAmountOriginal
        {
            get { return creditAmountOriginal; }
            set { creditAmountOriginal = value; }
        }

        public virtual Guid ReferenceID
        {
            get { return referenceID; }
            set { referenceID = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual string Account
        {
            get { return account; }
            set { account = value; }
        }

        public virtual string AccountCorresponding
        {
            get { return accountCorresponding; }
            set { accountCorresponding = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string VATDescription
        {
            get { return vATDescription; }
            set { vATDescription = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual Guid? DetailID
        {
            get { return detailID; }
            set { detailID = value; }
        }

        public virtual string RefNo
        {
            get { return refNo; }
            set { refNo = value; }
        }

        public virtual DateTime? RefDate
        {
            get { return refDate; }
            set { refDate = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual int ? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual bool? IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }
    }
}