﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CostSetBook
    {
        public Guid CostSetID { get; set; }
        public Guid RefID { get; set; }
        public int TypeID { get; set; }
        public string CostSetCode { get; set; }
        public string CostSetName { get; set; }
        public DateTime NgayChungTu { get; set; }
        public string SoChungTu { get; set; }
        public string DienGiai { get; set; }
        public string TK { get; set; }
        public string TKDoiUng { get; set; }
        public decimal SoTienNo { get; set; }
        public decimal SoTienCo { get; set; }

    }
    public class CostSetBook_Period
    {
        public string Period { get; set; }

    }
}

