using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CreditCard
    {
        
        private Guid iD;
        private Guid? branchID;
        private string creditCardNumber;
        private string creditCardType;
        private string ownerCard;
        private Guid? bankIDIssueCard;
        private string bankName;
        private int? exFromMonth;
        private int? exFromYear;
        private int? exToMonth;
        private int? exToYear;
        private string description;
        private bool isActive;


       

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }
        public virtual string CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        public virtual string CreditCardType
        {
            get { return creditCardType; }
            set { creditCardType = value; }
        }
        public virtual string OwnerCard
        {
            get { return ownerCard; }
            set { ownerCard = value; }
        }

        public virtual Guid? BankIDIssueCard
        {
            get { return bankIDIssueCard; }
            set { bankIDIssueCard = value; }
        }
        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual int? ExFromMonth
        {
            get { return exFromMonth; }
            set { exFromMonth = value; }
        }

        public virtual int? ExFromYear
        {
            get { return exFromYear; }
            set { exFromYear = value; }
        }

        public virtual int? ExToMonth
        {
            get { return exToMonth; }
            set { exToMonth = value; }
        }

        public virtual int? ExToYear
        {
            get { return exToYear; }
            set { exToYear = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

    }
}
