﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialGoodsAssembly
    {
        private Guid iD;
        private Guid? materialGoodsID;
        private Guid? materialAssemblyID;
        private string materialAssemblyDescription;
        private decimal? quantity;
        private decimal unitPrice;
        private string unit;
        private decimal? convertRate;
        private string unitConvert;
        private decimal? unitPriceConvert;
        private decimal? quantityConvert;
        private decimal? totalMoney;



        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }
        public virtual Guid? MaterialAssemblyID
        {
            get { return materialAssemblyID; }
            set { materialAssemblyID = value; }
        }
        public virtual string MaterialAssemblyDescription
        {
            get { return materialAssemblyDescription; }
            set { materialAssemblyDescription = value; }
        }
        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }
        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }
        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }
        public virtual decimal? UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value; }
        }
        public virtual decimal? QuantityConvert
        {
            get { return quantityConvert; }
            set { quantityConvert = value; }
        }
        public virtual decimal? TotalMoney
        {
            get { return totalMoney; }
            set { totalMoney = value; }
        }
    }
}
