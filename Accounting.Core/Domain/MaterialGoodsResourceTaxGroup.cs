﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialGoodsResourceTaxGroup
    {
        private Guid iD;
        private string materialGoodsResourceTaxGroupCode;
        private string materialGoodsResourceTaxGroupName;
        private string unit;
        private decimal? taxRate;
        private Guid? parentID;
        private bool isParentNode;
        private int grade;
        private int orderPriority;
        private bool isActive;
        private bool isSecurity;
        private string orderFixCode;

        public MaterialGoodsResourceTaxGroup() { }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual string MaterialGoodsResourceTaxGroupCode
        {
            get { return materialGoodsResourceTaxGroupCode; }
            set { materialGoodsResourceTaxGroupCode = value; }
        }
        public virtual string MaterialGoodsResourceTaxGroupName
        {
            get { return materialGoodsResourceTaxGroupName; }
            set { materialGoodsResourceTaxGroupName = value; }
        }
        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }
        public virtual decimal? TaxRate
        {
            get { return taxRate; }
            set { taxRate = value; }
        }
        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }
        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }
        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }
        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }
    }
}
