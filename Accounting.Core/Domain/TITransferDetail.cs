using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TITransferDetail
    {
        private Guid _ID;
        private Guid _TITransferID;
        private Guid? _ToolsID;
        private string _Description;
        private Guid? _FromDepartmentID;
        private Guid? _ToDepartmentID;
        private Decimal _Quantity;
        private Decimal _TransferQuantity;
        private Decimal _OWAmount;
        private Decimal _AllocateAmount;
        private Decimal _RemainAmount;
        private string _DebitAccount;
        private string _CreditAccount;
        private Guid? _CostSetID;
        private Guid? _StatisticsCodeID;
        private Guid? _TIIncrementID;
        private Decimal _UnitPrice;
        private int _AllocationTimes;
        private int _AllocationType;
        private int _ConfrontTypeID;
        private Boolean _IsIrrationalCost;
        private int _OrderPriority;
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TITransferID
        {
            get { return _TITransferID; }
            set { _TITransferID = value; }
        }

        public virtual decimal TransferQuantity
        {
            get { return _TransferQuantity; }
            set { _TransferQuantity = value; }
        }

        public virtual Guid? ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Guid? FromDepartmentID
        {
            get { return _FromDepartmentID; }
            set { _FromDepartmentID = value; }
        }

        public virtual Guid? ToDepartmentID
        {
            get { return _ToDepartmentID; }
            set { _ToDepartmentID = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal OWAmount
        {
            get { return _OWAmount; }
            set { _OWAmount = value; }
        }

        public virtual Decimal AllocateAmount
        {
            get { return _AllocateAmount; }
            set { _AllocateAmount = value; }
        }

        public virtual Decimal RemainAmount
        {
            get { return _RemainAmount; }
            set { _RemainAmount = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual Guid? TIIncrementID
        {
            get { return _TIIncrementID; }
            set { _TIIncrementID = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual int AllocationTimes
        {
            get { return _AllocationTimes; }
            set { _AllocationTimes = value; }
        }

        public virtual int AllocationType
        {
            get { return _AllocationType; }
            set { _AllocationType = value; }
        }

        public virtual int ConfrontTypeID
        {
            get { return _ConfrontTypeID; }
            set { _ConfrontTypeID = value; }
        }

        public virtual Boolean IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
