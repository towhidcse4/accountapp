using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GenCode
    {
        private Guid iD;
        private int typeGroupID;
        private int length;
        private bool isSecurity;
        private string suffix;
        private decimal? currentValue;
        private string typeGroupName;
        private string prefix;
        private bool isReset;
        private Guid? branchID;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual int TypeGroupID
        {
            get { return typeGroupID; }
            set { typeGroupID = value; }
        }

        public virtual int Length
        {
            get { return length; }
            set { length = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual string Suffix
        {
            get { return suffix; }
            set { suffix = value; }
        }

        public virtual decimal? CurrentValue
        {
            get { return currentValue; }
            set { currentValue = value; }
        }

        public virtual string TypeGroupName
        {
            get { return typeGroupName; }
            set { typeGroupName = value; }
        }

        public virtual string Prefix
        {
            get { return prefix; }
            set { prefix = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual bool IsReset
        {
            get { return isReset; }
            set { isReset = value; }
        }
        public virtual bool Check { get; set; }
    }
}
