using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TransportMethod
    {
        private Guid iD;
        private string transportMethodCode;
        private string transportMethodName;
        
        private bool isSecurity;
        private string description;
        private bool isActive;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string TransportMethodCode
        {
            get { return transportMethodCode; }
            set { transportMethodCode = value; }
        }

        public virtual string TransportMethodName
        {
            get { return transportMethodName; }
            set { transportMethodName = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }




    }
}
