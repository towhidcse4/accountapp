using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPPeriodDetail
    {
        private Guid _ID;
        private Guid _CPPeriodID;
        private Guid _CostSetID;
        private Guid _ContractID;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }


    }
}
