﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GoodsServicePurchase
    {
        private Guid iD;
        private string _goodsServicePurchaseCode;
        private string _goodsServicePurchaseName;
        private string _description;
        private bool _isActive;
        private bool _isSecurity;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual string GoodsServicePurchaseCode
        {
            get { return _goodsServicePurchaseCode; }
            set { _goodsServicePurchaseCode = value; }
        }
        public virtual string GoodsServicePurchaseName
        {
            get { return _goodsServicePurchaseName; }
            set { _goodsServicePurchaseName = value; }
        }
        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public virtual bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public virtual bool IsSecurity
        {
            get { return _isSecurity; }
            set { _isSecurity = value; }
        }
    }
}
