

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPOrderDetail
    {
        private decimal unitPrice;
        private decimal unitPriceOriginal;
        private decimal unitPriceConvert;
        private decimal unitPriceConvertOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private decimal vATAmount;
        private decimal vATAmountOriginal;
        private Guid iD;
        private Guid pPOrderID;
        private Guid? materialGoodsID;
        private decimal discountAmount;
        private decimal discountAmountOriginal;
        private decimal discountAmountAfterTax;
        private decimal discountAmountAfterTaxOriginal;
        private decimal unitPriceAfterTax;
        private decimal unitPriceAfterTaxOriginal;
        private decimal amountAfterTax;
        private decimal amountAfterTaxOriginal;
        private decimal? discountRate;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;
        private string description;
        private Guid? repositoryID;
        private string debitAccount;
        private string creditAccount;
        private string unit;
        private decimal? quantity;
        private decimal? quantityConvert;
        private decimal? quantityReceipt;
        private decimal? quantityReceiptConvert;
        private decimal? vATRate;
        private string vATAccount;
        private Guid? accountingObjectID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? statisticsCodeID;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private Guid? budgetItemID;
        private DateTime? expiryDate;
        private string lotNo;
        private string unitConvert;
        private decimal? convertRate;
        private Guid? employeeID;

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }
        private decimal unitPriceConvertAfterTax;
        private decimal unitPriceConvertAfterTaxOriginal;

        public virtual decimal UnitPriceConvertAfterTaxOriginal
        {
            get { return unitPriceConvertAfterTaxOriginal; }
            set { unitPriceConvertAfterTaxOriginal = value; }
        }

        public virtual decimal UnitPriceConvertAfterTax
        {
            get { return unitPriceConvertAfterTax; }
            set { unitPriceConvertAfterTax = value; }
        }
        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal UnitPriceOriginal
        {
            get { return unitPriceOriginal; }
            set { unitPriceOriginal = value; }
        }

        public virtual decimal UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value; }
        }

        public virtual decimal UnitPriceConvertOriginal
        {
            get { return unitPriceConvertOriginal; }
            set { unitPriceConvertOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal VATAmount
        {
            get { return vATAmount; }
            set { vATAmount = value; }
        }

        public virtual decimal VATAmountOriginal
        {
            get { return vATAmountOriginal; }
            set { vATAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid PPOrderID
        {
            get { return pPOrderID; }
            set { pPOrderID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return discountAmountOriginal; }
            set { discountAmountOriginal = value; }
        }

        public virtual decimal DiscountAmountAfterTax
        {
            get { return discountAmountAfterTax; }
            set { discountAmountAfterTax = value; }
        }

        public virtual decimal DiscountAmountAfterTaxOriginal
        {
            get { return discountAmountAfterTaxOriginal; }
            set { discountAmountAfterTaxOriginal = value; }
        }

        public virtual decimal UnitPriceAfterTax
        {
            get { return unitPriceAfterTax; }
            set { unitPriceAfterTax = value; }
        }

        public virtual decimal UnitPriceAfterTaxOriginal
        {
            get { return unitPriceAfterTaxOriginal; }
            set { unitPriceAfterTaxOriginal = value; }
        }

        public virtual decimal AmountAfterTax
        {
            get { return amountAfterTax; }
            set { amountAfterTax = value; }
        }

        public virtual decimal AmountAfterTaxOriginal
        {
            get { return amountAfterTaxOriginal; }
            set { amountAfterTaxOriginal = value; }
        }

        public virtual decimal? DiscountRate
        {
            get { return discountRate; }
            set { discountRate = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual decimal? QuantityConvert
        {
            get { return quantityConvert; }
            set { quantityConvert = value; }
        }

        public virtual decimal? QuantityReceipt
        {
            get { return quantityReceipt; }
            set { quantityReceipt = value; }
        }

        public virtual decimal? QuantityReceiptConvert
        {
            get { return quantityReceiptConvert; }
            set { quantityReceiptConvert = value; }
        }

        public virtual decimal? VATRate
        {
            get { return vATRate; }
            set { vATRate = value; }
        }

        public virtual string VATAccount
        {
            get { return vATAccount; }
            set { vATAccount = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return lotNo; }
            set { lotNo = value; }
        }

        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }
        public virtual string VATDescription { get; set; }
    }
}
