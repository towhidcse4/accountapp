using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RSAssemblyDismantlementDetail
    {
        private Guid iD;
        private Guid rSAssemblyDismantlementID;
        private Guid? materialGoodsID;
        private Guid? repositoryID;
        private decimal unitPrice;
        private decimal unitPriceOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private decimal unitPriceConvert;
        private decimal unitPriceConvertOriginal;
        private decimal? quantityConvert;
        private decimal? convertRate;
        private Guid? departmentID;
        private int? orderPriority;
        private Guid? contractID;
        private Guid? confrontID;
        private Guid? confrontDetailID;
        private DateTime? expiryDate;
        private string lotNo;
        private string unitConvert;
        private string unit;
        private decimal? quantity;
        private string description;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid RSAssemblyDismantlementID
        {
            get { return rSAssemblyDismantlementID; }
            set { rSAssemblyDismantlementID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual Guid? RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal UnitPriceOriginal
        {
            get { return unitPriceOriginal; }
            set { unitPriceOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value; }
        }

        public virtual decimal UnitPriceConvertOriginal
        {
            get { return unitPriceConvertOriginal; }
            set { unitPriceConvertOriginal = value; }
        }

        public virtual decimal? QuantityConvert
        {
            get { return quantityConvert; }
            set { quantityConvert = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }
        
        public virtual Guid? ConfrontID
        {
            get { return confrontID; }
            set { confrontID = value; }
        }

        public virtual Guid? ConfrontDetailID
        {
            get { return confrontDetailID; }
            set { confrontDetailID = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return lotNo; }
            set { lotNo = value; }
        }

        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Repository Repository { get; set; }
        public virtual string RepositoryName
        {
            get { return Repository != null ? Repository.RepositoryName : null; }
        }
    }
}
