﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMRegistrationDetail
    {
        private Guid iD;
        private Guid? eMRegistrationID;
        private Guid? stockCategoryID;
        private decimal unitPrice;
        private decimal quantityBuyable;
        private decimal quantityRegistered;
        private decimal quantityApproved;
        private decimal quantityBought;
        private decimal amountBought;
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid? EMRegistrationID
        {
            get { return eMRegistrationID; }
            set { eMRegistrationID = value; }
        }
        public virtual Guid? StockCategoryID
        {
            get { return stockCategoryID; }
            set { stockCategoryID = value; }
        }
        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }
        public virtual decimal QuantityBuyable
        {
            get { return quantityBuyable; }
            set { quantityBuyable = value; }
        }
        public virtual decimal QuantityRegistered
        {
            get { return quantityRegistered; }
            set { quantityRegistered = value; }
        }
        public virtual decimal QuantityApproved
        {
            get { return quantityApproved; }
            set { quantityApproved = value; }
        }
        public virtual decimal QuantityBought
        {
            get { return quantityBought; }
            set { quantityBuyable = value; }
        }
        public virtual decimal AmountBought
        {
            get { return amountBought; }
            set { amountBought = value; }
        }
      








    }
}