﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class HealthFinanceEnterpriseChart
    {
        public virtual string PostedMonthYear { get; set; }
        public virtual decimal TurnoverTotal { get; set; }
        public virtual decimal CostsTotal { get; set; }
    }
}
