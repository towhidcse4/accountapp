using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GVoucherListDetail
    {
        private bool _status;
        private Guid _iD;
        private Guid _gVoucherListID;
        private Guid _voucherID;
        private Guid _voucherDetailID;
        private int _voucherTypeID;
        private DateTime? _voucherDate; // cho ph�p null date
        private DateTime _voucherPostedDate;
        private string _voucherNo;
        private string _voucherDescription;
        private string _voucherDebitAccount;
        private string _voucherCreditAccount;
        private decimal? _voucherAmount; // cho ph�p null amount
        private string _currencyID;
        private string _reason;
        private int _orderPriority;
        private Guid _generalLedgerID;
        public virtual int ordercode { set; get; }
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string Description { get; set; }
        public virtual bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual Guid ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        public virtual Guid GVoucherListID
        {
            get { return _gVoucherListID; }
            set { _gVoucherListID = value; }
        }

        public virtual Guid VoucherID
        {
            get { return _voucherID; }
            set { _voucherID = value; }
        }

        public virtual Guid VoucherDetailID
        {
            get { return _voucherDetailID; }
            set { _voucherDetailID = value; }
        }

        public virtual int VoucherTypeID
        {
            get { return _voucherTypeID; }
            set { _voucherTypeID = value; }
        }

        public virtual DateTime? VoucherDate
        {
            get { return _voucherDate; }
            set { _voucherDate = value; }
        }

        public virtual DateTime VoucherPostedDate
        {
            get { return _voucherPostedDate; }
            set { _voucherPostedDate = value; }
        }

        public virtual string VoucherNo
        {
            get { return _voucherNo; }
            set { _voucherNo = value; }
        }

        public virtual string VoucherDescription
        {
            get { return _voucherDescription; }
            set { _voucherDescription = value; }
        }

        public virtual string VoucherDebitAccount
        {
            get { return _voucherDebitAccount; }
            set { _voucherDebitAccount = value; }
        }

        public virtual string VoucherCreditAccount
        {
            get { return _voucherCreditAccount; }
            set { _voucherCreditAccount = value; }
        }

        public virtual decimal? VoucherAmount
        {
            get { return _voucherAmount; }
            set { _voucherAmount = value; }
        }

        public virtual string CurrencyID
        {
            get { return _currencyID; }
            set { _currencyID = value; }
        }

        public virtual string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }

        public virtual int OrderPriority
        {
            get { return _orderPriority; }
            set { _orderPriority = value; }
        }
        public virtual Guid GeneralLedgerID
        {
            get { return _generalLedgerID; }
            set { _generalLedgerID = value; }
        }
        public virtual string Type { get; set; }
    }
    public class GVoucherListDetail_period
    {
        public string Period { get; set; }
    }
}