﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SABill
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _TypeID;
        private Guid? _AccountingObjectID;
        private string _AccountingObjectName;
        private string _AccountingObjectAddress;
        private int _InvoiceType;
        private DateTime? _InvoiceDate;
        private string _InvoiceNo;
        private string _InvoiceSeries;
        private int? _InvoiceForm;
        private Guid? _InvoiceTypeID;
        private string _InvoiceTemplate;
        private string _CurrencyID;
        private Decimal _ExchangeRate;
        private Decimal _TotalAmount;
        private Decimal _TotalAmountOriginal;
        private Decimal _TotalDiscountAmount;
        private Decimal _TotalDiscountAmountOriginal;
        private Decimal _TotalVATAmount;
        private Decimal _TotalVATAmountOriginal;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private DateTime? _RefDateTime;
        private Guid? _TemplateID;
        public virtual bool Check { get; set; }
        public virtual IList<SABillDetail> SABillDetails { get; set; }
        public virtual string ListNo { get; set; }
        public virtual DateTime? ListDate { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual string ListCommonNameInventory { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual bool? IsAttachList { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual string PaymentMethod { get; set; }
        private decimal _totalPaymentAMOriginalStand;
        public virtual decimal TotalPaymentAmountOriginal
        {
            get { return TotalAmountOriginal - TotalDiscountAmountOriginal + TotalVATAmountOriginal; }
            set { _totalPaymentAMOriginalStand = value; }
        }

        private decimal _totalPaymentAMStand;
        public virtual decimal TotalPaymentAmount { get { return TotalAmount - TotalDiscountAmount + TotalVATAmount; } set { _totalPaymentAMStand = value; } }
        public virtual List<Guid> SAIDs { get; set; }
        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public SABill()
        {
            RefVouchers = new List<RefVoucher>();
            SABillDetails = new List<SABillDetail>();
            SAIDs = new List<Guid>();
            ExchangeRate = 1;
            IsAttachList = false;
        }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return _AccountingObjectAddress; }
            set { _AccountingObjectAddress = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual int? InvoiceForm
        {
            get { return _InvoiceForm; }
            set { _InvoiceForm = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual string InvoiceTemplate
        {
            get { return _InvoiceTemplate; }
            set { _InvoiceTemplate = value; }
        }

        public virtual string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }

        public virtual Decimal ExchangeRate
        {
            get { return _ExchangeRate; }
            set { _ExchangeRate = value; }
        }

        public virtual Decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public virtual Decimal TotalAmountOriginal
        {
            get { return _TotalAmountOriginal; }
            set { _TotalAmountOriginal = value; }
        }

        public virtual Decimal TotalDiscountAmount
        {
            get { return _TotalDiscountAmount; }
            set { _TotalDiscountAmount = value; }
        }

        public virtual Decimal TotalDiscountAmountOriginal
        {
            get { return _TotalDiscountAmountOriginal; }
            set { _TotalDiscountAmountOriginal = value; }
        }

        public virtual Decimal TotalVATAmount
        {
            get { return _TotalVATAmount; }
            set { _TotalVATAmount = value; }
        }

        public virtual Decimal TotalVATAmountOriginal
        {
            get { return _TotalVATAmountOriginal; }
            set { _TotalVATAmountOriginal = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual DateTime? RefDateTime
        {
            get { return _RefDateTime; }
            set { _RefDateTime = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }
        public virtual int StatusInvoice { get; set; }
        public virtual bool StatusSendMail { get; set; }
        public virtual DateTime? DateSendMail { get; set; }
        public virtual string Email { get; set; }
        public virtual bool StatusConverted { get; set; }
        public virtual int CheckInvoice { get; set; }
        public virtual Guid? IDReplaceInv { get; set; }
        public virtual Guid? IDAdjustInv { get; set; }
        public virtual int? Type { get; set; } //2 tăng, 3 giảm, 4 tt
        public virtual string MaHD { get; set; }
        public virtual bool isHD { get; set; }
        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (SABillDetails.Count > 0)
                {
                    max = SABillDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        public virtual int? MaxOrderPriority
        {
            get
            {
                int? max1 = null;
                if (SABillDetails.Count > 0)
                {
                    max1 = SABillDetails.Max(k => k.OrderPriority) + 100000;
                }
                return max1;
            }
        }
        public virtual decimal? MaxDiscountRate
        {
            get
            {
                decimal? max = null;
                if (SABillDetails.Count > 0)
                {
                    max = SABillDetails.Max(k => k.DiscountRate);
                }
                return max;
            }
        }
        public virtual Guid? ID_MIV { get; set; }
        public virtual string DocumentNo { get; set; }
        public virtual DateTime? DocumentDate { get; set; }
        public virtual string DocumentNote { get; set; }
        public virtual Guid? ID_MIVAdjust { get; set; }
    }
}
