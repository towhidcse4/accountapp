using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class VoucherType
    {
        private Guid _iD;
        private string _voucherTypeCode;
        private string _voucherTypeName;
        private string _debitAccount;
        private string _creditAccount;
        private string _description;
        private int _typeGroupID;
        private Boolean _isSecurity;


        public virtual Guid ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        public virtual string VoucherTypeCode
        {
            get { return _voucherTypeCode; }
            set { _voucherTypeCode = value; }
        }

        public virtual string VoucherTypeName
        {
            get { return _voucherTypeName; }
            set { _voucherTypeName = value; }
        }

        public virtual string DebitAccount
        {
            get { return _debitAccount; }
            set { _debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _creditAccount; }
            set { _creditAccount = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual int TypeGroupID
        {
            get { return _typeGroupID; }
            set { _typeGroupID = value; }
        }

        public virtual Boolean IsSecurity
        {
            get { return _isSecurity; }
            set { _isSecurity = value; }
        }


    }
}
