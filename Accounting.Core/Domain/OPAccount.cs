﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class OPAccount
    {
        private Guid _iD;
        private Guid? _branchID;
        private int _typeID;
        private DateTime _postedDate;
        private string _accountNumber;
        private string _currencyID;
        private decimal? _exchangeRate;
        private decimal _debitAmount;
        private decimal _debitAmountOriginal;
        private decimal _creditAmount;
        private decimal _creditAmountOriginal;
        private Guid? _BankAccountDetailID;
        private Guid? _accountingObjectID;
        private Guid? _employeeID;
        private Guid? _contractID;
        private Guid? _costSetID;
        private Guid? _expenseItemID;
        private int _orderPriority;

        public virtual Guid ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        public virtual int TypeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }
        public virtual DateTime PostedDate
        {
            get { return _postedDate; }
            set { _postedDate = value; }
        }

        public virtual string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        public virtual string CurrencyID
        {
            get { return _currencyID; }
            set { _currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return _exchangeRate; }
            set { _exchangeRate = value; }
        }

        public virtual decimal DebitAmount
        {
            get { return _debitAmount; }
            set { _debitAmount = value; }
        }


        public virtual decimal DebitAmountOriginal
        {
            get { return _debitAmountOriginal; }
            set { _debitAmountOriginal = value; }
        }

        public virtual decimal CreditAmount
        {
            get { return _creditAmount; }
            set { _creditAmount = value; }
        }

        public virtual decimal CreditAmountOriginal
        {
            get { return _creditAmountOriginal; }
            set { _creditAmountOriginal = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return _BankAccountDetailID; }
            set { _BankAccountDetailID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _accountingObjectID; }
            set { _accountingObjectID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _contractID; }
            set { _contractID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _costSetID; }
            set { _costSetID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _expenseItemID; }
            set { _expenseItemID = value; }
        }

        public virtual int OrderPriority
        {
            get { return _orderPriority; }
            set { _orderPriority = value; }
        }

    }
}
