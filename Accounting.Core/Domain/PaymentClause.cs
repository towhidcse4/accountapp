using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PaymentClause
    {
        private Guid iD;
        private string paymentClauseCode;
        private string paymentClauseName;
        private int dueDate;
        private int discountDate;
        private bool isActive;
        private bool isSecurity;
        private decimal? discountPercent;
        private decimal? overdueInterestPercent;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string PaymentClauseCode
        {
            get { return paymentClauseCode; }
            set { paymentClauseCode = value; }
        }

        public virtual string PaymentClauseName
        {
            get { return paymentClauseName; }
            set { paymentClauseName = value; }
        }

        public virtual int DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual decimal? DiscountPercent
        {
            get { return discountPercent; }
            set { discountPercent = value; }
        }

        public virtual int DiscountDate
        {
            get { return discountDate; }
            set { discountDate = value; }
        }

        

        public virtual decimal? OverdueInterestPercent
        {
            get { return overdueInterestPercent; }
            set { overdueInterestPercent = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        
    }
}
