using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ExceptVoucher
    {

        private DateTime _date = DateTime.Now;
        private DateTime _postedDate = DateTime.Now;
        private DateTime _invoiceDate = DateTime.Now;
        public virtual Guid ID { get; set; }
        public virtual bool Status { get; set; }
        public virtual Guid GLVoucherID { get; set; }
        public virtual Guid GLVoucherExceptID { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual decimal RefVoucherExchangeRate { get; set; }
        public virtual decimal LastExchangeRate { get; set; }
        public virtual decimal DiffAmount { get; set; }

        public virtual DateTime Date
        {
            get;set;
        }
        public virtual DateTime PostedDate
        {
            get;set;
        }
        public virtual string No { get; set; }
        public virtual string ExceptNo { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual Guid AccountingObjectID { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime InvoiceDate
        {
            get { return _invoiceDate; }
            set { value = _invoiceDate; }
        }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int OrderPriority { get; set; }

        public virtual int TypeID { get; set; }
        public virtual Type Type { get; set; }
        public virtual string TypeName
        {
            get
            {
                try
                {
                    return Type.TypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual int ExceptTypeId { get; set; }
        public virtual Type ExceptType { get; set; }
        public virtual string ExceptTypeName
        {
            get
            {
                try
                {
                    return ExceptType.TypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual Guid? EmployeeID { get; set; }
        public virtual AccountingObject Employee { get; set; }
        public virtual string EmployeeName
        {
            get {
                try
                {
                    return Employee.AccountingObjectName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual Guid? ExceptEmployeeID { get; set; }
        public virtual AccountingObject ExceptEmployee { get; set; }
        public virtual string ExceptEmployeeName
        {
            get
            {
                try
                {
                    return ExceptEmployee.AccountingObjectName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual string CurrencyID{ get; set; }
    }
}