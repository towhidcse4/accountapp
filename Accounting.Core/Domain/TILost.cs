using System;
using System.Text;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TILost
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _TypeID;
        private DateTime _Date;
        private string _No;
        private string _Reason;
        private Decimal _TotalAmount;
        private Boolean _Recorder;
        private Boolean _Exported;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual Decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public virtual Boolean Recorder
        {
            get { return _Recorder; }
            set { _Recorder = value; }
        }

        public virtual Boolean Exported
        {
            get { return _Exported; }
            set { _Exported = value; }
        }
        public virtual Guid? TemplateID { get; set; }
        public virtual IList<TILostDetail> TILostDetails { get; set; }
    }
}
