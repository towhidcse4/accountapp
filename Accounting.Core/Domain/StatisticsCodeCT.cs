﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class StatisticsCodeCT
    {
        public bool Check { get; set; }
        private Guid iD;
        private string StatisticsCode;
        private string StatisticsCodeName;
        private int grade;
        private bool isActive;
        private bool isSecurity;
        private string description;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;

        public Guid ID { get => iD; set => iD = value; }
        public string StatisticsCode1 { get => StatisticsCode; set => StatisticsCode = value; }
        public string StatisticsCodeName1 { get => StatisticsCodeName; set => StatisticsCodeName = value; }
        public int Grade { get => grade; set => grade = value; }
        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsSecurity { get => isSecurity; set => isSecurity = value; }
        public string Description { get => description; set => description = value; }
        public Guid? ParentID { get => parentID; set => parentID = value; }
        public string OrderFixCode { get => orderFixCode; set => orderFixCode = value; }
        public bool IsParentNode { get => isParentNode; set => isParentNode = value; }
        public StatisticsCodeCT()
        {
            Check = false;
        }
    }
}
