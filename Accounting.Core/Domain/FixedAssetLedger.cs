﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FixedAssetLedger
    {
        private Guid id;
        private Guid branchID;
        private Guid referenceID;
        private string no;
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private Guid? fixedAssetID;
        private Guid? departmentID;
        private decimal? usedTime;
        private decimal? depreciationRate;
        private decimal? depreciationAmount;
        private string originalPriceAccount;
        private decimal? originalPriceDebitAmount;
        private decimal? originalPriceCreditAmount;
        private string depreciationAccount;
        private decimal? depreciationDebitAmount;
        private decimal? depreciationCreditAmount;
        private string reason;
        private string description;
        private int orderPriority;
        private bool? isIrrationalCost;

        public virtual decimal? UsedTimeRemain { get; set; }
        public virtual DateTime? IncrementDate { get; set; }
        public virtual string Nos { get; set; }
        public virtual decimal? DifferUsedTime { get; set; }
        public virtual decimal? MonthDepreciationRate { get; set; }
        public virtual decimal? MonthPeriodDepreciationAmount { get; set; }
        public virtual decimal? AcDepreciationAmount { get; set; }
        public virtual decimal? RemainingAmount { get; set; }
        public virtual decimal? OriginalPrice { get; set; }
        public virtual decimal? DifferOrgPrice { get; set; }
        public virtual decimal? DifferAcDepreciationAmount { get; set; }
        public virtual decimal? DifferMonthlyDepreciationAmount { get; set; }
        public virtual decimal? DifferRemainingAmount { get; set; }
        public virtual decimal? DifferDepreciationAmount { get; set; }

        public virtual bool? IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }
        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }
        public virtual decimal? DepreciationCreditAmount
        {
            get { return depreciationCreditAmount; }
            set { depreciationCreditAmount = value; }
        }
        public virtual decimal? DepreciationDebitAmount
        {
            get { return depreciationDebitAmount; }
            set { depreciationDebitAmount = value; }
        }
        public virtual string DepreciationAccount
        {
            get { return depreciationAccount; }
            set { depreciationAccount = value; }
        }
        public virtual decimal? OriginalPriceCreditAmount
        {
            get { return originalPriceCreditAmount; }
            set { originalPriceCreditAmount = value; }
        }
        public virtual decimal? OriginalPriceDebitAmount
        {
            get { return originalPriceDebitAmount; }
            set { originalPriceDebitAmount = value; }
        }
        public virtual string OriginalPriceAccount
        {
            get { return originalPriceAccount; }
            set { originalPriceAccount = value; }
        }
        public virtual decimal? DepreciationAmount
        {
            get { return depreciationAmount; }
            set { depreciationAmount = value; }
        }
        public virtual decimal? DepreciationRate
        {
            get { return depreciationRate; }
            set { depreciationRate = value; }
        }
        public virtual decimal? UsedTime
        {
            get { return usedTime; }
            set { usedTime = value; }
        }
        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }
        public virtual Guid? FixedAssetID
        {
            get { return fixedAssetID; }
            set { fixedAssetID = value; }
        }
        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }
        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }
        public virtual Guid ReferenceID
        {
            get { return referenceID; }
            set { referenceID = value; }
        }
        public virtual Guid BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid ID
        {
            get { return id; }
            set { id = value; }
        }
    }
}
