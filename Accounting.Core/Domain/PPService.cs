

using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPService
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private decimal totalDiscountAmount;
        private decimal totalDiscountAmountOriginal;
        private Guid iD;
        private bool isFeightService;
        private bool recorded;
        private bool exported;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? branchID;
        private Guid? paymentClauseID;
        private bool isMatch;
        private DateTime? matchDate;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private DateTime? dueDate;
        private string creditCardNumber;
        private Guid? bankAccountDetailID;
        private string bankName;
        private Guid? pPInvoiceID;
        private Guid? pPOrderID;
        private Guid? employeeID;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string accountingObjectContactName;
        private Guid? accountingObjectBankAccountDetailID;
        private string accountingObjectBankName;
        private string companyTaxCode;
        private string reason;
        private string numberAttach;
        private string contactName;
        private string identificationNo;
        private string issueBy;
        private DateTime? issueDate;
        private Guid? templateID;
        private decimal? totalAll;
        private decimal? totalAllOriginal;
        private bool _checkColumn;

        private Type _type;

        public virtual IList<PPServiceDetail> PPServiceDetails { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        private bool billReceived;
        public virtual IList<PPServiceDetail> MCPaymentDetails
        {
            get { return PPServiceDetails; }
            set { PPServiceDetails = value; }
        }

        public virtual IList<MCPaymentDetailTax> MCPaymentDetailTaxs { get; set; }
        public virtual IList<MBTellerPaperDetail> MBTellerPaperDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public PPService()
        {
            RefVouchers = new List<RefVoucher>();
            CurrencyID = "VND";
            PPServiceDetails = new List<PPServiceDetail>();
            MCPaymentDetailTaxs = new List<MCPaymentDetailTax>();
            MBTellerPaperDetails = new List<MBTellerPaperDetail>();
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual decimal TotalDiscountAmount
        {
            get { return totalDiscountAmount; }
            set { totalDiscountAmount = value; }
        }

        public virtual decimal TotalDiscountAmountOriginal
        {
            get { return totalDiscountAmountOriginal; }
            set { totalDiscountAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool IsFeightService
        {
            get { return isFeightService; }
            set { isFeightService = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual string CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual Guid? PPInvoiceID
        {
            get { return pPInvoiceID; }
            set { pPInvoiceID = value; }
        }

        public virtual Guid? PPOrderID
        {
            get { return pPOrderID; }
            set { pPOrderID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string AccountingObjectContactName
        {
            get { return accountingObjectContactName; }
            set { accountingObjectContactName = value; }
        }

        public virtual Guid? AccountingObjectBankAccountDetailID
        {
            get { return accountingObjectBankAccountDetailID; }
            set { accountingObjectBankAccountDetailID = value; }
        }

        public virtual string AccountingObjectBankName
        {
            get { return accountingObjectBankName; }
            set { accountingObjectBankName = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string NumberAttach
        {
            get { return numberAttach; }
            set { numberAttach = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual string IdentificationNo
        {
            get { return identificationNo; }
            set { identificationNo = value; }
        }

        public virtual string IssueBy
        {
            get { return issueBy; }
            set { issueBy = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }


        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual decimal? TotalAll
        {
            get { return TotalAmount - TotalDiscountAmount + TotalVATAmount; }
            set { totalAll = value; }
        }

        public virtual decimal? TotalAllOriginal
        {
            get { return TotalAmountOriginal - TotalDiscountAmountOriginal + TotalVATAmountOriginal; }
            set { totalAllOriginal = value; }
        }

        public virtual string TypeName
        {
            get { return Type.TypeName; }
        }

        public virtual string InvoiceNo
        {
            get
            {
                string s = string.Empty;
                for (int i = 0; i < PPServiceDetails.Count; i++)
                {
                    if (string.IsNullOrEmpty(PPServiceDetails[i].InvoiceNo) || s.Split(';').ToList().Any(n => n == PPServiceDetails[i].InvoiceNo)) continue;
                    if (i == 0)
                    {
                        s += PPServiceDetails[i].InvoiceNo;
                        //if (i < PPServiceDetails.Count - 1)
                        //    s += "; ";
                    }
                    else
                    {
                        s += ";" + PPServiceDetails[i].InvoiceNo;
                    }
                }
                return s;
            }
        }

        public virtual bool CheckColumn
        {
            get { return _checkColumn; }
            set { _checkColumn = value; }
        }
        public virtual bool BillReceived
        {
            get { return billReceived; }
            set { billReceived = value; }
        }
    }
}
