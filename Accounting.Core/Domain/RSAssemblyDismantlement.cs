using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RSAssemblyDismantlement
    {
        private int typeID;
        private string typeIDViewNameCode;
        private Guid iD;
        private Guid? materialGoodsID;
        private Guid? repositoryID;
        private decimal unitPrice;
        private decimal unitPriceOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private bool recorded;
        private bool exported;
        private Guid? confrontID;
        private Guid? confrontDetailID;
        private string unit;
        private decimal? quantity;
        private string materialGoodsName;
        private Guid? branchID;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? templateID;
        private DateTime date;
        private string no;
        private string reason;

        public RSAssemblyDismantlement()
        {
            RSAssemblyDismantlementDetails = new List<RSAssemblyDismantlementDetail>();
            RefVouchers = new List<RefVoucher>();
        }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public virtual IList<RSAssemblyDismantlementDetail> RSAssemblyDismantlementDetails { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual Guid? RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal UnitPriceOriginal
        {
            get { return unitPriceOriginal; }
            set { unitPriceOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? ConfrontID
        {
            get { return confrontID; }
            set { confrontID = value; }
        }

        public virtual Guid? ConfrontDetailID
        {
            get { return confrontDetailID; }
            set { confrontDetailID = value; }
        }
        
        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual string MaterialGoodsName
        {
            get { return materialGoodsName; }
            set { materialGoodsName = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }
        
        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        public virtual string TypeIDViewNameCode
        {
            get { return typeIDViewNameCode; }
            set { typeIDViewNameCode = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }
        
        public virtual MaterialGoods MaterialGoods { get; set; }
    }
}
