using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCPaymentDetailInsurance
    {
        private decimal accumAmount;
        private decimal payAmount;
        private decimal remainningAmount;
        private Guid iD;
        private Guid mCPaymentID;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private Guid? budgetItemID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? accountingObjectID;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private Guid? statisticsCodeID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;


        public virtual decimal AccumAmount
        {
            get { return accumAmount; }
            set { accumAmount = value; }
        }

        public virtual decimal PayAmount
        {
            get { return payAmount; }
            set { payAmount = value; }
        }

        public virtual decimal RemainningAmount
        {
            get { return remainningAmount; }
            set { remainningAmount = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MCPaymentID
        {
            get { return mCPaymentID; }
            set { mCPaymentID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }




    }
}
