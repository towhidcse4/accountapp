using System;

namespace Accounting.Core.Domain
{
    public class TM02TAINAdjust
    {
        private Guid _ID;
        private Guid _TM02TAINID;
        private string _Code;
        private string _Name;
        private Decimal _DeclaredAmount;
        private Decimal _AdjustAmount;
        private Decimal _DifferAmount;
        private int _LateDays;
        private Decimal _LateAmount;
        private Decimal _ExplainAmount;
        private string _CommandNo;
        private DateTime _CommandDate;
        private string _TaxCompanyName;
        private string _TaxCompanyDecisionName;
        private int _ReceiveDays;
        private Decimal _ExplainLateAmount;
        private string _DifferReason;
        private int? _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM02TAINID
        {
            get { return _TM02TAINID; }
            set { _TM02TAINID = value; }
        }

        public virtual string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual Decimal DeclaredAmount
        {
            get { return _DeclaredAmount; }
            set { _DeclaredAmount = value; }
        }

        public virtual Decimal AdjustAmount
        {
            get { return _AdjustAmount; }
            set { _AdjustAmount = value; }
        }

        public virtual Decimal DifferAmount
        {
            get { return _DifferAmount; }
            set { _DifferAmount = value; }
        }

        public virtual int LateDays
        {
            get { return _LateDays; }
            set { _LateDays = value; }
        }

        public virtual Decimal LateAmount
        {
            get { return _LateAmount; }
            set { _LateAmount = value; }
        }

        public virtual Decimal ExplainAmount
        {
            get { return _ExplainAmount; }
            set { _ExplainAmount = value; }
        }

        public virtual string CommandNo
        {
            get { return _CommandNo; }
            set { _CommandNo = value; }
        }

        public virtual DateTime CommandDate
        {
            get { return _CommandDate; }
            set { _CommandDate = value; }
        }

        public virtual string TaxCompanyName
        {
            get { return _TaxCompanyName; }
            set { _TaxCompanyName = value; }
        }

        public virtual string TaxCompanyDecisionName
        {
            get { return _TaxCompanyDecisionName; }
            set { _TaxCompanyDecisionName = value; }
        }

        public virtual int ReceiveDays
        {
            get { return _ReceiveDays; }
            set { _ReceiveDays = value; }
        }

        public virtual Decimal ExplainLateAmount
        {
            get { return _ExplainLateAmount; }
            set { _ExplainLateAmount = value; }
        }

        public virtual string DifferReason
        {
            get { return _DifferReason; }
            set { _DifferReason = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
