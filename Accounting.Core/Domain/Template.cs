using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Template
    {
        public Template()
        {
            TemplateDetails = new List<TemplateDetail>();
            TemplateDetailsBinding = new List<TemplateDetail>();
        }

        public virtual Guid ID { get; set; }
        public virtual string TemplateName { get; set; }
        public virtual int TypeID { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsSecurity { get; set; }
        public virtual int OrderPriority { get; set; }
        public virtual bool? IsDefault { get; set; }

        public virtual IList<TemplateDetail> TemplateDetails { get; set; }
        public virtual IList<TemplateDetail> TemplateDetailsBinding { get; set; }
    }
}
