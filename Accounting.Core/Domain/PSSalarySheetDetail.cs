using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSSalarySheetDetail
    {
        private Guid _ID;
        private Guid _PSSalarySheetID;
        private Guid? _EmployeeID;
        private string _AccountingObjectName;
        private string _AccountingObjectTitle;
        private Guid? _DepartmentID;
        private Decimal _SalaryCoefficient;
        private Decimal _BasicWage;
        private Decimal _WorkingDayUnitPrice;
        private Decimal _WorkingHourUnitPrice;
        private Decimal _AgreementSalary;
        private Decimal _NumberOfPaidWorkingDayTimeSheet;
        private Decimal _PaidWorkingDayAmount;
        private Decimal _NumberOfNonWorkingDayTimeSheet;
        private Decimal _NonWorkingDayAmount;
        private Decimal _ProductUnitPrice;
        private Decimal _TotalProduct;
        private Decimal _ProductAmount;
        private Decimal _ProductAmountOriginal;
        private Decimal _TotalOverTime;
        private Decimal _OverTimeAmount;
        private Decimal? _PayrollFundAllowance;
        private Decimal? _OtherAllowance;
        private Decimal _TotalAmount;
        private Decimal _TemporaryAmount;
        private Decimal _EmployeeSocityInsuranceAmount;
        private Decimal _EmployeeAccidentInsuranceAmount;
        private Decimal _EmployeeMedicalInsuranceAmount;
        private Decimal _EmployeeUnEmployeeInsuranceAmount;
        private Decimal _EmployeeTradeUnionInsuranceAmount;
        private Decimal _IncomeTaxAmount;
        private Decimal _SumOfDeductionAmount;
        private Decimal _TotalPersonalTaxIncomeAmount;
        private Decimal _ReduceSelfTaxAmount;
        private Decimal _ReduceDependTaxAmount;
        private Decimal _IncomeForTaxCalcuation;
        private string _SignName;
        private Decimal _InsuranceSalary;
        private Decimal _NetAmount;
        private Decimal _NetAmountOriginal;
        private Decimal _CompanySocityInsuranceAmount;
        private Decimal _CompanytAccidentInsuranceAmount;
        private Decimal _CompanyMedicalInsuranceAmount;
        private Decimal _CompanyUnEmployeeInsuranceAmount;
        private Decimal _CompanyTradeUnionInsuranceAmount;
        private int _OrderPriority;
        private string departmentCode;
        public virtual Department Departments { get; set; }
        public virtual int Stt { get; set; }
        public virtual string DepartmentCode
        {
            get
            {
                if (Departments != null)
                    return Departments.DepartmentCode;
                else return departmentCode;
            }
            set
            {
                departmentCode = value;
            }
        }
        public virtual string accountingObjectCode { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsEmployee { get; set; }
        public virtual bool? IsUnofficialStaff { get; set; }
        public virtual int NumberOfDependent { get; set; }
        public virtual decimal NotIncomeTaxAllowance { get; set; }
        public virtual decimal BasicWageAmount { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid PSSalarySheetID
        {
            get { return _PSSalarySheetID; }
            set { _PSSalarySheetID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string AccountingObjectTitle
        {
            get { return _AccountingObjectTitle; }
            set { _AccountingObjectTitle = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Decimal SalaryCoefficient
        {
            get { return _SalaryCoefficient; }
            set { _SalaryCoefficient = value; }
        }

        public virtual Decimal BasicWage
        {
            get { return _BasicWage; }
            set { _BasicWage = value; }
        }

        public virtual Decimal WorkingDayUnitPrice
        {
            get { return _WorkingDayUnitPrice; }
            set { _WorkingDayUnitPrice = value; }
        }

        public virtual Decimal WorkingHourUnitPrice
        {
            get { return _WorkingHourUnitPrice; }
            set { _WorkingHourUnitPrice = value; }
        }

        public virtual Decimal AgreementSalary
        {
            get { return _AgreementSalary; }
            set { _AgreementSalary = value; }
        }

        public virtual Decimal NumberOfPaidWorkingDayTimeSheet
        {
            get { return _NumberOfPaidWorkingDayTimeSheet; }
            set { _NumberOfPaidWorkingDayTimeSheet = value; }
        }

        public virtual Decimal PaidWorkingDayAmount
        {
            get { return _PaidWorkingDayAmount; }
            set { _PaidWorkingDayAmount = value; }
        }

        public virtual Decimal NumberOfNonWorkingDayTimeSheet
        {
            get { return _NumberOfNonWorkingDayTimeSheet; }
            set { _NumberOfNonWorkingDayTimeSheet = value; }
        }

        public virtual Decimal NonWorkingDayAmount
        {
            get { return _NonWorkingDayAmount; }
            set { _NonWorkingDayAmount = value; }
        }

        public virtual Decimal ProductUnitPrice
        {
            get { return _ProductUnitPrice; }
            set { _ProductUnitPrice = value; }
        }

        public virtual Decimal TotalProduct
        {
            get { return _TotalProduct; }
            set { _TotalProduct = value; }
        }

        public virtual Decimal ProductAmount
        {
            get { return _ProductAmount; }
            set { _ProductAmount = value; }
        }

        public virtual Decimal ProductAmountOriginal
        {
            get { return _ProductAmountOriginal; }
            set { _ProductAmountOriginal = value; }
        }

        public virtual Decimal TotalOverTime
        {
            get { return _TotalOverTime; }
            set { _TotalOverTime = value; }
        }

        public virtual Decimal OverTimeAmount
        {
            get { return _OverTimeAmount; }
            set { _OverTimeAmount = value; }
        }

        public virtual Decimal? PayrollFundAllowance
        {
            get { return _PayrollFundAllowance; }
            set { _PayrollFundAllowance = value; }
        }

        public virtual Decimal? OtherAllowance
        {
            get { return _OtherAllowance; }
            set { _OtherAllowance = value; }
        }

        public virtual Decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public virtual Decimal TemporaryAmount
        {
            get { return _TemporaryAmount; }
            set { _TemporaryAmount = value; }
        }

        public virtual Decimal EmployeeSocityInsuranceAmount
        {
            get { return _EmployeeSocityInsuranceAmount; }
            set { _EmployeeSocityInsuranceAmount = value; }
        }

        public virtual Decimal EmployeeAccidentInsuranceAmount
        {
            get { return _EmployeeAccidentInsuranceAmount; }
            set { _EmployeeAccidentInsuranceAmount = value; }
        }

        public virtual Decimal EmployeeMedicalInsuranceAmount
        {
            get { return _EmployeeMedicalInsuranceAmount; }
            set { _EmployeeMedicalInsuranceAmount = value; }
        }

        public virtual Decimal EmployeeUnEmployeeInsuranceAmount
        {
            get { return _EmployeeUnEmployeeInsuranceAmount; }
            set { _EmployeeUnEmployeeInsuranceAmount = value; }
        }

        public virtual Decimal EmployeeTradeUnionInsuranceAmount
        {
            get { return _EmployeeTradeUnionInsuranceAmount; }
            set { _EmployeeTradeUnionInsuranceAmount = value; }
        }

        public virtual Decimal IncomeTaxAmount
        {
            get { return _IncomeTaxAmount; }
            set { _IncomeTaxAmount = value; }
        }

        public virtual Decimal SumOfDeductionAmount
        {
            get { return _SumOfDeductionAmount; }
            set { _SumOfDeductionAmount = value; }
        }

        public virtual Decimal TotalPersonalTaxIncomeAmount
        {
            get { return _TotalPersonalTaxIncomeAmount; }
            set { _TotalPersonalTaxIncomeAmount = value; }
        }

        public virtual Decimal ReduceSelfTaxAmount
        {
            get { return _ReduceSelfTaxAmount; }
            set { _ReduceSelfTaxAmount = value; }
        }

        public virtual Decimal ReduceDependTaxAmount
        {
            get { return _ReduceDependTaxAmount; }
            set { _ReduceDependTaxAmount = value; }
        }

        public virtual Decimal IncomeForTaxCalcuation
        {
            get { return _IncomeForTaxCalcuation; }
            set { _IncomeForTaxCalcuation = value; }
        }

        public virtual string SignName
        {
            get { return _SignName; }
            set { _SignName = value; }
        }

        public virtual Decimal InsuranceSalary
        {
            get { return _InsuranceSalary; }
            set { _InsuranceSalary = value; }
        }

        public virtual Decimal NetAmount
        {
            get { return _NetAmount; }
            set { _NetAmount = value; }
        }

        public virtual Decimal NetAmountOriginal
        {
            get { return _NetAmountOriginal; }
            set { _NetAmountOriginal = value; }
        }

        public virtual Decimal CompanySocityInsuranceAmount
        {
            get { return _CompanySocityInsuranceAmount; }
            set { _CompanySocityInsuranceAmount = value; }
        }

        public virtual Decimal CompanytAccidentInsuranceAmount
        {
            get { return _CompanytAccidentInsuranceAmount; }
            set { _CompanytAccidentInsuranceAmount = value; }
        }

        public virtual Decimal CompanyMedicalInsuranceAmount
        {
            get { return _CompanyMedicalInsuranceAmount; }
            set { _CompanyMedicalInsuranceAmount = value; }
        }

        public virtual Decimal CompanyUnEmployeeInsuranceAmount
        {
            get { return _CompanyUnEmployeeInsuranceAmount; }
            set { _CompanyUnEmployeeInsuranceAmount = value; }
        }

        public virtual Decimal CompanyTradeUnionInsuranceAmount
        {
            get { return _CompanyTradeUnionInsuranceAmount; }
            set { _CompanyTradeUnionInsuranceAmount = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }

        public PSSalarySheetDetail()
        {
            Departments = new Department();
        }
    }
}
