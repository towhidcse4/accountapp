using System;

namespace Accounting.Core.Domain
{
    public class TT153DeletedInvoice
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime _Date;
        private string _No;
        private string _Reason;
        private string _AttachFileName;
        private Byte[] _AttachFileContent;
        private Guid _SAInvoiceID;
        private Guid ?_InvoiceTypeID;
        private string _InvoiceSeries;
        private string _InvoiceNo;
        private DateTime? _InvoiceDate;
        private string _AccountingObjectName;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual string AttachFileName
        {
            get { return _AttachFileName; }
            set { _AttachFileName = value; }
        }

        public virtual Byte[] AttachFileContent
        {
            get { return _AttachFileContent; }
            set { _AttachFileContent = value; }
        }

        public virtual Guid SAInvoiceID
        {
            get { return _SAInvoiceID; }
            set { _SAInvoiceID = value; }
        }

        public virtual Guid ?InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        //public virtual SAInvoice SAInvoice { get; set; }
        //public virtual DateTime ?DateS
        //{
        //    get
        //    {
        //            return SAInvoice.Date;
        //    }
        //    set { }
        //}
        
    }
}
