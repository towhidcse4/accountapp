using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBCreditCardDetailTax
    {
        private decimal pretaxAmount;
        private decimal pretaxAmountOriginal;
        private Guid iD ;
        private Guid mBCreditCardID;
        private decimal vATAmount;
        private decimal vATAmountOriginal;
        private decimal? vATRate;
        private string description;
        private int? invoiceType;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string invoiceSeries;
        private Guid? goodsServicePurchaseID;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string companyTaxCode;
        private string vATAccount;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;

        public virtual string InvoiceTemplate { get; set; }
        public virtual decimal PretaxAmount
        {
            get
            {
                if (VATRate.HasValue && VATRate.Value != -1 && VATRate.Value != 0)
                    return VATAmount / (VATRate.Value / 100);
                return 0;
            }
            set { pretaxAmount = value; }
        }

        public virtual decimal PretaxAmountOriginal
        {
            get { return pretaxAmountOriginal; }
            set { pretaxAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MBCreditCardID
        {
            get { return mBCreditCardID; }
            set { mBCreditCardID = value; }
        }

        public virtual decimal VATAmount
        {
            get { return vATAmount; }
            set { vATAmount = value; }
        }

        public virtual decimal VATAmountOriginal
        {
            get { return vATAmountOriginal; }
            set { vATAmountOriginal = value; }
        }

        public virtual decimal? VATRate
        {
            get { return vATRate; }
            set { vATRate = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual int? InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }

        public virtual Guid? GoodsServicePurchaseID
        {
            get { return goodsServicePurchaseID; }
            set { goodsServicePurchaseID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual string VATAccount
        {
            get { return vATAccount; }
            set { vATAccount = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        private MBCreditCard _MBCreditCard;
        public virtual MBCreditCard MBCreditCard
        {
            get { return _MBCreditCard; }
            set { _MBCreditCard = value; }
        }





    }
}
