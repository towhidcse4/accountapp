using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysRolepermission {
        public virtual System.Guid ID { get; set; }
        public virtual System.Guid RoleID { get; set; }
        public virtual System.Guid PermissionID { get; set; }
        public virtual string SubSystemCode { get; set; }
    }
}
