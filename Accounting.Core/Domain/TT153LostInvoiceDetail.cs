using System;
namespace Accounting.Core.Domain
{
    public class TT153LostInvoiceDetail
    {
        private Guid _ID;
        private Guid _TT153LostInvoiceID;
        private Guid _TT153ReportID;
        private Guid _TT153PublishInvoiceDetailID;
        private int _InvoiceType;
        private Guid _InvoiceTypeID;
        private string _InvoiceSeries;
        private string _FromNo;
        private string _ToNo;
        private Decimal _Quantity;
        private string _CopyPart;
        private string _Description;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TT153LostInvoiceID
        {
            get { return _TT153LostInvoiceID; }
            set { _TT153LostInvoiceID = value; }
        }

        public virtual Guid TT153ReportID
        {
            get { return _TT153ReportID; }
            set { _TT153ReportID = value; }
        }

        public virtual Guid TT153PublishInvoiceDetailID
        {
            get { return _TT153PublishInvoiceDetailID; }
            set { _TT153PublishInvoiceDetailID = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }

        public virtual Guid InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual string FromNo
        {
            get { return _FromNo; }
            set { _FromNo = value; }
        }

        public virtual string ToNo
        {
            get { return _ToNo; }
            set { _ToNo = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual string CopyPart
        {
            get { return _CopyPart; }
            set { _CopyPart = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }
        public virtual TT153Report tT153Report { get; set; }
        public virtual string ReportName
        {
            get
            {
                if (tT153Report == null) return "";
                try
                {
                    return tT153Report.ReportName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
        public virtual string InvoiceTemplate
        {
            get
            {
                if (tT153Report == null) return "";
                try
                {
                    return tT153Report.InvoiceTemplate;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
    }
}
