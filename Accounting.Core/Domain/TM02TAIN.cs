using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TM02TAIN
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private string _DeclarationName;
        private string _DeclarationTerm;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private Boolean _IsFirstDeclaration;
        private int _AdditionTime;
        private DateTime? _AdditionDate;
        private string _CompanyName;
        private string _CompanyTaxCode;
        private string _TaxAgencyTaxCode;
        private string _TaxAgencyName;
        private string _TaxAgencyEmployeeName;
        private string _CertificationNo;
        private string _SignName;
        private DateTime? _SignDate;

        public TM02TAIN()
        {
            TM02TAINDetails = new List<TM02TAINDetail>();
        }
        public virtual IList<TM02TAINDetail> TM02TAINDetails { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string DeclarationName
        {
            get { return _DeclarationName; }
            set { _DeclarationName = value; }
        }

        public virtual string DeclarationTerm
        {
            get { return _DeclarationTerm; }
            set { _DeclarationTerm = value; }
        }

        public virtual DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public virtual Boolean IsFirstDeclaration
        {
            get { return _IsFirstDeclaration; }
            set { _IsFirstDeclaration = value; }
        }

        public virtual int AdditionTime
        {
            get { return _AdditionTime; }
            set { _AdditionTime = value; }
        }

        public virtual DateTime? AdditionDate
        {
            get { return _AdditionDate; }
            set { _AdditionDate = value; }
        }

        public virtual string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return _CompanyTaxCode; }
            set { _CompanyTaxCode = value; }
        }

        public virtual string TaxAgencyTaxCode
        {
            get { return _TaxAgencyTaxCode; }
            set { _TaxAgencyTaxCode = value; }
        }

        public virtual string TaxAgencyName
        {
            get { return _TaxAgencyName; }
            set { _TaxAgencyName = value; }
        }

        public virtual string TaxAgencyEmployeeName
        {
            get { return _TaxAgencyEmployeeName; }
            set { _TaxAgencyEmployeeName = value; }
        }

        public virtual string CertificationNo
        {
            get { return _CertificationNo; }
            set { _CertificationNo = value; }
        }

        public virtual string SignName
        {
            get { return _SignName; }
            set { _SignName = value; }
        }

        public virtual DateTime? SignDate
        {
            get { return _SignDate; }
            set { _SignDate = value; }
        }
        public virtual List<TM02TAINDetail> TM02TAINDetails1 { get; set; }
        public virtual List<TM02TAINDetail> TM02TAINDetails2 { get; set; }


    }
}
