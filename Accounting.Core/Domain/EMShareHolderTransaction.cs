﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMShareHolderTransaction
    {
        private Guid iD;
        private Guid eMShareHolderID;
        private string eMShareHolderViewID;
        private Guid eMRegistrationID;
        private Guid stockCategoryID;
        private string stockCategoryViewID;
        private DateTime transactionDate;
        private int transactionType;
        private decimal incrementQuantity;
        private decimal decrementQuantity;
        private string description;
        private int orderPriority;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid EMShareHolderID
        {
            get { return eMShareHolderID; }
            set { eMShareHolderID = value; }
        }
        public virtual string EMShareHolderViewID
        {
            get { return eMShareHolderViewID; }
            set { eMShareHolderViewID = value; }
        }
        public virtual Guid EMRegistrationID
        {
            get { return eMRegistrationID; }
            set { eMRegistrationID = value; }
        }
        public virtual Guid StockCategoryID
        {
            get { return stockCategoryID; }
            set { stockCategoryID = value; }
        }
        public virtual string StockCategoryViewID
        {
            get { return stockCategoryViewID; }
            set { stockCategoryViewID = value; }
        }
        public virtual DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }
        public virtual int TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }
        public virtual decimal IncrementQuantity
        {
            get { return incrementQuantity; }
            set { incrementQuantity = value; }
        }
        public virtual decimal DecrementQuantity
        {
            get { return decrementQuantity; }
            set { decrementQuantity = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
    }
}
