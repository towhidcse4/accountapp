﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class OverBalanceBook
    {
        public Int64 RowNum { get; set; }
        public Guid ID { get; set; }
        public string BankAccount { get; set; }
        public string BankName { get; set; }
        public string BankBranchName { get; set; }
        //public decimal OpenAmountOC { get; set; } //comment by cuongpv
        public decimal? OpenAmount { get; set; }
        //public decimal DebitAmountOC { get; set; } //comment by cuongpv
        public decimal? DebitAmount { get; set; }
        //public decimal CreditAmountOC { get; set; } //comment by cuongpv
        public decimal? CreditAmount { get; set; }
        //public decimal CloseAmountOC { get; set; } //comment by cuongpv
        public decimal? CloseAmount { get; set; }
    }
    public class OverBalanceBookDetail
    {
        public string Period { get; set; }
    }
}
