

using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPInvoice
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private bool billReceived;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalImportTaxAmount;
        private decimal totalImportTaxAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private decimal totalDiscountAmount;
        private decimal totalDiscountAmountOriginal;
        private decimal totalInwardAmount;
        private decimal totalInwardAmountOriginal;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid? branchID;
        private bool isMatch;
        private DateTime? matchDate;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? paymentClauseID;
        private DateTime? dueDate;
        private Guid? transportMethodID;
        private Guid? employeeID;
        private bool? isImportPurchase;
        private bool? isSpecialConsumeTax;
        private bool? storedInRepository;
        private string inwardNo;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string contactName;
        private string reason;
        private string originalNo;
        private DateTime? mPostedDate;
        private DateTime? mDate;
        private Guid? mAccountingObjectBankAccountDetailID;
        private string mAccountingObjectBankName;
        private string mContactName;
        private string mReasonPay;
        private Guid? bankAccountDetailID;
        private string bankName;
        private string creditCardNumber;
        private string numberAttach;
        private string identificationNo;
        private DateTime? issueDate;
        private string issueBy;
        private Guid? templateID;

        private Type _type;
        public virtual IList<PPInvoiceDetail> PPInvoiceDetails { get; set; }
        public virtual IList<PPInvoiceDetailCost> PPInvoiceDetailCosts { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public PPInvoice()
        {
            RefVouchers = new List<RefVoucher>();
            PPInvoiceDetails = new List<PPInvoiceDetail>();
            //PPServices = new List<PPService>();
            PPInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
            TotalImportTaxAmount = 0;
            TotalImportTaxAmountOriginal = 0;
            TotalAll = 0;
            TotalAllOriginal = 0;
            TotalAmount = 0;
            TotalAmountOriginal = 0;
            TotalDiscountAmount = 0;
            TotalDiscountAmountOriginal = 0;
            TotalFreightAmount = 0;
            TotalFreightAmountOriginal = 0;
            TotalImportTaxAmount = 0;
            TotalImportTaxAmountOriginal = 0;
            TotalInwardAmount = 0;
            TotalInwardAmountOriginal = 0;
            TotalSpecialConsumeTaxAmount = 0;
            TotalSpecialConsumeTaxAmountOriginal = 0;
            TotalVATAmount = 0;
            TotalVATAmountOriginal = 0;
            TotalImportTaxExpenseAmountOriginal = 0;
            TotalImportTaxExpenseAmount = 0;
            TotalAllNoVatOriginal = 0;
            TotalAllNoVat = 0;
            TotalAllValue = 0;
            TotalAllValueOriginal = 0;
            CurrencyID = "VND";
        }
        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual bool BillReceived
        {
            get { return billReceived; }
            set { billReceived = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalImportTaxAmount
        {
            get { return totalImportTaxAmount; }
            set { totalImportTaxAmount = value; }
        }

        public virtual decimal TotalImportTaxAmountOriginal
        {
            get { return totalImportTaxAmountOriginal; }
            set { totalImportTaxAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual decimal TotalDiscountAmount
        {
            get { return totalDiscountAmount; }
            set { totalDiscountAmount = value; }
        }

        public virtual decimal TotalDiscountAmountOriginal
        {
            get { return totalDiscountAmountOriginal; }
            set { totalDiscountAmountOriginal = value; }
        }

        public virtual decimal TotalInwardAmount
        {
            get { return totalInwardAmount; }
            set { totalInwardAmount = value; }
        }

        public virtual decimal TotalInwardAmountOriginal
        {
            get { return totalInwardAmountOriginal; }
            set { totalInwardAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual bool? IsImportPurchase
        {
            get { return isImportPurchase; }
            set { isImportPurchase = value; }
        }

        public virtual bool? IsSpecialConsumeTax
        {
            get { return isSpecialConsumeTax; }
            set { isSpecialConsumeTax = value; }
        }

        public virtual bool? StoredInRepository
        {
            get { return storedInRepository; }
            set { storedInRepository = value; }
        }

        public virtual string InwardNo
        {
            get { return inwardNo; }
            set { inwardNo = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string OriginalNo
        {
            get { return originalNo; }
            set { originalNo = value; }
        }

        public virtual DateTime? MPostedDate
        {
            get { return mPostedDate; }
            set { mPostedDate = value; }
        }

        public virtual DateTime? MDate
        {
            get { return mDate; }
            set { mDate = value; }
        }

        public virtual Guid? MAccountingObjectBankAccountDetailID
        {
            get { return mAccountingObjectBankAccountDetailID; }
            set { mAccountingObjectBankAccountDetailID = value; }
        }

        public virtual string MAccountingObjectBankName
        {
            get { return mAccountingObjectBankName; }
            set { mAccountingObjectBankName = value; }
        }

        public virtual string MContactName
        {
            get { return mContactName; }
            set { mContactName = value; }
        }

        public virtual string MReasonPay
        {
            get { return mReasonPay; }
            set { mReasonPay = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual string CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        public virtual string NumberAttach
        {
            get { return numberAttach; }
            set { numberAttach = value; }
        }

        public virtual string IdentificationNo
        {
            get { return identificationNo; }
            set { identificationNo = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }

        public virtual string IssueBy
        {
            get { return issueBy; }
            set { issueBy = value; }
        }


        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual MCPayment PcMcPayment { get; set; }
        public virtual MBTellerPaper UncMbTellerPaper { get; set; }
        public virtual MBTellerPaper SckMbTellerPaper { get; set; }
        public virtual MBTellerPaper StmMbTellerPaper { get; set; }
        public virtual MBCreditCard TtdMbCreditCard { get; set; }

        private decimal? totalFreightAmount;
        private decimal? totalFreightAmountOriginal;
        public virtual decimal? TotalFreightAmount
        {
            get { return totalFreightAmount; }
            set { totalFreightAmount = value; }
        }

        public virtual decimal? TotalFreightAmountOriginal
        {
            get { return totalFreightAmountOriginal; }
            set { totalFreightAmountOriginal = value; }
        }

        public virtual decimal? TotalAll
        {
            get
            {
                if (IsImportPurchase ?? false) return TotalAmount - TotalDiscountAmount;
                else return TotalAmount - TotalDiscountAmount + TotalVATAmount;
            }
            set { _totalAll = value; }
        }

        public virtual decimal? TotalAllOriginal
        {
            get
            {
                if (IsImportPurchase ?? false) return TotalAmountOriginal - TotalDiscountAmountOriginal;
                else return TotalAmountOriginal - TotalDiscountAmountOriginal + TotalVATAmountOriginal;
            }
            set { _totalAllOriginal = value; }
        }

        public virtual decimal? TotalSpecialConsumeTaxAmountOriginal
        {
            get { return _totalSpecialConsumeTaxAmountOriginal; }
            set { _totalSpecialConsumeTaxAmountOriginal = value; }
        }

        public virtual decimal? TotalSpecialConsumeTaxAmount
        {
            get { return _totalSpecialConsumeTaxAmount; }
            set { _totalSpecialConsumeTaxAmount = value; }
        }

        //public virtual IList<PPService> PPServices
        //{
        //    get { return _ppServices; }
        //    set { _ppServices = value; }
        //}

        private decimal? _totalSpecialConsumeTaxAmountOriginal;
        private decimal? _totalSpecialConsumeTaxAmount;
        private decimal? _totalAll;
        private decimal? _totalAllOriginal;
        //private IList<PPService> _ppServices;
        private decimal? _totalAllNoVat;

        public virtual decimal? TotalAllNoVat
        {
            get { return TotalAmount - TotalDiscountAmount; }
            set { _totalAllNoVat = value; }
        }
        private decimal? _totalAllNoVatOriginal;

        public virtual decimal? TotalAllNoVatOriginal
        {
            get { return TotalAmountOriginal - TotalDiscountAmountOriginal; }
            set { _totalAllNoVatOriginal = value; }
        }
        public virtual decimal? TotalImportTaxExpenseAmountOriginal { get; set; }

        public virtual decimal? TotalImportTaxExpenseAmount { get; set; }

        private decimal? _totalAllValue;

        public virtual decimal? TotalAllValue
        {
            get { return TotalInwardAmount;/*(TotalAmount - TotalDiscountAmount + TotalImportTaxExpenseAmount + TotalFreightAmount + TotalImportTaxAmount + TotalSpecialConsumeTaxAmount)*/; }
            set { _totalAllValue = value; }
        }
        private decimal? _totalAllValueOriginal;

        public virtual decimal? TotalAllValueOriginal
        {
            get { return TotalInwardAmountOriginal;/*(TotalAllValue / ExchangeRate);*/ }
            set { _totalAllValueOriginal = value; }
        }
        public virtual string InvoiceNo
        {
            get
            {
                string s = string.Empty;
                for (int i = 0; i < PPInvoiceDetails.Count; i++)
                {
                    if (string.IsNullOrEmpty(PPInvoiceDetails[i].InvoiceNo) || s.Split(';').ToList().Any(n => n == PPInvoiceDetails[i].InvoiceNo)) continue;
                    if (i == 0)
                    {
                        s += PPInvoiceDetails[i].InvoiceNo;
                        //if (i < PPServiceDetails.Count - 1)
                        //    s += "; ";
                    }
                    else
                    {
                        s += ";" + PPInvoiceDetails[i].InvoiceNo;
                    }
                }
                return s;
            }
        }

        public virtual DateTime? RefDateTime { get; set; }
    }
}
