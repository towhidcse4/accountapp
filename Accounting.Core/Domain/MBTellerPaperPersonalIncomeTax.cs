

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBTellerPaperPersonalIncomeTax
    {
        private Guid iD;
        private Guid mBTellerPaperID;
        private decimal accumAmount;
        private decimal payAmount;
        private int orderPriority;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? employeeID;
        private string employeeName;
        private string description;
        private Guid? departmentID;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MBTellerPaperID
        {
            get { return mBTellerPaperID; }
            set { mBTellerPaperID = value; }
        }

        public virtual decimal AccumAmount
        {
            get { return accumAmount; }
            set { accumAmount = value; }
        }

        public virtual decimal PayAmount
        {
            get { return payAmount; }
            set { payAmount = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual string EmployeeName
        {
            get { return employeeName; }
            set { employeeName = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }




    }
}
