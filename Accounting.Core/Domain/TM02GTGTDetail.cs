using System;

namespace Accounting.Core.Domain
{
    public class TM02GTGTDetail
    {
        private Guid _ID;
        private Guid _TM02GTGTID;
        private string _Code;
        private string _Name;
        private string _Data;
        private int? _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM02GTGTID
        {
            get { return _TM02GTGTID; }
            set { _TM02GTGTID = value; }
        }

        public virtual string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
