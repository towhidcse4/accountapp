using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Role
    {
        private Guid _RoleId;
        private string _RoleName;
        private Boolean _IsLocked;
        private DateTime _CreatedDate;
        private Boolean _IsActive;


        public virtual Guid RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        public virtual string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }

        public virtual Boolean IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }


    }
}
