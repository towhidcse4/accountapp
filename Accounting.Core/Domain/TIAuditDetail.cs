using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIAuditDetail
    {
        private Guid _ID;
        private Guid? _TIAuditID;
        private Guid? _ToolsID;
        private Guid? _DepartmentID;
        private Decimal _QuantityOnBook;
        private Decimal _QuantityInventory;
        private Decimal _DiffQuantity;
        private Decimal _ExecuteQuantity;
        private int _Recommendation;
        private string _Note;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private string _Unit;
        private string _ToolsName;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? TIAuditID
        {
            get { return _TIAuditID; }
            set { _TIAuditID = value; }
        }

        public virtual Guid? ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Decimal QuantityOnBook
        {
            get { return _QuantityOnBook; }
            set { _QuantityOnBook = value; }
        }

        public virtual Decimal QuantityInventory
        {
            get { return _QuantityInventory; }
            set { _QuantityInventory = value; }
        }

        public virtual Decimal DiffQuantity
        {
            get { return _DiffQuantity; }
            set { _DiffQuantity = value; }
        }

        public virtual Decimal ExecuteQuantity
        {
            get { return _ExecuteQuantity; }
            set { _ExecuteQuantity = value; }
        }

        public virtual int Recommendation
        {
            get { return _Recommendation; }
            set { _Recommendation = value; }
        }

        public virtual string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        public virtual string ToolsName
        {
            get { return _ToolsName; }
            set { _ToolsName = value; }
        }
        private string departmentCode;
        public virtual Department Departments { get; set; }
        public virtual string DepartmentCode
        {
            get
            {
                if (Departments != null)
                    return Departments.DepartmentCode;
                else return departmentCode;
            }
            set
            {
                departmentCode = value;
            }
        }
        private string toolCode;
        public virtual TIInit TIInits { get; set; }
        public virtual string ToolCode
        {
            get
            {
                if (TIInits != null)
                    return TIInits.ToolsCode;
                else return toolCode;
            }
            set
            {
                toolCode = value;
            }
        }
        public TIAuditDetail()
        {
            Departments = new Department();
            TIInits = new TIInit();
        }
    }
}
