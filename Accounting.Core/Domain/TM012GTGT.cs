using System;

namespace Accounting.Core.Domain
{
    public class TM012GTGT
    {
        private Guid _ID;
        private Guid _TM01GTGTID;
        private Guid _VoucherID;
        private Guid _VoucherDetailID;
        private DateTime _InvoiceDate;
        private string _InvoiceNo;
        private string _InvoiceSeries;
        private Guid _AccountingObjectID;
        private string _AccountingObjectName;
        private string _TaxCode;
        private Decimal _PretaxAmount;
        private Decimal _TaxAmount;
        private string _Note;
        private int? _OrderPriority;

        public virtual int? STT { get; set; }
        public virtual string GroupName { get; set; }
        public virtual int Type { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM01GTGTID
        {
            get { return _TM01GTGTID; }
            set { _TM01GTGTID = value; }
        }

        public virtual Guid VoucherID
        {
            get { return _VoucherID; }
            set { _VoucherID = value; }
        }

        public virtual Guid VoucherDetailID
        {
            get { return _VoucherDetailID; }
            set { _VoucherDetailID = value; }
        }

        public virtual DateTime InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual Guid AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string TaxCode
        {
            get { return _TaxCode; }
            set { _TaxCode = value; }
        }

        public virtual Decimal PretaxAmount
        {
            get { return _PretaxAmount; }
            set { _PretaxAmount = value; }
        }

        public virtual Decimal TaxAmount
        {
            get { return _TaxAmount; }
            set { _TaxAmount = value; }
        }

        public virtual string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
