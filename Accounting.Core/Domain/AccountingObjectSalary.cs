﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class AccountingObjectSalary
    {
        public Guid ID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public int InMonth { get; set; }
        public int InYear { get; set; }
        public string TaxCode { get; set; }
        public string IdentificationNo { get; set; }
        public decimal? TotalPersonalTaxIncomeAmount { get; set; }
        public decimal? GiamTruGiaCanh { get; set; }
        public decimal? BaoHiemDuocTru { get; set; }
        public decimal? IncomeForTaxCalcuation { get; set; }
        public decimal? IncomeTaxAmount { get; set; }
        public decimal? NetAmount { get; set; }
        public virtual int STT { get; set; }
      
    }
    public class AccountingObjectSalary_Period
    {
        public string Period { get; set; }
    }
}
