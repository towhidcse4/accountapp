using System;

namespace Accounting.Core.Domain
{
    public class TM032ATNDN
    {
        private Guid _ID;
        private Guid _TM03TNDNID;
        private int _Year;
        private Decimal _LostAmount;
        private Decimal _LostAmountTranferPreviousPeriods;
        private Decimal _LostAmountTranferThisPeriod;
        private Decimal _LostAmountRemain;
        private int? _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM03TNDNID
        {
            get { return _TM03TNDNID; }
            set { _TM03TNDNID = value; }
        }

        public virtual int Year
        {
            get { return _Year; }
            set { _Year = value; }
        }

        public virtual Decimal LostAmount
        {
            get { return _LostAmount; }
            set { _LostAmount = value; }
        }

        public virtual Decimal LostAmountTranferPreviousPeriods
        {
            get { return _LostAmountTranferPreviousPeriods; }
            set { _LostAmountTranferPreviousPeriods = value; }
        }

        public virtual Decimal LostAmountTranferThisPeriod
        {
            get { return _LostAmountTranferThisPeriod; }
            set { _LostAmountTranferThisPeriod = value; }
        }

        public virtual Decimal LostAmountRemain
        {
            get { return _LostAmountRemain; }
            set { _LostAmountRemain = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
