﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BalanceAccountF01
    {
        public virtual Guid AccountID { get; set; }
        public virtual bool DetailByAccountObject { get; set; }
        public virtual int AccountObjectType { get; set; }
        public virtual string AccountObjectID { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountNameEnglish { get; set; }
        public virtual int AccountCategoryKind { get; set; }
        public virtual bool IsParent { get; set; }
        public virtual Guid? ParentID { get; set; }
        public virtual int Grade { get; set; }
        public virtual int AccountKind { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual decimal? OpeningDebitAmount { get; set; }
        public virtual decimal? OpeningCreditAmount { get; set; }
        public virtual decimal? DebitAmount { get; set; }
        public virtual decimal? CreditAmount { get; set; }
        public virtual decimal? DebitAmountAccum { get; set; }
        public virtual decimal? CreditAmountAccum { get; set; }
        public virtual decimal? ClosingDebitAmount { get; set; }
        public virtual decimal? ClosingCreditAmount { get; set; }
    }

    public class BalanceAccountF01Detail
    {
        public virtual string Period { get; set; }
    }
}
