using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Repository
    {
        private string repositoryCode;
        private string repositoryName;
        private Guid iD;
        private bool isActive;
        private Guid? branchID;
        private string description;
        private string defaultAccount;
        private string activeName;
        public virtual string ActiveName
        {
            get { return activeName; }
            set { activeName = value; }
        }

        public virtual string RepositoryCode
        {
            get { return repositoryCode; }
            set { repositoryCode = value; }
        }
        public virtual bool Check { get; set; }
        public virtual string RepositoryName
        {
            get { return repositoryName; }
            set { repositoryName = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DefaultAccount
        {
            get { return defaultAccount; }
            set { defaultAccount = value; }
        }
    }
}
