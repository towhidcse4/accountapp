﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TT153PublishInvoice
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime _Date;
        private string _No;
        private string _ReceiptedTaxOffical;
        private string _RepresentationInLaw;
        private int _Status;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private Boolean _IsActive;

        public TT153PublishInvoice()
        {
            TT153PublishInvoiceDetails = new List<TT153PublishInvoiceDetail>();
        }
        public virtual IList<TT153PublishInvoiceDetail> TT153PublishInvoiceDetails { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string ReceiptedTaxOffical
        {
            get { return _ReceiptedTaxOffical; }
            set { _ReceiptedTaxOffical = value; }
        }

        public virtual string RepresentationInLaw
        {
            get { return _RepresentationInLaw; }
            set { _RepresentationInLaw = value; }
        }

        public virtual int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        public virtual String Status_String
        {
            get
            {
                if (_Status == 0) return "Chưa có hiệu lực";
                if (_Status == 1) return "Đã có hiệu lực";
                return null;
            }
        }

    }
}
