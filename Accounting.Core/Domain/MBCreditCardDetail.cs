

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBCreditCardDetail
    {
        private decimal amount;
        private decimal amountOriginal;
        private Guid iD ;
        private Guid mBCreditCardID;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private Guid? budgetItemID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? accountingObjectID;
        private Guid? statisticsCodeID;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private bool? isIrrationalCost;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;
        private Guid? bankAccountDetailID;
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }
        public virtual decimal Amount
        {
            get 
            {
                return amount;
            }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MBCreditCardID
        {
            get { return mBCreditCardID; }
            set { mBCreditCardID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual bool? IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }


        private MBCreditCard _MBCreditCard;
        public virtual MBCreditCard MBCreditCard
        {
            get { return _MBCreditCard; }
            set { _MBCreditCard = value; }
        }
    }
}
