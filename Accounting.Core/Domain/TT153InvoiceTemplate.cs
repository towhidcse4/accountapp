using System;

namespace Accounting.Core.Domain
{
    public class TT153InvoiceTemplate
    {
        private Guid _ID;
        private string _InvoiceTempName;
        private string _InvoiceTempPath;
        private int _InvoiceType;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string InvoiceTempName
        {
            get { return _InvoiceTempName; }
            set { _InvoiceTempName = value; }
        }

        public virtual string InvoiceTempPath
        {
            get { return _InvoiceTempPath; }
            set { _InvoiceTempPath = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }


    }
}
