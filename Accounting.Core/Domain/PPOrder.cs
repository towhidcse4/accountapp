

using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPOrder
    {
        private int typeID;
        private DateTime date;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private decimal totalDiscountAmount;
        private decimal totalDiscountAmountOriginal;
        private Guid iD;
        private bool exported;
        private Guid? branchID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string companyTaxCode;
        private string contactName;
        private string reason;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? paymentClauseID;
        private DateTime? dueDate;
        private Guid? transpotMethodID;
        private DateTime? deliverDate;
        private string shippingPlace;
        private Guid? employeeID;
        private Guid? templateID;
        private decimal totalPaymentAmount;
        private decimal totalPaymentAmountOriginal;

        PaymentClause paymentClause;

        private string paymentClauseName;

        public virtual PaymentClause PaymentClause
        {
            get { return paymentClause; }
            set { paymentClause = value; }
        }

        public virtual string PaymentClauseName
        {
            get
            {
                try
                {
                    return PaymentClause.PaymentClauseName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
           
        }
        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (PPOrderDetails.Count > 0)
                {
                    max = PPOrderDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        public virtual decimal? MaxDiscountRate
        {
            get
            {
                decimal? max = null;
                if (PPOrderDetails.Count > 0)
                {
                    max = PPOrderDetails.Max(k => k.DiscountRate);
                }
                return max;
            }
        }
        private Type _type;
        public virtual IList<PPOrderDetail> PPOrderDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public PPOrder()
        {
            CurrencyID = "VND";
            RefVouchers = new List<RefVoucher>();
            PPOrderDetails = new List<PPOrderDetail>();
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual decimal TotalDiscountAmount
        {
            get { return totalDiscountAmount; }
            set { totalDiscountAmount = value; }
        }

        public virtual decimal TotalDiscountAmountOriginal
        {
            get { return totalDiscountAmountOriginal; }
            set { totalDiscountAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual Guid? TranspotMethodID
        {
            get { return transpotMethodID; }
            set { transpotMethodID = value; }
        }

        public virtual DateTime? DeliverDate
        {
            get { return deliverDate; }
            set { deliverDate = value; }
        }

        public virtual string ShippingPlace
        {
            get { return shippingPlace; }
            set { shippingPlace = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual decimal TotalPaymentAmount
        {
            get { return TotalAmount + TotalVATAmount - TotalDiscountAmount; }
            set { totalPaymentAmount = value; }
        }

        public virtual decimal TotalPaymentAmountOriginal
        {
            get { return TotalAmountOriginal + TotalVATAmountOriginal - TotalDiscountAmountOriginal; }
            set { totalPaymentAmountOriginal = value; }
        }
    }
}
