using System;

namespace Accounting.Core.Domain
{
    public class ToolLedger
    {
        private Guid _ID;
        private Guid? _BranchID;
        private string _No;
        private int _TypeID;
        private DateTime _Date;
        private DateTime _PostedDate;
        private Guid? _ToolsID;
        private string _Reason;
        private string _Description;
        private Decimal? _IncrementAllocationTime;
        private Decimal? _DecrementAllocationTime;
        private Decimal? _IncrementQuantity;
        private Decimal? _DecrementQuantity;
        private Decimal? _IncrementAmount;
        private Decimal? _DecrementAmount;
        private Decimal? _AllocationAmount;
        private Decimal? _AllocatedAmount;
        private Decimal? _UnitPrice;
        private string _OrderPriority;
        private Guid _ReferenceID;
        private Guid? _DepartmentID;
        public virtual decimal RemainingQuantity { get; set; }
        public virtual decimal RemainingAmount { get; set; }
        public virtual decimal RemainingAllocaitonTimes { get; set; }
        public virtual string Nos { get; set; }
        public virtual DateTime? IncrementDate { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual Guid ReferenceID
        {
            get { return _ReferenceID; }
            set { _ReferenceID = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual Guid? ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Decimal? IncrementAllocationTime
        {
            get { return _IncrementAllocationTime; }
            set { _IncrementAllocationTime = value; }
        }

        public virtual Decimal? DecrementAllocationTime
        {
            get { return _DecrementAllocationTime; }
            set { _DecrementAllocationTime = value; }
        }

        public virtual Decimal? UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal? IncrementQuantity
        {
            get { return _IncrementQuantity; }
            set { _IncrementQuantity = value; }
        }

        public virtual Decimal? DecrementQuantity
        {
            get { return _DecrementQuantity; }
            set { _DecrementQuantity = value; }
        }

        public virtual Decimal? IncrementAmount
        {
            get { return _IncrementAmount; }
            set { _IncrementAmount = value; }
        }

        public virtual Decimal? DecrementAmount
        {
            get { return _DecrementAmount; }
            set { _DecrementAmount = value; }
        }

        public virtual Decimal? AllocationAmount
        {
            get { return _AllocationAmount; }
            set { _AllocationAmount = value; }
        }

        public virtual Decimal? AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual string OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
