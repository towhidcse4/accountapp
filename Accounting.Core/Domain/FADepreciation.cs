﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADepreciation
    {
        private Guid id;
        private Guid branchID;
        private int typeID;
        private DateTime postedDate;
        private DateTime date;
        private string no;
        private string reason;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? templateID;
        private bool recorded;
        private bool exported;
        public virtual int Month { get; set; }
        public virtual int Year { get; set; }
        public FADepreciation()
        {
            FADepreciationDetails = new List<FADepreciationDetail>();
            FADepreciationAllocations = new List<FADepreciationAllocation>();
            FADepreciationPosts = new List<FADepreciationPost>();

            RefVouchers = new List<RefVoucher>();

        }
        public virtual IList<FADepreciationDetail> FADepreciationDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public virtual IList<FADepreciationAllocation> FADepreciationAllocations { get; set; }
        public virtual IList<FADepreciationPost> FADepreciationPosts { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }
        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }
        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }
        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }
        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }
        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }
        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }
        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual Guid BranchID
        {
            get { return branchID; }
            set { branchID = value; }

        }
        public virtual Guid ID
        {
            get { return id; }
            set { id = value; }
        }
    }
}
