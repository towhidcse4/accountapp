﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class VoucherPatternsReport
    {
        private int _ID;
        private int _TypeID;
        private int _TypeGroup;
        private string _VoucherPatternsCode;
        private string _VoucherPatternsName;
        private string _FilePath;
        private string _ListTypeID;
        private Boolean _IsStartup;

       


        public virtual int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }
        public virtual int TypeGroup
        {
            get { return _TypeGroup; }
            set { _TypeGroup = value; }
        }
        public virtual string VoucherPatternsCode
        {
            get { return _VoucherPatternsCode; }
            set { _VoucherPatternsCode = value; }
        }

        public virtual string VoucherPatternsName
        {
            get { return _VoucherPatternsName; }
            set { _VoucherPatternsName = value; }
        }

        public virtual string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
        public virtual string ListTypeID
        {
            get { return _ListTypeID; }
            set { _ListTypeID = value; }
        }

        public virtual Boolean IsStartup
        {
            get { return _IsStartup; }
            set { _IsStartup = value; }
        }


    }
}
