﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCAuditDetailMember
    {
        public virtual Guid ID { get; set; }
        public virtual Guid MCAuditID { get; set; }
        public virtual string AccountingObjectTitle { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual int? OrderPriority { get; set; }
    }
}
