using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FixedAssetCategory
    {
        private Guid iD;
        private string fixedAssetCategoryCode;
        private string fixedAssetCategoryName;
        private int grade;
        private bool isActive;
        private decimal? usedTime;
        private decimal? depreciationRate;
        private string originalPriceAccount;
        private string depreciationAccount;
        private string expenditureAccount;
        private string description;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;

        

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string FixedAssetCategoryCode
        {
            get { return fixedAssetCategoryCode; }
            set { fixedAssetCategoryCode = value; }
        }

        public virtual string FixedAssetCategoryName
        {
            get { return fixedAssetCategoryName; }
            set { fixedAssetCategoryName = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual decimal? UsedTime
        {
            get { return usedTime; }
            set { usedTime = value; }
        }

        public virtual decimal? DepreciationRate
        {
            get { return depreciationRate; }
            set { depreciationRate = value; }
        }

        public virtual string OriginalPriceAccount
        {
            get { return originalPriceAccount; }
            set { originalPriceAccount = value; }
        }

        public virtual string DepreciationAccount
        {
            get { return depreciationAccount; }
            set { depreciationAccount = value; }
        }

        public virtual string ExpenditureAccount
        {
            get { return expenditureAccount; }
            set { expenditureAccount = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }


    }
}
