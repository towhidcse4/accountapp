

using System;
using System.Collections.Generic;
using FX.Core;
using Accounting.Core.IService;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBTellerPaper
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid? branchID;
        private Guid? employeeID;
        private bool isMatch;
        private DateTime? matchDate;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? templateID;
        private string bankName;
        private string reason;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private Guid? accountingObjectBankAccount;
        private string accountingObjectBankName;
        private string receiver;
        private string identificationNo;
        private DateTime? issueDate;
        private string issueBy;
        private string currencyID ;
        private decimal? exchangeRate = 1;
        private Guid? pPOrderID;
        private bool? isImportPurchase;
        private Guid? paymentClauseID;
        private Guid? transportMethodID;
        private int accountingObjectType;
        private decimal totalAll;
        private decimal totalAllOriginal;

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }
        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return exchangeRate.HasValue ? TotalAmountOriginal * ExchangeRate.Value : TotalAmountOriginal; }
            set { totalAmount = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return IoC.Resolve<ITypeService>().Getbykey(typeID).TypeName;
                }
                catch
                {
                    return "";
                }
            }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }


        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual Guid? AccountingObjectBankAccount
        {
            get { return accountingObjectBankAccount; }
            set { accountingObjectBankAccount = value; }
        }

        public virtual string AccountingObjectBankName
        {
            get { return accountingObjectBankName; }
            set { accountingObjectBankName = value; }
        }

        public virtual string Receiver
        {
            get { return receiver; }
            set { receiver = value; }
        }

        public virtual string IdentificationNo
        {
            get { return identificationNo; }
            set { identificationNo = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }

        public virtual string IssueBy
        {
            get { return issueBy; }
            set { issueBy = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? PPOrderID
        {
            get { return pPOrderID; }
            set { pPOrderID = value; }
        }

        public virtual bool? IsImportPurchase
        {
            get { return isImportPurchase; }
            set { isImportPurchase = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }
        public virtual int AccountingObjectType
        {
            get { return accountingObjectType; }
            set { accountingObjectType = value; }
        }

        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual decimal TotalAll
        {
            get
            {
                if (typeID == 120 || typeID == 130 || typeID == 140)
                    return TotalAmount;
                else if (typeID == 127 || typeID == 141 || typeID == 131 || typeID == 133 || typeID == 143 || typeID == 126 ||
                    typeID == 903 || typeID == 904 || typeID == 905 || typeID == 129 || typeID == 132 || typeID == 142) return totalAll;
                else return TotalAllOriginal * ExchangeRate ?? 1;
            }
            set { totalAll = value; }
        }
        public virtual decimal TotalAllOriginal
        {
            get
            {
                if (typeID == 120 || typeID == 130 || typeID == 140)
                    return TotalAmountOriginal;
                else return totalAllOriginal;
            }
            set { totalAllOriginal = value; }
        }

        public virtual IList<MBTellerPaperDetail> MBTellerPaperDetails { get; set; }
        public virtual IList<MBTellerPaperDetailTax> MBTellerPaperDetailTaxs { get; set; }
        public virtual IList<MBTellerPaperDetailVendor> MBTellerPaperDetailVendors { get; set; }
        public virtual IList<MBTellerPaperDetailImportVAT> MBTellerPaperDetailImportVATs { get; set; }
        public virtual IList<MBTellerPaperDetailInsurance> MBTellerPaperDetailInsurances { get; set; }
        public virtual IList<MBTellerPaperPersonalIncomeTax> MBTellerPaperPersonalIncomeTaxs { get; set; }
        public virtual IList<MBTellerPaperDetailSalary> MBTellerPaperDetailSalarys { get; set; }
        public virtual IList<RefVoucherRSInwardOutward> RefVoucherRSInwardOutwards { get; set; }

        public MBTellerPaper()
        {
            MBTellerPaperDetails = new List<MBTellerPaperDetail>();
            MBTellerPaperDetailTaxs = new List<MBTellerPaperDetailTax>();
            MBTellerPaperDetailVendors = new List<MBTellerPaperDetailVendor>();
            MBTellerPaperDetailImportVATs = new List<MBTellerPaperDetailImportVAT>();
            MBTellerPaperDetailInsurances = new List<MBTellerPaperDetailInsurance>();
            MBTellerPaperPersonalIncomeTaxs = new List<MBTellerPaperPersonalIncomeTax>();
            MBTellerPaperDetailSalarys = new List<MBTellerPaperDetailSalary>();
            RefVoucherRSInwardOutwards = new List<RefVoucherRSInwardOutward>();
        }
    }
}
