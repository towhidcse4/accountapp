﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class FU_GetCashDetailBook
    {
        //public int? RowNum { get; set; }
        public Guid RefID { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? PostedDate { get; set; }
        public DateTime? RefDate { get; set; }
        public string ReceiptRefNo { get; set; }
        public string PaymentRefNo { get; set; }
        public int RefType { get; set; }
        public string JournalMemo { get; set; }
        public string CorrespondingAccountNumber { get; set; }
        //public decimal? ExchangeRate { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? DebitAmountOC { get; set; }
        public decimal? CreditAmount { get; set; }
        public decimal? CreditAmountOC { get; set; }
        public decimal? ClosingAmount { get; set; }
        public decimal? ClosingAmountOC { get; set; }
        public string ContactName { get; set; }
        //public string AccountObjectId { get; set; }
        //public string AccountObjectCode { get; set; }
        //public string AccountObjectName { get; set; }
        //public string EmployeeCode { get; set; }
        //public string EmployeeName { get; set; }
        //public string InvoiceOrder { get; set; }
        public Guid BranchID { get; set; }
        //public int? BranchName { get; set; }
        //public bool IsBold { get; set; }


    }
    public class FU_GetCashDetailBookDetail
    {
        public string Period { get; set; }
        public decimal? SumTonDK { get; set; }
        public decimal? SumTonDKQC { get; set; }//add by cuongpv
    }
}
