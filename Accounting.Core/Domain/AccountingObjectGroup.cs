using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountingObjectGroup
    {
        private Guid _ID;
        private string _AccountingObjectGroupCode;
        private string _AccountingObjectGroupName;
        private Guid? _ParentID;
        private Boolean _IsParentNode;
        private string _OrderFixCode;
        private int _Grade;
        private string _Description;
        private Boolean _IsActive;
        private Boolean _IsSecurity;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string AccountingObjectGroupCode
        {
            get { return _AccountingObjectGroupCode; }
            set { _AccountingObjectGroupCode = value; }
        }

        public virtual string AccountingObjectGroupName
        {
            get { return _AccountingObjectGroupName; }
            set { _AccountingObjectGroupName = value; }
        }

        public virtual Guid? ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }

        public virtual Boolean IsParentNode
        {
            get { return _IsParentNode; }
            set { _IsParentNode = value; }
        }

        public virtual string OrderFixCode
        {
            get { return _OrderFixCode; }
            set { _OrderFixCode = value; }
        }

        public virtual int Grade
        {
            get { return _Grade; }
            set { _Grade = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public virtual Boolean IsSecurity
        {
            get { return _IsSecurity; }
            set { _IsSecurity = value; }
        }
    }
}
