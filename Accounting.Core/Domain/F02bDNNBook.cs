﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class F02bDNNBook
    {
        public string SoHieu { get; set; }
        public DateTime NgayChungTu { get; set; }
        public decimal SoTien { get; set; }
        public string Quy { get; set; }
        public int Thang { get; set; }
        public int Nam { get; set; }
    }
    public class F02bDNNBook_Period
    {
        public string Period { get; set; }

    }
}
