using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailExcept
    {       
        public virtual Guid ID { get; set; }
        public virtual Guid GOtherVoucherID { get; set; }
        public virtual Guid PayRefID { get; set; }
        public virtual DateTime? PayRefDate { get; set; }
        public virtual string PayRefNo { get; set; }
        public virtual decimal? PayAmountOriginal { get; set; }
        public virtual decimal? PayAmount { get; set; }
        public virtual Guid DebtRefID { get; set; }
        public virtual DateTime? DebtRefDate { get; set; }
        public virtual string DebtRefNo { get; set; }
        public virtual decimal? DebtAmountOriginal { get; set; }
        public virtual decimal? DebtAmount { get; set; }
        public virtual decimal DiffAmount { get; set; }
    }
}
