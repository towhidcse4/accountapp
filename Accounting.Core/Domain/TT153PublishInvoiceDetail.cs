using System;

namespace Accounting.Core.Domain
{
    public class TT153PublishInvoiceDetail
    {
        private Guid _ID;
        private Guid _TT153PublishInvoiceID;
        private Guid _TT153ReportID;
        private Guid _TT153RegisterInvoiceDetailID;
        private int _InvoiceForm;
        private int _InvoiceType;
        private Guid _InvoiceTypeID;
        private string _InvoiceSeries;
        private string _FromNo;
        private string _ToNo;
        private Decimal _Quantity;
        private DateTime _StartUsing;
        private string _CompanyName;
        private string _CompanyTaxCode;
        private string _ContractNo;
        private DateTime? _ContractDate;
        private int _OrderPriority;
        //private bool _Status;
       // private string _InvoiceTemplate;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TT153PublishInvoiceID
        {
            get { return _TT153PublishInvoiceID; }
            set { _TT153PublishInvoiceID = value; }
        }

        public virtual Guid TT153ReportID
        {
            get { return _TT153ReportID; }
            set { _TT153ReportID = value; }
        }

        public virtual Guid TT153RegisterInvoiceDetailID
        {
            get { return _TT153RegisterInvoiceDetailID; }
            set { _TT153RegisterInvoiceDetailID = value; }
        }

        public virtual int InvoiceForm
        {
            get { return _InvoiceForm; }
            set { _InvoiceForm = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }

        public virtual Guid InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual string FromNo
        {
            get { return _FromNo; }
            set { _FromNo = value; }
        }

        public virtual string ToNo
        {
            get { return _ToNo; }
            set { _ToNo = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual DateTime StartUsing
        {
            get { return _StartUsing; }
            set { _StartUsing = value; }
        }

        public virtual string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return _CompanyTaxCode; }
            set { _CompanyTaxCode = value; }
        }

        public virtual string ContractNo
        {
            get { return _ContractNo; }
            set { _ContractNo = value; }
        }

        public virtual DateTime? ContractDate
        {
            get { return _ContractDate; }
            set { _ContractDate = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }
        //public bool Status
        //{
        //    get { return _Status; }
        //    set { _Status = value; }
        //}
        //public string InvoiceTemplate
        //{
        //    get { return _InvoiceTemplate; }
        //    set { _InvoiceTemplate = value; }
        //}
        public virtual TT153Report tT153Report { get; set; }
        public virtual string ReportName
        {
            get
            {
                if (tT153Report == null) return "";
                try
                {
                    return tT153Report.ReportName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
        public virtual string InvoiceTemplate
        {
            get
            {
                if (tT153Report == null) return "";
                try
                {
                    return tT153Report.InvoiceTemplate;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
    }
}
