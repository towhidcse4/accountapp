﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ViewVouchersCloseBook
    {
        public virtual System.Guid ID { get; set; }
        public virtual System.Guid DetailID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeName { get; set; }
        public virtual string No { get; set; }
        public virtual string InwardNo { get; set; }
        public virtual string OutwardNo { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? OrgPrice { get; set; }
        public virtual string Reason { get; set; }
        public virtual string Description { get; set; }
        public virtual System.Nullable<System.Guid> CostSetID { get; set; }
        public virtual string CostSetCode { get; set; }
        public virtual System.Nullable<System.Guid> ContractID { get; set; }
        public virtual string ContractCode { get; set; }
        public virtual System.Nullable<System.Guid> AccountingObjectID { get; set; }
        public virtual string AccountingObjectCategoryName { get; set; }
        public virtual string AccountingObjectCode { get; set; }
        public virtual System.Nullable<System.Guid> EmployeeID { get; set; }
        public virtual System.Nullable<System.Guid> BranchID { get; set; }
        public virtual System.Nullable<System.Guid> FixedAssetID { get; set; }
        public virtual string FixedAssetName { get; set; }
        public virtual System.Nullable<System.Guid> MaterialGoodsID { get; set; }
        public virtual string MaterialGoodsName { get; set; }
        public virtual System.Nullable<System.Guid> RepositoryID { get; set; }
        public virtual string RepositoryName { get; set; }
        public virtual System.Nullable<System.Guid> DepartmentID { get; set; }
        public virtual string DepartmentName { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual bool? Recorded { get; set; }
        public virtual decimal? TotalAmount { get; set; }
        public virtual decimal? TotalAmountOriginal { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual decimal? FreightAmountOriginal { get; set; }
        public virtual decimal? FreightAmount { get; set; }
        public virtual int DetailTax { get; set; }
        public virtual decimal? InwardAmountOriginal { get; set; }
        public virtual decimal? InwardAmount { get; set; }
        public virtual bool? StoredInRepository { get; set; }
        public virtual decimal? OutwardAmountOriginal { get; set; }
        public virtual decimal? OutwardAmount { get; set; }
        public virtual string RefTable { get; set; }
    }

    public class VoucherUnSaveLedger
    {
        public virtual System.Guid ID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeName { get; set; }
        public virtual string No { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual decimal? TotalAmount { get; set; }
        public virtual decimal? TotalAmountOriginal { get; set; }
        public virtual int Method { get; set; }
        public virtual string Cause { get; set; }
        public virtual string RefTable { get; set; }
    }
}
