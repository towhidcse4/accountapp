using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAInit
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int? TypeID { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual Guid? FixedAssetID { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual DateTime? IncrementDate { get; set; }
        public virtual DateTime? DepreciationDate { get; set; }
        public virtual Decimal? UsedTime { get; set; }
        public virtual Decimal? UsedTimeRemain { get; set; }
        public virtual Decimal? OriginalPrice { get; set; }
        public virtual Decimal? PurchasePrice { get; set; }
        public virtual Decimal? AcDepreciationAmount { get; set; }
        public virtual Decimal? RemainingAmount { get; set; }
        public virtual Decimal? MonthPeriodDepreciationAmount { get; set; }
        public virtual string OriginalPriceAccount { get; set; }
        public virtual string ExpenditureAccount { get; set; }
        public virtual string DepreciationAccount { get; set; }
        public virtual string FixedAssetCode { get; set; }
        public virtual string FixedAssetName { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? FixedAssetCategoryID { get; set; }
        public virtual bool IsMonthUsedTime { get; set; }
        public virtual bool IsMonthUsedTimeRemain { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual FixedAssetCategory FixedAssetCategory { get; set; }
        public virtual string FixedAssetCategoryName
        {
            get { return FixedAssetCategory != null ? FixedAssetCategory.FixedAssetCategoryName : ""; }
        }
    }
}
