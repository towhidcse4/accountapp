using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountGroup
    {
        private string iD = string.Empty;
        private string accountGroupName = string.Empty;
        private int accountGroupKind = 0;
        private string accountGroupKindView = string.Empty;
        private string detailType = string.Empty;

        public AccountGroup()
        {
            Accounts = new List<Account>();
        }
        public virtual IList<Account> Accounts { get; set; }

        public virtual string ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string AccountGroupName
        {
            get { return accountGroupName; }
            set { accountGroupName = value; }
        }

        public virtual int AccountGroupKind
        {
            get { return accountGroupKind; }
            set { accountGroupKind = value; }
        }

        public virtual string AccountGroupKindView
        {
            get { return accountGroupKindView; }
            set { accountGroupKindView = value; }
        }

        public virtual string DetailType
        {
            get { return detailType; }
            set { detailType = value; }
        }
    }
}