using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PersonalSalaryTax
    {
        private Guid _ID;
        private string _PersonalSalaryTaxName;
        private int _PersonalSalaryTaxGrade;
        private int _SalaryType;
        string _SalaryTypeView;
        private Decimal _TaxRate;
        private Decimal _FromAmount;
        private Decimal _ToAmount;
        private string _Formula;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string PersonalSalaryTaxName
        {
            get { return _PersonalSalaryTaxName; }
            set { _PersonalSalaryTaxName = value; }
        }

        public virtual int PersonalSalaryTaxGrade
        {
            get { return _PersonalSalaryTaxGrade; }
            set { _PersonalSalaryTaxGrade = value; }
        }

        public virtual int SalaryType
        {
            get { return _SalaryType; }
            set { _SalaryType = value; }
        }
        public virtual string SalaryTypeView
        {
            get { return _SalaryTypeView; }
            set { _SalaryTypeView = value; }
        }

        public virtual Decimal TaxRate
        {
            get { return _TaxRate; }
            set { _TaxRate = value; }
        }

        public virtual Decimal FromAmount
        {
            get { return _FromAmount; }
            set { _FromAmount = value; }
        }

        public virtual Decimal ToAmount
        {
            get { return _ToAmount; }
            set { _ToAmount = value; }
        }

        public virtual string Formula
        {
            get { return _Formula; }
            set { _Formula = value; }
        }

    }
}
