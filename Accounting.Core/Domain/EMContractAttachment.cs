﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMContractAttachment
    {
        private Guid iD;
        private Guid eMContractID;
        private string fileName;
        private int? fileSize;
        private string fileExtension;
        private string fileType;
        private byte[] fileAttachment;
        private string fileLink;
        private string description;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid EMContractID
        {
            get { return eMContractID; }
            set { eMContractID = value; }
        }

        public virtual string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public virtual int? FileSize
        {
            get { return fileSize; }
            set { fileSize = value; }
        }

        public virtual string FileExtension
        {
            get { return fileExtension; }
            set { fileExtension = value; }
        }

        public virtual string FileType
        {
            get { return fileType; }
            set { fileType = value; }
        }

        public virtual byte[] FileAttachment
        {
            get { return fileAttachment; }
            set { fileAttachment = value; }
        }

        public virtual string FileLink
        {
            get { return fileLink; }
            set { fileLink = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
}
