﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMTransfer
    {
        private Guid iD;
        private string transferCode;
        private DateTime transferDate;
        private Guid? fromEMShareHolderID;
        private string fromEMShareHolderName;
        private Guid? toEMShareHolderID;
        private string toEMShareHolderName;
        private Decimal quantity;
        private Decimal amount;
        private string description;
        private int transferOrder;
        private int toEMShareHolderType;
        private Decimal transferFee;
        private int typeID;
        private bool recorded;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual string TransferCode
        {
            get { return transferCode; }
            set { transferCode = value; }
        }
        public virtual DateTime TransferDate
        {
            get { return transferDate; }
            set { transferDate = value; }
        }
        public virtual Guid? FromEMShareHolderID
        {
            get { return fromEMShareHolderID; }
            set { fromEMShareHolderID = value; }
        }
        public virtual string FromEMShareHolderName
        {
            get { return fromEMShareHolderName; }
            set { fromEMShareHolderName = value; }
        }
        public virtual Guid? ToEMShareHolderID
        {
            get { return toEMShareHolderID; }
            set { toEMShareHolderID = value; }
        }
        public virtual string ToEMShareHolderName
        {
            get { return toEMShareHolderName; }
            set { toEMShareHolderName = value; }
        }
        public virtual Decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        public virtual Decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual int TransferOrder
        {
            get { return transferOrder; }
            set { transferOrder = value; }
        }
        public virtual int ToEMShareHolderType
        {
            get { return toEMShareHolderType; }
            set { toEMShareHolderType = value; }
        }
        public virtual Decimal TransferFee
        {
            get { return transferFee; }
            set { transferFee = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public EMTransfer()
        {
            EMTransferDetails = new List<EMTransferDetail>();
        }
        public virtual IList<EMTransferDetail> EMTransferDetails { get; set; }
    }
}
