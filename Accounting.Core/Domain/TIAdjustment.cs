using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public partial class TIAdjustment {
        public virtual System.Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual IList<TIAdjustmentDetail> TIAdjustmentDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public TIAdjustment()
        {
            RefVouchers = new List<RefVoucher>();
        }
    }
}
