﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class InterestRateDetail   //chi tiết tính lãi ([Bán hàng] -> [Tính lãi])
    {
        public virtual string Stt { get; set; }                 //số tự động tăng
        public virtual string SoChungTu { get; set; }               //lấy trên chứng từ
        public virtual DateTime NgayChungTu { get; set; }           //lấy trên chứng từ
        public virtual DateTime NgayHoachToan { get; set; }         //lấy trên chứng từ
        public virtual string SoTienTrenChungTu { get; set; }       //lấy trên chứng từ
        public virtual string SoTienNoQuaHan { get; set; }      //tự tính
        public virtual int SoNgayNoQuaHan { get; set; }         //tự tính dựa trên ngày đang xét
        public virtual string PhanTramTinhLai { get; set; }     //lấy từ (điều khoản thanh toán)
        public virtual string TienLai { get; set; }             //tự tính
    }
}