using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Investor
    {
        private Guid iD;
        private string investorCode;
        private string investorName;
        private Guid? investorGroupID;
        private string investorCategoryIDVIEW;
        private int investorType;
        
        private string identicationNumber;
        private DateTime? contactIssueDate;
        private string contactIssueBy;
        private string address;
        private string tel;
        private string fax;
        private string email;
        private string website;
        private string businessRegistrationNumber;
        private DateTime? issueDate;
        private string issueBy;
        private string bankAccount;
        private string bankName;
        private string taxCode;
        private string contactName;
        private string contactTitle;
        private string contactPrefix;
        private string contactMobile;
        private string contactEmail;
        private string contactOfficeTel;
        private string contactAddress;
        private bool isActive;
        //private bool isParentNode;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string InvestorCode
        {
            get { return investorCode; }
            set { investorCode = value; }
        }

        public virtual string InvestorName
        {
            get { return investorName; }
            set { investorName = value; }
        }

        public virtual Guid? InvestorGroupID
        {
            get { return investorGroupID; }
            set { investorGroupID = value; }
        }

        public virtual string InvestorCategoryIDVIEW
        {
            get { return investorCategoryIDVIEW; }
            set { investorCategoryIDVIEW = value; }
        }

        public virtual int InvestorType
        {
            get { return investorType; }
            set { investorType = value; }
        }

        

        public virtual string IdenticationNumber
        {
            get { return identicationNumber; }
            set { identicationNumber = value; }
        }

        public virtual DateTime? ContactIssueDate
        {
            get { return contactIssueDate; }
            set { contactIssueDate = value; }
        }

        public virtual string ContactIssueBy
        {
            get { return contactIssueBy; }
            set { contactIssueBy = value; }
        }

        public virtual string Address
        {
            get { return address; }
            set { address = value; }
        }

        public virtual string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public virtual string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }

        public virtual string Website
        {
            get { return website; }
            set { website = value; }
        }

        public virtual string BusinessRegistrationNumber
        {
            get { return businessRegistrationNumber; }
            set { businessRegistrationNumber = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }

        public virtual string IssueBy
        {
            get { return issueBy; }
            set { issueBy = value; }
        }

        public virtual string BankAccount
        {
            get { return bankAccount; }
            set { bankAccount = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual string TaxCode
        {
            get { return taxCode; }
            set { taxCode = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual string ContactTitle
        {
            get { return contactTitle; }
            set { contactTitle = value; }
        }

        public virtual string ContactPrefix
        {
            get { return contactPrefix; }
            set { contactPrefix = value; }
        }

        public virtual string ContactMobile
        {
            get { return contactMobile; }
            set { contactMobile = value; }
        }

        public virtual string ContactEmail
        {
            get { return contactEmail; }
            set { contactEmail = value; }
        }

        public virtual string ContactOfficeTel
        {
            get { return contactOfficeTel; }
            set { contactOfficeTel = value; }
        }

        public virtual string ContactAddress
        {
            get { return contactAddress; }
            set { contactAddress = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        //public virtual bool IsParentNode
        //{
        //    get { return isParentNode; }
        //    set { isParentNode = value; }
        //}


    }
}
