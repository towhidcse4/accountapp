using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Permission
    {
        private Guid _PermissionId;
        private string _PermissionName;
        private string _Description;
        private Boolean _IsLocked;
        private DateTime _CreatedDate;
        private Boolean _IsActive;


        public virtual Guid PermissionId
        {
            get { return _PermissionId; }
            set { _PermissionId = value; }
        }

        public virtual string PermissionName
        {
            get { return _PermissionName; }
            set { _PermissionName = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Boolean IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }


    }
}
