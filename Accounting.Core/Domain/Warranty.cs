using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Warranty
    {
        private int _ID;
        private int _WarrantyTime;
        private string _WarrantyName;
        private string _Description;
        private Boolean _IsActive;


        public virtual int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int WarrantyTime
        {
            get { return _WarrantyTime; }
            set { _WarrantyTime = value; }
        }

        public virtual string WarrantyName
        {
            get { return _WarrantyName; }
            set { _WarrantyName = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }


    }
}
