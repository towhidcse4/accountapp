using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CashFlowStatementReport
    {
        public virtual Guid ID { get; set; }
        public virtual Guid GeneralLedgerID { get; set; }
        public virtual int CategoriesActive { get; set; }
    }
}
