using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIInitDetail
    {
        private Guid _ID;
        private Guid? _TIInitID;
        private Guid? _ObjectID;
        private int _ObjectType;
        private Decimal? _Quantity;
        private Decimal? _Rate;
        private string _CostAccount;
        private Guid? _ExpenseItemID;
        private int _OrderPriority;
        private Guid? _DepartmentID;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? TIInitID
        {
            get { return _TIInitID; }
            set { _TIInitID = value; }
        }

        public virtual Guid? ObjectID
        {
            get { return _ObjectID; }
            set { _ObjectID = value; }
        }

        public virtual int ObjectType
        {
            get { return _ObjectType; }
            set { _ObjectType = value; }
        }

        public virtual Decimal? Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal? Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }

        public virtual string CostAccount
        {
            get { return _CostAccount; }
            set { _CostAccount = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }


    }
}
