using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class BankAccountDetail
    {
        private Guid iD;
        private Guid bankID;
        
        private string bankAccount;
        private string bankIDView;
        private string bankName;
        private bool isActive;
        private string address;
        private string description;
        private string bankBranchName;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid BankID
        {
            get { return bankID; }
            set { bankID = value; }
        }

        

        public virtual string BankAccount
        {
            get { return bankAccount; }
            set { bankAccount = value; }
        }
        public virtual string BankIDView
        {
            get { return bankIDView; }
            set { bankIDView = value; }
        }
        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual string Address
        {
            get { return address; }
            set { address = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string BankBranchName
        {
            get { return bankBranchName; }
            set { bankBranchName = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
    }
}
