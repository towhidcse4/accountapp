﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADepreciationAllocation
    {
        private Guid _ID;
        private Guid? _BranchID;
        private Guid? _FADepreciationID;
        private Guid? _FixedAssetID;
        private string _Description;
        private Guid? objectID;
        private string _AccountNumber;
        private Decimal? _Rate;
        private Decimal? _Amount;
        private Guid? _CostSetID;
        private Guid? _ExpenseItemID;
        private Guid? _ContractID;
        private Boolean? _IsIrrationalCost;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual Guid? FADepreciationID
        {
            get { return _FADepreciationID; }
            set { _FADepreciationID = value; }
        }

        public virtual Guid? FixedAssetID
        {
            get { return _FixedAssetID; }
            set { _FixedAssetID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Guid? ObjectID
        {
            get { return objectID; }
            set { objectID = value; }
        }

        public virtual string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }

        public virtual Decimal? Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }

        public virtual Decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Boolean? IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}

