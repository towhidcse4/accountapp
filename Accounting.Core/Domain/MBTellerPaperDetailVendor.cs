

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBTellerPaperDetailVendor
    {
        private decimal payableAmount;
        private decimal payableAmountOriginal;
        private decimal remainingAmount;
        private decimal remainingAmountOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private decimal discountAmount;
        private decimal discountAmountOriginal;
        private Guid iD;
        private Guid mBTellerPaperID;
        private string debitAccount;
        private DateTime? voucherDate;
        private DateTime? voucherPostedDate;
        private string voucherNo;
        private int? voucherTypeID;
        private DateTime? dueDate;
        private Guid? pPInvoiceID;
        private decimal? discountRate;
        private string discountAccount;
        private Guid? genID;
        private Guid? accountingObjectID;
        private string invoiceNo;
        private DateTime? invoiceDate;
        private Guid? employeeID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;
        private Guid? deptEmployeeID;
        private int? paymentType;

        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal RefVoucherExchangeRate { get; set; }
        public virtual decimal LastExchangeRate { get; set; }
        public virtual decimal DifferAmount { get; set; }
        public virtual decimal PayableAmount
        {
            get { return payableAmount; }
            set { payableAmount = value; }
        }

        public virtual decimal PayableAmountOriginal
        {
            get { return payableAmountOriginal; }
            set { payableAmountOriginal = value; }
        }

        public virtual decimal RemainingAmount
        {
            get { return remainingAmount; }
            set { remainingAmount = value; }
        }

        public virtual decimal RemainingAmountOriginal
        {
            get { return remainingAmountOriginal; }
            set { remainingAmountOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return discountAmountOriginal; }
            set { discountAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MBTellerPaperID
        {
            get { return mBTellerPaperID; }
            set { mBTellerPaperID = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual DateTime? VoucherDate
        {
            get { return voucherDate; }
            set { voucherDate = value; }
        }

        public virtual DateTime? VoucherPostedDate
        {
            get { return voucherPostedDate; }
            set { voucherPostedDate = value; }
        }

        public virtual string VoucherNo
        {
            get { return voucherNo; }
            set { voucherNo = value; }
        }

        public virtual int? VoucherTypeID
        {
            get { return voucherTypeID; }
            set { voucherTypeID = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual Guid? PPInvoiceID
        {
            get { return pPInvoiceID; }
            set { pPInvoiceID = value; }
        }

        public virtual decimal? DiscountRate
        {
            get { return discountRate; }
            set { discountRate = value; }
        }

        public virtual string DiscountAccount
        {
            get { return discountAccount; }
            set { discountAccount = value; }
        }

        public virtual Guid? GenID
        {
            get { return genID; }
            set { genID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? DeptEmployeeID
        {
            get { return deptEmployeeID; }
            set { deptEmployeeID = value; }
        }

        public virtual int? PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }


        private MBTellerPaper _MBTellerPaper;
        public virtual MBTellerPaper MBTellerPaper
        {
            get { return _MBTellerPaper; }
            set { _MBTellerPaper = value; }
        }



    }
}
