using System;
using System.Collections.Generic;
using Accounting.Core;
using FX.Core;
using Accounting.Core.IService;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBCreditCard
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid? branchID;
        private Guid? employeeID;
        private bool isMatch;
        private DateTime? matchDate;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? templateID;
        private string creditCardNumber;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private Guid? accountingObjectBankAccount;
        private string accountingObjectBankName;
        private int? accountingObjectType;
        private string reason;
        private string currencyID = "VND";
        private decimal? exchangeRate = 1;
        private bool? isImportPurchase;
        private Guid? pPOrderID;
        private Guid? paymentClauseID;
        private Guid? transportMethodID;
        private decimal totalAll;
        private decimal totalAllOriginal;
        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual decimal TotalAmount
        {
            get
            {
                return TotalAmountOriginal * ExchangeRate.Value;
            }
            set { totalAmount = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return IoC.Resolve<ITypeService>().Getbykey(typeID).TypeName;
                }
                catch
                {
                    return "";
                }
            }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }



        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }



        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual Guid? AccountingObjectBankAccount
        {
            get { return accountingObjectBankAccount; }
            set { accountingObjectBankAccount = value; }
        }

        public virtual string AccountingObjectBankName
        {
            get { return accountingObjectBankName; }
            set { accountingObjectBankName = value; }
        }

        public virtual int? AccountingObjectType
        {
            get { return accountingObjectType; }
            set { accountingObjectType = value; }
        }



        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual bool? IsImportPurchase
        {
            get { return isImportPurchase; }
            set { isImportPurchase = value; }
        }

        public virtual Guid? PPOrderID
        {
            get { return pPOrderID; }
            set { pPOrderID = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }

        public virtual IList<MBCreditCardDetail> MBCreditCardDetails { get; set; }
        public virtual IList<MBCreditCardDetailTax> MBCreditCardDetailTaxs { get; set; }
        public virtual IList<MBCreditCardDetailVendor> MBCreditCardDetailVendors { get; set; }
        public virtual IList<RefVoucherRSInwardOutward> RefVoucherRSInwardOutwards { get; set; }

        public MBCreditCard()
        {
            MBCreditCardDetails = new List<MBCreditCardDetail>();
            MBCreditCardDetailTaxs = new List<MBCreditCardDetailTax>();
            MBCreditCardDetailVendors = new List<MBCreditCardDetailVendor>();
            RefVoucherRSInwardOutwards = new List<RefVoucherRSInwardOutward>();

        }

        //private CreditCard _creditCard;
        //public virtual CreditCard CreditCard
        //{
        //    get { return _creditCard; }
        //    set { _creditCard = value; }
        //}

        public virtual decimal TotalAllAmount
        {
            get { return totalAmount + totalVATAmount; }
        }

        public virtual decimal TotalAll
        {
            get
            {
                if (typeID == 170)
                    return TotalAmount;
                else if (typeID == 171 || typeID == 173 || typeID == 172 || typeID == 906) return totalAll;
                else return TotalAllOriginal * ExchangeRate??1;
            }
            set { totalAll = value; }
        }
        public virtual decimal TotalAllOriginal
        {
            get
            {
                if (typeID == 170)
                    return TotalAmountOriginal;
                else return totalAllOriginal;
            }
            set { totalAllOriginal = value; }
        }
    }
}
