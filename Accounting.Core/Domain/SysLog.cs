using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysLog {
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual Guid UserID { get; set; }
        public virtual string UserName { get; set; }
        public virtual string ComputerName { get; set; }
        public virtual string ComputerMAC { get; set; }
        public virtual string ComputerIP { get; set; }
        public virtual string WorkName { get; set; }
        public virtual string Action { get; set; }
        public virtual string Reference { get; set; }
        public virtual Guid? RefID { get; set; }
        public virtual DateTime Time { get; set; }
        public virtual string Description { get; set; }

        public virtual string StringTime
        {
            get
            {
                return string.Format("{0} {1}", Time.ToString("dd/MM/yyyy"), Time.ToString("h:mm:ss tt"));
            }
        }
    }
}
