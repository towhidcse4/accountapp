﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ViewGLPayExceedCash
    {
        public virtual Guid ID { get; set; }
        public virtual string Account { get; set; }
        public virtual decimal DebitAmount { get; set; }
        public virtual decimal DebitAmountOriginal { get; set; }
        public virtual decimal CreditAmount { get; set; }
        public virtual decimal CreditAmountOriginal { get; set; }

    }
}
