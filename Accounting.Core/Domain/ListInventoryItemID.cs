﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ListInventoryItemID
    {
        public Guid RefID { get; set; }
        public int TypeID { get; set; }
        public DateTime ngay_CT { get; set; }
        public DateTime ngay_HT { get; set; }
        public string So_Hieu { get; set; }
        public DateTime Ngay_HD { get; set; }
        public string SO_HD { get; set; }
        public string Dien_giai { get; set; }
        public string TK_DOIUNG { get; set; }
        public string DVT { get; set; }
        public decimal SL { get; set; }
        public decimal DON_GIA { get; set; }
        public decimal KHAC { get; set; }
        public decimal TT { get; set; }
        public Guid InventoryItemID { get; set; }
        public string MaterialGoodsName { get; set; }
        public decimal SUM_GIA_GOC { get; set; }
    }
    public class ListInventoryItemID_period
    {
        public string Period { get; set; }
        public decimal GiaBanGoc { get; set; }
    }
}
