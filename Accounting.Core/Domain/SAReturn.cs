using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAReturn
    {
        public virtual Guid? TemplateID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime RefDateTime { get; set; }
        public virtual string No { get; set; }
        public virtual int InvoiceType { get; set; }
        public virtual decimal TotalSaleAmount { get; set; }
        public virtual decimal TotalSaleAmountOriginal { get; set; }
        public virtual decimal TotalDiscountAmount { get; set; }
        public virtual decimal TotalDiscountAmountOriginal { get; set; }
        public virtual decimal? TotalDiscountRate { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal TotalAmountOriginal { get; set; }
        private decimal _totalOWAmount;
        public virtual decimal TotalOWAmount
        {
            get { return SAReturnDetails.Sum(x=>x.OWAmount); }
            set { _totalOWAmount = value; }
        }
        private decimal _totalOWAmountOriginal;
        public virtual decimal TotalOWAmountOriginal
        {
            get { return SAReturnDetails.Sum(x => x.OWAmountOriginal); }
            set { _totalOWAmountOriginal = value; }
        }
        private decimal _totalPaymentAmountOriginal;
        public virtual decimal TotalPaymentAmountOriginal
        {
            get { return TotalAmountOriginal + TotalVATAmountOriginal - TotalDiscountAmountOriginal; }
            set { _totalPaymentAmountOriginal = value; }
        }

        private decimal _totalPaymentAmount;
        public virtual decimal TotalPaymentAmount
        {
            get { return TotalAmount + TotalVATAmount - TotalDiscountAmount; }
            set { _totalPaymentAmount = value; }
        }
        public virtual int? MaxOrderPriority
        {
            get
            {
                int? max1 = null;
                if (SAReturnDetails.Count > 0)
                {
                    max1 = SAReturnDetails.Max(k => k.OrderPriority) + 100000;
                }
                return max1;
            }
        }
        public virtual Guid ID { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual bool Exported { get; set; }
        public virtual bool? IsDeliveryVoucher { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int? AutoOWAmountCal { get; set; }
        public virtual Guid? InvoiceTypeID { get; set; }
        public virtual string IWNo { get; set; }
        public virtual DateTime? IWDate { get; set; }
        public virtual DateTime? IWPostedDate { get; set; }
        public virtual string IWContactName { get; set; }
        public virtual string IWReason { get; set; }
        public virtual string IWNumberAttach { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual Guid? PaymentClauseID { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual Guid? TransportMethodID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal TotalVATAmount { get; set; }
        public virtual decimal TotalVATAmountOriginal { get; set; }
        public virtual int? InvoiceForm { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual Guid? BillRefID { get; set; }
        public virtual string PaymentMethod { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public SAReturn()
        {
            SAReturnDetails = new List<SAReturnDetail>();
            SAReturnDetailCustomers = new List<SAReturnDetailCustomer>();
            RefVouchers = new List<RefVoucher>();
        }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public virtual IList<SAReturnDetail> SAReturnDetails { get; set; }
        public virtual IList<SAReturnDetailCustomer> SAReturnDetailCustomers { get; set; }
        public virtual int StatusInvoice { get; set; }
        public virtual bool StatusSendMail { get; set; }
        public virtual DateTime? DateSendMail { get; set; }
        public virtual string Email { get; set; }
        public virtual bool StatusConverted { get; set; }
        public virtual Guid? IDReplaceInv { get; set; }
        public virtual Guid? IDAdjustInv { get; set; }
        public virtual bool IsBill { get; set; }
        public virtual bool IsAttachListBill { get; set; }
        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (SAReturnDetails.Count > 0)
                {
                    max = SAReturnDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        public virtual decimal? MaxDiscountRate
        {
            get
            {
                decimal? max = null;
                if (SAReturnDetails.Count > 0)
                {
                    max = SAReturnDetails.Max(k => k.DiscountRate);
                }
                return max;
            }
        }
        public virtual Guid? ID_MIV { get; set; }
    }
}
