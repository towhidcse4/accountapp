using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FixedAssetDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid FixedAssetID { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual string WarrantyTime { get; set; }
        public virtual int? OrderPriority { get; set; }
    }
}
