﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TT153LostInvoice
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime _Date;
        private string _No;
        private string _GetDetectionObject;
        private string _Reason;
        private string _RespresentationInLaw;
        private DateTime _DateLost;
        private int _Status;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private string _AttachFileName;
        private Byte[] _AttachFileContent;

        public TT153LostInvoice()
        {
            TT153LostInvoiceDetails = new List<TT153LostInvoiceDetail>();
        }
        public virtual IList<TT153LostInvoiceDetail> TT153LostInvoiceDetails { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string GetDetectionObject
        {
            get { return _GetDetectionObject; }
            set { _GetDetectionObject = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual string RespresentationInLaw
        {
            get { return _RespresentationInLaw; }
            set { _RespresentationInLaw = value; }
        }

        public virtual DateTime DateLost
        {
            get { return _DateLost; }
            set { _DateLost = value; }
        }

        public virtual int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual string AttachFileName
        {
            get { return _AttachFileName; }
            set { _AttachFileName = value; }
        }

        public virtual Byte[] AttachFileContent
        {
            get { return _AttachFileContent; }
            set { _AttachFileContent = value; }
        }


        public virtual string StatusString
        {
            get
            {
                if (_Status == 0) return "Chưa nộp cho cơ quan thuế";
                if (_Status == 1) return "Đã nộp cho cơ quan thuế";
                return null;
            }
        }
    }
}
