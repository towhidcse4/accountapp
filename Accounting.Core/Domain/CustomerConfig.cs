using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CustomerConfig
    {
        private string _ID;
        private string _Code;
        private string _Name;
        private string _ValueDefault;
        private string _ValueCurent;
        private string _UnionData;
        private string _Descript;

        public virtual string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual string ValueDefault
        {
            get { return _ValueDefault; }
            set { _ValueDefault = value; }
        }

        public virtual string ValueCurent
        {
            get { return _ValueCurent; }
            set { _ValueCurent = value; }
        }

        public virtual string UnionData
        {
            get { return _UnionData; }
            set { _UnionData = value; }
        }

        public virtual string Descript
        {
            get { return _Descript; }
            set { _Descript = value; }
        }
    }
}