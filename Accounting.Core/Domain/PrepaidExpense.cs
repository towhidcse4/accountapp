using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PrepaidExpense
    {
        private Guid _ID;
        private int _TypeExpense;
        private string _PrepaidExpenseCode;
        private string _PrepaidExpenseName;
        private DateTime _Date;
        private Decimal _Amount;
        private Decimal _AllocationAmount;
        private int _AllocationTime;
        private int _AllocatedPeriod;
        private Decimal _AllocatedAmount;
        private string _AllocationAccount;
        private Boolean _IsActive;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int TypeExpense
        {
            get { return _TypeExpense; }
            set { _TypeExpense = value; }
        }

        public virtual string PrepaidExpenseCode
        {
            get { return _PrepaidExpenseCode; }
            set { _PrepaidExpenseCode = value; }
        }

        public virtual string PrepaidExpenseName
        {
            get { return _PrepaidExpenseName; }
            set { _PrepaidExpenseName = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal AllocationAmount
        {
            get { return _AllocationAmount; }
            set { _AllocationAmount = value; }
        }

        public virtual int AllocationTime
        {
            get { return _AllocationTime; }
            set { _AllocationTime = value; }
        }

        public virtual int AllocatedPeriod
        {
            get { return _AllocatedPeriod; }
            set { _AllocatedPeriod = value; }
        }

        public virtual Decimal AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual string AllocationAccount
        {
            get { return _AllocationAccount; }
            set { _AllocationAccount = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        public virtual IList<PrepaidExpenseAllocation> PrepaidExpenseAllocations { get; set; }
        public virtual IList<PrepaidExpenseVoucher> PrepaidExpenseVouchers { get; set; }

        public PrepaidExpense()
        {
            Date = DateTime.Now;
            Amount = 0;
            AllocatedAmount = 0;
            AllocatedPeriod = 0;
            AllocationAmount = 0;
            AllocationTime = 0;
            PrepaidExpenseAllocations = new List<PrepaidExpenseAllocation>();
            PrepaidExpenseVouchers = new List<PrepaidExpenseVoucher>();
        }
    }
}
