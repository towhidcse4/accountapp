﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialGoodsCategory
    {
        private Guid iD;
        private string materialGoodsCode;
        private string materialGoodsName;
        private Guid? parentID;
        private bool isParentNode;
        private string orderFixCode;
        private int grade;
        private bool isActive;
        private bool isSecurity = false;
        private bool isTool;
        public virtual string Unit { get; set; }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string MaterialGoodsCategoryCode
        {
            get { return materialGoodsCode; }
            set { materialGoodsCode = value; }
        }

        public virtual string MaterialGoodsCategoryName
        {
            get { return materialGoodsName; }
            set { materialGoodsName = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual bool IsTool
        {
            get { return isTool; }
            set { isTool = value; }
        }
    }
}
