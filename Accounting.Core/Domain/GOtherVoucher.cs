using System;
using System.Collections.Generic;
using Accounting.Core;
using FX.Core;
using Accounting.Core.IService;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucher
    {

        public virtual Guid ID { get; set; }
        public virtual Guid BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string Reason { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal TotalAmountOriginal { get; set; }
        public virtual Guid GenID { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual Guid? PSSalarySheetID { get; set; }
        public virtual Guid? CPPeriodID { get; set; }
        public virtual Guid? CPAcceptanceID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }

        //public virtual string DebitAccount { get; set; }
        //public virtual string CreditAccount { get; set; }
        public virtual IList<GOtherVoucherDetail> GOtherVoucherDetails { get; set; }
        public virtual IList<GOtherVoucherDetailTax> GOtherVoucherDetailTaxs { get; set; }
        public virtual IList<GOtherVoucherDetailExpense> GOtherVoucherDetailExpenses { get; set; }
        public virtual IList<GOtherVoucherDetailExcept> GOtherVoucherDetailExcepts { get; set; }
        public virtual IList<GOtherVoucherDetailExpenseAllocation> GOtherVoucherDetailExpenseAllocations { get; set; }
        public virtual IList<GOtherVoucherDetailDebtPayment> GOtherVoucherDetailDebtPayments { get; set; }
        public virtual IList<GOtherVoucherDetailForeignCurrency> GOtherVoucherDetailForeignCurrencys { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        private Type _type { get; set; }
        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return Type.TypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public GOtherVoucher()
        {
            GOtherVoucherDetails = new List<GOtherVoucherDetail>();
            GOtherVoucherDetailTaxs = new List<GOtherVoucherDetailTax>();
            GOtherVoucherDetailExpenses = new List<GOtherVoucherDetailExpense>();
            GOtherVoucherDetailExcepts = new List<GOtherVoucherDetailExcept>();
            GOtherVoucherDetailExpenseAllocations = new List<GOtherVoucherDetailExpenseAllocation>();
            GOtherVoucherDetailDebtPayments = new List<GOtherVoucherDetailDebtPayment>();
            GOtherVoucherDetailForeignCurrencys = new List<GOtherVoucherDetailForeignCurrency>();
            RefVouchers = new List<RefVoucher>();
        }
    }
}
