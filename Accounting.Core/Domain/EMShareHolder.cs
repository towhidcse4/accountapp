﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMShareHolder
    {
        private Guid iD;
        private Guid? shareHolderCategoryID;
        private string shareHolderCode;
        private string shareHolderName;
        private string personalTaxCode;
        private DateTime bookIssuedDate;
        private string businessRegistrationNumber;
        private DateTime businessIssuedDate;
        private string businessIssueBy;
        private Guid? bankAccountDetailID;
        private string bankName;
        private string tel;
        private string fax;
        private string email;
        private string website;
        private string address;
        private string contactPrefix;
        private string contactName;
        private string contactTitle;
        private string contactIdentificationNo;
        private DateTime contactIssueDate;
        private string contactIssueBy;
        private string contactOfficeTel;
        private string contactHomeTel;
        private string contactMobile;
        private string contactEmail;
        private string contactAddress;
        private bool isPersonal;
        private int typeID;
        private decimal totalShare;
        private bool isActive;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid? ShareHolderCategoryID
        {
            get { return shareHolderCategoryID; }
            set { shareHolderCategoryID = value; }
        }
        public virtual string ShareHolderCode
        {
            get { return shareHolderCode; }
            set { shareHolderCode = value; }
        }
        public virtual string ShareHolderName
        {
            get { return shareHolderName; }
            set { shareHolderName = value; }
        }
        public virtual string PersonalTaxCode
        {
            get { return personalTaxCode; }
            set { personalTaxCode = value; }
        }
        public virtual DateTime BookIssuedDate
        {
            get { return bookIssuedDate; }
            set { bookIssuedDate = value; }
        }
        public virtual string BusinessRegistrationNumber
        {
            get { return businessRegistrationNumber; }
            set { businessRegistrationNumber = value; }
        }
        public virtual DateTime BusinessIssuedDate
        {
            get { return businessIssuedDate; }
            set { businessIssuedDate = value; }
        }
        public virtual string BusinessIssueBy
        {
            get { return businessIssueBy; }
            set { businessIssueBy = value; }
        }
        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }
        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }
        public virtual string Tel
        {
            get { return tel; }
            set { tel = value; }
        }
        public virtual string Fax
        {
            get { return fax; }
            set { fax = value; }
        }
        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }
        public virtual string Website
        {
            get { return website; }
            set { website = value; }
        }
        public virtual string Address
        {
            get { return address; }
            set { address = value; }
        }
        public virtual string ContactPrefix
        {
            get { return contactPrefix; }
            set { contactPrefix = value; }
        }
        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }
        public virtual string ContactTitle
        {
            get { return contactTitle; }
            set { contactTitle = value; }
        }
        public virtual string ContactIdentificationNo
        {
            get { return contactIdentificationNo; }
            set { contactIdentificationNo = value; }
        }
         
        public virtual DateTime ContactIssueDate
        {
            get { return contactIssueDate; }
            set { contactIssueDate = value; }
        }
        public virtual string ContactIssueBy
        {
            get { return contactIssueBy; }
            set { contactIssueBy = value; }
        }
        public virtual string ContactOfficeTel
        {
            get { return contactOfficeTel; }
            set { contactOfficeTel = value; }
        }
        public virtual string ContactHomeTel
        {
            get { return contactHomeTel; }
            set { contactHomeTel = value; }
        }
        public virtual string ContactMobile
        {
            get { return contactMobile; }
            set { contactMobile = value; }
        }
        public virtual string ContactEmail
        {
            get { return contactEmail; }
            set { contactEmail = value; }
        }
        public virtual string ContactAddress
        {
            get { return contactAddress; }
            set { contactAddress = value; }
        }
        public virtual bool IsPersonal
        {
            get { return isPersonal; }
            set { isPersonal = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual decimal TotalShare
        {
            get { return totalShare; }
            set { totalShare = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public EMShareHolder()
        {
            BankAccounts = new List<EMRegistration>();
        }
        public virtual IList<EMRegistration> BankAccounts { get; set; }


        
    }
}
