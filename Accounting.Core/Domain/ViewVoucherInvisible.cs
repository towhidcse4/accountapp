﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ViewVoucherInvisible
    {
        public virtual System.Guid ID { get; set; }
        public virtual System.Guid RefID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeName { get; set; }
        public virtual int TypeGroupID { get; set; }
        public virtual string No { get; set; }
        public virtual string InwardNo { get; set; }
        public virtual string OutwardNo { get; set; }
        public virtual string MoneydNo { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual string Reason { get; set; }
        public virtual System.Nullable<System.Guid> EmployeeID { get; set; }
        public virtual System.Nullable<System.Guid> BranchID { get; set; }
        public virtual bool? Recorded { get; set; }
        public virtual decimal? TotalAmount { get; set; }
        public virtual decimal? TotalAmountOriginal { get; set; }
        public virtual string RefTable { get; set; }
    }

    public class ViewVoucherReset
    {
        public virtual System.Guid ID { get; set; }
        public virtual string No { get; set; }
        public virtual string NoReset { get; set; }
        public virtual int TypeGroupID { get; set; }
        public virtual bool? Recorded { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual int? NoCurrentValue { get; set; }
        public virtual bool Checked { get; set; }
    }
}
