﻿using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Accounting.Core.Domain.ControlsMapping.ConfigImport
{
    public class ConfigTMNH
    {
        public List<object> SetDataPhieuThu(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            List<MCReceipt_Model> dt_cv = data.Cast<MCReceipt_Model>().ToList();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();

            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaDoiTuong = item_first.MaDoiTuong;
                        t.AccountingObjectName = item_first.AccountingObjectName;
                        t.AccountingObjectAddress = item_first.AccountingObjectAddress;
                        t.Payers = item_first.Payers;
                        t.Reason = item_first.Reason;
                        t.NumberAttach = item_first.NumberAttach;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                    }
                });
            }

            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaDoiTuong,
                                 a.AccountingObjectAddress,
                                 a.AccountingObjectName,
                                 a.Payers,
                                 a.Reason,
                                 a.NumberAttach,
                                 a.CurrencyID,
                                 a.ExchangeRate
                             } into gr
                             select new MCReceipt_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaDoiTuong = gr.Key.MaDoiTuong,
                                 AccountingObjectAddress = gr.Key.AccountingObjectAddress,
                                 AccountingObjectName = gr.Key.AccountingObjectName,
                                 Payers = gr.Key.Payers,
                                 Reason = gr.Key.Reason,
                                 NumberAttach = gr.Key.NumberAttach,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 Amount = gr.Sum(t => t.Amount),
                                 AmountOriginal = gr.Sum(t => t.AmountOriginal),
                             };
            var list_detail = from b in dt_cv
                              group b by new
                              {
                                  b.No,
                                  b.DebitAccount,
                                  b.CreditAccount,
                                  b.MaPhongBan,
                                  b.MaKhoanMucCP,
                                  b.MaDoiTuongCP,
                                  b.MaHopDong
                              } into gr
                              select new MCReceipt_Model
                              {
                                  No = gr.Key.No,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal)
                              };
            List<MCReceipt> data_phieuthu = new List<MCReceipt>();
            foreach (var item in list_group)
            {
                MCReceipt mCReceipt = new MCReceipt();
                mCReceipt.Date = item.Date ?? (new DateTime()).Date;
                mCReceipt.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                mCReceipt.No = item.No;
                var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaDoiTuong);
                if (accountingobj != null)
                {
                    mCReceipt.AccountingObjectID = accountingobj.ID;
                    mCReceipt.AccountingObjectName = accountingobj.AccountingObjectName;
                    //mCReceipt.AccountingObjectAddress = accountingobj.Address;
                }
                mCReceipt.AccountingObjectAddress = item.AccountingObjectAddress;
                mCReceipt.Payers = item.Payers;
                mCReceipt.Reason = item.Reason;
                mCReceipt.NumberAttach = item.NumberAttach;
                mCReceipt.CurrencyID = item.CurrencyID;
                mCReceipt.ExchangeRate = item.ExchangeRate;
                mCReceipt.Recorded = true;
                mCReceipt.AccountingObjectType = item.AccountingObjectType;
                if (mCReceipt.AccountingObjectType == 2)
                {
                    if (accountingobj != null)
                    {
                        mCReceipt.EmployeeID = accountingobj.ID;
                    }
                    else
                        mCReceipt.EmployeeID = null;
                }
                else
                    mCReceipt.EmployeeID = null;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MCReceiptDetail> list_tmp = new List<MCReceiptDetail>();
                    foreach (var item1 in list_dt)
                    {
                        MCReceiptDetail dtl = new MCReceiptDetail();
                        dtl.Description = "Thu tiền mặt";
                        dtl.DebitAccount = item1.DebitAccount;
                        dtl.CreditAccount = item1.CreditAccount;
                        dtl.Amount = (decimal)item1.Amount;
                        dtl.AmountOriginal = (decimal)item1.AmountOriginal;
                        if (accountingobj != null)
                        {
                            dtl.AccountingObjectID = accountingobj.ID;
                            dtl.AccountingObjectName = accountingobj.AccountingObjectName;
                        }
                        var department = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == item1.MaPhongBan);
                        if (department != null)
                            dtl.DepartmentID = department.ID;
                        var exp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == item1.MaKhoanMucCP);
                        if (exp != null)
                            dtl.ExpenseItemID = exp.ID;
                        var costSet = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == item1.MaDoiTuongCP);
                        if (costSet != null)
                            dtl.CostSetID = costSet.ID;
                        var contact = _IEMContractService.Query.FirstOrDefault(t => t.Code == item1.MaHopDong);
                        if (contact != null)
                            dtl.ContractID = contact.ID;
                        list_tmp.Add(dtl);
                    }
                    mCReceipt.MCReceiptDetails = list_tmp;
                    var sum_amount = list_tmp.Sum(t => t.Amount);
                    var sum_allorigi = list_tmp.Sum(t => t.AmountOriginal);
                    mCReceipt.TotalAmount = sum_amount;
                    mCReceipt.TotalAmountOriginal = sum_allorigi;
                    mCReceipt.TotalAll = sum_amount;
                    mCReceipt.TotalAllOriginal = sum_allorigi;
                }
                data_phieuthu.Add(mCReceipt);
            }
            return data_phieuthu.Cast<object>().ToList();
        }
        public List<object> SetDataPhieuChi(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();

            List<MCPayment_Model> dt_cv = data.Cast<MCPayment_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaDoiTuong = item_first.MaDoiTuong;
                        t.AccountingObjectName = item_first.AccountingObjectName;
                        t.AccountingObjectAddress = item_first.AccountingObjectAddress;
                        t.AccountingObjectType = item_first.AccountingObjectType;
                        t.Receiver = item_first.Receiver;
                        t.Reason = item_first.Reason;
                        t.NumberAttach = item_first.NumberAttach;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaDoiTuong,
                                 a.AccountingObjectAddress,
                                 a.AccountingObjectName,
                                 a.AccountingObjectType,
                                 a.Receiver,
                                 a.Reason,
                                 a.NumberAttach,
                                 a.CurrencyID,
                                 a.ExchangeRate
                             } into gr
                             select new MCPayment_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaDoiTuong = gr.Key.MaDoiTuong,
                                 AccountingObjectAddress = gr.Key.AccountingObjectAddress,
                                 AccountingObjectName = gr.Key.AccountingObjectName,
                                 AccountingObjectType = gr.Key.AccountingObjectType,
                                 Receiver = gr.Key.Receiver,
                                 Reason = gr.Key.Reason,
                                 NumberAttach = gr.Key.NumberAttach,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 Amount = gr.Sum(t => t.Amount),
                                 AmountOriginal = gr.Sum(t => t.AmountOriginal),
                             };
            var list_detail = from b in dt_cv
                              group b by new
                              {
                                  b.No,
                                  b.DebitAccount,
                                  b.CreditAccount,
                                  b.MaMucThuChi,
                                  b.MaKhoanMucCP,
                                  b.MaPhongBan,
                                  b.MaDoiTuongCP,
                                  b.MaHopDong,
                                  b.MaThongKe,
                                  b.Description,
                                  b.VATAccount,
                                  b.InvoiceDate,
                                  b.InvoiceNo,
                                  b.InvoiceSeries,
                                  b.InvoiceTemplate,
                                  b.MaNhomHHDV

                              } into gr
                              select new MCPayment_Model
                              {
                                  No = gr.Key.No,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaThongKe = gr.Key.MaThongKe,
                                  VATAccount = gr.Key.VATAccount,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  MaNhomHHDV = gr.Key.MaNhomHHDV,
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATRate = gr.Sum(t => t.VATRate)
                              };
            List<MCPayment> list_phieu_chi = new List<MCPayment>();
            foreach (var item in list_group)
            {
                MCPayment mC = new MCPayment();
                mC.No = item.No;
                mC.Date = item.Date ?? (new DateTime()).Date;
                mC.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                var accountingobj = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaDoiTuong);
                if (accountingobj != null)
                {
                    mC.AccountingObjectID = accountingobj.ID;
                    mC.AccountingObjectName = accountingobj.AccountingObjectName;
                    mC.AccountingObjectAddress = accountingobj.Address;
                    mC.AccountingObjectType = accountingobj.ObjectType;
                }
                mC.Receiver = item.Receiver;
                mC.Reason = item.Reason;
                mC.NumberAttach = item.NumberAttach;
                mC.CurrencyID = item.CurrencyID;
                mC.ExchangeRate = item.ExchangeRate;
                mC.Recorded = true;
                if (mC.AccountingObjectType == 2)
                {
                    if (accountingobj != null)
                    {
                        mC.EmployeeID = accountingobj.ID;
                    }
                    else
                        mC.EmployeeID = null;
                }
                else
                    mC.EmployeeID = null;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MCPaymentDetail> list_mcDetail = new List<MCPaymentDetail>();
                    List<MCPaymentDetailTax> list_mcDetailtax = new List<MCPaymentDetailTax>();
                    foreach (var item1 in list_dt)
                    {
                        MCPaymentDetail mcDetail = new MCPaymentDetail();

                        mcDetail.DebitAccount = item1.DebitAccount;
                        mcDetail.CreditAccount = item1.CreditAccount;
                        mcDetail.Amount = (decimal)item1.Amount;
                        mcDetail.AmountOriginal = (decimal)item1.AmountOriginal;
                        var BudgetObj = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == item1.MaMucThuChi);
                        if (BudgetObj != null)
                            mcDetail.BudgetItemID = BudgetObj.ID;
                        var CostsetObj = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == item1.MaDoiTuongCP);
                        if (CostsetObj != null)
                            mcDetail.CostSetID = CostsetObj.ID;
                        var EMcontractObj = _IEMContractService.Query.FirstOrDefault(t => t.Code == item1.MaHopDong);
                        if (EMcontractObj != null)
                            mcDetail.ContractID = EMcontractObj.ID;
                        var StatisticsCodeObj = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == item1.MaThongKe);
                        if (StatisticsCodeObj != null)
                            mcDetail.StatisticsCodeID = StatisticsCodeObj.ID;
                        var DepartmentObj = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == item1.MaPhongBan);
                        if (DepartmentObj != null)
                            mcDetail.DepartmentID = DepartmentObj.ID;
                        var ExpenseItemObj = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == item1.MaKhoanMucCP);
                        if (ExpenseItemObj != null)
                            mcDetail.ExpenseItemID = ExpenseItemObj.ID;
                        list_mcDetail.Add(mcDetail);

                        MCPaymentDetailTax mcTax = new MCPaymentDetailTax();
                        mcTax.Description = "Thuế giá trị gia tăng";
                        mcTax.VATAmount = (decimal)item1.VATAmount;
                        mcTax.VATAmountOriginal = (decimal)item1.VATAmountOriginal;
                        mcTax.VATRate = item1.VATRate;
                        if (accountingobj != null)
                        {
                            mcDetail.Description = "Chi tiền mặt cho " + accountingobj.AccountingObjectName;
                            mcTax.AccountingObjectID = accountingobj.ID;
                            mcTax.AccountingObjectName = accountingobj.AccountingObjectName;
                            mcTax.AccountingObjectAddress = accountingobj.Address;
                            mcTax.CompanyTaxCode = accountingobj.TaxCode;
                        }
                        var GoodsServicePurchaseObj = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == item1.MaNhomHHDV);
                        if (GoodsServicePurchaseObj != null)
                            mcTax.GoodsServicePurchaseID = GoodsServicePurchaseObj.ID;
                        mcTax.VATAccount = item1.VATAccount;
                        mcTax.InvoiceType = 0;
                        mcTax.InvoiceDate = item1.InvoiceDate;
                        mcTax.InvoiceNo = item1.InvoiceNo;
                        mcTax.InvoiceSeries = item1.InvoiceSeries;
                        mcTax.InvoiceTemplate = item1.InvoiceTemplate;
                        list_mcDetailtax.Add(mcTax);
                    }
                    mC.MCPaymentDetails = list_mcDetail;
                    mC.MCPaymentDetailTaxs = list_mcDetailtax;
                    mC.TotalAmount = list_mcDetail.Sum(t => t.Amount);
                    mC.TotalAmountOriginal = list_mcDetail.Sum(t => t.AmountOriginal);
                    mC.TotalVATAmount = list_mcDetailtax.Sum(t => t.VATAmount);
                    mC.TotalVATAmountOriginal = list_mcDetailtax.Sum(t => t.VATAmountOriginal);
                    mC.TotalAll = list_mcDetail.Sum(t => t.Amount);
                    mC.TotalAllOriginal = list_mcDetail.Sum(t => t.AmountOriginal);
                }
                list_phieu_chi.Add(mC);
            }
            return list_phieu_chi.Cast<object>().ToList();
        }
        public List<object> SetDataSecUNC(List<object> data)
        {
            List<object> data_cv = new List<object>();
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            IInvoiceTypeService _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            List<MBTellerPaper_Model> dt_cv = data.Cast<MBTellerPaper_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.Reason = item_first.Reason;
                        t.MaDoiTuong = item_first.MaDoiTuong;
                        t.AccountingObjectName = item_first.AccountingObjectName;
                        t.AccountingObjectAddress = item_first.AccountingObjectAddress;
                        t.AccountingObjectType = item_first.AccountingObjectType;
                        t.IdentificationNo = item_first.IdentificationNo;
                        t.IssueDate = item_first.IssueDate;
                        t.IssueBy = item_first.IssueBy;
                        t.Receiver = item_first.Receiver;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        t.LoaiGiayBN = item_first.LoaiGiayBN;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.Reason,
                                 a.MaDoiTuong,
                                 a.IdentificationNo,
                                 a.IssueDate,
                                 a.IssueBy,
                                 a.CurrencyID,
                                 a.Receiver,
                                 a.ExchangeRate,
                                 a.MaTKChi,
                                 a.LoaiGiayBN
                             } into gr
                             select new MBTellerPaper_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 Reason = gr.Key.Reason,
                                 MaDoiTuong = gr.Key.MaDoiTuong,
                                 IdentificationNo = gr.Key.IdentificationNo,
                                 IssueDate = gr.Key.IssueDate,
                                 IssueBy = gr.Key.IssueBy,
                                 CurrencyID = gr.Key.CurrencyID,
                                 Receiver = gr.Key.Receiver,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 MaTKChi = gr.Key.MaTKChi,
                                 LoaiGiayBN = gr.Key.LoaiGiayBN
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaThongKe,
                                  a.MaPhongBan,
                                  a.MaKhoanMucCP,
                                  a.Description,
                                  a.VATAccount,
                                  a.VATRate,
                                  a.InvoiceTemplate,
                                  a.InvoiceDate,
                                  a.InvoiceNo,
                                  a.InvoiceSeries,
                                  a.MaNhomHHDV
                              } into gr
                              select new MBTellerPaper_Model
                              {
                                  No = gr.Key.No,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaThongKe = gr.Key.MaThongKe,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  Description = gr.Key.Description,
                                  VATAccount = gr.Key.VATAccount,
                                  VATRate = gr.Key.VATRate,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  MaNhomHHDV = gr.Key.MaNhomHHDV,
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  PretaxAmount = gr.Sum(t => t.PretaxAmount)
                              };
            List<MBTellerPaper> list_MBTellerPaper = new List<MBTellerPaper>();
            foreach (var item in list_group)
            {
                MBTellerPaper mBTellerPaper = new MBTellerPaper();
                mBTellerPaper.No = item.No;
                mBTellerPaper.Date = item.Date ?? (new DateTime()).Date;
                mBTellerPaper.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                mBTellerPaper.TypeID = item.LoaiGiayBN == 0 ? 120 : (item.LoaiGiayBN == 1 ? 130 : 140);
                var bankChi = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKChi);
                if (bankChi != null)
                    mBTellerPaper.BankName = bankChi.BankName;
                mBTellerPaper.Reason = item.Reason;
                var acount_object = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaDoiTuong);
                if (acount_object != null)
                {
                    mBTellerPaper.AccountingObjectID = acount_object.ID;
                    mBTellerPaper.AccountingObjectName = acount_object.AccountingObjectName;
                    mBTellerPaper.AccountingObjectAddress = acount_object.Address;
                    if (acount_object.ObjectType == 2)
                        mBTellerPaper.EmployeeID = acount_object.ID;
                    else
                        mBTellerPaper.EmployeeID = null;
                }
                var banknhan = _IAccountingObjectBankAccountService.Query.FirstOrDefault();
                if (banknhan != null)
                {
                    mBTellerPaper.AccountingObjectBankAccount = banknhan.ID;
                    mBTellerPaper.AccountingObjectBankName = banknhan.BankName;
                }
                mBTellerPaper.Receiver = item.Receiver;
                mBTellerPaper.IdentificationNo = item.IdentificationNo;
                mBTellerPaper.IssueDate = item.IssueDate;
                mBTellerPaper.IssueBy = item.IssueBy;
                mBTellerPaper.CurrencyID = item.CurrencyID;
                mBTellerPaper.ExchangeRate = item.ExchangeRate;
                mBTellerPaper.Recorded = true;
                var tkNhan = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKNguoiNhan);
                if (tkNhan != null)
                    mBTellerPaper.BankAccountDetailID = tkNhan.ID;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MBTellerPaperDetail> list_mcDetail = new List<MBTellerPaperDetail>();
                    List<MBTellerPaperDetailTax> list_mcDetailtax = new List<MBTellerPaperDetailTax>();
                    foreach (var item1 in list_dt)
                    {
                        MBTellerPaperDetail detail = new MBTellerPaperDetail();
                        detail.Description = item.Reason;
                        detail.DebitAccount = item1.DebitAccount;
                        detail.CreditAccount = item1.CreditAccount;
                        detail.Amount = item1.Amount ?? 0;
                        detail.AmountOriginal = item1.AmountOriginal ?? 0;

                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == item.MaMucThuChi);
                        if (mucthuchi != null)
                            detail.BudgetItemID = mucthuchi.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == item1.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            detail.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == item1.MaHopDong);
                        if (hopdong != null)
                            detail.ContractID = hopdong.ID;
                        var doituong = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item1.MaDoiTuong);
                        if (doituong != null)
                        {
                            detail.AccountingObjectID = doituong.ID;
                            detail.CreditAccountingObjectID = doituong.ID;
                        }
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == item1.MaThongKe);
                        if (thongke != null)
                            detail.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == item1.MaPhongBan);
                        if (phongban != null)
                            detail.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == item1.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            detail.ExpenseItemID = khoanmuccp.ID;
                        if (bankChi != null)
                            detail.BankAccountDetailID = bankChi.ID;
                        list_mcDetail.Add(detail);

                        MBTellerPaperDetailTax tax = new MBTellerPaperDetailTax();
                        tax.Description = item1.Description;
                        tax.VATAmount = item1.VATAmount ?? 0;
                        tax.VATAmountOriginal = item1.VATAmountOriginal ?? 0;
                        tax.VATRate = item1.VATRate;
                        tax.PretaxAmount = item1.PretaxAmount ?? 0;
                        tax.PretaxAmountOriginal = 0;
                        tax.InvoiceTemplate = item1.InvoiceTemplate;
                        tax.InvoiceType = 0;
                        tax.InvoiceDate = item1.InvoiceDate;
                        tax.InvoiceNo = item1.InvoiceNo;
                        tax.InvoiceSeries = item1.InvoiceSeries;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == item1.MaNhomHHDV);
                        if (nhomhhdv != null)
                            tax.GoodsServicePurchaseID = nhomhhdv.ID;
                        if (doituong != null)
                        {
                            tax.AccountingObjectID = doituong.ID;
                            tax.AccountingObjectName = doituong.AccountingObjectName;
                            tax.AccountingObjectAddress = doituong.Address;
                            tax.CompanyTaxCode = doituong.TaxCode;
                        }
                        tax.VATAccount = item.VATAccount;
                        list_mcDetailtax.Add(tax);
                    }
                    mBTellerPaper.MBTellerPaperDetails = list_mcDetail;
                    mBTellerPaper.MBTellerPaperDetailTaxs = list_mcDetailtax;
                    var sumquydoi = list_mcDetail.Sum(t => t.Amount);
                    var sumsotien = list_mcDetail.Sum(t => t.AmountOriginal);
                    mBTellerPaper.TotalAmount = sumquydoi;
                    mBTellerPaper.TotalAmountOriginal = sumsotien;
                    mBTellerPaper.TotalAll = sumquydoi;
                    mBTellerPaper.TotalAllOriginal = sumsotien;
                    mBTellerPaper.TotalVATAmount = list_mcDetailtax.Sum(t => t.VATAmount);
                    mBTellerPaper.TotalVATAmountOriginal = list_mcDetailtax.Sum(t => t.VATAmountOriginal);
                }
                list_MBTellerPaper.Add(mBTellerPaper);
            }
            data_cv = list_MBTellerPaper.Cast<object>().ToList();
            return data_cv;
        }
        public List<object> SetDataThuNganHang(List<object> data)
        {
            List<object> data_cv = new List<object>();
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();//
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();//
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();//
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();//
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();//
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();//
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();//
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();

            List<MBDeposit_Model> dt_cv = data.Cast<MBDeposit_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaDoiTuong = item_first.MaDoiTuong;
                        t.AccountingObjectName = item_first.AccountingObjectName;
                        t.AccountingObjectAddress = item_first.AccountingObjectAddress;
                        t.AccountingObjectType = item_first.AccountingObjectType;
                        t.SoTKHuong = item_first.SoTKHuong;
                        t.Reason = item_first.Reason;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaDoiTuong,
                                 a.AccountingObjectName,
                                 a.AccountingObjectAddress,
                                 a.AccountingObjectType,
                                 a.SoTKHuong,
                                 a.Reason,
                                 a.CurrencyID,
                                 a.ExchangeRate
                             } into gr
                             select new MBDeposit_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaDoiTuong = gr.Key.MaDoiTuong,
                                 AccountingObjectName = gr.Key.AccountingObjectName,
                                 AccountingObjectAddress = gr.Key.AccountingObjectAddress,
                                 AccountingObjectType = gr.Key.AccountingObjectType,
                                 SoTKHuong = gr.Key.SoTKHuong,
                                 Reason = gr.Key.Reason,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.InvoiceTemplate,
                                  a.InvoiceSeries,
                                  a.InvoiceNo,
                                  a.InvoiceDate,
                                  a.MaMucThuChi,
                                  a.MaKhoanMucCP,
                                  a.MaPhongBan,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaThongKe
                              } into gr
                              select new MBDeposit_Model
                              {
                                  No = gr.Key.No,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaThongKe = gr.Key.MaThongKe,
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount)
                              };
            List<MBDeposit> list_MBDeposit = new List<MBDeposit>();
            foreach (var item in list_group)
            {
                MBDeposit master = new MBDeposit();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.TypeID = 160;
                var doituong = _IAccountingObjectService.Query.First(t => t.AccountingObjectCode == item.MaDoiTuong);
                if (doituong != null)
                {
                    master.AccountingObjectID = doituong.ID;
                    master.AccountingObjectName = doituong.AccountingObjectName;
                    master.AccountingObjectAddress = doituong.Address;
                    master.AccountingObjectType = doituong.ObjectType;
                }
                var tkhuong = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.SoTKHuong);
                if (tkhuong != null)
                {
                    master.BankAccountDetailID = tkhuong.ID;
                    master.BankName = tkhuong.BankName;
                }
                master.Reason = item.SoTKHuong;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.InvoiceDate = item.InvoiceDate;
                master.InvoiceNo = item.InvoiceNo;
                master.InvoiceSeries = item.InvoiceSeries;
                master.InvoiceTemplate = item.InvoiceTemplate;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MBDepositDetail> list_mbDetail = new List<MBDepositDetail>();
                    foreach (var item1 in list_dt)
                    {
                        MBDepositDetail detail = new MBDepositDetail();
                        detail.Description = item.Reason;
                        detail.DebitAccount = item1.DebitAccount;
                        detail.CreditAccount = item1.CreditAccount;
                        detail.Amount = item1.Amount ?? 0;
                        detail.AmountOriginal = item1.AmountOriginal ?? 0;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == item1.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            detail.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == item1.MaHopDong);
                        if (hopdong != null)
                            detail.ContractID = hopdong.ID;
                        if (doituong != null)
                            detail.AccountingObjectID = doituong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == item1.MaThongKe);
                        if (thongke != null)
                            detail.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == item1.MaPhongBan);
                        if (phongban != null)
                            detail.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == item1.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            detail.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == item1.MaMucThuChi);
                        if (mucthuchi != null)
                            detail.BudgetItemID = mucthuchi.ID;
                        list_mbDetail.Add(detail);
                    }
                    master.MBDepositDetails = list_mbDetail;
                }
                list_MBDeposit.Add(master);
            }
            data_cv = list_MBDeposit.Cast<object>().ToList();
            return data_cv;
        }
        public List<object> SetDataTheTinDung(List<object> data)
        {
            List<object> data_cv = new List<object>();
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();//
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();//
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();//
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();//
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();//
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();//
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();//
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            List<MBCreditCard_Model> dt_cv = data.Cast<MBCreditCard_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaDoiTuong = item_first.MaDoiTuong;
                        t.AccountingObjectName = item_first.AccountingObjectName;
                        t.AccountingObjectType = item_first.AccountingObjectType;
                        t.CreditCardNumber = item_first.CreditCardNumber;
                        t.Reason = item_first.Reason;
                        t.AccountingObjectBankAccount = item_first.AccountingObjectBankAccount;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaDoiTuong,
                                 a.AccountingObjectName,
                                 a.AccountingObjectType,
                                 a.CreditCardNumber,
                                 a.Reason,
                                 a.AccountingObjectBankAccount,
                                 a.CurrencyID,
                                 a.ExchangeRate
                             } into gr
                             select new MBCreditCard_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaDoiTuong = gr.Key.MaDoiTuong,
                                 AccountingObjectName = gr.Key.AccountingObjectName,
                                 AccountingObjectType = gr.Key.AccountingObjectType,
                                 CreditCardNumber = gr.Key.CreditCardNumber,
                                 Reason = gr.Key.Reason,
                                 AccountingObjectBankAccount = gr.Key.AccountingObjectBankAccount,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.MaNhanVien,
                                  a.MaMucThuChi,
                                  a.MaKhoanMucCP,
                                  a.MaPhongBan,
                                  a.MaDoiTuongCP,
                                  a.MaThongKe,
                                  a.Description,
                                  a.VATAccount,
                                  a.VATRate,
                                  a.InvoiceTemplate,
                                  a.InvoiceSeries,
                                  a.InvoiceNo,
                                  a.InvoiceDate,
                                  a.MaNhomHHDV
                              } into gr
                              select new MBCreditCard_Model
                              {
                                  No = gr.Key.No,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  MaNhanVien = gr.Key.MaNhanVien,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaThongKe = gr.Key.MaThongKe,
                                  Description = gr.Key.Description,
                                  VATAccount = gr.Key.VATAccount,
                                  VATRate = gr.Key.VATRate,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  MaNhomHHDV = gr.Key.MaNhomHHDV,
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  PretaxAmountOriginal = gr.Sum(t => t.PretaxAmountOriginal),
                                  PretaxAmount = gr.Sum(t => t.PretaxAmount),
                              };
            List<MBCreditCard> list_MBDeposit = new List<MBCreditCard>();
            foreach (var item in list_group)
            {
                MBCreditCard master = new MBCreditCard();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.CreditCardNumber = item.CreditCardNumber;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.TypeID = 170;
                AccountingObject doituong = null;
                if (!string.IsNullOrEmpty(item.MaDoiTuong))
                    doituong = _IAccountingObjectService.Query.First(t => t.AccountingObjectCode == item.MaDoiTuong);
                if (doituong != null)
                {
                    master.AccountingObjectID = doituong.ID;
                    master.AccountingObjectName = doituong.AccountingObjectName;
                    master.AccountingObjectAddress = doituong.Address;
                    master.AccountingObjectType = doituong.ObjectType;
                    if (master.AccountingObjectType == 2)
                        master.EmployeeID = doituong.ID;
                }
                var tknguoinhan = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == item.AccountingObjectBankAccount);
                if (tknguoinhan != null)
                {
                    master.AccountingObjectBankAccount = tknguoinhan.ID;
                    master.AccountingObjectBankName = tknguoinhan.BankName;
                }
                master.Reason = item.Reason;
                master.Exported = true;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MBCreditCardDetail> list_mbdt = new List<MBCreditCardDetail>();
                    List<MBCreditCardDetailTax> list_mbdttax = new List<MBCreditCardDetailTax>();
                    foreach (var ct in list_dt)
                    {
                        MBCreditCardDetail itdetail = new MBCreditCardDetail();
                        itdetail.Description = ct.Reason;
                        itdetail.DebitAccount = ct.DebitAccount;
                        itdetail.CreditAccount = ct.CreditAccount;
                        itdetail.Amount = ct.Amount ?? 0;
                        itdetail.AmountOriginal = ct.AmountOriginal ?? 0;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            itdetail.BudgetItemID = mucthuchi.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            itdetail.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            itdetail.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            itdetail.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            itdetail.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            itdetail.ExpenseItemID = khoanmuccp.ID;
                        if (doituong != null)
                        {
                            if (doituong.ObjectType == 2)
                                itdetail.EmployeeID = doituong.ID;
                        }
                        list_mbdt.Add(itdetail);
                        MBCreditCardDetailTax tax = new MBCreditCardDetailTax();
                        tax.Description = ct.Description;
                        tax.VATAmount = ct.VATAmount ?? 0;
                        tax.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                        tax.VATRate = ct.VATRate;
                        tax.PretaxAmount = ct.PretaxAmount ?? 0;
                        tax.PretaxAmountOriginal = ct.PretaxAmountOriginal ?? 0;
                        tax.InvoiceDate = ct.InvoiceDate;
                        tax.InvoiceNo = ct.InvoiceNo;
                        tax.InvoiceSeries = ct.InvoiceSeries;
                        tax.InvoiceTemplate = ct.InvoiceTemplate;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == ct.MaNhomHHDV);
                        if (nhomhhdv != null)
                            tax.GoodsServicePurchaseID = nhomhhdv.ID;
                        if (doituong != null)
                        {
                            tax.AccountingObjectID = doituong.ID;
                            tax.AccountingObjectName = doituong.AccountingObjectName;
                            tax.AccountingObjectAddress = doituong.Address;
                            tax.CompanyTaxCode = doituong.TaxCode;
                        }
                        tax.VATAccount = ct.VATAccount;
                        list_mbdttax.Add(tax);
                    }
                    master.MBCreditCardDetails = list_mbdt;
                    master.MBCreditCardDetailTaxs = list_mbdttax;
                    var sumsotienqd = list_mbdt.Sum(t => t.Amount);
                    var sumsotien = list_mbdt.Sum(t => t.AmountOriginal);
                    master.TotalAmount = sumsotienqd;
                    master.TotalAmountOriginal = sumsotien;
                    master.TotalAll = sumsotienqd;
                    master.TotalAllOriginal = sumsotien;
                    master.TotalVATAmount = list_mbdttax.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_mbdttax.Sum(t => t.VATAmountOriginal);
                }
                list_MBDeposit.Add(master);
            }
            data_cv = list_MBDeposit.Cast<object>().ToList();
            return data_cv;
        }
        public List<object> SetDataChuyenTienNB(List<object> data)
        {
            List<object> data_cv = new List<object>();
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();

            List<MBInternalTransfer_Model> dt_cv = data.Cast<MBInternalTransfer_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.Reason = item_first.Reason;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.Reason,
                             } into gr
                             select new MBInternalTransfer_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 Reason = gr.Key.Reason,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.TKNHChuyen,
                                  a.TKNHNhan,
                                  a.MaNhanVien
                              } into gr
                              select new MBInternalTransfer_Model
                              {
                                  No = gr.Key.No,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  MaNhanVien = gr.Key.MaNhanVien,
                                  TKNHChuyen = gr.Key.TKNHChuyen,
                                  TKNHNhan = gr.Key.TKNHNhan,
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                              };
            List<MBInternalTransfer> list_MBMaster = new List<MBInternalTransfer>();
            foreach (var item in list_group)
            {
                MBInternalTransfer master = new MBInternalTransfer();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.Reason = item.Reason;
                master.Recorded = true;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.TypeID = 150;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MBInternalTransferDetail> list_mbdt = new List<MBInternalTransferDetail>();
                    foreach (var ct in list_dt)
                    {
                        MBInternalTransferDetail tmpct = new MBInternalTransferDetail();
                        tmpct.DebitAccount = ct.DebitAccount;
                        tmpct.CreditAccount = ct.CreditAccount;
                        var tkchuyen = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == ct.TKNHChuyen);
                        if (tkchuyen != null)
                            tmpct.FromBankAccountDetailID = tkchuyen.ID;
                        var tknhan = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == ct.TKNHNhan);
                        if (tknhan != null)
                            tmpct.ToBankAccountDetailID = tknhan.ID;
                        tmpct.Amount = ct.Amount ?? 0;
                        tmpct.AmountOriginal = ct.AmountOriginal ?? 0;
                        var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == ct.MaNhanVien);
                        if (nhanvien != null)
                            tmpct.EmployeeID = nhanvien.ID;
                        tmpct.Description = item.Reason;
                        list_mbdt.Add(tmpct);
                    }
                    master.MBInternalTransferDetails = list_mbdt;
                    master.TotalAmount = list_mbdt.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_mbdt.Sum(t => t.AmountOriginal);
                }
                list_MBMaster.Add(master);

            }
            data_cv = list_MBMaster.Cast<object>().ToList();
            return data_cv;
        }
        public List<object> SetDataKiẹmkequy(List<object> data)
        {
            List<object> data_cv = new List<object>();
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();//
            IMCAuditService _IMCAuditService = IoC.Resolve<IMCAuditService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();

            List<MCAudit_Model> dt_cv = data.Cast<MCAudit_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.Description = item_first.Description;
                        t.Summary = item_first.Summary;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.Description,
                                 a.Summary,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                             } into gr
                             select new MCAudit_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 Description = gr.Key.Description,
                                 Summary = gr.Key.Summary,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.ValueOfMoney,
                                  a.Description2,
                                  a.MaNhanVien
                              } into gr
                              select new MCAudit_Model
                              {
                                  No = gr.Key.No,
                                  ValueOfMoney = gr.Key.ValueOfMoney,
                                  Description2 = gr.Key.Description2,
                                  MaNhanVien = gr.Key.MaNhanVien,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  Amount = gr.Sum(t => t.Amount)
                              };
            List<MCAudit> list_MBMaster = new List<MCAudit>();
            foreach (var item in list_group)
            {
                MCAudit master = new MCAudit();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.AuditDate = master.Date;
                master.Description = item.Description;
                master.Summary = item.Summary;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate ?? 0;
                master.TypeID = 180;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<MCAuditDetail> list_MBdt = new List<MCAuditDetail>();
                    List<MCAuditDetailMember> list_mbMenber = new List<MCAuditDetailMember>();
                    foreach (var ct in list_dt)
                    {
                        MCAuditDetail tmpct = new MCAuditDetail();
                        tmpct.ValueOfMoney = (int?)ct.ValueOfMoney ?? 0;
                        tmpct.Description = ct.Description2;
                        tmpct.Quantity = (int?)ct.Quantity ?? 0;
                        tmpct.Amount = ct.Amount ?? 0;
                        list_MBdt.Add(tmpct);
                        MCAuditDetailMember tmpmb = new MCAuditDetailMember();
                        var doituong = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == ct.MaNhanVien);
                        if (doituong != null)
                        {
                            tmpmb.AccountingObjectID = doituong.ID;
                            tmpmb.AccountingObjectName = doituong.AccountingObjectName;
                            tmpmb.AccountingObjectTitle = doituong.ContactTitle;
                            tmpmb.DepartmentID = doituong.DepartmentID;
                        }
                        list_mbMenber.Add(tmpmb);
                    }
                    master.MCAuditDetails = list_MBdt;
                    master.MCAuditDetailMembers = list_mbMenber;
                    master.TotalAuditAmount = list_MBdt.Sum(t => t.Amount);
                    var soduquy = _IMCAuditService.SoDuSoQuy(master.AuditDate, master.CurrencyID);
                    master.TotalBalanceAmount = soduquy;
                    master.DifferAmount = master.TotalAuditAmount - soduquy;

                }
                list_MBMaster.Add(master);
            }
            data_cv = list_MBMaster.Cast<object>().ToList();
            return data_cv;
        }
    }
}
