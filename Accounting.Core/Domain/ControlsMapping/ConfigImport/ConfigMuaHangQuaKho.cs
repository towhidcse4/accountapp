﻿using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.ControlsMapping.ConfigImport
{
    public class ConfigMuaHangQuaKho
    {
        public object SetDataMuaHangQuaKho(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            ICreditCardService _ICreditCardService = IoC.Resolve<ICreditCardService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            List<MuaHangQuaKho_Model> dt_cv = data.Cast<MuaHangQuaKho_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.InwardNo).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.InwardNo == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.InwardNo == item_first.InwardNo)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.InwardNo = item_first.InwardNo;
                        t.No = item_first.No;
                        t.MaNCC = item_first.MaNCC;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.OriginalNo = item_first.OriginalNo;
                        t.MaTKHuong = item_first.MaTKHuong;
                        t.MReasonPay = item_first.MReasonPay;
                        t.MaTKChi = item_first.MaTKChi;
                        t.CreditCardNumber = item_first.CreditCardNumber;
                        t.NumberAttach = item_first.NumberAttach;
                        t.IdentificationNo = item_first.IdentificationNo;
                        t.IssueDate = item_first.IssueDate;
                        t.IssueBy = item_first.IssueBy;
                        t.DueDate = item_first.DueDate;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.PhuongThucTT = item_first.PhuongThucTT;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.PhuongThucTT,
                                 a.Date,
                                 a.PostedDate,
                                 a.InwardNo,
                                 a.MaNCC,
                                 a.ContactName,
                                 a.Reason,
                                 a.OriginalNo,
                                 a.MaTKHuong,
                                 a.MReasonPay,
                                 a.MaTKChi,
                                 a.CreditCardNumber,
                                 a.IdentificationNo,
                                 a.IssueDate,
                                 a.IssueBy,
                                 a.DueDate,
                                 a.MaNhanVien,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.InvoiceTemplate,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.InvoiceSeries,
                                 a.NumberAttach,
                             } into gr
                             select new MuaHangQuaKho_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                                 PostedDate = gr.Key.PostedDate,
                                 InwardNo = gr.Key.InwardNo,
                                 MaNCC = gr.Key.MaNCC,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 OriginalNo = gr.Key.OriginalNo,
                                 MaTKHuong = gr.Key.MaTKHuong,
                                 MReasonPay = gr.Key.MReasonPay,
                                 MaTKChi = gr.Key.MaTKChi,
                                 CreditCardNumber = gr.Key.CreditCardNumber,
                                 IdentificationNo = gr.Key.IdentificationNo,
                                 IssueDate = gr.Key.IssueDate,
                                 IssueBy = gr.Key.IssueBy,
                                 DueDate = gr.Key.DueDate,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                                 NumberAttach = gr.Key.NumberAttach,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.PhuongThucTT,
                                  a.MReasonPay,
                                  a.InvoiceTemplate,
                                  a.InvoiceSeries,
                                  a.InvoiceNo,
                                  a.InvoiceDate,
                                  a.MaHang,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.VATAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaKhoanMucCP,
                                  a.VATRate,
                                  a.ImportTaxAccount,
                                  a.PPOrderNo,
                                  a.DeductionDebitAccount,
                                  a.SpecialConsumeTaxRate,
                                  a.SpecialConsumeTaxAccount,
                                  a.FreightAmount,
                                  a.InwardNo,
                                  a.MaNhomHHDV,
                                  a.MaThongKe,
                                  a.DiscountRate
                              } into gr
                              select new MuaHangQuaKho_Model
                              {
                                  No = gr.Key.No,
                                  PhuongThucTT = gr.Key.PhuongThucTT,
                                  MReasonPay = gr.Key.MReasonPay,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InwardNo = gr.Key.InwardNo,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  MaHang = gr.Key.MaHang,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  VATAccount = gr.Key.VATAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  VATRate = gr.Key.VATRate,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  ImportTaxAccount = gr.Key.ImportTaxAccount,
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  DiscountRate = gr.Key.DiscountRate,
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  ImportTaxRate = gr.Sum(t => t.ImportTaxRate),
                                  ImportTaxAmount = gr.Sum(t => t.ImportTaxAmount),
                                  ImportTaxExpenseAmount = gr.Sum(t => t.ImportTaxExpenseAmount),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  InwardAmount = gr.Sum(t => t.InwardAmount),
                                  InwardAmountOriginal = gr.Sum(t => t.InwardAmountOriginal),
                                  SpecialConsumeTaxAmount = gr.Sum(t => t.SpecialConsumeTaxAmount),
                                  SpecialConsumeTaxAmountOriginal = gr.Sum(t => t.SpecialConsumeTaxAmountOriginal),
                                  FreightAmount = gr.Key.FreightAmount,
                                  FreightAmountOriginal = gr.Sum(t => t.FreightAmountOriginal),
                                  ImportTaxExpenseAmountOriginal = gr.Sum(t => t.ImportTaxExpenseAmountOriginal),
                                  PPOrderNo = gr.Key.PPOrderNo,
                                  DeductionDebitAccount = gr.Key.DeductionDebitAccount,
                                  SpecialConsumeTaxRate = gr.Key.SpecialConsumeTaxRate,
                                  SpecialConsumeTaxAccount = gr.Key.SpecialConsumeTaxAccount,
                                  MaNhomHHDV = gr.Key.MaNhomHHDV,
                                  MaThongKe = gr.Key.MaThongKe
                              };
            List<PPInvoice> list_master = new List<PPInvoice>();
            List<RSInwardOutward> list_RSInwardOutward = new List<RSInwardOutward>();
            List<MCPayment> list_MCPayment = new List<MCPayment>();
            List<MBTellerPaper> list_MBTellerPaper = new List<MBTellerPaper>();
            List<MBCreditCard> list_MBCreditCard = new List<MBCreditCard>();
            foreach (var item in list_group)
            {
                PPInvoice master = new PPInvoice();
                master.No = item.No;
                master.InwardNo = item.InwardNo;
                master.MDate = item.Date;
                master.MPostedDate = item.PostedDate;
                master.MContactName = item.ContactName;
                master.MReasonPay = item.MReasonPay;
                master.IdentificationNo = item.IdentificationNo;
                master.IssueDate = item.IssueDate;
                master.IssueBy = item.IssueBy;
                master.BillReceived = false;
                master.TypeID = int.Parse(item.PhuongThucTT);
                master.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == master.TypeID).ID;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OriginalNo = item.OriginalNo;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                }
                var bankdetail = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKChi);
                if (bankdetail != null)
                {
                    master.BankAccountDetailID = bankdetail.ID;
                    master.BankName = bankdetail.BankName;
                }
                var bankdetailacc = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKHuong);
                if (bankdetailacc != null)
                {
                    master.MAccountingObjectBankAccountDetailID = bankdetailacc.ID;
                    master.MAccountingObjectBankName = bankdetailacc.BankName;
                }
                master.CreditCardNumber = item.CreditCardNumber;
                master.NumberAttach = item.NumberAttach;
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.RefDateTime = DateTime.Now;
                var list_dt = list_detail.Where(t => t.InwardNo == item.InwardNo);
                if (list_dt.Count() != 0)
                {
                    List<PPInvoiceDetail> list_sadetail = new List<PPInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        PPInvoiceDetail saitem = new PPInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            saitem.MaterialGoodsID = hanghoa.ID;
                            saitem.RepositoryID = hanghoa.RepositoryID;
                            saitem.Description = hanghoa.MaterialGoodsName;
                            saitem.Unit = hanghoa.Unit;
                        }
                        saitem.DebitAccount = ct.DebitAccount;
                        saitem.CreditAccount = ct.CreditAccount;
                        saitem.Quantity = ct.Quantity;
                        saitem.UnitPrice = ct.UnitPrice ?? 0;
                        saitem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        saitem.Amount = ct.Amount ?? 0;
                        saitem.AmountOriginal = ct.AmountOriginal ?? 0;
                        saitem.DiscountRate = ct.DiscountRate;
                        saitem.DiscountAmount = ct.DiscountAmount ?? 0;
                        saitem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        saitem.VATRate = ct.VATRate;
                        saitem.VATAmount = ct.VATAmount ?? 0;
                        saitem.VATAccount = ct.VATAccount;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.InvoiceNo = ct.InvoiceNo;
                        saitem.InvoiceDate = ct.InvoiceDate;
                        saitem.InvoiceSeries = ct.InvoiceSeries;
                        saitem.InvoiceTemplate = ct.InvoiceTemplate;
                        saitem.DeductionDebitAccount = ct.DeductionDebitAccount;
                        saitem.SpecialConsumeTaxAccount = ct.SpecialConsumeTaxAccount;
                        saitem.PPOrderNo = ct.PPOrderNo;
                        saitem.SpecialConsumeTaxRate = ct.SpecialConsumeTaxRate ?? 0;
                        if (khachhang != null)
                            saitem.AccountingObjectID = khachhang.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            saitem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            saitem.ContractID = hopdong.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            saitem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            saitem.BudgetItemID = mucthuchi.ID;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == ct.MaNhomHHDV);
                        if (nhomhhdv != null)
                            saitem.GoodsServicePurchaseID = nhomhhdv.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            saitem.StatisticsCodeID = thongke.ID;
                        var ncc = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                        if (ncc != null)
                        {
                            saitem.CompanyTaxCode = ncc.TaxCode;
                            saitem.AccountingObjectTaxID = ncc.ID;
                            saitem.AccountingObjectTaxName = ncc.AccountingObjectName;
                        }
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.TaxExchangeRate = master.ExchangeRate ?? 0;
                        saitem.ImportTaxRate = ct.ImportTaxRate ?? 0;
                        saitem.ImportTaxAmount = ct.ImportTaxAmount ?? 0;
                        saitem.ImportTaxAccount = ct.ImportTaxAccount;
                        saitem.VATDescription = ct.VATDescription;
                        saitem.InwardAmountOriginal = ct.InwardAmountOriginal ?? 0;
                        saitem.InwardAmount = (ct.InwardAmountOriginal ?? 0) * (master.ExchangeRate ?? 1);
                        saitem.SpecialConsumeTaxAmount = ct.SpecialConsumeTaxAmount ?? 0;
                        saitem.SpecialConsumeTaxAmountOriginal = ((ct.Amount ?? 0) * (ct.SpecialConsumeTaxRate ?? 0)) / 100;
                        saitem.FreightAmountOriginal = ct.FreightAmountOriginal ?? 0;
                        saitem.FreightAmount = ((ct.FreightAmountOriginal ?? 0) * (master.ExchangeRate ?? 0));
                        saitem.ImportTaxExpenseAmount = ct.ImportTaxExpenseAmount ?? 0;
                        saitem.ImportTaxExpenseAmountOriginal = (ct.ImportTaxExpenseAmount ?? 0) / (master.ExchangeRate ?? 1);
                        saitem.ImportTaxAmountOriginal = ((ct.AmountOriginal ?? 0) * (ct.ImportTaxRate ?? 0)) / 100;
                        saitem.VATAmountOriginal = ((ct.AmountOriginal ?? 0) * (ct.VATRate ?? 0)) / 100;
                        list_sadetail.Add(saitem);
                    }
                    master.PPInvoiceDetails = list_sadetail;
                    master.TotalAmount = list_sadetail.Sum(t => t.Amount);
                    master.TotalImportTaxAmount = list_sadetail.Sum(t => t.ImportTaxAmount);
                    master.TotalAmountOriginal = list_sadetail.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadetail.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadetail.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadetail.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadetail.Sum(t => t.VATAmountOriginal);
                    master.TotalImportTaxAmountOriginal = list_sadetail.Sum(t => t.ImportTaxAmountOriginal);
                    master.TotalInwardAmount = list_sadetail.Sum(t => t.InwardAmount);
                    master.TotalInwardAmountOriginal = list_sadetail.Sum(t => t.InwardAmountOriginal);
                    master.TotalSpecialConsumeTaxAmount = list_sadetail.Sum(t => t.SpecialConsumeTaxAmount);
                    master.TotalSpecialConsumeTaxAmountOriginal = list_sadetail.Sum(t => t.SpecialConsumeTaxAmountOriginal);
                    master.TotalFreightAmount = list_sadetail.Sum(t => t.FreightAmount);
                    master.TotalFreightAmountOriginal = list_sadetail.Sum(t => t.FreightAmountOriginal);
                    master.TotalImportTaxExpenseAmount = list_sadetail.Sum(t => t.ImportTaxExpenseAmount);
                    master.TotalImportTaxExpenseAmountOriginal = list_sadetail.Sum(t => t.ImportTaxExpenseAmountOriginal);
                    master.StoredInRepository = true;
                    list_master.Add(master);
                    //Lưu thêm bảng RSInwardOutward
                    RSInwardOutward _RSInwardOutward = new RSInwardOutward();
                    _RSInwardOutward.TypeID = 402;
                    _RSInwardOutward.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == 402).ID;
                    _RSInwardOutward.No = master.InwardNo;
                    _RSInwardOutward.Date = master.Date;
                    _RSInwardOutward.PostedDate = master.PostedDate;
                    _RSInwardOutward.AccountingObjectID = master.AccountingObjectID;
                    _RSInwardOutward.AccountingObjectName = master.AccountingObjectName;
                    _RSInwardOutward.AccountingObjectAddress = master.AccountingObjectAddress;
                    _RSInwardOutward.ContactName = master.ContactName;
                    _RSInwardOutward.EmployeeID = master.EmployeeID;
                    _RSInwardOutward.Reason = master.Reason;
                    _RSInwardOutward.CurrencyID = master.CurrencyID;
                    _RSInwardOutward.ExchangeRate = master.ExchangeRate;
                    _RSInwardOutward.Recorded = false;
                    _RSInwardOutward.Exported = master.Exported;
                    _RSInwardOutward.TotalAmount = master.TotalAmount;
                    _RSInwardOutward.TotalAmountOriginal = master.TotalAmountOriginal;
                    _RSInwardOutward.TotalAmount = master.TotalAmount;
                    _RSInwardOutward.TotalAmountOriginal = master.TotalAmountOriginal;
                    list_RSInwardOutward.Add(_RSInwardOutward);
                    if (item.PhuongThucTT == "260")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MCPayment payment = new MCPayment();
                        payment.TypeID = 117;
                        payment.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == payment.TypeID).ID;
                        payment.No = master.No;
                        payment.Date = master.Date;
                        payment.PostedDate = master.PostedDate;
                        payment.AccountingObjectID = master.AccountingObjectID;
                        payment.AccountingObjectName = master.AccountingObjectName;
                        payment.AccountingObjectAddress = master.AccountingObjectAddress;
                        payment.AccountingObjectType = khachhang.ObjectType ?? 0;
                        payment.Receiver = master.MContactName;
                        payment.Reason = master.Reason;
                        payment.NumberAttach = master.NumberAttach;
                        payment.CurrencyID = master.CurrencyID;
                        payment.ExchangeRate = master.ExchangeRate;
                        payment.PaymentClauseID = master.PaymentClauseID;
                        payment.EmployeeID = master.EmployeeID;
                        payment.Recorded = false;
                        payment.Exported = master.Exported;
                        payment.TotalAll = master.TotalAmountOriginal;
                        payment.TotalAllOriginal = master.TotalAmount;
                        payment.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        payment.TotalAmount = master.TotalAmount;
                        payment.TotalAmountOriginal = master.TotalAmountOriginal;
                        payment.TotalVATAmount = master.TotalVATAmount;
                        if (khachhang != null)
                            payment.AccountingObjectType = khachhang.ObjectType;
                        list_MCPayment.Add(payment);
                    }
                    if (item.PhuongThucTT == "261" || item.PhuongThucTT == "262" || item.PhuongThucTT == "264")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBTellerPaper TellerPaper = new MBTellerPaper();
                        TellerPaper.TypeID = item.PhuongThucTT == "261" ? 127 : item.PhuongThucTT == "262" ? 131 : 141;
                        TellerPaper.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == TellerPaper.TypeID).ID;
                        TellerPaper.No = master.No;
                        TellerPaper.Date = master.Date;
                        TellerPaper.PostedDate = master.PostedDate;
                        TellerPaper.AccountingObjectID = master.AccountingObjectID;
                        TellerPaper.AccountingObjectName = master.AccountingObjectName;
                        TellerPaper.AccountingObjectAddress = master.AccountingObjectAddress;
                        TellerPaper.AccountingObjectType = khachhang.ObjectType ?? 0;
                        TellerPaper.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID ?? null;
                        TellerPaper.AccountingObjectBankName = master.MAccountingObjectBankName;
                        TellerPaper.BankAccountDetailID = master.BankAccountDetailID;
                        TellerPaper.Receiver = master.MContactName;
                        TellerPaper.BankName = master.BankName;
                        TellerPaper.Reason = master.Reason;
                        TellerPaper.CurrencyID = master.CurrencyID;
                        TellerPaper.ExchangeRate = master.ExchangeRate;
                        TellerPaper.PaymentClauseID = master.PaymentClauseID;
                        //TellerPaper.TotalAmount = master.TotalAmount;
                        TellerPaper.TotalAmountOriginal = master.TotalAmountOriginal;
                        TellerPaper.TotalVATAmount = master.TotalVATAmount;
                        TellerPaper.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        TellerPaper.EmployeeID = master.EmployeeID;
                        TellerPaper.Recorded = false;
                        TellerPaper.Exported = master.Exported;
                        TellerPaper.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        TellerPaper.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        TellerPaper.TotalVATAmount = master.TotalVATAmount;
                        if (khachhang != null)
                            TellerPaper.AccountingObjectType = khachhang.ObjectType ?? 0;
                        list_MBTellerPaper.Add(TellerPaper);
                    }
                    if (item.PhuongThucTT == "263")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBCreditCard CreditCard = new MBCreditCard();
                        CreditCard.TypeID = 171;
                        CreditCard.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == CreditCard.TypeID).ID;
                        CreditCard.No = master.No;
                        CreditCard.Date = master.Date;
                        CreditCard.PostedDate = master.PostedDate;
                        CreditCard.AccountingObjectID = master.AccountingObjectID;
                        CreditCard.AccountingObjectName = master.AccountingObjectName;
                        CreditCard.AccountingObjectAddress = master.AccountingObjectAddress;
                        CreditCard.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID;
                        CreditCard.AccountingObjectType = khachhang.ObjectType ?? 0;
                        CreditCard.CreditCardNumber = master.CreditCardNumber;
                        CreditCard.Reason = master.Reason;
                        CreditCard.CurrencyID = master.CurrencyID;
                        CreditCard.ExchangeRate = master.ExchangeRate;
                        CreditCard.PaymentClauseID = master.PaymentClauseID;
                        CreditCard.TotalAmount = master.TotalAmount;
                        CreditCard.TotalAmountOriginal = master.TotalAmountOriginal;
                        CreditCard.TotalVATAmount = master.TotalVATAmount;
                        CreditCard.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        CreditCard.EmployeeID = master.EmployeeID;
                        CreditCard.Recorded = false;
                        CreditCard.Exported = master.Exported;
                        CreditCard.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        CreditCard.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        CreditCard.TotalVATAmount = master.TotalVATAmount;
                        list_MBCreditCard.Add(CreditCard);
                    }
                }
            }
            MH_QuaKho_model model = new MH_QuaKho_model();
            model.listPPInvoice = list_master;
            model.listRSInwardOutward = list_RSInwardOutward;
            model.listMCPayment = list_MCPayment;
            model.listMBTellerPaper = list_MBTellerPaper;
            model.listMBCreditCard = list_MBCreditCard;
            return model;
        }
    }
}
