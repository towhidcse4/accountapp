﻿using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.ControlsMapping.ConfigImport
{
    public class ConfigMuaHang
    {
        public List<object> SetDataDonMuaHang(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();//
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            List<object> data_cv = new List<object>();
            List<PPOrder_Model> dt_cv = data.Cast<PPOrder_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu) //Set lại giá trị theo số đơn hàng.
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item); //Lấy giá trị đầu tiên tìm đc theo Số đơn hàng
                dem++;
                dt_cv.ForEach(t => //Gán lại dữ liệu chi tiết. theo dữ liệu đầu tiên lấy đc theo số đơn hàng
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.MaDoiTuong = item_first.MaDoiTuong;
                        t.Reason = item_first.Reason;
                        t.DeliverDate = item_first.DeliverDate;
                        t.ShippingPlace = item_first.ShippingPlace;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                    }
                });
            }
            var list_group = from a in dt_cv //Group theo Số đơn hàng, ngày đơn hàng ... để làm list Master
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.MaDoiTuong,
                                 a.Reason,
                                 a.DeliverDate,
                                 a.ShippingPlace,
                                 a.MaNhanVien,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                             } into gr
                             select new PPOrder_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 MaDoiTuong = gr.Key.MaDoiTuong,
                                 Reason = gr.Key.Reason,
                                 DeliverDate = gr.Key.DeliverDate,
                                 ShippingPlace = gr.Key.ShippingPlace,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                             };
            var list_detail = from a in dt_cv //Group theo số đơn hàng, mã hàng, diễn giải thuế, mã phòng ban, khoản mục CP, mã hợp đồng, mã thống kê, Unit(Đơn vị tính)
                              group a by new
                              {
                                  a.No,
                                  a.MaHang,
                                  a.VATDescription,
                                  a.MaPhongBan,
                                  a.MaMucThuChi,
                                  a.MaKhoanMucCP,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaThongKe,
                              } into gr
                              select new PPOrder_Model
                              {
                                  No = gr.Key.No,
                                  MaHang = gr.Key.MaHang,
                                  VATDescription = gr.Key.VATDescription,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaThongKe = gr.Key.MaThongKe,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  VATRate = gr.Sum(t => t.VATRate),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                              };
            List<PPOrder> list_master = new List<PPOrder>();
            foreach (var item in list_group)
            {
                PPOrder master = new PPOrder();
                master.TypeID = 200;
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaDoiTuong);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectAddress = khachhang.Address;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.CompanyTaxCode = khachhang.TaxCode;
                    master.ContactName = khachhang.ContactName;
                }
                master.Reason = item.Reason;
                master.DeliverDate = item.DeliverDate;
                master.ShippingPlace = item.ShippingPlace;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<PPOrderDetail> list_sadt = new List<PPOrderDetail>();
                    foreach (var ct in list_dt)
                    {
                        PPOrderDetail ctiem = new PPOrderDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            ctiem.MaterialGoodsID = hanghoa.ID;
                            ctiem.Description = hanghoa.MaterialGoodsName;
                            ctiem.Unit = hanghoa.Unit;
                        }
                        ctiem.Quantity = ct.Quantity;
                        ctiem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        ctiem.Amount = ct.Amount ?? 0;
                        ctiem.AmountOriginal = ct.AmountOriginal ?? 0;
                        ctiem.VATRate = ct.VATRate ?? 0;
                        ctiem.VATAmount = ct.VATAmount ?? 0;
                        ctiem.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                        ctiem.DiscountRate = ct.DiscountRate ?? 0;
                        ctiem.DiscountAmount = ct.DiscountAmount ?? 0;
                        ctiem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        if (khachhang != null)
                            ctiem.AccountingObjectID = khachhang.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            ctiem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            ctiem.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            ctiem.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            ctiem.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            ctiem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            ctiem.BudgetItemID = mucthuchi.ID;
                        ctiem.VATDescription = ct.VATDescription;
                        list_sadt.Add(ctiem);
                    }
                    master.PPOrderDetails = list_sadt;
                    master.TotalAmount = list_sadt.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_sadt.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadt.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadt.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadt.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadt.Sum(t => t.VATAmountOriginal);
                }
                list_master.Add(master);
            }
            data_cv = list_master.Cast<object>().ToList();
            return data_cv;
        }
        public object SetDataMuaHangQuaKho(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            ICreditCardService _ICreditCardService = IoC.Resolve<ICreditCardService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            List<MuaHangQuaKho_Model> dt_cv = data.Cast<MuaHangQuaKho_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.InwardNo).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.InwardNo == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.InwardNo == item_first.InwardNo)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.InwardNo = item_first.InwardNo;
                        t.No = item_first.No;
                        t.MaNCC = item_first.MaNCC;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.OriginalNo = item_first.OriginalNo;
                        t.MaTKHuong = item_first.MaTKHuong;
                        t.MReasonPay = item_first.MReasonPay;
                        t.MaTKChi = item_first.MaTKChi;
                        t.CreditCardNumber = item_first.CreditCardNumber;
                        t.NumberAttach = item_first.NumberAttach;
                        t.IdentificationNo = item_first.IdentificationNo;
                        t.IssueDate = item_first.IssueDate;
                        t.IssueBy = item_first.IssueBy;
                        t.DueDate = item_first.DueDate;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.PhuongThucTT = item_first.PhuongThucTT;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.PhuongThucTT,
                                 a.Date,
                                 a.PostedDate,
                                 a.InwardNo,
                                 a.MaNCC,
                                 a.ContactName,
                                 a.Reason,
                                 a.OriginalNo,
                                 a.MaTKHuong,
                                 a.MReasonPay,
                                 a.MaTKChi,
                                 a.CreditCardNumber,
                                 a.IdentificationNo,
                                 a.IssueDate,
                                 a.IssueBy,
                                 a.DueDate,
                                 a.MaNhanVien,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.InvoiceTemplate,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.InvoiceSeries,
                                 a.NumberAttach,
                             } into gr
                             select new MuaHangQuaKho_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                                 PostedDate = gr.Key.PostedDate,
                                 InwardNo = gr.Key.InwardNo,
                                 MaNCC = gr.Key.MaNCC,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 OriginalNo = gr.Key.OriginalNo,
                                 MaTKHuong = gr.Key.MaTKHuong,
                                 MReasonPay = gr.Key.MReasonPay,
                                 MaTKChi = gr.Key.MaTKChi,
                                 CreditCardNumber = gr.Key.CreditCardNumber,
                                 IdentificationNo = gr.Key.IdentificationNo,
                                 IssueDate = gr.Key.IssueDate,
                                 IssueBy = gr.Key.IssueBy,
                                 DueDate = gr.Key.DueDate,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                                 NumberAttach = gr.Key.NumberAttach,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.PhuongThucTT,
                                  a.MReasonPay,
                                  a.InvoiceTemplate,
                                  a.InvoiceSeries,
                                  a.InvoiceNo,
                                  a.InvoiceDate,
                                  a.MaHang,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.VATAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaKhoanMucCP,
                                  a.VATRate,
                                  a.ImportTaxAccount,
                                  a.PPOrderNo,
                                  a.DeductionDebitAccount,
                                  a.SpecialConsumeTaxRate,
                                  a.SpecialConsumeTaxAccount,
                              } into gr
                              select new MuaHangKhongQuaKho_Model
                              {
                                  No = gr.Key.No,
                                  PhuongThucTT = gr.Key.PhuongThucTT,
                                  MReasonPay = gr.Key.MReasonPay,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  MaHang = gr.Key.MaHang,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  VATAccount = gr.Key.VATAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  VATRate = gr.Key.VATRate,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  ImportTaxAccount = gr.Key.ImportTaxAccount,
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  ImportTaxRate = gr.Sum(t => t.ImportTaxRate),
                                  ImportTaxAmount = gr.Sum(t => t.ImportTaxAmount),
                                  ImportTaxExpenseAmount = gr.Sum(t => t.ImportTaxExpenseAmount),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  InwardAmount = gr.Sum(t => t.InwardAmount),
                                  InwardAmountOriginal = gr.Sum(t => t.InwardAmountOriginal),
                                  SpecialConsumeTaxAmount = gr.Sum(t => t.SpecialConsumeTaxAmount),
                                  SpecialConsumeTaxAmountOriginal = gr.Sum(t => t.SpecialConsumeTaxAmountOriginal),
                                  FreightAmount = gr.Sum(t => t.FreightAmount),
                                  FreightAmountOriginal = gr.Sum(t => t.FreightAmountOriginal),
                                  ImportTaxExpenseAmountOriginal = gr.Sum(t => t.ImportTaxExpenseAmountOriginal),
                                  PPOrderNo = gr.Key.PPOrderNo,
                                  DeductionDebitAccount = gr.Key.DeductionDebitAccount,
                                  SpecialConsumeTaxRate = gr.Key.SpecialConsumeTaxRate,
                                  SpecialConsumeTaxAccount = gr.Key.SpecialConsumeTaxAccount,
                              };
            List<PPInvoice> list_master = new List<PPInvoice>();
            List<RSInwardOutward> list_RSInwardOutward = new List<RSInwardOutward>();
            List<MCPayment> list_MCPayment = new List<MCPayment>();
            List<MBTellerPaper> list_MBTellerPaper = new List<MBTellerPaper>();
            List<MBCreditCard> list_MBCreditCard = new List<MBCreditCard>();
            foreach (var item in list_group)
            {
                PPInvoice master = new PPInvoice();
                master.No = item.No;
                master.InwardNo = item.InwardNo;
                master.MDate = item.Date;
                master.MPostedDate = item.PostedDate;
                master.MContactName = item.ContactName;
                master.MReasonPay = item.MReasonPay;
                master.IdentificationNo = item.IdentificationNo;
                master.IssueDate = item.IssueDate;
                master.IssueBy = item.IssueBy;
                master.BillReceived = false;
                master.TypeID = int.Parse(item.PhuongThucTT);
                master.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == master.TypeID).ID;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OriginalNo = item.OriginalNo;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                }
                var bankdetail = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKChi);
                if (bankdetail != null)
                {
                    master.BankAccountDetailID = bankdetail.ID;
                    master.BankName = bankdetail.BankName;
                }
                var bankdetailacc = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKHuong);
                if (bankdetailacc != null)
                {
                    master.MAccountingObjectBankAccountDetailID = bankdetailacc.ID;
                    master.MAccountingObjectBankName = bankdetailacc.BankName;
                }
                master.CreditCardNumber = item.CreditCardNumber;
                master.NumberAttach = item.NumberAttach;
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.RefDateTime = DateTime.Now;
                var list_dt = list_detail.Where(t => t.InwardNo == item.InwardNo);
                if (list_dt.Count() != 0)
                {
                    List<PPInvoiceDetail> list_sadetail = new List<PPInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        PPInvoiceDetail saitem = new PPInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            saitem.MaterialGoodsID = hanghoa.ID;
                            saitem.RepositoryID = hanghoa.RepositoryID;
                            saitem.Description = hanghoa.MaterialGoodsName;
                            saitem.Unit = hanghoa.Unit;
                        }
                        saitem.DebitAccount = ct.DebitAccount;
                        saitem.CreditAccount = ct.CreditAccount;
                        saitem.Quantity = ct.Quantity;
                        saitem.UnitPrice = ct.UnitPrice ?? 0;
                        saitem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        saitem.Amount = ct.Amount ?? 0;
                        saitem.AmountOriginal = ct.AmountOriginal ?? 0;
                        saitem.DiscountRate = ct.DiscountRate;
                        saitem.DiscountAmount = ct.DiscountAmount ?? 0;
                        saitem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        saitem.VATRate = ct.VATRate;
                        saitem.VATAmount = ct.VATAmount ?? 0;
                        saitem.VATAccount = ct.VATAccount;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.InvoiceNo = ct.InvoiceNo;
                        saitem.InvoiceDate = ct.InvoiceDate;
                        saitem.InvoiceSeries = ct.InvoiceSeries;
                        saitem.InvoiceTemplate = ct.InvoiceTemplate;
                        saitem.DeductionDebitAccount = ct.DeductionDebitAccount;
                        saitem.SpecialConsumeTaxAccount = ct.SpecialConsumeTaxAccount;
                        saitem.PPOrderNo = ct.PPOrderNo;
                        saitem.SpecialConsumeTaxRate = ct.SpecialConsumeTaxRate ?? 0;
                        if (khachhang != null)
                            saitem.AccountingObjectID = khachhang.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            saitem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            saitem.ContractID = hopdong.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            saitem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            saitem.BudgetItemID = mucthuchi.ID;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == ct.MaNhomHHDV);
                        if (nhomhhdv != null)
                            saitem.GoodsServicePurchaseID = nhomhhdv.ID;
                        var ncc = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                        if (ncc != null)
                        {
                            saitem.CompanyTaxCode = ncc.TaxCode;
                            saitem.AccountingObjectTaxID = ncc.ID;
                            saitem.AccountingObjectTaxName = ncc.AccountingObjectName;
                        }
                        saitem.TaxExchangeRate = master.ExchangeRate ?? 0;
                        saitem.ImportTaxRate = ct.ImportTaxRate ?? 0;
                        saitem.ImportTaxAmount = ct.ImportTaxAmount ?? 0;
                        saitem.ImportTaxAccount = ct.ImportTaxAccount;
                        saitem.VATDescription = ct.VATDescription;
                        saitem.InwardAmount = ct.InwardAmount ?? 0;
                        saitem.InwardAmountOriginal = ct.InwardAmountOriginal ?? 0;
                        saitem.SpecialConsumeTaxAmount = ct.SpecialConsumeTaxAmount ?? 0;
                        saitem.SpecialConsumeTaxAmountOriginal = ct.SpecialConsumeTaxAmountOriginal ?? 0;
                        saitem.FreightAmount = ct.FreightAmount ?? 0;
                        saitem.FreightAmountOriginal = ct.FreightAmountOriginal ?? 0;
                        saitem.ImportTaxExpenseAmountOriginal = ct.ImportTaxExpenseAmountOriginal ?? 0;
                        saitem.ImportTaxAmountOriginal = (ct.AmountOriginal * ct.ImportTaxRate ?? 0) / 100;
                        saitem.VATAmountOriginal = (ct.AmountOriginal * ct.VATRate ?? 0) / 100;
                        list_sadetail.Add(saitem);
                    }
                    master.PPInvoiceDetails = list_sadetail;
                    master.TotalAmount = list_sadetail.Sum(t => t.Amount);
                    master.TotalImportTaxAmount = list_sadetail.Sum(t => t.ImportTaxAmount);
                    master.TotalAmountOriginal = list_sadetail.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadetail.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadetail.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadetail.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadetail.Sum(t => t.VATAmountOriginal);
                    master.TotalImportTaxAmountOriginal = list_sadetail.Sum(t => t.ImportTaxAmountOriginal);
                    master.TotalInwardAmount = list_sadetail.Sum(t => t.InwardAmount);
                    master.TotalInwardAmountOriginal = list_sadetail.Sum(t => t.InwardAmountOriginal);
                    master.TotalSpecialConsumeTaxAmount = list_detail.Sum(t => t.SpecialConsumeTaxAmount);
                    master.TotalSpecialConsumeTaxAmountOriginal = list_detail.Sum(t => t.SpecialConsumeTaxAmountOriginal);
                    master.TotalFreightAmount = list_detail.Sum(t => t.FreightAmount);
                    master.TotalFreightAmountOriginal = list_detail.Sum(t => t.FreightAmountOriginal);
                    master.TotalImportTaxExpenseAmount = list_detail.Sum(t => t.ImportTaxExpenseAmount);
                    master.TotalImportTaxExpenseAmountOriginal = list_detail.Sum(t => t.ImportTaxExpenseAmountOriginal);
                    master.StoredInRepository = true;
                    list_master.Add(master);
                    //Lưu thêm bảng RSInwardOutward
                    RSInwardOutward _RSInwardOutward = new RSInwardOutward();
                    _RSInwardOutward.TypeID = 402;
                    _RSInwardOutward.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == 402).ID;
                    _RSInwardOutward.No = master.InwardNo;
                    _RSInwardOutward.Date = master.Date;
                    _RSInwardOutward.PostedDate = master.PostedDate;
                    _RSInwardOutward.AccountingObjectID = master.AccountingObjectID;
                    _RSInwardOutward.AccountingObjectName = master.AccountingObjectName;
                    _RSInwardOutward.AccountingObjectAddress = master.AccountingObjectAddress;
                    _RSInwardOutward.ContactName = master.ContactName;
                    _RSInwardOutward.EmployeeID = master.EmployeeID;
                    _RSInwardOutward.Reason = master.Reason;
                    _RSInwardOutward.CurrencyID = master.CurrencyID;
                    _RSInwardOutward.ExchangeRate = master.ExchangeRate;
                    _RSInwardOutward.Recorded = false;
                    _RSInwardOutward.Exported = master.Exported;
                    _RSInwardOutward.TotalAmount = master.TotalAmount;
                    _RSInwardOutward.TotalAmountOriginal = master.TotalAmountOriginal;
                    _RSInwardOutward.TotalAmount = master.TotalAmount;
                    _RSInwardOutward.TotalAmountOriginal = master.TotalAmountOriginal;
                    list_RSInwardOutward.Add(_RSInwardOutward);
                    if (item.PhuongThucTT == "260")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MCPayment payment = new MCPayment();
                        payment.TypeID = 117;
                        payment.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == payment.TypeID).ID;
                        payment.No = master.No;
                        payment.Date = master.Date;
                        payment.PostedDate = master.PostedDate;
                        payment.AccountingObjectID = master.AccountingObjectID;
                        payment.AccountingObjectName = master.AccountingObjectName;
                        payment.AccountingObjectAddress = master.AccountingObjectAddress;
                        payment.AccountingObjectType = khachhang.ObjectType ?? 0;
                        payment.Receiver = master.MContactName;
                        payment.Reason = master.Reason;
                        payment.NumberAttach = master.NumberAttach;
                        payment.CurrencyID = master.CurrencyID;
                        payment.ExchangeRate = master.ExchangeRate;
                        payment.PaymentClauseID = master.PaymentClauseID;
                        payment.EmployeeID = master.EmployeeID;
                        payment.Recorded = false;
                        payment.Exported = master.Exported;
                        payment.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        payment.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        payment.TotalVATAmountOriginal = master.TotalAmountOriginal * master.TotalVATAmount;
                        payment.TotalAmount = master.TotalAmount;
                        payment.TotalAmountOriginal = master.TotalAmountOriginal;
                        payment.TotalVATAmount = master.TotalVATAmount;
                        list_MCPayment.Add(payment);
                    }
                    if (item.PhuongThucTT == "261" || item.PhuongThucTT == "262" || item.PhuongThucTT == "264")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBTellerPaper TellerPaper = new MBTellerPaper();
                        TellerPaper.TypeID = item.PhuongThucTT == "261" ? 127 : item.PhuongThucTT == "262" ? 131 : 141;
                        TellerPaper.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == TellerPaper.TypeID).ID;
                        TellerPaper.No = master.No;
                        TellerPaper.Date = master.Date;
                        TellerPaper.PostedDate = master.PostedDate;
                        TellerPaper.AccountingObjectID = master.AccountingObjectID;
                        TellerPaper.AccountingObjectName = master.AccountingObjectName;
                        TellerPaper.AccountingObjectAddress = master.AccountingObjectAddress;
                        TellerPaper.AccountingObjectType = khachhang.ObjectType ?? 0;
                        TellerPaper.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID ?? null;
                        TellerPaper.AccountingObjectBankName = master.MAccountingObjectBankName;
                        TellerPaper.BankAccountDetailID = master.BankAccountDetailID;
                        TellerPaper.Receiver = master.MContactName;
                        TellerPaper.BankName = master.BankName;
                        TellerPaper.Reason = master.Reason;
                        TellerPaper.CurrencyID = master.CurrencyID;
                        TellerPaper.ExchangeRate = master.ExchangeRate;
                        TellerPaper.PaymentClauseID = master.PaymentClauseID;
                        TellerPaper.TotalAmount = master.TotalAmount;
                        TellerPaper.TotalAmountOriginal = master.TotalAmountOriginal;
                        TellerPaper.TotalVATAmount = master.TotalVATAmount;
                        TellerPaper.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        TellerPaper.EmployeeID = master.EmployeeID;
                        TellerPaper.Recorded = false;
                        TellerPaper.Exported = master.Exported;
                        TellerPaper.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        TellerPaper.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        TellerPaper.TotalVATAmountOriginal = master.TotalAmountOriginal * master.TotalVATAmount;
                        TellerPaper.TotalVATAmount = master.TotalVATAmount;
                        list_MBTellerPaper.Add(TellerPaper);
                    }
                    if (item.PhuongThucTT == "263")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBCreditCard CreditCard = new MBCreditCard();
                        CreditCard.TypeID = 171;
                        CreditCard.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == CreditCard.TypeID).ID;
                        CreditCard.No = master.No;
                        CreditCard.Date = master.Date;
                        CreditCard.PostedDate = master.PostedDate;
                        CreditCard.AccountingObjectID = master.AccountingObjectID;
                        CreditCard.AccountingObjectName = master.AccountingObjectName;
                        CreditCard.AccountingObjectAddress = master.AccountingObjectAddress;
                        CreditCard.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID;
                        CreditCard.AccountingObjectType = khachhang.ObjectType ?? 0;
                        CreditCard.CreditCardNumber = master.CreditCardNumber;
                        CreditCard.Reason = master.Reason;
                        CreditCard.CurrencyID = master.CurrencyID;
                        CreditCard.ExchangeRate = master.ExchangeRate;
                        CreditCard.PaymentClauseID = master.PaymentClauseID;
                        CreditCard.TotalAmount = master.TotalAmount;
                        CreditCard.TotalAmountOriginal = master.TotalAmountOriginal;
                        CreditCard.TotalVATAmount = master.TotalVATAmount;
                        CreditCard.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        CreditCard.EmployeeID = master.EmployeeID;
                        CreditCard.Recorded = false;
                        CreditCard.Exported = master.Exported;
                        CreditCard.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        CreditCard.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        CreditCard.TotalVATAmount = master.TotalVATAmount;
                        CreditCard.TotalVATAmountOriginal = master.TotalAmountOriginal * master.TotalVATAmount;
                        list_MBCreditCard.Add(CreditCard);
                    }
                }
            }
            MH_QuaKho_model model = new MH_QuaKho_model();
            model.listPPInvoice = list_master;
            model.listRSInwardOutward = list_RSInwardOutward;
            model.listMCPayment = list_MCPayment;
            model.listMBTellerPaper = list_MBTellerPaper;
            model.listMBCreditCard = list_MBCreditCard;
            return model;
        }
        public object SetDataMuaHangKhongQuaKho(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            List<MuaHangKhongQuaKho_Model> dt_cv = data.Cast<MuaHangKhongQuaKho_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.MDate = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaNCC = item_first.MaNCC;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.MReasonPay = item_first.MReasonPay;
                        t.OriginalNo = item_first.OriginalNo;
                        t.MPostedDate = item_first.MPostedDate;
                        t.MDate = item_first.MDate;
                        t.SoTKHuong = item_first.SoTKHuong;
                        t.SoTKChi = item_first.SoTKChi;
                        t.CreditCardNumber = item_first.CreditCardNumber;
                        t.IdentificationNo = item_first.IdentificationNo;
                        t.IssueDate = item_first.IssueDate;
                        t.IssueBy = item_first.IssueBy;
                        t.DueDate = item_first.DueDate;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.PhuongThucTT = item_first.PhuongThucTT;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                        t.NumberAttach = item_first.NumberAttach;
                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaNCC,
                                 a.ContactName,
                                 a.Reason,
                                 a.MReasonPay,
                                 a.OriginalNo,
                                 a.MPostedDate,
                                 a.MDate,
                                 a.SoTKHuong,
                                 a.SoTKChi,
                                 a.CreditCardNumber,
                                 a.NumberAttach,
                                 a.IdentificationNo,
                                 a.IssueDate,
                                 a.IssueBy,
                                 a.DueDate,
                                 a.MaNhanVien,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.PhuongThucTT,
                                 a.InvoiceTemplate,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.InvoiceSeries,
                             } into gr
                             select new MuaHangKhongQuaKho_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaNCC = gr.Key.MaNCC,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 MReasonPay = gr.Key.MReasonPay,
                                 OriginalNo = gr.Key.OriginalNo,
                                 MPostedDate = gr.Key.MPostedDate,
                                 MDate = gr.Key.MDate,
                                 SoTKHuong = gr.Key.SoTKHuong,
                                 SoTKChi = gr.Key.SoTKChi,
                                 CreditCardNumber = gr.Key.CreditCardNumber,
                                 NumberAttach = gr.Key.NumberAttach,
                                 IdentificationNo = gr.Key.IdentificationNo,
                                 IssueDate = gr.Key.IssueDate,
                                 IssueBy = gr.Key.IssueBy,
                                 DueDate = gr.Key.DueDate,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.Date,
                                  a.PhuongThucTT,
                                  a.InvoiceTemplate,
                                  a.InvoiceSeries,
                                  a.InvoiceNo,
                                  a.InvoiceDate,
                                  a.MaHang,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.VATAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaKhoanMucCP,
                                  a.VATRate,
                                  a.MReasonPay,
                                  a.DeductionDebitAccount,
                                  a.SpecialConsumeTaxAccount,
                                  a.PPOrderNo,
                                  a.SpecialConsumeTaxRate,
                                  a.MaThongKe,
                                  a.ImportTaxRate
                              } into gr
                              select new MuaHangKhongQuaKho_Model
                              {
                                  No = gr.Key.No,
                                  Date = gr.Key.Date,
                                  PhuongThucTT = gr.Key.PhuongThucTT,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  MaHang = gr.Key.MaHang,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  VATAccount = gr.Key.VATAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  VATRate = gr.Key.VATRate,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  ImportTaxRate = gr.Key.ImportTaxRate,
                                  ImportTaxAmount = gr.Sum(t => t.ImportTaxAmount),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  TotalVATAmountOriginal = gr.Sum(t => t.TotalVATAmountOriginal),
                                  MReasonPay = gr.Key.MReasonPay,
                                  DeductionDebitAccount = gr.Key.DeductionDebitAccount,
                                  SpecialConsumeTaxAccount = gr.Key.SpecialConsumeTaxAccount,
                                  PPOrderNo = gr.Key.PPOrderNo,
                                  SpecialConsumeTaxRate = gr.Key.SpecialConsumeTaxRate,
                                  SpecialConsumeTaxAmount = gr.Sum(t => t.SpecialConsumeTaxAmount),
                                  SpecialConsumeTaxAmountOriginal = gr.Sum(t => t.SpecialConsumeTaxAmountOriginal),
                                  FreightAmount = gr.Sum(t => t.FreightAmount),
                                  FreightAmountOriginal = gr.Sum(t => t.FreightAmountOriginal),
                                  InwardAmount = gr.Sum(t => t.InwardAmount),
                                  InwardAmountOriginal = gr.Sum(t => t.InwardAmountOriginal),
                                  ImportTaxExpenseAmount = gr.Sum(t => t.ImportTaxExpenseAmount),
                                  ImportTaxExpenseAmountOriginal = gr.Sum(t => t.ImportTaxExpenseAmountOriginal),
                                  MaThongKe = gr.Key.MaThongKe,
                              };
            List<PPInvoice> list_master = new List<PPInvoice>();
            List<MCPayment> list_MCPayment = new List<MCPayment>();
            List<MBTellerPaper> list_MBTellerPaper = new List<MBTellerPaper>();
            List<MBCreditCard> list_MBCreditCard = new List<MBCreditCard>();
            foreach (var item in list_group)
            {
                PPInvoice master = new PPInvoice();
                master.No = item.No;
                master.TypeID = int.Parse(item.PhuongThucTT);
                master.MPostedDate = item.PostedDate;
                master.MContactName = item.ContactName;
                master.MReasonPay = item.MReasonPay;
                master.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == master.TypeID).ID;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.MDate = item.MDate ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OriginalNo = item.OriginalNo;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                }
                master.CreditCardNumber = item.CreditCardNumber;
                master.NumberAttach = item.NumberAttach;
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.IdentificationNo = item.IdentificationNo;
                master.IssueDate = item.IssueDate;
                master.IssueBy = item.IssueBy;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                var bankdetailacc = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == item.SoTKHuong);
                if (bankdetailacc != null)
                {
                    master.MAccountingObjectBankAccountDetailID = bankdetailacc.ID;
                    master.MAccountingObjectBankName = bankdetailacc.BankName;
                }
                var tkchi = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.SoTKChi);
                if (tkchi != null)
                {
                    master.BankAccountDetailID = tkchi.ID;
                    master.BankName = tkchi.BankName;
                }
                master.RefDateTime = DateTime.Now;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<PPInvoiceDetail> list_sadetail = new List<PPInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        PPInvoiceDetail saitem = new PPInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            saitem.MaterialGoodsID = hanghoa.ID;
                            saitem.RepositoryID = hanghoa.RepositoryID;
                            saitem.Description = hanghoa.MaterialGoodsName;
                            saitem.Unit = hanghoa.Unit;
                        }
                        saitem.DebitAccount = ct.DebitAccount;
                        saitem.CreditAccount = ct.CreditAccount;
                        saitem.Quantity = ct.Quantity;
                        saitem.UnitPrice = ct.UnitPrice ?? 0;
                        saitem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        saitem.Amount = ct.Amount ?? 0;
                        saitem.AmountOriginal = ct.AmountOriginal ?? 0;
                        saitem.DiscountRate = ct.DiscountRate;
                        saitem.DiscountAmount = ct.DiscountAmount ?? 0;
                        saitem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        saitem.VATRate = ct.VATRate;
                        saitem.VATAmount = ct.VATAmount ?? 0;
                        saitem.VATAccount = ct.VATAccount;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.InvoiceNo = ct.InvoiceNo;
                        saitem.InvoiceDate = ct.InvoiceDate;
                        saitem.InvoiceSeries = ct.InvoiceSeries;
                        saitem.InvoiceTemplate = ct.InvoiceTemplate;
                        saitem.DeductionDebitAccount = ct.DeductionDebitAccount;
                        saitem.SpecialConsumeTaxAccount = ct.SpecialConsumeTaxAccount;
                        saitem.PPOrderNo = ct.PPOrderNo;
                        saitem.SpecialConsumeTaxRate = ct.SpecialConsumeTaxRate ?? 0;
                        if (khachhang != null)
                            saitem.AccountingObjectID = khachhang.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            saitem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            saitem.ContractID = hopdong.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            saitem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            saitem.BudgetItemID = mucthuchi.ID;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == ct.MaNhomHHDV);
                        if (nhomhhdv != null)
                            saitem.GoodsServicePurchaseID = nhomhhdv.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            saitem.StatisticsCodeID = thongke.ID;
                        if (khachhang != null)
                        {
                            saitem.AccountingObjectID = khachhang.ID;
                            saitem.AccountingObjectTaxID = khachhang.ID;
                            saitem.AccountingObjectTaxName = khachhang.AccountingObjectName;
                            saitem.CompanyTaxCode = khachhang.TaxCode;
                        }
                        saitem.TaxExchangeRate = master.ExchangeRate ?? 0;
                        saitem.ImportTaxRate = ct.ImportTaxRate ?? 0;
                        saitem.ImportTaxAmount = ct.ImportTaxAmount ?? 0;
                        saitem.ImportTaxAccount = ct.ImportTaxAccount;
                        saitem.VATDescription = ct.VATDescription;
                        saitem.ImportTaxAmountOriginal = (ct.ImportTaxAmount * ct.ImportTaxRate ?? 0) / 100;
                        saitem.VATAmountOriginal = (ct.ImportTaxAmount * ct.VATRate ?? 0) / 100;
                        saitem.SpecialConsumeTaxAmount = ct.SpecialConsumeTaxAmount ?? 0;
                        saitem.SpecialConsumeTaxAmountOriginal = ct.SpecialConsumeTaxAmount * ct.SpecialConsumeTaxRate ?? 0;
                        saitem.FreightAmount = ct.FreightAmount ?? 0;
                        saitem.FreightAmountOriginal = ct.FreightAmountOriginal ?? 0;
                        saitem.InwardAmount = ct.InwardAmount ?? 0;
                        saitem.InwardAmountOriginal = ct.InwardAmountOriginal ?? 0;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.ImportTaxExpenseAmount = ct.ImportTaxExpenseAmount ?? 0;
                        saitem.ImportTaxExpenseAmountOriginal = (ct.ImportTaxRate != null && ct.ImportTaxRate != 0) ? (ct.ImportTaxExpenseAmount / ct.ImportTaxRate ?? 0) : 0;

                        list_sadetail.Add(saitem);
                    }
                    master.PPInvoiceDetails = list_sadetail;
                    master.TotalAmount = list_sadetail.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_sadetail.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadetail.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadetail.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadetail.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadetail.Sum(t => t.VATAmountOriginal);
                    master.TotalImportTaxAmount = list_sadetail.Sum(t => t.ImportTaxAmount);
                    master.TotalImportTaxAmountOriginal = list_sadetail.Sum(t => t.ImportTaxAmountOriginal);
                    master.TotalInwardAmount = list_sadetail.Sum(t => t.InwardAmount);
                    master.TotalInwardAmountOriginal = list_sadetail.Sum(t => t.InwardAmountOriginal);
                    master.TotalSpecialConsumeTaxAmount = list_detail.Sum(t => t.SpecialConsumeTaxAmount);
                    master.TotalSpecialConsumeTaxAmountOriginal = list_detail.Sum(t => t.SpecialConsumeTaxAmountOriginal);
                    master.TotalFreightAmount = list_detail.Sum(t => t.FreightAmount);
                    master.TotalFreightAmountOriginal = list_detail.Sum(t => t.FreightAmountOriginal);
                    master.TotalImportTaxExpenseAmount = list_detail.Sum(t => t.ImportTaxExpenseAmount);
                    master.TotalImportTaxExpenseAmountOriginal = list_detail.Sum(t => t.ImportTaxExpenseAmountOriginal);
                    list_master.Add(master);
                    if (item.PhuongThucTT == "260")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MCPayment payment = new MCPayment();
                        payment.TypeID = 117;
                        payment.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == payment.TypeID).ID;
                        payment.No = master.No;
                        payment.Date = master.Date;
                        payment.PostedDate = master.PostedDate;
                        payment.AccountingObjectID = master.AccountingObjectID;
                        payment.AccountingObjectName = master.AccountingObjectName;
                        payment.AccountingObjectAddress = master.AccountingObjectAddress;
                        payment.Receiver = master.MContactName;
                        payment.Reason = master.Reason;
                        payment.NumberAttach = master.NumberAttach;
                        payment.CurrencyID = master.CurrencyID;
                        payment.ExchangeRate = master.ExchangeRate;
                        payment.PaymentClauseID = master.PaymentClauseID;
                        payment.EmployeeID = master.EmployeeID;
                        payment.Recorded = false;
                        payment.Exported = master.Exported;
                        payment.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        payment.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MCPayment.Add(payment);
                    }
                    if (item.PhuongThucTT == "261" || item.PhuongThucTT == "262" || item.PhuongThucTT == "264")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBTellerPaper TellerPaper = new MBTellerPaper();
                        TellerPaper.TypeID = item.PhuongThucTT == "261" ? 127 : item.PhuongThucTT == "262" ? 131 : 141;
                        TellerPaper.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == TellerPaper.TypeID).ID;
                        TellerPaper.No = master.No;
                        TellerPaper.Date = master.Date;
                        TellerPaper.PostedDate = master.PostedDate;
                        TellerPaper.AccountingObjectID = master.AccountingObjectID;
                        TellerPaper.AccountingObjectName = master.AccountingObjectName;
                        TellerPaper.AccountingObjectAddress = master.AccountingObjectAddress;
                        TellerPaper.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID;
                        TellerPaper.AccountingObjectBankName = master.MAccountingObjectBankName;
                        TellerPaper.BankAccountDetailID = master.BankAccountDetailID;
                        TellerPaper.Receiver = master.MContactName;
                        TellerPaper.BankName = master.BankName;
                        TellerPaper.Reason = master.Reason;
                        TellerPaper.CurrencyID = master.CurrencyID;
                        TellerPaper.ExchangeRate = master.ExchangeRate;
                        TellerPaper.PaymentClauseID = master.PaymentClauseID;
                        TellerPaper.TotalAmount = master.TotalAmount;
                        TellerPaper.TotalAmountOriginal = master.TotalAmountOriginal;
                        TellerPaper.TotalVATAmount = master.TotalVATAmount;
                        TellerPaper.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        TellerPaper.EmployeeID = master.EmployeeID;
                        TellerPaper.Recorded = false;
                        TellerPaper.Exported = master.Exported;
                        TellerPaper.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        TellerPaper.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MBTellerPaper.Add(TellerPaper);
                    }
                    if (item.PhuongThucTT == "263")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBCreditCard CreditCard = new MBCreditCard();
                        CreditCard.TypeID = 171;
                        CreditCard.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == CreditCard.TypeID).ID;
                        CreditCard.No = master.No;
                        CreditCard.Date = master.Date;
                        CreditCard.PostedDate = master.PostedDate;
                        CreditCard.AccountingObjectID = master.AccountingObjectID;
                        CreditCard.AccountingObjectName = master.AccountingObjectName;
                        CreditCard.AccountingObjectAddress = master.AccountingObjectAddress;
                        CreditCard.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID;
                        CreditCard.AccountingObjectBankName = master.MAccountingObjectBankName;
                        if (khachhang != null)
                            CreditCard.AccountingObjectType = khachhang.ObjectType;
                        CreditCard.CreditCardNumber = master.CreditCardNumber;
                        CreditCard.Reason = master.Reason;
                        CreditCard.CurrencyID = master.CurrencyID;
                        CreditCard.ExchangeRate = master.ExchangeRate;
                        CreditCard.PaymentClauseID = master.PaymentClauseID;
                        CreditCard.TotalAmount = master.TotalAmount;
                        CreditCard.TotalAmountOriginal = master.TotalAmountOriginal;
                        CreditCard.TotalVATAmount = master.TotalVATAmount;
                        CreditCard.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        CreditCard.EmployeeID = master.EmployeeID;
                        CreditCard.Recorded = false;
                        CreditCard.Exported = master.Exported;
                        CreditCard.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        CreditCard.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MBCreditCard.Add(CreditCard);
                    }
                }
            }
            MH_QuaKho_model model = new MH_QuaKho_model();
            model.listPPInvoice = list_master;
            model.listMCPayment = list_MCPayment;
            model.listMBTellerPaper = list_MBTellerPaper;
            model.listMBCreditCard = list_MBCreditCard;
            return model;
        }
        public object SetDataMuaDichVu(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            List<MuaDichVu_Model> dt_cv = data.Cast<MuaDichVu_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        //t.Date = item_first.Date;
                        //t.DueDate = item_first.DueDate;
                        //t.PostedDate = item_first.PostedDate;
                        //t.CreditCardNumber = item_first.CreditCardNumber;
                        //t.TKNHChi = item_first.TKNHChi;
                        //t.MaNCC = item_first.MaNCC;
                        //t.TKHuong = item_first.TKHuong;
                        //t.Reason = item_first.Reason;
                        //t.NumberAttach = item_first.NumberAttach;
                        //t.ContactName = item_first.ContactName;
                        //t.IdentificationNo = item_first.IdentificationNo;
                        //t.IssueBy = item_first.IssueBy;
                        //t.IssueDate = item_first.IssueDate;
                        //t.IsFeightService = item_first.IsFeightService;
                        //t.ExchangeRate = item_first.ExchangeRate;
                        //t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        //if (t.ExchangeRate == null)
                        //{
                        //    var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                        //    if (current != null)
                        //        t.ExchangeRate = current.ExchangeRate;
                        //}
                        //t.PhuongThucTT = item_first.PhuongThucTT;
                        //t.NumberAttach = item_first.NumberAttach;

                        t.No = item_first.No;
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.ContactName = item_first.ContactName;
                        t.CreditCardNumber = item_first.CreditCardNumber;
                        t.TKNHChi = item_first.TKNHChi;
                        t.MaNCC = item_first.MaNCC;
                        t.TKHuong = item_first.TKHuong;
                        t.Reason = item_first.Reason;
                        t.NumberAttach = item_first.NumberAttach;
                        t.IdentificationNo = item_first.IdentificationNo;
                        t.IssueBy = item_first.IssueBy;
                        t.IssueDate = item_first.IssueDate;
                        t.IsFeightService = item_first.IsFeightService;
                        t.PhuongThucTT = item_first.PhuongThucTT;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.DueDate = item_first.DueDate;
                        t.MaNhanVien = item_first.MaNhanVien;

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.ContactName,
                                 a.CreditCardNumber,
                                 a.TKNHChi,
                                 a.MaNCC,
                                 a.TKHuong,
                                 a.Reason,
                                 a.NumberAttach,
                                 a.IdentificationNo,
                                 a.IssueBy,
                                 a.IssueDate,
                                 a.IsFeightService,
                                 a.PhuongThucTT,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.DueDate,
                                 a.MaNhanVien
                             } into gr
                             select new MuaDichVu_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 CreditCardNumber = gr.Key.CreditCardNumber,
                                 TKNHChi = gr.Key.TKNHChi,
                                 MaNCC = gr.Key.MaNCC,
                                 TKHuong = gr.Key.TKHuong,
                                 Reason = gr.Key.Reason,
                                 NumberAttach = gr.Key.NumberAttach,
                                 ContactName = gr.Key.ContactName,
                                 IdentificationNo = gr.Key.IdentificationNo,
                                 IssueBy = gr.Key.IssueBy,
                                 IssueDate = gr.Key.IssueDate,
                                 IsFeightService = gr.Key.IsFeightService,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 DueDate = gr.Key.DueDate,
                                 MaNhanVien = gr.Key.MaNhanVien,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.MaDichVu,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.DiscountRate,
                                  a.VATRate,
                                  a.VATAccount,
                                  a.InvoiceDate,
                                  a.InvoiceNo,
                                  a.InvoiceSeries,
                                  a.MaNhomHHDV,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaThongKe,
                                  a.MaMucThuChi,
                                  a.VATDescription,
                                  a.InvoiceTemplate,
                                  a.MaPhongBan,
                                  a.MaKhoanMucCP,
                              } into gr
                              select new MuaDichVu_Model
                              {
                                  No = gr.Key.No,
                                  MaDichVu = gr.Key.MaDichVu,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  DiscountRate = gr.Key.DiscountRate,
                                  VATRate = gr.Key.VATRate,
                                  VATAccount = gr.Key.VATAccount,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  MaNhomHHDV = gr.Key.MaNhomHHDV,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaThongKe = gr.Key.MaThongKe,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  VATDescription = gr.Key.VATDescription,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP
                              };
            List<PPService> list_master = new List<PPService>();
            List<MCPayment> list_MCPayment = new List<MCPayment>();
            List<MBTellerPaper> list_MBTellerPaper = new List<MBTellerPaper>();
            List<MBCreditCard> list_MBCreditCard = new List<MBCreditCard>();
            foreach (var item in list_group)
            {
                PPService master = new PPService();
                master.No = item.No;
                master.TypeID = int.Parse(item.PhuongThucTT);
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.DueDate = item.DueDate;
                master.CreditCardNumber = item.CreditCardNumber;
                var tkchi = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.TKNHChi);
                if (tkchi != null)
                {
                    master.BankAccountDetailID = tkchi.ID;
                    master.BankName = tkchi.BankName;
                }
                var ncc = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                if (ncc != null)
                {
                    master.AccountingObjectID = ncc.ID;
                    master.AccountingObjectName = ncc.AccountingObjectName;
                    master.AccountingObjectAddress = ncc.Address;
                    master.CompanyTaxCode = ncc.TaxCode;
                }
                var tkhuong = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == item.TKHuong);
                if (tkhuong != null)
                {
                    master.AccountingObjectBankAccountDetailID = tkhuong.ID;
                    master.AccountingObjectBankName = tkhuong.BankName;
                }
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.Reason = item.Reason;
                master.NumberAttach = item.NumberAttach;
                master.ContactName = item.ContactName;
                master.IdentificationNo = item.IdentificationNo;
                master.IssueBy = item.IssueBy;
                master.IssueDate = item.IssueDate;
                master.IsFeightService = item.IsFeightService == null ? false : (item.IsFeightService == "0" ? false : true);
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.Recorded = false;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<PPServiceDetail> list_PPServiceDetail = new List<PPServiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        PPServiceDetail pp_ct = new PPServiceDetail();
                        var dichvu = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaDichVu);
                        if (dichvu != null)
                        {
                            pp_ct.MaterialGoodsID = dichvu.ID;
                            pp_ct.Description = dichvu.MaterialGoodsName;
                        }
                        pp_ct.DebitAccount = ct.DebitAccount;
                        pp_ct.CreditAccount = ct.CreditAccount;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            pp_ct.BudgetItemID = mucthuchi.ID;
                        pp_ct.DiscountAmount = ct.DiscountAmount;
                        pp_ct.DiscountAmountOriginal = ct.DiscountAmountOriginal;
                        pp_ct.DiscountRate = ct.DiscountRate;
                        pp_ct.VATRate = ct.VATRate;
                        pp_ct.Amount = ct.Amount ?? 0;
                        pp_ct.VATAmount = ct.VATAmount;
                        pp_ct.AmountOriginal = ct.AmountOriginal ?? 0;
                        pp_ct.VATAmountOriginal = ct.VATAmountOriginal;
                        pp_ct.VATAccount = ct.VATAccount;
                        pp_ct.InvoiceDate = ct.InvoiceDate;
                        pp_ct.InvoiceNo = ct.InvoiceNo;
                        pp_ct.InvoiceSeries = ct.InvoiceSeries;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == ct.MaNhomHHDV);
                        if (nhomhhdv != null)
                            pp_ct.GoodsServicePurchaseID = nhomhhdv.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            pp_ct.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            pp_ct.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            pp_ct.StatisticsCodeID = thongke.ID;
                        if (ncc != null)
                        {
                            pp_ct.AccountingObjectID = ncc.ID;
                            pp_ct.AccountingObjectTaxID = ncc.ID;
                            pp_ct.AccountingObjectTaxName = ncc.AccountingObjectName;
                            pp_ct.CompanyTaxCode = ncc.TaxCode;
                        }
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            pp_ct.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            pp_ct.ExpenseItemID = khoanmuccp.ID;
                        pp_ct.VATDescription = ct.VATDescription;
                        pp_ct.InvoiceTemplate = ct.InvoiceTemplate;
                        pp_ct.Quantity = ct.Quantity ?? 0;
                        pp_ct.UnitPrice = ct.UnitPrice ?? 0;
                        pp_ct.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        list_PPServiceDetail.Add(pp_ct);
                    }
                    master.TemplateID = new Guid("0BCFF3C0-EE42-412B-9A47-08DB6DB262BF");
                    master.PPServiceDetails = list_PPServiceDetail;
                    master.TotalAmount = list_PPServiceDetail.Sum(t => t.Amount ?? 0);
                    master.TotalAmountOriginal = list_PPServiceDetail.Sum(t => t.AmountOriginal ?? 0);
                    master.TotalVATAmountOriginal = list_PPServiceDetail.Sum(t => t.AmountOriginal ?? 0) / (master.ExchangeRate ?? 1);
                    master.TotalDiscountAmount = list_PPServiceDetail.Sum(t => t.DiscountAmount ?? 0);
                    master.TotalDiscountAmountOriginal = list_PPServiceDetail.Sum(t => t.DiscountAmountOriginal ?? 0);
                    master.TotalVATAmount = list_PPServiceDetail.Sum(t => t.VATAmount ?? 0);
                    master.Recorded = false;
                    var check_hoadon = list_PPServiceDetail.Any(t => t.InvoiceDate != null && t.InvoiceNo != null && t.InvoiceSeries != null && t.InvoiceTemplate != null);
                    master.BillReceived = check_hoadon;
                    list_master.Add(master);
                    if (item.PhuongThucTT == "116")
                    {
                        MCPayment mc = new MCPayment();
                        master.TypeID = 116;
                        mc.TypeID = 116;
                        mc.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == mc.TypeID).ID;
                        mc.No = master.No;
                        mc.Date = master.Date;
                        mc.PostedDate = master.PostedDate;
                        mc.AccountingObjectID = master.AccountingObjectID;
                        mc.AccountingObjectName = master.AccountingObjectName;
                        mc.AccountingObjectAddress = master.AccountingObjectAddress;
                        mc.Receiver = master.ContactName;
                        mc.Reason = master.Reason;
                        mc.CurrencyID = master.CurrencyID;
                        mc.ExchangeRate = master.ExchangeRate;
                        mc.TotalAmount = master.TotalAmount;
                        mc.TotalAmountOriginal = master.TotalAmountOriginal;
                        mc.TotalVATAmount = master.TotalVATAmount;
                        mc.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        mc.EmployeeID = master.EmployeeID;
                        if (ncc != null)
                            mc.AccountingObjectType = ncc.ObjectType;
                        mc.Recorded = false;
                        mc.TotalAll = mc.TotalAmount + mc.TotalVATAmount - master.TotalDiscountAmount;
                        mc.TotalAllOriginal = mc.TotalAmountOriginal + mc.TotalVATAmountOriginal - master.TotalDiscountAmountOriginal;
                        list_MCPayment.Add(mc);
                    }
                    if (item.PhuongThucTT == "126" || item.PhuongThucTT == "133" || item.PhuongThucTT == "143")
                    {
                        MBTellerPaper mc = new MBTellerPaper();
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        mc.TypeID = int.Parse(item.PhuongThucTT);
                        mc.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == mc.TypeID).ID;
                        mc.No = master.No;
                        mc.Date = master.Date;
                        mc.PostedDate = master.PostedDate;
                        mc.AccountingObjectID = master.AccountingObjectID;
                        mc.AccountingObjectName = master.AccountingObjectName;
                        mc.AccountingObjectAddress = master.AccountingObjectAddress;
                        mc.Receiver = master.ContactName;
                        mc.Reason = master.Reason;
                        mc.CurrencyID = master.CurrencyID;
                        mc.ExchangeRate = master.ExchangeRate;
                        mc.TotalAmount = master.TotalAmount;
                        mc.TotalAmountOriginal = master.TotalAmountOriginal;
                        mc.TotalVATAmount = master.TotalVATAmount;
                        mc.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        if (ncc != null)
                            mc.AccountingObjectType = ncc.ObjectType ?? 0;
                        mc.Recorded = false;
                        mc.TotalAll = mc.TotalAmount + mc.TotalVATAmount - master.TotalDiscountAmount;
                        mc.TotalAllOriginal = mc.TotalAmountOriginal + mc.TotalVATAmountOriginal - master.TotalDiscountAmountOriginal;
                        mc.BankAccountDetailID = master.BankAccountDetailID;
                        mc.BankName = master.BankName;
                        if (tkhuong != null)
                        {
                            mc.AccountingObjectBankAccount = tkhuong.ID;
                            mc.AccountingObjectBankName = tkhuong.BankName;
                        }
                        mc.IdentificationNo = master.IdentificationNo;
                        mc.IssueDate = master.IssueDate;
                        mc.IssueBy = master.IssueBy;
                        mc.TotalVATAmount = master.TotalVATAmount;
                        mc.EmployeeID = master.EmployeeID;
                        list_MBTellerPaper.Add(mc);
                    }
                    if (item.PhuongThucTT == "173")
                    {
                        MBCreditCard mc = new MBCreditCard();
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        mc.TypeID = int.Parse(item.PhuongThucTT);
                        mc.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == mc.TypeID).ID;
                        mc.No = master.No;
                        mc.Date = master.Date;
                        mc.PostedDate = master.PostedDate;
                        mc.AccountingObjectID = master.AccountingObjectID;
                        mc.AccountingObjectName = master.AccountingObjectName;
                        mc.AccountingObjectAddress = master.AccountingObjectAddress;
                        mc.CreditCardNumber = master.CreditCardNumber;
                        mc.Reason = master.Reason;
                        mc.CurrencyID = master.CurrencyID;
                        mc.ExchangeRate = master.ExchangeRate;
                        //mc.TotalAmount = master.TotalAmount;
                        mc.TotalAmountOriginal = master.TotalAmountOriginal;
                        mc.TotalVATAmount = master.TotalVATAmount;
                        mc.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        mc.EmployeeID = master.EmployeeID;
                        if (ncc != null)
                            mc.AccountingObjectType = ncc.ObjectType;
                        if (tkhuong != null)
                        {
                            mc.AccountingObjectBankAccount = tkhuong.ID;
                            mc.AccountingObjectBankName = tkhuong.BankName;
                        }
                        mc.Recorded = false;
                        mc.TotalAll = master.TotalAmount + master.TotalVATAmount - master.TotalDiscountAmount;
                        mc.TotalAllOriginal = mc.TotalAmountOriginal + mc.TotalVATAmountOriginal - master.TotalDiscountAmountOriginal;
                        list_MBCreditCard.Add(mc);
                    }
                }
            }
            MH_DichVu_model model = new MH_DichVu_model();
            model.listPPService = list_master;
            model.listMCPayment = list_MCPayment;
            model.listMBTellerPaper = list_MBTellerPaper;
            model.listMBCreditCard = list_MBCreditCard;
            return model;
        }
    }
}
