﻿using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.ControlsMapping.ConfigImport
{
    public class ConfigMuaHangKhongQuaKho
    {
        public object SetDataMuaHangKhongQuaKho(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            IAccountingObjectBankAccountService _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            IGoodsServicePurchaseService _IGoodsServicePurchaseService = IoC.Resolve<IGoodsServicePurchaseService>();
            List<MuaHangKhongQuaKho_Model> dt_cv = data.Cast<MuaHangKhongQuaKho_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.MDate = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaNCC = item_first.MaNCC;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.MReasonPay = item_first.MReasonPay;
                        t.OriginalNo = item_first.OriginalNo;
                        t.MPostedDate = item_first.MPostedDate;
                        t.MDate = item_first.MDate;
                        t.SoTKHuong = item_first.SoTKHuong;
                        t.SoTKChi = item_first.SoTKChi;
                        t.CreditCardNumber = item_first.CreditCardNumber;
                        t.IdentificationNo = item_first.IdentificationNo;
                        t.IssueDate = item_first.IssueDate;
                        t.IssueBy = item_first.IssueBy;
                        t.DueDate = item_first.DueDate;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.PhuongThucTT = item_first.PhuongThucTT;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                        t.NumberAttach = item_first.NumberAttach;
                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaNCC,
                                 a.ContactName,
                                 a.Reason,
                                 a.MReasonPay,
                                 a.OriginalNo,
                                 a.MPostedDate,
                                 a.MDate,
                                 a.SoTKHuong,
                                 a.SoTKChi,
                                 a.CreditCardNumber,
                                 a.NumberAttach,
                                 a.IdentificationNo,
                                 a.IssueDate,
                                 a.IssueBy,
                                 a.DueDate,
                                 a.MaNhanVien,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.PhuongThucTT,
                                 a.InvoiceTemplate,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.InvoiceSeries,
                             } into gr
                             select new MuaHangKhongQuaKho_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaNCC = gr.Key.MaNCC,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 MReasonPay = gr.Key.MReasonPay,
                                 OriginalNo = gr.Key.OriginalNo,
                                 MPostedDate = gr.Key.MPostedDate,
                                 MDate = gr.Key.MDate,
                                 SoTKHuong = gr.Key.SoTKHuong,
                                 SoTKChi = gr.Key.SoTKChi,
                                 CreditCardNumber = gr.Key.CreditCardNumber,
                                 NumberAttach = gr.Key.NumberAttach,
                                 IdentificationNo = gr.Key.IdentificationNo,
                                 IssueDate = gr.Key.IssueDate,
                                 IssueBy = gr.Key.IssueBy,
                                 DueDate = gr.Key.DueDate,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.Date,
                                  a.PhuongThucTT,
                                  a.InvoiceTemplate,
                                  a.InvoiceSeries,
                                  a.InvoiceNo,
                                  a.InvoiceDate,
                                  a.MaHang,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.VATAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaKhoanMucCP,
                                  a.VATRate,
                                  a.MReasonPay,
                                  a.DeductionDebitAccount,
                                  a.SpecialConsumeTaxAccount,
                                  a.PPOrderNo,
                                  a.SpecialConsumeTaxRate,
                                  a.MaThongKe,
                                  a.ImportTaxRate,
                                  a.MaNhomHHDV,
                                  a.ImportTaxAccount,
                              } into gr
                              select new MuaHangKhongQuaKho_Model
                              {
                                  No = gr.Key.No,
                                  Date = gr.Key.Date,
                                  PhuongThucTT = gr.Key.PhuongThucTT,
                                  InvoiceTemplate = gr.Key.InvoiceTemplate,
                                  InvoiceSeries = gr.Key.InvoiceSeries,
                                  InvoiceNo = gr.Key.InvoiceNo,
                                  InvoiceDate = gr.Key.InvoiceDate,
                                  MaHang = gr.Key.MaHang,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  VATAccount = gr.Key.VATAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  VATRate = gr.Key.VATRate,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  ImportTaxRate = gr.Key.ImportTaxRate,
                                  ImportTaxAmount = gr.Sum(t => t.ImportTaxAmount),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  TotalVATAmountOriginal = gr.Sum(t => t.TotalVATAmountOriginal),
                                  MReasonPay = gr.Key.MReasonPay,
                                  DeductionDebitAccount = gr.Key.DeductionDebitAccount,
                                  SpecialConsumeTaxAccount = gr.Key.SpecialConsumeTaxAccount,
                                  PPOrderNo = gr.Key.PPOrderNo,
                                  SpecialConsumeTaxRate = gr.Key.SpecialConsumeTaxRate,
                                  SpecialConsumeTaxAmount = gr.Sum(t => t.SpecialConsumeTaxAmount),
                                  SpecialConsumeTaxAmountOriginal = gr.Sum(t => t.SpecialConsumeTaxAmountOriginal),
                                  FreightAmount = gr.Sum(t => t.FreightAmount),
                                  FreightAmountOriginal = gr.Sum(t => t.FreightAmountOriginal),
                                  InwardAmount = gr.Sum(t => t.InwardAmount),
                                  InwardAmountOriginal = gr.Sum(t => t.InwardAmountOriginal),
                                  ImportTaxExpenseAmount = gr.Sum(t => t.ImportTaxExpenseAmount),
                                  ImportTaxExpenseAmountOriginal = gr.Sum(t => t.ImportTaxExpenseAmountOriginal),
                                  MaThongKe = gr.Key.MaThongKe,
                                  MaNhomHHDV = gr.Key.MaNhomHHDV,
                                  ImportTaxAccount = gr.Key.ImportTaxAccount,
                              };
            List<PPInvoice> list_master = new List<PPInvoice>();
            List<MCPayment> list_MCPayment = new List<MCPayment>();
            List<MBTellerPaper> list_MBTellerPaper = new List<MBTellerPaper>();
            List<MBCreditCard> list_MBCreditCard = new List<MBCreditCard>();
            foreach (var item in list_group)
            {
                PPInvoice master = new PPInvoice();
                master.No = item.No;
                master.TypeID = int.Parse(item.PhuongThucTT);
                master.MPostedDate = item.PostedDate;
                master.MContactName = item.ContactName;
                master.MReasonPay = item.MReasonPay;
                master.TemplateID = new Guid("2DF8175E-428F-4285-A84D-815E7697318E");
                master.Date = item.Date ?? (new DateTime()).Date;
                master.MDate = item.MDate ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OriginalNo = item.OriginalNo;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNCC);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                }
                master.CreditCardNumber = item.CreditCardNumber;
                master.NumberAttach = item.NumberAttach;
                master.OriginalNo = item.NumberAttach;
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.IdentificationNo = item.IdentificationNo;
                master.IssueDate = item.IssueDate;
                master.IssueBy = item.IssueBy;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                var bankdetailacc = _IAccountingObjectBankAccountService.Query.FirstOrDefault(t => t.BankAccount == item.SoTKHuong);
                if (bankdetailacc != null)
                {
                    master.MAccountingObjectBankAccountDetailID = bankdetailacc.ID;
                    master.MAccountingObjectBankName = bankdetailacc.BankName;
                }
                var tkchi = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.SoTKChi);
                if (tkchi != null)
                {
                    master.BankAccountDetailID = tkchi.ID;
                    master.BankName = tkchi.BankName;
                }
                master.RefDateTime = DateTime.Now;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<PPInvoiceDetail> list_sadetail = new List<PPInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        PPInvoiceDetail saitem = new PPInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            saitem.MaterialGoodsID = hanghoa.ID;
                            saitem.RepositoryID = hanghoa.RepositoryID;
                            saitem.Description = hanghoa.MaterialGoodsName;
                            saitem.Unit = hanghoa.Unit;
                        }
                        saitem.DebitAccount = ct.DebitAccount;
                        saitem.CreditAccount = ct.CreditAccount;
                        saitem.Quantity = ct.Quantity;
                        saitem.UnitPrice = ct.UnitPrice ?? 0;
                        saitem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        saitem.Amount = ct.Amount ?? 0;
                        saitem.AmountOriginal = ct.AmountOriginal ?? 0;
                        saitem.DiscountRate = ct.DiscountRate;
                        saitem.DiscountAmount = ct.DiscountAmount ?? 0;
                        saitem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        saitem.VATRate = ct.VATRate;
                        saitem.VATAmount = ct.VATAmount ?? 0;
                        saitem.VATAccount = ct.VATAccount;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.InvoiceNo = ct.InvoiceNo;
                        saitem.InvoiceDate = ct.InvoiceDate;
                        saitem.InvoiceSeries = ct.InvoiceSeries;
                        saitem.InvoiceTemplate = ct.InvoiceTemplate;
                        saitem.DeductionDebitAccount = ct.DeductionDebitAccount;
                        saitem.SpecialConsumeTaxAccount = ct.SpecialConsumeTaxAccount;
                        saitem.PPOrderNo = ct.PPOrderNo;
                        saitem.SpecialConsumeTaxRate = ct.SpecialConsumeTaxRate ?? 0;
                        if (khachhang != null)
                            saitem.AccountingObjectID = khachhang.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            saitem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            saitem.ContractID = hopdong.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            saitem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            saitem.BudgetItemID = mucthuchi.ID;
                        var nhomhhdv = _IGoodsServicePurchaseService.Query.FirstOrDefault(t => t.GoodsServicePurchaseCode == ct.MaNhomHHDV);
                        if (nhomhhdv != null)
                            saitem.GoodsServicePurchaseID = nhomhhdv.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            saitem.StatisticsCodeID = thongke.ID;
                        if (khachhang != null)
                        {
                            saitem.AccountingObjectID = khachhang.ID;
                            saitem.AccountingObjectTaxID = khachhang.ID;
                            saitem.AccountingObjectTaxName = khachhang.AccountingObjectName;
                            saitem.CompanyTaxCode = khachhang.TaxCode;
                        }
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        saitem.TaxExchangeRate = master.ExchangeRate ?? 1;
                        saitem.ImportTaxRate = ct.ImportTaxRate ?? 0;
                        saitem.ImportTaxAmount = ct.ImportTaxAmount ?? 0;
                        saitem.ImportTaxAccount = ct.ImportTaxAccount;
                        saitem.VATDescription = ct.VATDescription;
                        saitem.InwardAmountOriginal = ct.InwardAmountOriginal ?? 0;
                        saitem.InwardAmount = ((ct.InwardAmountOriginal ?? 0) * (master.ExchangeRate ?? 0));
                        saitem.SpecialConsumeTaxAmount = ct.SpecialConsumeTaxAmount ?? 0;
                        saitem.SpecialConsumeTaxAmountOriginal = ((ct.Amount ?? 0) * (ct.SpecialConsumeTaxRate ?? 0))/100;
                        saitem.FreightAmountOriginal = ct.FreightAmountOriginal ?? 0;
                        saitem.FreightAmount = ((ct.FreightAmountOriginal ?? 0) * (master.ExchangeRate ?? 0));
                        saitem.ImportTaxExpenseAmount = ct.ImportTaxExpenseAmount ?? 0;
                        saitem.ImportTaxExpenseAmountOriginal = (ct.ImportTaxExpenseAmount ?? 0) / (master.ExchangeRate ?? 1);
                        saitem.ImportTaxAmountOriginal = ((ct.AmountOriginal ?? 0) * (ct.ImportTaxRate ?? 0)) / 100;
                        saitem.VATAmountOriginal = ((ct.AmountOriginal ?? 0) * (ct.VATRate ?? 0)) / 100;
                        list_sadetail.Add(saitem);
                    }
                    master.PPInvoiceDetails = list_sadetail;
                    master.TotalAmount = list_sadetail.Sum(t => t.Amount);
                    master.TotalImportTaxAmount = list_sadetail.Sum(t => t.ImportTaxAmount);
                    master.TotalAmountOriginal = list_sadetail.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadetail.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadetail.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadetail.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadetail.Sum(t => t.VATAmountOriginal);
                    master.TotalImportTaxAmountOriginal = list_sadetail.Sum(t => t.ImportTaxAmountOriginal);
                    master.TotalInwardAmount = list_sadetail.Sum(t => t.InwardAmount);
                    master.TotalInwardAmountOriginal = list_sadetail.Sum(t => t.InwardAmountOriginal);
                    master.TotalSpecialConsumeTaxAmount = list_sadetail.Sum(t => t.SpecialConsumeTaxAmount);
                    master.TotalSpecialConsumeTaxAmountOriginal = list_sadetail.Sum(t => t.SpecialConsumeTaxAmountOriginal);
                    master.TotalFreightAmount = list_sadetail.Sum(t => t.FreightAmount);
                    master.TotalFreightAmountOriginal = list_sadetail.Sum(t => t.FreightAmountOriginal);
                    master.TotalImportTaxExpenseAmount = list_sadetail.Sum(t => t.ImportTaxExpenseAmount);
                    master.TotalImportTaxExpenseAmountOriginal = list_sadetail.Sum(t => t.ImportTaxExpenseAmountOriginal);
                    list_master.Add(master);
                    if (item.PhuongThucTT == "260")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MCPayment payment = new MCPayment();
                        payment.TypeID = 117;
                        payment.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == payment.TypeID).ID;
                        payment.No = master.No;
                        payment.Date = master.Date;
                        payment.PostedDate = master.PostedDate;
                        payment.AccountingObjectID = master.AccountingObjectID;
                        payment.AccountingObjectName = master.AccountingObjectName;
                        payment.AccountingObjectAddress = master.AccountingObjectAddress;
                        payment.Receiver = master.MContactName;
                        payment.Reason = master.Reason;
                        payment.NumberAttach = master.NumberAttach;
                        payment.CurrencyID = master.CurrencyID;
                        payment.ExchangeRate = master.ExchangeRate;
                        payment.PaymentClauseID = master.PaymentClauseID;
                        payment.EmployeeID = master.EmployeeID;
                        payment.Recorded = false;
                        payment.Exported = master.Exported;
                        payment.TotalAmount = master.TotalAmount;
                        payment.TotalAmountOriginal = master.TotalAmountOriginal;
                        payment.TotalVATAmount = master.TotalVATAmount;
                        payment.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        payment.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        payment.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        if (khachhang != null)
                            payment.AccountingObjectType = khachhang.ObjectType;
                        list_MCPayment.Add(payment);
                    }
                    if (item.PhuongThucTT == "261" || item.PhuongThucTT == "262" || item.PhuongThucTT == "264")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBTellerPaper TellerPaper = new MBTellerPaper();
                        TellerPaper.TypeID = item.PhuongThucTT == "261" ? 127 : item.PhuongThucTT == "262" ? 131 : 141;
                        TellerPaper.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == TellerPaper.TypeID).ID;
                        TellerPaper.No = master.No;
                        TellerPaper.Date = master.Date;
                        TellerPaper.PostedDate = master.PostedDate;
                        TellerPaper.AccountingObjectID = master.AccountingObjectID;
                        TellerPaper.AccountingObjectName = master.AccountingObjectName;
                        TellerPaper.AccountingObjectAddress = master.AccountingObjectAddress;
                        TellerPaper.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID;
                        TellerPaper.AccountingObjectBankName = master.MAccountingObjectBankName;
                        TellerPaper.BankAccountDetailID = master.BankAccountDetailID;
                        TellerPaper.Receiver = master.MContactName;
                        TellerPaper.BankName = master.BankName;
                        TellerPaper.Reason = master.Reason;
                        TellerPaper.CurrencyID = master.CurrencyID;
                        TellerPaper.ExchangeRate = master.ExchangeRate;
                        TellerPaper.PaymentClauseID = master.PaymentClauseID;
                        TellerPaper.TotalAmount = master.TotalAmount;
                        TellerPaper.TotalAmountOriginal = master.TotalAmountOriginal;
                        TellerPaper.TotalVATAmount = master.TotalVATAmount;
                        TellerPaper.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        TellerPaper.EmployeeID = master.EmployeeID;
                        TellerPaper.Recorded = false;
                        TellerPaper.Exported = master.Exported;
                        TellerPaper.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        TellerPaper.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        if (khachhang != null)
                            TellerPaper.AccountingObjectType = khachhang.ObjectType ?? 0;
                        list_MBTellerPaper.Add(TellerPaper);
                    }
                    if (item.PhuongThucTT == "263")
                    {
                        master.TypeID = int.Parse(item.PhuongThucTT);
                        MBCreditCard CreditCard = new MBCreditCard();
                        CreditCard.TypeID = 171;
                        CreditCard.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == CreditCard.TypeID).ID;
                        CreditCard.No = master.No;
                        CreditCard.Date = master.Date;
                        CreditCard.PostedDate = master.PostedDate;
                        CreditCard.AccountingObjectID = master.AccountingObjectID;
                        CreditCard.AccountingObjectName = master.AccountingObjectName;
                        CreditCard.AccountingObjectAddress = master.AccountingObjectAddress;
                        CreditCard.AccountingObjectBankAccount = master.MAccountingObjectBankAccountDetailID;
                        CreditCard.AccountingObjectBankName = master.MAccountingObjectBankName;
                        if (khachhang != null)
                            CreditCard.AccountingObjectType = khachhang.ObjectType;
                        CreditCard.CreditCardNumber = master.CreditCardNumber;
                        CreditCard.Reason = master.Reason;
                        CreditCard.CurrencyID = master.CurrencyID;
                        CreditCard.ExchangeRate = master.ExchangeRate;
                        CreditCard.PaymentClauseID = master.PaymentClauseID;
                        CreditCard.TotalAmount = master.TotalAmount;
                        CreditCard.TotalAmountOriginal = master.TotalAmountOriginal;
                        CreditCard.TotalVATAmount = master.TotalVATAmount;
                        CreditCard.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        CreditCard.EmployeeID = master.EmployeeID;
                        CreditCard.Recorded = false;
                        CreditCard.Exported = master.Exported;
                        CreditCard.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        CreditCard.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MBCreditCard.Add(CreditCard);
                    }
                }
            }
            MH_QuaKho_model model = new MH_QuaKho_model();
            model.listPPInvoice = list_master;
            model.listMCPayment = list_MCPayment;
            model.listMBTellerPaper = list_MBTellerPaper;
            model.listMBCreditCard = list_MBCreditCard;
            return model;
        }
    }
}
