﻿using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.ControlsMapping.ConfigImport
{
    public class ConfigBanHang
    {
        public List<object> SetDataBaoGia(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();//
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();

            List<object> data_cv = new List<object>();
            List<SAQuote_Model> dt_cv = data.Cast<SAQuote_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.FinalDate = item_first.FinalDate;
                        t.MaKhachHang = item_first.MaKhachHang;
                        t.Reason = item_first.Reason;
                        t.ContactName = item_first.ContactName;
                        t.ContactMobile = item_first.ContactMobile;
                        t.ContactEmail = item_first.ContactEmail;
                        t.DeliveryTime = item_first.DeliveryTime;
                        t.GuaranteeDuration = item_first.GuaranteeDuration;
                        t.Description = item_first.Description;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.DieuKhoanTT = item_first.DieuKhoanTT;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.FinalDate,
                                 a.MaKhachHang,
                                 a.Reason,
                                 a.ContactName,
                                 a.ContactMobile,
                                 a.ContactEmail,
                                 a.DeliveryTime,
                                 a.GuaranteeDuration,
                                 a.Description,
                                 a.MaNhanVien,
                                 a.DieuKhoanTT,
                                 a.CurrencyID,
                                 a.ExchangeRate,

                             } into gr
                             select new SAQuote_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 FinalDate = gr.Key.FinalDate,
                                 MaKhachHang = gr.Key.MaKhachHang,
                                 Reason = gr.Key.Reason,
                                 ContactName = gr.Key.ContactName,
                                 ContactMobile = gr.Key.ContactMobile,
                                 ContactEmail = gr.Key.ContactEmail,
                                 DeliveryTime = gr.Key.DeliveryTime,
                                 GuaranteeDuration = gr.Key.GuaranteeDuration,
                                 Description = gr.Key.Description,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 DieuKhoanTT = gr.Key.DieuKhoanTT,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,

                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.MaHang,
                                  a.VATDescription,
                              } into gr
                              select new SAQuote_Model
                              {
                                  No = gr.Key.No,
                                  MaHang = gr.Key.MaHang,
                                  VATDescription = gr.Key.VATDescription,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  VATRate = gr.Sum(t => t.VATRate),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                              };
            List<SAQuote> dt_SAQuote = new List<SAQuote>();
            foreach (var item in list_group)
            {
                SAQuote master = new SAQuote();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.FinalDate = item.FinalDate;
                master.TypeID = 300;
                master.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == master.TypeID).ID;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaKhachHang);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                    master.CompanyTaxCode = khachhang.TaxCode;
                }
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                var dkthanhtoan = _IPaymentClauseService.Query.FirstOrDefault(t => t.PaymentClauseCode == item.DieuKhoanTT);
                if (dkthanhtoan != null)
                    master.PaymentClauseID = dkthanhtoan.ID;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.ContactMobile = item.ContactMobile;
                master.ContactEmail = item.ContactEmail;
                master.DeliveryTime = item.DeliveryTime;
                master.GuaranteeDuration = item.GuaranteeDuration;
                master.Description = item.Description;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<SAQuoteDetail> list_sadt = new List<SAQuoteDetail>();
                    foreach (var ct in list_dt)
                    {
                        SAQuoteDetail dtct = new SAQuoteDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            dtct.MaterialGoodsID = hanghoa.ID;
                            dtct.Description = hanghoa.MaterialGoodsName;
                            dtct.Unit = hanghoa.Unit;
                            dtct.Quantity = ct.Quantity;
                            dtct.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                            dtct.UnitPrice = ct.UnitPrice ?? 0;
                            dtct.Amount = ct.Amount ?? 0;
                            dtct.AmountOriginal = ct.AmountOriginal ?? 0;
                            dtct.VATAmount = ct.VATAmount ?? 0;
                            dtct.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                            dtct.VATRate = ct.VATRate;
                            dtct.DiscountRate = ct.DiscountRate;
                            dtct.DiscountAmount = ct.DiscountAmount ?? 0;
                            dtct.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                            if (khachhang != null)
                                dtct.AccountingObjectID = khachhang.ID;
                            dtct.VATDescription = ct.VATDescription;
                            list_sadt.Add(dtct);
                        }
                    }
                    master.SAQuoteDetails = list_sadt;
                    master.TotalAmount = list_sadt.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_sadt.Sum(t => t.AmountOriginal);
                    master.TotalVATAmount = list_sadt.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadt.Sum(t => t.VATAmountOriginal);
                    master.TotalDiscountAmount = list_sadt.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadt.Sum(t => t.DiscountAmountOriginal);
                }
                dt_SAQuote.Add(master);
            }
            data_cv = dt_SAQuote.Cast<object>().ToList();
            return data_cv;
        }
        public List<object> SetDataDonDatHang(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();//
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();

            List<object> data_cv = new List<object>();
            List<SAOrder_Model> dt_cv = data.Cast<SAOrder_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.MaKhachHang = item_first.MaKhachHang;
                        t.Reason = item_first.Reason;
                        t.ContactName = item_first.ContactName;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.MaKhachHang,
                                 a.Reason,
                                 a.ContactName,
                                 a.MaNhanVien,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                             } into gr
                             select new SAOrder_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 MaKhachHang = gr.Key.MaKhachHang,
                                 Reason = gr.Key.Reason,
                                 ContactName = gr.Key.ContactName,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.MaHang,
                                  a.VATDescription,
                                  a.MaPhongBan,
                                  a.MaMucThuChi,
                                  a.MaKhoanMucCP,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaThongKe
                              } into gr
                              select new SAOrder_Model
                              {
                                  No = gr.Key.No,
                                  MaHang = gr.Key.MaHang,
                                  VATDescription = gr.Key.VATDescription,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaThongKe = gr.Key.MaThongKe,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  VATRate = gr.Sum(t => t.VATRate),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                              };
            List<SAOrder> list_master = new List<SAOrder>();
            foreach (var item in list_group)
            {
                SAOrder master = new SAOrder();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.TypeID = 310;
                master.TemplateID = _ITemplateService.Query.FirstOrDefault(x => x.TypeID == master.TypeID).ID;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaKhachHang);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectAddress = khachhang.Address;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                }
                master.Reason = item.Reason;
                master.DeliveDate = item.DeliveDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<SAOrderDetail> list_sadt = new List<SAOrderDetail>();
                    foreach (var ct in list_dt)
                    {
                        SAOrderDetail ctiem = new SAOrderDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            ctiem.MaterialGoodsID = hanghoa.ID;
                            ctiem.Description = hanghoa.MaterialGoodsName;
                        }
                        ctiem.Quantity = ct.Quantity;
                        ctiem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        ctiem.UnitPrice = ct.UnitPrice ?? 0;
                        ctiem.Amount = ct.Amount ?? 0;
                        ctiem.AmountOriginal = ct.AmountOriginal ?? 0;
                        ctiem.VATRate = ct.VATRate ?? 0;
                        ctiem.VATAmount = ct.VATAmount ?? 0;
                        ctiem.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                        ctiem.DiscountRate = ct.DiscountRate ?? 0;
                        ctiem.DiscountAmount = ct.DiscountAmount ?? 0;
                        ctiem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        if (khachhang != null)
                            ctiem.AccountingObjectID = khachhang.ID;
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            ctiem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            ctiem.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            ctiem.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            ctiem.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            ctiem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            ctiem.BudgetItemID = mucthuchi.ID;
                        ctiem.VATDescription = ct.VATDescription;
                        list_sadt.Add(ctiem);
                    }
                    master.SAOrderDetails = list_sadt;
                    master.TotalAmount = list_sadt.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_sadt.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadt.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadt.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadt.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadt.Sum(t => t.VATAmountOriginal);
                }
                list_master.Add(master);
            }
            data_cv = list_master.Cast<object>().ToList();
            return data_cv;
        }
        public object SetDataBHChuaThuTien(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            ITT153ReportService _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            List<object> data_cv = new List<object>();
            List<SAInvoice_Model> dt_cv = data.Cast<SAInvoice_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaKhachHang = item_first.MaKhachHang;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.MaDKTT = item_first.MaDKTT;
                        t.Exported = item_first.Exported;
                        t.DueDate = item_first.DueDate;
                        t.OutwardNo = item_first.OutwardNo;
                        t.SReason = item_first.SReason;
                        t.OriginalNo = item_first.OriginalNo;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.AccountingObjectBankAccount = item_first.AccountingObjectBankAccount;
                        t.AccountingObjectBankName = item_first.AccountingObjectBankName;
                        t.PaymentMethod = item_first.PaymentMethod;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }

                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaKhachHang,
                                 a.ContactName,
                                 a.Reason,
                                 a.MaNhanVien,
                                 a.MaDKTT,
                                 a.Exported,
                                 a.DueDate,
                                 a.OutwardNo,
                                 a.SReason,
                                 a.OriginalNo,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.InvoiceTemplate,
                                 a.InvoiceSeries,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.AccountingObjectBankAccount,
                                 a.AccountingObjectBankName,
                                 a.PaymentMethod,

                             } into gr
                             select new SAInvoice_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaKhachHang = gr.Key.MaKhachHang,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 MaDKTT = gr.Key.MaDKTT,
                                 Exported = gr.Key.Exported,
                                 DueDate = gr.Key.DueDate,
                                 OutwardNo = gr.Key.OutwardNo,
                                 SReason = gr.Key.SReason,
                                 OriginalNo = gr.Key.OriginalNo,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 AccountingObjectBankAccount = gr.Key.AccountingObjectBankAccount,
                                 AccountingObjectBankName = gr.Key.AccountingObjectBankName,
                                 PaymentMethod = gr.Key.PaymentMethod,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.MaHang,
                                  a.IsPromotion,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.DiscountAccount,
                                  a.SAOrderNo,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.ExportTaxAccount,
                                  a.ExportTaxAccountCorresponding,
                                  a.VATAccount,
                                  a.VATRate,
                                  a.RepositoryAccount,
                                  a.CostAccount,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaPhongBan,
                                  a.MaKhoanMucCP,
                                  a.MaThongKe
                              } into gr
                              select new SAInvoice_Model
                              {
                                  No = gr.Key.No,
                                  MaHang = gr.Key.MaHang,
                                  IsPromotion = gr.Key.IsPromotion,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  DiscountAccount = gr.Key.DiscountAccount,
                                  SAOrderNo = gr.Key.SAOrderNo,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  ExportTaxAccount = gr.Key.ExportTaxAccount,
                                  ExportTaxAccountCorresponding = gr.Key.ExportTaxAccountCorresponding,
                                  VATAccount = gr.Key.VATAccount,
                                  VATRate = gr.Key.VATRate,
                                  VATAmount = gr.Sum(t => t.VATAmount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  RepositoryAccount = gr.Key.RepositoryAccount,
                                  CostAccount = gr.Key.CostAccount,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaThongKe = gr.Key.MaThongKe,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  ExportTaxRate = gr.Sum(t => t.ExportTaxRate),
                                  ExportTaxAmount = gr.Sum(t => t.ExportTaxAmount),

                              };
            List<SAInvoice> list_master = new List<SAInvoice>();
            List<RSInwardOutward> list_RSOut = new List<RSInwardOutward>();
            foreach (var item in list_group)
            {
                SAInvoice master = new SAInvoice();
                RSInwardOutward rS = new RSInwardOutward();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OutwardNo = item.OutwardNo;
                master.OriginalNo = item.OriginalNo;
                master.TypeID = 320;
                master.TemplateID = new Guid("E0089786-A334-4C44-8699-765552F9FCB3");
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaKhachHang);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                    master.CompanyTaxCode = khachhang.TaxCode;
                }
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.SDate = item.Date ?? (new DateTime()).Date;
                master.SPostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.SReason = item.SReason;
                master.IsDeliveryVoucher = true;
                master.InvoiceDate = item.InvoiceDate;
                master.InvoiceNo = item.InvoiceNo;
                master.InvoiceSeries = item.InvoiceSeries;
                master.InvoiceTemplate = item.InvoiceTemplate;
                master.AccountingObjectBankAccount = item.AccountingObjectBankAccount;
                master.AccountingObjectBankName = item.AccountingObjectBankName;
                master.PaymentMethod = item.PaymentMethod;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                var dkthanhtoan = _IPaymentClauseService.Query.FirstOrDefault(t => t.PaymentClauseCode == item.MaDKTT);
                if (dkthanhtoan != null)
                    master.PaymentClauseID = dkthanhtoan.ID;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.Exported = item.Exported == null ? false : (item.Exported == 1 ? true : false);
                master.RefDateTime = DateTime.Now;
                if (master.InvoiceTemplate != null && master.InvoiceSeries != null && master.InvoiceNo != null && master.InvoiceDate != null)
                {
                    master.IsBill = true;
                }
                else
                {
                    master.IsBill = false;
                }
                var tt153rp = _ITT153ReportService.Query.FirstOrDefault(x => x.InvoiceTemplate == item.InvoiceTemplate && x.InvoiceSeries == item.InvoiceSeries);
                if (tt153rp != null)
                {
                    master.InvoiceTypeID = tt153rp.InvoiceTypeID;
                    master.InvoiceForm = tt153rp.InvoiceForm;
                }
                master.PaymentMethod = item.PaymentMethod;
                master.AccountingObjectBankAccount = item.AccountingObjectBankAccount;
                master.AccountingObjectBankName = item.AccountingObjectBankName;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<SAInvoiceDetail> list_Sadt = new List<SAInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        SAInvoiceDetail sadt = new SAInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            sadt.MaterialGoodsID = hanghoa.ID;
                            sadt.RepositoryID = hanghoa.RepositoryID;
                            sadt.Description = hanghoa.MaterialGoodsName;
                            sadt.Unit = hanghoa.Unit;
                        }
                        sadt.DebitAccount = ct.DebitAccount;
                        sadt.CreditAccount = ct.CreditAccount;
                        sadt.Quantity = ct.Quantity;
                        sadt.UnitPrice = ct.UnitPrice ?? 0;
                        sadt.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        sadt.Amount = ct.Amount ?? 0;
                        sadt.AmountOriginal = ct.AmountOriginal ?? 0;
                        sadt.DiscountRate = ct.DiscountRate;
                        sadt.DiscountAmount = ct.DiscountAmount ?? 0;
                        sadt.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        sadt.DiscountAccount = ct.DiscountAccount;
                        sadt.VATRate = ct.VATRate;
                        sadt.VATAmount = ct.VATAmount ?? 0;
                        sadt.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                        sadt.VATAccount = ct.VATAccount;
                        sadt.RepositoryAccount = ct.RepositoryAccount;
                        sadt.CostAccount = ct.CostAccount;
                        sadt.ExpiryDate = ct.ExpiryDate;
                        sadt.LotNo = ct.LotNo;
                        if (khachhang != null)
                        {
                            sadt.AccountingObjectID = khachhang.ID;
                            sadt.CreditAccountingObjectID = khachhang.ID;
                        }
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            sadt.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            sadt.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongKe);
                        if (thongke != null)
                            sadt.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            sadt.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            sadt.ExpenseItemID = khoanmuccp.ID;
                        sadt.SAOrderNo = ct.SAOrderNo;
                        sadt.IsPromotion = ct.IsPromotion == null ? false : (ct.IsPromotion == 0 ? false : true);
                        sadt.TaxExchangeRate = item.ExchangeRate ?? 0;
                        sadt.ExportTaxRate = ct.ExportTaxRate ?? 0;
                        sadt.ExportTaxAmount = ct.ExportTaxAmount ?? 0;
                        sadt.ExportTaxAccount = ct.ExportTaxAccount;
                        sadt.VATDescription = ct.VATDescription;
                        sadt.ExportTaxAccountCorresponding = ct.ExportTaxAccountCorresponding;
                        list_Sadt.Add(sadt);
                    }
                    master.SAInvoiceDetails = list_Sadt;
                    master.TotalAmount = list_Sadt.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_Sadt.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_Sadt.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_Sadt.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_Sadt.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_Sadt.Sum(t => t.VATAmountOriginal);
                    master.TotalExportTaxAmount = list_Sadt.Sum(t => t.ExportTaxAmount);
                }
                rS.TypeID = 412;
                rS.No = master.OutwardNo;
                rS.Date = master.Date;
                rS.PostedDate = master.PostedDate;
                rS.AccountingObjectID = master.AccountingObjectID;
                rS.AccountingObjectName = master.AccountingObjectName;
                rS.AccountingObjectAddress = master.AccountingObjectAddress;
                rS.ContactName = master.ContactName;
                rS.Reason = master.Reason;
                rS.CurrencyID = master.CurrencyID;
                rS.ExchangeRate = master.ExchangeRate;
                rS.RefDateTime = master.PostedDate;
                rS.Recorded = false;
                rS.Exported = master.Exported;
                list_master.Add(master);
                list_RSOut.Add(rS);
            }
            BH_ThuNgay_model model = new BH_ThuNgay_model();
            model.listSAInvoice = list_master;
            model.list_RSOut = list_RSOut;
            //data_cv = list_master.Cast<object>().ToList();
            return model;
        }
        public object SetDataBHThuTienNgay(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            ITemplateService _ITemplateService = IoC.Resolve<ITemplateService>();
            ITT153ReportService _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            List<BHTHUTIENNGAY_Model> dt_cv = data.Cast<BHTHUTIENNGAY_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaKhachHang = item_first.MaKhachHang;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.MaDKTT = item_first.MaDKTT;
                        t.Exported = item_first.Exported;
                        t.DueDate = item_first.DueDate;
                        t.OutwardNo = item_first.OutwardNo;
                        t.SReason = item_first.SReason;
                        t.SContactName = item_first.SContactName;
                        t.OriginalNo = item_first.OriginalNo;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.MNo = item_first.MNo;
                        t.MContactName = item_first.MContactName;
                        t.MReasonPay = item_first.MReasonPay;
                        t.NumberAttach = item_first.NumberAttach;
                        t.MaTKNopTien = item_first.MaTKNopTien;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.PaymentMethod = item_first.PaymentMethod;
                        t.AccountingObjectBankAccount = item_first.AccountingObjectBankAccount;
                        t.AccountingObjectBankName = item_first.AccountingObjectBankName;
                        t.PhuongThucTT = item_first.PhuongThucTT;
                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaKhachHang,
                                 a.ContactName,
                                 a.Reason,
                                 a.MaNhanVien,
                                 a.MaDKTT,
                                 a.Exported,
                                 a.DueDate,
                                 a.OutwardNo,
                                 a.SReason,
                                 a.SContactName,
                                 a.OriginalNo,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.MNo,
                                 a.MContactName,
                                 a.MReasonPay,
                                 a.NumberAttach,
                                 a.MaTKNopTien,
                                 a.InvoiceTemplate,
                                 a.InvoiceSeries,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.PaymentMethod,
                                 a.AccountingObjectBankAccount,
                                 a.AccountingObjectBankName,
                                 a.PhuongThucTT,
                             } into gr
                             select new BHTHUTIENNGAY_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaKhachHang = gr.Key.MaKhachHang,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 MaDKTT = gr.Key.MaDKTT,
                                 Exported = gr.Key.Exported,
                                 DueDate = gr.Key.DueDate,
                                 OutwardNo = gr.Key.OutwardNo,
                                 SReason = gr.Key.SReason,
                                 SContactName = gr.Key.SContactName,
                                 OriginalNo = gr.Key.OriginalNo,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 MNo = gr.Key.MNo,
                                 MContactName = gr.Key.MContactName,
                                 MReasonPay = gr.Key.MReasonPay,
                                 NumberAttach = gr.Key.NumberAttach,
                                 MaTKNopTien = gr.Key.MaTKNopTien,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 PaymentMethod = gr.Key.PaymentMethod,
                                 AccountingObjectBankAccount = gr.Key.AccountingObjectBankAccount,
                                 AccountingObjectBankName = gr.Key.AccountingObjectBankName,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.MaTKNopTien,
                                  a.PaymentMethod,
                                  a.MaHang,
                                  a.IsPromotion,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.DiscountAccount,
                                  a.SAOrderNo,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.VATAccount,
                                  a.RepositoryAccount,
                                  a.CostAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaPhongBan,
                                  a.MaKhoanMucCP,
                                  a.MaThongke,
                                  a.ExportTaxAccount,
                                  a.ExportTaxAccountCorresponding,
                                  a.VATRate
                              } into gr
                              select new BHTHUTIENNGAY_Model
                              {
                                  No = gr.Key.No,
                                  MaTKNopTien = gr.Key.MaTKNopTien,
                                  PaymentMethod = gr.Key.PaymentMethod,
                                  MaHang = gr.Key.MaHang,
                                  IsPromotion = gr.Key.IsPromotion,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  DiscountAccount = gr.Key.DiscountAccount,
                                  SAOrderNo = gr.Key.SAOrderNo,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  VATAccount = gr.Key.VATAccount,
                                  RepositoryAccount = gr.Key.RepositoryAccount,
                                  CostAccount = gr.Key.CostAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaThongke = gr.Key.MaThongke,
                                  ExportTaxAccount = gr.Key.ExportTaxAccount,
                                  ExportTaxAccountCorresponding = gr.Key.ExportTaxAccountCorresponding,
                                  VATRate = gr.Key.VATRate,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  Amount = gr.Sum(t => t.Amount),
                                  DiscountRate = gr.Sum(t => t.DiscountRate),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  ExportTaxRate = gr.Sum(t => t.ExportTaxRate),
                                  ExportTaxAmount = gr.Sum(t => t.ExportTaxAmount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                              };
            List<SAInvoice> list_master = new List<SAInvoice>();
            List<RSInwardOutward> list_RSOut = new List<RSInwardOutward>();
            List<MCReceipt> list_MCReceipt = new List<MCReceipt>();
            List<MBDeposit> list_MBDeposit = new List<MBDeposit>();
            foreach (var item in list_group)
            {
                SAInvoice master = new SAInvoice();
                RSInwardOutward rS = new RSInwardOutward();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OriginalNo = item.OriginalNo;
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaKhachHang);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                    master.CompanyTaxCode = khachhang.TaxCode;
                }
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.SPostedDate = master.PostedDate;
                master.SDate = master.Date;
                master.SReason = item.SReason;
                master.InvoiceDate = item.InvoiceDate;
                master.InvoiceNo = item.InvoiceNo;
                master.InvoiceSeries = item.InvoiceSeries;
                master.InvoiceTemplate = item.InvoiceTemplate;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                master.OutwardNo = item.OutwardNo;
                master.SContactName = item.SContactName;
                master.MPostedDate = master.PostedDate;
                master.MNo = item.MNo;
                master.MDate = item.Date;
                master.TemplateID = new Guid("DEEE0050-E8FE-458E-9894-FE3ABF34CE39");
                master.MContactName = item.MContactName;
                master.MReasonPay = item.MReasonPay;
                master.NumberAttach = item.NumberAttach;
                var dkthanhtoan = _IPaymentClauseService.Query.FirstOrDefault(t => t.PaymentClauseCode == item.MaDKTT);
                if (dkthanhtoan != null)
                    master.PaymentClauseID = dkthanhtoan.ID;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                var tknop = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKNopTien);
                if (tknop != null)
                {
                    master.BankAccountDetailID = tknop.ID;
                    master.BankName = tknop.BankName;
                }
                var tt153rp = _ITT153ReportService.Query.FirstOrDefault(x => x.InvoiceTemplate == item.InvoiceTemplate && x.InvoiceSeries == item.InvoiceSeries);
                if (tt153rp != null)
                {
                    master.InvoiceTypeID = tt153rp.InvoiceTypeID;
                    master.InvoiceForm = tt153rp.InvoiceForm;
                }
                master.IsDeliveryVoucher = true;
                master.Exported = item.Exported == null ? false : (item.Exported == 0 ? false : true);
                master.RefDateTime = DateTime.Now;
                if (master.InvoiceTemplate != null && master.InvoiceSeries != null && master.InvoiceNo != null && master.InvoiceDate != null)
                {
                    master.IsBill = true;
                }
                else
                {
                    master.IsBill = false;
                }
                master.PaymentMethod = item.PaymentMethod;
                master.AccountingObjectBankAccount = item.AccountingObjectBankAccount;
                master.AccountingObjectBankName = item.AccountingObjectBankName;

                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<SAInvoiceDetail> list_sadetail = new List<SAInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        SAInvoiceDetail saitem = new SAInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            saitem.MaterialGoodsID = hanghoa.ID;
                            saitem.RepositoryID = hanghoa.RepositoryID;
                            saitem.Description = hanghoa.MaterialGoodsName;
                            saitem.Unit = hanghoa.Unit;
                        }
                        saitem.DebitAccount = ct.DebitAccount;
                        saitem.CreditAccount = ct.CreditAccount;
                        saitem.Quantity = ct.Quantity;
                        saitem.UnitPrice = ct.UnitPrice ?? 0;
                        saitem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        saitem.Amount = ct.Amount ?? 0;
                        saitem.AmountOriginal = ct.AmountOriginal ?? 0;
                        saitem.DiscountRate = ct.DiscountRate;
                        saitem.DiscountAmount = ct.DiscountAmount ?? 0;
                        saitem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        saitem.DiscountAccount = ct.DiscountAccount;
                        saitem.VATRate = ct.VATRate;
                        saitem.VATAmount = ct.VATAmount ?? 0;
                        saitem.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                        saitem.VATAccount = ct.VATAccount;
                        saitem.RepositoryAccount = ct.RepositoryAccount;
                        saitem.CostAccount = ct.CostAccount;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        if (khachhang != null)
                        {
                            saitem.AccountingObjectID = khachhang.ID;
                            saitem.CreditAccountingObjectID = khachhang.ID;
                        }
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            saitem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            saitem.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongke);
                        if (thongke != null)
                            saitem.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            saitem.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            saitem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            saitem.BudgetItemID = mucthuchi.ID;
                        saitem.SAOrderNo = ct.SAOrderNo;
                        saitem.IsPromotion = ct.IsPromotion == null ? false : (ct.IsPromotion == 0 ? false : true);
                        saitem.TaxExchangeRate = master.ExchangeRate ?? 0;
                        saitem.ExportTaxRate = ct.ExportTaxRate ?? 0;
                        saitem.ExportTaxAmount = ct.ExportTaxAmount ?? 0;
                        saitem.ExportTaxAccount = ct.ExportTaxAccount;
                        saitem.VATDescription = ct.VATDescription;
                        saitem.ExportTaxAccountCorresponding = ct.ExportTaxAccountCorresponding;
                        list_sadetail.Add(saitem);
                    }
                    master.SAInvoiceDetails = list_sadetail;
                    master.TotalAmount = list_sadetail.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_sadetail.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadetail.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadetail.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadetail.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadetail.Sum(t => t.VATAmountOriginal);
                    master.TotalExportTaxAmount = list_sadetail.Sum(t => t.ExportTaxAmount);

                    rS.TypeID = 412;
                    rS.No = master.OutwardNo;
                    rS.Date = master.Date;
                    rS.PostedDate = master.PostedDate;
                    rS.AccountingObjectID = master.AccountingObjectID;
                    rS.AccountingObjectName = master.AccountingObjectName;
                    rS.AccountingObjectAddress = master.AccountingObjectAddress;
                    rS.ContactName = master.ContactName;
                    rS.Reason = master.SReason;
                    rS.CurrencyID = master.CurrencyID;
                    rS.ExchangeRate = master.ExchangeRate;
                    rS.RefDateTime = master.PostedDate;
                    rS.Recorded = false;
                    rS.Exported = master.Exported;

                    if (item.PhuongThucTT == "321")
                    {
                        master.TypeID = 321;
                        MCReceipt receipt = new MCReceipt();
                        receipt.No = master.MNo;
                        receipt.TypeID = 102;
                        receipt.Date = master.Date;
                        receipt.PostedDate = master.PostedDate;
                        receipt.AccountingObjectID = master.AccountingObjectID;
                        receipt.AccountingObjectName = master.AccountingObjectName;
                        receipt.AccountingObjectAddress = master.AccountingObjectAddress;
                        receipt.Payers = master.MContactName;
                        receipt.Reason = master.MReasonPay;
                        receipt.NumberAttach = master.NumberAttach;
                        receipt.CurrencyID = master.CurrencyID;
                        receipt.ExchangeRate = master.ExchangeRate;
                        receipt.PaymentClauseID = master.PaymentClauseID;
                        receipt.InvoiceDate = master.InvoiceDate;
                        receipt.InvoiceNo = master.InvoiceNo;
                        receipt.InvoiceSeries = master.InvoiceSeries;
                        receipt.InvoiceTemplate = master.InvoiceTemplate;
                        receipt.EmployeeID = master.EmployeeID;
                        receipt.Recorded = master.Recorded;
                        receipt.Exported = master.Exported;
                        receipt.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        receipt.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MCReceipt.Add(receipt);
                    }
                    if (item.PhuongThucTT == "322")
                    {
                        master.TypeID = 322;
                        MBDeposit deposit = new MBDeposit();
                        deposit.TypeID = 162;
                        deposit.No = master.MNo;
                        deposit.Date = master.Date;
                        deposit.PostedDate = master.PostedDate;
                        deposit.AccountingObjectID = master.AccountingObjectID;
                        deposit.AccountingObjectName = master.AccountingObjectName;
                        deposit.AccountingObjectAddress = master.AccountingObjectAddress;
                        deposit.BankAccountDetailID = master.BankAccountDetailID;
                        deposit.BankName = master.BankName;
                        deposit.Reason = master.MReasonPay;
                        deposit.CurrencyID = master.CurrencyID;
                        deposit.ExchangeRate = master.ExchangeRate;
                        deposit.PaymentClauseID = master.PaymentClauseID;
                        deposit.InvoiceDate = master.InvoiceDate;
                        deposit.InvoiceNo = master.InvoiceNo;
                        deposit.InvoiceSeries = master.InvoiceSeries;
                        deposit.TotalAmount = master.TotalAmount;
                        deposit.TotalAmountOriginal = master.TotalAmountOriginal;
                        deposit.TotalVATAmount = master.TotalVATAmount;
                        deposit.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        deposit.EmployeeID = master.EmployeeID;
                        deposit.InvoiceTemplate = master.InvoiceTemplate;
                        deposit.Recorded = master.Recorded;
                        deposit.Exported = master.Exported;
                        deposit.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        deposit.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MBDeposit.Add(deposit);
                    }
                    list_master.Add(master);
                    list_RSOut.Add(rS);
                }
            }
            BH_ThuNgay_model model = new BH_ThuNgay_model();
            model.listSAInvoice = list_master;
            model.listMCReceipt = list_MCReceipt;
            model.listMBDeposit = list_MBDeposit;
            model.list_RSOut = list_RSOut;
            return model;
        }
        public object SetDataBHDaiLy(List<object> data)
        {
            IAccountingObjectService _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            IBankAccountDetailService _IBankAccountDetailService = IoC.Resolve<IBankAccountDetailService>();
            IPaymentClauseService _IPaymentClauseService = IoC.Resolve<IPaymentClauseService>();
            IMaterialGoodsService _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            ICostSetService _ICostSetService = IoC.Resolve<ICostSetService>();
            IEMContractService _IEMContractService = IoC.Resolve<IEMContractService>();
            IStatisticsCodeService _IStatisticsCodeService = IoC.Resolve<IStatisticsCodeService>();
            IDepartmentService _IDepartmentService = IoC.Resolve<IDepartmentService>();
            IExpenseItemService _IExpenseItemService = IoC.Resolve<IExpenseItemService>();
            IBudgetItemService _IBudgetItemService = IoC.Resolve<IBudgetItemService>();
            ICurrencyService _ICurrencyService = IoC.Resolve<ICurrencyService>();
            ITT153ReportService _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            List<BHDAILYTHUDUNGGIA_Model> dt_cv = data.Cast<BHDAILYTHUDUNGGIA_Model>().ToList();
            var list_so_ctu = dt_cv.Select(t => t.No).Distinct();
            int dem = 0;
            foreach (var item in list_so_ctu)
            {
                var item_first = dt_cv.FirstOrDefault(t => t.No == item);
                dem++;
                dt_cv.ForEach(t =>
                {
                    if (t.No == item_first.No)
                    {
                        t.Date = item_first.Date;
                        t.PostedDate = item_first.PostedDate;
                        t.MaKhachHang = item_first.MaKhachHang;
                        t.ContactName = item_first.ContactName;
                        t.Reason = item_first.Reason;
                        t.MaNhanVien = item_first.MaNhanVien;
                        t.MaDKTT = item_first.MaDKTT;
                        t.Exported = item_first.Exported;
                        t.DueDate = item_first.DueDate;
                        t.OutwardNo = item_first.OutwardNo;
                        t.SReason = item_first.SReason;
                        t.SContactName = item_first.SContactName;
                        t.OriginalNo = item_first.OriginalNo;
                        t.ExchangeRate = item_first.ExchangeRate;
                        t.CurrencyID = string.IsNullOrEmpty(item_first.CurrencyID) ? "VND" : item_first.CurrencyID;
                        if (t.ExchangeRate == null)
                        {
                            var current = _ICurrencyService.Query.FirstOrDefault(t1 => t1.ID == t.CurrencyID);
                            if (current != null)
                                t.ExchangeRate = current.ExchangeRate;
                        }
                        t.PhuongThucTT = item_first.PhuongThucTT;
                        t.MReasonPay = item_first.MReasonPay;
                        t.MContactName = item_first.MContactName;
                        t.MaTKNopTien = item_first.MaTKNopTien;
                        t.NumberAttach = item_first.NumberAttach;
                        t.InvoiceTemplate = item_first.InvoiceTemplate;
                        t.InvoiceSeries = item_first.InvoiceSeries;
                        t.InvoiceNo = item_first.InvoiceNo;
                        t.InvoiceDate = item_first.InvoiceDate;
                        t.AccountingObjectBankAccount = item_first.AccountingObjectBankAccount;
                        t.AccountingObjectBankName = item_first.AccountingObjectBankName;
                        t.PaymentMethod = item_first.PaymentMethod;
                        t.MNo = item_first.MNo;
                    }
                });
            }
            var list_group = from a in dt_cv
                             group a by new
                             {
                                 a.No,
                                 a.Date,
                                 a.PostedDate,
                                 a.MaKhachHang,
                                 a.ContactName,
                                 a.Reason,
                                 a.MaNhanVien,
                                 a.MaDKTT,
                                 a.Exported,
                                 a.DueDate,
                                 a.OutwardNo,
                                 a.SReason,
                                 a.SContactName,
                                 a.OriginalNo,
                                 a.CurrencyID,
                                 a.ExchangeRate,
                                 a.PhuongThucTT,
                                 a.MReasonPay,
                                 a.MContactName,
                                 a.MaTKNopTien,
                                 a.NumberAttach,
                                 a.InvoiceTemplate,
                                 a.InvoiceSeries,
                                 a.InvoiceNo,
                                 a.InvoiceDate,
                                 a.AccountingObjectBankAccount,
                                 a.AccountingObjectBankName,
                                 a.PaymentMethod,
                                 a.MNo
                             } into gr
                             select new BHDAILYTHUDUNGGIA_Model
                             {
                                 No = gr.Key.No,
                                 Date = gr.Key.Date,
                                 PostedDate = gr.Key.PostedDate,
                                 MaKhachHang = gr.Key.MaKhachHang,
                                 ContactName = gr.Key.ContactName,
                                 Reason = gr.Key.Reason,
                                 MaNhanVien = gr.Key.MaNhanVien,
                                 MaDKTT = gr.Key.MaDKTT,
                                 Exported = gr.Key.Exported,
                                 DueDate = gr.Key.DueDate,
                                 OutwardNo = gr.Key.OutwardNo,
                                 SReason = gr.Key.SReason,
                                 SContactName = gr.Key.SContactName,
                                 OriginalNo = gr.Key.OriginalNo,
                                 CurrencyID = gr.Key.CurrencyID,
                                 ExchangeRate = gr.Key.ExchangeRate,
                                 PhuongThucTT = gr.Key.PhuongThucTT,
                                 MReasonPay = gr.Key.MReasonPay,
                                 MContactName = gr.Key.MContactName,
                                 MaTKNopTien = gr.Key.MaTKNopTien,
                                 NumberAttach = gr.Key.NumberAttach,
                                 InvoiceTemplate = gr.Key.InvoiceTemplate,
                                 InvoiceSeries = gr.Key.InvoiceSeries,
                                 InvoiceNo = gr.Key.InvoiceNo,
                                 InvoiceDate = gr.Key.InvoiceDate,
                                 AccountingObjectBankAccount = gr.Key.AccountingObjectBankAccount,
                                 AccountingObjectBankName = gr.Key.AccountingObjectBankName,
                                 PaymentMethod = gr.Key.PaymentMethod,
                                 MNo = gr.Key.MNo
                             };
            var list_detail = from a in dt_cv
                              group a by new
                              {
                                  a.No,
                                  a.MaHang,
                                  a.IsPromotion,
                                  a.DebitAccount,
                                  a.CreditAccount,
                                  a.DiscountRate,
                                  a.DiscountAccount,
                                  a.SAOrderNo,
                                  a.LotNo,
                                  a.ExpiryDate,
                                  a.VATDescription,
                                  a.ExportTaxRate,
                                  a.ExportTaxAccount,
                                  a.ExportTaxAccountCorresponding,
                                  a.VATRate,
                                  a.VATAccount,
                                  a.RepositoryAccount,
                                  a.CostAccount,
                                  a.MaMucThuChi,
                                  a.MaDoiTuongCP,
                                  a.MaHopDong,
                                  a.MaPhongBan,
                                  a.MaKhoanMucCP,
                                  a.MaThongke
                              } into gr
                              select new BHDAILYTHUDUNGGIA_Model
                              {
                                  No = gr.Key.No,
                                  MaHang = gr.Key.MaHang,
                                  IsPromotion = gr.Key.IsPromotion,
                                  DebitAccount = gr.Key.DebitAccount,
                                  CreditAccount = gr.Key.CreditAccount,
                                  DiscountRate = gr.Key.DiscountRate,
                                  DiscountAccount = gr.Key.DiscountAccount,
                                  SAOrderNo = gr.Key.SAOrderNo,
                                  LotNo = gr.Key.LotNo,
                                  ExpiryDate = gr.Key.ExpiryDate,
                                  VATDescription = gr.Key.VATDescription,
                                  ExportTaxRate = gr.Key.ExportTaxRate,
                                  ExportTaxAccount = gr.Key.ExportTaxAccount,
                                  ExportTaxAccountCorresponding = gr.Key.ExportTaxAccountCorresponding,
                                  VATRate = gr.Key.VATRate,
                                  VATAccount = gr.Key.VATAccount,
                                  RepositoryAccount = gr.Key.RepositoryAccount,
                                  CostAccount = gr.Key.CostAccount,
                                  MaMucThuChi = gr.Key.MaMucThuChi,
                                  MaDoiTuongCP = gr.Key.MaDoiTuongCP,
                                  MaHopDong = gr.Key.MaHopDong,
                                  MaPhongBan = gr.Key.MaPhongBan,
                                  MaKhoanMucCP = gr.Key.MaKhoanMucCP,
                                  MaThongke = gr.Key.MaThongke,
                                  Quantity = gr.Sum(t => t.Quantity),
                                  UnitPriceOriginal = gr.Sum(t => t.UnitPriceOriginal),
                                  UnitPrice = gr.Sum(t => t.UnitPrice),
                                  Amount = gr.Sum(t => t.Amount),
                                  AmountOriginal = gr.Sum(t => t.AmountOriginal),
                                  DiscountAmountOriginal = gr.Sum(t => t.DiscountAmountOriginal),
                                  DiscountAmount = gr.Sum(t => t.DiscountAmount),
                                  ExportTaxAmount = gr.Sum(t => t.ExportTaxAmount),
                                  VATAmountOriginal = gr.Sum(t => t.VATAmountOriginal),
                                  VATAmount = gr.Sum(t => t.VATAmount),
                              };
            List<SAInvoice> list_master = new List<SAInvoice>();
            List<MCReceipt> list_MCReceipt = new List<MCReceipt>();
            List<MBDeposit> list_MBDeposit = new List<MBDeposit>();
            List<RSInwardOutward> list_RSOut = new List<RSInwardOutward>();
            foreach (var item in list_group)
            {
                SAInvoice master = new SAInvoice();
                RSInwardOutward rS = new RSInwardOutward();
                master.No = item.No;
                master.Date = item.Date ?? (new DateTime()).Date;
                master.PostedDate = item.PostedDate ?? (new DateTime()).Date;
                master.OutwardNo = item.OutwardNo;
                master.OriginalNo = item.OriginalNo;
                master.SReason = item.SReason;
                master.IsDeliveryVoucher = true;
                master.TemplateID = new Guid("FF079521-A8DB-46B9-A6DB-67D8DEA4F608");
                var khachhang = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaKhachHang);
                if (khachhang != null)
                {
                    master.AccountingObjectID = khachhang.ID;
                    master.AccountingObjectName = khachhang.AccountingObjectName;
                    master.AccountingObjectAddress = khachhang.Address;
                    master.CompanyTaxCode = khachhang.TaxCode;
                }
                master.ContactName = item.ContactName;
                master.Reason = item.Reason;
                master.SPostedDate = master.PostedDate;
                master.SDate = master.Date;
                master.SContactName = item.SContactName;
                master.MPostedDate = master.PostedDate;
                master.MNo = item.MNo;
                master.MDate = master.Date;
                master.MContactName = item.MContactName;
                master.MReasonPay = item.MReasonPay;
                master.NumberAttach = item.NumberAttach;
                var tknop = _IBankAccountDetailService.Query.FirstOrDefault(t => t.BankAccount == item.MaTKNopTien);
                if (tknop != null)
                {
                    master.BankAccountDetailID = tknop.ID;
                    master.BankName = tknop.BankName;
                }
                master.InvoiceDate = item.InvoiceDate;
                master.InvoiceNo = item.InvoiceNo;
                master.InvoiceSeries = item.InvoiceSeries;
                master.InvoiceTemplate = item.InvoiceTemplate;
                master.CurrencyID = item.CurrencyID;
                master.ExchangeRate = item.ExchangeRate;
                var dkthanhtoan = _IPaymentClauseService.Query.FirstOrDefault(t => t.PaymentClauseCode == item.MaDKTT);
                if (dkthanhtoan != null)
                    master.PaymentClauseID = dkthanhtoan.ID;
                master.DueDate = item.DueDate;
                var nhanvien = _IAccountingObjectService.Query.FirstOrDefault(t => t.AccountingObjectCode == item.MaNhanVien);
                if (nhanvien != null)
                    master.EmployeeID = nhanvien.ID;
                master.Exported = item.Exported == null ? false : (item.Exported == 0 ? false : true);
                master.RefDateTime = DateTime.Now;
                if (master.InvoiceTemplate != null && master.InvoiceSeries != null && master.InvoiceNo != null && master.InvoiceDate != null)
                {
                    master.IsBill = true;
                }
                else
                {
                    master.IsBill = false;
                }
                var tt153rp = _ITT153ReportService.Query.FirstOrDefault(x => x.InvoiceTemplate == item.InvoiceTemplate && x.InvoiceSeries == item.InvoiceSeries);
                if (tt153rp != null)
                {
                    master.InvoiceTypeID = tt153rp.InvoiceTypeID;
                    master.InvoiceForm = tt153rp.InvoiceForm;
                }
                master.PaymentMethod = item.PaymentMethod;
                master.AccountingObjectBankAccount = item.AccountingObjectBankAccount;
                master.AccountingObjectBankName = item.AccountingObjectBankName;
                var list_dt = list_detail.Where(t => t.No == item.No);
                if (list_dt.Count() != 0)
                {
                    List<SAInvoiceDetail> list_sadetail = new List<SAInvoiceDetail>();
                    foreach (var ct in list_dt)
                    {
                        SAInvoiceDetail saitem = new SAInvoiceDetail();
                        var hanghoa = _IMaterialGoodsService.Query.FirstOrDefault(t => t.MaterialGoodsCode == ct.MaHang);
                        if (hanghoa != null)
                        {
                            saitem.MaterialGoodsID = hanghoa.ID;
                            saitem.RepositoryID = hanghoa.RepositoryID;
                            saitem.Description = hanghoa.MaterialGoodsName;
                            saitem.Unit = hanghoa.Unit;
                        }
                        saitem.DebitAccount = ct.DebitAccount;
                        saitem.CreditAccount = ct.CreditAccount;
                        saitem.Quantity = ct.Quantity;
                        saitem.UnitPrice = ct.UnitPrice ?? 0;
                        saitem.UnitPriceOriginal = ct.UnitPriceOriginal ?? 0;
                        saitem.Amount = ct.Amount ?? 0;
                        saitem.AmountOriginal = ct.AmountOriginal ?? 0;
                        saitem.DiscountRate = ct.DiscountRate;
                        saitem.DiscountAmount = ct.DiscountAmount ?? 0;
                        saitem.DiscountAmountOriginal = ct.DiscountAmountOriginal ?? 0;
                        saitem.DiscountAccount = ct.DiscountAccount;
                        saitem.VATRate = ct.VATRate;
                        saitem.VATAmount = ct.VATAmount ?? 0;
                        saitem.VATAmountOriginal = ct.VATAmountOriginal ?? 0;
                        saitem.VATAccount = ct.VATAccount;
                        saitem.RepositoryAccount = ct.RepositoryAccount;
                        saitem.CostAccount = ct.CostAccount;
                        saitem.ExpiryDate = ct.ExpiryDate;
                        saitem.LotNo = ct.LotNo;
                        if (khachhang != null)
                        {
                            saitem.AccountingObjectID = khachhang.ID;
                            saitem.CreditAccountingObjectID = khachhang.ID;
                        }
                        var dttaphopcp = _ICostSetService.Query.FirstOrDefault(t => t.CostSetCode == ct.MaDoiTuongCP);
                        if (dttaphopcp != null)
                            saitem.CostSetID = dttaphopcp.ID;
                        var hopdong = _IEMContractService.Query.FirstOrDefault(t => t.Code == ct.MaHopDong);
                        if (hopdong != null)
                            saitem.ContractID = hopdong.ID;
                        var thongke = _IStatisticsCodeService.Query.FirstOrDefault(t => t.StatisticsCode_ == ct.MaThongke);
                        if (thongke != null)
                            saitem.StatisticsCodeID = thongke.ID;
                        var phongban = _IDepartmentService.Query.FirstOrDefault(t => t.DepartmentCode == ct.MaPhongBan);
                        if (phongban != null)
                            saitem.DepartmentID = phongban.ID;
                        var khoanmuccp = _IExpenseItemService.Query.FirstOrDefault(t => t.ExpenseItemCode == ct.MaKhoanMucCP);
                        if (khoanmuccp != null)
                            saitem.ExpenseItemID = khoanmuccp.ID;
                        var mucthuchi = _IBudgetItemService.Query.FirstOrDefault(t => t.BudgetItemCode == ct.MaMucThuChi);
                        if (mucthuchi != null)
                            saitem.BudgetItemID = mucthuchi.ID;
                        saitem.SAOrderNo = ct.SAOrderNo;
                        saitem.IsPromotion = ct.IsPromotion == null ? false : (ct.IsPromotion == 0 ? false : true);
                        saitem.TaxExchangeRate = master.ExchangeRate ?? 0;
                        saitem.ExportTaxRate = ct.ExportTaxRate ?? 0;
                        saitem.ExportTaxAmount = ct.ExportTaxAmount ?? 0;
                        saitem.ExportTaxAccount = ct.ExportTaxAccount;
                        saitem.VATDescription = ct.VATDescription;
                        saitem.ExportTaxAccountCorresponding = ct.ExportTaxAccountCorresponding;
                        list_sadetail.Add(saitem);
                    }
                    master.SAInvoiceDetails = list_sadetail;
                    master.TotalAmount = list_sadetail.Sum(t => t.Amount);
                    master.TotalAmountOriginal = list_sadetail.Sum(t => t.AmountOriginal);
                    master.TotalDiscountAmount = list_sadetail.Sum(t => t.DiscountAmount);
                    master.TotalDiscountAmountOriginal = list_sadetail.Sum(t => t.DiscountAmountOriginal);
                    master.TotalVATAmount = list_sadetail.Sum(t => t.VATAmount);
                    master.TotalVATAmountOriginal = list_sadetail.Sum(t => t.VATAmountOriginal);
                    master.TotalExportTaxAmount = list_sadetail.Sum(t => t.ExportTaxAmount);
                    master.TypeID = Convert.ToInt32(item.PhuongThucTT);

                    rS.TypeID = 412;
                    rS.No = master.OutwardNo;
                    rS.Date = master.Date;
                    rS.PostedDate = master.PostedDate;
                    rS.AccountingObjectID = master.AccountingObjectID;
                    rS.AccountingObjectName = master.AccountingObjectName;
                    rS.AccountingObjectAddress = master.AccountingObjectAddress;
                    rS.ContactName = master.ContactName;
                    rS.Reason = master.SReason;
                    rS.CurrencyID = master.CurrencyID;
                    rS.ExchangeRate = master.ExchangeRate;
                    rS.RefDateTime = master.PostedDate;
                    rS.Recorded = false;
                    rS.Exported = master.Exported;

                    if (item.PhuongThucTT == "324")
                    {
                        MCReceipt receipt = new MCReceipt();
                        receipt.No = master.MNo;
                        receipt.TypeID = 103;
                        receipt.Date = master.Date;
                        receipt.PostedDate = master.PostedDate;
                        receipt.AccountingObjectID = master.AccountingObjectID;
                        receipt.AccountingObjectName = master.AccountingObjectName;
                        receipt.AccountingObjectAddress = master.AccountingObjectAddress;
                        receipt.Payers = master.MContactName;
                        receipt.Reason = master.Reason;
                        receipt.NumberAttach = master.NumberAttach;
                        receipt.CurrencyID = master.CurrencyID;
                        receipt.ExchangeRate = master.ExchangeRate;
                        receipt.PaymentClauseID = master.PaymentClauseID;
                        receipt.InvoiceDate = master.InvoiceDate;
                        receipt.InvoiceNo = master.InvoiceNo;
                        receipt.InvoiceSeries = master.InvoiceSeries;
                        receipt.InvoiceTemplate = master.InvoiceTemplate;
                        receipt.EmployeeID = master.EmployeeID;
                        receipt.Recorded = master.Recorded;
                        receipt.Exported = master.Exported;
                        receipt.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        receipt.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MCReceipt.Add(receipt);
                    }
                    if (item.PhuongThucTT == "325")
                    {
                        MBDeposit deposit = new MBDeposit();
                        deposit.No = master.MNo;
                        deposit.TypeID = 162;
                        deposit.Date = master.Date;
                        deposit.PostedDate = master.PostedDate;
                        deposit.AccountingObjectID = master.AccountingObjectID;
                        deposit.AccountingObjectName = master.AccountingObjectName;
                        deposit.AccountingObjectAddress = master.AccountingObjectAddress;
                        deposit.BankAccountDetailID = master.BankAccountDetailID;
                        deposit.BankName = master.BankName;
                        deposit.Reason = master.Reason;
                        deposit.CurrencyID = master.CurrencyID;
                        deposit.ExchangeRate = master.ExchangeRate;
                        deposit.PaymentClauseID = master.PaymentClauseID;
                        deposit.InvoiceDate = master.InvoiceDate;
                        deposit.InvoiceNo = master.InvoiceNo;
                        deposit.InvoiceSeries = master.InvoiceSeries;
                        deposit.TotalAmount = master.TotalAmount;
                        deposit.TotalAmountOriginal = master.TotalAmountOriginal;
                        deposit.TotalVATAmount = master.TotalVATAmount;
                        deposit.TotalVATAmountOriginal = master.TotalVATAmountOriginal;
                        deposit.EmployeeID = master.EmployeeID;
                        deposit.InvoiceTemplate = master.InvoiceTemplate;
                        deposit.Recorded = master.Recorded;
                        deposit.Exported = master.Exported;
                        deposit.TotalAll = master.TotalAmount - master.TotalDiscountAmount + master.TotalVATAmount;
                        deposit.TotalAllOriginal = master.TotalAmountOriginal - master.TotalDiscountAmountOriginal + master.TotalVATAmountOriginal;
                        list_MBDeposit.Add(deposit);
                    }
                    list_master.Add(master);
                    list_RSOut.Add(rS);
                }
            }
            BH_ThuNgay_model model = new BH_ThuNgay_model();
            model.listSAInvoice = list_master;
            model.listMCReceipt = list_MCReceipt;
            model.listMBDeposit = list_MBDeposit;
            model.list_RSOut = list_RSOut;
            return model;
        }
    }
}
