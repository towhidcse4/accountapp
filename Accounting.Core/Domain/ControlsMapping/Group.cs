﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.ControlsMapping
{
    public class Group
    {
        public string KeyCode { get; set; }
        public string Caption { get; set; }
        public string ToolTip { get; set; }
        public int Position { get; set; }
        public bool IsVisible { get; set; }
        public bool IsSecurity { get; set; }
    }
}
