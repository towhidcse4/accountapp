﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.ControlsMapping
{
    [Serializable]
    public class Item
    {
        public string ItemKey { get; set; }
        public string GroupKey { get; set; }
        public string Caption { get; set; }
        public byte[] Image { get; set; }
        public string MappingCode { get; set; }
        public int Position { get; set; }
    }
    public class ItemCBB { 
        public int key { get; set; }
        public string value { get; set; }
    }
}
