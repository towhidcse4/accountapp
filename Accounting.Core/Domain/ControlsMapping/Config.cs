﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.ControlsMapping
{
    public class Config
    {
        public Guid UserId { get; set; }
        public List<Group> Groups { get; set; }
        public List<Item> Items { get; set; }
        public Config()
        {
            Groups = new List<Group>();
            Items = new List<Item>();
        }
    }
}
