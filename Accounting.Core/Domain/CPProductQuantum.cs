using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPProductQuantum
    {
        private Guid _ID;
        private Decimal _DirectMaterialAmount;
        private Decimal _DirectLaborAmount;
        private Decimal _GeneralExpensesAmount;
        private Decimal _TotalCostAmount;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Decimal DirectMaterialAmount
        {
            get { return _DirectMaterialAmount; }
            set { _DirectMaterialAmount = value; }
        }

        public virtual Decimal DirectLaborAmount
        {
            get { return _DirectLaborAmount; }
            set { _DirectLaborAmount = value; }
        }

        public virtual Decimal GeneralExpensesAmount
        {
            get { return _GeneralExpensesAmount; }
            set { _GeneralExpensesAmount = value; }
        }

        public virtual Decimal TotalCostAmount
        {
            get { return _TotalCostAmount; }
            set { _TotalCostAmount = value; }
        }


    }
}
