﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMContractDetailPayment
    {
        private Guid _ID;
        private Guid _EMContractID;
        private DateTime? _RecordDate;
        private Guid? _DepartmentID;
        private Guid? _ExpenseItemID;
        private string _Description;
        private decimal _Amount;
        private decimal _AmountOriginal;
        private decimal _PercentAmount;
        private int _Methods;
        private string _methods;
        private decimal _AmountBeforeCancel;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid EMContractID
        {
            get { return _EMContractID; }
            set { _EMContractID = value; }
        }

        public virtual DateTime? RecordDate
        {
            get { return _RecordDate; }
            set { _RecordDate = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual decimal PercentAmount
        {
            get { return _PercentAmount; }
            set { _PercentAmount = value; }
        }

        public virtual int Methods
        {
            get { return _Methods; }
            set { _Methods = value; }
        }

        public virtual string Method
        {
            get
            {
                return (Methods == 0) ? "Tỷ lệ %" : "Số tiền";  
            }
            set { _methods = value; }
        }
        public virtual decimal AmountBeforeCancel
        {
            get { return _AmountBeforeCancel; }
            set { _AmountBeforeCancel = value; }
        }

    }
}
