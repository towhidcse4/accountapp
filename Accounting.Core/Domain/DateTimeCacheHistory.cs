using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class DateTimeCacheHistory
    {
        private Guid iD;
        private Guid iDUser;
        private int? typeID;
        private string subSystemCode;
        private int? selectedItem;
        private DateTime? dtBeginDate;
        private DateTime? dtEndDate;
        private int? monthOrPrecious;
        private int? month;
        private int? precious;
        private int? year;



        public DateTimeCacheHistory() { }
        
        
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid IDUser
        {
            get { return iDUser; }
            set { iDUser = value; }
        }

        public virtual int? TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual int? Month
        {
            get { return month; }
            set { month = value; }
        }
        public virtual int? MonthOrPrecious
        {
            get { return monthOrPrecious; }
            set { monthOrPrecious = value; }
        }
        public virtual int? Precious
        {
            get { return precious; }
            set { precious = value; }
        }
        public virtual int? Year
        {
            get { return year; }
            set { year = value; }
        }

        public virtual string SubSystemCode
        {
            get { return subSystemCode; }
            set { subSystemCode = value; }
        }

        public virtual int? SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; }
        }

        public virtual DateTime? DtBeginDate
        {
            get { return dtBeginDate; }
            set { dtBeginDate = value; }
        }

        public virtual DateTime? DtEndDate
        {
            get { return dtEndDate; }
            set { dtEndDate = value; }
        }

        
    }
}