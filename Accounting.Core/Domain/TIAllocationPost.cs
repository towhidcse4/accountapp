using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIAllocationPost
    {
        private Guid _ID;
        private Guid _TIAllocationID;
        private string _Description;
        private Guid _ToolsID;
        private string _DebitAccount;
        private string _CreditAccount;
        private Decimal _Amount;
        private Decimal _AmountOriginal;
        private Guid? _DepartmentID;
        private Guid? _CostSetID;
        private Guid? _ExpenseItemID;
        private Guid? _BudgetItemID;
        private Guid? _StatisticsCodeID;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TIAllocationID
        {
            get { return _TIAllocationID; }
            set { _TIAllocationID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Guid ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return _BudgetItemID; }
            set { _BudgetItemID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
