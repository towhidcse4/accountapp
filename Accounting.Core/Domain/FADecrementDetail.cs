﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADecrementDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid FADecrementID { get; set; }
        public virtual Guid? FixedAssetID { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Boolean IsIrrationalCost { get; set; }
        public virtual int OrderPriority { get; set; }
        public virtual decimal? OriginalPrice { get; set; }
        public virtual decimal? PurchasePrice { get; set; }
        public virtual decimal? AcDepreciationAmount { get; set; }
        public virtual decimal? RemainingAmount { get; set; }
        public virtual string OriginalPriceAccount { get; set; }
        public virtual string DepreciationAccount { get; set; }
        public virtual string ExpenditureAccount { get; set; }
        public virtual string HandlingResidualValueAccount { get; set; }

        public virtual IList<FADecrementDetailPost> FADecrementDetailPosts { get; set; }
        public FADecrementDetail()
        {
            FADecrementDetailPosts = new List<FADecrementDetailPost>();
        }
    }
}
