﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BookDepositListDetail
    {
        public int? RowNum { get; set; }
        public Guid RefID { get; set; }
        public string BankAccount { get; set; }
        public DateTime? PostedDate { get; set; }
        public DateTime? RefDate { get; set; }
        public string RefNo { get; set; }
        public int RefType { get; set; }
        public string JournalMemo { get; set; }
        public string CorrespondingAccountNumber { get; set; }
        public decimal? ExchangeRate { get; set; }
        public decimal? DebitAmountOC { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? CreditAmountOC { get; set; }
        public decimal? CreditAmount { get; set; }
        public decimal? ClosingAmountOC { get; set; }
        public decimal? ClosingAmount { get; set; }
        public string AccountObjectCode { get; set; }
        public string AccountObjectName { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string ProjectWorkCode { get; set; }
        public string ProjectWorkName { get; set; }
        public string ExpenseItemCode { get; set; }
        public string ExpenseItemName { get; set; }
        public string ListItemCode { get; set; }
        public string ListItemName { get; set; }
        public string ContractCode { get; set; }
        public Guid? BranchID { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public bool IsBold { get; set; }
        public int? PaymentType { get; set; }
        public int? OrderType { get; set; }
        public int? RefOrder { get; set; }

    }
    public class BookDepositListDetail_period
    {
        public string Period { get; set; }
    }
}
