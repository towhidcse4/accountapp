﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SynthesisReport
    {
        private Guid _ID;
        private int _TypeID;
        private string _ItemCode;       //mã chỉ tiêu
        private string _ItemName;       //tên chỉ tiêu
        private int _ItemIndex;         //số thứ tự
        private string _Description;
        private Boolean _IsUsedFormula;
        private Guid? _ParentID;
        private string _Formula1;
        private string _Formula2;
        private string _Formula3;
        private string _Formula4;
        private string _Formula5;
        private string _Formula6;
        private string _Formula7;
        private string _Formula8;
        private string _Formula9;
        private string _Formula10;
        private string _Formula11;
        private string _Formula12;
        private string _Formula13;
        private string _Formula14;
        private string _Formula15;
        private string _Formula16;
        private string _Formula17;
        private string _Formula18;
        private string _Formula19;
        private string _Formula20;
        private string _Formula21;
        private string _Formula22;
        private string _Formula23;
        private string _Formula24;
        private string _Formula25;
        private string _Formula26;
        private string _Formula27;
        private string _Formula28;
        private string _Formula29;
        private string _Formula30;
        private int _TotalFormulaUse;
        private Boolean _IsBold;            //in đậm
        private Boolean _IsItalic;          //in nghiêng
        private int _DeterministicPolicy;
        private string _ItemNameEnglish;
        private Boolean _IsHidden;          //ẩn cột
        private Boolean _IsDefault;
        private int _FormulaOnGeneralIndicator;
        private int _FormulaOfIndicator;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string ItemCode
        {
            get { return _ItemCode; }
            set { _ItemCode = value; }
        }

        public virtual string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }

        public virtual int ItemIndex
        {
            get { return _ItemIndex; }
            set { _ItemIndex = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Boolean IsUsedFormula
        {
            get { return _IsUsedFormula; }
            set { _IsUsedFormula = value; }
        }

        public virtual Guid? ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }

        public virtual string Formula1
        {
            get { return _Formula1; }
            set { _Formula1 = value; }
        }

        public virtual  string Formula2
        {
            get { return _Formula2; }
            set { _Formula2 = value; }
        }

        public virtual string Formula3
        {
            get { return _Formula3; }
            set { _Formula3 = value; }
        }

        public virtual string Formula4
        {
            get { return _Formula4; }
            set { _Formula4 = value; }
        }

        public virtual string Formula5
        {
            get { return _Formula5; }
            set { _Formula5 = value; }
        }

        public virtual string Formula6
        {
            get { return _Formula6; }
            set { _Formula6 = value; }
        }

        public virtual string Formula7
        {
            get { return _Formula7; }
            set { _Formula7 = value; }
        }

        public virtual string Formula8
        {
            get { return _Formula8; }
            set { _Formula8 = value; }
        }

        public virtual string Formula9
        {
            get { return _Formula9; }
            set { _Formula9 = value; }
        }

        public virtual string Formula10
        {
            get { return _Formula10; }
            set { _Formula10 = value; }
        }

        public virtual string Formula11
        {
            get { return _Formula11; }
            set { _Formula11 = value; }
        }

        public virtual string Formula12
        {
            get { return _Formula12; }
            set { _Formula12 = value; }
        }

        public virtual string Formula13
        {
            get { return _Formula13; }
            set { _Formula13 = value; }
        }

        public virtual string Formula14
        {
            get { return _Formula14; }
            set { _Formula14 = value; }
        }

        public virtual string Formula15
        {
            get { return _Formula15; }
            set { _Formula15 = value; }
        }

        public virtual string Formula16
        {
            get { return _Formula16; }
            set { _Formula16 = value; }
        }

        public virtual string Formula17
        {
            get { return _Formula17; }
            set { _Formula17 = value; }
        }

        public virtual string Formula18
        {
            get { return _Formula18; }
            set { _Formula18 = value; }
        }

        public virtual string Formula19
        {
            get { return _Formula19; }
            set { _Formula19 = value; }
        }

        public virtual string Formula20
        {
            get { return _Formula20; }
            set { _Formula20 = value; }
        }

        public virtual string Formula21
        {
            get { return _Formula21; }
            set { _Formula21 = value; }
        }

        public virtual string Formula22
        {
            get { return _Formula22; }
            set { _Formula22 = value; }
        }

        public virtual string Formula23
        {
            get { return _Formula23; }
            set { _Formula23 = value; }
        }

        public virtual string Formula24
        {
            get { return _Formula24; }
            set { _Formula24 = value; }
        }

        public virtual string Formula25
        {
            get { return _Formula25; }
            set { _Formula25 = value; }
        }

        public virtual string Formula26
        {
            get { return _Formula26; }
            set { _Formula26 = value; }
        }

        public virtual string Formula27
        {
            get { return _Formula27; }
            set { _Formula27 = value; }
        }

        public virtual string Formula28
        {
            get { return _Formula28; }
            set { _Formula28 = value; }
        }

        public virtual string Formula29
        {
            get { return _Formula29; }
            set { _Formula29 = value; }
        }

        public virtual string Formula30
        {
            get { return _Formula30; }
            set { _Formula30 = value; }
        }

        public virtual int TotalFormulaUse
        {
            get { return _TotalFormulaUse; }
            set { _TotalFormulaUse = value; }
        }

        public virtual Boolean IsBold
        {
            get { return _IsBold; }
            set { _IsBold = value; }
        }

        public virtual Boolean IsItalic
        {
            get { return _IsItalic; }
            set { _IsItalic = value; }
        }

        public virtual int DeterministicPolicy
        {
            get { return _DeterministicPolicy; }
            set { _DeterministicPolicy = value; }
        }

        public virtual string ItemNameEnglish
        {
            get { return _ItemNameEnglish; }
            set { _ItemNameEnglish = value; }
        }

        public virtual Boolean IsHidden
        {
            get { return _IsHidden; }
            set { _IsHidden = value; }
        }

        public virtual Boolean IsDefault
        {
            get { return _IsDefault; }
            set { _IsDefault = value; }
        }
        public virtual int FormulaOnGeneralIndicator
        {
            get { return _FormulaOnGeneralIndicator; }
            set { _FormulaOnGeneralIndicator = value; }
        }
        public virtual int FormulaOfIndicator
        {
            get { return _FormulaOfIndicator; }
            set { _FormulaOfIndicator = value; }
        }

    }
}
