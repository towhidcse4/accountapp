using System;

namespace Accounting.Core.Domain
{
    public class TT153RegisterInvoiceDetail
    {
        private Guid _ID;
        private Guid _TT153RegisterInvoiceID;
        private Guid _TT153ReportID;
        private string _Purpose;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TT153RegisterInvoiceID
        {
            get { return _TT153RegisterInvoiceID; }
            set { _TT153RegisterInvoiceID = value; }
        }

        public virtual Guid TT153ReportID
        {
            get { return _TT153ReportID; }
            set { _TT153ReportID = value; }
        }

        public virtual string Purpose
        {
            get { return _Purpose; }
            set { _Purpose = value; }
        }
    }
}
