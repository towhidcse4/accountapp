﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADecrement
    {
        private decimal totalamount;
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual decimal TotalAmount
        {
            get
            {
                if (FADecrementDetails.Count > 0)
                    return totalamount = FADecrementDetails.Sum(x => x.RemainingAmount) ?? 0;
                else return totalamount;
            }
            set
            {
                value = totalamount;
            }
        }
        public virtual decimal TotalAmountOriginal { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual decimal ExchangeRate { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? RefID { get; set; }
        public virtual IList<FADecrementDetail> FADecrementDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public FADecrement()
        {
            RefVouchers = new List<RefVoucher>();
            FADecrementDetails = new List<FADecrementDetail>();
            ExchangeRate = 1;

        }
    }
}
