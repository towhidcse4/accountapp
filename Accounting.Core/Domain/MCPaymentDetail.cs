using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCPaymentDetail
    {
        private string _accountingObjectName;
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual Guid ID { get; set; }
        public virtual Guid MCPaymentID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string InvoiceTemplate
        {
            get;
            set;
        }
        public virtual string AccountingObjectName
        {
            get { return /*AccountingObject != null ? AccountingObject.AccountingObjectName :*/ _accountingObjectName; }
            set { _accountingObjectName = value; }
        }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual bool? IsIrrationalCost { get; set; }
        public virtual bool IsMatch { get; set; }
        public virtual DateTime? MatchDate { get; set; }
        public virtual int? OrderPriority { get; set; }

        public virtual AccountingObject AccountingObject { get; set; }

        public virtual decimal TaxAmount { get; set; }
        //private MCPayment _MCPayment;
        //public virtual MCPayment MCPayment
        //{
        //    get { return _MCPayment; }
        //    set { _MCPayment = value; }
        //}
    }
}
