﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMAllocationRequired
    {
        private Guid iD;
        private Guid branchID;
        private DateTime requestDate;
        private DateTime aprovedDate;
        private int budgetMonth;
        private int budgetYear;
        private string requestReason;
        private string apovedReason;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }
        public virtual DateTime RequestDate
        {
            get { return requestDate; }
            set { requestDate = value; }
        }
        public virtual DateTime AprovedDate
        {
            get { return aprovedDate; }
            set { aprovedDate = value; }
        }
        public virtual int BudgetMonth
        {
            get { return budgetMonth; }
            set { budgetMonth = value; }
        }

        public virtual int BudgetYear
        {
            get { return budgetYear; }
            set { budgetYear = value; }
        }
        public virtual string RequestReason
        {
            get { return requestReason; }
            set { requestReason = value; }
        }
        public virtual string ApovedReason
        {
            get { return apovedReason; }
            set { apovedReason = value; }
        }

        public EMAllocationRequired()
        {
            EMAllocationRequiredDetails = new List<EMAllocationRequiredDetail>();
        }
        public virtual IList<EMAllocationRequiredDetail> EMAllocationRequiredDetails { get; set; }
    }
}
