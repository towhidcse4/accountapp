﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class Invoice_MIV
    {
        public string windowid { get; set; }
        public string editmode { get; set; }
        public List<DataInvoice_MIV> data = new List<DataInvoice_MIV>();
        
    }
    public class DataInvoice_MIV
    {
        public string inv_invoiceSeries { get; set; }
        public string inv_invoiceNumber { get; set; }
        public string inv_invoiceIssuedDate { get; set; }
        public string inv_currencyCode { get; set; }
        public decimal inv_exchangeRate { get; set; }
        public string inv_buyerDisplayName { get; set; }
        public string ma_dt { get; set; }
        public string inv_buyerLegalName { get; set; }
        public string inv_buyerTaxCode { get; set; }
        public string inv_buyerAddressLine { get; set; }
        public string inv_buyerEmail { get; set; }
        public string inv_buyerBankAccount { get; set; }
        public string inv_buyerBankName { get; set; }
        public string inv_paymentMethodName { get; set; }
        public string inv_sellerBankAccount { get; set; }
        public string inv_sellerBankName { get; set; }
        public string mau_hd { get; set; }
        public List<Details_MIV> details = new List<Details_MIV>();
        public decimal inv_TotalAmountWithoutVat { get; set; }
        public decimal inv_discountAmount { get; set; }
        public decimal inv_vatAmount { get; set; }
        public decimal inv_TotalAmount { get; set; }

    }
    public class Details_MIV
    {
        public string tab_id { get; set; }
        public List<DataDetails_MIV> data = new List<DataDetails_MIV>();
    }
    public class DataDetails_MIV
    {
        public string stt_rec0 { get; set; }
        public string inv_itemCode { get; set; }
        public string inv_itemName { get; set; }
        public string inv_unitCode { get; set; }
        public string inv_unitName { get; set; }
        public decimal inv_unitPrice { get; set; }
        public decimal inv_quantity { get; set; }
        public decimal inv_TotalAmountWithoutVat { get; set; }
        public decimal inv_vatAmount { get; set; }
        public decimal inv_TotalAmount { get; set; }

        public bool inv_promotion { get; set; }
        public decimal inv_discountPercentage { get; set; }
        public decimal inv_discountAmount { get; set; }
        public decimal ma_thue { get; set; }
    }
}
