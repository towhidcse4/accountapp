﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class Login_MIV
    {
        public string username { get; set; }
        public string password { get; set; }
        public string ma_dvcs { get; set; }
    }
}
