﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class Respone_MIV
    {
        public string windowid { get; set; }
        public string error { get; set; }
        public bool ok { get; set; }
        public List<DetailsRespone_MIV> data { get; set; }
        Respone_MIV()
        {
            data = new List<DetailsRespone_MIV>();
        }
    }
    public class DetailsRespone_MIV
    {
        public Guid inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceType { get; set; }
        public Guid inv_InvoiceCode_id { get; set; }

        public string inv_invoiceSeries { get; set; }
        public string inv_invoiceNumber { get; set; }
        public string inv_invoiceName { get; set; }
        public string inv_invoiceIssuedDate { get; set; }
        public string inv_submittedDate { get; set; }

        public string inv_contractNumber { get; set; }
        public string inv_contractDate { get; set; }
        public string inv_currencyCode { get; set; }
        public string inv_exchangeRate { get; set; }
        public string inv_invoiceNote { get; set; }
        public int inv_adjustmentType { get; set; }
        public string ten_trang_thai { get; set; }
        public string id { get; set; }
        public int trang_thai_hd { get; set; }
        public string trang_thai { get; set; }

    }
    public class GetPDF_MIV
    {
        public string id { get; set; }
    }
    public class GetStatusInvoice_MIV
    {
        public string data { get; set; }
    }
    public class DataStatus_MIV
    {
        public List<Data> data = new List<Data>();
    }
    public class Data
    {
        public Guid inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceNumber { get; set; }
        public string trang_thai { get; set; }
        public int StatusInvoice
        {
            get
            {
                if (trang_thai == "Chờ ký") return 0;
                if (trang_thai == "Đã ký") return 1;
                if (trang_thai == "Xóa bỏ") return 5;
                return 1;
            }
        }
    }
    public class AdjustInformation_MIV
    {
        public Guid ?inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceIssuedDate { get; set; }
        public string sovb { get; set; }
        public string ngayvb { get; set; }
        public string ghi_chu { get; set; }
        public string inv_buyerDisplayName { get; set; }
        public string ma_dt { get; set; }
        public string inv_buyerLegalName { get; set; }
        public string inv_buyerAddressLine { get; set; }
        public string inv_buyerEmail { get; set; }
        public string inv_buyerBankAccount { get; set; }
        public string inv_buyerBankName { get; set; }
    }
    public class AdjustIncrease_MIV
    {
        public Guid? inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceIssuedDate { get; set; }
        public string sovb { get; set; }
        public string ngayvb { get; set; }
        public string ghi_chu { get; set; }
        public List<DataAdjust> data = new List<DataAdjust>();
    }
    public class DataAdjust
    {
        public string inv_itemCode { get; set; }
        public decimal? inv_quantity { get; set; }
        public decimal? inv_unitPrice { get; set; }
        public decimal ?ma_thue { get; set; }
        public decimal ?nv_discountPercentage { get; set; }
        public decimal ?inv_discountAmount { get; set; }
        public string tieu_thuc { get; set; }
    }
    public class AdjustReduction_MIV
    {
        public Guid ?inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceIssuedDate { get; set; }
        public string sovb { get; set; }
        public string ngayvb { get; set; }
        public string ghi_chu { get; set; }
        
        public List<DataAdjust> data = new List<DataAdjust>();
    }
    public class ResponeAdjust
    {
        public string error { get; set; }
        public DataResponeAdjust ok = new DataResponeAdjust();
    }
    public class DataResponeAdjust
    {
        public Guid inv_InvoiceAuth_id { get; set; }
        public string inv_invoiceNumber { get; set; }
    }
    public class DestructionInvoiceMIV
    {
        public Guid ?inv_InvoiceAuth_id { get; set; }
        public string sovb { get; set; }
        public string ngayvb { get; set; }
        public string ghi_chu { get; set; }

    }
    public class DataResponeDestructionInvoiceMIV
    {
        public bool ok { get; set; }
        public string Message { get; set; }
        public string error { get; set; }
    }
}
