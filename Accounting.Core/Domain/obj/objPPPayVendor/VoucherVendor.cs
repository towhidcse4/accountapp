﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class VoucherVendor
    {
        private DateTime _vouchersDate; // ngày chứng từ
        private string _no; // số chứng từ
        private DateTime? _postedDate; // hạn thanh toán
        private decimal _soPhatSinh;
        private decimal _soDaTra;        
        public virtual DateTime VouchersDate
        {
            get { return _vouchersDate; }
            set { _vouchersDate = value; }
        }

        public virtual string No
        {
            get { return _no; }
            set { _no = value; }
        }
        public virtual DateTime? VouchersPostedDate { get; set; } // Ngày hạch toán
        public virtual DateTime? PostedDate
        {
            get { return _postedDate; }
            set { _postedDate = value; }
        }

        public virtual decimal SoPhatSinh
        {
            get { return _soPhatSinh; }
            set { _soPhatSinh = value; }
        }

        public virtual decimal SoDaTra
        {
            get { return _soDaTra; }
            set { _soDaTra = value; }
        }
    }
}
