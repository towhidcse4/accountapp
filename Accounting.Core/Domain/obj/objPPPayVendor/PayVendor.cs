﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{

    public class PayVendor
    {
        private Guid _accountingObjectID;
        private string _accountingObjectCode;

        public string AccountingObjectCode
        {
            get { return _accountingObjectCode; }
            set { _accountingObjectCode = value; }
        }
        private string _accountingObjectName;
        private string _accountingObjectAddress;
        private string _phone;
        private string _fax;
        private string _email;
        private string _mst;
        private string _nguoiLienHe;
        private decimal _soDuDauNam;
        private decimal _soPhatSinh;
        private decimal _soDaTra;
        private decimal _soConPhaiTra;
        private string _reason;

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private DateTime _postedDate;

        public DateTime PostedDate
        {
            get { return _postedDate; }
            set { _postedDate = value; }
        }
        private string _no;

        public string No
        {
            get { return _no; }
            set { _no = value; }
        }

        public string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }
        private string _numberAttach;

        public string NumberAttach
        {
            get { return _numberAttach; }
            set { _numberAttach = value; }
        }

        private string _bankAccount;

        public string BankAccount
        {
            get { return _bankAccount; }
            set { _bankAccount = value; }
        }

        private string _bankName;

        public string BankName
        {
            get { return _bankName; }
            set { _bankName = value; }
        }
        private string _identificationNo;

        public string IdentificationNo
        {
            get { return _identificationNo; }
            set { _identificationNo = value; }
        }
        private DateTime? _issueDate;

        public DateTime? IssueDate
        {
            get { return _issueDate; }
            set { _issueDate = value; }
        }
        private string _issueBy;

        public string IssueBy
        {
            get { return _issueBy; }
            set { _issueBy = value; }
        }
        private string _creditCardNumber;

        public string CreditCardNumber
        {
            get { return _creditCardNumber; }
            set { _creditCardNumber = value; }
        }

        private string _creditCardType;

        public string CreditCardType
        {
            get { return _creditCardType; }
            set { _creditCardType = value; }
        }
        private string _ownerCard;

        public string OwnerCard
        {
            get { return _ownerCard; }
            set { _ownerCard = value; }
        }
        private Guid? _accountingObjectBankAccount;

        public Guid? AccountingObjectBankAccount
        {
            get { return _accountingObjectBankAccount; }
            set { _accountingObjectBankAccount = value; }
        }

        private string _accountingObjectBankName;

        public string AccountingObjectBankName
        {
            get { return _accountingObjectBankName; }
            set { _accountingObjectBankName = value; }
        }


        private List<VoucherVendor> _receipts = new List<VoucherVendor>();
        private List<BillPPPayVendorDetail> _bill = new List<BillPPPayVendorDetail>();
        private List<AccountingPPPayVendorDetail> _accouting = new List<AccountingPPPayVendorDetail>();

        public virtual Guid AccountingObjectID
        {
            get { return _accountingObjectID; }
            set { _accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _accountingObjectName; }
            set { _accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return _accountingObjectAddress; }
            set { _accountingObjectAddress = value; }
        }

        public virtual string Tel
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public virtual string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        public virtual string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public virtual string TaxCode
        {
            get { return _mst; }
            set { _mst = value; }
        }

        public virtual string ContactName
        {
            get { return _nguoiLienHe; }
            set { _nguoiLienHe = value; }
        }

        public virtual decimal SoDuDauNam
        {
            get { return _soDuDauNam; }
            set { _soDuDauNam = value; }
        }

        public virtual decimal SoPhatSinh
        {
            get { return _soPhatSinh; }
            set { _soPhatSinh = value; }
        }

        public virtual decimal SoDaTra
        {
            get { return _soDaTra; }
            set { _soDaTra = value; }
        }

        public virtual decimal SoConPhaiTra
        {
            get { return _soConPhaiTra; }
            set { _soConPhaiTra = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual List<VoucherVendor> Voucher
        {
            get { return _receipts; }
            set { _receipts = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual List<BillPPPayVendorDetail> Bill
        {
            get { return _bill; }
            set { _bill = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual List<AccountingPPPayVendorDetail> Accounting
        {
            get { return _accouting; }
            set { _accouting = value; }
        }

        public PayVendor()
        {
            Bill = new List<BillPPPayVendorDetail>();
            Accounting = new List<AccountingPPPayVendorDetail>();
            Voucher = new List<VoucherVendor>();
        }
        //public List<PayVendor> GetExample()
        //{
        //    PayVendor c1 = new PayVendor
        //    {
        //        AccountingObjectCode = "0001",
        //        AccountingObjectName = "Nguyễn Tuấn Anh",
        //        AccountingObjectAddress = "Hoàng Mai",
        //        Tel = "0977156899",
        //        Fax = "043-987549",
        //        Email = "diciem001@gmail.com",
        //        TaxCode = "0100691544",
        //        ContactName = "abc",
        //        SoConPhaiTra = 500000,
        //        SoDaTra = 200000,
        //        SoDuDauNam = 5000000,
        //        SoPhatSinh = 1000000
        //    };
        //    PayVendor c2 = new PayVendor
        //    {
        //        AccountingObjectCode = "0002",
        //        AccountingObjectName = "Ngô Văn Tuấn",
        //        AccountingObjectAddress = "Cầu Giấy",
        //        Tel = "0912094460",
        //        Fax = "043-123456",
        //        Email = "diciem006@gmail.com",
        //        TaxCode = "2700113651",
        //        ContactName = "bac",
        //        SoConPhaiTra = 550000,
        //        SoDaTra = 220000,
        //        SoDuDauNam = 5900000,
        //        SoPhatSinh = 2000000
        //    };
        //    VoucherVendor r1c1 = new VoucherVendor
        //    {
        //        VouchersDate = DateTime.Now,
        //        No = "0001",
        //        //InvoiceNo = "2000",
        //        PostedDate = DateTime.Now,
        //        SoDaTra = 5000000,
        //        SoPhatSinh = 10000000,
        //        //TypeId = 14,
        //        //TypeName = "Hóa đơn bán hàng"
        //    };
        //    VoucherVendor r2c1 = new VoucherVendor
        //    {
        //        VouchersDate = DateTime.Now,
        //        No = "0002",
        //        //InvoiceNo = "2001",
        //        PostedDate = DateTime.Now,
        //        SoDaTra = 7500000,
        //        SoPhatSinh = 40000000,
        //        //TypeId = 14,
        //        //TypeName = "Phiếu thu tiền khách hàng"
        //    };
        //    BillPPPayVendorDetail b1c1 = new BillPPPayVendorDetail
        //    {
        //        Account = "111",
        //        No = "0002",
        //        TotalDebitOriginal=100000,
        //        DebitAmountOriginal = 100000

        //    };
        //    BillPPPayVendorDetail b2c1 = new BillPPPayVendorDetail
        //    {
        //        Account = "001",
        //        No = "0002",
        //        TotalDebitOriginal = 200000,
        //        DebitAmountOriginal = 200000

        //    };
        //    c1.Voucher.Add(r1c1);
        //    c1.Voucher.Add(r2c1);
        //    c1.Bill.Add(b1c1);
        //    c1.Bill.Add(b2c1);

        //    return new List<PayVendor> { c1, c2 };
        //}

    }
}
