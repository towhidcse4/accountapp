﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BillPPPayVendorDetail // MCPaymentDetailVendor
    {
        private Boolean _check;
        public Guid ReferenceID { get; set; }
        public Boolean CheckColumn
        {
            get { return _check; }
            set { _check = value; }
        }



        private DateTime _date;//Ngày chứng từ
        private string _no;//Số chứng từ
        private string _invoiceNo;//Số hóa đơn
        private DateTime? _dueDate;//Hạn thanh toán
        private decimal _totalDebitOriginal;//Tổng nợ
        private decimal _totalDebit;//Tổng nợ quy đổi
        private decimal _debitAmountOriginal;//Số còn nợ
        private decimal _debitAmount;//Số còn nợ quy đổi
        private string _account;//TK phải trả
        private string _employeeName;//Nhân viên
        private decimal? _amountOriginal;//Số đã trả
        private decimal? _amount;//Quy đổi
        public decimal? PaymentAmount { get; set; }//Quy đổi
        private string _paymentClause;//ĐK thanh toán
        private decimal _discountRate;//Tỷ lệ CK
        private decimal _discountAmountOriginal;//Tiền CK
        private decimal _discountAmount;//Tiền CK quy đổi
        private string _discountAccount;//TK chiết khấu
        public virtual decimal? RefVoucherExchangeRate { get; set; }
        public virtual decimal LastExchangeRate { get; set; }
        public virtual decimal DifferAmount { get; set; }
        public virtual decimal AmountTT { get; set; }

        public virtual DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public virtual string No
        {
            get { return _no; }
            set { _no = value; }
        }

        public virtual string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        public virtual decimal TotalDebitOriginal
        {
            get { return _totalDebitOriginal; }
            set { _totalDebitOriginal = value; }
        }

        public virtual decimal TotalDebit
        {
            get { return _totalDebit; }
            set { _totalDebit = value; }
        }

        public virtual decimal DebitAmountOriginal
        {
            get { return _debitAmountOriginal; }
            set { _debitAmountOriginal = value; }
        }

        public virtual decimal DebitAmount
        {
            get { return _debitAmount; }
            set { _debitAmount = value; }
        }

        public virtual string Account
        {
            get { return _account; }
            set { _account = value; }
        }

        public virtual string EmployeeName
        {
            get { return _employeeName; }
            set { _employeeName = value; }
        }

        public virtual decimal? AmountOriginal
        {
            get { return _amountOriginal; }
            set { _amountOriginal = value; }
        }

        public virtual decimal? Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public virtual string PaymentClause
        {
            get { return _paymentClause; }
            set { _paymentClause = value; }
        }

        public virtual decimal DiscountRate
        {
            get { return _discountRate; }
            set { _discountRate = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return _discountAmountOriginal; }
            set { _discountAmountOriginal = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }

        public virtual string DiscountAccount
        {
            get { return _discountAccount; }
            set { _discountAccount = value; }
        }
        private string _currencyID;
        public virtual string CurrencyID
        {
            get { return _currencyID; }
            set { _currencyID = value; }
        }
        public virtual int TypeID { get; set; }       
    }
}
