﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class AccountingPPPayVendorDetail // MCPaymentDetail
    {
        public virtual string Description { get; set; }//Diễn giải
        public virtual string DebitAccount { get; set; }//TK nợ
        public virtual string CreditAccount { get; set; }//TK có
        public virtual decimal TotalAmountOriginal { get; set; }//Số tiền
        public virtual decimal TotalAmount { get; set; }//Số tiền QĐ
        public virtual string AccountingObjectCode { get; set; }//Đối tượng
        public virtual Guid? BudgetItemCode { get; set; }//Mục thu/chi
        public virtual Guid? ExpenseItemCode { get; set; }//Khoản mục CP
        public virtual Guid? DepartmentCode { get; set; }//Phòng ban
        public virtual Guid? CostSetCode { get; set; }//ĐT tập hợp CP
        public virtual Guid? EmContractCode { get; set; }//Hợp đồng
        public virtual bool IsIrrationalCost { get; set; }//CP không hợp lý
        public virtual Guid? StatisticsCode { get; set; }//Mã thống kê
    }
}
