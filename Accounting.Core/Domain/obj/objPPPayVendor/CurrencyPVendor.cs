﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class CurrencyPVendor
    {
        private decimal _totalAmountOriginal; //Số tiền
        private decimal _totalAmount; //Quy đổi
        private string _currencyID; //Loại tiền
        private decimal _exchangeRate; //Tỷ giá

        public virtual decimal TotalAmountOriginal
        {
            get { return _totalAmountOriginal; }
            set { _totalAmountOriginal = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        public virtual string CurrencyID
        {
            get { return _currencyID; }
            set { _currencyID = value; }
        }

        public virtual decimal ExchangeRate
        {
            get { return _exchangeRate; }
            set { _exchangeRate = value; }
        }
    }
}
