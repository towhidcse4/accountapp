﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class SubSystem
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public List<Guid> Permissions { get; set; }
        public string ParentID { get; set; }
        public bool IsParent { get; set; }
    }
}
