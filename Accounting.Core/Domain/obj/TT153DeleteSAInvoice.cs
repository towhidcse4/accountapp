﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TT153DeleteSAInvoice
    {
        public bool Status { get; set; }
        public Guid SAInvoiceID { get; set; }
        public int InvoiceType { get; set; }
        public string InvoiceTypeString { get; set; }
        public Guid ?InvoiceTypeID { get; set; }
        public string InvoiceTemplate { get; set; }
        public string InvoiceSeries { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime ?InvoiceDate { get; set; }
        public string AccountingObjectName { get; set; }
    }
}
