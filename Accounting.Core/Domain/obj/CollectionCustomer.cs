﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class CollectionCustomer
    {
        
        public Guid AccountingObjectID { set; get; }
        public virtual string AccountingObjectCode { set; get; }
        public virtual string AccountingObjectName { set; get; }
        public virtual string Address { set; get; }
        public virtual decimal SoDuDauNam { get; set; }
        public virtual decimal SoPhatSinh { get; set; }
        public virtual decimal SoDaThu { get; set; }
        public virtual decimal SoConPhaiThu { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MContactName { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual Guid BankAccountDetailID { get; set; }
        public virtual string BankName { get; set; }
        public virtual DateTime? Date { get; set; }//thêm mới
        public virtual string No { get; set; }//thêm mới
        public virtual DateTime? PostedDate { get; set; }//thêm mới
        public virtual string CurrencyID { get; set; }//loại tiền chọn để trả(mặc định VND)
        public virtual Guid? EmployeeID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalAmountOriginal { get; set; }
        public AccountingObject AccountingObject { set; get; }   //Lấy đối tượng AccountingObject là thuộc tính
        public List<ReceiptDebit> ReceiptDebitDetails { get; set; }//danh sách chi tiết
        public List<ReceiptDebit> ReceiptDebits { get; set; }//hóa đơn
        public List<Flexaccount> ListfFlexaccounts { get; set; }//danh sách hạch toán
        public CollectionCustomer()
        {
            TotalAmount = 0;
            TotalAmountOriginal = 0;
            CurrencyID = "VND";
            ExchangeRate = 1;
            ReceiptDebitDetails = new List<ReceiptDebit>();
            ReceiptDebits = new List<ReceiptDebit>();
            ListfFlexaccounts = new List<Flexaccount>();
        }
    }
}
