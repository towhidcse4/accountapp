﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core
{
    public class Tax
    {
        public virtual Guid ID { get; set; }
        public virtual string DeclarationName { get; set; }
        public virtual string DeclarationTerm { get; set; }
        public virtual bool IsFirstDeclaration { get; set; } // là tờ khai lần đầu ?
        public virtual int? AdditionTime { get; set; }
        public virtual string AdditionTime_String
        {
            get
            {
                if (AdditionTime == 0 || AdditionTime == null) return "";
                else return AdditionTime.ToString();
            }
        }
        public virtual int TypeID { get; set; }
        public virtual DateTime DateTo { get; set; }
    }
}
