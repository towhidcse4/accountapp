﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TNDN03Report
    {

        public virtual Guid BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string DeclarationName { get; set; }
        public virtual string DeclarationTerm { get; set; }
        public virtual DateTime FromDate { get; set; }
        public virtual DateTime ToDate { get; set; }
        public virtual Boolean IsFirstDeclaration { get; set; }
        public virtual int AdditionTime { get; set; }
        public virtual DateTime? AdditionDate { get; set; }
        public virtual int Career { get; set; }
        public virtual Boolean IsAppendix031ATNDN { get; set; }
        public virtual Boolean IsAppendix032ATNDN { get; set; }
        public virtual Boolean IsSMEEnterprise { get; set; }
        public virtual Boolean IsDependentEnterprise { get; set; }
        public virtual Boolean IsRelatedTransactionEnterprise { get; set; }
        public virtual Guid TMIndustryTypeID { get; set; }
        public virtual Decimal Rate { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string TaxAgencyTaxCode { get; set; }
        public virtual string TaxAgencyName { get; set; }
        public virtual string TaxAgencyEmployeeName { get; set; }
        public virtual string CertificationNo { get; set; }
        public virtual string SignName { get; set; }

        public virtual string IndustryTypeName1 { get; set; }
        public virtual string ExtensionCaseName { get; set; }
        public virtual DateTime? SignDate { get; set; }
        public virtual Boolean IsExtend { get; set; }
        public virtual int? ExtensionCase { get; set; }
        public virtual DateTime? ExtendTime { get; set; }
        public virtual Decimal ExtendTaxAmount { get; set; }
        public virtual Decimal NotExtendTaxAmount { get; set; }
        public virtual int LateDays { get; set; }
        public virtual DateTime? FromDateLate { get; set; }
        public virtual DateTime? ToDateLate { get; set; }
        public virtual Decimal LateAmount { get; set; }
        public virtual int Year { get; set; }
        public TNDN03Report()
        {
           
            TM03TNDNDocuments1 = new List<TM03TNDNDocument>();
            TM031ATNDNs = new List<TM031ATNDN>();
            TM032ATNDNs = new List<TM032ATNDN>();
        }
        public virtual IList<TM03TNDNDocument> TM03TNDNDocuments1 { get; set; }
        public virtual IList<TM031ATNDN> TM031ATNDNs { get; set; }
        public virtual IList<TM032ATNDN> TM032ATNDNs { get; set; }
        public virtual Decimal Itema { get; set; }
        public virtual Decimal Itema1 { get; set; }
        public virtual Decimal Itemb { get; set; }
        public virtual Decimal Itemb1 { get; set; }
        public virtual Decimal Itemb2 { get; set; }
        public virtual Decimal Itemb3 { get; set; }
        public virtual Decimal Itemb4 { get; set; }
        public virtual Decimal Itemb5 { get; set; }
        public virtual Decimal Itemb6 { get; set; }
        public virtual Decimal Itemb7 { get; set; }
        public virtual Decimal Itemb8 { get; set; }
        public virtual Decimal Itemb9 { get; set; }
        public virtual Decimal Itemb10 { get; set; }
        public virtual Decimal Itemb11 { get; set; }
        public virtual Decimal Itemb12 { get; set; }
        public virtual Decimal Itemb13 { get; set; }
        public virtual Decimal Itemb14 { get; set; }
        public virtual Decimal Itemc { get; set; }
        public virtual Decimal Itemc1 { get; set; }
        public virtual Decimal Itemc2 { get; set; }
        public virtual Decimal Itemc3 { get; set; }
        public virtual Decimal Itemc3a { get; set; }
        public virtual Decimal Itemc3b { get; set; }
        public virtual Decimal Itemc4 { get; set; }
        public virtual Decimal Itemc5 { get; set; }
        public virtual Decimal Itemc6 { get; set; }
        public virtual Decimal Itemc7 { get; set; }
        public virtual Decimal Itemc8 { get; set; }
        public virtual Decimal Itemc9 { get; set; }
        public virtual Decimal Itemc9a { get; set; }
        public virtual Decimal Itemc10 { get; set; }
        public virtual Decimal Itemc11 { get; set; }
        public virtual Decimal Itemc12 { get; set; }
        public virtual Decimal Itemc13 { get; set; }
        public virtual Decimal Itemc14 { get; set; }
        public virtual Decimal Itemc15 { get; set; }
        public virtual Decimal Itemc16 { get; set; }
        public virtual Decimal Itemd { get; set; }
        public virtual Decimal Itemd1 { get; set; }
        public virtual Decimal Itemd2 { get; set; }
        public virtual Decimal Itemd3 { get; set; }
        public virtual Decimal Iteme { get; set; }
        public virtual Decimal Iteme1 { get; set; }
        public virtual Decimal Iteme2 { get; set; }
        public virtual Decimal Iteme3 { get; set; }
        public virtual Decimal Itemg { get; set; }
        public virtual Decimal Itemg1 { get; set; }
        public virtual Decimal Itemg2 { get; set; }
        public virtual Decimal Itemg3 { get; set; }
        public virtual Decimal Itemh { get; set; }
        public virtual Decimal Itemi { get; set; }
        public virtual string Item1 { get; set; }
        public virtual string Item2 { get; set; }
        public virtual string Item3 { get; set; }
        public virtual string Item4 { get; set; }
        public virtual string Item5 { get; set; }
        public virtual string Item6 { get; set; }
        public virtual string Item7 { get; set; }
        public virtual string Item8 { get; set; }
        public virtual string Item9 { get; set; }
        public virtual string Item10 { get; set; }
        public virtual string Item11 { get; set; }
        public virtual string Item12 { get; set; }
        public virtual string Item13 { get; set; }
        public virtual string Item14 { get; set; }
        public virtual string Item15 { get; set; }
        public virtual string Item16 { get; set; }
        public virtual string Item17 { get; set; }
        public virtual string Item18 { get; set; }
        public virtual string Item19 { get; set; }
    }
}
