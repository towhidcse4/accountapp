﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class GTGT01DetailReport
    {
        public virtual string Item21 { get; set; }
        public virtual string Item22 { get; set; }
        public virtual string Item23 { get; set; }
        public virtual string Item24 { get; set; }
        public virtual string Item25 { get; set; }
        public virtual string Item26 { get; set; }
        public virtual string Item27 { get; set; }
        public virtual string Item28 { get; set; }
        public virtual string Item29 { get; set; }
        public virtual string Item30 { get; set; }
        public virtual string Item31 { get; set; }
        public virtual string Item32 { get; set; }
        public virtual string Item32a { get; set; }
        public virtual string Item33 { get; set; }
        public virtual string Item34 { get; set; }
        public virtual string Item35 { get; set; }
        public virtual string Item36 { get; set; }
        public virtual string Item37 { get; set; }
        public virtual string Item38 { get; set; }
        public virtual string Item39 { get; set; }
        public virtual string Item40a { get; set; }
        public virtual string Item40b { get; set; }
        public virtual string Item40 { get; set; }
        public virtual string Item41 { get; set; }
        public virtual string Item42 { get; set; }
        public virtual string Item43 { get; set; }


        public virtual Guid ID { get; set; }
        public virtual Guid BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string DeclarationName { get; set; }
        public virtual string DeclarationTerm { get; set; }
        public virtual DateTime FromDate { get; set; }
        public virtual DateTime ToDate { get; set; }
        public virtual Boolean IsFirstDeclaration { get; set; }
        public virtual int? AdditionTime { get; set; }
        public virtual DateTime? AdditionDate { get; set; }
        public virtual string Career { get; set; }
        public virtual Boolean IsExtend { get; set; }
        public virtual string ExtensionCase { get; set; }
        public virtual Boolean IsAppendix011GTGT { get; set; }
        public virtual Boolean IsAppendix012GTGT { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string TaxAgencyTaxCode { get; set; }
        public virtual string TaxAgencyName { get; set; }
        public virtual string TaxAgencyEmployeeName { get; set; }
        public virtual string CertificationNo { get; set; }
        public virtual string SignName { get; set; }
        public virtual DateTime? SignDate { get; set; }

        public virtual List<TM011GTGT> lstKCT { get; set; }
        public virtual List<TM011GTGT> lstRate0 { get; set; }
        public virtual List<TM011GTGT> lstRate5 { get; set; }
        public virtual List<TM011GTGT> lstRate10 { get; set; }
        public virtual IList<TM01GTGTAdjust> TM01GTGTAdjusts1 { get; set; }
        public virtual IList<TM01GTGTAdjust> TM01GTGTAdjusts2 { get; set; }
        public virtual IList<TM01GTGTAdjust> TM01GTGTAdjusts3 { get; set; }
        public virtual IList<TM012GTGT> TM012GTGTs1 { get; set; }
        public virtual IList<TM012GTGT> TM012GTGTs2 { get; set; }
        public virtual IList<TM012GTGT> TM012GTGTs3 { get; set; }
        public virtual IList<TM01GTGTDetail> TM01GTGTDetails { get; set; }

        public virtual IList<TM01GTGTAdjust> TM01GTGTAdjusts { get; set; }
        public virtual IList<TM011GTGT> TM011GTGTs { get; set; }
        public virtual IList<TM012GTGT> TM012GTGTs { get; set; }
        public GTGT01DetailReport()
        {
            lstKCT = new List<TM011GTGT>();
            lstRate0 = new List<TM011GTGT>();
            lstRate5 = new List<TM011GTGT>();
            lstRate10 = new List<TM011GTGT>();
            TM01GTGTAdjusts1 = new List<TM01GTGTAdjust>();
            TM01GTGTAdjusts2 = new List<TM01GTGTAdjust>();
            TM01GTGTAdjusts3 = new List<TM01GTGTAdjust>();
            TM012GTGTs1 = new List<TM012GTGT>();
            TM012GTGTs2 = new List<TM012GTGT>();
            TM012GTGTs3 = new List<TM012GTGT>();
            TM01GTGTDetails = new List<TM01GTGTDetail>();
            TM01GTGTAdjusts = new List<TM01GTGTAdjust>();

            TM011GTGTs = new List<TM011GTGT>();

            TM012GTGTs = new List<TM012GTGT>();
        }


        }
    
}
