﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class GTGT04Report
    {
        public virtual Guid ID { get; set; }
        public virtual Guid BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string DeclarationName { get; set; }
        public virtual string DeclarationTerm { get; set; }
        public virtual DateTime FromDate { get; set; }
        public virtual DateTime ToDate { get; set; }
        public virtual Boolean IsFirstDeclaration { get; set; }
        public virtual int? AdditionTime { get; set; }
        public virtual DateTime? AdditionDate { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string TaxAgencyTaxCode { get; set; }
        public virtual string TaxAgencyName { get; set; }
        public virtual string TaxAgencyEmployeeName { get; set; }
        public virtual string CertificationNo { get; set; }
        public virtual string SignName { get; set; }
        public virtual DateTime? SignDate { get; set; }
        public virtual string Item21 { get; set; }
        public virtual string Item22 { get; set; }
        public virtual string Item23 { get; set; }
        public virtual string Item24 { get; set; }
        public virtual string Item25 { get; set; }
        public virtual string Item26 { get; set; }
        public virtual string Item27 { get; set; }
        public virtual string Item28 { get; set; }
        public virtual string Item29 { get; set; }
        public virtual string Item30 { get; set; }
        public virtual string Item31 { get; set; }
        public virtual string Item32 { get; set; }
        public virtual string Item33 { get; set; }
        public virtual IList<TM04GTGTDetail> TM04GTGTDetails { get; set; }

        public virtual IList<TM04GTGTAdjust> TM04GTGTAdjusts { get; set; }
    }
}
