﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class TIAllocationImportTax
    {
        public bool CheckColumn { get; set; }
        public Guid ToolsID { get; set; }
        public string ToolsCode { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal ImportTaxExpenseRate { get; set; }
        public decimal ImportTaxExpenseAmount { get; set; }
        public TIIncrementDetail TIIncrementDetail { get; set; }
        public TIAllocationImportTax()
        {
            Quantity = 0;
            Amount = 0;
            ImportTaxExpenseRate = 0;
            ImportTaxExpenseAmount = 0;
        }
    }
}
