﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class Company
    {
        public Guid ID { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyName { get; set; }
        public string ServerName { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AppUser { get; set; }
        public string AppPass { get; set; }
    }
}
