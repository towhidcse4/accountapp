﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class EMContractSale
    {
        public virtual string TypeName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual string DepartmentName { get; set; }
        public virtual string ExpenseItemName { get; set; }
        public virtual Guid GLID { get; set; }
        public virtual bool Select { get; set; }
    }
}
