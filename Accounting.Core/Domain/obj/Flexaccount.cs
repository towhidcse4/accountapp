﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class Flexaccount
    {
        public string Description { get; set; } // diễn giải
        public string DebitAccount { get; set; } // tài khoản nợ
        public string CreditAccount { get; set; } // tài khoản có
        public decimal TotalAmount { get; set; } // số tiền
        public decimal TotalAmountOriginal { get; set; } //số tiền quy đổi (=số tiền * tỷ giá )
        public string AccountingObjectCode { get; set; } // mã đối tượng
        public string BudgetItemID { get; set; } // mã mục thu chi
        public string ExpenseItemID { get; set; } // mã khoản mục chi phí
        public string DepartmentID { get; set; } // mã phòng ban
        public string CostSetID { get; set; } // mã đối tượng tập hợp chi phí
        public string EMContractID { get; set; } // mã hợp đồng
        public bool IsIrrationalCost { get; set; } // chi phí không hợp lý
        public string StatisticsCodeID { get; set; } // mã thống kê
        public Guid? AccountingObjectID { get; set; }
        public string CurrencyID { get; set; }

    }
}
