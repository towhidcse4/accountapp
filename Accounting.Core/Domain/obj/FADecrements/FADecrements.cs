﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
   public class FADecrements
    {
        public virtual Guid ID { get; set; }// Id tài sản ghi giảm
        public virtual Guid? BranchID { get; set; }//Id chi nhánh (chưa dùng)
        public virtual int TypeID { get; set; } //Loại chứng từ ghi giảm TSCĐ
        public virtual DateTime PostedDate { get; set; }//ngày hạch toán
        public virtual DateTime Date { get; set; }//Ngày chứng từ
        public virtual string No { get; set; }// số chứng từ
        public virtual string Reason { get; set; }// diễn giải
        public virtual string NumberAttach { get; set; }// tài liệu kèm theo
        public virtual decimal TotalAmount { get; set; }// tổng tiền    
        public virtual decimal TotalAmountOriginal { get; set; }// tổng tiền nguyên tệ
        public virtual string CurrencyID { get; set; }// Id loại tiền
        public virtual decimal ExchangeRate { get; set; }// tỷ giá
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int TemplateID { get; set; }//id mẫu giao diện 
        public virtual bool Recorded { get; set; }//trạng thái ghi sổ
        public virtual bool Exported { get; set; }// trạng thái kiết xuất mặc định là 1
    }
}
