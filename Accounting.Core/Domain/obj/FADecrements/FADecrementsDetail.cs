﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class FADecrementsDetail
    {
        public virtual Guid ID { get; set; }// ID chi tiết ghi giảm TSCĐ
        public virtual Guid? FADecrementID { get; set; }//ID ghi tăng tài sản cố định
        public virtual string FixedAssetID { get; set; }// ID tài sản cố định
        public virtual string DepartmentID { get; set; } // ID phòng ban 
        public virtual string Description { get; set; }// Diễn giải
        public virtual string DebitAccount { get; set; }// Tk nợ
        public virtual string CreditAccount { get; set; }// TK có
        public virtual decimal Amount { get; set; }//Giá mua
        public virtual decimal AmountOriginal { get; set; }// giá mua nguyên tệ
        public virtual string EmployeeID { get; set; }//ID nhân viên
        public string AccountingObjectCode { get; set; } // mã đối tượng
        public string BudgetItemCode { get; set; } // mã mục thu chi
        public string ExpenseItemCode { get; set; } // mã khoản mục chi phí
        public string DepartmentCode { get; set; } // mã phòng ban
        public string CostSetCode { get; set; } // mã đối tượng tập hợp chi phí
        public string EmContractCode { get; set; } // mã hợp đồng
        public string StatisticsCode { get; set; } // mã thống kê
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public bool IsIrrationalCost { get; set; } // chi phí không hợp lý
        public virtual int OrderPriority { get; set; } //thứ tự ưu tiên
    }
}
