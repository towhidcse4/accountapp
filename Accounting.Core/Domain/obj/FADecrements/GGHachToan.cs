﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.FADecrements
{
    [Serializable]
    public class GGHachToan
    {
        public virtual bool Status { get; set; }
        public virtual string FACode { get; set; } // Mã tài sản
        public virtual string Description { get; set; } // Diễn giải
        public virtual string DebitAccount { get; set; } // TK Nợ
        public virtual string CreditAccount { get; set; } // TK Có
        public virtual decimal Amount { get; set; } // Số tiền
    }
}
