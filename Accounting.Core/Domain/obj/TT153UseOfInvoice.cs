﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TT153UseOfInvoice
    {
        public string SelectMonthquarter{ get; set; }
        public Guid TT153ReportID { get; set; }
        public string ReportName { get; set; }
        public string InvoiceTemplate { get; set; }
        public string InvoiceSeries { get; set; }
        public string Total { get; set; }
        public string FromNoEarlyPeriod { get; set; }
        public string ToNoEarlyPeriod { get; set; }
        public string FromNoPurchase { get; set; }
        public string ToNoPurchase { get; set; }
        public string FromNoUse { get; set; }
        public string ToNoUse { get; set; }
        public string Sum { get; set; }
        public string QuantityUsed { get; set; }
        public string QuantityDeleted { get; set; }
        public string NoDeleted { get; set; }
        public string QuantityLost { get; set; }
        public string NoLost { get; set; }
        public string QuantityDestruction { get; set; }
        public string NoDestruction { get; set; }
        public string FromNoEndPeriod { get; set; }
        public string ToNoEndPeriod { get; set; }
        public string Quantity { get; set; }

    }
}
