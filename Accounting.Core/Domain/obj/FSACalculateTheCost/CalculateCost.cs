﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.FSACalculateTheCost
{
    [Serializable]
    public class CalculateCost : MaterialGoods
    {
        private string _MaterialGoodsCategory;
        public virtual bool Status { get; set; }
        public virtual string MaterialGoodsCategory
        {
            //get { return MaterialGoosCategory.MaterialGoodsCategoryCode; }
            get
            {
                if (MaterialGoosCategory == null) return "";
                try
                {
                    return MaterialGoosCategory.MaterialGoodsCategoryCode;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { _MaterialGoodsCategory = value; }
        }

    }
}
