﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class ChangeInformation
    {
        public virtual string NameInformation { get; set; }
        public virtual string OldInformation { get; set; }
        public virtual string NewInformation { get; set; }
        public virtual bool Status { get; set; }
    }
}
