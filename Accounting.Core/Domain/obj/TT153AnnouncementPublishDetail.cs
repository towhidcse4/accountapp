﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TT153AnnouncementPublishDetail
    {
        public bool Status { get; set; }
        public string InvoiceTemplate { get; set; }
        public int InvoiceType { get; set; }
        public string InvoiceSeries { get; set; }
        public String FromNo { get; set; }
        public String ToNo { get; set; }
        public int Quantity { get; set; }
        public string InvoiceType_string { get; set; }
        public Guid TT153ReportID { get; set; }
        public Guid InvoiceTypeID { get; set; }
        public Guid TT153PublishInvoiceDetailID { get; set; }
    }
}
