﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class InsurancePayment
    {
        public bool Check { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalAmount { get; set; }
        public string DebitAccount { get; set; }
        public InsurancePayment()
        {
            Amount = 0;
            TotalAmount = 0;
        }
        public string AccountNumber { get; set; }
    }
}
