﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialGoodsCustom : MaterialGoods
    {
        private decimal sumIWQuantity;
        private decimal sumIWAmount;
        private decimal inventoryIWQuantity;
        private string tinhChat;
        private string loaiVTHH;
        private string khoNgamDinh;
        private string nhomHHDVchiuthueTTĐB;
        private string materialGoodsCode;
        public virtual string TinhChat
        {
            get { return tinhChat; }
            set { tinhChat = value; }
        }
        public virtual string LoaiVTHH
        {
            get { return loaiVTHH; }
            set { loaiVTHH = value; }
        }
        public virtual string KhoNgamDinh
        {
            get { return khoNgamDinh; }
            set { khoNgamDinh = value; }
        }
        public virtual string NhomHHDVchiuthueTTĐB
        {
            get { return nhomHHDVchiuthueTTĐB; }
            set { nhomHHDVchiuthueTTĐB = value; }
        }
        public decimal InventoryIWQuantity
        {
            get { return inventoryIWQuantity; }
            set { inventoryIWQuantity = value; }
        }
        public virtual string MaterialGoodsCategoryCode
        {
            get { return materialGoodsCode; }
            set { materialGoodsCode = value; }
        }
        private decimal inventoryIWAmount;

        public decimal InventoryIWAmount
        {
            get { return inventoryIWAmount; }
            set { inventoryIWAmount = value; }
        }
        private decimal diffSumIWQuantity;
        private decimal diffSumIWAmount;

        public decimal DiffSumIWAmount
        {
            get { return diffSumIWAmount; }
            set { diffSumIWAmount = value; }
        }

        public decimal DiffSumIWQuantity
        {
            get { return diffSumIWQuantity; }
            set { diffSumIWQuantity = value; }
        }

        public decimal SumIWAmount
        {
            get { return sumIWAmount; }
            set { sumIWAmount = value; }
        }

        public decimal SumIWQuantity
        {
            get { return sumIWQuantity; }
            set { sumIWQuantity = value; }
        }
    }
}
