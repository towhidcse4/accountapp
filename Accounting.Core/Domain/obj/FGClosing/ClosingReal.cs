﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ClosingReal
    {
        public virtual bool Status { get; set; }
        public virtual string AccountNumber { get; set; } // Tài khoản
        public virtual string AccountBank { get; set; }
        public virtual Guid AccountingObjectId { get; set; }
        public virtual decimal DebitAmount { get; set; }
        public virtual decimal DebitAmountOriginal { get; set; }
        public virtual decimal DebitRealAmount { get; set; }
        public virtual decimal DebitNotMatchAmount { get; set; }
        public virtual decimal CreditAmount { get; set; }
        public virtual decimal CreditAmountOriginal { get; set; }
        public virtual decimal CreditRealAmount { get; set; }
        public virtual decimal CreditNotMatchAmount { get; set; }
    }
    
}
