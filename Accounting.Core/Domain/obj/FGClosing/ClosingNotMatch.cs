﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ClosingNotMatch
    {
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; } 
        public virtual string CreditAccount { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal BankAccountNumber { get; set; }
        public virtual Guid AccountingObjectDebitId { get; set; }
        public virtual Guid AccountingObjectCreditId { get; set; }
        public virtual Guid BudgetItemId { get; set; }
        public virtual Guid ExpenseItemId { get; set; }
        public virtual Guid CostSetId { get; set; }
        public virtual Guid StatisticCodeId { get; set; }
    }
    
}
