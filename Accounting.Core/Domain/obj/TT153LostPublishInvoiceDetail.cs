﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TT153LostPublishInvoiceDetail
    {
        public bool Status { get; set; }
        public Guid TT153ReportID { get; set; }
        public Guid TT153PublishInvoiceDetailID { get; set; }
        public int InvoiceForm { get; set; }
        public String InvoiceTemplate { get; set; }
        public String InvoiceFormString { get; set; }
        public int InvoiceType { get; set; }
        public String InvoiceTypeString { get; set; }
        public Guid InvoiceTypeID { get; set; }
        public String InvoiceSeries { get; set; }
        public String FromNo { get; set; }
        public String ToNo { get; set; }
        public int Quantity { get; set; }
        public String CopyPart { get; set; }
        public String Description { get; set; }

    }
}
