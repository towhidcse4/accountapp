﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class SelectFactor
    {
        public virtual bool Select { get; set; }
        public virtual Guid _ID { get; set; }
        public virtual string _CostSetCode { get; set; }
        public virtual string _CostSetName { get; set; }
        public virtual string _MaterialGoodsName { get; set; }
        public virtual string _MaterialGoodsCode { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string CostSetCode
        {
            get { return _CostSetCode; }
            set { _CostSetCode = value; }
        }
        public virtual string CostSetName
        {
            get { return _CostSetName; }
            set { _CostSetName = value; }
        }
        public virtual string MaterialGoodsName
        {
            get { return _MaterialGoodsName; }
            set { _MaterialGoodsName = value; }
        }

        public virtual string MaterialGoodsCode
        {
            get { return _MaterialGoodsCode; }
            set { _MaterialGoodsCode = value; }
        }
    }
}
