﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class SelectConstruction
    {
        public virtual bool Select { get; set; }
        public virtual Guid _ID { get; set; }
        public virtual string _ConstructionCode { get; set; }
        public virtual string _ConstructionName { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string ConstructionCode
        {
            get { return _ConstructionCode; }
            set { _ConstructionCode = value; }
        }
        public virtual string ConstructionName
        {
            get { return _ConstructionName; }
            set { _ConstructionName = value; }
        }
    }
}
