﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CPMaterialProductQuantum
    {
        public virtual Guid MaterialGoodsID { get; set; }
        public virtual string MaterialGoodsCode { get; set; }
        public virtual string MaterialGoodsName { get; set; }
        public virtual string Unit { get; set; }
        public virtual Guid CostSetID { get; set; }
        public virtual Decimal? DirectMaterialAmount
        {
            get; set;
        }

        public virtual Decimal? DirectLaborAmount
        {
            get; set;
        }

        public virtual Decimal? GeneralExpensesAmount
        {
            get; set;
        }

        public virtual Decimal? TotalCostAmount
        {
            get; set;
        }
    }
}
