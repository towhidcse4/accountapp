﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CPCostingPeriod
    {
        private Guid iD;
        private DateTime fromDate;
        private DateTime toDate;
        private string costingPeriodName; //tên kỳ tính giá thành
        private Decimal uncomplete; //dở dang đầu kỳ
        private Decimal expensesDuringPeriod; //chi phí phát sinh trong kỳ 
        private Decimal uncompleteEndOfPeriod; //dở dang cuối kỳ
        private Decimal totalCost; //tổng giá thành
        private bool isDelete;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }

        public virtual string CostingPeriodName
        {
            get { return costingPeriodName; }
            set { costingPeriodName = value; }
        }

        public virtual Decimal Uncomplete
        {
            get { return uncomplete; }
            set { uncomplete = value; }
        }

        public virtual Decimal ExpensesDuringPeriod
        {
            get { return expensesDuringPeriod; }
            set { expensesDuringPeriod = value; }
        }

        public virtual Decimal UncompleteEndOfPeriod
        {
            get { return uncompleteEndOfPeriod; }
            set { uncompleteEndOfPeriod = value; }
        }

        public virtual Decimal TotalCost
        {
            get { return totalCost; }
            set { totalCost = value; }
        }

        public virtual bool IsDelete
        {
            get { return isDelete; }
            set { isDelete = value; }
        }
    }
}
