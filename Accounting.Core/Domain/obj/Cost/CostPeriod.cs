﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CostPeriod
    {

        private DateTime fromDate; //từ ngày
        private DateTime toDate; // đến ngày
        private string costingPeriodName; //tên kỳ tính giá thành
        private Decimal accumulatedAllocateAmount; //lũy kế phát sinh kỳ trước
        private Decimal expensesDuringPeriod; //chi phí phát sinh trong kỳ 
        private Decimal unAcceptedAmount; //Số chưa nghiệm thu
        private Decimal acceptedAmount; //Số tiền nghiệm thu
        public virtual Guid ID
        {
            get; set;
        }
        public virtual DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }

        public virtual string CostingPeriodName
        {
            get { return costingPeriodName; }
            set { costingPeriodName = value; }
        }

        public virtual Decimal AccumulatedAllocateAmount
        {
            get { return accumulatedAllocateAmount; }
            set { accumulatedAllocateAmount = value; }
        }

        public virtual Decimal ExpensesDuringPeriod
        {
            get { return expensesDuringPeriod; }
            set { expensesDuringPeriod = value; }
        }

        public virtual Decimal UnAcceptedAmount
        {
            get { return unAcceptedAmount; }
            set { unAcceptedAmount = value; }
        }

        public virtual Decimal AcceptedAmount
        {
            get { return acceptedAmount; }
            set { acceptedAmount = value; }
        }
    }
}
