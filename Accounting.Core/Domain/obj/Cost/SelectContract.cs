﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class SelectContract
    {
        public virtual bool Select { get; set; }
        public virtual Guid _ID { get; set; }
        public virtual string _ContractCode { get; set; }
        public virtual DateTime _SignedDate { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string ContractCode
        {
            get { return _ContractCode; }
            set { _ContractCode = value; }
        }
        public virtual DateTime SignedDate
        {
            get { return _SignedDate; }
            set { _SignedDate = value; }
        }
    }
}
