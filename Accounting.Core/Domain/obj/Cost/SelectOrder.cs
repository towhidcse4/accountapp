﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class SelectOrder
    {
        public virtual bool Select { get; set; }
        public virtual Guid _ID { get; set; }
        public virtual string _OrderCode { get; set; }
        public virtual string _OrderName { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string OrderCode
        {
            get { return _OrderCode; }
            set { _OrderCode = value; }
        }
        public virtual string OrderName
        {
            get { return _OrderName; }
            set { _OrderName = value; }
        }
    }
}
