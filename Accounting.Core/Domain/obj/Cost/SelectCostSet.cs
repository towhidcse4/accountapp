﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class SelectCostSet
    {
        public virtual bool Select { get; set; }
        public virtual Guid _ID { get; set; }
        public virtual string _CostSetCode { get; set; }
        public virtual string _CostSetName { get; set; }
        public virtual int _CostSetType { get; set; }
        public virtual string _CostSetTypeView { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string CostSetCode
        {
            get { return _CostSetCode; }
            set { _CostSetCode = value; }
        }
        public virtual string CostSetName
        {
            get { return _CostSetName; }
            set { _CostSetName = value; }
        }
        public virtual int CostSetType
        {
            get { return _CostSetType; }
            set { _CostSetType = value; }
        }
        public virtual string CostSetTypeView
        {
            get
            {
                string costSetTypeView = string.Empty;
                switch(CostSetType)
                {
                    case 0:
                        {
                            costSetTypeView = "Đơn hàng";
                            break;
                        }
                    case 1:
                        {
                            costSetTypeView = "Công trình, vụ việc";
                            break;
                        }
                    case 2:
                        {
                            costSetTypeView = "Phân xưởng, phòng ban";
                            break;
                        }
                    case 3:
                        {
                            costSetTypeView = "Quy trình công nghệ sản xuất";
                            break;
                        }
                    case 4:
                        {
                            costSetTypeView = "Sản phẩm";
                            break;
                        }
                    case 5:
                        {
                            costSetTypeView = "Khác";
                            break;
                        }
                    default:
                        {
                            costSetTypeView = "Đơn hàng";
                            break;
                        }
                }
                return costSetTypeView;
            }
            set { _CostSetTypeView = value; }
        }
    }
}
