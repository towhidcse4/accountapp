﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CPAccountAllocationQuantum
    {
        public virtual Guid AccountingObjectID { get; set; }
        public virtual string AccountingObjectCode { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual Decimal? DirectMaterialAmount
        {
            get; set;
        }

        public virtual Decimal? DirectLaborAmount
        {
            get; set;
        }

        public virtual Decimal? GeneralExpensesAmount
        {
            get; set;
        }

        public virtual Decimal? TotalCostAmount
        {
            get; set;
        }
    }
}
