﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class TIOriginalVoucher
    {
        public virtual bool Check { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual Guid ID { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual int TypeVoucher { get; set; }
    }
}
