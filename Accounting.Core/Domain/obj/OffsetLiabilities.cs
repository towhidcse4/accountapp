﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class OffsetLiabilities
    {
        public Guid? AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }    //ID đối tượng kế toán
        public string AccountingObjectName { get; set; }    //tên đối tượng
        public bool Status { get; set; }
        public string AccountNumber { get; set; }           //tài khoản
        public decimal DebitAmountLast { get; set; }        //nợ cuối kỳ
        public decimal CreditAmountLast { get; set; }       //có cuối kỳ
        public decimal DebitAmountLastOriginal { get; set; }        //nợ cuối kỳ
        public decimal CreditAmountLastOriginal { get; set; }       //có cuối kỳ

    }
}
