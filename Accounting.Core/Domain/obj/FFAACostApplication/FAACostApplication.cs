﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class FAACostApplication
    {
        public virtual string FixedAssetCode { get; set; }// mã tài sản cố định
        public virtual string FixedAssetName { get; set; }//tên tài sản cố định
        public virtual string AccountingObjectName { get; set; }// tên tài khoản
        public virtual string DepartmentCode { get; set; }// mã phòng ban
        public virtual string ExpenseItemCode { get; set; } //khoản mục chi phí
        public virtual string ContractCode { get; set; }// mã hợp đồng
        public virtual string CostSetCode { get; set; } //đối tượng tập hợp chi phí
        public virtual decimal Rate { get; set; } //tỷ lệ %
        public virtual decimal Amount { get; set; }// số tiền
        public virtual bool IsIrrationalCost { get; set; }// chi phí không hợp lý
        public BindingList<FAACostApplicationDetail> ListFaaCostApplioncation { get; set; }
    }
}
