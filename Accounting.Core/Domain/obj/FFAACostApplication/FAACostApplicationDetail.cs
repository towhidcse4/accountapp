﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
   public class FAACostApplicationDetail
    {
       public virtual bool Statust{ get; set; }//trạng thái
       public virtual Guid ID { get; set; }
       public virtual string FixedAssetCode { get; set; }// mã tscđ
       public virtual string FixedAssetName { get; set; }// tên tscđ
       public virtual decimal? DepreciationAmount { get; set; }//giá trị khấu hao
       public virtual Guid? AccountingObjectName { get; set; }// số hiệu tài khoản
       public virtual Guid? DepartmentCode { get; set; }// mã phòng ban
       public virtual Guid? ExpenseItemCode { get; set; } //khoản mục chi phí
       public virtual Guid? ContractCode { get; set; }// mã hợp đồng
       public virtual Guid? CostSetID { get; set; } //đối tượng tập hợp chi phí
       public virtual decimal Rate { get; set; }// tỷ lệ 
       public virtual bool IsIrrationalCost { get; set; }// chi phí không hợp lý
      
       public static T CloneObject<T>(T obj)
        {
            using (System.IO.MemoryStream memStream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter(null,
                     new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.Clone));
                binaryFormatter.Serialize(memStream, obj);
                memStream.Seek(0, System.IO.SeekOrigin.Begin);
                return (T)binaryFormatter.Deserialize(memStream);
            }
        }
    }
}
