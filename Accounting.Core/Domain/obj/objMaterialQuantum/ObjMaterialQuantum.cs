﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ObjMaterialQuantum : MaterialQuantum
    {
        public virtual bool Check { get; set; }
        public virtual decimal TotalQuantity { get; set; }
        public virtual decimal Quantity { get; set; }


    }
}
