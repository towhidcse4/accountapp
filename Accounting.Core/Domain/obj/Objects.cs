﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Objects
    {
        public virtual Guid ID { get; set; }
        public virtual string ObjectCode { get; set; }
        public virtual string ObjectName { get; set; }
        public virtual bool IsParentNode { get; set; }
        public virtual Guid? ParentID { get; set; }
        public virtual int ObjectType { get; set; }
        public virtual string ObjectTypeName { get; set; }
        public virtual string CostAccount { get; set; }
    }
}
