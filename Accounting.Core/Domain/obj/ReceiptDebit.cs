﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ReceiptDebit
    {
        public virtual bool CheckColumn { get; set; } 
        public virtual Guid? DetailID { get; set; } 
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string TypeName { get; set; }
        public virtual int TypeID { get; set; }
        public virtual decimal SoPhatSinh { get; set; }
        public virtual decimal SoDaThu { get; set; }
        public virtual decimal SoPhaiThu { get; set; }
        public virtual decimal SoPhaiThuQD { get; set; }
        public virtual decimal SoChuaThu { get; set; }
        public virtual decimal SoChuaThuQD { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public virtual decimal? SoThu { get; set; }
        public virtual decimal? SoThuQD { get; set; }
        public virtual decimal SoThuHT { get; set; }
        public Guid? PaymentClauseID { get; set; }
        public string PaymentClauseCode { get; set; }
        public string PaymentClauseName { get; set; }
        public virtual decimal DiscountRate { get; set; } //Tỷ lệ chiết khấu
        public virtual decimal DiscountAmount { get; set; } // số tiền chiết khấu
        public virtual decimal DiscountAmountOriginal { get; set; }//số tiền chiết khấu quy đổi
        public virtual string DiscountAccount { get; set; }//tài khoản chiết khấu
        public virtual string CurrencyID { get; set; }//loại tiền tương ứng hóa đơn
        public virtual Guid SaleInvoiceID { get; set; }
        public virtual decimal? RefVoucherExchangeRate { get; set; }
        public virtual decimal LastExchangeRate { get; set; }
        public virtual decimal DifferAmount { get; set; }
    }
}
