﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ObjectQuantum
    {
        public virtual Guid ObjectID { get; set; }
        public virtual string ObjectCode { get; set; }
        public virtual string ObjectName { get; set; }
        public virtual string ObjectType { get; set; }
    }
}
