﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class PrepaidExpenseAllocation_Report
    {
        public Guid ID { get; set; }
        public string MaCP { get; set; }
        public string TenCP { get; set; }
        public DateTime? NgayGhiNhan { get; set; }
        public decimal? SoTien { get; set; }
        public int? SoKyPB { get; set; }
        public int? SoKyDaPB { get; set; }
        public int? SoKyPBConLai { get; set; }
        public decimal? LuyKeDaPB { get; set; }
        public decimal? SoTienConLai { get; set; }
    }
    public class PrepaidExpenseAllocation_Report_DNDetail
    {
        public string Period { get; set; }
    }
}
