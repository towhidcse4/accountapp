﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class FAAllocationImportTax
    {
        public bool CheckColumn { get; set; }
        public Guid FixedAssetID { get; set; }
        public string FixedAssetCode { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal ImportTaxExpenseRate { get; set; }
        public decimal ImportTaxExpenseAmount { get; set; }
        public FAIncrementDetail fAIncrementDetail { get; set; }
        public FAAllocationImportTax()
        {
            Quantity = 0;
            Amount = 0;
            ImportTaxExpenseRate = 0;
            ImportTaxExpenseAmount = 0;
        }
    }
}
