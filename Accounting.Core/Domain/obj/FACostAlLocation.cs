﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class FACostAllocation
    {
        public virtual Guid FixedAssetID { get; set; }
        public virtual string FixedAssetCode { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal FreightRate { get; set; }
        public virtual decimal FreightAmount { get; set; }
        public virtual FAIncrementDetail FAIncrementDetail { get; set; }

        public FACostAllocation()
        {
            Quantity = 0;
            Amount = 0;
            FreightRate = 0;
            FreightAmount = 0;
        }
    }
}
