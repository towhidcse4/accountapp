﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class CostAllocation
    {
        private Guid _materialGoodsID;
        private string _materialGoodCode;
        private string _description;
        private decimal _quantity;
        private decimal _amount;
        private decimal _freightRate;
        private decimal _freightAmount;
        private PPInvoiceDetail _ppInvoiceDetail;

        public CostAllocation()
        {
            Quantity = 0;
            Amount = 0;
            FreightRate = 0;
            FreightAmount = 0;
        }

        public Guid MaterialGoodsID
        {
            get { return _materialGoodsID; }
            set { _materialGoodsID = value; }
        }

        public string MaterialGoodsCode
        {
            get { return _materialGoodCode; }
            set { _materialGoodCode = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public decimal Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public decimal FreightRate
        {
            get { return _freightRate; }
            set { _freightRate = value; }
        }

        public decimal FreightAmount
        {
            get { return _freightAmount; }
            set { _freightAmount = value; }
        }

        public PPInvoiceDetail PPInvoiceDetail
        {
            get { return _ppInvoiceDetail; }
            set { _ppInvoiceDetail = value; }
        }
    }
}
