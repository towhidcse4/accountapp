﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class TICostAllocation
    {
        public virtual Guid ToolsID { get; set; }
        public virtual string ToolsCode { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal FreightRate { get; set; }
        public virtual decimal FreightAmount { get; set; }
        public virtual TIIncrementDetail TIIncrementDetail { get; set; }

        public TICostAllocation()
        {
            Quantity = 0;
            Amount = 0;
            FreightRate = 0;
            FreightAmount = 0;
        }
    }
}
