﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ForSetOff
    {
        public virtual bool Status { get; set; }
        public virtual string AccountingObjectName { get; set; } // chuỗi có dạng: "Tên đối tượng - Mã đối tượng"
        public virtual string AccountNumber { get; set; }
        public virtual decimal DebitAmount { get; set; }
        public virtual decimal CreditAmount { get; set; }
        public virtual Guid AccountingObjectId { get; set; }
    }
    
}
