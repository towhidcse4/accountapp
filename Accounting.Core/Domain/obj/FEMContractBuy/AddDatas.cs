﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class AddDatas
    {
        public virtual string NameFile { get; set; }
        public virtual long SizeFile { get; set; }
        public virtual string FormatFile { get; set; }
        public virtual string Depcription { get; set; }
        public virtual string Local { get; set; }
    }
}
