﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
   public class EMContractBuy
   {
        public virtual Guid GLID { get; set; }
        public virtual string TypeName { get; set; }
       public virtual DateTime Date { get; set; }
       public virtual string No { get; set; }
       public virtual string Reason { get; set; }
       public virtual decimal Amount { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string InvoiceNo { get; set; }
    }
}
