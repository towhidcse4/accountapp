﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
   public class ListLibitiesReport
   {
        public bool Status { get; set; }
        public virtual DateTime? NgayThongBao { get; set; }// ngày thông báo
        public virtual DateTime? CongNoTuNgay { get; set; }// ngày kết thúc tính công nợ 
        public virtual string CongNoDenNgay { get; set; }// ngày bắt đầu tính công nợ        
        public virtual string TaxCode { get; set; } // diễn giải
        public virtual string Address { get; set; } // diễn giải
        public virtual Guid? ID { get; set; }// mã khách hàng
        public virtual string MaKhanhHang { get; set; }// mã khách hàng
        public virtual string TenKhachHang { get; set; }// tên khách hàng
        public virtual decimal TongTien { get; set; }// tổng số công nợ
        public virtual decimal? NoPhaiTra { get; set; }
        public virtual decimal? LaiNoPhaiTra { get; set; }
        public virtual decimal? SoDuDauKy { get; set; }
        public virtual decimal? SoDuCuoiKy { get; set; }
        public virtual decimal? SoPhatSinh { get; set; }
        public virtual decimal? SoDaThu { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual List<ListLibitiesReportDetail> bill { get; set; }

    }
    public class ListLibitiesReportDetail
    {
        public virtual DateTime? Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual decimal? SoPhatSinh { get; set; }
        public virtual decimal? SoDaThu { get; set; }
        public virtual decimal? SoChuaThu { get; set; }
        public virtual decimal? SoChuaThuQD { get; set; }
        public virtual decimal? LaiNo { get; set; }
        public virtual string CurrencyID { get; set; }//loại tiền tương ứng hóa đơn

    }
    public class ListLibitiesReport_period
    {
        public string Period { get; set; }
    }
}
