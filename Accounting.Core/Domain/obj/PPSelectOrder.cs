﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class PPSelectOrder
    {
        public virtual Guid ID { get; set; }
        public virtual bool CheckColumn { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual string MaterialGoodsName { get; set; }
        public virtual decimal? QuantityRemaining { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? QuantityInward { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual PPOrderDetail PpOrderDetail { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
       public virtual string Description { get; set; }
        //add by cuongpv
        public virtual int? OrderPriority { get; set; }
        //end add by cuongpv
    }
}
