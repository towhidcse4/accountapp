﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class DirectCost
    {
        private string costingObject; // Đối tượng THCP
        private Decimal amount; // Số tiền
        private DateTime dateVoucher; //ngày chứng từ
        private DateTime dateAccounting; //ngày hạch toán
        private string reason; //diễn giải
        private string no; // số chứng từ

        public virtual string CostingObject
        {
            get { return costingObject; }
            set { costingObject = value; }
        }

        public virtual Decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual DateTime DateVoucher
        {
            get { return dateVoucher; }
            set { dateVoucher = value; }
        }

        public virtual DateTime DateAccounting
        {
            get { return dateAccounting; }
            set { dateAccounting = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }
    }
}
