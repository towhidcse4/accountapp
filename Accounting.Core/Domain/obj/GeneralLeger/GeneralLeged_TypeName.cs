﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
   public class GeneralLeged_TypeName:GeneralLedger
    {
        private string _typename;
        public virtual string TypeName
        {
            get { return _typename; }
            set { _typename = value; }
        }
    }
}
