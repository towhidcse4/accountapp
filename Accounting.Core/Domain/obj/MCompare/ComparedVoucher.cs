﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.Domain
{
    /// <summary>
    /// chứng từ đã thực hiện đối chiếu
    /// </summary>
    [Serializable]
    public class ComparedVoucher
    {
        public virtual DateTime Date { get; set; } // ngày chứng từ
        public virtual string No { get; set; } // số chứng từ
        public virtual string AccountingObjectName { get; set; } // Tên đối tượng nộp
        public virtual decimal Amount { get; set; } // tiền sau quy đổi
        public virtual string TypeName { get; set; } // Tên loại chứng từ
        public virtual string Description { get; set; } // diễn giải
    }
}
