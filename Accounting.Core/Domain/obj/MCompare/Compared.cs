﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.Domain
{
    /// <summary>
    /// chứng từ đã thực hiện đối chiếu
    /// </summary>
    [Serializable]
    public class Compared
    {
        public virtual Guid? Id { get; set; } // ID của chứng từ chi
        public virtual Guid BankAccountID { get; set; }
        public virtual bool Status { get; set; }
        public virtual string BankAccount { get; set; } // Số tài khoản ngân hàng
        public virtual string BankName { get; set; } // Tên ngân hàng
        public virtual DateTime DateCompare { get; set; } // ngày đối chiếu
        public List<Guid?> lstIDGeneralLedger = new List<Guid?>();
    }
}
