﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.Domain
{
    /// <summary>
    /// chứng từ thu tiền
    /// </summary>
    [Serializable]
    public class CollectionVoucher
    {
        public virtual bool Status { get; set; }
        public virtual Guid? Id { get; set; } // ID của chứng từ chi
        public virtual int TypeId { get; set; } // loại chứng từ (--> chứng từ thuộc nghiệp vụ nào)
        public virtual DateTime Date { get; set; } // ngày chứng từ
        public virtual string No { get; set; } // số chứng từ
        public virtual string AccountingObjectName { get; set; } // mã đối tượng nộp
        public virtual decimal Rate { get; set; } // tỷ giá
        public virtual decimal Amount { get; set; } // tiền sau quy đổi
        public virtual decimal AmountOriginal { get; set; } // tiền sau nguyên tệ
        public virtual string Description { get; set; } // diễn giải
        public virtual Guid BankAccountDetailId { get; set; }
        public virtual DateTime MathDate { get; set; } // Ngày chứng từ đối chiếu


    }
}
