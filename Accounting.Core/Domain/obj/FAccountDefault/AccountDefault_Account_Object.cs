﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class AccountDefault_Account_Object:Account
    {
        private bool _check;
        public virtual bool Check
        {
            get { return _check; }
            set { _check = value; }
        }
    }
}
