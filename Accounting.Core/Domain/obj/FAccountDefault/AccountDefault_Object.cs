﻿using System;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountDefault_Object
    {
        private Guid _ID;
        private int _TypeID;
        private string _TypeName;
        private string _COGSAccount;
        private string _COGSAccountDF;
        private string _CreditAccount;
        private string _CreditAccountDF;
        private string _DebitAccount;
        private string _DebitAccountDF;
        private string _DeductionDebitAccount;
        private string _DeductionDebitAccountDF;
        private string _DiscountAccount;
        private string _DiscountAccountDF;
        private string _ImportTaxAccount;
        private string _ImportTaxAccountDF;
        private string _InventoryAccount;
        private string _InventoryAccountDF;
        private string _SpecialConsumeTaxAccount;
        private string _SpecialConsumeTaxAccountDF;
        private string _VATAccount;
        private string _VATAccountDF;
        IList<AccountDefault> _accountDefaults;

        public virtual IList<AccountDefault> AccountDefaults
        {
            get { return _accountDefaults; }
            set { _accountDefaults = value; }
        } 

        //private string _ColumnName;
        //private string _FilterAccount;
        //private string _DefaultAccount;
        //public virtual string DefaultAccount
        //{
        //    get { return _DefaultAccount; }
        //    set { _DefaultAccount = value; }
        //}
        //public virtual string FilterAccount
        //{
        //    get { return _FilterAccount; }
        //    set { _FilterAccount = value; }
        //}
        //public virtual string ColumnName
        //{
        //    get { return _ColumnName; }
        //    set { _ColumnName = value; }
        //}
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }
        public virtual string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public virtual string COGSAccount
        {
            get { return _COGSAccount; }
            set { _COGSAccount = value; }
        }
        public virtual string COGSAccountDF
        {
            get { return _COGSAccountDF; }
            set { _COGSAccountDF = value; }
        }
        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public virtual string CreditAccountDF
        {
            get { return _CreditAccountDF; }
            set { _CreditAccountDF = value; }
        }
        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public virtual string DebitAccountDF
        {
            get { return _DebitAccountDF; }
            set { _DebitAccountDF = value; }
        }
        public virtual string DeductionDebitAccount
        {
            get { return _DeductionDebitAccount; }
            set { _DeductionDebitAccount = value; }
        }
        public virtual string DeductionDebitAccountDF
        {
            get { return _DeductionDebitAccountDF; }
            set { _DeductionDebitAccountDF = value; }
        }
        public virtual string DiscountAccount
        {
            get { return _DiscountAccount; }
            set { _DiscountAccount = value; }
        }
        public virtual string DiscountAccountDF
        {
            get { return _DiscountAccountDF; }
            set { _DiscountAccountDF = value; }
        }
        public virtual string ImportTaxAccount
        {
            get { return _ImportTaxAccount; }
            set { _ImportTaxAccount = value; }
        }
        public virtual string ImportTaxAccountDF
        {
            get { return _ImportTaxAccountDF; }
            set { _ImportTaxAccountDF = value; }
        }
        public virtual string InventoryAccount
        {
            get { return _InventoryAccount; }
            set { _InventoryAccount = value; }
        }
        public virtual string InventoryAccountDF
        {
            get { return _InventoryAccountDF; }
            set { _InventoryAccountDF = value; }
        }
        public virtual string SpecialConsumeTaxAccount
        {
            get { return _SpecialConsumeTaxAccount; }
            set { _SpecialConsumeTaxAccount = value; }
        }
        public virtual string SpecialConsumeTaxAccountDF
        {
            get { return _SpecialConsumeTaxAccountDF; }
            set { _SpecialConsumeTaxAccountDF = value; }
        }
        public virtual string VATAccount
        {
            get { return _VATAccount; }
            set { _VATAccount = value; }
        }
        public virtual string VATAccountDF
        {
            get { return _VATAccountDF; }
            set { _VATAccountDF = value; }
        }

        public AccountDefault_Object()
        {
            AccountDefaults = new List<AccountDefault>();
        }
    }
}
