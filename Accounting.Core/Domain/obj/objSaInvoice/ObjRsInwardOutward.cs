﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ObjRsInwardOutward : RSInwardOutwardDetail
    {
        public bool Check { get; set; }

        public string No { get; set; }
        public DateTime Date { get; set; }
        public string AccountingObjectName { get; set; }
        public string AccountingObjectAddress { get; set; }
        public Guid? SAInvoiceID { get; set; }
        public Guid? AccountingObjectID { get; set; }
    }
}
