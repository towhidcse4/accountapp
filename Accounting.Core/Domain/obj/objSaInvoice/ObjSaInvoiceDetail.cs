﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ObjSaInvoiceDetail : SAInvoiceDetail
    {
        public bool Check { get; set; }
    }
}
