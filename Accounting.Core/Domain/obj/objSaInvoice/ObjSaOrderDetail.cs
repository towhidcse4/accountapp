﻿using Accounting.Core.Domain;
using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ObjSaOrderDetail
    {
        public virtual Guid ID { get; set; }
        public virtual bool Check { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual string MaterialGoodsCode { get; set; }
        public virtual string MaterialGoodsName { get; set; }
        public virtual decimal? QuantityStore { get; set; }     //số lượng còn lại = sl đơn hàng () - sl nhận
        public virtual decimal? QuantityExport { get; set; }    //số lượng xuất
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual Guid? PaymentClauseID { get; set; }
        public virtual SAOrderDetail SAOrderDetails { get; set; }

    }
}
