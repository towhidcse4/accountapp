﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ObjSaQuoteDetail : SAQuoteDetail
    {
        public bool Check { get; set; }
    }
}
