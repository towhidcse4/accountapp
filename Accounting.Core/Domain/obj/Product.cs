﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Accounting.Core
{
    public class Product
    {
        public virtual string Code { get; set; }
        public virtual string ProdName { get; set; }
        public virtual string ProdUnit { get; set; }
        public virtual decimal ProdQuantity { get; set; }
        public virtual decimal ProdPrice { get; set; }
        public virtual decimal Total { get; set; }
        public virtual int? VATRate { get; set; }
        public virtual decimal VATAmount { get; set; }
        [XmlIgnore()]
        public virtual decimal? DiscountRate { get; set; }
        [XmlIgnore()]
        public virtual decimal? Discount { get; set; }
        [XmlIgnore()]
        public virtual decimal DiscountAmount { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Extra { get; set; }
        [XmlIgnore()]
        public virtual string VATRateString
        {
            get
            {
                if (VATRate == -1) return "Không chịu thuế";
                if (VATRate == -2) return "Không tính thuế";
                return VATRate.ToString()+".00";
            }        }
    }
}
