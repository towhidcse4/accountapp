﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.OverdueInterest
{
   public class OverdueInterest
    {
        public virtual Guid ID { get; set; }

        public virtual string MaKhachHang { get; set; }
        public virtual string TenKhachHang { get; set; }

        public virtual string SoChungTu { get; set; }

        public virtual string LoaiTien { get; set; }

        public virtual decimal NoQuaHan { get; set; }

        public virtual decimal NoQuaHanQd { get; set; }

        public virtual decimal TienPhat { get; set; }

        public virtual decimal TienPhatQd { get; set; }

        public virtual string TkPhaiThu { get; set; }


    }

}
