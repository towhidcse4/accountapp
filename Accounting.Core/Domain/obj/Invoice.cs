﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj
{
   public class Invoice
    {
        public bool Status { get; set; } // trạng thái hóa đơn
        public DateTime VoucherDate { get; set; } // ngày chứng từ
        public string VoucherNo { get; set; } // số chứng từ
        public string InvoiceNo { get; set; } // số hóa đơn
        public double SoPhaiThu { get; set; } // số tiền phải thu
        public double SoPhaiThuQd { get; set; } // số tiền phải thu quy đổi (= số tiền phải thu * tỉ giá)
        public double SoChuaThu { get; set; } // số tiền chưa thu
        public double SoChuaThuQd { get; set; } // số tiền chưa thu quy đổi (= số tiền chưa thu * tỷ giá)
        public string AccountNumber { get; set; } // tài khoản phải thu
        public string EmployeeName { get; set; } // tên nhân viên
        public double Amount { get; set; }     // số tiền thu
        //public string PaymentClauseName { get; set; } // tên điều khoản thanh toán
        public double TotalAmount { get; set; } // số tiền quy đổi
        public double DiscountAmount { get; set; } // tiền chiết khấu quy đổi
        public double DiscountRate { get; set; } // tỷ lệ chiết khấu
        public string PaymentClauseCode { get; set; } // mã điều khoản thanh toán
        public double TransferRate { get; set; } // tỷ lệ chiết khấu
        public double TransferAmount { get; set; } // số tiền chiết khấu
        public string DiscountAccount { get; set; } // tài khoản chiết khấu
       
    
    }
}
