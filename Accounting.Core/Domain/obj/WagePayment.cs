﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class WagePayment
    {
        public bool Check { get; set; }
        public Guid? AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public Guid? DepartmentID { get; set; }
        public string AccountNumber { get; set; }
        public string DepartmentCode { get; set; }
        public string BankName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Amount { get; set; }
        public int Type { get; set; }
        public WagePayment()
        {
            Amount = 0;
            TotalAmount = 0;
        }
    }
}
