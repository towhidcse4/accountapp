﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class RSInwardVoucher
    {
        public virtual Guid ID { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string No { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual decimal? TotalAmountOriginal { get; set; }
        public virtual IList<RSInwardVoucherDetail> RSInwardVoucherDetails { get; set; }
        public RSInwardVoucher()
        {
            RSInwardVoucherDetails = new List<RSInwardVoucherDetail>();
        }
    }
    public class RSInwardVoucherDetail
    {
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string LotNo { get; set; }
        public virtual Guid? RepositoryID { get; set; }
        public virtual string Description { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? QuantityConvert { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual int? OrderPriority { get; set; }


    }
}
