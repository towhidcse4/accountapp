﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class MCPaymentVoucher
    {
        public virtual  Guid ID { get; set; }
        public virtual string No { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual  DateTime? Date { get; set; }
        public virtual string Payers { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal? TotalAllOriginal { get; set; }
        public virtual decimal? TotalAll { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string CurrencyID { get; set; }
    }
}
