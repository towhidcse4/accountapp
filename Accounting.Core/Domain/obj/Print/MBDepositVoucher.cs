﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class MBDepositVoucher
    {
        public virtual Guid ID { get; set; }
        public virtual string No { get; set; }
        public virtual string BankBranchName { get; set; }
        public virtual string BankAccount { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public DateTime Date { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual string Tel { get; set; }
        public virtual string Branch { get; set; }
        public virtual string BankCode { get; set; }
        public virtual decimal? TotalAmount { get; set; }
        public virtual decimal? TotalAmountOriginal { get; set; }
        public virtual string Reason { get; set; }
        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual string CurrencyName { get; set; }
        public virtual string BankName { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? TotalAllOriginal { get; set; }
        public virtual decimal? TotalVATAmountOriginal { get; set; }
        public virtual decimal? TotalVATAmount { get; set; }
        public virtual decimal? TotalDiscountAmountOriginal { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual decimal? TotalExportTaxAmount { get; set; }
        public virtual decimal? TotalAll { get; set; }
        public virtual IList<MBDepositVoucherDetail> MBDepositVoucherDetails { get; set; }
        public MBDepositVoucher()
        {
            MBDepositVoucherDetails = new List<MBDepositVoucherDetail>();
        }
    }
    public class MBDepositVoucherDetail
    {
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string ExportTaxAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual int OrderPriority { get; set; }
    }
}
