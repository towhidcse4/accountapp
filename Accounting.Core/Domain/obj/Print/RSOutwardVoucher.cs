﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class RSOutwardVoucher
    {
        public virtual Guid ID { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string No { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectAdress { get; set; }
        public virtual string Reason { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual decimal? TotalAmount { get; set; }
        public virtual decimal? TotalAmountOriginal { get; set; }
        public virtual decimal? TotalDiscountAmountOriginal { get; set; }
        public virtual decimal? TotalVATAmountOriginal { get; set; }
        public virtual decimal? MaxVATRate { get; set; }
        public virtual decimal? MaxDiscountRate { get; set; }
        
        public virtual IList<RSOutwardVoucherDetail> RSOutwardVoucherDetails { get; set; }
        public RSOutwardVoucher()
        {
            RSOutwardVoucherDetails = new List<RSOutwardVoucherDetail>();
        }        
    }
    public class RSOutwardVoucherDetail
    {
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string Repository { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string Description { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? QuantityConvert { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? DiscountRate { get; set; }


    }
}
