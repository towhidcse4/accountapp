﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj
{
    public class Registration
    {
        public Guid ProductID { get; set; }
        public string TaxCode { get; set; }
        public DateTime ActivedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public bool Status { get; set; }
        public int LimitedVoucher { get; set; }
        public string ServicePackage { get; set; }
    }
}
