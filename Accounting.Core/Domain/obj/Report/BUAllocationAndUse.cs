﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BUAllocationAndUse
    {
        public bool IsParent { get; set; }              // mục cha của phẩn mục chi
        public string SortOrder { get; set; }           //STT   
        public string BudgetItemName { get; set; }      //Mục chi                   //Bảng BudgetItem xác định đầu là toàn khoản Chi dựa vào BudgetItemType = 0 
        public decimal PlanAmount { get; set; }         //Dự Toán                   //Bảng EMEstimate xác định đâu là dự toán Chi dựa vào Type = 0//Dựa vào AmountMonth EstimateBudgetYear để xác định số tiền dự toán theo tháng trong năm
        public decimal AllowcationAmount { get; set; }  //Số Yêu Cầu                //Bảng EMAllocationRequiredDetail cột RequestAmount
        public decimal UsedAmount { get; set; }         //Số được duyệt             //Bảng EMAllocationRequiredDetail cột  AprovedAmount
        public decimal Amount { get; set; }             //Số đã cấp phát            //Bảng EMAllocationRequiredDetail cột  Amount
        public decimal ApprovedAmount { get; set; }     //Số đã chi                 //Chị hiền chưa đưa công thức
        public decimal UnUsedAmount { get; set; }       //Số chưa chi
        public decimal LeftOverPlanAmount { get; set; } //Số còn lại với dự toán
        public decimal Percentage { get; set; }         //Tỷ lệ %
        public decimal Thang1 { get; set; }
        public decimal Thang2 { get; set; }
        public decimal Thang3 { get; set; }
        public decimal Thang4 { get; set; }
        public decimal Thang5 { get; set; }
        public decimal Thang6 { get; set; }
        public decimal Thang7 { get; set; }
        public decimal Thang8 { get; set; }
        public decimal Thang9 { get; set; }
        public decimal Thang10 { get; set; }
        public decimal Thang11 { get; set; }
        public decimal Thang12 { get; set; }
        public BUAllocationAndUse() { }
        public BUAllocationAndUse(int i)
        {
            IsParent = i % 35 == 0;

            SortOrder = IsParent ? i / 35 + "" : i / 35 + "." + i % 35;
            BudgetItemName = "Tên item " + i;
            PlanAmount = 20020 + (i % 35) * 20000;
            AllowcationAmount = 2010 * i + 1000;
            UsedAmount = 1300 * i;
            Amount = 12000 * (i % 35);
            ApprovedAmount = 1000;
            UnUsedAmount = Amount - ApprovedAmount;
            LeftOverPlanAmount = PlanAmount - ApprovedAmount;
            Percentage = ApprovedAmount / PlanAmount;
        }
    }
    public class BUAllocationAndUseDetail
    {
        public string Period { get; set; }
    }
}
