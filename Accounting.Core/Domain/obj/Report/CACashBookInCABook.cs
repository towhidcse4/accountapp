﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class CACashBookInCABook
    {
        public Guid RefID { get; set; }
        public DateTime? RefDate { get; set; }
        public DateTime? PostedDate { get; set; }
        public string ReceiptRefNo { get; set; }
        public string PaymentRefNo { get; set; }
        public string AccountObjectName { get; set; }
        public string JournalMemo { get; set; }
        public string CurrencyID { get; set; }
        public decimal? TotalReceiptFBCurrencyID { get; set; }
        public decimal? TotalPaymentFBCurrencyID { get; set; }
        public decimal? ClosingFBCurrencyID { get; set; }
        public string Note { get; set; }
        public decimal? Sum_SoTon { get; set; }
        public int RefType { get; set; }
    }
    public class CACashBookInCABookDetail
    {
        public string Period { get; set; }
    }
}
