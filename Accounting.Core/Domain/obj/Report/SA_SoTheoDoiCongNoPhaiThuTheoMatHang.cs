﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_SoTheoDoiCongNoPhaiThuTheoMatHang
    {
        public Guid MaterialGoodsID { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string MaterialGoodsName { get; set; }
        public decimal? GiaTriHang { get; set; }
        public decimal? Thue { get; set; }
        public decimal? ChietKhau { get; set; }
        public decimal? GiamGia { get; set; }
        public decimal? TraLai { get;set; }
        public decimal? DaThanhToan { get; set; }
        public decimal? ConLai { get; set; }
    }
    public class SA_SoTheoDoiCongNoPhaiThuTheoMatHang_period
    {
        public string Period { get; set; }
    }
}
