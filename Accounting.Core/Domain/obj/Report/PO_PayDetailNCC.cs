﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class PO_PayDetailNCC
    {
        public DateTime? Ngay_HT { get; set; }
        public DateTime? ngayCtu { get; set; }
        public string SoCtu { get; set; }
        public string DienGiai { get; set; }
        public string TK_CONGNO { get; set; }
        public string TkDoiUng { get; set; }
        public string AccountObjectCode { get; set; }
        public string AccountObjectName { get; set; }
        public string AccountNumber { get; set; }
        public decimal OpeningDebitAmount { get; set; }
        public decimal OpeningCreditAmount { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal ClosingDebitAmount { get; set; }
        public decimal ClosingCreditAmount { get; set; }
        //add by cuongpv
        public decimal DebitAmountOC { get; set; }
        public decimal CreditAmountOC { get; set; }
        public decimal ClosingDebitAmountOC { get; set; }
        public decimal ClosingCreditAmountOC { get; set; }
        //end add by cuongpv
        public decimal Sum_DuNo { get; set; }
        public decimal Sum_DuCo { get; set; }
        
    }

    public class PO_PayDetailNCC_Detail
    {
        public string Period { get; set; }
    }

}
