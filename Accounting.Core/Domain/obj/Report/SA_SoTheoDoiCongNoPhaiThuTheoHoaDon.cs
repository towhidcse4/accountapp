﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_SoTheoDoiCongNoPhaiThuTheoHoaDon
    {
        public Guid SAInvoiceID { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string AccountingObjectName { get; set; }
        public decimal? GiaTriHoaDon { get; set; }
        public decimal? TraLai { get; set; }
        public decimal? GiamGia { get; set; }
        public decimal? ChietKhauTT_GiamTruKhac { get; set; }
        public decimal? SoDaThu { get; set; }
        public decimal? SoConPhaiThu { get; set; }
    }
    public class SA_SoTheoDoiCongNoPhaiThuTheoHoaDon_period
    {
        public string Period { get; set; }
    }
}
