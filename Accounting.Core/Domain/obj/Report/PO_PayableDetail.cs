﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class PO_PayableDetail
    {
        public string AccountObjectCode { get; set; }	//Mã NCC
        public string AccountObjectName { get; set; }	//Tên NCC
        public string AccountNumber { get; set; }	//TK nợ
        public decimal OpeningDebitAmount { get; set; }	//DK Nợ
        public decimal OpeningCreditAmount { get; set; } //DK Có
        public decimal DebitAmount { get; set; }	// PS Nợ 
        public decimal CreditAmount { get; set; }	// PS Có
        public decimal ClosingDebitAmount { get; set; }	//CK Nợ
        public decimal ClosingCreditAmount { get; set; }//CK Có
        //add by cuongpv
        public decimal OpeningDebitAmountOC { get; set; }	//DK Nợ
        public decimal OpeningCreditAmountOC { get; set; } //DK Có
        public decimal DebitAmountOC { get; set; }	// PS Nợ 
        public decimal CreditAmountOC { get; set; }	// PS Có
        public decimal CloseDebitAmountOC { get; set; }	//CK Nợ
        public decimal CloseCreditAmountOC { get; set; }//CK Có
        //end add by cuongpv
    }
    public class PO_PayableDetail_SP
    {
        public string Period { get; set; }
    }
}
