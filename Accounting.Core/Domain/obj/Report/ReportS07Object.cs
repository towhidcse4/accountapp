﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.Report
{
    public class ReportS07
    {
        public int OrderType { get; set; }
        public string AccountNumber { get; set; }
        public string RefDate { get; set; }
        public string PostedDate { get; set; }
        //  public string RefNo { get; set; }
        public string ReceiptRefNo { get; set; }
        public string PaymentRefNo { get; set; }
        public string Hyperlink { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal ClosingAmount { get; set; }
        public string Note { get; set; }
        public string Description { get; set; }
    }
    public class ReportS07Detail
    {
        public string Period { get; set; } // Kỳ báo cáo
        public string ProductInfo { get; set; } // chữ ký chương trình
        public string CompanyInfoName { get; set; }
        public string CompanyInfoAddress { get; set; }
        public string CompanyLogoPath { get; set; }
        public ReportS07Detail()
        {
            Period = "Báo cáo test";
        }
    }

}
