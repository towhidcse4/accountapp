﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class PO_DiaryBook
    {
        public Guid RefID { get; set; }
        public DateTime PostedDate { get; set; }	//Ngày hạch toán
        public DateTime RefDate { get; set; }	//Ngày chứng từ
        public string RefNo { get; set; }	//Số chứng từ
        public DateTime? InvoiceDate { get; set; }	//Ngày hoá đơn
        public string InvoiceNo { get; set; }	//Số hoá đơn
        public string Description { get; set; }	//Diễn giải
        public decimal? GoodsAmount { get; set; }	//Hàng hoá
        public decimal? EquipmentAmount { get; set; }	//Nguyên vật liệu
        public decimal? AnotherAmount { get; set; }	//Số tiền tk khác
        public decimal? PaymentableAmount { get; set; }	//Phải trả người bán
        public string AnotherAccount { get; set; }
        public int TypeID { get; set; }
    }
    public class PO_DiaryBook_Detail {
        public string Period { get; set; }
    }
}
