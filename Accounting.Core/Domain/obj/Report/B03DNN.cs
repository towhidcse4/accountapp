﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class B03DNN
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public decimal AmountCurrentYear { get; set; }
        public decimal AmountLastYear { get; set; }
        public bool IsItalic { get; set; }
        public bool IsBold { get; set; }
    }
    public class B03DNNDetail
    {
        public string Period { get; set; }
    }
}
