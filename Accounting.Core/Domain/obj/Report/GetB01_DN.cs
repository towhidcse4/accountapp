﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GetB01_DN
    {
        public string ItemCode { get; set; }	//mã số
        public string ItemName { get; set; }	//Chỉ tiêu
        public int ItemIndex { get; set; }
        public string Description { get; set; }	//Thuyết minh
        public bool IsBold { get; set; }
        public decimal? Amount { get; set; }	// Số cuối kỳ
        public decimal? PrevAmount { get; set; }    // Số đầu kỳ

    }
    public class GetB01_DNDetail
    {
        public string Period { get; set; }
        public string MaBaoCao { get; set; }
        public string thamso { get; set; }
    }
}
