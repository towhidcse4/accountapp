﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class DividendPayable
    {   // Báo cáo cổ tức phải trả
        public DateTime LockDate { get; set; } //Ngày Chốt                          "Trong Bảng EMDevidend
        public string ShareHolderGroupCode { get; set; }//Mã nhóm cổ đông           "Trong Bảng ShareHolderGroup
        public string ShareHolderGroupName { get; set; }//Tên Nhóm Cổ đông          "Trong Bảng ShareHolderGroup
        public float DevidendPerSharePercent { get; set; }//Tỷ Lệ cổ tức            "Trong Bảng EMDevidend
        public string Description { get; set; }//Đợt chia cổ tức                    "Trong Bảng EMDevidend
        public string ShareHolderCode { get; set; }//Mã Cổ đông                    " Trong Bảng EMShareHolder
        public string ShareHolderName { get; set; }//Tên Cổ đông                    " Trong Bảng EMShareHolder
        public string ContactIdentificationNo { get; set; }//Số CMT cổ đông         " Trong Bảng EMShareHolder
        public DateTime ContactIssueDate { get; set; }//Ngày cấp số CMT            " Trong Bảng EMShareHolder
        public decimal HoldQuantiy { get; set; } //Số cổ phần nắm giữ             "Trong Bảng EMDevidendDetail
        public decimal AmountPayable { get; set; }//Số tiền                       "Trong Bảng EMDevidendDetail
        public DividendPayable() { }

    }

    public class DividendPayableDetail
    {
        public string Period { get; set; }
    }
}
