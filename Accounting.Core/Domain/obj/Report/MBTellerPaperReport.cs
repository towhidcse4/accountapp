﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class MBTellerPaperReport
    {
        public Guid ID { get; set; }
        public string No { get; set; }
        public DateTime Date { get; set; }
        public string BankAccount { get; set; }
        public string BankName { get; set; }
        public string BankBranchName { get; set; }
        public string AccountingObjectName { get; set; }
        public string AccountingObjectAddress { get; set; }
        public string AccountingObjectBankAccount { get; set; }
        public string AccountingObjectBankName { get; set; }
        public string Tel { get; set; }
        public string CurrencyName { get; set; }
        public decimal TotalAmount { get; set; }
        public string Reason { get; set; }
        public string Branch { get; set; }
        public string BankCode { get; set; }
    }
}
