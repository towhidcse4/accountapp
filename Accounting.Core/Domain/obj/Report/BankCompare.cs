﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BankCompare
    {
        public string BankName { get; set; }            // Số tài khoản ngân hàng
        public string BankAccount { get; set; }         // Số tài khoản ngân hàng
        public decimal OpenningAmount { get; set; }     // Số dư đầu kỳ
        public decimal RealOpenningAmount { get; set; } // Thực dư đầu kỳ
        public DateTime Date { get; set; }              // Ngày
        public string RefNo { get; set; }               // Số
        public string Hyperlink { get; set; }
        public string RefType { get; set; }             // Loại
        public string Description { get; set; }         // Diễn giải
        public string Check { get; set; }               // Xác nhận (ký hiệu 'v' nếu true, trống nếu false)        
        public decimal DebitAmount { get; set; }        // Thu
        public decimal RealDebitAmount { get; set; }    // Thực thu
        public decimal CreditAmount { get; set; }       // Chi
        public decimal RealCreditAmount { get; set; }   // Thực thi
        public decimal ClosingAmount { get; set; }      // Tồn
        public decimal RealClosingAmount { get; set; }  // Thực tồn
        public BankCompare()
        {
            BankName = "";
            BankAccount = "";
            Date = DateTime.MinValue;
        }
        public BankCompare(int i)
        {
            BankName = "BankName" + i / 35;
            BankAccount = "Bank Account " + i / 35;
            OpenningAmount = 20000 + i;
            RealOpenningAmount = 10000 + i;
            Date = DateTime.Now.AddDays(i);
            RefNo = "Ref No " + i;
            Hyperlink = "link" + i;
            RefType = "type" + i;
            Description = "Description" + i;
            Check = i % 3 == 0 ? "v" : "";
            DebitAmount = 200100 + 1000 * i;
            RealDebitAmount = 1500 * i;
            CreditAmount = 3100 - (i % 2) * i;
            RealCreditAmount = 35000 * 200 * i;
        }
    }
    public class BankCompareDetail
    {
        public string Period { get; set; }
    }
}
