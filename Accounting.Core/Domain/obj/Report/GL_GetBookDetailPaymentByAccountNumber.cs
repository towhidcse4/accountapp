﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GL_GetBookDetailPaymentByAccountNumber
    {
        public DateTime? PostedDate { get; set; }	//Ngày hạch toán
        public DateTime? RefDate { get; set; }	//Ngày chứng từ
        public string RefNo { get; set; }	//Số chứng từ
        public string JournalMemo { get; set; }	//Diễn giải
        public string AccountNumber { get; set; }	//Tài khoản
        public string CorrespondingAccountNumber { get; set; }	//Tài khoản đối ứng
        public Decimal DebitAmount { get; set; }	// PS Nợ
        public Decimal CreditAmount { get; set; }	// PS Có
        public Decimal ClosingDebitAmount { get; set; }	//Dư nợ CK
        public Decimal ClosingCreditAmount { get; set; }	//Dư có CK
        public Decimal Sum_DuNo { get; set; }
        public Decimal Sum_DuCo { get; set; }
        public string AccountGroupKind { get; set; }
        public Guid? RefID { get; set; }
        public int? RefType { get; set; }

    }
    public class GL_GetBookDetailPaymentByAccountNumber_Detail
    {
        public string Period { get; set; }
        //public string LoaiTien { get; set; } comment by cuongpv
    }
}