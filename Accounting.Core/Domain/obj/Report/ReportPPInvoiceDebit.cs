﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ReportPPInvoiceDebit
    {
        public string Account { get; set; }//tài khoản
        public string AccountingObjectCode { get; set; }//ID đối tượng kế toán
        public string AccountingObjectName { get; set; }//tên đối tượng
        public string Hyperlink  { get; set; } 
        public decimal DebitAmountFirst {get; set; }//nợ đầu kỳ
        public decimal CreditAmountFirst { get; set; }//có đầu kỳ
        public decimal DebitAmountArise { get; set; }//phát sinh nợ
        public decimal CreditAmountArise { get; set; }//phát sinh có
        public decimal DebitAmountAccum { get; set; }//lũy kế phát sinh nợ
        public decimal CreditAmountAccum { get; set; }//lũy kế phát sinh có
        public decimal DebitAmountLast { get; set; }//nợ cuối kỳ
        public decimal CreditAmountLast { get; set; }//có cuối kỳ

        public ReportPPInvoiceDebit()
        {
            DebitAmountLast = DebitAmountFirst + DebitAmountArise - CreditAmountArise;
            CreditAmountLast = CreditAmountFirst + CreditAmountArise - DebitAmountArise;
        }
    }
}
