﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S03A1DNN
    {
        public DateTime PostedDate { get; set; }        //Ngày Tháng Ghi Sổ (PostedDate)
        public string RefNo { get; set; }               //Số chứng từ (No)
        public DateTime RefDate { get; set; }           //Ngày Tháng Chứng từ (Date)
        public string Description { get; set; }         //Diễn Giải (Description)
        public string Reason { get; set; }              //Diễn Giải khi click vào but toán cộng gộp
        public string ListAccount { get; set; }         //Loại Tài Khoản        ListAccount = "acc1,acc2,acc3,acc4,acc5";
        public string AccountCorresponding { get; set; }
        public string Hyperlink { get; set; }
        public decimal DebitAmount { get; set; }        //Ghi Nợ Tài KHoản
        public decimal DebitAmount1 { get; set; }       //
        public decimal DebitAmount2 { get; set; }       //
        public decimal DebitAmount3 { get; set; }       //
        public decimal DebitAmount4 { get; set; }       //
        public decimal DebitAmount5 { get; set; }       //
        public decimal DebitAmount6 { get; set; }       //Siền Debit của trường hợp không hiển thị được
        public string AccountNumber6 { get; set; }      //Số hiệu là số tiền của tài khoản đối ứng tương ứng như tài khoản trên
        public S03A1DNN() { }
        public S03A1DNN(int i)
        {
            ListAccount = "111,1111,1112,1234,1235,126";
            PostedDate = DateTime.Now.Date.AddDays(i);
            RefDate = PostedDate.AddDays(-i % 3);
            RefNo = "Ref no " + i;
            //Hyperlink
            Description = "Description " + i;
            DebitAmount = 10230 * (i % 25 + 1);
            DebitAmount1 = 0;
            DebitAmount2 = 0;
            DebitAmount3 = 0;
            DebitAmount4 = 0;
            DebitAmount5 = 0;
            switch (i % 6)
            {
                case 1: DebitAmount1 = DebitAmount; break;
                case 2: DebitAmount2 = DebitAmount; break;
                case 3: DebitAmount3 = DebitAmount; break;
                case 4: DebitAmount4 = DebitAmount; break;
                case 5: DebitAmount5 = DebitAmount; break;
                case 0: DebitAmount6 = DebitAmount; break;
            }
            AccountNumber6 = "" + i;
            Hyperlink = ""+i;
        }

    }

    public class S03A1DNNDetail
    {
        public string Period { get; set; }
    }
}
