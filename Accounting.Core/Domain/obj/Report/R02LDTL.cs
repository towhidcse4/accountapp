﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class R02LDTL
    {
        public string DepartmentID { get; set; }                // phòng
        public string DepartmentCode { get; set; }              //Mã phòng
        public string DepartmentName { get; set; }              //Tên phòng
        public string AccountingObjectName { get; set; }        //Tên nhân viên
        public string Title { get; set; }                       //Chức vụ
        public string SalaryCoefficient { get; set; }           //Hệ số lương
        public string BasicWage { get; set; }                   //Lương cơ bản
        public string WorkingDayUnitPrice { get; set; }         //Đơn giá công
        public string NumberOfPaidWorkingdayTimeSheet { get; set; }     //Lương thời gian - Số công         
        public string PaidWorkingdayAmount { get; set; }                //Lương thời gian - Số tiền
        public string NumberOfNonWorkingdayTimeSheet { get; set; }      //Lương nghỉ việc - Số công
        public string NonWorkingdayAmount { get; set; }                 //Lương nghỉ việc - Số tiền
        public string OtherAllowance { get; set; }                      //Phụ cấp
        public string TotalAmount { get; set; }                         //Tổng
        public string PeriodIAmount { get; set; }                       //Tạm ứng kỳ
        public string SocialInsuranceAmount { get; set; }               //Khấu trừ - BHXH
        public string HealthInsurranceAmount { get; set; }              //Khấu trừ - BHYT
        public string UnemploymentInsurranceAmount { get; set; }        //Khấu trừ - BHTN
        public string IncomeTaxAmount { get; set; }                     //Khấu trừ - Thuế TNCN phải nộp
        public string SumOfDeductionAmount { get; set; }                //Khấu trừ - Cộng
        public string PeriodIIAmount { get; set; }                      //Kỳ 2 đc lĩnh - Số tiền
        public string Sign { get; set; }                                //Ký nhận
    }
}
