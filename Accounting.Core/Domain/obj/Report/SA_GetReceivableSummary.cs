﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_GetReceivableSummary
    {
        public long RowNum { get; set; }
        public Guid AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public string AccountObjectAddress { get; set; }
        public string AccountObjectTaxCode { get; set; }
        public string AccountNumber { get; set; }
        public long AccountCategoryKind { get; set; }
        public decimal? OpenningDebitAmountOC { get; set; }
        public decimal? OpenningDebitAmount { get; set; }
        public decimal? OpenningCreditAmountOC { get; set; }
        public decimal? OpenningCreditAmount { get; set; }
        public decimal? DebitAmountOC { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? CreditAmountOC { get; set; }
        public decimal? CreditAmount { get; set; }
       // public decimal? AccumDebitAmountOC { get; set; }
        //public decimal? AccumDebitAmount { get; set; }
        //public decimal? AccumCreditAmountOC { get; set; }
        //public decimal? AccumCreditAmount { get; set; }
        public decimal? CloseDebitAmountOC { get; set; }
        public decimal? CloseCreditAmountOC { get; set; }
        public decimal? CloseDebitAmount { get; set; }
        public decimal? CloseCreditAmount { get; set; }
        public string AccountObjectGroupListCode { get; set; }
        public string AccountObjectCategoryName { get; set; }

    }
    public class SA_GetReceivableSummary_period
    {
        public string Period { get; set; }
    }
}
