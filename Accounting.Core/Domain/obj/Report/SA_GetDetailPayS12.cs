﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_GetDetailPayS12
    {
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public Guid AccountObjectID { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public DateTime? NgayThangGS { get; set; }
        public string Sohieu { get; set; }
        public DateTime? ngayCTu { get; set; }
        public string Diengiai { get; set; }
        public string TKDoiUng { get; set; }
        public DateTime? ThoiHanDuocCKhau { get; set; }
        public decimal PSNO { get; set; }
        public decimal PSCO { get; set; }
        public decimal DuNo { get; set; }
        public decimal DuCo { get; set; }
        public decimal Sum_DuNo { get; set; }
        public decimal Sum_DuCo { get; set; }
    }
    public class SA_GetDetailPayS12_Detail
    {
        public string Period { get; set; }
        public string LoaiTien { get; set; }
    }
}
