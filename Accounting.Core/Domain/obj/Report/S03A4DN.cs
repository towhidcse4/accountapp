﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S03A4DNN
    {
        public DateTime PostedDate { get; set; }            //ngày ghi sổ
        public DateTime Date { get; set; }                  //ngày chứng từ
        public string No { get; set; }                      // số chứng từ = chứng từ gốc OriginalNo nếu không có = No
        public string Description { get; set; }             //diễn giải
        public decimal Amount { get; set; }          //số tiền phải thu của khách hàng
        public decimal Amount5111 { get; set; }              //thành tiền nếu là hàng hóa
        public decimal Amount5112 { get; set; }              //thành tiền nếu là thành phẩm
        public decimal Amount5113 { get; set; }            //thành tiền nếu là dịch vụ
        public decimal AmountOther { get; set; }            //thành tiền khác - 5118
        public string Hyperlink { get; set; }
        public S03A4DNN() { }
        public S03A4DNN(int i)
        {
            PostedDate = DateTime.Now.Date.AddDays(i);
            Date = PostedDate.AddMonths(-1);
            No = "No " + i;
            Hyperlink = "Hyp" + i;
            Description = "Description" + i;
            Amount = 10000 + i * 200;
            if (i % 4 == 0) Amount5111 = Amount;
            else if (i % 4 == 1) Amount5112 = Amount;
            else if (i % 4 == 2) Amount5113 = Amount;
            else AmountOther = Amount;
        }
    }
    public class S03A4DNNDetail
    {
        public string Period { get; set; }
    }
}
