﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S03A3DNN
    {
        public DateTime PostedDate { get; set; }            //ngày ghi sổ
        public DateTime Date { get; set; }                  //ngày chứng từ
        public string No { get; set; }                      // số chứng từ = chứng từ gốc OriginalNo nếu không có = No
        public string Description { get; set; }             //diễn giải
        public decimal Amount156 { get; set; }              //thành tiền nếu là hàng hóa
        public decimal Amount152 { get; set; }              //thành tiền nếu là nguyên liệu, vật liệu
        public string AccountOther { get; set; }            //số tài khoản ghi nợ khác
        public decimal AmountOther { get; set; }            //số tiền ghi nợ khác
        public decimal PayableAmount { get; set; }          //số tiền phải trả nhà cung cấp
        public string Hyperlink { get; set; }
        public S03A3DNN() { }
        public S03A3DNN(int i)
        {
            PostedDate = DateTime.Now.Date.AddDays(i);
            Date = PostedDate.AddDays(-i % 3);
            No = "Ref no " + i;
            Hyperlink = "link " + i;
            Description = "Description " + i;
            Amount152 = 10230 * (i % 25 + 1);
            Amount156 = 10230 * (i % 25 + 1);
            AmountOther = 10230 * (i % 25 + 1); 
            PayableAmount = 10230 * (i % 25 + 1);
            AccountOther = "" + i;
        }

    }

    public class S03A3DNNDetail
    {
        public string Period { get; set; }
    }
}
