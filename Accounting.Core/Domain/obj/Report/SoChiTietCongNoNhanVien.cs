﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SoChiTietCongNoNhanVien
    {
        public virtual string AccountingObjectCode { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string SoCT { get; set; }
        public virtual string DienGiai { get; set; }
        public virtual string TK { get; set; }
        public virtual string TKDU { get; set; }
        public virtual decimal PSNo { get; set; }
        public virtual decimal PSCo { get; set; }

    }
}
