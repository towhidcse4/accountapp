﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S11DNN
    {
        public string DepartmentCode { get; set; }              //Mã phòng ban
        public string DepartmentName { get; set; }              //Tên phòng ban
        public string No { get; set; }                          //Số hiệu chứng từ                  - ghi tăng
        public string Hyperlink { get; set; }                   //                                  - Ghi tăng
        public DateTime PostedDate { get; set; }                //Ngày tháng số hiệu chứng từ       - ghi tăng
        public string Description { get; set; }                 //Tên nhãn hiệu quy cách            - Ghi Tăng
        public string Unit { get; set; }                        //Đơn vị Tính để mặc định null
        public decimal Quantity { get; set; }                   //Số lượng                          - ghi tăng
        public decimal OriginalPriceDebitAmount { get; set; }   //Đơn giá ghi tăng
        public decimal Moneys { get; set; }                     //Số Tiền ghi tăng
        public string DesRefNo { get; set; }                    // Số chứng từ                      - Ghi giảm  
        public DateTime DesPostedDate { get; set; }             // Ngày Tháng Số chứng từ           - Ghi giảm  
        public string Reason { get; set; }                      //Lý do ghi giảm vẫ là trường Description
        public string DesQuantity { get; set; }                //Số lượng                          - ghi Giảm
        public decimal? DesMoneys { get; set; }                  //Số Tiền ghi Giảm chính bằng OriginalPriceCreaditAmount
        public string Note { get; set; }                        //Ghi chú ghi giảm
        public string FAName { get; set; }
        public string FACode { get; set; }
        public Guid? FAID { get; set; }
        public Guid? DepartmentID { get; set; }
        public S11DNN() { }
        public S11DNN(int i)
        {
            DepartmentCode = "Mã phòng " + i / 35;
            DepartmentName = "Tên phòng " + i;
            No = "No" + i;
            Hyperlink = "Hyper" + i;
            PostedDate = DateTime.Now.AddDays(-123);
            Description = "Des " + i;
            Unit = "Unit" + i;
            Quantity = 11 * i + 212;
            OriginalPriceDebitAmount = 120 + 23 * i;
            Moneys = i * 430 + 34100;
            DesRefNo = "Des no" + i;
            DesPostedDate = PostedDate.AddDays(43);
            Reason = "Reason" + i;
            DesMoneys = 567890 + i;
            Note = "Note" + i;
        }



    }
    public class S11DNNDetail
    {
        public string Period { get; set; }
    }
}
