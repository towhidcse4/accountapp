﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.Report
{
    public class ReportFilter
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public System.Type FieldType { get; set; }
        public string Operator { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Sort { get; set; }
        public string Condition { get; set; }

        public ReportFilter(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }
        public ReportFilter(string name, string description, string typeName, string opper, string value1, string value2, string sort, string condition)
        {
            Name = name;
            DateTime d;
            Description = description;
            FieldType = System.Type.GetType(typeName);
            Operator = opper;
            Value1 = value1;
            Value2 = value2;
            Sort = sort;
            Condition = condition;
        }
        public static System.Type GetType(string typeName)
        {
            var type = System.Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }
}
