﻿using System;

namespace Accounting.Core.Domain
{
public class S05aDNN
{
    public Guid ReferenceID { get; set; }
    public int TypeID { get; set; }//typeid tương ứng
    public DateTime Date { get; set; } // tương đương RefDate
    public string Reason { get; set; } //không dùng
    public decimal BalanceAmount { get; set; }//số dư bị trừ dần trong kỳ

    public int OrderType { get; set; } //nếu là số dư đầu kỳ = -1, nếu là không phải là 0
    public string AccountNumber { get; set; }//số tài khoản
    public DateTime PostedDate { get; set; }
    public string ReceiptRefNo { get; set; }//chứng từ thu
    public string PaymentRefNo { get; set; }//chứng từ chi
    public string Hyperlink { get; set; }   //RefNo
    public decimal DebitAmount { get; set; }
    public decimal CreditAmount { get; set; }
    public decimal ClosingAmount { get; set; } //số dư đầu kì
    public string Description { get; set; } //diễn giải

    public string Note { get; set; }//ghi chú

    public S05aDNN()
    {
        //TypeID = null;
        //Date = null;
        //Reason = "";
        //BalanceAmount = 0;
        OrderType = 0;
        AccountNumber = "";
        //PostedDate = null;
        //ReceiptRefNo = "";
        //PaymentRefNo = "";
        //DebitAmount = null;
        //CreditAmount = null;
        ClosingAmount = 0;
        //Description = "Số dư đầu kỳ";
        //Note = "";
        //Hyperlink = "";
    }
    public S05aDNN(string i)
    {
        //TypeID = null;
        //Date = null;
        //Reason = "";
        //BalanceAmount = 0;
        OrderType = -1;
        AccountNumber = i;
        //PostedDate = null;
        //ReceiptRefNo = "";
        //PaymentRefNo = "";
        //DebitAmount = null;
        //CreditAmount = null;
        ClosingAmount = 0;
        Description = "Số dư đầu kỳ";
        //Note = "";
        //Hyperlink = "";
    }
}

    public class S05aDNNDetail
    {
        public string Period { get; set; } // Kỳ báo cáo
        public S05aDNNDetail()
        {
            Period = "Báo cáo test";
        }
    }
}
