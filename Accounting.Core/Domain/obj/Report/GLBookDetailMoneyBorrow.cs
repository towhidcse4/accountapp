﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GLBookDetailMoneyBorrow
    {
        public DateTime PostedDate { get; set; }
        public DateTime RefDate { get; set; }
        public string RefNo { get; set; }
        public string JournalMemo { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string CorrespondingAccountNumber { get; set; }
        public DateTime DueDate { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public string AccountObjectCode { get; set; }
        public string AccountObjectName { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
    }
    public class GLBookDetailMoneyBorrowDetail
    {
        public string Period { get; set; }
    }
}
