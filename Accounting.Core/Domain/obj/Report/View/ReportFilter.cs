﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ReportFilter
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public System.Type FieldType { get; set; }
        public string Operator { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Sort { get; set; }
        public string Condition { get; set; }
        public bool Show { get; set; }
        public ReportFilter()
        {
            Name = "";
            Description = "";
            Operator = "";
            Sort = "";
            Condition = "";
            Show = true;
        }
        public ReportFilter(string name, string description, bool show,string sort)
        {
            this.Name = name;
            this.Description = description;
            this.Show = show;
            this.Sort = sort;
        }
        public ReportFilter(string name, string description, string typeName, string opper, string value1, string value2, string sort, string condition)
        {
            Name = name;
            Description = description;
            FieldType = System.Type.GetType(typeName);
            Operator = opper;
            Value1 = value1;
            Value2 = value2;
            Sort = sort;
            Condition = condition;
        }
        public static System.Type GetType(string typeName)
        {
            var type = System.Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }

    public class Filter
    {
        public string Name { get; set; }
        public List<ReportFilter> ListReportFilter { get; set; }
        public Filter(string name, List<ReportFilter> lst)
        {
            Name = name;
            ListReportFilter = lst;
        }
        public Filter()
        {
            Name = "Name";
            ListReportFilter = new List<ReportFilter>();
        }
    }
    public class FilterCollection : System.Collections.CollectionBase
    {
        public FilterCollection()
        {
            //    List = new List<Filter>();
        }
        // public List<Filter> List { get; set; }
        public Filter this[int index]
        {
            get { return this.List[index] as Filter; }
        }
        public Filter this[string name]
        {
            get
            {
                foreach (Filter f in List)
                    if (f.Name == name) return f;
                return null;
            }
        }
        public Filter Add(Filter value)
        {
            List.Add(value);
            return value;
        }
    }
}
