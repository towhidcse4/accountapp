﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class EMContractReport
    {
        public bool Check { get; set; }
        public Guid ID { get; set; }
        public string ContractCode { get; set; }
        public string ContractName { get; set; }

        public EMContractReport()
        {
            Check = false;
        }
    }
}
