﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BankAccountDetailReport
    {
        public bool Check { get; set; }
        public int Order { get; set; }
        public Guid ID { get; set; }
        public Guid BankID { get; set; }
        public string BankAccount { get; set; }
        public string BankName { get; set; }
        public string Description { get; set; }
        public BankAccountDetailReport()
        {
            Check = false;
        }
        
    }
}
