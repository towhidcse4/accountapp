﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class GeneralLedgerChild : GeneralLedger
    {
        private Guid _IDChild;
        private Guid _GeneralLedgerID;
        private int _CategoriesActive;
        
        public virtual Guid IDChild
        {
            get { return _IDChild; }
            set { _IDChild = value; }
        }

        public virtual Guid GeneralLedgerID
        {
            get { return _GeneralLedgerID; }
            set { _GeneralLedgerID = value; }
        }

        public virtual int CategoriesActive
        {
            get { return _CategoriesActive; }
            set { _CategoriesActive = value; }
        }
    }
}
