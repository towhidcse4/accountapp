﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class MaterialGoodsReport
    {
        public bool Check { get; set; }
        public Guid ID { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string MaterialGoodsName { get; set; }
        public Guid MaterialGoodsGSTID { get; set; }
        public Guid MaterialGoodsCategoryID { get; set; }

        public Guid RepositoryID { get; set; }
        public MaterialGoodsReport()
        {
            Check = false;
        }
        
    }
}
