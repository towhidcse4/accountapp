﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class DepartmentReport
    {
        public bool Check { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string DepartmentCode { get; set; }
        public virtual string DepartmentName { get; set; }
        public DepartmentReport()
        {
            Check = false;
        }
    }
}
