﻿using System;

namespace Accounting.Core.Domain
{
    public class AccountingObjectReport
    {
        public Boolean Check { get; set; }
        private string _accountingObjectCode;
        private string _accountingObjectName;
        private Guid _iD;
        private string _address;
        private int _objectType ;
        private string _accountingObjectCategory;
        private Guid _accountObjectGroupID;
        private string _DepartmentName;
        public virtual Guid AccountObjectGroupID
        {
            get { return _accountObjectGroupID; }
            set { _accountObjectGroupID = value; }
        }
        public virtual string AccountingObjectCategory
        {
            get { return _accountingObjectCategory; }
            set { _accountingObjectCategory = value; }
        }


        public virtual string AccountingObjectCode
        {
            get { return _accountingObjectCode; }
            set { _accountingObjectCode = value; }
        }
        public virtual int ObjectType
        {
            get { return _objectType; }
            set { _objectType = value; }
        }
        public virtual string AccountingObjectName
        {
            get { return _accountingObjectName; }
            set { _accountingObjectName = value; }
        }

        public virtual Guid ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
       public virtual string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public virtual string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }

        public AccountingObjectReport ()
        {
            Check = false;
        }
    }
}
