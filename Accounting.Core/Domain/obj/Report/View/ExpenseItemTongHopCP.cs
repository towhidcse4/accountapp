﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class ExpenseItemTongHopCP
    {
        public bool Check { get; set; }
        private Guid iD;
        private string expenseItemCode;
        private string expenseItemName;
        private int grade;
        private bool isActive;
        private bool isSecurity;
        private string description;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;
        private int expenseItemType;
        string expenseItemTypeView;
        public int ExpenseType
        {
            get { return expenseItemType; }
            set { expenseItemType = value; }
        }
        public string ExpenseItemTypeView
        {
            get { return expenseItemTypeView; }
            set { expenseItemTypeView = value; }
        }
        public Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public string ExpenseItemCode
        {
            get { return expenseItemCode; }
            set { expenseItemCode = value; }
        }

        public string ExpenseItemName
        {
            get { return expenseItemName; }
            set { expenseItemName = value; }
        }

        public int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
        public ExpenseItemTongHopCP()
        {
            Check = false;
        }
    }
}
