﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class AccountingObjectCategoryReport
    {
        public bool Check { get; set; }
        public Guid ID { get; set; }
        public string AccountingObjectCategoryCode { get; set; }
        public string AccountingObjectCategoryName { get; set; }

        public AccountingObjectCategoryReport()
        {
            Check = false;
        }
    }
}
