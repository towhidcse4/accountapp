﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public sealed class AccountReport
    {
        private bool _check;
        private Guid _iD;
        private string _accountNumber;
        private string _accountName;
        private bool _isParentNode;
        private int _grade;
        private string _accountGroupID;
        private int _accountGroupKind;
        private string _accountGroupKindView = string.Empty;
        private bool _isActive = false;
        private string _detailType;
        private string _accountNameGlobal;
        private string _description;
        private Guid? _parentID;
        public AccountReport()
        {
            Check = false;
        }
        public AccountReport(string accountNumber)
        {
            this._accountNumber = accountNumber;
        }
        public bool Check
        {
            get { return _check; }
            set { _check = value; }
        }
        public Guid ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        public string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        public string AccountName
        {
            get { return _accountName; }
            set { _accountName = value; }
        }

        public string AccountNameGlobal
        {
            get { return _accountNameGlobal; }
            set { _accountNameGlobal = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public Guid? ParentID
        {
            get { return _parentID; }
            set { _parentID = value; }
        }

        public bool IsParentNode
        {
            get { return _isParentNode; }
            set { _isParentNode = value; }
        }

        public int Grade
        {
            get { return _grade; }
            set { _grade = value; }
        }

        public string AccountGroupID
        {
            get { return _accountGroupID; }
            set { _accountGroupID = value; }
        }

        public int AccountGroupKind
        {
            get { return _accountGroupKind; }
            set { _accountGroupKind = value; }
        }

        public string AccountGroupKindView
        {
            get { return _accountGroupKindView; }
            set { _accountGroupKindView = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public string DetailType
        {
            get { return _detailType; }
            set { _detailType = value; }
        }
       
    }
}
