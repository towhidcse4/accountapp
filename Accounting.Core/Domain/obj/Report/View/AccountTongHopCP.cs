﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class AccountTongHopCP
    {
        public bool Check { get; set; }
        public Guid ID { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public AccountTongHopCP()
        {
            Check = false;
        }

    }
}
