﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class MaterialGoodsCategoryReport
    {
        public bool Check { get; set; }
        public Guid ID { get; set; }
        public string MaterialGoodsCategoryCode { get; set; }
        public string MaterialGoodsCategoryName { get; set; }

        public MaterialGoodsCategoryReport()
        {
            Check = false;
        }
    }
}
