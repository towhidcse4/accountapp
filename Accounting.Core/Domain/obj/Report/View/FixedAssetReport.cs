﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class FixedAssetReport
    {
        public virtual bool Check { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string FixedAssetCode { get; set; }
        public virtual string FixedAssetName { get; set; }
        public virtual Guid FixedAssetCategoryID { get; set; }
        public FixedAssetReport()
        {
            Check = false;
        }
    }
}
