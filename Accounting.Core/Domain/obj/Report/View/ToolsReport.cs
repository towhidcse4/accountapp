﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ToolsReport
    {
        public virtual bool Check { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string ToolsCode { get; set; }
        public virtual string ToolsName { get; set; }
        public ToolsReport()
        {
            Check = false;
        }
    }
}
