﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class RepositoryReport
    {
        public bool Check { get; set; }
        public Guid ID { get; set; }
        public string RepositoryCode { get; set; }
        public string RepositoryName { get; set; }
        public RepositoryReport()
        {
            Check = false;
        }
        
    }
}
