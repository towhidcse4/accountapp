﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
   /*Công nợ phải trả*/
    public class PayableReceiptableSummary
    {
        public string AccountNumber { get; set; }           //tài khoản
        public string AccountingObjectCode { get; set; }    //ID đối tượng kế toán
        public string AccountingObjectName { get; set; }    //tên đối tượng
        public string Hyperlink { get; set; }
        public string AccountNumberDetail { get; set; }
        public decimal DebitAmountFirst { get; set; }        //nợ đầu kỳ
        public decimal CreditAmountFirst { get; set; }      //có đầu kỳ
        public decimal DebitAmountArise { get; set; }       //phát sinh nợ
        public decimal CreditAmountArise { get; set; }      //phát sinh có
        public decimal DebitAmountAccum { get; set; }       //lũy kế phát sinh nợ
        public decimal CreditAmountAccum { get; set; }      //lũy kế phát sinh có
        public decimal DebitAmountLast { get; set; }        //nợ cuối kỳ
        public decimal CreditAmountLast { get; set; }       //có cuối kỳ

        public void AmountLast()
        {
            DebitAmountLast = 0;
            CreditAmountLast = 0;
            decimal tongNo = 0 + DebitAmountFirst + DebitAmountArise - CreditAmountArise;
            decimal tongCo = 0 + CreditAmountFirst + CreditAmountArise - DebitAmountArise;
            DebitAmountLast = tongNo;
            CreditAmountLast = tongCo;
        }
        public PayableReceiptableSummary()
        {
            AccountNumber = "";
            AccountingObjectCode = "";
            AccountingObjectName = "";
            Hyperlink = "";
        }

        public PayableReceiptableSummary(int i)
        {
            AccountNumber = "Tài khoản: "+i/35;
            AccountingObjectCode = "Mã công ty "+i;
            AccountingObjectName = "Tên công ty "+i;
            Hyperlink = "hyperlink";
            DebitAmountFirst = 1000 * (i % 2 - 1)*(i % 2 - 1);
            CreditAmountFirst = 2000 *(i%2) ;
            DebitAmountArise = 32000 * (i % 2); ;
            CreditAmountArise = 900 * (i % 2 - 1) * (i % 2 - 1);
            DebitAmountAccum = 1500 * (i % 2 - 1) * (i % 2 - 1);
            CreditAmountAccum = 5000 * (i % 2);
            DebitAmountLast = 20010 * (i % 2);
            CreditAmountLast = 2100 * (i % 2 - 1) * (i % 2 - 1);
        }
    }
    public class PayableReceiptableSummaryDetail
    {
        public string Period { get; set; } // Kỳ báo cáo
        public PayableReceiptableSummaryDetail()
        {
            Period = "SummaryPayableDebtDetail";
        }
    }
}
