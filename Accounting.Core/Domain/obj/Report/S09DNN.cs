﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S09DNN
    {
        public int OrderType { get; set; }
       public string MaterialGoodsCode { get; set; }   //Mã Vật tư hàng hóa
        public string MaterialGoodsName { get; set; }   //Tên Vật tư Hàng Hóa   
        public string Unit { get; set; }                //Đơn vị tính  
        public DateTime Date { get; set; }              //Ngày tháng chứng từ
        public string InPut { get; set; }               //Số hiệu chứng từ nhập
        public string OutPut { get; set; }              //Số hiệu chứng từ xuất
        public string Description { get; set; }         //Diễn giải chứng từ trong sổ kho
        public DateTime PostedDate { get; set; }        //Ngày Nhập Xuất của chứng từ
        public decimal IWQuantity { get; set; }         //Số lượng nhập
        public decimal OWQuantity { get; set; }         //Số lượng Xuất
        public decimal OpeningBalance { get; set; }     //Số tồn đầu kỳ       
        public string SignatureAccountancy = "";        // Chữ ký của kế toán
        public decimal TotalExitsRow { get; set; }      //Tổng tồn trên từng dòng
        public string No { get; set; }                  //Số hiệu trứng từ      
        public string Hyperlink { get; set; }
        public S09DNN() { }
        public S09DNN(int i)
        {
            if (i % 35 == 1)
            {
                OrderType = -1;
                MaterialGoodsCode = "Mã Hàng " + i / 35;
                MaterialGoodsName = "Tên hàng" + i / 35;
                Unit = "đơn vị " + i;
                Description = "Description " + i;
                OpeningBalance = 200000;
            }
            else
            {
                OrderType = 0;
                MaterialGoodsCode = "Mã Hàng " + i / 35;
                MaterialGoodsName = "Tên hàng" + i / 35;
                Unit = "đơn vị " + i;
                Date = DateTime.Now.AddDays(i);
                InPut = "In Ref No " + i;
                OutPut = "Out Ref No " + i;
                Description = "Description " + i;
                PostedDate = DateTime.Now.AddDays(21);
                IWQuantity = i % 3 == 1 ? 1000 * i : 0;
                OWQuantity = i % 3 == 1 ? 0 : 2001 * i;
                SignatureAccountancy = "";
                Hyperlink = "Hyperlink";
            }
        }
    }
    public class S09DNNDetail
    {
        public string Period { get; set; }
        public S09DNNDetail()
        {
            Period = "Kỳ báo cáo";
        }
    }
}
