﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting
{
    public class S11DNN_PBCCDC
    {
        public Guid? ToolsID { get; set; }
        public string ToolsCode { get; set; }//maCCDC
        public string ToolsName { get; set; }//tenCCDC
        public DateTime? IncrementDate { get; set; }//ngay ghi tang
        public DateTime? PostedDate { get; set; }//ngay ghi giam
        public decimal? Amount { get; set; }//tong gia tri
        public int? AllocationTimes { get; set; }//tong so ky phan bo
        public decimal? AllocatedAmount { get; set; }//gia tri phan bo hang ky
        public int? DecrementAllocationTime { get; set; }//so ky da phan bo
        public int? RemainingAllocationTime { get; set; }//so ky phan bo con lai
        public decimal? DecrementAmount { get; set; }//Gia tri da phan bo
        public decimal? RemainingAmount { get; set; }//Gia tri con lai
    }

    public class S11DNN_PBCCDC_period
    {
        public string Period { get; set; }
    }
}
