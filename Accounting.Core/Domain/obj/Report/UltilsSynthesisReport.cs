﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using FX.Core;

namespace Accounting.Core.Domain
{

    public class UltilsSynthesisReport
    {
        private static IGeneralLedgerService GeneralLedgerService
        {
            get { return IoC.Resolve<IGeneralLedgerService>(); }
        }
        private static List<GeneralLedger> _lstGeneralLedger = new List<GeneralLedger>();
        public static List<GeneralLedger> LstGeneralLedger
        {
            get
            {
                if (_lstGeneralLedger.Count == 0)
                    _lstGeneralLedger = GeneralLedgerService.GetAll();
                return _lstGeneralLedger;
            }
            set { _lstGeneralLedger = value; }
        }
        public static decimal SD_CO(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate <= toDate && a.Account != null
                    && a.Account.StartsWith(accountNumbers)
                    select new
                    {
                        OpeningDebit = a.DebitAmount
                    }).Sum(c => c.OpeningDebit);

        }
        public static decimal SD_CO_CtiettheoTK(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return SD_CO(fromDate, toDate, accountNumbers);
        }
        public static decimal SD_CO_CtiettheoTK_Dtuong(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return SD_CO(fromDate, toDate, accountNumbers);
        }
        public static decimal SD_NO(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate <= toDate && a.Account != null
                    && a.Account.StartsWith(accountNumbers)
                    select new
                    {
                        OpeningCreadit = a.CreditAmount
                    }).Sum(c => c.OpeningCreadit);
        }
        public static decimal SD_NO_CtiettheoTK(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return SD_NO(fromDate, toDate, accountNumbers);
        }
        public static decimal SD_NO_CtiettheoTK_Dtuong(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return SD_NO(fromDate, toDate, accountNumbers);
        }

        //Số dư đầu kỳ và số tồn đầu kỳ như nhau
        public static decimal SD_DK(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return GeneralLedgerService.GetOpeningBalanceDD(fromDate, accountNumbers);
        }

        public static decimal PS_NO(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate >= fromDate && a.PostedDate <= toDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && !a.TypeID.ToString().StartsWith("70")
                    select new { ps = a.DebitAmount - a.CreditAmount }).Sum(c => c.ps);
        }
        public static decimal PS_CO(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate >= fromDate && a.PostedDate <= toDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && !a.TypeID.ToString().StartsWith("70")
                    select new { ps = a.CreditAmount - a.DebitAmount }).Sum(c => c.ps);

        }
        public static decimal PS_DU(DateTime fromDate, DateTime toDate, string accountNumbers, string accountCorresponding)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate >= fromDate && a.PostedDate <= toDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding == accountCorresponding && !a.TypeID.ToString().StartsWith("70")
                    select new { ps = a.CreditAmount + a.DebitAmount }).Sum(c => c.ps);
        }
        public static decimal PS_NO_NT(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate < fromDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && !a.TypeID.ToString().StartsWith("70")
                    select new { ps = a.DebitAmount - a.CreditAmount }).Sum(c => c.ps);
        }
        public static decimal PS_CO_NT(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate < fromDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && !a.TypeID.ToString().StartsWith("70")
                    select new { ps = a.CreditAmount - a.DebitAmount }).Sum(c => c.ps);

        }
        public static decimal PS_DU_NT(DateTime fromDate, DateTime toDate, string accountNumbers, string accountCorresponding)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate < fromDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding == accountCorresponding && !a.TypeID.ToString().StartsWith("70")
                    select new { ps = a.CreditAmount + a.DebitAmount }).Sum(c => c.ps);
        }

        /// <summary>
        /// Hàm lấy danh sách dữ liệu theo accountNumbers và accountCorresponding trả về một list GeneralLedgerChild
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <param name="accountCorresponding"></param>
        /// <returns></returns>
        public static IEnumerable<GeneralLedgerChild> Ds_PS_DU_HoatDong(string accountNumbers, string accountCorresponding)
        {
            List<GeneralLedgerChild> dsChild = new List<GeneralLedgerChild>();
            List<GeneralLedger> ds = (from a in LstGeneralLedger
                                      where a.Account != null && a.AccountCorresponding != null
                                            && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding.StartsWith(accountCorresponding) && !a.TypeID.ToString().StartsWith("70")
                                      select a).ToList();
            foreach (GeneralLedger row in ds)
            {
                GeneralLedgerChild generalLedgerChild = new GeneralLedgerChild();
                PropertyInfo[] propertyInfo = generalLedgerChild.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(generalLedgerChild, propItem.GetValue(row, null), null);
                    }
                }
                generalLedgerChild.GeneralLedgerID = generalLedgerChild.ID;
                generalLedgerChild.ID = Guid.NewGuid();
                generalLedgerChild.CategoriesActive = 0;
                dsChild.Add(generalLedgerChild);
            }
            return dsChild;
        }


        public static decimal PS_DU_CtietDauTu(DateTime fromDate, DateTime toDate, string accountNumbers,
            string accountCorresponding, IEnumerable<GeneralLedgerChild> dsGeneralLedgerChildren)
        {
            return (from a in dsGeneralLedgerChildren
                    where a.CategoriesActive == 1
                          && a.PostedDate >= fromDate && a.PostedDate <= toDate
                          && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding.StartsWith(accountCorresponding) && !a.TypeID.ToString().StartsWith("70")
                    select new { Amount = a.CreditAmount + a.DebitAmount }).Sum(c => c.Amount);
        }
        public static decimal PS_DU_CtietHD(DateTime fromDate, DateTime toDate, string accountNumbers,
           string accountCorresponding, IEnumerable<GeneralLedgerChild> dsGeneralLedgerChildren)
        {
            return (from a in dsGeneralLedgerChildren
                    where a.CategoriesActive == 0
                          && a.PostedDate >= fromDate && a.PostedDate <= toDate
                          && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding.StartsWith(accountCorresponding) && !a.TypeID.ToString().StartsWith("70")
                    select new { Amount = a.CreditAmount + a.DebitAmount }).Sum(c => c.Amount);

        }
        public static decimal PS_DU_CtietSXKD(DateTime fromDate, DateTime toDate, string accountNumbers,
             string accountCorresponding, IEnumerable<GeneralLedgerChild> dsGeneralLedgerChildren)
        {
            return (from a in dsGeneralLedgerChildren
                    where a.CategoriesActive == 2
                          && a.PostedDate >= fromDate && a.PostedDate <= toDate
                          && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding.StartsWith(accountCorresponding) && !a.TypeID.ToString().StartsWith("70")
                    select new { Amount = a.CreditAmount + a.DebitAmount }).Sum(c => c.Amount);
        }
        public static decimal PS_DU_CtietTaiChinh(DateTime fromDate, DateTime toDate, string accountNumbers,
            string accountCorresponding, IEnumerable<GeneralLedgerChild> dsGeneralLedgerChildren)
        {
            return (from a in dsGeneralLedgerChildren
                    where a.CategoriesActive == 3
                          && a.PostedDate >= fromDate && a.PostedDate <= toDate
                          && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding.StartsWith(accountCorresponding) && !a.TypeID.ToString().StartsWith("70")
                    select new { Amount = a.CreditAmount + a.DebitAmount }).Sum(c => c.Amount);
        }
        public static decimal SD_NO_DK(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate <= fromDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers)
                    select new { ps = a.DebitAmount - a.CreditAmount }).Sum(c => c.ps);
        }
        public static decimal SD_CO_DK(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate <= fromDate && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers)
                    select new { ps = a.CreditAmount - a.DebitAmount }).Sum(c => c.ps);
        }
        public static decimal PS_DU_CtietHD_NT(DateTime fromDate, DateTime toDate, string accountNumbers,
            string accountCorresponding)
        {
            return (from a in LstGeneralLedger
                    where a.PostedDate >= fromDate && a.PostedDate <= toDate
                          && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers) && a.AccountCorresponding.StartsWith(accountCorresponding) && !a.TypeID.ToString().StartsWith("70")
                    select new { Amount = a.CreditAmount + a.DebitAmount }).Sum(c => c.Amount);

        }
        public static decimal PS_DU_CtietDauTu_NT(DateTime fromDate, DateTime toDate, string accountNumbers,
            string accountCorresponding)
        {
            return 0;
        }
        public static decimal PS_DU_CtietSXKD_NT(DateTime fromDate, DateTime toDate, string accountNumbers,
            string accountCorresponding)
        {
            return 0;
        }
        public static decimal PS_DU_CtietTaiChinh_NT(DateTime fromDate, DateTime toDate, string accountNumbers,
            string accountCorresponding)
        {
            return 0;
        }
        public static decimal SD_NO_DK_NT(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            DateTime fromDatePeriodPrevious;
            DateTime toDatePeriodPrevious;
            Utils.GetPeriodPrevious(fromDate, toDate, out fromDatePeriodPrevious, out toDatePeriodPrevious);
            return (from a in LstGeneralLedger
                    where a.PostedDate <= fromDatePeriodPrevious && a.Account != null && a.AccountCorresponding != null
                          && a.Account.StartsWith(accountNumbers)
                    select new { ps = a.DebitAmount - a.CreditAmount }).Sum(c => c.ps);
        }

        public static decimal PS_LUYKE_NO(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return
                (LstGeneralLedger.Where(
                    c =>
                        c.PostedDate >= DateTime.Parse("1/1/" + fromDate.Year) && c.PostedDate <= toDate && 
                        c.Account != null && c.Account.StartsWith(accountNumbers)
                        && !c.TypeID.ToString().StartsWith("70")
                        ).Select(c => c.DebitAmount)).Sum();
        }
        public static decimal PS_LUYKE_CO(DateTime fromDate, DateTime toDate, string accountNumbers)
        {
            return
                (LstGeneralLedger.Where(
                    c =>
                        c.PostedDate >= DateTime.Parse("1/1/" + fromDate.Year) && c.PostedDate <= toDate &&
                        c.Account != null && c.Account.StartsWith(accountNumbers)
                        && !c.TypeID.ToString().StartsWith("70")
                        ).Select(c => c.CreditAmount)).Sum();
        }
    }
}
