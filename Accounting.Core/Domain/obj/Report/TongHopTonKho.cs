﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class TongHopTonKho
    {
        public virtual string RepositoryCode { get; set; }
        public virtual string RepositoryName { get; set; }
        public virtual IList<TongHopTonKhoDetail> TongHopTonKhoDetails { get; set; }
        public virtual decimal SumSLDK { get; set; }
        public virtual decimal SumSLIWGK { get; set; }
        public virtual decimal SumSLOWGK { get; set; }
        public virtual decimal SumSLCK { get; set; }
        public virtual decimal SumDK { get; set; }
        public virtual decimal SumIWGK { get; set; }
        public virtual decimal SumOWGK { get; set; }
        public virtual decimal SumCK { get; set; }
        public TongHopTonKho()
        {
            TongHopTonKhoDetails = new List<TongHopTonKhoDetail>();
        }
    }
    public class TongHopTonKhoDetail
    {
        public virtual string MaterialGoodCode { get; set; }
        public virtual string MaterialGoodName { get; set; }
        public virtual Guid MaterialGoodID { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal SLDK { get; set; }
        public virtual decimal SLIWGK { get; set; }
        public virtual decimal SLOWGK { get; set; }
        public virtual decimal SLCK { get; set; }
        public virtual decimal DK { get; set; }
        public virtual decimal IWGK { get; set; }
        public virtual decimal OWGK { get; set; }
        public virtual decimal CK { get; set; }

        public virtual string RepositoryCode { get; set; }

        public virtual int Ordercode { get; set; }

    }
    public class Detail_Period
    {
        public string Period { get; set; }
    }

}
