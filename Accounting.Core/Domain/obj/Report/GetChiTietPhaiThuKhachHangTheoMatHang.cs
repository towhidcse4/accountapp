﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
        public class GetChiTietPhaiThuKhachHangTheoMatHang
         {
        public Guid AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public Guid SAInvoiceDetail { get; set; }
        public DateTime PostedDateOrder { get; set; }
        public DateTime PostedDate { get; set; }
        public DateTime Date { get; set; }
        public string No { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public string AccountCorresponding { get; set; }
        public string Unit { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal SoTien { get; set; }
        public decimal ChietKhau { get; set; }
        public decimal ThueGTGT { get; set; }
        public decimal TraLaiGiamGia { get; set; }
        public decimal SoDaThu { get; set; }
        public decimal SoConPhaiThu { get; set; }
        public int Type { get; set; }
        public Guid ReferenceID { get; set; }
        public int RefType { get; set; }
    }
    public class GetChiTietPhaiThuKhachHangTheoMatHang_period
    {
        public string Period { get; set; }
    }
}
