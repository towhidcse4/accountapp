﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class B09BlockVI
    {
        public string Name { get; set; }
        public decimal? ThisYear { get; set; }
        public decimal? LastYear { get; set; }
    }

    public class B09SecI
    {
        public string Sec_I_1 { get; set; }
        public string Sec_I_2 { get; set; }
        public string Sec_I_3 { get; set; }
        public string Sec_I_4 { get; set; }
        public string Sec_I_5 { get; set; }
        public string Sec_I_6 { get; set; }
    }

    public class B09SecII
    {
        public string Sec_II_1 { get; set; }
        public string Sec_II_2 { get; set; }
    }

    public class B09SecIII
    {
        public string Sec_III_1 { get; set; }
    }

    public class B09SecIV
    {
        public string Sec_IV_1 { get; set; }
        public string Sec_IV_2 { get; set; }
        public string Sec_IV_3 { get; set; }
        public string Sec_IV_4 { get; set; }
        public string Sec_IV_5 { get; set; }
        public string Sec_IV_6 { get; set; }
        public string Sec_IV_7 { get; set; }
        public string Sec_IV_8 { get; set; }
        public string Sec_IV_9 { get; set; }
        public string Sec_IV_10 { get; set; }
        public string Sec_IV_11 { get; set; }
        public string Sec_IV_12 { get; set; }
    }

    public class B09SecV4
    {
        public string Sec_V_4_1 { get; set; }
        public string Sec_V_4_2 { get; set; }
        public string Sec_V_4_3 { get; set; }
        public string Sec_V_4_4 { get; set; }
    }

    public class B09SecV5
    {
        public string Sec_V_5_1 { get; set; }
        public string Sec_V_5_2 { get; set; }
        public string Sec_V_5_3 { get; set; }
        public string Sec_V_5_4 { get; set; }
        public string Sec_V_5_5 { get; set; }
    }

    public class B09SecV6
    {
        public string Sec_V_6_1 { get; set; }
        public string Sec_V_6_2 { get; set; }
        public string Sec_V_6_3 { get; set; }
    }

    public class B09SecV13
    {
        public string Sec_V_13_1 { get; set; }
    }

    public class B09SecV14
    {
        public string Sec_V_14_1 { get; set; }
        public string Sec_V_14_2 { get; set; }
        public string Sec_V_14_3 { get; set; }
        public string Sec_V_14_4 { get; set; }
        public string Sec_V_14_5 { get; set; }
        public string Sec_V_14_6 { get; set; }
        public string Sec_V_14_7 { get; set; }
        public string Sec_V_14_8 { get; set; }
    }

    public class B09SecV15
    {
        public string Sec_V_15_1 { get; set; }
    }

    public class B09SecV16
    {
        public string Sec_V_16_1 { get; set; }
    }

    public class B09SecVII
    {
        public string Sec_VII_1 { get; set; }
    }

    public class B09SecVIII
    {
        public string Sec_VIII_1 { get; set; }
        public string Sec_VIII_2 { get; set; }
        public string Sec_VIII_3 { get; set; }
        public string Sec_VIII_4 { get; set; }
        public string Sec_VIII_5 { get; set; }
    }
    public class B09Num
    {
        // sector V.1
        public decimal V_1_1_1 { get; set; }
        public decimal V_1_1_2 { get; set; }
        public decimal V_1_1_3 { get; set; }
        public decimal V_1_2_1 { get; set; }
        public decimal V_1_2_2 { get; set; }
        public decimal V_1_2_3 { get; set; }
        public decimal V_1_Sum_1 {
            get {
                return (V_1_1_1 ) + (V_1_1_2 ) + (V_1_1_3 );
            }
        }
        public decimal V_1_Sum_2 {
            get
            {
                return (V_1_2_1 ) + (V_1_2_2 ) + (V_1_2_3 );
            }
        }

        // sector V.2
        public decimal V_2_a_1 { get; set; }
        public decimal V_2_a_1_1 { get; set; }
        public decimal V_2_a_1_2 { get; set; }
        public decimal V_2_a_1_3 { get; set; }
        public decimal V_2_a_2 { get; set; }
        public decimal V_2_a_2_1 { get; set; }
        public decimal V_2_a_2_2 { get; set; }
        public decimal V_2_a_2_3 { get; set; }
        public decimal V_2_b_1 {
            get
            {
                return (V_2_b_1_1 ) + (V_2_b_1_2 );
            }
        }
        public decimal V_2_b_1_1 { get; set; }
        public decimal V_2_b_1_2 { get; set; }
        public decimal V_2_b_2 {
            get {
                return (V_2_b_2_1 ) + (V_2_b_2_2 );
            }
        }
        public decimal V_2_b_2_1 { get; set; }
        public decimal V_2_b_2_2 { get; set; }
        public decimal V_2_c_1 {
            get
            {
                return (V_2_c_1_1 ) + (V_2_c_1_2 );
            }
        }
        public decimal V_2_c_1_1 { get; set; }
        public decimal V_2_c_1_2 { get; set; }
        public decimal V_2_c_2 {
            get
            {
                return (V_2_c_2_1 ) + (V_2_c_2_2 );
            }
        }
        public decimal V_2_c_2_1 { get; set; }
        public decimal V_2_c_2_2 { get; set; }
        public decimal V_2_Sum_1 {
            get
            {
                return (V_2_a_1 ) + (V_2_b_1 ) + (V_2_c_1 );
            }
        }
        public decimal V_2_Sum_2 {
            get
            {
                return (V_2_a_2 ) + (V_2_b_2 ) + (V_2_c_2 );
            }
        }

        // sector V.3
        public decimal V_3_a_1 { get; set; }
        public decimal V_3_a_1_1 { get; set; }
        public decimal V_3_a_2 { get; set; }
        public decimal V_3_a_2_1 { get; set; }

        public decimal V_3_b_1 { get; set; }
        public decimal V_3_b_1_1 { get; set; }
        public decimal V_3_b_2 { get; set; }
        public decimal V_3_b_2_1 { get; set; }

        public decimal V_3_c_1 {
            get
            {
                return (V_3_c_1_1 ) + (V_3_c_1_2 ) + (V_3_c_1_3 ) + (V_3_c_1_4 );
            }
        }
        public decimal V_3_c_1_1 { get; set; }
        public decimal V_3_c_1_2 { get; set; }
        public decimal V_3_c_1_3 { get; set; }
        public decimal V_3_c_1_4 { get; set; }
        public decimal V_3_c_2 {
            get
            {
                return (V_3_c_2_1 ) + (V_3_c_2_2 ) + (V_3_c_2_3 ) + (V_3_c_2_4 );
            }
        }
        public decimal V_3_c_2_1 { get; set; }
        public decimal V_3_c_2_2 { get; set; }
        public decimal V_3_c_2_3 { get; set; }
        public decimal V_3_c_2_4 { get; set; }

        public decimal V_3_d_1 { get; set; }
        public decimal V_3_d_1_1 { get; set; }
        public decimal V_3_d_1_2 { get; set; }
        public decimal V_3_d_1_3 { get; set; }
        public decimal V_3_d_1_4 { get; set; }
        public decimal V_3_d_2 { get; set; }
        public decimal V_3_d_2_1 { get; set; }
        public decimal V_3_d_2_2 { get; set; }
        public decimal V_3_d_2_3 { get; set; }
        public decimal V_3_d_2_4 { get; set; }

        public decimal V_3_e_1 { get; set; }
        public decimal V_3_e_2 { get; set; }
        public decimal V_3_Sum_1 {
            get
            {
                return (V_3_a_1 ) + (V_3_b_1 ) + (V_3_c_1 ) + (V_3_d_1 ) + (V_3_e_1 );
            }
        }
        public decimal V_3_Sum_2 {
            get
            {
                return (V_3_a_2 ) + (V_3_b_2 ) + (V_3_c_2 ) + (V_3_d_2 ) + (V_3_e_2 );
            }
        }

        // sector V.4
        public decimal V_4_1_1 { get; set; }
        public decimal V_4_1_2 { get; set; }
        public decimal V_4_1_3 { get; set; }
        public decimal V_4_1_4 { get; set; }
        public decimal V_4_1_5 { get; set; }
        public decimal V_4_1_6 { get; set; }
        public decimal V_4_1_7 { get; set; }
        public decimal V_4_Sum_1 {
            get
            {
                return (V_4_1_1 ) + (V_4_1_2 ) + (V_4_1_3 ) + (V_4_1_4 ) + (V_4_1_5 ) + (V_4_1_6 ) + (V_4_1_7 );
            }
        }

        public decimal V_4_2_1 { get; set; }
        public decimal V_4_2_2 { get; set; }
        public decimal V_4_2_3 { get; set; }
        public decimal V_4_2_4 { get; set; }
        public decimal V_4_2_5 { get; set; }
        public decimal V_4_2_6 { get; set; }
        public decimal V_4_2_7 { get; set; }
        public decimal V_4_Sum_2 {
            get
            {
                return (V_4_2_1 ) + (V_4_2_2 ) + (V_4_2_3 ) + (V_4_2_4 ) + (V_4_2_5 ) + (V_4_2_6 ) + (V_4_2_7 );
            }
        }

        // Sec V.5
        public decimal V_5_a_1_1 { get; set; }
        public decimal V_5_a_1_2 { get; set; }
        public decimal V_5_a_1_3 {
            get
            {
                return (V_5_a_1_1 ) - (V_5_a_1_2 );
            }
        }
        public decimal V_5_a_2_1 { get; set; }
        public decimal V_5_a_2_2 { get; set; }
        public decimal V_5_a_2_3 {
            get
            {
                return (V_5_a_2_1 ) - (V_5_a_2_2 );
            }
        }
        public decimal V_5_a_3_1 { get; set; }
        public decimal V_5_a_3_2 { get; set; }
        public decimal V_5_a_3_3
        {
            get
            {
                return (V_5_a_3_1 ) - (V_5_a_3_2 );
            }
        }
        public decimal V_5_a_4_1 { get; set; }
        public decimal V_5_a_4_2 { get; set; }
        public decimal V_5_a_4_3
        {
            get
            {
                return (V_5_a_4_1 ) - (V_5_a_4_2 );
            }
        }
        public decimal V_5_b_1_1 { get; set; }
        public decimal V_5_b_1_2 { get; set; }
        public decimal V_5_b_1_3
        {
            get
            {
                return (V_5_b_1_1 ) - (V_5_b_1_2 );
            }
        }
        public decimal V_5_b_2_1 { get; set; }
        public decimal V_5_b_2_2 { get; set; }
        public decimal V_5_b_2_3
        {
            get
            {
                return (V_5_b_2_1 ) - (V_5_b_2_2 );
            }
        }
        public decimal V_5_b_3_1 { get; set; }
        public decimal V_5_b_3_2 { get; set; }
        public decimal V_5_b_3_3
        {
            get
            {
                return (V_5_b_3_1 ) - (V_5_b_3_2 );
            }
        }
        public decimal V_5_b_4_1 { get; set; }
        public decimal V_5_b_4_2 { get; set; }
        public decimal V_5_b_4_3
        {
            get
            {
                return (V_5_b_4_1 ) - (V_5_b_4_2 );
            }
        }
        public decimal V_5_c_1_1 { get; set; }
        public decimal V_5_c_1_2 { get; set; }
        public decimal V_5_c_1_3
        {
            get
            {
                return (V_5_c_1_1 ) - (V_5_c_1_2 );
            }
        }
        public decimal V_5_c_2_1 { get; set; }
        public decimal V_5_c_2_2 { get; set; }
        public decimal V_5_c_2_3
        {
            get
            {
                return (V_5_c_2_1 ) - (V_5_c_2_2 );
            }
        }
        public decimal V_5_c_3_1 { get; set; }
        public decimal V_5_c_3_2 { get; set; }
        public decimal V_5_c_3_3
        {
            get
            {
                return (V_5_c_3_1 ) - (V_5_c_3_2 );
            }
        }
        public decimal V_5_c_4_1 { get; set; }
        public decimal V_5_c_4_2 { get; set; }
        public decimal V_5_c_4_3
        {
            get
            {
                return (V_5_c_4_1 ) - (V_5_c_4_2 );
            }
        }

        // sector V.6
        public decimal V_6_a_1_1 { get; set; }
        public decimal V_6_a_1_2 { get; set; }
        public decimal V_6_a_1_3
        {
            get
            {
                return (V_6_a_1_1 ) - (V_6_a_1_2 );
            }
        }
        public decimal V_6_a_2_1 { get; set; }
        public decimal V_6_a_2_2 { get; set; }
        public decimal V_6_a_2_3
        {
            get
            {
                return (V_6_a_2_1 ) - (V_6_a_2_2 );
            }
        }
        public decimal V_6_a_3_1 { get; set; }
        public decimal V_6_a_3_2 { get; set; }
        public decimal V_6_a_3_3
        {
            get
            {
                return (V_6_a_3_1 ) - (V_6_a_3_2 );
            }
        }
        public decimal V_6_a_4_1 { get; set; }
        public decimal V_6_a_4_2 { get; set; }
        public decimal V_6_a_4_3
        {
            get
            {
                return (V_6_a_4_1 ) - (V_6_a_4_2 );
            }
        }
        public decimal V_6_b_1_1 { get; set; }
        public decimal V_6_b_1_2 { get; set; }
        public decimal V_6_b_1_3 { get; set; }
        public decimal V_6_b_1_4 { get; set; }
        public decimal V_6_b_2_1 { get; set; }
        public decimal V_6_b_2_2 { get; set; }
        public decimal V_6_b_2_3 { get; set; }
        public decimal V_6_b_2_4 { get; set; }
        public decimal V_6_b_3_1 { get; set; }
        public decimal V_6_b_3_2 { get; set; }
        public decimal V_6_b_3_3 { get; set; }
        public decimal V_6_b_3_4 { get; set; }
        public decimal V_6_b_4_1 { get; set; }
        public decimal V_6_b_4_2 { get; set; }
        public decimal V_6_b_4_3 { get; set; }
        public decimal V_6_b_4_4 { get; set; }

        // sector V.7
        public decimal V_7_1_1 { get; set; }
        public decimal V_7_1_2 { get; set; }
        public decimal V_7_1_3 { get; set; }
        public decimal V_7_2_1 { get; set; }
        public decimal V_7_2_2 { get; set; }
        public decimal V_7_2_3 { get; set; }
        public decimal V_7_Sum_1
        {
            get
            {
                return (V_7_1_1 ) + (V_7_1_2 ) + (V_7_1_3 );
            }
        }
        public decimal V_7_Sum_2
        {
            get
            {
                return (V_7_2_1 ) + (V_7_2_2 ) + (V_7_2_3 );
            }
        }

        // sector V.8
        public decimal V_8_1_1 { get; set; }
        public decimal V_8_1_2 { get; set; }
        public decimal V_8_2_1 { get; set; }
        public decimal V_8_2_2 { get; set; }

        // sector V.9
        public decimal V_9_a_1 { get; set; }
        public decimal V_9_a_1_1 { get; set; }
        public decimal V_9_a_2 { get; set; }
        public decimal V_9_a_2_1 { get; set; }
        public decimal V_9_b_1 { get; set; }
        public decimal V_9_b_1_1 { get; set; }
        public decimal V_9_b_2 { get; set; }
        public decimal V_9_b_2_1 { get; set; }
        public decimal V_9_c_1 {
            get
            {
                return (V_9_c_1_1 ) + (V_9_c_1_2 ) + (V_9_c_1_3 );
            }
        }
        public decimal V_9_c_1_1 { get; set; }
        public decimal V_9_c_1_2 { get; set; }
        public decimal V_9_c_1_3
        {
            get
            {
                return (V_9_c_1_3_1 ) + (V_9_c_1_3_2 ) + (V_9_c_1_3_3 );
            }
        }
        public decimal V_9_c_1_3_1 { get; set; }
        public decimal V_9_c_1_3_2 { get; set; }
        public decimal V_9_c_1_3_3 { get; set; }
        public decimal V_9_c_2
        {
            get
            {
                return (V_9_c_2_1 ) + (V_9_c_2_2 ) + (V_9_c_2_3 );
            }
        }
        public decimal V_9_c_2_1 { get; set; }
        public decimal V_9_c_2_2 { get; set; }
        public decimal V_9_c_2_3
        {
            get
            {
                return (V_9_c_2_3_1 ) + (V_9_c_2_3_2 ) + (V_9_c_2_3_3 );
            }
        }
        public decimal V_9_c_2_3_1 { get; set; }
        public decimal V_9_c_2_3_2 { get; set; }
        public decimal V_9_c_2_3_3 { get; set; }
        public decimal V_9_d_1 { get; set; }
        public decimal V_9_d_2 { get; set; }

        // sector V.10
        public decimal V_10_1_1 { get; set; }
        public decimal V_10_1_2 { get; set; }
        public decimal V_10_1_3 { get; set; }
        public decimal V_10_1_4 { get; set; }
        public decimal V_10_1_5 { get; set; }
        public decimal V_10_1_6 { get; set; }
        public decimal V_10_1_7 { get; set; }
        public decimal V_10_1_8 { get; set; }
        public decimal V_10_1_9 { get; set; }
        public decimal V_10_2_1 { get; set; }
        public decimal V_10_2_2 { get; set; }
        public decimal V_10_2_3 { get; set; }
        public decimal V_10_2_4 { get; set; }
        public decimal V_10_2_5 { get; set; }
        public decimal V_10_2_6 { get; set; }
        public decimal V_10_2_7 { get; set; }
        public decimal V_10_2_8 { get; set; }
        public decimal V_10_2_9 { get; set; }
        public decimal V_10_Sum_1 {
            get
            {
                return (V_10_1_1 ) + (V_10_1_2 ) + (V_10_1_3 ) + (V_10_1_4 ) + (V_10_1_5 ) + (V_10_1_6 ) + (V_10_1_7 ) + (V_10_1_8 ) + (V_10_1_9 );
            }
        }
        public decimal V_10_Sum_2
        {
            get
            {
                return (V_10_2_1 ) + (V_10_2_2 ) + (V_10_2_3 ) + (V_10_2_4 ) + (V_10_2_5 ) + (V_10_2_6 ) + (V_10_2_7 ) + (V_10_2_8 ) + (V_10_2_9 );
            }
        }

        // sector V.11
        public decimal V_11_a_1 { get; set; }
        public decimal V_11_a_1_1 { get; set; }
        public decimal V_11_a_2 { get; set; }
        public decimal V_11_a_2_1 { get; set; }
        public decimal V_11_a_3 { get; set; }
        public decimal V_11_a_3_1 { get; set; }
        public decimal V_11_a_4 { get; set; }
        public decimal V_11_a_4_1 { get; set; }
        public decimal V_11_b_1 { get; set; }
        public decimal V_11_b_1_1 { get; set; }
        public decimal V_11_b_2 { get; set; }
        public decimal V_11_b_2_1 { get; set; }
        public decimal V_11_b_3 { get; set; }
        public decimal V_11_b_3_1 { get; set; }
        public decimal V_11_b_4 { get; set; }
        public decimal V_11_b_4_1 { get; set; }
        public decimal V_11_c_1 { get; set; }
        public decimal V_11_c_1_1 { get; set; }
        public decimal V_11_c_2 { get; set; }
        public decimal V_11_c_2_1 { get; set; }
        public decimal V_11_c_3 { get; set; }
        public decimal V_11_c_3_1 { get; set; }
        public decimal V_11_c_4 { get; set; }
        public decimal V_11_c_4_1 { get; set; }
        public decimal V_11_Sum_1 {
            get
            {
                return V_11_a_1 + V_11_b_1 + V_11_c_1;
            }
        }
        public decimal V_11_Sum_2
        {
            get
            {
                return V_11_a_2 + V_11_b_2 + V_11_c_2;
            }
        }
        public decimal V_11_Sum_3
        {
            get
            {
                return V_11_a_3 + V_11_b_3 + V_11_c_3;
            }
        }
        public decimal V_11_Sum_4
        {
            get
            {
                return V_11_a_4 + V_11_b_4 + V_11_c_4;
            }
        }

        // sector V.12
        public decimal V_12_1_1 { get; set; }
        public decimal V_12_1_2 { get; set; }
        public decimal V_12_1_3 { get; set; }
        public decimal V_12_Sum_1
        {
            get
            {
                return V_12_1_1 + V_12_1_2 + V_12_1_3;
            }
        }
        public decimal V_12_2_1 { get; set; }
        public decimal V_12_2_2 { get; set; }
        public decimal V_12_2_3 { get; set; }
        public decimal V_12_Sum_2
        {
            get
            {
                return V_12_2_1 + V_12_2_2 + V_12_2_3;
            }
        }

        // sector V.13
        public decimal V_13_1_1 { get; set; }
        public decimal V_13_1_2 { get; set; }
        public decimal V_13_1_3 { get; set; }
        public decimal V_13_1_4 { get; set; }
        public decimal V_13_2_1 { get; set; }
        public decimal V_13_2_2 { get; set; }
        public decimal V_13_2_3 { get; set; }
        public decimal V_13_2_4 { get; set; }
        public decimal V_13_3_1 { get; set; }
        public decimal V_13_3_2 { get; set; }
        public decimal V_13_3_3 { get; set; }
        public decimal V_13_3_4 { get; set; }
        public decimal V_13_4_1 { get; set; }
        public decimal V_13_4_2 { get; set; }
        public decimal V_13_4_3 { get; set; }
        public decimal V_13_4_4 { get; set; }
        public decimal V_13_5_1 { get; set; }
        public decimal V_13_5_2 { get; set; }
        public decimal V_13_5_3 { get; set; }
        public decimal V_13_5_4 { get; set; }
        public decimal V_13_6_1 { get; set; }
        public decimal V_13_6_2 { get; set; }
        public decimal V_13_6_3 { get; set; }
        public decimal V_13_6_4 { get; set; }
        public decimal V_13_7_1 {
            get
            {
                return V_13_1_1 + V_13_2_1 + V_13_3_1 + V_13_4_1 + V_13_5_1 + V_13_6_1;
            }
        }
        public decimal V_13_7_2
        {
            get
            {
                return V_13_1_2 + V_13_2_2 + V_13_3_2 + V_13_4_2 + V_13_5_2 + V_13_6_2;
            }
        }
        public decimal V_13_7_3
        {
            get
            {
                return V_13_1_3 + V_13_2_3 + V_13_3_3 + V_13_4_3 + V_13_5_3 + V_13_6_3;
            }
        }
        public decimal V_13_7_4
        {
            get
            {
                return V_13_1_4 + V_13_2_4 + V_13_3_4 + V_13_4_4 + V_13_5_4 + V_13_6_4;
            }
        }

        // VI.1
        public decimal VI_1_a_1_1 { get; set; }
        public decimal VI_1_a_1_2 { get; set; }
        public decimal VI_1_a_1_3 { get; set; }
        public decimal VI_1_a_1_4 { get; set; }
        public decimal VI_1_a_Sum_1 {
            get
            {
                return VI_1_a_1_1 + VI_1_a_1_2 + VI_1_a_1_3 + VI_1_a_1_4;
            }
        }
        public decimal VI_1_a_2_1 { get; set; }
        public decimal VI_1_a_2_2 { get; set; }
        public decimal VI_1_a_2_3 { get; set; }
        public decimal VI_1_a_2_4 { get; set; }
        public decimal VI_1_a_Sum_2 {
            get
            {
                return VI_1_a_2_1 + VI_1_a_2_2 + VI_1_a_2_3 + VI_1_a_2_4;
            }
        }
        public decimal VI_1_b_1 { get; set; }
        public decimal VI_1_b_2 { get; set; }
        public decimal VI_1_c_1 { get; set; }
        public decimal VI_1_c_2 { get; set; }

        // VI.2
        public decimal VI_2_1_1 { get; set; }
        public decimal VI_2_1_2 { get; set; }
        public decimal VI_2_1_3 { get; set; }
        public decimal VI_2_Sum_1 {
            get
            {
                return VI_2_1_1 + VI_2_1_2 + VI_2_1_3;
            }
        }
        public decimal VI_2_2_1 { get; set; }
        public decimal VI_2_2_2 { get; set; }
        public decimal VI_2_2_3 { get; set; }
        public decimal VI_2_Sum_2 {
            get
            {
                return VI_2_2_1 + VI_2_2_2 + VI_2_2_3;
            }
        }

        // VI.3
        public decimal VI_3_1_1 { get; set; }
        public decimal VI_3_1_2 { get; set; }
        public decimal VI_3_1_3 { get; set; }
        public decimal VI_3_1_4 { get; set; }
        public decimal VI_3_1_5 { get; set; }
        public decimal VI_3_1_6 { get; set; }
        public decimal VI_3_Sum_1 {
            get
            {
                return VI_3_1_1 + VI_3_1_2 + VI_3_1_3 + VI_3_1_4 + VI_3_1_5 + VI_3_1_6;
            }
        }
        public decimal VI_3_2_1 { get; set; }
        public decimal VI_3_2_2 { get; set; }
        public decimal VI_3_2_3 { get; set; }
        public decimal VI_3_2_4 { get; set; }
        public decimal VI_3_2_5 { get; set; }
        public decimal VI_3_2_6 { get; set; }
        public decimal VI_3_Sum_2
        {
            get
            {
                return VI_3_2_1 + VI_3_2_2 + VI_3_2_3 + VI_3_2_4 + VI_3_2_5 + VI_3_2_6;
            }
        }

        // VI.4
        public decimal VI_4_1_1 { get; set; }
        public decimal VI_4_1_2 { get; set; }
        public decimal VI_4_1_3 { get; set; }
        public decimal VI_4_1_4 { get; set; }
        public decimal VI_4_1_5 { get; set; }
        public decimal VI_4_1_6 { get; set; }
        public decimal VI_4_Sum_1
        {
            get
            {
                return VI_4_1_1 + VI_4_1_2 + VI_4_1_3 + VI_4_1_4 + VI_4_1_5 + VI_4_1_6;
            }
        }
        public decimal VI_4_2_1 { get; set; }
        public decimal VI_4_2_2 { get; set; }
        public decimal VI_4_2_3 { get; set; }
        public decimal VI_4_2_4 { get; set; }
        public decimal VI_4_2_5 { get; set; }
        public decimal VI_4_2_6 { get; set; }
        public decimal VI_4_Sum_2
        {
            get
            {
                return VI_4_2_1 + VI_4_2_2 + VI_4_2_3 + VI_4_2_4 + VI_4_2_5 + VI_4_2_6;
            }
        }

        // VI.5
        public decimal VI_5_1_1 { get; set; }
        public decimal VI_5_1_2 { get; set; }
        public decimal VI_5_1_3 { get; set; }
        public decimal VI_5_1_4 { get; set; }
        public decimal VI_5_1_5 { get; set; }
        public decimal VI_5_1_6 { get; set; }
        public decimal VI_5_1_7 { get; set; }
        public decimal VI_5_2_1 { get; set; }
        public decimal VI_5_2_2 { get; set; }
        public decimal VI_5_2_3 { get; set; }
        public decimal VI_5_2_4 { get; set; }
        public decimal VI_5_2_5 { get; set; }
        public decimal VI_5_2_6 { get; set; }
        public decimal VI_5_2_7 { get; set; }

        // VI.6
        public decimal VI_6_a_1 { get; set; }
        public decimal VI_6_a_2 { get; set; }
        public decimal VI_6_b_1 { get; set; }
        public decimal VI_6_b_2 { get; set; }
        public decimal VI_6_c_1 { get; set; }
        public decimal VI_6_c_1_1 { get; set; }
        public decimal VI_6_c_1_2 { get; set; }
        public decimal VI_6_c_2 { get; set; }
        public decimal VI_6_c_2_1 { get; set; }
        public decimal VI_6_c_2_2 { get; set; }

        // VI.7
        public decimal VI_7_1_1 { get; set; }
        public decimal VI_7_1_2 { get; set; }
        public decimal VI_7_1_3 { get; set; }
        public decimal VI_7_1_4 { get; set; }
        public decimal VI_7_1_5 { get; set; }
        public decimal VI_7_Sum_1
        {
            get
            {
                return VI_7_1_1 + VI_7_1_2 + VI_7_1_3 + VI_7_1_4 + VI_7_1_5;
            }
        }
        public decimal VI_7_2_1 { get; set; }
        public decimal VI_7_2_2 { get; set; }
        public decimal VI_7_2_3 { get; set; }
        public decimal VI_7_2_4 { get; set; }
        public decimal VI_7_2_5 { get; set; }
        public decimal VI_7_Sum_2
        {
            get
            {
                return VI_7_2_1 + VI_7_2_2 + VI_7_2_3 + VI_7_2_4 + VI_7_2_5;
            }
        }

        // VI.8
        public decimal VI_8_1_1 { get; set; }
        public decimal VI_8_1_2 { get; set; }
        public decimal VI_8_1_3 { get; set; }
        public decimal VI_8_1_4 { get; set; }
        public decimal VI_8_Sum_1
        {
            get
            {
                return VI_8_1_1 + VI_8_1_2 + VI_8_1_3 + VI_8_1_4;
            }
        }
        public decimal VI_8_2_1 { get; set; }
        public decimal VI_8_2_2 { get; set; }
        public decimal VI_8_2_3 { get; set; }
        public decimal VI_8_2_4 { get; set; }
        public decimal VI_8_Sum_2
        {
            get
            {
                return VI_8_2_1 + VI_8_2_2 + VI_8_2_3 + VI_8_2_4;
            }
        }

        // VI.9
        public decimal VI_9_1_1 { get; set; }
        public decimal VI_9_1_2 { get; set; }
        public decimal VI_9_1_3 { get; set; }
        public decimal VI_9_2_1 { get; set; }
        public decimal VI_9_2_2 { get; set; }
        public decimal VI_9_2_3 { get; set; }
    }

    public class B09Sector
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class B09DNN
    {
        public Guid ID { get; set; }
        public B09Num b09Num { get; set; }
        
        public B09SecI b09SecI { get; set; }
        public B09SecII b09SecII { get; set; }
        public B09SecIII b09SecIII { get; set; }
        public B09SecIV b09SecIV { get; set; }
        public B09SecV4 b09SecV4 { get; set; }
        public B09SecV5 b09SecV5 { get; set; }
        public B09SecV6 b09SecV6 { get; set; }
        public B09SecV13 b09SecV13 { get; set; }
        public B09SecV14 b09SecV14 { get; set; }
        public B09SecV15 b09SecV15 { get; set; }
        public B09SecV16 b09SecV16 { get; set; }
        public B09SecVII b09SecVII { get; set; }
        public B09SecVIII b09SecVIII { get; set; }

        public List<B09Num> B09Number { get; set; }
        public List<B09Sector> B09SectorI { get; set; }
        public List<B09Sector> B09SectorII { get; set; }
        public List<B09Sector> B09SectorIII { get; set; }
        public List<B09Sector> B09SectorIV { get; set; }
        public List<B09Sector> B09SectorV4 { get; set; }
        public List<B09Sector> B09SectorV5 { get; set; }
        public List<B09Sector> B09SectorV6 { get; set; }
        public List<B09Sector> B09SectorV13 { get; set; }
        public List<B09Sector> B09SectorV14 { get; set; }
        public List<B09Sector> B09SectorV15 { get; set; }
        public List<B09Sector> B09SectorV16 { get; set; }
        public List<B09Sector> B09SectorVII { get; set; }
        public List<B09Sector> B09SectorVIII { get; set; }
        public string Period { get; set; }
        public B09DNN()
        {
            ID = new Guid();
            b09Num = new B09Num();
            b09SecI = new B09SecI();
            b09SecII = new B09SecII();
            b09SecIII = new B09SecIII();
            b09SecIV = new B09SecIV();
            b09SecV4 = new B09SecV4();
            b09SecV5 = new B09SecV5();
            b09SecV6 = new B09SecV6();
            b09SecV13 = new B09SecV13();
            b09SecV14 = new B09SecV14();
            b09SecV15 = new B09SecV15();
            b09SecV16 = new B09SecV16();
            b09SecVII = new B09SecVII();
            b09SecVIII = new B09SecVIII();

            B09Number = new List<B09Num>();
            B09SectorI = new List<B09Sector>();
            B09SectorII = new List<B09Sector>();
            B09SectorIII = new List<B09Sector>();
            B09SectorIV = new List<B09Sector>();
            B09SectorV4 = new List<B09Sector>();
            B09SectorV5 = new List<B09Sector>();
            B09SectorV6 = new List<B09Sector>();
            B09SectorV13 = new List<B09Sector>();
            B09SectorV14 = new List<B09Sector>();
            B09SectorV15 = new List<B09Sector>();
            B09SectorV16 = new List<B09Sector>();
            B09SectorVII = new List<B09Sector>();
            B09SectorVIII = new List<B09Sector>();
        }
    }
}
