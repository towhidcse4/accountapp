﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S03A2DNN
    {
        public DateTime PostedDate { get; set; }    //Ngày Tháng Ghi Sổ (PostedDate)
        public string RefNo { get; set; }           //Số chứng từ (No)
        public DateTime RefDate { get; set; }       //Ngày Tháng Chứng từ (Date)
        public string Description { get; set; }     //Diễn Giải (Description)
        public string Reason { get; set; }     //Diễn Giải khi click vào but toán cộng gộp
        public string ListAccount { get; set; }     //Loại Tài Khoản        ListAccount = "acc1,acc2,acc3,acc4,acc5";
        public string AccountCorresponding { get; set; }
        public string Hyperlink { get; set; }
        public decimal CreaditAmount { get; set; }   //Ghi Nợ Tài KHoản
        public decimal CAmount1 { get; set; }      //
        public decimal CAmount2 { get; set; }      //
        public decimal CAmount3 { get; set; }      //
        public decimal CAmount4 { get; set; }      //
        public decimal CAmount5 { get; set; }      //
        public decimal CAmount6 { get; set; }      //Siền Debit của trường hợp không hiển thị được
        public string AccountNumber6 { get; set; }              //Số hiệu là số tiền của tài khoản đối ứng tương ứng như tài khoản trên
        public S03A2DNN() { }
        public S03A2DNN(int i)
        {
            ListAccount = "111,1111,1112,1234,1235,126";
            PostedDate = DateTime.Now.Date.AddDays(i);
            RefDate = PostedDate.AddDays(-i % 3);
            RefNo = "Ref no " + i;
            //Hyperlink
            Description = "Description " + i;
            CreaditAmount = 10230 * (i % 25 + 1);
            CAmount1 = 0;
            CAmount2 = 0;
            CAmount3 = 0;
            CAmount4 = 0;
            CAmount5 = 0;
            switch (i % 6)
            {
                case 1: CAmount1 = CreaditAmount; break;
                case 2: CAmount2 = CreaditAmount; break;
                case 3: CAmount3 = CreaditAmount; break;
                case 4: CAmount4 = CreaditAmount; break;
                case 5: CAmount5 = CreaditAmount; break;
                case 0: CAmount6 = CreaditAmount; break;
            }
            AccountNumber6 = "" + i;
            Hyperlink = "" + i;
        }
    }

    public class S03A2DNNDetail
    {
        public string Period { get; set; }
    }
}
