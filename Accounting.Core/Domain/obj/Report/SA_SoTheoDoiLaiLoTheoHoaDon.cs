﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_SoTheoDoiLaiLoTheoHoaDon
    {
        public Guid SAInvoiceID { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string AccountingObjectName { get; set; }
        public string Reason { get; set; }
        public decimal? GiaTriHHDV { get; set; }
        public decimal? ChietKhau { get; set; }
        public decimal? GiamGia { get; set; }
        public decimal? TraLai { get; set; }
        public decimal? GiaVon { get; set; }
        public decimal? LaiLo { get; set; }
    }

    public class SA_SoTheoDoiLaiLoTheoHoaDon_period
    {
        public string Period { get; set; }
    }
}
