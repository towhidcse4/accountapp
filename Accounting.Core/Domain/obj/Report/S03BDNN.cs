﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S03BDNN
    {
        public string Account { get; set; }         //Tài khoản
        public string AccountName { get; set; }     //Tên Tài Khoản
        public decimal OpeningDebit { get; set; }   //Số dư đầu kỳ
        public decimal OpeningCredit { get; set; }  //Số dư đầu kỳ

        public decimal CumulateDebit { get; set; }  // Cộng lũy kế từ đầu năm Nợ
        public decimal CumulateCredit { get; set; } // Cộng lũy kế từ đầu năm Có

        public DateTime PostedDate { get; set; }    //Ngày tháng ghi sổ
        public string No { get; set; }              //Số hiệu chứng từ
        public string HyperLink { get; set; }
        public DateTime Date { get; set; }          //Ngày Tháng chứng từ
        public string Description { get; set; }     //Diễn Giải
        public string Reason { get; set; }          //Diễn giải khi cộng gộp
        public string PageNo { get; set; }          //Trang sổ nhật ký chung
        public string LineNo { get; set; }          //STT dòng
        public string AccountCorresponding { get; set; } //Tài Khoản đối ứng
        public decimal DebitAmount { get; set; }    //Nợ số tiền
        public decimal CreditAmount { get; set; }   //Có số tiền
        public int OrderType { get; set; }
        public S03BDNN() { }
        public S03BDNN(int i)
        {
            Account = "10" + i / 35;
            OpeningDebit = 201000 + 20000 * i;
            OpeningCredit = 12000 * i + 500000;
            PostedDate = DateTime.Now.AddDays(i);
            No = "No " + i;
            HyperLink = "Hyper" + i;
            Date = PostedDate.AddDays(66);
            Description = "Description" + i;
            PageNo = "PNo" + i;
            LineNo = "Lineno" + i;
            AccountCorresponding = "ACC" + i;
            DebitAmount = 10200 + i;
            CreditAmount = 30000 - i;
            CumulateCredit = 30000000 + i;
            CumulateDebit = 5000000 + i;
        }
    }
    public class S03BDNNDetail
    {
        public string Period { get; set; }
    }
}
