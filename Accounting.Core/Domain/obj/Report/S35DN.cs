﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{   // Sổ chi tiết bán hàng
    public class S17DNN
    {
        public Guid SAInvoiceID { get; set; }
        public Guid MaterialGoodsID { get; set; }
        public string MaterialGoodsName { get; set; }   //tên sản phẩm
        public DateTime PostedDate { get; set; }        //ngày ghi sổ
        public DateTime Date { get; set; }           //ngày chứng từ
        public string RefNo { get; set; }               //số hiệu chứng từ không có chứng từ gốc thì lấy No (số chứng từ)
        public string Hyperlink { get; set; }           // =  reftype;refID
        public string Description { get; set; }         //diễn giải
        public string CreditAccount { get; set; }        //TK đối ứng
        public decimal Quantity { get; set; }           //số lượng
        public decimal UnitPrice { get; set; }          //đơn giá
        public decimal Amount { get; set; }             //thành tiền
        public decimal VATRate { get; set; }            //giảm trừ thuế
        public decimal DiscountAmount { get; set; }     //giảm trừ khác
        public decimal OutwardAmount { get; set; }      //giá vốn
        public S17DNN()
        {
            MaterialGoodsName = "Tên sản phẩm";
            PostedDate = DateTime.Now;
            Date = DateTime.Now;
            RefNo = "RefNo";
            Hyperlink = "Hyperlink";
            Description = "Description";
            CreditAccount = "CreditAccount";
            Quantity = 10000;
            UnitPrice = 10000;
            Amount = 10000;
            VATRate = 1000;
            DiscountAmount = 2000;
            OutwardAmount = 3000;
        }
    }
    public class S17DNNDetail
    {
        public string Period { get; set; }
        public S17DNNDetail() {
            Period = "S35DN";
        }
    }
}
