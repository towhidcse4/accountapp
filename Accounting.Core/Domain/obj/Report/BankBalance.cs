﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BankBalance
    {
        public string AccountNumber { get; set; }   // Số tk
        public string Currency { get; set; }        //Loại tiền
        public string BankAccount { get; set; }     // Tài khoản ngân hàng
        public string BankName { get; set; }        // Tên Ngân Hàng
        public decimal OpenDebit { get; set; }      // Nợ Đầu Kỳ
        public decimal OpenCredit { get; set; }     // Có Đầu Kỳ
        public decimal CrementDebit { get; set; }   // Phát Sinh Nợ
        public decimal CrementCredit { get; set; }    // Phát Sinh Có
        public decimal CloseDebit { get; set; }     // Cuối Kỳ Nợ
        public decimal CloseCredit { get; set; }    // Cuối Kỳ Có
        public BankBalance() { }
        public BankBalance(int i)
        {
            AccountNumber = "AccNum " + i / 35;
            BankAccount = "Acc" + i / 35;
            BankName = "BankName " + i;
            Currency = "CCY";
            OpenDebit = i + 100000;
            OpenCredit = i * 20 + 500000;
            CrementDebit = 10 * i + 3000000;
            CrementCredit = 30000 * i + 10;
            CloseDebit = 1020 + i % 30 * 1000;
            CloseCredit = 3210 * i;


            //   AccountNumber = "";
        }
    }
    public class BankBalanceDetail
    {
        public string Period { get; set; }
    }
}
