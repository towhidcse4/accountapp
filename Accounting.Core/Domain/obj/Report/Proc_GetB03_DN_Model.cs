﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class Proc_GetB03_DN_Model
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public decimal? PrevAmount { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
    }
    public class Proc_GetB03_DN_Model_Detail
    {
        public string Period { get; set; }
        public string thamso { get; set; }
    }
}
