﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class TongHopCongNoNhanVien
    {
        public Guid AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public string TK { get; set; }
        public decimal? NoDK { get; set; }
        public decimal? CoDK { get; set; }
        public decimal? NoPS { get; set; }
        public decimal? CoPS { get; set; }
        public decimal? NoCK { get; set; }
        public decimal? CoCK { get; set; }
    }
    public class TongHopCongNoNhanVien_period
    {
        public string Period { get; set; }
    }
}
