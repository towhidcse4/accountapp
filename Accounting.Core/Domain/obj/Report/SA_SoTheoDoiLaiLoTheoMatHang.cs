﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_SoTheoDoiLaiLoTheoMatHang
    {
        public Guid MaterialGoodsID { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string MaterialGoodsName { get; set; }
        public string Unit { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? DoanhSoBan { get; set; }
        public decimal? ChietKhau { get; set; }
        public decimal? GiamGia { get; set; }
        public decimal? TraLai { get; set; }
        public decimal? GiaVon { get; set; }
        public decimal? LaiLo { get; set; }
    }

    public class SA_SoTheoDoiLaiLoTheoMatHang_period
    {
        public string Period { get; set; }
    }
}
