﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GLR_GetS03a1_DN
    {
        public DateTime? PostedDate { get; set; }
        public string RefNo { get; set; }
        public DateTime? RefDate { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
        public string Description { get; set; }
        public string AccountNumber { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Col2 { get; set; }
        public decimal? Col3 { get; set; }
        public decimal? Col4 { get; set; }
        public decimal? Col5 { get; set; }
        public decimal? Col6 { get; set; }
        public decimal? Col7 { get; set; }
        public string ColOtherAccount { get; set; }
        public string AccountNumberList { get; set; }
        public int SortOrder { get; set; }
        public decimal? DetailPostOrder { get; set; }

    }
    public class GLR_GetS03a1_DNDetail
    {
        public string Period { get; set; }
        public string TenTK { get; set; }
    }
}
