﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_GetReceivableSummaryByGroup
    {
        public string AccountingObjectCode { get; set; }		//Mã khách hàng
        public string AccountingObjectName { get; set; }		//Tên khách hàng
        public Guid AccountingObjectID { get; set; }		//ID khách hàng
        public string AccountNumber { get; set; }		//TK công nợ
        public decimal OpenDebitAmount { get; set; }		//Dư nợ đầu kỳ
        public decimal OpenCreditAmount { get; set; }		//Dư có đầu kỳ
        public decimal DebitAmount { get; set; }		//Phát sinh nợ
        public decimal CreditAmount { get; set; }		//Phát sinh có
        public decimal CloseDebitAmount { get; set; }		//Dư nợ cuối kỳ
        public decimal CloseCreditAmount { get; set; }		//Dư có cuối kỳ
        //add by cuongpv
        public decimal OpenDebitAmountOC { get; set; }		//Dư nợ đầu kỳ
        public decimal OpenCreditAmountOC { get; set; }		//Dư có đầu kỳ
        public decimal DebitAmountOC { get; set; }		//Phát sinh nợ
        public decimal CreditAmountOC { get; set; }		//Phát sinh có
        public decimal CloseDebitAmountOC { get; set; }		//Dư nợ cuối kỳ
        public decimal CloseCreditAmountOC { get; set; }		//Dư có cuối kỳ
        //end add by cuongpv
        public string AccountObjectGroupList { get; set; } //ID nhóm khách hàng
        public string AccountingObjectGroupCode { get; set; }
        public string AccountingObjectGroupName { get; set; } 
        public int Ordercode { get; set; }
    }
    public class SA_GetReceivableSummaryByGroup_period
    {
        public string Period { get; set; }
    }
}
