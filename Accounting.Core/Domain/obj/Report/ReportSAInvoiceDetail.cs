﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public abstract class ReportSaInvoiceDetail
    {
        public DateTime Date { get; set; }//ngày chứng từ
        public DateTime PostedDate { get; set; } //ngày ghi sổ
        public string NumberAttach { get; set; }//số hiệu chứng từ không có chứng từ gốc thì lấy No (số chứng từ)
        public string Description { get; set; } //diễn giải
        public string CreditAccount { get; set; } //TK Có 
        public decimal Quantity { get; set; }//số lượng
        public decimal UnitPrice { get; set; } //đơn giá
        public decimal Amount { get; set; }//thành tiền
        public decimal SubtractionTax { get; set; }//giảm trừ thuế
        public decimal SubtractionOther { get; set; }//giảm trừ khác
    }

    public class ReportAmount
    {
        public decimal PlusAmountArise { get; set; }//cống số phát sinh
        public decimal Revenue { get; set; }//doanh thu thuần
        public decimal OWPrice { get; set; }//giá vốn hàng bán
        public decimal Interest { get; set; }//lãi gộp

    }
}
