﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class F01DNN
    {
        public int AccountLevel { get; set; }
        public string AccountCategory { get; set; }         // nhóm (A.Tk trong bảng, B.Tk ngoài bảng)
        public string AccountNumber { get; set; }           //so tai khoan
        public string Hyperlink { get; set; }               //
        public string AccountName { get; set; }
        public decimal OpeningDebit { get; set; }           //nợ đầu kỳ
        public decimal OpeningCredit { get; set; }          //có đầu kỳ
        public decimal MovementDebit { get; set; }          //phát sinh nợ
        public decimal MovementCredit { get; set; }         //phát sinh có
        public decimal AccumDebit { get; set; }             //lũy kế phát sinh nợ
        public decimal AccumCredit { get; set; }            //lũy kế phát sinh có
        public decimal ClosingDebit { get; set; }           //nợ cuối kỳ
        public decimal ClosingCredit { get; set; }          //có cuối kỳ
        public F01DNN() { }
        public F01DNN(int i)
        {
            AccountLevel = 1 + (i % 15);
            AccountCategory = "Nhóm tk " + i / 30;
            AccountNumber = "" + i + "-" + Convert.ToByte(i % 30);
            AccountName = "Tài khoản " + i;
            OpeningDebit = i;
            OpeningCredit = i;
            MovementDebit = i;
            MovementCredit = i;
            ClosingDebit = i;
            ClosingCredit = i;
        }
    }
    public class F01DNNDetail
    {
        public string Period { get; set; }
    }
}
