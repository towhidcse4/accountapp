﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    // Sổ chi tiết mua hàng theo NCC và mặt hàng
    public class PurchaseDetailSupplerInventoryItem
    {
        public string AccountingObjectGroupCode { get; set; }  // Mã nhóm
        public string AccountingObjectGroupName { get; set; }   // Tên nhóm NCC
        public string AccountingObjectCode { get; set; }        // Mã NCC
        public string AccountingObjectName { get; set; }        // Tên NCC
        public string MaterialGoodsCode { get; set; }           //mã hàng
        public string MaterialGoodsName { get; set; }           //tên hàng

        public DateTime PostedDate { get; set; }                //
        public string NumberAttach { get; set; }                //hoặc No
        public string TypeName { get; set; }                    //Loại
        public string Description { get; set; }                 //diễn giải
        public decimal UnitPrice { get; set; }                  //đơn giá
        public decimal Quantity { get; set; }                   //số lượng
        public decimal Amount { get; set; }                     //thành tiền
        public decimal DiscountAmount { get; set; }             //chiết khấu
        public decimal DiscountTotalAmount { get; set; }       //tổng tiền giảm giá
        public decimal ReturnQuantity { get; set; }             //số lượng hàng trả lại
        public decimal ReturnTotalAmount { get; set; }          //tổng tiền trả lại

        public int TypeID { get; set; }                         //typeid tương ứng
        public Guid ReferenceID { get; set; }

        public string Hyperlink { get; set; }                   // Hyperlink
        public void AddHyperLink()
        {
            Hyperlink = TypeID + ";" + ReferenceID;
        }
        public PurchaseDetailSupplerInventoryItem()
        {
            AccountingObjectGroupCode = "";
            AccountingObjectGroupName = "";
            AccountingObjectCode = "";
            AccountingObjectName = "";
            MaterialGoodsCode = "";
            MaterialGoodsName = "";
        }
        public PurchaseDetailSupplerInventoryItem(int i)
        {
            AccountingObjectGroupCode = "Mã Nhóm NCC " + i / 50;
            AccountingObjectGroupName = "Tên Nhóm NCC" + i / 50;
            AccountingObjectCode = "Mã NCC  " + i / 35;
            AccountingObjectName = "Tên NCC " + i / 35;
            MaterialGoodsCode = "Mã hàng " + i / 20;
            MaterialGoodsName = "Tên hàng " + i / 20;

            PostedDate = DateTime.Now.Date.AddDays(i);
            NumberAttach = "";
            TypeName = "";
            Description = "";
            UnitPrice = i * 1200;
            Quantity = i % 3 * 100;
            Amount = i % 2 * 2000;
            DiscountAmount = i % 2 * 20;
            DiscountTotalAmount = DiscountAmount * Amount / 100;
            ReturnQuantity = i % 3 * 2;
            ReturnTotalAmount = 0;


        }
    }
    public class PurchaseDetailSupplerInventoryItemDetail
    {
        public string Period { get; set; }

        public PurchaseDetailSupplerInventoryItemDetail()
        {
            Period = "PurchaseDetailSupplerInventoryItemDetail";
        }
    }
}


