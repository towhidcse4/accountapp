﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_SoTheoDoiLaiLoTheoHopDongBan
    {
        public Guid ContractID { get; set; }
        public int RefType { get; set; }
        public DateTime? NgayKy { get; set; }
        public string SoHopDong { get; set; }
        public string DoiTuong { get; set; }
        public string TrichYeu { get; set; }
        public decimal? GiaTriHopDong { get; set; }
        public decimal? DoanhThuThucTe { get; set; }
        public decimal? GiamTruDoanhThu { get; set; }
        public decimal? GiaVon { get; set; }
        public decimal? LaiLo { get; set; }
    }

    public class SA_SoTheoDoiLaiLoTheoHopDongBan_period
    {
        public string Period { get; set; }
    }
}
