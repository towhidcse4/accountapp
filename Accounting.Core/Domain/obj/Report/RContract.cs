﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{/*Tình hình thực hiện hợp đồng mua/ bán*/
    public class RContract
    {
        // Bán              | Mua
        public Guid? ContractID { get; set; }//add by cuongpv
        public string Code { get; set; }        // Số hợp đồng 
        public DateTime SignedDate { get; set; }         // Ngày ký   
        public string SignedDates
        {
            get { return (SignedDate != null) ? SignedDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""; }
        }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public decimal Amount { get; set; }             //  Amount
        public decimal DiscountAmount { get; set; }           //Discount   
        public decimal VATAmount { get; set; }          // VAT    
        public decimal? ContractAmount { get; set; }
        //{
        //    get { return Math.Round(Amount - DiscountAmount + VATAmount + ActionAmount, 0); }
        //}     //ds hợp đồng 
        public string AccountingObjectName { get; set; }          // Tên đối tượng
        public string MaterialGoodsName { get; set; }     // Tên vật tư hàng hóa
        public string Unit { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? QuantityReceipt { get; set; }
        public decimal? QuantityLeft
        {
            get
            {
                if (Quantity != null && QuantityReceipt != null)
                    return Math.Round((decimal)Quantity - (decimal)QuantityReceipt, 2);
                else
                    return null;
            }
        }  //SL còn lại
        public string QuantityLefts
        {
            get { return QuantityLeft.ToString(); }
        }
        public bool RevenueType { get; set; }
        public string RevenueTypeText
        {
            get
            {
                return (RevenueType == false) ? "Chưa ghi doanh số" : "Đã ghi doanh số";
            }
        }
        //public decimal SAmount { get; set; }
        //public decimal SVATAmount { get; set; }
        //public decimal SDiscountAmount { get; set; }
        public decimal? ActionAmount { get; set; }
        //{
        //    get { return Math.Round(SAmount - SDiscountAmount + SVATAmount, 0); }
        //}   // Số tiền thực hiện
        public decimal? UnActionAmount
        {
            get
            {
                if (ContractAmount != null && ActionAmount != null)
                    return Math.Round((decimal)ContractAmount - (decimal)ActionAmount, 0);
                else
                    return null;
            }
        }    //Số tiền chưa thực hiện
        public Guid? AccountingObjectID { get; set; }
        public Guid? MaterialGoodsID { get; set; }
        public Guid? MaterialGoodsCategoryID { get; set; }
        public Guid? DepartmentID { get; set; }
        public Guid? ContractEmployeeID { get; set; }
        public Guid? AccountObjectGroupID { get; set; }
    }

    public class RContractDetail
    {
        public string Period { get; set; }
        public RContractDetail()
        {
            Period = "Kỳ báo cáo";
        }
    }
}
