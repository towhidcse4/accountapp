﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class RCost
    {
        public DateTime SignedDate { get; set; }         // Ngày ký   
        public string SignedDates
        {
            get { return (SignedDate != null) ? SignedDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""; }
        }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public Guid ID { get; set; }
        public string CostSetName { get; set; } //Tên hàng hóa, ct,hđ

        public decimal UncompleteFirstAmount { get; set; } //CP SXKD dở dang đầu kỳ
        public decimal UncompleteFirstMaterial { get; set; }
        public decimal UncompleteFirstLabor { get; set; }
        public decimal UncompleteFirstCost { get; set; }

        public decimal IncurredAmount { get; set; } //CP SXKD phát sinh
        public decimal IncurredMaterial { get; set; }
        public decimal IncurredLabor { get; set; }
        public decimal IncurredCost { get; set; }

        public decimal CostAmount { get; set; } // Giá thành sản phẩm, dịch vụ
        public decimal CostMaterial { get; set; }
        public decimal CostLabor { get; set; }
        public decimal CostOfCost { get; set; }

        public decimal UncompleteLastAmount { get; set; } // CP SXKD dở dang cuối kỳ
        public decimal UncompleteLastMaterial { get; set; }
        public decimal UncompleteLastLabor { get; set; }
        public decimal UncompleteLastCost { get; set; }

        public decimal? AcceptedAmount { get; set; } // Số nghiệm thu trong kỳ
        public decimal? UnAcceptedAmount { get; set; } // Số chưa nghiệm thu cuối kỳ       
    }
}
