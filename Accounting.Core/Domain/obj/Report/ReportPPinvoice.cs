﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class ReportPPInvoice
    {
        public string AccountingObjectCode { get; set; }// Mã NCC
        public string AccountingObjectName { get; set; }// Tên NCC
        public string MaterialGoodsCode { get; set; }//mã hàng
        public string MaterialGoodsName { get; set; }//tên hàng

        public DateTime PostedDate { get; set; }
        public string NumberAttach { get; set; } //hoặc No
        public string TypeName { get; set; } // Loại
        public string Description { get; set; } //diễn giải
        public decimal UnitPrice { get; set; }//đơn giá
        public decimal Quantity { get; set; } //số lượng
        public decimal Amount { get; set; }//thành tiền
        public decimal DiscountAmount { get; set; }//chiết khấu
        public decimal DiscountsTotalAmount { get; set; }//tổng tiền giảm giá
        public decimal QuantityReturn { get; set; }//số lượng hàng trả lại
        public decimal ReturnTotalAmount { get; set; }//tổng tiền trả lại

        public int TypeID { get; set; }//typeid tương ứng
        public Guid ReferenceID { get; set; }

        public string Hyperlink { get; set; } // Hyperlink
        public ReportPPInvoice()
        {
            Hyperlink = TypeID + ";" + ReferenceID;
        }
    }
}
