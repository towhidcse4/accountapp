﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S10DNN
    {
        public string FACategoryCode { get; set; }      // Mã loại TSCĐ
        public string FACategoryName { get; set; }      // Tên loại TSCĐ
        public string FACode { get; set; }              // Mã TSCĐ
        public string FAName { get; set; }              // Tên TSCĐ
        public string IncRefNo { get; set; }            // Số chứng từ          - Ghi tăng
        public string Hyperlink { get; set; }           //                      - Ghi tăng
        public DateTime IncRefDate { get; set; }        // NGày chứng từ        - Ghi tăng       
        public string MadeIn { get; set; }              // Nước sản xuất   
        public DateTime? UsedDate { get; set; }          // Ngày đưa vào sử dụng sử dụng
        public string SerialNumber { get; set; }        // Số hiệu TSCD
        public decimal? Price { get; set; }              // Nguyên giá
        public decimal? DepreciationRate { get; set; }    // Tỉ lệ khấu hao       - Khấu hao
        public decimal? DepreciationAmount { get; set; }  // Mức khấu hao         - Khấu hao
        public decimal? DepreciationCalc { get; set; }    // Khấu hao lũy kế      - Khấu hao
        public string DesRefNo { get; set; }            // Số chứng từ          - Ghi giảm       
        public DateTime DesRefDate { get; set; }        // Ngày chứng từ        - Ghi giảm
        public string Reason { get; set; }              //           Lý do           - Ghi giảm
        public int OrderType { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal? DiffDepreciation { get; set; }

        //public Guid? RefID { get; set; }
        //public int RefType { get; set; }


        public S10DNN() { }
        public S10DNN(int i)
        {
            FACategoryCode = "Loại TSCĐ " + i / 50;
            FACategoryName = "Tên loại TSCĐ " + i;
            FACode = "Mã TSCĐ " + i / 35;
            FAName = "Tên TSCĐ " + i;
            IncRefNo = "RefNo" + i;
            Hyperlink = "Hyperlink" + i;
            //IncRefDate = DateTime.Now.AddDays(25);
            MadeIn = "Madein" + i;
            //UsedDate = IncRefDate.AddDays(3);
            SerialNumber = "Serial" + i;
            Price = 250000 * (i % 35) + 1000000;
            DepreciationRate = i % 100 / 100;
            DepreciationAmount = 3000 * i;
            DepreciationCalc = 15000 * (i % 35);
            DesRefNo = "desRefNo" + i;
            //DesRefDate = UsedDate.AddDays(11);
            Reason = "Reason" + i;
            OrderType = i % 35 == 0 ? -1 : 0;
        }
    }
    public class S10DNNDetail
    {
        public string Period { get; set; }
    }
}
