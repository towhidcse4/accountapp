﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class BankForeignCurrency
    {
        public string AccountNumber { get; set; }       //Số Tài Khoản
        public string BankName { get; set; }            //Tên Ngân Hàng
        public string BankAccount { get; set; }         // Số Tài Khoản ngân hàng
        public string Currency { get; set; }            //Loại Tiền
        public DateTime Date { get; set; }              //Ngày 
        public string No { get; set; }                  //Số chứng từ
        public string Hyperlink { get; set; }
        public string Type { get; set; }                //Loại
        public string Description { get; set; }         //Diễn Giải
        public string Reason { get; set; }              //Diễn Giải khi tính nút cộng gộp
        public string CorrespondingAccount { get; set; } //Tài khoản đối ứng
        public decimal ExchangeRate { get; set; }       //Tỷ Giá
        public decimal Receipt { get; set; }            //Nguyên Tệ Thu
        public decimal EqReceipt { get; set; }          //Quy Đổi Thu
        public decimal Payment { get; set; }            //Nguyên tệ Chi
        public decimal EqPayment { get; set; }          //Quy Đổi Chi
        public decimal Closing { get; set; }            //Nguyên Tệ Tồn
        public decimal EqClosing { get; set; }          //Quy Đổi Tồn
        public decimal Opening { get; set; }            //Số dư đầu kỳ
        public decimal OpeningDebit { get; set; }            //Số dư đầu kỳ dư nợ
        public decimal OpeningCredit { get; set; }            //Số dư đầu kỳ dư có
        public decimal EqOpening { get; set; }          //Số dư đầu kỳ quy đổi
        public int OrderType { get; set; }
        public BankForeignCurrency() { }
        public BankForeignCurrency(int i)
        {
            AccountNumber = "Acc " + i / 35;
            BankName = "Bank " + i / 35;
            BankAccount = "BankAcc " + i / 35;
            Currency = "CCY" + i / 35;
            Date = DateTime.Now.AddDays(1);
            No = "No " + i;
            Hyperlink = "Hyperlink " + i;
            Type = "Type " + i;
            Description = "Des " + i;
            Reason = "";
            CorrespondingAccount = "Acc Cor " + i;
            ExchangeRate = 2000 + i * 10;
            Receipt = i * 2300 + 1000;
            EqReceipt = Receipt * ExchangeRate / 10;
            Payment = i * 30 + 20000;
            EqPayment = Payment * 10;
            Closing = 0;
            EqClosing = 0;
            Opening = i / 35 * 1000000 + 20000000;
            EqOpening = Opening * 150;
            OrderType = i % 35 == 0 ? -1 : 0;
        }
    }
    public class BankForeignCurrencyDetail
    {
        public string Period { get; set; }
    }
}