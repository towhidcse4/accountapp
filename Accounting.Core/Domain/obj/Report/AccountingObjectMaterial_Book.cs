﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class AccountingObjectMaterial_Book
    {
        public Guid AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public Guid MaterialGoodsID { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string MaterialGoodsName { get; set; }
        public decimal? DoanhSo { get; set; }
        public decimal? GiamTruDoanhThu { get; set; }
        public string GhiChu { get; set; }
    }
    public class AccountingObjectMaterial_Period
    {
        public string Period { get; set; }
    }
}
