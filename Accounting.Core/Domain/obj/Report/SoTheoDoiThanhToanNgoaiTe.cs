﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SoTheoDoiThanhToanNgoaiTe
    {
        public Int16 IdGroup { get; set; }
        public Guid AccountingObjectID { get; set; }
        public string AccountingObjectName { get; set; }
        public string Account { get; set; }
        public DateTime NgayHoachToan { get; set; }
        public DateTime NgayChungTu { get; set; }
        public string SoChungTu { get; set; }
        public string DienGiai { get; set; }
        public string TKDoiUng { get; set; }
        public decimal TyGiaHoiDoai { get; set; }
        public decimal PSNSoTien { get; set; }
        public decimal PSNQuyDoi { get; set; }
        public decimal PSCSoTien { get; set; }
        public decimal PSCQuyDoi { get; set; }
        public decimal DuNoSoTien { get; set; }
        public decimal DuNoQuyDoi { get; set; }
        public decimal DuCoSoTien { get; set; }
        public decimal DuCoQuyDoi { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
    }

    public class SoTheoDoiThanhToanNgoaiTe_period
    {
        public string Period { get; set; }
    }
}
