﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S07DNN16 //Sổ chi tiết vật liệu hàng hóa [DungNA]
    {
        public string RepositoryCode { get; set; }      //Mã Kho
        public string RepositoryName { get; set; }      //Tên Kho
        public string Currency { get; set; }            //Để mặc định là việt nam đồng
        public string MaterialGoodsCode { get; set; }   //Mã Hàng Hóa
        public string MaterialGoodsName { get; set; }   //Tên Hàng Hóa
        public string Hyperlink { get; set; }
        public DateTime PostedDate { get; set; }        //Ngày Chứng Từ
        public string Description { get; set; }         //Diễn Giải
        public string Reason { get; set; }              //Diễn Giải Khi Cộng Gộp
        public string Account { get; set; }             //Tài Khoản 
        public string AccountCorresponding { get; set; }//Tài Khoản Đối ứng
        public string Unit { get; set; }                //Đơn vị Tính
        public decimal UnitPrice { get; set; }          //Đơn Giá
        public decimal IWQuantity { get; set; }         //Số lượng nhập
        public decimal IWValue { get; set; }            //Thành Tiền nhập
        public decimal OWQuantity { get; set; }          //Số lượng xuất
        public decimal OWValue { get; set; }            //Thành Tiền Xuất
        public decimal ClosingQuantity { get; set; }    //SỐ lượng Tồn
        public decimal ClosingValue { get; set; }       //Thành Tiền Tồn
        public decimal OPQuantity { get; set; }         // Số lượng đầu kỳ
        public decimal OPAmount { get; set; }           // Thành tiền đầu kỳ
        public int OrderType { get; set; }
        public string No { get; set; }
        public string CurrencyID { get; set; }
        public S07DNN16() { }
    }
    public class S07DNN16Detail
    {
        public string Period { get; set; }
    }
}
