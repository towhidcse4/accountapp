﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class SAInvoiceDetailReport : SAInvoiceDetail
    {
        public virtual string MaterialGoodsCode { get; set; }

        public virtual string MaterialGoodsName { get; set; }
    }
}
