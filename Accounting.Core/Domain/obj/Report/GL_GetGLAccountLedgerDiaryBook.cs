﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GL_GetGLAccountLedgerDiaryBook
    {
        public string Currency { get; set; }        //Đơn vị tính
        public DateTime PostedDate { get; set; }    // Ngày tháng ghi sổ
        public DateTime RefDate { get; set; }    // Ngày tháng số hiệu
        public string RefNo { get; set; }    // Số hiệu
        public string JournalMemo { get; set; } //Diễn giải
        public string AccountNumber { get; set; } //Tài khoản      
        public string DetailAccountNumber { get; set; } //Tài khoản đối ứng   
        public string CorrespondingAccountNumber { get; set; } 
        public decimal OpenningDebitAmount { get; set; } //Số dư đầu kỳ có
        public decimal OpenningCreditAmount { get; set; } //Số dư đầu kỳ nợ   
        public decimal ClosingDebitAmount { get; set; } //Cộng phát sinh có
        public decimal ClosingCreditAmount { get; set; } //Cộng phát sinh nợ
        public decimal AccumDebitAmount { get; set; } //Số tiền nợ   
        public decimal AccumCreditAmount { get; set; } //Số tiền nợ   
        public decimal DebitAmount { get; set; } //Số tiền nợ   
        public decimal CreditAmount { get; set; } //Số tiền có
        public int AccountCategoryKind { get; set; }
        public Guid? RefID { get; set; }
        public int RefType { get; set; }

    }
    public class GL_GetGLAccountLedgerDiaryBookDetail
    {
        public string Period { get; set; }
    }
}
