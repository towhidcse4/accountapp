﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan
    {
        public Guid ContractID { get; set; }
        public int RefType { get; set; }
        public DateTime? NgayKy { get; set; }
        public string SoHopDong { get; set; }
        public string DoiTuong { get; set; }
        public string TrichYeu { get; set; }
        public decimal? GiaTriHopDong { get; set; }
        public decimal? GiaTriThucTe { get; set; }
        public decimal? CacKhoanGiamTru { get; set; }
        public decimal? DaThanhToan { get; set; }
        public decimal? ConLai { get; set; }
    }

    public class SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan_period
    {
        public string Period { get; set; }
    }
}
