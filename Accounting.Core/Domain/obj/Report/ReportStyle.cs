﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.Report
{
    public class ReportStyle
    {
        public PerpetuumSoft.Reporting.DOM.StyleCollection styleCollection { get; set; }
        public double AlignLeft { get; set; }
        public double AlignTop { get; set; }
        public double AlignBottom { get; set; }
        public double AlignRight { get; set; }
        public bool ShowProductInfo { get; set; }
    }
}
