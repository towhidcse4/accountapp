﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S03ADNN
    {
        public string Currency { get; set; }        //Đơn vị tính
        public DateTime PostedDate { get; set; }    // Ngày tháng ghi sổ
        public string No { get; set; }              //Số hiệu chứng từ
        public DateTime Date { get; set; }          //Ngày Tháng chứng từ
        public string Description { get; set; }     //Diễn Giải
        public string Reason { get; set; }          //Diễn Giải khi cộng gộp
        public string Posted { get; set; }          //Đã ghi sổ cái
        public string AccountCorresponding { get; set; } //Tài khoản đối ứng
        public string LineNo { get; set; }          //Số thứ tự
        public decimal DebitAmount { get; set; }    //Nợ
        public decimal CreditAmount { get; set; }   //Có
        public string HyperLink { get; set; }

        public S03ADNN() { }
        public S03ADNN(int i)
        {
            CreditAmount = 100000 + i * 2;
            DebitAmount = 200000 + i;
        }

    }
    public class S03ADNNDetail
    {
        public string Period { get; set; }
    }
}
