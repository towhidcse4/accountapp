﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class PO_GetDetailBook
    {
        public string MaKH { get; set; }
        public string TenKH { get; set; }
        public DateTime NgayHachToan { get; set; }
        public DateTime NgayCTu { get; set; }
        public string SoCTu { get; set; }
        public string SoHoaDon { get; set; }
        public DateTime? NgayHoaDon { get; set; }
        public string Mahang { get; set; }
        public string Tenhang { get; set; }
        public string DVT { get; set; }
        public decimal? SoLuongMua { get; set; }
        public decimal? DonGia { get; set; }
        public decimal? GiaTriMua { get; set; }
        public decimal? ChietKhau { get; set; }
        public decimal? GiaTriTraLai { get; set; }
        public decimal? GiaTriGiamGia { get; set; }
        public decimal? SoLuongTraLai { get; set; }
        public Guid RefID { get; set; }
        public int TypeID { get; set; }
    }

    public class PO_GetDetailBook_Detail
    {
        public string Period { get; set; }
    }

}
