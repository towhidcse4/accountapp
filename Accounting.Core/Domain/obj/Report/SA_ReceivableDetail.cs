﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.Report
{
    public class SA_ReceivableDetail
    {
        public DateTime? PostedDate { get; set; }	//Ngày hạch toán
        public DateTime? RefDate { get; set; }	//Ngày chứng từ
        public string RefNo { get; set; }	//Số chứng từ
        public string JournalMemo { get; set; }	//Diễn giải
        public string AccountNumber { get; set; }	//TK Công nợ
        public string CorrespondingAccountNumber { get; set; }	//Tài khoản đối ứng
        public int IDGroup { get; set;  }//add by namnh
        //add by cuongpv
        public decimal? DebitAmount { get; set; }	//Phát sinh nợ
        public decimal? CreditAmount { get; set; }	//Phát sinh có
        public decimal? ClosingDebitAmount { get; set; }	//Dư nợ
        public decimal? ClosingCreditAmount { get; set; }	//Dư có
        //end add by cuongpv
        public decimal? DebitAmountOC { get; set; }	//Phát sinh nợ
        public decimal? CreditAmountOC { get; set; }	//Phát sinh có
        public decimal? ClosingDebitAmountOC { get; set; }	//Dư nợ
        public decimal? ClosingCreditAmountOC { get; set; }	//Dư có
        public Guid? AccountObjectID { get; set; }
        public Guid? RefID { get; set; }
        public int? RefType { get; set; }
        public string AccountObjectCode { get; set; }	//Mã khách hàng
        public string AccountObjectName { get; set; }	//Tên khách hàng
        public decimal? Sum_DuNo { get; set; }
        public decimal? Sum_DuCo { get; set; }
    }
    public class SA_ReceivableDetail_period
    {
        public string Period { get; set; }
    }
}
