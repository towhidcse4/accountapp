﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GetChiTietBanHangTheoNhanVien
    {
        public Guid AccountingObjectID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public Guid EmployeeID { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public Guid SAInvoiceDetail { get; set; }
        public DateTime Date { get; set; }
        public string No { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Guid MaterialGoodsID { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string MaterialGoodsName { get; set; }
        public string Unit { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal DoanhSoBan { get; set; }
        public decimal ChietKhau { get; set; }
        public int SoLuongTraLai { get; set; }
        public decimal GiaTriTraLai { get; set; }
        public decimal GiamGia { get; set; }
        public int Type { get; set; }
        public int TypeID { get; set; }

        public Guid? RefID { get; set; }

    }
    public class GetChiTietBanHangTheoNhanVien_period
    {
        public string Period { get; set; }
    }
}
