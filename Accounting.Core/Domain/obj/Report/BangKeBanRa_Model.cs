﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class BangKeBanRa_Model
    {
        public string SO_HD { get; set; }
        public DateTime? NGAY_HD { get; set; }
        public string NGUOI_MUA { get; set; }
        public string MAT_HANG { get; set; }
        public string MST { get; set; }
        public decimal DOANH_SO { get; set; }
        public decimal THUE_GTGT { get; set; }
        public string TKTHUE { get; set; }
        public decimal VATRATE { get; set; }
        public string VATRATENAME { get; set; }
        public int? FLAG { get; set; }
        public string Note { get; set; }
         public int Ordercode { get; set; }
        public Guid? RefID { get; set; }
        public int RefType { get; set; }
    }

    public class BangKeBanRa_ModelDetail
    {
        public string Period { get; set; }
    }
}
