﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class Assets : SynthesisReport
    {
        public decimal AmountBeginning { get; set; }
        public decimal AmountEnding { get; set; }
        public int GroupID { get; set; }
        public String GroupName { get; set; }
        public Assets()
        {
            AmountBeginning = 0;
            AmountEnding = 0;
        }
    }
    public class B01DNN
    {
        public string ItemName { get; set; }            //
        public string ItemCode { get; set; }
        public string   Description      { get; set; }
        public decimal  ClosingAmount   { get; set; }
        public decimal  OpenningAmount  { get; set; }
        public int      GroupID       { get; set; }
        public string   GroupName   { get; set; }
        public bool     IsItalic        { get; set; }
        public bool     IsBold          { get; set; }
    }

    public class B01DNNDetail
    {
        public string Period { get; set; }
        public DateTime Date { get; set; }      // Ngày 
        public bool ShowB02DN { get; set; }
        public bool ShowB03DN { get; set; }
        public string MeasureUnit { get; set; } // Đơn vị tính
    }

}
