﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class GL_GeneralDiaryBook_S03a
    {
        public DateTime? PostedDate { get; set; }	// Ngày hạch toán
        public DateTime? RefDate { get; set; }	// Ngày chứng từ
        public string RefNo { get; set; }	// Số chứng từ
        public string JournalMemo { get; set; }	// Diễn giải
        public string AccountNumber { get; set; }	// Tài khoản
        public string CorrespondingAccountNumber { get; set; }	// Tài khoản đối ứng
        public decimal? DebitAmount { get; set; }	// Phát sinh nợ
        public decimal? CreditAmount { get; set; }	// Phát sinh có

        public Guid? ReferenceID { get; set; }	// id chung tu trungnq
        public int TypeID { get; set; }	// loai chung tu trungnq
        public int OderType { get; set; }// trungnq thêm để set sắp xếp hiển thị lũy kế kỳ trước chuyển sang lên đầu
    }
    public class GL_GeneralDiaryBook_S03a_Detail
    {
        public string Period { get; set; }
    }
}
