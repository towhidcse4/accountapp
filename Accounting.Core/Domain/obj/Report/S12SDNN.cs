﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S12DNN
    {
        public S12DNN(int i)
        {
            Infos = new List<RFAInfo>();
            Vouchers = new List<RFAVoucher>();
            Attacments = new List<RFAAttackment>();
            // info
            RFAInfo inf = new RFAInfo(i);
            Infos.Add(inf);
            // voucher
            for (int j = 0; j < 35; j++)
            {
                RFAVoucher v = new RFAVoucher(inf.FixedAssetCode, j);
                Vouchers.Add(v);
                RFAAttackment a = new RFAAttackment(inf.FixedAssetCode, j);
                Attacments.Add(a);
            }
        }

        public S12DNN()
        {
            Infos = new List<RFAInfo>();
            Vouchers = new List<RFAVoucher>();
            Attacments = new List<RFAAttackment>();
        }
        public List<RFAInfo> Infos { get; set; }
        public List<RFAVoucher> Vouchers { get; set; }
        public List<RFAAttackment> Attacments { get; set; }


        public void Add(S12DNN s23DN)
        {
            Infos.AddRange(s23DN.Infos);
            Vouchers.AddRange(s23DN.Vouchers);
            Attacments.AddRange(s23DN.Attacments);
        }
    }
    public class RFAInfo
    {

        public string DeliveryRecordNo { get; set; }        // Biên bản giao nhận số
        public DateTime DeliveryRecordDate { get; set; }    // Ngày biên bản giao nhận          
        public virtual string ProductionYear { get; set; }             // năm sản xuất
        public virtual string UseYear { get; set; }
        public string MadeIn { get; set; }                  // made in
        public string FixedAssetCode { get; set; }          // Code
        public DateTime UsedDate { get; set; }              // Thời gian sử d
        public string FixedAssetName { get; set; }          // tên tscđ
        public string SerialNumber { get; set; }            // Số hiệu tài sản cố định
        public Guid? DepartmentID { get; set; }          // bộ phận quản lý hoặc sử dụng
        public String DescriptionDecrement { get; set; }
        public DateTime DateDecrement { get; set; }
        public String NoDecrement { get; set; }
        public String No { get; set; }
        public DateTime? PostedDate { get; set; }

        public RFAInfo()
        {
            FixedAssetCode = "";
            PostedDate = null;
        }

        public RFAInfo(int i)
        {
            DeliveryRecordNo = "No" + i;
            DeliveryRecordDate = DateTime.Now.Date.AddDays(-20);
            FixedAssetCode = "FA Code " + i;
            UsedDate = DateTime.Now.Date.AddDays(i);
            FixedAssetName = "FA Name " + i;
            SerialNumber = "FA Serial " + i;
            ProductionYear = "2000" + i;
            UseYear = "2002" + i;
            MadeIn = "Made in " + i;
        }
    }
    public class RFAVoucher
    {
        public string FixedAssetCode { get; set; }      // Code
        public string RefNumber { get; set; }           // số hiệu chứng từ
        public string Hyperlink { get; set; }           //
        public DateTime RefDate { get; set; }           // Ngày tháng năm tài sản cố định là PostDate
        public string JournalMemo { get; set; }         // diễn giải tài sản cố định
        public virtual decimal? OrgPrice { get; set; }           // Nguyên giá có 2 cột số tiền ghi tăng và ghi giảm
        public int? YearDepreciation { get; set; }       //Năm hao mòn
        public virtual decimal? DepreciationValue { get; set; }  // SỐ tiền khấu hao có 2 cột là số tiền khấu hao ghi tăng và số tiền khấu hao ghi giảm
        public virtual decimal? TotalDepreciationValue { get; set; }  // SỐ tiền khấu hao có 2 cột là số tiền khấu hao ghi tăng và số tiền khấu hao ghi giảm

        public RFAVoucher() { }
        public RFAVoucher(string p, int j)
        {
            // TODO: Complete member initialization
            FixedAssetCode = p;
            RefNumber = p + "RefNum " + j;
            Hyperlink = p + "Hyperlink" + j;
            RefDate = DateTime.Now.Date.AddDays(j);
            JournalMemo = "Diễn giải " + j;
            OrgPrice = j * 10000;
            YearDepreciation = 1990 + j;
            DepreciationValue = j * 2010;
        }
    }
    public class RFAAttackment
    {
        public string FixedAssetCode { get; set; }                  // Code
        public string FixedAssetAccessoriesName { get; set; }       //Tên quy cách dụng cụ phụ tùng FixedAssetAttachmentName
        public string FixedAssetAccessoriesUnit { get; set; }       //Đơn vị tính FixedAssetAttachmentUnit
        public virtual decimal? FixedAssetAccessoriesQuantity { get; set; }  // Số lượng dụng cụ phụ tùng FixedAssetAttachmentQuantity
        public virtual decimal? FixedAssetAccessoriesAmount { get; set; }    //Giá trị dụng cụ phụ tùng FixedAssetAttachmentAmount
        public RFAAttackment() { }
        public RFAAttackment(string p, int j)
        {
            FixedAssetCode = p;
            FixedAssetAccessoriesName = p + "Tên phụ tùng " + j;
            FixedAssetAccessoriesUnit = p + "Đơn vị tính " + j;
            FixedAssetAccessoriesQuantity = 108 * j;
            FixedAssetAccessoriesAmount = 20020 * j;
        }
    }
    public class S12DNNDetails
    {
        public string Period { get; set; }
        public S12DNNDetails()
        {
            Period = "Kỳ báo cáo";
        }
    }
}
