﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj.Report
{
    public class BangKeMuaVao_Model
    {
        public string SO_HD { get; set; }
        public DateTime? NGAY_HD { get; set; }
        public string TEN_NBAN { get; set; }
        public string MAT_HANG { get; set; }
        public string MST { get; set; }
        public decimal GT_CHUATHUE{ get; set; }
        public decimal THUE_SUAT { get; set; }
        public int THUE_SUAT_DETAIL { get; set; }
        public string THUE_SUAT_STRING { get; set; }
        public decimal THUE_GTGT { get; set; }
        public string TK_THUE { get; set; }
        public string GOODSSERVICEPURCHASECODE { get; set; }
        public string GOODSSERVICEPURCHASENAME{ get; set; }
        public int? FLAG { get; set; }
        public int? STT { get; set; }
        public string Note { get; set; }
        public Guid VoucherID { get; set; }

        public Guid? RefID { get; set; }
        public int TypeID { get; set; }
        public int Ordercode { get; set; }
    }
    public class BangKeMuaVao_ModelDetail
    {
        public string Period { get; set; }
    }
}
