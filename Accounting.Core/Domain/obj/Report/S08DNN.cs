﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class S08DNN
    {
        public string BankAccount { get; set; }//Số tài khoản(Tại nơi tiền gửi)
        public string BankName { get; set; } //Tên Ngân Hàng(Nơi mở tài khoản giao dịch)
        public string AccountNumber { get; set; }//Loại Tài Khoản
        public DateTime Date { get; set; }//Ngày tháng ghi sổ
        public string No { get; set; } //Số hiệu chứng từ
        public DateTime PostedDate { get; set; } // Ngày tháng chứng từ   
        public string Description { get; set; }// Diễn giải
        public string AccountCorresponding { get; set; }//Tài khoản đối ứng
        public decimal DebitAmount { get; set; }//Thu(gửi vào)
        public decimal CreditAmount { get; set; }// Chi(Rút ra)        
        public string Note = "";//Trường ghi chú
        public string Reason { get; set; }//Hiển thị khi bấm nút cộng gộp để hiển thị diễn giải
        public string Hyperlink { get; set; }//
        public decimal OpeningDebit { get; set; }//Số dư đầu kỳ dư nợ
        public decimal OpeningCredit { get; set; }//Số dư đầu kỳ dư có
        public decimal OpeningBlance { get; set; }//Số dư đầu kỳ dự nợ
        public int OrderType { get; set; }
        public S08DNN()
        {
            OrderType = 0;
        }
        public S08DNN(int i)
        {
            if (i % 50 == 0)
            {
                Description = "- Số phát sinh trong kỳ";
                OrderType = -1;
                BankAccount = "Bank Account " + i / 70;
                BankName = "Bank Name " + i / 70;
                AccountNumber = "" + i / 50;
                No = "";

            }
            else if (i % 50 == 1)
            {
                OrderType = -2;
                Description = "Số dư đầu kỳ";
                OpeningBlance = 20000000;
                BankAccount = "Bank Account " + i / 70;
                BankName = "Bank Name " + i / 70;
                AccountNumber = "" + i / 50;
                No = "";
            }
            else
            {
                BankAccount = "Bank Account " + i / 70;
                BankName = "Bank Name " + i / 70;
                AccountNumber = "" + i / 50;
                Date = DateTime.Now.AddDays(i);
                No = "No " + i;
                PostedDate = DateTime.Now.AddDays(i).AddMonths(-1);
                Description = "Description " + i;
                AccountCorresponding = "AC" + i;
                DebitAmount = i % 2 == 0 ? 10000 * i : 0;
                CreditAmount = i % 2 == 0 ? 0 : 10021 * i;
                Note = "";
                Hyperlink = "Hyperlink";
                OrderType = 0;
            }
        }
    }
    public class S08DNNDetail
    {
        public string Period { get; set; } // Kỳ báo cáo
        public S08DNNDetail()
        {
            Period = "Kỳ Báo cáo ";
        }
    }
}
