﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core
{
    public class Result
    {
        public bool IsSucceed { get; set; }
        public DateTime? Date { get; set; }
        public string No { get; set; }
        public string TypeName { get; set; }
        public string Reason { get; set; }
        public string Code { get; set; }
        public Guid? refID { get; set; }
        public int? TypeID { get; set; }
    }
}
