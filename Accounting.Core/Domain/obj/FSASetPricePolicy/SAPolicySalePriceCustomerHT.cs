﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core
{
    public class SAPolicySalePriceCustomerHT
    {
        public Guid ID { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public string AccountingObjectCategory { get; set; }
        public string AccountinggObjectCategoryCode { get; set; }
        public string SalePriceGroupCode { get; set; }
    }
}
