﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core
{
    public class MaterialGoodHT 
    {
        public virtual bool Status { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string LoaiVTHH { get; set; }
        public virtual string MaVTHH { get; set; }
        public virtual string TenVTHH { get; set; }
        public virtual decimal GiaNhapGanNhat { get; set; }
        public virtual decimal FixedSalePrice { get; set; }
        public virtual decimal SalePrice { get; set; }
        public virtual decimal SalePrice2 { get; set; }
        public virtual decimal SalePrice3 { get; set; }
    }
}
