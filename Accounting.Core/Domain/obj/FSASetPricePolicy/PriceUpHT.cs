﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
   public class PriceUpHT
    {
       public Guid ID { get; set; }
       public string MaVTHH { get; set; }
       public string TenVTHH { get; set; }
       public string NhomVTHH { get; set; }
       public decimal GiaBanGanNhat { get; set; }
    }
}
