﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core
{
    public class SAPolicyPriceTableHTView
    {
        public Guid? MaterialGoodsCategoryID { get; set; }
        public string MaterialGoodsCategoryCode { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string MaterialGoodsName { get; set; }
    }
}
