﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core
{
   public class GroupSalePriceHT
    {
       public bool Status { get; set; }
       public Guid Id { get; set; }
       public string NhomGia { get; set; }
       public string DuaTren { get; set; }
       public string PhuongPhap { get; set; }
       public decimal? PhanTramHoacSTTG { get; set; }
       public string TenNhomGias { get; set; }
    }
}
