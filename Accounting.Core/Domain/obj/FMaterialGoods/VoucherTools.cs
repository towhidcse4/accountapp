﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class VoucherTools
    {
        public virtual Guid ID { get; set; }
        public virtual string No { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual string ToolsName { get; set; }
        public virtual string ToolsCode { get; set; }
        public virtual Guid? ToolsID { get; set; }
    }

}
