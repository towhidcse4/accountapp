﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InvoiceClient
{
    public class EInvoiceVietTel
    {
        //[JsonProperty(PropertyName = "generalInvoiceInfo")]
        public InvoiceInfo generalInvoiceInfo;
        public BuyerInfo buyerInfo;
        public SellerInfo sellerInfo;
        public List<payments> payments = new List<payments>();
        //[JsonProperty(PropertyName = "itemInfo")]
        public List<ItemInfo> itemInfo;
        //[JsonProperty(PropertyName = "discountItems")]
        public List<DiscountItemInfo> discountItems;
        //[JsonProperty(PropertyName = "summarizeInfo")]
        public SummarizeInfo summarizeInfo;
        public List<TaxBreakdowns> taxBreakdowns = new List<TaxBreakdowns>();
    }
    public class InvoiceInfo
    {
        public string transactionUuid { get; set; }
        public string templateCode { get; set; }
        public string invoiceSeries { get; set; }
        public string invoiceIssuedDate { get; set; }
        public string invoiceType { get; set; }
        public string currencyCode { get; set; }
        public string adjustmentType { get; set; }
        public string paymentStatus { get; set; }
        public string cusGetInvoiceRight { get; set; }
        public string invoiceNote { get; set; }
        public string adjustmentInvoiceType { get; set; }
        public string originalInvoiceId { get; set; }
        public string originalInvoiceIssueDate { get; set; }
        public string additionalReferenceDesc { get; set; }
        public string additionalReferenceDate { get; set; }
        public string certificateSerial { get; set; }
        public string userName { get; set; }
        [JsonIgnore]
        public decimal? ExchangeRate { get; set; }
    }

    public class BuyerInfo
    {
        public string buyerName { get; set; }
        [JsonIgnore]
        public string buyerCode { get; set; }
        public string buyerLegalName { get; set; }
        public string buyerTaxCode { get; set; }
        public string buyerAddressLine { get; set; }
        public string buyerPhoneNumber { get; set; }
        public string buyerBankName { get; set; }
        public string buyerBankAccount { get; set; }
        public string buyerEmail { get; set; }
        public string buyerIdNo { get; set; }
        public string buyerIdType { get; set; }
    }

    public class SellerInfo
    {
        public string sellerLegalName { get; set; }
        public string sellerTaxCode { get; set; }
        public string sellerAddressLine { get; set; }
        public string sellerPhoneNumber { get; set; }
        public string sellerEmail { get; set; }
        public string sellerBankName { get; set; }
        public string sellerBankAccount { get; set; }
    }

    public class ItemInfo
    {
        public string lineNumber { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string unitName { get; set; }
        public decimal ?unitPrice { get; set; }
        public decimal ?quantity { get; set; }
        public decimal itemTotalAmountWithoutTax { get; set; }
        public decimal ?taxPercentage { get; set; }
        public decimal ?taxAmount { get; set; }
        public decimal ?discount { get; set; }
        public decimal ?itemDiscount { get; set; }
        public decimal adjustmentTaxAmount { get; set; }
        public string isIncreaseItem { get; set; }
        public string itemNote { get; set; }
        public string batchNo { get; set; }
        public string expDate { get; set; }
        public int ?selection { get; set; }
        [JsonIgnore]
        public bool? IsPromotion { get; set; }
    }
    public class DiscountItemInfo
    {
        public string lineNumber { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string unitName { get; set; }
        public string unitPrice { get; set; }
        public string quantity { get; set; }
        public string itemTotalAmountWithoutTax { get; set; }
        public string taxPercentage { get; set; }
        public string taxAmount { get; set; }
        public string discount { get; set; }
        public string itemDiscount { get; set; }
        public string adjustmentTaxAmount { get; set; }
        public string isIncreaseItem { get; set; }
        public string itemNote { get; set; }
        public string batchNo { get; set; }
        public string expDate { get; set; }
        public string selection { get; set; }
    }

    public class ZipFileResponse
    {
        public string errorCode { get; set; }
        public string description { get; set; }
        public string fileName { get; set; }
        public byte[] fileToBytes { get; set; }
        public bool paymentStatus { get; set; }
    }
    public class payments
    {
        public string paymentMethodName { get; set; }
    }

    public class GetFileRequest
    {
        public string invoiceNo { get; set; }
        public string fileType { get; set; }
        public string strIssueDate { get; set; }
        public string additionalReferenceDesc { get; set; }
        public string additionalReferenceDate { get; set; }
        public string pattern { get; set; }
        public string supplierTaxCode { get; set; }
        public string transactionUuid { get; set; }
    }

    public class PDFFileResponse
    {
        public string errorCode { get; set; }
        public string description { get; set; }
        public string fileName { get; set; }
        public byte[] fileToBytes { get; set; }
    }

    public class SummarizeInfo
    {
        public decimal sumOfTotalLineAmountWithoutTax { get; set; }
        public decimal totalAmountWithoutTax { get; set; }
        public decimal totalTaxAmount { get; set; }
        public decimal totalAmountWithTax { get; set; }
        public string totalAmountWithTaxInWords { get; set; }
        public decimal discountAmount { get; set; }
        public decimal settlementDiscountAmount { get; set; }
        public decimal taxPercentage { get; set; }
        public string isTotalAmountPos { get; set; }
        public string isTotalTaxAmountPos { get; set; }
        public string isTotalAmtWithoutTaxPos { get; set; }
        public string isDiscountAmtPos { get; set; }
    }


    public class TaxBreakdowns
    {
        public decimal taxPercentage { get; set; }
        public decimal taxableAmount { get; set; }
        public decimal taxAmount { get; set; }
    }
    public class RequestSignature
    {
        public string supplierTaxCode { get; set; }
        public string templateCode { get; set; }
        public string hashString { get; set; }
        public string signature { get; set; }
    }
    public class ConvertInvoice
    {
        public string supplierTaxCode { get; set; }
        public string invoiceNo { get; set; }
        public string strIssueDate { get; set; }
        public string exchangeUser { get; set; }
    }

    public class DestructionInvoice
    {
        public string supplierTaxCode { get; set; }
        public string invoiceNo { get; set; }
        public string strIssueDate { get; set; }
        public string additionalReferenceDesc { get; set; }
        public string additionalReferenceDate { get; set; }

    }
    public class GetListInvoiceWithDate
    {
        public string supplierTaxCode { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}
