﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TT153PublishRegister
    {
        public bool Status { get; set; }
        //Khong dung ID nay
        public Guid TT153RegisterInvoiceDetailID { get; set; }
        public Guid ReportID { get; set; }
        public int InvoiceForm { get; set; }
        public int InvoiceType { get; set; }
        public String InvoiceSeries { get; set; }
        public Guid InvoiceTypeID { get; set; }
        public String InvoiceTemplate { get; set; }
        public String InvoiceForm_String { get; set; }
        public String InvoiceType_String { get; set; }

        public String FromNo { get; set; }
        public String ToNo { get; set; }
        public Decimal Quantity { get; set; }
        public DateTime StartUsing { get; set; }
        public String CompanyName { get; set; }
        public String CompanyTaxCode { get; set; }
        public String ContractNo { get; set; }
        public DateTime? DSigned { get; set; }
        public int OrderPriority { get; set; }
        public string Quantity_Int { get; set; }


    }
}
