﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class WagePaymentDetail
    {
        public DateTime Date { get; set; }
        public DateTime PostedDate { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal RemainingAmount { get; set; }
        public int Type { get; set; }
        public Guid ID { get; set; }
        public WagePaymentDetail()
        {
            Amount = 0;
            TotalAmount = 0;
        }
    }
}
