﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TT153RegisterReportDetail
    {   

        public bool Status { get; set; }
        public Guid ID { get; set; }
        public string InvoiceForm { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceTemplate { get; set; }
        public String InvoiceSeries { get; set; }
        public String Purpose { get; set; }
    }
}
