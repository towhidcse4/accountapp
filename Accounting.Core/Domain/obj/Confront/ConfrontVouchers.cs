﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.Domain
{
    public class ConfrontVouchers
    {
        public virtual bool Status { get; set; }
        public virtual DateTime? NgayChungTu { get; set; }
        public virtual string TenLoaiChungTu { get; set; }
        public virtual string LoaiChungTu { get; set; }
        public virtual string SoChungTu { get; set; }
        public virtual String SoHoaDon { get; set; }
        public virtual string NhanVien { get; set; }
        public virtual Decimal? TriGia { get; set; }
        public virtual Decimal? TriGiaQd { get; set; }
        public virtual Decimal? ChuaThanhToan { get; set; }// Chứng từ nợ
        public virtual Decimal? ChuaThanhToanQd { get; set; }// chứng từ nợ
        public virtual Decimal? SoConLai { get; set; } // chứng từ trả tiền/ Thu tiền
        public virtual Decimal? SoConLaiQd { get; set; }// chứng từ trả tiền/ thu tiền
        public virtual Decimal? SoDoiTru { get; set; }
        public virtual decimal SoDu { get; set; }
        public virtual Guid GlVoucherId { get; set; } // nợ
        public virtual Guid GlVoucherExceptId { get; set; } // có
       // public virtual int ThuTuuuTien { get; set; }
        public virtual bool isCease { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal TyGia { get; set; }
        public virtual decimal? RefVoucherExchangeRate { get; set; }
        public virtual decimal LastExchangeRate { get; set; }
        public virtual decimal SoDoiTruQD { get; set; }


    }
}
