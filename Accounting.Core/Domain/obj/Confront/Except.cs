﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.Domain
{
    public class Except
    {
        public virtual IList<ConfrontVouchers> Credits { get; set; }
        public virtual IList<ConfrontVouchers> Debits { get; set; }
        public Except()
        {
            Credits = new List<ConfrontVouchers>();
            Debits = new List<ConfrontVouchers>();
        }

    }
}
