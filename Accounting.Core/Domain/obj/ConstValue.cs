﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Accounting.Core.Domain
{
    /// <summary>
    /// Đối tượng Load các thiết lập const từ XML
    /// </summary>
    [XmlRootAttribute("ConstValue", Namespace = "", IsNullable = false)]
    [Serializable]
    public class ConstValue
    {
        [XmlArray("AccountKind"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> AccountKinds = new List<Item>();

        [XmlArray("DetailType"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> DetailTypes = new List<Item>();

        [XmlArray("SelectTime"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> SelectTimes = new List<Item>();

        [XmlArray("SelectTimeKVBD"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> SelectTimesKVBD = new List<Item>();

        [XmlArray("TypePeriod"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> TypePeriod = new List<Item>();

        [XmlArray("UtilitiesCaption"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> UtilitiesCaption = new List<Item>();

        [XmlArray("Utilities"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> Utilities = new List<Item>();

        [XmlArray("Browser"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> Browser = new List<Item>();

        [XmlArray("countries"), XmlArrayItem("country", typeof(country))]
        public List<country> countries = new List<country>();

        [XmlArray("DefaultValue"), XmlArrayItem("Item", typeof(Item))]
        public List<Item> DefaultValue = new List<Item>();
    }
}
