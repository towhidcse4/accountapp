﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj.AllOPAccount
{
    [Serializable]
    public class AllOpAccount : Account
    {
        public virtual decimal SumDebitAmount { get; set; } // Dư nợ
        public virtual decimal SumCreditAmount { get; set; }// Dư có
    }

    [Serializable]
    public class OpAccountDetailByOrther : OPAccount
    {
        public virtual string AccountName { get; set; }
        public virtual Guid? ParentID { get; set; }
        public virtual string DetailType { get; set; }
        public virtual int AccountGroupKind { get; set; }
        public virtual string BankAccount { get; set; }
        public virtual string BankName { get; set; }
    }

    [Serializable]
    public class OpAccountObjectByOrther : OPAccount
    {
        public virtual string AccountingObjectCode { get; set; }
        public virtual string AccountingObjectName { get; set; }
    }

}
