﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain.obj
{
    [Serializable]
    public class OPMaterialGoodsByOrther:OPMaterialGoods
    {
        public virtual string MaterialGoodsCode { get;set; }
        public virtual string MaterialGoodsName { get; set; }
        public Guid? MaterialGoodsCategoryID { get; set; }
        public virtual string RepositoryCode { get; set; }
    }
}
