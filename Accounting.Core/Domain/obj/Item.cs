﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Accounting.Core.Domain
{
    /// <summary>
    /// Đối tượng con của obj ConstValue
    /// </summary>
    [Serializable]
    public class Item
    {
        public Item() { }

        [XmlAttribute]
        public string ID { get; set; }
        [XmlAttribute]
        public string Value { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Visible { get; set; }
        [XmlAttribute]
        public string IsSecurity { get; set; }
    }

    public class country
    {
        public country() { }

        [XmlAttribute]
        public string countryCode { get; set; }
        [XmlAttribute]
        public string countryName { get; set; }
    }
}
