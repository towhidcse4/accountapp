﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain.obj
{
    public class TT153ReportDetail
    {
        public string CopyNumberName { get; set; } // tên liên
        public string Purpose { get; set; } // mục đích 
        public string CodeColor { get; set; } // mã màu
    }
}
