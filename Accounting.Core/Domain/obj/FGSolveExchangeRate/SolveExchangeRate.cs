﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SolveExchangeRateDetail
    {
        public virtual bool Status { get; set; }
        public virtual string AccountNumber { get; set; } // Tài khoản
        public virtual DateTime Date { get; set; } // ngày chứng từ
        public virtual string VoucherNo { get; set; } // số chứng từ
        public virtual string InvoiceNo { get; set; } // số hóa đơn
        public virtual string CurrencyId { get; set; } // loại tiền tệ
        public virtual decimal AmountBanlace { get; set; } // Số dư có
        public virtual decimal AmountDeficit { get; set; } // số dư nợ
        public virtual Guid AccountingObjectID  { get; set; }
        public virtual string AccountingObjectCode { get; set; }
        public virtual int ContractStateId { get; set; }
        public virtual Guid StatisticsCodeId { get; set; }
    }
    [Serializable]
    public class SolveExchangeRate
    {
        public string AccountInterest { get; set; } //tài khoản lãi
        public string AccountDeficit { get; set; } //tài khoản thâm hụt
        public DateTime PostedDate { get; set; }
        public DateTime Date { get; set; }
        public string No { get; set; }
        public string Reason { get; set; }

        public List<SolveExchangeRateDetail> ListSolveExchangeRateDetails { get; set; }

        public SolveExchangeRate()
        {
            ListSolveExchangeRateDetails = new List<SolveExchangeRateDetail>();
        }
    }
}
