﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    class PurchaseCostAllocation
    {
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? Rate { get; set; }
        public virtual decimal? FreightAmount { get; set; }
    }
}
