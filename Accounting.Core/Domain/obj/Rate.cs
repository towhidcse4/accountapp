﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class Rate
    {
        private decimal _value;
        private string _description;

        public virtual decimal Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
