﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
      [Serializable]
    public class UgridSelectDetail
    {
        public virtual string No { set; get; }// số chứng từ
        public virtual DateTime PostDate  { get; set; }// ngày chứng từ
        public virtual string  SaInvoiceNo { get; set; }// số hóa đơn
        public virtual DateTime SaInvoiceDate { get; set; } // ngày hóa đơn
        public virtual string Reason { get; set; }// diễn giải
        public virtual  decimal Amount { get; set; }// tổng tiền
         public virtual string SaInvoiceCode { get; set; } // mã hàng hóa
         public virtual string SaInvoiceName { get; set; }// tên hàng
         public virtual double quantumNo { get; set; } // số lượng
    }
}
