﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class CustomerDebit
    {
        private string _code;
        private string _name;
        private string _address;
        private string _phone;
        private string _fax;
        private string _email;
        private decimal _soDuDauNam;
        private decimal _soPhatSinh;
        private decimal _soDaThu;
        private decimal _soConPhaiThu;
        private List<ReceiptDebit> _receipts = new List<ReceiptDebit>();

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public virtual string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public virtual string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        public virtual string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public virtual decimal SoDuDauNam
        {
            get { return _soDuDauNam; }
            set { _soDuDauNam = value; }
        }

        public virtual decimal SoPhatSinh
        {
            get { return _soPhatSinh; }
            set { _soPhatSinh = value; }
        }

        public virtual decimal SoDaThu
        {
            get { return _soDaThu; }
            set { _soDaThu = value; }
        }

        public virtual decimal SoConPhaiThu
        {
            get { return _soConPhaiThu; }
            set { _soConPhaiThu = value; }
        }

        public virtual List<ReceiptDebit> Receipts
        {
            get { return _receipts; }
            set { _receipts = value; }
        }
    }
}
