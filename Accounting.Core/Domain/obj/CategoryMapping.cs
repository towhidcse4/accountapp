﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CategoryMapping
    {
        public bool Status { get; set; }
        public string SheetColumnHeader { get; set; }
        public string SheetColumnName1 { get; set; }
        public string SheetColumnName2 { get; set; }
        public string Describe { get; set; }
        public string ColumnMapping { get; set; }
    }
}
