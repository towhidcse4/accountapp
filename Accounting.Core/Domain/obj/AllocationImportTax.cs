﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class AllocationImportTax
    {
        public bool CheckColumn { get; set; }
        public Guid MaterialGoodsID { get; set; }
        public string MaterialGoodsCode { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal ImportTaxExpenseRate { get; set; }
        public decimal ImportTaxExpenseAmount { get; set; }
        public PPInvoiceDetail PPInvoiceDetail { get; set; }
        public AllocationImportTax()
        {
            Quantity = 0;
            Amount = 0;
            ImportTaxExpenseRate = 0;
            ImportTaxExpenseAmount = 0;
        }
    }
}
