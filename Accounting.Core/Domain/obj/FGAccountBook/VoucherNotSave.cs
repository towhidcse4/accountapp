﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class VoucherNotSave
    {
        public virtual Guid VoucherId { get; set; }
        public virtual string VoucherName { get; set; } // chuỗi có dạng: "Tên đối tượng - Mã đối tượng"
        public virtual DateTime VoucherDate { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string VoucherNo { get; set; }
        public virtual int ProcessType { get; set; }
    }
    
}
