﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Accounting.Core
{
    [Serializable]
    public class AuthenticatedUser
    {
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string GroupName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual Hashtable Roles { get; set; }
        public virtual Hashtable Permissions { get; set; }
    }
}
