﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Accounting.Core
{
    public class EInvoice
    {
        [XmlIgnore()]
        public virtual Guid ID { get; set; }
        [XmlIgnore()]
        public Dictionary<string, string> IkeyEmail { get; set; }
        //public virtual string Email { get; set; }
        public virtual string key { get; set; }
        [XmlIgnore()]
        public virtual string Pattern { get; set; } // Mẫu số hđ
        [XmlIgnore()]
        public virtual string Serial { get; set; } // kí hiệu hđ
        public virtual string InvNo { get; set; } // số hđ
        [XmlIgnore()]
        public virtual DateTime? InvDate { get; set; } // 
        [XmlIgnore()]
        public virtual DateTime? RefDateTime { get; set; } // 
        public virtual string CusCode { get; set; } //Mã khách hàng
        public virtual string Buyer { get; set; } // Tên người mua
        public virtual string CusName { get; set; } //Tên khách hàng
        public virtual string Email { get; set; } //Tên khách hàng
        public virtual string EmailCC { get; set; } //Tên khách hàng
        public virtual string CusAddress { get; set; }
        public virtual string CusBankName { get; set; }
        public virtual string CusBankNo { get; set; }
        public virtual string CusPhone { get; set; }
        public virtual string CusTaxCode { get; set; }
        public virtual string PaymentMethod { get; set; } //phương thức thanh toán
        public virtual string ArisingDate { get; set; } // Ngày tạo lập đồng thời ngày phát hành
        public virtual decimal? ExchangeRate { get; set; } //Tỷ giá chuyển đổi
        public virtual string CurrencyUnit { get; set; }
        public List<Product> Products { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual string VATRate { get; set; }
        public virtual decimal Amount { get; set; }
        [XmlIgnore()]
        public virtual decimal DiscountAmount { get; set; }
        public virtual string AmountInWords { get; set; }
        public virtual string Extra { get; set; }
        [XmlIgnore()]
        public virtual int StatusInvoice { get; set; }
        [XmlIgnore()]
        public virtual string StatusInvoiceStr
        {
            get
            {
                if (StatusInvoice == -1) return "Hoá đơn không tồn tại trong hệ thống";
                if (StatusInvoice == 0) return "Hoá đơn mới tạo lập";
                if (StatusInvoice == 1) return "Hoá đơn có chữ ký số";
                if (StatusInvoice == 2) return "Hoá đơn đã khai báo thuế";
                if (StatusInvoice == 3) return "Hoá đơn bị thay thế";
                if (StatusInvoice == 4) return "Hoá đơn bị điều chỉnh";
                if (StatusInvoice == 5) return "Hoá đơn bị huỷ";
                if (StatusInvoice == 6) return "Hoá đơn chờ";
                if (StatusInvoice == 7) return "Hoá đơn mới tạo lập(TT)";
                if (StatusInvoice == 8) return "Hoá đơn mới tạo lập(ĐC)";
                return null;
            }
        }
        [XmlIgnore()]
        public virtual bool StatusSendMail { get; set; }
        [XmlIgnore()]
        public virtual bool StatusCusView { get; set; }
        [XmlIgnore()]
        public virtual bool Status { get; set; }
        [XmlIgnore()]
        public virtual bool StatusConverted { get; set; }
        [XmlIgnore()]
        public virtual Guid? IDReplaceInv { get; set; }
        [XmlIgnore()]
        public virtual Guid? IDAdjustInv { get; set; }
        [XmlIgnore()]
        public virtual Guid? ID_MIV { get; set; }
        public EInvoice()
        {
            Products = new List<Product>();
        }
        #region Lấy thông tin hóa đơn JSON
        [XmlIgnore()]
        public virtual string Ikey { get; set; }
        [XmlIgnore()]
        public virtual string No { get; set; }
        //Ngày phát hành
        [XmlIgnore()]
        public virtual string IssueDate { get; set; }
        [XmlIgnore()]
        public virtual string CustomerName { get; set; }
        [XmlIgnore()]
        public virtual string CustomerCode { get; set; }
        [XmlIgnore()]
        public virtual int InvoiceStatus { get; set; }
        [XmlIgnore()]
        public virtual bool Recorded { get; set; }
        #endregion
    }
    public class EInvoices
    {
        public List<Inv> Invoices { get; set; }
        public EInvoices()
        {
            Invoices = new List<Inv>();
        }
    }
    public class Inv
    {
        [XmlElement("key")]
        public virtual string Key { get; set; }
        public EInvoice Invoice { get; set; }
        public Inv()
        {
            Invoice = new EInvoice();
        }
    }

    public class AdjustInv
    {
        [XmlElement("key")]
        public virtual string Key { get; set; }
        [XmlIgnore()]
        public virtual string KeyAdjusted { get; set; }
        [XmlIgnore()]
        public virtual string Pattern { get; set; } // Mẫu số hđ
        [XmlIgnore()]
        public virtual string Serial { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual string InvNo { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual string PatternAdjusted { get; set; } // Mẫu số hđ
        [XmlIgnore()]
        public virtual string SerialAdjusted { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual string InvNoAdjusted { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual DateTime? InvDate { get; set; } // số hđ
        public virtual string CusCode { get; set; } //Mã khách hàng
        public virtual string Buyer { get; set; } // Tên người mua
        public virtual string CusName { get; set; } //Tên khách hàng
        public virtual string Email { get; set; } //Tên khách hàng
        public virtual string EmailCC { get; set; } //Tên khách hàng
        public virtual string CusAddress { get; set; }
        public virtual string CusBankName { get; set; }
        public virtual string CusBankNo { get; set; }
        public virtual string CusPhone { get; set; }
        public virtual string CusTaxCode { get; set; }
        public virtual string PaymentMethod { get; set; } //phương thức thanh toán
        public virtual string ArisingDate { get; set; } // Ngày tạo lập đồng thời ngày phát hành
        public virtual decimal? ExchangeRate { get; set; } //Tỷ giá chuyển đổi
        public virtual string CurrencyUnit { get; set; }
        public virtual string Extra { get; set; }
        public virtual int Type { get; set; }
        public List<Product> Products { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual string VATRate { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string AmountInWords { get; set; }
        [XmlIgnore()]
        public virtual int StatusInvoice { get; set; }
        [XmlIgnore()]
        public virtual Guid? ID_MIV { get; set; }
        [XmlIgnore()]
        public virtual Guid? ID_MIVAdjust { get; set; }
        public AdjustInv()
        {
            Products = new List<Product>();
        }
    }
    public class ReplaceInv
    {
        [XmlElement("key")]
        public virtual string Key { get; set; }
        [XmlIgnore()]
        public virtual string KeyReplaced { get; set; }
        [XmlIgnore()]
        public virtual string Pattern { get; set; } // Mẫu số hđ
        [XmlIgnore()]
        public virtual string Serial { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual string InvNo { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual string PatternReplaced { get; set; } // Mẫu số hđ
        [XmlIgnore()]
        public virtual string SerialReplaced { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual string InvNoReplaced { get; set; } // kí hiệu hđ
        [XmlIgnore()]
        public virtual DateTime? InvDate { get; set; } // số hđ
        public virtual string CusCode { get; set; } //Mã khách hàng
        public virtual string Buyer { get; set; } // Tên người mua
        public virtual string CusName { get; set; } //Tên khách hàng
        public virtual string Email { get; set; } //Tên khách hàng
        public virtual string CusAddress { get; set; }
        public virtual string CusBankName { get; set; }
        public virtual string CusBankNo { get; set; }
        public virtual string CusPhone { get; set; }
        public virtual string CusTaxCode { get; set; }
        public virtual string PaymentMethod { get; set; } //phương thức thanh toán
        public virtual string ArisingDate { get; set; } // Ngày tạo lập đồng thời ngày phát hành
        public virtual decimal? ExchangeRate { get; set; } //Tỷ giá chuyển đổi
        public virtual string CurrencyUnit { get; set; }
        public virtual string Extra { get; set; }
        public virtual int Type { get; set; }
        public List<Product> Products { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual string VATRate { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string AmountInWords { get; set; }
        [XmlIgnore()]
        public virtual int StatusInvoice { get; set; }
        public ReplaceInv()
        {
            Products = new List<Product>();
        }
    }
    [XmlRoot("Invoices")]
    public class EInvoicesVNPT
    {
        public virtual string SerialCert { get; set; }
        public Inv_VNPT Inv { get; set; }
    }
    public class Inv_VNPT
    {
        [XmlElement("key")]
        public virtual string Key { get; set; }
        //public EInvoice Invoice { get; set; }
        //public Inv_VNPT()
        //{
        //    Invoice = new EInvoice();
        //}
        public virtual string idInv { get; set; }
        public virtual string hashValue { get; set; }
        public virtual string signValue { get; set; }
        public virtual string pattern { get; set; }
        public virtual string serial { get; set; }
    }
    public class DeleteInvoice_SDS
    {
        public string Ikey { get; set; }
        public string Pattern { get; set; }
        public string Serial { get; set; }
    }
    #region Thông tin khách hàng SDS
    public class Customers
    {
        [XmlElement("Customer")]
        public List<Customer_If> Customer = new List<Customer_If>();
    }

    public class Customer_If
    {
        public string Code { get; set; }
        public string AccountName { get; set; }
        public string Name { get; set; }
        public string Buyer { get; set; }
        public string Address { get; set; }
        public string TaxCode { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankNumber { get; set; }
        public string Email { get; set; }
        public string EmailCC { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        public string ContactPerson { get; set; }
        public string RepresentPerson { get; set; }
        public string CusType { get; set; }
    }
    #endregion
    #region Thông tin sản phẩm dịch vụ SDS
    public class Products
    {
        [XmlElement("Product")]
        public List<Product_If> Product = new List<Product_If>();
    }
    public class Product_If
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Unit { get; set; }
        public string Des { get; set; }
        public string VATRate { get; set; }
    }
    #endregion
}
