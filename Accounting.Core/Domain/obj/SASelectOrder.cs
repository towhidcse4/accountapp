﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class SASelectOrder
    {
        public virtual Guid ID { get; set; }
        public virtual bool CheckColumn { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string Reason { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual string MaterialGoodsName { get; set; }
        public virtual decimal? QuantityRemaining { get; set; }
        public virtual decimal? QuantityOutward { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual SAOrderDetail SaOrderDetail { get; set; }
    }
}
