﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
   public class InvoiceType
    {
        private Guid iD;
        private string invoiceTypeCode;
        private string invoiceTypeName;
        private int invoiceType;
        string _invoiceType;
        private decimal? fromNo;
        private decimal? toNo;
        private bool isActive;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual string InvoiceTypeCode
        {
            get { return invoiceTypeCode; }
            set { invoiceTypeCode = value; }
        }
        public virtual string InvoiceTypeName
        {
            get { return invoiceTypeName; }
            set { invoiceTypeName = value; }
        }
        public virtual int _InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }
        public virtual string _InvoiceTypeView
        {
            get { return _invoiceType; }
            set { _invoiceType = value; }
        }
        public virtual decimal? FromNo
        {
            get { return fromNo; }
            set { fromNo = value; }
        }
        public virtual decimal? ToNo
        {
            get { return toNo; }
            set { toNo = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
    }
}
