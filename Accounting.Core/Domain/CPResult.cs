using System;

namespace Accounting.Core.Domain
{
    public class CPResult
    {
        private Guid _ID;
        private Guid _CPPeriodID;
        private Guid _CPPeriodDetailID;
        private Guid _CostSetID;
        private Guid _MaterialGoodsID;
        private Decimal _Coefficien;
        private Decimal _DirectMaterialAmount;
        private Decimal _DirectLaborAmount;
        private Decimal _GeneralExpensesAmount;
        private Decimal _TotalCostAmount;
        private Decimal _TotalQuantity;
        private Decimal _UnitPrice;
        private CostSet _Costset;
        private string _CostSetCode;
        private string _CostSetName;
        private string _MaterialGoodsCode;
        private string _MaterialGoodsName;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual Guid CPPeriodDetailID
        {
            get { return _CPPeriodDetailID; }
            set { _CPPeriodDetailID = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual MaterialGoods MaterialGoods { get; set; }

        public virtual string MaterialGoodsName
        {
            get { return _MaterialGoodsName; }
            set { _MaterialGoodsName = value; }
        }
        public virtual string MaterialGoodsCode
        {
            get { return _MaterialGoodsCode; }
            set { _MaterialGoodsCode = value; }
        }

        public virtual Decimal Coefficien
        {
            get { return _Coefficien; }
            set { _Coefficien = value; }
        }

        public virtual Decimal DirectMaterialAmount
        {
            get { return _DirectMaterialAmount; }
            set { _DirectMaterialAmount = value; }
        }

        public virtual Decimal DirectLaborAmount
        {
            get { return _DirectLaborAmount; }
            set { _DirectLaborAmount = value; }
        }

        public virtual Decimal GeneralExpensesAmount
        {
            get { return _GeneralExpensesAmount; }
            set { _GeneralExpensesAmount = value; }
        }

        public virtual Decimal TotalCostAmount
        {
            get { return _TotalCostAmount; }
            set { _TotalCostAmount = value; }
        }

        public virtual Decimal TotalQuantity
        {
            get { return _TotalQuantity; }
            set { _TotalQuantity = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual CostSet CostSet
        {
            get { return _Costset; }
            set { _Costset = value; }
        }

        public virtual string CostSetCode
        {
            get { return _CostSetCode; }
            set { _CostSetCode = value; }
        }

        public virtual string CostSetName
        {
            get { return _CostSetName; }
            set { _CostSetName = value; }
        }
    }
}
