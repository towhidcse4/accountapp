﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountingObjectCategory
    {
        public virtual Guid ID { get; set; }
        public virtual string AccountingObjectCategoryCode { get; set; }
        public virtual string AccountingObjectCategoryName { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsSecurity { get; set; }
    }
}
