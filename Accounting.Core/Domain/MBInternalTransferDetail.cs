

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBInternalTransferDetail
    {
        private Guid iD;
        private Guid mBInternalTransferID;
        private string debitAccount;
        private string creditAccount;
        private Guid? fromBankAccountDetailID;
        private Guid? toBankAccountDetailID;
        private Guid? fromBranchID;
        private Guid? toBranchID;
        private string currencyID;
        private decimal exchangeRate;
        private decimal amount;
        private decimal amountOriginal;
        private Guid? budgetItemID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? employeeID;
        private Guid? statisticsCodeID;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private Guid? accountingObjectID;
        private string transferAccount;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool isIrrationalCost;
        private bool isMatch;
        private DateTime matchDate = new DateTime(1970, 1, 1);
        private int orderPriority;
        private string description;

        public virtual bool IsMatchTo { get; set; }
        public virtual DateTime? MatchDateTo { get; set; }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MBInternalTransferID
        {
            get { return mBInternalTransferID; }
            set { mBInternalTransferID = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual Guid? FromBankAccountDetailID
        {
            get { return fromBankAccountDetailID; }
            set { fromBankAccountDetailID = value; }
        }

        public virtual Guid? ToBankAccountDetailID
        {
            get { return toBankAccountDetailID; }
            set { toBankAccountDetailID = value; }
        }

        public virtual Guid? FromBranchID
        {
            get { return fromBranchID; }
            set { fromBranchID = value; }
        }

        public virtual Guid? ToBranchID
        {
            get { return toBranchID; }
            set { toBranchID = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string TransferAccount
        {
            get { return transferAccount; }
            set { transferAccount = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual decimal ExchangeRate { get; set; }

        //public virtual decimal Amount
        //{
        //    get { return AmountOriginal * ExchangeRate; }
        //    set { amount = value; }
        //}

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual string VATDescription
        {
            get;set;
        }
    }
}
