using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FormulaOperandUsing
    {
        private Guid _ID;
        private int _TypeID;
        private string _FormulaOperandUsingName;
        private string _FunctionName;
        private string _Description;
        private string _EnglishDescription;
        private string _Examples;
        private string _ExamplesPatterns;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string FormulaOperandUsingName
        {
            get { return _FormulaOperandUsingName; }
            set { _FormulaOperandUsingName = value; }
        }

        public virtual string FunctionName
        {
            get { return _FunctionName; }
            set { _FunctionName = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string EnglishDescription
        {
            get { return _EnglishDescription; }
            set { _EnglishDescription = value; }
        }

        public virtual string Examples
        {
            get { return _Examples; }
            set { _Examples = value; }
        }

        public virtual string ExamplesPatterns
        {
            get { return _ExamplesPatterns; }
            set { _ExamplesPatterns = value; }
        }


    }
}
