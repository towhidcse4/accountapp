﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CareerGroup
    {

        public virtual Guid ID
        {
            get;
            set;
        }
        public virtual string CareerGroupCode
        {
            get;
            set;
        }
        public virtual string CareerGroupName
        {
            get;
            set;
        }       
    }
}
