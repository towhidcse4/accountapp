using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysUser {
        public virtual Guid userid { get; set; }
        public virtual string username { get; set; }
        public virtual string password { get; set; }
        public virtual int? PasswordFormat { get; set; }
        public virtual string PasswordSalt { get; set; }
        public virtual string Job { get; set; }
        public virtual string FullName { get; set; }
        public virtual string Description { get; set; }
        public virtual bool? IsBranchMaster { get; set; }
        public virtual System.Guid? BranchID { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual string WorkPhone { get; set; }
        public virtual string HomePhone { get; set; }
        public virtual string MobilePhone { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Address { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool? IsOnline { get; set; }
        public virtual bool IsSystem { get; set; }
        public virtual int? AuthenticationType { get; set; }
        public virtual string Country { get; set; }
        public virtual string Province { get; set; }
        public virtual string City { get; set; }
        public virtual DateTime? BirthDay { get; set; }
        public virtual string IDCard { get; set; }

        public virtual IList<SysRole> Roles { get; private set; }
    }
}
