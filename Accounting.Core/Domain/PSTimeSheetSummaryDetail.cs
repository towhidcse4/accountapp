using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSTimeSheetSummaryDetail
    {
        private Guid _ID;
        private Guid _PSTimeSheetSummaryID;
        private Guid? _EmployeeID;
        private Guid? _DepartmentID;
        private string _AccountingObjectName;
        private string _AccountingObjectTitle;
        private Decimal _WorkAllDay;
        private Decimal _WorkHalfADay;
        private Decimal _TotalOverTime;
        private Decimal _Total;
        private int _OrderPriority;

        public virtual string EmployeeCode { get; set; }
        public virtual string DepartmentCode { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsEmployee { get; set; }
        public virtual string EmployeeName { get; set; }
        public virtual string Message { get; set; }


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid PSTimeSheetSummaryID
        {
            get { return _PSTimeSheetSummaryID; }
            set { _PSTimeSheetSummaryID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return _AccountingObjectName; }
            set { _AccountingObjectName = value; }
        }

        public virtual string AccountingObjectTitle
        {
            get { return _AccountingObjectTitle; }
            set { _AccountingObjectTitle = value; }
        }

        public virtual Decimal WorkAllDay
        {
            get { return _WorkAllDay; }
            set { _WorkAllDay = value; }
        }

        public virtual Decimal WorkHalfADay
        {
            get { return _WorkHalfADay; }
            set { _WorkHalfADay = value; }
        }

        public virtual Decimal TotalOverTime
        {
            get { return _TotalOverTime; }
            set { _TotalOverTime = value; }
        }

        public virtual Decimal Total
        {
            get { return _Total; }
            set { _Total = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
