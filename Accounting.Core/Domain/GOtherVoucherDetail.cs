

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid GOtherVoucherID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal ExchangeRate { get; set; }
        decimal _amount;
        public virtual decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public virtual decimal AmountOriginal { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? DebitAccountingObjectID { get; set; }
        public virtual string DebitAccountingObjectName { get; set; }
        public virtual string CreditAccountingObjectName { get; set; }
        public virtual Guid? CreditAccountingObjectID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual bool IsMatch { get; set; }
        public virtual DateTime? MatchDate { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual bool IsIrrationalCost { get; set; }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual decimal CashOutAmountVoucherOriginal { get; set; }
        public virtual decimal CashOutAmountVoucher { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual int RefTypeID { get; set; }
        public virtual Guid? RefExchangeID { get; set; }
    }
}
