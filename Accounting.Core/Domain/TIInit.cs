using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIInit
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _TypeID;
        private int _DeclareType;
        private DateTime? _PostedDate;
        private string _AllocationAwaitAccount;
        private string _ToolsCode;
        private string _ToolsName;
        private Decimal? _Quantity;
        private Decimal _UnitPrice;
        private Decimal _Amount;
        private int? _AllocationTimes;
        private Decimal? _AllocatedAmount;
        private Decimal? _AllocationAmount;
        private Decimal _RemainAmount;
        private Guid? _CostSetID;
        private Guid? _TemplateID;
        private int? _AllocationType;
        private int? _RemainAllocationTimes;
        private int? _InitStatus;
        private Boolean? _IsIrrationalCost;
        private Guid? _MaterialGoodsCategoryID;
        private string _FixedAssetAccessoriesUnit;
        private bool _IsActive;
        private string doiTuongPhanBo;
        private decimal? soLuongPhanBo;
        private decimal? tyLePhanBo;
        private string taiKhoanChiPhi;
        private string khoanMucChiPhi;
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual IList<TIInitDetail> TIInitDetails { get; set; }
        public virtual string Unit { get; set; }
        public virtual DateTime? DecrementDate { get; set; }
        public virtual DateTime? IncrementDate { get; set; }
        public virtual Decimal? AllocatedTimes { get; set; }
        public TIInit()
        {
            TIInitDetails = new List<TIInitDetail>();
        }
        public virtual decimal? TyLePhanBo
        {
            get { return tyLePhanBo; }
            set { tyLePhanBo = value; }
        }
        public virtual decimal? SoLuongPhanBo
        {
            get { return soLuongPhanBo; }
            set { soLuongPhanBo = value; }
        }
        public virtual string DoiTuongPhanBo
        {
            get { return doiTuongPhanBo; }
            set { doiTuongPhanBo = value; }
        }
        public virtual string KhoanMucChiPhi
        {
            get { return khoanMucChiPhi; }
            set { khoanMucChiPhi = value; }
        }
        public virtual string TaiKhoanChiPhi
        {
            get { return taiKhoanChiPhi; }
            set { taiKhoanChiPhi = value; }
        }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }
        public virtual int DeclareType
        {
            get { return _DeclareType; }
            set { _DeclareType = value; }
        }

        public virtual DateTime? PostedDate
        {
            get
            {
                return _PostedDate;
            }
            set { _PostedDate = value; }
        }

        public virtual string AllocationAwaitAccount
        {
            get { return _AllocationAwaitAccount; }
            set { _AllocationAwaitAccount = value; }
        }
        public virtual string FixedAssetAccessoriesUnit
        {
            get { return _FixedAssetAccessoriesUnit; }
            set { _FixedAssetAccessoriesUnit = value; }
        }

        public virtual string ToolsCode
        {
            get { return _ToolsCode; }
            set { _ToolsCode = value; }
        }

        public virtual string ToolsName
        {
            get { return _ToolsName; }
            set { _ToolsName = value; }
        }

        public virtual Decimal? Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual int? AllocationTimes
        {
            get { return _AllocationTimes; }
            set { _AllocationTimes = value; }
        }

        public virtual Decimal? AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }
        public virtual Decimal? AllocationAmount
        {
            get { return _AllocationAmount; }
            set { _AllocationAmount = value; }
        }

        public virtual Decimal RemainAmount
        {
            get { return _RemainAmount; }
            set { _RemainAmount = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual int? AllocationType
        {
            get { return _AllocationType; }
            set { _AllocationType = value; }
        }

        public virtual int? RemainAllocationTimes
        {
            get { return _RemainAllocationTimes; }
            set { _RemainAllocationTimes = value; }
        }

        public virtual int? InitStatus
        {
            get { return _InitStatus; }
            set { _InitStatus = value; }
        }

        public virtual Boolean? IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }
        public virtual Guid? TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }
        public virtual Guid? MaterialGoodsCategoryID
        {
            get { return _MaterialGoodsCategoryID; }
            set { _MaterialGoodsCategoryID = value; }
        }
        public virtual bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
    }
}
