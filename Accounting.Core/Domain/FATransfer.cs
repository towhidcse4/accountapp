﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
   public class FATransfer
    {
        private Guid _id;
        private Guid _branchID;
        private int _typeID;
        private DateTime _date;
        private string _no;
        private string _reason;
        private string _customProperty1;
        private string _customProperty2;
        private string _customProperty3;
        private Guid? _templateID;
        private Boolean _recorded;
        private Boolean _exported;
        private string _transferor;
        private string _reciever;

        public FATransfer()
        {
            FATransferDetails = new List<FATransferDetail>();
            RefVouchers = new List<RefVoucher>();
        }

        public virtual IList<FATransferDetail> FATransferDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public virtual Guid ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual Guid BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual int TypeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public virtual string No
        {
            get { return _no; }
            set { _no = value; }
        }

        public virtual string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _customProperty1; }
            set { _customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _customProperty2; }
            set { _customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _customProperty3; }
            set { _customProperty3 = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }

        public virtual Boolean Recorded
        {
            get { return _recorded; }
            set { _recorded = value; }
        }

        public virtual Boolean Exported
        {
            get { return _exported; }
            set { _exported = value; }
        }

        public virtual string Transferor
        {
            get { return _transferor; }
            set { _transferor = value; }
        }

        public virtual string Reciever
        {
            get { return _reciever; }
            set { _reciever = value; }
        }
    }
}
