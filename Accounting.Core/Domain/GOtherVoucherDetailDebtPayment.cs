using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailDebtPayment
    {
        private Guid _ID;
        private Guid _GOtherVoucherID;
        private string _AccountNumber;
        private Guid? _RefID;
        private DateTime _RefVoucherDate;
        private string _RefVoucherNo;
        private Guid? _AccountingObjectID;
        private string _Reason;
        private Decimal? _RefVoucherExchangeRate;
        private Decimal? _LastExchangeRate;
        private Decimal _RemainingAmountOriginal;
        private Decimal _RemainingAmount;
        private Decimal _RevaluedAmount;
        private Decimal _DifferAmount;
        public virtual DateTime? Date { get; set; }
        public virtual Decimal NewExchangeRate { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid GOtherVoucherID
        {
            get { return _GOtherVoucherID; }
            set { _GOtherVoucherID = value; }
        }

        public virtual string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }

        public virtual Guid? RefID
        {
            get { return _RefID; }
            set { _RefID = value; }
        }

        public virtual DateTime RefVoucherDate
        {
            get { return _RefVoucherDate; }
            set { _RefVoucherDate = value; }
        }

        public virtual string RefVoucherNo
        {
            get { return _RefVoucherNo; }
            set { _RefVoucherNo = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual Decimal? RefVoucherExchangeRate
        {
            get { return _RefVoucherExchangeRate; }
            set { _RefVoucherExchangeRate = value; }
        }

        public virtual Decimal? LastExchangeRate
        {
            get { return _LastExchangeRate; }
            set { _LastExchangeRate = value; }
        }

        public virtual Decimal RemainingAmountOriginal
        {
            get { return _RemainingAmountOriginal; }
            set { _RemainingAmountOriginal = value; }
        }

        public virtual Decimal RemainingAmount
        {
            get { return _RemainingAmount; }
            set { _RemainingAmount = value; }
        }

        public virtual Decimal RevaluedAmount
        {
            get { return _RevaluedAmount; }
            set { _RevaluedAmount = value; }
        }

        public virtual Decimal DifferAmount
        {
            get { return _DifferAmount; }
            set { _DifferAmount = value; }
        }


    }
}
