using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class InterestRate   //t�nh l�i ([B�n h�ng] -> [T�nh l�i])
    {
        public virtual string KhachHang { get; set; }
        public virtual string LoaiTien { get; set; }
        public virtual string NoQuaHan { get; set; }
        public virtual string NoQuaHanQuyDoi { get; set; }
        public virtual string TienPhat { get; set; }
        public virtual string TienPhatQuyDoi { get; set; }
        public virtual string TaiKhoanPhaiThu { get; set; }
        public List<InterestRateDetail> DsInterestRateDetail { get; set; }

        public InterestRate()
        {
            DsInterestRateDetail = new List<InterestRateDetail>();
        }
    }
}