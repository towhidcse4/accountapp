﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CostSetExpense
    {
        public Guid CostSetID { get; set; }
        public string CostSetCode { get; set; }
        public string CostSetName { get; set; }
        public Guid ExpenseItemID { get; set; }
        public string ExpenseItemCode { get; set; }
        public string ExpenseItemName { get; set; }
        public decimal SoDauKy { get; set; }
        public decimal SoPhatSinh { get; set; }
        public decimal LuyKeCuoiKy { get; set; }
    }
    public class CostSetExpense_Period
    {
        public string Period { get; set; }
    }
}
