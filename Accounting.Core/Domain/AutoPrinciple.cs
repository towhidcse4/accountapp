using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AutoPrinciple
    {
        private Guid iD;
        private string autoPrincipleName;
        private int typeID;
        //private string typeName;
        private bool isActive;
        private string debitAccount;
        private string creditAccount;
        private string description;
        private Type _type;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string AutoPrincipleName
        {
            get { return autoPrincipleName; }
            set { autoPrincipleName = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        //public virtual string TypeName
        //{
        //  get { return typeName; }
        //set { typeName = value; }
        //}

        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return Type.TypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

    }
}