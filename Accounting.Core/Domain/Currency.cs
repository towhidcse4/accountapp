using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Currency
    {
        private string iD;
        private string currencyName;
        private bool isActive;
        private decimal? exchangeRate;
        private string cashAccount;
        private string bankAccount;
        private string firstCharater;
        private string separatorCharater;
        private string afterCharater;
        private string firstCharacterDefault;
        private string separatorCharaterDefault;
        private string afterCharaterDefault;
        private bool _status;
       
        public virtual string ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string CurrencyName
        {
            get { return currencyName; }
            set { currencyName = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual string CashAccount
        {
            get { return cashAccount; }
            set { cashAccount = value; }
        }

        public virtual string BankAccount
        {
            get { return bankAccount; }
            set { bankAccount = value; }
        }

        public virtual string FirstCharater
        {
            get { return firstCharater; }
            set { firstCharater = value; }
        }

        public virtual string SeparatorCharater
        {
            get { return separatorCharater; }
            set { separatorCharater = value; }
        }

        public virtual string AfterCharater
        {
            get { return afterCharater; }
            set { afterCharater = value; }
        }

        public virtual string FirstCharacterDefault
        {
            get { return firstCharacterDefault; }
            set { firstCharacterDefault = value; }
        }


        public virtual string SeparatorCharaterDefault
        {
            get { return separatorCharaterDefault; }
            set { separatorCharaterDefault = value; }
        }

        public virtual string AfterCharaterDefault
        {
            get { return afterCharaterDefault; }
            set { afterCharaterDefault = value; }
        }

        public virtual bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

    }
}
