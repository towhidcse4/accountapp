using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Department
    {
        private Guid _ID;
        private Guid? _BranchID;
        private string _DepartmentCode;
        private string _DepartmentName;
        private string _Description;
        private string _CostAccount;
        private string _OrderFixCode;
        private Guid? _ParentID;
        private int _Grade;
        private Guid? _BlockDepartmentID;
        private Decimal _RateAllocation;
        private Boolean _IsActive;
        private bool isParentNode;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual string DepartmentCode
        {
            get { return _DepartmentCode; }
            set { _DepartmentCode = value; }
        }

        public virtual string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string CostAccount
        {
            get { return _CostAccount; }
            set { _CostAccount = value; }
        }

        public virtual string OrderFixCode
        {
            get { return _OrderFixCode; }
            set { _OrderFixCode = value; }
        }

        public virtual Guid? ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }

        public virtual int Grade
        {
            get { return _Grade; }
            set { _Grade = value; }
        }

        public virtual Guid? BlockDepartmentID
        {
            get { return _BlockDepartmentID; }
            set { _BlockDepartmentID = value; }
        }

        public virtual Decimal RateAllocation
        {
            get { return _RateAllocation; }
            set { _RateAllocation = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }

    }
}
