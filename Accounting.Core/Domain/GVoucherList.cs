using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GVoucherList
    {
        private Guid _iD = Guid.NewGuid();
        private Guid _branchID;
        private int _typeID;
        private DateTime _date;
        private string _no;
        private DateTime _fromDate;
        private DateTime _toDate;
        private string _description;
        private decimal _totalAmount;
        private Guid _templateID;
        private bool _exported;

        public virtual Guid ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        public virtual Guid BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        public virtual int TypeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public virtual string No
        {
            get { return _no; }
            set { _no = value; }
        }

        public virtual DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        public virtual Guid TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }

        public virtual bool Exported
        {
            get { return _exported; }
            set { _exported = value; }
        }

        public virtual int? dem { get; set; } //
        public GVoucherList()
        {
            GVoucherListDetails = new List<GVoucherListDetail>();
        }
        public virtual IList<GVoucherListDetail> GVoucherListDetails { get; set; }
    }
    //public class GVoucherList_Count
    //{
    //    public int dem { get; set; }
    //}


}