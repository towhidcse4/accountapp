using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAOrder
    {
        public virtual Guid? TemplateID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal TotalAmountOriginal { get; set; }
        public virtual decimal TotalDiscountAmount { get; set; }
        public virtual decimal TotalDiscountAmountOriginal { get; set; }
        public virtual decimal TotalVATAmount { get; set; }
        public virtual decimal TotalVATAmountOriginal { get; set; }
        private decimal totalPaymentAmount;
        public virtual decimal TotalPaymentAmount
        {
            get { return TotalAmount + TotalVATAmount - TotalDiscountAmount; }
            set { totalPaymentAmount = value; }
        }

        private decimal totalPaymentAmountOriginal;
        public virtual Guid ID { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual DateTime? DeliveDate { get; set; }
        public virtual string DeliveryPlace { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual Guid? SAQuoteID { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual Guid? PaymentClauseID { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual Guid? TranspotMethodID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual bool IsActive { get; set; }

        public SAOrder()
        {
            RefVouchers = new List<RefVoucher>();
            SAOrderDetails = new List<SAOrderDetail>();
        }

        public virtual IList<SAOrderDetail> SAOrderDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public virtual decimal TotalPaymentAmountOriginal
        {
            get { return TotalAmountOriginal + TotalVATAmountOriginal - TotalDiscountAmountOriginal; }
            set { totalPaymentAmountOriginal = value; }
        }
        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (SAOrderDetails.Count > 0)
                {
                    max = SAOrderDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        public virtual decimal? MaxDiscountRate
        {
            get
            {
                decimal? max = null;
                if (SAOrderDetails.Count > 0)
                {
                    max = SAOrderDetails.Max(k => k.DiscountRate);
                }
                return max;
            }
        }
    }
}
