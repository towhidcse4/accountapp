﻿

using System;
using System.Collections.Generic;
using FX.Core;
using Accounting.Core.IService;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBDeposit
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid? branchID;
        private Guid? employeeID;
        private int? accountingObjectType;
        private Guid? invoiceTypeID;
        private bool isMatch;
        private DateTime? matchDate;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? templateID;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private Guid? bankAccountDetailID;
        private string bankName;
        private string reason;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? sAQuoteID;
        private Guid? sAOrderID;
        private Guid? paymentClauseID;
        private Guid? transportMethodID;
        private int? invoiceType;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string invoiceSeries;
        public virtual string InvoiceTemplate { get; set; }// Mẫu số
        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }


        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return IoC.Resolve<ITypeService>().Getbykey(typeID).TypeName;
                }
                catch
                {
                    return "";
                }
            }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual int? AccountingObjectType
        {
            get { return accountingObjectType; }
            set { accountingObjectType = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return invoiceTypeID; }
            set { invoiceTypeID = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }


        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }


        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? SAQuoteID
        {
            get { return sAQuoteID; }
            set { sAQuoteID = value; }
        }

        public virtual Guid? SAOrderID
        {
            get { return sAOrderID; }
            set { sAOrderID = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }

        public virtual int? InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }


        public virtual IList<MBDepositDetail> MBDepositDetails { get; set; }
        public virtual IList<MBDepositDetailTax> MBDepositDetailTaxs { get; set; }
        public virtual IList<MBDepositDetailCustomer> MBDepositDetailCustomers { get; set; }
        public virtual IList<RefVoucherRSInwardOutward> RefVoucherRSInwardOutwards { get; set; }

        public MBDeposit()
        {
            MBDepositDetails = new List<MBDepositDetail>();
            MBDepositDetailTaxs = new List<MBDepositDetailTax>();
            MBDepositDetailCustomers = new List<MBDepositDetailCustomer>();
            RefVoucherRSInwardOutwards = new List<RefVoucherRSInwardOutward>();
        }
        private decimal totalAll;
        public virtual decimal TotalAll
        {
            get
            {
                if (TypeID == 160) return TotalAmount;
                else return totalAll;
            }
            set { totalAll = value; }
        }
        private decimal totalAllOriginal;
        public virtual decimal TotalAllOriginal
        {
            get
            {
                if (TypeID == 160) return TotalAmountOriginal;
                else return totalAllOriginal;
            }
            set { totalAllOriginal = value; }
        }
    }
}
