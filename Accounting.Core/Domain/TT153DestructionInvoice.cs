﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TT153DestructionInvoice
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime _Date;
        private string _No;
        private string _DestructionMethod;
        private string _Reason;
        private string _GetDetectionObject;
        private string _RepresentationInLaw;
        private DateTime? _DestructionDate;
        private int _Status;
        private string _AttachFileName;
        private Byte[] _AttachFileContent;

        public TT153DestructionInvoice()
        {
            TT153DestructionInvoiceDetails = new List<TT153DestructionInvoiceDetail>();
        }

        public virtual IList<TT153DestructionInvoiceDetail> TT153DestructionInvoiceDetails { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string DestructionMethod
        {
            get { return _DestructionMethod; }
            set { _DestructionMethod = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual string GetDetectionObject
        {
            get { return _GetDetectionObject; }
            set { _GetDetectionObject = value; }
        }

        public virtual string RepresentationInLaw
        {
            get { return _RepresentationInLaw; }
            set { _RepresentationInLaw = value; }
        }

        public virtual DateTime? DestructionDate
        {
            get { return _DestructionDate; }
            set { _DestructionDate = value; }
        }

        public virtual int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public virtual string AttachFileName
        {
            get { return _AttachFileName; }
            set { _AttachFileName = value; }
        }

        public virtual Byte[] AttachFileContent
        {
            get { return _AttachFileContent; }
            set { _AttachFileContent = value; }
        }

        public virtual string StatusString
        {
            get
            {
                if (_Status == 0) return "Chưa nộp cho cơ quan thuế";
                if (_Status == 1) return "Đã nộp cho cơ quan thuế";
                return null;
            }
        }

    }
}
