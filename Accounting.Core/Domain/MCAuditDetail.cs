﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCAuditDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid MCAuditID { get; set; }
        public virtual int ValueOfMoney { get; set; }
        public virtual int Quantity { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual int? OrderPriority { get; set; }           
    }
}
