﻿using Accounting.Core.IService;
using FX.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMContract
    {
        private int typeID;
        private decimal amount;
        private decimal amountOriginal;
        private decimal invoiceAmount;
        private decimal receiptAmount;
        private decimal paymentAmount;
        private decimal payableAmount;
        private Guid iD;
        private string code;
        private string name;
        private bool isSecurity;
        private bool isClosed;
        private decimal contractTotalAmount;
        private decimal contractTotalAmountOriginal;
        private decimal contractTotalAmountPayment;
        private decimal contractTotalAmountPaymentOriginal;
        private bool isProject;
        private bool isBillPaid;
        private decimal plantPaymentAmount;
        private int contractState;
        private string contractStateName;
        private decimal amountBeforCancel;
        private decimal openingPaymentAmount;
        private decimal openingRecreiptAmount;
        private bool isWatchForCostPrice;
        private decimal receivingAmount;
        private bool isActive;
        private Guid? contractGroupID;
        private string reason;
        private Guid? departmentID;
        private DateTime? postedDate;
        private string orderID;
        private Guid? projectID;
        private string projectName;
        private Guid? contractEmployeeID;
        private string employeeName;
        private DateTime? closedDate;
        private string closedReason;
        private string description;
        private Guid? branchID;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? costSetID;
        private DateTime? signedDate;
        private DateTime? effectiveDate;
        private string companyName;
        private string signerTitle;
        private string signerName;
        private string companyTel;
        private string companyAddress;
        private string companyFax;
        private string companyBankName;
        private Guid? companyBankAccountDetailID;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectCode;
        private string accountingObjectAddress;
        private string accountingObjectSignerName;
        private string accountingObjectTitle;
        private string accountingObjectTel;
        private string accountingObjectFax;
        private Guid? accountingObjectBankAccountDetailID;
        private string accountingObjectBankName;
        private string departmentName;
        private Department codeDepartment;
        private AccountingObject codeAccountingObject;
        private ContractState codeContractState;
        private EMContract codeProject;
        private AccountingObject codeEmployee;
        private bool _RevenueType;
        private DateTime? _DeliverDate;
        private DateTime? _DueDate;
        private static IEMContractService EMContractService
        {
            get { return IoC.Resolve<IEMContractService>(); }
        }

        private static IAccountingObjectService AccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }

        private static IEMContractDetailPaymentService EMContractDetailPaymentService
        {
            get { return IoC.Resolve<IEMContractDetailPaymentService>(); }
        }

        // hợp đồng mua
        private decimal rateProgress;
        private decimal invoiceAmountNo;
        //hợp đồng bán
        private decimal profitLoss;
        private decimal receivable;
        private decimal spending;
        private decimal expectedcosts;
        public virtual IList<EMContractDetailPayment> EMContractDetailPayments { get; set; }
        public virtual decimal Spending
        {
            get { return spending; }
            set { spending = value; }
        }
        public virtual decimal Receivable
        {
            get { return receivable; }
            set { receivable = value; }

        }
        public virtual decimal ProfitLoss
        {
            get { return profitLoss; }
            set { profitLoss = value; }
        }
        public virtual bool Select { get; set; }
        public virtual AccountingObject CodeAccountingObject
        {
            get { return codeAccountingObject; }
            set { codeAccountingObject = value; }
        }

        public virtual string AccountingObjectCode
        {
            get
            {
                try
                {
                    return (CodeAccountingObject != null && CodeAccountingObject.ID != Guid.Empty) ? CodeAccountingObject.AccountingObjectCode : "";
                }
                catch(Exception ex)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual decimal InvoiceAmountNo
        {
            get { return invoiceAmount; }
            set { invoiceAmount = value; }
        }

        public virtual Department CodeDepartment
        {
            get { return codeDepartment; }
            set { codeDepartment = value; }
        }
        public virtual string DepartmentName
        {
            get
            {
                try
                {
                    return (CodeDepartment != null && CodeDepartment.ID != Guid.Empty) ? CodeDepartment.DepartmentName : "";
                }
                catch(Exception ex)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual decimal RateProgress
                               
        {
            get { return rateProgress; }
            set { rateProgress = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal InvoiceAmount
        {
            get { return invoiceAmount; }
            set { invoiceAmount = value; }
        }

        public virtual decimal ReceiptAmount
        {
            get { return receiptAmount; }
            set { receiptAmount = value; }
        }

        public virtual decimal PaymentAmount
        {
            get { return paymentAmount; }
            set { paymentAmount = value; }
        }

        public virtual decimal PayableAmount
        {
            get { return payableAmount; }
            set { payableAmount = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual bool IsClosed
        {
            get { return isClosed; }
            set { isClosed = value; }
        }

        public virtual decimal ContractTotalAmount
        {
            get { return contractTotalAmount; }
            set { contractTotalAmount = value; }
        }

        public virtual decimal ContractTotalAmountOriginal
        {
            get { return contractTotalAmountOriginal; }
            set { contractTotalAmountOriginal = value; }
        }

        public virtual decimal ContractTotalAmountPayment
        {
            get { return contractTotalAmountPayment; }
            set { contractTotalAmountPayment = value; }
        }

        public virtual decimal ContractTotalAmountPaymentOriginal
        {
            get { return contractTotalAmountPaymentOriginal; }
            set { contractTotalAmountPaymentOriginal = value; }
        }

        public virtual bool IsProject
        {
            get { return isProject; }
            set { isProject = value; }
        }

        public virtual bool IsBillPaid
        {
            get { return isBillPaid; }
            set { isBillPaid = value; }
        }

        public virtual decimal PlantPaymentAmount
        {
            get { return plantPaymentAmount; }
            set { plantPaymentAmount = value; }
        }

        public virtual int ContractState
        {
            get { return contractState; }
            set { contractState = value; }
        }

        public virtual ContractState CodeContractState
        {
            get { return codeContractState; }
            set { codeContractState = value; }
        }

        public virtual string ContractStateName
        {
            get
            {
                try
                {
                    return (CodeContractState != null && CodeContractState.ID != 0) ? CodeContractState.ContractStateName : "";
                }
                catch(Exception ex)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual decimal AmountBeforCancel
        {
            get { return amountBeforCancel; }
            set { amountBeforCancel = value; }
        }

        public virtual decimal OpeningPaymentAmount
        {
            get { return openingPaymentAmount; }
            set { openingPaymentAmount = value; }
        }

        public virtual decimal OpeningRecreiptAmount
        {
            get { return openingRecreiptAmount; }
            set { openingRecreiptAmount = value; }
        }

        public virtual bool IsWatchForCostPrice
        {
            get { return isWatchForCostPrice; }
            set { isWatchForCostPrice = value; }
        }

        public virtual decimal ReceivingAmount
        {
            get { return receivingAmount; }
            set { receivingAmount = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual Guid? ContractGroupID
        {
            get { return contractGroupID; }
            set { contractGroupID = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual DateTime? PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }

        public virtual Guid? ProjectID
        {
            get { return projectID; }
            set { projectID = value; }
        }

        public virtual string ProjectName
        {
            get
            {
                return projectName;
            }
            set { projectName = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return contractEmployeeID; }
            set { contractEmployeeID = value; }
        }

        public virtual string EmployeeName
        {
            get
            {
                return employeeName;
            }
            set { employeeName = value; }
        }

        public virtual DateTime? ClosedDate
        {
            get { return closedDate; }
            set { closedDate = value; }
        }

        public virtual string ClosedReason
        {
            get { return closedReason; }
            set { closedReason = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual DateTime? SignedDate
        {
            get { return signedDate; }
            set { signedDate = value; }
        }

        public virtual DateTime? EffectiveDate
        {
            get { return effectiveDate; }
            set { effectiveDate = value; }
        }
        public virtual DateTime? InvoiceDate
        {
            get;
            set;
        }

        public virtual string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public virtual string SignerTitle
        {
            get { return signerTitle; }
            set { signerTitle = value; }
        }

        public virtual string SignerName
        {
            get { return signerName; }
            set { signerName = value; }
        }

        public virtual string CompanyTel
        {
            get { return companyTel; }
            set { companyTel = value; }
        }

        public virtual string CompanyAddress
        {
            get { return companyAddress; }
            set { companyAddress = value; }
        }

        public virtual string CompanyFax
        {
            get { return companyFax; }
            set { companyFax = value; }
        }

        public virtual string CompanyBankName
        {
            get { return companyBankName; }
            set { companyBankName = value; }
        }

        public virtual Guid? CompanyBankAccountDetailID
        {
            get { return companyBankAccountDetailID; }
            set { companyBankAccountDetailID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string AccountingObjectSignerName
        {
            get { return accountingObjectSignerName; }
            set { accountingObjectSignerName = value; }
        }

        public virtual string AccountingObjectTitle
        {
            get { return accountingObjectTitle; }
            set { accountingObjectTitle = value; }
        }

        public virtual string AccountingObjectTel
        {
            get { return accountingObjectTel; }
            set { accountingObjectTel = value; }
        }

        public virtual string AccountingObjectFax
        {
            get { return accountingObjectFax; }
            set { accountingObjectFax = value; }
        }

        public virtual Guid? AccountingObjectBankAccountDetailID
        {
            get { return accountingObjectBankAccountDetailID; }
            set { accountingObjectBankAccountDetailID = value; }
        }

        public virtual string AccountingObjectBankName
        {
            get { return accountingObjectBankName; }
            set { accountingObjectBankName = value; }
        }

        public virtual bool RevenueType
        {
            get { return _RevenueType; }
            set { _RevenueType = value; }
        }

        public virtual DateTime? DeliverDate
        {
            get
            {
                return _DeliverDate;
            }
            set
            {
                _DeliverDate = value;
            }
        }

        public virtual DateTime? DueDate
        {
            get
            {
                return _DueDate;
            }
            set { _DueDate = value; }
        }

        public virtual decimal ExpectedCosts
        {
            get
            {
                //return EMContractDetailPaymentService != null ? EMContractDetailPaymentService.TotalDetailPaymentByContractID(ID) : 0;
                return EMContractDetailPayments.Count > 0 ? EMContractDetailPayments.Sum(x=>x.AmountOriginal) : 0;
            }
            set { expectedcosts = value; }
        }
        public EMContract()
        {
            EMContractDetailPayments = new List<EMContractDetailPayment>();
            EmployeeName = EmployeeID != null ? AccountingObjectService.Getbykey((Guid)EmployeeID).AccountingObjectName : "";
            projectName = ProjectID != null ? EMContractService.Getbykey((Guid)ProjectID).Name : "";
        }
    }
}
