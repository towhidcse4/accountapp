using System;
using System.Collections.Generic;
namespace Accounting.Core.Domain
{
    [Serializable]
    public class RSTransfer
    {
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalIWAmount;
        private decimal totalIWAmountOriginal;
        private int typeID;
        private string typeIDViewNameCode;
        private DateTime postedDate;
        private DateTime date;
        private string no;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid? branchID;
        private Guid? accountingObjectID;
        private Guid? transportMethodID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string reason;
        private string currencyID;
        private decimal? exchangeRate;
        private string iWResponsitoryKeeper;
        private string oWResponsitoryKeeper;
        private Guid? invoiceTypeID;
        private string invSeries;
        private string contractNo;
        private string transport;
        private string mobilizationOrderNo;
        private DateTime? mobilizationOrderDate;
        private string mobilizationOrderOf;
        private string mobilizationOrderFor;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? templateID;
        private DateTime? refDateTime;
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public RSTransfer()
        {
            RSTransferDetails = new List<RSTransferDetail>();
            ExchangeRate = 1;
            RefVouchers = new List<RefVoucher>();
        }
        public virtual IList<RSTransferDetail> RSTransferDetails { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalIWAmount
        {
            get { return totalIWAmount; }
            set { totalIWAmount = value; }
        }

        public virtual decimal TotalIWAmountOriginal
        {
            get { return totalIWAmountOriginal; }
            set { totalIWAmountOriginal = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual string IWResponsitoryKeeper
        {
            get { return iWResponsitoryKeeper; }
            set { iWResponsitoryKeeper = value; }
        }

        public virtual string OWResponsitoryKeeper
        {
            get { return oWResponsitoryKeeper; }
            set { oWResponsitoryKeeper = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return invoiceTypeID; }
            set { invoiceTypeID = value; }
        }

        public virtual string InvSeries
        {
            get { return invSeries; }
            set { invSeries = value; }
        }
        public virtual string InvTemplate
        {
            get;
            set;
        }

        public virtual string InvNo
        {
            get;
            set;
        }

        public virtual string ContractNo
        {
            get { return contractNo; }
            set { contractNo = value; }
        }
        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }

        public virtual string Transport
        {
            get { return transport; }
            set { transport = value; }
        }

        public virtual string MobilizationOrderNo
        {
            get { return mobilizationOrderNo; }
            set { mobilizationOrderNo = value; }
        }

        public virtual DateTime? MobilizationOrderDate
        {
            get { return mobilizationOrderDate; }
            set { mobilizationOrderDate = value; }
        }

        public virtual string MobilizationOrderOf
        {
            get { return mobilizationOrderOf; }
            set { mobilizationOrderOf = value; }
        }

        public virtual string MobilizationOrderFor
        {
            get { return mobilizationOrderFor; }
            set { mobilizationOrderFor = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        public virtual string TypeIDViewNameCode
        {
            get { return typeIDViewNameCode; }
            set { typeIDViewNameCode = value; }
        }
        public virtual DateTime? RefDateTime
        {
            get { return refDateTime; }
            set { refDateTime = value; }
        }
    }
}
