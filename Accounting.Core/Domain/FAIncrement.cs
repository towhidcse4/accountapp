using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAIncrement
    {
        private int typeID;
        private DateTime postedDate;
        private DateTime date;
        private string no;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid? branchID;
        private DateTime? dueDate;
        private string creditCardNumber;
        private Guid? bankAccountDetailID;
        private string bankName;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string identificationNo;
        private DateTime? issueDate;
        private string issueBy;
        private Guid? accountingObjectBankAccount;
        private string accountingObjectBankName;
        private string reason;
        private string numberAttach;
        private bool? isImportPurchase;
        private bool? isSpecialConsumeTax;
        private decimal? totalAmount;
        private decimal? totalAmountOriginal;
        private decimal? totalVATAmount;
        private decimal? totalVATAmountOriginal;
        private decimal? totalDiscountAmount;
        private decimal? totalDiscountAmountOriginal;
        private decimal? totalFreightAmount;
        private decimal? totalFreightAmountOriginal;
        private decimal? totalOrgPrice;
        private decimal? totalOrgPriceOriginal;
        private bool isMatch;
        private DateTime? matchDate;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? transportMethodID;
        private Guid? paymentClauseID;
        private bool? isVATPaid;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private Guid? templateID;
        private Guid? employeeID;
        private string mcontactName;
        private string taxCode;
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string TypeCT { get; set; }
        public virtual decimal TotalAmountFromVoucher { get; set; }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual string CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string IdentificationNo
        {
            get { return identificationNo; }
            set { identificationNo = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }

        public virtual string IssueBy
        {
            get { return issueBy; }
            set { issueBy = value; }
        }

        public virtual Guid? AccountingObjectBankAccount
        {
            get { return accountingObjectBankAccount; }
            set { accountingObjectBankAccount = value; }
        }

        public virtual string AccountingObjectBankName
        {
            get { return accountingObjectBankName; }
            set { accountingObjectBankName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string NumberAttach
        {
            get { return numberAttach; }
            set { numberAttach = value; }
        }

        public virtual bool? IsImportPurchase
        {
            get { return isImportPurchase; }
            set { isImportPurchase = value; }
        }

        public virtual bool? IsSpecialConsumeTax
        {
            get { return isSpecialConsumeTax; }
            set { isSpecialConsumeTax = value; }
        }

        public virtual decimal? TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal? TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal? TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal? TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual decimal? TotalDiscountAmount
        {
            get { return totalDiscountAmount; }
            set { totalDiscountAmount = value; }
        }

        public virtual decimal? TotalDiscountAmountOriginal
        {
            get { return totalDiscountAmountOriginal; }
            set { totalDiscountAmountOriginal = value; }
        }

        public virtual decimal? TotalFreightAmount
        {
            get { return totalFreightAmount; }
            set { totalFreightAmount = value; }
        }

        public virtual decimal? TotalFreightAmountOriginal
        {
            get { return totalFreightAmountOriginal; }
            set { totalFreightAmountOriginal = value; }
        }

        public virtual decimal? TotalOrgPrice
        {
            get { return totalOrgPrice; }
            set { totalOrgPrice = value; }
        }

        public virtual decimal? TotalOrgPriceOriginal
        {
            get { return totalOrgPriceOriginal; }
            set { totalOrgPriceOriginal = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual bool? IsVATPaid
        {
            get { return isVATPaid; }
            set { isVATPaid = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }
        public virtual string MContactName
        {
            get { return mcontactName; }
            set { mcontactName = value; }
        }
        public virtual string Receiver
        {
            get { return mcontactName; }
        }
        public virtual string TaxCode
        {
            get { return taxCode; }
            set { taxCode = value; }
        }

        private decimal? _totalAll;
        private decimal? _totalAllOriginal;
        private Type _type;
        public FAIncrement()
        {
            FAIncrementDetails = new List<FAIncrementDetail>();
            PPInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
            TotalAll = 0;
            TotalAllOriginal = 0;
            TotalAmount = 0;
            TotalAmountOriginal = 0;
            TotalDiscountAmount = 0;
            TotalDiscountAmountOriginal = 0;
            TotalFreightAmount = 0;
            TotalFreightAmountOriginal = 0;
            TotalImportTaxAmount = 0;
            TotalImportTaxAmountOriginal = 0;
            TotalOrgPrice = 0;
            TotalOrgPriceOriginal = 0;
            TotalSpecialConsumeTaxAmount = 0;
            TotalSpecialConsumeTaxAmountOriginal = 0;
            TotalVATAmount = 0;
            TotalVATAmountOriginal = 0;
            ExchangeRate = 1;
            RefVouchers = new List<RefVoucher>();
        }
        public virtual IList<FAIncrementDetail> FAIncrementDetails { get; set; }
        public virtual IList<PPInvoiceDetailCost> PPInvoiceDetailCosts { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }

        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return Type.TypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual decimal? TotalAll
        {
            get { return TotalAmount - TotalDiscountAmount + (!(isImportPurchase ?? false) ? TotalVATAmount : 0); }
            set { _totalAll = value; }
        }

        public virtual decimal? TotalAllOriginal
        {
            get { return TotalAmountOriginal - TotalDiscountAmountOriginal + (!(isImportPurchase??false) ? TotalVATAmountOriginal : 0); }
            set { _totalAllOriginal = value; }
        }

        public virtual decimal? TotalImportTaxAmountOriginal
        {
            get { return _totalImportTaxAmountOriginal; }
            set { _totalImportTaxAmountOriginal = value; }
        }

        public virtual decimal? TotalImportTaxAmount
        {
            get { return _totalImportTaxAmount; }
            set { _totalImportTaxAmount = value; }
        }

        public virtual decimal? TotalSpecialConsumeTaxAmountOriginal
        {
            get { return _totalSpecialConsumeTaxAmountOriginal; }
            set { _totalSpecialConsumeTaxAmountOriginal = value; }
        }

        public virtual decimal? TotalSpecialConsumeTaxAmount
        {
            get { return _totalSpecialConsumeTaxAmount; }
            set { _totalSpecialConsumeTaxAmount = value; }
        }

        private decimal? _totalImportTaxAmountOriginal;
        private decimal? _totalImportTaxAmount;
        private decimal? _totalSpecialConsumeTaxAmountOriginal;
        private decimal? _totalSpecialConsumeTaxAmount;
    }
}
