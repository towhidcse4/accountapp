﻿using System;


namespace Accounting.Core.Domain
{
    public class Guide
    {
        public virtual string Title { get; set; }
        public virtual string Filedata { get; set; }
    }
}
