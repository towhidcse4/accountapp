using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SystemOption
    {
        private int _ID;
        private string _Code;
        private string _Name;
        private int _Type;
        private string _Data;
        private string _DefaultData;
        private string _Note;
        private Boolean _IsSecurity;


        public virtual int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public virtual string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public virtual string DefaultData
        {
            get { return _DefaultData; }
            set { _DefaultData = value; }
        }

        public virtual string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        public virtual Boolean IsSecurity
        {
            get { return _IsSecurity; }
            set { _IsSecurity = value; }
        }


    }
}
