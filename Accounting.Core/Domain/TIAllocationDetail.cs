using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public partial class TIAllocationDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid TIAllocationID { get; set; }
        public virtual Guid? ToolsID { get; set; }
        public virtual string ToolsName { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal OWAmount { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? TIIncrementID { get; set; }
        public virtual int? ConfrontTypeID { get; set; }
        public virtual bool? IsIrrationalCost { get; set; }
        
        public virtual decimal RemainingAmount { get; set; }
        public virtual decimal AllocationAmount { get; set; }
        public virtual decimal TotalAllocationAmount { get; set; }
        public virtual Guid? MaterialGoodsCategoryID { get; set; }
        public virtual int OrderPriority { get; set; }
    }
}
