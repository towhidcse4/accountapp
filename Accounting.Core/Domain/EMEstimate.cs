﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMEstimate
    {
        private Guid iD;
        private Guid branchID;
        private int estimateBudgetYear;
        private int type;
        private Guid? departmentID;
        private string departmentView;
        private Guid budgetItemID;
        private string budgetItemCode;
        private string budgetItemName;
        private Decimal sumAmount;
        private Decimal amountMonth1;
        private Decimal amountMonth2;
        private Decimal amountMonth3;
        private Decimal amountMonth4;
        private Decimal amountMonth5;
        private Decimal amountMonth6;
        private Decimal amountMonth7;
        private Decimal amountMonth8;
        private Decimal amountMonth9;
        private Decimal amountMonth10;
        private Decimal amountMonth11;
        private Decimal amountMonth12;
        private Decimal warningLevel;
        private Decimal warningLevelPercent;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }
        public virtual int EstimateBudgetYear
        {
            get { return estimateBudgetYear; }
            set { estimateBudgetYear = value; }
        }
        public virtual int Type
        {
            get { return type; }
            set { type = value; }
        }
        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }
        public virtual string DepartmentView
        {
            get { return departmentView; }
            set { departmentView = value; }
        }
        public virtual Guid BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }
        public virtual string BudgetItemCode
        {
            get { return budgetItemCode; }
            set { budgetItemCode = value; }
        }
        public virtual string BudgetItemName
        {
            get { return budgetItemName; }
            set { budgetItemName = value; }
        }
        public virtual Decimal SumAmount
        {
            get { return sumAmount; }
            set { sumAmount = value; }
        }

        public virtual Decimal AmountMonth1
        {
            get { return amountMonth1; }
            set { amountMonth1 = value; }
        }
        public virtual Decimal AmountMonth2
        {
            get { return amountMonth2; }
            set { amountMonth2 = value; }
        }
        public virtual Decimal AmountMonth3
        {
            get { return amountMonth3; }
            set { amountMonth3 = value; }
        }
        public virtual Decimal AmountMonth4
        {
            get { return amountMonth4; }
            set { amountMonth4 = value; }
        }
        public virtual Decimal AmountMonth5
        {
            get { return amountMonth5; }
            set { amountMonth5 = value; }
        }
        public virtual Decimal AmountMonth6
        {
            get { return amountMonth6; }
            set { amountMonth6 = value; }
        }
        public virtual Decimal AmountMonth7
        {
            get { return amountMonth7; }
            set { amountMonth7 = value; }
        }
        public virtual Decimal AmountMonth8
        {
            get { return amountMonth8; }
            set { amountMonth8 = value; }
        }
        public virtual Decimal AmountMonth9
        {
            get { return amountMonth9; }
            set { amountMonth9 = value; }
        }
        public virtual Decimal AmountMonth10
        {
            get { return amountMonth10; }
            set { amountMonth10 = value; }
        }
        public virtual Decimal AmountMonth11
        {
            get { return amountMonth11; }
            set { amountMonth11 = value; }
        }
        public virtual Decimal AmountMonth12
        {
            get { return amountMonth12; }
            set { amountMonth12 = value; }
        }
        public virtual Decimal WarningLevel
        {
            get { return warningLevel; }
            set { warningLevel = value; }
        }
        public virtual Decimal WarningLevelPercent
        {
            get { return warningLevelPercent; }
            set { warningLevelPercent = value; }
        }

    }
}
