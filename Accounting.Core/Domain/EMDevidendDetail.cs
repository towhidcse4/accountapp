using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMDevidendDetail
    {
        private Guid _ID;
        private Guid _EMDevidendID;
        private Guid _EMShareHolderID;
        private string _EMShareHolderName;
        private Decimal _HoldQuantiy;
        private Decimal _AmountPayable;
        private Decimal _AmountPaid;
        private Decimal _AmountRemainning;

        
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid EMDevidendID
        {
            get { return _EMDevidendID; }
            set { _EMDevidendID = value; }
        }

        public virtual Guid EMShareHolderID
        {
            get { return _EMShareHolderID; }
            set { _EMShareHolderID = value; }
        }

        public virtual string EMShareHolderName
        {
            get { return _EMShareHolderName; }
            set { _EMShareHolderName = value; }
        }

        public virtual Decimal HoldQuantiy
        {
            get { return _HoldQuantiy; }
            set { _HoldQuantiy = value; }
        }

        public virtual Decimal AmountPayable
        {
            get { return _AmountPayable; }
            set { _AmountPayable = value; }
        }

        public virtual Decimal AmountPaid
        {
            get { return _AmountPaid; }
            set { _AmountPaid = value; }
        }

        public virtual Decimal AmountRemainning
        {
            get { return _AmountRemainning; }
            set { _AmountRemainning = value; }
        }


    }
}
