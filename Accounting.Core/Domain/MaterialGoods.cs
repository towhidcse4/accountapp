using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialGoods
    {
        private Guid iD;
        private Guid? materialGoodsCategoryID;
        private string materialGoodsCategoryIdNameView;
        private string materialGoodsCode;
        private string materialGoodsName;

        //private string materialGoodsCategoryIDVIEW;
        //private string sltonView;
        //private string gttonView;

        private int materialGoodsType;
        private string materialGoodsTypeView;
        private int materialToolType;
        private string unit;
        private string convertUnit;
        private decimal quantity;//
        private decimal slchuyendoi;//
        private decimal dgchuyendoi;



        private decimal? convertRate;
        private decimal? purchasePrice;
        //private;//
        private decimal? salePrice;
        private decimal? salePrice2;
        public virtual int AllocationType { get; set; }
        public virtual string AllocationAwaitAccount { get; set; }


        public virtual decimal? SalePrice2
        {
            get { return salePrice2; }
            set { salePrice2 = value; }
        }

        public virtual decimal? SalePrice3
        {
            get { return salePrice3; }
            set { salePrice3 = value; }
        }

        public virtual decimal SalePriceAfterTax
        {
            get { return salePriceAfterTax; }
            set { salePriceAfterTax = value; }
        }

        public virtual decimal SalePriceAfterTax2
        {
            get { return salePriceAfterTax2; }
            set { salePriceAfterTax2 = value; }
        }

        public virtual decimal SalePriceAfterTax3
        {
            get { return salePriceAfterTax3; }
            set { salePriceAfterTax3 = value; }
        }

        public virtual bool IsSalePriceAfterTax
        {
            get { return isSalePriceAfterTax; }
            set { isSalePriceAfterTax = value; }
        }


        private decimal? salePrice3;
        private decimal? fixedSalePrice;
        private decimal salePriceAfterTax;
        private decimal salePriceAfterTax2;
        private decimal salePriceAfterTax3;
        private bool isSalePriceAfterTax;
        private Guid? repositoryID;
        private string reponsitoryAccount;
        private string expenseAccount;
        private string revenueAccount;
        private decimal? minimumStock;
        private Guid? accountingObjectID;
        private decimal? taxRate;
        private int? systemMaterialGoodsType;
        private string saleDescription;
        private string purchaseDescription;
        private string itemSource;
        private Guid? materialGoodsGSTID;
        private decimal? saleDiscountRate;
        private decimal? purchaseDiscountRate;
        private bool isSaleDiscountPolicy;
        private string guarantyPeriod;
        private int? costMethod;
        private bool isActive;
        private bool isSecurity;
        private bool printMetarial;
        public MaterialGoods()
        {
            MaterialGoodsAssemblys = new List<MaterialGoodsAssembly>();
            SaleDiscountPolicys = new List<SaleDiscountPolicy>();
            //RSInwardOutwardDetails = new List<RSInwardOutwardDetail>();
            SalePrice = 0;
            SalePrice2 = 0;
            SalePrice3 = 0;
        }
        public virtual IList<MaterialGoodsAssembly> MaterialGoodsAssemblys { get; set; }
        public virtual IList<SaleDiscountPolicy> SaleDiscountPolicys { get; set; }
        //public virtual List<RSInwardOutwardDetail> RSInwardOutwardDetails { get; set; }
        public virtual Repository Repository { get; set; }

        public MaterialGoods(int materialGoodsType)
        {
            this.materialGoodsType = materialGoodsType;
        }

        public virtual bool Status { get; set; }
        public virtual Guid? CareerGroupID { get; set; }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }


        public virtual Guid? MaterialGoodsCategoryID
        {
            get { return materialGoodsCategoryID; }
            set { materialGoodsCategoryID = value; }
        }


        public virtual string MaterialGoodsCode
        {
            get { return materialGoodsCode; }
            set { materialGoodsCode = value; }
        }


        public virtual string MaterialGoodsName
        {
            get { return materialGoodsName; }
            set { materialGoodsName = value; }
        }

        //public virtual string MaterialGoodsCategoryIDVIEW
        //{
        //    get { return materialGoodsCategoryIDVIEW; }
        //    set { materialGoodsCategoryIDVIEW = value; }
        //}

        //public virtual string SltonView
        //{
        //    get { return sltonView; }
        //    set { sltonView = value; }
        //}

        //public virtual string GttonView
        //{
        //    get { return gttonView; }
        //    set { gttonView = value; }
        //}

        public virtual int MaterialGoodsType
        {
            get { return materialGoodsType; }
            set { materialGoodsType = value; }
        }
        public virtual string MaterialGoodsTypeView
        {
            get { return materialGoodsTypeView; }
            set { materialGoodsTypeView = value; }
        }


        public virtual int MaterialToolType
        {
            get { return materialToolType; }
            set { materialToolType = value; }
        }


        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }


        public virtual string ConvertUnit
        {
            get { return convertUnit; }
            set { convertUnit = value; }
        }


        public virtual decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual decimal Slchuyendoi
        {
            get { return slchuyendoi; }
            set { slchuyendoi = value; }
        }

        public virtual decimal Dgchuyendoi
        {
            get { return dgchuyendoi; }
            set { dgchuyendoi = value; }
        }
        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }


        public virtual decimal? PurchasePrice
        {
            get { return purchasePrice; }
            set { purchasePrice = value; }
        }

        public virtual decimal? SalePrice
        {
            get { return salePrice; }
            set { salePrice = value; }
        }

        public virtual decimal? FixedSalePrice
        {
            get { return fixedSalePrice; }
            set { fixedSalePrice = value; }
        }


        public virtual Guid? RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }


        public virtual string ReponsitoryAccount
        {
            get { return reponsitoryAccount; }
            set { reponsitoryAccount = value; }
        }


        public virtual string ExpenseAccount
        {
            get { return expenseAccount; }
            set { expenseAccount = value; }
        }


        public virtual string RevenueAccount
        {
            get { return revenueAccount; }
            set { revenueAccount = value; }
        }


        public virtual decimal? MinimumStock
        {
            get { return minimumStock; }
            set { minimumStock = value; }
        }


        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }


        public virtual decimal? TaxRate
        {
            get { return taxRate; }
            set { taxRate = value; }
        }


        public virtual int? SystemMaterialGoodsType
        {
            get { return systemMaterialGoodsType; }
            set { systemMaterialGoodsType = value; }
        }


        public virtual string SaleDescription
        {
            get { return saleDescription; }
            set { saleDescription = value; }
        }


        public virtual string PurchaseDescription
        {
            get { return purchaseDescription; }
            set { purchaseDescription = value; }
        }


        public virtual string ItemSource
        {
            get { return itemSource; }
            set { itemSource = value; }
        }


        public virtual Guid? MaterialGoodsGSTID
        {
            get { return materialGoodsGSTID; }
            set { materialGoodsGSTID = value; }
        }


        public virtual decimal? SaleDiscountRate
        {
            get { return saleDiscountRate; }
            set { saleDiscountRate = value; }
        }


        public virtual decimal? PurchaseDiscountRate
        {
            get { return purchaseDiscountRate; }
            set { purchaseDiscountRate = value; }
        }


        public virtual bool IsSaleDiscountPolicy
        {
            get { return isSaleDiscountPolicy; }
            set { isSaleDiscountPolicy = value; }
        }


        public virtual string GuarantyPeriod
        {
            get { return guarantyPeriod; }
            set { guarantyPeriod = value; }
        }


        public virtual int? CostMethod
        {
            get { return costMethod; }
            set { costMethod = value; }
        }


        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }


        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }


        public virtual bool PrintMetarial
        {
            get { return printMetarial; }
            set { printMetarial = value; }
        }

        public virtual decimal? LastPurchasePriceAfterTax { get; set; }
        public virtual string WarrantyTime { get; set; }
        public virtual MaterialGoodsCategory MaterialGoosCategory { get; set; }
        public virtual string MaterialGoodsCategoryCode
        {
            get
            {
                if (MaterialGoosCategory == null) return "";
                try
                {
                    return MaterialGoosCategory.MaterialGoodsCategoryCode;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }

        //them name loai ccdc
        public virtual string MaterialGoodsCategoryName
        {
            get
            {
                if (MaterialGoosCategory == null) return "";
                try
                {
                    return MaterialGoosCategory.MaterialGoodsCategoryName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
        //end


        public virtual string RepositoryCode
        {
            get
            {
                if (Repository == null) return "";
                try
                {
                    return Repository.RepositoryCode;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
        private Decimal _Quantity;
        private Decimal _UnitPrice;
        private Decimal _Amount;
        private int _AllocationTimes;
        private decimal _AllocatedAmount;
        private string _AllocationAccount;
        private Guid? _CostSetID;

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual int AllocationTimes
        {
            get { return _AllocationTimes; }
            set { _AllocationTimes = value; }
        }

        public virtual decimal AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual string AllocationAccount
        {
            get { return _AllocationAccount; }
            set { _AllocationAccount = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }
        public virtual CPProductQuantum CPProductQuantum { get; set; }

    }
}
