using System;

namespace Accounting.Core.Domain
{
    public class TM01TAINDetail
    {
        private Guid _ID;
        private Guid _TM01TAINID;
        private int _Type;
        private Guid _MaterialGoodsResourceTaxGroupID;
        private string _MaterialGoodsResourceTaxGroupName;
        private string _Unit;
        private Decimal _Quantity;
        private Decimal _UnitPrice;
        private Decimal _TaxRate;
        private Decimal _ResourceTaxTaxAmountUnit;
        private Decimal _ResourceTaxAmountIncurration;
        private Decimal _ResourceTaxAmountDeduction;
        private Decimal _ResourceTaxAmount;
        private int? _OrderPriority;
        public virtual int? STT { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM01TAINID
        {
            get { return _TM01TAINID; }
            set { _TM01TAINID = value; }
        }

        public virtual int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public virtual Guid MaterialGoodsResourceTaxGroupID
        {
            get { return _MaterialGoodsResourceTaxGroupID; }
            set { _MaterialGoodsResourceTaxGroupID = value; }
        }

        public virtual string MaterialGoodsResourceTaxGroupName
        {
            get { return _MaterialGoodsResourceTaxGroupName; }
            set { _MaterialGoodsResourceTaxGroupName = value; }
        }

        public virtual string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal TaxRate
        {
            get { return _TaxRate; }
            set { _TaxRate = value; }
        }

        public virtual Decimal ResourceTaxTaxAmountUnit
        {
            get { return _ResourceTaxTaxAmountUnit; }
            set { _ResourceTaxTaxAmountUnit = value; }
        }

        public virtual Decimal ResourceTaxAmountIncurration
        {
            get { return _ResourceTaxAmountIncurration; }
            set { _ResourceTaxAmountIncurration = value; }
        }

        public virtual Decimal ResourceTaxAmountDeduction
        {
            get { return _ResourceTaxAmountDeduction; }
            set { _ResourceTaxAmountDeduction = value; }
        }

        public virtual Decimal ResourceTaxAmount
        {
            get { return _ResourceTaxAmount; }
            set { _ResourceTaxAmount = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
