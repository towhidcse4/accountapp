using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TIAllocationAllocated
    {
        private Guid _ID;
        private Guid _TIAllocationID;
        private Guid _ToolsID;
        private Guid? _ObjectID;
        private string _AccountNumber;
        private Decimal _Rate;
        private Decimal _TotalAllocationAmount;
        private Decimal _TotalAllocationAmountOriginal;
        private Decimal _AllocationAmount;
        private Decimal _AllocationAmountOriginal;
        private Guid? _ExpenseItemID;
        private Guid? _CostSetID;
        private int _OrderPriority;
        public virtual string ToolsName { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TIAllocationID
        {
            get { return _TIAllocationID; }
            set { _TIAllocationID = value; }
        }

        public virtual Guid ToolsID
        {
            get { return _ToolsID; }
            set { _ToolsID = value; }
        }

        public virtual Guid? ObjectID
        {
            get { return _ObjectID; }
            set { _ObjectID = value; }
        }

        public virtual string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }

        public virtual Decimal Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }

        public virtual Decimal TotalAllocationAmount
        {
            get { return _TotalAllocationAmount; }
            set { _TotalAllocationAmount = value; }
        }

        public virtual Decimal TotalAllocationAmountOriginal
        {
            get { return _TotalAllocationAmountOriginal; }
            set { _TotalAllocationAmountOriginal = value; }
        }

        public virtual Decimal AllocationAmount
        {
            get { return _AllocationAmount; }
            set { _AllocationAmount = value; }
        }

        public virtual Decimal AllocationAmountOriginal
        {
            get { return _AllocationAmountOriginal; }
            set { _AllocationAmountOriginal = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
