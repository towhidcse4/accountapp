﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ViewEInvoice
    {
        [XmlIgnore()]
        public virtual Guid ID { get; set; }
        [XmlIgnore()]
        public virtual int TypeID { get; set; }
        [XmlIgnore()]
        public virtual Dictionary<string, string> IkeyEmail { get; set; }
        //public virtual string Email { get; set; }
        public virtual string key { get; set; }
        [XmlIgnore()]
        public virtual string Pattern { get; set; } // Mẫu số hđ
        [XmlIgnore()]
        public virtual string Serial { get; set; } // kí hiệu hđ
        public virtual string InvNo { get; set; } // số hđ
        [XmlIgnore()]
        public virtual DateTime? InvDate { get; set; } // 
        [XmlIgnore()]
        public virtual DateTime? RefDateTime { get; set; } // 
        public virtual string CusCode { get; set; } //Mã khách hàng
        public virtual string Buyer { get; set; } // Tên người mua
        public virtual string CusName { get; set; } //Tên khách hàng
        public virtual string Email { get; set; } //Tên khách hàng
        public virtual string EmailCC { get; set; } //Tên khách hàng
        public virtual string CusAddress { get; set; }
        public virtual string CusBankName { get; set; }
        public virtual string CusBankNo { get; set; }
        public virtual string CusPhone { get; set; }
        public virtual string CusTaxCode { get; set; }
        public virtual string PaymentMethod { get; set; } //phương thức thanh toán
        public virtual string ArisingDate { get; set; } // Ngày tạo lập đồng thời ngày phát hành
        public virtual decimal? ExchangeRate { get; set; } //Tỷ giá chuyển đổi
        public virtual string CurrencyUnit { get; set; }
        public virtual List<Product> Products { get; set; }
        public virtual decimal Total { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual string VATRate { get; set; }
        public virtual decimal Amount { get; set; }
        [XmlIgnore()]
        public virtual decimal DiscountAmount { get; set; }
        public virtual string AmountInWords { get; set; }
        public virtual string Extra { get; set; }
        [XmlIgnore()]
        public virtual int StatusInvoice { get; set; }
        [XmlIgnore()]
        public virtual string StatusInvoiceStr
        {
            get
            {
                if (StatusInvoice == -1) return "Hoá đơn không tồn tại trong hệ thống";
                if (StatusInvoice == 0) return "Hoá đơn mới tạo lập";
                if (StatusInvoice == 1) return "Hoá đơn có chữ ký số";
                if (StatusInvoice == 2) return "Hoá đơn đã khai báo thuế";
                if (StatusInvoice == 3) return "Hoá đơn bị thay thế";
                if (StatusInvoice == 4) return "Hoá đơn bị điều chỉnh";
                if (StatusInvoice == 5) return "Hoá đơn bị huỷ";
                if (StatusInvoice == 6) return "Hoá đơn chờ";
                if (StatusInvoice == 7) return "Hoá đơn mới tạo lập(TT)";
                if (StatusInvoice == 8) return "Hoá đơn mới tạo lập(ĐC)";
                return null;
            }
        }
        [XmlIgnore()]
        public virtual bool StatusSendMail { get; set; }
        [XmlIgnore()]
        public virtual bool StatusCusView { get; set; }
        [XmlIgnore()]
        public virtual bool Status { get; set; }
        [XmlIgnore()]
        public virtual bool StatusConverted { get; set; }
        [XmlIgnore()]
        public virtual Guid? IDReplaceInv { get; set; }
        [XmlIgnore()]
        public virtual Guid? IDAdjustInv { get; set; }
        [XmlIgnore()]
        public virtual Guid? ID_MIV { get; set; }
        public ViewEInvoice()
        {
            Products = new List<Product>();
        }
        [XmlIgnore()]
        public virtual string Ikey { get; set; }
        [XmlIgnore()]
        public virtual string No { get; set; }
        //Ngày phát hành
        [XmlIgnore()]
        public virtual string IssueDate { get; set; }
        [XmlIgnore()]
        public virtual string CustomerName { get; set; }
        [XmlIgnore()]
        public virtual string CustomerCode { get; set; }
        [XmlIgnore()]
        public virtual int InvoiceStatus { get; set; }
        [XmlIgnore()]
        public virtual bool Recorded { get; set; }
    }
}
