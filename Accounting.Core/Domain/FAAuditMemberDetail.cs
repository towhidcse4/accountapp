using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAAuditMemberDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? FAAuditID { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectTitle { get; set; }
    }
}
