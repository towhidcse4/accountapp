﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPInvoiceDetailCost
    {
        public virtual Guid ID { get; set; }
        public virtual bool? CostType { get; set; }
        public virtual Guid PPInvoiceID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual Guid PPServiceID { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual decimal? TotalFreightAmount { get; set; }
        public virtual decimal? TotalFreightAmountOriginal { get; set; }
        public virtual decimal? AmountPB { get; set; }
        public virtual decimal? AmountPBOriginal { get; set; }
        public virtual decimal? AccumulatedAllocateAmount { get; set; }
        public virtual decimal? AccumulatedAllocateAmountOriginal { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public PPInvoiceDetailCost()
        {
            TotalFreightAmount = 0;
            TotalFreightAmountOriginal = 0;
            AmountPB = 0;
            AmountPBOriginal = 0;
            AccumulatedAllocateAmount = 0;
            AccumulatedAllocateAmountOriginal = 0;
        }
    }
}
