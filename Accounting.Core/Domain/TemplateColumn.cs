﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TemplateColumn
    {
        public TemplateColumn()
        {
            ColumnName = ColumnCaption = ColumnToolTip = string.Empty;
            IsReadOnly = false; //mặc định không chỉ đọc
            ColumnWidth = ColumnMaxWidth = ColumnMinWidth = -1;    //mặc định không xét
            IsVisible = true;   //mặc định được hiển thị
            VisiblePosition = -1;   //mặc định không xét

            IsVisibleCbb = false;   //mặc định được hiển thị
        }

        private Guid iD;
        private Guid templateDetailID;
        private string columnCaption;
        private bool isReadOnly;
        private int columnWidth;
        private int columnMaxWidth;
        private int columnMinWidth;
        private bool isVisible;
        private int visiblePosition;
        private string columnToolTip;
        private string columnName;
        private bool isColumnHeader;

        public virtual bool IsColumnHeader
        {
            get { return isColumnHeader; }
            set { isColumnHeader = value; }
        }
        private bool isVisibleCbb;

        public virtual bool IsVisibleCbb
        {
            get { return isVisibleCbb; }
            set { isVisibleCbb = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid TemplateDetailID
        {
            get { return templateDetailID; }
            set { templateDetailID = value; }
        }

        public virtual string ColumnCaption
        {
            get { return columnCaption; }
            set { columnCaption = value; }
        }

        public virtual bool IsReadOnly
        {
            get { return isReadOnly; }
            set { isReadOnly = value; }
        }

        public virtual int ColumnWidth
        {
            get { return columnWidth; }
            set { columnWidth = value; }
        }

        public virtual int ColumnMaxWidth
        {
            get { return columnMaxWidth; }
            set { columnMaxWidth = value; }
        }

        public virtual int ColumnMinWidth
        {
            get { return columnMinWidth; }
            set { columnMinWidth = value; }
        }

        public virtual bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; }
        }

        public virtual int VisiblePosition
        {
            get { return visiblePosition; }
            set { visiblePosition = value; }
        }

        public virtual string ColumnToolTip
        {
            get { return columnToolTip; }
            set { columnToolTip = value; }
        }

        public virtual string ColumnName
        {
            get { return columnName; }
            set { columnName = value; }
        }
    }
}
