using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailPayment
    {
        public virtual Guid ID { get; set; }
        public virtual Guid GOtherVoucherID { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual Guid VoucherID { get; set; }
        public virtual DateTime VoucherDate { get; set; }
        public virtual DateTime VoucherPostedDate { get; set; }
        public virtual string VoucherNo { get; set; }
        public virtual int VoucherTypeID { get; set; }
        public virtual DateTime DueDate { get; set; }
        public virtual decimal PRAmount { get; set; }
        public virtual decimal PRAmountOriginal { get; set; }
        public virtual decimal RemainingAmount { get; set; }
        public virtual decimal RemainingAmountOriginal { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual decimal DiscountRate { get; set; }
        public virtual decimal DiscountAmount { get; set; }
        public virtual decimal DiscountAmountOriginal { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual Guid GenID { get; set; }
        public virtual Guid AccountingObjectID { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime InvoiceDate { get; set; }
        public virtual Guid EmployeeID { get; set; }
        public virtual Guid DeptEmployeeID { get; set; }
        public virtual bool ReceiptType { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int OrderPriority { get; set; }

    }
}