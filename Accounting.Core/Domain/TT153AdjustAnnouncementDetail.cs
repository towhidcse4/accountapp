using System;

namespace Accounting.Core.Domain
{
    public class TT153AdjustAnnouncementDetail
    {
        private Guid _ID;
        private Guid _TT153AdjustAnnouncementID;
        private string _Name;
        private string _OldInformation;
        private string _NewInformation;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TT153AdjustAnnouncementID
        {
            get { return _TT153AdjustAnnouncementID; }
            set { _TT153AdjustAnnouncementID = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual string OldInformation
        {
            get { return _OldInformation; }
            set { _OldInformation = value; }
        }

        public virtual string NewInformation
        {
            get { return _NewInformation; }
            set { _NewInformation = value; }
        }


    }
}
