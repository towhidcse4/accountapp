using System;

namespace Accounting.Core.Domain
{
    public class TT153AdjustAnnouncementDetailListInvoice
    {
        private Guid _ID;
        private Guid _TT153AdjustAnnouncementID;
        private int _InvoiceType;
        private Guid _InvoiceTypeID;
        private string _InvoiceSeries;
        private string _FromNo;
        private string _ToNo;
        private Decimal _Quantity;
        private Guid _TT153PublishInvoiceDetailID;
        private Guid _TT153ReportID;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TT153AdjustAnnouncementID
        {
            get { return _TT153AdjustAnnouncementID; }
            set { _TT153AdjustAnnouncementID = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }

        public virtual Guid InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual string FromNo
        {
            get { return _FromNo; }
            set { _FromNo = value; }
        }

        public virtual string ToNo
        {
            get { return _ToNo; }
            set { _ToNo = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Guid TT153PublishInvoiceDetailID
        {
            get { return _TT153PublishInvoiceDetailID; }
            set { _TT153PublishInvoiceDetailID = value; }
        }

        public virtual Guid TT153ReportID
        {
            get { return _TT153ReportID; }
            set { _TT153ReportID = value; }
        }

        public virtual TT153Report tT153Report { get; set; }
        public virtual string ReportName
        {
            get
            {
                if (tT153Report == null) return "";
                try
                {
                    return tT153Report.ReportName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }

        public virtual string InvoiceTemplate
        {
            get
            {
                if (tT153Report == null) return "";
                try
                {
                    return tT153Report.InvoiceTemplate;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
            set { }
        }
    }
}
