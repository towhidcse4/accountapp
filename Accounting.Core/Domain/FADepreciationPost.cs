using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADepreciationPost
    {
        private Guid _ID;
        private Guid? _FixedAssetID;
        private Guid _FADepreciationID;
        private Guid? objectID;
        private string _Description;
        private string _FixedAssetName;
        private string _DebitAccount;
        private string _CreditAccount;
        private Decimal? _Amount;
        private Decimal? _AmountOriginal;
        private Guid? _BudgetItemID;
        private Guid? _CostSetID;
        private Guid? _ContractID;
        private Guid? _AccountingObjectID;
        private Guid? _EmployeeID;
        private Guid? _StatisticsCodeID;
        private Guid? _ExpenseItemID;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private Boolean _IsIrrationalCost;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? FixedAssetID
        {
            get { return _FixedAssetID; }
            set { _FixedAssetID = value; }
        }

        public virtual Guid FADepreciationID
        {
            get { return _FADepreciationID; }
            set { _FADepreciationID = value; }
        }

        public virtual Guid? ObjectID
        {
            get { return objectID; }
            set { objectID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string FixedAssetName
        {
            get { return _FixedAssetName; }
            set { _FixedAssetName = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }

        public virtual Decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal? AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return _BudgetItemID; }
            set { _BudgetItemID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual Boolean IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
