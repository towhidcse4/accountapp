using System;

namespace Accounting.Core.Domain
{
public class Package
{
private Guid _Id;
private string _Code;
private string _Name;
private DateTime _CreatedDate;
private string _CreatedBy;
private string _ModifiedBy;
private DateTime _ModifiedDate;


public virtual Guid Id
{
get { return _Id; }
set { _Id = value; }
}

public virtual string Code
{
get { return _Code; }
set { _Code = value; }
}

public virtual string Name
{
get { return _Name; }
set { _Name = value; }
}

public virtual DateTime CreatedDate
{
get { return _CreatedDate; }
set { _CreatedDate = value; }
}

public virtual string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}

public virtual string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}

public virtual DateTime ModifiedDate
{
get { return _ModifiedDate; }
set { _ModifiedDate = value; }
}


}
}
