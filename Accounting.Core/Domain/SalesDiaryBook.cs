﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    public class SalesDiaryBook
    {
        public Guid RefID { get; set; }
        public int TypeID { get; set; }
        public DateTime PostedDate { get; set; }
        public DateTime Date { get; set; }
        public string No { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string Description { get; set; }
        //public decimal TotalAmount
        //{
        //    get;
        //    set;

        //} comment by cuongpv
        public decimal? TurnOverAmountInv { get; set; } //Doanh thu hàng hóa
        public decimal? TurnOverAmountFinishedInv { get; set; } //Doanh thu thành phẩm
        public decimal? TurnOverAmountServiceInv { get; set; } //Doanh thu dịch vụ
        public decimal? TurnOverAmountOther { get; set; } //Doanh thu khác
        public decimal? DiscountAmount { get; set; } //Chiết khấu
        public decimal? ReturnAmount { get; set; } //Giá trị trả lại
        public decimal? ReduceAmount { get; set; } //Giảm giá hàng bán
        //public decimal VATAmount { get; set; } comment by cuongpv
        public string CustomerName { get; set; }
        public decimal? SumTurnOver { get; set; } //Tổng doanh thu
        public decimal? TurnOverPure { get; set; } //Doanh thu thuần
    }
    public class SalesDiaryBook_Detail {
        public string Period { get; set; }
    }
}
