

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBTellerPaperDetailImportVAT
    {
        private Guid iD;

        public virtual Guid ID {
          get { return iD; }
          set { iD = value; }
        }

        private Guid mbTellerPaperID;

        public virtual Guid MbTellerPaperID
        {
            get { return mbTellerPaperID; }
            set { mbTellerPaperID = value; }
        }

        private Guid voucherID;

        public virtual Guid VoucherID
        {
            get { return voucherID; }
            set { voucherID = value; }
        }

        private DateTime voucherDate;

        public virtual DateTime VoucherDate
        {
            get { return voucherDate; }
            set { voucherDate = value; }
        }

        private DateTime voucherPostedDate;

        public virtual DateTime VoucherPostedDate
        {
            get { return voucherPostedDate; }
            set { voucherPostedDate = value; }
        }

        private string voucherNo;

        public virtual string VoucherNo
        {
            get { return voucherNo; }
            set { voucherNo = value; }
        }

        private int voucherTypeID;

        public virtual int VoucherTypeID
        {
            get { return voucherTypeID; }
            set { voucherTypeID = value; }
        }

        private string debitAccount;

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        private string creditAccount;

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        private decimal? payAmount;

        public virtual decimal? PayAmount
        {
            get { return payAmount; }
            set { payAmount = value; }
        }

        private decimal? amount;

        public virtual decimal? Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        private decimal? remainingAmount;

        public virtual decimal? RemainingAmount
        {
            get { return remainingAmount; }
            set { remainingAmount = value; }
        }

        private string customProperty1;

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }
        private string customProperty2;

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }
        private string customProperty3;

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        private int orderPriority;

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
    }
}
