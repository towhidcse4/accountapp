using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCPayment
    {
        private decimal totalAll;
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal TotalAmountOriginal { get; set; }
        public virtual decimal TotalVATAmount { get; set; }
        public virtual decimal TotalVATAmountOriginal { get; set; }
        public virtual Guid ID { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual int? AccountingObjectType { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string Receiver { get; set; }
        public virtual string Reason { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual bool? IsImportPurchase { get; set; }
        public virtual Guid? PPOrderID { get; set; }
        public virtual Guid? AuditID { get; set; }
        public virtual Guid? PaymentClauseID { get; set; }
        public virtual Guid? TransportMethodID { get; set; }
        public virtual Type Type { get; set; }

        public MCPayment()
        {
            MCPaymentDetails = new List<MCPaymentDetail>();
            MCPaymentDetailImportVATs = new List<MCPaymentDetailImportVAT>();
            MCPaymentDetailInsurances = new List<MCPaymentDetailInsurance>();
            MCPaymentDetailSalarys = new List<MCPaymentDetailSalary>();
            MCPaymentDetailTaxs = new List<MCPaymentDetailTax>();
            MCPaymentDetailVendors = new List<MCPaymentDetailVendor>();
            MCPaymentPersonalIncomes = new List<MCPaymentPersonalIncome>();
            RefVoucherRSInwardOutwards = new List<RefVoucherRSInwardOutward>();
        }
        public virtual IList<RefVoucherRSInwardOutward> RefVoucherRSInwardOutwards { get; set; }
        public virtual IList<MCPaymentDetail> MCPaymentDetails { get; set; }
        public virtual IList<MCPaymentDetailImportVAT> MCPaymentDetailImportVATs { get; set; }
        public virtual IList<MCPaymentDetailInsurance> MCPaymentDetailInsurances { get; set; }
        public virtual IList<MCPaymentDetailSalary> MCPaymentDetailSalarys { get; set; }
        public virtual IList<MCPaymentDetailTax> MCPaymentDetailTaxs { get; set; }
        public virtual IList<MCPaymentDetailVendor> MCPaymentDetailVendors { get; set; }
        public virtual IList<MCPaymentPersonalIncome> MCPaymentPersonalIncomes { get; set; }

        public virtual string TypeName
        {
            get { return Type.TypeName; }
        }
        public virtual decimal TotalAll
        {
            get
            {
                if (TypeID == 110) return TotalAmount;
                else return totalAll;
            }
            set { totalAll = value; }
        }
        private decimal totalAllOriginal;
        public virtual decimal TotalAllOriginal
        {
            get
            {
                if (TypeID == 110) return TotalAmountOriginal;
                else return totalAllOriginal;
            }
            set { totalAllOriginal = value; }
        }
    }
}
