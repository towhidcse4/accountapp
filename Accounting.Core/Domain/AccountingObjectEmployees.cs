﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class AccountingObjectEmployees
    {
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public DateTime? NgayChungTu { get; set; }
        public DateTime? NgayHachToan { get; set; }
        public string SoChungTu { get; set; }
        public string DienGiai { get; set; }
        public decimal? DoanhSo { get; set; }
        public decimal? GiamDoanhThu { get; set; }
        public string GhiChu { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }

    }
    public class AccountingObjectEmployees_Period
    {
        public string Period { get; set; }
    }
}
