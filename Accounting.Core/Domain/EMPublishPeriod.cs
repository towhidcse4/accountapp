﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMPublishPeriod
    {
        private Guid iD;
        private string periodCode;
        private string periodName;
        private DateTime periodDate = DateTime.Now;
        private int typeID;
        private decimal quantityPreferredShare;
        private decimal quantityCommonShare;
        private decimal quantityTotal;
        private string description;
        private bool isActive;
        public EMPublishPeriod()
        {

            EMPublishPeriodDetails = new List<EMPublishPeriodDetail>();

        }
        public virtual IList<EMPublishPeriodDetail> EMPublishPeriodDetails { get; set; }


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string PeriodCode
        {
            get { return periodCode; }
            set { periodCode = value; }
        }

        public virtual string PeriodName
        {
            get { return periodName; }
            set { periodName = value; }
        }



        public virtual DateTime PeriodDate
        {
            get { return periodDate; }
            set { periodDate = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual decimal QuantityPreferredShare
        {
            get { return quantityPreferredShare; }
            set { quantityPreferredShare = value; }
        }

        public virtual decimal QuantityCommonShare
        {
            get { return quantityCommonShare; }
            set { quantityCommonShare = value; }
        }

        public virtual decimal QuantityTotal
        {
            get { return quantityTotal; }
            set { quantityTotal = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

    }
}
