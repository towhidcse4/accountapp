using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysUserrole {
        public virtual Guid ID { get; set; }
        public virtual Guid UserID { get; set; }
        public virtual Guid RoleID { get; set; }
        public virtual Guid BranchID { get; set; }
    }
}
