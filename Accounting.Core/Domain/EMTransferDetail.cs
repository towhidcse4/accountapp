﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMTransferDetail
    {
        private Guid iD;
        private Guid eMTransferID;
        private Guid? stockCategoryID;
        private string stockCategoryView;
        private Decimal quantityTransfered;
        private Decimal amountTransfered;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid EMTransferID
        {
            get { return eMTransferID; }
            set { eMTransferID = value; }
        }
        public virtual Guid? StockCategoryID
        {
            get { return stockCategoryID; }
            set { stockCategoryID = value; }
        }
        public virtual string StockCategoryView
        {
            get { return stockCategoryView; }
            set { stockCategoryView = value; }
        }
        public virtual Decimal QuantityTransfered
        {
            get { return quantityTransfered; }
            set { quantityTransfered = value; }
        }
        public virtual Decimal AmountTransfered
        {
            get { return amountTransfered; }
            set { amountTransfered = value; }
        }
    }
}
