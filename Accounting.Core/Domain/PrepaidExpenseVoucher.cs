using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PrepaidExpenseVoucher
    {
        private Guid _ID;
        private Guid _PrepaidExpenseID;
        private DateTime _Date;
        private string _No;
        private string _Reason;
        private string _DebitAccount;
        private string _CreditAccount;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid PrepaidExpenseID
        {
            get { return _PrepaidExpenseID; }
            set { _PrepaidExpenseID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public virtual decimal Amount { get; set; }

    }
}
