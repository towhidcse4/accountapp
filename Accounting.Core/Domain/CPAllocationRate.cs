using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPAllocationRate
    {
        private Guid _ID;
        private Guid _CPPeriodID;
        private Guid _CPPeriodDetailID;
        private int _AllocationMethod;
        private Guid _CostSetID;
        private Guid _MaterialGoodsID;
        private Boolean _IsStandardItem;
        private Decimal _Quantity;
        private Decimal _PriceQuantum;
        private Decimal _Coefficien;
        private Decimal _QuantityStandard;
        private Decimal _AllocationStandard;
        private Decimal _AllocatedRate;
        private CostSet _Costset;
        private string _CostSetCode;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual Guid CPPeriodDetailID
        {
            get { return _CPPeriodDetailID; }
            set { _CPPeriodDetailID = value; }
        }

        public virtual int AllocationMethod
        {
            get { return _AllocationMethod; }
            set { _AllocationMethod = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual Boolean IsStandardItem
        {
            get { return _IsStandardItem; }
            set { _IsStandardItem = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal PriceQuantum
        {
            get { return _PriceQuantum; }
            set { _PriceQuantum = value; }
        }

        public virtual Decimal Coefficien
        {
            get { return _Coefficien; }
            set { _Coefficien = value; }
        }

        public virtual Decimal QuantityStandard
        {
            get { return _QuantityStandard; }
            set { _QuantityStandard = value; }
        }

        public virtual Decimal AllocationStandard
        {
            get { return _AllocationStandard; }
            set { _AllocationStandard = value; }
        }

        public virtual Decimal AllocatedRate
        {
            get { return _AllocatedRate; }
            set { _AllocatedRate = value; }
        }

        public virtual MaterialGoods MaterialGoods { get; set; }

        public virtual string MaterialGoodsCode { get; set; }
        
        public virtual string MaterialGoodsName { get; set; }
        
        public virtual CostSet CostSet
        {
            get { return _Costset; }
            set { _Costset = value; }
        }

        public virtual string CostSetCode
        {
            get { return _CostSetCode;  }
            set { _CostSetCode = value; }
        }
    }
}
