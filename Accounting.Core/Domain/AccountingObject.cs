﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountingObject
    {
        private Guid iD;
        private Guid? branchID;
        private string accountingObjectCode;
        private string accountingObjectName;
        private string departmentIDViewName;
        private string accountingObjectCategory;
        private DateTime? employeeBirthday;
        private string address;
        private string tel;
        private string fax;
        private string email;
        private string website;
        private string bankAccount;
        private string bankName;
        private string taxCode;
        private string description;
        private string contactName;
        private string contactTitle;
        private int? contactSex;
        private string contactMobile;
        private string contactEmail;
        private string contactHomeTel;
        private string contactOfficeTel;
        private string contactAddress;
        private int? scaleType;
        private int? objectType;
        private bool isEmployee;
        private string identificationNo;
        private DateTime? issueDate;
        private string issueBy;
        private Guid? departmentID;
        Department department;
        private bool isInsured;
        private bool isLabourUnionFree;
        private decimal familyDeductionAmount;
        private decimal? maximizaDebtAmount;
        private int? dueTime;
        private Guid? accountObjectGroupID;
        private string accountObjectGroupIdNameView;
        private Guid? paymentClauseID;
        private bool isActive;
        private decimal? salaryCoefficient;
        private decimal? insuranceSalary;
        private decimal? agreementSalary;
        private string departmentCode;
        private string scaleTypeName;
        public virtual string ScaleTypeName
        {

            get { return scaleTypeName; }
            set { scaleTypeName = value; }
        }
        public virtual string DepartmentCode
        {
            
                get { return departmentCode; }
            set { departmentCode = value; }
        }
        public virtual decimal? SalaryCoefficient
        {
            get { return salaryCoefficient; }
            set { salaryCoefficient = value; }
        }
        public virtual decimal? InsuranceSalary
        {
            get { return insuranceSalary; }
            set { insuranceSalary = value; }
        }
        public virtual decimal? AgreementSalary
        {
            get { return agreementSalary; }
            set { agreementSalary = value; }
        }

        //add cot tu bang khac
        AccountingObjectGroup accountingObjectGroup;
        
        private string accountingObjectGroupName;
       
        public virtual AccountingObjectGroup AccountingObjectGroup
        {
            get { return accountingObjectGroup; }
            set { accountingObjectGroup = value; }
        }

        public virtual string AccountingObjectGroupName
        {
            get
            {
                try
                {
                    return AccountingObjectGroup.AccountingObjectGroupName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }
        //end

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }
        public virtual string AccountingObjectCode
        {
            get { return accountingObjectCode; }
            set { accountingObjectCode = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string DepartmentIDViewName
        {
            get { return departmentIDViewName; }
            set { departmentIDViewName = value; }
        }

        public virtual string AccountingObjectCategory
        {
            get { return accountingObjectCategory; }
            set { accountingObjectCategory = value; }
        }

        public virtual DateTime? EmployeeBirthday
        {
            get { return employeeBirthday; }
            set { employeeBirthday = value; }
        }

        public virtual string Address
        {
            get { return address; }
            set { address = value; }
        }

        public virtual string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public virtual string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }

        public virtual string Website
        {
            get { return website; }
            set { website = value; }
        }

        public virtual string BankAccount
        {
            get { return bankAccount; }
            set { bankAccount = value; }
        }

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }

        public virtual string TaxCode
        {
            get { return taxCode; }
            set { taxCode = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual string ContactTitle
        {
            get { return contactTitle; }
            set { contactTitle = value; }
        }

        public virtual int? ContactSex
        {
            get { return contactSex; }
            set { contactSex = value; }
        }

        public virtual string ContactMobile
        {
            get { return contactMobile; }
            set { contactMobile = value; }
        }

        public virtual string ContactEmail
        {
            get { return contactEmail; }
            set { contactEmail = value; }
        }

        public virtual string ContactHomeTel
        {
            get { return contactHomeTel; }
            set { contactHomeTel = value; }
        }

        public virtual string ContactOfficeTel
        {
            get { return contactOfficeTel; }
            set { contactOfficeTel = value; }
        }

        public virtual string ContactAddress
        {
            get { return contactAddress; }
            set { contactAddress = value; }
        }

        public virtual int? ScaleType
        {
            get { return scaleType; }
            set { scaleType = value; }
        }

        public virtual int? ObjectType
        {
            get { return objectType; }
            set { objectType = value; }
        }

        //doi kieu du lieu objtype sang string
        public virtual string ObjectTypeName
        {
            get
            {
                Dictionary<int, string> objDic = new Dictionary<int, string>();
                objDic.Add(0, "Khách hàng");
                objDic.Add(1, "Nhà cung cấp");
                objDic.Add(2, "Khách hàng/Nhà cung cấp");
                objDic.Add(3, "Không xác định");
                return objDic[objectType ?? 3];
            }
        }
        //end

        public virtual bool IsEmployee
        {
            get { return isEmployee; }
            set { isEmployee = value; }
        }

        public virtual string IdentificationNo
        {
            get { return identificationNo; }
            set { identificationNo = value; }
        }

        public virtual DateTime? IssueDate
        {
            get { return issueDate; }
            set { issueDate = value; }
        }

        public virtual string IssueBy
        {
            get { return issueBy; }
            set { issueBy = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Department Department
        {
            get { return department; }
            set { department = value; }
        }

        public virtual bool IsInsured
        {
            get { return isInsured; }
            set { isInsured = value; }
        }

        public virtual bool IsLabourUnionFree
        {
            get { return isLabourUnionFree; }
            set { isLabourUnionFree = value; }
        }

        public virtual decimal FamilyDeductionAmount
        {
            get { return familyDeductionAmount; }
            set { familyDeductionAmount = value; }
        }

        public virtual decimal? MaximizaDebtAmount
        {
            get { return maximizaDebtAmount; }
            set { maximizaDebtAmount = value; }
        }

        public virtual int? DueTime
        {
            get { return dueTime; }
            set { dueTime = value; }
        }

        public virtual Guid? AccountObjectGroupID
        {
            get { return accountObjectGroupID; }
            set { accountObjectGroupID = value; }
        }

        public virtual string AccountObjectGroupIdNameView
        {
            get { return accountObjectGroupIdNameView; }
            set { accountObjectGroupIdNameView = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        public virtual bool IsUnofficialStaff { get; set; }
        public virtual int NumberOfDependent { get; set; }

        public AccountingObject()
        {
            BankAccounts = new List<AccountingObjectBankAccount>();
            SAPolicySalePriceCustomers = new List<SAPolicySalePriceCustomer>();
            EMContracts = new List<EMContract>();
        }
        public virtual IList<AccountingObjectBankAccount> BankAccounts { get; set; }

        public virtual IList<SAPolicySalePriceCustomer> SAPolicySalePriceCustomers { get; set; }

        public virtual IList<EMContract> EMContracts { get; set; }
        public virtual string DepartmentName {
            get
            {
                if (Department == null) return "";
                try
                {
                    return Department.DepartmentName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }
    }
}
