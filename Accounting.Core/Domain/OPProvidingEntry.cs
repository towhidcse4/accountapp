using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class OPProvidingEntry
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private DateTime _PostedDate;
        private string _DebitAccount;
        private string _CreditAccount;
        private Guid _MaterialGoodsID;
        private Guid _DepartmentID;
        private Decimal _Quantity;
        private Decimal _UnitPrice;
        private Decimal _Amount;
        private int _AllocationTimes;
        private Decimal _AllocatedAmount;
        private Decimal _RemainingAmount;
        private Guid _CostSetID;
        private int _AllocationType;
        private int _RemainAllocationTimes;
        private int _OpeningStatus;
        private Guid _StatisticsCodeID;
        private Boolean _IsIrrationalCost;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }

        public virtual Guid MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual Guid DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual int AllocationTimes
        {
            get { return _AllocationTimes; }
            set { _AllocationTimes = value; }
        }

        public virtual Decimal AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual Decimal RemainingAmount
        {
            get { return _RemainingAmount; }
            set { _RemainingAmount = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual int AllocationType
        {
            get { return _AllocationType; }
            set { _AllocationType = value; }
        }

        public virtual int RemainAllocationTimes
        {
            get { return _RemainAllocationTimes; }
            set { _RemainAllocationTimes = value; }
        }

        public virtual int OpeningStatus
        {
            get { return _OpeningStatus; }
            set { _OpeningStatus = value; }
        }

        public virtual Guid StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual Boolean IsIrrationalCost
        {
            get { return _IsIrrationalCost; }
            set { _IsIrrationalCost = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
