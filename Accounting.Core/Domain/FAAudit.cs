using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAAudit
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime InventoryDate { get; set; }
        public virtual string Summary { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual IList<FAAuditDetail> FAAuditDetails { get; set; }
        public virtual IList<FAAuditMemberDetail> FAAuditMemberDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public FAAudit()
        {
            FAAuditDetails = new List<FAAuditDetail>();
            FAAuditMemberDetails = new List<FAAuditMemberDetail>();
            RefVouchers = new List<RefVoucher>();
        }
    }
}
