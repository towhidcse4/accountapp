

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialQuantumDetail
    {
        private Guid iD;
        private Guid materialQuantumID;
        private Guid? materialGoodsID;
        private decimal unitPrice;
        private decimal? amount;
        private Guid? costSetID;
        private string unit;
        private string convertUnit;
        private decimal? convertRate;
        private decimal? convertQuantity;
        private int? orderPriority;
        private string description;
        private decimal? quantity;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MaterialQuantumID
        {
            get { return materialQuantumID; }
            set { materialQuantumID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }
        public virtual decimal? Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual string ConvertUnit
        {
            get { return convertUnit; }
            set { convertUnit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual decimal? ConvertQuantity
        {
            get { return convertQuantity; }
            set { convertQuantity = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        

        




    }
}
