﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMAllocationDetail
    {
        private Guid iD;
        private Guid eMAllocationID;
        private DateTime dateView;
        private int budgetMonthView;
        private int budgetYearView;
        private string budgetItemName;
        private Decimal requestAmountView;
        private Decimal aprovedAmountView;
        private Decimal amount;
        private string description;
        private int orderPriority;
        private Guid budgetItemID;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid EMAllocationID
        {
            get { return eMAllocationID; }
            set { eMAllocationID = value; }
        }
        public virtual DateTime DateView
        {
            get { return dateView; }
            set { dateView = value; }
        }
        public virtual int BudgetMonthView
        {
            get { return budgetMonthView; }
            set { budgetMonthView = value; }
        }

        public virtual int BudgetYearView
        {
            get { return budgetYearView; }
            set { budgetYearView = value; }
        }
        public virtual string BudgetItemName
        {
            get { return budgetItemName; }
            set { budgetItemName = value; }
        }

        public virtual Decimal RequestAmountView
        {
            get { return requestAmountView; }
            set { requestAmountView = value; }
        }
        public virtual Decimal AprovedAmountView
        {
            get { return aprovedAmountView; }
            set { aprovedAmountView = value; }
        }

        public virtual Decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
        public virtual Guid BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }
    }
}
