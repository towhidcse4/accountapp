

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBDepositDetailCustomer
    {
        private Guid saleInvoiceID;
        private decimal receipableAmount;
        private decimal receipableAmountOriginal;
        private decimal remainingAmount;
        private decimal remainingAmountOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private decimal discountAmount;
        private decimal discountAmountOriginal;
        private Guid iD;
        private Guid mBDepositID;
        private bool receiptType;
        private string creditAccount;
        private string discountAccount;
        private Guid? genID;
        private Guid? accountingObjectID;
        private string voucherInvNo;
        private Guid? employeeID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;
        private Guid? debtEmployeeID;
        private decimal? discountRate;
        private DateTime? voucherDate;
        private DateTime? voucherPostedDate;
        private string voucherNo;
        private int? voucherTypeID;

        public virtual decimal RefVoucherExchangeRate { get; set; }
        public virtual decimal LastExchangeRate { get; set; }
        public virtual decimal DifferAmount { get; set; }
        public virtual Guid SaleInvoiceID
        {
            get { return saleInvoiceID; }
            set { saleInvoiceID = value; }
        }

        public virtual decimal ReceipableAmount
        {
            get { return receipableAmount; }
            set { receipableAmount = value; }
        }

        public virtual decimal ReceipableAmountOriginal
        {
            get { return receipableAmountOriginal; }
            set { receipableAmountOriginal = value; }
        }

        public virtual decimal RemainingAmount
        {
            get { return remainingAmount; }
            set { remainingAmount = value; }
        }

        public virtual decimal RemainingAmountOriginal
        {
            get { return remainingAmountOriginal; }
            set { remainingAmountOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return discountAmountOriginal; }
            set { discountAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MBDepositID
        {
            get { return mBDepositID; }
            set { mBDepositID = value; }
        }

        public virtual bool ReceiptType
        {
            get { return receiptType; }
            set { receiptType = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual string DiscountAccount
        {
            get { return discountAccount; }
            set { discountAccount = value; }
        }

        public virtual Guid? GenID
        {
            get { return genID; }
            set { genID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string VoucherInvNo
        {
            get { return voucherInvNo; }
            set { voucherInvNo = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? DebtEmployeeID
        {
            get { return debtEmployeeID; }
            set { debtEmployeeID = value; }
        }

        public virtual decimal? DiscountRate
        {
            get { return discountRate; }
            set { discountRate = value; }
        }

        public virtual DateTime? VoucherDate
        {
            get { return voucherDate; }
            set { voucherDate = value; }
        }

        public virtual DateTime? VoucherPostedDate
        {
            get { return voucherPostedDate; }
            set { voucherPostedDate = value; }
        }

        public virtual string VoucherNo
        {
            get { return voucherNo; }
            set { voucherNo = value; }
        }

        public virtual int? VoucherTypeID
        {
            get { return voucherTypeID; }
            set { voucherTypeID = value; }
        }




    }
}
