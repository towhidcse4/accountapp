using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RSTransferDetail
    {
        private decimal unitPrice;
        private decimal unitPriceOriginal;
        private decimal unitPriceConvert;
        private decimal unitPriceConvertOriginal;
        private decimal amount;
        private decimal amountOriginal;
        private decimal iWUnitPrice;
        private decimal iWUnitPriceOriginal;
        private decimal iWUnitPriceConvert;
        private decimal iWUnitPriceConvertOriginal;
        private decimal iWAmount;
        private decimal iWAmountOriginal;
        private Guid iD;
        private Guid rSTransferID;
        private Guid? materialGoodsID;
        private Guid? fromRepositoryID;
        private Guid? toRepositoryID;
        private string debitAccount;
        private string creditAccount;
        private string unit;
        private decimal? quantity;
        private decimal? quantityConvert;
        private string description;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? accountingObjectID;
        private Guid? employeeID;
        private Guid? statisticsCodeID;
        private Guid? confrontID;
        private Guid? confrontDetailID;
        private DateTime? expiryDate;
        private string lotNo;
        private string unitConvert;
        private decimal? convertRate;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private Guid? budgetItemID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;


        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal UnitPriceOriginal
        {
            get { return unitPriceOriginal; }
            set { unitPriceOriginal = value; }
        }

        public virtual decimal UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value; }
        }

        public virtual decimal UnitPriceConvertOriginal
        {
            get { return unitPriceConvertOriginal; }
            set { unitPriceConvertOriginal = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal IWUnitPrice
        {
            get { return iWUnitPrice; }
            set { iWUnitPrice = value; }
        }

        public virtual decimal IWUnitPriceOriginal
        {
            get { return iWUnitPriceOriginal; }
            set { iWUnitPriceOriginal = value; }
        }

        public virtual decimal IWUnitPriceConvert
        {
            get { return iWUnitPriceConvert; }
            set { iWUnitPriceConvert = value; }
        }

        public virtual decimal IWUnitPriceConvertOriginal
        {
            get { return iWUnitPriceConvertOriginal; }
            set { iWUnitPriceConvertOriginal = value; }
        }

        public virtual decimal IWAmount
        {
            get { return iWAmount; }
            set { iWAmount = value; }
        }

        public virtual decimal IWAmountOriginal
        {
            get { return iWAmountOriginal; }
            set { iWAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid RSTransferID
        {
            get { return rSTransferID; }
            set { rSTransferID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual Guid? FromRepositoryID
        {
            get { return fromRepositoryID; }
            set { fromRepositoryID = value; }
        }

        public virtual Guid? ToRepositoryID
        {
            get { return toRepositoryID; }
            set { toRepositoryID = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual decimal? QuantityConvert
        {
            get { return quantityConvert; }
            set { quantityConvert = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? ConfrontID
        {
            get { return confrontID; }
            set { confrontID = value; }
        }

        public virtual Guid? ConfrontDetailID
        {
            get { return confrontDetailID; }
            set { confrontDetailID = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return lotNo; }
            set { lotNo = value; }
        }

        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? RSInwardDetailID { get; set; }
        public virtual MaterialGoods MaterialGoods { get; set; }
        public virtual string MaterialGoodsCode
        {
            get { return MaterialGoods != null ? MaterialGoods.MaterialGoodsCode : null; }
            //set { MaterialGoodsCode = value; }
        }

        public virtual Guid? TransportMethodID { get; set; }
    }
}
