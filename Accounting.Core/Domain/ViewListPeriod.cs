﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ViewListPeriod
    {
        private Guid? eMPublishPeriodID;
        private string stockCategoryName;
        private decimal unitPrice;
        private decimal quantity;
        private decimal amount;
        private decimal limitTransferYear;

       
        public virtual Guid? EMPublishPeriodID
        {
            get { return eMPublishPeriodID; }
            set { eMPublishPeriodID = value; }
        }
        public virtual string StockCategoryName
        {
            get { return stockCategoryName; }
            set { stockCategoryName = value; }
        }
        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }
        public virtual decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual decimal LimitTransferYear
        {
            get { return limitTransferYear; }
            set { limitTransferYear = value; }
        }








    }
}