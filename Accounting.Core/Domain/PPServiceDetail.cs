

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPServiceDetail
    {
        private int invoiceType;
        private Guid iD;
        private Guid pPServiceID;
        private Guid? materialGoodsID;
        private decimal? discountAmount;
        private decimal? discountAmountOriginal;
        private decimal? amount;
        private decimal? amountOriginal;
        private decimal? vATAmount;
        private decimal? vATAmountOriginal;
        private string vATAccount;
        private string vatDescription;
        private DateTime? vatPostedDate;
        private Guid? budgetItemID;
        private decimal? discountRate;
        private string discountAccount;
        private decimal? vATRate;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string invoiceSeries;
        private Guid? goodsServicePurchaseID;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? statisticsCodeID;
        private Guid? accountingObjectID;
        private Guid? accountingObjectTaxID;
        private string accountingObjectTaxName;
        private string companyTaxCode;
        private Guid? invoiceTypeID;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool? isIrrationalCost;
        private int? orderPriority;
        public virtual decimal UnitPrice { get; set; }
        public virtual decimal UnitPriceOriginal { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal CashOutVATAmount { get; set; }
        public virtual decimal CashOutDifferVATAmount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual int InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid PPServiceID
        {
            get { return pPServiceID; }
            set { pPServiceID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual decimal? DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public virtual decimal? DiscountAmountOriginal
        {
            get { return discountAmountOriginal; }
            set { discountAmountOriginal = value; }
        }

        public virtual decimal? Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal? AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal? VATAmount
        {
            get { return vATAmount; }
            set { vATAmount = value; }
        }

        public virtual decimal? VATAmountOriginal
        {
            get { return vATAmountOriginal; }
            set { vATAmountOriginal = value; }
        }

        public virtual string VATAccount
        {
            get { return vATAccount; }
            set { vATAccount = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual decimal? DiscountRate
        {
            get { return discountRate; }
            set { discountRate = value; }
        }

        public virtual string DiscountAccount
        {
            get { return discountAccount; }
            set { discountAccount = value; }
        }

        public virtual decimal? VATRate
        {
            get { return vATRate; }
            set { vATRate = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }

        public virtual Guid? GoodsServicePurchaseID
        {
            get { return goodsServicePurchaseID; }
            set { goodsServicePurchaseID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? AccountingObjectTaxID
        {
            get { return accountingObjectTaxID; }
            set { accountingObjectTaxID = value; }
        }

        public virtual string AccountingObjectTaxName
        {
            get { return accountingObjectTaxName; }
            set { accountingObjectTaxName = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return invoiceTypeID; }
            set { invoiceTypeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool? IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual string VATDescription
        {
            get { return vatDescription; }
            set { vatDescription = value; }
        }

        public virtual DateTime? VATPostedDate
        {
            get { return vatPostedDate; }
            set { vatPostedDate = value; }
        }
    }
}
