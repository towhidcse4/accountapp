using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountTransfer
    {
        private Guid _ID;
        private string _AccountTransferCode;
        private int _AccountTransferOrder;
        private int _AccountTransferType;
        string _AccountTransferTypeView;
        private string _FromAccount;
        private string _ToAccount;
        private int _TransferSide;
        string _TransferSideView;
        private string _Description;
        private Boolean _IsActive;
        private Boolean _IsSecurity;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string AccountTransferCode
        {
            get { return _AccountTransferCode; }
            set { _AccountTransferCode = value; }
        }

        public virtual int AccountTransferOrder
        {
            get { return _AccountTransferOrder; }
            set { _AccountTransferOrder = value; }
        }

        public virtual int AccountTransferType
        {
            get { return _AccountTransferType; }
            set { _AccountTransferType = value; }
        }

        public virtual string AccountTransferTypeView
        {
            get { return _AccountTransferTypeView; }
            set { _AccountTransferTypeView = value; }
        }

        public virtual string FromAccount
        {
            get { return _FromAccount; }
            set { _FromAccount = value; }
        }

        public virtual string ToAccount
        {
            get { return _ToAccount; }
            set { _ToAccount = value; }
        }

        public virtual int TransferSide
        {
            get { return _TransferSide; }
            set { _TransferSide = value; }
        }

        public virtual string TransferSideView
        {
            get { return _TransferSideView; }
            set { _TransferSideView = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public virtual Boolean IsSecurity
        {
            get { return _IsSecurity; }
            set { _IsSecurity = value; }
        }

        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
    }
}
