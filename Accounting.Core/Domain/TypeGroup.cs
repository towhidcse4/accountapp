using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TypeGroup
    {
        private int iD;
        private string typeGroupName;
        private string debitAccountID;
        private string creditAccountID;


        public virtual int ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string TypeGroupName
        {
            get { return typeGroupName; }
            set { typeGroupName = value; }
        }

        public virtual string DebitAccountID
        {
            get { return debitAccountID; }
            set { debitAccountID = value; }
        }

        public virtual string CreditAccountID
        {
            get { return creditAccountID; }
            set { creditAccountID = value; }
        }




    }
}
