

using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPDiscountReturn
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private decimal totalVATAmount;
        private decimal totalVATAmountOriginal;
        private decimal totalDiscountAmount;
        private decimal totalDiscountAmountOriginal;
        private bool recorded;
        private bool exported;
        private Guid iD;
        private Guid? brachID;
        private DateTime? refOrder;
        private int? unitPriceMethod;
        private int? invoiceForm;
        private Guid? invoiceTypeID;
        private string listNo;
        private DateTime? listDate;
        private string listCommonNameInventory;
        private bool? isAttachList;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private string customProperty4;
        private string customProperty5;
        private string customProperty6;
        private string outwardNo;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string companyTaxCode;
        private string reason;
        private DateTime? oDate;
        private DateTime? oPostedDate;
        private string oContactName;
        private string oReason;
        private string originalNo;
        private int? invoiceType;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string invoiceSeries;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? transportMethodID;
        private DateTime? dueDate;
        private Guid? paymentClauseID;
        private Guid? employeeID;
        private Guid? templateID;
        private Type _type;
        public virtual IList<PPDiscountReturnDetail> PPDiscountReturnDetails { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual bool? IsDeliveryVoucher { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public PPDiscountReturn()
        {
            RefVouchers = new List<RefVoucher>();
            PPDiscountReturnDetails = new List<PPDiscountReturnDetail>();
            TotalPaymentAmountOriginal = 0;
            TotalPaymentAmount = 0;
            TotalAmountOriginal = 0;
            TotalAmount = 0;
            TotalDiscountAmount = 0;
            TotalDiscountAmountOriginal = 0;
            TotalVATAmount = 0;
            TotalVATAmountOriginal = 0;

        }
        public virtual int? MaxOrderPriority
        {
            get
            {
                int? max1 = null;
                if (PPDiscountReturnDetails.Count > 0)
                {
                    max1 = PPDiscountReturnDetails.Max(k => k.OrderPriority) + 100000;
                }
                return max1;
            }
        }
        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual decimal TotalVATAmount
        {
            get { return totalVATAmount; }
            set { totalVATAmount = value; }
        }

        public virtual decimal TotalVATAmountOriginal
        {
            get { return totalVATAmountOriginal; }
            set { totalVATAmountOriginal = value; }
        }

        public virtual decimal TotalDiscountAmount
        {
            get { return totalDiscountAmount; }
            set { totalDiscountAmount = value; }
        }

        public virtual decimal TotalDiscountAmountOriginal
        {
            get { return totalDiscountAmountOriginal; }
            set { totalDiscountAmountOriginal = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid? BrachID
        {
            get { return brachID; }
            set { brachID = value; }
        }

        public virtual DateTime? RefOrder
        {
            get { return refOrder; }
            set { refOrder = value; }
        }

        public virtual int? UnitPriceMethod
        {
            get { return unitPriceMethod; }
            set { unitPriceMethod = value; }
        }

        public virtual int? InvoiceForm
        {
            get { return invoiceForm; }
            set { invoiceForm = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return invoiceTypeID; }
            set { invoiceTypeID = value; }
        }

        public virtual string ListNo
        {
            get { return listNo; }
            set { listNo = value; }
        }

        public virtual DateTime? ListDate
        {
            get { return listDate; }
            set { listDate = value; }
        }

        public virtual string ListCommonNameInventory
        {
            get { return listCommonNameInventory; }
            set { listCommonNameInventory = value; }
        }

        public virtual bool? IsAttachList
        {
            get { return isAttachList; }
            set { isAttachList = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual string CustomProperty4
        {
            get { return customProperty4; }
            set { customProperty4 = value; }
        }

        public virtual string CustomProperty5
        {
            get { return customProperty5; }
            set { customProperty5 = value; }
        }

        public virtual string CustomProperty6
        {
            get { return customProperty6; }
            set { customProperty6 = value; }
        }

        public virtual string OutwardNo
        {
            get { return outwardNo; }
            set { outwardNo = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }

        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual DateTime? ODate
        {
            get { return oDate; }
            set { oDate = value; }
        }

        public virtual DateTime? OPostedDate
        {
            get { return oPostedDate; }
            set { oPostedDate = value; }
        }

        public virtual string OContactName
        {
            get { return oContactName; }
            set { oContactName = value; }
        }

        public virtual string OReason
        {
            get { return oReason; }
            set { oReason = value; }
        }

        public virtual string OriginalNo
        {
            get { return originalNo; }
            set { originalNo = value; }
        }

        public virtual int? InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }

        public virtual DateTime? DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }

        public virtual Guid? PaymentClauseID
        {
            get { return paymentClauseID; }
            set { paymentClauseID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        private decimal _totalPaymentAmount;
        private decimal _totalPaymentAmountOriginal;

        public virtual Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public virtual string TypeName
        {
            get
            {
                try
                {
                    return Type.TypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }

        public virtual decimal TotalPaymentAmount
        {
            get { return TotalAmount + TotalVATAmount - TotalDiscountAmount; }
            set { _totalPaymentAmount = value; }
        }

        public virtual decimal TotalPaymentAmountOriginal
        {
            get { return TotalAmountOriginal + TotalVATAmountOriginal - TotalDiscountAmountOriginal; }
            set { _totalPaymentAmountOriginal = value; }
        }
        public virtual string PaymentMethod { get; set; }
        public virtual int StatusInvoice { get; set; }
        public virtual bool StatusSendMail { get; set; }
        public virtual DateTime? DateSendMail { get; set; }
        public virtual string Email { get; set; }
        public virtual bool StatusConverted { get; set; }
        public virtual Guid? IDReplaceInv { get; set; }
        public virtual Guid? IDAdjustInv { get; set; }
        public virtual bool IsBill { get; set; }
        public virtual bool IsAttachListBill { get; set; }
        public virtual Guid? BillRefID { get; set; }
        public virtual Guid? ID_MIV { get; set; }
        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (PPDiscountReturnDetails.Count > 0)
                {
                    max = PPDiscountReturnDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        
    }
}
