using System;

namespace Accounting.Core.Domain
{
    public class TM031ATNDN
    {
        private Guid _ID;
        private Guid _TM03TNDNID;
        private string _Code;
        private string _Name;
        private string _Data;
        private int? _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TM03TNDNID
        {
            get { return _TM03TNDNID; }
            set { _TM03TNDNID = value; }
        }

        public virtual string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
