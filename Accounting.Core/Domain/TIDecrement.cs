using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain {
    
    [Serializable]
    public partial class TIDecrement {
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal TotalRemainingAmount { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual Guid? RefID { get; set; }
        public virtual IList<TIDecrementDetail> TIDecrementDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public TIDecrement()
        {

            RefVouchers = new List<RefVoucher>();
        }
    }
}
