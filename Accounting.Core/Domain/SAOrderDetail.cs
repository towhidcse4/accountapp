using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAOrderDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? SAOrderID { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual decimal UnitPriceOriginal { get; set; }
        public virtual decimal UnitPriceConvert { get; set; }
        public virtual decimal UnitPriceConvertOriginal { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual decimal VATAmountOriginal { get; set; }
        public virtual decimal DiscountAmount { get; set; }
        public virtual decimal DiscountAmountOriginal { get; set; }
        public virtual decimal UnitPriceAfterTax { get; set; }
        public virtual decimal UnitPriceAfterTaxOriginal { get; set; }
        public virtual decimal DiscountAmountAfterTax { get; set; }
        public virtual decimal DiscountAmountAfterTaxOriginal { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? ConfrontID { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string LotNo { get; set; }
        public virtual string UnitConvert { get; set; }
        public virtual decimal? ConvertRate { get; set; }
        public virtual string CostAccount { get; set; }
        public virtual string RepositoryAccount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual string Description { get; set; }
        public virtual Guid? RepositoryID { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? QuantityConvert { get; set; }
        public virtual decimal? QuantityReceipt { get; set; }
        public virtual decimal? QuantityReceiptConvert { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal AmountAfterTaxOriginal { get; set; }
        public virtual decimal AmountAfterTax { get; set; }
        public virtual string VATDescription { get; set; }
    }
}
