﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPPeriod
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _Type;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private string _Name;
        private bool _IsDelete;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public virtual DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public virtual bool IsDelete
        {
            get { return _IsDelete; }
            set { _IsDelete = value; }
        }

        public virtual IList<CPPeriodDetail> CPPeriodDetails { get; set; }
        public virtual IList<CPExpenseList> CPExpenseLists { get; set; }
        public virtual IList<CPAllocationGeneralExpense> CPAllocationGeneralExpenses { get; set; }
        public virtual IList<CPAcceptance> CPAcceptances { get; set; }
        public virtual IList<CPUncomplete> CPUncompletes { get; set; }
        public virtual IList<CPAllocationRate> CPAllocationRates { get; set; }
        public virtual IList<CPResult> CPResults { get; set; }
        public CPPeriod()
        {
            CPPeriodDetails = new List<CPPeriodDetail>();
            CPExpenseLists = new List<CPExpenseList>();
            CPAllocationGeneralExpenses = new List<CPAllocationGeneralExpense>();
            CPAcceptances = new List<CPAcceptance>();
            CPUncompletes = new List<CPUncomplete>();
            CPAllocationRates = new List<CPAllocationRate>();
            CPResults = new List<CPResult>();
        }
        public virtual string TypeString
        {
            get
            {
                if (_Type == 0) return "Phương pháp giản đơn";
                else if (_Type == 1) return "Phương pháp hệ số";
                else if (_Type == 2) return "Phương pháp tỷ lệ";
                else if (_Type == 3) return "Công trình";
                else if (_Type == 4) return "Đơn hàng";
                else if (_Type == 5) return "Hợp đồng";
                else return "";
            }
        }
    }

}
