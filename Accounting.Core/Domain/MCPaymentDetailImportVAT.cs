using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCPaymentDetailImportVAT
    {
        private Guid iD;
        private Guid mCPaymentID;
        private decimal payableAmount;
        private decimal amount;
        private decimal remainingAmount;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;
        private Guid? voucherID;
        private DateTime? voucherDate;
        private int? voucherTypeID;
        private string voucherNo;
        private DateTime? voucherPostedDate;
        private string debitAccount;
        private string creaditAccount;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MCPaymentID
        {
            get { return mCPaymentID; }
            set { mCPaymentID = value; }
        }

        public virtual decimal PayableAmount
        {
            get { return payableAmount; }
            set { payableAmount = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal RemainingAmount
        {
            get { return remainingAmount; }
            set { remainingAmount = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? VoucherID
        {
            get { return voucherID; }
            set { voucherID = value; }
        }

        public virtual DateTime? VoucherDate
        {
            get { return voucherDate; }
            set { voucherDate = value; }
        }

        public virtual int? VoucherTypeID
        {
            get { return voucherTypeID; }
            set { voucherTypeID = value; }
        }

        public virtual string VoucherNo
        {
            get { return voucherNo; }
            set { voucherNo = value; }
        }

        public virtual DateTime? VoucherPostedDate
        {
            get { return voucherPostedDate; }
            set { voucherPostedDate = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreaditAccount
        {
            get { return creaditAccount; }
            set { creaditAccount = value; }
        }
    }
}
