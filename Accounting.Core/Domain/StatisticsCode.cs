using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class StatisticsCode
    {
        private Guid iD;
        private string statisticsCode;
        private string statisticsCodeName;
        private int grade;
        private bool isActive;
        private bool isSecurity;
        private string description;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string StatisticsCode_
        {
            get { return statisticsCode; }
            set { statisticsCode = value; }
        }

        public virtual string StatisticsCodeName
        {
            get { return statisticsCodeName; }
            set { statisticsCodeName = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
        public class StatisticsCode_Period
        {
            public string Period { get; set; }
        }
    }
}
