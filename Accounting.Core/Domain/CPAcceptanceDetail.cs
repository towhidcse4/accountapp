using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPAcceptanceDetail
    {
        private Guid _ID;
        private Guid _CPAcceptanceID;
        private string _Description;
        private string _DebitAccount;
        private string _CreditAccount;
        private Decimal _TotalAmount;
        private Decimal _RevenueAmount;
        private Decimal _Amount;
        private Decimal _UnitPrice;
        private Decimal _AcceptedRate;
        private Decimal _TotalAcceptedAmount;
        private Guid _ExpenseItemID;
        private Guid _StatisticsCodeID;
        private Guid _CPPeriodID;
        private Guid _CostSetID;
        public virtual Guid ContractID { get; set; }
        public virtual string ContractCode { get; set; }
        public virtual DateTime? SignedDate { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid CPAcceptanceID
        {
            get { return _CPAcceptanceID; }
            set { _CPAcceptanceID = value; }
        }
        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }
        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }

        public virtual Decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public virtual Decimal RevenueAmount
        {
            get { return _RevenueAmount; }
            set { _RevenueAmount = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal AcceptedRate
        {
            get { return _AcceptedRate; }
            set { _AcceptedRate = value; }
        }

        public virtual Decimal TotalAcceptedAmount
        {
            get { return _TotalAcceptedAmount; }
            set { _TotalAcceptedAmount = value; }
        }

        public virtual Guid ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Guid StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }
        public virtual string CostSetCode { get; set; }
        public virtual string CostSetName { get; set; }        
    }
}
