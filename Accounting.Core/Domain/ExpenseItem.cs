using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ExpenseItem
    {
        private Guid iD;
        private string expenseItemCode;
        private string expenseItemName;
        private int grade;
        private string expenseItemTypeView;
        private bool isActive;
        private bool isSecurity;
        private string description;
        private Guid? parentID;
        private string orderFixCode;
        private bool isParentNode;
        private int expenseItemType;
        public virtual int STT { get; set; }
        public virtual int ExpenseType
        {
            get { return expenseItemType; }
            set { expenseItemType = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string ExpenseItemCode
        {
            get { return expenseItemCode; }
            set { expenseItemCode = value; }
        }

        public virtual string ExpenseItemName
        {
            get { return expenseItemName; }
            set { expenseItemName = value; }
        }
        public virtual string ExpenseItemTypeView
        {
            get { return expenseItemTypeView; }
            set { expenseItemTypeView = value; }
        }
        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
        public virtual decimal? Amount { get; set; }
    }
    public class ExpenseItem_Period
    {
        public string Period { get; set; }
        public string acc { get; set; }
    }
}
