using System;

namespace Accounting.Core.Domain
{
    public class EMContractDetailMG
    {
        private Guid _ID;
        private Guid? _MaterialGoodsID;
        private string _MaterialGoodsCode;
        private string _MaterialGoodsName;
        private string _Description;
        private string _Unit;
        private string _UnitConvert;
        private decimal _ConvertRate;
        private decimal _Quantity;
        private decimal _QuantityConvert;
        private decimal _QuantityReceipt;
        private decimal _QuantityReceiptConvert;
        private decimal _Amount;
        private decimal _AmountOriginal;
        private decimal _VATAmount;
        private decimal _VATAmountOriginal;
        private decimal _VATRate;
        private decimal _DiscountRate;
        private decimal _DiscountAmount;
        private decimal _DiscountAmountOriginal;
        private Guid _ContractID;
        private decimal _AmountReceipt;
        private decimal _AmountReceiptOriginal;
        private decimal _AmountBeforeCancel;
        private decimal _AmountReceiptCancelOriginal;
        private decimal _TotalAmount;
        private decimal _TotalAmountOriginal;
        private int _OrderPriority;
        private decimal _UnitPrice;
        private MaterialGoods codeMaterialGoods;
        private SAOrder codeSAOrder;
        private string _SAOrderCode;
        private PPOrder codePPOrder;
        private string _PPOrderCode;
        private Guid? _SAOrderID;
        private Guid? _PPOrderID;
        private Guid? _SAOrderDetailID;
        private Guid? _PPOrderDetailID;

        public virtual Guid? SAOrderID
        {
            get { return _SAOrderID; }
            set { _SAOrderID = value; }
        }

        public virtual Guid? PPOrderID
        {
            get { return _PPOrderID; }
            set { _PPOrderID = value; }
        }

        public virtual Guid? SAOrderDetailID
        {
            get { return _SAOrderDetailID; }
            set { _SAOrderDetailID = value; }
        }

        public virtual Guid? PPOrderDetailID
        {
            get { return _PPOrderDetailID; }
            set { _PPOrderDetailID = value; }
        }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }
        public virtual MaterialGoods CodeMaterialGoods
        {
            get { return codeMaterialGoods; }
            set { codeMaterialGoods = value; }
        }
        public virtual string MaterialGoodsName
        {
            get
            {
                return (CodeMaterialGoods != null && CodeMaterialGoods.ID != Guid.Empty) ? CodeMaterialGoods.MaterialGoodsName : _MaterialGoodsName;
            }
            set { _MaterialGoodsName = value; }
        }

        public virtual string MaterialGoodsCode
        {
            get
            {
                return (CodeMaterialGoods != null && CodeMaterialGoods.ID != Guid.Empty) ? CodeMaterialGoods.MaterialGoodsCode : _MaterialGoodsCode;
            }
            set { _MaterialGoodsCode = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        public virtual string UnitConvert
        {
            get { return _UnitConvert; }
            set { _UnitConvert = value; }
        }

        public virtual decimal ConvertRate
        {
            get { return _ConvertRate; }
            set { _ConvertRate = value; }
        }

        public virtual decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual decimal QuantityConvert
        {
            get { return _QuantityConvert; }
            set { _QuantityConvert = value; }
        }

        public virtual decimal QuantityReceipt
        {
            get { return _QuantityReceipt; }
            set { _QuantityReceipt = value; }
        }

        public virtual decimal QuantityReceiptConvert
        {
            get { return _QuantityReceiptConvert; }
            set { _QuantityReceiptConvert = value; }
        }

        public virtual decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual decimal VATAmount
        {
            get { return _VATAmount; }
            set { _VATAmount = value; }
        }

        public virtual decimal VATAmountOriginal
        {
            get { return _VATAmountOriginal; }
            set { _VATAmountOriginal = value; }
        }

        public virtual decimal VATRate
        {
            get { return _VATRate; }
            set { _VATRate = value; }
        }

        public virtual decimal DiscountRate
        {
            get { return _DiscountRate; }
            set { _DiscountRate = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return _DiscountAmount; }
            set { _DiscountAmount = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return _DiscountAmountOriginal; }
            set { _DiscountAmountOriginal = value; }
        }

        public virtual Guid ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual decimal AmountReceipt
        {
            get { return _AmountReceipt; }
            set { _AmountReceipt = value; }
        }

        public virtual decimal AmountReceiptOriginal
        {
            get { return _AmountReceiptOriginal; }
            set { _AmountReceiptOriginal = value; }
        }

        public virtual decimal AmountBeforeCancel
        {
            get { return _AmountBeforeCancel; }
            set { _AmountBeforeCancel = value; }
        }

        public virtual decimal AmountReceiptCancelOriginal
        {
            get { return _AmountReceiptCancelOriginal; }
            set { _AmountReceiptCancelOriginal = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return _TotalAmountOriginal; }
            set { _TotalAmountOriginal = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }
        public virtual decimal UnitPriceOriginal { get; set; }
        public virtual decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual SAOrder CodeSAOrder
        {
            get { return codeSAOrder; }
            set { codeSAOrder = value; }
        }

        public virtual string SAOrderCode
        {
            get { return (CodeSAOrder != null && CodeSAOrder.ID != Guid.Empty) ? CodeSAOrder.No : _SAOrderCode; }
            set { _SAOrderCode = value; }
        }

        public virtual PPOrder CodePPOrder
        {
            get { return codePPOrder; }
            set { codePPOrder = value; }
        }

        public virtual string PPOrderCode
        {
            get { return (CodePPOrder != null && CodePPOrder.ID != Guid.Empty) ? CodePPOrder.No : _PPOrderCode; }
            set { _PPOrderCode = value; }
        }
    }
}
