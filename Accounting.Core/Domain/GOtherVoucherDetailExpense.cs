using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailExpense
    {       
        private Guid _ID;
        private Guid _GOtherVoucherID;
        private Guid? _PrepaidExpenseID;
        private Decimal _Amount;
        private Decimal _RemainingAmount;
        private Decimal _AllocationAmount;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual int? OrderPriority { get; set; }
        public virtual Guid GOtherVoucherID
        {
            get { return _GOtherVoucherID; }
            set { _GOtherVoucherID = value; }
        }

        public virtual Guid? PrepaidExpenseID
        {
            get { return _PrepaidExpenseID; }
            set { _PrepaidExpenseID = value; }
        }
        public virtual string PrepaidExpenseName
        {
            get;
            set;
        }
        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal RemainingAmount
        {
            get { return _RemainingAmount; }
            set { _RemainingAmount = value; }
        }

        public virtual Decimal AllocationAmount
        {
            get { return _AllocationAmount; }
            set { _AllocationAmount = value; }
        }


    }
}
