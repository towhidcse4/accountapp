using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class OPMaterialGoods
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _TypeID;
        private DateTime _PostedDate;
        private string _AccountNumber;
        private Guid _MaterialGoodsID;
        private Guid _RepositoryID;
        private string _CurrencyID;
        private Decimal? _ExchangeRate;
        private Decimal _UnitPrice;
        private Decimal _UnitPriceOriginal;
        private Decimal? _Quantity;
        private Decimal _Amount;
        private Decimal _AmountOriginal;
        private DateTime? _ExpiryDate;
        private string _LotNo;
        private string _Unit;
        private string _UnitConvert;
        private Decimal? _QuantityConvert;
        private Decimal _UnitPriceConvert;
        private Decimal _UnitPriceConvertOriginal;
        private Guid? _CostSetID;
        private Guid? _ContractID;
        private Guid? _BankAccountDetailID;
        private string _ContractName;
        private string _CostSetName;
        private string _BankName;
        private Guid? _ExpenseItemID;
        private Decimal? _ConvertRate;
        private int _OrderPriority;
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }

        public virtual Guid MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual Guid RepositoryID
        {
            get { return _RepositoryID; }
            set { _RepositoryID = value; }
        }

        public virtual string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }

        public virtual Decimal? ExchangeRate
        {
            get { return _ExchangeRate; }
            set { _ExchangeRate = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal UnitPriceOriginal
        {
            get { return _UnitPriceOriginal; }
            set { _UnitPriceOriginal = value; }
        }

        public virtual Decimal? Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return _ExpiryDate; }
            set { _ExpiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return _LotNo; }
            set { _LotNo = value; }
        }

        public virtual string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        public virtual string UnitConvert
        {
            get { return _UnitConvert; }
            set { _UnitConvert = value; }
        }

        public virtual Decimal? QuantityConvert
        {
            get { return _QuantityConvert; }
            set { _QuantityConvert = value; }
        }

        public virtual Decimal UnitPriceConvert
        {
            get { return _UnitPriceConvert; }
            set { _UnitPriceConvert = value; }
        }

        public virtual Decimal UnitPriceConvertOriginal
        {
            get { return _UnitPriceConvertOriginal; }
            set { _UnitPriceConvertOriginal = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return _BankAccountDetailID; }
            set { _BankAccountDetailID = value; }
        }

        public virtual string ContractName
        {
            get { return _ContractName; }
            set { _ContractName = value; }
        }

        public virtual string CostSetName
        {
            get { return _CostSetName; }
            set { _CostSetName = value; }
        }

        public virtual string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Decimal? ConvertRate
        {
            get { return _ConvertRate; }
            set { _ConvertRate = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }
    }
}
