﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSTimeSheet
    {
        private Guid _ID;  
        private Guid _BranchID;
        private int _TypeID;
        private int _Month;
        private int _Year;
        private string _PSTimeSheetName;
        private Guid _TemplateID;
        public virtual IList<PSTimeSheetDetail> PSTimeSheetDetails { get; set; }
        public virtual List<Guid> Departments { get; set; }
        public virtual decimal WorkingHoursInDay { get; set; }
        public virtual Guid? BaseOn { get; set; }
        public virtual bool IsNewEmployee { get; set; }
        public virtual bool IsUnemployee { get; set; }

        public PSTimeSheet()
        {
            PSTimeSheetDetails = new List<PSTimeSheetDetail>();
        }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual Guid TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }

        public virtual string Type
        {
            get { return _TypeID == 821 ? "Chấm công theo ngày" : "Chấm công theo giờ"; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual int Month
        {
            get { return _Month; }
            set { _Month = value; }
        }

        public virtual int Year
        {
            get { return _Year; }
            set { _Year = value; }
        }

        public virtual string PSTimeSheetName
        {
            get { return _PSTimeSheetName; }
            set { _PSTimeSheetName = value; }
        }


    }
}
