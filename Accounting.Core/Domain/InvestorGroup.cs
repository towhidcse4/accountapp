using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class InvestorGroup
    {
        private Guid iD;
        private string investorGroupCode;
        private string investorGroupName;
        private int grade;
        private bool isActive;
        private Guid? parentID;
        private bool isParentNode;
        private string orderFixCode;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string InvestorGroupCode
        {
            get { return investorGroupCode; }
            set { investorGroupCode = value; }
        }

        public virtual string InvestorGroupName
        {
            get { return investorGroupName; }
            set { investorGroupName = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
    }
}
