﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class ViewRSLotNo
    {
        public virtual Guid ID { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual decimal TotalIWQuantity { get; set; }
        public virtual decimal TotalOWQuantity { get; set; }
        public virtual decimal TotalIOQuantityBalance { get; set; }
    }
}
