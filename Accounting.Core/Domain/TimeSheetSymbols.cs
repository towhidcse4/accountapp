using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TimeSheetSymbols
    {
        private Guid iD;
        public virtual bool Selected { get; set; }
        private string timeSheetSymbolsCode;
        private string timeSheetSymbolsName;
        private bool isActive;
        private bool isSecurity;
        private decimal? salaryRate;
        private bool? isDefault;
        private bool? isOverTime;
        private bool? isHalfDay;
        private int? overTimeSymbol;
        public virtual decimal? OT { get; set; }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string TimeSheetSymbolsCode
        {
            get { return timeSheetSymbolsCode; }
            set { timeSheetSymbolsCode = value; }
        }

        public virtual string TimeSheetSymbolsName
        {
            get { return timeSheetSymbolsName; }
            set { timeSheetSymbolsName = value; }
        }


        public virtual decimal? SalaryRate
        {
            get { return salaryRate; }
            set { salaryRate = value; }
        }

        public virtual bool? IsDefault
        {
            get { return isDefault; }
            set { isDefault = value; }
        }

        public virtual bool? IsOverTime
        {
            get { return isOverTime; }
            set { isOverTime = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }
        public virtual bool IsHalfDayDefault
        {
            get;
            set;
        }
        public virtual bool? IsHalfDay
        { /*get => isHalfDay; set => isHalfDay = value;*/
            get { return isHalfDay; }
            set { isHalfDay = value; }
        }
        public virtual int? OverTimeSymbol
        { /*get => overTimeSymbol; set => overTimeSymbol = value;*/
            get { return overTimeSymbol; }
            set { overTimeSymbol = value; }
        }
    }
}
