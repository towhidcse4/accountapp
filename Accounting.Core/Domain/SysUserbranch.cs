using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysUserbranch {
        public virtual System.Guid ID { get; set; }
        public virtual System.Guid UserID { get; set; }
        public virtual System.Nullable<System.Guid> BranchID { get; set; }
    }
}
