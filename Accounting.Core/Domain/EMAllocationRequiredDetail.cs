﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMAllocationRequiredDetail
    {
        private Guid iD;
        private Guid eMAllocationRequiredID;
        private Guid budgetItemID;
        private string budgetItemName;
        private Decimal requestAmount;
        private Decimal aprovedAmount;
        private Decimal amount;
        private Decimal restAmount;
        private string requestDescription;
        private string aprovedDescription;
        private int orderPriority;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid EMAllocationRequiredID
        {
            get { return eMAllocationRequiredID; }
            set { eMAllocationRequiredID = value; }
        }
        public virtual Guid BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }
        public virtual string BudgetItemName
        {
            get { return budgetItemName; }
            set { budgetItemName = value; }
        }
        public virtual Decimal RequestAmount
        {
            get { return requestAmount; }
            set { requestAmount = value; }
        }
        public virtual Decimal AprovedAmount
        {
            get { return aprovedAmount; }
            set { aprovedAmount = value; }
        }
        public virtual Decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual Decimal RestAmount
        {
            get { return restAmount; }
            set { restAmount = value; }
        }

        public virtual string RequestDescription
        {
            get { return requestDescription; }
            set { requestDescription = value; }
        }
        public virtual string AprovedDescription
        {
            get { return aprovedDescription; }
            set { aprovedDescription = value; }
        }
        public virtual int OrderPriority
        {
            get { return OrderPriority; }
            set { OrderPriority = value; }
        }
    }
}
