

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MBInternalTransfer
    {
        private int typeID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private Guid iD;
        private bool recorded;
        private bool exported;
        private Guid branchID;
        private string reason;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private Guid toBranchID;

        public virtual string CurrencyID { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual decimal TotalAmount
        {
            get
            {
                if (MBInternalTransferDetails.Count > 0)
                    return totalAmount = MBInternalTransferDetails.Sum(x => x.Amount);
                else return totalAmount;
            }
            set { totalAmount = value; }
        }

        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual Guid ToBranchID
        {
            get { return toBranchID; }
            set { toBranchID = value; }
        }

        public virtual Guid? TemplateID { get; set; }

        public MBInternalTransfer()
        {
            MBInternalTransferDetails = new List<MBInternalTransferDetail>();
            MBInternalTransferTaxs = new List<MBInternalTransferTax>();
            RefVouchers = new List<RefVoucher>();
        }
        public virtual IList<MBInternalTransferDetail> MBInternalTransferDetails { get; set; }
        public virtual IList<MBInternalTransferTax> MBInternalTransferTaxs { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }


    }
}
