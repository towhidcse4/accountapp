using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TemplateDetail
    {
        public TemplateDetail()
        {
            TemplateColumns = new List<TemplateColumn>();
        }

        public virtual IList<TemplateColumn> TemplateColumns { get; set; }
        public virtual Guid ID { get; set; }
        public virtual Guid TemplateID { get; set; }
        public virtual int GridType { get; set; }
        public virtual int TabIndex { get; set; }
        public virtual string TabCaption { get; set; }
    }
}
