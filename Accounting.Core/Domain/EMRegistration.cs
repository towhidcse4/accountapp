﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMRegistration
    {
        private Guid iD;
        private string registrationCode;
        private string registrationName;
        private Guid? eMPublishPeriodID;
        private string eMPublishPeriodName;
        private Guid? registrationGroupID;
        private int typeID;
        private string address;
        private string tel;
        private string fax;
        private string email;
        private string website;
        private string businessRegistrationNumber;
        private string businessRegistrationIssueBy;
        private DateTime businessRegistrationIssueDate;
        private Guid? bankAccountDetailID;
        private string bankName;
        private string contactTaxCode;
        private string contactName;
        private string contactTile;
        private string contactPrefix;
        private string contactMobile;
        private string contactEmail;
        private string contactOfficeTel;
        private string contactHomeTel;
        private string contactAddress;
        private bool isPersonal;
        private string contactIdentificationNo;
        private DateTime contactIssueDate;
        private string contactIssueBy;
        private bool status;
        private Guid? eMShareHolderID;
        private decimal totalQuantityBuyable;
        private decimal totalQuantityRegisted;
        private decimal totalQuantityApproved;
        private decimal totalQuantityBought;
        private decimal totalAmountBought;
        private bool recorded;
        public EMRegistration()
        {

            EMRegistrationDetails = new List<EMRegistrationDetail>();

        }
        public virtual IList<EMRegistrationDetail> EMRegistrationDetails { get; set; }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual string RegistrationCode
        {
            get {return registrationCode;}
            set { registrationCode = value; }
        }
        public virtual string RegistrationName
        {
            get { return registrationName; }
            set { registrationName = value; }
        }
        public virtual Guid? EMPublishPeriodID
        {
            get { return eMPublishPeriodID; }
            set { eMPublishPeriodID = value; }
        }
        public virtual string EMPublishPeriodName
        {
            get { return eMPublishPeriodName; }
            set { eMPublishPeriodName = value; }
        }
        public virtual Guid? RegistrationGroupID
        {
            get { return registrationGroupID; }
            set { registrationGroupID = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public virtual string Tel
        {
            get { return tel; }
            set { tel = value; }
        }
        public virtual string Fax
        {
            get { return fax; }
            set { fax = value; }
        }
        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }
        public virtual string Website
        {
            get {return website;}
            set { website = value; }
        }
        public virtual string BusinessRegistrationNumber
        {
            get { return businessRegistrationNumber; }
            set { businessRegistrationNumber = value; }
        }
        public virtual string BusinessRegistrationIssueBy
        {
            get { return businessRegistrationIssueBy; }
            set { businessRegistrationIssueBy = value; }
        }
        public virtual DateTime BusinessRegistrationIssueDate
        {
            get { return businessRegistrationIssueDate; }
            set { businessRegistrationIssueDate = value; }
        }
        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }
        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }
        public virtual string ContactTaxCode
        {
            get { return contactTaxCode; }
            set { contactTaxCode = value; }
        }
        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }
        public virtual string ContactTile
        {
            get { return contactTile; }
            set { contactTile = value; }
        }
        public virtual string ContactPrefix
        {
            get { return contactPrefix; }
            set { contactPrefix = value; }
        }
        public virtual string ContactMobile
        {
            get { return contactMobile; }
            set { contactMobile = value; }
        }
        public virtual string ContactEmail
        {
            get { return contactEmail; }
            set { contactEmail = value; }
        }
        public virtual string ContactOfficeTel
        {
            get { return contactOfficeTel; }
            set { contactOfficeTel = value; }
        }
        public virtual string ContactHomeTel
        {
            get { return contactHomeTel; }
            set { contactHomeTel = value; }
        }
        public virtual string ContactAddress
        {
            get { return contactAddress; }
            set { contactAddress = value; }
        }
        public virtual bool IsPersonal
        {
            get { return isPersonal; }
            set { isPersonal = value; }
        }
        public virtual string ContactIdentificationNo
        {
            get { return contactIdentificationNo; }
            set { contactIdentificationNo = value; }
        }
        public virtual DateTime ContactIssueDate
        {
            get { return contactIssueDate; }
            set { contactIssueDate = value; }
        }
        public virtual string ContactIssueBy
        {
            get { return contactIssueBy; }
            set { contactIssueBy = value; }
        }
        public virtual bool Status
        {
            get { return status; }
            set { status = value; }
        }
        public virtual Guid? EMShareHolderID
        {
            get { return eMShareHolderID; }
            set { eMShareHolderID = value; }
        }
        public virtual decimal TotalQuantityBuyable
        {
            get { return totalQuantityBuyable; }
            set { totalQuantityBuyable = value; }
        }
        public virtual decimal TotalQuantityRegisted
        {
            get { return totalQuantityRegisted; }
            set { totalQuantityRegisted = value; }
        }
        public virtual decimal TotalQuantityApproved
        {
            get { return totalQuantityApproved; }
            set { totalQuantityApproved = value; }
        }
        public virtual decimal TotalQuantityBought
        {
            get { return totalQuantityBought; }
            set { totalQuantityBought = value; }
        }
        public virtual decimal TotalAmountBought
        {
            get { return totalAmountBought; }
            set { totalAmountBought = value; }
        }
        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        private bool isSelect = false;
        public virtual bool IsSelect
        {
            get { return isSelect; }
            set { isSelect = value; }
        }

    }
}