﻿using System;

namespace Accounting.Core.Domain
{
    public class TT153Report
    {
        private Guid _ID;
        private string _ReportName;
        private int _InvoiceType;
        private int _InvoiceForm;
        private String _InvoiceTemplate;
        private Guid _InvoiceTypeID;
        private string _InvoiceSeries;
        private string _RootReportID;
        private Byte[] _TT153ReportFile;
        private Decimal _CopyNumber;
        private int _TempSortOrder;
        private Boolean _IsInPlace;
        private string _Purpose1;
        private string _CodeColor1;
        private string _Purpose2;
        private string _CodeColor2;
        private string _Purpose3;
        private string _CodeColor3;
        private string _Purpose4;
        private string _CodeColor4;
        private string _Purpose5;
        private string _CodeColor5;
        private string _Purpose6;
        private string _CodeColor6;
        private string _Purpose7;
        private string _CodeColor7;
        private string _Purpose8;
        private string _CodeColor8;
        private string _Purpose9;
        private string _CodeColor9;
        private string _TT156ReportFilePath;
        private string _TT156ReportFileName;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string ReportName
        {
            get { return _ReportName; }
            set { _ReportName = value; }
        }

        public virtual int InvoiceType
        {
            get { return _InvoiceType; }
            set { _InvoiceType = value; }
        }

        public virtual int InvoiceForm
        {
            get { return _InvoiceForm; }
            set { _InvoiceForm = value; }
        }

        public virtual string InvoiceTemplate
        {
            get { return _InvoiceTemplate; }
            set { _InvoiceTemplate = value; }
        }
        public virtual Guid InvoiceTypeID
        {
            get { return _InvoiceTypeID; }
            set { _InvoiceTypeID = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return _InvoiceSeries; }
            set { _InvoiceSeries = value; }
        }

        public virtual string RootReportID
        {
            get { return _RootReportID; }
            set { _RootReportID = value; }
        }

        public virtual Byte[] TT153ReportFile
        {
            get { return _TT153ReportFile; }
            set { _TT153ReportFile = value; }
        }

        public virtual Decimal CopyNumber
        {
            get { return _CopyNumber; }
            set { _CopyNumber = value; }
        }

        public virtual int TempSortOrder
        {
            get { return _TempSortOrder; }
            set { _TempSortOrder = value; }
        }

        public virtual Boolean IsInPlace
        {
            get { return _IsInPlace; }
            set { _IsInPlace = value; }
        }

        public virtual string Purpose1
        {
            get { return _Purpose1; }
            set { _Purpose1 = value; }
        }

        public virtual string CodeColor1
        {
            get { return _CodeColor1; }
            set { _CodeColor1 = value; }
        }

        public virtual string Purpose2
        {
            get { return _Purpose2; }
            set { _Purpose2 = value; }
        }

        public virtual string CodeColor2
        {
            get { return _CodeColor2; }
            set { _CodeColor2 = value; }
        }

        public virtual string Purpose3
        {
            get { return _Purpose3; }
            set { _Purpose3 = value; }
        }

        public virtual string CodeColor3
        {
            get { return _CodeColor3; }
            set { _CodeColor3 = value; }
        }

        public virtual string Purpose4
        {
            get { return _Purpose4; }
            set { _Purpose4 = value; }
        }

        public virtual string CodeColor4
        {
            get { return _CodeColor4; }
            set { _CodeColor4 = value; }
        }

        public virtual string Purpose5
        {
            get { return _Purpose5; }
            set { _Purpose5 = value; }
        }

        public virtual string CodeColor5
        {
            get { return _CodeColor5; }
            set { _CodeColor5 = value; }
        }

        public virtual string Purpose6
        {
            get { return _Purpose6; }
            set { _Purpose6 = value; }
        }

        public virtual string CodeColor6
        {
            get { return _CodeColor6; }
            set { _CodeColor6 = value; }
        }

        public virtual string Purpose7
        {
            get { return _Purpose7; }
            set { _Purpose7 = value; }
        }

        public virtual string CodeColor7
        {
            get { return _CodeColor7; }
            set { _CodeColor7 = value; }
        }

        public virtual string Purpose8
        {
            get { return _Purpose8; }
            set { _Purpose8 = value; }
        }

        public virtual string CodeColor8
        {
            get { return _CodeColor8; }
            set { _CodeColor8 = value; }
        }

        public virtual string Purpose9
        {
            get { return _Purpose9; }
            set { _Purpose9 = value; }
        }

        public virtual string CodeColor9
        {
            get { return _CodeColor9; }
            set { _CodeColor9 = value; }
        }

        public virtual string TT156ReportFilePath
        {
            get { return _TT156ReportFilePath; }
            set { _TT156ReportFilePath = value; }
        }

        public virtual string TT156ReportFileName
        {
            get { return _TT156ReportFileName; }
            set { _TT156ReportFileName = value; }
        }

        public virtual InvoiceType InvoiceTypeS { get; set; }

        public virtual string InvoiceTypeString
        {
            get
            {
                if (InvoiceTypeS == null) return "";
                try
                {
                    return InvoiceTypeS.InvoiceTypeName;
                }
                catch (Exception)
                {
                    return "";
                    throw;
                }
            }
        }
        public virtual string InvoiceFormString
        {
            get
            {
                if (_InvoiceForm == 2) return "Hóa đơn điện tử";
                else if (_InvoiceForm == 1) return "Hóa đơn đặt in";
                else if (_InvoiceForm == 0) return "Hóa đơn tự in";
                else return null;
            }
        }

        public virtual int CopyNumber_Int
        {
            get
            {
                return Convert.ToInt32(_CopyNumber);
            }
        }
    }
}
