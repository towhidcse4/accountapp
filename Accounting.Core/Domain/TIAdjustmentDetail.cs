using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public partial class TIAdjustmentDetail {
        public virtual Guid ID { get; set; }
        public virtual Guid TIAdjustmentID { get; set; }
        public virtual Guid? ToolsID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal Amount { get; set; }
       
        public virtual decimal AllocatedAmount { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? TIIncrementID { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual int? ConfrontTypeID { get; set; }
        public virtual int? OrderPriority { get; set; }

        public virtual decimal? RemainingAmount { get; set; }
        public virtual decimal? NewRemainingAmount { get; set; }
        public virtual decimal? DifferRemainingAmount { get; set; }

        public virtual decimal? RemainAllocationTimes { get; set; }
        public virtual decimal? NewRemainAllocationTimes { get; set; }
        public virtual decimal? DifferRemainAllocationTimes { get; set; }
    }
}
