using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SABillDetail
    {
        private Guid _ID;
        private Guid? _SABillID;
        private Guid? _MaterialGoodsID;
        private string _Description;
        private string _Unit;
        private Decimal _Quantity;
        private Decimal _UnitPrice;
        private Decimal _UnitPriceOriginal;
        private Decimal _Amount;
        private Decimal _AmountOriginal;
        private Decimal? _DiscountRate;
        private Decimal _DiscountAmount;
        private Decimal _DiscountAmountOriginal;
        private string _DiscountAccount;
        private Decimal? _VATRate;
        private Decimal _VATAmount;
        private Decimal _VATAmountOriginal;
        private string _VATAccount;
        private string _RepositoryAccount;
        private string _CostAccount;
        private Guid? _AccountingObjectID;
        private Guid? _CostSetID;
        private Guid? _ContractID;
        private Guid? _StatisticsCodeID;
        private Guid? _DepartmentID;
        private Guid? _ExpenseItemID;
        private Guid? _BudgetItemID;
        private string _CustomProperty1;
        private string _CustomProperty2;
        private string _CustomProperty3;
        private int? _OrderPriority;
        private Guid _DetailID;

        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual bool? IsPromotion { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? SABillID
        {
            get { return _SABillID; }
            set { _SABillID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Decimal UnitPriceOriginal
        {
            get { return _UnitPriceOriginal; }
            set { _UnitPriceOriginal = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Decimal AmountOriginal
        {
            get { return _AmountOriginal; }
            set { _AmountOriginal = value; }
        }

        public virtual Decimal? DiscountRate
        {
            get { return _DiscountRate; }
            set { _DiscountRate = value; }
        }

        public virtual Decimal DiscountAmount
        {
            get { return _DiscountAmount; }
            set { _DiscountAmount = value; }
        }

        public virtual Decimal DiscountAmountOriginal
        {
            get { return _DiscountAmountOriginal; }
            set { _DiscountAmountOriginal = value; }
        }

        public virtual string DiscountAccount
        {
            get { return _DiscountAccount; }
            set { _DiscountAccount = value; }
        }

        public virtual Decimal? VATRate
        {
            get { return _VATRate; }
            set { _VATRate = value; }
        }

        public virtual Decimal VATAmount
        {
            get { return _VATAmount; }
            set { _VATAmount = value; }
        }

        public virtual Decimal VATAmountOriginal
        {
            get { return _VATAmountOriginal; }
            set { _VATAmountOriginal = value; }
        }

        public virtual string VATAccount
        {
            get { return _VATAccount; }
            set { _VATAccount = value; }
        }

        public virtual string RepositoryAccount
        {
            get { return _RepositoryAccount; }
            set { _RepositoryAccount = value; }
        }

        public virtual string CostAccount
        {
            get { return _CostAccount; }
            set { _CostAccount = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _StatisticsCodeID; }
            set { _StatisticsCodeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return _BudgetItemID; }
            set { _BudgetItemID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _CustomProperty1; }
            set { _CustomProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _CustomProperty2; }
            set { _CustomProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _CustomProperty3; }
            set { _CustomProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }

        public virtual Guid DetailID
        {
            get { return _DetailID; }
            set { _DetailID = value; }
        }
        public virtual MaterialGoods MaterialGoods { get; set; }
        private string materialGoodsName;
        public virtual string MaterialGoodsName
        {
            get
            {
                if (MaterialGoods != null)
                    return MaterialGoods.MaterialGoodsName;
                else
                    return "";
            }
            set { materialGoodsName = value; }
        }
        public SABillDetail()
        {
            IsPromotion = false;
        }

    }
}
