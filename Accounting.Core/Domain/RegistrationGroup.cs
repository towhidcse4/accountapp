using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RegistrationGroup
    {
        private Guid iD;
        private string registrationGroupCode;
        private string registrationGroupName;        
        private Guid? parentID;
        private bool isParentNode;
        private int grade;
        private string orderFixCode;        
        private bool isActive;
        private string isActiveString;   
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string RegistrationGroupCode
        {
            get { return registrationGroupCode; }
            set { registrationGroupCode = value; }
        }

        public virtual string RegistrationGroupName
        {
            get { return registrationGroupName; }
            set { registrationGroupName = value; }
        }      

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }


        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
        public virtual int  Grade
        {
            get { return grade; }
            set { grade = value; }
        }
        
        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        public virtual string IsActiveString
        {
            get { return isActiveString; }
            set { isActiveString = value; }
        }
    }
}
