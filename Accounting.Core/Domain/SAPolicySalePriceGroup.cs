using System;

namespace Accounting.Core.Domain
{
    public class SAPolicySalePriceGroup
    {
        private Guid _ID;
        private Guid _SAPolicyPriceSettingID;
        private Guid _SalePriceGroupID;
        private int _BasedOn;
        private int _Method;
        //private Decimal _AmountAdjust;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid SAPolicyPriceSettingID
        {
            get { return _SAPolicyPriceSettingID; }
            set { _SAPolicyPriceSettingID = value; }
        }

        public virtual Guid SalePriceGroupID
        {
            get { return _SalePriceGroupID; }
            set { _SalePriceGroupID = value; }
        }

        public virtual int BasedOn
        {
            get { return _BasedOn; }
            set { _BasedOn = value; }
        }

        public virtual int Method
        {
            get { return _Method; }
            set { _Method = value; }
        }

        public virtual Decimal? AmountAdjust { get; set; }        
    }
}
