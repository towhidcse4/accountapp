using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPOPN
    {
        private Guid _ID;
        private Guid? _BranchID;
        private int _ObjectType;
        private Guid? _CostSetID;
        private Guid? _ContractID;
        private Decimal? _DirectMaterialAmount;
        private Decimal? _DirectLaborAmount;
        private Decimal? _GeneralExpensesAmount;
        private Decimal? _TotalCostAmount;
        private Decimal? _AcceptedAmount;
        private Decimal? _NotAcceptedAmount;
        private string _UncompletedAccount;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid? BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int ObjectType
        {
            get { return _ObjectType; }
            set { _ObjectType = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Decimal? DirectMaterialAmount
        {
            get { return _DirectMaterialAmount; }
            set { _DirectMaterialAmount = value; }
        }

        public virtual Decimal? DirectLaborAmount
        {
            get { return _DirectLaborAmount; }
            set { _DirectLaborAmount = value; }
        }

        public virtual Decimal? GeneralExpensesAmount
        {
            get { return _GeneralExpensesAmount; }
            set { _GeneralExpensesAmount = value; }
        }

        public virtual Decimal? TotalCostAmount
        {
            get { return _TotalCostAmount; }
            set { _TotalCostAmount = value; }
        }

        public virtual Decimal? AcceptedAmount
        {
            get { return _AcceptedAmount; }
            set { _AcceptedAmount = value; }
        }

        public virtual Decimal? NotAcceptedAmount
        {
            get { return _NotAcceptedAmount; }
            set { _NotAcceptedAmount = value; }
        }

        public virtual string UncompletedAccount
        {
            get { return _UncompletedAccount; }
            set { _UncompletedAccount = value; }
        }
        public virtual CostSet CostSets { get; set; }
        public virtual string CostSetCode { get { return CostSets.CostSetCode; } }
        public virtual string CostSetName { get { return CostSets.CostSetName; } }
        public virtual EMContract EMContractSales { get; set; }
        public virtual string Code { get { return EMContractSales.Code; } }
        //public virtual string Name { get { return EMContractSales.Name; } }
        public virtual DateTime? SignedDate { get { return EMContractSales.SignedDate; } }
        public virtual string AccountingObjectName { get { return EMContractSales.AccountingObjectName; } }
        public CPOPN()
        {
            CostSets = new CostSet();
            EMContractSales = new EMContract();
        }

    }
}
