﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class CTBook
    {
        public Guid CostSetID { get; set; }
        public Guid RefID { get; set; }
        public int RefType { get; set; }
        public string CostSetCode { get; set; }
        public string CostSetName { get; set; }
       
        public DateTime? PostedDate { get; set; }
        public DateTime? Date { get; set; }
        public string No { get; set; }
        public string Reason { get; set; }
        public decimal? NVLTT { get; set; }
        public decimal? NCTT { get; set; }
        public decimal? CPSXC { get; set; }
        public decimal? Cong { get; set; }
        public string AccountingObjectCode { get; set; }
        public string AccountingObjectName { get; set; }
        public decimal? DoanhThu { get; set; }
        public decimal? GiamTru { get; set; }
        public decimal? ChiPhi { get; set; }
        public decimal? GiaVon { get; set; }
        public decimal? DoDang { get; set; }
        public decimal? LaiLo { get; set; }

    }
    public class CTBook_Period
    {
        public string Period { get; set; }
    }
}
