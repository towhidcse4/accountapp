﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMPublishPeriodDetail
    {
        private Guid iD;
        private Guid ? eMPublishPeriodID;
        private Guid ? stockCategoryID;
        private decimal unitPrice;
        private decimal quantity;
        private decimal amount;
        private decimal limitTransferYear;
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid? EMPublishPeriodID
        {
            get { return eMPublishPeriodID; }
            set { eMPublishPeriodID = value; }
        }
        public virtual Guid? StockCategoryID
        {
            get { return stockCategoryID; }
            set { stockCategoryID = value; }
        }
        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }
        public virtual decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public virtual decimal LimitTransferYear
        {
            get { return limitTransferYear; }
            set { limitTransferYear = value; }
        }
        
        
    
        
    



    }
}