using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class SupplierService
    {
        private Guid _ID;
        private string _SupplierServiceCode;
        private string _SupplierServiceName;
        private string _PathAccess;
        public virtual IList<ApiService> ApiService { get; set; }

        public SupplierService()
        {
            ApiService = new List<ApiService>();
        }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string SupplierServiceCode
        {
            get { return _SupplierServiceCode; }
            set { _SupplierServiceCode = value; }
        }

        public virtual string SupplierServiceName
        {
            get { return _SupplierServiceName; }
            set { _SupplierServiceName = value; }
        }

        public virtual string PathAccess
        {
            get { return _PathAccess; }
            set { _PathAccess = value; }
        }


    }
}
