﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FATransferDetail
    {
        private Guid _id;
        private Guid _faTransferID;
        private Guid? _fixedAssetID;
        private string _description;
        private Guid? _fromDepartment;
        private Guid? _toDepartment;
        private Guid _employeeID;
        private Guid? _statisticsCodeID;
        private Guid? _costSetID;
        private Guid? _expenseItemID;
        private Guid? _budgetItemID;
        private string _costAccount;
        private string _customProperty1;
        private string _customProperty2;
        private string _customProperty3;
        private int _orderPriority;
        private decimal? _amount;

        public virtual decimal? Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public virtual Guid ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual Guid FATransferID
        {
            get { return _faTransferID; }
            set { _faTransferID = value; }
        }

        public virtual Guid? FixedAssetID
        {
            get { return _fixedAssetID; }
            set { _fixedAssetID = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual Guid? FromDepartment
        {
            get { return _fromDepartment; }
            set { _fromDepartment = value; }
        }

        public virtual Guid? ToDepartment
        {
            get { return _toDepartment; }
            set { _toDepartment = value; }
        }

        public virtual Guid EmployeeID
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return _statisticsCodeID; }
            set { _statisticsCodeID = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return _costSetID; }
            set { _costSetID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _expenseItemID; }
            set { _expenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return _budgetItemID; }
            set { _budgetItemID = value; }
        }

        public virtual string CostAccount
        {
            get { return _costAccount; }
            set { _costAccount = value; }
        }

        public virtual string CustomProperty1
        {
            get { return _customProperty1; }
            set { _customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return _customProperty2; }
            set { _customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return _customProperty3; }
            set { _customProperty3 = value; }
        }

        public virtual int OrderPriority
        {
            get { return _orderPriority; }
            set { _orderPriority = value; }
        }

    }
}
