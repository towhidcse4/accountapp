using System;

namespace Accounting.Core.Domain
{
public class SAPolicySalePriceCustomer
{
private Guid _ID;
private Guid _SAPolicySalePriceGroupID;
private Guid _AccountingObjectID;


public virtual Guid ID
{
get { return _ID; }
set { _ID = value; }
}

public virtual Guid SAPolicySalePriceGroupID
{
get { return _SAPolicySalePriceGroupID; }
set { _SAPolicySalePriceGroupID = value; }
}

public virtual Guid AccountingObjectID
{
get { return _AccountingObjectID; }
set { _AccountingObjectID = value; }
}


}
}
