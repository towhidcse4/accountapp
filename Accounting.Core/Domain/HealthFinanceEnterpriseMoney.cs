﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class HealthFinanceEnterpriseMoney
    {
        public virtual decimal tySoNo { get; set; }
        public virtual decimal tyTaiTro { get; set; }
        public virtual decimal vonLuanChuyen { get; set; }
        public virtual decimal hsTTNganHan { get; set; }
        public virtual decimal hsTTNhanh { get; set; }
        public virtual decimal hsTTTucThoi { get; set; }
        public virtual decimal hsTTChung { get; set; }
        public virtual decimal hsQVHangTonKho { get; set; }
        public virtual decimal hsLNTrenVonKD { get; set; }
        public virtual decimal hsLNTrenDTThuan { get; set; }
        public virtual decimal hsLNTrenVonCSH { get; set; }
        public virtual decimal tienMat { get; set; }
        public virtual decimal tienGui { get; set; }
        public virtual decimal doanhThu { get; set; }
        public virtual decimal chiPhi { get; set; }
        public virtual decimal loiNhuanTruocThue { get; set; }
        public virtual decimal phaiThu { get; set; }
        public virtual decimal phaiTra { get; set; }
        public virtual decimal hangTonKho { get; set; }
    }
    public class MCReceipt_Model
    {
        public virtual int? AccountingObjectType { get; set; }
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaDoiTuong { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string Payers { get; set; }
        public virtual string Reason { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
    }
    public class MCPayment_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual int? AccountingObjectType { get; set; }
        public virtual string MaDoiTuong { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string Receiver { get; set; }
        public virtual string Reason { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string Description { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        //public virtual int? InvoiceType { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string MaNhomHHDV { get; set; }
    }
    public class MBTellerPaper_Model
    {
        public virtual int? LoaiGiayBN { get; set; }
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual int? AccountingObjectType { get; set; }
        public virtual string MaTKChi { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MaDoiTuong { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string MaTKNguoiNhan { get; set; }
        public virtual string Receiver { get; set; }
        public virtual string IdentificationNo { get; set; }
        public virtual DateTime? IssueDate { get; set; }
        public virtual string IssueBy { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
        public virtual string Description { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? PretaxAmount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        //public virtual int? InvoiceType { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string MaNhomHHDV { get; set; }
    }
    public class MBDeposit_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual int? AccountingObjectType { get; set; }
        public virtual string MaDoiTuong { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string SoTKHuong { get; set; }
        public virtual string Reason { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        //public virtual int? InvoiceType { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
    }
    public class MBCreditCard_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual int? AccountingObjectType { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string CreditCardNumber { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MaDoiTuong { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
        public virtual string Description { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? PretaxAmountOriginal { get; set; }
        public virtual decimal? PretaxAmount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string MaNhomHHDV { get; set; }
    }
    public class MBInternalTransfer_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string Reason { get; set; }
        public virtual string TKNHChuyen { get; set; }
        public virtual string TKNHNhan { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string MaNhanVien { get; set; }

    }
    public class MCAudit_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string Description { get; set; }
        public virtual string Summary { get; set; }
        public virtual decimal? ValueOfMoney { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual string Description2 { get; set; }
        public virtual string MaNhanVien { get; set; }

    }
    public class PPOrder_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string MaDoiTuong { get; set; }
        public virtual string ShippingPlace { get; set; }
        public virtual string Reason { get; set; }
        public virtual DateTime? DeliverDate { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string MaHang { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? QuantityReceipt { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
    }
    public class SAQuote_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? FinalDate { get; set; }
        public virtual string MaKhachHang { get; set; }
        public virtual string Reason { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string ContactMobile { get; set; }
        public virtual string ContactEmail { get; set; }
        public virtual string DeliveryTime { get; set; }
        public virtual string GuaranteeDuration { get; set; }
        public virtual string Description { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string DieuKhoanTT { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string MaHang { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
    }
    public class SAOrder_Model
    {
        public virtual DateTime? Date { get; set; }
        public virtual string No { get; set; }
        public virtual string MaKhachHang { get; set; }
        public virtual string DeliveryPlace { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual DateTime? DeliveDate { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string MaHang { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
    }
    public class SAInvoice_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaKhachHang { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string MaDKTT { get; set; }
        public virtual decimal? Exported { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string OutwardNo { get; set; }
        public virtual string SReason { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual string PaymentMethod { get; set; }
        public virtual string MaHang { get; set; }
        public virtual decimal? IsPromotion { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual string SAOrderNo { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? ExportTaxRate { get; set; }
        public virtual decimal? ExportTaxAmount { get; set; }
        public virtual string ExportTaxAccount { get; set; }
        public virtual string ExportTaxAccountCorresponding { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual string RepositoryAccount { get; set; }
        public virtual string CostAccount { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaThongKe { get; set; }
    }
    public class BHTHUTIENNGAY_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaKhachHang { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string MaDKTT { get; set; }
        public virtual decimal? Exported { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string OutwardNo { get; set; }
        public virtual string SReason { get; set; }
        public virtual string SContactName { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string PhuongThucTT { get; set; }
        public virtual string MNo { get; set; }
        public virtual string MReasonPay { get; set; }
        public virtual string MContactName { get; set; }
        public virtual string MaTKNopTien { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual string PaymentMethod { get; set; }
        public virtual string MaHang { get; set; }
        public virtual decimal? IsPromotion { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual string SAOrderNo { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? ExportTaxRate { get; set; }
        public virtual decimal? ExportTaxAmount { get; set; }
        public virtual string ExportTaxAccount { get; set; }
        public virtual string ExportTaxAccountCorresponding { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual string RepositoryAccount { get; set; }
        public virtual string CostAccount { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaThongke { get; set; }

    }
    public class BHDAILYTHUDUNGGIA_Model
    {
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaKhachHang { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual string MaDKTT { get; set; }
        public virtual decimal? Exported { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string OutwardNo { get; set; }
        public virtual string SReason { get; set; }
        public virtual string SContactName { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string PhuongThucTT { get; set; }
        public virtual string MNo { get; set; }
        public virtual string MReasonPay { get; set; }
        public virtual string MContactName { get; set; }
        public virtual string MaTKNopTien { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string AccountingObjectBankAccount { get; set; }
        public virtual string AccountingObjectBankName { get; set; }
        public virtual string PaymentMethod { get; set; }
        public virtual string MaHang { get; set; }
        public virtual decimal? IsPromotion { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual string SAOrderNo { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? ExportTaxRate { get; set; }
        public virtual decimal? ExportTaxAmount { get; set; }
        public virtual string ExportTaxAccount { get; set; }
        public virtual string ExportTaxAccountCorresponding { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual string RepositoryAccount { get; set; }
        public virtual string CostAccount { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaThongke { get; set; }
    }
    public class MuaHangQuaKho_Model
    {
        public virtual string PhuongThucTT { get; set; }
        public virtual string InwardNo { get; set; }
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaNCC { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string Reason { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual string MaTKChi { get; set; }
        public virtual string MReasonPay { get; set; }
        public virtual string MaTKHuong { get; set; }
        public virtual string IdentificationNo { get; set; }
        public virtual DateTime? IssueDate { get; set; }
        public virtual string IssueBy { get; set; }
        public virtual string CreditCardNumber { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string MaHang { get; set; }
        public virtual string MaKho { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual decimal? FreightAmount { get; set; }
        public virtual decimal? FreightAmountOriginal { get; set; }
        public virtual decimal? SpecialConsumeTaxAmountOriginal { get; set; }
        public virtual decimal? InwardAmount { get; set; }
        public virtual decimal? InwardAmountOriginal { get; set; }
        public virtual decimal? ImportTaxAmountOriginal { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string PPOrderNo { get; set; }
        public virtual decimal? ImportTaxExpenseAmount { get; set; }
        public virtual decimal? ImportTaxExpenseAmountOriginal { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? ImportTaxRate { get; set; }
        public virtual decimal? ImportTaxAmount { get; set; }
        public virtual string ImportTaxAccount { get; set; }
        public virtual decimal? SpecialConsumeTaxRate { get; set; }
        public virtual decimal? SpecialConsumeTaxAmount { get; set; }
        public virtual string SpecialConsumeTaxAccount { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual string DeductionDebitAccount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string MaNhomHHDV { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }

    }
    public class MuaHangKhongQuaKho_Model
    {
        public virtual string PhuongThucTT { get; set; }
        public virtual string No { get; set; }
        public virtual string InwardNo { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaNCC { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string Reason { get; set; }
        public virtual string MReasonPay { get; set; }
        public virtual string MaNhanVien { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string OriginalNo { get; set; }
        public virtual DateTime? MPostedDate { get; set; }
        public virtual DateTime? MDate { get; set; }
        public virtual string SoTKChi { get; set; }
        public virtual string SoTKHuong { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string IdentificationNo { get; set; }
        public virtual DateTime? IssueDate { get; set; }
        public virtual string IssueBy { get; set; }
        public virtual string CreditCardNumber { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string MaHang { get; set; }
        public virtual string MaKho { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string LotNo { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string PPOrderNo { get; set; }
        public virtual decimal? FreightAmount { get; set; }
        public virtual decimal? FreightAmountOriginal { get; set; }
        public virtual decimal? InwardAmount { get; set; }
        public virtual decimal? InwardAmountOriginal { get; set; }
        public virtual decimal? ImportTaxExpenseAmount { get; set; }
        public virtual decimal? ImportTaxExpenseAmountOriginal { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? ImportTaxRate { get; set; }
        public virtual decimal? ImportTaxAmount { get; set; }
        public virtual string ImportTaxAccount { get; set; }
        public virtual decimal? SpecialConsumeTaxRate { get; set; }
        public virtual decimal? SpecialConsumeTaxAmount { get; set; }
        public virtual decimal? SpecialConsumeTaxAmountOriginal { get; set; }
        public virtual string SpecialConsumeTaxAccount { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual decimal? TotalVATAmountOriginal { get; set; }        
        public virtual string VATAccount { get; set; }
        public virtual string DeductionDebitAccount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string MaNhomHHDV { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }

    }
    public class MuaDichVu_Model
    {
        public virtual string PhuongThucTT { get; set; }
        public virtual string IsFeightService { get; set; }
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual string MaNCC { get; set; }
        public virtual string Reason { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string NumberAttach { get; set; }
        public virtual string TKNHChi { get; set; }
        public virtual string TKHuong { get; set; }
        public virtual string IdentificationNo { get; set; }
        public virtual string IssueBy { get; set; }
        public virtual DateTime? IssueDate { get; set; }
        public virtual string CreditCardNumber { get; set; }
        public virtual string MaDichVu { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? UnitPriceOriginal { get; set; }
        public virtual decimal? AmountOriginal { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual decimal? DiscountAmountOriginal { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual string VATDescription { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? VATAmountOriginal { get; set; }
        public virtual decimal? VATAmount { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual string InvoiceTemplate { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string MaNhomHHDV { get; set; }
        public virtual string MaMucThuChi { get; set; }
        public virtual string MaKhoanMucCP { get; set; }
        public virtual string MaDoiTuongCP { get; set; }
        public virtual string MaHopDong { get; set; }
        public virtual string MaThongKe { get; set; }
        public virtual DateTime? DueDate { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual string MaPhongBan { get; set; }
        public virtual string MaNhanVien { get; set; }
    }
    public class MH_DichVu_model
    {
        public List<PPService> listPPService { get; set; }
        public List<MCPayment> listMCPayment { get; set; }
        public List<MBTellerPaper> listMBTellerPaper { get; set; }
        public List<MBCreditCard> listMBCreditCard { get; set; }
    }

    public class COLUMN_MODEL
    {
        public string key { get; set; }
        public string value { get; set; }
        public int check { get; set; }
    }
    public class BH_ThuNgay_model { 
        public List<SAInvoice> listSAInvoice { get; set; }
        public List<MCReceipt> listMCReceipt { get; set; }
        public List<MBDeposit> listMBDeposit { get; set; }
        public List<RSInwardOutward> list_RSOut { get; set; }
    }
    public class MH_QuaKho_model
    {
        public List<PPInvoice> listPPInvoice { get; set; }
        public List<MCPayment> listMCPayment { get; set; }
        public List<MBTellerPaper> listMBTellerPaper { get; set; }
        public List<MBCreditCard> listMBCreditCard { get; set; }
        public List<RSInwardOutward> listRSInwardOutward { get; set; }
        
    }
}
