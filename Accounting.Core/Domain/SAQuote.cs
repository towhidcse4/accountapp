using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAQuote
    {
        public virtual Guid? TemplateID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal TotalAmountOriginal { get; set; }
        public virtual decimal TotalVATAmount { get; set; }
        public virtual decimal TotalVATAmountOriginal { get; set; }
        public virtual decimal TotalDiscountAmount { get; set; }
        public virtual decimal TotalDiscountAmountOriginal { get; set; }
        private decimal _totalPaymentAmountOriginalStand;
        public virtual decimal TotalPaymentAmountStandOriginal
        {
            get { return TotalAmountOriginal + TotalVATAmountOriginal - TotalDiscountAmountOriginal; }
            set { _totalPaymentAmountOriginalStand = value; }
        }
        private decimal _totalPaymentAmountStand;
        public virtual decimal TotalPaymentAmountStand
        {
            get { return TotalAmount + TotalVATAmount - TotalDiscountAmount; }
            set { _totalPaymentAmountStand = value; }
        }
        public virtual Guid ID { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual string Reason { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal? ExchangeRate { get; set; }
        public virtual Guid? PaymentClauseID { get; set; }
        public virtual DateTime? FinalDate { get; set; }
        public virtual Guid? TranspotMethodID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual string ContactName { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string ContactMobile { get; set; }
        public virtual string ContactEmail { get; set; }
        public virtual string DeliveryTime { get; set; }
        public virtual string GuaranteeDuration { get; set; }
        public virtual string Description { get; set; }
        public SAQuote()
        {
            RefVouchers = new List<RefVoucher>();
            SAQuoteDetails = new List<SAQuoteDetail>();
            //FinalDate = 30;
            TotalAmount = 0;
            TotalAmountOriginal = 0;
            TotalDiscountAmount = 0;
            TotalDiscountAmountOriginal = 0;
            TotalVATAmount = 0;
            TotalVATAmountOriginal = 0;
        }

        public virtual IList<SAQuoteDetail> SAQuoteDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public virtual decimal? MaxVATRate
        {
            get
            {
                decimal? max = null;
                if (SAQuoteDetails.Count > 0)
                {
                    max = SAQuoteDetails.Max(k => k.VATRate);
                }
                return max;
            }
        }
        public virtual decimal? MaxDiscountRate
        {
            get
            {
                decimal? max = null;
                if (SAQuoteDetails.Count > 0)
                {
                    max = SAQuoteDetails.Max(k => k.DiscountRate);
                }
                return max;
            }
        }
    }
}
