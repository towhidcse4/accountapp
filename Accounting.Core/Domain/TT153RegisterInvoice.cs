﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TT153RegisterInvoice
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime _Date;
        private string _No;
        private string _Description;
        private string _Signer;
        private int _Status;
        private string _AttachFileName;
        private Byte[] _AttachFileContent;

        public TT153RegisterInvoice()
        {
            TT153RegisterInvoiceDetail = new List<TT153RegisterInvoiceDetail>();
        }
        public virtual IList<TT153RegisterInvoiceDetail> TT153RegisterInvoiceDetail { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }


        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual string Signer
        {
            get { return _Signer; }
            set { _Signer = value; }
        }
        public virtual int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public virtual string StatusString
        {
            get
            {
                if (_Status == 0) return "Chưa có hiệu lực";
                else if (_Status == 1) return "Đã có hiệu lực";
                else return null;
            }
            //set { _Status = value; }
        }

        public virtual string AttachFileName
        {
            get { return _AttachFileName; }
            set { _AttachFileName = value; }
        }

        public virtual Byte[] AttachFileContent
        {
            get { return _AttachFileContent; }
            set { _AttachFileContent = value; }
        }

        public virtual String Status_String
        {
            get
            {
                if (_Status == 0) return "Chưa có hiệu lực";
                if (_Status == 1) return "Đã có hiệu lực";
                return null;
            }
        }
    }
}
