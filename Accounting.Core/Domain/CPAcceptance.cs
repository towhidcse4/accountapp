using System;
using System.Collections;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPAcceptance
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private DateTime _Date;
        private DateTime _PostedDate;
        private string _No;
        private string _Description;
        private Guid _CPPeriodID;
        private Decimal _TotalAmount;
        private Decimal _TotalAmountOriginal;
        private Decimal _UnitPrice;

        public virtual int OrderPriority { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual Decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public virtual Decimal TotalAmountOriginal
        {
            get { return _TotalAmountOriginal; }
            set { _TotalAmountOriginal = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }
        public virtual IList<CPAcceptanceDetail> CPAcceptanceDetails { get; set; }
        public CPAcceptance()
        {
            CPAcceptanceDetails = new List<CPAcceptanceDetail>();
        }
    }
}

