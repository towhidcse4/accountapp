using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPExpenseList
    {
        private Guid _ID;
        private Guid _CPPeriodID;
        private int _TypeVoucher;
        private Guid _CostSetID;
        private int _TypeID;
        private DateTime _Date;
        private DateTime _PostedDate;
        private string _No;
        private string _Description;
        private Decimal _Amount;
        private Guid _ExpenseItemID;
        private CostSet _Costset;
        private string _CostsetCode;
        private string _CostsetName;
        private ExpenseItem _ExpenseItem;
        private string _ExpenseItemCode;
        private Guid _ContractID;

        public virtual string ContractCode { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual int TypeVoucher
        {
            get { return _TypeVoucher; }
            set { _TypeVoucher = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual Guid ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual string No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Guid ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual CostSet Costset
        {
            get { return _Costset; }
            set { _Costset = value; }
        }

        public virtual string CostsetCode
        {
            get { return _CostsetCode; }
            set { _CostsetCode = value; }
        }

        public virtual string CostsetName
        {
            get { return _CostsetName; }
            set { _CostsetName = value; }
        }

        public virtual ExpenseItem ExpenseItem
        {
            get { return _ExpenseItem; }
            set { _ExpenseItem = value; }
        }

        public virtual string ExpenseItemCode
        {
            get { return _ExpenseItemCode; }
            set { _ExpenseItemCode = value; }
        }
    }
}
