using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPUncompleteDetail
{
private Guid _ID;
        private Guid _CPUncompleteID;
private int _UncompleteType;
private Guid _MaterialGoodsID;
private string _MaterialGoodCode;
private string _MaterialGoodName;
        private Guid _CostSetID;
        private CostSet _Costset;
        private string _CostsetCode;
        private string _CostsetName;
        private Decimal _Quantity;
        private Decimal _PercentComplete;
        private Decimal _UnitPrice;

        public virtual Guid ID
{
get { return _ID; }
set { _ID = value; }
}

        public virtual Guid CPUncompleteID
        {
            get { return _CPUncompleteID; }
            set { _CPUncompleteID = value; }
        }

        public virtual int UncompleteType
{
get { return _UncompleteType; }
set { _UncompleteType = value; }
}
        public virtual Guid MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }
        public virtual string MaterialGoodCode
        {
            get { return _MaterialGoodCode; }
            set { _MaterialGoodCode = value; }
        }

        public virtual string MaterialGoodName
        {
            get { return _MaterialGoodName; }
            set { _MaterialGoodName = value; }
        }
        public virtual Decimal Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        public virtual Decimal PercentComplete
        {
            get { return _PercentComplete; }
            set { _PercentComplete = value; }
        }

        public virtual Decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public virtual Guid CostSetID
        {
            get { return _CostSetID; }
            set { _CostSetID = value; }
        }

        public virtual CostSet Costset
        {
            get { return _Costset; }
            set { _Costset = value; }
        }

        public virtual string CostsetCode
        {
            get { return _CostsetCode; }
            set { _CostsetCode = value; }
        }

        public virtual string CostsetName
        {
            get { return _CostsetName; }
            set { _CostsetName = value; }
        }

    }
}
