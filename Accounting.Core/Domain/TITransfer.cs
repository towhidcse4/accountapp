using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public partial class TITransfer {
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string No { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string Reason { get; set; }
        public virtual decimal TotalTransferQuantity { get; set; }
        public virtual bool Recorded { get; set; }
        public virtual bool Exported { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual string Transferor { get; set; }
        public virtual string Receiver { get; set; }
        public virtual IList<TITransferDetail> TITransferDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public TITransfer()
        {
            RefVouchers = new List<RefVoucher>();
        }
    }
}
