using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class CPAllocationGeneralExpense
    {
        private Guid _ID;
        private Guid _CPPeriodID;
        private Guid _ExpenseItemID;
        private Decimal _TotalCost;
        private Decimal _UnallocatedAmount;
        private Decimal _AllocatedRate;
        private Decimal _AllocatedAmount;
        private int _AllocationMethod;
        private string _AllocationMethodView;
        private ExpenseItem _ExpenseItem;
        private string _ExpenseItemCode;
        private Guid _ReferenceID;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid ReferenceID
        {
            get { return _ReferenceID; }
            set { _ReferenceID = value; }
        }

        public virtual Guid CPPeriodID
        {
            get { return _CPPeriodID; }
            set { _CPPeriodID = value; }
        }

        public virtual Guid ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }

        public virtual Decimal TotalCost
        {
            get { return _TotalCost; }
            set { _TotalCost = value; }
        }

        public virtual Decimal UnallocatedAmount
        {
            get { return _UnallocatedAmount; }
            set { _UnallocatedAmount = value; }
        }

        public virtual Decimal AllocatedRate
        {
            get { return _AllocatedRate; }
            set { _AllocatedRate = value; }
        }

        public virtual Decimal AllocatedAmount
        {
            get { return _AllocatedAmount; }
            set { _AllocatedAmount = value; }
        }

        public virtual int AllocationMethod
        {
            get { return _AllocationMethod; }
            set { _AllocationMethod = value; }
        }
        public virtual string AllocationMethodView
        {
            get { return _AllocationMethodView; }
            set { _AllocationMethodView = value; }
        }
        public virtual ExpenseItem ExpenseItem
        {
            get { return _ExpenseItem; }
            set { _ExpenseItem = value; }
        }

        public virtual string ExpenseItemCode
        {
            get { return _ExpenseItemCode; }
            set { _ExpenseItemCode = value; }
        }
        public virtual IList<CPAllocationGeneralExpenseDetail> CPAllocationGeneralExpenseDetails { get; set; }
        public CPAllocationGeneralExpense()
        {
            CPAllocationGeneralExpenseDetails = new List<CPAllocationGeneralExpenseDetail>();
        }
    }
}
