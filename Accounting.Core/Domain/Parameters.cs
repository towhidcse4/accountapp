using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Parameters
    {
        private Guid iD;
        private string code;
        private string name;
        private string data;
        private bool isActive;
        private DateTime createdDate;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Data
        {
            get { return data; }
            set { data = value; }
        }
        
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        
    }
}
