using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FADecrementDetailPost
    {
        public virtual Guid? ID { get; set; }
        public virtual Guid? FADecrementDetailID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual Decimal Amount { get; set; }
        public virtual Decimal AmountOriginal { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? EmployeeID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual Boolean IsIrrationalCost { get; set; }
        public virtual int OrderPriority { get; set; }
    }
}
