﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSTimeSheetSummary
    {
        private Guid _ID;
        private Guid _BranchID;
        private Guid _TemplateID;
        private int _TypeID;
        private int _Month;
        private int _Year;
        private string _PSTimeSheetSummaryName;
        public virtual IList<PSTimeSheetSummaryDetail> PSTimeSheetSummaryDetails { get; set; }
        private string _Type;
        

        public virtual IList<Guid> Departments { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual Guid TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }

        public virtual string Type
        {
            get { return _TypeID == 810 ? "Tổng hợp chấm công theo ngày" : "Tổng hợp chấm công theo giờ"; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual int Month
        {
            get { return _Month; }
            set { _Month = value; }
        }

        public virtual int Year
        {
            get { return _Year; }
            set { _Year = value; }
        }

        public virtual string PSTimeSheetSummaryName
        {
            get { return _PSTimeSheetSummaryName; }
            set { _PSTimeSheetSummaryName = value; }
        }


    }
}
