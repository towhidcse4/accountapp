using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AccountingObjectBankAccount
    {
        private Guid iD;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        private Guid accountingObjectID;

        public virtual Guid AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }
        private string bankName;

        public virtual string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }
        private string bankBranchName;

        public virtual string BankBranchName
        {
            get { return bankBranchName; }
            set { bankBranchName = value; }
        }
        private string accountHolderName;

        public virtual string AccountHolderName
        {
            get { return accountHolderName; }
            set { accountHolderName = value; }
        }
        private int orderPriority = 0;

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        private string bankAccount;
        public virtual string BankAccount
        {
            get { return bankAccount; }
            set { bankAccount = value; }
        }

        private bool isSelect = false;
        public virtual bool IsSelect
        {
            get { return isSelect; }
            set { isSelect = value; }
        }

    }
}
