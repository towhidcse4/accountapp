﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class TaxSubmit
    {
        public virtual bool icheck { get; set; }
        public virtual string tkThue { get; set; }
        public virtual string dienGiai { get; set; }
        public virtual decimal soTienPhaiNop { get; set; }
        public virtual decimal soTienNopLanNay { get; set; }

        public TaxSubmit()
        {
            icheck = false;
        }
    }
}
