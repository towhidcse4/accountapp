using System;

namespace Accounting.Core.Domain
{
public class SAPolicyPriceTable
{
private Guid _ID;
private Guid _SAPolicyPriceSettingID;
private Guid _SalePiceGroupID;
private Guid _MaterialGoodsID;
private Decimal _Price;
private Decimal _LastPrice;


public virtual Guid ID
{
get { return _ID; }
set { _ID = value; }
}

public virtual Guid SAPolicyPriceSettingID
{
get { return _SAPolicyPriceSettingID; }
set { _SAPolicyPriceSettingID = value; }
}

public virtual Guid SalePiceGroupID
{
get { return _SalePiceGroupID; }
set { _SalePiceGroupID = value; }
}

public virtual Guid MaterialGoodsID
{
get { return _MaterialGoodsID; }
set { _MaterialGoodsID = value; }
}

public virtual Decimal Price
{
get { return _Price; }
set { _Price = value; }
}

public virtual Decimal LastPrice
{
get { return _LastPrice; }
set { _LastPrice = value; }
}


}
}
