﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAAdjustmentDetail
    {
        private Guid iD;
        private Guid? faadjustmentId;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private decimal amount;
        private Guid? costSetID;
        private Guid? accountingObjectID;
        private Guid? statisticsCodeID;
        private Guid? contractID;
        private int adjustBy;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private Guid? budgetItemID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool isIrrationalCost;
        private int orderPriority;

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid? FAAdjustmentID
        {
            get { return faadjustmentId; }
            set { faadjustmentId = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }

        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }

        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual int AdjustBy
        {
            get { return adjustBy; }
            set { adjustBy = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }
        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }
        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }
    }
}
