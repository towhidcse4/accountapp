using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PrepaidExpenseAllocation
    {
        private Guid _ID;
        private Guid _PrepaidExpenseID;
        private Guid? _AllocationObjectID;
        private int _AllocationObjectType;
        private string _AllocationObjectName;
        private Decimal _AllocationRate;
        private string _CostAccount;
        private Guid? _ExpenseItemID;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid PrepaidExpenseID
        {
            get { return _PrepaidExpenseID; }
            set { _PrepaidExpenseID = value; }
        }

        public virtual Guid? AllocationObjectID
        {
            get { return _AllocationObjectID; }
            set { _AllocationObjectID = value; }
        }

        public virtual int AllocationObjectType
        {
            get { return _AllocationObjectType; }
            set { _AllocationObjectType = value; }
        }

        public virtual string AllocationObjectName
        {
            get { return _AllocationObjectName; }
            set { _AllocationObjectName = value; }
        }

        public virtual Decimal AllocationRate
        {
            get { return _AllocationRate; }
            set { _AllocationRate = value; }
        }

        public virtual string CostAccount
        {
            get { return _CostAccount; }
            set { _CostAccount = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }
        public PrepaidExpenseAllocation()
        {
            AllocationRate = 100;
        }

    }
}
