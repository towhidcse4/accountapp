using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAIncrementDetail
    {
        private Guid iD;
        private Guid fAIncrementID;
        private Guid? fixedAssetID;
        private Guid? departmentID;
        private decimal amount;
        private decimal amountOriginal;
        private decimal discountAmount;
        private decimal discountAmountOriginal;
        private decimal importTaxAmount;
        private decimal importTaxAmountOriginal;
        private decimal specialConsumeTaxAmount;
        private decimal specialConsumeTaxAmountOriginal;
        private string specialConsumeTaxAccount;
        private decimal vATAmount;
        private decimal vATAmountOriginal;
        private decimal freightAmount;
        private decimal freightAmountOriginal;
        private decimal orgPrice;
        private decimal orgPriceOriginal;
        private decimal importTaxExpenseAmount;
        private decimal importTaxExpenseAmountOriginal;
        private decimal customUnitPrice;
        private decimal customUnitPriceOriginal;
        private decimal? importTaxRate;
        private bool? isVATPaid;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool? isIrrationalCost;
        private int? orderPriority;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? accountingObjectID;
        private Guid? accountingObjectTaxID;
        private string accountingObjectTaxName;
        private Guid? statisticsCodeID;
        private Guid? paymentID;
        private string companyTaxCode;
        private Guid? invoiceTypeID;
        private Guid? expenseItemID;
        private string vATAccount;
        private string deductionDebitAccount;
        private int? invoiceType;
        private DateTime? invoiceDate;
        private string invoiceNo;
        private string invoiceSeries;
        private Guid? goodsServicePurchaseID;
        private decimal? vATRate;
        private string importTaxAccount;
        private decimal? specialConsumeTaxRate;
        private Guid? budgetItemID;
        private decimal? discountRate;
        private string description;
        private string debitAccount;
        private string creditAccount;
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal CashOutVATAmount { get; set; }
        public virtual decimal CashOutDifferVATAmount { get; set; }
        public virtual decimal TaxExchangeRate { get; set; }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid FAIncrementID
        {
            get { return fAIncrementID; }
            set { fAIncrementID = value; }
        }

        public virtual Guid? FixedAssetID
        {
            get { return fixedAssetID; }
            set { fixedAssetID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public virtual decimal AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value; }
        }

        public virtual decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public virtual decimal DiscountAmountOriginal
        {
            get { return discountAmountOriginal; }
            set { discountAmountOriginal = value; }
        }

        public virtual decimal ImportTaxAmount
        {
            get { return importTaxAmount; }
            set { importTaxAmount = value; }
        }

        public virtual decimal ImportTaxAmountOriginal
        {
            get { return importTaxAmountOriginal; }
            set { importTaxAmountOriginal = value; }
        }

        public virtual decimal SpecialConsumeTaxAmount
        {
            get { return specialConsumeTaxAmount; }
            set { specialConsumeTaxAmount = value; }
        }

        public virtual decimal SpecialConsumeTaxAmountOriginal
        {
            get { return specialConsumeTaxAmountOriginal; }
            set { specialConsumeTaxAmountOriginal = value; }
        }

        public virtual string SpecialConsumeTaxAccount
        {
            get { return specialConsumeTaxAccount; }
            set { specialConsumeTaxAccount = value; }
        }

        public virtual decimal VATAmount
        {
            get { return vATAmount; }
            set { vATAmount = value; }
        }
        public virtual string InvoiceTemplate { get; set; }
        public virtual decimal VATAmountOriginal
        {
            get { return vATAmountOriginal; }
            set { vATAmountOriginal = value; }
        }

        public virtual decimal FreightAmount
        {
            get { return freightAmount; }
            set { freightAmount = value; }
        }

        public virtual decimal FreightAmountOriginal
        {
            get { return freightAmountOriginal; }
            set { freightAmountOriginal = value; }
        }

        public virtual decimal OrgPrice
        {
            get { return orgPrice; }
            set { orgPrice = value; }
        }

        public virtual decimal OrgPriceOriginal
        {
            get { return orgPriceOriginal; }
            set { orgPriceOriginal = value; }
        }

        public virtual decimal ImportTaxExpenseAmount
        {
            get { return importTaxExpenseAmount; }
            set { importTaxExpenseAmount = value; }
        }

        public virtual decimal ImportTaxExpenseAmountOriginal
        {
            get { return importTaxExpenseAmountOriginal; }
            set { importTaxExpenseAmountOriginal = value; }
        }

        public virtual decimal CustomUnitPrice
        {
            get { return customUnitPrice; }
            set { customUnitPrice = value; }
        }

        public virtual decimal CustomUnitPriceOriginal
        {
            get { return customUnitPriceOriginal; }
            set { customUnitPriceOriginal = value; }
        }

        public virtual decimal? ImportTaxRate
        {
            get { return importTaxRate; }
            set { importTaxRate = value; }
        }

        public virtual bool? IsVATPaid
        {
            get { return isVATPaid; }
            set { isVATPaid = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool? IsIrrationalCost
        {
            get { return isIrrationalCost; }
            set { isIrrationalCost = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? AccountingObjectTaxID
        {
            get { return accountingObjectTaxID; }
            set { accountingObjectTaxID = value; }
        }

        public virtual string AccountingObjectTaxName
        {
            get { return accountingObjectTaxName; }
            set { accountingObjectTaxName = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? PaymentID
        {
            get { return paymentID; }
            set { paymentID = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return companyTaxCode; }
            set { companyTaxCode = value; }
        }

        public virtual Guid? InvoiceTypeID
        {
            get { return invoiceTypeID; }
            set { invoiceTypeID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual string VATAccount
        {
            get { return vATAccount; }
            set { vATAccount = value; }
        }

        public virtual string DeductionDebitAccount
        {
            get { return deductionDebitAccount; }
            set { deductionDebitAccount = value; }
        }

        public virtual int? InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public virtual DateTime? InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public virtual string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public virtual string InvoiceSeries
        {
            get { return invoiceSeries; }
            set { invoiceSeries = value; }
        }

        public virtual Guid? GoodsServicePurchaseID
        {
            get { return goodsServicePurchaseID; }
            set { goodsServicePurchaseID = value; }
        }

        public virtual decimal? VATRate
        {
            get { return vATRate; }
            set { vATRate = value; }
        }

        public virtual string ImportTaxAccount
        {
            get { return importTaxAccount; }
            set { importTaxAccount = value; }
        }

        public virtual decimal? SpecialConsumeTaxRate
        {
            get { return specialConsumeTaxRate; }
            set { specialConsumeTaxRate = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual decimal? DiscountRate
        {
            get { return discountRate; }
            set { discountRate = value; }
        }

        public virtual decimal? Quantity { get; set; }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }
        public virtual string VATDescription { get; set; }
    }
}
