﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAReturnDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid SAReturnID { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual decimal UnitPriceOriginal { get; set; }
        public virtual decimal UnitPriceConvert { get; set; }
        public virtual decimal UnitPriceConvertOriginal { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual decimal OWPrice { get; set; }
        public virtual decimal OWPriceOriginal { get; set; }
        public virtual decimal OWAmount { get; set; }
        public virtual decimal OWAmountOriginal { get; set; }
        public virtual decimal SpecialConsumeTaxAmount { get; set; }
        public virtual decimal SpecialConsumeTaxAmountOriginal { get; set; }
        public virtual decimal SpecialConsumeUnitPrice { get; set; }
        public virtual decimal SpecialConsumeUnitPriceOriginal { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual decimal VATAmountOriginal { get; set; }
        public virtual decimal DiscountAmount { get; set; }
        public virtual decimal DiscountAmountOriginal { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual Guid? BankAccountDetailID { get; set; }
        public virtual decimal? ConvertRate { get; set; }
        public virtual bool? IsDeliveryVoucher { get; set; }
        public virtual Guid? SAInvoiceDetailID { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual string CostAccount { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? SAInvoiceID { get; set; }
        public virtual Guid? CareerGroupID { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string LotNo { get; set; }
        public virtual string UnitConvert { get; set; }
        public virtual decimal? SpecialConsumeTaxRate { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual Guid? RepositoryID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string Unit { get; set; }
        public virtual string RepositoryAccount { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? QuantityConvert { get; set; }


        public virtual string VATDescription { get; set; } // diễn giải thuế
        public virtual Guid PurchasePurposeID { get; set; }// nhóm hàng hóa , hóa đơn dịch vụ
        public virtual bool? IsPromotion { get; set; }
        public virtual decimal CashOutExchangeRate { get; set; }
        public virtual decimal CashOutAmount { get; set; }
        public virtual decimal CashOutDifferAmount { get; set; }
        public virtual string CashOutDifferAccount { get; set; }
        public virtual decimal CashOutVATAmount { get; set; }
        public virtual decimal CashOutDifferVATAmount { get; set; }
        public virtual MaterialGoods MaterialGoods { get; set; }
        //public virtual string MaterialGoodsCode
        //{
        //    get { return MaterialGoods.MaterialGoodsCode == null ? null : MaterialGoods.MaterialGoodsCode; }
        //}

        //public virtual string MaterialGoodsName
        //{
        //    get { return MaterialGoods.MaterialGoodsName == null ? null : MaterialGoods.MaterialGoodsCode; }
        //}

        public virtual string MaterialGoodsCode
        {
            get
            {
                if (MaterialGoods != null)
                    return MaterialGoods.MaterialGoodsCode;
                else
                    return "";
            }
        }
        private string materialGoodsName;
        public virtual string MaterialGoodsName
        {
            get
            {
                if (MaterialGoods != null)
                    return MaterialGoods.MaterialGoodsName;
                else
                    return "";
            }
            set { materialGoodsName = value; }
        }
        
        

        public virtual SAInvoice SAInvoice { get; set; }
        public virtual string No { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string ConfrontInvNo { get; set; }
        public virtual DateTime? ConfrontInvDate { get; set; }
    }
}
