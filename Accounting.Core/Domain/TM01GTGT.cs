using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class TM01GTGT
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private string _DeclarationName;
        private string _DeclarationTerm;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private Boolean _IsFirstDeclaration;
        private int? _AdditionTime;
        private DateTime? _AdditionDate;
        private string _Career;
        private Boolean _IsExtend;
        private string _ExtensionCase;
        private Boolean _IsAppendix011GTGT;
        private Boolean _IsAppendix012GTGT;
        private string _CompanyName;
        private string _CompanyTaxCode;
        private string _TaxAgencyTaxCode;
        private string _TaxAgencyName;
        private string _TaxAgencyEmployeeName;
        private string _CertificationNo;
        private string _SignName;
        private DateTime? _SignDate;


       

        public TM01GTGT()
        {
            TM01GTGTDetails = new List<TM01GTGTDetail>();
            TM01GTGTAdjusts = new List<TM01GTGTAdjust>();
          
            TM011GTGTs = new List<TM011GTGT>();
           
            TM012GTGTs = new List<TM012GTGT>();
           
         
        }
       
     
        public virtual IList<TM01GTGTDetail> TM01GTGTDetails { get; set; }
      
        public virtual IList<TM01GTGTAdjust> TM01GTGTAdjusts { get; set; }
        public virtual IList<TM011GTGT> TM011GTGTs { get; set; }
        public virtual IList<TM012GTGT> TM012GTGTs { get; set; }
        
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public virtual string AdditionTerm { get; set; }
        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string DeclarationName
        {
            get { return _DeclarationName; }
            set { _DeclarationName = value; }
        }

        public virtual string DeclarationTerm
        {
            get { return _DeclarationTerm; }
            set { _DeclarationTerm = value; }
        }

        public virtual DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public virtual Boolean IsFirstDeclaration
        {
            get { return _IsFirstDeclaration; }
            set { _IsFirstDeclaration = value; }
        }

        public virtual int? AdditionTime
        {
            get { return _AdditionTime; }
            set { _AdditionTime = value; }
        }

        public virtual DateTime? AdditionDate
        {
            get { return _AdditionDate; }
            set { _AdditionDate = value; }
        }

        public virtual string Career
        {
            get { return _Career; }
            set { _Career = value; }
        }

        public virtual Boolean IsExtend
        {
            get { return _IsExtend; }
            set { _IsExtend = value; }
        }

        public virtual string ExtensionCase
        {
            get { return _ExtensionCase; }
            set { _ExtensionCase = value; }
        }

        public virtual Boolean IsAppendix011GTGT
        {
            get { return _IsAppendix011GTGT; }
            set { _IsAppendix011GTGT = value; }
        }

        public virtual Boolean IsAppendix012GTGT
        {
            get { return _IsAppendix012GTGT; }
            set { _IsAppendix012GTGT = value; }
        }

        public virtual string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return _CompanyTaxCode; }
            set { _CompanyTaxCode = value; }
        }

        public virtual string TaxAgencyTaxCode
        {
            get { return _TaxAgencyTaxCode; }
            set { _TaxAgencyTaxCode = value; }
        }

        public virtual string TaxAgencyName
        {
            get { return _TaxAgencyName; }
            set { _TaxAgencyName = value; }
        }

        public virtual string TaxAgencyEmployeeName
        {
            get { return _TaxAgencyEmployeeName; }
            set { _TaxAgencyEmployeeName = value; }
        }

        public virtual string CertificationNo
        {
            get { return _CertificationNo; }
            set { _CertificationNo = value; }
        }

        public virtual string SignName
        {
            get { return _SignName; }
            set { _SignName = value; }
        }

        public virtual DateTime? SignDate
        {
            get { return _SignDate; }
            set { _SignDate = value; }
        }


    }
}
