using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FixedAsset
    {
        private string fixedAssetCategoryView;
        private string fixedAssetCategoryCode;
        private string maPhongBan;
        public virtual string FixedAssetCategoryView
        {
            get { return FixedAssetCategory != null ? FixedAssetCategory.FixedAssetCategoryName : ""; }
            set { fixedAssetCategoryView = value; }
        }
        public virtual string FixedAssetCategoryCode
        {
            get { return FixedAssetCategory != null ? FixedAssetCategory.FixedAssetCategoryCode : ""; }
            set { fixedAssetCategoryCode = value; }
        }
        public virtual string MaPhongBan
        {
            get { return Department != null ? Department.DepartmentCode : ""; }
            set { maPhongBan = value; }
        }
        public virtual decimal DisposedAmount { get; set; }
        public virtual string FixedAssetCode { get; set; }
        public virtual string FixedAssetName { get; set; }
        public virtual Guid ID { get; set; }
        public virtual Guid FixedAssetCategoryID { get; set; }
        public virtual bool IsDepreciationAllocation { get; set; }
        public virtual bool IsSecondHand { get; set; }
        public virtual bool IsDisplayMonth { get; set; }
        public virtual bool IsCreateFromOldDatabase { get; set; }
        public virtual int? CurrentState { get; set; }
        public virtual DateTime? DisposedDate { get; set; }
        public virtual decimal? MonthDepreciationRate { get; set; }
        public virtual decimal? MonthPeriodDepreciationAmount { get; set; }
        public virtual string DeliveryRecordNo { get; set; }
        public virtual DateTime? DeliveryRecordDate { get; set; }
        public virtual bool? IsIrrationalCost { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual string Description { get; set; }
        public virtual int? ProductionYear { get; set; }
        public virtual string MadeIn { get; set; }
        public virtual string SerialNumber { get; set; }
        public virtual string Accessories { get; set; }
        public virtual string MaNhaCungCap { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string GuaranteeDuration { get; set; }
        public virtual string GuaranteeCodition { get; set; }
        public virtual string DisposedReason { get; set; }
        public virtual DateTime? PurchasedDate { get; set; }
        public virtual DateTime? UsedDate { get; set; }
        public virtual DateTime? IncrementDate { get; set; }
        public virtual DateTime? DepreciationDate { get; set; }
        public virtual decimal? PurchasePrice { get; set; }
        public virtual decimal? OriginalPrice { get; set; }
        public virtual decimal? AcDepreciationAmount { get; set; }
        public virtual decimal? RemainingAmount { get; set; }
        public virtual decimal? UsedTime { get; set; }
        public virtual string DepreciationAccount { get; set; }
        public virtual decimal? PeriodDepreciationAmount { get; set; }
        public virtual string OriginalPriceAccount { get; set; }
        public virtual decimal? DepreciationRate { get; set; }
        public virtual string ExpenditureAccount { get; set; }
        public virtual int? DepreciationMethod { get; set; }
        public virtual decimal? CostByLoan { get; set; }
        public virtual decimal? CostByEquity { get; set; }
        public virtual decimal? CostByVenture { get; set; }
        public virtual decimal? CostByBudget { get; set; }
        public virtual decimal? CostByOther { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual bool IsInit { get; set; }

        public virtual string No { get; set; }
        public virtual bool IsDisplayMonthRemain { get; set; }
        public virtual decimal? UsedTimeRemain { get; set; }
        public virtual DateTime? PostedDate { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual decimal? UsedTime2 { get; set; }
        public virtual decimal? UsedTimeRemain2 { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual IList<FixedAssetDetail> FixedAssetDetails { get; set; }
        public virtual IList<FixedAssetAccessories> FixedAssetAccessoriess { get; set; }

        public virtual FixedAssetCategory FixedAssetCategory { get; set; }
        public virtual Department Department { get; set; }
        public FixedAsset()
        {
            FixedAssetDetails = new List<FixedAssetDetail>();
            FixedAssetAccessoriess = new List<FixedAssetAccessories>();
        }

        public virtual string FAIncrementDebitAccount { get; set; }
    }


}
