using System;

namespace Accounting.Core.Domain
{
    public class TMAppendixList
    {
        private Guid _ID;
        private int _TypeID;
        private string _AppendixCode;
        private string _AppendixName;
        public virtual int TypeGroup { get; set; }

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string AppendixCode
        {
            get { return _AppendixCode; }
            set { _AppendixCode = value; }
        }

        public virtual string AppendixName
        {
            get { return _AppendixName; }
            set { _AppendixName = value; }
        }
        public virtual bool Status { get; set; }
        


    }
}
