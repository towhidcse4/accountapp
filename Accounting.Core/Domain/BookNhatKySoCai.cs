﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class BookNhatKySoCai
    {
        public Int64 ID { get; set; }
        public string PostedDate { get; set; }
        public string No { get; set; }
        public string Date { get; set; }
        public string Reason { get; set; }
        public decimal PhatSinh { get; set; }
        public string DebtAccount { get; set; }
        public string CreAccount { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        
        public string Account { get; set; }
        public string AccountCorresponding { get; set; }
        public string Orderby { get; set; }
    }
    public class BookNhatKySoCai_Period
    {
        public string Period { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyReportPreparer { get; set; }
        public string ChiefAccountantName { get; set; }
        public string CompanyDirectorName { get; set; }
    }

}
