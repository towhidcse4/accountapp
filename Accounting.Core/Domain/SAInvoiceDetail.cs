using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class SAInvoiceDetail
    {
        public virtual Guid ID { get; set; }
        public virtual Guid SAInvoiceID { get; set; }
        public virtual Guid? MaterialGoodsID { get; set; }
        public virtual decimal UnitPrice { get; set; }
        public virtual decimal UnitPriceOriginal { get; set; }
        public virtual decimal UnitPriceConvert { get; set; }
        public virtual decimal UnitPriceConvertOriginal { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal AmountOriginal { get; set; }
        public virtual decimal DiscountAmount { get; set; }
        public virtual decimal DiscountAmountOriginal { get; set; }
        public virtual decimal OWPrice { get; set; }
        public virtual decimal OWPriceOriginal { get; set; }
        public virtual decimal OWAmount { get; set; }
        public virtual decimal OWAmountOriginal { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual decimal VATAmountOriginal { get; set; }
        public virtual decimal SpecialConsumeTaxAmount { get; set; }
        public virtual decimal SpecialConsumeTaxAmountOriginal { get; set; }
        public virtual decimal SpecialConsumeUnitPrice { get; set; }
        public virtual decimal SpecialConsumeUnitPriceOriginal { get; set; }
        public virtual decimal UnitPriceAfterTax { get; set; }
        public virtual decimal UnitPriceAfterTaxOriginal { get; set; }
        public virtual decimal AmountAfterTax { get; set; }
        public virtual decimal AmountAfterTaxOriginal { get; set; }
        public virtual decimal DiscountAmountAfterTax { get; set; }
        public virtual decimal DiscountAmountAfterTaxOriginal { get; set; }
        public virtual Guid? ContractDetailID { get; set; }
        public virtual bool? IsPromotion { get; set; }
        public virtual string CustomProperty1 { get; set; }
        public virtual string CustomProperty2 { get; set; }
        public virtual string CustomProperty3 { get; set; }
        public virtual int? OrderPriority { get; set; }
        public virtual Guid? DepartmentID { get; set; }
        public virtual Guid? ExpenseItemID { get; set; }
        public virtual Guid? BudgetItemID { get; set; }
        public virtual Guid? CreditAccountingObjectID { get; set; }
        public virtual Guid? ConfrontDetailID { get; set; }
        public virtual Guid? SAOrderID { get; set; }
        public virtual string SAOrderNo { get; set; }
        public virtual decimal? ConvertRate { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual string RepositoryAccount { get; set; }
        public virtual string CostAccount { get; set; }
        public virtual Guid? ConfrontID { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual string LotNo { get; set; }
        public virtual string Waranty { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual Guid? CostSetID { get; set; }
        public virtual Guid? ContractID { get; set; }
        public virtual Guid? StatisticsCodeID { get; set; }
        public virtual string UnitConvert { get; set; }
        public virtual decimal? SpecialConsumeTaxRate { get; set; }
        public virtual string DiscountAccount { get; set; }
        public virtual decimal? VATRate { get; set; }
        public virtual decimal? DiscountRate { get; set; }
        public virtual Guid? RepositoryID { get; set; }
        public virtual string Description { get; set; }
        public virtual string DebitAccount { get; set; }
        public virtual string CreditAccount { get; set; }
        public virtual string Unit { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? QuantityConvert { get; set; }
        public virtual string VATDescription { get; set; }
        private Guid? detailID; 

        public virtual Guid? DetailID
        {
            get { return detailID; }
            set { detailID = value; }
        }
        public virtual Guid? CareerGroupID { get; set; }
        public virtual Guid? RSInwardDetailID { get; set; }
        public virtual decimal OWPriceconvert { get; set; } // don gia von quy doi
        public virtual decimal OWPriceconvertOriginal { get; set; }// don gia von quy doi nguyen te 
        
        public virtual MaterialGoods MaterialGoods { get; set; }
        public virtual string MaterialGoodsCode { get; set; }
        private string materialGoodsName;
        public virtual string MaterialGoodsName
        {
            get
            {
                //if (MaterialGoods != null)
                //    if (!string.IsNullOrEmpty(MaterialGoods.MaterialGoodsName))
                //    {
                //        return MaterialGoods.MaterialGoodsName;
                //    }
                //    else
                //    {
                //        return materialGoodsName;
                //    }

                //else
                    return Description;
            }
            set { materialGoodsName = value; }
        }
        public virtual decimal TaxExchangeRate { get; set; }
        public virtual decimal ExportTaxRate { get; set; }
        public virtual decimal ExportTaxAmount { get; set; }
        public virtual string ExportTaxAccount { get; set; }
        public virtual string ExportTaxAccountCorresponding { get; set; }
        
        decimal _itemDiscount;
        public virtual decimal itemDiscount
        {
            get { return (AmountOriginal * (decimal)DiscountRate) / 100; }
            set { _itemDiscount = value; }
        }

    }
}
