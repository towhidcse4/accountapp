using System;

namespace Accounting.Core.Domain
{
public class CPCostSetQuantum
{
private Guid _ID;
private Guid _CostSetID;
private string _AccountNumber;
private Decimal _Amount;
private Guid _ExpenseItemID;


public virtual Guid ID
{
get { return _ID; }
set { _ID = value; }
}

public virtual Guid CostSetID
{
get { return _CostSetID; }
set { _CostSetID = value; }
}

public virtual string AccountNumber
{
get { return _AccountNumber; }
set { _AccountNumber = value; }
}

public virtual Decimal Amount
{
get { return _Amount; }
set { _Amount = value; }
}

public virtual Guid ExpenseItemID
{
get { return _ExpenseItemID; }
set { _ExpenseItemID = value; }
}


}
}
