using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailTax
    {
        public virtual Guid ID { get; set; }
        public virtual Guid GOtherVoucherID { get; set; }
        public virtual string Description { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal ExchangeRate { get; set; }
        public virtual string VATAccount { get; set; }
        public virtual decimal VATRate { get; set; }
        public virtual decimal VATAmount { get; set; }
        public virtual decimal VATAmountOriginal { get; set; }
        public virtual decimal PretaxAmount { get; set; }
        public virtual decimal PretaxAmountOriginal { get; set; }
        public virtual int InvoiceType { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual Guid? AccountingObjectID { get; set; }
        public virtual string AccountingObjectName { get; set; }
        public virtual string AccountingObjectAddress { get; set; }
        public virtual string CompanyTaxCode { get; set; }
        public virtual Guid? GoodsServicePurchaseID { get; set; }
        public virtual int OrderPriority { get; set; }
        public virtual Guid? InvoiceTypeID { get; set; }
        public virtual string InvoiceTemplate { get; set; }
    }
}