using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class GOtherVoucherDetailForeignCurrency
    {
        private Guid _ID;
        private Guid _GOtherVoucherID;
        private string _AccountNumber;
        private Guid? _BankAccountDetailID;
        private Guid? _AccountingObjectID;
        private Decimal _DebitAmountOriginal;
        private Decimal _DebitAmount;
        private Decimal _DebitReevaluate;
        private Decimal _DebitAmountDiffer;
        private Decimal _CreditAmountOriginal;
        private Decimal _CreditAmount;
        private Decimal _CreditReevaluate;
        private Decimal _CreditAmountDiffer;
        private int _OrderPriority;

        public virtual bool Check { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual DateTime DateReevaluate { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid GOtherVoucherID
        {
            get { return _GOtherVoucherID; }
            set { _GOtherVoucherID = value; }
        }

        public virtual string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return _BankAccountDetailID; }
            set { _BankAccountDetailID = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return _AccountingObjectID; }
            set { _AccountingObjectID = value; }
        }

        public virtual Decimal DebitAmountOriginal
        {
            get { return _DebitAmountOriginal; }
            set { _DebitAmountOriginal = value; }
        }

        public virtual Decimal DebitAmount
        {
            get { return _DebitAmount; }
            set { _DebitAmount = value; }
        }

        public virtual Decimal DebitReevaluate
        {
            get { return _DebitReevaluate; }
            set { _DebitReevaluate = value; }
        }

        public virtual Decimal DebitAmountDiffer
        {
            get { return _DebitAmountDiffer; }
            set { _DebitAmountDiffer = value; }
        }

        public virtual Decimal CreditAmountOriginal
        {
            get { return _CreditAmountOriginal; }
            set { _CreditAmountOriginal = value; }
        }

        public virtual Decimal CreditAmount
        {
            get { return _CreditAmount; }
            set { _CreditAmount = value; }
        }

        public virtual Decimal CreditReevaluate
        {
            get { return _CreditReevaluate; }
            set { _CreditReevaluate = value; }
        }

        public virtual Decimal CreditAmountDiffer
        {
            get { return _CreditAmountDiffer; }
            set { _CreditAmountDiffer = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }
        public virtual IList<VoucherDetailForeignCurrency> VoucherDetailForeignCurrencys { get; set; }
        public GOtherVoucherDetailForeignCurrency()
        {
            VoucherDetailForeignCurrencys = new List<VoucherDetailForeignCurrency>();
        }


    }
}
