﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMAllocation
    {
        private Guid iD;
        private Guid branchID;
        private Guid eMAllocationRequiredID;
        private int typeID;
        private DateTime date;
        private int budgetMonth;
        private int budgetYear;
        private string reason;
        private Decimal requestAmountView;
        private Decimal aprovedAmountView;
        private Decimal amountView;
      

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual Guid BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }
        public virtual Guid EMAllocationRequiredID
        {
            get { return eMAllocationRequiredID; }
            set { eMAllocationRequiredID = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        public virtual int BudgetMonth
        {
            get { return budgetMonth; }
            set { budgetMonth = value; }
        }
        public virtual int BudgetYear
        {
            get { return budgetYear; }
            set { budgetYear = value; }
        }
        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }
        public virtual Decimal RequestAmountView
        {
            get { return requestAmountView; }
            set { requestAmountView = value; }
        }
        public virtual Decimal AprovedAmountView
        {
            get { return aprovedAmountView; }
            set { aprovedAmountView = value; }
        }

        public virtual Decimal AmountView
        {
            get { return amountView; }
            set { amountView = value; }
        }
    }
}
    

