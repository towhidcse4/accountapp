﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FixedAssetAccessories
    {
        private Guid iD; //ID chi tiết TSCĐ
        private Guid fixedAssetID; //ID TSCĐ( DR: Cascade)
        private string fixedAssetAccessoriesName; //Tên phụ kiện
        private string fixedAssetAccessoriesUnit; //Đơn vị tính
        private decimal fixedAssetAccessoriesQuantity; //Số lượng
        private decimal fixedAssetAccessoriesAmount; //Số tiền 
        private int orderPriority; //Thứ tự ưu tiên

        public FixedAssetAccessories()
        {
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid FixedAssetID
        {
            get { return fixedAssetID; }
            set { fixedAssetID = value; }
        }

        public virtual string FixedAssetAccessoriesName
        {
            get { return fixedAssetAccessoriesName; }
            set { fixedAssetAccessoriesName = value; }
        }

        public virtual string FixedAssetAccessoriesUnit
        {
            get { return fixedAssetAccessoriesUnit; }
            set { fixedAssetAccessoriesUnit = value; }
        }

        public virtual decimal FixedAssetAccessoriesQuantity
        {
            get { return fixedAssetAccessoriesQuantity; }
            set { fixedAssetAccessoriesQuantity = value; }
        }

        public virtual decimal FixedAssetAccessoriesAmount
        {
            get { return fixedAssetAccessoriesAmount; }
            set { fixedAssetAccessoriesAmount = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

    }
}

