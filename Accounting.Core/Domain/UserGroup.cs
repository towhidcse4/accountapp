using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class UserGroup
    {
        private Guid _UserGroupId;
        private string _GroupName;
        private Boolean _IsLocked;
        private DateTime _CreatedDate;
        private Boolean _IsActive;


        public virtual Guid UserGroupId
        {
            get { return _UserGroupId; }
            set { _UserGroupId = value; }
        }

        public virtual string GroupName
        {
            get { return _GroupName; }
            set { _GroupName = value; }
        }

        public virtual Boolean IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }


    }
}
