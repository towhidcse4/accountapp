using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class FAAdjustment
    {
        private int typeID;
        private DateTime postedDate;
        private DateTime date;
        private string no;
        private Guid? fixAssetID;
        private decimal oldOrgPrice;
        private decimal oldDepreciationAmount;
        private decimal oldEachYearDepreciationAmount;
        private decimal oldMonthlyDepreciationAmount;
        private decimal oldAcDepreciationAmount;
        private decimal oldRemainingAmountOriginal;
        private Guid iD;
        private decimal newOrgPrice;
        private decimal differOrgPrice;
        private decimal differAcDepreciationAmount;
        private decimal differMonthlyDepreciationAmount;
        private decimal differRemainingAmount;
        private decimal totalAmount;
        private decimal newDepreciationAmount;
        private decimal newEachYearDepreciationAmount;
        private decimal newMonthlyDepreciationAmount;
        private decimal newAcDepreciationAmount;
        private decimal newRemainingAmount;
        private bool recorded;
        private bool exported;
        private int? newDepreciationMethod;
        private bool? isDisplayByMonth;
        private bool? isAdjustDepreciationAmount;
        private bool? isAdjustUsedTime;
        private bool? isAdjustDepreciation;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private decimal? newUsedTime;
        private Guid? branchID;
        private int? oldDepreciationMethod;
        private decimal? oldUsedTime;
        private decimal? oldEachYearDepreciationRate;
        private string fixAssetName;
        private string reason;
        private Guid? templateId;
        public virtual decimal? DifferUsedTime { get; set; }
        public virtual decimal? DifferDepreciationAmount { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string OriginalVoucher { get; set; }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual Guid? FixedAssetID
        {
            get { return fixAssetID; }
            set { fixAssetID = value; }
        }

        public virtual decimal OldOrgPrice
        {
            get { return oldOrgPrice; }
            set { oldOrgPrice = value; }
        }

        public virtual decimal OldDepreciationAmount
        {
            get { return oldDepreciationAmount; }
            set { oldDepreciationAmount = value; }
        }

        public virtual decimal OldEachYearDepreciationAmount
        {
            get { return oldEachYearDepreciationAmount; }
            set { oldEachYearDepreciationAmount = value; }
        }

        public virtual decimal OldMonthlyDepreciationAmount
        {
            get { return oldMonthlyDepreciationAmount; }
            set { oldMonthlyDepreciationAmount = value; }
        }

        public virtual decimal OldAcDepreciationAmount
        {
            get { return oldAcDepreciationAmount; }
            set { oldAcDepreciationAmount = value; }
        }

        public virtual decimal OldRemainingAmountOriginal
        {
            get { return oldRemainingAmountOriginal; }
            set { oldRemainingAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual decimal NewOrgPrice
        {
            get { return newOrgPrice; }
            set { newOrgPrice = value; }
        }

        public virtual decimal DifferOrgPrice
        {
            get { return differOrgPrice; }
            set { differOrgPrice = value; }
        }

        public virtual decimal DifferAcDepreciationAmount
        {
            get { return differAcDepreciationAmount; }
            set { differAcDepreciationAmount = value; }
        }

        public virtual decimal DifferMonthlyDepreciationAmount
        {
            get { return differMonthlyDepreciationAmount; }
            set { differMonthlyDepreciationAmount = value; }
        }

        public virtual decimal DifferRemainingAmount
        {
            get { return differRemainingAmount; }
            set { differRemainingAmount = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public virtual decimal NewDepreciationAmount
        {
            get { return newDepreciationAmount; }
            set { newDepreciationAmount = value; }
        }

        public virtual decimal NewEachYearDepreciationAmount
        {
            get { return newEachYearDepreciationAmount; }
            set { newEachYearDepreciationAmount = value; }
        }

        public virtual decimal NewMonthlyDepreciationAmount
        {
            get { return newMonthlyDepreciationAmount; }
            set { newMonthlyDepreciationAmount = value; }
        }

        public virtual decimal NewAcDepreciationAmount
        {
            get { return newAcDepreciationAmount; }
            set { newAcDepreciationAmount = value; }
        }

        public virtual decimal NewRemainingAmount
        {
            get { return newRemainingAmount; }
            set { newRemainingAmount = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual int? NewDepreciationMethod
        {
            get { return newDepreciationMethod; }
            set { newDepreciationMethod = value; }
        }

        public virtual bool? IsDisplayByMonth
        {
            get { return isDisplayByMonth; }
            set { isDisplayByMonth = value; }
        }

        public virtual bool? IsAdjustDepreciationAmount
        {
            get { return isAdjustDepreciationAmount; }
            set { isAdjustDepreciationAmount = value; }
        }

        public virtual bool? IsAdjustUsedTime
        {
            get { return isAdjustUsedTime; }
            set { isAdjustUsedTime = value; }
        }

        public virtual bool? IsAdjustDepreciation
        {
            get { return isAdjustDepreciation; }
            set { isAdjustDepreciation = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual decimal? NewUsedTime
        {
            get { return newUsedTime; }
            set { newUsedTime = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual int? OldDepreciationMethod
        {
            get { return oldDepreciationMethod; }
            set { oldDepreciationMethod = value; }
        }

        public virtual decimal? OldUsedTime
        {
            get { return oldUsedTime; }
            set { oldUsedTime = value; }
        }

        public virtual decimal? OldEachYearDepreciationRate
        {
            get { return oldEachYearDepreciationRate; }
            set { oldEachYearDepreciationRate = value; }
        }

        public virtual string FixedAssetName
        {
            get { return fixAssetName; }
            set { fixAssetName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }
        public virtual Guid? TemplateID
        {
            get { return templateId; }
            set { templateId = value; }
        }
        public virtual IList<FAAdjustmentDetail> FAAdjustmentDetails { get; set; }
        public virtual IList<RefVoucher> RefVouchers { get; set; }
        public FAAdjustment()
        {
            FAAdjustmentDetails = new List<FAAdjustmentDetail>();
            RefVouchers = new List<RefVoucher>();
        }


    }
}
