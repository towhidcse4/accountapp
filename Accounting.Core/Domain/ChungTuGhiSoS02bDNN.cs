﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Core.Domain
{
    public class ChungTuGhiSoS02bDNN
    {
        public Guid VoucherID { get; set; }
        public string Number { get; set; }
        public DateTime VoucherDate { get; set; }
        public string TrichYeu { get; set; }
        public string VoucherDebitAccount { get; set; }
        public string VoucherCreditAccount { get; set; }
        public decimal VoucherAmount { get; set; }
        public string GhiChu { get; set; }
    }
}
