using System;
using System.Text;
using System.Collections.Generic;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SysPermission
    {
        public virtual bool Check { get; set; }
        public virtual System.Guid PermissionID { get; set; }
        public virtual string PermissionCode { get; set; }
        public virtual string PermissionName { get; set; }
        public virtual string Description { get; set; }
        public virtual int OrderPriority { get; set; }
        public SysPermission()
        {
            Check = false;
        }
    }
}
