using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Type
    {
        private int iD;
        private bool recordable;
        private bool searchable;
        private int orderPriority;
        private int? postType;
        private string typeName;
        private int? typeGroupID;
        private string typeGroupViewID;

        public virtual int ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Recordable
        {
            get { return recordable; }
            set { recordable = value; }
        }

        public virtual bool Searchable
        {
            get { return searchable; }
            set { searchable = value; }
        }

        public virtual int OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual int? PostType
        {
            get { return postType; }
            set { postType = value; }
        }

        public virtual string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        public virtual int? TypeGroupID
        {
            get { return typeGroupID; }
            set { typeGroupID = value; }
        }


        public virtual string TypeGroupViewID
        {
            get { return typeGroupViewID; }
            set { typeGroupViewID = value; }
        }


    }
}
