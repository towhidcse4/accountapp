﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCAudit
    {
        public virtual Guid ID { get; set; }
        public virtual Guid? BranchID { get; set; }
        public virtual int TypeID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual DateTime AuditDate { get; set; }
        public virtual string No { get; set; }
        public virtual string Description { get; set; }
        public virtual string Summary { get; set; }
        public virtual string CurrencyID { get; set; }
        public virtual decimal TotalAuditAmount { get; set; }
        public virtual decimal TotalBalanceAmount { get; set; }
        public virtual decimal DifferAmount { get; set; }
        public virtual decimal ExchangeRate { get; set; }
        public virtual Guid? TemplateID { get; set; }
        public virtual Type Type { get; set; }

        public MCAudit()
        {
            MCAuditDetails = new List<MCAuditDetail>();
            MCAuditDetailMembers = new List<MCAuditDetailMember>();
        }

        public virtual IList<MCAuditDetail> MCAuditDetails { get; set; }
        public virtual IList<MCAuditDetailMember> MCAuditDetailMembers { get; set; }
        public virtual string TypeName
        {
            get { return Type.TypeName; }
        }
    }
}
