

using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PPDiscountReturnDetail
    {
        private decimal? vATAmount;
        private decimal? vATAmountOriginal;
        private decimal? unitPrice;
        private decimal? unitPriceOriginal;
        private decimal? unitPriceConvert;
        private decimal? unitPriceConvertOriginal;
        private decimal? amount;
        private decimal? amountOriginal;
        private Guid? materialGoodsID;
        private Guid iD ;
        private Guid pPDiscountReturnID;
        private Guid? repositoryID;
        private string description;
        private string debitAccount;
        private string creditAccount;
        private string unit;
        private decimal? quantity;
        private decimal? quantityConvert;
        private Guid? goodsServicePurchaseID;
        private decimal? vATRate;
        private string vATAccount;
        private Guid? accountingObjectID;
        private Guid? accountingObjectTaxID;
        private string accountingObjectTaxName;
        private Guid? costSetID;
        private Guid? contractID;
        private Guid? statisticsCodeID;
        private Guid? confrontID;
        private string confrontInvNo;
        private DateTime? confrontInvDate;
        private DateTime? expiryDate;
        private string lotNo;
        private string unitConvert;
        private decimal? convertRate;
        private Guid? bankAccountDetailID;
        private Guid? departmentID;
        private Guid? expenseItemID;
        private Guid? budgetItemID;
        private Guid? confrontDetailID;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool isMatch;
        private DateTime? matchDate;
        private int? orderPriority;
        private string no;
        private string vATDescription;

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual string VATDescription
        {
            get { return vATDescription; }
            set { vATDescription = value; }
        }

        private DateTime? date;

        public virtual DateTime? Date
        {
            get { return date; }
            set { date = value; }
        }


        public virtual decimal? VATAmount
        {
            get { return vATAmount; }
            set { vATAmount = value == null ? 0 : value; }
        }

        public virtual decimal? VATAmountOriginal
        {
            get { return vATAmountOriginal; }
            set { vATAmountOriginal = value == null ? 0 : value; }
        }

        public virtual decimal? UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value == null ? 0 : value; }
        }

        public virtual decimal? UnitPriceOriginal
        {
            get { return unitPriceOriginal; }
            set { unitPriceOriginal = value == null ? 0 : value; }
        }

        public virtual decimal? UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value == null ? 0 : value; }
        }

        public virtual decimal? UnitPriceConvertOriginal
        {
            get { return unitPriceConvertOriginal; }
            set { unitPriceConvertOriginal = value == null ? 0 : value; }
        }

        public virtual decimal? Amount
        {
            get { return amount; }
            set { amount = value == null ? 0 : value; }
        }

        public virtual decimal? AmountOriginal
        {
            get { return amountOriginal; }
            set { amountOriginal = value == null ? 0 : value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid PPDiscountReturnID
        {
            get { return pPDiscountReturnID; }
            set { pPDiscountReturnID = value; }
        }

        public virtual Guid? RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual string DebitAccount
        {
            get { return debitAccount; }
            set { debitAccount = value; }
        }

        public virtual string CreditAccount
        {
            get { return creditAccount; }
            set { creditAccount = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual decimal? Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public virtual decimal? QuantityConvert
        {
            get { return quantityConvert; }
            set { quantityConvert = value; }
        }

        public virtual Guid? GoodsServicePurchaseID
        {
            get { return goodsServicePurchaseID; }
            set { goodsServicePurchaseID = value; }
        }

        public virtual decimal? VATRate
        {
            get { return vATRate; }
            set { vATRate = value; }
        }

        public virtual string VATAccount
        {
            get { return vATAccount; }
            set { vATAccount = value; }
        }

        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual Guid? AccountingObjectTaxID
        {
            get { return accountingObjectTaxID; }
            set { accountingObjectTaxID = value; }
        }

        public virtual string AccountingObjectTaxName
        {
            get { return accountingObjectTaxName; }
            set { accountingObjectTaxName = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual Guid? ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual Guid? ConfrontID
        {
            get { return confrontID; }
            set { confrontID = value; }
        }

        public virtual string ConfrontInvNo
        {
            get { return confrontInvNo; }
            set { confrontInvNo = value; }
        }

        public virtual DateTime? ConfrontInvDate
        {
            get { return confrontInvDate; }
            set { confrontInvDate = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return lotNo; }
            set { lotNo = value; }
        }

        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }

        public virtual Guid? BankAccountDetailID
        {
            get { return bankAccountDetailID; }
            set { bankAccountDetailID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid? ExpenseItemID
        {
            get { return expenseItemID; }
            set { expenseItemID = value; }
        }

        public virtual Guid? BudgetItemID
        {
            get { return budgetItemID; }
            set { budgetItemID = value; }
        }

        public virtual Guid? ConfrontDetailID
        {
            get { return confrontDetailID; }
            set { confrontDetailID = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool IsMatch
        {
            get { return isMatch; }
            set { isMatch = value; }
        }

        public virtual DateTime? MatchDate
        {
            get { return matchDate; }
            set { matchDate = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }


        public virtual MaterialGoods MaterialGoods { get; set; }
        private string materialGoodsName;
        public virtual string MaterialGoodsCode { get { return MaterialGoods != null ? MaterialGoods.MaterialGoodsCode : null; } }
        public virtual string MaterialGoodsName { get { return MaterialGoods != null ? MaterialGoods.MaterialGoodsName : null; }
            set { materialGoodsName = value; }
        }

        public virtual PPInvoice PPInvoice { get; set; }
        public virtual Guid PPInvoiceDetailID { get; set; }
        //public virtual MaterialGoods MaterialGoods { get; set; }
        
    }
}
