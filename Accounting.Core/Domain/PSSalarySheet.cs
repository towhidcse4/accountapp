﻿using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSSalarySheet
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private Decimal _TotalNetAmount;
        private Decimal _TotalNetAmountOriginal;
        private Guid? _GOtherVoucherID;
        private string _PSSalarySheetName;
        private int _Month;
        private int _Year;
        private Guid _TemplateID;
        public virtual IList<PSSalarySheetDetail> PSSalarySheetDetails { get; set; }
        public virtual List<Guid> Departments { get; set; }
        private string _Type;
        private Decimal _TotalTemporaryAmount;

        public virtual decimal WorkDayInMonth { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid TemplateID
        {
            get { return _TemplateID; }
            set { _TemplateID = value; }
        }

        public virtual string Type
        {
            get { return _TypeID == 830 ? "Bảng lương theo buổi" : _TypeID == 831 ? "Bảng lương theo giờ" : _TypeID == 832 ? "Bảng lương cơ bản cố định" : _TypeID == 834 ? "Bảng lương tạm ứng" : ""; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual Decimal TotalNetAmount
        {
            get { return _TypeID == 834 ? _TotalTemporaryAmount : _TotalNetAmount; }
            set { _TotalNetAmount = value; }
        }

        public virtual Decimal TotalTemporaryAmount
        {
            get { return _TotalTemporaryAmount; }
            set { _TotalTemporaryAmount = value; }
        }

        public virtual Decimal TotalNetAmountOriginal
        {
            get { return _TypeID == 834 ? _TotalTemporaryAmount : _TotalNetAmountOriginal; }
            set { _TotalNetAmountOriginal = value; }
        }

        public virtual Guid? GOtherVoucherID
        {
            get { return _GOtherVoucherID; }
            set { _GOtherVoucherID = value; }
        }

        public virtual string PSSalarySheetName
        {
            get { return _PSSalarySheetName; }
            set { _PSSalarySheetName = value; }
        }

        public virtual int Month
        {
            get { return _Month; }
            set { _Month = value; }
        }

        public virtual int Year
        {
            get { return _Year; }
            set { _Year = value; }
        }


    }
}
