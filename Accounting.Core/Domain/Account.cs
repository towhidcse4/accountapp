using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class Account
    {
        private Guid iD;
        private string accountNumber;
        private string accountName;
        private bool isParentNode;
        private int grade;
        private string accountGroupID;
        private int accountGroupKind;
        private string accountGroupKindView = string.Empty;
        private bool isActive = false;
        private string detailType;
        private string accountNameGlobal;
        private string description;
        private Guid? parentID;
        private int detailByAccountObject;

        public Account() { }
        public Account(string accountNumber)
        {
            this.accountNumber = accountNumber;
        }
        public virtual bool IsForeignCurrency { get; set; }
        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual string AccountNumber
        {
            get { return accountNumber; }
            set { accountNumber = value; }
        }

        public virtual string AccountName
        {
            get { return accountName; }
            set { accountName = value; }
        }

        public virtual string AccountNameGlobal
        {
            get { return accountNameGlobal; }
            set { accountNameGlobal = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }
        public virtual int DetailByAccountObject
        {
            get { return detailByAccountObject; }
            set { detailByAccountObject = value; }
        }
        public virtual string AccountGroupID
        {
            get { return accountGroupID; }
            set { accountGroupID = value; }
        }

        public virtual int AccountGroupKind
        {
            get { return accountGroupKind; }
            set { accountGroupKind = value; }
        }

        public virtual string AccountGroupKindView
        {
            get { return accountGroupKindView; }
            set { accountGroupKindView = value; }
        }

        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public virtual string DetailType
        {
            get { return detailType; }
            set { detailType = value; }
        }
    }
}