using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class EMContractDetailRevenue
    {
        private Guid _ID;
        private Guid _ContractID;
        private Guid? _MaterialGoodsID;
        private decimal _RevenueAmount;
        private int _OrderPriority;
        private Guid? _ContractEmployeeID;
        private Guid? _DepartmentID;
        private MaterialGoods _CodeMaterialGood;
        private string _MaterialGoodsName;
        private Department _CodeDepartment;
        private string _DepartmentName;
        private AccountingObject _CodeAccountingObject;
        private string _AccountingObjectName;
        private DateTime _RevenueDate;

        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }

        public virtual Guid? MaterialGoodsID
        {
            get { return _MaterialGoodsID; }
            set { _MaterialGoodsID = value; }
        }

        public virtual decimal RevenueAmount
        {
            get { return _RevenueAmount; }
            set { _RevenueAmount = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }

        public virtual Guid? ContractEmployeeID
        {
            get { return _ContractEmployeeID; }
            set { _ContractEmployeeID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual MaterialGoods CodeMaterialGoods
        {
            get { return _CodeMaterialGood; }
            set { _CodeMaterialGood = value; }
        }

        public virtual string MaterialGoodsName
        {
            get
            {
                return (CodeMaterialGoods != null && CodeMaterialGoods.ID != Guid.Empty) ? CodeMaterialGoods.MaterialGoodsName : "";
            }
            set { _MaterialGoodsName = value; }
        }

        public virtual Department CodeDepartment
        {
            get { return _CodeDepartment; }
            set { _CodeDepartment = value; }
        }

        public virtual string DepartmentName
        {
            get
            {
                return (CodeDepartment != null && CodeDepartment.ID != Guid.Empty) ? CodeDepartment.DepartmentName : "";
            }
            set { _DepartmentName = value; }
        }

        public virtual AccountingObject CodeAccountingObject
        {
            get { return _CodeAccountingObject; }
            set { _CodeAccountingObject = value; }
        }

        public virtual string AccountingObjectName
        {
            get
            {
                return (CodeAccountingObject != null && CodeAccountingObject.ID != Guid.Empty) ? CodeAccountingObject.AccountingObjectName : "";
            }
            set { _AccountingObjectName = value; }
        }

        public virtual DateTime RevenueDate
        {
            get { return _RevenueDate; }
            set { _RevenueDate = value; }
        }
    }
}