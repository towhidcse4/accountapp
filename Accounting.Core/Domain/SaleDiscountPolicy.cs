﻿using System;


namespace Accounting.Core.Domain
{
    [Serializable]
    public class SaleDiscountPolicy
    {
        private Guid id;
        private Guid materialGoodsID;
        private string materialGoodsIDVIEW;
        private decimal? quantityFrom;
        private decimal? quantityTo;
        private int? discountType;
        private decimal? discountResult;
        public virtual Guid ID
        {
            get { return id; }
            set { id = value; }
        }

        public virtual Guid MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }
        public virtual string MaterialGoodsIDVIEW
        {
            get { return materialGoodsIDVIEW; }
            set { materialGoodsIDVIEW = value; }
        }

        public virtual decimal? QuantityFrom
        {
            get { return quantityFrom; }
            set { quantityFrom = value; }
        }
        public virtual decimal? QuantityTo
        {
            get { return quantityTo; }
            set { quantityTo = value; }
        }
        public virtual int? DiscountType
        {
            get { return discountType; }
            set { discountType = value; }
        }

        public virtual decimal? DiscountResult
        {
            get { return discountResult; }
            set { discountResult = value; }
        }

    }
}
