using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class PSSalaryTaxInsuranceRegulation
    {
        private Guid _ID;
        private Guid _BranchID;
        private DateTime? _FromDate;
        private DateTime? _ToDate;
        private Decimal _BasicWage;
        private Decimal _InsuaranceMaximumizeSalary;
        private Decimal _ReduceSelfTaxAmount;
        private Decimal _ReduceDependTaxAmount;
        private Decimal _WorkDayInMonth;
        private Decimal _WorkingHoursInDay;
        private Boolean _IsWorkingOnSaturday;
        private Boolean _IsWorkingOnSunday;
        private Boolean _IsWorkingOnSaturdayNoon;
        private Boolean _IsWorkingOnSundayNoon;
        private Decimal _OvertimeDailyPercent;
        private Decimal _OvertimeWeekendPercent;
        private Decimal _OvertimeHolidayPercent;
        private Decimal _OvertimeWorkingDayNightPercent;
        private Decimal _OvertimeWeekendDayNightPercent;
        private Decimal _OvertimeHolidayNightPercent;
        private Decimal _CompanySocityInsurancePercent;
        private Decimal _CompanytAccidentInsurancePercent;
        private Decimal _CompanyMedicalInsurancePercent;
        private Decimal _CompanyUnEmployeeInsurancePercent;
        private Decimal _CompanyTradeUnionInsurancePercent;
        private Decimal _EmployeeSocityInsurancePercent;
        private Decimal _EmployeeAccidentInsurancePercent;
        private Decimal _EmployeeMedicalInsurancePercent;
        private Decimal _EmployeeUnEmployeeInsurancePercent;
        private Decimal _EmployeeTradeUnionInsurancePercent;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual DateTime? FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public virtual DateTime? ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public virtual Decimal BasicWage
        {
            get { return _BasicWage; }
            set { _BasicWage = value; }
        }

        public virtual Decimal InsuaranceMaximumizeSalary
        {
            get { return _InsuaranceMaximumizeSalary; }
            set { _InsuaranceMaximumizeSalary = value; }
        }

        public virtual Decimal ReduceSelfTaxAmount
        {
            get { return _ReduceSelfTaxAmount; }
            set { _ReduceSelfTaxAmount = value; }
        }

        public virtual Decimal ReduceDependTaxAmount
        {
            get { return _ReduceDependTaxAmount; }
            set { _ReduceDependTaxAmount = value; }
        }

        public virtual Decimal WorkDayInMonth
        {
            get { return _WorkDayInMonth; }
            set { _WorkDayInMonth = value; }
        }

        public virtual Decimal WorkingHoursInDay
        {
            get { return _WorkingHoursInDay; }
            set { _WorkingHoursInDay = value; }
        }

        public virtual Boolean IsWorkingOnSaturday
        {
            get { return _IsWorkingOnSaturday; }
            set { _IsWorkingOnSaturday = value; }
        }

        public virtual Boolean IsWorkingOnSunday
        {
            get { return _IsWorkingOnSunday; }
            set { _IsWorkingOnSunday = value; }
        }

        public virtual Boolean IsWorkingOnSaturdayNoon
        {
            get { return _IsWorkingOnSaturdayNoon; }
            set { _IsWorkingOnSaturdayNoon = value; }
        }

        public virtual Boolean IsWorkingOnSundayNoon
        {
            get { return _IsWorkingOnSundayNoon; }
            set { _IsWorkingOnSundayNoon = value; }
        }

        public virtual Decimal OvertimeDailyPercent
        {
            get { return _OvertimeDailyPercent; }
            set { _OvertimeDailyPercent = value; }
        }

        public virtual Decimal OvertimeWeekendPercent
        {
            get { return _OvertimeWeekendPercent; }
            set { _OvertimeWeekendPercent = value; }
        }

        public virtual Decimal OvertimeHolidayPercent
        {
            get { return _OvertimeHolidayPercent; }
            set { _OvertimeHolidayPercent = value; }
        }

        public virtual Decimal OvertimeWorkingDayNightPercent
        {
            get { return _OvertimeWorkingDayNightPercent; }
            set { _OvertimeWorkingDayNightPercent = value; }
        }

        public virtual Decimal OvertimeWeekendDayNightPercent
        {
            get { return _OvertimeWeekendDayNightPercent; }
            set { _OvertimeWeekendDayNightPercent = value; }
        }

        public virtual Decimal OvertimeHolidayNightPercent
        {
            get { return _OvertimeHolidayNightPercent; }
            set { _OvertimeHolidayNightPercent = value; }
        }

        public virtual Decimal CompanySocityInsurancePercent
        {
            get { return _CompanySocityInsurancePercent; }
            set { _CompanySocityInsurancePercent = value; }
        }

        public virtual Decimal CompanytAccidentInsurancePercent
        {
            get { return _CompanytAccidentInsurancePercent; }
            set { _CompanytAccidentInsurancePercent = value; }
        }

        public virtual Decimal CompanyMedicalInsurancePercent
        {
            get { return _CompanyMedicalInsurancePercent; }
            set { _CompanyMedicalInsurancePercent = value; }
        }

        public virtual Decimal CompanyUnEmployeeInsurancePercent
        {
            get { return _CompanyUnEmployeeInsurancePercent; }
            set { _CompanyUnEmployeeInsurancePercent = value; }
        }

        public virtual Decimal CompanyTradeUnionInsurancePercent
        {
            get { return _CompanyTradeUnionInsurancePercent; }
            set { _CompanyTradeUnionInsurancePercent = value; }
        }

        public virtual Decimal EmployeeSocityInsurancePercent
        {
            get { return _EmployeeSocityInsurancePercent; }
            set { _EmployeeSocityInsurancePercent = value; }
        }

        public virtual Decimal EmployeeAccidentInsurancePercent
        {
            get { return _EmployeeAccidentInsurancePercent; }
            set { _EmployeeAccidentInsurancePercent = value; }
        }

        public virtual Decimal EmployeeMedicalInsurancePercent
        {
            get { return _EmployeeMedicalInsurancePercent; }
            set { _EmployeeMedicalInsurancePercent = value; }
        }

        public virtual Decimal EmployeeUnEmployeeInsurancePercent
        {
            get { return _EmployeeUnEmployeeInsurancePercent; }
            set { _EmployeeUnEmployeeInsurancePercent = value; }
        }

        public virtual Decimal EmployeeTradeUnionInsurancePercent
        {
            get { return _EmployeeTradeUnionInsurancePercent; }
            set { _EmployeeTradeUnionInsurancePercent = value; }
        }


    }
}
