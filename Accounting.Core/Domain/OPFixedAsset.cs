using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class OPFixedAsset
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private DateTime? _PostedDate;
        private Guid _FixedAssetID;
        private Guid? _DepartmentID;
        private string _CurrencyID;
        private Decimal _ExchangeRate;
        private string _OriginalPriceAccount;
        private Decimal? _OriginalPriceDebitAmount;
        private Decimal? _OriginalPriceDebitAmountOriginal;
        private string _DepreciationAccount;
        private Decimal? _DepreciationCreditAmount;
        private Decimal? _DepreciationCreditAmountOriginal;
        private int _OrderPriority;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual DateTime? PostedDate
        {
            get { return _PostedDate; }
            set { _PostedDate = value; }
        }

        public virtual Guid FixedAssetID
        {
            get { return _FixedAssetID; }
            set { _FixedAssetID = value; }
        }

        public virtual Guid? DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }

        public virtual string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }

        public virtual Decimal ExchangeRate
        {
            get { return _ExchangeRate; }
            set { _ExchangeRate = value; }
        }

        public virtual string OriginalPriceAccount
        {
            get { return _OriginalPriceAccount; }
            set { _OriginalPriceAccount = value; }
        }

        public virtual Decimal? OriginalPriceDebitAmount
        {
            get { return _OriginalPriceDebitAmount; }
            set { _OriginalPriceDebitAmount = value; }
        }

        public virtual Decimal? OriginalPriceDebitAmountOriginal
        {
            get { return _OriginalPriceDebitAmountOriginal; }
            set { _OriginalPriceDebitAmountOriginal = value; }
        }

        public virtual string DepreciationAccount
        {
            get { return _DepreciationAccount; }
            set { _DepreciationAccount = value; }
        }

        public virtual Decimal? DepreciationCreditAmount
        {
            get { return _DepreciationCreditAmount; }
            set { _DepreciationCreditAmount = value; }
        }

        public virtual Decimal? DepreciationCreditAmountOriginal
        {
            get { return _DepreciationCreditAmountOriginal; }
            set { _DepreciationCreditAmountOriginal = value; }
        }

        public virtual int OrderPriority
        {
            get { return _OrderPriority; }
            set { _OrderPriority = value; }
        }


    }
}
