﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MaterialGoodsSpecialTaxGroup
    {
        private Guid iD;
        private string materialGoodsSpecialTaxGroupCode;
        private string materialGoodsSpecialTaxGroupName;
        private string orderFixCode;
        private Guid? parentID;
        private bool isParentNode;
        private decimal? taxRate;
        private int grade;
        private string unit;
        private bool isActive = false;
        private bool isSecurity;


        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }
        public virtual string MaterialGoodsSpecialTaxGroupCode
        {
            get { return materialGoodsSpecialTaxGroupCode; }
            set { materialGoodsSpecialTaxGroupCode = value; }
        }
        public virtual string MaterialGoodsSpecialTaxGroupName
        {
            get { return materialGoodsSpecialTaxGroupName; }
            set { materialGoodsSpecialTaxGroupName = value; }
        }
        public virtual string OrderFixCode
        {
            get { return orderFixCode; }
            set { orderFixCode = value; }
        }
        public virtual Guid? ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public virtual bool IsParentNode
        {
            get { return isParentNode; }
            set { isParentNode = value; }
        }
        public virtual decimal? TaxRate
        {
            get { return taxRate; }
            set { taxRate = value; }
        }

        public virtual int Grade
        {
            get { return grade; }
            set { grade = value; }
        }
        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }
        public virtual bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }
        public virtual bool IsSecurity
        {
            get { return isSecurity; }
            set { isSecurity = value; }
        }

    }
}