using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RepositoryLedger
    {
        private Guid referenceID;
        private DateTime date;
        private DateTime postedDate;
        private string no;
        private Guid iD;
        private Guid repositoryID;
        private Guid materialGoodsID;
        private decimal iWAmountBalance;
        private decimal unitPrice;
        private decimal unitPriceConvert;
        private int? orderPriority;
        private decimal? iWQuantityConvert;
        private decimal? oWQuantityConvert;
        private Guid? statisticsCodeID;
        private decimal? convertRate;
        private decimal? iWQuantity;
        private decimal? oWQuantity;
        private decimal? iWAmount;
        private decimal? oWAmount;
        private decimal? iWQuantityBalance;
        private string reason;
        private string description;
        private int? oWPurpose;
        private DateTime? expiryDate;
        private string lotNo;
        private Guid? costSetID;
        private string unitConvert;
        private string unit;
        private Guid? branchID;
        private string account;
        private string accountCorresponding;
        private int typeID;
        private Guid? _detailID;
        private DateTime? refDateTime;

        public virtual Guid ReferenceID
        {
            get { return referenceID; }
            set { referenceID = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid RepositoryID
        {
            get { return repositoryID; }
            set { repositoryID = value; }
        }

        public virtual Guid MaterialGoodsID
        {
            get { return materialGoodsID; }
            set { materialGoodsID = value; }
        }

        public virtual decimal IWAmountBalance
        {
            get { return iWAmountBalance; }
            set { iWAmountBalance = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal UnitPriceConvert
        {
            get { return unitPriceConvert; }
            set { unitPriceConvert = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }

        public virtual decimal? IWQuantityConvert
        {
            get { return iWQuantityConvert; }
            set { iWQuantityConvert = value; }
        }

        public virtual decimal? OWQuantityConvert
        {
            get { return oWQuantityConvert; }
            set { oWQuantityConvert = value; }
        }

        public virtual Guid? StatisticsCodeID
        {
            get { return statisticsCodeID; }
            set { statisticsCodeID = value; }
        }

        public virtual decimal? ConvertRate
        {
            get { return convertRate; }
            set { convertRate = value; }
        }

        public virtual decimal? IWQuantity
        {
            get { return iWQuantity; }
            set { iWQuantity = value; }
        }

        public virtual decimal? OWQuantity
        {
            get { return oWQuantity; }
            set { oWQuantity = value; }
        }

        public virtual decimal? IWAmount
        {
            get { return iWAmount; }
            set { iWAmount = value; }
        }

        public virtual decimal? OWAmount
        {
            get { return oWAmount; }
            set { oWAmount = value; }
        }

        public virtual decimal? IWQuantityBalance
        {
            get { return iWQuantityBalance; }
            set { iWQuantityBalance = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual int? OWPurpose
        {
            get { return oWPurpose; }
            set { oWPurpose = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string LotNo
        {
            get { return lotNo; }
            set { lotNo = value; }
        }

        public virtual Guid? CostSetID
        {
            get { return costSetID; }
            set { costSetID = value; }
        }

        public virtual string UnitConvert
        {
            get { return unitConvert; }
            set { unitConvert = value; }
        }

        public virtual string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual string Account
        {
            get { return account; }
            set { account = value; }
        }

        public virtual string AccountCorresponding
        {
            get { return accountCorresponding; }
            set { accountCorresponding = value; }
        }

        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public virtual Guid? DetailID
        {
            get { return _detailID; }
            set { _detailID = value; }
        }
        public virtual DateTime? RefDateTime
        {
            get { return refDateTime; }
            set { refDateTime = value; }
        }
        public virtual decimal QuantityT { get; set; }
        public virtual decimal IWAmountT { get; set; }
    }
}
