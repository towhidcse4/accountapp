using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class RSInwardOutward
    {
        private int typeID;
        private string typeIDViewNameCode;
        private DateTime postedDate;
        private DateTime date;
        private string no;
        private decimal totalAmount;
        private decimal totalAmountOriginal;
        private Guid iD;
        private bool exported;
        private Guid? branchID;
        private Guid? rSAssemblyDismantlementID;
        private Guid? materialQuantumID;
        private bool? isOutwardQuantum;
        private bool? isOutwardSAInvoice;
        private bool? isValueAdjust;
        private decimal? materialQuantity;
        private int? isInwardOutwardType;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private bool recorded;
        private Guid? accountingObjectID;
        private string accountingObjectName;
        private string accountingObjectAddress;
        private string contactName;
        private string reason;
        private Guid? rSInwardID;
        private Guid? rSOutwardID;
        private Guid? sAInvoiceID;
        private string currencyID;
        private decimal? exchangeRate;
        private Guid? employeeID;
        private string originalNo;
        private bool? isImportPurchase;
        private Guid? sAOrderID;
        private Guid? transportMethodID;
        private Guid? templateID;
        private DateTime? refDateTime;
        public virtual IList<RSInwardOutwardDetail> RSInwardOutwardDetails { get; set; }
        public virtual IList<RefVoucherRSInwardOutward> RefVoucherRSInwardOutwards { get; set; }
        public RSInwardOutward()
        {
            RSInwardOutwardDetails = new List<RSInwardOutwardDetail>();
            RefVoucherRSInwardOutwards = new List<RefVoucherRSInwardOutward>();
            SaInvoiceCustormIds = new List<Guid>(); 
        }
        public virtual Guid? AccountingObjectID
        {
            get { return accountingObjectID; }
            set { accountingObjectID = value; }
        }

        public virtual string AccountingObjectName
        {
            get { return accountingObjectName; }
            set { accountingObjectName = value; }
        }
        public virtual string OriginalVoucher { get; set; }
        public virtual string TypeVoucher { get; set; }
        public virtual string AccountingObjectAddress
        {
            get { return accountingObjectAddress; }
            set { accountingObjectAddress = value; }
        }

        public virtual string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual Guid? RSInwardID
        {
            get { return rSInwardID; }
            set { rSInwardID = value; }
        }

        public virtual Guid? RSOutwardID
        {
            get { return rSOutwardID; }
            set { rSOutwardID = value; }
        }

        public virtual Guid? SAInvoiceID
        {
            get { return sAInvoiceID; }
            set { sAInvoiceID = value; }
        }

        public virtual string CurrencyID
        {
            get { return currencyID; }
            set { currencyID = value; }
        }

        public virtual decimal? ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual string OriginalNo
        {
            get { return originalNo; }
            set { originalNo = value; }
        }

        public virtual bool? IsImportPurchase
        {
            get { return isImportPurchase; }
            set { isImportPurchase = value; }
        }

        public virtual Guid? SAOrderID
        {
            get { return sAOrderID; }
            set { sAOrderID = value; }
        }

        public virtual Guid? TransportMethodID
        {
            get { return transportMethodID; }
            set { transportMethodID = value; }
        }


        public virtual Guid? TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        public virtual int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }
        public virtual string TypeIDViewNameCode
        {
            get { return Type != null ? Type.TypeName : null; }
        }
        public virtual DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string No
        {
            get { return no; }
            set { no = value; }
        }

        public virtual decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }
        public virtual decimal TotalAmountOriginal
        {
            get { return totalAmountOriginal; }
            set { totalAmountOriginal = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual bool Exported
        {
            get { return exported; }
            set { exported = value; }
        }

        public virtual Guid? BranchID
        {
            get { return branchID; }
            set { branchID = value; }
        }

        public virtual Guid? RSAssemblyDismantlementID
        {
            get { return rSAssemblyDismantlementID; }
            set { rSAssemblyDismantlementID = value; }
        }

        public virtual Guid? MaterialQuantumID
        {
            get { return materialQuantumID; }
            set { materialQuantumID = value; }
        }

        public virtual bool? IsOutwardQuantum
        {
            get { return isOutwardQuantum; }
            set { isOutwardQuantum = value; }
        }

        public virtual bool? IsOutwardSAInvoice
        {
            get { return isOutwardSAInvoice; }
            set { isOutwardSAInvoice = value; }
        }

        public virtual bool? IsValueAdjust
        {
            get { return isValueAdjust; }
            set { isValueAdjust = value; }
        }

        public virtual decimal? MaterialQuantity
        {
            get { return materialQuantity; }
            set { materialQuantity = value; }
        }

        public virtual int? IsInwardOutwardType
        {
            get { return isInwardOutwardType; }
            set { isInwardOutwardType = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual bool Recorded
        {
            get { return recorded; }
            set { recorded = value; }
        }

        public virtual DateTime? RefDateTime
        {
            get { return refDateTime; }
            set { refDateTime = value; }
        }

        public virtual Type Type { get; set; }
        public virtual List<Guid> SaInvoiceCustormIds { get; set; }
    }
}
