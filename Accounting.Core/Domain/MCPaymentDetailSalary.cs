using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class MCPaymentDetailSalary
    {
        private Guid departmentID;
        private Guid iD;
        private Guid mCPaymentID;
        private Guid? employeeID;
        private string description;
        private decimal? accumAmount;
        private decimal? accumAmountOriginal;
        private decimal? currentMonthAmount;
        private decimal? currentMonthAmountOriginal;
        private decimal? payAmount;
        private decimal? payAmountOriginal;
        private string customProperty1;
        private string customProperty2;
        private string customProperty3;
        private int? orderPriority;


        public virtual Guid DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public virtual Guid ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public virtual Guid MCPaymentID
        {
            get { return mCPaymentID; }
            set { mCPaymentID = value; }
        }

        public virtual Guid? EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual decimal? AccumAmount
        {
            get { return accumAmount; }
            set { accumAmount = value; }
        }

        public virtual decimal? AccumAmountOriginal
        {
            get { return accumAmountOriginal; }
            set { accumAmountOriginal = value; }
        }

        public virtual decimal? CurrentMonthAmount
        {
            get { return currentMonthAmount; }
            set { currentMonthAmount = value; }
        }

        public virtual decimal? CurrentMonthAmountOriginal
        {
            get { return currentMonthAmountOriginal; }
            set { currentMonthAmountOriginal = value; }
        }

        public virtual decimal? PayAmount
        {
            get { return payAmount; }
            set { payAmount = value; }
        }

        public virtual decimal? PayAmountOriginal
        {
            get { return payAmountOriginal; }
            set { payAmountOriginal = value; }
        }

        public virtual string CustomProperty1
        {
            get { return customProperty1; }
            set { customProperty1 = value; }
        }

        public virtual string CustomProperty2
        {
            get { return customProperty2; }
            set { customProperty2 = value; }
        }

        public virtual string CustomProperty3
        {
            get { return customProperty3; }
            set { customProperty3 = value; }
        }

        public virtual int? OrderPriority
        {
            get { return orderPriority; }
            set { orderPriority = value; }
        }




    }
}
