using System;

namespace Accounting.Core.Domain
{
    public class TMIndustryType
    {
        private Guid _ID;
        private string _IndustryTypeCode;
        private string _IndustryTypeName;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual string IndustryTypeCode
        {
            get { return _IndustryTypeCode; }
            set { _IndustryTypeCode = value; }
        }

        public virtual string IndustryTypeName
        {
            get { return _IndustryTypeName; }
            set { _IndustryTypeName = value; }
        }


    }
}
