using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class OPCostSetByCostAccount
    {
        private Guid _ID;
        private Guid _ReferenceID;
        private string _CostAccount;
        private Decimal _Amount;
        private Guid _ExpenseItemID;


        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid ReferenceID
        {
            get { return _ReferenceID; }
            set { _ReferenceID = value; }
        }

        public virtual string CostAccount
        {
            get { return _CostAccount; }
            set { _CostAccount = value; }
        }

        public virtual Decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        public virtual Guid ExpenseItemID
        {
            get { return _ExpenseItemID; }
            set { _ExpenseItemID = value; }
        }


    }
}
