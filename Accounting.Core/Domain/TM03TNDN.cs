using System;
using System.Collections.Generic;

namespace Accounting.Core.Domain
{
    public class TM03TNDN
    {
        private Guid _ID;
        private Guid _BranchID;
        private int _TypeID;
        private string _DeclarationName;
        private string _DeclarationTerm;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private Boolean _IsFirstDeclaration;
        private int _AdditionTime;
        private DateTime ?_AdditionDate;
        private int _Career;
        private Boolean _IsAppendix031ATNDN;
        private Boolean _IsAppendix032ATNDN;
        private Boolean _IsSMEEnterprise;
        private Boolean _IsDependentEnterprise;
        private Boolean _IsRelatedTransactionEnterprise;
        private Guid _TMIndustryTypeID;
        private Decimal _Rate;
        private string _CompanyName;
        private string _CompanyTaxCode;
        private string _TaxAgencyTaxCode;
        private string _TaxAgencyName;
        private string _TaxAgencyEmployeeName;
        private string _CertificationNo;
        private string _SignName;
        private DateTime? _SignDate;
        private Boolean _IsExtend;
        private int ?_ExtensionCase;
        private DateTime? _ExtendTime;
        private Decimal _ExtendTaxAmount;
        private Decimal _NotExtendTaxAmount;
        private int _LateDays;
        private DateTime ?_FromDateLate;
        private DateTime ?_ToDateLate;
        private Decimal _LateAmount;
        private int _Year;

        public TM03TNDN()
        {
            TM03TNDNDetails = new List<TM03TNDNDetail>();
            TM031ATNDNs = new List<TM031ATNDN>();
            TM032ATNDNs = new List<TM032ATNDN>();
            TM03TNDNDocuments = new List<TM03TNDNDocument>();
        }
        public virtual IList<TM03TNDNDetail> TM03TNDNDetails { get; set; }
        public virtual IList<TM031ATNDN> TM031ATNDNs { get; set; }
        public virtual IList<TM032ATNDN> TM032ATNDNs { get; set; }
        public virtual IList<TM03TNDNDocument> TM03TNDNDocuments { get; set; }
        public virtual Guid ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual Guid BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }

        public virtual int TypeID
        {
            get { return _TypeID; }
            set { _TypeID = value; }
        }

        public virtual string DeclarationName
        {
            get { return _DeclarationName; }
            set { _DeclarationName = value; }
        }

        public virtual string DeclarationTerm
        {
            get { return _DeclarationTerm; }
            set { _DeclarationTerm = value; }
        }

        public virtual DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        public virtual DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public virtual Boolean IsFirstDeclaration
        {
            get { return _IsFirstDeclaration; }
            set { _IsFirstDeclaration = value; }
        }

        public virtual int AdditionTime
        {
            get { return _AdditionTime; }
            set { _AdditionTime = value; }
        }

        public virtual DateTime ?AdditionDate
        {
            get { return _AdditionDate; }
            set { _AdditionDate = value; }
        }

        public virtual int Career
        {
            get { return _Career; }
            set { _Career = value; }
        }

        public virtual Boolean IsAppendix031ATNDN
        {
            get { return _IsAppendix031ATNDN; }
            set { _IsAppendix031ATNDN = value; }
        }

        public virtual Boolean IsAppendix032ATNDN
        {
            get { return _IsAppendix032ATNDN; }
            set { _IsAppendix032ATNDN = value; }
        }

        public virtual Boolean IsSMEEnterprise
        {
            get { return _IsSMEEnterprise; }
            set { _IsSMEEnterprise = value; }
        }

        public virtual Boolean IsDependentEnterprise
        {
            get { return _IsDependentEnterprise; }
            set { _IsDependentEnterprise = value; }
        }

        public virtual Boolean IsRelatedTransactionEnterprise
        {
            get { return _IsRelatedTransactionEnterprise; }
            set { _IsRelatedTransactionEnterprise = value; }
        }

        public virtual Guid TMIndustryTypeID
        {
            get { return _TMIndustryTypeID; }
            set { _TMIndustryTypeID = value; }
        }

        public virtual Decimal Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }

        public virtual string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }

        public virtual string CompanyTaxCode
        {
            get { return _CompanyTaxCode; }
            set { _CompanyTaxCode = value; }
        }

        public virtual string TaxAgencyTaxCode
        {
            get { return _TaxAgencyTaxCode; }
            set { _TaxAgencyTaxCode = value; }
        }

        public virtual string TaxAgencyName
        {
            get { return _TaxAgencyName; }
            set { _TaxAgencyName = value; }
        }

        public virtual string TaxAgencyEmployeeName
        {
            get { return _TaxAgencyEmployeeName; }
            set { _TaxAgencyEmployeeName = value; }
        }

        public virtual string CertificationNo
        {
            get { return _CertificationNo; }
            set { _CertificationNo = value; }
        }

        public virtual string SignName
        {
            get { return _SignName; }
            set { _SignName = value; }
        }

        public virtual DateTime? SignDate
        {
            get { return _SignDate; }
            set { _SignDate = value; }
        }

        public virtual Boolean IsExtend
        {
            get { return _IsExtend; }
            set { _IsExtend = value; }
        }

        public virtual int ?ExtensionCase
        {
            get { return _ExtensionCase; }
            set { _ExtensionCase = value; }
        }

        public virtual DateTime? ExtendTime
        {
            get { return _ExtendTime; }
            set { _ExtendTime = value; }
        }

        public virtual Decimal ExtendTaxAmount
        {
            get { return _ExtendTaxAmount; }
            set { _ExtendTaxAmount = value; }
        }

        public virtual Decimal NotExtendTaxAmount
        {
            get { return _NotExtendTaxAmount; }
            set { _NotExtendTaxAmount = value; }
        }

        public virtual int LateDays
        {
            get { return _LateDays; }
            set { _LateDays = value; }
        }

        public virtual DateTime ?FromDateLate
        {
            get { return _FromDateLate; }
            set { _FromDateLate = value; }
        }

        public virtual DateTime ?ToDateLate
        {
            get { return _ToDateLate; }
            set { _ToDateLate = value; }
        }

        public virtual Decimal LateAmount
        {
            get { return _LateAmount; }
            set { _LateAmount = value; }
        }
        public virtual int Year
        {
            get { return _Year; }
            set { _Year = value; }
        }


    }
}
