using System;

namespace Accounting.Core.Domain
{
public class SAPolicyPriceMaterialGoods
{
private Guid _ID;
private Guid _SAPolicyPriceSettingID;
private Guid _MaterialGoodsID;


public virtual Guid ID
{
get { return _ID; }
set { _ID = value; }
}

public virtual Guid SAPolicyPriceSettingID
{
get { return _SAPolicyPriceSettingID; }
set { _SAPolicyPriceSettingID = value; }
}

public virtual Guid MaterialGoodsID
{
get { return _MaterialGoodsID; }
set { _MaterialGoodsID = value; }
}


}
}
