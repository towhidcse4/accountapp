using System;

namespace Accounting.Core.Domain
{
public class SAPolicyPriceSetting
{
private Guid _ID;
private string _SAPolicyPriceName;
private Boolean _IsActive;


public virtual Guid ID
{
get { return _ID; }
set { _ID = value; }
}

public virtual string SAPolicyPriceName
{
get { return _SAPolicyPriceName; }
set { _SAPolicyPriceName = value; }
}

public virtual Boolean IsActive
{
get { return _IsActive; }
set { _IsActive = value; }
}


}
}
