﻿using System;

namespace Accounting.Core.Domain
{
    [Serializable]
    public class AppUser
    {
        private Guid _UserId;
        private string _UserName;
        private string _Password;
        private string _FirstName;
        private string _Lastname;
        private string _Email;
        private Boolean _IsLocked;
        private DateTime _CreatedDate;
        private Boolean _IsActive;
        private Guid _GroupUserId;


        public virtual Guid UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public virtual string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        public virtual string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public virtual string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        public virtual string Lastname
        {
            get { return _Lastname; }
            set { _Lastname = value; }
        }

        public virtual string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public virtual Boolean IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public virtual Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public virtual Guid GroupUserId
        {
            get { return _GroupUserId; }
            set { _GroupUserId = value; }
        }


    }
}
