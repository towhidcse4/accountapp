﻿using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;
using Accounting.Core.Domain;
using FX.Core;
using System.Collections.Generic;
using FX.Data;
using Type = System.Type;

namespace Accounting.Core
{
    public static class Utils
    {
        public static Type getType(string typeName)
        {
            return Type.GetType(typeName);
        }

        public static readonly DateTime MinDate = new DateTime(1753, 10, 28);
        public static string SetNameStockKind(int StockKind)
        {
            switch (StockKind)
            {
                case 0: return "Cổ phần phổ thông";
                case 1: return "Cổ phần ưu đãi";
                default: return "";
            }
        }
        public static string SetNameTransactionType(int TransactionType)
        {
            switch (TransactionType)
            {
                case 0: return "Cổ phần ban đầu";
                case 1: return "Mua từ đợt phát hành";
                case 2: return "Chuyển nhượng";
                default: return "";
            }
        }
        public static string SetDateTax(int datetax)
        {
            switch (datetax)
            {
                case 1: return "Kê khai theo tháng";
                case 2: return "Kê khai theo quý";
                case 3: return "Kê khai theo kỳ";
                case 4: return "Kê khai theo năm";
                case 5: return "Kê khai theo từng lần phát sinh";
                default: return "Kê khai theo tháng";
            }
        }
        public static int GetInt(char ss)
        {
            int a = 0;
            a = Convert.ToInt32(ss) - 48;
            return a;
        }
        public static bool CheckMST(string mST)
        {
            bool istrue = false;
            if (mST.Length <= 14 && mST.Length >= 10)
            {
                int value = GetInt(mST[0]) * 31 + GetInt(mST[1]) * 29 + GetInt(mST[2]) * 23 + GetInt(mST[3]) * 19 + GetInt(mST[4]) * 17 + GetInt(mST[5]) * 13 + GetInt(mST[6]) * 7 + GetInt(mST[7]) * 5 + GetInt(mST[8]) * 3;
                int mod = 10 - value % 11;
                if (Math.Abs(mod) == GetInt(mST[9]))
                    istrue = true;
            }
            else
                istrue = false;
            return istrue;
        }

        public static bool IsKyHieuHoaDon(string kyhieuhoadon)
        {
            string pattern1 = @"^[ABCDEGHKLMNPQRSTUVXY]{2}/[0-9]{2}(E|T|P)$";//6 so
            string pattern2 = @"^[0-9]{2}[ABCDEGHKLMNPQRSTUVXY]{2}/[0-9]{2}(E|T|P)$";//8 so
            Match match = Regex.Match(kyhieuhoadon, pattern1, RegexOptions.IgnoreCase);

            // Here we check the Match instance.
            if (match.Success)
            {
                return true;
            }
            match = Regex.Match(kyhieuhoadon, pattern2, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                if (double.Parse(kyhieuhoadon.Substring(0, 2)) > 64d || double.Parse(kyhieuhoadon.Substring(0, 2)) == 0d) return false;
                return true;
            }
            return false;

        }

        public static void SendMail(string countTK, string tr, string email, string temp)
        {
            try
            {
                var emailContact = ConfigurationManager.AppSettings.Get("MailAddress");
                FX.Utils.EmailService.IEmailService emailSrv =
                        IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                subjectParams.Add("$subject", "Khai thuế Online bằng dịch vụ T-Van của VNPT");
                Dictionary<string, string> bodyParams = new Dictionary<string, string>(2);
                bodyParams.Add("$countTK", countTK);
                bodyParams.Add("$tr", tr);
                emailSrv.ProcessEmail(emailContact, email, temp, subjectParams, bodyParams);
            }
            catch { }
        }
        /// <summary>
        /// Huy Anh chuyển hàm của Haipt vào (chuyển lõi ko thay đổi hàm - tác giả: Haipt)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_this"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static BaseService<T, Guid> GetIService<T>(this object _this, T input)
        {
            string interfaceName = input.GetType().ToString();
            interfaceName = interfaceName.Replace("Domain.", "IService.I") + "Service";
            if (interfaceName == "Accounting.Core.IService.IEMContractSaleService" || interfaceName == "Accounting.Core.IService.IEMContractBuyService") interfaceName = "Accounting.Core.IService.IEMContractService";
            System.Type tmpType = System.Type.GetType(interfaceName);
            var method = typeof(IoC).GetMethod("Resolve", new System.Type[0]).MakeGenericMethod(tmpType);

            return (BaseService<T, Guid>)method.Invoke(_this, null);
        }
        public static BaseService<T, String> GetIServiceString<T>(this object _this, T input)
        {
            string interfaceName = input.GetType().ToString();
            interfaceName = interfaceName.Replace("Domain.", "IService.I") + "Service";
            System.Type tmpType = System.Type.GetType(interfaceName);
            var method = typeof(IoC).GetMethod("Resolve", new System.Type[0]).MakeGenericMethod(tmpType);

            return (BaseService<T, String>)method.Invoke(_this, null);
        }
        public static BaseService<T, int> GetIServiceInt<T>(this object _this, T input)
        {
            string interfaceName = input.GetType().ToString();
            interfaceName = interfaceName.Replace("Domain.", "IService.I") + "Service";
            System.Type tmpType = System.Type.GetType(interfaceName);
            var method = typeof(IoC).GetMethod("Resolve", new System.Type[0]).MakeGenericMethod(tmpType);

            return (BaseService<T, int>)method.Invoke(_this, null);
        }
        public static BaseService<T, Guid> GetIService<T>(this object _this)
        {
            string interfaceName = typeof(T).ToString();
            interfaceName = interfaceName.Replace("Domain.", "IService.I") + "Service";
            System.Type tmpType = System.Type.GetType(interfaceName);
            var method = typeof(IoC).GetMethod("Resolve", new System.Type[0]).MakeGenericMethod(tmpType);

            return (BaseService<T, Guid>)method.Invoke(_this, null);
        }
        #region tạo mã chứng từ tự động
        /// <summary>
        /// Check số chứng từ
        /// 1. dic trả về null số chứng từ không hợp lệ.
        /// 2. dic trả về giá trị số chứng từ hợp lệ và cho phép insert DB 
        /// </summary>
        /// <param name="no"></param>
        /// <returns></returns>
        public static Dictionary<string, string> CheckNo(string no)
        {
            if (no.Contains(" "))
            { return null; }
            else
            {
                if (no.Length <= 25)//trungnq sửa check số chứng từ kiểu mới
                {
                    //if (string.IsNullOrEmpty(no)) return null;
                    string digitExtend = @"(\d{3,})";
                    Regex ValidateRegex = new Regex(digitExtend);
                    MatchCollection validate = ValidateRegex.Matches(no);
                    if (validate.Count < 1)
                    {
                        return null;
                    }

                    //const string noPattern = @"^([^\d]{0,10})(\d{6})([^\d]{0,10})$";  @"^(([\w\d]{0,9}\D){0,10})(\d{3,})((\D[\w\d]{0,9}){0,10})$";
                    const string noPattern = @"^(\D{0,})(\d{3,})((\D[\w\d]{0,9}){0,10})$";
                    
        System.Text.RegularExpressions.Match m = new System.Text.RegularExpressions.Regex(noPattern).Match(no);
                    return m.Groups.Count > 1
                               ? new Dictionary<string, string>
                                         {
                                   
                                     {"Prefix", m.Groups[1].Value},
                                     {"Value", m.Groups[2].Value},
                                     {"Suffix", m.Groups[3].Value}
                                     
                                         }
                               : null;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Get current No Value
        /// </summary>
        /// <param name="No"></param>
        /// <returns></returns>
        public static int? GetNoValue(string No)
        {
            var NoSplit = CheckNo(No);

            if (NoSplit == null) return null;

            var noValue = NoSplit["Value"];

            int value = 0;

            int.TryParse(noValue, out value);

            return value;
        }
        /// <summary>
        /// Tạo mã chứng từ với Gencode
        /// </summary>
        /// <param name="gencode">Đối tượng Gencode</param>
        /// <returns>Mã chứng từ</returns>
        public static string TaoMaChungTu(GenCode gencode)
        {
            if (gencode == null) return string.Empty;

            string strNumber = string.Empty;
            if (gencode.Length > gencode.Length - gencode.CurrentValue.ToString().Length)
            {
                for (int i = 0; i < gencode.Length - gencode.CurrentValue.ToString().Length; i++) strNumber += "0";
            }
            strNumber += gencode.CurrentValue.ToString();
            return string.Format("{0}{1}{2}", gencode.Prefix ?? string.Empty, strNumber, gencode.Suffix ?? string.Empty);
        }
        #endregion

        public static void GetPeriodPrevious(DateTime fromDate, DateTime toDate, out DateTime fromDateBegning, out DateTime toDateBegning)
        {
            fromDateBegning = new DateTime();
            toDateBegning = new DateTime();
            if (fromDate.Day == 1)
            {
                if (fromDate.Month == 1 && toDate.Day == 31 && toDate.Month == 12)
                {

                    toDateBegning = fromDate.AddDays(-1);
                    fromDateBegning = fromDate.AddYears(-1);
                    return;
                }
                if ((new List<int> { 1, 4, 7, 10 }).Contains(fromDate.Month) && fromDate.AddMonths(3).AddDays(-1).Date == toDate.Date)
                {


                    toDateBegning = fromDate.AddDays(-1);
                    fromDateBegning = fromDate.AddMonths(-3);
                    return;
                }
                if (fromDate.AddMonths(1).AddDays(-1) == toDate)
                {

                    toDateBegning = fromDate.AddDays(-1);
                    fromDateBegning = fromDate.AddMonths(-1);
                    return;
                }
            }
            else
            {
                int songay = (toDate - fromDate).Days;
                if (songay > 0)
                {
                    toDateBegning = fromDate.AddDays(-1);
                    fromDateBegning = toDateBegning.AddDays(-songay);
                    return;
                }
                if (songay == 0)
                {
                    fromDateBegning = fromDate.AddDays(-1);
                    toDateBegning = fromDate.AddDays(-1);
                    return;
                }
            }

        }

        /// <summary>
        /// Chuyển dữ liệu từ list cha sang list con
        /// </summary>
        /// <typeparam name="T">đối tượng cha</typeparam>
        /// <typeparam name="TG">đối tượng con</typeparam>
        /// <param name="lstParent">danh sách đối tượng cha</param>
        /// <param name="lstChild">danh sách đối tượng con</param>
        /// <returns></returns>
        public static List<TG> Convert_FromParent_ToChild<T, TG>(IEnumerable<T> lstParent, List<TG> lstChild)
        {
            Type elementTypeParent = typeof(T);
            Type elementTypeChild = typeof(TG);
            foreach (T item in lstParent)
            {
                TG child = Activator.CreateInstance<TG>();
                foreach (PropertyInfo propInfoParent in elementTypeParent.GetProperties())
                {
                    Type t = Type.GetType(propInfoParent.PropertyType.FullName);

                    foreach (PropertyInfo propInfoChild in elementTypeChild.GetProperties())
                    {
                        if (propInfoChild.Name == propInfoParent.Name && t != null)
                            propInfoChild.SetValue(child, propInfoParent.GetValue(item, null), null);
                    }
                }
                lstChild.Add(child);
            }
            return lstChild;
        }

        public static T GetAgainObject<T>(T objectInput) where T : new()
        {
            T temp = new T();
            PropertyInfo propRecord = objectInput.GetType()
                    .GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propRecord && propRecord.CanWrite && propRecord.PropertyType.Name == "Guid")
            {
                Guid tempGuid = (Guid)propRecord.GetValue(objectInput, null);
                BaseService<T, Guid> testInvoke = GetIService(objectInput, (T)Activator.CreateInstance(typeof(T)));
                temp = testInvoke.Getbykey(tempGuid);
            }
            System.Type elementType = typeof(T);
            PropertyInfo[] propertyInfo = elementType.GetProperties();
            foreach (PropertyInfo propItem in propertyInfo)
            {
                PropertyInfo propRecordTemp = objectInput.GetType().GetProperty(propItem.Name, BindingFlags.Public | BindingFlags.Instance);
                if (null != propRecord && propRecord.CanWrite)
                {
                    propItem.SetValue(temp, propRecordTemp.GetValue(objectInput, null), null);
                }
            }
            return temp;
        }

        /// <summary>
        /// Chuyển 2 list cha con thành dataset
        /// </summary>
        /// <typeparam name="T">kiểu dữ liệu thằng cha</typeparam>
        /// <typeparam name="TK">kiểu dữ liệu thằng con</typeparam>
        /// <param name="listChild">danh sách các thằng con</param>
        /// <param name="listParent">danh sách các thằng cha</param>
        /// <param name="pkParent"></param>
        /// <param name="fkChild"></param>
        /// <returns>trả về 1 dataset trong chứa 2 datatable</returns>
        public static DataSet Convert_Parent_And_Child_List_To_DataSet<T, TK>(IEnumerable<T> listParent, IEnumerable<TK> listChild, string pkParent, string fkChild)
        {

            Type elementTypeParent = typeof(T);
            Type elementTypeChild = typeof(TK);
            string colParent = elementTypeParent.Name;
            string colChild = elementTypeChild.Name;
            //lấy tên đối tượng

            DataSet ds = new DataSet();

            DataTable dtParent = Convert_FromList_ToDataTable(listParent);
            DataTable dtChild = Convert_FromList_ToDataTable(listChild);

            ds.Tables.Add(dtParent);
            ds.Tables[0].TableName = colParent;
            ds.Tables.Add(dtChild);
            ds.Tables[1].TableName = colChild;

            ds.Relations.Add(pkParent + "to" + fkChild, ds.Tables[0].Columns[pkParent], ds.Tables[1].Columns[fkChild]);
            return ds;
        }
        /// <summary>
        /// Chuyển từ List thành DataTable
        /// </summary>
        /// <typeparam name="T">loại đối tượng cần chuyền</typeparam>
        /// <param name="lstObj">danh sách đối tượng</param>
        /// <returns></returns>
        public static DataTable Convert_FromList_ToDataTable<T>(IEnumerable<T> lstObj)
        {
            DataTable dt = Convert_FromObject_NewDataTable<T>();
            Type elementType = typeof(T);
            foreach (T item in lstObj)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyInfo propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        /// <summary>
        /// Chuyển tất cả các thuộc tính của một đối tượng thành một cột trong datatable
        /// </summary>
        /// <typeparam name="T">Đối tượng có thuộc tính cần chuyển</typeparam>
        /// <returns>Trả về một datatable đã tạo sẵn các column tương ứng với thuộc tính của đối tượng</returns>
        public static DataTable Convert_FromObject_NewDataTable<T>()
        {
            DataTable dt = new DataTable();
            Type elementType = typeof(T);
            foreach (PropertyInfo propInfo in elementType.GetProperties())
            {
                Type colType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;
                dt.Columns.Add(propInfo.Name, colType);
            }
            return dt;
        }
        /// <summary>
        /// Tính tiền quy đổi từ nguyên giá và tỷ lệ quy đổi ngoại tệ
        /// </summary>
        /// <param name="amountOriginal"></param>
        /// <param name="exchangeRate"></param>
        /// <returns></returns>
        public static decimal calculateAmountFromOriginal(decimal amountOriginal, decimal? exchangeRate)
        {
            decimal amount = 0;
            decimal rate = exchangeRate ?? 1;
            amount = amountOriginal * (rate > 0 ? rate : 1);
            return Math.Round(amount);
        }
        public static T CloneObject<T>(this T obj)
        {
            try
            {
                if (Object.ReferenceEquals(obj, null))
                {
                    return default(T);
                }
                try
                {
                    if (obj.GetType().Name.Contains("List"))
                        if (((IList<T>)obj).Count == 0)
                        {
                            obj = (T)Activator.CreateInstance(obj.GetType());
                            return obj;
                        }
                }
                catch (Exception)
                {

                }
                
                PropertyInfo propCount = obj.GetType().GetProperty("Count");
                if (propCount != null && propCount.CanRead)
                    if ((int)propCount.GetValue(obj, null) == 0)
                    {
                        obj = (T)Activator.CreateInstance(obj.GetType());
                        return obj;
                    }
                using (System.IO.MemoryStream memStream = new System.IO.MemoryStream())
                {
                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter(null,
                         new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.Clone));
                    binaryFormatter.Serialize(memStream, obj);
                    memStream.Seek(0, System.IO.SeekOrigin.Begin);
                    return (T)binaryFormatter.Deserialize(memStream);
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

    }
}
