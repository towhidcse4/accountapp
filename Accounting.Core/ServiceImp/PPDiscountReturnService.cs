using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class PPDiscountReturnService : BaseService<PPDiscountReturn, Guid>, IPPDiscountReturnService
    {
        public PPDiscountReturnService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public PPDiscountReturn GetPPDiscountReturnbyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
        public int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            var lst = Query.Where(n => n.TypeID == 220 && n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null).ToList();
            if (lst.Count > 0) return lst.ToList().Select(n => int.Parse(n.InvoiceNo)).Max();
            else return 0;
        }

        public List<PPDiscountReturn> GetListPPDiscountReturnTT153(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            return Query.Where(n => n.TypeID == 220 && n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != null && n.InvoiceNo != "").ToList();
        }
    }

}