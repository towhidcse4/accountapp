using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM04GTGTAdjustService: BaseService<TM04GTGTAdjust ,Guid>,ITM04GTGTAdjustService
    {
        public TM04GTGTAdjustService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}