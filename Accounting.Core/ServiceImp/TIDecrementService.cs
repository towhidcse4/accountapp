﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
//using Accounting.Core.Domain.obj.TIDecrements;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;


namespace Accounting.Core.ServiceImp
{
   public class  TIDecrementService :BaseService<Accounting.Core.Domain.TIDecrement,Guid>,ITIDecrementService
   {
    public TIDecrementService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
   { }

    public IFAIncrementService _IFAIncrementService { get { return IoC.Resolve<IFAIncrementService>(); } }
    public IFAIncrementDetailService _IFAIncrementDetailService { get { return IoC.Resolve<IFAIncrementDetailService>(); } }    
    public IFAAdjustmentService _IFAAdjustmentService { get { return IoC.Resolve<IFAAdjustmentService>(); } }
    public IFAAdjustmentDetailService _IFAAdjustmentDetailService { get { return IoC.Resolve<IFAAdjustmentDetailService>(); } }
    public IFixedAssetService _IFixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
    public ITIDecrementService _ITIDecrementService { get { return IoC.Resolve<ITIDecrementService>(); } }
    public ITIDecrementDetailService _ITIDecrementDetailService { get { return IoC.Resolve<ITIDecrementDetailService>(); } }

        public TIDecrement findByRefID(Guid iD)
        {
            var lst = Query.Where(fad => fad.RefID == iD).ToList();
            if (lst != null && lst.Count > 0) return lst[0];
            return null;
        }

        //public List<GGHachToan> GetListGG(string FACode)
        //{
        //    List<GGHachToan> lstGG = new List<GGHachToan>();
        //    FixedAsset theFA = _IFixedAssetService.Query.Where(f => f.FixedAssetCode == FACode).FirstOrDefault();        

        //    FAIncrementDetail theFAIncrementDetail = _IFAIncrementDetailService.Query.Where(ad => ad.FixedAssetID == theFA.ID).FirstOrDefault();
        //    FAIncrement theFAIncrement = _IFAIncrementService.Query.Where(a => a.ID == theFAIncrementDetail.FAIncrementID).FirstOrDefault();

        //    FAAdjustment theFAAdjustment = _IFAAdjustmentService.Query.Where(aa => aa.FixedAssetID == theFA.ID).FirstOrDefault();
        //    FAAdjustmentDetail theFAAdjustmentDetail = null;
        //    if (theFAAdjustment != null)
        //    {
        //        theFAAdjustmentDetail = _IFAAdjustmentDetailService.Query.Where(aad => aad.FAAdjustmentID == theFAAdjustment.ID).FirstOrDefault();
        //    }

        //    GGHachToan FAIncrement_Hachtoan = new GGHachToan();
        //    FAIncrement_Hachtoan.FACode = theFA.FixedAssetCode;
        //    if (theFAAdjustment != null)
        //    {
        //        FAIncrement_Hachtoan.Description = theFAAdjustment.Reason;
        //        FAIncrement_Hachtoan.DebitAccount = theFAAdjustmentDetail.DebitAccount;
        //        FAIncrement_Hachtoan.CreditAccount = theFAAdjustmentDetail.CreditAccount;
        //        FAIncrement_Hachtoan.Amount = theFAAdjustmentDetail.Amount;
        //    }
        //    else
        //    {
        //        FAIncrement_Hachtoan.Description = theFAIncrement.Reason;
        //        FAIncrement_Hachtoan.DebitAccount = theFAIncrementDetail.DebitAccount;
        //        FAIncrement_Hachtoan.CreditAccount = theFAIncrementDetail.CreditAccount;
        //        FAIncrement_Hachtoan.Amount = theFAIncrementDetail.Amount;
        //    }
        //    lstGG.Add(FAIncrement_Hachtoan);

        //    TIDecrementDetail theTIDecrementDetail = _ITIDecrementDetailService.Query.Where(fadd => fadd.FixedAssetID == theFA.ID).FirstOrDefault();
        //    TIDecrement theTIDecrement = null;
        //    GGHachToan TIDecrement_Hachtoan = new GGHachToan();
        //    if (theTIDecrementDetail != null)
        //    {
        //        theTIDecrement = _ITIDecrementService.Query.Where(fad => fad.ID == theTIDecrementDetail.TIDecrementID).FirstOrDefault();
        //        TIDecrement_Hachtoan.FACode = theFA.FixedAssetCode;
        //        TIDecrement_Hachtoan.Description = theTIDecrement.Reason;
        //        TIDecrement_Hachtoan.DebitAccount = theTIDecrementDetail.DebitAccount;
        //        TIDecrement_Hachtoan.CreditAccount = theTIDecrementDetail.CreditAccount;
        //        TIDecrement_Hachtoan.Amount = theTIDecrementDetail.Amount;
        //        lstGG.Add(TIDecrement_Hachtoan);
        //    }

        //    return lstGG;
        //}

    }
}
