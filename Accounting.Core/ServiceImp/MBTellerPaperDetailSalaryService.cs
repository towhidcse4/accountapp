using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBTellerPaperDetailSalaryService: BaseService<MBTellerPaperDetailSalary ,Guid>,IMBTellerPaperDetailSalaryService
    {
        public MBTellerPaperDetailSalaryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}