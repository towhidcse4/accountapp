using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FormulaOperandUsingService: BaseService<FormulaOperandUsing ,int>,IFormulaOperandUsingService
    {
        public FormulaOperandUsingService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}