﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{

    public class TITransferService : BaseService<TITransfer, Guid>, ITITransferService
    {
        public TITransferService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public TITransfer GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
    }
}
