﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TIIncrementService : BaseService<TIIncrement, Guid>, ITIIncrementService
    {
        ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public TIIncrementService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListTIIncrementCode()
        {
            List<string> list = Query.Select(p => p.No).ToList();
            return list;
        }

        public TIIncrement findByRefID(Guid iD)
        {
            var lst = Query.Where(fad => fad.RefID == iD).ToList();
            if (lst != null && lst.Count > 0) return lst[0];
            return null;
        }
    }
}
