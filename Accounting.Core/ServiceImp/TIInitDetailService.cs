using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TIInitDetailService: BaseService<TIInitDetail ,Guid>,ITIInitDetailService
    {
        public TIInitDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public List<TIInitDetail> GetByTIInitID(Guid req)
        {
            List<TIInitDetail> res;
            try
            {
                res = Query.Where(o => o.TIInitID == req).ToList();
            }
            catch (Exception)
            {
                res = null;
            };
            return res;
        }
    }

}