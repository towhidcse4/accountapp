﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MaterialGoodsResourceTaxGroupService : BaseService<MaterialGoodsResourceTaxGroup, Guid>, IMaterialGoodsResourceTaxGroupService
    {
        public MaterialGoodsResourceTaxGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        //lấy danh sách 
        public List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroup(bool IsActive)
        {
            return GetAll().Where(p => p.IsActive == IsActive).ToList().OrderBy(p => p.MaterialGoodsResourceTaxGroupCode).ToList();
        }
        public List<string> GetListMaterialGoodsResourceTaxGroupCode()
        {
            List<string> lst = Query.Select(c => c.MaterialGoodsResourceTaxGroupCode).ToList();
            return lst;
        }
        public List<string> GetListOrderFixCodeParentID(Guid? parentID)
        {
            List<string> lstOrderFixCodeChild = Query.Where(a => a.ParentID == parentID).Select(a => a.OrderFixCode).ToList();
            return lstOrderFixCodeChild;
        }
        public List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupParentID(Guid? parentID)
        {
            List<MaterialGoodsResourceTaxGroup> lstChild = Query.Where(p => p.ParentID == parentID).ToList();
            return lstChild;
        }
        public int CountListMaterialGoodsResourceTaxGroupParentID(Guid? parentID)
        {
            return GetListMaterialGoodsResourceTaxGroupParentID(parentID).Count;
        }
        public List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupOrderFixCode(string orderFixCode)
        {
            List<MaterialGoodsResourceTaxGroup> lstChildCheck = Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode)).ToList();
            return lstChildCheck;
        }
        public List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupGrade(int grade)
        {
            List<MaterialGoodsResourceTaxGroup> listChildGradeNo1 = Query.Where(a => a.Grade == grade).ToList();
            return listChildGradeNo1;
        }
        public List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupOrderFixCodeNotParent(string orderFixCode, Guid? parentID)
        {
            List<MaterialGoodsResourceTaxGroup> lstChild2 = Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode) && p.ID != parentID).OrderBy(p => p.OrderFixCode).ToList();
            return lstChild2;
        }
        public List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupOrderFixCode()
        {
            return GetAll().OrderByDescending(c => c.OrderFixCode).Reverse().ToList();
        }
        public List<MaterialGoodsResourceTaxGroup> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.MaterialGoodsResourceTaxGroupCode).ToList();
        }

    }
}
