using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class PSSalarySheetService: BaseService<PSSalarySheet ,Guid>,IPSSalarySheetService
    {
        public PSSalarySheetService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<PSSalarySheet> GetByGOtherVoucherIDIsNull()
        {
            return Query.Where(x => x.GOtherVoucherID == null || x.GOtherVoucherID == Guid.Empty).OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).ToList();
        }
    }

}