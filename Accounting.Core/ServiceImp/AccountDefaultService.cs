﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class AccountDefaultService : BaseService<AccountDefault, Guid>, IAccountDefaultService
    {

        public AccountDefaultService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public IAccountService _IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        private string _defaultDebitAccount = String.Empty;

        public string DefaultDebitAccount()
        {
            return _defaultDebitAccount;
        }
        private string _defaultCreditAccount = String.Empty;

        public string DefaultCreditAccount()
        {
            return _defaultCreditAccount;
        }
        private string _defaultVATAccount = String.Empty;

        public string DefaultVatAccount()
        {
            return _defaultVATAccount;
        }

        Dictionary<string, string> _defaultAccount = new Dictionary<string, string>();
        /// <summary>
        /// Lấy ra dictionary chứa các TK mặc định
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> DefaultAccount()
        {
            return _defaultAccount;
        }

        /// <summary>
        /// Lấy DS tài khoản ngầm định theo loại chứng từ có tùy biến 
        /// [Huy Anh]
        /// </summary>
        /// <param name="_dsAccountDefault">DS bảng AccountDefault</param>
        /// <param name="typeId">Loại chứng từ</param>
        /// <param name="accountName">Tên tài khoản</param>
        /// <param name="_dsAccount">DS bảng Account nếu có</param>
        /// <param name="isImportPurchase">Trạng thái mua hàng(Nhập khẩu/ Trong nước)</param>
        /// <returns></returns>
        public List<Account> GetAccountDefaultByTypeId(int typeId, string accountName, IList<AccountDefault> _dsAccountDefault = null, IList<Account> _dsAccount = null, bool isImportPurchase = false)
        {
            if (!isImportPurchase)
            {
                if (typeId == 430 && accountName == "SpecialConsumeTaxAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "3332").ToList();
                }
                else if (typeId == 430 && accountName == "VATAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "1331" || x.AccountNumber == "1332").OrderBy(x => x.AccountNumber).ToList();
                }
                else if (typeId == 500 && accountName == "SpecialConsumeTaxAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "3332").ToList();
                }
            }
            else
            {
                
                if (typeId == 430 && accountName == "SpecialConsumeTaxAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "3332").ToList();
                }
                else if (typeId == 430 && accountName == "VATAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "33312").ToList();
                }
                else if (typeId == 430 && accountName == "ImportTaxAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "3333").ToList();
                }
                else if ((typeId == 430 || typeId == 500 ) && accountName == "DeductionDebitAccount")
                {
                    return _IAccountService.Query.Where(x => x.AccountNumber == "1331" || x.AccountNumber == "1332" || x.AccountNumber == "1388" || x.AccountNumber == "3388").ToList();
                }
            }


            
            //lấy danh sách tài khoản mặc định theo ID
            if (_dsAccountDefault == null)
                _dsAccountDefault = Query.ToList();
            if (_dsAccount == null)
            {
                _dsAccount = _IAccountService.Query.ToList();
            }
            AccountDefault accountDefaults = _dsAccountDefault.FirstOrDefault(k => k.TypeID == typeId && k.ColumnName == accountName && k.PPType == isImportPurchase);
            //khai báo danh sách Account trong một tài khoản mặc định của một chứng từ
            if (accountDefaults == null)
                return _dsAccount.OrderBy(p => p.AccountNumber).Where(p => !p.IsParentNode).ToList();
            List<Account> dsAccounts = new List<Account>();
            //mảng string các tài khoản cần lọc
            string[] filterAccounts = accountDefaults.FilterAccount == null
                                          ? new string[0]
                                          : accountDefaults.FilterAccount.Split(';');
            //duyệt các tài khoản cần lọc để thực hiện lọc dữ liệu
            foreach (string filterAccountitem in filterAccounts)
            {
                string accountitem = filterAccountitem;
                dsAccounts.AddRange(_dsAccount.Where(k => k.AccountNumber.StartsWith(accountitem) && k.IsParentNode == false));
            }
            if (accountName.Equals("DebitAccount"))
                _defaultDebitAccount = accountDefaults.DefaultAccount;
            else if (accountName.Equals("CreditAccount"))
                _defaultCreditAccount = accountDefaults.DefaultAccount;
            else
                _defaultVATAccount = accountDefaults.DefaultAccount;
            if (_defaultAccount.Any(p => p.Key == string.Format("{0};{1};{2}", typeId, accountName, isImportPurchase)))
                _defaultAccount.Remove(string.Format("{0};{1};{2}", typeId, accountName, isImportPurchase));
            _defaultAccount.Add(string.Format("{0};{1};{2}", typeId, accountName, isImportPurchase), accountDefaults.DefaultAccount);
            //add vào kết quả, nếu không có tài khoản lọc mặc định (filterAccounts.Length == 0) thì 
            return filterAccounts.Length == 0 ? _dsAccount.OrderBy(a => a.AccountNumber).Where(p => !p.IsParentNode).ToList() : dsAccounts.OrderBy(a => a.AccountNumber).Where(p => !p.IsParentNode).ToList();

            //if (accountDefaults = 7) this = 6;
            //else this = 8;

            //this = accountDefaults = 7 ? 6 : 8;
        }



        /// <summary>
        /// Lấy ra danh sách các AccountDefault theo TypeID
        /// [DUYTN]
        /// </summary>
        /// <param name="typeID">là TypeID truyền vào</param>
        /// <returns>tập các AccountDefault, null nếu bị lỗi</returns>
        public List<AccountDefault> GetAll_ByTypeID(int typeID)
        {
            return Query.Where(p => p.TypeID == typeID).ToList();
        }
        public AccountDefault GetAccountDefaultByTypeIDAndColumnNameAndPPType(int typeID, string columnName, bool? ppType)
        {
            return Query.Where(p => p.TypeID == typeID&&(p.ColumnName==columnName)&&p.PPType==ppType).FirstOrDefault();
    }
        public AccountDefault GetAccountDefaultByTypeIDAndColumnName(int typeID, string columnName)
        {
            return Query.Where(p => p.TypeID == typeID && (p.ColumnName == columnName) ).FirstOrDefault();
        }
        /// <summary>
        /// Lấy về một tài khoản DefaultAccount (nếu null lấy tài khoản đầu tiên trong danh sách)
        /// </summary>
        /// <param name="typeID"></param>
        /// <param name="i">
        /// i=0 : lấy DebitAccount
        /// i=1 : lấy CreditAccount
        /// i=2 : lấy VATAccount
        /// </param>
        /// <returns>trả về chuỗi rỗng nếu báo lỗi</returns>
        public string GetDefaultAccount(int typeID, int i)
        {
            string s = "";
            string defaultAccount = "";
            if (i == 0)
            {
                s = "DebitAccount";
            }
            else if (i == 1)
            {
                s = "CreditAccount";
            }
            else if (i == 2)
            {
                s = "VATAccount";
            }
            if (string.IsNullOrEmpty(s)) return string.Empty;

            defaultAccount = GetAll_ByTypeID(typeID).Count(c => c.ColumnName == s) == 1
                ? (GetAll_ByTypeID(typeID).Single(c => c.ColumnName == s).DefaultAccount ??
                   GetAll_ByTypeID(typeID).Single(c => c.ColumnName == s).FilterAccount.Split(';')[0])
                : "";
            return defaultAccount;
        }


        public List<AccountDefault_Object> GetAll_Sort()
        {
            AccountDefault_Object FAObj = new AccountDefault_Object();
            List<AccountDefault_Object> lst = new List<AccountDefault_Object>();
            List<AccountDefault_Object> lst_Sort = new List<AccountDefault_Object>();
            List<AccountDefault> lstA = Query.OrderBy(c => c.TypeID).ToList();
            List<Accounting.Core.Domain.Type> lstType = _ITypeService.GetAll();
            //lst = (from a in Query.ToList()
            //               where a.TypeID != null && a.FilterAccount != null && a.DefaultAccount != null
            //               select new FAccountDefault_Object()
            //               {
            //                   TypeName = lsttype.SingleOrDefault(c => c.ID == a.TypeID).TypeName ?? "",
            //                   FilterAccount = a.FilterAccount == null ? "" : a.FilterAccount,
            //                   DefaultAccount = a.DefaultAccount == null ? "" : a.DefaultAccount,
            //               }).ToList();
            //return lst;
            //var result = from p in lstA
            //             group p by p.TypeID into g
            //             select g.OrderBy(p => p.ColumnName).First();
            foreach (var item in lstA)
            {
                foreach (var it in lstType)
                {
                    if (it.ID.Equals(item.TypeID) && item.FilterAccount != null)
                    {
                        switch (item.ColumnName)
                        {
                            case "COGSAccount":
                                FAObj.COGSAccount = item.FilterAccount;
                                FAObj.COGSAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "CreditAccount":
                                FAObj.CreditAccount = item.FilterAccount;
                                FAObj.CreditAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "DebitAccount":
                                FAObj.DebitAccount = item.FilterAccount;
                                FAObj.DebitAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "DeductionDebitAccount":
                                FAObj.DeductionDebitAccount = item.FilterAccount;
                                FAObj.DeductionDebitAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "DiscountAccount":
                                FAObj.DiscountAccount = item.FilterAccount;
                                FAObj.DiscountAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "ImportTaxAccount":
                                FAObj.ImportTaxAccount = item.FilterAccount;
                                FAObj.ImportTaxAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "InventoryAccount":
                                FAObj.InventoryAccount = item.FilterAccount;
                                FAObj.InventoryAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "SpecialConsumeTaxAccount":
                                FAObj.SpecialConsumeTaxAccount = item.FilterAccount;
                                FAObj.SpecialConsumeTaxAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                            case "VATAccount":
                                FAObj.VATAccount = item.FilterAccount;
                                FAObj.VATAccountDF = item.DefaultAccount;
                                FAObj.TypeID = item.TypeID;
                                lst.Add(FAObj);
                                FAObj = new AccountDefault_Object();
                                break;
                        }
                        break;
                    }
                }

            }

            var lstnew = from row in lst.AsEnumerable()
                         group row by row.TypeID into g
                         select new AccountDefault_Object
                         {
                             TypeID = g.Key,
                             CreditAccount = String.Join(";", g.Select(row => row.CreditAccount).Where(s => !string.IsNullOrEmpty(s)).ToArray()),
                             CreditAccountDF = String.Join(";", g.Select(row => row.CreditAccountDF).Where(s => !string.IsNullOrEmpty(s)).ToArray()),
                             DebitAccount = String.Join(";", g.Select(row => row.DebitAccount).Where(s => !string.IsNullOrEmpty(s)).ToArray()),
                             DebitAccountDF = String.Join(";", g.Select(row => row.DebitAccountDF).Where(s => !string.IsNullOrEmpty(s)).ToArray()),
                             AccountDefaults = lstA.Where(p => p.TypeID == g.Key && p.FilterAccount != null).ToList()
                         };
            foreach (var item in lstnew)
            {
                lst_Sort.Add(item);
            }
            foreach (var item in lst_Sort)
            {
                foreach (var it in lstType)
                {
                    if (item.TypeID == it.ID)
                    {
                        item.TypeName = it.TypeName;
                        break;
                    }
                }
            }
            return lst_Sort;
        }
    }

}