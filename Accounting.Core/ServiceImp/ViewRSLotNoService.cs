﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class ViewRSLotNoService : BaseService<ViewRSLotNo, Guid>, IViewRSLotNoService
    {
        public ViewRSLotNoService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private IRepositoryLedgerService _IRepositoryLedgerService
        {
            get { return IoC.Resolve<IRepositoryLedgerService>(); }
        }
        public List<ViewRSLotNo> GetAll_OrderBy()
        {
            return Query.OrderBy(o => o.LotNo).ToList();
        }
        public List<ViewRSLotNo> GetByMaterialGoodsID(Guid ID)
        {
            List<RepositoryLedger> lstrs = _IRepositoryLedgerService.GetByMaterialGoodsAndLotNo(ID).ToList();
            List<ViewRSLotNo> lstlotno = Query.ToList().Where(x => lstrs.Any(d => d.LotNo == x.LotNo)).OrderBy(o => o.LotNo).ToList();
            foreach(var x in lstlotno)
            {
                x.TotalIWQuantity = lstrs.FirstOrDefault(c => c.LotNo == x.LotNo).IWQuantity??0;
            }
            return lstlotno;
        }
    }
}
