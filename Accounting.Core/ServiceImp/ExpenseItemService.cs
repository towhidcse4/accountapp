﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class ExpenseItemService : BaseService<ExpenseItem, Guid>, IExpenseItemService
    {
        public ExpenseItemService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IGeneralLedgerService IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public ISystemOptionService ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public List<ExpenseItem> GetListExpenseItemParentID(Guid? parentID)
        {
            List<ExpenseItem> lstChild = Query.Where(p => p.ParentID == parentID).ToList();
            return lstChild;
        }

        public List<string> GetListExpenseItemCode()
        {
            List<string> lst = Query.Select(c => c.ExpenseItemCode).ToList();
            return lst;
        }

        public List<ExpenseItem> GetListExpenseItemOrderFixCode(string orderFixCode)
        {
            List<ExpenseItem> lstChildCheck = Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode)).ToList();
            return lstChildCheck;
        }

        public List<string> GetListOrderFixCodeExpenseItemChild(Guid? parentID)
        {
            return GetListExpenseItemParentID(parentID).Select(a => a.OrderFixCode).ToList();
        }
        public Guid? GetGuidByExpenseItemCode(string ma)
        {
            Guid? id = null;
            ExpenseItem dp = Query.Where(p => p.ExpenseItemCode == ma).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ID;
            }
            return id;
        }
        public string GetExpenseItemCodeByGuid(Guid? ma)
        {
            string id = "";
        ExpenseItem dp = Query.Where(p => p.ID == ma).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ExpenseItemCode;
            }
            return id;
        }
public List<ExpenseItem> GetListExpenseItemGrade(int grade)
        {
            List<ExpenseItem> listChildGrade = Query.Where(a => a.Grade == grade).ToList();
            return listChildGrade;
        }
        /// <summary>
        /// Lấy Khoản mục chi phí theo Mã khoản mục chi phí đang hoạt động
        /// [Longtx]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<ExpenseItem> GetExpenseItemCodeBool(bool isActive)
        {
            return GetAll().OrderBy(p => p.ExpenseItemCode).Where(p => p.IsActive == isActive).ToList();
        }

        public int CountListExpenseItemParentID(Guid? parentID)
        {
            return GetListExpenseItemParentID(parentID).Count;
        }


        /// <summary>
        /// Lấy danh sách các ExpenseItem theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách ExpenseItem được sắp xếp theo dạng cây cha con</returns>
        public List<ExpenseItem> GetByActive_OrderByTreeIsParentNode(bool isActive)
        {
            List<ExpenseItem> get = new List<ExpenseItem>();
            List<ExpenseItem> lst = Query.ToList();
            List<ExpenseItem> parent = lst.Where(x => x.IsActive == isActive && x.ParentID == null).OrderBy(x => x.ExpenseItemCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode) && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }

        /// <summary>
        /// Lấy danh sách các ExpenseItem và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách ExpenseItem được sắp xếp theo dạng cây cha con</returns>
        public List<ExpenseItem> GetAll_OrderByTreeIsParentNode()
        {
            List<ExpenseItem> get = new List<ExpenseItem>();
            List<ExpenseItem> lst = Query.ToList();
            List<ExpenseItem> parent = lst.Where(x => x.ParentID == null).OrderBy(x => x.ExpenseItemCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }
        public List<ExpenseItem> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.ExpenseItemCode).ToList();
        }
        public Guid? GetGuidExpenseItemByCode(string code)
        {
            Guid? id = null;
            ExpenseItem ei = Query.Where(p => p.ExpenseItemCode == code).SingleOrDefault();
            if (ei != null)
            {
                id = ei.ID;
            }
            return id;
        }
        public ExpenseItem GetExpenseItemByGuid(Guid code)
        {
            
            ExpenseItem ei = Query.Where(p => p.ID == code).SingleOrDefault();
            if (ei != null)
            {
                return ei;
            }
            else
            {
                return null;
            }
            
        }
        public List<ExpenseItem> GetListParent()
        {
            return Query.Where(x => x.ParentID == null).ToList();
        }
        public List<ExpenseItem> ReportExpenseItem(List<string> acc, List<ExpenseItemTongHopCP> lst, DateTime from, DateTime to)
        {
            var ddSoGia = ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);
            DateTime fromdate = new DateTime(from.Year, from.Month, from.Day);
            DateTime todate = new DateTime(to.Year, to.Month, to.Day);
            var lstGL = IGeneralLedgerService.Query.Where(x => x.ExpenseItemID != null && x.PostedDate >= fromdate && x.PostedDate <= todate).ToList().Where(c => acc.Any(d => d == c.Account) && lst.Any(d => d.ID == c.ExpenseItemID)).ToList();

            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query;
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query;

            var list_genera1 = (from a in lstGL
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in lstGL
                                join i in listSAInvoice on a.ReferenceID equals i.ID
                                //join b in listSABill on i.BillRefID equals b.ID
                                join d in list_delete on i.BillRefID equals d.SAInvoiceID
                                select a).ToList();
            lstGL = (from a in lstGL
                           where !list_genera1.Any(t => t.ID == a.ID) && !list_genera2.Any(t => t.ID == a.ID)
                     select a).ToList();
            var lstEx = (from a in lstGL
                         group a by a.ExpenseItemID
                        into g
                         select new ExpenseItem
                         {
                             ID = g.Key.Value,
                             ExpenseItemCode = lst.FirstOrDefault(x => x.ID == g.Key.Value).ExpenseItemCode,
                             ExpenseItemName = lst.FirstOrDefault(x => x.ID == g.Key.Value).ExpenseItemName,
                             Amount = (g.Sum(x => Math.Round(x.DebitAmount, lamTronGia)) - g.Sum(x => Math.Round(x.CreditAmount, lamTronGia)))
                         }).ToList();
            return lstEx.OrderBy(x => x.ExpenseItemCode).ToList();
        }
    }

}