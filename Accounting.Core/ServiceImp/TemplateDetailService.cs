using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class TemplateDetailService : BaseService<TemplateDetail, Guid>, ITemplateDetailService
    {
        public TemplateDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.List<TemplateDetail> getTableinTemplate(Guid TemplateID)
        {
            return Query.Where(k => k.TemplateID == TemplateID).ToList();
        }
    }

}