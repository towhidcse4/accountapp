﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMTransferService : BaseService<EMTransfer, Guid>, IEMTransferService
    {
        public EMTransferService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListEMTransferCode()
        {
            List<string> hh = Query.Select(a => a.TransferCode).ToList();
            return hh;
        }
    }
}
