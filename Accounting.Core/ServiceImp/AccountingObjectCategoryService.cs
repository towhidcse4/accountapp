﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class AccountingObjectCategoryService : BaseService<AccountingObjectCategory, Guid>, IAccountingObjectCategoryService
    {
        public AccountingObjectCategoryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.List<AccountingObjectCategory> GetByIsActiveOrderByCode(bool isActive)
        {
            return Query.OrderBy(p => p.AccountingObjectCategoryCode).Where(p => p.IsActive == isActive).ToList();
        }


        /// <summary>
        /// Lấy danh sách các AccountingObjectCategoryCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectCategoryCode, null nếu bị lỗi</returns>
        public List<string> GetAccountingObjectCategoryCode()
        {
            return Query.Select(c => c.AccountingObjectCategoryCode).ToList();
        }
        public List<AccountingObjectCategory> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.AccountingObjectCategoryCode).ToList();
        }

        public Guid? GetGuidAccountingObjectCategoryByCode(string code)
        {
            Guid? id = null;
            AccountingObjectCategory dp = Query.Where(p => p.AccountingObjectCategoryCode == code).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ID;
            }
            return id;
        }
    }

}