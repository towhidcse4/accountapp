﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class TimeSheetSymbolsService : BaseService<TimeSheetSymbols, Guid>, ITimeSheetSymbolsService
    {
        public TimeSheetSymbolsService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// Lấy danh sách Ký hiệu chấm công và sắp xếp theo mã ký hiệu chấm công [khanhtq]
        /// </summary>
        /// <returns>Danh sách ký hiệu chấm công</returns>
        public List<TimeSheetSymbols> OrderByCode()
        {
            return Query.OrderBy(c => c.TimeSheetSymbolsCode).ToList();
        }
        /// <summary>
        /// Danh sách Mã ký hiệu chấm công [khanhtq]
        /// </summary>
        /// <returns>Danh sách mã ký hiệu chấm công</returns>
        public List<string> GetCode()
        {
            return Query.Select(c => c.TimeSheetSymbolsCode).ToList();
        }
        /// <summary>
        /// danh sách các ký hiệu chấm công mặc định
        /// </summary>
        /// <param name="isDefault">bool</param>
        /// <returns>danh sách ký hiệu chấm công mặc định</returns>
        public List<TimeSheetSymbols> GetIsDefault(bool isDefault)
        {
            return Query.Where(t => t.IsDefault == isDefault).ToList();
        }
    }

}