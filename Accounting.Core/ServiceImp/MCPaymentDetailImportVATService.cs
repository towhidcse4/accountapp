using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentDetailImportVATService : BaseService<MCPaymentDetailImportVAT, Guid>, IMCPaymentDetailImportVATService
    {
        public MCPaymentDetailImportVATService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}