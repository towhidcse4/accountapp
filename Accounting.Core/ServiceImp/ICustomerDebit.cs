﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.ServiceImp
{
    public interface ICustomerDebitService : FX.Data.IBaseService<CustomerDebit, Guid>
    {
        /// <summary>
        /// Lấy danh sách khách hàng có phát sinh [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<CustomerDebit> GetCustomerDebits();
        /// <summary>
        /// lấy khách hàng có phát sinh theo mã khách hàng [khanhtq]
        /// </summary>
        /// <param name="customerCode">Mã khách hàng</param>
        /// <returns></returns>
        CustomerDebit GetCustomerDebisByCode(string customerCode);
    }
}
