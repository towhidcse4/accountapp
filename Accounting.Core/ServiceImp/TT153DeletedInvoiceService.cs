using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153DeletedInvoiceService : BaseService<TT153DeletedInvoice, Guid>, ITT153DeletedInvoiceService
    {
        public TT153DeletedInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public ISAInvoiceService _ISAInvoiceService { get { return IoC.Resolve<ISAInvoiceService>(); } }
        public ISABillService _ISABillService { get { return IoC.Resolve<ISABillService>(); } }
        public ISAReturnService _ISAReturnService { get { return IoC.Resolve<ISAReturnService>(); } }
        public IPPDiscountReturnService _IPPDiscountReturnService { get { return IoC.Resolve<IPPDiscountReturnService>(); } }
        public ITT153ReportService _ITT153ReportService { get { return IoC.Resolve<ITT153ReportService>(); } }
        public List<SAInvoice> GetAllFromDateToDate(DateTime From, DateTime To, Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate)
        {
            List<SAInvoice> lstSA = new List<SAInvoice>();
            //List<TT153DeletedInvoice> lst = Query.ToList();
            //var result = (from d in lst
            //         join s in _ISAInvoiceService.GetAll().Where(n => n.Date <= To && n.Date >= From)
            //         on d.SAInvoiceID equals s.ID
            //         where d.InvoiceTypeID == InvoiceTypeID
            //         select d).ToList();
            //return result;
            foreach (TT153DeletedInvoice item in Query.ToList())
            {
                lstSA.Add(_ISAInvoiceService.Getbykey(item.SAInvoiceID));
            }

            return lstSA.Where(n => n.Date <= To && n.Date >= From && n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && !string.IsNullOrEmpty(n.InvoiceNo)).ToList();
        }

        public List<SAInvoice> GetAllSAInvoice(Guid TT153ReportID)
        {
            TT153Report tT153Report = _ITT153ReportService.Getbykey(TT153ReportID);
            List<SAInvoice> lstSA = new List<SAInvoice>();
            foreach (TT153DeletedInvoice item in Query.ToList())
            {
                lstSA.Add(_ISAInvoiceService.Getbykey(item.SAInvoiceID));
            }
            var res = lstSA.Where(n => n != null && n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceNo != null && n.InvoiceNo != "").ToList();
            return res;
        }

        List<int> ITT153DeletedInvoiceService.GetAllFromDateToDate(DateTime From, DateTime To, Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            List<int> lstSA = new List<int>();

            foreach (TT153DeletedInvoice item in Query.ToList())
            {
                SAInvoice sAInvoice = _ISAInvoiceService.Getbykey(item.SAInvoiceID);
                if (sAInvoice == null)
                {
                    SABill sABill = _ISABillService.Getbykey(item.SAInvoiceID);
                    if (sABill == null)
                    {
                        SAReturn sAReturn = _ISAReturnService.Getbykey(item.SAInvoiceID);
                        if (sAReturn == null)
                        {
                            PPDiscountReturn pPDiscountReturn = _IPPDiscountReturnService.Getbykey(item.SAInvoiceID);
                            if (pPDiscountReturn.InvoiceDate <= To && pPDiscountReturn.InvoiceDate >= From && pPDiscountReturn.InvoiceTypeID == InvoiceTypeID && pPDiscountReturn.InvoiceForm == InvoiceForm && pPDiscountReturn.InvoiceTemplate == InvoiceTemplate && pPDiscountReturn.InvoiceSeries == InvoiceSeries)
                                lstSA.Add(int.Parse(pPDiscountReturn.InvoiceNo));
                        }
                        else
                        {
                            if (sAReturn.InvoiceDate <= To && sAReturn.InvoiceDate >= From && sAReturn.InvoiceTypeID == InvoiceTypeID && sAReturn.InvoiceForm == InvoiceForm && sAReturn.InvoiceTemplate == InvoiceTemplate && sAReturn.InvoiceSeries == InvoiceSeries)
                                lstSA.Add(int.Parse(sAReturn.InvoiceNo));
                        }
                    }
                    else
                    {
                        if (sABill.InvoiceDate <= To && sABill.InvoiceDate >= From && sABill.InvoiceTypeID == InvoiceTypeID && sABill.InvoiceForm == InvoiceForm && sABill.InvoiceTemplate == InvoiceTemplate && sABill.InvoiceSeries == InvoiceSeries)
                            lstSA.Add(int.Parse(sABill.InvoiceNo));
                    }
                }
                else
                {
                    if (sAInvoice.InvoiceDate <= To && sAInvoice.InvoiceDate >= From && sAInvoice.InvoiceTypeID == InvoiceTypeID && sAInvoice.InvoiceForm == InvoiceForm && sAInvoice.InvoiceTemplate == InvoiceTemplate && sAInvoice.InvoiceSeries == InvoiceSeries)
                        lstSA.Add(int.Parse(sAInvoice.InvoiceNo));
                }

            }
            return lstSA;
        }
    }

}