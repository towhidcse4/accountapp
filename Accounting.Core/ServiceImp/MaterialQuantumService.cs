﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MaterialQuantumService: BaseService<MaterialQuantum ,Guid>,IMaterialQuantumService
    {
        public MaterialQuantumService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        /// <summary>
        /// Lấy ra danh sách MaterialQuantum đã sắp xếp theo thứ tự tăng dần MaterialQuantumCode
        /// [DUYTN], sửa ngày 07/04/2014
        /// </summary>
        /// <returns>tập các MaterialQuantum, null nếu bị lỗi</returns>
        public List<MaterialQuantum> GetOrderBy_MaterialQuantumCode()
        {
            return Query.ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialQuantumCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các MaterialQuantumCode, null nếu bị lỗi</returns>
        public List<string> GetMaterialQuantumCode()
        {
            return Query.Select(c => c.CostSetName).ToList();
        }
    }

}