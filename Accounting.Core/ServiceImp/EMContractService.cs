using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class EMContractService : BaseService<EMContract, Guid>, IEMContractService
    {
        public EMContractService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public EMContract GetByID(Guid id)
        {
            return (EMContract)Query.Where(e => e.ID == id && e.IsActive == true);
        }


        public List<EMContract> GetAll_IsActive(bool isActive)
        {
            return Query.Where(c => c.IsActive == isActive).ToList();
        }

        public List<EMContract> GetAll_OrderBy()
        {
            return Query.Where(c => c.IsActive).OrderBy(p => p.Code).ToList();
        }

        public List<EMContract> GetAllContractBuy()
        {
            return Query.Where(p => (p.TypeID == 850)).OrderByDescending(p => p.Code).ToList();
        }

        public List<string> GetCloseReason()
        {
            List<string> lst = new List<string>();
            lst = Query.Where(p => p.ClosedReason != null).GroupBy(p => p.ClosedReason).Select(p => p.Key).ToList();
            return lst;
        }

        public List<EMContract> GetAllContractSale()
        {
            return Query.Where(p => (p.TypeID == 860)).OrderByDescending(p => p.Code).ToList();
        }

        public List<EMContract> GetAllProject()
        {
            return Query.Where(p => p.TypeID == 860 && p.IsProject == true).OrderByDescending(p => p.Code).ToList();
        }

        public Guid? GetGuidEMContractByCode(string code)
        {
            Guid? id = null;
            EMContract ec = Query.Where(p => p.Code == code).SingleOrDefault();
            if (ec != null)
            {
                id = ec.ID;
            }
            return id;
        }
    }

}