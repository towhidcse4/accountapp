using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Collections.Generic;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class PSTimeSheetSummaryDetailService: BaseService<PSTimeSheetSummaryDetail ,Guid>,IPSTimeSheetSummaryDetailService
    {
        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }
        public PSTimeSheetSummaryDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<PSTimeSheetSummaryDetail> FindEmployeeByDepartment(IList<Guid> departments)
        {
            //return _IAccountingObjectService.Query.Where(x => x.IsActive && x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty))
            //                        .Select(x => new PSTimeSheetSummaryDetail
            //                        {
            //                            EmployeeID = x.ID,
            //                            AccountingObjectName = x.AccountingObjectName,
            //                            DepartmentID = x.DepartmentID
            //                        }).ToList();
            return _IAccountingObjectService.Query.Join(_IDepartmentService.Query, n => n.DepartmentID, m => m.ID, (n, m) => new PSTimeSheetSummaryDetail
            {
                EmployeeID = n.ID,
                AccountingObjectName = n.AccountingObjectName,
                DepartmentID = n.DepartmentID,
                DepartmentCode = m.DepartmentCode,
                EmployeeCode = n.AccountingObjectCode,
                IsActive = n.IsActive,
                IsEmployee = n.IsEmployee
            }).Where(x => x.IsActive && x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty)).OrderBy(n => n.DepartmentCode).ThenBy(n => n.EmployeeCode).ToList();
        }
    }

}