﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FixedAssetDetailService:FX.Data.BaseService<FixedAssetDetail, Guid>, IFixedAssetDetailService
    {
        public FixedAssetDetailService(String sessionFactoryConfigPath) : base(sessionFactoryConfigPath) { }
        public IList<FixedAssetDetail> getFixedAssetDetailbyID(Guid id)
        {
            return Query.Where(k => k.FixedAssetID == id).ToList();
        }
        public List<FixedAssetDetail> getFixedAssetDetailbyIDlst(Guid id)
        {
            return Query.Where(k => k.FixedAssetID == id).ToList();
        }
    }
}
