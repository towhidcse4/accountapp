﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBInternalTransferService : BaseService<MBInternalTransfer, Guid>, IMBInternalTransferService
    {
        public MBInternalTransferService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy ra đối tượng MBInternalTransfer theo No
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public MBInternalTransfer GetByNo(string input)
        {
            return Query.First(mb => mb.No == input);
        }


        public MBInternalTransfer GetById(Guid input)
        {
            return Query.First(mb => mb.ID == input);
        }
    }

}