﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class BudgetItemService : BaseService<BudgetItem, Guid>, IBudgetItemService
    {
        public BudgetItemService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }


        public List<BudgetItem> GetListBudgetItemParentID(Guid? parentID)
        {
            List<BudgetItem> lstChild = Query.Where(p => p.ParentID == parentID).ToList();
            return lstChild;
        }

        public List<BudgetItem> GetListBudgetItemBudgetItemType(int budgetItemType)
        {
            List<BudgetItem> getParent = Query.Where(a => a.BudgetItemType == budgetItemType).ToList();
            return getParent;
        }

        public List<string> GetListOrderFixCodeBudgetItemChild(Guid? parentID)
        {
            List<string> lstOrderFixCodeChild = Query.Where(a => a.ParentID == parentID)
                        .Select(a => a.OrderFixCode).ToList();
            return lstOrderFixCodeChild;
        }

        public List<BudgetItem> GetListBudgetItemOrderFixCode(string orderFixCode)
        {
            List<BudgetItem> lstChildCheck = Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode)).ToList();
            return lstChildCheck;
        }

        public List<BudgetItem> GetListBudgetItemGrade(int grade)
        {
            List<BudgetItem> listChildGrade = Query.Where(a => a.Grade == grade).ToList();
            return listChildGrade;
        }

        public List<int> GetListBudgetItemType()
        {
            List<int> hh = Query.Select(a => a.BudgetItemType).ToList();
            return hh;
        }

        public List<string> GetListBudgetItemCode()
        {
            List<string> hh = Query.Select(a => a.BudgetItemCode).ToList();
            return hh;
        }

        public List<BudgetItem> GetListBudgetItemBudgetItemNameStartWith(string startWith)
        {
            List<BudgetItem> lst = new List<BudgetItem>();
            string[] arrayAccount = new string[] { };
            if (!string.IsNullOrEmpty(startWith))
            {
                arrayAccount = startWith.Split(';');
            }
            foreach (var itemAccount in arrayAccount)
            {
                lst.AddRange(Query.Where(a => a.BudgetItemName.StartsWith(itemAccount)));
            }
            return lst;
        }
        /// <summary>
        /// L?y danh sách BudgetItemCode t? BudgetItem theo isActive= true
        /// [Longtx]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<BudgetItem> GetBudgetItemCode(bool isActive)
        {
            return Query.OrderBy(p => p.BudgetItemCode).Where(p => p.IsActive == isActive).ToList();
        }

        public int CountBudgetItemParentID(Guid? parentID)
        {
            return GetListBudgetItemParentID(parentID).Count;
        }


        /// <summary>
        /// Lấy danh sách các BudgetItem theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách BudgetItem được sắp xếp theo dạng cây cha con</returns>
        public List<BudgetItem> GetByActive_OrderByTreeIsParentNode(bool isActive)
        {
            List<BudgetItem> get = new List<BudgetItem>();
            List<BudgetItem> lst = Query.ToList();
            List<BudgetItem> parent = lst.Where(x => x.IsActive == isActive && x.ParentID == null).OrderBy(x => x.BudgetItemCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode) && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }

        /// <summary>
        /// Lấy danh sách các BudgetItem và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách BudgetItem được sắp xếp theo dạng cây cha con</returns>
        public List<BudgetItem> GetAll_OrderByTreeIsParentNode()
        {
            List<BudgetItem> get = new List<BudgetItem>();
            List<BudgetItem> lst = Query.ToList();
            List<BudgetItem> parent = lst.Where(x => x.ParentID == null).OrderBy(x => x.BudgetItemCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }
        public List<BudgetItem> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.BudgetItemCode).ToList();
        }

        public Guid GetGuidBudgetItemByCode(string code)
        {
            Guid id = Guid.Empty;
            BudgetItem ag = Query.Where(p => p.BudgetItemCode == code).SingleOrDefault();
            if (ag != null)
            {
                id = ag.ID;
            }
            return id;
        }
    }

}