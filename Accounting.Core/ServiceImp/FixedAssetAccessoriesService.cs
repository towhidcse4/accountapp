﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
namespace Accounting.Core.ServiceImp
{
    public class FixedAssetAccessoriesService : BaseService<FixedAssetAccessories, Guid>, IFixedAssetAccessoriesService
    {
        public FixedAssetAccessoriesService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath) { }
    }
}
