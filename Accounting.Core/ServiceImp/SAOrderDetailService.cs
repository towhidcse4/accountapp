using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class SAOrderDetailService: BaseService<SAOrderDetail ,Guid>,ISAOrderDetailService
    {
        ISAOrderService _ISAOrderService { get { return IoC.Resolve<ISAOrderService>(); } }
        public SAOrderDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<SAOrderDetail> GetSAOrderDetailByOrderID(Guid OrderID)
        {
            return Query.Where(x => x.SAOrderID == OrderID).ToList();
        }

        public List<SAOrderDetail> GetSAOrderDetailBySAOrderCode(Guid? SAOrderID, Guid? ContractID)
        {
            return Query.Where(x => x.SAOrderID == SAOrderID && x.ContractID == ContractID).ToList();
        }

        public List<SAOrderDetail> GetSAOrderDetailByContractID(Guid ContractID)
        {
            return Query.Where(x => x.ContractID == ContractID).ToList();
        }
    }

}