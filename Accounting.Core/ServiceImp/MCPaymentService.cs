﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentService : BaseService<MCPayment, Guid>, IMCPaymentService
    {

        public MCPaymentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<TIOriginalVoucher> FindByListID(string originalVoucher)
        {
            string[] ss = originalVoucher.Substring(0, originalVoucher.Length - 1).Split(';');
            List<Guid> guids = new List<Guid>();
            foreach (string s in ss)
            {
                guids.Add(Guid.Parse(s));
            }
            return Query.Where(haha => guids.Contains(haha.ID)).Select(hihi => new TIOriginalVoucher
            {
                PostedDate = hihi.PostedDate,
                Date = hihi.Date,
                No = hihi.No,
                Reason = hihi.Reason,
                Amount = hihi.TotalAmountOriginal,
                ID = hihi.ID
            }).ToList();
        }

        public MCPayment GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        public List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null)
        {
            List<MCPayment> lst = new List<MCPayment>();
            if(currencyID == null) lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                     && x.Recorded ).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            else lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                    && x.Recorded &&  x.CurrencyID == currencyID).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<TIOriginalVoucher> result = new List<TIOriginalVoucher>();
            foreach (MCPayment io in lst)
            {
                TIOriginalVoucher ov = new TIOriginalVoucher()
                {
                    Amount = io.TotalAmountOriginal,
                    PostedDate = io.PostedDate,
                    Date = io.Date,
                    No = io.No,
                    Reason = io.Reason,
                    ID = io.ID

                };
                result.Add(ov);
            }
            return result;
        }

        public List<MCPayment> OrderByPostedDate(int postedYear)
        {
            return Query.Where(k => k.PostedDate.Year == postedYear).OrderByDescending(k => k.PostedDate).ToList();
        }
        public MCPayment findByAuditID(Guid ID)
        {
            return Query.FirstOrDefault(x => x.AuditID == ID);
        }
    }
}