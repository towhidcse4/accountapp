﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class RefVoucherService : BaseService<RefVoucher, Guid>, IRefVoucherService
    {
        public RefVoucherService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<RefVoucher> GetByRefID2(Guid refID2)
        {
            return Query.Where(o => o.RefID2 == refID2).ToList();
        }
    }
}