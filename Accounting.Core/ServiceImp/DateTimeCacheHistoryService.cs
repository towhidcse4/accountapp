﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;


namespace Accounting.Core.ServiceImp
{
    public class DateTimeCacheHistoryService : BaseService<DateTimeCacheHistory, Guid>, IDateTimeCacheHistoryService
    {
        public DateTimeCacheHistoryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        

        public DateTimeCacheHistory GetDateTimeCacheHistoryByTypeID(int? typeID, Guid iDUser)
        {
            DateTimeCacheHistory Child = Query.Where(p => p.TypeID == typeID && p.IDUser == iDUser).FirstOrDefault();
            return Child;
        }
        public DateTimeCacheHistory GetDateTimeCacheHistoryBySubSystemCode(string subSystemCode, Guid iDUser)
        {
            DateTimeCacheHistory Child = Query.Where(p => p.SubSystemCode == subSystemCode && p.IDUser==iDUser).FirstOrDefault();
            return Child;
        }

        public List<DateTimeCacheHistory> GetAllDateTimeCacheHistory()
        {
            
            return GetAll().ToList();
        }
        
    }

}