using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class SABillService : BaseService<SABill, Guid>, ISABillService
    {
        public SABillService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<SABill> GetListSABillTT153(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            return Query.Where(n => n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != null && n.InvoiceNo != "").ToList();
        }

        public int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            var lst = Query.Where(n => n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null).ToList();
            if (lst.Count > 0) return lst.ToList().Select(n => int.Parse(n.InvoiceNo)).Max();
            else return 0;
        }
    }

}