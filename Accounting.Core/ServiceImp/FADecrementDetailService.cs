﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FADecrementDetailService :BaseService<FADecrementDetail,Guid>,IFADecrementDetailService
    {
        public IFADecrementService _IFADecrementService { get { return IoC.Resolve<IFADecrementService>(); } }

        public FADecrementDetailService(string sessionFactoryConfigPath):base(sessionFactoryConfigPath)
        { }

        public DateTime? FindDecrementDate(Guid FixedAssetID, DateTime PostedDate)
        {
            List<DateTime> dates = (from a in Query
                                    join b in _IFADecrementService.Query on a.FADecrementID equals b.ID
                                    where b.PostedDate.Month == PostedDate.Month && b.PostedDate.Year == PostedDate.Year
                                        && a.FixedAssetID == FixedAssetID
                                    orderby b.PostedDate
                                    select b.PostedDate).ToList();
            return dates.Count > 0 ? dates[0] : (DateTime?)null;
        }

        public List<FADecrementDetail> GetByFADecrementDetailID(Guid faDecrementID)
        {
            return Query.Where(k => k.FADecrementID ==faDecrementID).ToList();
        }
    }
}
