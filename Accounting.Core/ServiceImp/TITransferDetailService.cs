﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TITransferDetailService : BaseService<TITransferDetail, Guid>, ITITransferDetailService
    {
        public TITransferDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<TITransferDetail> GetByTITransferID(Guid TITransferID)
        {
            return Query.Where(k => k.TITransferID == TITransferID).ToList();
        }

       
    }

}
