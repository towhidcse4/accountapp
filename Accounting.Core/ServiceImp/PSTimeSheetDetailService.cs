using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Collections.Generic;
using FX.Core;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class PSTimeSheetDetailService: BaseService<PSTimeSheetDetail ,Guid>,IPSTimeSheetDetailService
    {

        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public ITimeSheetSymbolsService _ITimeSheetSymbolsService { get { return IoC.Resolve<ITimeSheetSymbolsService>(); } }
        public IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }

        public PSTimeSheetDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}


        public List<PSTimeSheetDetail> FindEmployeeByDepartment(List<Guid> departments, int daysInMonth, List<int> t7, List<int> cn, decimal? WorkingHoursInDay, 
            Guid? baseOn, bool isNewEmployee, bool isUnemployee, bool st7, bool ct7, bool scn, bool ccn)
        {
            if (departments == null || departments.Count == 0)
            {
                return new List<PSTimeSheetDetail>();
            }
            List<PSTimeSheetDetail> lst = null;
            if (baseOn != null)
            {
                if (isUnemployee)
                {
                    lst = _IAccountingObjectService.Query.Where(x => x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty) && Query.Any(y => y.PSTimeSheetID == baseOn && y.EmployeeID == x.ID))
                                .Select(x => new PSTimeSheetDetail
                                {
                                    EmployeeID = x.ID,
                                    EmployeeCode = x.AccountingObjectCode,
                                    AccountingObjectName = x.AccountingObjectName,
                                    DepartmentID = x.DepartmentID
                                }).ToList();
                }
                else if (!isUnemployee)
                {
                    lst = _IAccountingObjectService.Query.Where(x => x.IsActive && x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty) && Query.Any(y => y.PSTimeSheetID == baseOn && y.EmployeeID == x.ID))
                                .Select(x => new PSTimeSheetDetail
                                {
                                    EmployeeID = x.ID,
                                    AccountingObjectName = x.AccountingObjectName,
                                    EmployeeCode = x.AccountingObjectCode,
                                    DepartmentID = x.DepartmentID
                                }).ToList();
                }
                if (isNewEmployee)
                {
                    lst.AddRange(_IAccountingObjectService.Query.Where(x => x.IsActive && x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty) && !Query.Any(y => y.PSTimeSheetID == baseOn && y.EmployeeID == x.ID))
                                    .Select(x => new PSTimeSheetDetail
                                    {
                                        EmployeeID = x.ID,
                                        EmployeeCode = x.AccountingObjectCode,
                                        AccountingObjectName = x.AccountingObjectName,
                                        DepartmentID = x.DepartmentID
                                    }).ToList());
                }
            }
            else
            {
                lst = _IAccountingObjectService.Query.Where(x => x.IsActive && x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty))
                    .Select(x => new PSTimeSheetDetail
                    {
                        EmployeeID = x.ID,
                        AccountingObjectName = x.AccountingObjectName,
                        EmployeeCode = x.AccountingObjectCode,
                        DepartmentID = x.DepartmentID
                    }).ToList();
            }

            TimeSheetSymbols symbol = _ITimeSheetSymbolsService.Query.Where(x => x.IsDefault == true).FirstOrDefault();
            TimeSheetSymbols halfDay = _ITimeSheetSymbolsService.Query.Where(x => x.IsHalfDayDefault == true).FirstOrDefault();
            if (halfDay == null) halfDay = new TimeSheetSymbols();
            List<Department> departmentss = _IDepartmentService.GetAll();
            if (symbol != null)
            {
                string sym = WorkingHoursInDay != null ? String.Format("{0} {1}", symbol.TimeSheetSymbolsCode, decimal.Round(WorkingHoursInDay ?? 0, 2, MidpointRounding.AwayFromZero)) : symbol.TimeSheetSymbolsCode;
                lst.ForEach(x =>
                {
                    x.Day1 = GetSymbol(1, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day2 = GetSymbol(2, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day3 = GetSymbol(3, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day4 = GetSymbol(4, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day5 = GetSymbol(5, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day6 = GetSymbol(6, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day7 = GetSymbol(7, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day8 = GetSymbol(8, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day9 = GetSymbol(9, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day10 = GetSymbol(10, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day11 = GetSymbol(11, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day12 = GetSymbol(12, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day13 = GetSymbol(13, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day14 = GetSymbol(14, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day15 = GetSymbol(15, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day16 = GetSymbol(16, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day17 = GetSymbol(17, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day18 = GetSymbol(18, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day19 = GetSymbol(19, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day20 = GetSymbol(20, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day21 = GetSymbol(21, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day22 = GetSymbol(22, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day23 = GetSymbol(23, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day24 = GetSymbol(24, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day25 = GetSymbol(25, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day26 = GetSymbol(26, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day27 = GetSymbol(27, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day28 = GetSymbol(28, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn);
                    x.Day29 = daysInMonth > 28 ? GetSymbol(29, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn) : null;
                    x.Day30 = daysInMonth > 29 ? GetSymbol(30, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn) : null;
                    x.Day31 = daysInMonth == 31 ? GetSymbol(31, t7, cn, symbol, halfDay, WorkingHoursInDay, st7, ct7, scn, ccn) : null;
                    x.PaidWorkingDay = (daysInMonth - (!st7 ? (decimal)t7.Count / 2 : 0) - (!ct7 ? (decimal)t7.Count / 2 : 0) - (!scn ? (decimal)cn.Count / 2 : 0) - (!ccn ? (decimal)cn.Count / 2 : 0)) * (WorkingHoursInDay ?? 1);
                    x.DepartmentCode = departmentss.Where(z => z.ID == x.DepartmentID).FirstOrDefault().DepartmentCode;
                });
            }
            
            int i = 1;
            
            var y2 = lst.OrderBy(x => x.DepartmentCode).ThenBy(x => x.EmployeeCode).ToList();
            foreach (var a in y2)
            {
                a.OrderPriority = i++;
            }
            return y2;
        }

        public string GetSymbol(int day, List<int> t7, List<int> cn, TimeSheetSymbols allDay, TimeSheetSymbols haftDay, decimal? WorkingHoursInDay,
            bool st7, bool ct7, bool scn, bool ccn)
        {
            string sym = WorkingHoursInDay != null ? String.Format("{0} {1}", allDay.TimeSheetSymbolsCode, WorkingHoursInDay) : allDay.TimeSheetSymbolsCode;
            string symHd = WorkingHoursInDay != null ? String.Format("{0} {1}", haftDay.TimeSheetSymbolsCode, WorkingHoursInDay / 2) : haftDay.TimeSheetSymbolsCode;
            if (!t7.Contains(day) && !cn.Contains(day))
            {
                return sym;
            }
            else
            {
                if (t7.Contains(day))
                {
                    if (st7 && ct7) return sym;
                    else if (st7 || ct7) return symHd;
                    else return null;
                }
                else if (cn.Contains(day))
                {
                    if (scn && ccn) return sym;
                    else if (scn || ccn) return symHd;
                    else return null;
                }
                else return null;
            }
        }
    }

}