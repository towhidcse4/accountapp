using System;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FAAuditService: BaseService<FAAudit ,Guid>,IFAAuditService
    {
        public FAAuditService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}