using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM01TAINService: BaseService<TM01TAIN ,Guid>,ITM01TAINService
    {
        public TM01TAINService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}