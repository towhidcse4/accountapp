﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class ViewEInvoiceService : BaseService<ViewEInvoice, Guid>, IViewEInvoiceService
    {
        public ViewEInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }
}
