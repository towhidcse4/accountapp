using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class PSSalaryTaxInsuranceRegulationService: BaseService<PSSalaryTaxInsuranceRegulation ,Guid>,IPSSalaryTaxInsuranceRegulationService
    {
        public PSSalaryTaxInsuranceRegulationService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public bool checkDate(Guid id, DateTime fromDate, DateTime toDate)
        {
            return Query.Any(x => x.ID != id && ((x.FromDate <= fromDate && x.ToDate >= fromDate) || (x.FromDate <= toDate && x.ToDate >= toDate) || (fromDate <= x.FromDate && toDate >= x.FromDate)));
        }

        public PSSalaryTaxInsuranceRegulation FindByDate(DateTime postedDate)
        {
            DateTime now = new DateTime(postedDate.Year, postedDate.Month, postedDate.Day);
            var lst = Query.Where(x => x.FromDate <= now && x.ToDate >= now).ToList();
            if (lst != null && lst.Count > 0)
                return lst[0];
            return null;
        }
    }

}