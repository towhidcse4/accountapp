﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FAIncrementService : BaseService<FAIncrement, Guid>, IFAIncrementService
    {
        ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public FAIncrementService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListFAIncrementCode()
        {
            List<string> list = Query.Select(p => p.No).ToList();
            return list;
        }
    }

}