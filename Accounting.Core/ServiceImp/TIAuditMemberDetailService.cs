using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class TIAuditMemberDetailService: BaseService<TIAuditMemberDetail ,Guid>,ITIAuditMemberDetailService
    {
        public TIAuditMemberDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}