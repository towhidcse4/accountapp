﻿using System;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMRegistrationDetailService : BaseService<EMRegistrationDetail, Guid>, IEMRegistrationDetailService
    {
        public EMRegistrationDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public BindingList<EMRegistrationDetail> GetBinddingEMRegistrationDetailID(Guid? inputID)
        {
            BindingList<EMRegistrationDetail> bdEMRegistrationDetail = new BindingList<EMRegistrationDetail>(Query.Where(p => p.EMRegistrationID == inputID).ToList());
            return bdEMRegistrationDetail;
        }
    }

}


