using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class PPServiceDetailService: BaseService<PPServiceDetail ,Guid>,IPPServiceDetailService
    {
        public PPServiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IList<PPServiceDetail> GetPPServiceDetailbyID(Guid id)
        {
            return Query.Where(k => k.PPServiceID == id).ToList();
        }
    }

}