﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class GOtherVoucherService : BaseService<GOtherVoucher, Guid>, IGOtherVoucherService
    {
        public IGOtherVoucherDetailService _IGOtherVoucherDetailService { get { return IoC.Resolve<IGOtherVoucherDetailService>(); } }
        public IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        public IMBTellerPaperService _IMBTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        public IPSSalarySheetService _IPSSalarySheetService { get { return IoC.Resolve<IPSSalarySheetService>(); } }
        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IMCPaymentDetailSalaryService _IMCPaymentDetailSalaryService { get { return IoC.Resolve<IMCPaymentDetailSalaryService>(); } }
        public IMBTellerPaperDetailSalaryService _IMBTellerPaperDetailSalaryService { get { return IoC.Resolve<IMBTellerPaperDetailSalaryService>(); } }
        public IPSSalarySheetDetailService _IPSSalarySheetDetailService { get { return IoC.Resolve<IPSSalarySheetDetailService>(); } }
        public IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }

        public GOtherVoucherService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public GOtherVoucher GetGOtherVoucherReturnbyNo(string No)
        {
            return Query.FirstOrDefault(p => p.No == No);
        }
        public bool GetGOtherVoucherGetByDate(DateTime dt)
        {
            if (Query.Any(x => x.PostedDate.Month == dt.Month && x.PostedDate.Year == dt.Year && x.TypeID == 660)) return false;
            else return true;
        }
        #region Nghiệp vụ 
        /// <summary>
        /// Thêm mới GOtherVoucher
        /// </summary>
        /// <param name="GOtherVoucher">Đối tượng GOtherVoucher</param>
        /// <param name="TypeGroup"></param>
        public void Add(GOtherVoucher GOtherVoucher, int TypeGroup)
        {
            try
            {
                this.BeginTran();
                this.CreateNew(GOtherVoucher);
                #region Up bảng gencode sinh chứng từ tiếp theo
                IGenCodeService IgenCodeService = IoC.Resolve<IGenCodeService>();
                GenCode gencode = IgenCodeService.getGenCode(TypeGroup);
                Dictionary<string, string> dicNo = Utils.CheckNo(GOtherVoucher.No);
                if (dicNo != null)
                {
                    gencode.Prefix = dicNo["Prefix"];
                    decimal _decimal;
                    decimal.TryParse(dicNo["Value"], out _decimal);
                    gencode.CurrentValue = _decimal + 1;
                    gencode.Suffix = dicNo["Suffix"];
                    IgenCodeService.Update(gencode);
                }
                #endregion

                this.CommitTran();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra khi tạo mới chứng từ.\nHãy kiểm tra lại!", "Thông báo",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.RolbackTran();
            }
        }
        /// <summary>
        /// Lưu sửa GOtherVoucher
        /// </summary>
        /// <param name="GOtherVoucher">Đối tượng GOtherVoucher</param>
        public void Edit(GOtherVoucher GOtherVoucher)
        {
            try
            {
                BeginTran();
                Update(GOtherVoucher);
                CommitTran();
            }
            catch
            {
                MessageBox.Show("Có lỗi xảy ra khi lưu chứng từ.\nHãy kiểm tra lại!", "Thông báo",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                RolbackTran();
            }
        }

        /// <summary>
        /// Lưu sổ cái
        /// </summary>
        /// <param name="No"></param>
        /// <param name="_dsAccount">List Account nếu có (mặc định để trống)</param>
        public void Saveleged(string No, IList<Account> _dsAccount = null)
        {
            try
            {
                GOtherVoucher temp = GetGOtherVoucherReturnbyNo(No);
                if (temp == null)
                {
                    return;
                }
                List<GOtherVoucherDetail> listTempDetail =
                    IoC.Resolve<IGOtherVoucherDetailService>().GetByGOtherVoucherID(temp.ID);
                if (_dsAccount == null)
                    _dsAccount = IoC.Resolve<IAccountService>().GetListAccountIsActive(true);
                foreach (GOtherVoucherDetail item in listTempDetail)
                {
                    Account debitAccount = _dsAccount.FirstOrDefault(p => p.AccountNumber == item.DebitAccount);
                    Account creditAccount = _dsAccount.FirstOrDefault(p => p.AccountNumber == item.CreditAccount);
                    if (CheckAccountIsError(debitAccount, item, true))
                        return;
                    else if (CheckAccountIsError(creditAccount, item, false))
                        return;
                }
                IoC.Resolve<IGeneralLedgerService>().SaveLedger(temp);
            }
            catch
            {
                MessageBox.Show("Lỗi ghi chứng từ vào sổ cái!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                RolbackTran();
            }
        }
        #endregion

        #region Utils
        /// <summary>
        /// Check lỗi khi lưu sổ cái dựa trên TK nợ - có
        /// </summary>
        /// <param name="account"></param>
        /// <param name="temp"></param>
        /// <param name="isDebitAccount"></param>
        /// <returns></returns>
        private bool CheckAccountIsError(Account account, GOtherVoucherDetail temp, bool isDebitAccount)
        {
            bool isError = false;
            string msg = string.Empty;
            switch (account.DetailType)
            {
                case "0":
                    if (isDebitAccount && temp.DebitAccountingObjectID == null)
                    {
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Đối tượng nợ. Ghi sổ không thành công.";
                        isError = true;
                    }
                    else if (temp.CreditAccountingObjectID == null)
                    {
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Đối tượng có. Ghi sổ không thành công.";
                        isError = true;
                    }
                    break;
                case "1":
                    if (isDebitAccount && temp.DebitAccountingObjectID == null)
                    {
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Đối tượng nợ. Ghi sổ không thành công.";
                        isError = true;
                    }
                    else if (temp.CreditAccountingObjectID == null)
                    {
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Đối tượng có. Ghi sổ không thành công.";
                        isError = true;
                    }
                    break;
                case "2":
                    if (temp.EmployeeID == null)
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Nhân viên. Ghi sổ không thành công."; isError = true;
                    break;
                case "3":
                    if (temp.CostSetID == null)
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Đối tượng tập hợp chi phí. Ghi sổ không thành công."; isError = true;
                    break;
                case "4":
                    if (temp.ContractID == null)
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Hợp đồng. Ghi sổ không thành công."; isError = true;
                    break;
                case "7":
                    if (temp.ExpenseItemID == null)
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Khoản mục chi phí. Ghi sổ không thành công."; isError = true;
                    break;
                case "9":
                    if (temp.DepartmentID == null)
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Phòng ban. Ghi sổ không thành công."; isError = true;
                    break;
                case "10":
                    if (temp.BudgetItemID == null)
                        msg = "Lỗi ghi chứng từ vào sổ cái: thiếu Mục thu/chi. Ghi sổ không thành công."; isError = true;
                    break;
                default:
                    break;
            }
            if (isError) MessageBox.Show(msg, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return isError;
        }

        public List<TIOriginalVoucher> getOriginalVoucher(DateTime dteDateFrom, DateTime dteDateTo, string currencyID = null)
        {
            List<GOtherVoucher> lst = Query.Where(x => x.Date >= dteDateFrom && x.Date <= dteDateTo
                    && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<TIOriginalVoucher> result = new List<TIOriginalVoucher>();
            foreach (GOtherVoucher io in lst)
            {
                TIOriginalVoucher ov = new TIOriginalVoucher()
                {
                    Amount = io.TotalAmountOriginal,
                    PostedDate = io.PostedDate,
                    Date = io.Date,
                    No = io.No,
                    Reason = io.Reason,
                    ID = io.ID

                };
                result.Add(ov);
            }
            return result;
        }

        public List<TIOriginalVoucher> FindByListID(string originalVoucher)
        {
            string[] ss = originalVoucher.Substring(0, originalVoucher.Length - 1).Split(';');
            List<Guid> guids = new List<Guid>();
            foreach (string s in ss)
            {
                guids.Add(Guid.Parse(s));
            }
            return Query.Where(haha => guids.Contains(haha.ID)).Select(hihi => new TIOriginalVoucher
            {
                PostedDate = hihi.PostedDate,
                Date = hihi.Date,
                No = hihi.No,
                Reason = hihi.Reason,
                Amount = hihi.TotalAmountOriginal,
                ID = hihi.ID
            }).ToList();
        }

        public List<InsurancePayment> FindInsurancePayment(DateTime date)
        {
            List<MCPayment> mCPayments = _IMCPaymentService.Query.Where(x => x.PostedDate <= date && x.Recorded && x.TypeID == 115).ToList();
            List<MBTellerPaper> mBTellerPapers = _IMBTellerPaperService.Query.Where(x => x.PostedDate <= date && x.Recorded && x.TypeID == 125).ToList();
            decimal bhxh = 0, bhyt = 0, bhtn = 0, cd = 0;
            if (mCPayments != null && mCPayments.Count > 0)
            {
                foreach (MCPayment mCPayment in mCPayments)
                {
                    MCPaymentDetailInsurance detail1 = mCPayment.MCPaymentDetailInsurances.Where(x => x.DebitAccount == "3383").FirstOrDefault();
                    if (detail1 != null) bhxh += detail1.PayAmount;

                    MCPaymentDetailInsurance detail2 = mCPayment.MCPaymentDetailInsurances.Where(x => x.DebitAccount == "3384").FirstOrDefault();
                    if (detail2 != null) bhyt += detail2.PayAmount;

                    MCPaymentDetailInsurance detail3 = mCPayment.MCPaymentDetailInsurances.Where(x => x.DebitAccount == "3385").FirstOrDefault();
                    if (detail3 != null) bhtn += detail3.PayAmount;

                    MCPaymentDetailInsurance detail4 = mCPayment.MCPaymentDetailInsurances.Where(x => x.DebitAccount == "3382").FirstOrDefault();
                    if (detail4 != null) cd += detail4.PayAmount;
                }

            }

            if (mBTellerPapers != null && mBTellerPapers.Count > 0)
            {
                foreach (MBTellerPaper mBTellerPaper in mBTellerPapers)
                {
                    MBTellerPaperDetailInsurance detail1 = mBTellerPaper.MBTellerPaperDetailInsurances.Where(x => x.DebitAccount == "3383").FirstOrDefault();
                    if (detail1 != null) bhxh += detail1.PayAmount;

                    MBTellerPaperDetailInsurance detail2 = mBTellerPaper.MBTellerPaperDetailInsurances.Where(x => x.DebitAccount == "3384").FirstOrDefault();
                    if (detail2 != null) bhyt += detail2.PayAmount;

                    MBTellerPaperDetailInsurance detail3 = mBTellerPaper.MBTellerPaperDetailInsurances.Where(x => x.DebitAccount == "3385").FirstOrDefault();
                    if (detail3 != null) bhtn += detail3.PayAmount;

                    MBTellerPaperDetailInsurance detail4 = mBTellerPaper.MBTellerPaperDetailInsurances.Where(x => x.DebitAccount == "3382").FirstOrDefault();
                    if (detail4 != null) cd += detail4.PayAmount;
                }

            }
            List<InsurancePayment> lst = new List<InsurancePayment>
            {
                new InsurancePayment
                {
                    Description = "BHXH và BH tai nạn lao động",
                    Amount = bhxh,
                    TotalAmount = bhxh,
                },
                new InsurancePayment
                {
                    Description = "Bảo hiểm y tế",
                    Amount = bhyt,
                    TotalAmount = bhyt,
                },
                new InsurancePayment
                {
                    Description = "Bảo hiểm thất nghiệp",
                    Amount = bhtn,
                    TotalAmount = bhtn,
                },
                new InsurancePayment
                {
                    Description = "Kinh phí công đoàn",
                    Amount = cd,
                    TotalAmount = cd,
                }
            };
            List<GOtherVoucher> gOtherVouchers = Query.Where(x => x.PostedDate <= date && x.TypeID == 840 && x.Recorded).ToList();

            if (gOtherVouchers == null || gOtherVouchers.Count == 0) return lst;
            foreach (GOtherVoucher GOtherVoucher in gOtherVouchers)
            {
                bhxh -= GOtherVoucher.GOtherVoucherDetails.Sum(x => x.CreditAccount == "3383" ? x.AmountOriginal : 0);
                bhyt -= GOtherVoucher.GOtherVoucherDetails.Sum(x => x.CreditAccount == "3384" ? x.AmountOriginal : 0);
                bhtn -= GOtherVoucher.GOtherVoucherDetails.Sum(x => x.CreditAccount == "3385" ? x.AmountOriginal : 0);
                cd -= GOtherVoucher.GOtherVoucherDetails.Sum(x => x.CreditAccount == "3382" ? x.AmountOriginal : 0);
            }

            lst = new List<InsurancePayment>
            {
                new InsurancePayment
                {
                    Description = "BHXH và BH tai nạn lao động",
                    Amount = -bhxh,
                    TotalAmount = -bhxh ,
                    DebitAccount = "3383"
                },
                new InsurancePayment
                {
                    Description = "Bảo hiểm y tế",
                    Amount = -bhyt,
                    TotalAmount = -bhyt,
                    DebitAccount = "3384"
                },
                new InsurancePayment
                {
                    Description = "Bảo hiểm thất nghiệp",
                    Amount = -bhtn,
                    TotalAmount = -bhtn,
                    DebitAccount = "3385"
                },
                new InsurancePayment
                {
                    Description = "Kinh phí công đoàn",
                    Amount = -cd,
                    TotalAmount = -cd,
                    DebitAccount = "3382"
                }
            };
            return lst;
        }

        public List<WagePayment> FindWagePayment(DateTime date)
        {
            List<Department> departments = _IDepartmentService.GetAll();
            List<MCPayment> mCPayments = _IMCPaymentService.Query.Where(x => x.PostedDate <= date && x.Recorded && x.TypeID == 111).ToList();
            List<MBTellerPaper> mBTellerPapers = _IMBTellerPaperService.Query.Where(x => x.PostedDate <= date && x.Recorded && x.TypeID == 121).ToList();

            List<WagePayment> finalList = new List<WagePayment>();
            if (mCPayments != null && mCPayments.Count > 0)
            {
                //var gr1 = mCPayments.GroupBy(x => x.AccountingObjectID, y => y, (x, y) => new { x, y });
                foreach (MCPayment mCPayment in mCPayments)
                {
                    finalList.AddRange(mCPayment.MCPaymentDetailSalarys.Select(x => new WagePayment { AccountingObjectID = x.EmployeeID, Amount = x.PayAmountOriginal ?? 0, Type = 1 }));
                }

            }

            if (mBTellerPapers != null && mBTellerPapers.Count > 0)
            {
                foreach (MBTellerPaper mBTellerPaper in mBTellerPapers)
                {
                    finalList.AddRange(mBTellerPaper.MBTellerPaperDetailSalarys.Select(x => new WagePayment { AccountingObjectID = x.EmployeeID, Amount = x.PayAmountOriginal, Type = 1 }));
                }
            }

            List<PSSalarySheet> PSSalarySheets = _IPSSalarySheetService.Query.Where(x => x.GOtherVoucherID != null && ((x.Year == date.Year && x.Month <= date.Month) || x.Year < date.Year)).ToList();
            if (PSSalarySheets != null && PSSalarySheets.Count > 0)
            {
                foreach (PSSalarySheet PSSalarySheet in PSSalarySheets)
                {
                    finalList.AddRange(PSSalarySheet.PSSalarySheetDetails.Select(x => new WagePayment { AccountingObjectID = x.EmployeeID, Amount = x.NetAmount, Type = 0 }));
                }

            }
            List<WagePayment> lst = new List<WagePayment>();
            List<AccountingObject> accs = _IAccountingObjectService.GetAll();
            if (finalList != null && finalList.Count > 0)
            {
                var grp = finalList.GroupBy(x => x.AccountingObjectID, y => y, (x, y) => new { AccountingObjectID = x, wages = y }).ToList();
                foreach (var WagePayment in grp)
                {
                    AccountingObject acc = accs.Where(x => x.ID == WagePayment.AccountingObjectID).FirstOrDefault();
                    AccountingObjectBankAccount accountingObjectBankAccount = acc.BankAccounts.FirstOrDefault(n => n.IsSelect);
                    string bankAcc = accountingObjectBankAccount == null ? null : accountingObjectBankAccount.BankAccount; //Hautv edit
                    string bankName = accountingObjectBankAccount == null ? null : accountingObjectBankAccount.BankName; //Hautv edit
                    if (acc == null) continue;
                    lst.Add(new WagePayment
                    {
                        AccountingObjectID = WagePayment.AccountingObjectID,
                        Amount = WagePayment.wages.Sum(x => x.Type == 0 ? x.Amount : -x.Amount),
                        TotalAmount = WagePayment.wages.Sum(x => x.Type == 0 ? x.Amount : -x.Amount),
                        AccountingObjectName = acc.AccountingObjectName,
                        AccountingObjectCode = acc.AccountingObjectCode,
                        BankName = bankName,
                        AccountNumber = bankAcc,
                        DepartmentID = acc.DepartmentID,
                        DepartmentCode = departments.FirstOrDefault(x => x.ID == acc.DepartmentID).DepartmentCode
                    });
                }
            }

            return lst;
        }

        public List<WagePaymentDetail> WagePaymentDetail(Guid id)
        {
            var data = _IMCPaymentService.Query
                .Join(_IMCPaymentDetailSalaryService.Query, x => x.ID, y => y.MCPaymentID, (x, y)
                        => new WagePaymentDetail { Date = x.Date, PostedDate = x.PostedDate, No = x.No, Description = y.Description, ID = (Guid)y.EmployeeID, Amount = y.PayAmountOriginal ?? 0, TotalAmount = y.AccumAmountOriginal ?? 0, RemainingAmount = y.CurrentMonthAmountOriginal ?? 0 })
                        .Where(x => x.ID == id).ToList();
            var data2 = _IMBTellerPaperService.Query
                .Join(_IMBTellerPaperDetailSalaryService.Query, x => x.ID, y => y.MBTellerPaperID, (x, y)
                        => new WagePaymentDetail { Date = x.Date, PostedDate = x.PostedDate, No = x.No, Description = y.Description, ID = y.EmployeeID ?? Guid.Empty, Amount = y.PayAmountOriginal, TotalAmount = y.AccumAmountOriginal, RemainingAmount = y.CurrentMonthAmountOriginal })
                        .Where(x => x.ID == id).ToList();

            data.AddRange(data2);
            return data.OrderBy(x=>x.Date).ToList();
        }
        #endregion
    }

}