using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153LostInvoiceDetailService: BaseService<TT153LostInvoiceDetail ,Guid>,ITT153LostInvoiceDetailService
    {
        public TT153LostInvoiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        ITT153ReportService _ITT153ReportService = IoC.Resolve<ITT153ReportService>();

        public List<ITT153LostInvoiceDetailService> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportID)
        {
            //return Query.Where(n=>From<n.)
            return null;
        }

        public List<TT153LostInvoiceDetail> GetAllBySAInvoiceID(Guid SAInvoiceID)
        {
            TT153Report tT153Report = _ITT153ReportService.GetbySAInvoiceID(SAInvoiceID);
            if (tT153Report != null)
                return Query.Where(n => n.TT153ReportID == tT153Report.ID).ToList();
            else
                return new List<TT153LostInvoiceDetail>();
        }

        public List<TT153LostInvoiceDetail> GetAllByTT153ReportID(Guid TT153ReportID)
        {
            return Query.Where(n => n.TT153ReportID == TT153ReportID).ToList();
        }
    }

}