﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CostSetMaterialGoodsService : BaseService<CostSetMaterialGoods, Guid>, ICostSetMaterialGoodsService
    {
        public CostSetMaterialGoodsService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {

        }

        public List<CostSetMaterialGoods> GetCostSetMaterialGoodsCostSetID(Guid? costSetID)
        {
            List<CostSetMaterialGoods> test1s = Query.Where(k => k.CostSetID == costSetID).ToList();
            return test1s;
        }
    }
}
