using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153PublishInvoiceService: BaseService<TT153PublishInvoice ,Guid>,ITT153PublishInvoiceService
    {
        public TT153PublishInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}