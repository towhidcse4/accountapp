﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FAAdjustmentDetailService : BaseService<FAAdjustmentDetail, Guid>, IFAAdjustmentDetailService
    {
        public FAAdjustmentDetailService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath) { }

        public List<FAAdjustmentDetail> GetByFAADjeusmenttId(Guid faAdjustmenID)
        {
            return Query.Where(k => k.FAAdjustmentID == faAdjustmenID).ToList();
        }
    }
}
