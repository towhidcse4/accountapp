using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM01GTGTAdjustService: BaseService<TM01GTGTAdjust ,Guid>,ITM01GTGTAdjustService
    {
        public TM01GTGTAdjustService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}