using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentDetailInsuranceService : BaseService<MCPaymentDetailInsurance, Guid>, IMCPaymentDetailInsuranceService
    {
        public MCPaymentDetailInsuranceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}