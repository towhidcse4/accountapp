﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class PPInvoiceDetailService: BaseService<PPInvoiceDetail ,Guid>,IPPInvoiceDetailService
    {
        private static IPPInvoiceService IPPInvoiceService
        {
            get { return IoC.Resolve<IPPInvoiceService>(); }
        }
        public PPInvoiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IList<PPInvoiceDetail> GetPPInvoiceDetailbyID(Guid id)
        {
            return Query.Where(k => k.PPInvoiceID == id).ToList();
        }

        /// <summary>
        /// Lấy danh sách PPInvoiceID từ PPInvoiceDetail 
        /// [Longtx]
        /// </summary>
        /// <param name="InvoiceId"></param>
        /// <returns></returns>
        public List<PPInvoiceDetail> GetListPPInvoiceDetailbyID(Guid InvoiceId)
        {
            return Query.Where(p => p.PPInvoiceID == InvoiceId).ToList();
        }

        public decimal GetPPInvoiceQuantity(Guid materialGoodsID, Guid contractID)
        {
            List<PPInvoiceDetail> lstPPInvoiceDetail = Query.Where(X => X.MaterialGoodsID == materialGoodsID && X.ContractID == contractID && IPPInvoiceService.Query.Any(d => d.ID == X.PPInvoiceID && d.Recorded)).ToList();
            decimal quantity = lstPPInvoiceDetail.Count == 0 ? 0 : (lstPPInvoiceDetail.Sum(x => x.Quantity) ?? 0);
            return (decimal)quantity;
        }
    }

}