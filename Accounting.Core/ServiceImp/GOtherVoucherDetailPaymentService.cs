﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class GOtherVoucherDetailPaymentService : BaseService<GOtherVoucherDetailPayment, Guid>, IGOtherVoucherDetailPaymentService
    {
        public GOtherVoucherDetailPaymentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}