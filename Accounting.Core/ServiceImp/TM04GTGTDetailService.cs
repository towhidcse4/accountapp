using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM04GTGTDetailService: BaseService<TM04GTGTDetail ,Guid>,ITM04GTGTDetailService
    {
        public TM04GTGTDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}