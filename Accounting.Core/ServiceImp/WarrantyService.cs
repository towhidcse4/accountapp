﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class WarrantyService : BaseService<Warranty, int>, IWarrantyService
    {
        public WarrantyService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy danh sách các gói bảo hành
        /// </summary>
        /// <returns></returns>
        public List<Warranty> getActive()
        {
            List<Warranty> list = Query.Where(s => s.IsActive == true).ToList();

            return list;
        }
    }

}