using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153AdjustAnnouncementDetailListInvoiceService: BaseService<TT153AdjustAnnouncementDetailListInvoice ,Guid>,ITT153AdjustAnnouncementDetailListInvoiceService
    {
        public TT153AdjustAnnouncementDetailListInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}