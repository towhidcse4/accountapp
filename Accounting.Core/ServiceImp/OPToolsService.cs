using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class OPToolsService : BaseService<OPTools, Guid>, IOPToolsService
    {
        public OPToolsService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public OPTools FindByMaterialGoodsID(Guid iD)
        {
            return Query.FirstOrDefault(o => o.ToolsID == iD);
        }

        public OPTools GetByInitID(Guid req)
        {
            return Query.FirstOrDefault(o => o.ToolsID == req);
        }
    }

}