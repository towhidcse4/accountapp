using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153RegisterInvoiceService: BaseService<TT153RegisterInvoice ,Guid>,ITT153RegisterInvoiceService
    {
        public TT153RegisterInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}