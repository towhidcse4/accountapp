﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TIAuditDetailService: BaseService<TIAuditDetail ,Guid>,ITIAuditDetailService
    {
        IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        ITIIncrementDetailService _ITIIncrementDetailService { get { return IoC.Resolve<ITIIncrementDetailService>(); } }
        ITIInitService _ITIInitService { get { return IoC.Resolve<ITIInitService>(); } }
        ITIInitDetailService _ITIInitDetailService { get { return IoC.Resolve<ITIInitDetailService>(); } }
        ITIDecrementDetailService _ITIDecrementDetailService { get { return IoC.Resolve<ITIDecrementDetailService>(); } }
        ITIIncrementService _ITIIncrementService { get { return IoC.Resolve<ITIIncrementService>(); } }
        ITIDecrementService _ITIDecrementService { get { return IoC.Resolve<ITIDecrementService>(); } }
        ITITransferService _ITITransferService { get { return IoC.Resolve<ITITransferService>(); } }
        ITITransferDetailService _ITITransferDetailService { get { return IoC.Resolve<ITITransferDetailService>(); } }

        public TIAuditDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public IList<TIAuditDetail> FindAllFaInitAndIncrementTools(DateTime inventoryDate)
        {
            List<TIAuditDetail> TIAuditDetails = new List<TIAuditDetail>();
            List<TIAuditDetail> result = new List<TIAuditDetail>();
            try
            {
                var ids = _ITIInitService.Query.Where(fix => 
                                                        ((_ITIIncrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID && _ITIIncrementService.Query.Any(fal => fal.ID == fde.TIIncrementID && fal.Recorded && fal.PostedDate <= inventoryDate)) || fix.DeclareType == 0)
                                                            || fix.DeclareType == 1)
                                                        && (_ITIDecrementDetailService.Query.Where(fde => fde.ToolsID == fix.ID && _ITIDecrementService.Query.Any(fal => fal.ID == fde.TIDecrementID && fal.Recorded && fal.PostedDate <= inventoryDate)).Sum(a => a.DecrementQuantity) < fix.Quantity
                                                        || !_ITIDecrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID))
                                                        ).Select(fix => fix.ID ).ToList();
                //var FAAuditDetails = ids.Select(x => x.ID).ToList(); ;
                //result = _ITIInitDetailService.Query.Where(fix => ids.Contains(fix.TIInitID ?? Guid.Empty));
                TIAuditDetails = _ITIInitDetailService.Query.Join(_ITIInitService.Query, a => a.TIInitID, b => b.ID, (a, b) => new TIAuditDetail
                {
                    ToolsID = b.ID, 
                    ToolsName = b.ToolsName,
                    Unit = b.Unit,
                    QuantityOnBook = a.Quantity ?? 0,
                    DepartmentID = a.ObjectID
                }).Where(c => ids.Contains(c.ToolsID ?? Guid.Empty)).ToList();

                for (int i = 0; i < TIAuditDetails.Count; i++)
                {
                    TIAuditDetail detail = TIAuditDetails[i];
                    //var tranfers = _ITITransferDetailService.Query.Where(a => detail.TIInitID == a.ToolsID).ToList();
                    // Lấy ra dữ liệu của dụng cụ đã chuyển phòng ban
                    // Lọc ra những phòng ban chưa có trong list resut và insert vào list resut 


                    var tranfers = _ITITransferDetailService.Query
                                      .Join(_ITITransferService.Query, a => a.TITransferID, b => b.ID, (a, b) => new { a.FromDepartmentID, a.ToDepartmentID, b.PostedDate, a.TransferQuantity, a.ToolsID })
                                      .Where(a => a.ToolsID == detail.ToolsID).OrderBy(a => a.PostedDate).ToList();
                    var other1 = tranfers.Where(c => !TIAuditDetails.Any(d => c.FromDepartmentID == d.DepartmentID)).Select(a => new TIAuditDetail { DepartmentID = a.FromDepartmentID, ToolsID = a.ToolsID, QuantityOnBook = 0, ToolsName = detail.ToolsName, Unit = detail.Unit }).ToList();
                    var other2 = tranfers.Where(c => !TIAuditDetails.Any(d => c.ToDepartmentID == d.DepartmentID)).Select(a => new TIAuditDetail { DepartmentID = a.ToDepartmentID, ToolsID = a.ToolsID, QuantityOnBook = 0, ToolsName = detail.ToolsName, Unit = detail.Unit }).ToList();
                    TIAuditDetails.AddRange(other1);
                    TIAuditDetails.AddRange(other2);
                    foreach (var tranfer in tranfers)
                    {
                        if (detail.DepartmentID == tranfer.FromDepartmentID)
                        {
                            detail.QuantityOnBook -= tranfer.TransferQuantity;
                        }
                        else if (detail.DepartmentID == tranfer.ToDepartmentID)
                        {
                            detail.QuantityOnBook += tranfer.TransferQuantity;
                        }
                    }
                    detail.QuantityOnBook = detail.QuantityInventory = _ITIDecrementDetailService.GetNotDecrementQuantity(detail.ToolsID ?? Guid.Empty, detail.QuantityOnBook, inventoryDate);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            return TIAuditDetails.Where(x => x.QuantityOnBook > 0).OrderBy(x => x.ToolsName).ToList();
        }
    }

}