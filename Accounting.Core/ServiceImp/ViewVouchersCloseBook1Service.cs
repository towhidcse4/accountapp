﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class ViewVouchersCloseBook1Service : BaseService<ViewVouchersCloseBook1, Guid>, IViewVouchersCloseBook1Service
    {
        public ViewVouchersCloseBook1Service(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<ViewVouchersCloseBook1> GetAllVoucherUnRecorded1(DateTime newDBDateClosed, Guid? branchId = null)
        {
            if (branchId.HasValue)
                return Query.Where(v => v.BranchID == branchId.Value && v.PostedDate <= newDBDateClosed && v.Recorded == false).ToList();
            return Query.Where(v => v.PostedDate <= newDBDateClosed && v.Recorded == false).ToList();
        }

        public List<ViewVouchersCloseBook1> GetAllVoucherAccountingObject1()
        {
            return Query.Where(o => o.AccountingObjectID != null || o.EmployeeID != null).OrderBy(p => p.No).ToList();
        }

        public ViewVouchersCloseBook1 GetByNo1(string No)
        {
            return Query.FirstOrDefault(o => o.No == No);
        }

        public bool CheckNoExist1(string No)
        {
            return Query.Any(o => o.No == No);
        }
    }
}
