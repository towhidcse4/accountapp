using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class SAInvoiceDetailService: BaseService<SAInvoiceDetail ,Guid>,ISAInvoiceDetailService
    {
        private static ISAInvoiceService ISAInvoiceService
        {
            get { return IoC.Resolve<ISAInvoiceService>(); }
        }
        public SAInvoiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public decimal GetSAInvoiceQuantity(Guid materialGoodsID, Guid contractID)
        {
            List<SAInvoiceDetail> lstSAInvoiceDetail = Query.Where(X => X.MaterialGoodsID == materialGoodsID && X.ContractID == contractID && ISAInvoiceService.Query.Any(d => d.ID == X.SAInvoiceID && d.Recorded)).ToList();
            decimal quantity = lstSAInvoiceDetail.Count == 0 ? 0 : (lstSAInvoiceDetail.Sum(x => x.Quantity) ?? 0);
            return (decimal)quantity;
        }
    }

}