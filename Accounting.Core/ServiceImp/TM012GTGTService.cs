using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM012GTGTService: BaseService<TM012GTGT ,Guid>,ITM012GTGTService
    {
        public TM012GTGTService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}