using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBCreditCardDetailTaxService: BaseService<MBCreditCardDetailTax ,Guid>,IMBCreditCardDetailTaxService
    {
        public MBCreditCardDetailTaxService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}