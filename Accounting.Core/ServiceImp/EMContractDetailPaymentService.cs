﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class EMContractDetailPaymentService : BaseService<EMContractDetailPayment, Guid>, IEMContractDetailPaymentService
    {
        IEMContractDetailPaymentService _EMContractDetailPaymentService
        {
            get { return IoC.Resolve<IEMContractDetailPaymentService>(); }
        }
        public EMContractDetailPaymentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<EMContractDetailPayment> getEMContractDetailPaymentbyIDEMContract (Guid id)
        {
            return Query.Where(p => p.EMContractID == id).ToList();
        }

        public EMContractDetailPayment GetByID(Guid id)
        {
            return Query.Where(x => x.ID == id).SingleOrDefault();
        }

        public Decimal TotalDetailPaymentByContractID(Guid contractId)
        {            
            var lst = Query.Where(x=>x.EMContractID == contractId).ToList();
            if (lst != null) return lst.Sum(c => c.AmountOriginal);
            else return 0;
        }
    }
}
