using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM032ATNDNService: BaseService<TM032ATNDN ,Guid>,ITM032ATNDNService
    {
        public TM032ATNDNService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}