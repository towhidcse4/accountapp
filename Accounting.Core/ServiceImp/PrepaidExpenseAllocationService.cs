using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class PrepaidExpenseAllocationService: BaseService<PrepaidExpenseAllocation ,Guid>,IPrepaidExpenseAllocationService
    {
        public PrepaidExpenseAllocationService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}