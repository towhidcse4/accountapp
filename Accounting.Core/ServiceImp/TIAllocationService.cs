﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TIAllocationService : BaseService<TIAllocation, Guid>, ITIAllocationService
    {
        private IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        private ITIAllocationDetailService _ITIAllocationDetailService { get { return IoC.Resolve<ITIAllocationDetailService>(); } }
        private ITIAllocationService _ITIAllocationService { get { return IoC.Resolve<ITIAllocationService>(); } }
        private ITIAdjustmentDetailService _ITIAdjustmentDetailService { get { return IoC.Resolve<ITIAdjustmentDetailService>(); } }
        private ITIAdjustmentService _ITIAdjustmentService { get { return IoC.Resolve<ITIAdjustmentService>(); } }
        private IToolLedgerService _IToolLedgerService { get { return IoC.Resolve<IToolLedgerService>(); } }
        private ITIInitService _ITIInitService { get { return IoC.Resolve<ITIInitService>(); } }


        public TIAllocationService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {

        }

        public List<TIAllocation> GetAll_OrderBy()
        {
            return Query.OrderByDescending(p => p.Date).ThenBy(p => p.No).ToList();
        }

        public TIAllocation GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        public decimal GetDepreciationByMonth(DateTime from, DateTime to, Guid MaterialGoodsID)
        {
            var a = Query.Where(
                x =>
                //x.Date >= from && x.PostedDate <= to &&
                x.TIAllocationDetails.Any(y => y.ToolsID == MaterialGoodsID)).ToList();
            return
                a.Sum(x =>
                            {
                                var TIAllocationDetail = x.TIAllocationDetails.FirstOrDefault(y => y.ToolsID == MaterialGoodsID);
                                return TIAllocationDetail != null ? TIAllocationDetail.Amount : 0;
                            });
        }

        public decimal GetRemainingAmount(Guid MaterialGoodsID, DateTime dp, decimal? unitPrice, decimal numberOfTools)
        {

            // tính số tiền còn lại bằng cách
            // - nếu chưa có điều chỉnh
            //   =>lấy đơn giá mỗi ccdc x số tiền - số lần khâu hao tính tới tháng hiện tại
            // - nếu đã có điều chỉnh
            //   => lấy số tiền điều chỉnh Giá trị còn lại mới - số lần khấu hao từ lúc điều chỉnh đến tháng hiện tại
            ToolLedger toolLedger = _IToolLedgerService.Query.Where(a => a.ToolsID == MaterialGoodsID && a.TypeID == 432 && a.PostedDate <= dp).OrderByDescending(a => a.PostedDate).FirstOrDefault();
            decimal newRemainingAmount = toolLedger != null ? toolLedger.RemainingAmount : unitPrice ?? 0;
            decimal up = unitPrice ?? _ITIInitService.Getbykey(MaterialGoodsID).UnitPrice;
            decimal totalAllocationAmount = toolLedger == null 
                                                ? _ITIAllocationDetailService.Query.Where(a => a.ToolsID == MaterialGoodsID && _ITIAllocationService.Query.Any(b => b.Recorded && b.PostedDate <= dp)).ToList().Sum(b => b.AllocationAmount)
                                                : _ITIAllocationDetailService.Query.Where(a => a.ToolsID == MaterialGoodsID && _ITIAllocationService.Query.Any(b => b.Recorded && b.PostedDate <= dp && b.PostedDate >= toolLedger.PostedDate)).ToList().Sum(b => b.AllocationAmount);
            return toolLedger == null ? (up * numberOfTools - totalAllocationAmount) : (newRemainingAmount - totalAllocationAmount);
        }

        public decimal GetRemaningAllocationTimes(Guid MaterialGoodsID, DateTime dp, decimal AllocationTime)
        {
            // Tính số kì phân bổ còn lại cách
            // - nếu chưa có điều chỉnh
            //   => lấy số kì phân bổ - số lần phân bổ
            // - Nếu đã có điều chỉnh
            //   => lấy số kì phân bổ còn lại mới ở điều chỉnh - số lần phần bổ từ lúc điều chỉnh đến tháng hiện tại
            ToolLedger toolLedger = _IToolLedgerService.Query.Where(a => a.ToolsID == MaterialGoodsID && a.TypeID == 432 && a.PostedDate <= dp).OrderByDescending(a => a.PostedDate).FirstOrDefault();
            decimal? newRemainingAmount = toolLedger != null ? (toolLedger.IncrementAllocationTime ?? 0) + (toolLedger.DecrementAllocationTime ?? 0) : AllocationTime;
            decimal totalAllocationTime = toolLedger == null
                                                ? _ITIAllocationDetailService.Query.Where(a => a.ToolsID == MaterialGoodsID && _ITIAllocationService.Query.Any(b => b.Recorded && b.PostedDate <= dp)).ToList().Sum(b => b.AllocationAmount)
                                                : _ITIAllocationDetailService.Query.Where(a => a.ToolsID == MaterialGoodsID && _ITIAllocationService.Query.Any(b => b.Recorded && b.PostedDate <= dp && b.PostedDate >= toolLedger.PostedDate)).ToList().Count();

            return AllocationTime - totalAllocationTime;
        }
    }
}
