﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class RSAssemblyDismantlementService : BaseService<RSAssemblyDismantlement, Guid>, IRSAssemblyDismantlementService
    {
        public RSAssemblyDismantlementService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public RSAssemblyDismantlement getRSAssemblyDismantlementbyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        /// <summary>
        /// Lấy danh sách Recorded từ RSAssemblyDismantlement
        /// [Longtx]
        /// </summary>
        /// <returns></returns>
        public List<bool> GetByRecorded()
        {
            return Query.Select(p => p.Recorded).ToList();
        }
        /// <summary>
        /// Lấy danh sách chứng từ lắp ráp tháo rỡ được sắp xếp theo ngày chứng từ
        /// </summary>
        /// <returns></returns>
        public List<RSAssemblyDismantlement> getAllOrderbyDate()
        {
            return Query.OrderBy(o => o.Date).ToList();
        }

        public List<RSAssemblyDismantlement> getByType(int TypeID, DateTime fromDate, DateTime toDate)
        {
            var query = Query.Where(o => o.TypeID == TypeID);

            if(fromDate != DateTime.MinValue)
            {
                query = query.Where( o => o.Date >= fromDate);
            }

            if(toDate != DateTime.MinValue)
            {
                DateTime nextDay = toDate.AddDays(1);
                query = query.Where(o => o.Date <= nextDay);
            }



            return query.OrderBy(o => o.Date).ToList();
        }
    }

}