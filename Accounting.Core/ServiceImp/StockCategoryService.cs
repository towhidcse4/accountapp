using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class StockCategoryService : BaseService<StockCategory, Guid>, IStockCategoryService
    {
        public StockCategoryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<string> GetCode()
        {
            return Query.Select(a => a.StockCategoryCode).ToList();
        }
        public List<StockCategory> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.StockCategoryCode).ToList();
        }
    }

}