using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class TemplateService : BaseService<Template, Guid>, ITemplateService
    {
        public TemplateService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.List<Template> getTemplateinTypeForm(int typeID)
        {
            return Query.Where(k => k.TypeID == typeID).ToList();
        }
        public Template getTemplateStandard(int typeID)
        {
            var qr = from t in Query
                     join d in IoC.Resolve<ITemplateDetailService>().Query on t.ID equals d.TemplateID
                     where t.IsDefault == false && t.TypeID == 170
                     select t;
            return qr.FirstOrDefault();

        }
    }

}