using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentDetailVendorService : BaseService<MCPaymentDetailVendor, Guid>, IMCPaymentDetailVendorService
    {
        public MCPaymentDetailVendorService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<MCPaymentDetailVendor> GetByMcPaymentId(Guid mCPaymentId)
        {
            return Query.Where(k => k.MCPaymentID == mCPaymentId).ToList();
        }
    }

}