using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153DestructionInvoiceDetailService: BaseService<TT153DestructionInvoiceDetail ,Guid>,ITT153DestructionInvoiceDetailService
    {
        public TT153DestructionInvoiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        ITT153ReportService _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
        public List<TT153DestructionInvoiceDetail> GetAllByTT153ReportID(Guid TT153ReportID)
        {
            return Query.Where(n => n.TT153ReportID == TT153ReportID).ToList();
        }

        public List<TT153DestructionInvoiceDetail> GetAllBySAInvoiceID(Guid SAInvoiceID)
        {
            TT153Report tT153Report = _ITT153ReportService.GetbySAInvoiceID(SAInvoiceID);
            if (tT153Report != null)
                return Query.Where(n => n.TT153ReportID == tT153Report.ID).ToList();
            else
                return new List<TT153DestructionInvoiceDetail>();
        }
    }

}