using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CPUncompleteDetailService: BaseService<CPUncompleteDetail ,Guid>,ICPUncompleteDetailService
    {
        public CPUncompleteDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<CPUncompleteDetail> GetList()
        {
            return Query.ToList();
        }
        
    }

}