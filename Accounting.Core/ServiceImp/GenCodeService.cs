﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System.Windows.Forms;

namespace Accounting.Core.ServiceImp
{
    public class GenCodeService : BaseService<GenCode, Guid>, IGenCodeService
    {
        public GenCodeService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public GenCode getGenCode(int TypeGroupID)
        {
            //UnbindSession(Query);
            var u = Query.Where(k => k.TypeGroupID == TypeGroupID).FirstOrDefault();
            UnbindSession(u);
            u = Query.Where(k => k.TypeGroupID == TypeGroupID).FirstOrDefault();
            return u;
        }


        public bool UpdateGenCodeForm(int TypeGroupID, string textNo, Guid VoucherID)
        {
            try
            {
                GenCode gencode = Query.Single(k => k.TypeGroupID == TypeGroupID);
                Dictionary<string, string> dicNo = Utils.CheckNo(textNo);
                gencode.Prefix = dicNo["Prefix"];
                decimal _decimal;
                decimal.TryParse(dicNo["Value"], out _decimal);
                if(gencode.CurrentValue <= _decimal)
                {
                    gencode.CurrentValue = _decimal + 1;
                }
                else
                {
                    IViewVoucherInvisibleService _iView = IoC.Resolve<IViewVoucherInvisibleService>();
                    ViewVoucherInvisible existNo = _iView.GetByNo(textNo);
                    if(existNo != null && VoucherID == existNo.ID)
                    {
                        MessageBox.Show(string.Format("Số chứng từ {0} đã tồn tại. Vui lòng nhập lại số chứng từ!", textNo), "Thông báo");
                        return false;
                    }
                    //if(!_iView.CheckNoExist(textNo))
                    //{
                    //    gencode.CurrentValue = gencode.CurrentValue;
                    //}
                    //else
                    //{
                    //    MessageBox.Show(string.Format("Số chứng từ {0} đã tồn tại. Vui lòng nhập lại số chứng từ!", textNo), "Thông báo");
                    //    return false;
                    //}
                    
                }
                if (gencode.CurrentValue != null)
                {
                    if (gencode.CurrentValue.ToString().Length != dicNo["Value"].Length&& dicNo["Value"].Length>=3)
                    {
                        gencode.CurrentValue = decimal.Parse(dicNo["Value"]) + 1;
                        gencode.Length = dicNo["Value"].Length;
                    }
                }
                gencode.Suffix = dicNo["Suffix"];
                Update(gencode);
                return true;
            }
            catch (Exception)
            {
                //MessageBox.Show("Cập nhật sinh mã tự động không thành công!", "Thông báo");
                return false;
            }
        }


        public bool UpdateGenCodeCatalog(int TypeGroupID, string textNo)
        {
            GenCode gencode = Query.Single(k => k.TypeGroupID == TypeGroupID);
            Dictionary<string, string> dicNo = Utils.CheckNo(textNo);
            if (dicNo != null)
            {
                gencode.Prefix = dicNo["Prefix"];
                decimal _decimal;
                decimal.TryParse(dicNo["Value"], out _decimal);
                gencode.CurrentValue = gencode.CurrentValue <= _decimal ? _decimal + 1 : gencode.CurrentValue;
                gencode.Suffix = dicNo["Suffix"];
                Update(gencode);
                return true;
            }
            return false;
        }
    }

}