using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class PPOrderDetailService: BaseService<PPOrderDetail ,Guid>,IPPOrderDetailService
    {
        IPPOrderService _IPPOrderService { get { return IoC.Resolve<IPPOrderService>(); } }
        public PPOrderDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public IList<PPOrderDetail> GetPPOrderDetailbyID(Guid id)
        {
            return Query.Where(k => k.PPOrderID == id).ToList();
        }

        public List<PPOrderDetail> GetPPOrderDetailbyPPOrderID(Guid id)
        {
            return Query.Where(k => k.PPOrderID == id).ToList();
        }

        public List<PPOrderDetail> GetPPOrderDetailbyContractID(Guid contractID)
        {
            return Query.Where(k => k.ContractID == contractID).ToList();
        }

        public List<PPOrderDetail> GetPPOrderDetailByPPOrderCode(Guid PPOrderID, Guid? ContractID)
        {
            return Query.Where(x => x.PPOrderID == PPOrderID && x.ContractID == ContractID).ToList();
        }
    }

}