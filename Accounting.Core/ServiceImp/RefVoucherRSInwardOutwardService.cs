﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class RefVoucherRSInwardOutwardService : BaseService<RefVoucherRSInwardOutward, Guid>, IRefVoucherRSInwardOutwardService
    {

        public RefVoucherRSInwardOutwardService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<RefVoucherRSInwardOutward> GetByRefID2(Guid refID2)
        {
            return Query.Where(o => o.RefID2 == refID2).ToList();
        }
    }
}