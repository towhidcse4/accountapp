﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCAuditDetailService : BaseService<MCAuditDetail, Guid>, IMCAuditDetailService
    {
        public MCAuditDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }
}