﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Confront;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class ExceptVoucherService : BaseService<ExceptVoucher, Guid>, IExceptVoucherService
    {
        private IAccountingObjectService _IAccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        public ExceptVoucherService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<ExceptVoucher> GetListExceptVoucher(Guid accountingObjectId, string debitAccount)
        {
           
            List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.ToList();
            List<ExceptVoucher> listDoiTru =
                Query.Where(
                    c =>
                        c.AccountingObjectID == accountingObjectId && c.DebitAccount == debitAccount &&
                        c.AccountingObjectID != null && c.DebitAccount != null).ToList();
            var ds = (from a in listDoiTru.ToList()
                      select new ExceptVoucher()
                {
                    ID = a.ID,
                    Date = a.Date,
                    No = a.No,
                    ExceptNo = a.ExceptNo,
                    EmployeeID =a.EmployeeID,
                    ExceptEmployeeID = a.ExceptEmployeeID,
                    Amount = a.Amount,
                    AmountOriginal = a.AmountOriginal,
                }).ToList();
            return ds.ToList();
        }
        public List<RemoveExpectVoucher> RemovExceptVouchers(Guid accountingObjectId, string debitAccount)
        {
             List<RemoveExpectVoucher> listDoiTru1 = new List<RemoveExpectVoucher>();
             List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.Where(c => c.IsEmployee != null && c.IsEmployee == true).ToList();
            listDoiTru1 =
                Query.Where(
                    c =>
                        c.AccountingObjectID == accountingObjectId && c.DebitAccount == debitAccount &&
                        c.AccountingObjectID != null && c.DebitAccount != null).ToList().Select(a=> new RemoveExpectVoucher()
                        {
                            ID = a.ID,
                            Date = a.Date,
                            No = a.No,
                            ExceptNo = a.ExceptNo,
                            EmployeeID = a.EmployeeID,
                            ExceptEmployeeID = a.ExceptEmployeeID,
                            Amount = a.Amount,
                            AmountOriginal = a.AmountOriginal,
                            //add by cuongpv
                            GLVoucherExceptID = a.GLVoucherExceptID,
                            GLVoucherID = a.GLVoucherID,
                            //end add by cuongpv
                        }).ToList();
            var ds = (from a in listDoiTru1.ToList()
                      select new RemoveExpectVoucher()
                      {
                          ID = a.ID,
                          Date = a.Date,
                          No = a.No,
                          ExceptNo = a.ExceptNo,
                          TenNhanVien =lstAccountingObjects.Count(c => c.ID == a.EmployeeID) == 1 ? lstAccountingObjects.SingleOrDefault(c => c.ID == a.EmployeeID).AccountingObjectCode : null,
                          ExceptEmployeeID = a.ExceptEmployeeID,
                          Amount = a.Amount,
                          AmountOriginal = a.AmountOriginal,
                          //add by cuongpv
                          GLVoucherExceptID = a.GLVoucherExceptID,
                          GLVoucherID = a.GLVoucherID,
                          //end add by cuongpv
                      }).ToList();
            return ds.ToList();
        }
    }

}