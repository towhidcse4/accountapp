using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class CPProductQuantumService : BaseService<CPProductQuantum, Guid>, ICPProductQuantumService
    {
        public CPProductQuantumService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public List<CPMaterialProductQuantum> GetAllByType()
        {
            List<CPMaterialProductQuantum> newlst = new List<CPMaterialProductQuantum>();
            List<CPProductQuantum> lst = Query.ToList();
            List<MaterialGoods> lstmt = _IMaterialGoodsService.GetAll().ToList().Where(x => x.MaterialGoodsType == 3 || x.MaterialGoodsType == 1).ToList();
            newlst = (from a in lstmt
                   select new CPMaterialProductQuantum()
                   {
                       MaterialGoodsID = a.ID,
                       MaterialGoodsCode = a.MaterialGoodsCode,
                       MaterialGoodsName = a.MaterialGoodsName,
                       Unit = a.Unit,
                       DirectMaterialAmount = 0,
                       DirectLaborAmount = 0,
                       GeneralExpensesAmount = 0,
                       TotalCostAmount = 0
                   }).ToList();
            foreach(var x in newlst)
            {
                var a = lst.FirstOrDefault(d=> d.ID == x.MaterialGoodsID);
                if(a != null)
                {
                    x.DirectMaterialAmount = a.DirectMaterialAmount;
                    x.DirectLaborAmount = a.DirectLaborAmount;
                    x.GeneralExpensesAmount = a.GeneralExpensesAmount;
                    x.TotalCostAmount = a.TotalCostAmount;
                }
            }
            return newlst;
        }
    }

}