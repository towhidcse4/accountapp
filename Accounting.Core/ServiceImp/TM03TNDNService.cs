using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM03TNDNService: BaseService<TM03TNDN ,Guid>,ITM03TNDNService
    {
        public TM03TNDNService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}