﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FADepreciationAllocationService : BaseService<FADepreciationAllocation, Guid>, IFADepreciationAllocationService
    {
        public FADepreciationAllocationService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
      
        }

        public List<FADepreciationAllocation> GetAll_OrderBy()
        {
            return Query.OrderByDescending(p => p.OrderPriority).ToList();
        }

       
    }
}
