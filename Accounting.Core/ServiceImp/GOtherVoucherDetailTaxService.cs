﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class GOtherVoucherDetailTaxService : BaseService<GOtherVoucherDetailTax, Guid>, IGOtherVoucherDetailTaxService
    {
        public GOtherVoucherDetailTaxService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}