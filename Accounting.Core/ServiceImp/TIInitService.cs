﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using NHibernate;

namespace Accounting.Core.ServiceImp
{
    public class TIInitService : BaseService<TIInit, Guid>, ITIInitService
    {
        ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        ITITransferDetailService _ITITransferDetailService { get { return IoC.Resolve<ITITransferDetailService>(); } }
        ITIDecrementDetailService _ITIDecrementDetailService { get { return IoC.Resolve<ITIDecrementDetailService>(); } }
        ITIAdjustmentDetailService _ITIAdjustmentDetailService { get { return IoC.Resolve<ITIAdjustmentDetailService>(); } }
        ITIIncrementDetailService _ITIIncrementDetailService { get { return IoC.Resolve<ITIIncrementDetailService>(); } }
        ITIInitService _ITIInitService { get { return IoC.Resolve<ITIInitService>(); } }
        IToolLedgerService _IToolLedgerService { get { return IoC.Resolve<IToolLedgerService>(); } }
        IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        ITIIncrementService _ITIIncrementService { get { return IoC.Resolve<ITIIncrementService>(); } }
        ITIDecrementService _ITIDecrementService { get { return IoC.Resolve<ITIDecrementService>(); } }
        ITIAllocationDetailService _ITIAllocationDetailService { get { return IoC.Resolve<ITIAllocationDetailService>(); } }
        ITIAuditDetailService _ITIAuditDetailService { get { return IoC.Resolve<ITIAuditDetailService>(); } }
        ITIInitDetailService _ITIInitDetailService { get { return IoC.Resolve<ITIInitDetailService>(); } }
        ITITransferService _ITITransferService { get { return IoC.Resolve<ITITransferService>(); } }
        IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }

        public TIInitService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public ISession GetSession()
        {
            return NHibernateSession;
        }

        public bool ExistsToolsCode(string req)
        {
            if (Query.Any(o => o.ToolsCode.ToLower().Equals(req.ToLower())))
                return true;
            return false;
        }
        public List<TIInit> GetToolsCategory()
        {
            return Query.Where(o => o.DeclareType == 0).ToList();
        }
        public List<TIInit> GetToolsInit()
        {
            return Query.Where(o => o.DeclareType == 1).ToList();
        }
        public Guid? GetGuidBytiinitCode(string ma)
        {
            Guid? id = null;
            TIInit dp = Query.Where(p => p.ToolsCode == ma).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ID;
            }
            return id;
        }
        public TIInit GettiinitByCode(string ma)
        {

            TIInit dp = Query.Where(p => p.ToolsCode == ma).SingleOrDefault();
            if (dp != null)
            {
                return dp;
            }
            return null;
        }
        public bool CheckDeleteTIInit(Guid iD)
        {
            if (_ITIIncrementDetailService.Query.Any(fad => fad.ToolsID == iD)) return true;
            if (_ITIDecrementDetailService.Query.Any(fad => fad.ToolsID == iD)) return true;
            if (_ITIAdjustmentDetailService.Query.Any(fad => fad.ToolsID == iD)) return true;
            if (_ITIAllocationDetailService.Query.Any(fad => fad.ToolsID == iD)) return true;
            if (_ITITransferDetailService.Query.Any(fad => fad.ToolsID == iD)) return true;
            if (_ITIAuditDetailService.Query.Any(fad => fad.ToolsID == iD)) return true;

            return false;
        }

        public List<Guid> FindByType(int? type, string id, DateTime postedDate2)
        {
            DateTime PostedDate = new DateTime(postedDate2.Year, postedDate2.Month, postedDate2.Day);
            List<Guid> result = new List<Guid>();
            if (type == 0)
            {
                // lây ra danh sách danh mục đã ghi tăng ghi sổ và chưa ghi giảm chưa ghi sổ
                // hoặc ghi giảm rồi mà chưa hết số lượng
                var pd = new DateTime(PostedDate.Year, PostedDate.Month, PostedDate.Day);
                result = Query.Where(fix => ((_ITIIncrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID && _ITIIncrementService.Query.Any(fal => fal.ID == fde.TIIncrementID && fal.Recorded && fal.PostedDate <= pd)) && fix.DeclareType == 0)
                                               || fix.DeclareType == 1)
                                                && (_ITIDecrementDetailService.Query.Where(fde => fde.ToolsID == fix.ID && _ITIDecrementService.Query.Any(fal => fal.ID == fde.TIDecrementID && fal.Recorded && fal.PostedDate <= pd)).Sum(a => a.DecrementQuantity) < fix.Quantity
                                                || !_ITIDecrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID))
                                                ).Select(fix => fix.ID).ToList();
                if (id != null)
                {
                    var includeFa = _ITIDecrementDetailService.Query.Where(fai => fai.ToolsID == new Guid(id)).Select(fai => fai.ID).ToList();
                    result.AddRange(includeFa);
                }
                return result.ToList();
            }
            else if (type == 1)
            {
                // lấy ra danh sách ccdc để ghi tăng
                // là những ccdc không phải là khai đầu kì,
                // hoặc là ccdc với tổng số lượng ghi tăng nhỏ hơn số lượng khai báo
                var materialGoods = Query.Where(fix => fix.DeclareType == 0
                                        && !_ITIIncrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID && _ITIIncrementService.Query.Any(tii => tii.Recorded && tii.ID == fde.TIIncrementID))
                                        ).Select(fix => fix.ID).ToList();
                if (id != null)
                {
                    var includeFa = _ITIIncrementDetailService.Query.Where(fai => fai.TIIncrementID == new Guid(id)).Select(fai => fai.ID).ToList();
                    materialGoods.AddRange(includeFa);
                }
                return materialGoods;
            }
            else if (type == 2)
            {
                var fixedAssets = Query.Where(fix => !_ITIDecrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID && _IToolLedgerService.Query.Any(fal => fal.ToolsID == fix.ID && fal.ReferenceID == fde.TIDecrementID))).Select(fix => fix.ID).ToList();
                if (id != null)
                {
                    var includeFa = _ITIDecrementDetailService.Query.Where(fai => fai.ToolsID == new Guid(id)).Select(fai => fai.ID).ToList();
                    fixedAssets.AddRange(includeFa);
                }
                return fixedAssets;
            }
            return result;
        }

        public TIInit FindByMaterialGoodsIdAndDepartmentId(Guid iD)
        {
            return Query.Where(a => a.MaterialGoodsID == iD).FirstOrDefault();
        }

        public List<TIInit> FindAllotionTools(DateTime postedDate2)
        {
            List<ToolLedger> tl = _IToolLedgerService.Query.Where(fal => fal.PostedDate <= postedDate2).ToList();
            List<TIIncrement> tiIncre = _ITIIncrementService.Query.Where(fal => fal.Recorded && fal.PostedDate <= postedDate2).ToList();
            List<TIInit> result = Query.Where(fix =>
                                        ((_ITIIncrementDetailService.Query.Any(fde => fde.ToolsID == fix.ID && _ITIIncrementService.Query.Any(fal => fal.ID == fde.TIIncrementID && fal.Recorded && fal.PostedDate <= postedDate2)) && fix.DeclareType == 0)
                                        || fix.DeclareType == 1) && !fix.IsActive
                                        ).Select(fix => new TIInit
                                        {
                                            ID = fix.ID,
                                            ToolsCode = fix.ToolsCode,
                                            ToolsName = fix.ToolsName,
                                            PostedDate = fix.PostedDate,
                                            Quantity = fix.Quantity,
                                            AllocatedAmount = fix.AllocatedAmount,
                                            AllocationTimes = fix.AllocationTimes,
                                            AllocationType = fix.AllocationType,
                                            AllocationAwaitAccount = fix.AllocationAwaitAccount,
                                            Amount = fix.Amount,
                                            AllocationAmount = fix.AllocationAmount
                                        }).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                if(result[i].PostedDate == null)
                    result[i].PostedDate = tiIncre.FirstOrDefault(d => d.TIIncrementDetails.Any(x => x.ToolsID == result[i].ID)).PostedDate;
                ToolLedger toolLedger = tl.Where(fal => fal.ToolsID == result[i].ID && fal.TypeID != 431).OrderByDescending(fal => fal.PostedDate).FirstOrDefault();
                ToolLedger dc = tl.Where(fal => fal.ToolsID == result[i].ID && fal.TypeID == 432).OrderByDescending(fal => fal.PostedDate).FirstOrDefault();
                if (toolLedger != null)
                {
                    if (toolLedger.RemainingAmount == 0 || toolLedger.RemainingAllocaitonTimes == 0)
                    {
                        result.Remove(result[i]);
                        i--;
                    }
                    else
                    {
                        result[i].AllocationTimes = Convert.ToInt32(toolLedger.RemainingAllocaitonTimes);
                        result[i].Quantity = toolLedger.RemainingQuantity;
                        result[i].RemainAmount = toolLedger.RemainingAmount;
                        result[i].AllocationAmount = toolLedger.AllocationAmount;
                        if (dc != null)
                        {
                            result[i].AllocatedAmount = toolLedger.AllocatedAmount;
                        }

                    }
                }
            }
            return result;
        }
        public DateTime GetDateIncrement(TIInit init)
        {
            //var incres = _ITIIncrementService.Query
            if (init.DeclareType == 0 && _ITIIncrementService.Query.Any(fal => _ITIIncrementDetailService.Query.Any(x => x.ToolsID == init.ID && x.TIIncrementID == fal.ID)))
                return _ITIIncrementService.Query.FirstOrDefault(fal => _ITIIncrementDetailService.Query.Any(x => x.ToolsID == init.ID && x.TIIncrementID == fal.ID)).PostedDate;
            else return _ITIInitService.Query.FirstOrDefault(fal => fal.ID == init.ID).PostedDate ?? DateTime.Now;
        }
        public List<TIInitDetail> FindAllocationAllocated(Guid id, DateTime PostedDate)
        {

            // Lấy ra dữ liệu từ khai báo hoặc đầu kì,
            List<TIInitDetail> fromInit2 = (from dtl in _ITIInitDetailService.Query
                                            join hdr in Query on dtl.TIInitID equals hdr.ID
                                            where hdr.ID == id
                                            select dtl).ToList();
            List<TIInitDetail> fromInit = Utils.CloneObject(fromInit2);
            // Lấy ra dữ liệu từ bảng điều chỉnh tương ứng với từng dụng cụ, cộng trừ số lượng theo phòng ban điều chỉnh
            var tf = _ITITransferDetailService.Query
                                  .Join(_ITITransferService.Query, a => a.TITransferID, b => b.ID, (a, b) => new { a.FromDepartmentID, a.ToDepartmentID, b.PostedDate, a.TransferQuantity, a.ToolsID, b.Recorded })
                                  .Where(a => a.Recorded).ToList();
            for (int i = 0; i < fromInit.Count; i++)
            {
                TIInitDetail detail = fromInit[i];
                if (detail.ObjectType != 1) continue;
                // Lấy ra dữ liệu của dụng cụ đã chuyển phòng ban
                // Lọc ra những phòng ban chưa có trong list resut và insert vào list resut 

                //var tranfers = _ITITransferDetailService.Query
                //                  .Join(_ITITransferService.Query, a => a.TITransferID, b => b.ID, (a, b) => new { a.FromDepartmentID, a.ToDepartmentID, b.PostedDate, a.TransferQuantity, a.ToolsID, b.Recorded })
                //                  .Where(a => a.ToolsID == detail.TIInitID && a.Recorded).OrderBy(a => a.PostedDate).ToList();
                var tranfers = tf.Where(x => x.ToolsID == detail.TIInitID).OrderBy(a => a.PostedDate).ToList();
                var other1 = tranfers.Where(c => !fromInit.Any(d => c.FromDepartmentID == d.ObjectID)).Select(a => new TIInitDetail { ObjectID = a.FromDepartmentID, TIInitID = a.ToolsID, Quantity = 0 }).ToList();
                var other2 = tranfers.Where(c => !fromInit.Any(d => c.ToDepartmentID == d.ObjectID)).Select(a => new TIInitDetail { ObjectID = a.ToDepartmentID, TIInitID = a.ToolsID, Quantity = 0 }).ToList();
                fromInit.AddRange(other1);
                fromInit.AddRange(other2);
                foreach (var tranfer in tranfers)
                {
                    if (detail.ObjectID == tranfer.FromDepartmentID)
                    {
                        detail.Quantity -= tranfer.TransferQuantity;
                    }
                    else if (detail.ObjectID == tranfer.ToDepartmentID)
                    {
                        detail.Quantity += tranfer.TransferQuantity;
                    }
                }

            }
            return fromInit.Where(a => a.Quantity > 0 || a.ObjectType != 1).ToList();
        }

        public bool CheckDeleteIncrement(List<Guid?> tIIncrementIDs)
        {
            if (_ITITransferDetailService.Query.Any(fad => tIIncrementIDs.Contains(fad.ToolsID))) return true;
            if (_ITIDecrementDetailService.Query.Any(fad => tIIncrementIDs.Contains(fad.ToolsID))) return true;
            if (_ITIAdjustmentDetailService.Query.Any(fad => tIIncrementIDs.Contains(fad.ToolsID))) return true;
            if (_ITIAllocationDetailService.Query.Any(fad => tIIncrementIDs.Contains(fad.ToolsID))) return true;
            if (_ITIAuditDetailService.Query.Any(fad => tIIncrementIDs.Contains(fad.ToolsID))) return true;
            return false;
        }

        public List<TIInit> FindByDeclareType(int type)
        {
            return Query.Where(a => a.DeclareType == type).OrderBy(x=>x.ToolsCode).ToList();
        }

        public decimal FindQuantityForAdjustment(Guid toolsID, Guid fromDepartmentID, DateTime dp)
        {
            // Lấy ra số lượng bạn đầu
            TIInitDetail detail = (from dtl in _ITIInitDetailService.Query
                                   join hdr in Query on dtl.TIInitID equals hdr.ID
                                   where hdr.ID == toolsID
                                   && dtl.ObjectID == fromDepartmentID
                                   select dtl).FirstOrDefault();
            // Công với số lượng điều chuyển đến / trừ số lượng điều chuyển đi
            var tranfers = _ITITransferDetailService.Query
                                .Join(_ITITransferService.Query, a => a.TITransferID, b => b.ID, (a, b) => new { a.FromDepartmentID, a.ToDepartmentID, b.PostedDate, a.TransferQuantity, a.ToolsID, b.Recorded })
                                .Where(a => a.ToolsID == toolsID && a.PostedDate <= dp && a.Recorded).ToList().Sum(a => a.FromDepartmentID == fromDepartmentID ? -a.TransferQuantity : (a.ToDepartmentID == fromDepartmentID ? a.TransferQuantity : 0));
            // trừ số lượng ghi giảm
            var decrement = _ITIDecrementDetailService.Query
                                .Join(_ITIDecrementService.Query, a => a.TIDecrementID, b => b.ID, (a, b) => new { a.DepartmentID, b.PostedDate, a.DecrementQuantity, a.ToolsID, b.Recorded })
                                .Where(a => a.ToolsID == toolsID && a.PostedDate <= dp && a.Recorded && a.DepartmentID == fromDepartmentID).ToList().Sum(a => a.DecrementQuantity);
            // Công số lượng ghi tăng khi kiểm kê
            var increment = _ITIIncrementDetailService.Query
                                .Join(_ITIIncrementService.Query, a => a.TIIncrementID, b => b.ID, (a, b) => new { a.DepartmentID, b.PostedDate, a.Quantity, a.ToolsID, b.Recorded, b.TypeID })
                                .Where(a => a.ToolsID == toolsID && a.PostedDate <= dp && a.Recorded && a.DepartmentID == fromDepartmentID && a.TypeID == 907).ToList().Sum(a => a.Quantity);

            return detail != null ? (detail.Quantity + tranfers - decrement + increment) ?? 0 : (tranfers - decrement + increment) ?? 0;
        }

        public List<Department> FindDepartment(Guid toolsID, DateTime dateTime)
        {
            //var data = _ITIInitDetailService.Query.Join(_IDepartmentService.Query, a => a.ObjectID, b => b.ID, (a, b) => new { department = b, tiinitdetail = a}).Where(x => x.tiinitdetail.TIInitID == toolsID).ToList();
            List<TIInitDetail> fromInit = FindAllocationAllocated(toolsID, dateTime);
            List<Guid?> guids = fromInit.Select(x => x.ObjectID).Distinct().ToList();
            //return _IDepartmentService.Query.Join(fromInit, a => a.ID, b => b.ObjectID, (a, b) => a).ToList();
            var a = (from init in guids
                     join de in _IDepartmentService.Query on init equals de.ID
                     select de).ToList();
            return a;
            //return _IDepartmentService.Query.Where(x => fromInit.Select(y => y.ObjectID).Any(z => (z ?? Guid.Empty) == x.ID)).ToList();



        }
    }
}
