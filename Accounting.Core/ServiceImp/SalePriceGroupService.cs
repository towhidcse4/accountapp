﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;
namespace Accounting.Core.ServiceImp
{
    public class SalePriceGroupService : BaseService<SalePriceGroup, Guid>, ISalePriceGroupService
    {
        public SalePriceGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy ra danh sách tất cả các SalePriceGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>Danh sách SalePriceGroupCode dạng chuỗi, null nếu bị lỗi</returns>
        public List<string> GetSalePriceGroupCode()
        {
            return Query.Select(Duy => Duy.SalePriceGroupCode).ToList();
        }

        public string GetCodeSalePriceGroupById(Guid Id)
        {
            SalePriceGroup salePriceGroup = Query.FirstOrDefault(p => p.ID == Id);
            if (salePriceGroup != null)
            {
                return salePriceGroup.SalePriceGroupCode;
            }

            return "";
        }

        public SalePriceGroup GetSalePriceGroupByCode(string code)
        {
            return Query.SingleOrDefault(p => p.SalePriceGroupCode == code);
        }
        public List<SalePriceGroup> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.SalePriceGroupCode).ToList();
        }
    }
}
