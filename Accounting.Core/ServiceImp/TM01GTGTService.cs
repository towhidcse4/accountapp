using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM01GTGTService: BaseService<TM01GTGT ,Guid>,ITM01GTGTService
    {
        public TM01GTGTService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}