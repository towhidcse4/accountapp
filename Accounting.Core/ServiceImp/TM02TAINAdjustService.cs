using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM02TAINAdjustService: BaseService<TM02TAINAdjust ,Guid>,ITM02TAINAdjustService
    {
        public TM02TAINAdjustService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}