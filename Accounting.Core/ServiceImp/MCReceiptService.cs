﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class MCReceiptService : BaseService<MCReceipt, Guid>, IMCReceiptService
    {
        public MCReceiptService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public MCReceipt GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
        /// <summary>
        /// Lấy ra danh sách các MCReceipt theo TypeID
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là TypeID truyền vào</param>
        /// <returns>tập các MCReceipt, null nếu bị lỗi</returns>
        public System.Collections.Generic.List<MCReceipt> GetAll_ByTypeID(int TypeID)
        {
            return Query.Where(k => k.TypeID == TypeID).ToList();
        }
        public MCReceipt findByAuditID(Guid ID)
        {
            return Query.FirstOrDefault(x => x.AuditID == ID);
        }

        public List<TIOriginalVoucher> FindByListID(string originalVoucher)
        {
            string[] ss = originalVoucher.Substring(0, originalVoucher.Length - 1).Split(';');
            List<Guid> guids = new List<Guid>();
            foreach (string s in ss)
            {
                guids.Add(Guid.Parse(s));
            }
            return Query.Where(haha => guids.Contains(haha.ID)).Select(hihi => new TIOriginalVoucher
            {
                PostedDate = hihi.PostedDate,
                Date = hihi.Date,
                No = hihi.No,
                Reason = hihi.Reason,
                ID = hihi.ID
            }).ToList();
        }
        public List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null)
        {
            List<MCReceipt> lst = new List<MCReceipt>();
            if (currencyID == null) lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                      && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            else lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                    && x.Recorded && x.CurrencyID == currencyID).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<TIOriginalVoucher> result = new List<TIOriginalVoucher>();
            foreach (MCReceipt io in lst)
            {
                TIOriginalVoucher ov = new TIOriginalVoucher()
                {
                    PostedDate = io.PostedDate,
                    Date = io.Date,
                    No = io.No,
                    Reason = io.Reason,
                    ID = io.ID

                };
                result.Add(ov);
            }
            return result;
        }
    }

}