﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class FAAuditDetailService : BaseService<FAAuditDetail, Guid>, IFAAuditDetailService
    {
        public FAAuditDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public IFAInitService _IFAInitService { get { return IoC.Resolve<IFAInitService>(); } }
        public IFixedAssetService _IFixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
        public IFAIncrementDetailService _IFAIncrementDetailService { get { return IoC.Resolve<IFAIncrementDetailService>(); } }
        public IFADecrementDetailService _IFADecrementDetailService { get { return IoC.Resolve<IFADecrementDetailService>(); } }
        public IFADepreciationDetailService _IFADepreciationDetailService { get { return IoC.Resolve<IFADepreciationDetailService>(); } }
        public IFixedAssetLedgerService _IFixedAssetLedgerService { get { return IoC.Resolve<IFixedAssetLedgerService>(); } }
        

        List<FAAuditDetail> IFAAuditDetailService.FindAllFaInitAndIncrementFixedAsset(DateTime postedDate, DateTime inventoryDate)
        {
            List<FAAuditDetail> result = new List<FAAuditDetail>();
            // Lấy ra tất cả tài sản trong bảng khai báo FAInit

            // Lấy ra tất cả tài sản trong bảng TSCĐ FAInit đã từng ghi tăng trong bảng Increment mà không qua khai báo
            // !_IFADecrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fde.FADecrementID))
            List<FAAuditDetail> FAAuditDetails = (from fa in _IFixedAssetService.Query
                               where fa.IncrementDate <= inventoryDate
                                    && !_IFADecrementDetailService.Query.Any(f => f.FixedAssetID == fa.ID  && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fa.ID && fal.ReferenceID == f.FADecrementID && fal.PostedDate <= postedDate))
                                    && (_IFAInitService.Query.Any(fai => fai.FixedAssetID == fa.ID)
                                    || _IFAIncrementDetailService.Query.Any(fai => fa.ID == fai.FixedAssetID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fa.ID && fal.ReferenceID == fai.FAIncrementID && fal.PostedDate <= postedDate)))
                               select new FAAuditDetail
                               {
                                   AcDepreciationAmount = fa.AcDepreciationAmount,
                                   DepartmentID = fa.DepartmentID,
                                   DepreciationAmount = fa.PurchasePrice,
                                   FixedAssetID = fa.ID,
                                   FixedAssetName = fa.FixedAssetName,
                                   OriginalPrice = fa.OriginalPrice,
                                   RemainingAmount = fa.RemainingAmount,
                                   ExistInStock = 1
                               }).ToList();
            foreach (FAAuditDetail FAAuditDetail in FAAuditDetails)
            {
                FixedAssetLedger fixedAssetLedger = _IFixedAssetLedgerService.Query.Where(fal => fal.FixedAssetID == FAAuditDetail.FixedAssetID && fal.PostedDate <= postedDate)
                    .OrderByDescending(fal => fal.PostedDate)
                    .FirstOrDefault();
                FAAuditDetail.AcDepreciationAmount = _IFixedAssetLedgerService.GetTotalAcDepreciationAmount(FAAuditDetail.FixedAssetID ?? Guid.Empty, postedDate);
                FAAuditDetail.DepartmentID = fixedAssetLedger.DepartmentID != null && fixedAssetLedger.DepartmentID != Guid.Empty ? fixedAssetLedger.DepartmentID : null;
                FAAuditDetail.DepreciationAmount = fixedAssetLedger.DepreciationAmount ?? 0;
                FAAuditDetail.OriginalPrice = fixedAssetLedger.OriginalPrice;
                FAAuditDetail.RemainingAmount = (FAAuditDetail.DepreciationAmount??0) - (FAAuditDetail.AcDepreciationAmount??0);
                
            }
            return FAAuditDetails;
        }
    }

}