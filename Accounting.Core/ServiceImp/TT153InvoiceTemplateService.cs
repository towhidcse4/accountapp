using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153InvoiceTemplateService: BaseService<TT153InvoiceTemplate ,Guid>,ITT153InvoiceTemplateService
    {
        public TT153InvoiceTemplateService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}