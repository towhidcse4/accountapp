﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class GoodsServicePurchaseService : BaseService<GoodsServicePurchase, Guid>, IGoodsServicePurchaseService
    {
        public GoodsServicePurchaseService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListGoodServicePurchaseCode()
        {
            return Query.Select(p => p.GoodsServicePurchaseCode).ToList();;
        }

        public List<GoodsServicePurchase> GetAll_OrderBy()
        {
            return Query.Where(p=>p.IsActive==true).OrderBy(o => o.GoodsServicePurchaseCode).ToList();
        }
    }
}
