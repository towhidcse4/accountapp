﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using System.Reflection;
using Accounting.Core.DAO;

namespace Accounting.Core.ServiceImp
{
    public class GeneralLedgerService : BaseService<GeneralLedger, Guid>, IGeneralLedgerService
    {
        public GeneralLedgerService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        #region Mở Services
        private IAccountingObjectService _IAccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        private IViewGLPayExceedCashService _IViewGLPayExceedCashService
        {
            get { return IoC.Resolve<IViewGLPayExceedCashService>(); }
        }
        private ITypeService _ITypeService
        {
            get { return IoC.Resolve<ITypeService>(); }
        }
        public IExceptVoucherService _IExceptVoucherService
        {
            get { return IoC.Resolve<IExceptVoucherService>(); }
        }
        public IMCReceiptService _IMCReceiptService
        {
            get { return IoC.Resolve<IMCReceiptService>(); }
        }
        public IMCPaymentService _IMCPaymentService
        {
            get { return IoC.Resolve<IMCPaymentService>(); }
        }
        public IMBDepositService _IMBDepositService
        {
            get { return IoC.Resolve<IMBDepositService>(); }
        }
        public ITIInitService _TIInitService
        {
            get { return IoC.Resolve<ITIInitService>(); }
        }

        public IFixedAssetLedgerService _IFixedAssetLedgerService
        {
            get { return IoC.Resolve<IFixedAssetLedgerService>(); }
        }
        public IFixedAssetService _IFixedAssetService
        {
            get { return IoC.Resolve<IFixedAssetService>(); }
        }

        public IMBCreditCardService _IMBCreditCardService
        {
            get { return IoC.Resolve<IMBCreditCardService>(); }
        }
        public ISystemOptionService _ISystemOptionService
        {
            get { return IoC.Resolve<ISystemOptionService>(); }
        }
        public IMBTellerPaperService _IMBTellerPaperService
        {
            get { return IoC.Resolve<IMBTellerPaperService>(); }
        }

        public IRepositoryLedgerService _IRepositoryLedgerService
        {
            get { return IoC.Resolve<IRepositoryLedgerService>(); }
        }
        public IToolLedgerService _IToolLedgerService
        {
            get { return IoC.Resolve<IToolLedgerService>(); }
        }
        private IBankAccountDetailService _IBankAccountDetailService { get { return IoC.Resolve<IBankAccountDetailService>(); } }
        private IMBInternalTransferDetailService _IMBInternalTransferDetailService { get { return IoC.Resolve<IMBInternalTransferDetailService>(); } }
        private IGOtherVoucherService _IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        private IGOtherVoucherDetailService _IGOtherVoucherDetailService { get { return IoC.Resolve<IGOtherVoucherDetailService>(); } }
        private IGOtherVoucherDetailForeignCurrencyService _IGOtherVoucherDetailForeignCurrencyService { get { return IoC.Resolve<IGOtherVoucherDetailForeignCurrencyService>(); } }
        private IGOtherVoucherDetailDebtPaymentService _IGOtherVoucherDetailDebtPaymentService { get { return IoC.Resolve<IGOtherVoucherDetailDebtPaymentService>(); } }
        private IAccountService _IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        private IGVoucherListDetailService _IGVoucherListDetailService { get { return IoC.Resolve<IGVoucherListDetailService>(); } }
        private ITIInitDetailService _ITIInitDetailService { get { return IoC.Resolve<ITIInitDetailService>(); } }
        public IRSAssemblyDismantlementService _IRSAssemblyDismantlementService { get { return IoC.Resolve<IRSAssemblyDismantlementService>(); } }
        public IEMContractService _IEMContractService { get { return IoC.Resolve<IEMContractService>(); } }
        public IGenCodeService IGenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
        private ICurrencyService _ICurrencyService
        {
            get { return IoC.Resolve<ICurrencyService>(); }
        }

        private IDepartmentService _IDepartmentService
        {
            get { return IoC.Resolve<IDepartmentService>(); }
        }

        private IExpenseItemService _IExpenseItemService
        {
            get { return IoC.Resolve<IExpenseItemService>(); }
        }
        private IMaterialGoodsService _IMaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        private ITIInitService _ITIInitService
        {
            get { return IoC.Resolve<ITIInitService>(); }
        }
        private ISAInvoiceService _ISAInvoiceService { get { return IoC.Resolve<ISAInvoiceService>(); } }
        private ISAInvoiceDetailService _ISAInvoiceDetailService { get { return IoC.Resolve<ISAInvoiceDetailService>(); } }//add by cuongpv 20190422
        private ISAReturnService _ISAReturnService { get { return IoC.Resolve<ISAReturnService>(); } }
        private ISAReturnDetailService _ISAReturnDetailService { get { return IoC.Resolve<ISAReturnDetailService>(); } }//add by cuongpv 20190422
        private ICostSetService _ICostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        private IEMContractDetailMGService _IEMContractDetailMGService
        {
            get { return IoC.Resolve<IEMContractDetailMGService>(); }
        }
        private IPPInvoiceDetailService IPPInvoiceDetailService { get { return IoC.Resolve<IPPInvoiceDetailService>(); } }
        private IPPServiceDetailService IPPServiceDetailService { get { return IoC.Resolve<IPPServiceDetailService>(); } }
        private IFAIncrementDetailService IFAIncrementDetailService { get { return IoC.Resolve<IFAIncrementDetailService>(); } }
        private ITIIncrementDetailService ITIIncrementDetailService { get { return IoC.Resolve<ITIIncrementDetailService>(); } }
        private ISAReturnDetailService ISAReturnDetailService { get { return IoC.Resolve<ISAReturnDetailService>(); } }
        private IGOtherVoucherDetailService IGOtherVoucherDetailService { get { return IoC.Resolve<IGOtherVoucherDetailService>(); } }
        private IMCPaymentDetailService IMCPaymentDetailService { get { return IoC.Resolve<IMCPaymentDetailService>(); } }
        private IMCPaymentDetailVendorService IMCPaymentDetailVendorService { get { return IoC.Resolve<IMCPaymentDetailVendorService>(); } }
        private IMBInternalTransferDetailService IMBInternalTransferDetailService { get { return IoC.Resolve<IMBInternalTransferDetailService>(); } }
        private IMCReceiptDetailService IMCReceiptDetailService { get { return IoC.Resolve<IMCReceiptDetailService>(); } }
        private IMCReceiptDetailCustomerService IMCReceiptDetailCustomerService { get { return IoC.Resolve<IMCReceiptDetailCustomerService>(); } }
        private IMBTellerPaperDetailService IMBTellerPaperDetailService { get { return IoC.Resolve<IMBTellerPaperDetailService>(); } }
        private IMBTellerPaperDetailVendorService IMBTellerPaperDetailVendorService { get { return IoC.Resolve<IMBTellerPaperDetailVendorService>(); } }
        private IMBCreditCardDetailService IMBCreditCardDetailService { get { return IoC.Resolve<IMBCreditCardDetailService>(); } }
        private IMBCreditCardDetailVendorService IMBCreditCardDetailVendorService { get { return IoC.Resolve<IMBCreditCardDetailVendorService>(); } }
        private IMBDepositDetailCustomerService IMBDepositDetailCustomerService { get { return IoC.Resolve<IMBDepositDetailCustomerService>(); } }
        #endregion
        public List<GeneralLedger> GetListByReferenceID(Guid referenceID)
        {
            return Query.Where(p => p.ReferenceID == referenceID).ToList();
        }

        /// <summary>
        /// Lấy danh sách ReferenceId ở bảng GeneralLedger
        /// [Longtx]
        /// </summary>
        /// <param name="Id">Tập các ID</param>
        /// <returns></returns>
        /// 
        public List<GeneralLedger> GetByReferenceID(Guid Id)
        {
            return Query.Where(p => p.ReferenceID == Id).ToList();
        }
        public List<GeneralLedger> GetByDetailID(Guid Id)
        {
            return Query.Where(p => p.DetailID == Id).ToList();
        }
        /// <summary>
        /// Lấy thông tin bảng GeneralLedger theo 1 tập ID của AccountingObject
        /// [PhongNT]
        /// </summary>
        /// <param name="Id">Tập các ID</param>
        /// 29/04/2014
        /// <returns></returns>
        public List<GeneralLedger> GetGLByListID(List<string> lstAcctNo)
        {
            List<GeneralLedger> lstAllGL = GetAllGL();
            return lstAllGL.Where(gl => lstAcctNo.Any(a => gl.Account.ToUpper().Trim() == a.ToUpper().Trim())).ToList();
        }

        public List<GeneralLedger> GetAllGL()
        {
            return Query.Where(g => g.ID != null).ToList();
        }
        public List<ReportPPInvoiceDebit> ReportsPpInvoice(DateTime date, DateTime toDate, string currencyID,
            string account, List<Guid> accountingObjectID)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.ToList();
            return (from g in lstGeneralLedgers
                    where g.PostedDate >= date && g.PostedDate <= toDate
                          && g.Account != null && g.Account.StartsWith(account) && g.CurrencyID != null &&
                          g.CurrencyID == currencyID
                          && g.AccountingObjectID != null &&
                          accountingObjectID.ToString().Contains(g.AccountingObjectID.ToString())
                    group g by new { g.AccountingObjectID }
                        into t
                    select new ReportPPInvoiceDebit()
                    {
                        AccountingObjectCode =
                            _IAccountingObjectService.Getbykey((Guid)t.Key.AccountingObjectID).AccountingObjectCode,
                        AccountingObjectName =
                            _IAccountingObjectService.Getbykey((Guid)t.Key.AccountingObjectID).AccountingObjectName,
                        DebitAmountFirst =
                            (t.Where(c => c.TypeID.ToString().StartsWith("70"))
                                .Select(c => c.DebitAmount)
                                .FirstOrDefault()),
                        CreditAmountFirst =
                            (t.Where(c => c.TypeID.ToString().StartsWith("70"))
                                .Select(c => c.CreditAmount)
                                .FirstOrDefault()),
                        DebitAmountArise =
                            (t.Where(c => c.PostedDate >= date && c.PostedDate <= toDate)
                                .Select(c => c.DebitAmount)
                                .ToList()).Sum(),
                        CreditAmountArise =
                            (t.Where(c => c.PostedDate >= date && c.PostedDate <= toDate)
                                .Select(c => c.CreditAmount)
                                .ToList()).Sum(),
                        DebitAmountAccum =
                            (t.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + date.Year) && c.PostedDate <= toDate)
                                .Select(c => c.DebitAmount)).Sum(),
                        CreditAmountAccum =
                            (t.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + date.Year) && c.PostedDate <= toDate)
                                .Select(c => c.CreditAmount)).Sum(),
                    }).ToList();
        }

        #region From đối trừ chứng từ Dũng edit: Chượng
        #region Form đối trừ mu hàng
        /// <summary>
        /// [DungNA]
        /// Form FPPExceptVoucher
        /// Danh sách chứng từ còn nợ
        /// </summary>
        /// <param name="accountingObjectId"> ID Nhà cung cấp</param>
        /// <param name="accountNumber"> Số tài khoản</param>
        /// <param name="currencyId"> ID Loại tiền</param>
        /// <returns></returns>
        public Except GetVouchers(Guid accountingObjectId, string accountNumber, string currencyId)
        {
            var Except = new Except();
            int lamtrondg = 0;
            int lamtrondg1 = 0;//add by cuongpv
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            var ddSoTyGia1 = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");//add by cuongpv
            if (ddSoTyGia1 != null) lamtrondg1 = int.Parse(ddSoTyGia1.Data);//add by cuongpv
            List<GOtherVoucherDetailForeignCurrency> lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(x => _IGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
            List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.Where(c => c.IsEmployee != null && c.IsEmployee == true).ToList();
            List<Accounting.Core.Domain.Type> lsTypes = _ITypeService.Query.ToList();
            List<GeneralLedger> lstGeneralLegerCre = Query.Where(c => c.ReferenceID != Guid.Empty && c.ReferenceID != null && c.AccountingObjectID != Guid.Empty && c.AccountingObjectID != null && c.Account != null
            && c.CurrencyID != null && c.DebitAmount > 0 && c.AccountingObjectID == accountingObjectId && c.Account == accountNumber && c.CurrencyID == currencyId
            && (new int[] { 110, 111, 112, 113, 114, 115, 116, 117, 119, 120, 121, 122, 123, 124, 125, 126, 127, 129, 130, 131, 132, 133, 140, 141, 142, 143, 170, 171, 172, 173, 220, 250, 701, 600 }.Contains(c.TypeID))).ToList();
            List<MBTellerPaperDetailVendor> lstMbTellerPaper = _IMBTellerPaperService.Query.SelectMany(c => c.MBTellerPaperDetailVendors).ToList();
            List<MBCreditCardDetailVendor> lstMbCreditCard = _IMBCreditCardService.Query.SelectMany(c => c.MBCreditCardDetailVendors).ToList();
            List<MCPaymentDetailVendor> lstMcPayment = _IMCPaymentService.Query.SelectMany(c => c.MCPaymentDetailVendors).ToList();
            List<ExceptVoucher> lstExceptVouchers = _IExceptVoucherService.Query.ToList();
            //ListCredit
            var lstCredit = (from c in lstGeneralLegerCre.ToList()
                             group c by new { c.AccountingObjectID, c.No, c.TypeID, c.InvoiceNo, c.Account, c.CurrencyID, c.Date, c.ReferenceID, c.EmployeeID, c.ExchangeRate } into a
                             let guid = a.Key
                             where guid != null
                             select new ConfrontVouchers()
                             {
                                 NgayChungTu = a.Key.Date,
                                 LoaiChungTu = lsTypes.SingleOrDefault(c => c.ID == a.Key.TypeID).TypeName,
                                 SoChungTu = a.Key.No ?? "",
                                 SoHoaDon = a.Key.InvoiceNo ?? "",
                                 NhanVien = a.Key.EmployeeID != null ? lstAccountingObjects.FirstOrDefault(c => c.ID == a.Key.EmployeeID).AccountingObjectCode : "",
                                 TriGia = a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                 a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)),//Tiền việt nam
                                 TriGiaQd = a.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),//Tiền ngoại tệ
                                 SoConLai = (a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                 (lstExceptVouchers.Where(c => c.GLVoucherExceptID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))) -
                                 lstMbCreditCard.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                 lstMbTellerPaper.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                 lstMcPayment.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal ?? 0, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))),//Chưa thanh toán tiền việt nam
                                 SoConLaiQd = (a.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                 (lstExceptVouchers.Where(c => c.GLVoucherExceptID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))) -
                                 lstMbCreditCard.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                 lstMbTellerPaper.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                 lstMcPayment.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount ?? 0, lamtrondg, MidpointRounding.AwayFromZero))),// * a.Key.ExchangeRate ?? 1 Tiền ngoại tệ chưa thanh toán
                                 GlVoucherExceptId = a.Key.ReferenceID,
                                 CurrencyID = a.Key.CurrencyID,
                                 RefVoucherExchangeRate = a.Key.ExchangeRate,
                                 LastExchangeRate = lstgo.Any(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)) ?
                                lstgo.Where(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)).OrderByDescending(d => d.DateReevaluate)
                                .FirstOrDefault().VoucherDetailForeignCurrencys.FirstOrDefault(x => x.ReferenceID == a.Key.ReferenceID).NewExchangeRate : 0,
                                 SoDoiTruQD = 0,
                                 SoDoiTru = 0,
                                 ChuaThanhToanQd = 0,
                                 ChuaThanhToan = 0,
                                 TyGia = 0,
                             }).ToList();
            //comment by cuongpv
            //foreach (var x in lstCredit)
            //{
            //    x.SoConLaiQd = x.LastExchangeRate > 0 ? x.SoConLai * x.LastExchangeRate : x.SoConLai * x.RefVoucherExchangeRate;
            //}
            //end comment by cuongpv
            Except.Credits = lstCredit.Where(x => x.SoConLai != 0 && x.SoConLaiQd != 0 && x.TriGia >= 0).ToList();
            //ListDebit
            List<GeneralLedger> lstGeneralLegerDebit = Query.Where(c => c.ReferenceID != Guid.Empty && c.ReferenceID != null && c.AccountingObjectID != Guid.Empty &&
            c.AccountingObjectID != null && c.Account != null && c.CurrencyID != null && c.CreditAmount > 0 && c.AccountingObjectID == accountingObjectId && c.Account == accountNumber &&
            c.CurrencyID == currencyId && (new int[] { 210, 240, 701, 600, 430, 500 }.Contains(c.TypeID))).ToList();
            var lstDebit = (from c in lstGeneralLegerDebit.ToList()
                            group c by new { c.AccountingObjectID, c.No, c.TypeID, c.InvoiceNo, c.Account, c.CurrencyID, c.Date, c.ReferenceID, c.EmployeeID, c.ExchangeRate } into a
                            let guid = a.Key
                            where guid != null
                            select new ConfrontVouchers()
                            {
                                NgayChungTu = a.Key.Date,
                                LoaiChungTu = lsTypes.SingleOrDefault(c => c.ID == a.Key.TypeID).TypeName,
                                SoChungTu = a.Key.No ?? "",
                                SoHoaDon = a.Key.InvoiceNo ?? "",
                                NhanVien = a.Key.EmployeeID != null ? lstAccountingObjects.FirstOrDefault(c => c.ID == a.Key.EmployeeID).AccountingObjectCode : "",
                                TriGia = a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)),//Tiền việt nam
                                TriGiaQd = a.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)),//Tiền ngoại tệ
                                ChuaThanhToan = (a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                (lstExceptVouchers.Where(c => c.GLVoucherID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))) -
                                lstMbCreditCard.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                lstMbTellerPaper.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                lstMcPayment.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal ?? 0, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))),//Chưa thanh toán tiền việt nam
                                ChuaThanhToanQd = (a.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                (lstExceptVouchers.Where(c => c.GLVoucherID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))) -
                                lstMbCreditCard.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                lstMbTellerPaper.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                lstMcPayment.Where(b => b.PPInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount ?? 0, lamtrondg, MidpointRounding.AwayFromZero))),//* a.Key.ExchangeRate ?? 1 Tiền ngoại tệ chưa thanh toán
                                GlVoucherId = a.Key.ReferenceID,
                                CurrencyID = a.Key.CurrencyID,
                                RefVoucherExchangeRate = a.Key.ExchangeRate,
                                LastExchangeRate = lstgo.Any(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)) ?
                               lstgo.Where(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)).OrderByDescending(d => d.DateReevaluate)
                               .FirstOrDefault().VoucherDetailForeignCurrencys.FirstOrDefault(x => x.ReferenceID == a.Key.ReferenceID).NewExchangeRate : 0,
                                SoDoiTruQD = 0,
                                SoDoiTru = 0,
                                SoConLai = 0,
                                SoConLaiQd = 0,
                                TyGia = 0,
                            }).ToList();
            //comment by cuongpv
            //foreach (var x in lstDebit)
            //{
            //    x.ChuaThanhToanQd = x.LastExchangeRate > 0 ? (x.ChuaThanhToan * x.LastExchangeRate) : (x.ChuaThanhToan * x.RefVoucherExchangeRate);
            //}
            //end comment by cuonpgv
            Except.Debits = lstDebit.Where(x => x.ChuaThanhToan != 0 && x.ChuaThanhToanQd != 0 && x.TriGia >= 0).ToList();
            return Except;
        }
        #endregion
        #region Form đối trừ bán hàng
        /// <summary>
        /// [DungNA]
        /// Form FSAExceptVoucher
        /// Danh sách chứng từ còn nợ
        /// </summary>
        /// <param name="accountingObjectId"> ID Nhà cung cấp</param>
        /// <param name="accountNumber"> Số tài khoản</param>
        /// <param name="currencyId"> ID Loại tiền</param>
        /// <returns></returns>
        public Except GetVouchersSell(Guid accountingObjectId, string accountNumber, string currencyId)
        {
            var Except = new Except();
            int lamtrondg = 0;
            int lamtrondg1 = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            var ddSoTyGia1 = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
            if (ddSoTyGia1 != null) lamtrondg1 = int.Parse(ddSoTyGia1.Data);
            List<GOtherVoucherDetailForeignCurrency> lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(x => _IGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
            List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.Where(c => c.IsEmployee != null && c.IsEmployee == true).ToList();
            List<Accounting.Core.Domain.Type> lsTypes = _ITypeService.Query.ToList();
            List<GeneralLedger> lstGeneralLegerCredit = Query.Where(c => c.ReferenceID != Guid.Empty && c.ReferenceID != null && c.AccountingObjectID != Guid.Empty && c.AccountingObjectID != null && c.Account != null
            && c.CurrencyID != null && c.AccountingObjectID == accountingObjectId && c.Account == accountNumber && c.CurrencyID == currencyId && (new int[] { 100, 102, 103, 160, 162, 163, 360, 701, 330, 600 }.Contains(c.TypeID))).ToList();
            List<MBDepositDetailCustomer> lstDepositDetailCustomers = _IMBDepositService.Query.Where(x => x.AccountingObjectID == accountingObjectId && x.CurrencyID == currencyId).SelectMany(c => c.MBDepositDetailCustomers).ToList();
            List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = _IMCReceiptService.Query.Where(x => x.AccountingObjectID == accountingObjectId && x.CurrencyID == currencyId).SelectMany(c => c.MCReceiptDetailCustomers).ToList();
            List<ExceptVoucher> lstExceptVouchers = _IExceptVoucherService.Query.ToList();
            //List Credit
            List<ConfrontVouchers> lstCredit = (from c in lstGeneralLegerCredit.ToList()
                                                group c by new { c.AccountingObjectID, c.No, c.TypeID, c.InvoiceNo, c.Account, c.CurrencyID, c.Date, c.ReferenceID, c.EmployeeID, c.ExchangeRate } into a
                                                let guid = a.Key
                                                where guid != null
                                                select new ConfrontVouchers()
                                                {
                                                    NgayChungTu = a.Key.Date,
                                                    LoaiChungTu = lsTypes.SingleOrDefault(c => c.ID == a.Key.TypeID).TypeName,
                                                    SoChungTu = a.Key.No ?? "",
                                                    SoHoaDon = a.Key.InvoiceNo ?? "",
                                                    NhanVien = a.Key.EmployeeID != null ? lstAccountingObjects.FirstOrDefault(c => c.ID == a.Key.EmployeeID).AccountingObjectCode : "",
                                                    TriGia = a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)),//Tiền việt nam
                                                    TriGiaQd = a.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)),//Tiền ngoại tệ
                                                    SoConLai = (a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                                    (lstExceptVouchers.Where(c => c.GLVoucherExceptID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) -
                                                    lstDepositDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                                    lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))),//Chưa thanh toán tiền việt nam
                                                    SoConLaiQd = (a.Sum(t => Math.Round(t.CreditAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                                    (lstExceptVouchers.Where(c => c.GLVoucherExceptID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) -
                                                    lstDepositDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                    lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) * a.Key.ExchangeRate ?? 1,
                                                    GlVoucherExceptId = a.Key.ReferenceID,
                                                    CurrencyID = a.Key.CurrencyID,
                                                    RefVoucherExchangeRate = a.Key.ExchangeRate,
                                                    LastExchangeRate = lstgo.Any(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)) ?
                             lstgo.Where(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)).OrderByDescending(d => d.DateReevaluate)
                             .FirstOrDefault().VoucherDetailForeignCurrencys.FirstOrDefault(x => x.ReferenceID == a.Key.ReferenceID).NewExchangeRate : 0,
                                                    SoDoiTruQD = 0,
                                                    SoDoiTru = 0,
                                                    TyGia = 0,
                                                    ChuaThanhToan = 0,
                                                    ChuaThanhToanQd = 0,
                                                }).ToList();
            foreach (var x in lstCredit)
            {
                x.SoConLaiQd = x.LastExchangeRate > 0 ? (x.SoConLai * x.LastExchangeRate) : (x.SoConLai * x.RefVoucherExchangeRate);
            }
            Except.Credits = lstCredit.Where(x => x.SoConLai != 0 && x.SoConLaiQd != 0 && x.TriGia >= 0).ToList();
            //List Debit
            List<GeneralLedger> lstGeneralLegerDebit = Query.Where(c => c.ReferenceID != Guid.Empty && c.ReferenceID != null && c.AccountingObjectID != Guid.Empty && c.AccountingObjectID != null && c.Account != null && c.CurrencyID != null
            && c.AccountingObjectID == accountingObjectId && c.Account == accountNumber && c.CurrencyID == currencyId && (new int[] { 320, 323, 701, 600 }.Contains(c.TypeID))).ToList();
            List<ConfrontVouchers> lstDebit = (from c in lstGeneralLegerDebit.ToList()
                                               group c by new { c.AccountingObjectID, c.No, c.TypeID, c.InvoiceNo, c.Account, c.CurrencyID, c.Date, c.ReferenceID, c.EmployeeID, c.ExchangeRate } into a
                                               let guid = a.Key
                                               where guid != null
                                               select new ConfrontVouchers()
                                               {
                                                   NgayChungTu = a.Key.Date,
                                                   LoaiChungTu = lsTypes.SingleOrDefault(c => c.ID == a.Key.TypeID).TypeName,
                                                   SoChungTu = a.Key.No ?? "",
                                                   SoHoaDon = a.Key.InvoiceNo ?? "",
                                                   NhanVien = a.Key.EmployeeID != null ? lstAccountingObjects.FirstOrDefault(c => c.ID == a.Key.EmployeeID).AccountingObjectCode : "",
                                                   TriGia = a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)),//Tiền việt nam
                                                   TriGiaQd = a.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),//Tiền ngoại tệ
                                                   ChuaThanhToan = (a.Sum(t => Math.Round(t.DebitAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                                   (lstExceptVouchers.Where(c => c.GLVoucherID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))) -
                                                    lstDepositDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                                   lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (currencyId == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))),//Chưa thanh toán tiền việt nam
                                                   ChuaThanhToanQd = (a.Sum(t => Math.Round(t.DebitAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) - a.Sum(t => Math.Round(t.CreditAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                   (lstExceptVouchers.Where(c => c.GLVoucherID == a.Key.ReferenceID && c.CurrencyID == a.Key.CurrencyID && c.DebitAccount == a.Key.Account).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) -
                                                    lstDepositDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                   lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == a.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) * a.Key.ExchangeRate ?? 1,
                                                   GlVoucherId = a.Key.ReferenceID,
                                                   CurrencyID = a.Key.CurrencyID,
                                                   RefVoucherExchangeRate = a.Key.ExchangeRate,
                                                   LastExchangeRate = lstgo.Any(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)) ?
                            lstgo.Where(x => x.CurrencyID == a.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == a.Key.ReferenceID)).OrderByDescending(d => d.DateReevaluate)
                            .FirstOrDefault().VoucherDetailForeignCurrencys.FirstOrDefault(x => x.ReferenceID == a.Key.ReferenceID).NewExchangeRate : 0,
                                                   SoDoiTruQD = 0,
                                                   SoDoiTru = 0,
                                                   TyGia = 0,
                                                   SoConLai = 0,
                                                   SoConLaiQd = 0,
                                               }).ToList();
            foreach (var x in lstDebit)
            {
                x.ChuaThanhToanQd = x.LastExchangeRate > 0 ? (x.ChuaThanhToan * x.LastExchangeRate) : (x.ChuaThanhToan * x.RefVoucherExchangeRate);
            }
            Except.Debits = lstDebit.Where(x => x.ChuaThanhToan != 0 && x.ChuaThanhToanQd != 0 && x.TriGia >= 0).ToList();
            return Except;
        }
        #endregion
        #endregion
        #region Lưu sổ cái

        #region Tạo chứng từ để lưu sổ

        #region Tạo chứng từ sổ kho
        //Sổ Kho
        /// <summary>
        /// Tạo chứng từ Sổ kho trong trường hợp mua hàng qua kho
        /// </summary>
        /// <param name="ppInvoice"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GenRepositoryLedgers(PPInvoice ppInvoice)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            for (int i = 0; i < ppInvoice.PPInvoiceDetails.Count; i++)
            {
                RepositoryLedger reposTemp = new RepositoryLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppInvoice.ID,
                    TypeID = 402,
                    Date = ppInvoice.Date,
                    PostedDate = ppInvoice.PostedDate,
                    No = ppInvoice.InwardNo,
                    Account = ppInvoice.PPInvoiceDetails[i].DebitAccount,
                    AccountCorresponding = ppInvoice.PPInvoiceDetails[i].CreditAccount,
                    RepositoryID = ppInvoice.PPInvoiceDetails[i].RepositoryID ?? Guid.Empty,
                    MaterialGoodsID = ppInvoice.PPInvoiceDetails[i].MaterialGoodsID ?? Guid.Empty,
                    Unit = ppInvoice.PPInvoiceDetails[i].Unit,
                    UnitPrice = 0,
                    IWQuantity = ppInvoice.PPInvoiceDetails[i].Quantity,
                    IWAmount = ppInvoice.PPInvoiceDetails[i].InwardAmount,
                    OWQuantity = 0,
                    OWAmount = 0,
                    IWQuantityBalance = ppInvoice.PPInvoiceDetails[i].Quantity,
                    IWAmountBalance = ppInvoice.PPInvoiceDetails[i].InwardAmount,
                    Reason = ppInvoice.Reason,
                    //Description = "",
                    OWPurpose = null,
                    ExpiryDate = ppInvoice.PPInvoiceDetails[i].ExpiryDate,
                    LotNo = ppInvoice.PPInvoiceDetails[i].LotNo,
                    CostSetID = ppInvoice.PPInvoiceDetails[i].CostSetID,
                    UnitConvert = ppInvoice.PPInvoiceDetails[i].UnitConvert,
                    UnitPriceConvert = ppInvoice.PPInvoiceDetails[i].UnitPriceConvert,
                    IWQuantityConvert = ppInvoice.PPInvoiceDetails[i].QuantityConvert,
                    OWQuantityConvert = 0,
                    StatisticsCodeID = ppInvoice.PPInvoiceDetails[i].StatisticsCodeID,
                    ConvertRate = ppInvoice.PPInvoiceDetails[i].ConvertRate,
                    DetailID = ppInvoice.PPInvoiceDetails[i].ID,
                    RefDateTime = ppInvoice.RefDateTime,

                };
                if (ppInvoice.PPInvoiceDetails[i].Quantity != 0)
                {
                    reposTemp.UnitPrice =
                        (decimal)(ppInvoice.PPInvoiceDetails[i].InwardAmount / ppInvoice.PPInvoiceDetails[i].Quantity);
                }
                listReposTemp.Add(reposTemp);
            }
            return listReposTemp;
        }

        /// <summary>
        /// Tạo chứng từ sổ kho trong trường hợp hàng bán trả lại
        /// </summary>
        /// <param name="ppDiscountReturn"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GenRepositoryLedgers(SAReturn saReturn)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            if (saReturn.TypeID == 330 && (saReturn.IsDeliveryVoucher ?? false))
            {
                for (int i = 0; i < saReturn.SAReturnDetails.Count; i++)
                {
                    RepositoryLedger reposTemp = new RepositoryLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = 403,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.IWNo,
                        Account = saReturn.SAReturnDetails[i].RepositoryAccount,
                        AccountCorresponding = saReturn.SAReturnDetails[i].CostAccount,
                        RepositoryID = saReturn.SAReturnDetails[i].RepositoryID ?? Guid.Empty,
                        MaterialGoodsID = saReturn.SAReturnDetails[i].MaterialGoodsID ?? Guid.Empty,
                        Unit = saReturn.SAReturnDetails[i].Unit,
                        UnitPrice = saReturn.SAReturnDetails[i].OWPrice,
                        IWQuantity = saReturn.SAReturnDetails[i].Quantity,
                        IWAmount = saReturn.SAReturnDetails[i].OWAmount,
                        OWQuantity = 0,
                        OWAmount = 0,
                        IWQuantityBalance = saReturn.SAReturnDetails[i].Quantity,
                        IWAmountBalance = saReturn.SAReturnDetails[i].OWAmount,
                        Reason = saReturn.Reason,
                        OWPurpose = null,
                        ExpiryDate = saReturn.SAReturnDetails[i].ExpiryDate,
                        LotNo = saReturn.SAReturnDetails[i].LotNo,
                        CostSetID = saReturn.SAReturnDetails[i].CostSetID,
                        UnitConvert = saReturn.SAReturnDetails[i].UnitConvert,
                        UnitPriceConvert = saReturn.SAReturnDetails[i].UnitPriceConvert,
                        IWQuantityConvert = saReturn.SAReturnDetails[i].QuantityConvert,
                        OWQuantityConvert = 0,
                        StatisticsCodeID = saReturn.SAReturnDetails[i].StatisticsCodeID,
                        ConvertRate = saReturn.SAReturnDetails[i].ConvertRate,
                        DetailID = saReturn.SAReturnDetails[i].ID,
                        RefDateTime = saReturn.RefDateTime
                    };
                    listReposTemp.Add(reposTemp);
                }
            }
            return listReposTemp;
        }
        /// <summary>
        /// Tạo chứng từ sổ kho trong trường hợp hàng mua trả lại giảm giá
        /// </summary>
        /// <param name="ppDiscountReturn"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GenRepositoryLedgers(PPDiscountReturn ppDiscountReturn)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            //Trường hợp hàng mua trả lại typeID = 220
            if (ppDiscountReturn.TypeID == 220 && (ppDiscountReturn.IsDeliveryVoucher ?? false))
            {
                for (int i = 0; i < ppDiscountReturn.PPDiscountReturnDetails.Count; i++)
                {
                    RepositoryLedger reposTemp = new RepositoryLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppDiscountReturn.ID,
                        TypeID = 413,
                        Date = ppDiscountReturn.Date,
                        PostedDate = ppDiscountReturn.PostedDate,
                        No = ppDiscountReturn.OutwardNo,
                        Account = ppDiscountReturn.PPDiscountReturnDetails[i].DebitAccount,
                        AccountCorresponding = ppDiscountReturn.PPDiscountReturnDetails[i].CreditAccount,
                        RepositoryID = ppDiscountReturn.PPDiscountReturnDetails[i].RepositoryID ?? Guid.Empty,
                        MaterialGoodsID = ppDiscountReturn.PPDiscountReturnDetails[i].MaterialGoodsID ?? Guid.Empty,
                        Unit = ppDiscountReturn.PPDiscountReturnDetails[i].Unit,
                        UnitPrice = ppDiscountReturn.PPDiscountReturnDetails[i].UnitPrice ?? 0,
                        IWQuantity = 0,
                        IWAmount = 0,
                        OWQuantity = ppDiscountReturn.PPDiscountReturnDetails[i].Quantity,
                        OWAmount = ppDiscountReturn.PPDiscountReturnDetails[i].Amount,
                        IWQuantityBalance = 0,
                        IWAmountBalance = 0,
                        Reason = ppDiscountReturn.Reason,
                        //Description = ppDiscountReturn.Description,
                        OWPurpose = null,
                        ExpiryDate = ppDiscountReturn.PPDiscountReturnDetails[i].ExpiryDate,
                        LotNo = ppDiscountReturn.PPDiscountReturnDetails[i].LotNo,
                        CostSetID = ppDiscountReturn.PPDiscountReturnDetails[i].CostSetID,
                        UnitConvert = ppDiscountReturn.PPDiscountReturnDetails[i].UnitConvert,
                        UnitPriceConvert = ppDiscountReturn.PPDiscountReturnDetails[i].UnitPriceConvert ?? 0,
                        IWQuantityConvert = 0,
                        OWQuantityConvert = ppDiscountReturn.PPDiscountReturnDetails[i].QuantityConvert,
                        StatisticsCodeID = ppDiscountReturn.PPDiscountReturnDetails[i].StatisticsCodeID,
                        ConvertRate = ppDiscountReturn.PPDiscountReturnDetails[i].ConvertRate,
                        DetailID = ppDiscountReturn.PPDiscountReturnDetails[i].ID
                    };
                    listReposTemp.Add(reposTemp);
                }
            }
            //Trường hợp hàng giảm giá 
            if (ppDiscountReturn.TypeID == 230)
            {
                for (int i = 0; i < ppDiscountReturn.PPDiscountReturnDetails.Count; i++)
                {
                    RepositoryLedger reposTemp = new RepositoryLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppDiscountReturn.ID,
                        TypeID = ppDiscountReturn.TypeID,
                        Date = ppDiscountReturn.Date,
                        PostedDate = ppDiscountReturn.PostedDate,
                        No = ppDiscountReturn.No,
                        Account = ppDiscountReturn.PPDiscountReturnDetails[i].DebitAccount,
                        AccountCorresponding = ppDiscountReturn.PPDiscountReturnDetails[i].CreditAccount,
                        RepositoryID = ppDiscountReturn.PPDiscountReturnDetails[i].RepositoryID ?? Guid.Empty,
                        MaterialGoodsID = ppDiscountReturn.PPDiscountReturnDetails[i].MaterialGoodsID ?? Guid.Empty,
                        Unit = ppDiscountReturn.PPDiscountReturnDetails[i].Unit,
                        UnitPrice = ppDiscountReturn.PPDiscountReturnDetails[i].UnitPrice ?? 0,
                        IWQuantity = 0,
                        IWAmount = 0,
                        OWQuantity = 0,
                        OWAmount = ppDiscountReturn.PPDiscountReturnDetails[i].Amount,
                        IWQuantityBalance = 0,
                        IWAmountBalance = 0,
                        Reason = ppDiscountReturn.Reason,
                        //Description = ppDiscountReturn.Description,
                        OWPurpose = null,
                        ExpiryDate = ppDiscountReturn.PPDiscountReturnDetails[i].ExpiryDate,
                        LotNo = ppDiscountReturn.PPDiscountReturnDetails[i].LotNo,
                        CostSetID = ppDiscountReturn.PPDiscountReturnDetails[i].CostSetID,
                        UnitConvert = ppDiscountReturn.PPDiscountReturnDetails[i].UnitConvert,
                        UnitPriceConvert = ppDiscountReturn.PPDiscountReturnDetails[i].UnitPriceConvert ?? 0,
                        IWQuantityConvert = ppDiscountReturn.PPDiscountReturnDetails[i].QuantityConvert,
                        OWQuantityConvert = 0,
                        StatisticsCodeID = ppDiscountReturn.PPDiscountReturnDetails[i].StatisticsCodeID,
                        ConvertRate = ppDiscountReturn.PPDiscountReturnDetails[i].ConvertRate,
                        DetailID = ppDiscountReturn.PPDiscountReturnDetails[i].ID

                    };
                    listReposTemp.Add(reposTemp);
                }
            }
            return listReposTemp;
        }

        /// <summary>
        /// Tạo chứng sổ kho trong trường hợp xuất nhập kho
        /// </summary>
        /// <param name="rsInwardOutward"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GenRepositoryLedgers(RSInwardOutward rsInwardOutward)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            if (rsInwardOutward.TypeID.ToString(CultureInfo.InvariantCulture).StartsWith("41"))
            {
                for (int i = 0; i < rsInwardOutward.RSInwardOutwardDetails.Count; i++)
                {
                    RepositoryLedger reposTemp = new RepositoryLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = rsInwardOutward.ID,
                        TypeID = rsInwardOutward.TypeID,
                        Date = (DateTime)rsInwardOutward.Date,
                        PostedDate = (DateTime)rsInwardOutward.PostedDate,
                        No = rsInwardOutward.No,
                        Account = rsInwardOutward.RSInwardOutwardDetails[i].CreditAccount,
                        AccountCorresponding = rsInwardOutward.RSInwardOutwardDetails[i].DebitAccount,
                        RepositoryID = rsInwardOutward.RSInwardOutwardDetails[i].RepositoryID ?? Guid.Empty,
                        MaterialGoodsID = rsInwardOutward.RSInwardOutwardDetails[i].MaterialGoodsID ?? Guid.Empty,
                        Unit = rsInwardOutward.RSInwardOutwardDetails[i].Unit,
                        UnitPrice = (decimal)rsInwardOutward.RSInwardOutwardDetails[i].UnitPrice,
                        OWQuantity = rsInwardOutward.RSInwardOutwardDetails[i].Quantity,
                        IWQuantity = 0,
                        OWAmount = rsInwardOutward.RSInwardOutwardDetails[i].Amount,
                        IWAmount = 0,
                        IWQuantityBalance = 0,
                        IWAmountBalance = 0,
                        Reason = rsInwardOutward.Reason,
                        Description = rsInwardOutward.Reason,
                        OWPurpose = null,
                        ExpiryDate = rsInwardOutward.RSInwardOutwardDetails[i].ExpiryDate,
                        LotNo = rsInwardOutward.RSInwardOutwardDetails[i].LotNo,
                        CostSetID = rsInwardOutward.RSInwardOutwardDetails[i].CostSetID,
                        UnitConvert = rsInwardOutward.RSInwardOutwardDetails[i].UnitConvert,
                        UnitPriceConvert = (decimal)rsInwardOutward.RSInwardOutwardDetails[i].UnitPriceConvert,
                        OWQuantityConvert = rsInwardOutward.RSInwardOutwardDetails[i].QuantityConvert,
                        StatisticsCodeID = rsInwardOutward.RSInwardOutwardDetails[i].StatisticsCodeID,
                        ConvertRate = rsInwardOutward.RSInwardOutwardDetails[i].ConvertRate,
                        RefDateTime = rsInwardOutward.RefDateTime,
                        DetailID = rsInwardOutward.RSInwardOutwardDetails[i].ID
                    };
                    listReposTemp.Add(reposTemp);
                    string s = _ISystemOptionService.GetByCode("VTHH_PPTinhGiaXKho").Data;
                    if (rsInwardOutward.RSAssemblyDismantlementID != null && s == "Đích danh" && rsInwardOutward.RSInwardOutwardDetails[i].LotNo != null)
                    {
                        var RSAssemblyDismantlement = _IRSAssemblyDismantlementService.Getbykey(rsInwardOutward.RSAssemblyDismantlementID ?? Guid.Empty);

                        foreach (RSAssemblyDismantlementDetail RSAssemblyDismantlementDetail in RSAssemblyDismantlement.RSAssemblyDismantlementDetails)
                        {

                            if (rsInwardOutward.RSInwardOutwardDetails[i].MaterialGoodsID == RSAssemblyDismantlementDetail.MaterialGoodsID)
                            {
                                RSAssemblyDismantlementDetail.UnitPriceOriginal = rsInwardOutward.RSInwardOutwardDetails[i].UnitPrice / (rsInwardOutward.ExchangeRate ?? 1);
                                RSAssemblyDismantlementDetail.UnitPrice = rsInwardOutward.RSInwardOutwardDetails[i].UnitPrice;
                                RSAssemblyDismantlementDetail.AmountOriginal = RSAssemblyDismantlementDetail.UnitPriceOriginal * (RSAssemblyDismantlementDetail.Quantity ?? 1);
                                RSAssemblyDismantlementDetail.Amount = RSAssemblyDismantlementDetail.AmountOriginal * (rsInwardOutward.ExchangeRate ?? 1);
                            }

                        }
                        RSAssemblyDismantlement.Amount = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.Amount);
                        RSAssemblyDismantlement.AmountOriginal = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.AmountOriginal);
                        _IRSAssemblyDismantlementService.Update(RSAssemblyDismantlement);
                    }

                }

            }
            else if (rsInwardOutward.TypeID.ToString(CultureInfo.InvariantCulture).StartsWith("40"))
            {
                for (int i = 0; i < rsInwardOutward.RSInwardOutwardDetails.Count; i++)
                {
                    RepositoryLedger reposTemp = new RepositoryLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = rsInwardOutward.ID,
                        TypeID = rsInwardOutward.TypeID,
                        Date = (DateTime)rsInwardOutward.Date,
                        PostedDate = (DateTime)rsInwardOutward.PostedDate,
                        No = rsInwardOutward.No,
                        Account = rsInwardOutward.RSInwardOutwardDetails[i].DebitAccount,
                        AccountCorresponding = rsInwardOutward.RSInwardOutwardDetails[i].CreditAccount,
                        RepositoryID = rsInwardOutward.RSInwardOutwardDetails[i].RepositoryID ?? Guid.Empty,
                        MaterialGoodsID = rsInwardOutward.RSInwardOutwardDetails[i].MaterialGoodsID ?? Guid.Empty,
                        Unit = rsInwardOutward.RSInwardOutwardDetails[i].Unit,
                        UnitPrice = (decimal)rsInwardOutward.RSInwardOutwardDetails[i].UnitPrice,
                        IWQuantity = rsInwardOutward.RSInwardOutwardDetails[i].Quantity,
                        OWQuantity = 0,
                        IWAmount = rsInwardOutward.RSInwardOutwardDetails[i].Amount,
                        OWAmount = 0,
                        IWQuantityBalance = rsInwardOutward.RSInwardOutwardDetails[i].Quantity,
                        IWAmountBalance = rsInwardOutward.RSInwardOutwardDetails[i].Amount,
                        Reason = rsInwardOutward.Reason,
                        Description = rsInwardOutward.Reason,
                        OWPurpose = null,
                        ExpiryDate = rsInwardOutward.RSInwardOutwardDetails[i].ExpiryDate,
                        LotNo = rsInwardOutward.RSInwardOutwardDetails[i].LotNo,
                        CostSetID = rsInwardOutward.RSInwardOutwardDetails[i].CostSetID,
                        UnitConvert = rsInwardOutward.RSInwardOutwardDetails[i].UnitConvert,
                        UnitPriceConvert = (decimal)rsInwardOutward.RSInwardOutwardDetails[i].UnitPriceConvert,
                        OWQuantityConvert = rsInwardOutward.RSInwardOutwardDetails[i].QuantityConvert,
                        StatisticsCodeID = rsInwardOutward.RSInwardOutwardDetails[i].StatisticsCodeID,
                        ConvertRate = rsInwardOutward.RSInwardOutwardDetails[i].ConvertRate,
                        RefDateTime = rsInwardOutward.RefDateTime,
                        DetailID = rsInwardOutward.RSInwardOutwardDetails[i].ID
                    };
                    listReposTemp.Add(reposTemp);
                }
            }

            return listReposTemp;
        }

        /// <summary>
        /// Tạo chứng từ sổ kho với chứng từ lắp ráp tháo dỡ
        /// </summary>
        /// <param name="rsAssemblyDismantlement"></param>
        /// <returns></returns>
        //public List<RepositoryLedger> GenRepositoryLedgers(RSAssemblyDismantlement rsAssemblyDismantlement)
        //{
        //    List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
        //    for (int i = 0; i < rsAssemblyDismantlement.RSAssemblyDismantlementDetails.Count; i++)
        //    {
        //        RepositoryLedger reposTemp = new RepositoryLedger
        //        {
        //            ID = Guid.NewGuid(),
        //            BranchID = null,
        //            ReferenceID = rsAssemblyDismantlement.ID,
        //            TypeID = rsAssemblyDismantlement.TypeID,
        //            Date = (DateTime)rsAssemblyDismantlement.IWDate,
        //            PostedDate = (DateTime)rsAssemblyDismantlement.IWPostedDate,
        //            No = rsAssemblyDismantlement.IWNo,
        //            Account = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DebitAccount,
        //            AccountCorresponding = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CreditAccount,
        //            RepositoryID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].RepositoryID ?? Guid.Empty,
        //            MaterialGoodsID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].MaterialGoodsID ?? Guid.Empty,
        //            Unit = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Unit,
        //            UnitPrice = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].UnitPrice,
        //            IWQuantity = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Quantity,
        //            IWAmount = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Amount,
        //            OWQuantity = 0,
        //            OWAmount = 0,
        //            IWQuantityBalance = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Quantity,
        //            IWAmountBalance = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Amount,
        //            Reason = rsAssemblyDismantlement.IWReason,
        //            //Description = rsAssemblyDismantlement.Description,
        //            OWPurpose = null,
        //            ExpiryDate = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ExpiryDate,
        //            LotNo = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].LotNo,
        //            CostSetID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CostSetID,
        //            UnitConvert = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].UnitConvert,
        //            UnitPriceConvert = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].UnitPriceConvert,
        //            IWQuantityConvert = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].QuantityConvert,
        //            OWQuantityConvert = 0,
        //            StatisticsCodeID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].StatisticsCodeID,
        //            ConvertRate = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ConvertRate,
        //            DetailID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ID

        //        };
        //        listReposTemp.Add(reposTemp);
        //    }
        //    return listReposTemp;
        //}

        /// <summary>
        /// Tạo chứng từ sổ kho trong trường hợp chuyển kho
        /// </summary>
        /// <param name="rsTransfer"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GenRepositoryLedgers(RSTransfer rsTransfer)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            for (int i = 0; i < rsTransfer.RSTransferDetails.Count; i++)
            {
                RepositoryLedger reposTemp = new RepositoryLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = rsTransfer.ID,
                    TypeID = rsTransfer.TypeID,
                    Date = (DateTime)rsTransfer.Date,
                    PostedDate = (DateTime)rsTransfer.PostedDate,
                    No = rsTransfer.No,
                    Account = rsTransfer.RSTransferDetails[i].DebitAccount,
                    AccountCorresponding = rsTransfer.RSTransferDetails[i].CreditAccount,
                    RepositoryID = rsTransfer.RSTransferDetails[i].ToRepositoryID ?? Guid.Empty,
                    MaterialGoodsID = rsTransfer.RSTransferDetails[i].MaterialGoodsID ?? Guid.Empty,
                    Unit = rsTransfer.RSTransferDetails[i].Unit,
                    UnitPrice = (decimal)rsTransfer.RSTransferDetails[i].UnitPrice,
                    IWQuantity = rsTransfer.RSTransferDetails[i].Quantity,
                    IWAmount = rsTransfer.RSTransferDetails[i].Amount,
                    OWQuantity = 0,
                    OWAmount = 0,
                    IWQuantityBalance = rsTransfer.RSTransferDetails[i].Quantity,
                    IWAmountBalance = rsTransfer.RSTransferDetails[i].Amount,
                    Reason = rsTransfer.Reason,
                    //Description = rsTransfer.Description,
                    OWPurpose = null,
                    ExpiryDate = rsTransfer.RSTransferDetails[i].ExpiryDate,
                    LotNo = rsTransfer.RSTransferDetails[i].LotNo,
                    CostSetID = rsTransfer.RSTransferDetails[i].CostSetID,
                    UnitConvert = rsTransfer.RSTransferDetails[i].UnitConvert,
                    UnitPriceConvert = rsTransfer.RSTransferDetails[i].UnitPriceConvert,
                    IWQuantityConvert = rsTransfer.RSTransferDetails[i].QuantityConvert,
                    OWQuantityConvert = 0,
                    StatisticsCodeID = rsTransfer.RSTransferDetails[i].StatisticsCodeID,
                    ConvertRate = rsTransfer.RSTransferDetails[i].ConvertRate,
                    DetailID = rsTransfer.RSTransferDetails[i].ID,
                    RefDateTime = rsTransfer.RefDateTime


                };
                listReposTemp.Add(reposTemp);
            }
            for (int i = 0; i < rsTransfer.RSTransferDetails.Count; i++)
            {
                RepositoryLedger reposTemp = new RepositoryLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = rsTransfer.ID,
                    TypeID = rsTransfer.TypeID,
                    Date = (DateTime)rsTransfer.Date,
                    PostedDate = (DateTime)rsTransfer.PostedDate,
                    No = rsTransfer.No,
                    Account = rsTransfer.RSTransferDetails[i].CreditAccount,
                    AccountCorresponding = rsTransfer.RSTransferDetails[i].DebitAccount,
                    RepositoryID = rsTransfer.RSTransferDetails[i].FromRepositoryID ?? Guid.Empty,
                    MaterialGoodsID = rsTransfer.RSTransferDetails[i].MaterialGoodsID ?? Guid.Empty,
                    Unit = rsTransfer.RSTransferDetails[i].Unit,
                    UnitPrice = (decimal)rsTransfer.RSTransferDetails[i].UnitPrice,
                    IWQuantity = 0,
                    IWAmount = 0,
                    OWQuantity = rsTransfer.RSTransferDetails[i].Quantity,
                    OWAmount = rsTransfer.RSTransferDetails[i].Amount,
                    IWQuantityBalance = 0,
                    IWAmountBalance = 0,
                    Reason = rsTransfer.Reason,
                    //Description = rsTransfer.Description,
                    OWPurpose = null,
                    ExpiryDate = rsTransfer.RSTransferDetails[i].ExpiryDate,
                    LotNo = rsTransfer.RSTransferDetails[i].LotNo,
                    CostSetID = rsTransfer.RSTransferDetails[i].CostSetID,
                    UnitConvert = rsTransfer.RSTransferDetails[i].UnitConvert,
                    UnitPriceConvert = rsTransfer.RSTransferDetails[i].UnitPriceConvert,
                    IWQuantityConvert = rsTransfer.RSTransferDetails[i].QuantityConvert,
                    OWQuantityConvert = 0,
                    StatisticsCodeID = rsTransfer.RSTransferDetails[i].StatisticsCodeID,
                    ConvertRate = rsTransfer.RSTransferDetails[i].ConvertRate,
                    DetailID = rsTransfer.RSTransferDetails[i].ID,
                    RefDateTime = rsTransfer.RefDateTime
                };
                listReposTemp.Add(reposTemp);
            }
            return listReposTemp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="saInvoice"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GenRepositoryLedgers(SAInvoice saInvoice)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            int type = 412;
            if (new int[] { 323, 324, 325 }.Any(d => d == saInvoice.TypeID)) type = 415;
            // Lưu vào sổ kho phiếu xuất kho nếu xuất kho bằng phương pháp đích danh hoặc nhập trước, xuất trước
            for (int i = 0; i < saInvoice.SAInvoiceDetails.Count; i++)
            {
                RepositoryLedger reposTemp = new RepositoryLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = saInvoice.ID,
                    TypeID = type,
                    Date = saInvoice.Date,
                    PostedDate = saInvoice.PostedDate,
                    No = saInvoice.OutwardNo,
                    Account = saInvoice.SAInvoiceDetails[i].RepositoryAccount,
                    AccountCorresponding = saInvoice.SAInvoiceDetails[i].CostAccount,
                    RepositoryID = saInvoice.SAInvoiceDetails[i].RepositoryID ?? Guid.Empty,
                    MaterialGoodsID = saInvoice.SAInvoiceDetails[i].MaterialGoodsID ?? Guid.Empty,
                    Unit = saInvoice.SAInvoiceDetails[i].Unit,
                    UnitPrice = (decimal)saInvoice.SAInvoiceDetails[i].OWPrice,
                    IWQuantity = 0,
                    IWAmount = 0,
                    OWQuantity = saInvoice.SAInvoiceDetails[i].Quantity,
                    OWAmount = saInvoice.SAInvoiceDetails[i].OWAmount,
                    //Khi bán hàng chưa thu tiền với phương pháp đích danh hoặc nhập trước xuất trước, sinh ra 1 phiếu xuất kho và lưu vào sổ kho
                    //Chọn 1 mã hàng để xuất sẽ hiển thị 1 cửa sổ để chọn các phiếu nhập kho trước đó. 
                    //Lưu ý Tổng số lượng tồn và Tổng Giá trị tồn của các phiếu nhập kho đã chọn tương ứng là X và Y
                    // Khi đó IWQuantityBalance = X - OWQuantity
                    // IWAmountBalance = Y - OWAmount
                    //IWQuantityBalance =???,
                    //IWAmountBalance =???,
                    Reason = saInvoice.Reason,
                    //Description = saInvoice.Description,
                    OWPurpose = null,
                    ExpiryDate = saInvoice.SAInvoiceDetails[i].ExpiryDate,
                    LotNo = saInvoice.SAInvoiceDetails[i].LotNo,
                    CostSetID = saInvoice.SAInvoiceDetails[i].CostSetID,
                    UnitConvert = saInvoice.SAInvoiceDetails[i].UnitConvert,
                    UnitPriceConvert = (decimal)saInvoice.SAInvoiceDetails[i].UnitPriceConvert,
                    IWQuantityConvert = 0,
                    OWQuantityConvert = saInvoice.SAInvoiceDetails[i].QuantityConvert,
                    StatisticsCodeID = saInvoice.SAInvoiceDetails[i].StatisticsCodeID,
                    ConvertRate = saInvoice.SAInvoiceDetails[i].ConvertRate,
                    DetailID = saInvoice.SAInvoiceDetails[i].ID,
                    RefDateTime = saInvoice.RefDateTime
                };
                listReposTemp.Add(reposTemp);
            }
            return listReposTemp;
        }
        #endregion

        #region Tạo chứng từ sổ tài sản cố định
        //Sổ TCCĐ
        /// <summary>
        /// Tạo chứng từ Sổ TSCĐ cho Phiếu Chi mua TSCĐ
        /// </summary>
        /// <param name="mcPayment"></param>
        /// <returns></returns>
        protected List<FixedAssetLedger> GenFixedAssetLedgers(MCPayment mcPayment)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            //Code Maping

            return listFixAssetLedger;
        }

        protected List<ToolLedger> GenToolLedgers(TIIncrement tiIncrement)
        {
            List<ToolLedger> listToolLedger = new List<ToolLedger>();
            List<TIIncrementDetail> lstDT = tiIncrement.TIIncrementDetails.OrderBy(n => n.OrderPriority).ToList();

            for (int i = 0; i < lstDT.Count; i++)
            {
                var toolsID = lstDT[i].ToolsID ?? Guid.Empty;
                if (lstDT[i].DepartmentID == Guid.Empty || lstDT[i].DepartmentID == null)
                {
                    List<TIInitDetail> details = _ITIInitDetailService.GetByTIInitID(toolsID);
                    foreach (TIInitDetail detail in details)
                    {
                        if (detail.ObjectType != 1) continue;
                        ToolLedger ToolLedger = GetLastestToolLedger(toolsID, tiIncrement.PostedDate);
                        ToolLedger.ID = Guid.NewGuid();
                        ToolLedger.ReferenceID = tiIncrement.ID;
                        ToolLedger.No = tiIncrement.No;
                        ToolLedger.TypeID = tiIncrement.TypeID;
                        ToolLedger.Date = tiIncrement.Date;
                        ToolLedger.PostedDate = tiIncrement.PostedDate;
                        ToolLedger.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                        ToolLedger.Reason = tiIncrement.Reason;
                        ToolLedger.Description = lstDT[i].Description;

                        ToolLedger.IncrementAllocationTime = lstDT[i].AllocationTimes;
                        ToolLedger.IncrementQuantity = detail.Quantity;
                        ToolLedger.IncrementAmount = lstDT[i].AmountOriginal * detail.Quantity / lstDT[i].Quantity;
                        ToolLedger.AllocatedAmount = lstDT[i].AllocationTimes != 0 ? ToolLedger.IncrementAmount / lstDT[i].AllocationTimes : 0;
                        ToolLedger.UnitPrice = lstDT[i].UnitPrice;
                        ToolLedger.DepartmentID = detail.ObjectID;

                        ToolLedger.RemainingAllocaitonTimes = lstDT[i].AllocationTimes;
                        ToolLedger.RemainingAmount = ToolLedger.RemainingAmount;
                        ToolLedger.RemainingQuantity = lstDT[i].Quantity;
                        ToolLedger.IncrementDate = tiIncrement.PostedDate;
                        ToolLedger.Nos = tiIncrement.No;
                        ToolLedger.ReferenceID = tiIncrement.ID;


                        listToolLedger.Add(ToolLedger);
                    }
                }
                else
                {
                    ToolLedger ToolLedger = GetLastestToolLedger(toolsID, tiIncrement.PostedDate);
                    ToolLedger.ID = Guid.NewGuid();
                    ToolLedger.ReferenceID = tiIncrement.ID;
                    ToolLedger.No = tiIncrement.No;
                    ToolLedger.TypeID = tiIncrement.TypeID;
                    ToolLedger.Date = tiIncrement.Date;
                    ToolLedger.PostedDate = tiIncrement.PostedDate;
                    ToolLedger.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                    ToolLedger.Reason = tiIncrement.Reason;
                    ToolLedger.Description = lstDT[i].Description;

                    ToolLedger.IncrementAllocationTime = lstDT[i].AllocationTimes;
                    ToolLedger.IncrementQuantity = lstDT[i].Quantity;
                    ToolLedger.IncrementAmount = lstDT[i].AmountOriginal * lstDT[i].Quantity;
                    ToolLedger.AllocatedAmount = lstDT[i].AllocationTimes != 0 ? ToolLedger.IncrementAmount / lstDT[i].AllocationTimes : 0;
                    ToolLedger.UnitPrice = lstDT[i].UnitPrice;
                    ToolLedger.DepartmentID = lstDT[i].DepartmentID;

                    ToolLedger.RemainingAllocaitonTimes = lstDT[i].AllocationTimes;
                    ToolLedger.RemainingAmount = ToolLedger.RemainingAmount;
                    ToolLedger.RemainingQuantity = lstDT[i].Quantity;
                    ToolLedger.IncrementDate = tiIncrement.PostedDate;
                    ToolLedger.Nos = tiIncrement.No;
                    ToolLedger.ReferenceID = tiIncrement.ID;


                    listToolLedger.Add(ToolLedger);
                }


            }

            return listToolLedger;
        }

        protected List<ToolLedger> GenToolLedgers(TIDecrement TIDecrement)
        {
            List<ToolLedger> listFixAssetLedger = new List<ToolLedger>();
            List<TIDecrementDetail> lstDT = TIDecrement.TIDecrementDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var toolsID = lstDT[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (toolsID != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)toolsID);
                //}
                ToolLedger ToolLedger = GetLastestToolLedger(toolsID, TIDecrement.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = TIDecrement.ID;
                ToolLedger.No = TIDecrement.No;
                ToolLedger.TypeID = TIDecrement.TypeID;
                ToolLedger.Date = TIDecrement.Date;
                ToolLedger.PostedDate = TIDecrement.PostedDate;
                ToolLedger.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = TIDecrement.Reason;
                ToolLedger.Description = lstDT[i].Description;
                ToolLedger.DecrementQuantity = lstDT[i].DecrementQuantity;
                ToolLedger.DepartmentID = lstDT[i].DepartmentID;
                ToolLedger.UnitPrice = lstDT[i].UnitPrice;
                ToolLedger.DecrementAmount = ToolLedger.UnitPrice * ToolLedger.DecrementQuantity;
                ToolLedger.IncrementQuantity = 0;
                ToolLedger.IncrementAmount = 0;
                ToolLedger.IncrementAllocationTime = 0;

                ToolLedger.RemainingQuantity = ToolLedger.RemainingQuantity - (lstDT[i].DecrementQuantity ?? 0);
                listFixAssetLedger.Add(ToolLedger);
            }

            return listFixAssetLedger;
        }

        protected List<ToolLedger> GenToolLedgers(TIAdjustment TIAdjustment)
        {
            List<ToolLedger> listFixAssetLedger = new List<ToolLedger>();
            List<TIAdjustmentDetail> lstDT = TIAdjustment.TIAdjustmentDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var toolsID = lstDT[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (toolsID != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)toolsID);
                //}
                ToolLedger ToolLedger = GetLastestToolLedger(toolsID, TIAdjustment.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = TIAdjustment.ID;
                ToolLedger.No = TIAdjustment.No;
                ToolLedger.TypeID = TIAdjustment.TypeID;
                ToolLedger.Date = TIAdjustment.Date;
                ToolLedger.PostedDate = TIAdjustment.PostedDate;
                ToolLedger.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = TIAdjustment.Reason;
                ToolLedger.Description = lstDT[i].Description;
                decimal time = (lstDT[i].NewRemainAllocationTimes ?? 0) - (lstDT[i].RemainAllocationTimes ?? 0);
                if (time > 0)
                {
                    ToolLedger.IncrementAllocationTime = time;
                    ToolLedger.DecrementAllocationTime = 0;
                }
                else
                {
                    ToolLedger.DecrementAllocationTime = -time;
                    ToolLedger.IncrementAllocationTime = 0;
                }
                decimal amount = (lstDT[i].NewRemainingAmount ?? 0) - (lstDT[i].RemainingAmount ?? 0);
                if (amount > 0)
                {
                    ToolLedger.IncrementAmount = amount;
                    ToolLedger.DecrementAmount = 0;
                }
                else
                {
                    ToolLedger.IncrementAmount = 0;
                    ToolLedger.DecrementAmount = -amount;
                }
                ToolLedger.DepartmentID = lstDT[i].DepartmentID;
                ToolLedger.AllocatedAmount = lstDT[i].AllocatedAmount;

                ToolLedger.RemainingAllocaitonTimes = lstDT[i].NewRemainAllocationTimes ?? 0;
                ToolLedger.RemainingAmount = lstDT[i].NewRemainingAmount ?? 0;

                listFixAssetLedger.Add(ToolLedger);
            }

            return listFixAssetLedger;
        }

        protected List<ToolLedger> GenToolLedgers(TITransfer TITransfer)
        {
            List<ToolLedger> listFixAssetLedger = new List<ToolLedger>();
            List<TITransferDetail> lstDT = TITransfer.TITransferDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var toolsID = lstDT[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (toolsID != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)toolsID);
                //}
                ToolLedger ToolLedger = GetLastestToolLedger(toolsID, TITransfer.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = TITransfer.ID;
                ToolLedger.No = TITransfer.No;
                ToolLedger.TypeID = TITransfer.TypeID;
                ToolLedger.Date = TITransfer.Date;
                ToolLedger.PostedDate = TITransfer.PostedDate;
                ToolLedger.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = TITransfer.Reason;
                ToolLedger.Description = lstDT[i].Description;
                ToolLedger.DepartmentID = lstDT[i].ToDepartmentID;
                ToolLedger.IncrementQuantity = lstDT[i].TransferQuantity;
                ToolLedger.DecrementQuantity = 0;
                ToolLedger.IncrementAmount = ToolLedger.IncrementQuantity * ToolLedger.UnitPrice;
                ToolLedger.DecrementAmount = 0;
                listFixAssetLedger.Add(ToolLedger);

                ToolLedger ToolLedger2 = GetLastestToolLedger(toolsID, TITransfer.PostedDate);
                ToolLedger2.ID = Guid.NewGuid();
                ToolLedger2.ReferenceID = TITransfer.ID;
                ToolLedger2.No = TITransfer.No;
                ToolLedger2.TypeID = TITransfer.TypeID;
                ToolLedger2.Date = TITransfer.Date;
                ToolLedger2.PostedDate = TITransfer.PostedDate;
                ToolLedger2.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                ToolLedger2.Reason = TITransfer.Reason;
                ToolLedger2.Description = lstDT[i].Description;
                ToolLedger2.DecrementQuantity = lstDT[i].TransferQuantity;
                ToolLedger2.IncrementQuantity = 0;
                ToolLedger2.DecrementAmount = ToolLedger.IncrementQuantity * ToolLedger.UnitPrice;
                ToolLedger2.IncrementAmount = 0;
                ToolLedger2.DepartmentID = lstDT[i].FromDepartmentID;
                listFixAssetLedger.Add(ToolLedger2);
            }

            return listFixAssetLedger;
        }

        protected List<ToolLedger> GenToolLedgers(TIAudit TIAudit)
        {
            List<ToolLedger> listFixAssetLedger = new List<ToolLedger>();
            for (int i = 0; i < TIAudit.TIAuditDetails.Count; i++)
            {
                var toolsID = TIAudit.TIAuditDetails[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (toolsID != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)toolsID);
                //}
                ToolLedger ToolLedger = GetLastestToolLedger(toolsID, TIAudit.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = TIAudit.ID;
                ToolLedger.No = TIAudit.No;
                ToolLedger.TypeID = TIAudit.TypeID;
                ToolLedger.Date = TIAudit.Date;
                ToolLedger.PostedDate = TIAudit.PostedDate;
                ToolLedger.ToolsID = TIAudit.TIAuditDetails[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = TIAudit.Description;
                ToolLedger.Description = TIAudit.TIAuditDetails[i].Note;
                ToolLedger.DepartmentID = TIAudit.TIAuditDetails[i].DepartmentID;
                if (TIAudit.TIAuditDetails[i].DiffQuantity < 0)
                {
                    ToolLedger.RemainingQuantity = ToolLedger.RemainingQuantity - TIAudit.TIAuditDetails[i].ExecuteQuantity;
                    ToolLedger.DecrementQuantity = TIAudit.TIAuditDetails[i].ExecuteQuantity;
                }
                else
                {
                    ToolLedger.RemainingQuantity = ToolLedger.RemainingQuantity + TIAudit.TIAuditDetails[i].ExecuteQuantity;
                    ToolLedger.IncrementQuantity = TIAudit.TIAuditDetails[i].ExecuteQuantity;
                }

                listFixAssetLedger.Add(ToolLedger);

            }

            return listFixAssetLedger;
        }

        protected List<ToolLedger> GenToolLedgers(TIAllocation TIAllocation)
        {
            List<ToolLedger> listToolLedger = new List<ToolLedger>();
            List<TIAllocationDetail> lstDT = TIAllocation.TIAllocationDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var fixedAssetId = lstDT[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (fixedAssetId != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                //}
                ToolLedger ToolLedger = GetLastestToolLedger(fixedAssetId, TIAllocation.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = TIAllocation.ID;
                ToolLedger.No = TIAllocation.No;
                ToolLedger.TypeID = TIAllocation.TypeID;
                ToolLedger.Date = TIAllocation.Date;
                ToolLedger.PostedDate = TIAllocation.PostedDate;
                ToolLedger.ToolsID = lstDT[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = TIAllocation.Reason;
                ToolLedger.Description = lstDT[i].Description;

                ToolLedger.IncrementAllocationTime = 0;
                ToolLedger.IncrementQuantity = 0;
                ToolLedger.IncrementAmount = 0;
                ToolLedger.DecrementAllocationTime = 1;
                ToolLedger.DecrementQuantity = 0;
                ToolLedger.DecrementAmount = 0;
                ToolLedger.AllocationAmount = (ToolLedger.AllocationAmount ?? 0) + lstDT[i].AllocationAmount;

                ToolLedger.DepartmentID = lstDT[i].DepartmentID;

                ToolLedger.RemainingAllocaitonTimes = ToolLedger.RemainingAllocaitonTimes - 1;
                ToolLedger.RemainingAmount = ToolLedger.RemainingAmount - lstDT[i].RemainingAmount - lstDT[i].AllocationAmount;

                ToolLedger.ReferenceID = TIAllocation.ID;


                listToolLedger.Add(ToolLedger);
            }

            return listToolLedger;
        }

        protected List<FixedAssetLedger> GenFixedAssetLedgers(FAIncrement faIncrement)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            List<FAIncrementDetail> lstDT = faIncrement.FAIncrementDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var fixedAssetId = lstDT[i].FixedAssetID;
                var fixedAsset = new FixedAsset();
                if (fixedAssetId != null)
                {
                    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                }
                FixedAssetLedger FixedAssetLedger = GetLastFixedAssetLedger(fixedAssetId, faIncrement.PostedDate);
                FixedAssetLedger.ID = Guid.NewGuid();
                FixedAssetLedger.ReferenceID = faIncrement.ID;
                FixedAssetLedger.No = faIncrement.No;
                FixedAssetLedger.TypeID = faIncrement.TypeID;
                FixedAssetLedger.Date = faIncrement.Date;
                FixedAssetLedger.PostedDate = faIncrement.PostedDate;
                FixedAssetLedger.FixedAssetID = lstDT[i].FixedAssetID ?? fixedAsset.ID;
                FixedAssetLedger.DepartmentID = lstDT[i].DepartmentID ?? fixedAsset.DepartmentID;
                FixedAssetLedger.UsedTime = FixedAssetLedger.UsedTimeRemain = fixedAsset.IsDisplayMonth ? fixedAsset.UsedTime : fixedAsset.UsedTime * 12;
                FixedAssetLedger.DepreciationRate = fixedAsset.DepreciationRate;
                FixedAssetLedger.MonthDepreciationRate = fixedAsset.MonthDepreciationRate;

                FixedAssetLedger.OriginalPriceDebitAmount = lstDT[i].AmountOriginal;
                FixedAssetLedger.OriginalPriceCreditAmount = 0;
                FixedAssetLedger.DepreciationDebitAmount = lstDT[i].AmountOriginal;
                FixedAssetLedger.DepreciationCreditAmount = 0;
                FixedAssetLedger.Reason = faIncrement.Reason;
                FixedAssetLedger.Description = lstDT[i].Description;
                FixedAssetLedger.IsIrrationalCost = lstDT[i].IsIrrationalCost ?? false;
                if (faIncrement.ExchangeRate == 1)
                {
                    FixedAssetLedger.DepreciationAmount = lstDT[i].OrgPriceOriginal;
                    FixedAssetLedger.OriginalPrice = lstDT[i].OrgPriceOriginal;
                }
                else
                {
                    FixedAssetLedger.DepreciationAmount = lstDT[i].OrgPrice;
                    FixedAssetLedger.OriginalPrice = lstDT[i].OrgPrice;
                }

                FixedAssetLedger.AcDepreciationAmount = fixedAsset.AcDepreciationAmount;
                FixedAssetLedger.IncrementDate = faIncrement.PostedDate;
                FixedAssetLedger.Nos = faIncrement.No;

                FixedAssetLedger.MonthPeriodDepreciationAmount = FixedAssetLedger.UsedTime != 0 ? FixedAssetLedger.DepreciationAmount / FixedAssetLedger.UsedTime : 0;


                listFixAssetLedger.Add(FixedAssetLedger);
            }

            return listFixAssetLedger;
        }

        protected List<FixedAssetLedger> GenFixedAssetLedgers(FADecrement faDecrement)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            List<FADecrementDetail> lstDT = faDecrement.FADecrementDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var fixedAssetId = lstDT[i].FixedAssetID;
                //var fixedAsset = new FixedAsset();
                //if (fixedAssetId != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                //}
                FixedAssetLedger FixedAssetLedger = GetLastFixedAssetLedger(fixedAssetId, faDecrement.PostedDate);
                FixedAssetLedger.ID = Guid.NewGuid();
                FixedAssetLedger.ReferenceID = faDecrement.ID;
                FixedAssetLedger.No = faDecrement.No;
                FixedAssetLedger.TypeID = faDecrement.TypeID;
                FixedAssetLedger.Date = faDecrement.Date;
                FixedAssetLedger.PostedDate = faDecrement.PostedDate;
                FixedAssetLedger.FixedAssetID = lstDT[i].FixedAssetID ?? Guid.Empty;
                FixedAssetLedger.DepartmentID = lstDT[i].DepartmentID ?? Guid.Empty;
                //OriginalPriceAccount = lstDT[i].CreditAccount,
                FixedAssetLedger.OriginalPriceDebitAmount = 0;
                //OriginalPriceCreditAmount = lstDT[i].AmountOriginal,
                //DepreciationAccount = lstDT[i].DebitAccount,
                FixedAssetLedger.DepreciationDebitAmount = 0;
                //DepreciationCreditAmount = lstDT[i].AmountOriginal,
                FixedAssetLedger.Reason = faDecrement.Reason;
                //Description = lstDT[i].Description,
                //IsIrrationalCost = lstDT[i].IsIrrationalCost,
                listFixAssetLedger.Add(FixedAssetLedger);
            }

            return listFixAssetLedger;
        }
        protected List<FixedAssetLedger> GenFixedAssetLedgers(FAAdjustment faAdjustment)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            List<FAAdjustmentDetail> lstDT = faAdjustment.FAAdjustmentDetails.OrderBy(n => n.OrderPriority).ToList();
            if (lstDT.Count > 0)
                for (int i = 0; i < lstDT.Count; i++)
                {
                    var fixedAssetId = faAdjustment.FixedAssetID;
                    //var fixedAsset = new FixedAsset();
                    //if (fixedAssetId != null)
                    //{
                    //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                    //}
                    FixedAssetLedger FixedAssetLedger = GetLastFixedAssetLedger(fixedAssetId, faAdjustment.PostedDate);
                    FixedAssetLedger.ID = Guid.NewGuid();
                    FixedAssetLedger.ReferenceID = faAdjustment.ID;
                    FixedAssetLedger.No = faAdjustment.No;
                    FixedAssetLedger.TypeID = faAdjustment.TypeID;
                    FixedAssetLedger.Date = faAdjustment.Date;
                    FixedAssetLedger.PostedDate = faAdjustment.PostedDate;
                    FixedAssetLedger.FixedAssetID = faAdjustment.FixedAssetID ?? Guid.Empty;
                    FixedAssetLedger.DepartmentID = lstDT[i].DepartmentID ?? FixedAssetLedger.DepartmentID;
                    FixedAssetLedger.OriginalPriceDebitAmount = 0;
                    FixedAssetLedger.OriginalPriceCreditAmount = faAdjustment.NewOrgPrice;
                    FixedAssetLedger.DepreciationDebitAmount = 0;
                    FixedAssetLedger.DepreciationCreditAmount = faAdjustment.NewDepreciationAmount;
                    FixedAssetLedger.Reason = faAdjustment.Reason;
                    FixedAssetLedger.Description = lstDT[i].Description;
                    FixedAssetLedger.IsIrrationalCost = lstDT[i].IsIrrationalCost;
                    FixedAssetLedger.DifferUsedTime = faAdjustment.DifferUsedTime;
                    FixedAssetLedger.MonthPeriodDepreciationAmount = faAdjustment.NewMonthlyDepreciationAmount;
                    FixedAssetLedger.AcDepreciationAmount = faAdjustment.NewAcDepreciationAmount;
                    FixedAssetLedger.RemainingAmount = faAdjustment.NewRemainingAmount;
                    FixedAssetLedger.OriginalPrice = faAdjustment.NewOrgPrice;
                    FixedAssetLedger.DifferOrgPrice = faAdjustment.DifferOrgPrice;
                    FixedAssetLedger.DifferAcDepreciationAmount = faAdjustment.DifferAcDepreciationAmount;
                    FixedAssetLedger.DifferMonthlyDepreciationAmount = faAdjustment.DifferMonthlyDepreciationAmount;
                    FixedAssetLedger.DifferRemainingAmount = faAdjustment.DifferRemainingAmount;
                    FixedAssetLedger.DifferDepreciationAmount = faAdjustment.DifferDepreciationAmount;
                    FixedAssetLedger.DepreciationAmount = faAdjustment.NewDepreciationAmount;
                    FixedAssetLedger.UsedTimeRemain = faAdjustment.IsDisplayByMonth ?? true ? faAdjustment.NewUsedTime : faAdjustment.NewUsedTime * 12;

                    FixedAssetLedger.MonthDepreciationRate = FixedAssetLedger.UsedTimeRemain == 0 ? 0 : Math.Round((1 / FixedAssetLedger.UsedTimeRemain * 100) ?? 0, 2);
                    FixedAssetLedger.DepreciationRate = FixedAssetLedger.MonthDepreciationRate * 12;

                    listFixAssetLedger.Add(FixedAssetLedger);
                }
            else
            {
                var fixedAssetId = faAdjustment.FixedAssetID;
                //var fixedAsset = new FixedAsset();
                //if (fixedAssetId != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                //}
                FixedAssetLedger FixedAssetLedger = GetLastFixedAssetLedger(fixedAssetId, faAdjustment.PostedDate);
                FixedAssetLedger.ID = Guid.NewGuid();
                FixedAssetLedger.ReferenceID = faAdjustment.ID;
                FixedAssetLedger.No = faAdjustment.No;
                FixedAssetLedger.TypeID = faAdjustment.TypeID;
                FixedAssetLedger.Date = faAdjustment.Date;
                FixedAssetLedger.PostedDate = faAdjustment.PostedDate;
                FixedAssetLedger.FixedAssetID = faAdjustment.FixedAssetID ?? Guid.Empty;
                FixedAssetLedger.OriginalPriceDebitAmount = 0;
                FixedAssetLedger.OriginalPriceCreditAmount = faAdjustment.NewOrgPrice;
                FixedAssetLedger.DepreciationDebitAmount = 0;
                FixedAssetLedger.DepreciationCreditAmount = faAdjustment.NewDepreciationAmount;
                FixedAssetLedger.Reason = faAdjustment.Reason;
                FixedAssetLedger.DifferUsedTime = faAdjustment.DifferUsedTime;
                FixedAssetLedger.MonthPeriodDepreciationAmount = faAdjustment.NewMonthlyDepreciationAmount;
                FixedAssetLedger.AcDepreciationAmount = faAdjustment.NewAcDepreciationAmount;
                FixedAssetLedger.RemainingAmount = faAdjustment.NewRemainingAmount;
                FixedAssetLedger.OriginalPrice = faAdjustment.NewOrgPrice;
                FixedAssetLedger.DifferOrgPrice = faAdjustment.DifferOrgPrice;
                FixedAssetLedger.DifferAcDepreciationAmount = faAdjustment.DifferAcDepreciationAmount;
                FixedAssetLedger.DifferMonthlyDepreciationAmount = faAdjustment.DifferMonthlyDepreciationAmount;
                FixedAssetLedger.DifferRemainingAmount = faAdjustment.DifferRemainingAmount;
                FixedAssetLedger.DifferDepreciationAmount = faAdjustment.DifferDepreciationAmount;
                FixedAssetLedger.DepreciationAmount = faAdjustment.NewDepreciationAmount;
                FixedAssetLedger.UsedTimeRemain = faAdjustment.IsDisplayByMonth ?? true ? faAdjustment.NewUsedTime : faAdjustment.NewUsedTime * 12;

                FixedAssetLedger.MonthDepreciationRate = FixedAssetLedger.UsedTimeRemain == 0 ? 0 : Math.Round((1 / FixedAssetLedger.UsedTimeRemain * 100) ?? 0, 2);
                FixedAssetLedger.DepreciationRate = FixedAssetLedger.MonthDepreciationRate * 12;

                listFixAssetLedger.Add(FixedAssetLedger);
            }
            return listFixAssetLedger;
        }

        protected List<FixedAssetLedger> GenFixedAssetLedgers(FADepreciation FADepreciation)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            List<FADepreciationDetail> lstDT = FADepreciation.FADepreciationDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var fixedAssetId = lstDT[i].FixedAssetID;
                //var fixedAsset = new FixedAsset();
                //if (fixedAssetId != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                //}
                FixedAssetLedger FixedAssetLedger = GetLastFixedAssetLedger(fixedAssetId, FADepreciation.PostedDate);
                FixedAssetLedger.ID = Guid.NewGuid();
                FixedAssetLedger.ReferenceID = FADepreciation.ID;
                FixedAssetLedger.No = FADepreciation.No;
                FixedAssetLedger.TypeID = FADepreciation.TypeID;
                FixedAssetLedger.Date = FADepreciation.Date;
                FixedAssetLedger.PostedDate = FADepreciation.PostedDate;
                FixedAssetLedger.FixedAssetID = lstDT[i].FixedAssetID;
                FixedAssetLedger.OriginalPriceDebitAmount = 0;
                FixedAssetLedger.OriginalPriceCreditAmount = lstDT[i].Amount;
                FixedAssetLedger.DepreciationDebitAmount = 0;
                FixedAssetLedger.DepreciationCreditAmount = lstDT[i].Amount;
                FixedAssetLedger.Reason = FADepreciation.Reason;
                FixedAssetLedger.Description = lstDT[i].Description;
                FixedAssetLedger.IsIrrationalCost = lstDT[i].IsIrrationalCost;
                FixedAssetLedger.MonthPeriodDepreciationAmount = lstDT[i].MonthlyDepreciationAmountOriginal;
                FixedAssetLedger.UsedTimeRemain--;
                FixedAssetLedger.AcDepreciationAmount = _IFixedAssetLedgerService.GetTotalAcDepreciationAmount((Guid)fixedAssetId, FADepreciation.PostedDate) + lstDT[i].MonthlyDepreciationAmountOriginal;
                FixedAssetLedger.RemainingAmount = FixedAssetLedger.DepreciationAmount - FixedAssetLedger.AcDepreciationAmount;
                listFixAssetLedger.Add(FixedAssetLedger);
            }

            return listFixAssetLedger;
        }

        protected List<FixedAssetLedger> GenFixedAssetLedgers(FATransfer FATransfer)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            List<FATransferDetail> lstDT = FATransfer.FATransferDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                var fixedAssetId = lstDT[i].FixedAssetID;
                //var fixedAsset = new FixedAsset();
                //if (fixedAssetId != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                //}
                FixedAssetLedger FixedAssetLedger = GetLastFixedAssetLedger(fixedAssetId, FATransfer.Date);
                FixedAssetLedger.ID = Guid.NewGuid();
                FixedAssetLedger.ReferenceID = FATransfer.ID;
                FixedAssetLedger.No = FATransfer.No;
                FixedAssetLedger.TypeID = FATransfer.TypeID;
                FixedAssetLedger.Date = FATransfer.Date;
                FixedAssetLedger.PostedDate = FATransfer.Date;
                FixedAssetLedger.FixedAssetID = lstDT[i].FixedAssetID;
                FixedAssetLedger.DepartmentID = lstDT[i].ToDepartment;
                FixedAssetLedger.OriginalPriceDebitAmount = 0;
                FixedAssetLedger.OriginalPriceCreditAmount = lstDT[i].Amount;
                FixedAssetLedger.DepreciationDebitAmount = 0;
                FixedAssetLedger.DepreciationCreditAmount = lstDT[i].Amount;
                FixedAssetLedger.Reason = FATransfer.Reason;
                FixedAssetLedger.Description = lstDT[i].Description;

                listFixAssetLedger.Add(FixedAssetLedger);
            }

            return listFixAssetLedger;
        }
        #endregion

        #region Tạo chứng từ sổ cái
        //Sổ Cái
        /// <summary>
        /// Tạo chứng từ sổ cái cho Phiếu chi
        /// </summary>
        /// <param name="mcReceipt"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(MCReceipt mcReceipt)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<MCReceiptDetail> lstDT = mcReceipt.MCReceiptDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcReceipt.ID,
                    TypeID = mcReceipt.TypeID,
                    Date = mcReceipt.Date,
                    PostedDate = mcReceipt.PostedDate,
                    No = mcReceipt.No,
                    InvoiceDate = mcReceipt.InvoiceDate,
                    InvoiceNo = mcReceipt.InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = mcReceipt.CurrencyID,
                    ExchangeRate = mcReceipt.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcReceipt.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mcReceipt.AccountingObjectID,
                    EmployeeID = mcReceipt.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = mcReceipt.InvoiceSeries,
                    ContactName = mcReceipt.Payers,
                    DetailID = lstDT[i].ID,
                    RefNo = mcReceipt.No,
                    RefDate = mcReceipt.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if (!(string.IsNullOrEmpty(lstDT[i].CreditAccount)) &&
                    (((lstDT[i].CreditAccount.StartsWith("133"))
                      || lstDT[i].CreditAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mcReceipt.MCReceiptDetailTaxs.Any(x => x.VATAccount == genTemp.AccountCorresponding) ? mcReceipt.MCReceiptDetailTaxs.FirstOrDefault(x => x.VATAccount == genTemp.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcReceipt.ID,
                    TypeID = mcReceipt.TypeID,
                    Date = mcReceipt.Date,
                    PostedDate = mcReceipt.PostedDate,
                    No = mcReceipt.No,
                    InvoiceDate = mcReceipt.InvoiceDate,
                    InvoiceNo = mcReceipt.InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = mcReceipt.CurrencyID,
                    ExchangeRate = mcReceipt.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = mcReceipt.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mcReceipt.AccountingObjectID,
                    EmployeeID = mcReceipt.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = mcReceipt.InvoiceSeries,
                    ContactName = mcReceipt.Payers,
                    DetailID = lstDT[i].ID,
                    RefNo = mcReceipt.No,
                    RefDate = mcReceipt.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(lstDT[i].CreditAccount)) &&
                    (((lstDT[i].CreditAccount.StartsWith("133"))
                      || lstDT[i].CreditAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mcReceipt.MCReceiptDetailTaxs.Any(x => x.VATAccount == genTempCorresponding.Account) ? mcReceipt.MCReceiptDetailTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.Account).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ Sổ cái cho Phiếu Chi
        /// </summary>
        /// <param name="mcPayment"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(MCPayment mcPayment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<MCPaymentDetail> lstDT = mcPayment.MCPaymentDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = lstDT[i].Amount != 0 ? lstDT[i].Amount : Utils.calculateAmountFromOriginal(lstDT[i].AmountOriginal, mcPayment.ExchangeRate),
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcPayment.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = lstDT[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost,
                };
                if (!(string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mcPayment.MCPaymentDetailTaxs.Any(x => x.VATAccount == genTemp.Account) ? mcPayment.MCPaymentDetailTaxs.FirstOrDefault(x => x.VATAccount == genTemp.Account).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount != 0 ? lstDT[i].Amount : Utils.calculateAmountFromOriginal(lstDT[i].AmountOriginal, mcPayment.ExchangeRate),
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = mcPayment.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = lstDT[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost,
                };
                if ((!string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mcPayment.MCPaymentDetailTaxs.Any(x => x.VATAccount == genTempCorresponding.AccountCorresponding) ? mcPayment.MCPaymentDetailTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái cho Nộp tiền vào TK
        /// </summary>
        /// <param name="mbDeposit"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(MBDeposit mbDeposit)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<MBDepositDetail> lstDT = mbDeposit.MBDepositDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbDeposit.ID,
                    TypeID = mbDeposit.TypeID,
                    Date = mbDeposit.Date,
                    PostedDate = mbDeposit.PostedDate,
                    No = mbDeposit.No,
                    InvoiceDate = mbDeposit.InvoiceDate,
                    InvoiceNo = mbDeposit.InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = mbDeposit.BankAccountDetailID,
                    CurrencyID = mbDeposit.CurrencyID,
                    ExchangeRate = mbDeposit.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbDeposit.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = mbDeposit.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = "",
                    DetailID = lstDT[i].ID,
                    RefNo = mbDeposit.No,
                    RefDate = mbDeposit.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mbDeposit.MBDepositDetailTaxs.Any(x => x.VATAccount == genTemp.Account) ? mbDeposit.MBDepositDetailTaxs.FirstOrDefault(x => x.VATAccount == genTemp.Account).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbDeposit.ID,
                    TypeID = mbDeposit.TypeID,
                    Date = mbDeposit.Date,
                    PostedDate = mbDeposit.PostedDate,
                    No = mbDeposit.No,
                    InvoiceDate = mbDeposit.InvoiceDate,
                    InvoiceNo = mbDeposit.InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = mbDeposit.BankAccountDetailID,
                    CurrencyID = mbDeposit.CurrencyID,
                    ExchangeRate = mbDeposit.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = mbDeposit.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = mbDeposit.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = "",
                    DetailID = lstDT[i].ID,
                    RefNo = mbDeposit.No,
                    RefDate = mbDeposit.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mbDeposit.MBDepositDetailTaxs.Any(x => x.VATAccount == genTempCorresponding.AccountCorresponding) ? mbDeposit.MBDepositDetailTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái Ủy nhiệm chi
        /// </summary>
        /// <param name="mbTellerPaper"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(MBTellerPaper mbTellerPaper)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<MBTellerPaperDetail> lstDT = mbTellerPaper.MBTellerPaperDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbTellerPaper.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = lstDT[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if (!(string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mbTellerPaper.MBTellerPaperDetailTaxs.Any(x => x.VATAccount == genTemp.Account) ? mbTellerPaper.MBTellerPaperDetailTaxs.FirstOrDefault(x => x.VATAccount == genTemp.Account).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbTellerPaper.ID,
                    TypeID = mbTellerPaper.TypeID,
                    Date = mbTellerPaper.Date,
                    PostedDate = mbTellerPaper.PostedDate,
                    No = mbTellerPaper.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = mbTellerPaper.BankAccountDetailID,
                    CurrencyID = mbTellerPaper.CurrencyID,
                    ExchangeRate = mbTellerPaper.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = mbTellerPaper.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mbTellerPaper.AccountingObjectID,
                    EmployeeID = mbTellerPaper.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mbTellerPaper.Receiver,
                    DetailID = lstDT[i].ID,
                    RefNo = mbTellerPaper.No,
                    RefDate = mbTellerPaper.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mbTellerPaper.MBTellerPaperDetailTaxs.Any(x => x.VATAccount == genTempCorresponding.AccountCorresponding) ? mbTellerPaper.MBTellerPaperDetailTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái Thẻ tín dụng
        /// </summary>
        /// <param name="mbCreditCard"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(MBCreditCard mbCreditCard)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<MBCreditCardDetail> lstDT = mbCreditCard.MBCreditCardDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbCreditCard.ID,
                    TypeID = mbCreditCard.TypeID,
                    Date = mbCreditCard.Date,
                    PostedDate = mbCreditCard.PostedDate,
                    No = mbCreditCard.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = null,
                    CurrencyID = mbCreditCard.CurrencyID,
                    ExchangeRate = mbCreditCard.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbCreditCard.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mbCreditCard.AccountingObjectID,
                    EmployeeID = mbCreditCard.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = "",
                    DetailID = lstDT[i].ID,
                    RefNo = mbCreditCard.No,
                    RefDate = mbCreditCard.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if (!(string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mbCreditCard.MBCreditCardDetailTaxs.Any(x => x.VATAccount == genTemp.Account) ? mbCreditCard.MBCreditCardDetailTaxs.FirstOrDefault(x => x.VATAccount == genTemp.Account).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbCreditCard.ID,
                    TypeID = mbCreditCard.TypeID,
                    Date = mbCreditCard.Date,
                    PostedDate = mbCreditCard.PostedDate,
                    No = mbCreditCard.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = null,
                    CurrencyID = mbCreditCard.CurrencyID,
                    ExchangeRate = mbCreditCard.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = mbCreditCard.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? mbCreditCard.AccountingObjectID,
                    EmployeeID = mbCreditCard.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = "",
                    DetailID = lstDT[i].ID,
                    RefNo = mbCreditCard.No,
                    RefDate = mbCreditCard.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mbCreditCard.MBCreditCardDetailTaxs.Any(x => x.VATAccount == genTempCorresponding.AccountCorresponding) ? mbCreditCard.MBCreditCardDetailTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái Chuyển tiền nội bộ
        /// </summary>
        /// <param name="mbInternalTransfer"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(MBInternalTransfer mbInternalTransfer)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<MBInternalTransferDetail> lstDT = mbInternalTransfer.MBInternalTransferDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbInternalTransfer.ID,
                    TypeID = mbInternalTransfer.TypeID,
                    Date = mbInternalTransfer.Date,
                    PostedDate = mbInternalTransfer.PostedDate,
                    No = mbInternalTransfer.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = lstDT[i].ToBankAccountDetailID,
                    CurrencyID = /*lstDT[i].DebitAccount == "1121" ? "VND" : lstDT[i]*/mbInternalTransfer.CurrencyID,
                    ExchangeRate = /*lstDT[i]*/mbInternalTransfer.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mbInternalTransfer.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = "",
                    DetailID = lstDT[i].ID,
                    RefNo = mbInternalTransfer.No,
                    RefDate = mbInternalTransfer.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if (!(string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mbInternalTransfer.MBInternalTransferTaxs.Any(x => x.VATAccount == genTemp.Account) ? mbInternalTransfer.MBInternalTransferTaxs.FirstOrDefault(x => x.VATAccount == genTemp.Account).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mbInternalTransfer.ID,
                    TypeID = mbInternalTransfer.TypeID,
                    Date = mbInternalTransfer.Date,
                    PostedDate = mbInternalTransfer.PostedDate,
                    No = mbInternalTransfer.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = lstDT[i].FromBankAccountDetailID,
                    CurrencyID = /*lstDT[i].CreditAccount == "1121" ? "VND" : lstDT[i]*/mbInternalTransfer.CurrencyID,
                    ExchangeRate = /*lstDT[i]*/mbInternalTransfer.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = mbInternalTransfer.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = "",
                    DetailID = lstDT[i].ID,
                    RefNo = mbInternalTransfer.No,
                    RefDate = mbInternalTransfer.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(lstDT[i].DebitAccount)) &&
                    (((lstDT[i].DebitAccount.StartsWith("133"))
                      || lstDT[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mbInternalTransfer.MBInternalTransferTaxs.Any(x => x.VATAccount == genTempCorresponding.AccountCorresponding) ? mbInternalTransfer.MBInternalTransferTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái Mua hàng
        /// </summary>
        /// <param name="ppInvoice"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(PPInvoice ppInvoice)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = _ICurrencyService.Getbykey(ppInvoice.CurrencyID);
            List<int> listCashType = new List<int>() { 116, 117, 119, 260, 500 };
            List<int> listNoPayType = new List<int>() { 210, 240 };
            string currencyAccount = (listCashType.Contains(ppInvoice.TypeID)) ? iCurrency.CashAccount : (listNoPayType.Contains(ppInvoice.TypeID) ? null : iCurrency.BankAccount);
            List<PPInvoiceDetail> lstDT = ppInvoice.PPInvoiceDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ-Có
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppInvoice.ID,
                    TypeID = ppInvoice.TypeID,
                    Date = ppInvoice.Date,
                    PostedDate = ppInvoice.PostedDate,
                    No = ppInvoice.No,
                    InvoiceDate = lstDT[i].InvoiceDate,
                    InvoiceNo = lstDT[i].InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = ppInvoice.BankAccountDetailID,
                    CurrencyID = ppInvoice.CurrencyID,
                    ExchangeRate = ppInvoice.ExchangeRate,
                    DebitAmount = lstDT[i].Amount - lstDT[i].DiscountAmount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal - lstDT[i].DiscountAmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = ppInvoice.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                    EmployeeID = ppInvoice.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = lstDT[i].InvoiceSeries,
                    ContactName = ppInvoice.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = ppInvoice.No,
                    RefDate = ppInvoice.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost,
                    VATDescription = lstDT[i].VATDescription

                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppInvoice.ID,
                    TypeID = ppInvoice.TypeID,
                    Date = ppInvoice.Date,
                    PostedDate = ppInvoice.PostedDate,
                    No = ppInvoice.No,
                    InvoiceDate = lstDT[i].InvoiceDate,
                    InvoiceNo = lstDT[i].InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = ppInvoice.BankAccountDetailID,
                    CurrencyID = ppInvoice.CurrencyID,
                    ExchangeRate = ppInvoice.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount - lstDT[i].DiscountAmount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal - lstDT[i].DiscountAmountOriginal,
                    Reason = ppInvoice.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                    EmployeeID = ppInvoice.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = lstDT[i].InvoiceSeries,
                    ContactName = ppInvoice.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = ppInvoice.No,
                    RefDate = ppInvoice.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost,
                    VATDescription = lstDT[i].VATDescription
                };
                listGenTemp.Add(genTempCorresponding);


                #endregion

                if (ppInvoice.IsImportPurchase == true)
                {
                    if (lstDT[i].VATAmount != 0)
                    {
                        #region Cặp Nợ - Thuế GTGT

                        GeneralLedger genTempVAT = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = ppInvoice.ID,
                            TypeID = ppInvoice.TypeID,
                            Date = ppInvoice.Date,
                            PostedDate = ppInvoice.PostedDate,
                            No = ppInvoice.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            //Account = lstDT[i].DebitAccount,
                            //AccountCorresponding = lstDT[i].VATAccount,
                            Account = lstDT[i].DeductionDebitAccount,
                            AccountCorresponding = lstDT[i].VATAccount,
                            BankAccountDetailID = ppInvoice.BankAccountDetailID,
                            CurrencyID = ppInvoice.CurrencyID,
                            ExchangeRate = ppInvoice.ExchangeRate,
                            DebitAmount = (decimal)lstDT[i].VATAmount,
                            DebitAmountOriginal = (decimal)lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = ppInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                            EmployeeID = ppInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            //StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = lstDT[i].InvoiceSeries,
                            ContactName = ppInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = ppInvoice.No,
                            RefDate = ppInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost
                        };

                        listGenTemp.Add(genTempVAT);
                        GeneralLedger genTempVATCorresponding = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = ppInvoice.ID,
                            TypeID = ppInvoice.TypeID,
                            Date = ppInvoice.Date,
                            PostedDate = ppInvoice.PostedDate,
                            No = ppInvoice.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            //Account = lstDT[i].VATAccount,
                            //AccountCorresponding = lstDT[i].DebitAccount,
                            Account = lstDT[i].VATAccount,
                            AccountCorresponding = lstDT[i].DeductionDebitAccount,
                            BankAccountDetailID = ppInvoice.BankAccountDetailID,
                            CurrencyID = ppInvoice.CurrencyID,
                            ExchangeRate = ppInvoice.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = (decimal)lstDT[i].VATAmount,
                            CreditAmountOriginal = (decimal)lstDT[i].VATAmountOriginal,
                            Reason = ppInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                            EmployeeID = ppInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            //StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = lstDT[i].InvoiceSeries,
                            ContactName = ppInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = ppInvoice.No,
                            RefDate = ppInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost,
                            VATDescription = lstDT[i].VATDescription
                        };

                        listGenTemp.Add(genTempVATCorresponding);

                        #endregion
                    }
                    #region Cặp Nợ - Thuế Nhập khẩu: Nếu chọn Mua hàng = Nhập khẩu

                    GeneralLedger genTempVATImportPurchase = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppInvoice.ID,
                        TypeID = ppInvoice.TypeID,
                        Date = ppInvoice.Date,
                        PostedDate = ppInvoice.PostedDate,
                        No = ppInvoice.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        //Account = lstDT[i].DebitAccount,
                        //AccountCorresponding = lstDT[i].ImportTaxAccount,
                        Account = currencyAccount ?? lstDT[i].DebitAccount,
                        AccountCorresponding = lstDT[i].ImportTaxAccount,
                        BankAccountDetailID = ppInvoice.BankAccountDetailID,
                        CurrencyID = ppInvoice.CurrencyID,
                        ExchangeRate = ppInvoice.ExchangeRate,
                        DebitAmount = (decimal)lstDT[i].ImportTaxAmount,
                        DebitAmountOriginal = (decimal)lstDT[i].ImportTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = ppInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                        EmployeeID = ppInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        //StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = ppInvoice.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppInvoice.No,
                        RefDate = ppInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATImportPurchase);
                    GeneralLedger genTempVATCorrespondingImportPurchase = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppInvoice.ID,
                        TypeID = ppInvoice.TypeID,
                        Date = ppInvoice.Date,
                        PostedDate = ppInvoice.PostedDate,
                        No = ppInvoice.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        //Account = lstDT[i].ImportTaxAccount,
                        //AccountCorresponding = lstDT[i].DebitAccount,
                        Account = lstDT[i].ImportTaxAccount,
                        AccountCorresponding = currencyAccount ?? lstDT[i].DebitAccount,
                        BankAccountDetailID = ppInvoice.BankAccountDetailID,
                        CurrencyID = ppInvoice.CurrencyID,
                        ExchangeRate = ppInvoice.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)lstDT[i].ImportTaxAmount,
                        CreditAmountOriginal = (decimal)lstDT[i].ImportTaxAmountOriginal,
                        Reason = ppInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                        EmployeeID = ppInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        //StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = ppInvoice.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppInvoice.No,
                        RefDate = ppInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription
                    };
                    listGenTemp.Add(genTempVATCorrespondingImportPurchase);
                    #endregion
                }
                else
                {
                    if (lstDT[i].VATAmount != 0)
                    {
                        #region Cặp Nợ - Thuế GTGT

                        GeneralLedger genTempVAT = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = ppInvoice.ID,
                            TypeID = ppInvoice.TypeID,
                            Date = ppInvoice.Date,
                            PostedDate = ppInvoice.PostedDate,
                            No = ppInvoice.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            Account = lstDT[i].VATAccount,
                            AccountCorresponding = lstDT[i].DeductionDebitAccount,
                            BankAccountDetailID = ppInvoice.BankAccountDetailID,
                            CurrencyID = ppInvoice.CurrencyID,
                            ExchangeRate = ppInvoice.ExchangeRate,
                            DebitAmount = (decimal)lstDT[i].VATAmount,
                            DebitAmountOriginal = (decimal)lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = ppInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                            EmployeeID = ppInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            //StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = lstDT[i].InvoiceSeries,
                            ContactName = ppInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = ppInvoice.No,
                            RefDate = ppInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost
                        };

                        listGenTemp.Add(genTempVAT);
                        GeneralLedger genTempVATCorresponding = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = ppInvoice.ID,
                            TypeID = ppInvoice.TypeID,
                            Date = ppInvoice.Date,
                            PostedDate = ppInvoice.PostedDate,
                            No = ppInvoice.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            Account = lstDT[i].DeductionDebitAccount,
                            AccountCorresponding = lstDT[i].VATAccount,
                            BankAccountDetailID = ppInvoice.BankAccountDetailID,
                            CurrencyID = ppInvoice.CurrencyID,
                            ExchangeRate = ppInvoice.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = (decimal)lstDT[i].VATAmount,
                            CreditAmountOriginal = (decimal)lstDT[i].VATAmountOriginal,
                            Reason = ppInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                            EmployeeID = ppInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            //StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = lstDT[i].InvoiceSeries,
                            ContactName = ppInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = ppInvoice.No,
                            RefDate = ppInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost,
                            VATDescription = lstDT[i].VATDescription
                        };

                        listGenTemp.Add(genTempVATCorresponding);

                        #endregion
                    }
                }

                if (lstDT[i].SpecialConsumeTaxAmount != 0)
                {
                    #region Cặp Nợ - Thuế tiêu thụ đặc biệt: Nếu hàng hóa có thuế TTĐB

                    GeneralLedger genTempVAT = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppInvoice.ID,
                        TypeID = ppInvoice.TypeID,
                        Date = ppInvoice.Date,
                        PostedDate = ppInvoice.PostedDate,
                        No = ppInvoice.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        //Account = lstDT[i].DebitAccount,
                        //AccountCorresponding = lstDT[i].SpecialConsumeTaxAccount,
                        Account = currencyAccount ?? lstDT[i].DebitAccount,
                        AccountCorresponding = lstDT[i].SpecialConsumeTaxAccount,
                        BankAccountDetailID = ppInvoice.BankAccountDetailID,
                        CurrencyID = ppInvoice.CurrencyID,
                        ExchangeRate = ppInvoice.ExchangeRate,
                        DebitAmount = (decimal)lstDT[i].SpecialConsumeTaxAmount,
                        DebitAmountOriginal = (decimal)lstDT[i].SpecialConsumeTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = ppInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                        EmployeeID = ppInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = ppInvoice.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppInvoice.No,
                        RefDate = ppInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVAT);

                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppInvoice.ID,
                        TypeID = ppInvoice.TypeID,
                        Date = ppInvoice.Date,
                        PostedDate = ppInvoice.PostedDate,
                        No = ppInvoice.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        //Account = lstDT[i].SpecialConsumeTaxAccount,
                        //AccountCorresponding = lstDT[i].DebitAccount,
                        Account = lstDT[i].SpecialConsumeTaxAccount,
                        AccountCorresponding = currencyAccount ?? lstDT[i].DebitAccount,
                        BankAccountDetailID = ppInvoice.BankAccountDetailID,
                        CurrencyID = ppInvoice.CurrencyID,
                        ExchangeRate = ppInvoice.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)lstDT[i].SpecialConsumeTaxAmount,
                        CreditAmountOriginal = (decimal)lstDT[i].SpecialConsumeTaxAmountOriginal,
                        Reason = ppInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppInvoice.AccountingObjectID,
                        EmployeeID = ppInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = ppInvoice.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppInvoice.No,
                        RefDate = ppInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription,
                    };
                    listGenTemp.Add(genTempVATCorresponding);
                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ lưu sổ cái  Hàng Trả Lại Giảm giá
        /// </summary>
        /// <param name="ppDiscountReturn">Đối tượng PPDiscountReturn tạm</param>
        public List<GeneralLedger> GenGeneralLedgers(PPDiscountReturn ppDiscountReturn)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = _ICurrencyService.Getbykey(ppDiscountReturn.CurrencyID);
            //string currencyAccount = (ppDiscountReturn.BankAccountDetailID == null && ppDiscountReturn.CreditCardNumber == null) ? iCurrency.CashAccount : iCurrency.BankAccount;
            List<PPDiscountReturnDetail> lstDT = ppDiscountReturn.PPDiscountReturnDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ - Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppDiscountReturn.ID,
                    TypeID = ppDiscountReturn.TypeID,
                    Date = ppDiscountReturn.Date,
                    PostedDate = ppDiscountReturn.PostedDate,
                    No = ppDiscountReturn.No,
                    InvoiceDate = ppDiscountReturn.InvoiceDate,
                    InvoiceNo = ppDiscountReturn.InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = ppDiscountReturn.CurrencyID,
                    ExchangeRate = ppDiscountReturn.ExchangeRate,
                    DebitAmount = lstDT[i].Amount ?? 0,
                    DebitAmountOriginal = lstDT[i].AmountOriginal ?? 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = ppDiscountReturn.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? ppDiscountReturn.AccountingObjectID,
                    EmployeeID = ppDiscountReturn.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = ppDiscountReturn.InvoiceSeries,
                    ContactName = ppDiscountReturn.OContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = ppDiscountReturn.No,
                    RefDate = ppDiscountReturn.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if (((lstDT[i].CreditAccount.StartsWith("133")) ||
                     lstDT[i].CreditAccount.StartsWith("33311")))
                {
                    genTemp.VATDescription = lstDT.Count > i ? lstDT[i].VATDescription : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppDiscountReturn.ID,
                    TypeID = ppDiscountReturn.TypeID,
                    Date = ppDiscountReturn.Date,
                    PostedDate = ppDiscountReturn.PostedDate,
                    No = ppDiscountReturn.No,
                    InvoiceDate = ppDiscountReturn.InvoiceDate,
                    InvoiceNo = ppDiscountReturn.InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = ppDiscountReturn.CurrencyID,
                    ExchangeRate = ppDiscountReturn.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount ?? 0,
                    CreditAmountOriginal = lstDT[i].AmountOriginal ?? 0,
                    Reason = ppDiscountReturn.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? ppDiscountReturn.AccountingObjectID,
                    EmployeeID = ppDiscountReturn.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = ppDiscountReturn.InvoiceSeries,
                    ContactName = ppDiscountReturn.OContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = ppDiscountReturn.No,
                    RefDate = ppDiscountReturn.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if (((lstDT[i].CreditAccount.StartsWith("133")) ||
                     lstDT[i].CreditAccount.StartsWith("33311")))
                {
                    genTempCorresponding.VATDescription = lstDT.Count > i ? lstDT[i].VATDescription : null;
                }
                listGenTemp.Add(genTempCorresponding);

                #endregion
                if (lstDT[i].VATAmount != 0)
                {
                    #region Cặp Nợ - Thuế GTGT
                    //string currencyAccount = (lstDT[i].BankAccountDetailID == null) ? iCurrency.CashAccount : iCurrency.BankAccount;

                    GeneralLedger genTempVAT = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppDiscountReturn.ID,
                        TypeID = ppDiscountReturn.TypeID,
                        Date = ppDiscountReturn.Date,
                        PostedDate = ppDiscountReturn.PostedDate,
                        No = ppDiscountReturn.No,
                        InvoiceDate = ppDiscountReturn.InvoiceDate,
                        InvoiceNo = ppDiscountReturn.InvoiceNo,
                        Account = lstDT[i].DebitAccount,
                        AccountCorresponding = lstDT[i].VATAccount,
                        //Account = lstDT[i].VATAccount,
                        //AccountCorresponding = currencyAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = ppDiscountReturn.CurrencyID,
                        ExchangeRate = ppDiscountReturn.ExchangeRate,
                        DebitAmount = lstDT[i].VATAmount ?? 0,
                        DebitAmountOriginal = lstDT[i].VATAmountOriginal ?? 0,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = ppDiscountReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppDiscountReturn.AccountingObjectID,
                        EmployeeID = ppDiscountReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = ppDiscountReturn.InvoiceSeries,
                        ContactName = ppDiscountReturn.OContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppDiscountReturn.No,
                        RefDate = ppDiscountReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        //VATDescription = lstDT[i].VATDescription,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = false
                    };
                    if (((lstDT[i].CreditAccount.StartsWith("133")) ||
                         lstDT[i].CreditAccount.StartsWith("33311")))
                    {
                        genTempVAT.VATDescription = lstDT[i].VATDescription;
                    }
                    listGenTemp.Add(genTempVAT);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppDiscountReturn.ID,
                        TypeID = ppDiscountReturn.TypeID,
                        Date = ppDiscountReturn.Date,
                        PostedDate = ppDiscountReturn.PostedDate,
                        No = ppDiscountReturn.No,
                        InvoiceDate = ppDiscountReturn.InvoiceDate,
                        InvoiceNo = ppDiscountReturn.InvoiceNo,
                        Account = lstDT[i].VATAccount,
                        AccountCorresponding = lstDT[i].DebitAccount,
                        //Account = currencyAccount,
                        //AccountCorresponding = lstDT[i].VATAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = ppDiscountReturn.CurrencyID,
                        ExchangeRate = ppDiscountReturn.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = lstDT[i].VATAmount ?? 0,
                        CreditAmountOriginal = lstDT[i].VATAmountOriginal ?? 0,
                        Reason = ppDiscountReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppDiscountReturn.AccountingObjectID,
                        EmployeeID = ppDiscountReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = ppDiscountReturn.InvoiceSeries,
                        ContactName = ppDiscountReturn.OContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppDiscountReturn.No,
                        RefDate = ppDiscountReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        //VATDescription = lstDT[i].VATDescription,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = false
                    };
                    if (((lstDT[i].CreditAccount.StartsWith("133")) ||
                         lstDT[i].CreditAccount.StartsWith("33311")))
                    {
                        genTempVATCorresponding.VATDescription = lstDT[i].VATDescription;
                    }
                    listGenTemp.Add(genTempVATCorresponding);

                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ ghi sổ mua dịch vụ
        /// </summary>
        /// <param name="ppService"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(PPService ppService)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = _ICurrencyService.Getbykey(ppService.CurrencyID);
            List<int> dsType = new List<int>() { 126, 133, 143, 173 };

            string currencyAccount = (!dsType.Contains(ppService.TypeID)) ? iCurrency.CashAccount : iCurrency.BankAccount;
            List<PPServiceDetail> lstDT = ppService.PPServiceDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ - Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppService.ID,
                    TypeID = ppService.TypeID,
                    Date = ppService.Date,
                    PostedDate = ppService.PostedDate,
                    No = ppService.No,
                    InvoiceDate = lstDT[i].InvoiceDate,
                    InvoiceNo = lstDT[i].InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = ppService.BankAccountDetailID,
                    CurrencyID = ppService.CurrencyID,
                    ExchangeRate = ppService.ExchangeRate,
                    DebitAmount = ((lstDT[i].Amount ?? 0) - (lstDT[i].DiscountAmount ?? 0)),
                    DebitAmountOriginal = ((lstDT[i].AmountOriginal ?? 0) - (lstDT[i].DiscountAmountOriginal ?? 0)),
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = ppService.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? ppService.AccountingObjectID,
                    EmployeeID = ppService.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = lstDT[i].InvoiceSeries,
                    ContactName = ppService.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = ppService.No,
                    RefDate = ppService.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };

                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = ppService.ID,
                    TypeID = ppService.TypeID,
                    Date = ppService.Date,
                    PostedDate = ppService.PostedDate,
                    No = ppService.No,
                    InvoiceDate = lstDT[i].InvoiceDate,
                    InvoiceNo = lstDT[i].InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = ppService.BankAccountDetailID,
                    CurrencyID = ppService.CurrencyID,
                    ExchangeRate = ppService.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = ((lstDT[i].Amount ?? 0) - (lstDT[i].DiscountAmount ?? 0)),
                    CreditAmountOriginal = ((lstDT[i].AmountOriginal ?? 0) - (lstDT[i].DiscountAmountOriginal ?? 0)),
                    Reason = ppService.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? ppService.AccountingObjectID,
                    EmployeeID = ppService.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = lstDT[i].InvoiceSeries,
                    ContactName = ppService.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = ppService.No,
                    RefDate = ppService.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };

                listGenTemp.Add(genTempCorresponding);

                #endregion
                if (lstDT[i].VATAmount != 0 || lstDT[i].VATAmountOriginal != 0)
                {
                    #region Cặp Nợ - Thuế GTGT

                    GeneralLedger genTempVAT = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppService.ID,
                        TypeID = ppService.TypeID,
                        Date = ppService.Date,
                        PostedDate = ppService.PostedDate,
                        No = ppService.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        //Account = lstDT[i].DebitAccount,
                        //AccountCorresponding = lstDT[i].VATAccount,
                        Account = lstDT[i].VATAccount,
                        AccountCorresponding = lstDT[i].CreditAccount,
                        BankAccountDetailID = ppService.BankAccountDetailID,
                        CurrencyID = ppService.CurrencyID,
                        ExchangeRate = ppService.ExchangeRate,
                        DebitAmount = Utils.calculateAmountFromOriginal(lstDT[i].VATAmountOriginal ?? 0, ppService.ExchangeRate),
                        DebitAmountOriginal = lstDT[i].VATAmountOriginal ?? 0,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = ppService.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppService.AccountingObjectID,
                        EmployeeID = ppService.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = lstDT[i].InvoiceSeries,
                        ContactName = ppService.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppService.No,
                        RefDate = ppService.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVAT);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = ppService.ID,
                        TypeID = ppService.TypeID,
                        Date = ppService.Date,
                        PostedDate = ppService.PostedDate,
                        No = ppService.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        //Account = lstDT[i].VATAccount,
                        //AccountCorresponding = lstDT[i].DebitAccount,
                        Account = lstDT[i].CreditAccount,
                        AccountCorresponding = lstDT[i].VATAccount,
                        BankAccountDetailID = ppService.BankAccountDetailID,
                        CurrencyID = ppService.CurrencyID,
                        ExchangeRate = ppService.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = Utils.calculateAmountFromOriginal(lstDT[i].VATAmountOriginal ?? 0, ppService.ExchangeRate),
                        CreditAmountOriginal = lstDT[i].VATAmountOriginal ?? 0,
                        Reason = ppService.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? ppService.AccountingObjectID,
                        EmployeeID = ppService.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = lstDT[i].InvoiceSeries,
                        ContactName = ppService.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = ppService.No,
                        RefDate = ppService.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription
                    };
                    listGenTemp.Add(genTempVATCorresponding);
                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ sổ cái  Điều chỉnh tồn kho
        /// [Huy Anh]
        /// </summary>
        /// <param name="rsInwardOutward">Đối tượng RSInwardOutward</param>
        public List<GeneralLedger> GenGeneralLedgers(RSInwardOutward rsInwardOutward)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            List<RSInwardOutwardDetail> lstDT = rsInwardOutward.RSInwardOutwardDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ - Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = rsInwardOutward.ID,
                    TypeID = rsInwardOutward.TypeID,
                    Date = rsInwardOutward.Date,
                    PostedDate = rsInwardOutward.PostedDate,
                    No = rsInwardOutward.No,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    CurrencyID = rsInwardOutward.CurrencyID,
                    ExchangeRate = rsInwardOutward.ExchangeRate,
                    DebitAmount = (decimal)lstDT[i].Amount,
                    DebitAmountOriginal = (decimal)lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = rsInwardOutward.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? rsInwardOutward.AccountingObjectID,
                    EmployeeID = rsInwardOutward.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    DetailID = lstDT[i].ID,
                    RefNo = rsInwardOutward.No,
                    RefDate = rsInwardOutward.RefDateTime,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if ((!string.IsNullOrEmpty(lstDT[i].CreditAccount) &&
                    ((lstDT[i].CreditAccount.StartsWith("133")) ||
                    lstDT[i].CreditAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = lstDT.Count > i ? lstDT[i].Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = rsInwardOutward.ID,
                    TypeID = rsInwardOutward.TypeID,
                    Date = rsInwardOutward.Date,
                    PostedDate = rsInwardOutward.PostedDate,
                    No = rsInwardOutward.No,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    CurrencyID = rsInwardOutward.CurrencyID,
                    ExchangeRate = rsInwardOutward.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = (decimal)lstDT[i].Amount,
                    CreditAmountOriginal = (decimal)lstDT[i].AmountOriginal,
                    Reason = rsInwardOutward.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? rsInwardOutward.AccountingObjectID,
                    EmployeeID = rsInwardOutward.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    DetailID = lstDT[i].ID,
                    RefNo = rsInwardOutward.No,
                    RefDate = rsInwardOutward.RefDateTime,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if ((!string.IsNullOrEmpty(lstDT[i].CreditAccount) &&
                    ((lstDT[i].CreditAccount.StartsWith("133")) ||
                     lstDT[i].CreditAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = lstDT.Count > i ? lstDT[i].Description : null;
                }
                listGenTemp.Add(genTempCorresponding);

                #endregion
            }
            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ sổ cái Chuyển kho
        /// </summary>
        /// <param name="rsTransfer">Đối tượng RSTransfer tạm</param>
        public List<GeneralLedger> GenGeneralLedgers(RSTransfer rsTransfer)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            List<RSTransferDetail> lstDT = rsTransfer.RSTransferDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ - Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = rsTransfer.ID,
                    TypeID = rsTransfer.TypeID,
                    Date = rsTransfer.Date,
                    PostedDate = rsTransfer.PostedDate,
                    No = rsTransfer.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = null,
                    CurrencyID = rsTransfer.CurrencyID,
                    ExchangeRate = rsTransfer.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = rsTransfer.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = rsTransfer.InvSeries,
                    ContactName = rsTransfer.IWResponsitoryKeeper,
                    DetailID = lstDT[i].ID,
                    RefNo = rsTransfer.No,
                    RefDate = rsTransfer.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = false
                };

                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = rsTransfer.ID,
                    TypeID = rsTransfer.TypeID,
                    Date = rsTransfer.Date,
                    PostedDate = rsTransfer.PostedDate,
                    No = rsTransfer.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = null,
                    CurrencyID = rsTransfer.CurrencyID,
                    ExchangeRate = rsTransfer.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = rsTransfer.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = rsTransfer.InvSeries,
                    ContactName = rsTransfer.OWResponsitoryKeeper,
                    DetailID = lstDT[i].ID,
                    RefNo = rsTransfer.No,
                    RefDate = rsTransfer.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = false
                };
                listGenTemp.Add(genTempCorresponding);

                #endregion
            }
            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ sổ cái Lắp ráp, tháo dỡ
        /// </summary>
        /// <param name="rsAssemblyDismantlement">Đối tượng RSAssemblyDismantlement tạm</param>
        //public List<GeneralLedger> GenGeneralLedgers(RSAssemblyDismantlement rsAssemblyDismantlement)
        //{
        //    List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
        //    for (int i = 0; i < rsAssemblyDismantlement.RSAssemblyDismantlementDetails.Count; i++)
        //    {
        #region Cặp Nợ - Có của chứng từ nhập kho cho kho chuyển đến

        //        GeneralLedger genTemp = new GeneralLedger
        //        {
        //            ID = Guid.NewGuid(),
        //            BranchID = null,
        //            ReferenceID = rsAssemblyDismantlement.ID,
        //            TypeID = rsAssemblyDismantlement.TypeID,
        //            Date = rsAssemblyDismantlement.IWDate,
        //            PostedDate = rsAssemblyDismantlement.IWPostedDate,
        //            No = rsAssemblyDismantlement.IWNo,
        //            InvoiceDate = null,
        //            InvoiceNo = null,
        //            Account = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DebitAccount,
        //            AccountCorresponding = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CreditAccount,
        //            BankAccountDetailID = null,
        //            CurrencyID = rsAssemblyDismantlement.CurrencyID,
        //            ExchangeRate = rsAssemblyDismantlement.ExchangeRate,
        //            DebitAmount = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Amount,
        //            DebitAmountOriginal = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].AmountOriginal,
        //            CreditAmount = 0,
        //            CreditAmountOriginal = 0,
        //            Reason = rsAssemblyDismantlement.IWReason,
        //            Description = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Description,
        //            AccountingObjectID = rsAssemblyDismantlement.IWAccountingObjectID,
        //            //EmployeeID = rsAssemblyDismantlement.RSAssemblyDismantlements[i].EmployeeID,
        //            BudgetItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].BudgetItemID,
        //            CostSetID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CostSetID,
        //            ContractID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ContractID,
        //            StatisticsCodeID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].StatisticsCodeID,
        //            InvoiceSeries = null,
        //            ContactName = rsAssemblyDismantlement.IWAccountingObjectName,
        //            DetailID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ID,
        //            RefNo = rsAssemblyDismantlement.IWNo,
        //            RefDate = rsAssemblyDismantlement.OWDate,
        //            DepartmentID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DepartmentID,
        //            ExpenseItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ExpenseItemID,
        //            VATDescription = null,
        //            IsIrrationalCost = false
        //        };

        //        listGenTemp.Add(genTemp);
        //        GeneralLedger genTempCorresponding = new GeneralLedger
        //        {
        //            ID = Guid.NewGuid(),
        //            BranchID = null,
        //            ReferenceID = rsAssemblyDismantlement.ID,
        //            TypeID = rsAssemblyDismantlement.TypeID,
        //            Date = rsAssemblyDismantlement.IWDate,
        //            PostedDate = rsAssemblyDismantlement.IWPostedDate,
        //            No = rsAssemblyDismantlement.IWNo,
        //            InvoiceDate = null,
        //            InvoiceNo = null,
        //            Account = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CreditAccount,
        //            AccountCorresponding = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DebitAccount,
        //            BankAccountDetailID = null,
        //            CurrencyID = rsAssemblyDismantlement.CurrencyID,
        //            ExchangeRate = rsAssemblyDismantlement.ExchangeRate,
        //            DebitAmount = 0,
        //            DebitAmountOriginal = 0,
        //            CreditAmount = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Amount,
        //            CreditAmountOriginal = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].AmountOriginal,
        //            Reason = rsAssemblyDismantlement.IWReason,
        //            Description = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Description,
        //            AccountingObjectID = rsAssemblyDismantlement.IWAccountingObjectID,
        //            //EmployeeID = rsAssemblyDismantlement.RSAssemblyDismantlements[i].EmployeeID,
        //            BudgetItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].BudgetItemID,
        //            CostSetID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CostSetID,
        //            ContractID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ContractID,
        //            StatisticsCodeID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].StatisticsCodeID,
        //            InvoiceSeries = null,
        //            ContactName = rsAssemblyDismantlement.IWAccountingObjectName,
        //            DetailID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ID,
        //            RefNo = rsAssemblyDismantlement.IWNo,
        //            RefDate = rsAssemblyDismantlement.IWDate,
        //            DepartmentID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DepartmentID,
        //            ExpenseItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ExpenseItemID,
        //            VATDescription = null,
        //            IsIrrationalCost = false
        //        };
        //        listGenTemp.Add(genTempCorresponding);

        #endregion
        //    }
        //    for (int i = 0; i < rsAssemblyDismantlement.RSAssemblyDismantlementDetails.Count; i++)
        //    {
        #region Cặp Nợ - Có của chứng từ xuất kho cho kho chuyển đi

        //        GeneralLedger genTempCK = new GeneralLedger
        //        {
        //            ID = Guid.NewGuid(),
        //            BranchID = null,
        //            ReferenceID = rsAssemblyDismantlement.ID,
        //            TypeID = rsAssemblyDismantlement.TypeID,
        //            Date = rsAssemblyDismantlement.OWDate,
        //            PostedDate = rsAssemblyDismantlement.OWPostedDate,
        //            No = rsAssemblyDismantlement.OWNo,
        //            InvoiceDate = null,
        //            InvoiceNo = null,
        //            Account = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DebitAccount,
        //            AccountCorresponding = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CreditAccount,
        //            BankAccountDetailID = null,
        //            CurrencyID = rsAssemblyDismantlement.CurrencyID,
        //            ExchangeRate = rsAssemblyDismantlement.ExchangeRate,
        //            DebitAmount = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Amount,
        //            DebitAmountOriginal = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].AmountOriginal,
        //            CreditAmount = 0,
        //            CreditAmountOriginal = 0,
        //            Reason = rsAssemblyDismantlement.OWReason,
        //            Description = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Description,
        //            AccountingObjectID = rsAssemblyDismantlement.OWAccountingObjectID,
        //            //EmployeeID = rsAssemblyDismantlement.RSAssemblyDismantlements[i].EmployeeID,
        //            BudgetItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].BudgetItemID,
        //            CostSetID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CostSetID,
        //            ContractID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ContractID,
        //            StatisticsCodeID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].StatisticsCodeID,
        //            InvoiceSeries = null,
        //            ContactName = rsAssemblyDismantlement.OWAccountingObjectName,
        //            DetailID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ID,
        //            RefNo = rsAssemblyDismantlement.OWNo,
        //            RefDate = rsAssemblyDismantlement.OWDate,
        //            DepartmentID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DepartmentID,
        //            ExpenseItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ExpenseItemID,
        //            VATDescription = null,
        //            IsIrrationalCost = false
        //        };

        //        listGenTemp.Add(genTempCK);
        //        GeneralLedger genTempCorrespondingCK = new GeneralLedger
        //        {
        //            ID = Guid.NewGuid(),
        //            BranchID = null,
        //            ReferenceID = rsAssemblyDismantlement.ID,
        //            TypeID = rsAssemblyDismantlement.TypeID,
        //            Date = rsAssemblyDismantlement.OWDate,
        //            PostedDate = rsAssemblyDismantlement.OWPostedDate,
        //            No = rsAssemblyDismantlement.OWNo,
        //            InvoiceDate = null,
        //            InvoiceNo = null,
        //            Account = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CreditAccount,
        //            AccountCorresponding = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DebitAccount,
        //            BankAccountDetailID = null,
        //            CurrencyID = rsAssemblyDismantlement.CurrencyID,
        //            ExchangeRate = rsAssemblyDismantlement.ExchangeRate,
        //            DebitAmount = 0,
        //            DebitAmountOriginal = 0,
        //            CreditAmount = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Amount,
        //            CreditAmountOriginal = (decimal)rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].AmountOriginal,
        //            Reason = rsAssemblyDismantlement.OWReason,
        //            Description = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].Description,
        //            AccountingObjectID = rsAssemblyDismantlement.OWAccountingObjectID,
        //            //EmployeeID = rsAssemblyDismantlement.RSAssemblyDismantlements[i].EmployeeID,
        //            BudgetItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].BudgetItemID,
        //            CostSetID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].CostSetID,
        //            ContractID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ContractID,
        //            StatisticsCodeID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].StatisticsCodeID,
        //            InvoiceSeries = null,
        //            ContactName = rsAssemblyDismantlement.OWAccountingObjectName,
        //            DetailID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ID,
        //            RefNo = rsAssemblyDismantlement.OWNo,
        //            RefDate = rsAssemblyDismantlement.OWDate,
        //            DepartmentID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].DepartmentID,
        //            ExpenseItemID = rsAssemblyDismantlement.RSAssemblyDismantlementDetails[i].ExpenseItemID,
        //            VATDescription = null,
        //            IsIrrationalCost = false
        //        };
        //        listGenTemp.Add(genTempCorrespondingCK);

        #endregion
        //    }
        //    return listGenTemp;
        //}

        /// <summary>
        /// Tạo chứng từ sổ cái Bán hàng chưa thu tiền
        /// </summary>
        /// <param name="saInvoice"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(SAInvoice saInvoice)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = _ICurrencyService.Getbykey(saInvoice.CurrencyID);
            string currencyAccount = (saInvoice.TypeID == 321) ? iCurrency.CashAccount : iCurrency.BankAccount;

            if (saInvoice.TypeID == 320)
            {
                currencyAccount = null;
            }
            List<SAInvoiceDetail> lstDT = saInvoice.SAInvoiceDetails.OrderBy(n => n.OrderPriority).ToList();
            int[] typeInvoiceReceipting = { 321, 322, 324, 325 };
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ-Có
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = saInvoice.ID,
                    TypeID = saInvoice.TypeID,
                    Date = saInvoice.Date,
                    PostedDate = saInvoice.PostedDate,
                    No = saInvoice.No,
                    InvoiceDate = saInvoice.InvoiceDate,
                    InvoiceNo = saInvoice.InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = saInvoice.BankAccountDetailID,
                    CurrencyID = saInvoice.CurrencyID,
                    ExchangeRate = saInvoice.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = saInvoice.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                    EmployeeID = saInvoice.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = saInvoice.InvoiceSeries,
                    ContactName = saInvoice.ContactName,
                    DetailID = lstDT[i].ID,
                    //RefNo = saInvoice.No,
                    RefDate = saInvoice.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = false
                };
                if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                {
                    genTemp.No = saInvoice.MNo;
                    genTemp.RefNo = saInvoice.MNo;
                    genTemp.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;
                }
                else
                {
                    genTemp.No = saInvoice.No;
                    genTemp.RefNo = saInvoice.No;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = saInvoice.ID,
                    TypeID = saInvoice.TypeID,
                    Date = saInvoice.Date,
                    PostedDate = saInvoice.PostedDate,
                    No = saInvoice.No,
                    InvoiceDate = saInvoice.InvoiceDate,
                    InvoiceNo = saInvoice.InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = saInvoice.BankAccountDetailID,
                    CurrencyID = saInvoice.CurrencyID,
                    ExchangeRate = saInvoice.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = saInvoice.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                    EmployeeID = saInvoice.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = saInvoice.InvoiceSeries,
                    ContactName = saInvoice.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = saInvoice.No,
                    RefDate = saInvoice.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = false
                };
                if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                {
                    genTempCorresponding.No = saInvoice.MNo;
                    genTempCorresponding.RefNo = saInvoice.MNo;
                    genTempCorresponding.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;
                }
                else
                {
                    genTempCorresponding.No = saInvoice.No;
                    genTempCorresponding.RefNo = saInvoice.No;
                }
                listGenTemp.Add(genTempCorresponding);
                #endregion
                if (saInvoice.Exported)
                {
                    if (lstDT[i].ExportTaxAmount != 0)
                    {
                        #region Cặp Nợ-Thuế VAT
                        GeneralLedger genTemp1 = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = saInvoice.ID,
                            TypeID = saInvoice.TypeID,
                            Date = saInvoice.Date,
                            PostedDate = saInvoice.PostedDate,
                            No = saInvoice.No,
                            InvoiceDate = saInvoice.InvoiceDate,
                            InvoiceNo = saInvoice.InvoiceNo,
                            //Account = lstDT[i].DebitAccount,
                            //AccountCorresponding = lstDT[i].VATAccount,
                            Account = lstDT[i].ExportTaxAccountCorresponding,
                            AccountCorresponding = lstDT[i].ExportTaxAccount,
                            BankAccountDetailID = saInvoice.BankAccountDetailID,
                            CurrencyID = saInvoice.CurrencyID,
                            ExchangeRate = saInvoice.ExchangeRate,
                            DebitAmount = lstDT[i].ExportTaxAmount,
                            DebitAmountOriginal = lstDT[i].ExportTaxAmount,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = saInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                            EmployeeID = saInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = saInvoice.InvoiceSeries,
                            ContactName = saInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = saInvoice.No,
                            RefDate = saInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = false
                        };
                        if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                        {
                            genTemp1.No = saInvoice.MNo;
                            genTemp1.RefNo = saInvoice.MNo;
                            genTemp1.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;
                        }
                        else
                        {
                            genTemp1.No = saInvoice.No;
                            genTemp1.RefNo = saInvoice.No;
                        }
                        listGenTemp.Add(genTemp1);
                        GeneralLedger genTempCorresponding1 = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = saInvoice.ID,
                            TypeID = saInvoice.TypeID,
                            Date = saInvoice.Date,
                            PostedDate = saInvoice.PostedDate,
                            No = saInvoice.No,
                            InvoiceDate = saInvoice.InvoiceDate,
                            InvoiceNo = saInvoice.InvoiceNo,
                            //Account = lstDT[i].VATAccount,
                            //AccountCorresponding = lstDT[i].DebitAccount,
                            Account = lstDT[i].ExportTaxAccount,
                            AccountCorresponding = lstDT[i].ExportTaxAccountCorresponding,
                            BankAccountDetailID = saInvoice.BankAccountDetailID,
                            CurrencyID = saInvoice.CurrencyID,
                            ExchangeRate = saInvoice.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = lstDT[i].ExportTaxAmount,
                            CreditAmountOriginal = lstDT[i].ExportTaxAmount,
                            Reason = saInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                            EmployeeID = saInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = saInvoice.InvoiceSeries,
                            ContactName = saInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = saInvoice.No,
                            RefDate = saInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = false
                        };
                        if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                        {
                            genTempCorresponding1.No = saInvoice.MNo;
                            genTempCorresponding1.RefNo = saInvoice.MNo;
                            genTempCorresponding1.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                        }
                        else
                        {
                            genTempCorresponding1.No = saInvoice.No;
                            genTempCorresponding1.RefNo = saInvoice.No;
                        }
                        listGenTemp.Add(genTempCorresponding1);
                        #endregion
                    }
                }
                else
                {
                    if (lstDT[i].VATAmount != 0)
                    {
                        #region Cặp Nợ-Thuế VAT
                        GeneralLedger genTemp2 = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = saInvoice.ID,
                            TypeID = saInvoice.TypeID,
                            Date = saInvoice.Date,
                            PostedDate = saInvoice.PostedDate,
                            No = saInvoice.No,
                            InvoiceDate = saInvoice.InvoiceDate,
                            InvoiceNo = saInvoice.InvoiceNo,
                            //Account = lstDT[i].DebitAccount,
                            //AccountCorresponding = lstDT[i].VATAccount,
                            Account = lstDT[i].DebitAccount,
                            AccountCorresponding = lstDT[i].VATAccount,
                            BankAccountDetailID = saInvoice.BankAccountDetailID,
                            CurrencyID = saInvoice.CurrencyID,
                            ExchangeRate = saInvoice.ExchangeRate,
                            DebitAmount = lstDT[i].VATAmount,
                            DebitAmountOriginal = lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = saInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                            EmployeeID = saInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = saInvoice.InvoiceSeries,
                            ContactName = saInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = saInvoice.No,
                            RefDate = saInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = false
                        };
                        if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                        {
                            genTemp2.No = saInvoice.MNo;
                            genTemp2.RefNo = saInvoice.MNo;
                            genTemp2.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                        }
                        else
                        {
                            genTemp2.No = saInvoice.No;
                            genTemp2.RefNo = saInvoice.No;
                        }
                        listGenTemp.Add(genTemp2);
                        GeneralLedger genTempCorresponding2 = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = saInvoice.ID,
                            TypeID = saInvoice.TypeID,
                            Date = saInvoice.Date,
                            PostedDate = saInvoice.PostedDate,
                            No = saInvoice.No,
                            InvoiceDate = saInvoice.InvoiceDate,
                            InvoiceNo = saInvoice.InvoiceNo,
                            //Account = lstDT[i].VATAccount,
                            //AccountCorresponding = lstDT[i].DebitAccount,
                            Account = lstDT[i].VATAccount,
                            AccountCorresponding = lstDT[i].DebitAccount,
                            BankAccountDetailID = saInvoice.BankAccountDetailID,
                            CurrencyID = saInvoice.CurrencyID,
                            ExchangeRate = saInvoice.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = lstDT[i].VATAmount,
                            CreditAmountOriginal = lstDT[i].VATAmountOriginal,
                            Reason = saInvoice.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                            EmployeeID = saInvoice.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = saInvoice.InvoiceSeries,
                            ContactName = saInvoice.ContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = saInvoice.No,
                            RefDate = saInvoice.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = false
                        };
                        if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                        {
                            genTempCorresponding2.No = saInvoice.MNo;
                            genTempCorresponding2.RefNo = saInvoice.MNo;
                            genTempCorresponding2.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                        }
                        else
                        {
                            genTempCorresponding2.No = saInvoice.No;
                            genTempCorresponding2.RefNo = saInvoice.No;
                        }
                        listGenTemp.Add(genTempCorresponding2);
                        #endregion
                    }
                }
                if ((lstDT[i].DiscountAmountOriginal != 0) &&
                    (!string.IsNullOrEmpty(lstDT[i].DiscountAccount)))
                {
                    #region Cặp Nợ-Chiết khấu nếu có chiết khấu
                    GeneralLedger genTemp3 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saInvoice.ID,
                        TypeID = saInvoice.TypeID,
                        Date = saInvoice.Date,
                        PostedDate = saInvoice.PostedDate,
                        No = saInvoice.No,
                        InvoiceDate = saInvoice.InvoiceDate,
                        InvoiceNo = saInvoice.InvoiceNo,
                        Account = lstDT[i].DiscountAccount,
                        AccountCorresponding = lstDT[i].DebitAccount,
                        BankAccountDetailID = saInvoice.BankAccountDetailID,
                        CurrencyID = saInvoice.CurrencyID,
                        ExchangeRate = saInvoice.ExchangeRate,
                        DebitAmount = lstDT[i].DiscountAmountOriginal * saInvoice.ExchangeRate ?? 1,
                        DebitAmountOriginal = lstDT[i].DiscountAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = saInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                        EmployeeID = saInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saInvoice.InvoiceSeries,
                        ContactName = saInvoice.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saInvoice.No,
                        RefDate = saInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };
                    if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                    {
                        genTemp3.No = saInvoice.MNo;
                        genTemp3.RefNo = saInvoice.MNo;
                        genTemp3.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                    }
                    else
                    {
                        genTemp3.No = saInvoice.No;
                        genTemp3.RefNo = saInvoice.No;
                    }
                    listGenTemp.Add(genTemp3);
                    GeneralLedger genTempCorresponding3 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saInvoice.ID,
                        TypeID = saInvoice.TypeID,
                        Date = saInvoice.Date,
                        PostedDate = saInvoice.PostedDate,
                        No = saInvoice.No,
                        InvoiceDate = saInvoice.InvoiceDate,
                        InvoiceNo = saInvoice.InvoiceNo,
                        Account = lstDT[i].DebitAccount,
                        AccountCorresponding = lstDT[i].DiscountAccount,
                        BankAccountDetailID = saInvoice.BankAccountDetailID,
                        CurrencyID = saInvoice.CurrencyID,
                        ExchangeRate = saInvoice.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = lstDT[i].DiscountAmountOriginal * saInvoice.ExchangeRate ?? 1,
                        CreditAmountOriginal = lstDT[i].DiscountAmountOriginal,
                        Reason = saInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                        EmployeeID = saInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saInvoice.InvoiceSeries,
                        ContactName = saInvoice.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saInvoice.No,
                        RefDate = saInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };
                    if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                    {
                        genTempCorresponding3.No = saInvoice.MNo;
                        genTempCorresponding3.RefNo = saInvoice.MNo;
                        genTempCorresponding3.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                    }
                    else
                    {
                        genTempCorresponding3.No = saInvoice.No;
                        genTempCorresponding3.RefNo = saInvoice.No;
                    }
                    listGenTemp.Add(genTempCorresponding3);
                    #endregion
                }

                if (saInvoice.OutwardNo != null)
                {
                    #region Cặp Kho - Giá vốn
                    GeneralLedger genTemp4 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saInvoice.ID,
                        TypeID = 412,
                        Date = saInvoice.Date,
                        PostedDate = saInvoice.PostedDate,
                        No = saInvoice.OutwardNo,
                        InvoiceDate = saInvoice.InvoiceDate,
                        InvoiceNo = saInvoice.InvoiceNo,
                        //Account = lstDT[i].RepositoryAccount,
                        //AccountCorresponding = lstDT[i].CostAccount,
                        Account = lstDT[i].CostAccount,
                        AccountCorresponding = lstDT[i].RepositoryAccount,
                        BankAccountDetailID = saInvoice.BankAccountDetailID,
                        CurrencyID = saInvoice.CurrencyID,
                        ExchangeRate = saInvoice.ExchangeRate,
                        DebitAmount = lstDT[i].OWAmount,
                        DebitAmountOriginal = lstDT[i].OWAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = saInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                        EmployeeID = saInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saInvoice.InvoiceSeries,
                        ContactName = saInvoice.SContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saInvoice.No,
                        RefDate = saInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };
                    if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                    {
                        genTemp4.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                    }
                    listGenTemp.Add(genTemp4);
                    GeneralLedger genTempCorresponding4 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saInvoice.ID,
                        TypeID = 412,
                        Date = saInvoice.Date,
                        PostedDate = saInvoice.PostedDate,
                        No = saInvoice.OutwardNo,
                        InvoiceDate = saInvoice.InvoiceDate,
                        InvoiceNo = saInvoice.InvoiceNo,
                        //Account = lstDT[i].CostAccount,
                        //AccountCorresponding = lstDT[i].RepositoryAccount,
                        Account = lstDT[i].RepositoryAccount,
                        AccountCorresponding = lstDT[i].CostAccount,
                        BankAccountDetailID = saInvoice.BankAccountDetailID,
                        CurrencyID = saInvoice.CurrencyID,
                        ExchangeRate = saInvoice.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = lstDT[i].OWAmount,
                        CreditAmountOriginal = lstDT[i].OWAmountOriginal,
                        Reason = saInvoice.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saInvoice.AccountingObjectID,
                        EmployeeID = saInvoice.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saInvoice.InvoiceSeries,
                        ContactName = saInvoice.SContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saInvoice.No,
                        RefDate = saInvoice.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };
                    if (typeInvoiceReceipting.Contains(saInvoice.TypeID))
                    {
                        genTempCorresponding4.Reason = (saInvoice.SReason == null || string.IsNullOrEmpty(saInvoice.SReason)) ? saInvoice.MReasonPay : saInvoice.SReason;

                    }
                    listGenTemp.Add(genTempCorresponding4);
                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Tạo chứng từ sổ cái Hàng bán trả lại, giảm giá
        /// </summary>
        /// <param name="saReturn"></param>
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(SAReturn saReturn)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = _ICurrencyService.Getbykey(saReturn.CurrencyID);
            //string currencyAccount = (ppService.BankAccountDetailID == null && ppService.CreditCardNumber == null) ? iCurrency.CashAccount : iCurrency.BankAccount;

            List<SAReturnDetail> lstDT = saReturn.SAReturnDetails.OrderBy(n => n.OrderPriority).ToList();

            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ-Có
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = saReturn.ID,
                    TypeID = saReturn.TypeID,
                    Date = saReturn.Date,
                    PostedDate = saReturn.PostedDate,
                    No = saReturn.No,
                    InvoiceDate = saReturn.InvoiceDate,
                    InvoiceNo = saReturn.InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = saReturn.CurrencyID,
                    ExchangeRate = saReturn.ExchangeRate,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = saReturn.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                    EmployeeID = saReturn.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = saReturn.InvoiceSeries,
                    ContactName = saReturn.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = saReturn.No,
                    RefDate = saReturn.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = false,
                };

                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = saReturn.ID,
                    TypeID = saReturn.TypeID,
                    Date = saReturn.Date,
                    PostedDate = saReturn.PostedDate,
                    No = saReturn.No,
                    InvoiceDate = saReturn.InvoiceDate,
                    InvoiceNo = saReturn.InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = saReturn.CurrencyID,
                    ExchangeRate = saReturn.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = saReturn.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                    EmployeeID = saReturn.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = saReturn.InvoiceSeries,
                    ContactName = saReturn.ContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = saReturn.No,
                    RefDate = saReturn.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = false
                };
                listGenTemp.Add(genTempCorresponding);
                #endregion
                if (lstDT[i].VATAmount != 0)
                {
                    #region Cặp Nợ-Thuế VAT

                    //string currencyAccount = (lstDT[i].BankAccountDetailID == null) ? iCurrency.CashAccount : iCurrency.BankAccount;

                    GeneralLedger genTemp1 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = saReturn.TypeID,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.No,
                        InvoiceDate = saReturn.InvoiceDate,
                        InvoiceNo = saReturn.InvoiceNo,
                        Account = lstDT[i].VATAccount,
                        AccountCorresponding = lstDT[i].CreditAccount,
                        //AccountCorresponding = currencyAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = saReturn.CurrencyID,
                        ExchangeRate = saReturn.ExchangeRate,
                        DebitAmount = lstDT[i].VATAmount,
                        DebitAmountOriginal = lstDT[i].VATAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = saReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                        EmployeeID = saReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saReturn.InvoiceSeries,
                        ContactName = saReturn.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saReturn.No,
                        RefDate = saReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };

                    listGenTemp.Add(genTemp1);
                    GeneralLedger genTempCorresponding1 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = saReturn.TypeID,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.No,
                        InvoiceDate = saReturn.InvoiceDate,
                        InvoiceNo = saReturn.InvoiceNo,
                        Account = lstDT[i].CreditAccount,
                        //Account = currencyAccount,
                        AccountCorresponding = lstDT[i].VATAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = saReturn.CurrencyID,
                        ExchangeRate = saReturn.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = lstDT[i].VATAmount,
                        CreditAmountOriginal = lstDT[i].VATAmountOriginal,
                        Reason = saReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                        EmployeeID = saReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saReturn.InvoiceSeries,
                        ContactName = saReturn.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saReturn.No,
                        RefDate = saReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };

                    listGenTemp.Add(genTempCorresponding1);
                    #endregion
                }
                if (lstDT[i].DiscountAmount != 0 || lstDT[i].DiscountAmountOriginal != 0)
                {
                    #region Cặp Nợ-Chiết khấu nếu có chiết khấu


                    GeneralLedger genTemp2 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = saReturn.TypeID,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.No,
                        InvoiceDate = saReturn.InvoiceDate,
                        InvoiceNo = saReturn.InvoiceNo,
                        Account = lstDT[i].CreditAccount,
                        AccountCorresponding = lstDT[i].DiscountAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = saReturn.CurrencyID,
                        ExchangeRate = saReturn.ExchangeRate,
                        DebitAmount = Utils.calculateAmountFromOriginal(lstDT[i].DiscountAmountOriginal, saReturn.ExchangeRate),
                        DebitAmountOriginal = lstDT[i].DiscountAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = saReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                        EmployeeID = saReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saReturn.InvoiceSeries,
                        ContactName = saReturn.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saReturn.No,
                        RefDate = saReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };

                    listGenTemp.Add(genTemp2);
                    GeneralLedger genTempCorresponding2 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = saReturn.TypeID,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.No,
                        InvoiceDate = saReturn.InvoiceDate,
                        InvoiceNo = saReturn.InvoiceNo,
                        Account = lstDT[i].DiscountAccount,
                        AccountCorresponding = lstDT[i].CreditAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = saReturn.CurrencyID,
                        ExchangeRate = saReturn.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = Utils.calculateAmountFromOriginal(lstDT[i].DiscountAmountOriginal, saReturn.ExchangeRate),
                        CreditAmountOriginal = lstDT[i].DiscountAmountOriginal,
                        Reason = saReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                        EmployeeID = saReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saReturn.InvoiceSeries,
                        ContactName = saReturn.ContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saReturn.No,
                        RefDate = saReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };

                    listGenTemp.Add(genTempCorresponding2);

                    #endregion
                }
                if (lstDT[i].OWAmountOriginal != 0)
                {
                    #region Cặp Kho - Giá vốn
                    GeneralLedger genTemp3 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = saReturn.TypeID,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.No,
                        InvoiceDate = saReturn.InvoiceDate,
                        InvoiceNo = saReturn.InvoiceNo,
                        Account = lstDT[i].CostAccount,
                        AccountCorresponding = lstDT[i].RepositoryAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = saReturn.CurrencyID,
                        ExchangeRate = saReturn.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = Utils.calculateAmountFromOriginal(lstDT[i].OWAmountOriginal, saReturn.ExchangeRate),
                        CreditAmountOriginal = lstDT[i].OWAmountOriginal,
                        Reason = saReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                        EmployeeID = saReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saReturn.InvoiceSeries,
                        ContactName = saReturn.IWContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saReturn.No,
                        RefDate = saReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };

                    listGenTemp.Add(genTemp3);
                    GeneralLedger genTempCorresponding3 = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = saReturn.ID,
                        TypeID = saReturn.TypeID,
                        Date = saReturn.Date,
                        PostedDate = saReturn.PostedDate,
                        No = saReturn.No,
                        InvoiceDate = saReturn.InvoiceDate,
                        InvoiceNo = saReturn.InvoiceNo,
                        Account = lstDT[i].RepositoryAccount,
                        AccountCorresponding = lstDT[i].CostAccount,
                        BankAccountDetailID = lstDT[i].BankAccountDetailID,
                        CurrencyID = saReturn.CurrencyID,
                        ExchangeRate = saReturn.ExchangeRate,
                        DebitAmount = Utils.calculateAmountFromOriginal(lstDT[i].OWAmountOriginal, saReturn.ExchangeRate),
                        DebitAmountOriginal = lstDT[i].OWAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = saReturn.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? saReturn.AccountingObjectID,
                        EmployeeID = saReturn.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = saReturn.InvoiceSeries,
                        ContactName = saReturn.IWContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = saReturn.No,
                        RefDate = saReturn.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = false
                    };

                    listGenTemp.Add(genTempCorresponding3);
                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái ghi tăng CCDC
        /// </summary>
        /// <param name="tiIncrement"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(TIIncrement tiIncrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            if (tiIncrement.TypeID == 907) return listGenTemp;
            Currency iCurrency = _ICurrencyService.Getbykey(tiIncrement.CurrencyID);
            //string currencyAccount = (tiIncrement.TypeID == 430) ? iCurrency.CashAccount : iCurrency.BankAccount;

            List<TIIncrementDetail> lstDT = tiIncrement.TIIncrementDetails.OrderBy(n => n.OrderPriority).ToList();

            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ-Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiIncrement.ID,
                    TypeID = tiIncrement.TypeID,
                    Date = tiIncrement.Date,
                    PostedDate = tiIncrement.PostedDate,
                    No = tiIncrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = tiIncrement.BankAccountDetailID,
                    CurrencyID = tiIncrement.CurrencyID,
                    ExchangeRate = tiIncrement.ExchangeRate,
                    DebitAmount = lstDT[i].Amount - lstDT[i].DiscountAmount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal - lstDT[i].DiscountAmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = tiIncrement.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                    EmployeeID = tiIncrement.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = null,
                    ContactName = tiIncrement.MContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = tiIncrement.No,
                    RefDate = tiIncrement.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiIncrement.ID,
                    TypeID = tiIncrement.TypeID,
                    Date = tiIncrement.Date,
                    PostedDate = tiIncrement.PostedDate,
                    No = tiIncrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = tiIncrement.BankAccountDetailID,
                    CurrencyID = tiIncrement.CurrencyID,
                    ExchangeRate = tiIncrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount - lstDT[i].DiscountAmount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal - lstDT[i].DiscountAmountOriginal,
                    Reason = tiIncrement.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                    EmployeeID = tiIncrement.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = null,
                    ContactName = tiIncrement.MContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = tiIncrement.No,
                    RefDate = tiIncrement.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);

                #endregion
                if (tiIncrement.IsImportPurchase == false)
                {
                    if (lstDT[i].VATAmountOriginal != 0)
                    {
                        #region Cặp Nợ - Thuế GTGT

                        GeneralLedger genTempVAT = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = tiIncrement.ID,
                            TypeID = tiIncrement.TypeID,
                            Date = tiIncrement.Date,
                            PostedDate = tiIncrement.PostedDate,
                            No = tiIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            AccountCorresponding = lstDT[i].DeductionDebitAccount,
                            Account = lstDT[i].VATAccount,
                            BankAccountDetailID = tiIncrement.BankAccountDetailID,
                            CurrencyID = tiIncrement.CurrencyID,
                            ExchangeRate = tiIncrement.ExchangeRate,
                            DebitAmount = lstDT[i].VATAmount,
                            DebitAmountOriginal = lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = tiIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                            EmployeeID = tiIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = tiIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = tiIncrement.No,
                            RefDate = tiIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost
                        };

                        listGenTemp.Add(genTempVAT);
                        GeneralLedger genTempVATCorresponding = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = tiIncrement.ID,
                            TypeID = tiIncrement.TypeID,
                            Date = tiIncrement.Date,
                            PostedDate = tiIncrement.PostedDate,
                            No = tiIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            AccountCorresponding = lstDT[i].VATAccount,
                            Account = lstDT[i].DeductionDebitAccount,
                            BankAccountDetailID = tiIncrement.BankAccountDetailID,
                            CurrencyID = tiIncrement.CurrencyID,
                            ExchangeRate = tiIncrement.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = (decimal)lstDT[i].VATAmount,
                            CreditAmountOriginal = (decimal)lstDT[i].VATAmountOriginal,
                            Reason = tiIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                            EmployeeID = tiIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = tiIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = tiIncrement.No,
                            RefDate = tiIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost,
                            VATDescription = lstDT[i].VATDescription
                        };

                        listGenTemp.Add(genTempVATCorresponding);

                        #endregion
                    }
                }
                else
                {
                    if (lstDT[i].VATAmountOriginal != 0)
                    {
                        #region Cặp Nợ - Thuế GTGT

                        GeneralLedger genTempVAT = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = tiIncrement.ID,
                            TypeID = tiIncrement.TypeID,
                            Date = tiIncrement.Date,
                            PostedDate = tiIncrement.PostedDate,
                            No = tiIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            AccountCorresponding = lstDT[i].VATAccount,
                            Account = lstDT[i].DeductionDebitAccount,
                            BankAccountDetailID = tiIncrement.BankAccountDetailID,
                            CurrencyID = tiIncrement.CurrencyID,
                            ExchangeRate = tiIncrement.ExchangeRate,
                            DebitAmount = lstDT[i].VATAmount,
                            DebitAmountOriginal = lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = tiIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                            EmployeeID = tiIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = tiIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = tiIncrement.No,
                            RefDate = tiIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost
                        };

                        listGenTemp.Add(genTempVAT);
                        GeneralLedger genTempVATCorresponding = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = tiIncrement.ID,
                            TypeID = tiIncrement.TypeID,
                            Date = tiIncrement.Date,
                            PostedDate = tiIncrement.PostedDate,
                            No = tiIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            AccountCorresponding = lstDT[i].DeductionDebitAccount,
                            Account = lstDT[i].VATAccount,
                            BankAccountDetailID = tiIncrement.BankAccountDetailID,
                            CurrencyID = tiIncrement.CurrencyID,
                            ExchangeRate = tiIncrement.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = (decimal)lstDT[i].VATAmount,
                            CreditAmountOriginal = (decimal)lstDT[i].VATAmountOriginal,
                            Reason = tiIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                            EmployeeID = tiIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = tiIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = tiIncrement.No,
                            RefDate = tiIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost,
                            VATDescription = lstDT[i].VATDescription
                        };

                        listGenTemp.Add(genTempVATCorresponding);

                        #endregion
                    }
                }
                if (lstDT[i].SpecialConsumeTaxAmountOriginal != 0)
                {
                    #region Cặp Nợ - Thuế tiêu thụ đặc biệt: Nếu hàng hóa có thuế TTĐB

                    GeneralLedger genTempVATTTDB = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        AccountCorresponding = lstDT[i].SpecialConsumeTaxAccount,
                        Account = lstDT[i].DebitAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = lstDT[i].SpecialConsumeTaxAmount,
                        DebitAmountOriginal = lstDT[i].SpecialConsumeTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = tiIncrement.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATTTDB);
                    GeneralLedger genTempVATTTDBCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        AccountCorresponding = lstDT[i].DebitAccount,
                        Account = lstDT[i].SpecialConsumeTaxAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)lstDT[i].SpecialConsumeTaxAmount,
                        CreditAmountOriginal = (decimal)lstDT[i].SpecialConsumeTaxAmountOriginal,
                        Reason = tiIncrement.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription,
                    };

                    listGenTemp.Add(genTempVATTTDBCorresponding);

                    #endregion
                }
                if (lstDT[i].ImportTaxAmountOriginal != 0)
                {
                    #region Cặp Nợ - Thuế Nhập khẩu: Nếu chọn Mua hàng = Nhập khẩu

                    GeneralLedger genTempVATNK = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        Account = lstDT[i].DebitAccount,
                        AccountCorresponding = lstDT[i].ImportTaxAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = lstDT[i].ImportTaxAmount,
                        DebitAmountOriginal = lstDT[i].ImportTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = tiIncrement.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATNK);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        Account = lstDT[i].ImportTaxAccount,
                        AccountCorresponding = lstDT[i].DebitAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)lstDT[i].ImportTaxAmount,
                        CreditAmountOriginal = (decimal)lstDT[i].ImportTaxAmountOriginal,
                        Reason = tiIncrement.Reason,
                        //Description = lstDT[i].VATDescription,
                        AccountingObjectID = tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription
                    };
                    listGenTemp.Add(genTempVATCorresponding);
                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái ghi giảm CCDC
        /// </summary>
        /// <param name="tiDecrement"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(TIDecrement tiDecrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<TIDecrementDetail> lstDT = tiDecrement.TIDecrementDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                TIDecrementDetail post = lstDT[i];
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiDecrement.ID,
                    TypeID = tiDecrement.TypeID,
                    Date = tiDecrement.Date,
                    PostedDate = tiDecrement.PostedDate,
                    No = tiDecrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    //Account = post.DebitAccount,
                    //AccountCorresponding = post.CreditAccount,
                    //CurrencyID = tiDecrement.CurrencyID,
                    //ExchangeRate = tiDecrement.ExchangeRate,
                    DebitAmount = post.Amount,
                    //DebitAmountOriginal = post.AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = tiDecrement.Reason,
                    Description = post.Description,
                    //AccountingObjectID = post.AccountingObjectID,
                    //EmployeeID = post.EmployeeID,
                    //BudgetItemID = post.BudgetItemID,
                    //CostSetID = post.CostSet,
                    //ContractID = post.ContractID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = tiDecrement.No,
                    RefDate = tiDecrement.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    //ExpenseItemID = post.ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = post.IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiDecrement.ID,
                    TypeID = tiDecrement.TypeID,
                    Date = tiDecrement.Date,
                    PostedDate = tiDecrement.PostedDate,
                    No = tiDecrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    //Account = post.CreditAccount,
                    //AccountCorresponding = post.DebitAccount,
                    //BankAccountDetailID = faDecrement.BankAccountDetailID,
                    //CurrencyID = tiDecrement.CurrencyID,
                    //ExchangeRate = tiDecrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = post.Amount,
                    //CreditAmountOriginal = post.AmountOriginal,
                    Reason = tiDecrement.Reason,
                    Description = post.Description,
                    //AccountingObjectID = post.AccountingObjectID,
                    //EmployeeID = post.EmployeeID,
                    //BudgetItemID = post.BudgetItemID,
                    //CostSetID = post.CostSetID,
                    //ContractID = post.ContractID,
                    //StatisticsCodeID = faDecrement.TIIncrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = tiDecrement.No,
                    RefDate = tiDecrement.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    //ExpenseItemID = post.ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = post.IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);

            }
            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái điều chỉnh CCDC
        /// </summary>
        /// <param name="TIAdjustment"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(TIAdjustment TIAdjustment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có
            List<TIAdjustmentDetail> lstDT = TIAdjustment.TIAdjustmentDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TIAdjustment.ID,
                    TypeID = TIAdjustment.TypeID,
                    Date = TIAdjustment.Date,
                    PostedDate = TIAdjustment.PostedDate,
                    No = TIAdjustment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].Amount,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = TIAdjustment.Reason,
                    Description = lstDT[i].Description,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = TIAdjustment.No,
                    RefDate = TIAdjustment.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TIAdjustment.ID,
                    TypeID = TIAdjustment.TypeID,
                    Date = TIAdjustment.Date,
                    PostedDate = TIAdjustment.PostedDate,
                    No = TIAdjustment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].Amount,
                    Reason = TIAdjustment.Reason,
                    Description = lstDT[i].Description,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = TIAdjustment.No,
                    RefDate = TIAdjustment.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái điều chỉnh CCDC
        /// </summary>
        /// <param name="TITransfer"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(TITransfer TITransfer)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có
            List<TITransferDetail> lstDT = TITransfer.TITransferDetails.OrderBy(n => n.OrderPriority).ToList();

            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TITransfer.ID,
                    TypeID = TITransfer.TypeID,
                    Date = TITransfer.Date,
                    PostedDate = TITransfer.PostedDate,
                    No = TITransfer.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = TITransfer.Reason,
                    Description = lstDT[i].Description,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = TITransfer.No,
                    RefDate = TITransfer.Date,
                    DepartmentID = lstDT[i].ToDepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TITransfer.ID,
                    TypeID = TITransfer.TypeID,
                    Date = TITransfer.Date,
                    PostedDate = TITransfer.PostedDate,
                    No = TITransfer.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = TITransfer.Reason,
                    Description = lstDT[i].Description,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = TITransfer.No,
                    RefDate = TITransfer.Date,
                    DepartmentID = lstDT[i].ToDepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái điều chỉnh CCDC
        /// </summary>
        /// <param name="TIAllocation"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(TIAllocation TIAllocation)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có
            List<TIAllocationPost> lstDT = TIAllocation.TIAllocationPosts.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TIAllocation.ID,
                    TypeID = TIAllocation.TypeID,
                    Date = TIAllocation.Date,
                    PostedDate = TIAllocation.PostedDate,
                    No = TIAllocation.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = TIAllocation.Reason,
                    Description = lstDT[i].Description,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = TIAllocation.No,
                    RefDate = TIAllocation.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    CostSetID = lstDT[i].CostSetID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TIAllocation.ID,
                    TypeID = TIAllocation.TypeID,
                    Date = TIAllocation.Date,
                    PostedDate = TIAllocation.PostedDate,
                    No = TIAllocation.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal,
                    Reason = TIAllocation.Reason,
                    Description = lstDT[i].Description,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = TIAllocation.No,
                    RefDate = TIAllocation.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    CostSetID = lstDT[i].CostSetID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái kiểm kê CCDC
        /// </summary>
        /// <param name="TIAudit"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(TIAudit TIAudit)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có
            for (int i = 0; i < TIAudit.TIAuditDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TIAudit.ID,
                    TypeID = TIAudit.TypeID,
                    Date = TIAudit.Date,
                    PostedDate = TIAudit.PostedDate,
                    No = TIAudit.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    //Account = TIAudit.TIAuditDetails[i].DebitAccount,
                    //AccountCorresponding = TIAudit.TIAuditDetails[i].CreditAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = TIAudit.Description,
                    Description = TIAudit.TIAuditDetails[i].Note,
                    EmployeeID = null,
                    //BudgetItemID = TIAudit.TIAuditDetails[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = TIAudit.TIAuditDetails[i].ID,
                    RefNo = TIAudit.No,
                    RefDate = TIAudit.Date,
                    DepartmentID = TIAudit.TIAuditDetails[i].DepartmentID,
                    //ExpenseItemID = TIAudit.TIAuditDetails[i].ExpenseItemID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = TIAudit.ID,
                    TypeID = TIAudit.TypeID,
                    Date = TIAudit.Date,
                    PostedDate = TIAudit.PostedDate,
                    No = TIAudit.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    //Account = TIAudit.TIAuditDetails[i].CreditAccount,
                    //AccountCorresponding = TIAudit.TIAuditDetails[i].DebitAccount,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = TIAudit.Description,
                    Description = TIAudit.TIAuditDetails[i].Note,
                    EmployeeID = null,
                    //BudgetItemID = TIAudit.TIAuditDetails[i].BudgetItemID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = TIAudit.TIAuditDetails[i].ID,
                    RefNo = TIAudit.No,
                    RefDate = TIAudit.Date,
                    DepartmentID = TIAudit.TIAuditDetails[i].DepartmentID,
                    //ExpenseItemID = TIAudit.TIAuditDetails[i].ExpenseItemID,
                    VATDescription = null,
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái ghi tăng TSCĐ
        /// </summary>
        /// <param name="faIncrement"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(FAIncrement faIncrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = _ICurrencyService.Getbykey(faIncrement.CurrencyID);
            if (faIncrement.TypeID == 510) return listGenTemp;
            string currencyAccount = (faIncrement.TypeID == 500) ? iCurrency.CashAccount : iCurrency.BankAccount;
            List<FAIncrementDetail> lstDT = faIncrement.FAIncrementDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ-Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faIncrement.ID,
                    TypeID = faIncrement.TypeID,
                    Date = faIncrement.Date,
                    PostedDate = faIncrement.PostedDate,
                    No = faIncrement.No,
                    InvoiceDate = lstDT[i].InvoiceDate,
                    InvoiceNo = lstDT[i].InvoiceNo,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    BankAccountDetailID = faIncrement.BankAccountDetailID,
                    CurrencyID = faIncrement.CurrencyID,
                    ExchangeRate = faIncrement.ExchangeRate,
                    DebitAmount = lstDT[i].Amount - lstDT[i].DiscountAmount,
                    DebitAmountOriginal = lstDT[i].AmountOriginal - lstDT[i].DiscountAmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = faIncrement.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                    EmployeeID = faIncrement.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = null,
                    ContactName = faIncrement.MContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = faIncrement.No,
                    RefDate = faIncrement.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faIncrement.ID,
                    TypeID = faIncrement.TypeID,
                    Date = faIncrement.Date,
                    PostedDate = faIncrement.PostedDate,
                    No = faIncrement.No,
                    InvoiceDate = lstDT[i].InvoiceDate,
                    InvoiceNo = lstDT[i].InvoiceNo,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    BankAccountDetailID = faIncrement.BankAccountDetailID,
                    CurrencyID = faIncrement.CurrencyID,
                    ExchangeRate = faIncrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount - lstDT[i].DiscountAmount,
                    CreditAmountOriginal = lstDT[i].AmountOriginal - lstDT[i].DiscountAmountOriginal,
                    Reason = faIncrement.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                    EmployeeID = faIncrement.EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = null,
                    ContactName = faIncrement.MContactName,
                    DetailID = lstDT[i].ID,
                    RefNo = faIncrement.No,
                    RefDate = faIncrement.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = lstDT[i].VATDescription,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);

                #endregion
                if (faIncrement.IsImportPurchase == false)
                {
                    if (lstDT[i].VATAmount != 0)
                    {
                        #region Cặp Nợ - Thuế GTGT

                        GeneralLedger genTempVAT = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = faIncrement.ID,
                            TypeID = faIncrement.TypeID,
                            Date = faIncrement.Date,
                            PostedDate = faIncrement.PostedDate,
                            No = faIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            Account = lstDT[i].VATAccount,
                            AccountCorresponding = lstDT[i].DeductionDebitAccount ?? lstDT[i].CreditAccount,
                            BankAccountDetailID = faIncrement.BankAccountDetailID,
                            CurrencyID = faIncrement.CurrencyID,
                            ExchangeRate = faIncrement.ExchangeRate,
                            DebitAmount = lstDT[i].VATAmount,
                            DebitAmountOriginal = lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = faIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                            EmployeeID = faIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = faIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = faIncrement.No,
                            RefDate = faIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost
                        };


                        listGenTemp.Add(genTempVAT);
                        GeneralLedger genTempVATCorresponding = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = faIncrement.ID,
                            TypeID = faIncrement.TypeID,
                            Date = faIncrement.Date,
                            PostedDate = faIncrement.PostedDate,
                            No = faIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            Account = lstDT[i].DeductionDebitAccount ?? lstDT[i].CreditAccount,
                            AccountCorresponding = lstDT[i].VATAccount,
                            BankAccountDetailID = faIncrement.BankAccountDetailID,
                            CurrencyID = faIncrement.CurrencyID,
                            ExchangeRate = faIncrement.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = lstDT[i].VATAmount,
                            CreditAmountOriginal = lstDT[i].VATAmountOriginal,
                            Reason = faIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                            EmployeeID = faIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = faIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = faIncrement.No,
                            RefDate = faIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost,
                            VATDescription = lstDT[i].VATDescription
                        };

                        listGenTemp.Add(genTempVATCorresponding);

                        #endregion
                    }
                }
                else
                {
                    if (lstDT[i].VATAmount != 0)
                    {
                        #region Cặp Nợ - Thuế GTGT

                        GeneralLedger genTempVAT = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = faIncrement.ID,
                            TypeID = faIncrement.TypeID,
                            Date = faIncrement.Date,
                            PostedDate = faIncrement.PostedDate,
                            No = faIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            Account = lstDT[i].DeductionDebitAccount ?? lstDT[i].CreditAccount,
                            AccountCorresponding = lstDT[i].VATAccount,
                            BankAccountDetailID = faIncrement.BankAccountDetailID,
                            CurrencyID = faIncrement.CurrencyID,
                            ExchangeRate = faIncrement.ExchangeRate,
                            DebitAmount = lstDT[i].VATAmount,
                            DebitAmountOriginal = lstDT[i].VATAmountOriginal,
                            CreditAmount = 0,
                            CreditAmountOriginal = 0,
                            Reason = faIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                            EmployeeID = faIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = faIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = faIncrement.No,
                            RefDate = faIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            VATDescription = lstDT[i].VATDescription,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost
                        };


                        listGenTemp.Add(genTempVAT);
                        GeneralLedger genTempVATCorresponding = new GeneralLedger
                        {
                            ID = Guid.NewGuid(),
                            BranchID = null,
                            ReferenceID = faIncrement.ID,
                            TypeID = faIncrement.TypeID,
                            Date = faIncrement.Date,
                            PostedDate = faIncrement.PostedDate,
                            No = faIncrement.No,
                            InvoiceDate = lstDT[i].InvoiceDate,
                            InvoiceNo = lstDT[i].InvoiceNo,
                            Account = lstDT[i].VATAccount,
                            AccountCorresponding = lstDT[i].DeductionDebitAccount ?? lstDT[i].CreditAccount,
                            BankAccountDetailID = faIncrement.BankAccountDetailID,
                            CurrencyID = faIncrement.CurrencyID,
                            ExchangeRate = faIncrement.ExchangeRate,
                            DebitAmount = 0,
                            DebitAmountOriginal = 0,
                            CreditAmount = lstDT[i].VATAmount,
                            CreditAmountOriginal = lstDT[i].VATAmountOriginal,
                            Reason = faIncrement.Reason,
                            Description = lstDT[i].Description,
                            AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                            EmployeeID = faIncrement.EmployeeID,
                            BudgetItemID = lstDT[i].BudgetItemID,
                            CostSetID = lstDT[i].CostSetID,
                            ContractID = lstDT[i].ContractID,
                            StatisticsCodeID = lstDT[i].StatisticsCodeID,
                            InvoiceSeries = null,
                            ContactName = faIncrement.MContactName,
                            DetailID = lstDT[i].ID,
                            RefNo = faIncrement.No,
                            RefDate = faIncrement.Date,
                            DepartmentID = lstDT[i].DepartmentID,
                            ExpenseItemID = lstDT[i].ExpenseItemID,
                            IsIrrationalCost = lstDT[i].IsIrrationalCost,
                            VATDescription = lstDT[i].VATDescription
                        };

                        listGenTemp.Add(genTempVATCorresponding);

                        #endregion
                    }
                }
                if (lstDT[i].SpecialConsumeTaxAmount != 0)
                {
                    #region Cặp Nợ - Thuế tiêu thụ đặc biệt: Nếu hàng hóa có thuế TTĐB

                    GeneralLedger genTempVATTTDB = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = faIncrement.ID,
                        TypeID = faIncrement.TypeID,
                        Date = faIncrement.Date,
                        PostedDate = faIncrement.PostedDate,
                        No = faIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        AccountCorresponding = lstDT[i].SpecialConsumeTaxAccount,
                        Account = lstDT[i].DebitAccount,
                        BankAccountDetailID = faIncrement.BankAccountDetailID,
                        CurrencyID = faIncrement.CurrencyID,
                        ExchangeRate = faIncrement.ExchangeRate,
                        DebitAmount = lstDT[i].SpecialConsumeTaxAmount,
                        DebitAmountOriginal = lstDT[i].SpecialConsumeTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = faIncrement.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                        EmployeeID = faIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = faIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = faIncrement.No,
                        RefDate = faIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATTTDB);
                    GeneralLedger genTempVATTTDBCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = faIncrement.ID,
                        TypeID = faIncrement.TypeID,
                        Date = faIncrement.Date,
                        PostedDate = faIncrement.PostedDate,
                        No = faIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        AccountCorresponding = lstDT[i].DebitAccount,
                        Account = lstDT[i].SpecialConsumeTaxAccount,
                        BankAccountDetailID = faIncrement.BankAccountDetailID,
                        CurrencyID = faIncrement.CurrencyID,
                        ExchangeRate = faIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)lstDT[i].SpecialConsumeTaxAmount,
                        CreditAmountOriginal = (decimal)lstDT[i].SpecialConsumeTaxAmountOriginal,
                        Reason = faIncrement.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = lstDT[i].AccountingObjectID ?? faIncrement.AccountingObjectID,
                        EmployeeID = faIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = faIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = faIncrement.No,
                        RefDate = faIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription,
                    };

                    listGenTemp.Add(genTempVATTTDBCorresponding);

                    #endregion
                }
                if (lstDT[i].ImportTaxAmountOriginal != 0)
                {
                    #region Cặp Nợ - Thuế Nhập khẩu: Nếu chọn Mua hàng = Nhập khẩu

                    GeneralLedger genTempVATNK = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = faIncrement.ID,
                        TypeID = faIncrement.TypeID,
                        Date = faIncrement.Date,
                        PostedDate = faIncrement.PostedDate,
                        No = faIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        Account = lstDT[i].DebitAccount,
                        AccountCorresponding = lstDT[i].ImportTaxAccount,
                        BankAccountDetailID = faIncrement.BankAccountDetailID,
                        CurrencyID = faIncrement.CurrencyID,
                        ExchangeRate = faIncrement.ExchangeRate,
                        DebitAmount = lstDT[i].ImportTaxAmount,
                        DebitAmountOriginal = lstDT[i].ImportTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = faIncrement.Reason,
                        Description = lstDT[i].Description,
                        AccountingObjectID = faIncrement.AccountingObjectID,
                        EmployeeID = faIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = faIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = faIncrement.No,
                        RefDate = faIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        VATDescription = lstDT[i].VATDescription,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATNK);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = faIncrement.ID,
                        TypeID = faIncrement.TypeID,
                        Date = faIncrement.Date,
                        PostedDate = faIncrement.PostedDate,
                        No = faIncrement.No,
                        InvoiceDate = lstDT[i].InvoiceDate,
                        InvoiceNo = lstDT[i].InvoiceNo,
                        Account = lstDT[i].ImportTaxAccount,
                        AccountCorresponding = lstDT[i].DebitAccount,
                        BankAccountDetailID = faIncrement.BankAccountDetailID,
                        CurrencyID = faIncrement.CurrencyID,
                        ExchangeRate = faIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)lstDT[i].ImportTaxAmount,
                        CreditAmountOriginal = (decimal)lstDT[i].ImportTaxAmountOriginal,
                        Reason = faIncrement.Reason,
                        //Description = lstDT[i].VATDescription,
                        AccountingObjectID = faIncrement.AccountingObjectID,
                        EmployeeID = faIncrement.EmployeeID,
                        BudgetItemID = lstDT[i].BudgetItemID,
                        CostSetID = lstDT[i].CostSetID,
                        ContractID = lstDT[i].ContractID,
                        StatisticsCodeID = lstDT[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = faIncrement.MContactName,
                        DetailID = lstDT[i].ID,
                        RefNo = faIncrement.No,
                        RefDate = faIncrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = lstDT[i].ExpenseItemID,
                        IsIrrationalCost = lstDT[i].IsIrrationalCost,
                        VATDescription = lstDT[i].VATDescription
                    };
                    listGenTemp.Add(genTempVATCorresponding);
                    #endregion
                }
            }
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái ghi giảm TSCĐ
        /// </summary>
        /// <param name="faDecrement"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(FADecrement faDecrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<FADecrementDetail> lstDT = faDecrement.FADecrementDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                FADecrementDetail detail = lstDT[i];
                for (int j = 0; j < detail.FADecrementDetailPosts.Count; j++)
                {
                    FADecrementDetailPost post = detail.FADecrementDetailPosts[j];
                    GeneralLedger genTemp = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = faDecrement.ID,
                        TypeID = faDecrement.TypeID,
                        Date = faDecrement.Date,
                        PostedDate = faDecrement.PostedDate,
                        No = faDecrement.No,
                        InvoiceDate = null,
                        InvoiceNo = null,
                        Account = post.DebitAccount,
                        AccountCorresponding = post.CreditAccount,
                        CurrencyID = faDecrement.CurrencyID,
                        ExchangeRate = faDecrement.ExchangeRate,
                        DebitAmount = post.AmountOriginal,
                        DebitAmountOriginal = post.AmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = faDecrement.Reason,
                        Description = post.Description,
                        AccountingObjectID = post.AccountingObjectID,
                        EmployeeID = post.EmployeeID,
                        BudgetItemID = post.BudgetItemID,
                        CostSetID = post.CostSetID,
                        ContractID = post.ContractID,
                        StatisticsCodeID = post.StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = null,
                        DetailID = lstDT[i].ID,
                        RefNo = faDecrement.No,
                        RefDate = faDecrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = post.ExpenseItemID,
                        VATDescription = null,
                        IsIrrationalCost = post.IsIrrationalCost
                    };
                    listGenTemp.Add(genTemp);
                    GeneralLedger genTempCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = faDecrement.ID,
                        TypeID = faDecrement.TypeID,
                        Date = faDecrement.Date,
                        PostedDate = faDecrement.PostedDate,
                        No = faDecrement.No,
                        InvoiceDate = null,
                        InvoiceNo = null,
                        Account = post.CreditAccount,
                        AccountCorresponding = post.DebitAccount,
                        //BankAccountDetailID = faDecrement.BankAccountDetailID,
                        CurrencyID = faDecrement.CurrencyID,
                        ExchangeRate = faDecrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = post.AmountOriginal,
                        CreditAmountOriginal = post.AmountOriginal,
                        Reason = faDecrement.Reason,
                        Description = post.Description,
                        AccountingObjectID = post.AccountingObjectID,
                        EmployeeID = post.EmployeeID,
                        BudgetItemID = post.BudgetItemID,
                        CostSetID = post.CostSetID,
                        ContractID = post.ContractID,
                        //StatisticsCodeID = lstDT[i].StatisticCodeID,
                        InvoiceSeries = null,
                        ContactName = null,
                        DetailID = lstDT[i].ID,
                        RefNo = faDecrement.No,
                        RefDate = faDecrement.Date,
                        DepartmentID = lstDT[i].DepartmentID,
                        ExpenseItemID = post.ExpenseItemID,
                        StatisticsCodeID = post.StatisticsCodeID,
                        VATDescription = null,
                        IsIrrationalCost = post.IsIrrationalCost
                    };
                    listGenTemp.Add(genTempCorresponding);
                }
            }
            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái điều chỉnh TSCĐ
        /// </summary>
        /// <param name="faAdjustment"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(FAAdjustment faAdjustment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có
            List<FAAdjustmentDetail> lstDT = faAdjustment.FAAdjustmentDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faAdjustment.ID,
                    TypeID = faAdjustment.TypeID,
                    Date = faAdjustment.Date,
                    PostedDate = faAdjustment.PostedDate,
                    No = faAdjustment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    //BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = lstDT[i].Amount,
                    DebitAmountOriginal = lstDT[i].Amount,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = faAdjustment.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    //StatisticsCodeID = lstDT[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = faAdjustment.No,
                    RefDate = faAdjustment.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faAdjustment.ID,
                    TypeID = faAdjustment.TypeID,
                    Date = faAdjustment.Date,
                    PostedDate = faAdjustment.PostedDate,
                    No = faAdjustment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    //BankAccountDetailID = faAdjustment.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount,
                    CreditAmountOriginal = lstDT[i].Amount,
                    Reason = faAdjustment.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = null,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    //StatisticsCodeID = lstDT[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = faAdjustment.No,
                    RefDate = faAdjustment.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = lstDT[i].IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion
            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái Tính khấu hao TSCĐ
        /// </summary>
        /// <param name="faDepreciation"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(FADepreciation faDepreciation)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            List<FADepreciationPost> lstDT = faDepreciation.FADepreciationPosts.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faDepreciation.ID,
                    TypeID = faDepreciation.TypeID,
                    Date = faDepreciation.Date,
                    PostedDate = faDepreciation.PostedDate,
                    No = faDepreciation.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    //BankAccountDetailID = faDepreciation.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = lstDT[i].Amount ?? 0,
                    DebitAmountOriginal = lstDT[i].AmountOriginal ?? 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = faDepreciation.Reason,
                    Description = faDepreciation.Reason,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = faDepreciation.No,
                    RefDate = faDepreciation.Date,
                    DepartmentID = lstDT[i].ObjectID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = false
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faDepreciation.ID,
                    TypeID = faDepreciation.TypeID,
                    Date = faDepreciation.Date,
                    PostedDate = faDepreciation.PostedDate,
                    No = faDepreciation.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    //BankAccountDetailID = faDepreciation.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = lstDT[i].Amount ?? 0,
                    CreditAmountOriginal = lstDT[i].AmountOriginal ?? 0,
                    Reason = faDepreciation.Reason,
                    Description = faDepreciation.Reason,
                    AccountingObjectID = lstDT[i].AccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = lstDT[i].ID,
                    RefNo = faDepreciation.No,
                    RefDate = faDepreciation.Date,
                    DepartmentID = lstDT[i].ObjectID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = false
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion

            return listGenTemp;
        }

        /// <summary>
        /// Hàm tạo chứng từ sổ cái Tính khấu hao TSCĐ
        /// </summary>
        /// <param name="faDepreciation"></param>		
        /// <returns></returns>
        protected List<GeneralLedger> GenGeneralLedgers(FATransfer faTransfer)
        {
            //Chưa làm
            List<GeneralLedger> listReturn = new List<GeneralLedger>();
            return listReturn;
        }

        /// <summary>
        /// Lấy Tạo chứng từ sổ cái Chứng từ nghiệp vụ khác
        /// </summary>
        /// <param name="gOtherVoucher">Đối tượng GOtherVoucher tạm</param>
        public List<GeneralLedger> GenGeneralLedgers(GOtherVoucher gOtherVoucher)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            List<GOtherVoucherDetail> lstDT = gOtherVoucher.GOtherVoucherDetails.OrderBy(n => n.OrderPriority).ToList();
            for (int i = 0; i < lstDT.Count; i++)
            {
                #region Cặp Nợ - Có
                if (gOtherVoucher.TypeID == 840 && lstDT[i].Amount == 0 && lstDT[i].AmountOriginal == 0)
                    continue;
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = gOtherVoucher.ID,
                    TypeID = gOtherVoucher.TypeID,
                    Date = gOtherVoucher.Date,
                    PostedDate = gOtherVoucher.PostedDate,
                    No = gOtherVoucher.No,
                    Account = lstDT[i].DebitAccount,
                    AccountCorresponding = lstDT[i].CreditAccount,
                    CurrencyID = /*(lstDT[i].CurrencyID == null || string.IsNullOrEmpty(lstDT[i].CurrencyID)) ? "VND" : lstDT[i]*/gOtherVoucher.CurrencyID,
                    ExchangeRate = /*lstDT[i]*/gOtherVoucher.ExchangeRate,
                    DebitAmount = (decimal)lstDT[i].Amount,
                    DebitAmountOriginal = (decimal)lstDT[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = gOtherVoucher.Reason,
                    VATDescription = gOtherVoucher.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].DebitAccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    DetailID = lstDT[i].ID,
                    RefNo = gOtherVoucher.No,
                    RefDate = gOtherVoucher.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if (((lstDT[i].CreditAccount.StartsWith("133")) ||
                     lstDT[i].CreditAccount.StartsWith("33311")))
                {
                    genTemp.VATDescription = gOtherVoucher.GOtherVoucherDetailTaxs.Any(x => x.VATAccount == genTemp.AccountCorresponding) ? gOtherVoucher.GOtherVoucherDetailTaxs.FirstOrDefault(x => x.VATAccount == genTemp.AccountCorresponding).Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = gOtherVoucher.ID,
                    TypeID = gOtherVoucher.TypeID,
                    Date = gOtherVoucher.Date,
                    PostedDate = gOtherVoucher.PostedDate,
                    No = gOtherVoucher.No,
                    Account = lstDT[i].CreditAccount,
                    AccountCorresponding = lstDT[i].DebitAccount,
                    CurrencyID = /*(lstDT[i].CurrencyID == null || string.IsNullOrEmpty(lstDT[i].CurrencyID)) ? "VND" : lstDT[i]*/gOtherVoucher.CurrencyID,
                    ExchangeRate = /*lstDT[i]*/gOtherVoucher.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = (decimal)lstDT[i].Amount,
                    CreditAmountOriginal = (decimal)lstDT[i].AmountOriginal,
                    Reason = gOtherVoucher.Reason,
                    VATDescription = gOtherVoucher.Reason,
                    Description = lstDT[i].Description,
                    AccountingObjectID = lstDT[i].CreditAccountingObjectID,
                    EmployeeID = lstDT[i].EmployeeID,
                    BankAccountDetailID = lstDT[i].BankAccountDetailID,
                    BudgetItemID = lstDT[i].BudgetItemID,
                    CostSetID = lstDT[i].CostSetID,
                    ContractID = lstDT[i].ContractID,
                    StatisticsCodeID = lstDT[i].StatisticsCodeID,
                    DetailID = lstDT[i].ID,
                    RefNo = gOtherVoucher.No,
                    RefDate = gOtherVoucher.Date,
                    DepartmentID = lstDT[i].DepartmentID,
                    ExpenseItemID = lstDT[i].ExpenseItemID,
                    IsIrrationalCost = false
                };
                if (((lstDT[i].CreditAccount.StartsWith("133")) ||
                     lstDT[i].CreditAccount.StartsWith("33311")))
                {
                    genTempCorresponding.VATDescription = gOtherVoucher.GOtherVoucherDetailTaxs.Any(x => x.VATAccount == genTempCorresponding.Account) ? gOtherVoucher.GOtherVoucherDetailTaxs.FirstOrDefault(x => x.VATAccount == genTempCorresponding.Account).Description : null;
                }
                listGenTemp.Add(genTempCorresponding);

                #endregion
            }
            return listGenTemp;
        }
        #endregion

        #endregion

        #region Lưu Sổ

        /// <summary>
        /// Lưu sổ cái trong trường hợp một chứng từ
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool SaveLedger<T>(T input)
        {
            bool status = true;
            try
            {
                try
                {
                    if (_ISystemOptionService.Query.FirstOrDefault(x => x.ID == 89).Data == "Bình quân tức thời")
                        FormulaExchangeRate_AverageForInstantaneous(input);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Tính tỷ giá thất bại!", "Thông báo");
                }
                BeginTran();
                ProcessSaveLedger(input);
                CommitTran();
            }
            catch (Exception ex)
            {
                viewExcepiton(ex);
                RolbackTran();
                MessageBox.Show("Ghi sổ thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        private void viewExcepiton(Exception ex)
        {
            var message = ex.Message;
            var trace = ex.StackTrace;
        }

        public bool SaveLedgerNoTran<T>(T input, bool isMess = true)
        {

            bool status = true;
            try
            {
                try
                {
                    if (_ISystemOptionService.Query.FirstOrDefault(x => x.ID == 89).Data == "Bình quân tức thời")
                        FormulaExchangeRate_AverageForInstantaneous(input);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Tính tỷ giá thất bại!", "Thông báo");
                }
                ProcessSaveLedger(input);
            }
            catch (Exception ex)
            {
                viewExcepiton(ex);
                if (isMess) MessageBox.Show("Ghi sổ thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Lưu sổ cái trong trường hợp 2 chứng từ
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="input1"></param>
        /// <param name="input2"></param>
        /// <returns></returns>
        public bool SaveLedger<T1, T2>(T1 input1, T2 input2)
        {
            bool status = true;
            try
            {
                if (_ISystemOptionService.Query.FirstOrDefault(x => x.ID == 89).Data == "Bình quân tức thời")
                    FormulaExchangeRate_AverageForInstantaneous(input1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Tính tỷ giá thất bại!", "Thông báo");
            }
            try
            {
                BeginTran();
                ProcessSaveLedger(input1);
                ProcessSaveLedger(input2);
                CommitTran();
            }
            catch (Exception)
            {
                RolbackTran();
                MessageBox.Show("Ghi sổ thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        public bool SaveLedgerNoTran<T1, T2>(T1 input1, T2 input2, bool isMess = true)
        {
            bool status = true;
            try
            {
                if (_ISystemOptionService.Query.FirstOrDefault(x => x.ID == 89).Data == "Bình quân tức thời")
                    FormulaExchangeRate_AverageForInstantaneous(input1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Tính tỷ giá thất bại!", "Thông báo");
            }
            try
            {
                ProcessSaveLedger(input1);
                ProcessSaveLedger(input2);
            }
            catch (Exception ex)
            {
                viewExcepiton(ex);

                if (isMess) MessageBox.Show("Ghi sổ thất bại \r\n" + ex.ToString(), "Thông báo");
                status = false;
            }
            return status;
        }

        public void ProcessSaveLedger<T>(T input)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            List<FixedAssetLedger> listFixedAssetTemp = new List<FixedAssetLedger>();
            List<ToolLedger> listToolLedgerTemp = new List<ToolLedger>();
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            PropertyInfo propRecord = input.GetType().GetProperty("Recorded", BindingFlags.Public | BindingFlags.Instance);
            BaseService<T, Guid> testInvoke = this.GetIService(input);
            switch (input.GetType().ToString())
            {
                #region MCReceipt
                case "Accounting.Core.Domain.MCReceipt":
                    MCReceipt mcReceipt = (MCReceipt)(object)input;
                    listGenTemp = GenGeneralLedgers(mcReceipt);
                    switch (mcReceipt.TypeID)
                    {
                        //Phiếu thu thông thường
                        case 100:
                            break;
                        //Phiếu thu tiền khách hàng
                        case 101:
                            break;
                        //Phiếu thu từ bán hàng
                        case 102:
                            break;
                        //Phiếu thu từ bán hàng đại lý bán đúng giá, nhận ủy thác XNK
                        case 103:
                            break;
                    }
                    break;
                #endregion

                #region MCPayment
                case "Accounting.Core.Domain.MCPayment":
                    MCPayment mcPayment = (MCPayment)(object)input;
                    listGenTemp = GenGeneralLedgers(mcPayment);
                    switch (mcPayment.TypeID)
                    {
                        //Phiếu chi
                        case 110:
                            break;
                        //Phiếu chi trả lương
                        case 111:
                            break;
                        //Phiếu chi nộp thuế 
                        case 112:
                            break;
                        //Phiếu chi nộp thuế GTGT hàng NK
                        case 113:
                            break;
                        //Phiếu chi nộp thuế TNCN
                        case 114:
                            break;
                        //Phiếu chi nộp bảo hiểm
                        case 115:
                            break;
                        //Phiếu chi mua dịch vụ
                        case 116:
                            break;
                        //Phiếu chi mua hàng 
                        case 117:
                            break;
                        //Phiếu chi trả tiền NCC
                        case 118:
                            break;
                        //Phiếu chi mua TSCĐ
                        case 119:
                            listFixedAssetTemp = GenFixedAssetLedgers(mcPayment);
                            foreach (var fixedAssetLedger in listFixedAssetTemp)
                            {
                                _IFixedAssetLedgerService.CreateNew(fixedAssetLedger);
                            }
                            break;
                    }
                    break;
                #endregion

                #region MBDeposit
                case "Accounting.Core.Domain.MBDeposit":
                    MBDeposit mbDeposit = (MBDeposit)(object)input;
                    listGenTemp = GenGeneralLedgers(mbDeposit);
                    break;
                #endregion

                #region MBTellerPaper
                case "Accounting.Core.Domain.MBTellerPaper":
                    MBTellerPaper mbTellerPaper = (MBTellerPaper)(object)input;
                    listGenTemp = GenGeneralLedgers(mbTellerPaper);
                    switch (mbTellerPaper.TypeID)
                    {
                        //Ủy nhiệm chi
                        case 120:
                            break;
                        //Trả lương bằng Ủy nhiệm chi
                        case 121:
                            break;
                        //Nộp thuế bằng Ủy nhiệm chi 
                        case 122:
                            break;
                        //Nộp thuế GTGT hàng NK bằng Ủy nhiệm chi
                        case 123:
                            break;
                        //Nộp thuế TNCN bằng Ủy nhiệm chi
                        case 124:
                            break;
                        //Nộp bảo hiểm bằng Ủy nhiệm chi
                        case 125:
                            break;
                        //Mua dịch vụ bằng Ủy nhiệm chi
                        case 126:
                            break;
                        //Mua hàng bằng Ủy nhiệm chi 
                        case 127:
                            break;
                        //Trả tiền NCC bằng Ủy nhiệm chi
                        case 128:
                            break;
                        //Mua TSCĐ bằng Ủy nhiệm chi
                        case 129:
                            break;
                    }
                    break;
                #endregion

                #region MBCreditCard
                case "Accounting.Core.Domain.MBCreditCard":
                    MBCreditCard mbCreditCard = (MBCreditCard)(object)input;
                    listGenTemp = GenGeneralLedgers(mbCreditCard);
                    break;
                #endregion

                #region MBInternalTransfer
                case "Accounting.Core.Domain.MBInternalTransfer":
                    MBInternalTransfer mbInternalTransfer = (MBInternalTransfer)(object)input;
                    listGenTemp = GenGeneralLedgers(mbInternalTransfer);
                    break;
                #endregion

                #region SAInvoice
                case "Accounting.Core.Domain.SAInvoice":
                    SAInvoice saInvoice = (SAInvoice)(object)input;
                    listGenTemp = GenGeneralLedgers(saInvoice);
                    //Lưu sổ kho trong trường hợp kiêm phiếu xuất
                    if (saInvoice.OutwardNo != null)
                    {
                        listReposTemp = GenRepositoryLedgers(saInvoice);
                    }
                    if (saInvoice.OutwardNo != null && saInvoice.SAInvoiceDetails.Count > 0)
                    {
                        foreach (var x in saInvoice.SAInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    _IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt + x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt + (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal + (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    _IEMContractDetailMGService.Update(md);
                                    _IEMContractDetailMGService.CommitTran();
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region SAReturn
                case "Accounting.Core.Domain.SAReturn":
                    SAReturn saReturn = (SAReturn)(object)input;
                    listGenTemp = GenGeneralLedgers(saReturn);
                    listReposTemp = GenRepositoryLedgers(saReturn);
                    if (saReturn.SAReturnDetails.Count > 0)
                    {
                        foreach (var x in saReturn.SAReturnDetails)
                        {
                            if (x.SAInvoiceDetailID != null)
                            {
                                var sadetail = _ISAInvoiceDetailService.Getbykey(x.SAInvoiceDetailID ?? Guid.Empty);
                                if (sadetail != null && sadetail.ContractID != null)
                                {
                                    EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                    if (md != null)
                                    {
                                        _IEMContractDetailMGService.BeginTran();
                                        md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                        md.AmountReceipt = md.AmountReceipt - (x.Amount + x.VATAmount - x.DiscountAmount);
                                        md.AmountReceiptOriginal = md.AmountReceiptOriginal - (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                        _IEMContractDetailMGService.Update(md);
                                        _IEMContractDetailMGService.CommitTran();
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region PPInvoice
                case "Accounting.Core.Domain.PPInvoice":
                    PPInvoice ppInvoice = (PPInvoice)(object)input;
                    listGenTemp = GenGeneralLedgers(ppInvoice);
                    switch (ppInvoice.TypeID)
                    {
                        //Mua hàng chưa thanh toán
                        case 210:
                            break;
                        //Hàng mua trả lại
                        case 220:
                            break;
                        //Hàng mua giảm giá
                        case 230:
                            break;
                        //Mua dịch vụ chưa thanh toán
                        case 240:
                            break;
                        //Trả tiền nhà cung cấp
                        case 250:
                            break;
                        //Hóa đơn mua hàng(TT Tiền mặt)
                        case 260:
                            break;
                        //Hóa đơn mua hàng(TT Ủy nhiệm chi)
                        case 261:
                            MBTellerPaper mbTellerPaperPp = _IMBTellerPaperService.Getbykey(ppInvoice.ID);
                            if (mbTellerPaperPp != null)
                            {
                                mbTellerPaperPp.Recorded = true;
                                _IMBTellerPaperService.Update(mbTellerPaperPp);
                            }
                            break;
                        //Hóa đơn mua hàng(Chuyển khoản)
                        case 262:
                            break;
                        //Hóa đơn mua hàng(TT Séc tiền mặt)
                        case 263:
                            break;
                        //Hóa đơn mua hàng(TT Thẻ tín dụng)
                        case 264:
                            break;
                    }
                    //Trường hợp mua hàng qua kho thì ghi chứng từ vào sổ kho
                    if (ppInvoice.StoredInRepository == true)
                    {
                        listReposTemp = GenRepositoryLedgers(ppInvoice);
                    }
                    if (ppInvoice.PPInvoiceDetails.Count > 0)
                    {
                        foreach (var x in ppInvoice.PPInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    _IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt + x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt + (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal + (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    _IEMContractDetailMGService.Update(md);
                                    _IEMContractDetailMGService.CommitTran();
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region PPDiscountReturn
                case "Accounting.Core.Domain.PPDiscountReturn":
                    PPDiscountReturn ppDiscountReturn = (PPDiscountReturn)(object)input;
                    listGenTemp = GenGeneralLedgers(ppDiscountReturn);
                    listReposTemp = GenRepositoryLedgers(ppDiscountReturn);
                    if (ppDiscountReturn.PPDiscountReturnDetails.Count > 0)
                    {
                        foreach (var x in ppDiscountReturn.PPDiscountReturnDetails)
                        {
                            if (x.ConfrontDetailID != null)
                            {
                                var ppdetail = IPPInvoiceDetailService.Getbykey(x.ConfrontDetailID ?? Guid.Empty);
                                if (ppdetail != null && ppdetail.ContractID != null)
                                {
                                    EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                    if (md != null)
                                    {
                                        _IEMContractDetailMGService.BeginTran();
                                        md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                        md.AmountReceipt = md.AmountReceipt - ((x.Amount ?? 0) + (x.VATAmount ?? 0));
                                        md.AmountReceiptOriginal = md.AmountReceiptOriginal - ((x.AmountOriginal ?? 0) + (x.VATAmountOriginal ?? 0));
                                        _IEMContractDetailMGService.Update(md);
                                        _IEMContractDetailMGService.CommitTran();
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region PPService
                case "Accounting.Core.Domain.PPService":
                    PPService ppService = (PPService)(object)input;
                    listGenTemp = GenGeneralLedgers(ppService);
                    break;
                #endregion

                #region RSInwardOutward
                case "Accounting.Core.Domain.RSInwardOutward":
                    RSInwardOutward rsInwardOutward = (RSInwardOutward)(object)input;
                    listGenTemp = GenGeneralLedgers(rsInwardOutward);
                    listReposTemp = GenRepositoryLedgers(rsInwardOutward);
                    break;
                #endregion

                #region RSAssemblyDismantlement
                //case "Accounting.Core.Domain.RSAssemblyDismantlement":
                //    RSAssemblyDismantlement rsAssemblyDismantlement = (RSAssemblyDismantlement)(object)input;
                //    listGenTemp = GenGeneralLedgers(rsAssemblyDismantlement);
                //    listReposTemp = GenRepositoryLedgers(rsAssemblyDismantlement);
                //    break;
                #endregion

                #region RSTransfer
                case "Accounting.Core.Domain.RSTransfer":
                    RSTransfer rsTransfer = (RSTransfer)(object)input;
                    listGenTemp = GenGeneralLedgers(rsTransfer);
                    listReposTemp = GenRepositoryLedgers(rsTransfer);
                    break;
                #endregion

                #region GOtherVoucher
                case "Accounting.Core.Domain.GOtherVoucher":
                    GOtherVoucher gOtherVoucher = (GOtherVoucher)(object)input;
                    listGenTemp = GenGeneralLedgers(gOtherVoucher);
                    break;
                #endregion

                #region FAIncrement
                case "Accounting.Core.Domain.FAIncrement":
                    FAIncrement faIncrement = (FAIncrement)(object)input;
                    listGenTemp = GenGeneralLedgers(faIncrement);
                    listFixedAssetTemp = GenFixedAssetLedgers(faIncrement);
                    break;
                #endregion

                #region FADecrement
                case "Accounting.Core.Domain.FADecrement":
                    FADecrement faDecrement = (FADecrement)(object)input;
                    listGenTemp = GenGeneralLedgers(faDecrement);
                    listFixedAssetTemp = GenFixedAssetLedgers(faDecrement);
                    break;
                #endregion

                #region FAAdjustment
                case "Accounting.Core.Domain.FAAdjustment":
                    FAAdjustment faAdjustment = (FAAdjustment)(object)input;
                    listGenTemp = GenGeneralLedgers(faAdjustment);
                    listFixedAssetTemp = GenFixedAssetLedgers(faAdjustment);
                    break;
                #endregion

                #region FADepreciation
                case "Accounting.Core.Domain.FADepreciation":
                    FADepreciation faDepreciation = (FADepreciation)(object)input;
                    listGenTemp = GenGeneralLedgers(faDepreciation);
                    listFixedAssetTemp = GenFixedAssetLedgers(faDepreciation);
                    break;
                #endregion

                #region FATransfer
                case "Accounting.Core.Domain.FATransfer":
                    FATransfer faTransfer = (FATransfer)(object)input;
                    listGenTemp = GenGeneralLedgers(faTransfer);
                    listFixedAssetTemp = GenFixedAssetLedgers(faTransfer);
                    break;
                #endregion

                #region TIIncrement
                case "Accounting.Core.Domain.TIIncrement":
                    TIIncrement tiIncrement = (TIIncrement)(object)input;
                    listGenTemp = GenGeneralLedgers(tiIncrement);
                    listToolLedgerTemp = GenToolLedgers(tiIncrement);
                    break;
                #endregion

                #region TIDecrement
                case "Accounting.Core.Domain.TIDecrement":
                    TIDecrement tiDecrement = (TIDecrement)(object)input;
                    //listGenTemp = GenGeneralLedgers(tiDecrement);
                    listToolLedgerTemp = GenToolLedgers(tiDecrement);
                    break;
                #endregion

                #region TIAdjustment
                case "Accounting.Core.Domain.TIAdjustment":
                    TIAdjustment tiAdjustment = (TIAdjustment)(object)input;
                    //listGenTemp = GenGeneralLedgers(tiAdjustment);
                    listToolLedgerTemp = GenToolLedgers(tiAdjustment);
                    break;
                #endregion

                #region TITransfer
                case "Accounting.Core.Domain.TITransfer":
                    TITransfer TITransfer = (TITransfer)(object)input;
                    //listGenTemp = GenGeneralLedgers(TITransfer);
                    listToolLedgerTemp = GenToolLedgers(TITransfer);
                    break;
                #endregion

                #region TIAllocation
                case "Accounting.Core.Domain.TIAllocation":
                    TIAllocation TIAllocation = (TIAllocation)(object)input;
                    listGenTemp = GenGeneralLedgers(TIAllocation);
                    listToolLedgerTemp = GenToolLedgers(TIAllocation);
                    break;
                    #endregion
            }
            //if ((listGenTemp != null) && (listGenTemp.Count > 0))
            //{
            //    if (!_IAccountService.Query.FirstOrDefault(x => x.AccountNumber == listGenTemp[0].Account).IsForeignCurrency && listGenTemp[0].CurrencyID != "VND")
            //    {
            //        MessageBox.Show("Ghi sổ thất bại! Tài khoản hạch toán không chi tiết theo ngoại tệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }
            //}
            if (null != propRecord && propRecord.CanWrite)
            {
                propRecord.SetValue(input, true, null);
            }
            testInvoke.Update(input);
            if ((listGenTemp != null) && (listGenTemp.Count > 0))
            {
                foreach (var generalLedger in listGenTemp)
                {
                    CreateNew(generalLedger);
                }
            }
            if ((listReposTemp != null) && (listReposTemp.Count > 0))
            {
                foreach (var repositoryLedger in listReposTemp)
                {
                    _IRepositoryLedgerService.CreateNew(repositoryLedger);
                }
            }
            if ((listFixedAssetTemp != null) && (listFixedAssetTemp.Count > 0))
            {
                foreach (var fixedAssetLedger in listFixedAssetTemp)
                {
                    _IFixedAssetLedgerService.CreateNew(fixedAssetLedger);
                }
            }
            if ((listToolLedgerTemp != null) && (listToolLedgerTemp.Count > 0))
            {
                foreach (var fixedAssetLedger in listToolLedgerTemp)
                {
                    _IToolLedgerService.CreateNew(fixedAssetLedger);
                }
            }
        }
        #endregion

        #region Xóa chứng từ trong sổ cái

        /// <summary>
        /// Xóa chứng từ trong sổ
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        public bool RemoveLedger<T>(T input)
        {
            bool status = true;
            try
            {
                BeginTran();
                ProcessRemoveLedger(input);
                CommitTran();

            }
            catch (Exception ex)
            {
                RolbackTran();
                MessageBox.Show("Bỏ ghi thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        public bool RemoveLedgerNoTran<T>(T input, bool isMess = true)
        {
            bool status = true;
            try
            {
                ProcessRemoveLedger(input);
            }
            catch (Exception ex)
            {
                if (isMess) MessageBox.Show("Bỏ ghi thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        public bool RemoveLedger<T1, T2>(T1 input1, T2 input2)
        {
            bool status = true;
            try
            {
                BeginTran();
                ProcessRemoveLedger(input1);
                ProcessRemoveLedger(input2);
                CommitTran();

            }
            catch (Exception ex)
            {
                RolbackTran();
                MessageBox.Show("Bỏ ghi thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        public bool RemoveLedgerNoTran<T1, T2>(T1 input1, T2 input2, bool isMess = true)
        {
            bool status = true;
            try
            {
                ProcessRemoveLedger(input1);
                ProcessRemoveLedger(input2);
            }
            catch (Exception ex)
            {
                if (isMess) MessageBox.Show("Bỏ ghi thất bại", "Thông báo");
                status = false;
            }
            return status;
        }

        public void ProcessRemoveLedger<T>(T input)
        {
            PropertyInfo propRecord = input.GetType()
                   .GetProperty("Recorded", BindingFlags.Public | BindingFlags.Instance);
            if (null != propRecord && propRecord.CanWrite)
            {
                propRecord.SetValue(input, false, null);
            }
            switch (input.GetType().ToString())
            {
                #region PPInvoice
                case "Accounting.Core.Domain.PPInvoice":
                    PPInvoice ppInvoice = (PPInvoice)(object)input;
                    switch (ppInvoice.TypeID)
                    {
                        //Mua hàng chưa thanh toán
                        case 210:
                            break;
                        //Hàng mua trả lại
                        case 220:
                            break;
                        //Hàng mua giảm giá
                        case 230:
                            break;
                        //Mua dịch vụ chưa thanh toán
                        case 240:
                            break;
                        //Trả tiền nhà cung cấp
                        case 250:
                            break;
                        //Hóa đơn mua hàng(TT Tiền mặt)
                        case 260:
                            break;
                        //Hóa đơn mua hàng(TT Ủy nhiệm chi)
                        case 261:
                            MBTellerPaper mbTellerPaper = _IMBTellerPaperService.Getbykey(ppInvoice.ID);
                            if (mbTellerPaper != null)
                            {
                                mbTellerPaper.Recorded = false;
                                _IMBTellerPaperService.Update(mbTellerPaper);
                            }
                            break;
                        //Hóa đơn mua hàng(Chuyển khoản)
                        case 262:
                            break;
                        //Hóa đơn mua hàng(TT Séc tiền mặt)
                        case 263:
                            break;
                        //Hóa đơn mua hàng(TT Thẻ tín dụng)
                        case 264:
                            break;
                    }
                    if (ppInvoice.PPInvoiceDetails.Count > 0)
                    {
                        foreach (var x in ppInvoice.PPInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    _IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt - (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal - (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    _IEMContractDetailMGService.Update(md);
                                    _IEMContractDetailMGService.CommitTran();
                                }
                            }
                        }
                    }
                    break;
                case "Accounting.Core.Domain.SAInvoice":
                    SAInvoice saInvoice = (SAInvoice)(object)input;
                    if (saInvoice.SAInvoiceDetails.Count > 0)
                    {
                        foreach (var x in saInvoice.SAInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    _IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt - (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal - (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    _IEMContractDetailMGService.Update(md);
                                    _IEMContractDetailMGService.CommitTran();
                                }
                            }
                        }
                    }
                    break;
                    #endregion
            }
            List<GeneralLedger> listGenTemp = null;
            List<FixedAssetLedger> listFixTemp = null;
            List<RepositoryLedger> listRepTemp = null;
            List<ToolLedger> listToolLedgerTemp = null;
            PropertyInfo propId = input.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(input, null);
                if (temp is Guid)
                {
                    listGenTemp = GetByReferenceID((Guid)temp);
                    listFixTemp = _IFixedAssetLedgerService.GetByReferenceID((Guid)temp);
                    listRepTemp = _IRepositoryLedgerService.GetByListReferenceID((Guid)temp);
                    listToolLedgerTemp = _IToolLedgerService.GetByListReferenceID((Guid)temp);
                }
            }
            if ((listGenTemp != null) && (listGenTemp.Count > 0))
            {
                foreach (var generalLedger in listGenTemp)
                {
                    Delete(generalLedger);
                }
            }
            if ((listFixTemp != null) && (listFixTemp.Count > 0))
            {
                foreach (var fixedAssetLedger in listFixTemp)
                {
                    _IFixedAssetLedgerService.Delete(fixedAssetLedger);
                }
            }
            if ((listRepTemp != null) && (listRepTemp.Count > 0))
            {
                foreach (var repositoryLedger in listRepTemp)
                {
                    _IRepositoryLedgerService.Delete(repositoryLedger);
                }
            }
            if ((listToolLedgerTemp != null) && (listToolLedgerTemp.Count > 0))
            {
                foreach (var repositoryLedger in listToolLedgerTemp)
                {
                    _IToolLedgerService.Delete(repositoryLedger);
                }
            }
            //string interfaceName = input.GetType().ToString();
            //interfaceName = interfaceName.Replace("Domain.", "IService.I") + "Service";
            //System.Type tmpType = System.Type.GetType(interfaceName);
            //var method = typeof(IoC).GetMethod("Resolve", new System.Type[0]).MakeGenericMethod(tmpType);
            BaseService<T, Guid> testInvoke = this.GetIService(input);

            testInvoke.Update(input);
        }
        //Add by Hautv
        public List<object> ProcessRemoveLedger_XLCT<T>(T input)
        {
            List<object> lstObj = new List<object>();
            PropertyInfo propRecord = input.GetType()
                   .GetProperty("Recorded", BindingFlags.Public | BindingFlags.Instance);
            if (null != propRecord && propRecord.CanWrite)
            {
                propRecord.SetValue(input, false, null);
            }
            switch (input.GetType().ToString())
            {
                #region PPInvoice
                case "Accounting.Core.Domain.PPInvoice":
                    PPInvoice ppInvoice = (PPInvoice)(object)input;
                    switch (ppInvoice.TypeID)
                    {
                        //Mua hàng chưa thanh toán
                        case 210:
                            break;
                        //Hàng mua trả lại
                        case 220:
                            break;
                        //Hàng mua giảm giá
                        case 230:
                            break;
                        //Mua dịch vụ chưa thanh toán
                        case 240:
                            break;
                        //Trả tiền nhà cung cấp
                        case 250:
                            break;
                        //Hóa đơn mua hàng(TT Tiền mặt)
                        case 260:
                            break;
                        //Hóa đơn mua hàng(TT Ủy nhiệm chi)
                        case 261:
                            MBTellerPaper mbTellerPaper = _IMBTellerPaperService.Getbykey(ppInvoice.ID);
                            if (mbTellerPaper != null)
                            {
                                mbTellerPaper.Recorded = false;
                                //_IMBTellerPaperService.Update(mbTellerPaper);
                                lstObj.Add(mbTellerPaper);
                            }
                            break;
                        //Hóa đơn mua hàng(Chuyển khoản)
                        case 262:
                            break;
                        //Hóa đơn mua hàng(TT Séc tiền mặt)
                        case 263:
                            break;
                        //Hóa đơn mua hàng(TT Thẻ tín dụng)
                        case 264:
                            break;
                    }
                    if (ppInvoice.PPInvoiceDetails.Count > 0)
                    {
                        foreach (var x in ppInvoice.PPInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    //_IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt - (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal - (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    //_IEMContractDetailMGService.Update(md);
                                    //_IEMContractDetailMGService.CommitTran();
                                    lstObj.Add(md);
                                }
                            }
                        }
                    }
                    break;
                case "Accounting.Core.Domain.SAInvoice":
                    SAInvoice saInvoice = (SAInvoice)(object)input;
                    if (saInvoice.SAInvoiceDetails.Count > 0)
                    {
                        foreach (var x in saInvoice.SAInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    //_IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt - (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal - (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    //_IEMContractDetailMGService.Update(md);
                                    //_IEMContractDetailMGService.CommitTran();
                                    lstObj.Add(md);
                                }
                            }
                        }
                    }
                    break;
                    #endregion
            }
            List<GeneralLedger> listGenTemp = null;
            List<FixedAssetLedger> listFixTemp = null;
            List<RepositoryLedger> listRepTemp = null;
            List<ToolLedger> listToolLedgerTemp = null;
            PropertyInfo propId = input.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(input, null);
                if (temp is Guid)
                {
                    listGenTemp = GetByReferenceID((Guid)temp);
                    listFixTemp = _IFixedAssetLedgerService.GetByReferenceID((Guid)temp);
                    listRepTemp = _IRepositoryLedgerService.GetByListReferenceID((Guid)temp);
                    listToolLedgerTemp = _IToolLedgerService.GetByListReferenceID((Guid)temp);
                }
            }
            if ((listGenTemp != null) && (listGenTemp.Count > 0))
            {
                //foreach (var generalLedger in listGenTemp)
                //{
                //    Delete(generalLedger);
                //}
                lstObj.AddRange(listGenTemp);
            }
            if ((listFixTemp != null) && (listFixTemp.Count > 0))
            {
                //foreach (var fixedAssetLedger in listFixTemp)
                //{
                //    _IFixedAssetLedgerService.Delete(fixedAssetLedger);
                //}
                lstObj.AddRange(listFixTemp);
            }
            if ((listRepTemp != null) && (listRepTemp.Count > 0))
            {
                //foreach (var repositoryLedger in listRepTemp)
                //{
                //    _IRepositoryLedgerService.Delete(repositoryLedger);
                //}
                lstObj.AddRange(listRepTemp);
            }
            if ((listToolLedgerTemp != null) && (listToolLedgerTemp.Count > 0))
            {
                //foreach (var repositoryLedger in listToolLedgerTemp)
                //{
                //    _IToolLedgerService.Delete(repositoryLedger);
                //}
                lstObj.AddRange(listToolLedgerTemp);
            }
            //string interfaceName = input.GetType().ToString();
            //interfaceName = interfaceName.Replace("Domain.", "IService.I") + "Service";
            //System.Type tmpType = System.Type.GetType(interfaceName);
            //var method = typeof(IoC).GetMethod("Resolve", new System.Type[0]).MakeGenericMethod(tmpType);
            //BaseService<T, Guid> testInvoke = this.GetIService(input);
            return lstObj;
            //testInvoke.Update(input);
        }

        #endregion

        #endregion
        #region Số tiền gửi ngân hàng DUNGNA

        /// <summary>
        /// Báo cáo số tiền gửi ngân hàng theo danh sách tài khoản
        /// [DungNA]
        /// </summary>
        /// <param name="ListAccount"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="CurrencyID1"></param>
        /// <param name="BankAccounDetailID1"></param>
        /// <param name="TotalGroup"></param>
        /// <returns></returns>
        public List<S08DNN> ReportS06DNN(List<string> ListAccount, DateTime fromDate, DateTime ToDate, string CurrencyID1, Guid BankAccounDetailID1, bool TotalGroup)
        {
            List<S08DNN> list = new List<S08DNN>();
            foreach (var x in ListAccount)
            {
                List<S08DNN> list1 = ReportBankS06DNN(x, fromDate, ToDate, CurrencyID1, BankAccounDetailID1, TotalGroup);
                list.AddRange(list1);
            }
            return list;
        }

        /// <summary>
        /// [DungNA]
        /// Hàm lấy ra báo cáo theo tài khoản 
        /// </summary>
        /// <param name="Account1"></param>
        /// <param name="Date1"></param>
        /// <param name="PostedDate1"></param>
        /// <param name="CurrencyID1"></param>
        /// <param name="BankAccounDetailID1"></param>
        /// <param name="TotalGroup"></param>
        /// <returns></returns>
        public List<S08DNN> ReportBankS06DNN(string Account1, DateTime Date1, DateTime PostedDate1, string CurrencyID1, Guid BankAccounDetailID1, bool TotalGroup)
        {
            var listkq = new List<S08DNN>();
            var thongke = (from a in Query.ToList()
                           join g in _IBankAccountDetailService.Query.ToList() on a.BankAccountDetailID equals g.ID
                           where a.PostedDate <= PostedDate1
                           && a.BankAccountDetailID != null
                           && a.Account != null
                           && a.Account == Account1
                           && a.CurrencyID != null
                           && a.CurrencyID == CurrencyID1
                           //&& a.Account.StartsWith(Account1)
                           && (BankAccounDetailID1 == Guid.Empty ? true : a.BankAccountDetailID == BankAccounDetailID1)
                           && !a.TypeID.ToString().StartsWith("70")
                           select new S08DNN
                           {
                               AccountNumber = a.Account,//Loại Tài khoản
                               Date = a.PostedDate,//Ngày tháng ghi sổ
                               No = a.DebitAmount == 0 && a.CreditAmount == 0 ? "" : a.No,//Số hiệu chứng từ
                               PostedDate = a.Date,//Ngày Chứng từ
                               AccountCorresponding = a.AccountCorresponding,//Tài khoản đối ứng
                               DebitAmount = a.DebitAmount,//Thu
                               CreditAmount = a.CreditAmount,//Chi
                               Reason = a.Reason,//Diễn giải khi cộng gộp
                               Description = a.Description,//Diễn giải khi không cộng gộp với từng tài khoản
                               BankName = g.BankName,//Tên ngân hàng
                               BankAccount = g.BankAccount,//Số tài khoản
                               Hyperlink = a.TypeID + ";" + a.ReferenceID,
                               OrderType = 0,
                           });

            if (TotalGroup == false)
            {
                foreach (var x in thongke)
                {
                    var addlist = new S08DNN();
                    if (
                                thongke.Any(c => c.No == x.No &&
                        (c.DebitAmount == x.CreditAmount || c.CreditAmount == x.DebitAmount)))
                    {
                        continue;
                    }
                    addlist.AccountNumber = x.AccountNumber;
                    addlist.Date = x.Date;
                    addlist.No = x.No;
                    addlist.PostedDate = x.PostedDate;
                    addlist.AccountCorresponding = x.AccountCorresponding;
                    addlist.DebitAmount = x.DebitAmount;
                    addlist.CreditAmount = x.CreditAmount;
                    addlist.Description = x.Description;
                    addlist.BankName = x.BankName;
                    addlist.BankAccount = x.BankAccount;
                    addlist.Hyperlink = x.Hyperlink;
                    addlist.Note = "";
                    listkq.Add(addlist);
                }
            }
            else   //Sử dụng group by 2 cột để lọc giá trị gộp các bút toán giống nhau
            {
                var ReportGroup = from p in listkq
                                  group p by new
                                  {
                                      p.No,
                                      p.AccountCorresponding,
                                  } into rp
                                  select new S08DNN
                                  {
                                      No = rp.Key.No,
                                      AccountCorresponding = rp.Key.AccountCorresponding,
                                      Reason = rp.Select(c => c.Reason).ToString(),
                                      DebitAmount = rp.Sum(c => c.DebitAmount),
                                      CreditAmount = rp.Sum(c => c.CreditAmount),
                                  };
                foreach (var y in ReportGroup)
                {
                    var addlist = new S08DNN();
                    if (
                                thongke.Any(c => c.No == y.No &&
                        (c.DebitAmount == y.CreditAmount || c.CreditAmount == y.DebitAmount)))
                    {
                        continue;
                    }
                    addlist.AccountNumber = y.AccountNumber;
                    addlist.Date = y.Date;
                    addlist.No = y.No;
                    addlist.PostedDate = y.PostedDate;
                    addlist.AccountCorresponding = y.AccountCorresponding;
                    addlist.DebitAmount = y.DebitAmount;
                    addlist.CreditAmount = y.CreditAmount;
                    addlist.Description = y.Reason;//Cột lý do được thay đổi khi click vào nút cộng gộp thì lý do được đổi sang reason để nhóm lại
                    addlist.BankName = y.BankName;
                    addlist.BankAccount = y.BankAccount;
                    addlist.Hyperlink = y.Hyperlink;
                    addlist.Note = "";
                    listkq.Add(addlist);
                }
            }
            S08DNN Abc = new S08DNN();
            Abc.Description = "Số Phát Sinh Trong Kỳ";//Add thêm dòng số phát sinh trong kỳ
            Abc.AccountNumber = Account1;
            Abc.OrderType = -1;
            Abc.AccountNumber = "";
            Abc.AccountCorresponding = "";
            Abc.BankAccount = "";
            Abc.BankName = "";
            Abc.No = "";
            Abc.Hyperlink = "";
            listkq.Add(Abc);
            S08DNN OpeningBalance = new S08DNN();//Số dư đầu kỳ
            if (Account1.StartsWith("1"))
            {
                OpeningBalance.OpeningDebit = GetOpeningBalanceDD(Date1, CurrencyID1, Account1);
            }
            else if (Account1.StartsWith("2"))
            {
                OpeningBalance.OpeningCredit = GetOpeningBalanceDD(Date1, CurrencyID1, Account1);
            }
            OpeningBalance.AccountNumber = Account1;
            OpeningBalance.AccountNumber = "";
            OpeningBalance.AccountCorresponding = "";
            OpeningBalance.BankAccount = "";
            OpeningBalance.BankName = "";
            OpeningBalance.No = "";
            OpeningBalance.Description = "Số dư đầu kỳ";
            OpeningBalance.Hyperlink = "";
            listkq.Add(OpeningBalance); // add thêm dòng sô dư đầu kỳ          
            return listkq.OrderBy(c => c.Date).ToList();
        }
        #endregion
        #region Recevied Journal Books(DungNA Sổ Nhật Ký Thu Tiền)
        /// <summary>
        /// [DungNA]
        /// </summary>
        /// <param name="from">từ ngày</param>
        /// <param name="To">Đến Ngày</param>
        /// <param name="Account">Loại Tài Khoản</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="GroupAA"> Nút cộng gộp</param>
        /// <returns>Danh Sách các tài khoản tương ứng với loại tại khoản truyền vào</returns>
        public List<S03A1DNN> RS03A1DNN(DateTime from, DateTime To, string Account, string CurrencyID, bool GroupAA)
        {
            var LstRS03A1 = (from a in Query.ToList()
                             where a.Account != null
                             && a.AccountCorresponding != null
                             && a.CurrencyID != null
                             && a.Description != null
                             && a.Reason != null
                             && a.DebitAmount != 0
                             && a.PostedDate <= To
                             && a.Account == Account
                             && a.CurrencyID == CurrencyID
                             select new S03A1DNN
                             {
                                 PostedDate = a.PostedDate,
                                 RefNo = a.No,
                                 RefDate = a.Date,
                                 Description = a.Description,
                                 Reason = a.Reason,
                                 DebitAmount = a.DebitAmount,
                                 AccountCorresponding = a.AccountCorresponding//Tài Khoản Đối ứng
                             });

            List<string> lst5Acc = LstRS03A1.Select(c => c.AccountCorresponding).Distinct().ToList();//Lấy ra list danh sách tài khoản đối ứng
            if (lst5Acc.Count > 5) lst5Acc = lst5Acc.Take(5).ToList();//Nếu tài khoản đối ứng lớn hơn 5 thì lấy ra 5 tài khoản đầu tiền
            string lstAccNum = string.Join(",", lst5Acc.ToArray());//Đổi list thành 1 chuỗi và tách nhau bởi dấu phẩy
            lstAccNum = Account + "," + lstAccNum;//Loại tài khoản cộng với 
            if (GroupAA == false)
            {
                foreach (var x in LstRS03A1)
                {
                    if (lst5Acc.Count >= 0 && x.AccountCorresponding == lst5Acc[0])
                    {
                        x.DebitAmount1 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 1 && x.AccountCorresponding == lst5Acc[1])
                    {
                        x.DebitAmount2 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 2 && x.AccountCorresponding == lst5Acc[2])
                    {
                        x.DebitAmount3 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 3 && x.AccountCorresponding == lst5Acc[3])
                    {
                        x.DebitAmount4 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 4 && x.AccountCorresponding == lst5Acc[4])
                    {
                        x.DebitAmount5 = x.DebitAmount;
                    }
                    else
                    {
                        x.DebitAmount6 = x.DebitAmount;
                        x.AccountNumber6 = x.AccountCorresponding;
                    }
                    x.ListAccount = lstAccNum;
                }
            }
            else
            {
                var group = from a in LstRS03A1
                            group a by new
                            {
                                a.RefNo,
                                a.AccountCorresponding,
                            } into ga
                            select new S03A1DNN
                            {
                                RefNo = ga.Key.RefNo,
                                AccountCorresponding = ga.Key.AccountCorresponding,
                                Description = ga.Select(c => c.Reason).ToString(),
                            };
                foreach (var x in group)
                {
                    if (lst5Acc.Count >= 0 && x.AccountCorresponding == lst5Acc[0])
                    {
                        x.DebitAmount1 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 1 && x.AccountCorresponding == lst5Acc[1])
                    {
                        x.DebitAmount2 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 2 && x.AccountCorresponding == lst5Acc[2])
                    {
                        x.DebitAmount3 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 3 && x.AccountCorresponding == lst5Acc[3])
                    {
                        x.DebitAmount4 = x.DebitAmount;
                    }
                    else if (lst5Acc.Count >= 4 && x.AccountCorresponding == lst5Acc[4])
                    {
                        x.DebitAmount5 = x.DebitAmount;
                    }
                    else
                    {
                        x.DebitAmount6 = x.DebitAmount;
                        x.AccountNumber6 = x.AccountCorresponding;
                    }
                    x.ListAccount = lstAccNum;
                }

            }

            return LstRS03A1.ToList();
        }
        #endregion
        #region PayMent Journal Books(DungNA Sổ Nhật Ký Chi Tiền)
        /// <summary>
        /// [DungNA]
        /// Sổ nhật ký thu tiền
        /// </summary>
        /// <param name="from">Từ ngày</param>
        /// <param name="To">Đến Ngày</param>
        /// <param name="Account"></param>
        /// <param name="CurrencyID"></param>
        /// <param name="GroupAA"></param>
        /// <returns></returns>
        public List<S03A2DNN> RS03A2DNN(DateTime from, DateTime To, string Account, string CurrencyID, bool GroupAA)
        {
            var LstRS03A2 = (from a in Query.ToList()
                             where a.Account != null
                             && a.AccountCorresponding != null
                             && a.CurrencyID != null
                             && a.Description != null
                             && a.Reason != null
                             && a.CreditAmount != 0
                             && a.PostedDate <= To
                             && a.Account == Account
                             && a.CurrencyID == CurrencyID
                             select new S03A2DNN
                             {
                                 PostedDate = a.PostedDate,
                                 RefNo = a.No,
                                 RefDate = a.Date,
                                 Description = a.Description,
                                 Reason = a.Reason,
                                 CreaditAmount = a.CreditAmount,
                                 AccountCorresponding = a.AccountCorresponding//Tài Khoản Đối ứng
                             });

            List<string> lst5Acc = LstRS03A2.Select(c => c.AccountCorresponding).Distinct().ToList();//Lấy ra list danh sách tài khoản đối ứng
            if (lst5Acc.Count > 5) lst5Acc = lst5Acc.Take(5).ToList();//Nếu tài khoản đối ứng lớn hơn 5 thì lấy ra 5 tài khoản đầu tiền
            string lstAccNum = string.Join(",", lst5Acc.ToArray());//Đổi list thành 1 chuỗi và tách nhau bởi dấu phẩy
            lstAccNum = Account + "," + lstAccNum;//Loại tài khoản cộng với 
            if (GroupAA == false)
            {
                foreach (var x in LstRS03A2)
                {
                    if (lst5Acc.Count >= 0 && x.AccountCorresponding == lst5Acc[0])
                    {
                        x.CAmount1 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 1 && x.AccountCorresponding == lst5Acc[1])
                    {
                        x.CAmount2 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 2 && x.AccountCorresponding == lst5Acc[2])
                    {
                        x.CAmount3 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 3 && x.AccountCorresponding == lst5Acc[3])
                    {
                        x.CAmount4 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 4 && x.AccountCorresponding == lst5Acc[4])
                    {
                        x.CAmount5 = x.CreaditAmount;
                    }
                    else
                    {
                        x.CAmount6 = x.CreaditAmount;
                        x.AccountNumber6 = x.AccountCorresponding;
                    }
                    x.ListAccount = lstAccNum;
                }
            }
            else
            {
                var group = from a in LstRS03A2
                            group a by new
                            {
                                a.RefNo,
                                a.AccountCorresponding,
                            } into ga
                            select new S03A2DNN
                            {
                                RefNo = ga.Key.RefNo,
                                AccountCorresponding = ga.Key.AccountCorresponding,
                                Description = ga.Select(c => c.Reason).ToString(),
                            };
                foreach (var x in group)
                {
                    if (lst5Acc.Count >= 0 && x.AccountCorresponding == lst5Acc[0])
                    {
                        x.CAmount1 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 1 && x.AccountCorresponding == lst5Acc[1])
                    {
                        x.CAmount2 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 2 && x.AccountCorresponding == lst5Acc[2])
                    {
                        x.CAmount3 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 3 && x.AccountCorresponding == lst5Acc[3])
                    {
                        x.CAmount4 = x.CreaditAmount;
                    }
                    else if (lst5Acc.Count >= 4 && x.AccountCorresponding == lst5Acc[4])
                    {
                        x.CAmount5 = x.CreaditAmount;
                    }
                    else
                    {
                        x.CAmount6 = x.CreaditAmount;
                        x.AccountNumber6 = x.AccountCorresponding;
                    }
                    x.ListAccount = lstAccNum;
                }

            }
            return LstRS03A2.ToList();
        }
        #endregion
        #region Báo cáo bảng đối chiều với ngân hàng [DungNA] BankCompare
        /// <summary>
        /// [DungNA]
        /// Bảng đối chiếu với ngân hàng
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="BankAccountDetailID">Mã tài khoản ngân hàng</param>
        /// <returns></returns>
        public List<BankCompare> RBankCompare(DateTime froms, DateTime to, string CurrencyID, List<Guid> BankAccountDetailID)
        {
            List<BankCompare> output = new List<BankCompare>();
            foreach (Guid x in BankAccountDetailID)
            {
                List<BankCompare> aaa = GetBankCompare(froms, to, CurrencyID, x);
                output.AddRange(aaa);
            }
            return output;
        }
        /// <summary>
        /// [DungNA]
        /// Bảng Đối chiều với ngân hàng
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="BankAccountDetailID">ID tài khoản ngân hàng</param>
        /// <returns></returns>
        public List<BankCompare> GetBankCompare(DateTime froms, DateTime to, string CurrencyID, Guid BankAccountDetailID)
        {
            var listkq = new List<BankCompare>();
            var BC = (from a in Query.ToList()
                      where a.CurrencyID != null
                       && a.PostedDate >= froms
                       && a.PostedDate <= to
                       && a.Account != null
                       && a.CurrencyID == CurrencyID
                       && a.Account.StartsWith("112")
                       && a.TypeID.ToString().StartsWith("70")
                      group a by new
                      {
                          a.ReferenceID,
                          a.No,
                          a.Date,
                          a.BankAccountDetailID
                      } into Ga
                      select new BankCompare
                      {
                          BankAccount = (from b in _IBankAccountDetailService.Query.ToList()
                                         where b.BankAccount != null
                                         && b.BankName != null
                                         && Ga.Key.BankAccountDetailID == b.ID
                                         select b.BankAccount).FirstOrDefault(),
                          BankName = (from b in _IBankAccountDetailService.Query.ToList()
                                      where b.BankAccount != null
                                      && b.BankName != null
                                      && Ga.Key.BankAccountDetailID == b.ID
                                      select b.BankName).FirstOrDefault(),
                          Date = Convert.ToDateTime(Ga.Select(b => b.PostedDate)),
                          RefNo = (Ga.Select(b => b.RefNo)).ToString(),
                          RefType = (from b in _ITypeService.Query.ToList()
                                     where b.TypeName != null
                                         && Ga.Where(c => c.TypeID == b.ID).Count() > 0
                                     select b.TypeName).ToString(),
                          Description = (Ga.Select(b => b.Description)).ToString(),
                          Check = ((from b in _IMBDepositService.Query.ToList()
                                    where Ga.Key.ReferenceID == b.ID
                                    && b.IsMatch == true
                                    select b.IsMatch).Count() > 0
                                  || ((from b in _IMBInternalTransferDetailService.Query.ToList()
                                       where Ga.Key.ReferenceID == b.ID
                                        && b.IsMatch == true
                                       select b.IsMatch).Count() > 0)
                                       || ((from b in _IGOtherVoucherDetailService.Query.ToList()
                                            where Ga.Key.ReferenceID == b.ID
                                             && b.IsMatch == true
                                            select b.IsMatch).Count() > 0)
                                            || ((from b in _IMBTellerPaperService.Query.ToList()
                                                 where Ga.Key.ReferenceID == b.ID
                                                  && b.IsMatch == true
                                                 select b.IsMatch).Count() > 0)
                                                 || ((from b in _IMBCreditCardService.Query.ToList()
                                                      where Ga.Key.ReferenceID == b.ID
                                                       && b.IsMatch == true
                                                      select b.IsMatch).Count() > 0))
                                       ? "v" : "",//Kiểm tra 5 với điều kiện nếu IsMath được check bằng true thì trả về V còn nếu bằng false thì  để trống
                          DebitAmount = Ga.Sum(c => c.DebitAmount),
                          RealDebitAmount = ((from b in _IMBDepositService.Query.ToList()
                                              where Ga.Key.ReferenceID == b.ID
                                              && b.IsMatch == true
                                              select b.IsMatch).Count() > 0
                                   || ((from b in _IMBInternalTransferDetailService.Query.ToList()
                                        where Ga.Key.ReferenceID == b.ID
                                        && b.IsMatch == true
                                        select b.IsMatch).Count() > 0)
                                        || ((from b in _IGOtherVoucherDetailService.Query.ToList()
                                             where Ga.Key.ReferenceID == b.ID
                                              && b.IsMatch == true
                                             select b.IsMatch).Count() > 0)
                                             || ((from b in _IMBTellerPaperService.Query.ToList()
                                                  where Ga.Key.ReferenceID == b.ID
                                                   && b.IsMatch == true
                                                  select b.IsMatch).Count() > 0)
                                                  || ((from b in _IMBCreditCardService.Query.ToList()
                                                       where Ga.Key.ReferenceID == b.ID
                                                        && b.IsMatch == true
                                                       select b.IsMatch).Count() > 0))
                                        ? (Ga.Sum(c => c.DebitAmount)) : 0,//Thực thu = thu khi ismath = true hoặc ngược lại
                          CreditAmount = Ga.Sum(c => c.CreditAmount),
                          RealCreditAmount = ((from b in _IMBDepositService.Query.ToList()
                                               where Ga.Key.ReferenceID == b.ID
                                                && b.IsMatch == true
                                               select b.IsMatch).Count() > 0
                                  || ((from b in _IMBInternalTransferDetailService.Query.ToList()
                                       where Ga.Key.ReferenceID == b.ID
                                        && b.IsMatch == true
                                       select b.IsMatch).Count() > 0)
                                       || ((from b in _IGOtherVoucherDetailService.Query.ToList()
                                            where Ga.Key.ReferenceID == b.ID
                                             && b.IsMatch == true
                                            select b.IsMatch).Count() > 0)
                                            || ((from b in _IMBTellerPaperService.Query.ToList()
                                                 where Ga.Key.ReferenceID == b.ID
                                                 && b.IsMatch == true
                                                 select b.IsMatch).Count() > 0)
                                                 || ((from b in _IMBCreditCardService.Query.ToList()
                                                      where Ga.Key.ReferenceID == b.ID
                                                       && b.IsMatch == true
                                                      select b.IsMatch).Count() > 0))
                                       ? (Ga.Sum(c => c.CreditAmount)) : 0,
                          Hyperlink = Ga.Select(c => c.TypeID + ";" + c.ReferenceID).ToString(),
                      }).ToList();
            foreach (var x in BC)
            {
                var addlist = new BankCompare();
                addlist.BankAccount = x.BankAccount;
                addlist.BankName = x.BankName;
                addlist.Date = x.Date;
                addlist.RefNo = x.RefNo;
                addlist.RefType = x.RefType;
                addlist.Description = x.Description;
                addlist.Check = x.Check;
                addlist.DebitAmount = x.DebitAmount;
                addlist.RealDebitAmount = x.RealDebitAmount;
                addlist.CreditAmount = x.CreditAmount;
                addlist.RealCreditAmount = x.RealCreditAmount;
                addlist.Hyperlink = x.Hyperlink;
                listkq.Add(addlist);
            }
            BankCompare OpeningBalance = new BankCompare();//Số dư đầu kỳ            
            OpeningBalance.OpenningAmount = GetOpeningBalanceCompare(to, CurrencyID, BankAccountDetailID);
            OpeningBalance.BankAccount = _IBankAccountDetailService.Getbykey(BankAccountDetailID).BankAccount;
            listkq.Add(OpeningBalance);
            return listkq;
        }
        /// <summary>
        /// [DungNA]
        /// Lất số dư đầu kỳ  cho bảng đối chiếu với ngân hàng
        /// </summary>
        /// <param name="to">Từ Ngày</param>
        /// <param name="currencyID">Loại Tiền</param>
        /// <param name="BankAccountDetailID">ID tài khoản ngân hàng</param>
        /// <returns></returns>
        public decimal GetOpeningBalanceCompare(DateTime froms, string currencyID, Guid BankAccountDetailID)//Số dư đầu kỳ
        {
            decimal rp = 0;
            var list = (from a in Query.ToList()
                        where a.PostedDate < froms
                        && a.CurrencyID == currencyID
                        && a.BankAccountDetailID == BankAccountDetailID
                        && a.Account.StartsWith("112")
                        select new
                        {
                            OpenningAmount = a.DebitAmount - a.CreditAmount
                        }).Sum(c => c.OpenningAmount);
            rp = list;
            return rp;
        }
        #endregion
        #region DungNA Bảng kê số dư ngân hàng
        /// <summary>
        /// [DungNA]
        /// Báo cáo bảng kê số dư ngân hàng
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="ListAccount">Danh Sách Tài kHoản</param>
        /// <param name="CurrencyID">Loại Tiền Tệ</param>
        /// <returns></returns>
        public List<BankBalance> RBankBlance(DateTime froms, DateTime to, List<string> ListAccount, string CurrencyID)
        {
            List<BankBalance> output = new List<BankBalance>();
            foreach (var x in ListAccount)
            {
                List<BankBalance> list1 = GetBankBlance(froms, to, x, CurrencyID);
                output.AddRange(list1);
            }
            return output;
        }
        /// <summary>
        /// [DungNA]
        /// Báo cáo bảng kê số dư ngân hàng
        /// </summary>
        /// <param name="froms">Từ ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="CurrencyID">Loại tiền</param>
        /// <param name="Account">Số tài khoản</param>
        /// <returns></returns>
        public List<BankBalance> GetBankBlance(DateTime froms, DateTime to, string CurrencyID, string Account)
        {
            var bl = (from a in Query.ToList()
                      join b in _IBankAccountDetailService.Query.ToList() on a.BankAccountDetailID equals b.ID
                      where a.CurrencyID != null
                      && a.Account != null
                      && b.BankName != null
                      && b.BankAccount != null
                      && a.PostedDate >= froms
                      && a.PostedDate <= to
                      && a.CurrencyID == CurrencyID
                      && a.Account == Account
                      select new BankBalance
                      {
                          AccountNumber = a.Account,
                          Currency = a.CurrencyID,
                          BankAccount = b.BankAccount,
                          BankName = b.BankName,
                          OpenDebit = ((Convert.ToDecimal(from d in Query.ToList()
                                                          where d.TypeID == 700
                                                          && d.Account == Account
                                                          && d.No.Contains("OPN")
                                                          && d.CurrencyID == CurrencyID
                                                          && d.PostedDate >= to
                                                          select d.DebitAmount)) + (Convert.ToDecimal(from e in Query.ToList()
                                                                                                      where e.Account == Account
                                                                                                      && e.CurrencyID == CurrencyID
                                                                                                      group e by new { e.BankAccountDetailID, e.Account, e.CurrencyID } into g
                                                                                                      select g.Sum(x => x.DebitAmount)))
                                                                                 - (Convert.ToDecimal(from e in Query.ToList()
                                                                                                      where e.Account == Account
                                                                                                      && e.CurrencyID == CurrencyID
                                                                                                      group e by new { e.BankAccountDetailID, e.Account, e.CurrencyID } into g
                                                                                                      select g.Sum(x => x.CreditAmount)))),
                          OpenCredit = 0,//Không có giá trị gì trong cột số dư đầu kỳ
                          CrementDebit = (Convert.ToDecimal(from e in Query.ToList()
                                                            where e.Account == Account
                                                            && e.CurrencyID == CurrencyID
                                                            group e by new { e.BankAccountDetailID, e.Account, e.CurrencyID } into g
                                                            select g.Sum(x => x.DebitAmount))),
                          CrementCredit = (Convert.ToDecimal(from e in Query.ToList()
                                                             where e.Account == Account
                                                             && e.CurrencyID == CurrencyID
                                                             group e by new { e.BankAccountDetailID, e.Account, e.CurrencyID } into g
                                                             select g.Sum(x => x.CreditAmount))),
                      }).ToList();
            foreach (var x in bl)
            {
                x.CloseDebit = x.OpenDebit + x.CrementDebit - x.CrementCredit;

            }
            return bl;
        }
        #endregion
        #region DungNA Sổ chi tiết tiền gửi ngân hàng bằng ngoại tệ
        /// <summary>
        /// [DungNA]
        /// Sổ chi tiết tiền gửi ngân hàng bằng ngoại tệ
        /// </summary>
        /// <param name="froms">Từ ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="Account">Số tài khoản</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="ListBankAccountDetailID">Danh sách tài khoản ngân hàng</param>
        /// <param name="TotalGroup">Nút cộng gộp</param>
        /// <returns></returns>
        public List<BankForeignCurrency> RForegignCurrencyCash(DateTime froms, DateTime to, string Account, string CurrencyID, List<Guid> ListBankAccountDetailID, bool TotalGroup)
        {
            List<BankForeignCurrency> output = new List<BankForeignCurrency>();
            foreach (Guid x in ListBankAccountDetailID)
            {
                List<BankForeignCurrency> list = GetForegignCurrencyCash(froms, to, Account, CurrencyID, x, TotalGroup);
                output.AddRange(list);
            }
            return output;
        }
        /// <summary>
        /// [DungNA]
        /// Sổ chi tiết tiền gử ngân hàng bằng ngoại tệ
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="Account">Số tài khoản</param>
        /// <param name="CurrencyID">Loại tiền</param>
        /// <param name="BankAccountDetailID"> ID Số tài khoản ngân hàng</param>
        /// <param name="TotalGroup">Check cộng gộp</param>
        /// <returns></returns>
        public List<BankForeignCurrency> GetForegignCurrencyCash(DateTime froms, DateTime to, string Account, string CurrencyID, Guid BankAccountDetailID, bool TotalGroup)
        {
            var listkq = new List<BankForeignCurrency>();
            var GFC = (from a in Query.ToList()
                       join b in _IBankAccountDetailService.Query.ToList() on a.BankAccountDetailID equals b.ID
                       join c in _ITypeService.Query.ToList() on a.TypeID equals c.ID
                       where b.BankAccount != null
                       && b.BankName != null
                       && a.Account != null
                       && a.No != null
                       && c.TypeName != null
                       && a.Description != null
                       && a.AccountCorresponding != null
                       && a.ExchangeRate != null
                       && a.PostedDate >= froms
                       && a.PostedDate <= to
                       && a.Account == Account
                       && a.CurrencyID == CurrencyID
                       && (BankAccountDetailID == Guid.Empty ? true : a.BankAccountDetailID == BankAccountDetailID)
                       select new BankForeignCurrency
                       {
                           BankName = b.BankName,
                           BankAccount = b.BankAccount,
                           AccountNumber = a.Account,
                           Currency = a.CurrencyID,
                           Date = a.PostedDate,
                           No = a.No,
                           Type = c.TypeName,
                           Description = a.Description,
                           Reason = a.Reason,
                           CorrespondingAccount = a.AccountCorresponding,
                           ExchangeRate = (decimal)a.ExchangeRate,
                           Receipt = a.DebitAmountOriginal,
                           EqReceipt = a.DebitAmount,
                           Payment = a.CreditAmountOriginal,
                           EqPayment = a.CreditAmount,
                       }).ToList();
            if (TotalGroup == false)
            {
                foreach (var x in GFC)
                {
                    var addlist = new BankForeignCurrency();
                    addlist.BankName = x.BankName;
                    addlist.BankAccount = x.BankAccount;
                    addlist.AccountNumber = x.AccountNumber;
                    addlist.Currency = x.Currency;
                    addlist.Date = x.Date;
                    addlist.No = x.No;
                    addlist.Type = x.Type;
                    addlist.Description = x.Description;
                    addlist.CorrespondingAccount = x.CorrespondingAccount;
                    addlist.ExchangeRate = x.ExchangeRate;
                    addlist.Receipt = x.Receipt;
                    addlist.EqReceipt = x.EqReceipt;
                    addlist.Payment = x.Payment;
                    addlist.EqPayment = x.EqPayment;
                    listkq.Add(addlist);
                }
            }
            else   //Sử dụng group by 2 cột để lọc giá trị gộp các bút toán giống nhau
            {
                var ReportGroup = from p in listkq
                                  group p by new
                                  {
                                      p.No,
                                      p.CorrespondingAccount,
                                      p.Date
                                  } into rp
                                  select new BankForeignCurrency
                                  {
                                      No = rp.Key.No,
                                      CorrespondingAccount = rp.Key.CorrespondingAccount,
                                      Reason = rp.Select(c => c.Reason).ToString(),
                                      Receipt = rp.Sum(c => c.Receipt),
                                      EqReceipt = rp.Sum(c => c.EqReceipt),
                                      Payment = rp.Sum(c => c.Payment),
                                      EqPayment = rp.Sum(c => c.EqPayment)

                                  };
                foreach (var y in ReportGroup)
                {
                    var addlist = new BankForeignCurrency();
                    addlist.BankName = y.BankName;
                    addlist.BankAccount = y.BankAccount;
                    addlist.AccountNumber = y.AccountNumber;
                    addlist.Currency = y.Currency;
                    addlist.Date = y.Date;
                    addlist.No = y.No;
                    addlist.Type = y.Type;
                    addlist.Description = y.Reason;
                    addlist.CorrespondingAccount = y.CorrespondingAccount;
                    addlist.ExchangeRate = y.ExchangeRate;
                    addlist.Receipt = y.Receipt;
                    addlist.EqReceipt = y.EqReceipt;
                    addlist.Payment = y.Payment;
                    addlist.EqPayment = y.EqPayment;
                    listkq.Add(addlist);
                }
            }
            BankForeignCurrency OpeningBalance = new BankForeignCurrency();//Số dư đầu kỳ
            if (Account.StartsWith("1"))
            {
                OpeningBalance.OpeningDebit = GetOpeningBalanceDD(froms, CurrencyID, Account);
            }
            else if (Account.StartsWith("2"))
            {
                OpeningBalance.OpeningCredit = GetOpeningBalanceDD(froms, CurrencyID, Account);
            }
            OpeningBalance.AccountNumber = Account;
            listkq.Add(OpeningBalance);
            return listkq.OrderBy(c => c.Date).ToList();
        }

        #endregion
        #region Sổ nhật ký chung S03ADNN [DungNA]
        /// <summary>
        /// [DungNA]
        /// Sổ nhật ký chung
        /// </summary>
        /// <param name="froms">Từ ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="NoOrInvoiceNo">True hoặc Flase tương ứng với Số chứng từ hoặc Số hóa đơn</param>
        /// <param name="TotalGroup">Check cộng gộp các but toán giống nhau</param>
        /// <returns></returns>
        public List<S03ADNN> RS03ADNN(DateTime froms, DateTime to, bool NoOrInvoiceNo, bool TotalGroup)
        {
            var listkq = new List<S03ADNN>();
            var thongke = (from a in Query
                           where a.PostedDate >= froms
                           && a.PostedDate <= to
                           select new S03ADNN
                           {
                               Currency = "VND",
                               PostedDate = a.PostedDate,
                               No = NoOrInvoiceNo == true ? a.No : a.InvoiceNo,
                               HyperLink = a.TypeID + ";" + a.ReferenceID,
                               Date = Convert.ToDateTime(NoOrInvoiceNo == true ? a.Date : a.InvoiceDate),
                               Description = a.Description,
                               Reason = a.Reason,
                               Posted = "X",
                               AccountCorresponding = a.AccountCorresponding,
                               LineNo = "",
                               DebitAmount = a.CreditAmount == 0 ? a.DebitAmount : 0,
                               CreditAmount = a.DebitAmount == 0 ? a.CreditAmount : 0,
                           }).ToList();
            if (TotalGroup == false)
            {
                foreach (var x in thongke)
                {
                    var addlist = new S03ADNN();
                    addlist.Currency = x.Currency;
                    addlist.PostedDate = x.PostedDate;
                    addlist.No = x.No;
                    addlist.HyperLink = x.HyperLink;
                    addlist.Date = x.Date;
                    addlist.Description = x.Description;
                    addlist.Posted = x.Posted;
                    addlist.AccountCorresponding = x.AccountCorresponding;
                    addlist.LineNo = x.LineNo;
                    addlist.DebitAmount = x.DebitAmount;
                    addlist.CreditAmount = x.CreditAmount;
                    listkq.Add(addlist);
                }
            }
            else   //Sử dụng group by 2 cột để lọc giá trị gộp các bút toán giống nhau
            {
                var ReportGroup = from p in listkq
                                  group p by new
                                  {
                                      p.No,
                                      p.AccountCorresponding,
                                      p.Date,
                                      p.PostedDate,
                                  } into rp
                                  select new S03ADNN
                                  {
                                      No = rp.Key.No,
                                      AccountCorresponding = rp.Key.AccountCorresponding,
                                      Reason = rp.Select(c => c.Reason).ToString(),
                                      DebitAmount = rp.Sum(c => c.DebitAmount),
                                      CreditAmount = rp.Sum(c => c.CreditAmount),
                                  };
                foreach (var y in ReportGroup)
                {
                    var addlist = new S03ADNN();
                    addlist.Currency = y.Currency;
                    addlist.PostedDate = y.PostedDate;
                    addlist.No = y.No;
                    addlist.HyperLink = y.HyperLink;
                    addlist.Date = y.Date;
                    addlist.Description = y.Reason;
                    addlist.Posted = y.Posted;
                    addlist.AccountCorresponding = y.AccountCorresponding;
                    addlist.LineNo = y.LineNo;
                    addlist.DebitAmount = y.DebitAmount;
                    addlist.CreditAmount = y.CreditAmount;
                    listkq.Add(addlist);
                }
            }
            return listkq.OrderBy(c => c.PostedDate).ToList();
        }
        #endregion
        #region Sổ cái tài khoản (Hình thức nhật ký chung)[S03BDNN]
        /// <summary>
        /// [DungNa]
        /// Sổ cái tài khoản (Hình thức nhật ký chung)
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="NoOrInvoiceNo">True là số chứng từ còn false là số hóa đơn</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="ListAccount">Danh sách tài khoản</param>
        /// <param name="TotalGroup">But toán cộng gộp</param>
        /// <returns></returns>
        public List<S03BDNN> RS03BDNN(DateTime froms, DateTime to, bool NoOrInvoiceNo, string CurrencyID, List<string> ListAccount, bool TotalGroup)
        {
            List<S03BDNN> list = new List<S03BDNN>();
            foreach (var x in ListAccount)
            {
                List<S03BDNN> list1 = GetS03BDNN(froms, to, NoOrInvoiceNo, CurrencyID, x, TotalGroup);
                list.AddRange(list1);
            }
            return list;
        }
        /// <summary>
        /// [DungNA]
        /// Sổ cái tài khoản (Hình thức nhật ký chung)
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="NoOrInvoiceNo">Hóa đơn hoặc chứng từ</param>
        /// <param name="CurrencyID">Loại tiền</param>
        /// <param name="Account">Số tài khoản</param>
        /// <param name="TotalGroup">But toán cộng gộp</param>
        /// <returns></returns>
        public List<S03BDNN> GetS03BDNN(DateTime froms, DateTime to, bool NoOrInvoiceNo, string CurrencyID, string Account, bool TotalGroup)
        {
            var listkq = new List<S03BDNN>();
            var thongke = (from a in Query
                           join b in _IAccountService.Query.ToList() on a.Account equals b.AccountNumber
                           where a.PostedDate >= froms
                           && a.PostedDate <= to
                           && a.CurrencyID == CurrencyID
                           && a.Account == Account
                           select new S03BDNN
                           {
                               Account = a.Account,
                               AccountName = b.AccountName,
                               PostedDate = a.PostedDate,
                               No = NoOrInvoiceNo == true ? a.No : a.InvoiceNo,
                               HyperLink = a.TypeID + ";" + a.ReferenceID,
                               Date = Convert.ToDateTime(NoOrInvoiceNo == true ? a.Date : a.InvoiceDate),
                               Description = a.Description,
                               Reason = a.Reason,
                               PageNo = "",
                               LineNo = "",
                               AccountCorresponding = a.AccountCorresponding,
                               DebitAmount = a.DebitAmount,
                               CreditAmount = a.CreditAmount,
                           }).ToList();
            if (TotalGroup == false)
            {
                foreach (var x in thongke)
                {
                    var addlist = new S03BDNN();
                    addlist.Account = x.Account;
                    addlist.AccountName = x.AccountName;
                    addlist.PostedDate = x.PostedDate;
                    addlist.No = x.No;
                    addlist.HyperLink = x.HyperLink;
                    addlist.Date = x.Date;
                    addlist.Description = x.Description;
                    addlist.PageNo = x.PageNo;
                    addlist.LineNo = x.LineNo;
                    addlist.AccountCorresponding = x.AccountCorresponding;
                    addlist.DebitAmount = x.DebitAmount;
                    addlist.CreditAmount = x.CreditAmount;
                    listkq.Add(addlist);

                }
            }
            else
            {
                var ReportGroup = from p in listkq
                                  group p by new
                                  {
                                      p.No,
                                      p.AccountCorresponding,
                                      p.Date,
                                      p.PostedDate,
                                  } into rp
                                  select new S03BDNN
                                  {
                                      No = rp.Key.No,
                                      AccountCorresponding = rp.Key.AccountCorresponding,
                                      Reason = rp.Select(c => c.Reason).ToString(),
                                      DebitAmount = rp.Sum(c => c.DebitAmount),
                                      CreditAmount = rp.Sum(c => c.CreditAmount),
                                  };
                foreach (var y in ReportGroup)
                {
                    var addlist = new S03BDNN();
                    addlist.Account = y.Account;
                    addlist.AccountName = y.AccountName;
                    addlist.PostedDate = y.PostedDate;
                    addlist.No = y.No;
                    addlist.HyperLink = y.HyperLink;
                    addlist.Date = y.Date;
                    addlist.Description = y.Reason;
                    addlist.PageNo = y.PageNo;
                    addlist.LineNo = y.LineNo;
                    addlist.AccountCorresponding = y.AccountCorresponding;
                    addlist.DebitAmount = y.DebitAmount;
                    addlist.CreditAmount = y.CreditAmount;
                    listkq.Add(addlist);
                }
            }
            S03BDNN Abc = new S03BDNN();
            Abc.Description = "Số Phát Sinh Trong Kỳ";//Add thêm dòng số phát sinh trong kỳ
            Abc.Account = Account;
            listkq.Add(Abc);
            S03BDNN OpeningBalance = new S03BDNN();//Số dư đầu kỳ
            if (Account.StartsWith("1"))
            {
                OpeningBalance.OpeningDebit = GetOpeningBalanceDD(froms, CurrencyID, Account);
            }
            else if (Account.StartsWith("2"))
            {
                OpeningBalance.OpeningCredit = GetOpeningBalanceDD(froms, CurrencyID, Account);
            }
            OpeningBalance.Account = Account;
            listkq.Add(OpeningBalance); // add thêm dòng sô dư đầu kỳ  
            return listkq.OrderBy(c => c.PostedDate).ToList();
        }
        #endregion
        #region Hàm lấy số dư đầu kỳ báo cáo dùng chung
        /// <summary>
        /// [DungNA] And [Duy]
        /// Lấy số dư đầu kỳ báo cáo dùng chung
        /// </summary>
        /// <param name="fromDate">Từ Ngày (ngày bắt đầu báo cáo)</param>        
        /// <param name="currencyID">Loại Tiền</param>
        /// <param name="account">Số Tài khoản</param>
        /// <returns></returns>
        public decimal GetOpeningBalanceDD(DateTime fromDate, string account, string currencyID = "")//Số dư đầu kỳ và số tồn đầu kỳ như nhau
        {
            decimal rp = 0;
            if (account.StartsWith("1") || account.StartsWith("3"))//Với Dự nợ đa phần bắt đầu với tài khoản bắt đâu bằng 1(111,112...)
            {
                var soTonDauKyDuNo = (from a in Query.ToList()
                                      where a.PostedDate < fromDate && a.Account != null
                                      && a.Account.StartsWith(account)
                                      && (currencyID == "" || a.CurrencyID == currencyID)
                                      select new
                                      {
                                          OpeningDebit = a.DebitAmount - a.CreditAmount
                                      }).Sum(c => c.OpeningDebit);
                rp = soTonDauKyDuNo;
            }
            else if (account.StartsWith("2") || account.StartsWith("4"))
            {
                var soTonDauKyDuCo = (from a in Query.ToList()
                                      where a.PostedDate < fromDate && a.Account != null
                                      && a.Account.StartsWith(account)
                                      && a.CurrencyID == currencyID
                                      select new
                                      {
                                          OpeningCreadit = a.CreditAmount - a.DebitAmount
                                      }).Sum(c => c.OpeningCreadit);
                rp = soTonDauKyDuCo;
            }
            return rp;
        }
        #endregion
        #region Hàm lấy số dư cuối kỳ báo cáo dùng chung
        /// <summary>
        /// [DungNA] And [Duy]
        /// Lấy số dư cuối kỳ dùng chung
        /// </summary>
        /// <param name="toDate">Đến Ngày (ngày kết thúc báo cáo)</param>        
        /// <param name="currencyID">Loại Tiền</param>
        /// <param name="account">Số Tài khoản</param>
        /// <returns></returns>
        public decimal GetClosingBalanceDD(DateTime toDate, string account, string currencyID = "")
        {
            decimal rp = 0;
            if (account.StartsWith("1") || account.StartsWith("3"))//Với Dự nợ đa phần bắt đầu với tài khoản bắt đâu bằng 1(111,112...)
            {
                var soTonCuoiKyDuNo = (from a in Query.ToList()
                                       where a.PostedDate <= toDate && a.Account != null
                                      && a.Account.StartsWith(account)
                                      && (currencyID == "" || a.CurrencyID == currencyID)
                                       select new
                                       {
                                           OpeningDebit = a.DebitAmount - a.CreditAmount
                                       }).Sum(c => c.OpeningDebit);
                rp = soTonCuoiKyDuNo;
            }
            else if (account.StartsWith("2") || account.StartsWith("4"))
            {
                var soTonCuoiKyDuNo = (from a in Query.ToList()
                                       where a.PostedDate <= toDate && a.Account != null
                                      && a.Account.StartsWith(account)
                                      && a.CurrencyID == currencyID
                                       select new
                                       {
                                           OpeningCreadit = a.CreditAmount - a.DebitAmount
                                       }).Sum(c => c.OpeningCreadit);
                rp = soTonCuoiKyDuNo;
            }
            return rp;
        }
        #endregion

        #region DuyTN

        /// <summary>
        /// Sổ quỹ
        /// [DuyTN] ngày 2-7-2013
        /// Report 
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="currencyID">Mã loại tiền </param>
        /// <param name="account">Danh sách mã tài khoản</param>
        /// <returns>Một danh sách GeneralLedgerReport, null nếu bị lỗi</returns>
        List<S05aDNN> ReportS07DN(DateTime fromDate, DateTime toDate, string currencyID, string account)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.ToList();
            var listDS = (lstGeneralLedgers.Where(g => g.PostedDate >= fromDate && g.PostedDate <= toDate
                                                 && g.CurrencyID != null && g.CurrencyID == currencyID
                                                 && g.Account != null && g.Account.StartsWith(account)
                                                 && g.DebitAmount != g.CreditAmount)
                                         //&& !g.TypeID.ToString().StartsWith("70"))
                                         .Select(g => new
                                         {
                                             g.ID,
                                             g.No,
                                             g.TypeID,
                                             g.Date,
                                             g.PostedDate,
                                             ReceiptRefNo = g.DebitAmount == 0 ? "" : g.No,
                                             PaymentRefNo = g.CreditAmount == 0 ? "" : g.No,
                                             g.Reason,
                                             g.DebitAmount,
                                             g.CreditAmount,
                                             g.Description,
                                             g.Account,
                                             g.RefNo,
                                             g.ReferenceID
                                         })).ToList();
            var listKetQua = new List<S05aDNN>();
            S05aDNN generalLedgerReportFirstAmount = new S05aDNN();
            generalLedgerReportFirstAmount.BalanceAmount = GetOpeningBalanceDD(fromDate, account, currencyID);
            generalLedgerReportFirstAmount.OrderType = -1;
            generalLedgerReportFirstAmount.ClosingAmount = generalLedgerReportFirstAmount.BalanceAmount;
            generalLedgerReportFirstAmount.Description = "Số dư đầu kỳ";
            generalLedgerReportFirstAmount.AccountNumber = account;
            listKetQua.Add(generalLedgerReportFirstAmount);
            var balanceAmountFirst = generalLedgerReportFirstAmount.ClosingAmount;
            foreach (var dong in listDS)
            {
                var addlist = new S05aDNN();
                if (listDS.Any(c => c.No == dong.No && c.Account == dong.Account && c.ID != dong.ID && (c.DebitAmount == dong.CreditAmount || c.CreditAmount == dong.DebitAmount)))
                {
                    continue;
                }
                addlist.TypeID = dong.TypeID;
                addlist.Date = dong.Date;
                addlist.PostedDate = dong.PostedDate;
                addlist.ReceiptRefNo = dong.ReceiptRefNo;
                addlist.PaymentRefNo = dong.PaymentRefNo;
                addlist.Reason = dong.Reason;
                addlist.DebitAmount = dong.DebitAmount;
                addlist.CreditAmount = dong.CreditAmount;
                addlist.BalanceAmount = balanceAmountFirst = balanceAmountFirst + addlist.DebitAmount - addlist.CreditAmount;
                addlist.Description = dong.Description;
                addlist.AccountNumber = account;
                addlist.OrderType = 0;
                addlist.Hyperlink = dong.TypeID + ";" + dong.RefNo;
                addlist.ReferenceID = dong.ReferenceID;
                listKetQua.Add(addlist);
            }
            return listKetQua.OrderBy(c => c.PostedDate).ToList();
        }
        List<GeneralLedger> ReportS07DN2(DateTime fromDate, DateTime toDate, string currencyID, string account)
        {
            ReportProcedureSDS procedure = new ReportProcedureSDS();
            var listDS = procedure.ReportS07DNSQL(fromDate, toDate, currencyID, account);
            return listDS;
        }
        public List<S05aDNN> ReportS05aDNN(DateTime date, DateTime toDate, string currencyID, IEnumerable<string> account)
        {
            var lstS07DN = new List<S05aDNN>();
            var lstGeneralLedger = new List<GeneralLedger>();
            foreach (var acc in account)
            {
                //List<S05aDNN> data0 = ReportS07DN(date, toDate, currencyID, acc);
                //List<S05aDNN> data0 = ReportS07DN2(date, toDate, currencyID, acc);
                //if (data0.Count > 1) lstS07DN.AddRange(data0);
                var listLayVe = ReportS07DN2(date, toDate, currencyID, acc);
                var listID = lstGeneralLedger.Select(t => t.ID);
                listLayVe = listLayVe.Where(t => !(listID.Contains(t.ID))).ToList();
                var balanceAmountFirst = GetOpeningBalanceDD(date, acc, currencyID);
                var listKetQua = new List<S05aDNN>();
                listKetQua = listLayVe.Select(dong => new S05aDNN
                {
                    TypeID = dong.TypeID,
                    Date = dong.Date,
                    PostedDate = dong.PostedDate,
                    ReceiptRefNo = dong.DebitAmount == 0 ? "" : dong.No,
                    PaymentRefNo = dong.CreditAmount == 0 ? "" : dong.No,
                    Reason = dong.Reason,
                    DebitAmount = dong.DebitAmount,
                    CreditAmount = dong.CreditAmount,
                    BalanceAmount = balanceAmountFirst = balanceAmountFirst + dong.DebitAmount - dong.CreditAmount,
                    Description = dong.Description,
                    AccountNumber = acc,
                    OrderType = 0,
                    Hyperlink = dong.TypeID + ";" + dong.RefNo,
                    ReferenceID = dong.ReferenceID
                }).ToList();
                S05aDNN generalLedgerReportFirstAmount = new S05aDNN
                {
                    BalanceAmount = GetOpeningBalanceDD(date, acc, currencyID),
                    OrderType = -1,
                    ClosingAmount = GetOpeningBalanceDD(date, acc, currencyID),
                    Description = "Số dư đầu kỳ",
                    AccountNumber = acc
                };
                listKetQua.Add(generalLedgerReportFirstAmount);
                lstS07DN.AddRange(listKetQua);
            }
            return lstS07DN;
        }

        /// <summary>
        /// Tổng hợp công nợ phải trả
        /// [DuyTN]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="currencyID">mã loại tiền</param>
        /// <param name="account">tài khoản</param>
        /// <param name="accountingObjectID">ds nhà cung cấp</param>
        /// <returns></returns>
        List<PayableReceiptableSummary> ReportsPayableSummary(DateTime fromDate, DateTime toDate, string currencyID,
            string account, List<Guid> accountingObjectID)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.ToList();
            var lst = lstGeneralLedgers.
                Where(g => g.PostedDate >= fromDate && g.PostedDate <= toDate && g.Account != null
                    && g.Account.StartsWith(account) && g.CurrencyID != null
                    && g.CurrencyID == currencyID && g.AccountingObjectID != null && !g.TypeID.ToString().StartsWith("70")
                    && accountingObjectID.ToString().Contains(g.AccountingObjectID.ToString()))
                    .GroupBy(g => new { g.AccountingObjectID, g.Account }).Select(t => new PayableReceiptableSummary()
                    {
                        AccountingObjectCode = _IAccountingObjectService.Getbykey((Guid)t.Key.AccountingObjectID).AccountingObjectCode,
                        AccountingObjectName = _IAccountingObjectService.Getbykey((Guid)t.Key.AccountingObjectID).AccountingObjectName,
                        DebitAmountFirst = UltilsSynthesisReport.SD_NO_DK(fromDate, toDate, t.Key.Account),
                        CreditAmountFirst = UltilsSynthesisReport.SD_CO_DK(fromDate, toDate, t.Key.Account),
                        DebitAmountArise = UltilsSynthesisReport.PS_NO(fromDate, toDate, t.Key.Account),
                        CreditAmountArise = UltilsSynthesisReport.PS_CO(fromDate, toDate, t.Key.Account),
                        DebitAmountAccum = UltilsSynthesisReport.PS_LUYKE_NO(fromDate, toDate, t.Key.Account),
                        CreditAmountAccum = UltilsSynthesisReport.PS_LUYKE_CO(fromDate, toDate, t.Key.Account),
                        Hyperlink = t.Key.AccountingObjectID + ";" + t.Where(c => c.AccountingObjectID == t.Key.AccountingObjectID).Select(c => c.TypeID).FirstOrDefault()
                        + ";" + fromDate + ";" + toDate + ";" + currencyID
                        + t.Where(c => c.AccountingObjectID == t.Key.AccountingObjectID).Select(c => c.ReferenceID).FirstOrDefault(),
                        AccountNumber = account
                    }).ToList();
            foreach (var summaryPayableDebt in lst)
            {
                summaryPayableDebt.AmountLast();
            }
            return lst.ToList();
        }

        public List<PayableReceiptableSummary> ReportsPayableReceiptableSummary(DateTime fromdate, DateTime toDate, string currencyID,
            IEnumerable<string> account, List<Guid> accountingObjectID)
        {
            var lstPayableSummaries = new List<PayableReceiptableSummary>();
            foreach (var acc in account)
            {
                List<PayableReceiptableSummary> data0 = ReportsPayableSummary(fromdate, toDate, currencyID, acc, accountingObjectID);
                if (data0.Count > 1) lstPayableSummaries.AddRange(data0);
            }
            return lstPayableSummaries;
        }

        /// <summary>
        /// Bảng cân đối tài khoản
        /// [DuyTN]
        /// </summary>
        /// <param name="fromDate">Từ ngày</param>
        /// <param name="toDate">Đến ngày</param>
        /// <param name="grade">Bậc tài khoản</param>
        /// <returns></returns>
        public List<F01DNN> ReportF01DNN(DateTime fromDate, DateTime toDate, int grade)
        {
            List<F01DNN> dsF01Dnns = new List<F01DNN>();
            var accounts = _IAccountService.GetAll();
            var ad = from a in _IAccountService.GetAll()
                     where a.Grade == grade
                     select new { a.AccountNumber, a.AccountName };
            foreach (var a in ad)
            {
                List<F01DNN> ar = (from b in Query
                                   where b.PostedDate >= fromDate
                                         && b.PostedDate <= toDate
                                         && b.Account.StartsWith(a.AccountNumber)
                                   select new F01DNN()
                                   {
                                       AccountNumber = a.AccountNumber,
                                       AccountName = a.AccountName,
                                       OpeningDebit = UltilsSynthesisReport.SD_NO_DK(fromDate, toDate, a.AccountNumber),
                                       OpeningCredit = UltilsSynthesisReport.SD_CO_DK(fromDate, toDate, a.AccountNumber),
                                       MovementDebit = UltilsSynthesisReport.PS_NO(fromDate, toDate, a.AccountNumber),
                                       MovementCredit = UltilsSynthesisReport.PS_CO(fromDate, toDate, a.AccountNumber),
                                       AccumDebit = UltilsSynthesisReport.PS_LUYKE_NO(fromDate, toDate, a.AccountNumber),
                                       AccumCredit = UltilsSynthesisReport.PS_LUYKE_CO(fromDate, toDate, a.AccountNumber),
                                       ClosingDebit = GetClosingBalanceDD(toDate, a.AccountNumber, ""),
                                       ClosingCredit = GetClosingBalanceDD(toDate, a.AccountNumber, "")
                                   }).ToList();
                dsF01Dnns.AddRange(ar);
            }
            return dsF01Dnns;
        }

        /// <summary>
        /// Bù trừ công nợ
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="currency">loại tiền</param>
        /// <returns></returns>
        public List<OffsetLiabilities> GetOffsetLiabilitieses(DateTime fromDate, DateTime toDate, Currency currency)
        {
            int lamtrondg = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            List<OffsetLiabilities> listOffsetLiabilitieses = new List<OffsetLiabilities>();
            List<AccountingObject> listAccountingObjects = _IAccountingObjectService.GetAccountingObjects(4);
            List<Guid> listAccountingObjID = listAccountingObjects.Select(o => o.ID).ToList();
            List<GeneralLedger> dsChungtuGhiso = Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.CurrencyID == currency.ID && c.AccountingObjectID != null && (c.Account.StartsWith("131") || c.Account.StartsWith("331")) && listAccountingObjID.Contains(c.AccountingObjectID ?? Guid.Empty)).ToList();
            var listGeneralLedgers =
                (from t in dsChungtuGhiso
                 group t by new { t.AccountingObjectID, t.Account }
                     into g
                 select new { g.Key, g }).ToList();
            foreach (var generalLedger in listGeneralLedgers)
            {
                //if (generalLedger.g.Any(c => c.Account == "331") && generalLedger.g.Any(c => c.Account == "131"))
                //{
                OffsetLiabilities offsetLiabilitiesDebit = new OffsetLiabilities
                {
                    AccountNumber = generalLedger.Key.Account,
                    AccountingObjectID = generalLedger.Key.AccountingObjectID,
                    AccountingObjectCode =
                        listAccountingObjects.Single(c => c.ID == generalLedger.Key.AccountingObjectID).AccountingObjectCode,
                    AccountingObjectName =
                        listAccountingObjects.Single(c => c.ID == generalLedger.Key.AccountingObjectID).AccountingObjectName,
                    Status = false,
                    CreditAmountLast = 0,
                    DebitAmountLast = generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.DebitAmount) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) > 0 ?
                    generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.DebitAmount) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) : 0,
                    //DebitAmountLast = generalLedger.g.Where(c => c.Account == "131").Sum(c => c.DebitAmount),
                    CreditAmountLastOriginal = 0,
                    DebitAmountLastOriginal = generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.DebitAmountOriginal) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.CreditAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) > 0 ?
                    generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.DebitAmountOriginal) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.CreditAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) : 0,
                    //DebitAmountLastOriginal = generalLedger.g.Where(c => c.Account == "131").Sum(c => c.DebitAmountOriginal)
                };


                OffsetLiabilities offsetLiabilitiesCredit = new OffsetLiabilities
                {
                    AccountNumber = generalLedger.Key.Account,
                    AccountingObjectID = generalLedger.Key.AccountingObjectID,
                    AccountingObjectCode =
                        listAccountingObjects.Single(c => c.ID == generalLedger.Key.AccountingObjectID).AccountingObjectCode,
                    AccountingObjectName =
                        listAccountingObjects.Single(c => c.ID == generalLedger.Key.AccountingObjectID).AccountingObjectName,
                    Status = false,
                    CreditAmountLast = generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.CreditAmount) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) > 0 ?
                    generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.CreditAmount) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) : 0,
                    //CreditAmountLast = generalLedger.g.Where(c => c.Account == "331").Sum(c => c.CreditAmount),
                    DebitAmountLast = 0,
                    CreditAmountLastOriginal = generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.CreditAmountOriginal) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.DebitAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) > 0 ?
                    generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => c.CreditAmountOriginal) - generalLedger.g.Where(c => c.Account == generalLedger.Key.Account && c.AccountingObjectID == generalLedger.Key.AccountingObjectID).Sum(c => Math.Round(c.DebitAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) : 0,
                    //CreditAmountLastOriginal = generalLedger.g.Where(c => c.Account == "331").Sum(c => c.CreditAmountOriginal),
                    DebitAmountLastOriginal = 0,
                };
                if (offsetLiabilitiesDebit.DebitAmountLast > 0)
                {
                    listOffsetLiabilitieses.Add(offsetLiabilitiesDebit);
                }
                if (offsetLiabilitiesCredit.CreditAmountLast > 0)
                {
                    listOffsetLiabilitieses.Add(offsetLiabilitiesCredit);
                }
                //}  
            }
            return listOffsetLiabilitieses;
        }

        #endregion

        #region Xem công nợ
        /// <summary>
        /// Lấy công nợ của khách hàng (Nghiệp vụ BÁN HÀNG)
        /// [kiendd]
        /// -- lọc theo đối tượng
        /// -- lọc theo ngày ghi sổ chứng từ nhỏ hơn hoặc bằng ngày hạch toán
        /// -- lọc tất cả những trường chi tiết theo khách hàng
        ///  Công nợ = tổng debit - tổng credit
        /// </summary>
        /// <param name="accountingObjectId">khách hàng cần lấy công nợ</param>
        /// <param name="postedDate">Ngày hạch toán</param>
        /// <returns></returns>
        public decimal GetDebtCustomer(Guid accountingObjectId, DateTime postedDate)
        {
            int lamtrondg = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            List<string> lstAcc = _IAccountService.GetByDetailType(1).Select(k => k.AccountNumber).ToList();
            List<GeneralLedger> dsGeneralLedgers = Query.Where(k => k.AccountingObjectID == accountingObjectId && k.PostedDate <= postedDate && lstAcc.Contains(k.Account)).ToList();
            decimal kq = dsGeneralLedgers.Sum(k => Math.Round(k.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) - dsGeneralLedgers.Sum(k => Math.Round(k.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero));
            return kq;
        }

        /// <summary>
        /// Lấy công nợ của NCC (Nghiệp vụ Mua HÀNG)
        /// [anhth]
        /// -- lọc theo đối tượng
        /// -- lọc theo ngày ghi sổ chứng từ nhỏ hơn hoặc bằng ngày hạch toán
        /// -- lọc tất cả những trường chi tiết theo khách hàng
        ///  Công nợ = tổng credit - tổng debit
        /// </summary>
        /// <param name="accountingObjectId">khách hàng cần lấy công nợ</param>
        /// <param name="postedDate">Ngày hạch toán</param>
        /// <returns></returns>
        public decimal GetCredCustomer(Guid accountingObjectId, DateTime postedDate)
        {
            int lamtrondg = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            List<string> lstAcc = _IAccountService.GetByDetailType(0).Select(k => k.AccountNumber).ToList();
            List<GeneralLedger> dsGeneralLedgers = Query.Where(k => k.AccountingObjectID == accountingObjectId && k.PostedDate <= postedDate && lstAcc.Contains(k.Account)).ToList();
            decimal kq = dsGeneralLedgers.Sum(k => Math.Round(k.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) - dsGeneralLedgers.Sum(k => Math.Round(k.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero));
            return kq;
        }

        #endregion

        #region Xử lý chứng từ
        public List<object> GetListObject_Xulychungtu<T>(T input, ref string msg, List<ViewGLPayExceedCash> lstViewGLPayExceedCash)
        {
            msg = "";
            List<object> listObj = new List<object>();
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            List<FixedAssetLedger> listFixedAssetTemp = new List<FixedAssetLedger>();
            List<ToolLedger> listToolLedgerTemp = new List<ToolLedger>();
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            PropertyInfo propRecord = input.GetType().GetProperty("Recorded", BindingFlags.Public | BindingFlags.Instance);
            BaseService<T, Guid> testInvoke = this.GetIService(input);
            string check = input.GetType().ToString();
            switch (input.GetType().ToString())
            {
                #region MCReceipt
                case "Accounting.Core.Domain.MCReceipt":
                    MCReceipt mcReceipt = (MCReceipt)(object)input;
                    listGenTemp = GenGeneralLedgers(mcReceipt);
                    switch (mcReceipt.TypeID)
                    {
                        //Phiếu thu thông thường
                        case 100:
                            break;
                        //Phiếu thu tiền khách hàng
                        case 101:
                            break;
                        //Phiếu thu từ bán hàng
                        case 102:
                            break;
                        //Phiếu thu từ bán hàng đại lý bán đúng giá, nhận ủy thác XNK
                        case 103:
                            break;
                    }
                    break;
                #endregion

                #region MCPayment
                case "Accounting.Core.Domain.MCPayment":
                    MCPayment mcPayment = (MCPayment)(object)input;
                    listGenTemp = GenGeneralLedgers(mcPayment);
                    switch (mcPayment.TypeID)
                    {
                        //Phiếu chi
                        case 110:
                            break;
                        //Phiếu chi trả lương
                        case 111:
                            break;
                        //Phiếu chi nộp thuế 
                        case 112:
                            break;
                        //Phiếu chi nộp thuế GTGT hàng NK
                        case 113:
                            break;
                        //Phiếu chi nộp thuế TNCN
                        case 114:
                            break;
                        //Phiếu chi nộp bảo hiểm
                        case 115:
                            break;
                        //Phiếu chi mua dịch vụ
                        case 116:
                            break;
                        //Phiếu chi mua hàng 
                        case 117:
                            break;
                        //Phiếu chi trả tiền NCC
                        case 118:
                            break;
                        //Phiếu chi mua TSCĐ
                        case 119:
                            listFixedAssetTemp = GenFixedAssetLedgers(mcPayment);
                            listObj.AddRange(listFixedAssetTemp);
                            //foreach (var fixedAssetLedger in listFixedAssetTemp)
                            //{
                            //    _IFixedAssetLedgerService.CreateNew(fixedAssetLedger);
                            //}
                            break;
                    }
                    break;
                #endregion

                #region MBDeposit
                case "Accounting.Core.Domain.MBDeposit":
                    MBDeposit mbDeposit = (MBDeposit)(object)input;
                    listGenTemp = GenGeneralLedgers(mbDeposit);
                    break;
                #endregion

                #region MBTellerPaper
                case "Accounting.Core.Domain.MBTellerPaper":
                    MBTellerPaper mbTellerPaper = (MBTellerPaper)(object)input;
                    listGenTemp = GenGeneralLedgers(mbTellerPaper);
                    switch (mbTellerPaper.TypeID)
                    {
                        //Ủy nhiệm chi
                        case 120:
                            break;
                        //Trả lương bằng Ủy nhiệm chi
                        case 121:
                            break;
                        //Nộp thuế bằng Ủy nhiệm chi 
                        case 122:
                            break;
                        //Nộp thuế GTGT hàng NK bằng Ủy nhiệm chi
                        case 123:
                            break;
                        //Nộp thuế TNCN bằng Ủy nhiệm chi
                        case 124:
                            break;
                        //Nộp bảo hiểm bằng Ủy nhiệm chi
                        case 125:
                            break;
                        //Mua dịch vụ bằng Ủy nhiệm chi
                        case 126:
                            break;
                        //Mua hàng bằng Ủy nhiệm chi 
                        case 127:
                            break;
                        //Trả tiền NCC bằng Ủy nhiệm chi
                        case 128:
                            break;
                        //Mua TSCĐ bằng Ủy nhiệm chi
                        case 129:
                            break;
                    }
                    break;
                #endregion

                #region MBCreditCard
                case "Accounting.Core.Domain.MBCreditCard":
                    MBCreditCard mbCreditCard = (MBCreditCard)(object)input;
                    listGenTemp = GenGeneralLedgers(mbCreditCard);
                    break;
                #endregion

                #region MBInternalTransfer
                case "Accounting.Core.Domain.MBInternalTransfer":
                    MBInternalTransfer mbInternalTransfer = (MBInternalTransfer)(object)input;
                    listGenTemp = GenGeneralLedgers(mbInternalTransfer);
                    break;
                #endregion

                #region SAInvoice
                case "Accounting.Core.Domain.SAInvoice":
                    SAInvoice saInvoice = (SAInvoice)(object)input;
                    listGenTemp = GenGeneralLedgers(saInvoice);
                    //Lưu sổ kho trong trường hợp kiêm phiếu xuất
                    if (saInvoice.OutwardNo != null)
                    {
                        listReposTemp = GenRepositoryLedgers(saInvoice);
                    }
                    if (saInvoice.OutwardNo != null && saInvoice.SAInvoiceDetails.Count > 0)
                    {
                        foreach (var x in saInvoice.SAInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    //_IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt + x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt + (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal + (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    //_IEMContractDetailMGService.Update(md);
                                    //_IEMContractDetailMGService.CommitTran();
                                    listObj.Add(md);
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region SAReturn
                case "Accounting.Core.Domain.SAReturn":
                    SAReturn saReturn = (SAReturn)(object)input;
                    listGenTemp = GenGeneralLedgers(saReturn);
                    listReposTemp = GenRepositoryLedgers(saReturn);
                    if (saReturn.SAReturnDetails.Count > 0)
                    {
                        foreach (var x in saReturn.SAReturnDetails)
                        {
                            if (x.SAInvoiceDetailID != null)
                            {
                                var sadetail = _ISAInvoiceDetailService.Getbykey(x.SAInvoiceDetailID ?? Guid.Empty);
                                if (sadetail != null && sadetail.ContractID != null)
                                {
                                    EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                    if (md != null)
                                    {
                                        //_IEMContractDetailMGService.BeginTran();
                                        md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                        md.AmountReceipt = md.AmountReceipt - (x.Amount + x.VATAmount - x.DiscountAmount);
                                        md.AmountReceiptOriginal = md.AmountReceiptOriginal - (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                        //_IEMContractDetailMGService.Update(md);
                                        //_IEMContractDetailMGService.CommitTran();
                                        listObj.Add(md);
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region PPInvoice
                case "Accounting.Core.Domain.PPInvoice":
                    PPInvoice ppInvoice = (PPInvoice)(object)input;
                    listGenTemp = GenGeneralLedgers(ppInvoice);
                    switch (ppInvoice.TypeID)
                    {
                        //Mua hàng chưa thanh toán
                        case 210:
                            break;
                        //Hàng mua trả lại
                        case 220:
                            break;
                        //Hàng mua giảm giá
                        case 230:
                            break;
                        //Mua dịch vụ chưa thanh toán
                        case 240:
                            break;
                        //Trả tiền nhà cung cấp
                        case 250:
                            break;
                        //Hóa đơn mua hàng(TT Tiền mặt)
                        case 260:
                            break;
                        //Hóa đơn mua hàng(TT Ủy nhiệm chi)
                        case 261:
                            MBTellerPaper mbTellerPaperPp = _IMBTellerPaperService.Getbykey(ppInvoice.ID);
                            if (mbTellerPaperPp != null)
                            {
                                //mbTellerPaperPp.Recorded = true;
                                //_IMBTellerPaperService.Update(mbTellerPaperPp);
                                listObj.Add(mbTellerPaperPp);
                            }
                            break;
                        //Hóa đơn mua hàng(Chuyển khoản)
                        case 262:
                            break;
                        //Hóa đơn mua hàng(TT Séc tiền mặt)
                        case 263:
                            break;
                        //Hóa đơn mua hàng(TT Thẻ tín dụng)
                        case 264:
                            break;
                    }
                    //Trường hợp mua hàng qua kho thì ghi chứng từ vào sổ kho
                    if (ppInvoice.StoredInRepository == true)
                    {
                        listReposTemp = GenRepositoryLedgers(ppInvoice);
                    }
                    if (ppInvoice.PPInvoiceDetails.Count > 0)
                    {
                        foreach (var x in ppInvoice.PPInvoiceDetails)
                        {
                            if (x.ContractID != null)
                            {
                                EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                if (md != null)
                                {
                                    //_IEMContractDetailMGService.BeginTran();
                                    md.QuantityReceipt = md.QuantityReceipt + x.Quantity ?? 0;
                                    md.AmountReceipt = md.AmountReceipt + (x.Amount + x.VATAmount - x.DiscountAmount);
                                    md.AmountReceiptOriginal = md.AmountReceiptOriginal + (x.AmountOriginal + x.VATAmountOriginal - x.DiscountAmountOriginal);
                                    //_IEMContractDetailMGService.Update(md);
                                    //_IEMContractDetailMGService.CommitTran();
                                    listObj.Add(md);
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region PPDiscountReturn
                case "Accounting.Core.Domain.PPDiscountReturn":
                    PPDiscountReturn ppDiscountReturn = (PPDiscountReturn)(object)input;
                    listGenTemp = GenGeneralLedgers(ppDiscountReturn);
                    listReposTemp = GenRepositoryLedgers(ppDiscountReturn);
                    if (ppDiscountReturn.PPDiscountReturnDetails.Count > 0)
                    {
                        foreach (var x in ppDiscountReturn.PPDiscountReturnDetails)
                        {
                            if (x.ConfrontDetailID != null)
                            {
                                var ppdetail = IPPInvoiceDetailService.Getbykey(x.ConfrontDetailID ?? Guid.Empty);
                                if (ppdetail != null && ppdetail.ContractID != null)
                                {
                                    EMContractDetailMG md = _IEMContractDetailMGService.GetEMContractDetailMGByContractIDandMID(x.ContractID ?? Guid.Empty, x.MaterialGoodsID ?? Guid.Empty);
                                    if (md != null)
                                    {
                                        //_IEMContractDetailMGService.BeginTran();
                                        md.QuantityReceipt = md.QuantityReceipt - x.Quantity ?? 0;
                                        md.AmountReceipt = md.AmountReceipt - ((x.Amount ?? 0) + (x.VATAmount ?? 0));
                                        md.AmountReceiptOriginal = md.AmountReceiptOriginal - ((x.AmountOriginal ?? 0) + (x.VATAmountOriginal ?? 0));
                                        //_IEMContractDetailMGService.Update(md);
                                        //_IEMContractDetailMGService.CommitTran();
                                        listObj.Add(md);
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region PPService
                case "Accounting.Core.Domain.PPService":
                    PPService ppService = (PPService)(object)input;
                    listGenTemp = GenGeneralLedgers(ppService);
                    break;
                #endregion

                #region RSInwardOutward
                case "Accounting.Core.Domain.RSInwardOutward":
                    RSInwardOutward rsInwardOutward = (RSInwardOutward)(object)input;
                    listGenTemp = GenGeneralLedgers(rsInwardOutward);
                    listReposTemp = GenRepositoryLedgers(rsInwardOutward);
                    break;
                #endregion

                #region RSAssemblyDismantlement
                //case "Accounting.Core.Domain.RSAssemblyDismantlement":
                //    RSAssemblyDismantlement rsAssemblyDismantlement = (RSAssemblyDismantlement)(object)input;
                //    listGenTemp = GenGeneralLedgers(rsAssemblyDismantlement);
                //    listReposTemp = GenRepositoryLedgers(rsAssemblyDismantlement);
                //    break;
                #endregion

                #region RSTransfer
                case "Accounting.Core.Domain.RSTransfer":
                    RSTransfer rsTransfer = (RSTransfer)(object)input;
                    listGenTemp = GenGeneralLedgers(rsTransfer);
                    listReposTemp = GenRepositoryLedgers(rsTransfer);
                    break;
                #endregion

                #region GOtherVoucher
                case "Accounting.Core.Domain.GOtherVoucher":
                    GOtherVoucher gOtherVoucher = (GOtherVoucher)(object)input;
                    listGenTemp = GenGeneralLedgers(gOtherVoucher);
                    break;
                #endregion
                //Fix

                #region FAIncrement
                case "Accounting.Core.Domain.FAIncrement":
                    FAIncrement faIncrement = (FAIncrement)(object)input;
                    listGenTemp = GenGeneralLedgers(faIncrement);
                    listFixedAssetTemp = GenFixedAssetLedgers(faIncrement);
                    break;
                #endregion

                #region FADecrement
                case "Accounting.Core.Domain.FADecrement":
                    FADecrement faDecrement = (FADecrement)(object)input;
                    listGenTemp = GenGeneralLedgers(faDecrement);
                    listFixedAssetTemp = GenFixedAssetLedgers(faDecrement);
                    break;
                #endregion

                #region FAAdjustment
                case "Accounting.Core.Domain.FAAdjustment":
                    FAAdjustment faAdjustment = (FAAdjustment)(object)input;
                    listGenTemp = GenGeneralLedgers(faAdjustment);
                    listFixedAssetTemp = GenFixedAssetLedgers(faAdjustment);
                    break;
                #endregion

                #region FADepreciation
                case "Accounting.Core.Domain.FADepreciation":
                    FADepreciation faDepreciation = (FADepreciation)(object)input;
                    listGenTemp = GenGeneralLedgers(faDepreciation);
                    listFixedAssetTemp = GenFixedAssetLedgers(faDepreciation);
                    break;
                #endregion

                #region FATransfer
                case "Accounting.Core.Domain.FATransfer":
                    FATransfer faTransfer = (FATransfer)(object)input;
                    listGenTemp = GenGeneralLedgers(faTransfer);
                    listFixedAssetTemp = GenFixedAssetLedgers(faTransfer);
                    break;
                #endregion
                //--------------
                //Tools

                #region TIIncrement
                case "Accounting.Core.Domain.TIIncrement":
                    TIIncrement tiIncrement = (TIIncrement)(object)input;
                    listGenTemp = GenGeneralLedgers(tiIncrement);
                    listToolLedgerTemp = GenToolLedgers(tiIncrement);
                    break;
                #endregion

                #region TIDecrement
                case "Accounting.Core.Domain.TIDecrement":
                    TIDecrement tiDecrement = (TIDecrement)(object)input;
                    //listGenTemp = GenGeneralLedgers(tiDecrement);
                    listToolLedgerTemp = GenToolLedgers(tiDecrement);
                    break;
                #endregion

                #region TIAdjustment
                case "Accounting.Core.Domain.TIAdjustment":
                    TIAdjustment tiAdjustment = (TIAdjustment)(object)input;
                    //listGenTemp = GenGeneralLedgers(tiAdjustment);
                    listToolLedgerTemp = GenToolLedgers(tiAdjustment);
                    break;
                #endregion

                #region TITransfer
                case "Accounting.Core.Domain.TITransfer":
                    TITransfer TITransfer = (TITransfer)(object)input;
                    //listGenTemp = GenGeneralLedgers(TITransfer);
                    listToolLedgerTemp = GenToolLedgers(TITransfer);
                    break;
                #endregion

                #region TIAllocation
                case "Accounting.Core.Domain.TIAllocation":
                    TIAllocation TIAllocation = (TIAllocation)(object)input;
                    listGenTemp = GenGeneralLedgers(TIAllocation);
                    listToolLedgerTemp = GenToolLedgers(TIAllocation);
                    break;
                    #endregion
                    //-------------
            }
            if ((listGenTemp != null) && (listGenTemp.Count > 0) && _ISystemOptionService.Query.FirstOrDefault(x => x.ID == 98).Data == "0")
            {
                //var list = _IViewGLPayExceedCashService.GetAll().CloneObject();
                var list = lstViewGLPayExceedCash;
                var listAccount = _IAccountService.Query.Where(x => x.AccountNumber.StartsWith("111") || x.AccountNumber.StartsWith("112")).ToList();
                if (list.Count < listAccount.Count)
                {
                    foreach (var acc in listAccount.Where(x => !list.Any(c => c.Account == x.AccountNumber)).ToList())
                    {
                        ViewGLPayExceedCash m1 = new ViewGLPayExceedCash();
                        m1.ID = Guid.NewGuid();
                        m1.Account = acc.AccountNumber;
                        m1.DebitAmount = 0;
                        m1.DebitAmountOriginal = 0;
                        m1.CreditAmount = 0;
                        m1.CreditAmountOriginal = 0;
                        list.Add(m1);
                    }
                }
                var lstAcc = list.Select(x => x.Account).ToList();
                foreach (var generalLedger in listGenTemp)
                {
                    if (lstAcc.Any(x => x == generalLedger.Account) && generalLedger.CreditAmount > 0)
                    {
                        var model = list.FirstOrDefault(x => x.Account == generalLedger.Account);
                        list.FirstOrDefault(x => x.Account == generalLedger.Account).CreditAmount = model.CreditAmount + generalLedger.CreditAmount;
                    }
                }
                foreach (var item in list)
                {
                    if (item.DebitAmount - item.CreditAmount < 0 && listGenTemp.Any(x => list.Any(c => c.Account == x.Account)))
                    {
                        //MessageBox.Show("Ghi sổ thất bại! Số tiền chi hiện đang vượt quá số tồn quỹ. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        msg = "Ghi sổ thất bại! Số tiền chi hiện đang vượt quá số tồn quỹ.";
                        return new List<object>();
                    }
                }
            }
            //if ((listGenTemp != null) && (listGenTemp.Count > 0))
            //{
            //    if (!_IAccountService.Query.FirstOrDefault(x => x.AccountNumber == listGenTemp[0].Account).IsForeignCurrency && listGenTemp[0].CurrencyID != "VND")
            //    {
            //        MessageBox.Show("Ghi sổ thất bại! Tài khoản hạch toán không chi tiết theo ngoại tệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }
            //}
            if (null != propRecord && propRecord.CanWrite)
            {
                propRecord.SetValue(input, true, null);
            }
            //testInvoke.Update(input); // Update lại chứng từ sang trạng thái ghi sổ
            //if ((listGenTemp != null) && (listGenTemp.Count > 0))
            //{
            //    foreach (var generalLedger in listGenTemp)
            //    {
            //        CreateNew(generalLedger);
            //    }
            //}
            //if ((listReposTemp != null) && (listReposTemp.Count > 0))
            //{
            //    foreach (var repositoryLedger in listReposTemp)
            //    {
            //        _IRepositoryLedgerService.CreateNew(repositoryLedger);
            //    }
            //}
            //if ((listFixedAssetTemp != null) && (listFixedAssetTemp.Count > 0))
            //{
            //    foreach (var fixedAssetLedger in listFixedAssetTemp)
            //    {
            //        _IFixedAssetLedgerService.CreateNew(fixedAssetLedger);
            //    }
            //}
            //if ((listToolLedgerTemp != null) && (listToolLedgerTemp.Count > 0))
            //{
            //    foreach (var fixedAssetLedger in listToolLedgerTemp)
            //    {
            //        _IToolLedgerService.CreateNew(fixedAssetLedger);
            //    }
            //}

            if ((listGenTemp != null) && (listGenTemp.Count > 0))
                listObj.AddRange(listGenTemp);
            if ((listReposTemp != null) && (listReposTemp.Count > 0))
                listObj.AddRange(listReposTemp);
            if ((listFixedAssetTemp != null) && (listFixedAssetTemp.Count > 0))
                listObj.AddRange(listFixedAssetTemp);
            if ((listToolLedgerTemp != null) && (listToolLedgerTemp.Count > 0))
                listObj.AddRange(listToolLedgerTemp);

            return listObj;
        }
        #endregion

        #region Chứng từ ghi sổ
        /// <summary>
        /// Lấy ra danh sách chứng từ cần thao tác vào bảng chứng từ ghi sổ
        /// </summary>
        /// <param name="typeID">loại chứng từ cần lấy ra</param>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <returns></returns>
        public List<GVoucherListDetail> GetRecordingVouchers(int typeGroupID, DateTime fromDate, DateTime toDate)
        {
            UnbindSession(Query.ToList());
            List<int> lstTypeID = new List<int>();
            List<GVoucherListDetail> lstVoucherLists = _IGVoucherListDetailService.Query.ToList();
            List<GVoucherListDetail> listGVoucherListDetails = new List<GVoucherListDetail>();
            List<Domain.Type> lstType = _ITypeService.Query.Where(n => n.TypeGroupID == typeGroupID).ToList();
            if (lstType.Count > 0) lstTypeID = lstType.Select(n => n.ID).ToList();
            if (typeGroupID == 10)
            {
                lstTypeID.AddRange(new List<int> { 321, 324 });
            }
            else if (typeGroupID == 16)
            {
                lstTypeID.AddRange(new List<int> { 322, 325 });
            }
            else if (typeGroupID == 32)
            {
                lstTypeID.Remove(321);
                lstTypeID.Remove(322);
                lstTypeID.Remove(323);
                lstTypeID.Remove(325);
            }
            #region Code cũ 
            //listGVoucherListDetails = (from c in Query.Where(c => c.PostedDate >= fromDate && fromDate != toDate && c.PostedDate <= toDate && c.TypeID == typeID).ToList()
            //                           where !lstVoucherLists.Any(d => d.VoucherID == c.ReferenceID && d.VoucherTypeID == c.TypeID)
            //                           group c by new { c.TypeID, c.ReferenceID, c.PostedDate, c.No, c.Date, c.CurrencyID } into g
            //                           //orderby g.Key
            //                           let firstOrDefault = g.FirstOrDefault()
            //                           where firstOrDefault != null
            //                           let lastOrDefault = g.LastOrDefault()
            //                           where lastOrDefault != null
            //                           select new GVoucherListDetail()
            //                           {
            //                               Status = false,
            //                               ID = Guid.NewGuid(),
            //                               VoucherID = g.Key.ReferenceID,
            //                               VoucherDetailID = Guid.NewGuid(),
            //                               VoucherTypeID = g.Key.TypeID,
            //                               VoucherDate = g.Key.Date,
            //                               VoucherPostedDate = g.Key.PostedDate,
            //                               VoucherNo = g.Key.No,
            //                               VoucherDescription = firstOrDefault.Description,
            //                               VoucherDebitAccount = firstOrDefault.DebitAmount > 0 ? firstOrDefault.Account : lastOrDefault.Account,
            //                               VoucherCreditAccount = lastOrDefault.CreditAmount > 0 ? lastOrDefault.Account : firstOrDefault.Account,
            //                               VoucherAmount = (g.Sum(c => c.DebitAmount) > g.Sum(c => c.CreditAmount)) ? g.Sum(c => c.DebitAmount) : g.Sum(c => c.CreditAmount),
            //                               CurrencyID = g.Key.CurrencyID,
            //                               Reason = firstOrDefault.Reason,
            //                               OrderPriority = lastOrDefault.OrderPriority ?? 0
            //                           }).OrderBy(c => c.VoucherPostedDate).ThenBy(c => c.VoucherNo).ToList();
            #endregion Code cũ
            #region Code mới
            listGVoucherListDetails = (from c in Query.Where(c => c.PostedDate >= fromDate /*&& fromDate != toDate*/ && c.PostedDate <= toDate && lstTypeID.Contains(c.TypeID) && c.DebitAmount > 0).ToList()
                                       where !lstVoucherLists.Any(d => d.GeneralLedgerID == c.ID && d.VoucherTypeID == c.TypeID)
                                       //group c by new { c.TypeID, c.ReferenceID, c.PostedDate, c.No, c.Date, c.CurrencyID, c.DetailID, c.Description } into g
                                       //orderby g.Key
                                       //let firstOrDefault = g.FirstOrDefault()
                                       //where firstOrDefault != null
                                       //let lastOrDefault = g.LastOrDefault()
                                       //where lastOrDefault != null
                                       select new GVoucherListDetail()
                                       {
                                           Status = false,
                                           ID = Guid.NewGuid(),
                                           VoucherID = c.ReferenceID,
                                           VoucherDetailID = c.DetailID ?? Guid.Empty,
                                           VoucherTypeID = c.TypeID,
                                           VoucherDate = c.Date,
                                           VoucherPostedDate = c.PostedDate,
                                           VoucherNo = c.No,
                                           VoucherDescription = c.Description,
                                           VoucherDebitAccount = c.Account,
                                           VoucherCreditAccount = c.AccountCorresponding,
                                           VoucherAmount = Math.Round(c.DebitAmount, MidpointRounding.AwayFromZero),
                                           CurrencyID = c.CurrencyID,
                                           Reason = c.Reason,
                                           OrderPriority = c.OrderPriority ?? 0,
                                           GeneralLedgerID = c.ID
                                       }).OrderBy(c => c.VoucherPostedDate).ThenBy(c => c.VoucherNo).ToList();
            #endregion

            return listGVoucherListDetails;
        }
        public List<GVoucherListDetail> GetRecordingVouchers(List<int> lstTypeGroupID, DateTime fromDate, DateTime toDate)
        {
            UnbindSession(Query.ToList());
            List<int> lstTypeID = new List<int>();
            List<GVoucherListDetail> lstVoucherLists = _IGVoucherListDetailService.Query.ToList();
            List<GVoucherListDetail> listGVoucherListDetails = new List<GVoucherListDetail>();
            List<Domain.Type> lstType = new List<Domain.Type>();
            foreach (var item in lstTypeGroupID)
            {
                List<Domain.Type> lstTypeDT = _ITypeService.Query.Where(n => n.TypeGroupID == item).ToList();
                if (lstTypeDT.Count > 0) lstTypeID.AddRange(lstTypeDT.Select(n => n.ID).ToList());
                if (item == 10)
                {
                    lstTypeID.AddRange(new List<int> { 321, 324 });
                }
                else if (item == 16)
                {
                    lstTypeID.AddRange(new List<int> { 322, 325 });
                }
                else if (item == 32)
                {
                    lstTypeID.Remove(321);
                    lstTypeID.Remove(322);
                    lstTypeID.Remove(323);
                    lstTypeID.Remove(325);
                }
            }

            #region Code mới
            
            var list_genera = Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && lstTypeID.Contains(c.TypeID) && c.DebitAmount > 0).ToList();
            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query;
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query;

            var list_genera1 = (from a in list_genera
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in list_genera
                                join i in listSAInvoice on a.ReferenceID equals i.ID
                                //join b in listSABill on i.BillRefID equals b.ID
                                join d in list_delete on i.BillRefID equals d.SAInvoiceID
                                select a).ToList();
            list_genera = (from a in list_genera
                           where !list_genera1.Any(t => t.ID == a.ID)
                           select a).ToList();
            list_genera = (from a in list_genera
                           where !list_genera2.Any(t => t.ID == a.ID)
                           select a).ToList();

            listGVoucherListDetails = (from c in list_genera
                                       where !lstVoucherLists.Any(d => d.GeneralLedgerID == c.ID && d.VoucherTypeID == c.TypeID)
                                       join b in _ITypeService.Query on c.TypeID equals b.ID
                                       select new GVoucherListDetail()
                                       {
                                           Status = false,
                                           ID = Guid.NewGuid(),
                                           VoucherID = c.ReferenceID,
                                           VoucherDetailID = c.DetailID ?? Guid.Empty,
                                           VoucherTypeID = c.TypeID,
                                           VoucherDate = c.Date,
                                           VoucherPostedDate = c.PostedDate,
                                           VoucherNo = c.No,
                                           VoucherDescription = c.Description,
                                           VoucherDebitAccount = c.Account,
                                           VoucherCreditAccount = c.AccountCorresponding,
                                           VoucherAmount = Math.Round(c.DebitAmount, MidpointRounding.AwayFromZero),
                                           CurrencyID = c.CurrencyID,
                                           Reason = c.Reason,
                                           OrderPriority = c.OrderPriority ?? 0,
                                           GeneralLedgerID = c.ID,
                                           Type = b.TypeName,
                                           ordercode = 2 //edit by tungnt: sắp xếp chứng từ để hiển thị lên grid. Loại chứng từ ordercode = 1, cộng nhóm ordercode = 3
                                       }).OrderBy(c => c.VoucherPostedDate).ThenBy(c => c.VoucherNo).ToList();
            #endregion

            return listGVoucherListDetails;
        }
        #endregion


        public List<GeneralLeged_TypeName> GetListEmp(Guid emplID, DateTime fromdate, DateTime todate)
        {
            List<Accounting.Core.Domain.Type> lstType = _ITypeService.GetAll();
            List<GeneralLeged_TypeName> lst = (from p in Query
                                               where p.AccountingObjectID == emplID
                                               && p.Date >= fromdate && p.Date <= todate
                                               select new GeneralLeged_TypeName()
                                               {
                                                   TypeID = p.TypeID,
                                                   Date = p.Date,
                                                   No = p.No,
                                                   Description = p.Description
                                               }).ToList();
            foreach (var item in lst)
            {
                foreach (var it in lstType)
                {
                    if (item.TypeID == it.ID)
                    {
                        item.TypeName = it.TypeName;
                        break;
                    }
                }
            }
            return lst;
        }

        #region Tính tỉ gía xuất qũy theo phương pháp bình quân cuối kỳ
        /// <summary>
        /// Tính tỉ gía xuất qũy theo phương pháp bình quân cuối kỳ
        /// </summary>
        /// <param name="listCurrency">danh sách loại tiền</param>
        /// <param name="toDate">Đến ngày</param>
        /// <returns></returns>
        public GOtherVoucher FormulaExchangeRate_AverageForEndOfPeriods(List<Currency> listCurrency, DateTime toDate)
        {
            #region code cũ
            //DateTime fromdate = /*toDate.AddMonths(1).AddDays(-1)*/new DateTime((int)toDate.Year, (int)toDate.Month, 1);

            //List<int> listTypeGetExchangeRate = new List<int>() { 100, 101, 102, 103, 160, 161, 162, 163 };
            //List<int> listTypeSetExchangeRate = new List<int>();
            //for (int i = 110; i < 144; i++)
            //{
            //    listTypeSetExchangeRate.Add(i);
            //}
            //List<GeneralLedger> listGeneralLedgersAll = Query.Where(c => c.PostedDate <= fromdate).ToList();

            //if (listGeneralLedgersAll.Count <= 0)
            //    return new GOtherVoucher();

            //List<GeneralLedger> listGeneralLedgersGet =
            //    listGeneralLedgersAll.Where(c => listTypeGetExchangeRate.Any(d => d == c.TypeID) && listCurrency.Any(b => b.ID == c.CurrencyID)).ToList();
            //List<GeneralLedger> listGeneralLedgersSet =
            //    listGeneralLedgersAll.Where(c => listTypeSetExchangeRate.Any(d => d == c.TypeID) && listCurrency.Any(b => b.ID == c.CurrencyID)).ToList();

            //GOtherVoucher gOtherVoucher = new GOtherVoucher()
            //{
            //    ID = Guid.NewGuid(),
            //    TypeID = 600,
            //    PostedDate = toDate,
            //    Date = toDate,
            //    //No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(60)),
            //    Reason = "Tỷ giá xuất quỹ",
            //    TotalAmount = 0,
            //    TotalAmountOriginal = 0,
            //    Recorded = false,
            //    Exported = true,
            //};
            //var firstOrDefault = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLai");
            //var systemOption = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLo");
            //var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TyGia");
            //if (firstOrDefault != null && systemOption != null && ddSoTyGia != null)
            //{
            //    string accountTkcLechLai = firstOrDefault.Data;
            //    string accountTkcLechLo = systemOption.Data;
            //    int ngoaite = int.Parse(ddSoTyGia.Data);
            //    foreach (Currency currency in listCurrency)
            //    {
            //        if (currency.ID == "VND")
            //            continue;
            //        decimal updatingExchangeRate =
            //            ((decimal)(listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.DebitAmount) //nhập kỳ trước
            //                       - listGeneralLedgersSet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.CreditAmount) //xuất kỳ trước
            //                       + listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).Sum(c => c.DebitAmount) //nhập trong kỳ này

            //                )) /
            //            ((decimal)(listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.DebitAmountOriginal) //nhập kỳ trước
            //                            - listGeneralLedgersSet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.CreditAmountOriginal) //xuất kỳ trước
            //                            + listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).Sum(c => c.DebitAmountOriginal) //nhập trong kỳ này
            //                            )
            //                 == 0
            //                ? 1
            //                : (decimal)(listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.DebitAmountOriginal) //nhập kỳ trước
            //                            - listGeneralLedgersSet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.CreditAmountOriginal) //xuất kỳ trước
            //                            + listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).Sum(c => c.DebitAmountOriginal) //nhập trong kỳ này
            //                    ));
            //        updatingExchangeRate = Math.Round(updatingExchangeRate, ngoaite);
            //        Currency currency1 = currency;
            //        var group = (from t in listGeneralLedgersSet.Where(c => c.CurrencyID == currency1.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate)
            //                     where t.CreditAmount > 0 && t.CreditAmountOriginal > 0
            //                     group t by t.Account
            //                         into g
            //                     select new { g.Key, g }).ToList();
            //        foreach (var t in @group)
            //        {
            //            decimal? amount =
            //                t.g.Select(c => new { sum = (updatingExchangeRate - c.ExchangeRate) * c.CreditAmount })
            //                    .Sum(c => c.sum);
            //            GOtherVoucherDetail gOtherVoucherDetail = new GOtherVoucherDetail
            //            {
            //                GOtherVoucherID = gOtherVoucher.ID,
            //                Description = "Tỷ giá xuất quỹ",
            //                DebitAccount = amount > 0 ? t.Key : accountTkcLechLo,
            //                CreditAccount = amount > 0 ? accountTkcLechLai : t.Key,
            //                CurrencyID = currency.ID,
            //                ExchangeRate = updatingExchangeRate,
            //                Amount = Math.Abs(amount ?? 0),
            //                AmountOriginal = Math.Abs(amount ?? 0),
            //                IsMatch = false
            //            };
            //            gOtherVoucher.GOtherVoucherDetails.Add(gOtherVoucherDetail);
            //        }
            //    }
            //}
            //gOtherVoucher.TotalAmountOriginal = gOtherVoucher.GOtherVoucherDetails.Sum(c => c.AmountOriginal);
            //return gOtherVoucher;
            #endregion
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);
            DateTime fromdate = new DateTime((int)toDate.Year, (int)toDate.Month, 1);

            List<GeneralLedger> listGeneralLedgersAll = Query.Where(c => (c.Account.StartsWith("1112") || c.Account.StartsWith("1122")) && c.PostedDate <= toDate).ToList();

            List<GeneralLedger> listGeneralLedgersIn =
                listGeneralLedgersAll.Where(c => c.DebitAmount != 0 && c.DebitAmountOriginal != 0 && listCurrency.Any(b => b.ID == c.CurrencyID)).ToList();
            List<GeneralLedger> listGeneralLedgersOut =
                listGeneralLedgersAll.Where(c => c.CreditAmount != 0 && c.CreditAmountOriginal != 0 && listCurrency.Any(b => b.ID == c.CurrencyID)).ToList();

            GOtherVoucher gOtherVoucher = new GOtherVoucher()
            {
                ID = Guid.NewGuid(),
                TypeID = 660,
                PostedDate = toDate,
                Date = toDate,
                No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(60)),
                Reason = string.Format("Xử lý chênh lệch tỷ giá xuất quỹ tháng {0} năm {1}", toDate.Month, toDate.Year),
                TotalAmount = 0,
                TotalAmountOriginal = 0,
                CurrencyID = "VND",
                ExchangeRate = 1,
                Recorded = false,
                Exported = true,
            };
            var firstOrDefault = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLai");
            var systemOption = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLo");
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TyGia");
            if (firstOrDefault != null && systemOption != null && ddSoTyGia != null)
            {
                string accountTkcLechLai = firstOrDefault.Data;
                string accountTkcLechLo = systemOption.Data;
                int ngoaite = int.Parse(ddSoTyGia.Data);
                foreach (Currency currency in listCurrency)
                {
                    if (currency.ID == "VND")
                        continue;
                    var lstOutCaculator = (from a in listGeneralLedgersOut
                                           where a.CurrencyID == currency.ID
                                           group a by new { a.PostedDate.Month, a.PostedDate.Year, a.CurrencyID }
                                           into t
                                           select new GeneralLedger
                                           {
                                               PostedDate = new DateTime(t.Key.Year, t.Key.Month, 1),
                                               CreditAmount = t.Sum(c => c.CreditAmount),
                                               CreditAmountOriginal = t.Sum(c => c.CreditAmountOriginal)
                                           }).ToList().OrderBy(c => c.PostedDate).ToList();
                    var lstInCaculator = (from a in listGeneralLedgersIn
                                          where a.CurrencyID == currency.ID
                                          group a by new { a.PostedDate.Month, a.PostedDate.Year, a.CurrencyID }
                                           into t
                                          select new GeneralLedger
                                          {
                                              PostedDate = new DateTime(t.Key.Year, t.Key.Month, 1),
                                              DebitAmount = t.Sum(c => c.DebitAmount),
                                              DebitAmountOriginal = t.Sum(c => c.DebitAmountOriginal)
                                          }).ToList();
                    decimal updatingExchangeRate = 0;
                    if (lstOutCaculator.Count > 0)
                    {
                        for (int i = 0; i < lstOutCaculator.Count; i++)
                        {
                            decimal indknt = lstInCaculator.Where(c => c.PostedDate < lstOutCaculator[i].PostedDate).Sum(c => c.DebitAmountOriginal);
                            decimal outdknt = lstOutCaculator.Where(c => c.PostedDate < lstOutCaculator[i].PostedDate).Sum(c => c.CreditAmountOriginal);
                            decimal midnt = lstInCaculator.Where(c => c.PostedDate == lstOutCaculator[i].PostedDate).Sum(c => c.DebitAmountOriginal);
                            decimal indk = lstInCaculator.Where(c => c.PostedDate < lstOutCaculator[i].PostedDate).Sum(c => c.DebitAmount);
                            decimal outdk = CreditAmount(lstOutCaculator.Where(c => c.PostedDate < lstOutCaculator[i].PostedDate).ToList());
                            decimal mid = lstInCaculator.Where(c => c.PostedDate == lstOutCaculator[i].PostedDate).Sum(c => c.DebitAmount);
                            lstOutCaculator[i].CashOutExchangeRate = (indk - outdk + mid) / ((indknt - outdknt + midnt) == 0 ? 1 : (indknt - outdknt + midnt));
                            updatingExchangeRate = lstOutCaculator[i].CashOutExchangeRate;
                        }
                    }
                    #region comment
                    //decimal updatingExchangeRate =
                    //    ((decimal)(listGeneralLedgersIn.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.DebitAmount) //nhập kỳ trước
                    //               - listGeneralLedgersOut.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.CreditAmount) //xuất kỳ trước
                    //               + listGeneralLedgersIn.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).Sum(c => c.DebitAmount) //nhập trong kỳ này

                    //        )) /
                    //    ((decimal)(listGeneralLedgersIn.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.DebitAmountOriginal) //nhập kỳ trước
                    //                    - listGeneralLedgersOut.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.CreditAmountOriginal) //xuất kỳ trước
                    //                    + listGeneralLedgersIn.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).Sum(c => c.DebitAmountOriginal) //nhập trong kỳ này
                    //                    )
                    //         == 0
                    //        ? 1
                    //        : (decimal)(listGeneralLedgersIn.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.DebitAmountOriginal) //nhập kỳ trước
                    //                    - listGeneralLedgersOut.Where(c => c.CurrencyID == currency.ID && c.PostedDate < fromdate).Sum(c => c.CreditAmountOriginal) //xuất kỳ trước
                    //                    + listGeneralLedgersIn.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).Sum(c => c.DebitAmountOriginal) //nhập trong kỳ này
                    //            ));
                    //updatingExchangeRate = Math.Round(updatingExchangeRate, ngoaite);

                    //var lstUpdate = listGeneralLedgersOut.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate).ToList();
                    //BeginTran();
                    //foreach (var x in lstUpdate)
                    //{
                    //    #region PPInvoice
                    //    if (new int[] { 117, 127, 131, 141, 171, 260, 261, 262, 263, 264 }.Any(a => a == x.TypeID))
                    //    {
                    //        var ppInvoiceDetail = IPPInvoiceDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (ppInvoiceDetail != null)
                    //        {
                    //            ppInvoiceDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            ppInvoiceDetail.CashOutAmount = (ppInvoiceDetail.AmountOriginal * updatingExchangeRate);
                    //            ppInvoiceDetail.CashOutDifferAmount = Math.Abs(ppInvoiceDetail.Amount - ppInvoiceDetail.CashOutAmount);
                    //            ppInvoiceDetail.CashOutDifferAccount = (ppInvoiceDetail.Amount > ppInvoiceDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            ppInvoiceDetail.CashOutVATAmount = (ppInvoiceDetail.VATAmountOriginal * updatingExchangeRate);
                    //            ppInvoiceDetail.CashOutDifferVATAmount = Math.Abs(ppInvoiceDetail.VATAmount - ppInvoiceDetail.CashOutVATAmount);
                    //            IPPInvoiceDetailService.Update(ppInvoiceDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region PPService
                    //    if (new int[] { 116, 126, 133, 143, 173 }.Any(a => a == x.TypeID))
                    //    {
                    //        var ppServiceDetail = IPPServiceDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (ppServiceDetail != null)
                    //        {
                    //            ppServiceDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            ppServiceDetail.CashOutAmount = (ppServiceDetail.AmountOriginal ?? 0 * updatingExchangeRate);
                    //            ppServiceDetail.CashOutDifferAmount = Math.Abs(ppServiceDetail.Amount ?? 0 - ppServiceDetail.CashOutAmount);
                    //            ppServiceDetail.CashOutDifferAccount = (ppServiceDetail.Amount > ppServiceDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            ppServiceDetail.CashOutVATAmount = (ppServiceDetail.VATAmountOriginal ?? 0 * updatingExchangeRate);
                    //            ppServiceDetail.CashOutDifferVATAmount = Math.Abs(ppServiceDetail.VATAmount ?? 0 - ppServiceDetail.CashOutVATAmount);
                    //            IPPServiceDetailService.Update(ppServiceDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region FAIncrement
                    //    if (new int[] { 119, 129, 132, 142, 172 }.Any(a => a == x.TypeID))
                    //    {
                    //        var faIncrementDetail = IFAIncrementDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (faIncrementDetail != null)
                    //        {
                    //            faIncrementDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            faIncrementDetail.CashOutAmount = (faIncrementDetail.AmountOriginal * updatingExchangeRate);
                    //            faIncrementDetail.CashOutDifferAmount = Math.Abs(faIncrementDetail.Amount - faIncrementDetail.CashOutAmount);
                    //            faIncrementDetail.CashOutDifferAccount = (faIncrementDetail.Amount > faIncrementDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            faIncrementDetail.CashOutVATAmount = (faIncrementDetail.VATAmountOriginal * updatingExchangeRate);
                    //            faIncrementDetail.CashOutDifferVATAmount = Math.Abs(faIncrementDetail.VATAmount - faIncrementDetail.CashOutVATAmount);
                    //            IFAIncrementDetailService.Update(faIncrementDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region TIIncrement
                    //    if (new int[] { 902, 903, 904, 905, 906 }.Any(a => a == x.TypeID))
                    //    {
                    //        var tiIncrementDetail = ITIIncrementDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (tiIncrementDetail != null)
                    //        {
                    //            tiIncrementDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            tiIncrementDetail.CashOutAmount = (tiIncrementDetail.AmountOriginal * updatingExchangeRate);
                    //            tiIncrementDetail.CashOutDifferAmount = Math.Abs(tiIncrementDetail.Amount - tiIncrementDetail.CashOutAmount);
                    //            tiIncrementDetail.CashOutDifferAccount = (tiIncrementDetail.Amount > tiIncrementDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            tiIncrementDetail.CashOutVATAmount = (tiIncrementDetail.VATAmountOriginal * updatingExchangeRate);
                    //            tiIncrementDetail.CashOutDifferVATAmount = Math.Abs(tiIncrementDetail.VATAmount - tiIncrementDetail.CashOutVATAmount);
                    //            ITIIncrementDetailService.Update(tiIncrementDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region SAReturn
                    //    if (new int[] { 330, 340 }.Any(a => a == x.TypeID))
                    //    {
                    //        var saReturnDetail = ISAReturnDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (saReturnDetail != null)
                    //        {
                    //            saReturnDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            saReturnDetail.CashOutAmount = (saReturnDetail.AmountOriginal * updatingExchangeRate);
                    //            saReturnDetail.CashOutDifferAmount = Math.Abs(saReturnDetail.Amount - saReturnDetail.CashOutAmount);
                    //            saReturnDetail.CashOutDifferAccount = (saReturnDetail.Amount > saReturnDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            saReturnDetail.CashOutVATAmount = (saReturnDetail.VATAmountOriginal * updatingExchangeRate);
                    //            saReturnDetail.CashOutDifferVATAmount = Math.Abs(saReturnDetail.VATAmount - saReturnDetail.CashOutVATAmount);
                    //            ISAReturnDetailService.Update(saReturnDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region MCPayment
                    //    if (new int[] { 110 }.Any(a => a == x.TypeID))
                    //    {
                    //        var mcPaymentDetail = IMCPaymentDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (mcPaymentDetail != null)
                    //        {
                    //            mcPaymentDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            mcPaymentDetail.CashOutAmount = (mcPaymentDetail.AmountOriginal * updatingExchangeRate);
                    //            mcPaymentDetail.CashOutDifferAmount = Math.Abs(mcPaymentDetail.Amount - mcPaymentDetail.CashOutAmount);
                    //            mcPaymentDetail.CashOutDifferAccount = (mcPaymentDetail.Amount > mcPaymentDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            IMCPaymentDetailService.Update(mcPaymentDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region MBTellerPaper
                    //    if (new int[] { 120, 130, 140 }.Any(a => a == x.TypeID))
                    //    {
                    //        var mbTellerPaperDetail = IMBTellerPaperDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (mbTellerPaperDetail != null)
                    //        {
                    //            mbTellerPaperDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            mbTellerPaperDetail.CashOutAmount = (mbTellerPaperDetail.AmountOriginal * updatingExchangeRate);
                    //            mbTellerPaperDetail.CashOutDifferAmount = Math.Abs(mbTellerPaperDetail.Amount - mbTellerPaperDetail.CashOutAmount);
                    //            mbTellerPaperDetail.CashOutDifferAccount = (mbTellerPaperDetail.Amount > mbTellerPaperDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            IMBTellerPaperDetailService.Update(mbTellerPaperDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region MBCreditCard
                    //    if (new int[] { 170 }.Any(a => a == x.TypeID))
                    //    {
                    //        var mbCreditCardDetail = IMBCreditCardDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (mbCreditCardDetail != null)
                    //        {
                    //            mbCreditCardDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            mbCreditCardDetail.CashOutAmount = (mbCreditCardDetail.AmountOriginal * updatingExchangeRate);
                    //            mbCreditCardDetail.CashOutDifferAmount = Math.Abs(mbCreditCardDetail.Amount - mbCreditCardDetail.CashOutAmount);
                    //            mbCreditCardDetail.CashOutDifferAccount = (mbCreditCardDetail.Amount > mbCreditCardDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            IMBCreditCardDetailService.Update(mbCreditCardDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region MBInternalTransfer
                    //    if (new int[] { 150 }.Any(a => a == x.TypeID))
                    //    {
                    //        var mbInternalTransferDetail = IMBInternalTransferDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (mbInternalTransferDetail != null)
                    //        {
                    //            mbInternalTransferDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            mbInternalTransferDetail.CashOutAmount = (mbInternalTransferDetail.AmountOriginal * updatingExchangeRate);
                    //            mbInternalTransferDetail.CashOutDifferAmount = Math.Abs(mbInternalTransferDetail.Amount - mbInternalTransferDetail.CashOutAmount);
                    //            mbInternalTransferDetail.CashOutDifferAccount = (mbInternalTransferDetail.Amount > mbInternalTransferDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            IMBInternalTransferDetailService.Update(mbInternalTransferDetail);
                    //        }
                    //    }
                    //    #endregion
                    //    #region GOtherVoucher
                    //    if (new int[] { 600 }.Any(a => a == x.TypeID))
                    //    {
                    //        var gotherVoucherDetail = IGOtherVoucherDetailService.Getbykey(x.DetailID ?? Guid.Empty);
                    //        if (gotherVoucherDetail != null)
                    //        {
                    //            gotherVoucherDetail.CashOutExchangeRate = updatingExchangeRate;
                    //            gotherVoucherDetail.CashOutAmount = (gotherVoucherDetail.AmountOriginal * updatingExchangeRate);
                    //            gotherVoucherDetail.CashOutDifferAmount = Math.Abs(gotherVoucherDetail.Amount - gotherVoucherDetail.CashOutAmount);
                    //            gotherVoucherDetail.CashOutDifferAccount = (gotherVoucherDetail.Amount > gotherVoucherDetail.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                    //            IGOtherVoucherDetailService.Update(gotherVoucherDetail);
                    //        }
                    //    }
                    //    #endregion
                    //}
                    //try
                    //{
                    //    CommitTran();
                    //}
                    //catch (Exception ex)
                    //{
                    //    RolbackTran();
                    //}
                    #endregion
                    var group = (from t in listGeneralLedgersOut.Where(c => c.CurrencyID == currency.ID && c.PostedDate <= toDate && c.PostedDate >= fromdate)
                                 group t by new { t.Account, t.ReferenceID, t.No, t.CurrencyID, t.TypeID } into g
                                 select new GeneralLedger
                                 {
                                     Account = g.Key.Account,
                                     CreditAmount = g.Sum(x => x.CreditAmount),
                                     CreditAmountOriginal = g.Sum(x => x.CreditAmountOriginal),
                                     ReferenceID = g.Key.ReferenceID,
                                     No = g.Key.No,
                                     CurrencyID = g.Key.CurrencyID,
                                     TypeID = g.Key.TypeID
                                 }).ToList();
                    foreach (var t in group)
                    {
                        decimal amount = (t.CreditAmountOriginal * updatingExchangeRate) - t.CreditAmount;
                        GOtherVoucherDetail gOtherVoucherDetail = new GOtherVoucherDetail
                        {
                            GOtherVoucherID = gOtherVoucher.ID,
                            Description = amount < 0 ? string.Format("Xử lý chênh lệch tỷ giá lãi tháng {0} năm {1} chứng từ {2}", toDate.Month, toDate.Year, t.No)
                            : string.Format("Xử lý chênh lệch tỷ giá lỗ tháng {0} năm {1} chứng từ {2}", toDate.Month, toDate.Year, t.No),
                            DebitAccount = amount < 0 ? t.Account : accountTkcLechLo,
                            CreditAccount = amount < 0 ? accountTkcLechLai : t.Account,
                            CurrencyID = t.CurrencyID,
                            ExchangeRate = updatingExchangeRate,
                            CashOutAmountVoucherOriginal = t.CreditAmountOriginal,
                            CashOutAmountVoucher = t.CreditAmount,
                            CashOutExchangeRate = updatingExchangeRate,
                            CashOutAmount = (t.CreditAmountOriginal * updatingExchangeRate),
                            CashOutDifferAmount = Math.Abs(amount),
                            Amount = Math.Abs(amount),
                            AmountOriginal = Math.Abs(amount),
                            RefTypeID = t.TypeID,
                            RefExchangeID = t.ReferenceID,
                            IsMatch = false
                        };
                        gOtherVoucher.GOtherVoucherDetails.Add(gOtherVoucherDetail);
                    }
                }
            }
            gOtherVoucher.TotalAmountOriginal = gOtherVoucher.GOtherVoucherDetails.Sum(c => c.CashOutDifferAmount);
            gOtherVoucher.TotalAmount = gOtherVoucher.GOtherVoucherDetails.Sum(c => c.CashOutDifferAmount);
            return gOtherVoucher;
        }

        #region Tính tiền xuất quỹ quy đổi đầu kỳ
        decimal CreditAmount(List<GeneralLedger> gl)
        {
            decimal Amount = 0;
            foreach (var x in gl)
            {
                Amount += (x.CreditAmountOriginal * x.CashOutExchangeRate);
            }
            return Amount;
        }
        #endregion
        #endregion

        #region Tính tỉ gía xuất qũy theo phương pháp bình quân tức thời
        /// <summary>
        /// Tính tỉ gía xuất qũy theo phương pháp bình quân tức thời
        /// </summary>
        /// <param name="listCurrency">danh sách loại tiền</param>
        /// <param name="toDate">Đến ngày</param>
        /// <returns></returns>
        public void FormulaExchangeRate_AverageForInstantaneous(object T, string Currency = null, DateTime? toDate = null)
        {
            DateTime fromdate = DateTime.Parse(_ISystemOptionService.Query.FirstOrDefault(c => c.Code == "ApplicationStartDate").Data);
            Guid? ID = null;
            List<Guid> lstupdate = new List<Guid>();
            var accountTkcLechLai = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLai").Data;
            var accountTkcLechLo = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLo").Data;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TyGia");
            int ngoaite = int.Parse(ddSoTyGia.Data);
            DateTime opnDate = fromdate.AddDays(-1);
            #region Model
            if (T.GetType() == typeof(PPInvoice))
            {
                PPInvoice model = T as PPInvoice;
                if (model.CurrencyID == "VND" || !model.PPInvoiceDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(PPService))
            {
                PPService model = T as PPService;
                if (model.CurrencyID == "VND" || !model.PPServiceDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(FAIncrement))
            {
                FAIncrement model = T as FAIncrement;
                if (model.CurrencyID == "VND" || !model.FAIncrementDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(TIIncrement))
            {
                TIIncrement model = T as TIIncrement;
                if (model.CurrencyID == "VND" || !model.TIIncrementDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(SAReturn))
            {
                SAReturn model = T as SAReturn;
                if (model.CurrencyID == "VND" || !model.SAReturnDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(MCPayment))
            {
                MCPayment model = T as MCPayment;
                if (model.CurrencyID == "VND" || !model.MCPaymentDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(MBTellerPaper))
            {
                MBTellerPaper model = T as MBTellerPaper;
                if (model.CurrencyID == "VND" || !model.MBTellerPaperDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(MBCreditCard))
            {
                MBCreditCard model = T as MBCreditCard;
                if (model.CurrencyID == "VND" || !model.MBCreditCardDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(MBInternalTransfer))
            {
                MBInternalTransfer model = T as MBInternalTransfer;
                if (model.CurrencyID == "VND" || !model.MBInternalTransferDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;
            }
            else if (T.GetType() == typeof(GOtherVoucher))
            {
                GOtherVoucher model = T as GOtherVoucher;
                if (model.CurrencyID == "VND" || !model.GOtherVoucherDetails.Any(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122"))) return;
                toDate = new DateTime(model.PostedDate.Year, model.PostedDate.Month, model.PostedDate.Day, model.PostedDate.Hour, model.PostedDate.Minute, model.PostedDate.Second);
                fromdate = new DateTime(model.PostedDate.Year, 1, 1);
                Currency = model.CurrencyID;
                ID = model.ID;

            }
            #endregion
            #region Tính tỷ giá
            if (toDate == null || fromdate == null || Currency == null) return;

            List<GeneralLedger> listGeneralLedgersAll = Query.Where(c => (c.Account.StartsWith("1112") || c.Account.StartsWith("1122")) && c.PostedDate < toDate).ToList();

            List<GeneralLedger> listGeneralLedgersIn =
                listGeneralLedgersAll.Where(c => (c.Account.StartsWith("1112") || c.Account.StartsWith("1122"))
                && c.PostedDate >= fromdate && c.DebitAmount != 0 && c.DebitAmountOriginal != 0 && c.CurrencyID == Currency).ToList();
            List<GeneralLedger> listGeneralLedgersOut =
                listGeneralLedgersAll.Where(c => (c.Account.StartsWith("1112") || c.Account.StartsWith("1122"))
                && c.PostedDate >= fromdate && c.CreditAmount != 0 && c.CreditAmountOriginal != 0 && c.CurrencyID == Currency).ToList().OrderByDescending(c => c.RefDate).ToList();

            decimal updatingExchangeRate = 0;

            decimal amountDK = listGeneralLedgersAll.FirstOrDefault(x => x.No == "OPN" && x.CurrencyID == Currency && x.PostedDate == opnDate) == null ? 0
                : listGeneralLedgersAll.FirstOrDefault(x => x.No == "OPN" && x.CurrencyID == Currency && x.PostedDate == opnDate).DebitAmount;
            decimal amountoriginalDK = listGeneralLedgersAll.FirstOrDefault(x => x.No == "OPN" && x.CurrencyID == Currency && x.PostedDate == opnDate) == null ? 0
                : listGeneralLedgersAll.FirstOrDefault(x => x.No == "OPN" && x.CurrencyID == Currency && x.PostedDate == opnDate).DebitAmountOriginal;

            decimal TonXuat = listGeneralLedgersOut.Sum(c => c.CreditAmount);
            decimal TonXuatOriginal = listGeneralLedgersOut.Sum(c => c.CreditAmountOriginal);
            decimal TonNhap = 0;
            decimal TonNhapOriginal = 0;
            decimal Nhap = 0;
            decimal NhapOriginal = 0;
            if (listGeneralLedgersOut.Count != 0)
            {
                decimal oldExchangeRate = 0;
                TonNhap = listGeneralLedgersIn.Where(c => c.PostedDate < listGeneralLedgersOut[0].PostedDate).Sum(c => c.DebitAmount);
                TonNhapOriginal = listGeneralLedgersIn.Where(c => c.PostedDate < listGeneralLedgersOut[0].PostedDate).Sum(c => c.DebitAmountOriginal);
                Nhap = listGeneralLedgersIn.Where(c => c.PostedDate > listGeneralLedgersOut[0].PostedDate && c.PostedDate < toDate).Sum(c => c.DebitAmount);
                NhapOriginal = listGeneralLedgersIn.Where(c => c.PostedDate > listGeneralLedgersOut[0].PostedDate && c.PostedDate < toDate).Sum(c => c.DebitAmountOriginal);
                #region Lấy tỷ giá trước đó
                if (new int[] { 117, 127, 131, 141, 171, 260, 261, 262, 263, 264 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IPPInvoiceDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 116, 126, 133, 143, 173 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IPPServiceDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 119, 129, 132, 142, 172 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IFAIncrementDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 902, 903, 904, 905, 906 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = ITIIncrementDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 330, 340 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = ISAReturnDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 110 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IMCPaymentDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 120, 130, 140 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IMBTellerPaperDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 170 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IMBCreditCardDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 150 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IMBInternalTransferDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                if (new int[] { 600 }.Any(a => a == listGeneralLedgersOut[0].TypeID))
                {
                    var model = IGOtherVoucherDetailService.Getbykey(listGeneralLedgersOut[0].DetailID ?? Guid.Empty);
                    if (model != null) oldExchangeRate = model.CashOutExchangeRate;
                }
                #endregion
                oldExchangeRate = oldExchangeRate == 0 ? (_ICurrencyService.Query.FirstOrDefault(x => x.ID == Currency).ExchangeRate ?? 1) : oldExchangeRate;
                if (Nhap == 0) updatingExchangeRate = oldExchangeRate;
                else updatingExchangeRate = ((((amountoriginalDK + TonNhapOriginal - TonXuatOriginal) * oldExchangeRate) + Nhap) / (amountoriginalDK + TonNhapOriginal - TonXuatOriginal + NhapOriginal));
            }
            else
            {
                TonNhap = listGeneralLedgersIn.Sum(c => c.DebitAmount);
                TonNhapOriginal = listGeneralLedgersIn.Sum(c => c.DebitAmountOriginal);
                updatingExchangeRate = ((amountDK + TonNhap) / (amountoriginalDK + TonNhapOriginal));
            }
            #endregion
            #region Update dữ liệu
            BeginTran();
            try
            {
                List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

                if (T.GetType() == typeof(PPInvoice))
                {
                    PPInvoice model = T as PPInvoice;
                    foreach (var x in model.PPInvoiceDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        x.CashOutVATAmount = (x.VATAmountOriginal * updatingExchangeRate);
                        x.CashOutDifferVATAmount = Math.Abs(x.VATAmount - x.CashOutVATAmount);
                        IPPInvoiceDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(PPService))
                {
                    PPService model = T as PPService;
                    foreach (var x in model.PPServiceDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal ?? 0 * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount ?? 0 - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        x.CashOutVATAmount = (x.VATAmountOriginal ?? 0 * updatingExchangeRate);
                        x.CashOutDifferVATAmount = Math.Abs(x.VATAmount ?? 0 - x.CashOutVATAmount);
                        IPPServiceDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(FAIncrement))
                {
                    FAIncrement model = T as FAIncrement;
                    foreach (var x in model.FAIncrementDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        x.CashOutVATAmount = (x.VATAmountOriginal * updatingExchangeRate);
                        x.CashOutDifferVATAmount = Math.Abs(x.VATAmount - x.CashOutVATAmount);
                        IFAIncrementDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(TIIncrement))
                {
                    TIIncrement model = T as TIIncrement;
                    foreach (var x in model.TIIncrementDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        x.CashOutVATAmount = (x.VATAmountOriginal * updatingExchangeRate);
                        x.CashOutDifferVATAmount = Math.Abs(x.VATAmount - x.CashOutVATAmount);
                        ITIIncrementDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(SAReturn))
                {
                    SAReturn model = T as SAReturn;
                    foreach (var x in model.SAReturnDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        x.CashOutVATAmount = (x.VATAmountOriginal * updatingExchangeRate);
                        x.CashOutDifferVATAmount = Math.Abs(x.VATAmount - x.CashOutVATAmount);
                        ISAReturnDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(MCPayment))
                {
                    MCPayment model = T as MCPayment;
                    foreach (var x in model.MCPaymentDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        IMCPaymentDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(MBTellerPaper))
                {
                    MBTellerPaper model = T as MBTellerPaper;
                    foreach (var x in model.MBTellerPaperDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        IMBTellerPaperDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(MBCreditCard))
                {
                    MBCreditCard model = T as MBCreditCard;
                    foreach (var x in model.MBCreditCardDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        IMBCreditCardDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(MBInternalTransfer))
                {
                    MBInternalTransfer model = T as MBInternalTransfer;
                    foreach (var x in model.MBInternalTransferDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        IMBInternalTransferDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                else if (T.GetType() == typeof(GOtherVoucher))
                {
                    GOtherVoucher model = T as GOtherVoucher;
                    foreach (var x in model.GOtherVoucherDetails.Where(x => x.CreditAccount.StartsWith("1112") || x.CreditAccount.StartsWith("1122")))
                    {
                        x.CashOutExchangeRate = updatingExchangeRate;
                        x.CashOutAmount = (x.AmountOriginal * updatingExchangeRate);
                        x.CashOutDifferAmount = Math.Abs(x.Amount - x.CashOutAmount);
                        x.CashOutDifferAccount = (x.Amount > x.CashOutAmount) ? accountTkcLechLai : accountTkcLechLo;
                        IGOtherVoucherDetailService.Update(x);
                        listGenTemp.AddRange(GenGeneralLedgers(model, x, updatingExchangeRate, (x.Amount > x.CashOutAmount), accountTkcLechLai, accountTkcLechLo));
                    }
                }
                foreach (var gl in listGenTemp)
                {
                    CreateNew(gl);
                }
                CommitTran();
            }
            catch (Exception ex)
            {
                RolbackTran();
            }
            #endregion
        }
        #endregion

        #region Sinh chênh lệch tỷ giá xuất quỹ pp tức thời
        protected List<GeneralLedger> GenGeneralLedgers(PPInvoice model, PPInvoiceDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.ContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription

            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.ContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            if ((detail.DeductionDebitAccount.StartsWith("1112") || detail.DeductionDebitAccount.StartsWith("1122")) && detail.VATAmount > 0)
            {
                #region Cặp Nợ-Có            
                GeneralLedger genTempVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    Account = lai ? detail.CreditAccount : acclo,
                    AccountCorresponding = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.ContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription

                };
                listGenTemp.Add(genTempVAT);
                GeneralLedger genTempCorrespondingVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    AccountCorresponding = lai ? detail.CreditAccount : acclo,
                    Account = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.ContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription
                };
                listGenTemp.Add(genTempCorrespondingVAT);

                #endregion
            }
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(PPService model, PPServiceDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs(((detail.Amount ?? 0) - (detail.DiscountAmount ?? 0)) - (((detail.AmountOriginal ?? 0) - (detail.DiscountAmountOriginal ?? 0)) * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.ContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription

            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs(((detail.Amount ?? 0) - (detail.DiscountAmount ?? 0)) - (((detail.AmountOriginal ?? 0) - (detail.DiscountAmountOriginal ?? 0)) * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.ContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            if (detail.VATAmount > 0)
            {
                #region Cặp Nợ-Có            
                GeneralLedger genTempVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    Account = lai ? detail.CreditAccount : acclo,
                    AccountCorresponding = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = Math.Abs(((detail.VATAmount ?? 0) - ((detail.VATAmountOriginal ?? 0) * exchange))),
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.ContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription

                };
                listGenTemp.Add(genTempVAT);
                GeneralLedger genTempCorrespondingVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    AccountCorresponding = lai ? detail.CreditAccount : acclo,
                    Account = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = Math.Abs(((detail.VATAmount ?? 0) - ((detail.VATAmountOriginal ?? 0) * exchange))),
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.ContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription
                };
                listGenTemp.Add(genTempCorrespondingVAT);

                #endregion
            }
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(FAIncrement model, FAIncrementDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.MContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.MContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            if ((detail.DeductionDebitAccount.StartsWith("1112") || detail.DeductionDebitAccount.StartsWith("1122")) && detail.VATAmount > 0)
            {
                #region Cặp Nợ-Có            
                GeneralLedger genTempVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    Account = lai ? detail.CreditAccount : acclo,
                    AccountCorresponding = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.MContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription

                };
                listGenTemp.Add(genTempVAT);
                GeneralLedger genTempCorrespondingVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    AccountCorresponding = lai ? detail.CreditAccount : acclo,
                    Account = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.MContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription
                };
                listGenTemp.Add(genTempCorrespondingVAT);

                #endregion
            }
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(TIIncrement model, TIIncrementDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.MContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription

            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = detail.InvoiceDate,
                InvoiceNo = detail.InvoiceNo,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = detail.InvoiceSeries,
                ContactName = model.MContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            if ((detail.DeductionDebitAccount.StartsWith("1112") || detail.DeductionDebitAccount.StartsWith("1122")) && detail.VATAmount > 0)
            {
                #region Cặp Nợ-Có            
                GeneralLedger genTempVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    Account = lai ? detail.CreditAccount : acclo,
                    AccountCorresponding = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.MContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription

                };
                listGenTemp.Add(genTempVAT);
                GeneralLedger genTempCorrespondingVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = detail.InvoiceDate,
                    InvoiceNo = detail.InvoiceNo,
                    AccountCorresponding = lai ? detail.CreditAccount : acclo,
                    Account = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = model.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = detail.InvoiceSeries,
                    ContactName = model.MContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    IsIrrationalCost = detail.IsIrrationalCost,
                    VATDescription = detail.VATDescription
                };
                listGenTemp.Add(genTempCorrespondingVAT);

                #endregion
            }
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(SAReturn model, SAReturnDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = model.InvoiceDate,
                InvoiceNo = model.InvoiceNo,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = model.InvoiceSeries,
                ContactName = model.ContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                InvoiceDate = model.InvoiceDate,
                InvoiceNo = model.InvoiceNo,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs((detail.Amount - detail.DiscountAmount) - ((detail.AmountOriginal - detail.DiscountAmountOriginal) * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                InvoiceSeries = model.InvoiceSeries,
                ContactName = model.ContactName,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            if (detail.VATAmount > 0)
            {
                #region Cặp Nợ-Có            
                GeneralLedger genTempVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = model.InvoiceDate,
                    InvoiceNo = model.InvoiceNo,
                    Account = lai ? detail.CreditAccount : acclo,
                    AccountCorresponding = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = detail.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    DebitAmountOriginal = 0,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = model.InvoiceSeries,
                    ContactName = model.ContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    VATDescription = detail.VATDescription

                };
                listGenTemp.Add(genTempVAT);
                GeneralLedger genTempCorrespondingVAT = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = model.ID,
                    TypeID = model.TypeID,
                    Date = model.Date,
                    PostedDate = model.PostedDate,
                    No = model.No,
                    InvoiceDate = model.InvoiceDate,
                    InvoiceNo = model.InvoiceNo,
                    AccountCorresponding = lai ? detail.CreditAccount : acclo,
                    Account = lai ? acclai : detail.CreditAccount,
                    BankAccountDetailID = detail.BankAccountDetailID,
                    CurrencyID = "VND",
                    ExchangeRate = 1,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = Math.Abs(detail.VATAmount - (detail.VATAmountOriginal * exchange)),
                    CreditAmountOriginal = 0,
                    Reason = model.Reason,
                    Description = detail.Description,
                    AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                    EmployeeID = model.EmployeeID,
                    BudgetItemID = detail.BudgetItemID,
                    CostSetID = detail.CostSetID,
                    ContractID = detail.ContractID,
                    StatisticsCodeID = detail.StatisticsCodeID,
                    InvoiceSeries = model.InvoiceSeries,
                    ContactName = model.ContactName,
                    DetailID = detail.ID,
                    RefNo = model.No,
                    RefDate = model.Date,
                    DepartmentID = detail.DepartmentID,
                    ExpenseItemID = detail.ExpenseItemID,
                    VATDescription = detail.VATDescription
                };
                listGenTemp.Add(genTempCorrespondingVAT);

                #endregion
            }
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(MCPayment model, MCPaymentDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost
            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(MBTellerPaper model, MBTellerPaperDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost
            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = model.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(MBCreditCard model, MBCreditCardDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost
            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID ?? model.AccountingObjectID,
                EmployeeID = model.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(MBInternalTransfer model, MBInternalTransferDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.ToBankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID,
                EmployeeID = detail.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription

            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.FromBankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                AccountingObjectID = detail.AccountingObjectID,
                EmployeeID = detail.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
                VATDescription = detail.VATDescription
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(GOtherVoucher model, GOtherVoucherDetail detail, decimal exchange, bool lai, string acclai, string acclo)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            #region Cặp Nợ-Có            
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                Account = lai ? detail.CreditAccount : acclo,
                AccountCorresponding = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                DebitAmountOriginal = 0,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                EmployeeID = detail.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,

            };
            listGenTemp.Add(genTemp);
            GeneralLedger genTempCorresponding = new GeneralLedger
            {
                ID = Guid.NewGuid(),
                BranchID = null,
                ReferenceID = model.ID,
                TypeID = model.TypeID,
                Date = model.Date,
                PostedDate = model.PostedDate,
                No = model.No,
                AccountCorresponding = lai ? detail.CreditAccount : acclo,
                Account = lai ? acclai : detail.CreditAccount,
                BankAccountDetailID = detail.BankAccountDetailID,
                CurrencyID = "VND",
                ExchangeRate = 1,
                DebitAmount = 0,
                DebitAmountOriginal = 0,
                CreditAmount = Math.Abs(detail.Amount - (detail.AmountOriginal * exchange)),
                CreditAmountOriginal = 0,
                Reason = model.Reason,
                Description = detail.Description,
                EmployeeID = detail.EmployeeID,
                BudgetItemID = detail.BudgetItemID,
                CostSetID = detail.CostSetID,
                ContractID = detail.ContractID,
                StatisticsCodeID = detail.StatisticsCodeID,
                DetailID = detail.ID,
                RefNo = model.No,
                RefDate = model.Date,
                DepartmentID = detail.DepartmentID,
                ExpenseItemID = detail.ExpenseItemID,
                IsIrrationalCost = detail.IsIrrationalCost,
            };
            listGenTemp.Add(genTempCorresponding);

            #endregion
            return listGenTemp;
        }
        #endregion

        #region Đánh giá lại tài khoản ngoại tệ
        public List<GOtherVoucherDetailForeignCurrency> GetGOtherVoucherDetailForeignCurrency(Currency cu, DateTime dt, decimal newEx)
        {
            var newdt = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            List<GOtherVoucherDetailForeignCurrency> lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(x => x.DateReevaluate < newdt && _IGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
            List<GeneralLedger> lstgl = Query.Where(c => c.CurrencyID == cu.ID && c.CurrencyID == cu.ID && c.PostedDate <= newdt && (c.Account.StartsWith("1112") || c.Account.StartsWith("1122") || c.Account.StartsWith("138") || c.Account.StartsWith("141")
            || c.Account.StartsWith("334") || c.Account.StartsWith("336") || c.Account.StartsWith("338") || c.Account.StartsWith("341") || c.Account.StartsWith("331") || c.Account.StartsWith("131"))).ToList();

            List<GeneralLedger> lstgl1 = lstgl.Where(c => c.Account.StartsWith("1112") || c.Account.StartsWith("1122")).ToList();
            List<GeneralLedger> lstgl2 = lstgl.Where(c => (c.Account.StartsWith("138") || c.Account.StartsWith("141") || c.Account.StartsWith("334") || c.Account.StartsWith("336")
                        || c.Account.StartsWith("338") || c.Account.StartsWith("341") || c.Account.StartsWith("331") || c.Account.StartsWith("131"))).ToList();
            var lst1 = (from a in lstgl1
                        group a by new { a.Account, a.BankAccountDetailID }
                       into t
                        select new GOtherVoucherDetailForeignCurrency
                        {
                            ID = Guid.NewGuid(),
                            AccountNumber = t.Key.Account,
                            BankAccountDetailID = t.Key.BankAccountDetailID,
                            DebitAmount = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount)) : 0,
                            DebitAmountOriginal = (t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal) > 0) ?
                      (t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal)) : 0,
                            DebitReevaluate = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? ((t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal)) * newEx) : 0,
                            DebitAmountDiffer = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ?
                      (((t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal)) * newEx) - (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount))) : 0,

                            CreditAmount = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? 0 : (t.Sum(u => u.CreditAmount) - t.Sum(u => u.DebitAmount)),
                            CreditAmountOriginal = (t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal) > 0) ?
                      0 : (t.Sum(u => u.CreditAmountOriginal) - t.Sum(u => u.DebitAmountOriginal)),
                            CreditReevaluate = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? 0 : ((t.Sum(u => u.CreditAmountOriginal) - t.Sum(u => u.DebitAmountOriginal)) * newEx),
                            CreditAmountDiffer = (t.Sum(u => u.CreditAmount) - t.Sum(u => u.DebitAmount) > 0) ?
                      (((t.Sum(u => u.CreditAmountOriginal) - t.Sum(u => u.DebitAmountOriginal)) * newEx) - (t.Sum(u => u.CreditAmount) - t.Sum(u => u.DebitAmount))) : 0,
                            CurrencyID = cu.ID,
                            VoucherDetailForeignCurrencys = (from d in t
                                                             group d by new { d.ReferenceID, d.Date, d.No, d.TypeID, d.AccountingObjectID, d.CurrencyID } into c
                                                             select new VoucherDetailForeignCurrency()
                                                             {
                                                                 ReferenceID = c.Key.ReferenceID,
                                                                 Date = c.Key.Date,
                                                                 No = c.Key.No,
                                                                 DebitAmountOriginal = c.Sum(u => u.DebitAmountOriginal),
                                                                 DebitAmount = c.Sum(u => u.DebitAmount),
                                                                 CreditAmountOriginal = c.Sum(u => u.CreditAmountOriginal),
                                                                 CreditAmount = c.Sum(u => u.CreditAmount),
                                                                 NewExchangeRate = newEx
                                                             }).ToList()
                        }).ToList();
            foreach (var x in lst1)
            {
                x.ID = Guid.NewGuid();
                foreach (var v in x.VoucherDetailForeignCurrencys) v.GOtherVoucherDetailForeignCurrencyID = x.ID;
                if (lstgo.Any(d => d.AccountNumber == x.AccountNumber && d.BankAccountDetailID == x.BankAccountDetailID))
                {
                    var md = lstgo.Where(d => d.AccountNumber == x.AccountNumber && d.BankAccountDetailID == x.BankAccountDetailID).OrderByDescending(d => d.DateReevaluate).ToList()[0];
                    if (x.DebitAmount > 0)
                    {
                        x.DebitAmount = (md.DebitReevaluate + x.VoucherDetailForeignCurrencys.Where(d => !md.VoucherDetailForeignCurrencys.Any(t => t.ReferenceID == d.ReferenceID)).ToList().Sum(t => t.DebitAmount - t.CreditAmount));
                        x.DebitAmountDiffer = x.DebitReevaluate - x.DebitAmount;
                    }
                    else if (x.CreditAmount > 0)
                    {
                        x.CreditAmount = (md.CreditReevaluate + x.VoucherDetailForeignCurrencys.Where(d => !md.VoucherDetailForeignCurrencys.Any(t => t.ReferenceID == d.ReferenceID)).ToList().Sum(t => t.CreditAmount - t.DebitAmount));
                        x.CreditAmountDiffer = x.CreditReevaluate - x.CreditAmount;
                    }

                }
            }
            var lst2 = (from a in lstgl2
                        group a by new { a.Account, a.AccountingObjectID }
                       into t
                        select new GOtherVoucherDetailForeignCurrency
                        {
                            AccountNumber = t.Key.Account,
                            AccountingObjectID = t.Key.AccountingObjectID,
                            DebitAmount = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount)) : 0,
                            DebitAmountOriginal = (t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal) > 0) ?
                      (t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal)) : 0,
                            DebitReevaluate = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? ((t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal)) * newEx) : 0,
                            DebitAmountDiffer = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ?
                      (((t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal)) * newEx) - (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount))) : 0,

                            CreditAmount = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? 0 : (t.Sum(u => u.CreditAmount) - t.Sum(u => u.DebitAmount)),
                            CreditAmountOriginal = (t.Sum(u => u.DebitAmountOriginal) - t.Sum(u => u.CreditAmountOriginal) > 0) ?
                      0 : (t.Sum(u => u.CreditAmountOriginal) - t.Sum(u => u.DebitAmountOriginal)),
                            CreditReevaluate = (t.Sum(u => u.DebitAmount) - t.Sum(u => u.CreditAmount) > 0) ? 0 : ((t.Sum(u => u.CreditAmountOriginal) - t.Sum(u => u.DebitAmountOriginal)) * newEx),
                            CreditAmountDiffer = (t.Sum(u => u.CreditAmount) - t.Sum(u => u.DebitAmount) > 0) ?
                      (((t.Sum(u => u.CreditAmountOriginal) - t.Sum(u => u.DebitAmountOriginal)) * newEx) - (t.Sum(u => u.CreditAmount) - t.Sum(u => u.DebitAmount))) : 0,
                            CurrencyID = cu.ID,
                            VoucherDetailForeignCurrencys = (from d in t
                                                             group d by new { d.ReferenceID, d.Date, d.No, d.TypeID, d.AccountingObjectID, d.CurrencyID } into c
                                                             select new VoucherDetailForeignCurrency()
                                                             {
                                                                 ReferenceID = c.Key.ReferenceID,
                                                                 Date = c.Key.Date,
                                                                 No = c.Key.No,
                                                                 DebitAmountOriginal = c.Sum(u => u.DebitAmountOriginal),
                                                                 DebitAmount = c.Sum(u => u.DebitAmount),
                                                                 CreditAmountOriginal = c.Sum(u => u.CreditAmountOriginal),
                                                                 CreditAmount = c.Sum(u => u.CreditAmount),
                                                                 NewExchangeRate = newEx
                                                             }).ToList()
                        }).ToList();
            foreach (var x in lst2)
            {
                x.ID = Guid.NewGuid();
                foreach (var v in x.VoucherDetailForeignCurrencys) v.GOtherVoucherDetailForeignCurrencyID = x.ID;
                if (lstgo.Any(d => d.AccountNumber == x.AccountNumber && d.AccountingObjectID == x.AccountingObjectID))
                {
                    var md = lstgo.Where(d => d.AccountNumber == x.AccountNumber && d.AccountingObjectID == x.AccountingObjectID).OrderByDescending(d => d.DateReevaluate).ToList()[0];
                    if (x.DebitAmount > 0)
                    {
                        x.DebitAmount = (md.DebitReevaluate + x.VoucherDetailForeignCurrencys.Where(d => !md.VoucherDetailForeignCurrencys.Any(t => t.ReferenceID == d.ReferenceID)).ToList().Sum(t => t.DebitAmount - t.CreditAmount));
                        x.DebitAmountDiffer = x.DebitReevaluate - x.DebitAmount;
                    }
                    else if (x.CreditAmount > 0)
                    {
                        x.CreditAmount = (md.CreditReevaluate + x.VoucherDetailForeignCurrencys.Where(d => !md.VoucherDetailForeignCurrencys.Any(t => t.ReferenceID == d.ReferenceID)).ToList().Sum(t => t.CreditAmount - t.DebitAmount));
                        x.CreditAmountDiffer = x.CreditReevaluate - x.CreditAmount;
                    }

                }
            }
            lst1.AddRange(lst2);
            return lst1.Where(x => x.CreditAmountOriginal != 0 || x.DebitAmountOriginal != 0).OrderBy(c => c.AccountNumber).ToList();
        }
        public List<GOtherVoucherDetailDebtPayment> GetGOtherVoucherDetailDebtPayment(Currency cu, DateTime dt, decimal newEx)
        {

            var newdt = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            List<MBDepositDetailCustomer> lstMbDepositDetailCustomers = IMBDepositDetailCustomerService.Query.Where(c => _IMBDepositService.Query.Any(d => d.CurrencyID == cu.ID && d.PostedDate < newdt)).ToList();
            List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = IMCReceiptDetailCustomerService.Query.Where(c => _IMCReceiptService.Query.Any(d => d.CurrencyID == cu.ID && d.PostedDate < newdt)).ToList();
            List<MBTellerPaperDetailVendor> lstMbTellerPaper = IMBTellerPaperDetailVendorService.Query.Where(c => _IMBTellerPaperService.Query.Any(d => d.CurrencyID == cu.ID && d.PostedDate < newdt)).ToList();
            List<MBCreditCardDetailVendor> lstMbCreditCard = IMBCreditCardDetailVendorService.Query.Where(c => _IMBCreditCardService.Query.Any(d => d.CurrencyID == cu.ID && d.PostedDate < newdt)).ToList();
            List<MCPaymentDetailVendor> lstMcPayment = IMCPaymentDetailVendorService.Query.Where(c => _IMCPaymentService.Query.Any(d => d.CurrencyID == cu.ID && d.PostedDate < newdt)).ToList();
            List<ExceptVoucher> lstexcept = _IExceptVoucherService.Query.Where(x => x.PostedDate < newdt).ToList();
            List<int> lstTypeID = new List<int>() { 101, 118, 161 };
            List<GOtherVoucherDetailDebtPayment> lstgo = _IGOtherVoucherDetailDebtPaymentService.Query.Where(x => x.Date < newdt && x.CurrencyID == cu.ID && _IGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
            List<GeneralLedger> lstgl = Query.Where(c => c.CurrencyID == cu.ID && c.PostedDate <= newdt && (c.Account.StartsWith("331") || c.Account.StartsWith("131")) && c.TypeID != 101 && c.TypeID != 118 && c.TypeID != 161
            && c.TypeID != 128 && c.TypeID != 134 && c.TypeID != 144 && c.TypeID != 174).ToList();
            var lst = (from a in lstgl
                       group a by new { a.Account, a.AccountingObjectID, a.No, a.PostedDate, a.Reason, a.ExchangeRate, a.ReferenceID, a.TypeID }
                       into t
                       select new GOtherVoucherDetailDebtPayment
                       {
                           TypeID = t.Key.TypeID,
                           RefID = t.Key.ReferenceID,
                           AccountNumber = t.Key.Account,
                           AccountingObjectID = t.Key.AccountingObjectID,
                           RefVoucherNo = t.Key.No,
                           RefVoucherDate = t.Key.PostedDate,
                           Reason = t.Key.Reason,
                           RefVoucherExchangeRate = t.Key.ExchangeRate,
                           CurrencyID = cu.ID,
                           LastExchangeRate = lstgo.Any(x => x.RefID == t.Key.ReferenceID) ?
                           lstgo.Where(x => x.RefID == t.Key.ReferenceID).OrderByDescending(x => x.Date).ToList()[0].NewExchangeRate : 0,
                           Date = dt,
                           RemainingAmountOriginal = (Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)),

                           RemainingAmount = (lstgo.Any(x => x.RefID == t.Key.ReferenceID) ?
                           ((Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal))
                           * lstgo.Where(x => x.RefID == t.Key.ReferenceID).OrderByDescending(x => x.Date).ToList()[0].NewExchangeRate)
                           : ((Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.Amount)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.Amount)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.Amount ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)) * t.Key.ExchangeRate ?? 1)),

                           RevaluedAmount = ((Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)) * newEx),

                           DifferAmount = (((Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)) * newEx) -
                           ((lstgo.Any(x => x.RefID == t.Key.ReferenceID) ?
                           ((Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal))
                           * lstgo.Where(x => x.RefID == t.Key.ReferenceID).OrderByDescending(x => x.Date).ToList()[0].NewExchangeRate)
                           : ((Math.Abs(t.Sum(x => x.DebitAmountOriginal) - t.Sum(x => x.CreditAmountOriginal))
                           - lstMbDepositDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMcReceiptDetailCustomers.Where(x => x.SaleInvoiceID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstMbCreditCard.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMbTellerPaper.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal)
                           - lstMcPayment.Where(b => b.PPInvoiceID == t.Key.ReferenceID).ToList().Sum(f => f.AmountOriginal ?? 0)
                           - lstexcept.Where(x => x.GLVoucherID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)
                           - lstexcept.Where(x => x.GLVoucherExceptID == t.Key.ReferenceID).Sum(x => x.AmountOriginal)) * t.Key.ExchangeRate ?? 1))))
                       }).ToList();
            return lst.Where(x => x.RemainingAmountOriginal != 0).ToList().OrderBy(c => c.AccountNumber).ThenByDescending(x => x.RefVoucherDate).ToList();
        }
        #endregion

        #region Hợp đồng
        //Thực chi, mua hàng
        public List<EMContractBuy> GetEmContractBuys(Guid contractID, List<int> typeID)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.Where(l => l.ContractID == contractID).ToList();//edit by cuongpv
            return (from g in lstGeneralLedgers
                    where typeID.Any(x => x == g.TypeID)
                    group g by new { g.ContractID, g.DetailID, g.TypeID, g.No, g.Description, g.Date, g.InvoiceDate, g.InvoiceNo, g.AccountingObjectID, g.CurrencyID }
                        into t
                    select new EMContractBuy()
                    {
                        TypeName = _ITypeService.Getbykey(t.Key.TypeID).TypeName,
                        Date = t.Key.Date,
                        No = t.Key.No,
                        Reason = t.Key.Description,
                        InvoiceDate = t.Key.InvoiceDate,
                        InvoiceNo = t.Key.InvoiceNo,
                        Amount = t.Sum(u => u.CreditAmount),
                        GLID = t.Key.DetailID ?? Guid.Empty
                    }).ToList().OrderByDescending(x => x.Date).ToList();
        }
        //Th?c thu, bán hàng
        public List<EMContractSale> GetEMContractSaleDetailByContractID(Guid contractID, List<int> typeID)  //bán hàng
        {
            List<EMContractSale> lst = new List<EMContractSale>();
            List<int> lsttypethu = new List<int> { 100, 101 };
            List<GeneralLedger> lstGeneralLedgers = Query.Where(l => l.ContractID == contractID).ToList();//edit by cuongpv
            List<EMContractSale> lstthu = (from g in lstGeneralLedgers
                                           where lsttypethu.Any(x => x == g.TypeID)
                                           group g by new { g.ContractID, g.DetailID, g.TypeID, g.No, g.Description, g.Date, g.InvoiceDate, g.InvoiceNo, g.AccountingObjectID, g.CurrencyID }
                        into t
                                           select new EMContractSale()
                                           {
                                               TypeName = _ITypeService.Getbykey(t.Key.TypeID).TypeName,
                                               Date = t.Key.Date,
                                               No = t.Key.No,
                                               Reason = t.Key.Description,
                                               InvoiceDate = t.Key.InvoiceDate,
                                               InvoiceNo = t.Key.InvoiceNo,
                                               Amount = t.Sum(u => u.DebitAmount),
                                               GLID = t.Key.DetailID ?? Guid.Empty

                                           }).ToList();
            lst.AddRange(lstthu);
            List<SAInvoice> lstSAInvoice = _ISAInvoiceService.Query.Where(x => x.Recorded && x.TypeID != 320).ToList();
            //comment by cuongpv

            //List<EMContractSale> lstbh = (from g in lstSAInvoice
            //                              from b in g.SAInvoiceDetails//from b in g.SAInvoiceDetails.Where(x => x.ContractID != null).ToList().Where(c => c.ContractID == contractID).ToList()
            //                              where b.ContractID == contractID
            //                              select new EMContractSale()
            //                              {
            //                                  TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
            //                                  Date = g.Date,
            //                                  No = g.MNo,
            //                                  Reason = b.Description,
            //                                  InvoiceDate = g.InvoiceDate,
            //                                  InvoiceNo = g.InvoiceNo,
            //                                  Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
            //                                  GLID = b.ID
            //                              }).ToList();

            //add by cuongpv
            List<SAInvoiceDetail> lstSaInvoiceDetail = _ISAInvoiceDetailService.Query.Where(x => x.ContractID == contractID).ToList();
            List<EMContractSale> lstbh = (from b in lstSaInvoiceDetail
                                          join
             g in lstSAInvoice on b.SAInvoiceID equals g.ID
                                          select new EMContractSale()
                                          {
                                              TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
                                              Date = g.Date,
                                              No = g.MNo,
                                              Reason = b.Description,
                                              InvoiceDate = g.InvoiceDate,
                                              InvoiceNo = g.InvoiceNo,
                                              Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
                                              GLID = b.ID
                                          }).ToList();
            //end add by cuongpv
            lst.AddRange(lstbh);
            return lst.OrderByDescending(x => x.Date).ToList();
        }
        //Hóa don bán hàng
        public List<EMContractSale> GetEMContractHoaDonByContractID(Guid contractID, List<int> typeID)  //bán hàng
        {
            List<EMContractSale> lst = new List<EMContractSale>();
            List<SAInvoice> lstSAInvoice = _ISAInvoiceService.Query.Where(x => x.Recorded && x.InvoiceNo != null).ToList();
            //comment by cuongpv

            //List<EMContractSale> lst1 = (from g in lstSAInvoice
            //                             from b in g.SAInvoiceDetails.Where(x => x.ContractID != null).ToList().Where(c => c.ContractID == contractID).ToList()
            //                             select new EMContractSale()
            //                             {
            //                                 TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
            //                                 Date = g.Date,
            //                                 No = g.No,
            //                                 Reason = b.Description,
            //                                 InvoiceDate = g.InvoiceDate,
            //                                 InvoiceNo = g.InvoiceNo,
            //                                 Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
            //                                 GLID = b.ID
            //                             }).ToList();

            //add by cuongpv
            List<SAInvoiceDetail> lstSAInvoiceDetail = _ISAInvoiceDetailService.Query.Where(x => x.ContractID == contractID).ToList();
            List<EMContractSale> lst1 = (from b in lstSAInvoiceDetail
                                         join g in lstSAInvoice on b.SAInvoiceID equals g.ID
                                         select new EMContractSale()
                                         {
                                             TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
                                             Date = g.Date,
                                             No = g.No,
                                             Reason = b.Description,
                                             InvoiceDate = g.InvoiceDate,
                                             InvoiceNo = g.InvoiceNo,
                                             Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
                                             GLID = b.ID
                                         }).ToList();
            //end add by cuongpv
            lst.AddRange(lst1);
            List<SAReturn> lstSAReturn = _ISAReturnService.Query.Where(x => x.Recorded && x.InvoiceNo != null).ToList();
            //comment by cuongpv

            //List<EMContractSale> lst2 = (from g in lstSAReturn
            //                             from b in g.SAReturnDetails.Where(x => x.ContractID != null).ToList().Where(c => c.ContractID == contractID).ToList()
            //                             select new EMContractSale()
            //                             {
            //                                 TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
            //                                 Date = g.Date,
            //                                 No = g.No,
            //                                 Reason = b.Description,
            //                                 InvoiceDate = g.InvoiceDate,
            //                                 InvoiceNo = g.InvoiceNo,
            //                                 Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
            //                                 GLID = b.ID
            //                             }).ToList();

            //add by cuongpv
            List<SAReturnDetail> lstSAReturnDetail = _ISAReturnDetailService.Query.Where(x => x.ContractID == contractID).ToList();
            List<EMContractSale> lst2 = (from b in lstSAReturnDetail
                                         join g in lstSAReturn on b.SAReturnID equals g.ID
                                         select new EMContractSale()
                                         {
                                             TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
                                             Date = g.Date,
                                             No = g.No,
                                             Reason = b.Description,
                                             InvoiceDate = g.InvoiceDate,
                                             InvoiceNo = g.InvoiceNo,
                                             Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
                                             GLID = b.ID
                                         }).ToList();
            //end add by cuongpv
            lst.AddRange(lst2);
            return lst.OrderByDescending(x => x.Date).ToList();
        }
        public List<EMContractSale> GetEMContractSaleThucChiDetailByContractID(Guid contractID, List<int> typeID)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.Where(l => l.ContractID == contractID).ToList();//edit by cuongpv
            return (from g in lstGeneralLedgers
                    where typeID.Any(x => x == g.TypeID)
                    group g by new { g.ContractID, g.DetailID, g.TypeID, g.No, g.Description, g.Date, g.InvoiceDate, g.InvoiceNo, g.AccountingObjectID, g.CurrencyID }
                        into t
                    select new EMContractSale()
                    {
                        TypeName = _ITypeService.Getbykey(t.Key.TypeID).TypeName,
                        Date = t.Key.Date,
                        No = t.Key.No,
                        Reason = t.Key.Description,
                        InvoiceDate = t.Key.InvoiceDate,
                        InvoiceNo = t.Key.InvoiceNo,
                        Amount = t.Sum(u => u.CreditAmount),
                        GLID = t.Key.DetailID ?? Guid.Empty
                    }).ToList().OrderByDescending(x => x.Date).ToList();
        }

        public List<EMContractSale> GetEMContractSaleThucChiDetailByTypeID(List<int> typeID, DateTime fromdate, DateTime todate)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.Where(l => l.Date >= fromdate && l.Date <= todate).ToList();//edit by cuongpv
            return (from g in lstGeneralLedgers
                    where typeID.Any(x => x == g.TypeID)
                    group g by new { g.ContractID, g.DetailID, g.TypeID, g.No, g.Description, g.Date, g.InvoiceDate, g.InvoiceNo, g.AccountingObjectID, g.CurrencyID }
                        into t
                    select new EMContractSale()
                    {
                        TypeName = _ITypeService.Getbykey(t.Key.TypeID).TypeName,
                        Date = t.Key.Date,
                        No = t.Key.No,
                        Reason = t.Key.Description,
                        InvoiceDate = t.Key.InvoiceDate,
                        InvoiceNo = t.Key.InvoiceNo,
                        Amount = t.Sum(u => u.CreditAmount),
                        GLID = t.Key.DetailID ?? Guid.Empty
                    }).ToList().OrderByDescending(x => x.Date).ToList();
        }

        public List<EMContractSale> GetEMContractSaleDetailByTypeID(List<int> typeID, DateTime fromdate, DateTime todate)
        {
            List<EMContractSale> lst = new List<EMContractSale>();
            List<int> lsttypethu = new List<int> { 100, 101 };
            List<GeneralLedger> lstGeneralLedgers = Query.Where(l => l.Date >= fromdate && l.Date <= todate).ToList();//edit by cuongpv
            List<EMContractSale> lstthu = (from g in lstGeneralLedgers
                                           where lsttypethu.Any(x => x == g.TypeID)
                                           group g by new { g.ContractID, g.DetailID, g.TypeID, g.No, g.Description, g.Date, g.InvoiceDate, g.InvoiceNo, g.AccountingObjectID, g.CurrencyID }
                        into t
                                           select new EMContractSale()
                                           {
                                               TypeName = _ITypeService.Getbykey(t.Key.TypeID).TypeName,
                                               Date = t.Key.Date,
                                               No = t.Key.No,
                                               Reason = t.Key.Description,
                                               InvoiceDate = t.Key.InvoiceDate,
                                               InvoiceNo = t.Key.InvoiceNo,
                                               Amount = t.Sum(u => u.DebitAmount),
                                               GLID = t.Key.DetailID ?? Guid.Empty
                                           }).ToList();
            lst.AddRange(lstthu);
            List<SAInvoice> lstSAInvoice = _ISAInvoiceService.Query.Where(x => x.Recorded && x.PostedDate >= fromdate && x.PostedDate <= todate && x.TypeID != 320).ToList();
            List<EMContractSale> lst1 = (from g in lstSAInvoice
                                         from b in g.SAInvoiceDetails
                                         select new EMContractSale()
                                         {
                                             TypeName = _ITypeService.Getbykey(g.TypeID).TypeName,
                                             Date = g.Date,
                                             No = g.MNo,
                                             Reason = b.Description,
                                             InvoiceDate = g.InvoiceDate,
                                             InvoiceNo = g.InvoiceNo,
                                             Amount = (b.Amount - b.DiscountAmount + b.VATAmount),
                                             GLID = b.ID
                                         }).ToList();
            lst.AddRange(lst1);
            return lst.OrderByDescending(x => x.Date).ToList();
        }
        public List<EMContractBuy> GetEmContractBuyInvoices(Guid contractID, List<int> typeID)
        {
            List<GeneralLedger> lstGeneralLedgers = Query.Where(l => l.ContractID == contractID).ToList();//edit by cuongpv
            List<EMContractBuy> lst = (from g in lstGeneralLedgers
                                       where typeID.Any(x => x == g.TypeID)
                                       group g by new { g.ContractID, g.DetailID, g.TypeID, g.No, g.Description, g.Date, g.InvoiceDate, g.InvoiceNo, g.AccountingObjectID, g.CurrencyID }
                        into t
                                       select new EMContractBuy()
                                       {
                                           TypeName = _ITypeService.Getbykey(t.Key.TypeID).TypeName,
                                           Date = t.Key.Date,
                                           No = t.Key.No,
                                           Reason = t.Key.Description,
                                           InvoiceDate = t.Key.InvoiceDate,
                                           InvoiceNo = t.Key.InvoiceNo,
                                           Amount = t.Sum(u => u.CreditAmount),
                                           GLID = t.Key.DetailID ?? Guid.Empty
                                       }).ToList().OrderByDescending(x => x.Date).ToList();
            return lst.Where(x => x.InvoiceNo != null).ToList();
        }
        #endregion
        public List<GeneralLedger> GetGLByDetailID(Guid ID)
        {
            return Query.Where(x => x.DetailID == ID).ToList();
        }

        public FixedAssetLedger GetLastFixedAssetLedger(Guid? fixedAssetId, DateTime postedDate)
        {
            //DateTime pd = Utils.string;
            var lstfal = _IFixedAssetLedgerService.Query.Where(fal => fal.FixedAssetID == fixedAssetId && fal.PostedDate <= postedDate).OrderByDescending(fal => fal.PostedDate).ThenByDescending(fal => fal.OrderPriority).ToList();
            if (lstfal != null && lstfal.Count > 0)
            {
                FixedAssetLedger a = new FixedAssetLedger
                {
                    BranchID = lstfal[0].BranchID,
                    ReferenceID = lstfal[0].ReferenceID,
                    No = lstfal[0].No,
                    TypeID = lstfal[0].TypeID,
                    Date = lstfal[0].Date,
                    PostedDate = lstfal[0].PostedDate,
                    FixedAssetID = lstfal[0].FixedAssetID,
                    DepartmentID = lstfal[0].DepartmentID,
                    UsedTime = lstfal[0].UsedTime,
                    DepreciationRate = lstfal[0].DepreciationRate,
                    DepreciationAmount = lstfal[0].DepreciationAmount,
                    OriginalPriceAccount = lstfal[0].OriginalPriceAccount,
                    OriginalPriceDebitAmount = lstfal[0].OriginalPriceDebitAmount,
                    OriginalPriceCreditAmount = lstfal[0].OriginalPriceCreditAmount,
                    DepreciationAccount = lstfal[0].DepreciationAccount,
                    DepreciationDebitAmount = lstfal[0].DepreciationDebitAmount,
                    DepreciationCreditAmount = lstfal[0].DepreciationCreditAmount,
                    Reason = lstfal[0].Reason,
                    Description = lstfal[0].Description,
                    OrderPriority = lstfal[0].OrderPriority,
                    IsIrrationalCost = lstfal[0].IsIrrationalCost,
                    OriginalPrice = lstfal[0].OriginalPrice,
                    RemainingAmount = lstfal[0].RemainingAmount,
                    UsedTimeRemain = lstfal[0].UsedTimeRemain,
                    MonthPeriodDepreciationAmount = lstfal[0].MonthPeriodDepreciationAmount,
                    MonthDepreciationRate = lstfal[0].MonthDepreciationRate,
                    Nos = lstfal[0].Nos,
                    IncrementDate = lstfal[0].IncrementDate
                };
                for (int i = 0; i < lstfal.Count; i++)
                {

                    if (new int[] { 530, 701, 119, 129, 132, 142, 172, 500, 510 }.Contains(lstfal[i].TypeID))
                    {
                        a.MonthPeriodDepreciationAmount = lstfal[i].MonthPeriodDepreciationAmount;
                        break;
                    }

                }
                return a;
            }
            else
            {
                FixedAsset fix = _IFixedAssetService.Getbykey(fixedAssetId ?? Guid.Empty);
                return new FixedAssetLedger
                {
                    No = fix.No,
                    FixedAssetID = fix.ID,
                    DepartmentID = fix.DepartmentID,
                    UsedTime = fix.UsedTime,
                    DepreciationRate = fix.DepreciationRate,
                    DepreciationAmount = fix.MonthPeriodDepreciationAmount,
                    MonthPeriodDepreciationAmount = fix.MonthPeriodDepreciationAmount,
                    OriginalPriceAccount = fix.OriginalPriceAccount,
                    OriginalPriceCreditAmount = 0,
                    DepreciationAccount = fix.DepreciationAccount,
                    DepreciationCreditAmount = 0,
                    Description = fix.Description,
                    IsIrrationalCost = fix.IsIrrationalCost,
                    OriginalPrice = fix.OriginalPrice,
                    RemainingAmount = fix.RemainingAmount,
                    UsedTimeRemain = fix.UsedTimeRemain,
                    MonthDepreciationRate = fix.MonthDepreciationRate,

                };
            }
        }

        public GeneralLedger FindByAccountNumber(string originalPriceAccount)
        {
            try
            {
                List<GeneralLedger> lst = Query.Where(opa => opa.Account == originalPriceAccount && opa.TypeID == 701).ToList();
                if (lst.Count > 0)
                {
                    foreach (GeneralLedger GeneralLedger in lst)
                    {
                        if (GeneralLedger.TypeID.ToString().StartsWith("70"))
                            return GeneralLedger;
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? FindLastPriority()
        {
            try
            {
                List<GeneralLedger> lst = Query.ToList();
                return lst.Count > 0 ? lst.Max(opfa => opfa.OrderPriority) : 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<GOtherVoucherDetail> getILTranferDetail(DateTime toDate)
        {
            var ddSoDonGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTron = 0;
            if (ddSoDonGia != null) lamTron = Convert.ToInt32(ddSoDonGia.Data);
            try
            {
                IAccountTransferService IAccountTransferService = IoC.Resolve<IAccountTransferService>();
                IGOtherVoucherService IGOtherVoucherService = IoC.Resolve<IGOtherVoucherService>();
                var lst = IGOtherVoucherService.Query.Where(x => x.TypeID == 620).OrderByDescending(x => x.PostedDate).ToList();
                List<GOtherVoucherDetail> listILTransferDetail = new List<GOtherVoucherDetail>();
                toDate = toDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                DateTime startDate = DateTime.Parse("01/01/1753");
                /*
                 * Lấy dữ liệu từ database 
                 * Hard Query:
                 *  select at.Description, at.ToAccount, at.FromAccount, at.TransferSide,
	                    case when at.TransferSide = 0 then sum(gl.DebitAmount) 
		                    when at.TransferSide = 1 then sum(gl.CreditAmount) 
		                    else 0 end as Amount
                    from dbo.AccountTransfer at left join dbo.GeneralLedger gl
                    on at.FromAccount = gl.Account
                    group by at.ToAccount, at.FromAccount, at.TransferSide, at.AccountTransferOrder, at.Description
                    order by at.AccountTransferOrderl;
                 */
                var lstacc = IAccountTransferService.Query.OrderBy(x => x.AccountTransferOrder).ToList();
                var lstgl = Query.Where(x => x.Account != null && x.PostedDate <= toDate).ToList().Where(c => lstacc.Any(d => c.Account.StartsWith(d.FromAccount))).ToList();
                var lstAccountGL = lstgl.Select(x => x.Account).Distinct().ToList();
                foreach (var item in lstAccountGL)
                {
                    if (!lstacc.Any(c => c.FromAccount == item))
                    {
                        var acc = lstacc.Any(c => item.StartsWith(c.FromAccount)) ? lstacc.FirstOrDefault(c => item.StartsWith(c.FromAccount)) : null;
                        if (acc != null)
                        {
                            AccountTransfer acctransfer = new AccountTransfer();
                            acctransfer.AccountTransferCode = acc.AccountTransferCode;
                            acctransfer.AccountTransferOrder = acc.AccountTransferOrder;
                            acctransfer.AccountTransferType = acc.AccountTransferType;
                            acctransfer.FromAccount = item;
                            acctransfer.ToAccount = acc.ToAccount;
                            acctransfer.TransferSide = acc.TransferSide;
                            acctransfer.Description = acc.Description;
                            acctransfer.IsActive = acc.IsActive;
                            acctransfer.IsSecurity = acc.IsSecurity;
                            lstacc.Add(acctransfer);
                        }
                    }
                }
                lstacc = lstacc.OrderBy(x => x.AccountTransferOrder).ThenBy(x => x.FromAccount).ToList();
                //var query = lstacc
                //.Join(Query, at => at.FromAccount, gl => gl.Account, (at, gl) => new { AccountTransfer = at, GeneralLedger = gl })
                //.Where(o => o.GeneralLedger.PostedDate > (lst.Count > 0 ? lst[0].PostedDate.AddHours(23).AddMinutes(59).AddSeconds(59) : startDate) && o.GeneralLedger.PostedDate <= toDate && o.AccountTransfer.FromAccount != "911");//edit by cuongpv lst[0].PostedDate -> lst[0].PostedDate.AddHours(23).AddMinutes(59).AddSeconds(59)
                //var query1 = query.ToList()
                //    .GroupBy(o => new { o.AccountTransfer.FromAccount, o.AccountTransfer.ToAccount, o.AccountTransfer.TransferSide, o.AccountTransfer.AccountTransferType, o.AccountTransfer.AccountTransferOrder, o.AccountTransfer.Description })
                //    .OrderBy(o => o.Key.AccountTransferOrder)
                //    .Select(o => new AccountTransfer
                //    {
                //        FromAccount = o.Key.FromAccount,
                //        ToAccount = o.Key.ToAccount,
                //        TransferSide = o.Key.TransferSide,
                //        Description = o.Key.Description,
                //        AccountTransferType = o.Key.AccountTransferType,
                //        AccountTransferOrder = o.Key.AccountTransferOrder,
                //        Amount = Math.Round(o.Sum(c => CalculateAmount(o.Key.TransferSide, Math.Round(c.GeneralLedger.CreditAmount, lamTron), Math.Round(c.GeneralLedger.DebitAmount, lamTron), o.Key.AccountTransferType)), lamTron),
                //        AmountOriginal = Math.Round(o.Sum(c => CalculateAmount(o.Key.TransferSide, Math.Round(c.GeneralLedger.CreditAmount, lamTron), Math.Round(c.GeneralLedger.DebitAmount, lamTron), o.Key.AccountTransferType)), lamTron)
                //    }).ToList();
                int order = 0;
                // chuyển thành dữ liệu chứng từ nghiệp vụ khác  
                foreach (var item in lstacc.Where(x => x.AccountTransferCode != "911-4212").ToList())
                {
                    GOtherVoucherDetail itemILTransferDetail = new GOtherVoucherDetail();
                    itemILTransferDetail.Description = item.Description;
                    itemILTransferDetail.CurrencyID = "VND";
                    itemILTransferDetail.Amount = lstgl.Where(x => x.Account == item.FromAccount).Sum(t => CalculateAmount(item.TransferSide, Math.Round(t.CreditAmount, lamTron), Math.Round(t.DebitAmount, lamTron), item.AccountTransferType));
                    itemILTransferDetail.AmountOriginal = itemILTransferDetail.Amount;
                    //itemILTransferDetail.Amount = query1.Any(x => x.FromAccount == item.FromAccount && x.ToAccount == item.ToAccount) ? query1.FirstOrDefault(x => x.FromAccount == item.FromAccount && x.ToAccount == item.ToAccount).Amount : 0;
                    //itemILTransferDetail.AmountOriginal = query1.Any(x => x.FromAccount == item.FromAccount && x.ToAccount == item.ToAccount) ? query1.FirstOrDefault(x => x.FromAccount == item.FromAccount && x.ToAccount == item.ToAccount).AmountOriginal : 0;
                    itemILTransferDetail.OrderPriority = order;
                    order++;
                    //switch (item.TransferSide)
                    //{
                    //    case 0:// với loại kết chuyển bên có
                    //        itemILTransferDetail.DebitAccount = item.ToAccount;
                    //        itemILTransferDetail.CreditAccount = item.FromAccount;
                    //        break;
                    //    case 1:// với loại kết chuyển bên nợ
                    //        itemILTransferDetail.DebitAccount = item.FromAccount;
                    //        itemILTransferDetail.CreditAccount = item.ToAccount;
                    //        break;
                    //    case 2:
                    //        itemILTransferDetail.DebitAccount = item.ToAccount;
                    //        itemILTransferDetail.CreditAccount = item.FromAccount;
                    //        break;
                    //}
                    switch (item.AccountTransferType)
                    {
                        case 0:// với loại kết chuyển bên có
                            itemILTransferDetail.DebitAccount = item.ToAccount;
                            itemILTransferDetail.CreditAccount = item.FromAccount;
                            if (!itemILTransferDetail.CreditAccount.StartsWith("413"))
                            {
                                itemILTransferDetail.Amount += listILTransferDetail.Where(x => x.DebitAccount == itemILTransferDetail.CreditAccount).ToList().Sum(x => x.Amount);
                                itemILTransferDetail.AmountOriginal += listILTransferDetail.Where(x => x.DebitAccount == itemILTransferDetail.CreditAccount).ToList().Sum(x => x.AmountOriginal);
                            }
                            break;
                        case 1:// với loại kết chuyển bên nợ
                            itemILTransferDetail.DebitAccount = item.FromAccount;
                            itemILTransferDetail.CreditAccount = item.ToAccount;
                            if (!itemILTransferDetail.DebitAccount.StartsWith("413"))
                            {
                                itemILTransferDetail.Amount += listILTransferDetail.Where(x => x.CreditAccount == itemILTransferDetail.DebitAccount).ToList().Sum(x => x.Amount);
                                itemILTransferDetail.AmountOriginal += listILTransferDetail.Where(x => x.CreditAccount == itemILTransferDetail.DebitAccount).ToList().Sum(x => x.AmountOriginal);
                            }
                            break;
                    }
                    if (itemILTransferDetail.Amount != 0)
                        listILTransferDetail.Add(itemILTransferDetail);
                }

                // tạo dữ liệu cặp kết chuyển 911-4212
                AccountTransfer general = IAccountTransferService.Query.Where(o => o.FromAccount == "911" && o.ToAccount == "4212").FirstOrDefault();
                GOtherVoucherDetail generalIL = new GOtherVoucherDetail();
                generalIL.CurrencyID = "VND";
                generalIL.Description = general.Description;
                generalIL.Amount = listILTransferDetail.Where(o => o.DebitAccount == "911").Sum(o => o.Amount) - listILTransferDetail.Where(o => o.CreditAccount == "911").Sum(o => o.Amount);
                generalIL.AmountOriginal = listILTransferDetail.Where(o => o.DebitAccount == "911").Sum(o => o.AmountOriginal) - listILTransferDetail.Where(o => o.CreditAccount == "911").Sum(o => o.AmountOriginal);
                generalIL.OrderPriority = order;
                //listILTransferDetail.Add(generalIL);//commnet by cuongpv

                if (generalIL.Amount < 0)
                {
                    generalIL.DebitAccount = general.FromAccount;
                    generalIL.CreditAccount = general.ToAccount;
                    generalIL.Amount = Math.Abs(generalIL.Amount);
                    generalIL.AmountOriginal = Math.Abs(generalIL.AmountOriginal);
                }
                else
                {
                    generalIL.DebitAccount = general.ToAccount;
                    generalIL.CreditAccount = general.FromAccount;
                }

                if (generalIL.Amount != 0)//add by cuongpv
                    listILTransferDetail.Add(generalIL);//add by cuongpv

                return listILTransferDetail;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return null;
            }
        }

        private decimal CalculateAmount(int transferSide, decimal creditAmount, decimal debitAmount, int transfertype)
        {
            decimal result = 0;
            if (transferSide == 0)
            {
                result = debitAmount /*- creditAmount*/;
            }
            else if (transferSide == 1)
            {
                result = creditAmount /*- debitAmount*/;
            }
            else if (transferSide == 2)
            {
                if (transfertype == 0)
                    result = debitAmount - creditAmount;
                else if (transfertype == 1)
                    result = creditAmount - debitAmount;
            }
            return result;
        }

        public List<GeneralLedger> GetByContractID(Guid contractID)
        {
            return Query.Where(x => x.ContractID == contractID).ToList();
        }

        public ToolLedger GetLastestToolLedger(Guid? iD, DateTime dp)
        {
            //DateTime pd = Utils.string;
            var lstfal = _IToolLedgerService.Query.Where(fal => fal.ToolsID == iD && fal.PostedDate <= dp).OrderByDescending(fal => fal.PostedDate).ThenByDescending(fal => fal.OrderPriority).ToList();
            if (lstfal != null && lstfal.Count > 0)
            {
                return new ToolLedger
                {
                    BranchID = lstfal[0].BranchID,
                    No = lstfal[0].No,
                    TypeID = lstfal[0].TypeID,
                    Date = lstfal[0].Date,
                    PostedDate = lstfal[0].PostedDate,
                    ToolsID = lstfal[0].ToolsID,
                    Reason = lstfal[0].Reason,
                    Description = lstfal[0].Description,
                    IncrementAllocationTime = lstfal[0].IncrementAllocationTime,
                    DecrementAllocationTime = lstfal[0].DecrementAllocationTime,
                    IncrementQuantity = lstfal[0].IncrementQuantity,
                    DecrementQuantity = lstfal[0].DecrementQuantity,
                    IncrementAmount = lstfal[0].IncrementAmount,
                    DecrementAmount = lstfal[0].DecrementAmount,
                    AllocationAmount = lstfal[0].AllocationAmount,
                    AllocatedAmount = lstfal[0].AllocatedAmount,
                    UnitPrice = lstfal[0].UnitPrice,
                    OrderPriority = lstfal[0].OrderPriority,
                    ReferenceID = lstfal[0].ReferenceID,
                    DepartmentID = lstfal[0].DepartmentID,

                    RemainingAllocaitonTimes = lstfal[0].RemainingAllocaitonTimes,
                    RemainingAmount = lstfal[0].RemainingAmount,
                    RemainingQuantity = lstfal[0].RemainingQuantity,
                    Nos = lstfal[0].No,
                    IncrementDate = lstfal[0].IncrementDate,

                };
            }
            else
            {
                TIInit fix = _TIInitService.Getbykey(iD ?? Guid.Empty);
                return new ToolLedger
                {
                    ToolsID = fix.ID,
                    AllocatedAmount = fix.AllocatedAmount,
                    UnitPrice = fix.UnitPrice,
                    RemainingAmount = fix.Amount,
                };

            }
        }

        public TIInit GetLastestTIInit(Guid iD, Guid departmentID, DateTime dp)
        {
            TIInit materialGoods = _ITIInitService.FindByMaterialGoodsIdAndDepartmentId(iD);
            TIInit tIInit = new TIInit();
            // Lấy ra giá trị ban đầu của ccdc
            if (materialGoods == null)
            {
                MaterialGoods goods = _IMaterialGoodsService.Getbykey(iD);
                tIInit = new TIInit()
                {
                    ToolsName = goods.MaterialGoodsName,
                    ToolsCode = goods.MaterialGoodsCode,
                    MaterialGoodsCategoryID = goods.MaterialGoodsCategoryID,
                    Quantity = goods.Quantity,
                    CostSetID = goods.CostSetID,
                    AllocationTimes = goods.AllocationTimes,
                    AllocationType = goods.AllocationType,
                    UnitPrice = goods.UnitPrice,
                    Unit = goods.Unit,
                    Amount = goods.Amount,
                    AllocatedAmount = goods.AllocatedAmount,
                    AllocationAwaitAccount = goods.AllocationAwaitAccount,
                };

                // lấy ra số lượng ghi tăng, giá nhập kho của ccdc theo phòng ban

            }
            else
            {
                tIInit = Utils.CloneObject(materialGoods);
            }
            // Lấy ra lần điều chỉnh gần nhất

            return materialGoods;
        }

        #region Gía thành
        // Tập hợp chi phí trực tiếp
        public List<CPExpenseList> GetCPExpenseListsOnGL(DateTime fromdate, DateTime todate, List<Guid> costsetID)
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<CostSet> listCostSet = _ICostSetService.Query.Where(x => costsetID.Contains(x.ID)).ToList();
            List<ExpenseItem> lstExpense = _IExpenseItemService.GetAll();
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<CPExpenseList> lst = new List<CPExpenseList>();
            try
            {
                lst = (from g in lstGeneralLedgers
                       where g.DebitAmount > 0 && g.ExpenseItemID != null && g.CostSetID != null
                        && g.PostedDate >= fromdate && g.PostedDate <= todate && costsetID.Any(x => x == g.CostSetID)
                       group g by new { g.CostSetID, g.Date, g.PostedDate, g.No, g.Reason, g.DebitAmount, g.ExpenseItemID, g.TypeID }
                        into t
                       select new CPExpenseList()
                       {
                           ID = Guid.NewGuid(),
                           CostSetID = t.Key.CostSetID ?? Guid.Empty,
                           CostsetCode = listCostSet.FirstOrDefault(x => x.ID == (Guid)t.Key.CostSetID).CostSetCode,
                           CostsetName = listCostSet.FirstOrDefault(x => x.ID == (Guid)t.Key.CostSetID).CostSetName,
                           Date = t.Key.Date,
                           PostedDate = t.Key.PostedDate,
                           No = t.Key.No,
                           Description = t.Key.Reason,
                           Amount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                           ExpenseItemCode = lstExpense.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                           ExpenseItemID = (Guid)t.Key.ExpenseItemID,
                           TypeVoucher = 0,
                           TypeID = t.Key.TypeID
                       }).OrderByDescending(x => x.CostsetCode).ToList();

            }
            catch (Exception ex)
            {

            }
            return lst;
        }
        public List<CPExpenseList> GetCPExpenseListsContractOnGL(DateTime fromdate, DateTime todate, List<Guid> costsetID) //chi phí trực tiếp hợp đồng
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<EMContract> lstContract = _IEMContractService.Query.Where(x => costsetID.Contains(x.ID)).ToList();
            List<ExpenseItem> lstExpense = _IExpenseItemService.GetAll();
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<CPExpenseList> lst = (from g in lstGeneralLedgers
                                       where g.DebitAmount > 0 && g.ExpenseItemID != null
                                        && g.PostedDate >= fromdate && g.PostedDate <= todate && costsetID.Any(x => x == g.ContractID)
                                       group g by new { g.ContractID, g.Date, g.PostedDate, g.No, g.Reason, g.DebitAmount, g.ExpenseItemID, g.TypeID }
                        into t
                                       select new CPExpenseList()
                                       {
                                           ID = Guid.NewGuid(),
                                           ContractID = t.Key.ContractID ?? Guid.Empty,
                                           ContractCode = lstContract.FirstOrDefault(x => x.ID == t.Key.ContractID).Code,
                                           Date = t.Key.Date,
                                           PostedDate = t.Key.PostedDate,
                                           No = t.Key.No,
                                           Description = t.Key.Reason,
                                           Amount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                           ExpenseItemCode = lstExpense.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                           ExpenseItemID = (Guid)t.Key.ExpenseItemID,
                                           TypeVoucher = 0,
                                           TypeID = t.Key.TypeID
                                       }).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
            return lst;
        }

        // Tập hợp các khoản giảm giá thành
        public List<CPExpenseList> GetCPExpenseListsOnGL2(DateTime fromdate, DateTime todate, List<Guid> costsetID)
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<CostSet> listCostSet = _ICostSetService.Query.Where(x => costsetID.Contains(x.ID)).ToList();
            List<ExpenseItem> lstExpense = _IExpenseItemService.GetAll();
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<CPExpenseList> lst = (from g in lstGeneralLedgers
                                       where g.CreditAmount > 0 && g.ExpenseItemID != null
                                        && g.PostedDate >= fromdate && g.PostedDate <= todate && costsetID.Any(x => x == g.CostSetID)
                                       group g by new { g.CostSetID, g.Date, g.PostedDate, g.No, g.Reason, g.DebitAmount, g.ExpenseItemID, g.TypeID }
                        into t
                                       select new CPExpenseList()
                                       {
                                           ID = Guid.NewGuid(),
                                           CostSetID = t.Key.CostSetID ?? Guid.Empty,
                                           CostsetCode = listCostSet.FirstOrDefault(x => x.ID == (Guid)t.Key.CostSetID).CostSetCode,
                                           CostsetName = listCostSet.FirstOrDefault(x => x.ID == (Guid)t.Key.CostSetID).CostSetName,
                                           Date = t.Key.Date,
                                           PostedDate = t.Key.PostedDate,
                                           No = t.Key.No,
                                           Description = t.Key.Reason,
                                           Amount = Math.Round(t.Sum(x => x.CreditAmount), lamtron, MidpointRounding.AwayFromZero),
                                           ExpenseItemCode = lstExpense.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                           ExpenseItemID = (Guid)t.Key.ExpenseItemID,
                                           TypeVoucher = 1,
                                           TypeID = t.Key.TypeID
                                       }).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
            return lst;
        }
        public List<CPExpenseList> GetCPExpenseListsContractOnGL2(DateTime fromdate, DateTime todate, List<Guid> costsetID)
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<EMContract> lstContract = _IEMContractService.Query.Where(x => costsetID.Contains(x.ID)).ToList();
            List<ExpenseItem> lstExpense = _IExpenseItemService.GetAll();
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<CPExpenseList> lst = (from g in lstGeneralLedgers
                                       where g.CreditAmount > 0 && g.ExpenseItemID != null
                                        && g.PostedDate >= fromdate && g.PostedDate <= todate && costsetID.Any(x => x == g.ContractID)
                                       group g by new { g.ContractID, g.Date, g.PostedDate, g.No, g.Reason, g.DebitAmount, g.ExpenseItemID, g.TypeID }
                        into t
                                       select new CPExpenseList()
                                       {
                                           ID = Guid.NewGuid(),
                                           ContractID = t.Key.ContractID ?? Guid.Empty,
                                           ContractCode = lstContract.FirstOrDefault(x => x.ID == t.Key.ContractID).Code,
                                           Date = t.Key.Date,
                                           PostedDate = t.Key.PostedDate,
                                           No = t.Key.No,
                                           Description = t.Key.Reason,
                                           Amount = Math.Round(t.Sum(x => x.CreditAmount), lamtron, MidpointRounding.AwayFromZero),
                                           ExpenseItemCode = lstExpense.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                           ExpenseItemID = (Guid)t.Key.ExpenseItemID,
                                           TypeVoucher = 1,
                                           TypeID = t.Key.TypeID
                                       }).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
            return lst;
        }
        // Chi phí phân bổ 
        public List<CPAllocationGeneralExpense> GetCPAllocationGeneralExpenseOnGL(DateTime fromdate, DateTime todate)
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<ExpenseItem> lstExpenseItem = _IExpenseItemService.GetAll();
            List<EMContract> lstContract = _IEMContractService.GetAll();
            List<EMContract> lstContract1 = (from g in lstContract where g.TypeID == 850 select g).Union(from g in lstContract where g.TypeID == 860 && g.IsWatchForCostPrice == false select g).ToList();
            List<CPAllocationGeneralExpense> lst = (from g in lstGeneralLedgers
                                                    join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                    where g.DebitAmount > 0 && g.ExpenseItemID != null && g.CostSetID == null
                                                     && (g.PostedDate >= fromdate && g.PostedDate <= todate) && h.ExpenseType == 2 && g.ContractID == null
                                                    group g by new { g.DetailID, g.ExpenseItemID, g.DebitAmount }
                        into t
                                                    select new CPAllocationGeneralExpense()
                                                    {
                                                        ID = Guid.NewGuid(),
                                                        ExpenseItemID = t.Key.ExpenseItemID ?? Guid.Empty,
                                                        ExpenseItemCode = lstExpenseItem.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                                        TotalCost = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                        UnallocatedAmount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                        ReferenceID = (Guid)t.Key.DetailID
                                                    }).
                                       Union(from g in lstGeneralLedgers
                                             join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                             join i in lstContract1 on g.ContractID equals i.ID
                                             where g.DebitAmount > 0 && g.ExpenseItemID != null && g.CostSetID == null
                                              && (g.PostedDate >= fromdate && g.PostedDate <= todate) && h.ExpenseType == 2
                                             group g by new { g.DetailID, g.ExpenseItemID, g.DebitAmount }
                        into t
                                             select new CPAllocationGeneralExpense()
                                             {
                                                 ID = Guid.NewGuid(),
                                                 ExpenseItemID = t.Key.ExpenseItemID ?? Guid.Empty,
                                                 ExpenseItemCode = lstExpenseItem.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                                 TotalCost = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                 UnallocatedAmount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                 ReferenceID = (Guid)t.Key.DetailID
                                             })
                                       .OrderBy(x => x.ExpenseItemCode).ToList();
            return lst;
        }

        #region add by cuongpv chinh sua lay luy ke phan bo ky truoc
        public List<CPAllocationGeneralExpense> GetCPAllocationGeneralExpenseOnGLBefore(DateTime fromdate)
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<ExpenseItem> lstExpenseItem = _IExpenseItemService.GetAll();
            List<EMContract> lstContract = _IEMContractService.GetAll();
            List<EMContract> lstContract1 = (from g in lstContract where g.TypeID == 850 select g).Union(from g in lstContract where g.TypeID == 860 && g.IsWatchForCostPrice == false select g).ToList();
            List<CPAllocationGeneralExpense> lst = (from g in lstGeneralLedgers
                                                    join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                    where g.DebitAmount > 0 && g.ExpenseItemID != null && g.CostSetID == null
                                                     && g.PostedDate < fromdate && h.ExpenseType == 2 && g.ContractID == null
                                                    group g by new { g.DetailID, g.ExpenseItemID, g.DebitAmount }
                        into t
                                                    select new CPAllocationGeneralExpense()
                                                    {
                                                        ID = Guid.NewGuid(),
                                                        ExpenseItemID = t.Key.ExpenseItemID ?? Guid.Empty,
                                                        ExpenseItemCode = lstExpenseItem.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                                        TotalCost = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                        UnallocatedAmount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                        ReferenceID = (Guid)t.Key.DetailID
                                                    }).
                                       Union(from g in lstGeneralLedgers
                                             join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                             join i in lstContract1 on g.ContractID equals i.ID
                                             where g.DebitAmount > 0 && g.ExpenseItemID != null && g.CostSetID == null
                                              && g.PostedDate < fromdate && h.ExpenseType == 2
                                             group g by new { g.DetailID, g.ExpenseItemID, g.DebitAmount }
                        into t
                                             select new CPAllocationGeneralExpense()
                                             {
                                                 ID = Guid.NewGuid(),
                                                 ExpenseItemID = t.Key.ExpenseItemID ?? Guid.Empty,
                                                 ExpenseItemCode = lstExpenseItem.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                                 TotalCost = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                 UnallocatedAmount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                 ReferenceID = (Guid)t.Key.DetailID
                                             })
                                       .OrderBy(x => x.ExpenseItemCode).ToList();
            return lst;
        }
        #endregion

        public List<CPAllocationGeneralExpense> GetCPAllocationGeneralExpenseContractOnGL(DateTime fromdate, DateTime todate)
        {
            int lamtron = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtron = int.Parse(ddSoTyGia.Data);
            List<GeneralLedger> lstGeneralLedgers = Query.Where(x => x.Account.StartsWith("154")).ToList();
            List<ExpenseItem> lstExpenseItem = _IExpenseItemService.GetAll();
            List<EMContract> lstContract = _IEMContractService.GetAll();
            List<EMContract> lstContract1 = (from g in lstContract where g.TypeID == 850 select g).Union(from g in lstContract where g.TypeID == 860 && g.IsWatchForCostPrice == false select g).ToList();
            List<CPAllocationGeneralExpense> lst = (from g in lstGeneralLedgers
                                                    join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                    where g.DebitAmount > 0 && g.ExpenseItemID != null && g.ContractID == null
                                                     && (g.PostedDate >= fromdate && g.PostedDate <= todate) && h.ExpenseType == 2
                                                    group g by new { g.DetailID, g.ExpenseItemID, g.DebitAmount }
                        into t
                                                    select new CPAllocationGeneralExpense()
                                                    {
                                                        ID = Guid.NewGuid(),
                                                        ExpenseItemID = t.Key.ExpenseItemID ?? Guid.Empty,
                                                        ExpenseItemCode = lstExpenseItem.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                                        TotalCost = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                        UnallocatedAmount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                        ReferenceID = (Guid)t.Key.DetailID
                                                    }).
                                                    Union(from g in lstGeneralLedgers
                                                          join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                          join i in lstContract1 on g.ContractID equals i.ID
                                                          where g.DebitAmount > 0 && g.ExpenseItemID != null && g.CostSetID == null
                                                           && (g.PostedDate >= fromdate && g.PostedDate <= todate) && h.ExpenseType == 2
                                                          group g by new { g.DetailID, g.ExpenseItemID, g.DebitAmount }
                        into t
                                                          select new CPAllocationGeneralExpense()
                                                          {
                                                              ID = Guid.NewGuid(),
                                                              ExpenseItemID = t.Key.ExpenseItemID ?? Guid.Empty,
                                                              ExpenseItemCode = lstExpenseItem.FirstOrDefault(x => x.ID == t.Key.ExpenseItemID).ExpenseItemCode,
                                                              TotalCost = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                              UnallocatedAmount = Math.Round(t.Sum(x => x.DebitAmount), lamtron, MidpointRounding.AwayFromZero),
                                                              ReferenceID = (Guid)t.Key.DetailID
                                                          })
                                                    .OrderBy(x => x.ExpenseItemCode).ToList();
            return lst;
        }
        #endregion
    }
}