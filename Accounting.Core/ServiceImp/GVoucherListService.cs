﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class GVoucherListService : BaseService<GVoucherList, Guid>, IGVoucherListService
    {
        public GVoucherListService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<GVoucherList> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.No).ToList();
        }
    }

}