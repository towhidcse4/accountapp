using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153AdjustAnnouncementDetailService: BaseService<TT153AdjustAnnouncementDetail ,Guid>,ITT153AdjustAnnouncementDetailService
    {
        public TT153AdjustAnnouncementDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}