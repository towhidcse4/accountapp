﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TemplateColumnService : BaseService<TemplateColumn, Guid>, ITemplateColumnService
    {
        public TemplateColumnService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public ITemplateService _templateSrv { get { return IoC.Resolve<ITemplateService>(); } }
        public ITemplateDetailService _templateDetailSrv { get { return IoC.Resolve<ITemplateDetailService>(); } }
        public ITemplateColumnService _templateColumnSrv { get { return IoC.Resolve<ITemplateColumnService>(); } }
        public System.Collections.Generic.List<TemplateColumn> getTemplateColumnsByTable(Guid TableID)
        {
            return Query.Where(k => k.TemplateDetailID == TableID).ToList();
        }
        /// <summary>
        /// Lấy mẫu chuẩn của tab Detail theo loại chứng từ
        /// </summary>
        /// <param name="typeId">Loại chứng từ</param>
        /// <returns></returns>
        public List<TemplateColumn> GetTemplateDetailDefaultByTypeId(int typeId)
        {
            try
            {
                string sql = "SELECT * FROM TemplateColumn WHERE TemplateDetailID IN (SELECT ID FROM TemplateDetail WHERE TabIndex = 0 AND TemplateID IN (SELECT ID FROM Template WHERE Template.IsDefault = 1 AND Template.IsSecurity = 1 AND Template.TypeID = " + typeId + " ))";
                var qr = GetbySQLQuery(sql);
                //var qr = from t in _templateSrv.Query.Where(t => t.TypeID == typeId && t.IsDefault == true && t.IsSecurity).ToList()
                //         join d in _templateDetailSrv.Query.Where(k => k.TabIndex == 0).ToList() on t.ID equals d.TemplateID
                //         join c in Query.ToList() on d.ID equals c.TemplateDetailID
                //         select c;
                return qr.ToList();
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }
    }

}