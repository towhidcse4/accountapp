using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class PrepaidExpenseService : BaseService<PrepaidExpense, Guid>, IPrepaidExpenseService
    {
        public PrepaidExpenseService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IGOtherVoucherService IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        public ISystemOptionService ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public IGOtherVoucherDetailExpenseService IGOtherVoucherDetailExpenseService { get { return IoC.Resolve<IGOtherVoucherDetailExpenseService>(); } }
        public List<PrepaidExpense> GetAll_ByDate(DateTime Date)
        {
            var date = new DateTime(Date.Year, Date.Month, DateTime.DaysInMonth(Date.Year, Date.Month));
            var lstcppb = IGOtherVoucherDetailExpenseService.GetAll();
            var lstcp = Query.Where(p => p.Date <= date && p.Amount != p.AllocationAmount && !p.IsActive).ToList().Where(p => ((p.AllocationTime - p.AllocatedPeriod) != lstcppb.Where(c => c.PrepaidExpenseID == p.ID).ToList().Count)).ToList();
            return lstcp;
        }
        public List<PrepaidExpense> GetAll_Order()
        {
            var lst = GetAll();
            var lstpbcp = IGOtherVoucherDetailExpenseService.GetAll();
            foreach (var x in lst)
            {
                x.AllocationAmount = ((x.TypeExpense == 0 ? x.AllocationAmount : 0) + (lstpbcp.Where(c => c.PrepaidExpenseID == x.ID).ToList().Count > 0 ? lstpbcp.Where(c => c.PrepaidExpenseID == x.ID).Sum(t => t.AllocationAmount) : 0));
                x.AllocatedPeriod = ((x.TypeExpense == 0 ? x.AllocatedPeriod : 0) + (lstpbcp.Where(c => c.PrepaidExpenseID == x.ID).ToList().Count));
            }
            return lst;
        }
        public List<PrepaidExpenseAllocation_Report> GetReport_ByDate(DateTime FDate, DateTime TDate)
        {
            var ddSoGia = ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);
            var fdate = new DateTime(FDate.Year, FDate.Month, FDate.Day);
            var tdate = new DateTime(TDate.Year, TDate.Month, TDate.Day);
            var lstPB = IGOtherVoucherDetailExpenseService.Query.Where(x => IGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID /*&& c.PostedDate >= fdate*/ && c.PostedDate <= tdate)).ToList();
            var lst = new List<PrepaidExpenseAllocation_Report>();
            var lst1 = Query.Where(x => x.Date <= tdate).ToList();
            try
            {
                foreach (var x in lst1)
                {
                    var model = new PrepaidExpenseAllocation_Report
                    {
                        ID = x.ID,
                        MaCP = x.PrepaidExpenseCode,
                        TenCP = x.PrepaidExpenseName,
                        NgayGhiNhan = x.Date,
                        SoTien = Math.Round(x.Amount, lamTronGia),
                        SoKyPB = x.AllocationTime,
                        SoKyDaPB = (x.AllocatedPeriod + lstPB.Where(c => c.PrepaidExpenseID == x.ID).ToList().Count),
                        SoKyPBConLai = (x.AllocationTime - (x.AllocatedPeriod + lstPB.Where(c => c.PrepaidExpenseID == x.ID).ToList().Count)),
                        LuyKeDaPB = (Math.Round(x.AllocationAmount, lamTronGia) + Math.Round(lstPB.Where(c => c.PrepaidExpenseID == x.ID).ToList().Sum(d => d.AllocationAmount), lamTronGia)),
                        SoTienConLai = (Math.Round(x.Amount, lamTronGia) - (Math.Round(x.AllocationAmount, lamTronGia) + Math.Round(lstPB.Where(c => c.PrepaidExpenseID == x.ID).ToList().Sum(d => d.AllocationAmount), lamTronGia)))
                    };
                    lst.Add(model);
                }

            }
            catch (Exception ex) { }
            return lst;
        }
        public bool CheckDelete(Guid ID)
        {
            if (IGOtherVoucherDetailExpenseService.Query.Any(x => x.PrepaidExpenseID == ID)) return false;
            else return true;
        }
    }

}