﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.FADecrements;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;


namespace Accounting.Core.ServiceImp
{
    public class FADecrementService : BaseService<Accounting.Core.Domain.FADecrement, Guid>, IFADecrementService
    {
        public FADecrementService(string sessionFactoryConfigPath)
                : base(sessionFactoryConfigPath)
        { }

        public IFAIncrementService _IFAIncrementService { get { return IoC.Resolve<IFAIncrementService>(); } }
        public IFAIncrementDetailService _IFAIncrementDetailService { get { return IoC.Resolve<IFAIncrementDetailService>(); } }
        public IFAAdjustmentService _IFAAdjustmentService { get { return IoC.Resolve<IFAAdjustmentService>(); } }
        public IFAAdjustmentDetailService _IFAAdjustmentDetailService { get { return IoC.Resolve<IFAAdjustmentDetailService>(); } }
        public IFixedAssetService _IFixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
        public IFADecrementService _IFADecrementService { get { return IoC.Resolve<IFADecrementService>(); } }
        public IFADecrementDetailService _IFADecrementDetailService { get { return IoC.Resolve<IFADecrementDetailService>(); } }

        public FADecrement findByRefID(Guid iD)
        {
            var lst = Query.Where(fad => fad.RefID == iD).ToList();
            if (lst != null && lst.Count > 0) return lst[0];
            return null;
        }

        public List<GGHachToan> GetListGG(string FACode)
        {
            List<GGHachToan> lstGG = new List<GGHachToan>();
            FixedAsset theFA = _IFixedAssetService.Query.Where(f => f.FixedAssetCode == FACode).FirstOrDefault();

            FAIncrementDetail theFAIncrementDetail = _IFAIncrementDetailService.Query.Where(ad => ad.FixedAssetID == theFA.ID).FirstOrDefault();
            FAIncrement theFAIncrement = _IFAIncrementService.Query.Where(a => a.ID == theFAIncrementDetail.FAIncrementID).FirstOrDefault();

            FAAdjustment theFAAdjustment = _IFAAdjustmentService.Query.Where(aa => aa.FixedAssetID == theFA.ID).FirstOrDefault();
            FAAdjustmentDetail theFAAdjustmentDetail = null;
            if (theFAAdjustment != null)
            {
                theFAAdjustmentDetail = _IFAAdjustmentDetailService.Query.Where(aad => aad.FAAdjustmentID == theFAAdjustment.ID).FirstOrDefault();
            }

            GGHachToan FAIncrement_Hachtoan = new GGHachToan();
            FAIncrement_Hachtoan.FACode = theFA.FixedAssetCode;
            if (theFAAdjustment != null)
            {
                FAIncrement_Hachtoan.Description = theFAAdjustment.Reason;
                FAIncrement_Hachtoan.DebitAccount = theFAAdjustmentDetail.DebitAccount;
                FAIncrement_Hachtoan.CreditAccount = theFAAdjustmentDetail.CreditAccount;
                FAIncrement_Hachtoan.Amount = theFAAdjustmentDetail.Amount;
            }
            else
            {
                FAIncrement_Hachtoan.Description = theFAIncrement.Reason;
                FAIncrement_Hachtoan.DebitAccount = theFAIncrementDetail.DebitAccount;
                FAIncrement_Hachtoan.CreditAccount = theFAIncrementDetail.CreditAccount;
                FAIncrement_Hachtoan.Amount = theFAIncrementDetail.Amount;
            }
            lstGG.Add(FAIncrement_Hachtoan);

            FADecrementDetail theFADecrementDetail = _IFADecrementDetailService.Query.Where(fadd => fadd.FixedAssetID == theFA.ID).FirstOrDefault();
            FADecrement theFADecrement = null;
            GGHachToan FADecrement_Hachtoan = new GGHachToan();
            if (theFADecrementDetail != null)
            {
                theFADecrement = _IFADecrementService.Query.Where(fad => fad.ID == theFADecrementDetail.FADecrementID).FirstOrDefault();
                FADecrement_Hachtoan.FACode = theFA.FixedAssetCode;
                FADecrement_Hachtoan.Description = theFADecrement.Reason;
                //FADecrement_Hachtoan.DebitAccount = theFADecrementDetail.DebitAccount;
                //FADecrement_Hachtoan.CreditAccount = theFADecrementDetail.CreditAccount;
                //FADecrement_Hachtoan.Amount = theFADecrementDetail.Amount;
                lstGG.Add(FADecrement_Hachtoan);
            }

            return lstGG;
        }

    }
}
