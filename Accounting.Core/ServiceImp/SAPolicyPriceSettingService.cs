﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Core;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class SAPolicyPriceSettingService : BaseService<SAPolicyPriceSetting, Guid>, ISAPolicyPriceSettingService
    {
        public SAPolicyPriceSettingService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private ISAPolicyPriceTableService _ISAPolicyPriceTableService
        {
            get { return IoC.Resolve<ISAPolicyPriceTableService>(); }
        }
        private IMaterialGoodsService _IMaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        private IMaterialGoodsCategoryService _IMaterialGoodsCategoryService
        {
            get { return IoC.Resolve<IMaterialGoodsCategoryService>(); }
        }

        private IAccountingObjectService _IAccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        private ISAPolicySalePriceCustomerService _ISAPolicySalePriceCustomerService
        {
            get { return IoC.Resolve<ISAPolicySalePriceCustomerService>(); }
        }
        private ISAPolicySalePriceGroupService _ISAPolicySalePriceGroupService
        {
            get { return IoC.Resolve<ISAPolicySalePriceGroupService>(); }
        }
        private ISalePriceGroupService _ISalePriceGroupService
        {
            get { return IoC.Resolve<ISalePriceGroupService>(); }
        }
        private IAccountingObjectCategoryService _IAccountingObjectCategoryService
        {
            get { return IoC.Resolve<IAccountingObjectCategoryService>(); }
        }

        /// <summary>
        /// [DungNA]
        /// Lấy danh sách chính sách giá giá bán
        /// </summary>
        /// <returns></returns>
        public List<SAPolicyPriceSetting> GetPolicyPriceSettings()
        {
            List<SAPolicyPriceSetting> lstPolicyPriceSettings = new List<SAPolicyPriceSetting>();
            lstPolicyPriceSettings = Query.ToList();

            return (from a in lstPolicyPriceSettings
                    select new SAPolicyPriceSetting()
                    {
                        ID = a.ID,
                        SAPolicyPriceName = a.SAPolicyPriceName ?? "",
                        IsActive = a.IsActive,
                    }).ToList();
        }
        /// <summary>
        /// [DungNA]
        /// Lấy danh bật tư hàng hóa áp dụng
        /// </summary>
        /// <returns></returns>
        public List<SAPolicyPriceTableHT> GetPolicyPriceTabless(Guid saPolicyPriceSettingID)
        {

            List<MaterialGoods> lstMaterialGoodses = _IMaterialGoodsService.Query.ToList();
            List<MaterialGoodsCategory> lstMaterialGoodsCategory = _IMaterialGoodsCategoryService.Query.ToList();

            List<SAPolicyPriceTable> lstSaPolicyPriceTables = new List<SAPolicyPriceTable>();
            List<SAPolicyPriceTableHT> lstPolicyPriceTableHts = new List<SAPolicyPriceTableHT>();
            lstSaPolicyPriceTables = _ISAPolicyPriceTableService.Query.Where(p => p.SAPolicyPriceSettingID == saPolicyPriceSettingID).ToList();
            lstPolicyPriceTableHts = (from a in lstSaPolicyPriceTables
                                      join b in lstMaterialGoodses on a.MaterialGoodsID equals b.ID
                                      //join c in lstMaterialGoodsCategory on b.MaterialGoodsCategoryID equals c.ID
                                      select new SAPolicyPriceTableHT()
                                      {
                                          MaterialGoodsCategoryID = b.MaterialGoodsCategoryID,
                                          MaterialGoodsCategoryCode = b.MaterialGoodsCategoryCode,
                                          MaterialGoodsCode = b.MaterialGoodsCode ?? "",
                                          MaterialGoodsName = b.MaterialGoodsName ?? "",
                                          SalePriceGroupID = a.SalePiceGroupID,
                                          Price = a.Price
                                      }).ToList();
            return lstPolicyPriceTableHts;
        }
        /// <summary>
        /// [DungNA]
        /// Lấy ra danh sách khách hàng áp dụng
        /// </summary>
        /// <returns></returns>
        public List<SAPolicySalePriceCustomerHT> GetSAPolicySalePriceCustomerHT(Guid saPolicyPriceSettingID)
        {
            List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.Where(c => c.ObjectType == 0 || c.ObjectType == 2).ToList();
            List<SAPolicySalePriceCustomer> lstSaPolicySalePriceCustomers = _ISAPolicySalePriceCustomerService.Query.ToList();
            List<SalePriceGroup> lstSalePriceGroups = _ISalePriceGroupService.Query.ToList();
            List<SAPolicySalePriceGroup> lstSaPolicySalePriceGroups = _ISAPolicySalePriceGroupService.Query.Where(p => p.SAPolicyPriceSettingID == saPolicyPriceSettingID).ToList();
            List<SAPolicySalePriceCustomerHT> lstPolicySalePriceCustomerHts = new List<SAPolicySalePriceCustomerHT>();
            lstPolicySalePriceCustomerHts = (from a in lstSaPolicySalePriceCustomers
                                             join b in lstSaPolicySalePriceGroups on a.SAPolicySalePriceGroupID equals b.ID
                                             join c in lstAccountingObjects on a.AccountingObjectID equals c.ID
                                             join d in lstSalePriceGroups on b.SalePriceGroupID equals d.ID
                                             select new SAPolicySalePriceCustomerHT()
                                             {
                                                 AccountingObjectCode = c.AccountingObjectCode ?? "",
                                                 AccountingObjectName = c.AccountingObjectName ?? "",
                                                 AccountingObjectCategory = d.SalePriceGroupName ?? "",
                                             }).ToList();

            return lstPolicySalePriceCustomerHts;


        }
        /// <summary>
        /// [DungNA]
        /// Lấy ra danh sách vật tư hàng hóa
        /// </summary>
        /// <returns></returns>
        public List<MaterialGoodHT> GetMaterialGoodHts(bool them, Guid ID)
        {            
            List<MaterialGoods> lstMaterialGoodses = new List<MaterialGoods>();
            if (them)
            {
                List<SAPolicyPriceSetting> lstpps = Query.Where(x => !x.IsActive).ToList();
                List<SAPolicyPriceTable> lstmg = _ISAPolicyPriceTableService.GetAll();
                var lstguid = from a in lstpps
                              join b in lstmg on a.ID equals b.SAPolicyPriceSettingID
                              select new { b.MaterialGoodsID };
                lstMaterialGoodses = _IMaterialGoodsService.Query.ToList().Where(x => !lstguid.Any(c => c.MaterialGoodsID == x.ID)).ToList();
            }
            else
            {
                SAPolicyPriceSetting lstpps = Getbykey(ID);
                List<SAPolicyPriceTable> lstmg = _ISAPolicyPriceTableService.Query.Where(x=>x.SAPolicyPriceSettingID == lstpps.ID).ToList();
                lstMaterialGoodses = _IMaterialGoodsService.Query.ToList().Where(x => lstmg.Any(c => c.MaterialGoodsID == x.ID)).ToList();
            }                
            List<MaterialGoodsCategory> lstMaterialGoodsCategories = _IMaterialGoodsCategoryService.Query.ToList();
            List<MaterialGoodHT> lstMaterialGoodHts = new List<MaterialGoodHT>();
            lstMaterialGoodHts = (from a in lstMaterialGoodses
                                  select new MaterialGoodHT()
                                  {
                                      ID = a.ID,
                                      LoaiVTHH = (a.MaterialGoodsCategoryID != null && lstMaterialGoodsCategories.Count(b => b.ID == a.MaterialGoodsCategoryID) == 1) ? lstMaterialGoodsCategories.Single(b => b.ID == a.MaterialGoodsCategoryID).MaterialGoodsCategoryCode : "",
                                      MaVTHH = a.MaterialGoodsCode ?? "",
                                      TenVTHH = a.MaterialGoodsName ?? "",
                                      GiaNhapGanNhat = (decimal)(a.PurchasePrice != null ? a.PurchasePrice : 0),
                                      FixedSalePrice = (decimal)(a.FixedSalePrice != null ? a.FixedSalePrice : 0),
                                      SalePrice = (decimal)(a.SalePrice != null ? a.SalePrice : 0),
                                      SalePrice2 = (decimal)(a.SalePrice2 != null ? a.SalePrice2 : 0),
                                      SalePrice3 = (decimal)(a.SalePrice3 != null ? a.SalePrice3 : 0)
                                  }).ToList();
            return lstMaterialGoodHts;
        }

        /// <summary>
        /// [DungNA]
        /// Lấy danh sách nhóm giá bán
        /// </summary>
        /// <returns></returns>
        public List<GroupSalePriceHT> GetGroupSalePriceHts()
        {
            List<GroupSalePriceHT> lstGroupSalePriceHts = new List<GroupSalePriceHT>();
            List<SalePriceGroup> lstSalePriceGroups = _ISalePriceGroupService.Query.ToList();
            //List<SAPolicySalePriceGroup> lstSaPolicySalePriceGroups = _ISAPolicySalePriceGroupService.Query.ToList();
            //var querySalePriceGroup = (from spGroup in lstSalePriceGroups
            //                           join spPriceGroup in lstSaPolicySalePriceGroups on spGroup.ID equals spPriceGroup.SalePriceGroupID into gj
            //                           from subspPriceGroup in gj.DefaultIfEmpty()
            //                           select new
            //                           {
            //                               Id = spGroup.ID,
            //                               NhomGia = spGroup.SalePriceGroupCode ?? "",
            //                               TenNhomGia = spGroup.SalePriceGroupName ?? "",
            //                               DuaTren = (subspPriceGroup == null ? -1 : subspPriceGroup.BasedOn),
            //                               PhuongPhap = (subspPriceGroup == null ? -1 : subspPriceGroup.Method),
            //                               PhanTram = (subspPriceGroup == null ? 0 : subspPriceGroup.AmountAdjust)
            //                           }).ToList();
            lstGroupSalePriceHts = (from a in lstSalePriceGroups
                                    select new GroupSalePriceHT()
                                    {
                                        Id = a.ID,
                                        NhomGia = a.SalePriceGroupCode,
                                        TenNhomGias = a.SalePriceGroupName,
                                    }).ToList();
            return lstGroupSalePriceHts;

        }
        /// <summary>
        /// [DungNA]
        /// Lấy danh sách thiết lập bảng giá
        /// </summary>
        /// <returns></returns>
        public List<PriceUpHT> GetListPriceUpHT(List<MaterialGoodHT> lsMaterialGoodHts)
        {
            List<PriceUpHT> lstPriceUpHts = new List<PriceUpHT>();
            List<MaterialGoodsCategory> lstMaterialGoodsCategories = _IMaterialGoodsCategoryService.Query.ToList();
            // List<MaterialGoods> lstMaterialGoodses = _IMaterialGoodsService.Query.ToList();
            lstPriceUpHts = (from a in lsMaterialGoodHts
                             select new PriceUpHT()
                             {
                                 ID = a.ID,
                                 MaVTHH = a.MaVTHH ?? "",
                                 TenVTHH = a.TenVTHH ?? "",
                                 NhomVTHH = a.LoaiVTHH ?? "",
                                 GiaBanGanNhat = (decimal)(a.GiaNhapGanNhat != null ? a.GiaNhapGanNhat : 0),

                             }).ToList();
            return lstPriceUpHts;
        }
        /// <summary>
        /// [DungNA]
        /// Chọn khách hàng cho các nhóm
        /// </summary>
        /// <returns></returns>
        public List<SAPolicySalePriceCustomerHT> GetLstChoiceAccountingOBJ()
        {
            List<SAPolicySalePriceCustomerHT> lstPolicySalePriceCustomerHts = new List<SAPolicySalePriceCustomerHT>();
            List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.Where(c => c.ObjectType != null && c.ObjectType == 0 || c.ObjectType == 2).ToList();
            List<AccountingObjectCategory> lstAccountingObjectCategories = _IAccountingObjectCategoryService.Query.ToList();
            List<SalePriceGroup> lstSalePriceGroups = _ISalePriceGroupService.Query.ToList();
            lstPolicySalePriceCustomerHts = (from a in lstAccountingObjects
                                             select new SAPolicySalePriceCustomerHT()
                                             {
                                                 ID = a.ID,
                                                 AccountingObjectCode = a.AccountingObjectCode ?? "",
                                                 AccountingObjectName = a.AccountingObjectName ?? "",
                                                 AccountingObjectCategory = a.AccountingObjectCategory ?? "",
                                                 // AccountinggObjectCategoryCode =  lstAccountingObjectCategories.FirstOrDefault(c => c.ID == a.ID).AccountingObjectCategoryCode ?? "",
                                                 //Bảng AccountingObjectCategory chưa móc với bảng accountingobject đang kiểm tra,móc theo accountingobjecteccatregoryName
                                             }
                                                ).ToList();
            return lstPolicySalePriceCustomerHts;
        }

        public decimal UnitPrice(Guid MID, Guid CID)
        {
            List<SAPolicySalePriceCustomer> lstpc = _ISAPolicySalePriceCustomerService.Query.ToList().Where(x=>x.AccountingObjectID == CID).ToList();
            List<SAPolicySalePriceGroup> lstspg = _ISAPolicySalePriceGroupService.Query.ToList().Where(x => lstpc.Any(d => d.SAPolicySalePriceGroupID == x.ID)).ToList();
            List<SAPolicyPriceTable> lstpt = _ISAPolicyPriceTableService.Query.ToList().Where(x=> lstspg.Any(d => d.SAPolicyPriceSettingID == x.SAPolicyPriceSettingID)).ToList().Where(a=>a.MaterialGoodsID == MID).ToList();
            if (lstpt.Count > 0)
                return lstpt[0].Price;
            return 0;
        }

    }

}