using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CPResultService: BaseService<CPResult ,Guid>,ICPResultService
    {
        public CPResultService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<CPResult> GetList()
        {
            return Query.ToList();
        }
    }

}