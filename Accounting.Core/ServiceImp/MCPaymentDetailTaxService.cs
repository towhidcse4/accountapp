using System;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentDetailTaxService : BaseService<MCPaymentDetailTax, Guid>, IMCPaymentDetailTaxService
    {
        public MCPaymentDetailTaxService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.IList<MCPaymentDetailTax> getMCPaymentDetailTaxbyID(Guid id)
        {
            return Query.Where(k => k.MCPaymentID == id).ToList();
        }
    }

}