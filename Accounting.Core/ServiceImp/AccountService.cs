﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;


namespace Accounting.Core.ServiceImp
{
    public class AccountService : BaseService<Account, Guid>, IAccountService
    {
        public AccountService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy các tài khoản con của một Account (các con cuối cùng)
        /// </summary>
        /// <param name="account">Tài khoản cha cần lấy (lọc theo account.AccountNumber)</param>
        /// <returns>Tập các con, cháu, chắt của nó, null nếu bị lỗi</returns>
        public List<Account> GetChildrenAccount(Account account)
        {
            return account == null ? null : Query.Where(k => k.AccountNumber.StartsWith(account.AccountNumber) && k.IsParentNode == false).ToList();
        }

        /// <summary>
        /// Lấy các tài khoản con của một Account (các con cuối cùng)
        /// </summary>
        /// <param name="strdsAccount">Danh sách tài khoản cha cần lấy. VD: "111,1112" (lọc theo account.AccountNumber)</param>
        /// <returns>Tập các con, cháu, chắt của nó, null nếu bị lỗi</returns>
        public List<Account> GetChildrenAccount(string strdsAccount)
        {
            List<Account> accounts = Query.Where(k => k.IsActive).ToList();
            List<Account> kq = new List<Account>();
            string[] filterAccounts = strdsAccount == null ? new string[0] : strdsAccount.Split(';');
            //duyệt các tài khoản cần lọc để thực hiện lọc dữ liệu
            foreach (string filterAccountitem in filterAccounts)
            {
                kq.AddRange(accounts.Where(k => k.AccountNumber.StartsWith(filterAccountitem) && k.IsParentNode == false));
            }
            return kq;
        }

        /// <summary>
        /// Lấy đối tượng Account từ giá trị string accountNumber
        /// </summary>
        /// <param name="accountNumber">string accountNumber cần lấy</param>
        /// <returns>đối tượng Account</returns>
        public Account GetByAccountNumber(string accountNumber)
        {
            return Query.SingleOrDefault(k => k.AccountNumber.Equals(accountNumber));
        }

        /// <summary>
        /// lấy danh sách các tài khoản Account từ chuỗi AcountNumber
        /// </summary>
        /// <param name="strdsAccount">chuỗi chứa các AccountNumber. VD: "111,331,3331"</param>
        /// <returns>tập các Account, null nếu bị lỗi</returns>
        public List<Account> GetByListAcountName(string strdsAccount)
        {
            List<string> list = strdsAccount.Split(';').ToList();
            return Query.Where(k => list.Contains(k.AccountNumber)).ToList();
        }
       

        public List<Account> GetByListAcountName(List<string> strdsAccount)
        {
            return Query.Where(k => strdsAccount.Contains(k.AccountNumber)).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Account> GetListOrderBy()
        {
            return Query.OrderBy(p => p.AccountNumber).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public List<Account> GetListChildren(Guid parentID)
        {
            return Query.Where(p => p.ParentID == parentID).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public int GetCountChildren(Guid parentID)
        {
            return Query.Count(p => p.ParentID == parentID);
        }


        public List<Account> GetListChildren(int grade)
        {
            return Query.Where(a => a.Grade == grade).ToList();
        }

        public List<Account> GetListChildrenStartWith(string start)
        {
            return Query.Where(p => p.AccountNumber.StartsWith(start)).ToList();
        }


        public List<string> GetFullListAccountNumber()
        {
            return Query.Select(p => p.AccountNumber).ToList();
        }

        public List<Account> GetListAccountIsActive(bool isActive)
        {
            List<Account> lstAccount = GetAll().Where(p => p.IsActive == isActive).OrderByDescending(p => p.AccountNumber).Reverse().ToList();
            return lstAccount;
        }

        /// <summary>
        /// Lấy ra danh sách Account theo AccountNumber và IsActive
        /// [haipt]
        /// </summary>
        /// <param name="listAccountNumber"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<Account> GetListAccountAccountNumberIsActive(string listAccountNumber, bool isActive)
        {
            List<Account> result = new List<Account>();
            string[] arrayAccount = new string[] { };
            if (!string.IsNullOrEmpty(listAccountNumber))
            {
                arrayAccount = listAccountNumber.Split(';');
            }
            foreach (var itemAccount in arrayAccount)
            {
                result.AddRange(Query.Where(a => a.AccountNumber.StartsWith(itemAccount) && a.IsActive == isActive));
            }
            return result;
        }

        /// <summary>
        /// Lấy ra danh sách Account theo AccountNumber và IsActive với lever theo type
        /// type 0 -> lấy lever 2 trở lên, type 1 -> lấy bỏ lever 1 nếu có lever > 2
        /// [hieudt]
        /// </summary>
        /// <param name="listAccountNumber"></param>
        /// <param name="isActive"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Account> GetListAccountAccountNumberIsActiveWithUpperLever(string listAccountNumber, bool isActive, int type)
        {
            List<Account> results = new List<Account>();
            string[] arrayAccount = new string[] { };
            if (!string.IsNullOrEmpty(listAccountNumber))
            {
                arrayAccount = listAccountNumber.Split(';');
            }
            foreach (var itemAccount in arrayAccount)
            {
                if (type == 0)
                    results.AddRange(Query.Where(a => a.AccountNumber.StartsWith(itemAccount) && a.IsActive == isActive && a.AccountNumber.Length > 3));
                else if (type == 1)
                {
                    results.AddRange(Query.Where(a => a.AccountNumber.StartsWith(itemAccount) && a.IsActive == isActive).OrderBy(a => a.AccountNumber));
                    for (int i = 1; i < results.Count; i++)
                    {
                        if (results[i].AccountNumber.Length > 3 && results[i-1].AccountNumber.Length == 3 
                            && results[i].AccountNumber.Substring(0, 3).Equals(results[i-1].AccountNumber))
                        {
                            results.RemoveAt(i - 1);
                            i--;
                        }
                    }
                }
                    
            }
            return results;
        }

        /// <summary>
        /// Lấy đầy đủ danh sách Account Active và order by theo AccountNumber
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns>list danh sách Account</returns>
        public List<Account> GetListActiveOrderBy(bool isActive)
        {
            return GetListOrderBy().Where(p => p.IsActive).ToList();
        }
        /// <summary>
        /// Lấy danh sách AccoutNumber từ Account theo IsActive= true
        /// [Longtx]
        /// </summary>
        /// <param name="IsActive">AccoutNumber</param>
        /// <returns></returns>
        public List<Account> GetAcountNumberBool(bool IsActive)
        {
            return Query.OrderBy(p => p.AccountNumber).Where(p => p.IsActive == IsActive).ToList();
        }

        public List<Account> GetCustomerAcctList(bool isActive, string detailType)
        {
            return Query.Where(a => a.IsActive == isActive && a.DetailType == detailType).ToList();
        }

        public List<Account> GetCustByDetailType(int detailType)
        {
            return Query.Where(a => a.DetailType == detailType.ToString()).ToList();
        }

        /// <summary>
        /// Lấy danh sách tài khoản theo loại đối tượng kế toán [khanhtq]
        /// edit by [DuyNT]
        /// </summary>
        /// <param name="detailType">
        /// 0=Nhà cung cấp,
        /// 1=Khách hàng,
        /// 2=Nhân viên,
        /// 3=Đối tượng tập hợp chi phí,
        /// 4=Hợp đồng,
        /// 5=Vật tư hàng hóa công cụ dụng cụ,
        /// 6=Tài khoản ngân hàng,
        /// 7=Khoản mục chi phí,
        /// 8=Chi tiết theo ngoại tệ,
        /// 9=Theo dõi phát sinh theo phòng ban,
        /// 10=Theo dõi phát sinh theo mục thu/chi</param>
        /// <param name="isActive">những khách hàng còn hoạt động</param>
        /// <returns>Danh sách tài khoản</returns>
        public List<Account> GetByDetailType(int detailType, bool isActive = true)
        {
            var lstAccount = new List<Account>();
            List<Account> accounts = Query.Where(c => c.IsActive == isActive).OrderBy(c => c.AccountNumber).ToList();
            foreach (var item in accounts)
            {
                try
                {
                    if (item.DetailType.Replace(" ", "").Length > 0)
                    {
                        List<string> lstDetailType = item.DetailType.Replace(" ", "").Split(';').ToList();
                        if (lstDetailType.Any(c => c == Convert.ToString(detailType))) lstAccount.Add(item);
                    }
                }
                catch (Exception)
                {
                }
               
            }
          return lstAccount;
        }
        public List<Account> GetByDetailType()
        {
            List<Account> accounts = Query.Where(c => c.IsActive == true && c.AccountNumber.StartsWith("131")).OrderBy(c => c.AccountNumber).ToList();
            return accounts;
        }
        /// <summary>
        /// Lấy đầy đủ danh sách Account Active các tài khoản có số dư và order by theo AccountNumber
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns>list danh sách Account</returns>
        public List<Account> GetListActiveKindOrderBy(bool isActive)
        {
            return GetListOrderBy().Where(p => p.IsActive /*&& p.AccountGroupKind != 3*/).ToList();
        }

        public List<Account> GetAccounts_ByDetailTypeFollowCurrency()
        {
            List<string> dsDieuKien = new List<string>() { "8" };
            return Query.Where(p => p.AccountGroupKind != 3 && (p.ParentID != null || p.IsParentNode == false) && p.IsActive).OrderBy(c => c.AccountNumber).ToList().Where(dc => dsDieuKien.Any(e => dc.DetailType.Contains(e))).ToList();
        }

        /// <summary>
        /// Lấy toàn bộ các tài khoản con (các con cuối cùng)
        /// </summary>
        /// <returns></returns>
        public List<Account> GetAllChildrenAccount()
        {
            List<Account> lstAccounts = Query.Where(c => c.IsActive).ToList();
            for (int i = lstAccounts.Count-1; i >= 0; i--)
            {
                if (lstAccounts.Any(c => c.ParentID == lstAccounts[i].ID) && lstAccounts[i].IsParentNode == true)
                {
                    lstAccounts.RemoveAt(i);
                }
            }
            return lstAccounts;
        }


        public List<AccountDefault_Account_Object> GetChildrenAccountWithCheck(string strdsAccount)
        {
            List<Account> accounts = GetAll();
            List<AccountDefault_Account_Object> lstkq = new List<AccountDefault_Account_Object>();
            
            string[] filterAccounts = strdsAccount == null ? new string[0] : strdsAccount.Split(';');
            Utils.Convert_FromParent_ToChild<Account, AccountDefault_Account_Object>(accounts, lstkq);
            foreach (AccountDefault_Account_Object item in lstkq)
            {
                foreach (string filterAccountitem in filterAccounts)
                {
                    if (item.AccountNumber == filterAccountitem)
                    {
                        item.Check = true;
                    }
                }
            }
            return lstkq.OrderBy(p => p.AccountNumber).ToList(); ;
        }
        /// <summary>
        /// Lâyd danh sách tài khoản cha
        /// </summary>
        /// <returns></returns>
        public List<string> getListParentsAccount()
        {
            List<Guid?> parentsID = Query.Select(o => o.ParentID).Distinct().ToList();
            return Query.Where(c => parentsID.Contains(c.ID)).Select(o => o.AccountNumber).ToList();            
        }
    }
}