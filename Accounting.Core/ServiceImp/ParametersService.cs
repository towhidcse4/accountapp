using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class ParametersService : BaseService<Parameters, Guid>, IParametersService
    {
        public ParametersService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public Parameters GetByCode(string code)
        {
            return Query.Where(c => c.Code == code && c.IsActive == true).FirstOrDefault();
        }
    }

}