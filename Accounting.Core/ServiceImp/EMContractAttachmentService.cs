﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMContractAttachmentService : BaseService<EMContractAttachment, Guid>, IEMContractAttachmentService
    {
        public EMContractAttachmentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<EMContractAttachment> GetEMContractAttachmentByContractID(Guid ID)
        {
            return Query.Where(x => x.EMContractID == ID).ToList();
        }

        public EMContractAttachment GetByID(Guid ID)
        {
            return Query.Where(x => x.ID == ID).SingleOrDefault();
        }
    }
}
