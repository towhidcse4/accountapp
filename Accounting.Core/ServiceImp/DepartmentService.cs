﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class DepartmentService : BaseService<Department, Guid>, IDepartmentService
    {
        public DepartmentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public ICostSetService _ICostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        public IExpenseItemService _IExpenseItemService { get { return IoC.Resolve<IExpenseItemService>(); } }

        public List<Department> GetListDepartmentParentID(Guid? parentID)
        {
            List<Department> lstChild = Query.Where(p => p.ParentID == parentID).ToList();
            return lstChild;
        }

        public List<Department> GetListDepartmentIsActive(bool isActive)
        {
            List<Department> lst = Query.Where(c => c.IsActive).ToList();
            return lst;
        }
        /// <summary>
        /// Lấy danh sách DepartmentCode từ Department theo IsActive = true
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<Department> GetListDepartmentBool(bool isActive)
        {
            return GetAll().OrderBy(p => p.DepartmentCode).Where(p => p.IsActive == isActive).ToList();
        }

        public List<Department> GetListDeparmentOrderDepartmentCode()
        {
            return GetAll().OrderByDescending(c => c.DepartmentCode).Reverse().ToList();
        }
        public Guid? GetGuidBydepartmentCode(string ma)
        {
            Guid? id = null;
            Department dp = Query.Where(p => p.DepartmentCode == ma).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ID;
            }
            return id;
        }
        public int CountListDepartmentParentID(Guid? parentID)
        {
            return GetListDepartmentParentID(parentID).Count;
        }

        public List<string> GetListOrderFixCodeParentID(Guid? parentID)
        {
            List<string> lstOrderFixCodeChild = Query.Where(a => a.ParentID == parentID).Select(a => a.OrderFixCode).ToList();
            return lstOrderFixCodeChild;
        }

        public List<Department> GetListDepartmentOrderFixCode(string orderFixCode)
        {
            List<Department> lstChildCheck = Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode)).ToList();
            return lstChildCheck;
        }

        public List<Department> GetListDepartmentGrade(int grade)
        {
            List<Department> listChildGradeNo1 = Query.Where(a => a.Grade == grade).ToList();
            return listChildGradeNo1;
        }

        public List<Department> GetListDepartmentOrderFixCodeNotParent(string orderFixCode, Guid? parentID)
        {
            List<Department> lstChild2 = Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode) && p.ID != parentID).OrderBy(p => p.OrderFixCode).ToList();
            return lstChild2;
        }

        public List<string> GetListDepartmentCode()
        {
            List<string> lst = Query.Select(c => c.DepartmentCode).ToList();
            return lst;
        }

        /// <summary>
        /// Lấy danh sách các Department theo IsActive được sắp xếp tăng dần theo DepartmentCode
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>tập các Department, null nếu bị lỗi</returns>
        public List<Department> GetAll_ByIsActive_OrderByDepartmentCode(bool IsActive)
        {
            return GetAll().Where(p => p.IsActive == IsActive).ToList().OrderBy(p => p.DepartmentCode).ToList();
        }

        /// <summary>
        /// Lấy danh sách các Department theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách Department được sắp xếp theo dạng cây cha con</returns>
        public List<Department> GetByActive_OrderByTreeIsParentNode(bool isActive)
        {
            List<Department> get = new List<Department>();
            List<Department> lst = Query.ToList();
            List<Department> parent = lst.Where(x => x.IsActive == isActive && x.ParentID == null).OrderBy(x => x.DepartmentCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                List<Department> lstChild = lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList();
                get.AddRange(lstChild);
            }
            return get;
        }

        /// <summary>
        /// Lấy danh sách các Department và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách Department được sắp xếp theo dạng cây cha con</returns>
        public List<Department> GetAll_OrderByTreeIsParentNode()
        {
            List<Department> get = new List<Department>();
            List<Department> lst = Query.ToList();
            List<Department> parent = lst.Where(x => x.ParentID == null).OrderBy(x => x.DepartmentCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                List<Department> lstChild = lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList();
                get.AddRange(lstChild);
            }
            return get;
        }

        public Guid? GetGuidDepartmentByCode(string code)
        {
            Guid? id = null;
            Department dp = Query.Where(p => p.DepartmentCode == code).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ID;
            }
            return id;
        }
        public string GetDepartmentCodeByGuid(Guid? guid)
        {
            string id = "";
            Department dp = Query.Where(p => p.ID == guid).SingleOrDefault();
            if (dp != null)
            {
                id = dp.DepartmentCode;
            }
            return id;
        }
        public List<Department> GetListParent()
        {
            return Query.Where(x => x.ParentID == null).ToList();
        }

        public List<Objects> GetObject()
        {
            List<Objects> objects = new List<Objects>();

            List<Department> lst = Query.Where(a => a.IsActive).ToList();
            List<Department> parent = lst.Where(x => x.ParentID == null).OrderBy(x => x.DepartmentCode).ToList();
            foreach (var item in parent)
            {
                objects.Add(new Objects
                {
                    ID = item.ID,
                    ObjectCode = item.DepartmentCode,
                    ObjectName = item.DepartmentName,
                    ParentID = item.ParentID,
                    IsParentNode = item.IsParentNode,
                    ObjectType = 1,
                    ObjectTypeName = "Phòng ban",
                    CostAccount = item.CostAccount
                });
                List<Objects> lstChild = lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).Select(a => new Objects
                {
                    ID = a.ID,
                    ObjectCode = a.DepartmentCode,
                    ObjectName = a.DepartmentName,
                    ParentID = a.ParentID,
                    IsParentNode = a.IsParentNode,
                    ObjectType = 1,
                    ObjectTypeName = "Phòng ban",
                    CostAccount = a.CostAccount
                    
                }).ToList();
                objects.AddRange(lstChild);
            }

            //List<Objects> departments = Query.Where(a => a.IsActive).Select( a => new Objects
            //{
            //    ID = a.ID,
            //    ObjectCode = a.DepartmentCode,
            //    ObjectName = a.DepartmentName,
            //    ParentID = a.ParentID,
            //    IsParentNode = a.IsParentNode,
            //    ObjectType = 1,
            //    ObjectTypeName = "Phòng ban"
            //}).ToList();

            List<Objects> costSets = _ICostSetService.Query.Where(a => a.IsActive).Select(a => new Objects
            {
                ID = a.ID,
                ObjectCode = a.CostSetCode,
                ObjectName = a.CostSetName,
                ParentID = a.ParentID,
                IsParentNode = a.IsParentNode,
                ObjectType = 0,
                ObjectTypeName = "ĐT tập hợp CP"
            }).ToList();

            //List<Objects> ExpenseItems = _IExpenseItemService.Query.Where(a => a.IsActive).Select(a => new Objects
            //{
            //    ID = a.ID,
            //    ObjectCode = a.ExpenseItemCode,
            //    ObjectName = a.ExpenseItemName,
            //    ParentID = a.ParentID,
            //    IsParentNode = a.IsParentNode,
            //    ObjectType = 2
            //}).ToList();

            //objects.AddRange(departments);
            objects.AddRange(costSets);
            //objects.AddRange(ExpenseItems);
            return objects;
        }
    }

}