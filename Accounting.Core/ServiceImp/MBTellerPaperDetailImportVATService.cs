using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBTellerPaperDetailImportVATService: BaseService<MBTellerPaperDetailImportVAT ,Guid>,IMBTellerPaperDetailImportVATService
    {
        public MBTellerPaperDetailImportVATService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}