﻿using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Collections.Generic;
using System.Linq;
using FX.Core;
using System.Linq.Dynamic;
using System.Reflection;
using System.Windows.Forms;

namespace Accounting.Core.ServiceImp
{
    public class PSSalarySheetDetailService : BaseService<PSSalarySheetDetail, Guid>, IPSSalarySheetDetailService
    {
        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IPSSalaryTaxInsuranceRegulationService _IPSSalaryTaxInsuranceRegulationService { get { return IoC.Resolve<IPSSalaryTaxInsuranceRegulationService>(); } }
        public IPersonalSalaryTaxService _IPersonalSalaryTaxService { get { return IoC.Resolve<IPersonalSalaryTaxService>(); } }
        public IPSSalarySheetService _IPSSalarySheetService { get { return IoC.Resolve<IPSSalarySheetService>(); } }
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public ITimeSheetSymbolsService _ITimeSheetSymbolsService { get { return IoC.Resolve<ITimeSheetSymbolsService>(); } }
        public IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }


        public PSSalarySheetDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public IList<PSSalarySheetDetail> FindEmployeeByDepartment(List<Guid> departments, DateTime date, int typeid, int type, decimal WorkDayInMonth, List<PSTimeSheetDetail> psts = null, List<PSTimeSheetSummaryDetail> pstss = null)
        {
            DateTime now = new DateTime(date.Year, date.Month, date.Day);
            PSSalaryTaxInsuranceRegulation stir = _IPSSalaryTaxInsuranceRegulationService.FindByDate(date);
            List<PersonalSalaryTax> pst = _IPersonalSalaryTaxService.GetAll();
            List<TimeSheetSymbols> lstsymbols = _ITimeSheetSymbolsService.GetAll();
            if (stir == null) stir = new PSSalaryTaxInsuranceRegulation();
            List<PSSalarySheetDetail> lst = new List<PSSalarySheetDetail>();
            bool isHsl = _ISystemOptionService.Query.Where(x => x.Code == "TL_TheoHeSoLuong").First().Data == "1";
            //int LamTron = int.Parse(_ISystemOptionService.Query.Where(x => x.Code == "DDSo_TienVND").First().Data);
            int LamTron = int.Parse(_ISystemOptionService.Query.Where(x => x.Code == "DDSo_DonGia").First().Data); 
            int LamTron_SotienHuong = int.Parse(_ISystemOptionService.Query.Where(x => x.Code == "DDSo_TienVND").First().Data); 
            if (psts != null || pstss != null)
            {
                //List<Guid?> gpsts = psts != null ? psts.Select(x => x.EmployeeID).ToList() : pstss != null ? pstss.Select(x => x.EmployeeID).ToList() : new List<Guid?>();
                //lst = _IAccountingObjectService.Query.Where(x => x.IsActive && x.IsEmployee && gpsts.Contains(x.ID))
                //.Select(x => new PSSalarySheetDetail
                //{
                //    EmployeeID = x.ID,
                //    AccountingObjectName = x.AccountingObjectName,
                //    AccountingObjectTitle = x.ContactTitle,
                //    DepartmentID = x.DepartmentID,
                //    AgreementSalary = !isHsl ? x.AgreementSalary ?? 0 : 0,
                //    SalaryCoefficient = isHsl ? x.SalaryCoefficient ?? 0 : 0,
                //    BasicWage = isHsl ? stir.BasicWage : 0,
                //    TotalAmount = x.AgreementSalary ?? (x.SalaryCoefficient ?? 0) * stir.BasicWage,
                //    InsuranceSalary = x.InsuranceSalary ?? 0,
                //    NumberOfDependent = x.NumberOfDependent
                //}).ToList();

                List<Guid?> gpsts = psts != null ? psts.Select(x => x.EmployeeID).ToList() : pstss != null ? pstss.Select(x => x.EmployeeID).ToList() : new List<Guid?>();

                lst = _IAccountingObjectService.Query.Join(_IDepartmentService.Query, n => n.DepartmentID, m => m.ID, (n, m) => new PSSalarySheetDetail
                {
                    EmployeeID = n.ID,
                    AccountingObjectName = n.AccountingObjectName,
                    AccountingObjectTitle = n.ContactTitle,
                    DepartmentID = n.DepartmentID,
                    AgreementSalary = n.AgreementSalary ?? Math.Round((n.SalaryCoefficient ?? 0) * stir.BasicWage, LamTron, MidpointRounding.AwayFromZero),
                    SalaryCoefficient = n.SalaryCoefficient ?? 0,
                    //BasicWage = stir.BasicWage,
                    BasicWageAmount = ((n.SalaryCoefficient ?? 0) /** stir.BasicWage*/),
                    TotalAmount = !isHsl ? (n.AgreementSalary ?? 0) : (n.SalaryCoefficient ?? 0),
                    InsuranceSalary = n.InsuranceSalary ?? 0,
                    NumberOfDependent = n.NumberOfDependent,
                    DepartmentCode = m.DepartmentCode,
                    accountingObjectCode = n.AccountingObjectCode,
                    IsActive = n.IsActive,
                    IsEmployee = n.IsEmployee,
                    IsUnofficialStaff = n.IsUnofficialStaff
                }).Where(x => x.IsActive && x.IsEmployee && gpsts.Contains(x.EmployeeID)).OrderBy(n => n.DepartmentCode).ThenBy(n => n.accountingObjectCode).ToList();
            }
            else
            {
                lst = _IAccountingObjectService.Query.Join(_IDepartmentService.Query, n => n.DepartmentID, m => m.ID, (n, m) => new PSSalarySheetDetail
                {
                    EmployeeID = n.ID,
                    AccountingObjectName = n.AccountingObjectName,
                    AccountingObjectTitle = n.ContactTitle,
                    DepartmentID = n.DepartmentID,
                    AgreementSalary = n.AgreementSalary ?? Math.Round((n.SalaryCoefficient ?? 0) * stir.BasicWage, LamTron, MidpointRounding.AwayFromZero),
                    SalaryCoefficient = n.SalaryCoefficient ?? 0,
                    //BasicWage = stir.BasicWage,
                    BasicWageAmount = ((n.SalaryCoefficient ?? 0) /** stir.BasicWage*/),
                    TotalAmount = !isHsl ? (n.AgreementSalary ?? 0) : (n.SalaryCoefficient ?? 0),
                    InsuranceSalary = n.InsuranceSalary ?? 0,
                    NumberOfDependent = n.NumberOfDependent,
                    DepartmentCode = m.DepartmentCode,
                    accountingObjectCode = n.AccountingObjectCode,
                    IsActive = n.IsActive,
                    IsEmployee = n.IsEmployee,
                    IsUnofficialStaff =  n.IsUnofficialStaff
                }).Where(x => x.IsActive && x.IsEmployee && departments.Contains(x.DepartmentID ?? Guid.Empty)).OrderBy(n => n.DepartmentCode).ThenBy(n => n.accountingObjectCode).ToList();
            }
            int order = 1;
            foreach (PSSalarySheetDetail detail in lst)
            {
                detail.BasicWage = stir.BasicWage;
                detail.BasicWageAmount = Math.Round((detail.BasicWageAmount * stir.BasicWage), LamTron, MidpointRounding.AwayFromZero);
                detail.TotalAmount = !isHsl ? detail.TotalAmount : Math.Round((detail.TotalAmount * stir.BasicWage), LamTron, MidpointRounding.AwayFromZero);
                // lấy dữ liệu theo các bảng khác
                if (typeid == 830 || typeid == 831)
                {
                    // Tính số ngày làm việc trong tháng 
                    //int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                    //#region code cũ Tính số ngày làm trong tháng
                    //decimal numberOfWorkingDay = 0;
                    //for (int i = 1; i <= daysInMonth; i++)
                    //{
                    //    DateTime dt = new DateTime(date.Year, date.Month, i);
                    //    if (dt.DayOfWeek == DayOfWeek.Saturday)
                    //    {
                    //        if (stir.IsWorkingOnSaturday)
                    //        {
                    //            numberOfWorkingDay += (decimal)0.5;
                    //        }
                    //        if (stir.IsWorkingOnSaturdayNoon)
                    //        {
                    //            numberOfWorkingDay += (decimal)0.5;
                    //        }
                    //    }
                    //    else if (dt.DayOfWeek == DayOfWeek.Sunday)
                    //    {
                    //        if (stir.IsWorkingOnSunday)
                    //        {
                    //            numberOfWorkingDay += (decimal)0.5;
                    //        }
                    //        if (stir.IsWorkingOnSundayNoon)
                    //        {
                    //            numberOfWorkingDay += (decimal)0.5;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        numberOfWorkingDay += 1;
                    //    }
                    //}
                    //#endregion
                    #region Code mới Tính số ngày làm trong tháng
                    decimal numberOfWorkingDay = WorkDayInMonth;
                    //if (stir.WorkDayInMonth != null)
                    //{
                    //    numberOfWorkingDay = stir.WorkDayInMonth;
                    //}                    
                    #endregion
                    detail.NumberOfPaidWorkingDayTimeSheet = numberOfWorkingDay;
                    detail.WorkingDayUnitPrice = Math.Round((typeid == 830 ? (!isHsl ? detail.AgreementSalary : detail.SalaryCoefficient * detail.BasicWage) / (numberOfWorkingDay == 0 ? 1 : numberOfWorkingDay) : 0), LamTron, MidpointRounding.AwayFromZero);
                    detail.WorkingHourUnitPrice = Math.Round(((!isHsl ? detail.AgreementSalary : detail.SalaryCoefficient * detail.BasicWage) / (stir.WorkingHoursInDay == 0 ? 1 : stir.WorkingHoursInDay * numberOfWorkingDay)), LamTron, MidpointRounding.AwayFromZero);
                    if (psts != null)
                    {
                        PSTimeSheetDetail tsDetail = psts.Where(x => x.EmployeeID == detail.EmployeeID).FirstOrDefault();
                        if (tsDetail != null)
                        {
                            detail.NumberOfPaidWorkingDayTimeSheet = tsDetail.PaidWorkingDay;
                            detail.PaidWorkingDayAmount = Math.Round(((typeid == 830 ? detail.WorkingDayUnitPrice : detail.WorkingHourUnitPrice) * detail.NumberOfPaidWorkingDayTimeSheet), LamTron_SotienHuong, MidpointRounding.AwayFromZero);
                            detail.NumberOfNonWorkingDayTimeSheet = tsDetail.PaidNonWorkingDay;
                            PropertyInfo[] propertyInfo = tsDetail.GetType().GetProperties();
                            foreach (PropertyInfo propItem in propertyInfo)
                            {
                                if (propItem.CanRead && propItem.Name.StartsWith("Day"))
                                {
                                    string str = (string)propItem.GetValue(tsDetail) ?? "";
                                    string[] list = str.Split(';');
                                    for (int i = 0; i < list.Length; i++)
                                    {
                                        var model = lstsymbols.FirstOrDefault(x => x.TimeSheetSymbolsCode == list[i].TrimStart());
                                        if (model != null && model.SalaryRate != 100)
                                            detail.NonWorkingDayAmount = detail.NonWorkingDayAmount + Math.Round((((typeid == 830 ? detail.WorkingDayUnitPrice : detail.WorkingHourUnitPrice) * model.SalaryRate ?? 0) / 100), LamTron_SotienHuong, MidpointRounding.AwayFromZero);
                                    }

                                }
                            }
                            detail.TotalOverTime = tsDetail.TotalOverTime;

                            //tính tiền làm thêm
                            if (typeid == 830)
                            {
                                decimal day = Math.Round((tsDetail.WorkingDay * stir.OvertimeDailyPercent / 100 * detail.WorkingDayUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal weekend = Math.Round((tsDetail.WeekendDay * stir.OvertimeWeekendPercent / 100 * detail.WorkingDayUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal holiday = Math.Round((tsDetail.Holiday * stir.OvertimeHolidayPercent / 100 * detail.WorkingDayUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal night = Math.Round((tsDetail.WorkingDayNight * stir.OvertimeWorkingDayNightPercent / 100 * detail.WorkingDayUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal weekendNight = Math.Round((tsDetail.WeekendDayNight * stir.OvertimeWeekendDayNightPercent / 100 * detail.WorkingDayUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal holidayNight = Math.Round((tsDetail.HolidayNight * stir.OvertimeHolidayNightPercent / 100 * detail.WorkingDayUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                detail.OverTimeAmount = day + weekend + holiday + night + weekendNight + holidayNight;
                                detail.TotalAmount = detail.PaidWorkingDayAmount + detail.OverTimeAmount;
                            }
                            else if (typeid == 831)
                            {
                                decimal day = Math.Round((tsDetail.WorkingDay * stir.OvertimeDailyPercent / 100 * detail.WorkingHourUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal weekend = Math.Round((tsDetail.WeekendDay * stir.OvertimeWeekendPercent / 100 * detail.WorkingHourUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal holiday = Math.Round((tsDetail.Holiday * stir.OvertimeHolidayPercent / 100 * detail.WorkingHourUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal night = Math.Round((tsDetail.WorkingDayNight * stir.OvertimeWorkingDayNightPercent / 100 * detail.WorkingHourUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal weekendNight = Math.Round((tsDetail.WeekendDayNight * stir.OvertimeWeekendDayNightPercent / 100 * detail.WorkingHourUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                decimal holidayNight = Math.Round((tsDetail.HolidayNight * stir.OvertimeHolidayNightPercent / 100 * detail.WorkingHourUnitPrice), LamTron, MidpointRounding.AwayFromZero);
                                detail.OverTimeAmount = day + weekend + holiday + night + weekendNight + holidayNight;
                                detail.TotalAmount = detail.PaidWorkingDayAmount + detail.OverTimeAmount + detail.NonWorkingDayAmount;
                            }

                        }
                    }
                    else if (pstss != null)
                    {
                        PSTimeSheetSummaryDetail tsDetail = pstss.Where(x => x.EmployeeID == detail.EmployeeID).FirstOrDefault();
                        if (tsDetail != null)
                        {
                            detail.NumberOfPaidWorkingDayTimeSheet = tsDetail.WorkAllDay + tsDetail.WorkHalfADay;
                            detail.PaidWorkingDayAmount = Math.Round(((typeid == 830 ? detail.WorkingDayUnitPrice : detail.WorkingHourUnitPrice) * detail.NumberOfPaidWorkingDayTimeSheet), LamTron_SotienHuong, MidpointRounding.AwayFromZero);
                            detail.TotalOverTime = tsDetail.TotalOverTime;
                            detail.TotalAmount = detail.PaidWorkingDayAmount + detail.OverTimeAmount + detail.NonWorkingDayAmount;
                        }
                    }
                }
                detail.OrderPriority = order++;
                // tính tiền đóng bh của nv
                if (detail.IsUnofficialStaff ?? false)
                {
                    detail.EmployeeSocityInsuranceAmount = 0;
                    detail.EmployeeMedicalInsuranceAmount = 0;
                    detail.EmployeeUnEmployeeInsuranceAmount = 0;
                    detail.EmployeeAccidentInsuranceAmount = 0;
                    detail.EmployeeTradeUnionInsuranceAmount = 0;
                    detail.TotalPersonalTaxIncomeAmount = detail.IncomeForTaxCalcuation = detail.TotalAmount;
                    detail.ReduceSelfTaxAmount = 0;
                    detail.ReduceDependTaxAmount = 0;
                                        
                    if (type == 0) detail.TotalAmount = 0;
                    // tính tiền thuế theo từng khoảng                   
                    detail.IncomeTaxAmount = detail.IncomeForTaxCalcuation * 10 / 100;

                    // các khoản khấu trừ bao gồm tiền thuế và tiền đóng bh của nv
                    detail.SumOfDeductionAmount = detail.EmployeeSocityInsuranceAmount
                                                                                        + detail.EmployeeMedicalInsuranceAmount
                                                                                        + detail.EmployeeUnEmployeeInsuranceAmount
                                                                                        + detail.EmployeeAccidentInsuranceAmount
                                                                                        + detail.EmployeeTradeUnionInsuranceAmount
                                                                                        + detail.IncomeTaxAmount;

                    // tiền thực nhận
                    detail.NetAmount = detail.NetAmountOriginal = detail.TotalPersonalTaxIncomeAmount - detail.SumOfDeductionAmount - detail.TemporaryAmount;

                    // Tính tiền bảo hiểm công ty đóng
                    detail.CompanySocityInsuranceAmount = 0;
                    detail.CompanyMedicalInsuranceAmount = 0;
                    detail.CompanyUnEmployeeInsuranceAmount = 0;
                    detail.CompanytAccidentInsuranceAmount = 0;
                    detail.CompanyTradeUnionInsuranceAmount = 0;
                }
                else
                {
                    detail.EmployeeSocityInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.EmployeeSocityInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.EmployeeMedicalInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.EmployeeMedicalInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.EmployeeUnEmployeeInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.EmployeeUnEmployeeInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.EmployeeAccidentInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.EmployeeAccidentInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.EmployeeTradeUnionInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.EmployeeTradeUnionInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.TotalPersonalTaxIncomeAmount = detail.TotalAmount;
                    detail.ReduceSelfTaxAmount = stir.ReduceSelfTaxAmount;
                    detail.ReduceDependTaxAmount = Math.Round((detail.NumberOfDependent * stir.ReduceDependTaxAmount), LamTron, MidpointRounding.AwayFromZero);

                    // số tiền chịu thuế = tổng tiền - các khoản đóng bh và tiền giảm trừ gia cảnh
                    detail.IncomeForTaxCalcuation = detail.TotalPersonalTaxIncomeAmount - detail.EmployeeSocityInsuranceAmount
                                                                                        - detail.EmployeeMedicalInsuranceAmount
                                                                                        - detail.EmployeeUnEmployeeInsuranceAmount
                                                                                        - detail.EmployeeAccidentInsuranceAmount
                                                                                        - detail.EmployeeTradeUnionInsuranceAmount
                                                                                        - detail.ReduceSelfTaxAmount
                                                                                        - detail.ReduceDependTaxAmount;
                    if (detail.IncomeForTaxCalcuation < 0) detail.IncomeForTaxCalcuation = 0;
                    List<PersonalSalaryTax> taxs = pst.Where(x => x.FromAmount <= detail.IncomeForTaxCalcuation && x.SalaryType == 0).OrderBy(x => x.ToAmount).ToList();
                    if (type == 0) detail.TotalAmount = 0;
                    // tính tiền thuế theo từng khoảng
                    if (taxs != null && taxs.Count > 0)
                    {
                        for (int i = 0; i < taxs.Count; i++)
                        {
                            if (taxs[i].ToAmount <= detail.IncomeForTaxCalcuation)
                            {
                                if (i != 0)
                                    detail.IncomeTaxAmount += Math.Round(((taxs[i].ToAmount - taxs[i - 1].ToAmount) * taxs[i].TaxRate / 100), LamTron, MidpointRounding.AwayFromZero);
                                else
                                    detail.IncomeTaxAmount += Math.Round((taxs[i].ToAmount * taxs[i].TaxRate / 100), LamTron, MidpointRounding.AwayFromZero);
                    }
                            else if (i != 0)
                            {
                                detail.IncomeTaxAmount += Math.Round(((detail.IncomeForTaxCalcuation - taxs[i - 1].ToAmount) * taxs[i].TaxRate / 100), LamTron, MidpointRounding.AwayFromZero);
                    }
                            else
                                detail.IncomeTaxAmount += Math.Round((detail.IncomeForTaxCalcuation * taxs[i].TaxRate / 100), LamTron, MidpointRounding.AwayFromZero);
                }
                    }
                    // các khoản khấu trừ bao gồm tiền thuế và tiền đóng bh của nv
                    detail.SumOfDeductionAmount = detail.EmployeeSocityInsuranceAmount
                                                                                        + detail.EmployeeMedicalInsuranceAmount
                                                                                        + detail.EmployeeUnEmployeeInsuranceAmount
                                                                                        + detail.EmployeeAccidentInsuranceAmount
                                                                                        + detail.EmployeeTradeUnionInsuranceAmount
                                                                                        + detail.IncomeTaxAmount;

                    // tiền thực nhận
                    detail.NetAmount = detail.NetAmountOriginal = detail.TotalPersonalTaxIncomeAmount - detail.SumOfDeductionAmount - detail.TemporaryAmount;

                    // Tính tiền bảo hiểm công ty đóng
                    detail.CompanySocityInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.CompanySocityInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.CompanyMedicalInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.CompanyMedicalInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.CompanyUnEmployeeInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.CompanyUnEmployeeInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.CompanytAccidentInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.CompanytAccidentInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                    detail.CompanyTradeUnionInsuranceAmount = Math.Round((detail.InsuranceSalary * stir.CompanyTradeUnionInsurancePercent / 100), LamTron, MidpointRounding.AwayFromZero);
                }
                
            }
            return lst;
        }
    }

}