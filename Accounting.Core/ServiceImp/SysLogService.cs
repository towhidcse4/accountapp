using System;
using System.Data.SqlClient;
using System.Linq.Dynamic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class SysLogService : BaseService<SysLog, Guid>, ISysLogService
    {
        public SysLogService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}