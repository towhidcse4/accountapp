using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TMAppendixListService: BaseService<TMAppendixList ,Guid>,ITMAppendixListService
    {
        public TMAppendixListService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public List<TMAppendixList> GetAppendixListByTypeGroup(int typeGroup)
        {
            return Query.Where(n => n.TypeGroup == typeGroup).ToList();
        }
    }

}