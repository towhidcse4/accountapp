using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class SystemOptionService: BaseService<SystemOption ,int>,ISystemOptionService
    {
        public SystemOptionService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public IBankAccountDetailService _IMBInternalTransferService
        {
            get { return IoC.Resolve<IBankAccountDetailService>(); }
        }
        
        public SystemOption GetByCode(string code)
        {
            return Query.FirstOrDefault(p => p.Code.Equals(code));
        }
    }

}