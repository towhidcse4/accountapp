﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMShareHolderTransactionService : BaseService<EMShareHolderTransaction, Guid>, IEMShareHolderTransactionService
    {
        public EMShareHolderTransactionService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<EMShareHolderTransaction> GetListEMShareHolderTransactionEMShareHolder(Guid? emShareHolderID)
        {
            List<EMShareHolderTransaction> listGenTemp = Query.Where(p => p.EMShareHolderID == emShareHolderID).ToList();
            return listGenTemp;
        }
    }
}
