using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class SAQuoteDetailService: BaseService<SAQuoteDetail ,Guid>,ISAQuoteDetailService
    {
        public SAQuoteDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}