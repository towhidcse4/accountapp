﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{

    public class FADepreciationDetailService : BaseService<FADepreciationDetail, Guid>, IFADepreciationDetailService
    {
        public FADepreciationDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        IFixedAssetService _IFixedAssetService = FX.Core.IoC.Resolve<IFixedAssetService>();
        public IFAAdjustmentService _IFAAdjustmentService { get { return FX.Core.IoC.Resolve<IFAAdjustmentService>(); } }
        public List<FADepreciationDetail> GetFADepreciationDetails(Guid faDepreciationId)
        {
            return Query.Where(k => k.FADepreciationID == faDepreciationId).ToList();
        }

        public List<FADepreciationDetail> FindByFixedAssetID(Guid? fixedAssetID)
        {
            return Query.Where(p => p.FixedAssetID == fixedAssetID).ToList();
        }
    }
}
