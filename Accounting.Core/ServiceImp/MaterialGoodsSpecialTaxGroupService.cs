﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MaterialGoodsSpecialTaxGroupService : BaseService<MaterialGoodsSpecialTaxGroup, Guid>, IMaterialGoodsSpecialTaxGroupService
    {
        public MaterialGoodsSpecialTaxGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theo IsActive
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>Tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        public List<MaterialGoodsSpecialTaxGroup> GetAll_ByIsActive(bool IsActive)
        {
            return Query.Where(p => p.IsActive == true).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID"> là ParentID truyền vào</param>
        /// <returns>tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        public List<MaterialGoodsSpecialTaxGroup> GetAll_ByParentID(Guid ParentID)
        {
            return Query.Where(p => p.ParentID == ParentID).ToList();
        }

        /// <summary>
        /// Số dòng theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>số dòng theo ParentID, null nếu lỗi</returns>
        public int CountByParentID(Guid? ParentID)
        {
            return Query.Count(p => p.ParentID == ParentID);
        }

        /// <summary>
        /// Lấy ra danh sách OrderFixCod theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các OrderFixCod, null nếu bị lỗi</returns>
        public List<string> GetOrderFixCode_ByParentID(Guid? ParentID)
        {
            return Query.Where(a => a.ParentID == ParentID).Select(a => a.OrderFixCode).ToList();
        }

        /// <summary>
        /// Lấy ra MaterialGoodsSpecialTaxGroup theo OrderFixCode (các con cuối cùng)
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode"> là OrderFixCode truyền vào</param>
        /// <returns>tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        public List<MaterialGoodsSpecialTaxGroup> GetStartsWith_ByOrderFixCode(string OrderFixCode)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(OrderFixCode)).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theoGrade
        /// [DUYTN], sửa ngày 07/04/2014
        /// </summary>
        /// <param name="Grade"> là tham số bậc của Grade</param>
        /// <returns>tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        public List<MaterialGoodsSpecialTaxGroup> GetAll_ByGrade(int Grade)
        {
            return Query.Where(a => a.Grade == Grade).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các MaterialGoodsSpecialTaxGroupCode, null nếu bị lỗi</returns>
        public List<string> GetMaterialGoodsSpecialTaxGroupCode()
        {
            return Query.Select(p => p.MaterialGoodsSpecialTaxGroupCode).ToList();
        }
        public string GetMaterialGoodsSpecialTaxGroupCodeByGuid(Guid? guid)
        {
            return Query.Where(p => p.ID == guid).FirstOrDefault().MaterialGoodsSpecialTaxGroupCode;
        }
        public List<MaterialGoodsSpecialTaxGroup> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.MaterialGoodsSpecialTaxGroupCode).ToList();
        }
        public Guid GetGuidMaterialGoodsSpecialTaxGroupByCode(string code)
        {
            Guid id = Guid.Empty;
            MaterialGoodsSpecialTaxGroup ag = Query.Where(p => p.MaterialGoodsSpecialTaxGroupCode == code).SingleOrDefault();
            if (ag != null)
            {
                id = ag.ID;
            }
            return id;
        }
    }

}