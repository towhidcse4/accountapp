﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class BankAccountDetailService : BaseService<BankAccountDetail, Guid>, IBankAccountDetailService
    {
        public BankAccountDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public BankAccountDetail GetById(Guid ID)
        {
            try
            {
                return Query.Where(b => b.ID == ID).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public List<string> GetListBankAccount()
        {
            List<string> listBankAccount = Query.Select(a => a.BankAccount).ToList();
            return listBankAccount;
        }

        public List<BankAccountDetail> GetIsActive(bool isActive)
        {
            return Query.Where(c => c.IsActive == isActive).OrderBy(c => c.BankAccount).ToList();
        }

        public List<BankAccountDetail> GetAllOrderBy()
        {
            return Query.OrderBy(p => p.BankAccount).ToList();
        }

        public Guid? GetGuildFromBankAccount(string bankAccount)
        {
            BankAccountDetail tempBank = Query.FirstOrDefault(p => p.BankAccount == bankAccount);
            if (tempBank != null)
            {
                return tempBank.ID;
            }
            return Guid.Empty;
        }
    }

}