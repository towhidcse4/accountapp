﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class TypeService : BaseService<Accounting.Core.Domain.Type, int>, ITypeService
    {
        public TypeService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// Lấy danh sách các ID của loại chứng từ [khanhtq]
        /// </summary>
        /// <returns>Danh sách ID của loại chứng từ</returns>
        public List<int> GetListId()
        {
            return Query.Select(p => p.ID).ToList();
        }
        public List<Accounting.Core.Domain.Type> GetByListId(List<int> lstId)
        {
            return Query.Where(c => lstId.Contains(c.ID)).ToList();
        }
        public List<Accounting.Core.Domain.Type> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.ID).ToList();
        }
    }

}