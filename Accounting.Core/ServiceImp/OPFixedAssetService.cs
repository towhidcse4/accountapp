using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class OPFixedAssetService: BaseService<OPFixedAsset ,Guid>,IOPFixedAssetService
    {
        public OPFixedAssetService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public OPFixedAsset FindByFixedAssetID(Guid iD)
        {
            var lst = Query.Where(fai => fai.FixedAssetID == iD).ToList();
            return lst.Count > 0 ? lst[0] : null;
        }

        public int FindLastOrderPriority()
        {
            List<OPFixedAsset> lst = Query.ToList();
            return lst.Count > 0 ? lst.Max(opfa => opfa.OrderPriority) : 0;
        }
    }

}