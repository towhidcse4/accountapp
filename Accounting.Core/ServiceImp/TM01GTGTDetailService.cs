using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;  

namespace Accounting.Core.ServiceImp
{
    public class TM01GTGTDetailService: BaseService<TM01GTGTDetail ,Guid>,ITM01GTGTDetailService
    {
        public TM01GTGTDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public string GetDataItem21(string ItemCode)
        {
            return Query.Where(n => n.Code == ItemCode).ToString();
        }

    }

}