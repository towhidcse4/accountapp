﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
namespace Accounting.Core.ServiceImp
{
    public class SaleDiscountPolicyService : BaseService<SaleDiscountPolicy, Guid>, ISaleDiscountPolicyService
    {
        public SaleDiscountPolicyService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy danh sách SaleDiscountPolicy theo MaterialGoodsID
        /// [DUYTN]
        /// </summary>
        /// <param name="MaterialGoodsID"> là MaterialGoodsID cần truyền vào</param>
        /// <returns>tập các SaleDiscountPolicy, null nếu bị lỗi</returns>
        public List<SaleDiscountPolicy> GetAll_ByMaterialGoodsID(Guid MaterialGoodsID)
        {
            return Query.Where(k => k.MaterialGoodsID == MaterialGoodsID).ToList();
        }
    }
}
