using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class FAInitService: BaseService<FAInit ,Guid>,IFAInitService
    {
        IFADepreciationDetailService _IFADepreciationDetailService = FX.Core.IoC.Resolve<IFADepreciationDetailService>();
        IFATransferDetailService _IFATransferDetailService = FX.Core.IoC.Resolve<IFATransferDetailService>();
        IFADecrementDetailService _IFADecrementDetailService = FX.Core.IoC.Resolve<IFADecrementDetailService>();
        IFAAdjustmentService _IFAAdjustmentService = FX.Core.IoC.Resolve<IFAAdjustmentService>();

        public FAInitService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public bool CheckDeleteFAInit(Guid fixedassetid)
        {
            if (_IFADepreciationDetailService.Query.Any(fad => fad.FixedAssetID == fixedassetid)) return true;
            if (_IFATransferDetailService.Query.Any(fad => fad.FixedAssetID == fixedassetid)) return true;
            if (_IFADecrementDetailService.Query.Any(fad => fad.FixedAssetID == fixedassetid)) return true;
            if (_IFAAdjustmentService.Query.Any(fad => fad.FixedAssetID == fixedassetid)) return true;
            return false;
        }

        public FAInit FindByFixedAssetID(Guid iD)
        {
            var lst = Query.Where(fai => fai.FixedAssetID == iD).ToList();
            return lst.Count > 0 ? lst[0] : null;
        }
    }

}