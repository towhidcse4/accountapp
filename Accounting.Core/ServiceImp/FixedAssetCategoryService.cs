﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using Accounting.Core.Domain;
using FX.Data;
namespace Accounting.Core.ServiceImp
{
    public class FixedAssetCategoryService : BaseService<FixedAssetCategory, Guid>, IFixedAssetCategoryService
    {
        public FixedAssetCategoryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<FixedAssetCategory> GetListFixedAssetCategoryIsActive(bool isActive)
        {
            List<FixedAssetCategory> list = Query.Where(c => c.IsActive == isActive).ToList();
            return list;
        }

        public List<FixedAssetCategory> GetListFixedAssetCategoryParentID(Guid? parentID)
        {
            List<FixedAssetCategory> lstChild = Query.Where(p => p.ParentID == parentID).ToList();
            return lstChild;
        }

        public List<string> GetListOrderFixCodeFixedAssetCategory(Guid? parentID)
        {
            List<string> lstOrderFixCodeChild = Query.Where(a => a.ParentID == parentID)
                        .Select(a => a.OrderFixCode).ToList();
            return lstOrderFixCodeChild;
        }

        public List<FixedAssetCategory> GetListFixedAssetCategoryOrderFixCode(string orderFixCode)
        {
            List<FixedAssetCategory> CheckChirent = Query.Where(c => c.OrderFixCode.StartsWith(orderFixCode)).ToList();
            return CheckChirent;
        }

        public List<FixedAssetCategory> GetListFixedAssetCategoryGrade(int grade)
        {
            List<FixedAssetCategory> listChildGradeNo1 = Query.Where(a => a.Grade == grade).ToList();
            return listChildGradeNo1;
        }

        public int CountListFixedAssetCategoryParentID(Guid? parentID)
        {
            return GetListFixedAssetCategoryParentID(parentID).Count;
        }

        public List<FixedAssetCategory> GetOrderby()
        {
            return Query.OrderBy(p => p.FixedAssetCategoryName).ToList();
        }
        public List<FixedAssetCategory> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.FixedAssetCategoryCode).ToList();
        }

        public Guid? GetGuidFixedAssetCategoryByCode(string code)
        {
            Guid? id = null;
            FixedAssetCategory fac = Query.Where(p => p.FixedAssetCategoryCode == code).SingleOrDefault();
            if (fac != null)
            {
                id = fac.ID;
            }
            return id;
        }
    }
}
