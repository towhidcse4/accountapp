using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class VoucherPatternsReportService : BaseService<VoucherPatternsReport, int>, IVoucherPatternsReportService
    {
        public VoucherPatternsReportService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        private ITypeService typeService
        {
            get { return IoC.Resolve<ITypeService>(); }
        }
        public List<VoucherPatternsReport> GetAll_ByTypeID(int typeID)
        {
            // return Query.Where(c => (c.TypeID != null && c.TypeID == typeID) ||  typeService.Getbykey(typeID).TypeGroupID == c.TypeGroup).ToList();
            // return Query.Where(c => c.TypeID != null && c.ListTypeID.Split(',').Contains(typeID.ToString())).ToList();
            var q = Query.ToList().Where(v => v.ListTypeID != null && v.ListTypeID.Contains(typeID.ToString()))
                                  .OrderBy(v => v.VoucherPatternsName)
                                  .ToList();
            return q;
        }
    }
}