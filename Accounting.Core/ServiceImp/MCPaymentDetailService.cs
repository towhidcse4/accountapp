using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentDetailService : BaseService<MCPaymentDetail, Guid>, IMCPaymentDetailService
    {
        public MCPaymentDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<MCPaymentDetail> GetByMcPaymentId(Guid mCPaymentId)
        {
            return Query.Where(k => k.MCPaymentID == mCPaymentId).ToList();
        }
    }

}