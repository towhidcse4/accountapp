﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMAllocationDetailService : BaseService<EMAllocationDetail, Guid>, IEMAllocationDetailService
    {
        public EMAllocationDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }
}
