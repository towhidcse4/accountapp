﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FATransferDetailService : BaseService<FATransferDetail, Guid>, IFATransferDetailService
    {
        public FATransferDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<FATransferDetail> GetByFATransferID(Guid fATransferID)
        {
            return Query.Where(k => k.FATransferID == fATransferID).ToList();
        }

       
    }

}
