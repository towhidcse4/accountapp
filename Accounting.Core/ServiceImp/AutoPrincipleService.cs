﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class AutoPrincipleService : BaseService<AutoPrinciple, Guid>, IAutoPrincipleService
    {
        public AutoPrincipleService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<AutoPrinciple> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.TypeID).ToList();
        }
        public List<AutoPrinciple> GetAll_Active()
        {
            return Query.Where(p => p.IsActive == true).ToList();
        }
        /// <summary>
        /// Lấy danh sách lý do bằng typeID
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns></returns>
        public List<AutoPrinciple> getByTypeID(int typeID)
        {
            string idstring = typeID.ToString().Substring(0, typeID.ToString().Length - 1);
            int.TryParse(idstring, out typeID);
            List<AutoPrinciple> data = Query.Where(p => p.IsActive == true && ((p.TypeID >= typeID * 10 && p.TypeID < (typeID + 1) * 10) || p.TypeID == 0)).OrderBy(o => o.TypeID).ToList();
            return data;
        }
    }

}