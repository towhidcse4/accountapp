﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FADepreciationService : BaseService<FADepreciation,Guid>,IFADepreciationService
    {
        public FADepreciationService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
      
        }

        public List<FADepreciation> GetAll_OrderBy()
        {
            return Query.OrderByDescending(p => p.PostedDate).ThenBy(p => p.No).ToList();
        }

        public FADepreciation GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        public decimal GetDepreciationByMonth(DateTime from, DateTime to, Guid fixedAssetId)
        {
            var a = Query.Where(
                x =>
                x.PostedDate >= from && x.PostedDate <= to &&
                x.FADepreciationDetails.Any(y => y.FixedAssetID == fixedAssetId)).ToList();
            return
                a.Sum(
                        x =>
                            {
                                var faDepreciationDetail = x.FADepreciationDetails.FirstOrDefault(y => y.FixedAssetID == fixedAssetId);
                                return faDepreciationDetail != null ? faDepreciationDetail.
                                                                           MonthlyDepreciationAmount : 0;
                            });
        }
    }
}
