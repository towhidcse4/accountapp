﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using FX.Data;
using FX.Core;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class AccountingObjectService : BaseService<AccountingObject, Guid>, IAccountingObjectService
    {
        public AccountingObjectService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        #region 1 khai báo Service
        private IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }

        private IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        private IAccountService _IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        private IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        private IMBTellerPaperService _IMBTellerPaper { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        private IMBTellerPaperDetailVendorService IMBTellerPaperDetailVendorService { get { return IoC.Resolve<IMBTellerPaperDetailVendorService>(); } }
        private IMBCreditCardService _IMBCreditCard { get { return IoC.Resolve<IMBCreditCardService>(); } }
        private IMBCreditCardDetailVendorService IMBCreditCardDetailVendorService { get { return IoC.Resolve<IMBCreditCardDetailVendorService>(); } }
        private IMCPaymentDetailService IMCPaymentDetailService { get { return IoC.Resolve<IMCPaymentDetailService>(); } }
        private IMCPaymentService _IMCPayment { get { return IoC.Resolve<IMCPaymentService>(); } }
        private IMCPaymentDetailVendorService _IMCPaymentDetailVendorService { get { return IoC.Resolve<IMCPaymentDetailVendorService>(); } }
        private IPaymentClauseService _IPaymentClauseService { get { return IoC.Resolve<IPaymentClauseService>(); } }
        private IBudgetItemService _IBudgetItemService { get { return IoC.Resolve<IBudgetItemService>(); } }
        private IExpenseItemService _IExpenseItemService { get { return IoC.Resolve<IExpenseItemService>(); } }
        private ICostSetService _ICostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        private IStatisticsCodeService _IStatisticsCodeService { get { return IoC.Resolve<IStatisticsCodeService>(); } }
        private IEMContractService _IEMContractService { get { return IoC.Resolve<IEMContractService>(); } }

        private IPPInvoiceService _IPPInvoiceService { get { return IoC.Resolve<IPPInvoiceService>(); } }
        private IPPServiceService _IPPServiceService { get { return IoC.Resolve<IPPServiceService>(); } }
        private IExceptVoucherService _IExceptVoucherService { get { return IoC.Resolve<IExceptVoucherService>(); } }
        private IMBDepositService _iMBDepositService { get { return IoC.Resolve<IMBDepositService>(); } }
        private IMBDepositDetailCustomerService IMBDepositDetailCustomerService { get { return IoC.Resolve<IMBDepositDetailCustomerService>(); } }
        private IMBInternalTransferService _imMbInternalTransferService { get { return IoC.Resolve<IMBInternalTransferService>(); } }
        private IMBInternalTransferDetailService _internalTransferDetailService { get { return IoC.Resolve<IMBInternalTransferDetailService>(); } }
        private IMCReceiptService _iMCReceiptService { get { return IoC.Resolve<IMCReceiptService>(); } }
        private IMCReceiptDetailCustomerService IMCReceiptDetailCustomerService { get { return IoC.Resolve<IMCReceiptDetailCustomerService>(); } }
        private ISAInvoiceService _iSAInvoiceService { get { return IoC.Resolve<ISAInvoiceService>(); } }
        private ISAReturnService _saReturnService { get { return IoC.Resolve<ISAReturnService>(); } }
        private IFAAdjustmentService _iFaAdjustmentService { get { return IoC.Resolve<IFAAdjustmentService>(); } }
        private IFAAdjustmentDetailService _iFaAdjustmentDetailService { get { return IoC.Resolve<IFAAdjustmentDetailService>(); } }
        private IFADepreciationService _iFaDepreciationService { get { return IoC.Resolve<IFADepreciationService>(); } }
        private IFADepreciationDetailService _iFaDepreciationDetailService { get { return IoC.Resolve<IFADepreciationDetailService>(); } }
        private IFAIncrementService _incrementService { get { return IoC.Resolve<IFAIncrementService>(); } }
        private IFADecrementService _iFaDecrementService { get { return IoC.Resolve<IFADecrementService>(); } }
        private IFADecrementDetailService _iFaDecrementDetailService { get { return IoC.Resolve<IFADecrementDetailService>(); } }
        private IGOtherVoucherService _iGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        private IGOtherVoucherDetailService _iGOtherVoucherDetailService { get { return IoC.Resolve<IGOtherVoucherDetailService>(); } }
        private IGOtherVoucherDetailForeignCurrencyService _IGOtherVoucherDetailForeignCurrencyService { get { return IoC.Resolve<IGOtherVoucherDetailForeignCurrencyService>(); } }
        private IRSInwardOutwardService _inwardOutwardService { get { return IoC.Resolve<IRSInwardOutwardService>(); } }
        private IPPDiscountReturnService _iPpDiscountReturnService { get { return IoC.Resolve<IPPDiscountReturnService>(); } }

        private ITypeService _iTypeService { get { return IoC.Resolve<ITypeService>(); } }
        private ICurrencyService _ICurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        private ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        private IViewVouchersCloseBookService _IVoucherPatternsReportService { get { return IoC.Resolve<IViewVouchersCloseBookService>(); } }

        #endregion

        /// <summary>
        /// Lấy danh sách nhân viên active và sắp xếp theo mã
        /// </summary>
        /// <returns></returns>
        public List<AccountingObject> GetListEmployeeActiveOrderCode()
        {
            return Query.OrderBy(p => p.AccountingObjectCode).Where(p => p.IsActive).Where(p => p.IsEmployee).ToList();
        }
        /// <summary>
        /// Lấy ra danh sách đối tượng kế toán theo IsEmployee
        /// isEmployee = true: lấy ra nhân viên
        /// isEmployee = false: lấy ra các đối tượng không phải nhân viên
        /// </summary>
        /// <returns></returns>
        public List<AccountingObject> GetListAccountingObjectByEmployee(bool isEmployee)
        {
            if (isEmployee)
            {
                return Query.Where(p => p.IsEmployee == isEmployee).ToList();
            }
            else
            {
                return Query.Where(n => n.IsEmployee == isEmployee || n.IsEmployee && n.ObjectType != null).ToList();
            }

        }
        /// <summary>
        /// Lấy danh sách AccountingObject theo IsActive
        /// isActive = true: Lấy ra danh sách hoạt động
        /// isActive = false: lấy ra danh sách ngừng hoạt động
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<AccountingObject> GetListAccoungingObjectByIsActive(bool isActive)
        {
            List<AccountingObject> list = Query.Where(c => c.IsActive).ToList();
            return list;
        }
        /// <summary>
        /// Lấy danh sách AccountingObject theo IsActive và sắp xếp theo Code
        /// isActive = true: Lấy ra danh sách hoạt động
        /// isActive = false: lấy ra danh sách ngừng hoạt động
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<AccountingObject> GetListAccoungingObjectByIsActiveOrderCode(bool isActive)
        {
            List<AccountingObject> list =
                GetListAccoungingObjectByIsActive(isActive).OrderBy(p => p.AccountingObjectCode).ToList();
            return list;
        }
        /// <summary>
        /// Lấy ra danh sách tất cả AccountingObject và sắp xếp theo mã
        /// </summary>
        /// <returns></returns>
        public List<AccountingObject> GetListAccountingObjectOrderCode()
        {
            return Query.OrderBy(k => k.AccountingObjectCode).ToList();

        }
        /// <summary>
        /// Danh sách các AccountingObject theo danh sách ObjectType
        /// [DUYTN]
        /// </summary>
        /// <param name="objectType">là danh sách các ObjectType truyền vào (0, 1, 2 , ...)</param>
        /// <returns>tập các AccountingObject, null nếu bị lỗi</returns>
        public List<AccountingObject> GetAll_ByListObjectType(List<int> objectType)
        {
            return Query.Where(c => objectType.Contains((int)c.ObjectType)).ToList();
        }
        /// <summary>
        /// Lấy danh sách các đối tượng nhân viên, khách hàng, nhà cung cấp
        /// [DUYTN]
        /// </summary>
        /// <param name="select">0: khách hàng hoặc vừa là khách hàng vừa là nhà cung cấp, 
        /// 1: nhà cung cấp hoặc vừa là nhà cung cấp vừa là khách hàng, 
        /// 2: nhân viên, 
        /// 3: tất cả khách hàng và nhà cung cấp</param>
        /// <param name="isActive">là IsActive =true có hoạt động, false không hoạt động</param>
        /// <returns>tập các AccountingObject, null nếu bị lỗi</returns>
        public List<AccountingObject> GetAccountingObjects(int select, Boolean isActive = true)
        {
            if (select == 0)
            {
                //Khách hàng
                return Query.Where(k => (k.ObjectType == 0 || k.ObjectType == 2) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 1)
            {
                //Nhà cung cấp
                return Query.Where(k => (k.ObjectType == 1 || k.ObjectType == 2) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 2)
            {
                //Nhân viên
                return Query.Where(k => k.IsEmployee && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 3)
            {
                //Khách hàng + nhà cung cấp
                return Query.Where(k => (k.ObjectType == 0 || k.ObjectType == 1 || k.ObjectType == 2) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 4)
            {
                //vừa là Khách hàng, vừa là nhà cung cấp
                return Query.Where(k => (k.ObjectType == 2) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 5)
            {
                //đối tượng khác  trungnq thêm sửa bug 6190
                return Query.Where(k => (k.ObjectType == 3) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 6)
            {
                //đối tượng khác  + khách hàng + nhà cung cấp trungnq
                return Query.Where(k => (k.ObjectType == 3 || k.ObjectType == 2 || k.ObjectType == 1 || k.ObjectType == 0) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            return new List<AccountingObject>();
        }
        public List<AccountingObject> GetAccountingObjectsByType(int select)
        {
            if (select == 0)
            {
                //Khách hàng
                return Query.Where(k => (k.ObjectType == 0 || k.ObjectType == 2) ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 1)
            {
                //Nhà cung cấp
                return Query.Where(k => (k.ObjectType == 1 || k.ObjectType == 2) ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 2)
            {
                //Nhân viên
                return Query.Where(k => k.IsEmployee ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 3)
            {
                //Khách hàng + nhà cung cấp
                return Query.Where(k => (k.ObjectType == 0 || k.ObjectType == 1 || k.ObjectType == 2) ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 4)
            {
                //vừa là Khách hàng, vừa là nhà cung cấp
                return Query.Where(k => (k.ObjectType == 2) ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 5)
            {
                //đối tượng khác  trungnq thêm sửa bug 6190
                return Query.Where(k => (k.ObjectType == 3) ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            else if (select == 6)
            {
                //đối tượng khác  + khách hàng + nhà cung cấp trungnq
                return Query.Where(k => (k.ObjectType == 3 || k.ObjectType == 2 || k.ObjectType == 1 || k.ObjectType == 0) ).OrderBy(k => k.AccountingObjectCode).ToList();
            }
            return new List<AccountingObject>();
        }

        public List<AccountingObject> GetAccountingObjectsGetInvoice(Boolean isActive = true)
        {
            //Nhà cung cấp
            return Query.Where(k => (k.ObjectType == 1 || k.ObjectType == 2 || k.ObjectType == 3) && k.IsActive == isActive).OrderBy(k => k.AccountingObjectCode).ToList();
        }
        /// <summary>
        /// Lấy ra AccountingObject theo ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public AccountingObject GetAccountingObjectById(Guid ID)
        {
            try
            {
                return Query.Where(a => a.ID == ID).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Lấy ra AccountingObject the mã
        /// </summary>
        /// <returns></returns>
        public AccountingObject GetAccountingObjectByCode(string accountingObjectCode)
        {
            return Query.Where(a => a.AccountingObjectCode == accountingObjectCode && a.IsActive).FirstOrDefault();
        }
        /// <summary>
        /// Lấy ra danh sách AccountingObjectCode
        /// </summary>
        /// <returns></returns>
        public List<string> GetListAccountingObjectCode()
        {
            return Query.Select(p => p.AccountingObjectCode).ToList();
        }

        /// <summary>
        /// lấy danh sách khách hàng có phát sinh theo mã khách hàng: hàm tổng quát [PhongNT]
        /// </summary>
        /// 05/05/2014
        /// <param name="customerCode">mã khách hàng</param>
        /// <returns></returns>
        public List<CustomerDebit> GetCustDrByCode(string customerCode)
        {
            #region [Query in SQL statements]
            /*
             -- lay so du dau ky
            select a.AccountingObjectCode,
                    a.AccountingObjectName,
                    SUM(ISNULL(o.DebitAmount,0)) op_sum_debit,
                    SUM(ISNULL(o.CreditAmount,0)) op_sum_credit
            from AccountingObject a left join OPAccount o on a.ID = o.AccountingObjectID
            where a.IsActive = 1
                and a.ObjectType in (0, 2)
            group by a.AccountingObjectCode, a.AccountingObjectName;

            -- lay so phai thu phat sinh, so da thu
            select a.AccountingObjectCode,
                    a.AccountingObjectName,
                    SUM(ISNULL(g.DebitAmount, 0)) sum_debit,
                    SUM(ISNULL(g.CreditAmount, 0)) sum_credit
            from AccountingObject a left join GeneralLedger g on a.ID = g.AccountingObjectID
            where a.IsActive = 1
                and a.ObjectType in (0, 2)
            group by a.AccountingObjectCode, a.AccountingObjectName;
             */
            #endregion [Query in SQL statements]

            List<CustomerDebit> lstCustDr = new List<CustomerDebit>();
            #region
            //List<Account> lstAccount = _IAccountService.GetCustomerAcctList(true, "1");
            //List<GeneralLedger> lstG = _IGeneralLedgerService.GetGLByListID(lstAccount.Select(a => a.AccountNumber).ToList());
            //List<AccountingObject> lstAccountingObj = Query.Where(a => a.IsActive == true).Where(a => a.ObjectType == 0).ToList();

            //if (customerCode == null || customerCode == "") // lấy danh sách tất cả các khách hàng có phát sinh
            //{
            //    var lstAccountGL = from g in lstG
            //                       join acct in lstAccount on g.Account equals acct.AccountNumber
            //                       join acctObj in lstAccountingObj on g.AccountingObjectID equals acctObj.ID
            //                       //group new { g, acct, acctObj } by new
            //                       group new { g.DebitAmount, g.CreditAmount } by new
            //                       {
            //                           g.Account,
            //                           g.No,
            //                           g.InvoiceDate,
            //                           g.InvoiceNo,
            //                           g.PostedDate,
            //                           g.TypeID,
            //                           g.AccountingObjectID,
            //                           //g.DebitAmount,
            //                           //g.CreditAmount,
            //                           acct.AccountName,
            //                           acct.DetailType,
            //                           acctObj.AccountingObjectCode,
            //                           acctObj.Address,
            //                           acctObj.Tel,
            //                           acctObj.Fax,
            //                           acctObj.Email
            //                       }
            //                           into joined
            //                           from g in joined.DefaultIfEmpty()
            //                           orderby joined.Key.Account
            //                           //where joined.Key.DetailType == "1"
            //                           select new
            //                           {
            //                               Code = joined.Key.AccountingObjectCode,
            //                               Name = joined.Key.AccountName,
            //                               Address = joined.Key.Address,
            //                               Phone = joined.Key.Tel,
            //                               Fax = joined.Key.Fax,
            //                               Email = joined.Key.Email,
            //                               VourcherDate = joined.Key.InvoiceDate,
            //                               No = joined.Key.No,
            //                               InvoiceNo = joined.Key.InvoiceNo,
            //                               PostedDate = joined.Key.PostedDate,
            //                               TypeId = joined.Key.TypeID,
            //                               //SoDuDauNam = joined.Sum(x => ((joined.Key.No.ToUpper().Contains("OPN")) ? joined.Key.DebitAmount - joined.Key.CreditAmount : 0)),
            //                               //SoPhatSinh = joined.Key.DebitAmount,
            //                               //SoDaThu = joined.Key.CreditAmount,
            //                               //SoConPhaiThu = joined.Sum(x => ((joined.Key.No.ToUpper().Contains("OPN")) ? joined.Key.DebitAmount - joined.Key.CreditAmount : 0) + (joined.Key.DebitAmount - joined.Key.CreditAmount))
            //                               SoDuDauNam = joined.Sum(x => (joined.Key.No.ToUpper().Contains("OPN")) ? x.DebitAmount - x.CreditAmount : 0),
            //                               SoPhatSinh = joined.Sum(x => x.DebitAmount),
            //                               SoDaThu = joined.Sum(x => x.CreditAmount),
            //                               SoConPhaiThu = joined.Sum(x => ((joined.Key.No.ToUpper().Contains("OPN")) ? x.DebitAmount - x.CreditAmount : 0) + (x.DebitAmount - x.CreditAmount))
            //                           };

            //    foreach (var accountGl in lstAccountGL)
            //    {
            //        CustomerDebit CustDr = new CustomerDebit();
            //        CustDr.Name = accountGl.Name;
            //        CustDr.Code = accountGl.Code;
            //        CustDr.SoDuDauNam = accountGl.SoDuDauNam;
            //        CustDr.SoPhatSinh = accountGl.SoPhatSinh;
            //        CustDr.SoDaThu = accountGl.SoDaThu;
            //        CustDr.SoConPhaiThu = accountGl.SoConPhaiThu;

            //        List<ReceiptDebit> lstReceipt = new List<ReceiptDebit>();
            //        ReceiptDebit receipt = new ReceiptDebit();
            //        receipt.VouchersDate = Convert.ToDateTime(accountGl.VourcherDate);
            //        receipt.No = accountGl.No;
            //        receipt.InvoiceNo = accountGl.InvoiceNo;
            //        receipt.PostedDate = accountGl.PostedDate;
            //        receipt.TypeId = accountGl.TypeId;
            //        lstReceipt.Add(receipt);
            //        CustDr.Receipts = lstReceipt;
            //        lstCustDr.Add(CustDr);
            //    }
            //}
            //else // lấy khách hàng có mã khách hàng như tham số đầu vào
            //{
            //    var lstAccountGL = from g in lstG
            //                       join acct in lstAccount on g.Account equals acct.AccountNumber
            //                       join acctObj in lstAccountingObj on g.AccountingObjectID equals acctObj.ID
            //                       //group new { g, acct, acctObj } by new
            //                       group new { g.DebitAmount, g.CreditAmount } by new
            //                       {
            //                           g.Account,
            //                           g.No,
            //                           g.InvoiceDate,
            //                           g.InvoiceNo,
            //                           g.PostedDate,
            //                           g.TypeID,
            //                           g.AccountingObjectID,
            //                           g.DebitAmount,
            //                           g.CreditAmount,
            //                           acct.AccountName,
            //                           acct.DetailType,
            //                           acctObj.AccountingObjectCode,
            //                           acctObj.Address,
            //                           acctObj.Tel,
            //                           acctObj.Fax,
            //                           acctObj.Email
            //                       }
            //                           into joined
            //                           from g in joined.DefaultIfEmpty()
            //                           orderby joined.Key.Account
            //                           //where joined.Key.DetailType == "1" && joined.Key.AccountingObjectCode == customerCode
            //                           where joined.Key.AccountingObjectCode == customerCode
            //                           select new
            //                           {
            //                               Code = joined.Key.AccountingObjectCode,
            //                               Name = joined.Key.AccountName,
            //                               Address = joined.Key.Address,
            //                               Phone = joined.Key.Tel,
            //                               Fax = joined.Key.Fax,
            //                               Email = joined.Key.Email,
            //                               VourcherDate = joined.Key.InvoiceDate,
            //                               No = joined.Key.No,
            //                               InvoiceNo = joined.Key.InvoiceNo,
            //                               PostedDate = joined.Key.PostedDate,
            //                               TypeId = joined.Key.TypeID,
            //                               //SoDuDauNam = joined.Sum(x => ((joined.Key.No.ToUpper().Contains("OPN")) ? joined.Key.DebitAmount - joined.Key.CreditAmount : 0)),
            //                               //SoPhatSinh = joined.Key.DebitAmount,
            //                               //SoDaThu = joined.Key.CreditAmount,
            //                               //SoConPhaiThu = joined.Sum(x => ((joined.Key.No.ToUpper().Contains("OPN")) ? joined.Key.DebitAmount - joined.Key.CreditAmount : 0) + (joined.Key.DebitAmount - joined.Key.CreditAmount))
            //                               SoDuDauNam = joined.Sum(x => (joined.Key.No.ToUpper().Contains("OPN")) ? x.DebitAmount - x.CreditAmount : 0),
            //                               SoPhatSinh = joined.Sum(x => x.DebitAmount),
            //                               SoDaThu = joined.Sum(x => x.CreditAmount),
            //                               SoConPhaiThu = joined.Sum(x => ((joined.Key.No.ToUpper().Contains("OPN")) ? x.DebitAmount - x.CreditAmount : 0) + (x.DebitAmount - x.CreditAmount))
            //                           };

            //    foreach (var accountGl in lstAccountGL)
            //    {
            //        CustomerDebit CustDr = new CustomerDebit();
            //        CustDr.Name = accountGl.Name;
            //        CustDr.Code = accountGl.Code;
            //        CustDr.SoDuDauNam = accountGl.SoDuDauNam;
            //        CustDr.SoPhatSinh = accountGl.SoPhatSinh;
            //        CustDr.SoDaThu = accountGl.SoDaThu;
            //        CustDr.SoConPhaiThu = accountGl.SoConPhaiThu;

            //        List<ReceiptDebit> lstReceipt = new List<ReceiptDebit>();
            //        ReceiptDebit receipt = new ReceiptDebit();
            //        receipt.VouchersDate = Convert.ToDateTime(accountGl.VourcherDate);
            //        receipt.No = accountGl.No;
            //        receipt.InvoiceNo = accountGl.InvoiceNo;
            //        receipt.PostedDate = accountGl.PostedDate;
            //        receipt.TypeId = accountGl.TypeId;
            //        lstReceipt.Add(receipt);
            //        CustDr.Receipts = lstReceipt;
            //        lstCustDr.Add(CustDr);
            //    }
            //}
            #endregion
            return lstCustDr;
        }
        public List<VoucherAccountingObject> GetListVoucherAccoungtingObjectView()
        {
            var result = new List<VoucherAccountingObject>();
            List<ViewVouchersCloseBook> lstVoucher = _IVoucherPatternsReportService.GetAllVoucherAccountingObject();
            for (int i = 0; i < lstVoucher.Count; i++)
            {
                var item = lstVoucher[i];
                var temp = new VoucherAccountingObject();
                temp.ID = item.ID;
                temp.AccountingObjectId = item.AccountingObjectID;
                temp.EmployeeId = item.EmployeeID;
                temp.TypeID = item.TypeID;
                temp.Date = item.Date;
                temp.No = item.No;
                temp.Reason = item.Reason;
                temp.TotalAmount = ((item.TotalAmount ?? 0) + (item.VATAmount ?? 0) - (item.DiscountAmount ?? 0));
                temp.TypeName = item.TypeName;
                result.Add(temp);
            }
            return result;
        }

        public Guid? GetGuidAccountingObjectByCode(string code)
        {
            Guid? id = null;
            AccountingObject ac = Query.Where(p => p.AccountingObjectCode == code).SingleOrDefault();
            if (ac != null)
            {
                id = ac.ID;
            }
            return id;
        }

        #region danh sách trả tiền nhà cung cấp DuyNT(edit Chuongnv)
        /// <summary>
        /// Lấy danh sách nhà cung cấp cần trả tiền theo ngày hạch toán
        /// [DuyNT]
        /// </summary>
        /// <param name="accountingDate">ngày hạch toán</param>
        /// <returns></returns>
        public List<PayVendor> GetListPayVendor(DateTime accountingDate, DateTime? beginDate, DateTime? endDate)
        {

            int lamtrondg = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            int lamtrondg1 = 0;
            var ddSoTyGia1 = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
            if (ddSoTyGia1 != null) lamtrondg1 = int.Parse(ddSoTyGia1.Data);
            List<int> dsTypePPInvoice = new List<int>() { 210, 230, 430, 500, 701, 601 };//danh sách type lưu vào bảng dữ liệu ppinvoice
            List<int> dsTypePPService = new List<int>() { 240 };//danh sách type lưu vào bảng PPService
            List<int> dsTypePC = new List<int>() { 110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174 };//danh sách type lưu vào bảng dữ liệu ppinvoice
            List<PPService> lstPPServices = new List<PPService>();
            List<PPInvoice> lstInvoices = new List<PPInvoice>();
            List<GOtherVoucherDetailForeignCurrency> lstgo = new List<GOtherVoucherDetailForeignCurrency>();
            List<MBTellerPaperDetailVendor> lstMbTellerPaper = new List<MBTellerPaperDetailVendor>();
            List<MBCreditCardDetailVendor> lstMbCreditCard = new List<MBCreditCardDetailVendor>();
            List<MCPaymentDetailVendor> lstMcPayment = new List<MCPaymentDetailVendor>();
            List<ExceptVoucher> lstExceptVoucher = new List<ExceptVoucher>();
            List<AccountingObject> lstAccountingObjects = new List<AccountingObject>();
            List<Account> lstAccounts = new List<Account>();
            List<GeneralLedger> lstGeneralLedger = new List<GeneralLedger>();
            // lọc dữ liệu theo 2 constructor beginDate và endDate
            if (beginDate == null && endDate == null)
            {
                lstPPServices = _IPPServiceService.GetAll();
                lstInvoices = _IPPInvoiceService.GetAll();
                lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(x => _iGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
                // danh sách đã trả tiền trong năm nay             
                lstMbTellerPaper = IMBTellerPaperDetailVendorService.Query.Where(x => _IMBTellerPaper.Query.Any(c => c.ID == x.MBTellerPaperID && c.Recorded)).ToList(); // edit by tungnt : lay nhung chung tu da ghi so
                lstMbCreditCard = IMBCreditCardDetailVendorService.Query.Where(x => _IMBCreditCard.Query.Any(c => c.ID == x.MBCreditCardID && c.Recorded)).ToList(); // edit by tungnt : lay nhung chung tu da ghi so
                lstMcPayment = _IMCPaymentDetailVendorService.GetAll();
                lstExceptVoucher = _IExceptVoucherService.GetAll();
                lstAccountingObjects = GetAccountingObjects(1);//danh sách nhà cung cấp
                lstAccounts = _IAccountService.GetByDetailType(0);//danh sách tài khoản theo dõi chi tiết theo nhà cung cấp
                lstGeneralLedger =
                    _IGeneralLedgerService.Query.Where(c => c.AccountingObjectID != null && c.Account.StartsWith("331")).ToList().Where(c => (dsTypePPInvoice.Any(a => a == c.TypeID) || dsTypePPService.Any(b => b == c.TypeID) || dsTypePC.Any(x => x == c.TypeID))).ToList();
            }
            else
            {
                DateTime _beginDate = (DateTime)beginDate;
                DateTime _endDate = (DateTime)endDate;
                beginDate = _beginDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                endDate = _endDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                lstPPServices = _IPPServiceService.GetAll().Where(n => n.PostedDate <= endDate).ToList();
                lstInvoices = _IPPInvoiceService.GetAll().Where(n => n.PostedDate <= endDate).ToList();
                lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(n => n.DateReevaluate >= beginDate && n.DateReevaluate <= endDate).ToList().Where(x => _iGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
                // danh sách đã trả tiền trong năm nay             
                lstMbTellerPaper = IMBTellerPaperDetailVendorService.Query.Where(n => n.VoucherDate <= endDate).ToList().Where(x => _IMBTellerPaper.Query.Any(c => c.ID == x.MBTellerPaperID && c.Recorded)).ToList(); // edit by tungnt : lay nhung chung tu da ghi so
                lstMbCreditCard = IMBCreditCardDetailVendorService.Query.Where(n => n.VoucherDate <= endDate).ToList().Where(x => _IMBCreditCard.Query.Any(c => c.ID == x.MBCreditCardID && c.Recorded)).ToList(); // edit by tungnt : lay nhung chung tu da ghi so
                lstMcPayment = _IMCPaymentDetailVendorService.GetAll().Where(n => n.VoucherDate <= endDate).ToList().Where(x => _IMCPayment.Query.Any(c => c.ID == x.MCPaymentID && c.Recorded)).ToList();
                lstExceptVoucher = _IExceptVoucherService.GetAll().Where(n => n.PostedDate <= endDate).ToList();
                lstAccountingObjects = GetAccountingObjects(1);//danh sách nhà cung cấp
                lstAccounts = _IAccountService.GetByDetailType(0);//danh sách tài khoản theo dõi chi tiết theo nhà cung cấp
                lstGeneralLedger =
                    _IGeneralLedgerService.Query.Where(c => c.AccountingObjectID != null && c.Account.StartsWith("331") && c.PostedDate <= endDate).ToList().Where(c => (dsTypePPInvoice.Any(a => a == c.TypeID) || dsTypePPService.Any(b => b == c.TypeID) || dsTypePC.Any(x => x == c.TypeID))).ToList();
            }
            //danh sách trong sổ cái theo có AccountingObjectID thuộc danh sách nhà cung cấp và có account thuộc danh sách tài khoản
            var accountingObjects = lstAccountingObjects;
            List<PayVendor> lstGroupAccountingObjects = new List<PayVendor>();
            try
            {
                lstGroupAccountingObjects =
                (from c in lstGeneralLedger.ToList()
                 group c by c.AccountingObjectID into g
                 where accountingObjects != null
                 select new PayVendor()
                 {
                     AccountingObjectID = g.Key.Value,
                     AccountingObjectCode = accountingObjects.Single(c => c.ID == g.Key.Value).AccountingObjectCode,
                     AccountingObjectName = accountingObjects.Single(c => c.ID == g.Key.Value).AccountingObjectName,
                     AccountingObjectAddress = accountingObjects.Single(c => c.ID == g.Key.Value).Address,
                     Tel = accountingObjects.Single(c => c.ID == g.Key.Value).Tel,
                     Fax = accountingObjects.Single(c => c.ID == g.Key.Value).Fax,
                     Email = accountingObjects.Single(c => c.ID == g.Key.Value).Email,
                     TaxCode = accountingObjects.Single(c => c.ID == g.Key.Value).TaxCode,
                     ContactName = accountingObjects.Single(c => c.ID == g.Key.Value).ContactName,
                     SoDuDauNam = (g.Where(c => c.PostedDate <= beginDate && c.TypeID == 701).Sum(c => Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) +
                                                                 g.Where(c => c.PostedDate < beginDate && c.TypeID != 701).Sum(c => Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero))),
                     //tổng các số dư ban đầu có typeid == 701 + [tổng các số phát sinh (DebitAmountOriginal-CreditAmountOriginal)--> tương đương với tổng số phát sinh của DebitAmountOriginal - tổng số phát sinh của CreditAmountOriginal]
                     //SoPhatSinh = g.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + accountingDate.Year.ToString()) && c.TypeID != 701 && !(dsTypePC.Any(d => d == c.TypeID))).Sum(c => Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                     ////tương đương số dư đầu kỳ + số phát sinh - số đã trả -> lấy số dư ban đầu - tất cả các phát sinh
                     SoDaTra = lstGeneralLedger.Where(x => /*dsTypePC.Any(d => d == x.TypeID) &&*/ x.AccountingObjectID == g.Key.Value).Where(n => n.PostedDate >= beginDate && n.PostedDate <= endDate).Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero))
                     /*+ lstExceptVoucher.Where(x => x.AccountingObjectID == g.Key.Value).Sum(t => Math.Round(t.Amount, lamtrondg, MidpointRounding.AwayFromZero))*/, // edit by tungnt: k can lấy từ bảng except voucher
                     SoConPhaiTra = 0,
                     //Voucher = g.GroupBy(c => new { c.ReferenceID, c.Date, c.No, c.PostedDate, c.TypeID }).Select(g2 => new VoucherVendor()
                     //{
                     //    VouchersDate = g2.Key.PostedDate,
                     //    No = g2.Key.No,
                     //    VouchersPostedDate = g2.Key.PostedDate,
                     //    PostedDate = (dsTypePPInvoice.Any(d => d == g2.Key.TypeID) && lstInvoices.Count(d => d.ID == g2.Key.ReferenceID) == 1) ? lstInvoices.Single(d => d.ID == g2.Key.ReferenceID).DueDate : (dsTypePPService.Any(d => d == g2.Key.TypeID) && lstPPServices.Count(d => d.ID == g2.Key.ReferenceID) == 1) ? lstPPServices.Single(d => d.ID == g2.Key.ReferenceID).DueDate : null,
                     //    SoDaTra = g2.Where(c => dsTypePC.Any(d => d == c.TypeID)).Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                     //    SoPhatSinh = g2.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),

                     //}).ToList(), // danh sách các chứng từ phát sinh năm nay


                     Bill = (from d in g.Where(c => (dsTypePPInvoice.Any(d => d == c.TypeID) || dsTypePPService.Any(d => d == c.TypeID)) /*&& (c.Date >= DateTime.Parse("1/1/" + accountingDate.Year.ToString()))*/).ToList()
                             group d by new { d.ReferenceID, d.No, d.InvoiceNo, d.PostedDate, d.TypeID, d.AccountingObjectID, d.EmployeeID, d.CurrencyID, d.ExchangeRate } into c
                             let firstOrDefault = c.FirstOrDefault(f => lstAccounts.Any(t => f.Account == t.AccountNumber))
                             where firstOrDefault != null
                             select new BillPPPayVendorDetail()
                             {
                                 ReferenceID = (Guid)c.Key.ReferenceID,
                                 CheckColumn = false,
                                 Date = c.Key.PostedDate,
                                 TypeID = c.Key.TypeID,
                                 No = c.Key.No,
                                 InvoiceNo = c.Key.InvoiceNo,
                                 RefVoucherExchangeRate = c.Key.ExchangeRate,
                                 AmountOriginal = 0M,
                                 Amount = 0M,
                                 PaymentAmount = c.Sum(t => Math.Round(t.CreditAmount - t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) - (decimal)(c.Sum(t => Math.Round(t.CreditAmount - t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstMbCreditCard.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstMbTellerPaper.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstMcPayment.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount ?? 0, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstExceptVoucher.Where(d => d.AccountingObjectID == c.Key.AccountingObjectID && d.DebitAccount == firstOrDefault.Account && d.GLVoucherID == c.Key.ReferenceID).ToList().Sum(d => Math.Round(d.Amount, lamtrondg, MidpointRounding.AwayFromZero))),
                                 DueDate = (dsTypePPInvoice.Any(d => d == c.Key.TypeID) && lstInvoices.Count(d => d.ID == c.Key.ReferenceID) == 1) ? lstInvoices.Single(d => d.ID == c.Key.ReferenceID).DueDate : (dsTypePPService.Any(d => d == c.Key.TypeID) && lstPPServices.Count(d => d.ID == c.Key.ReferenceID) == 1) ? lstPPServices.Single(d => d.ID == c.Key.ReferenceID).DueDate : null,
                                 TotalDebit = c.Sum(t => Math.Round(t.CreditAmount - t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                                 TotalDebitOriginal = c.Sum(t => Math.Round(t.CreditAmountOriginal - t.DebitAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)),
                                 DebitAmountOriginal = (decimal)((c.Sum(t => Math.Round(t.CreditAmountOriginal - t.DebitAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                            lstMbCreditCard.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                            lstMbTellerPaper.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                            lstMcPayment.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal ?? 0, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                                            lstExceptVoucher.Where(d => d.AccountingObjectID == c.Key.AccountingObjectID && d.DebitAccount == firstOrDefault.Account && d.GLVoucherID == c.Key.ReferenceID).ToList().Sum(d => Math.Round(d.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)))),
                                 //tổng tiền MBCreditCardDetailVendors với cùng tài khoản và AccountingObjectID
                                 //tổng tiền MBTellerPaperDetailVendors với cùng tài khoản và AccountingObjectID
                                 //tổng tiền MCPaymentDetailVendors với cùng tài khoản và AccountingObjectID
                                 //tổng tiền đã đối trừ trong ExceptVoucher với cùng tài khoản và AccountingObjectID
                                 DebitAmount = (decimal)(c.Sum(t => Math.Round(t.CreditAmount - t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstMbCreditCard.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstMbTellerPaper.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstMcPayment.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.PPInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount ?? 0, lamtrondg, MidpointRounding.AwayFromZero)) -
                                            lstExceptVoucher.Where(d => d.AccountingObjectID == c.Key.AccountingObjectID && d.DebitAccount == firstOrDefault.Account && d.GLVoucherID == c.Key.ReferenceID).ToList().Sum(d => Math.Round(d.Amount, lamtrondg, MidpointRounding.AwayFromZero))), 
                                 LastExchangeRate = lstgo.Any(x => x.CurrencyID == c.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == c.Key.ReferenceID)) ?
                       lstgo.Where(x => x.CurrencyID == c.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == c.Key.ReferenceID)).OrderByDescending(d => d.DateReevaluate)
                       .FirstOrDefault().VoucherDetailForeignCurrencys.FirstOrDefault(x => x.ReferenceID == c.Key.ReferenceID).NewExchangeRate : 0,
                                 DifferAmount = 0,
                                 Account = firstOrDefault.Account,
                                 EmployeeName = c.Key.EmployeeID != null ? _IAccountingObjectService.Getbykey((Guid)c.Key.EmployeeID).AccountingObjectName : "",
                                 PaymentClause = (dsTypePPInvoice.Any(d => d == c.Key.TypeID) && lstInvoices.Count(d => d.ID == c.Key.ReferenceID) == 1) ? lstInvoices.Single(d => d.ID == c.Key.ReferenceID).PaymentClauseID.ToString() : (dsTypePPService.Any(d => d == c.Key.TypeID) && lstPPServices.Count(d => d.ID == c.Key.ReferenceID) == 1) ? lstPPServices.Single(d => d.ID == c.Key.ReferenceID).PaymentClauseID.ToString() : "",
                                 CurrencyID = c.Key.CurrencyID
                             }).ToList(),
                     Accounting = (g.Where(c => c.TypeID != 701).Select(c => new AccountingPPPayVendorDetail()
                     {
                         Description = c.Description,
                         AccountingObjectCode = accountingObjects.Single(d => d.ID == g.Key.Value).AccountingObjectCode,
                         DebitAccount = c.Account,

                     })).ToList()
                 }).ToList();
            }
            catch (Exception ex)
            {

            }
            foreach (PayVendor payVendor in lstGroupAccountingObjects)
            {
                //payVendor.SoDaTra = payVendor.Voucher.Sum(c => c.SoDaTra);

                for (int i = payVendor.Bill.Count - 1; i >= 0; i--)
                {
                    if (payVendor.Bill[i].LastExchangeRate != 0)
                    {
                        payVendor.Bill[i].DebitAmount = payVendor.Bill[i].DebitAmountOriginal * payVendor.Bill[i].LastExchangeRate;
                    }
                    if (payVendor.Bill[i].TotalDebit == 0)
                    {
                        payVendor.Bill.RemoveAt(i);
                    }
                    //else if (payVendor.Bill[i].DebitAmountOriginal <= 0)
                    //{
                    //    payVendor.Bill.RemoveAt(i);
                    //}
                }
                payVendor.SoPhatSinh = payVendor.Bill.Where(x => x.TypeID != 701 && (x.Date >= beginDate && x.Date <= endDate)).Sum(t => t.TotalDebit);
                payVendor.SoConPhaiTra = payVendor.SoDuDauNam + payVendor.SoPhatSinh - payVendor.SoDaTra;
                payVendor.Bill.OrderByDescending(x => x.Date).ToList();
            }
            //for (int i = lstGroupAccountingObjects.Count - 1; i >= 0; i--)
            //{
            //    if (lstGroupAccountingObjects[i].SoConPhaiTra == 0)
            //    {
            //        lstGroupAccountingObjects.RemoveAt(i);
            //    }
            //}
            return lstGroupAccountingObjects.Where(x => (x.SoDuDauNam != 0 || x.SoPhatSinh != 0 || x.SoDaTra != 0)).ToList(); ;
        }
        #endregion

        #region danh sách thu tiền khách hàng DuyNT(edit Chuongnv)
        /// <summary>
        /// Lấy danh sách khách hàng phải trả tiền theo ngày hạch toán
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public List<CollectionCustomer> GetListCustomerDebits(DateTime startDate, DateTime? beginDate = null, DateTime? endDate = null)
        {
            //// danh sách đã thu tiền trong năm nay     
            int lamtrondg = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            int lamtrondg1 = 0;
            var ddSoTyGia1 = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
            if (ddSoTyGia1 != null) lamtrondg1 = int.Parse(ddSoTyGia1.Data);
            List<MBDepositDetailCustomer> lstMbDepositDetailCustomers = new List<MBDepositDetailCustomer>();
            List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = new List<MCReceiptDetailCustomer>();
            List<ExceptVoucher> lstExceptVoucher = new List<ExceptVoucher>();
            List<GOtherVoucherDetailForeignCurrency> lstgo = new List<GOtherVoucherDetailForeignCurrency>();
            List<SAInvoice> lstSaInvoice = new List<SAInvoice>();
            List<Domain.Type> lstTypes = new List<Domain.Type>();
            List<PaymentClause> lstPaymentClauses = new List<PaymentClause>();
            List<AccountingObject> lstAccountingObjects = GetAccountingObjects(0);//danh sách khách hàng
            List<AccountingObject> lstAccountingObjectsEmployee = GetAccountingObjects(2);//danh sách nhân viên
            List<Account> lstAccounts = _IAccountService.GetByDetailType(1);//danh sách tài khoản theo dõi chi tiết theo khách hàng
            List<GeneralLedger> lstGeneralLedger = new List<GeneralLedger>();
            List<int> dsTypeSaInvoice = new List<int>() { 320, 323, 701 };//danh sách type của bán hàng
            List<int> dsType = new List<int>() { 101, 161 };//danh sách type của trả tiền
            List<string> lstStringAccount = new List<string> { "131" };
            if (beginDate == null && endDate == null)
            {
                lstMbDepositDetailCustomers = IMBDepositDetailCustomerService.Query.Where(c => _iMBDepositService.Query.Any(d => d.PostedDate >= DateTime.Parse("1/1/" + startDate.Year.ToString()))).ToList();
                lstMcReceiptDetailCustomers = IMCReceiptDetailCustomerService.Query.Where(c => _iMCReceiptService.Query.Any(d => d.PostedDate >= DateTime.Parse("1/1/" + startDate.Year.ToString()))).ToList();
                lstExceptVoucher = _IExceptVoucherService.GetAll();
                lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(x => _iGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
                lstSaInvoice = _iSAInvoiceService.GetAll();
                lstTypes = _iTypeService.GetAll();
                lstPaymentClauses = IoC.Resolve<IPaymentClauseService>().GetIsActive(true);
                lstGeneralLedger =
                    _IGeneralLedgerService.Query.Where(c => c.AccountingObjectID != null && c.Account != null).ToList().Where(c => lstStringAccount.Any(d => c.Account.StartsWith(d))).ToList();
            }
            else
            {
                DateTime _beginDate = (DateTime)beginDate;
                DateTime _endDate = (DateTime)endDate;
                beginDate = _beginDate.AddHours(0).AddMinutes(0).AddSeconds(0);
                endDate = _endDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                lstMbDepositDetailCustomers = IMBDepositDetailCustomerService.GetAll().Where(n => n.VoucherDate <= endDate).ToList();
                lstMcReceiptDetailCustomers = IMCReceiptDetailCustomerService.GetAll().Where(n => n.VoucherDate <= endDate).ToList();
                lstExceptVoucher = _IExceptVoucherService.GetAll().Where(n => n.PostedDate <= endDate).ToList();
                lstgo = _IGOtherVoucherDetailForeignCurrencyService.Query.Where(n => n.DateReevaluate >= beginDate && n.DateReevaluate <= endDate).ToList().Where(x => _iGOtherVoucherService.Query.Any(c => c.ID == x.GOtherVoucherID && c.Recorded)).ToList();
                lstSaInvoice = _iSAInvoiceService.GetAll().Where(n => n.PostedDate >= beginDate && n.PostedDate <= endDate).ToList();
                lstTypes = _iTypeService.GetAll();
                lstPaymentClauses = IoC.Resolve<IPaymentClauseService>().GetIsActive(true);
                lstGeneralLedger = _IGeneralLedgerService.Query.Where(c => c.AccountingObjectID != null && c.Account != null).ToList().Where(c => lstStringAccount.Any(d => c.Account.StartsWith(d))).ToList();
            }

            // var accountingObjects = lstAccountingObjects.Where(c => lstGeneralLedger.Any(d => d.AccountingObjectID == c.ID)).ToList();
            var accountingObjects = lstAccountingObjects;
            List<CollectionCustomer> lstGroupAccountingObjects =
                (from c in lstGeneralLedger.ToList()
                 group c by c.AccountingObjectID into g
                 let guid = g.Key
                 where guid != null
                 select new CollectionCustomer()
                 {
                     AccountingObject = accountingObjects.FirstOrDefault(c => c.ID == guid.Value),
                     AccountingObjectCode = accountingObjects.Count(c => c.ID == guid.Value) == 1 ? accountingObjects.Single(c => c.ID == guid.Value).AccountingObjectCode : "",
                     AccountingObjectName = accountingObjects.Count(c => c.ID == guid.Value) == 1 ? accountingObjects.Single(c => c.ID == guid.Value).AccountingObjectName : "",
                     Address = accountingObjects.Count(c => c.ID == guid.Value) == 1 ? accountingObjects.Single(c => c.ID == guid.Value).Address : "",
                     SoDuDauNam = (g.Where(c => c.PostedDate <= beginDate && c.TypeID == 701).Sum(c => Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) +
                                                               g.Where(c => c.PostedDate < beginDate && c.TypeID != 701 /*&& dsTypeSaInvoice.Any(d => d == c.TypeID)*/).Sum(c => Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero))),
                         //SoPhatSinh = (g.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + startDate.Year.ToString()) && c.TypeID != 701 /*&& dsTypeSaInvoice.Any(d => d == c.TypeID)*/ && lstStringAccount.Any(d => c.Account.StartsWith(d))).Sum(c => Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero))
                         //- g.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + startDate.Year.ToString()) && c.TypeID != 701 /*&& dsTypeSaInvoice.Any(d => d == c.TypeID)*/ && lstStringAccount.Any(d => c.Account.StartsWith(d))).Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero))),
                         SoDaThu = lstGeneralLedger.Where(x => /*dsType.Any(d => d == x.TypeID) &&*/ x.AccountingObjectID == guid.Value).Where(n => n.PostedDate >= beginDate && n.PostedDate <= endDate).Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                     //+ lstExceptVoucher.Where(x => x.AccountingObjectID == guid.Value).Sum(t => Math.Round(t.Amount, lamtrondg, MidpointRounding.AwayFromZero)),
                     SoConPhaiThu = 0,
                     CurrencyID = "VND",
                     ExchangeRate = 1,
                     AccountingObjectID = guid.Value,
                     TotalAmount = 0,
                     TotalAmountOriginal = 0,
                         //ReceiptDebitDetails = g.GroupBy(
                         // c => new { c.ReferenceID, c.Date, c.No, c.InvoiceNo, c.PostedDate, c.TypeID }).Select(g2 => new ReceiptDebit()
                         // {
                         //     Date = g2.Key.Date,
                         //     No = g2.Key.No,
                         //     InvoiceNo = g2.Key.InvoiceNo,
                         //     PostedDate = g2.Key.PostedDate,
                         //     TypeName = lstTypes.Single(d => d.ID == g2.Key.TypeID).TypeName,
                         //     TypeID = g2.Key.TypeID,
                         //     SoPhatSinh = g2.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                         //     SoDaThu = g2.Where(x => dsType.Any(d => d == x.TypeID)).Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                         // }).ToList(),// danh sách các chứng từ phát sinh năm nay, // danh sách các chứng từ phát sinh năm nay
                         ReceiptDebits =
                  (from d in g.Where(c => !dsType.Any(d => d == c.TypeID) /*&& (c.Date >= DateTime.Parse("1/1/" + startDate.Year.ToString()))*/).ToList()
                   group d by new { d.ReferenceID, d.Date, d.No, d.InvoiceNo, d.PostedDate, d.TypeID, d.AccountingObjectID, d.EmployeeID, d.CurrencyID, d.Reason, d.ExchangeRate } into c
                   let firstOrDefault = c.FirstOrDefault(f => lstAccounts.Any(t => f.Account == t.AccountNumber))
                   where firstOrDefault != null
                   select new ReceiptDebit()
                   {
                       CheckColumn = false,
                       Date = c.Key.Date,
                       No = c.Key.No,
                       Reason = c.Key.Reason,
                       InvoiceNo = c.Key.InvoiceNo ?? "",
                       PostedDate = c.Key.PostedDate,
                       DueDate = (dsTypeSaInvoice.Any(d => d == c.Key.TypeID) && lstSaInvoice.Count(d => d.ID == c.Key.ReferenceID) == 1) ? lstSaInvoice.Single(d => d.ID == c.Key.ReferenceID).DueDate : null,
                       TypeName = lstTypes.Single(d => d.ID == c.Key.TypeID).TypeName,
                       TypeID = c.Key.TypeID,
                       SoPhaiThu = (decimal)c.Sum(t => Math.Round(t.DebitAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - c.Sum(t => Math.Round(t.CreditAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)),
                       SoPhaiThuQD = c.Sum(t => Math.Round(t.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero)) - c.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                       SoChuaThu = (decimal)c.Sum(t => Math.Round(t.DebitAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - c.Sum(t => Math.Round(t.CreditAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                           (decimal)(lstMbDepositDetailCustomers.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && lstAccounts.Any(t => t.AccountNumber == b.CreditAccount) && b.SaleInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) +
                                               lstMcReceiptDetailCustomers.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.SaleInvoiceID == c.Key.ReferenceID && lstAccounts.Any(t => t.AccountNumber == b.CreditAccount)).ToList().Sum(f => Math.Round(f.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) +
                                               lstExceptVoucher.Where(d => d.AccountingObjectID == c.Key.AccountingObjectID && lstAccounts.Any(t => t.AccountNumber == d.DebitAccount) && d.GLVoucherID == c.Key.ReferenceID).ToList().Sum(d => Math.Round(d.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))),
                       SoChuaThuQD = (decimal)((c.Sum(t => Math.Round(t.DebitAmount, lamtrondg)) - c.Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)) -
                           (decimal)(lstMbDepositDetailCustomers.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && lstAccounts.Any(t => t.AccountNumber == b.CreditAccount) && b.SaleInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) +
                                               lstMcReceiptDetailCustomers.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.SaleInvoiceID == c.Key.ReferenceID && lstAccounts.Any(t => t.AccountNumber == b.CreditAccount)).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero))))), // edit by tungnt: k can lay tu bang exceptvoucher

                       SoDaThu = (decimal)c.Sum(t => Math.Round(t.DebitAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - c.Sum(t => Math.Round(t.CreditAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - (decimal)c.Sum(t => Math.Round(t.DebitAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) - c.Sum(t => Math.Round(t.CreditAmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) -
                           (decimal)(lstMbDepositDetailCustomers.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && lstAccounts.Any(t => t.AccountNumber == b.CreditAccount) && b.SaleInvoiceID == c.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) +
                                               lstMcReceiptDetailCustomers.Where(b => b.AccountingObjectID == c.Key.AccountingObjectID && b.SaleInvoiceID == c.Key.ReferenceID && lstAccounts.Any(t => t.AccountNumber == b.CreditAccount)).ToList().Sum(f => Math.Round(f.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero)) +
                                               lstExceptVoucher.Where(d => d.AccountingObjectID == c.Key.AccountingObjectID && lstAccounts.Any(t => t.AccountNumber == d.DebitAccount) && d.GLVoucherID == c.Key.ReferenceID).ToList().Sum(d => Math.Round(d.AmountOriginal, (c.Key.CurrencyID == "VND" ? lamtrondg : lamtrondg1), MidpointRounding.AwayFromZero))),

                       CreditAccount = firstOrDefault.Account,
                       EmployeeID = c.Key.EmployeeID,
                       AccountingObjectCode = c.Key.EmployeeID == null ? "" : lstAccountingObjectsEmployee.Count(d => d.ID == c.Key.EmployeeID) == 1 ? lstAccountingObjectsEmployee.Single(d => d.ID == c.Key.EmployeeID).AccountingObjectCode : "",
                       AccountingObjectName = c.Key.EmployeeID == null ? "" : lstAccountingObjectsEmployee.Count == 1 ? lstAccountingObjectsEmployee.Single(d => d.ID == c.Key.EmployeeID).AccountingObjectName : "",
                       PaymentClauseID = dsTypeSaInvoice.All(d => d != c.Key.TypeID) ? null : (lstSaInvoice.Count(d => d.ID == c.Key.ReferenceID) == 1 ? lstSaInvoice.Single(d => d.ID == c.Key.ReferenceID).PaymentClauseID : null),
                       RefVoucherExchangeRate = c.Key.ExchangeRate,
                       LastExchangeRate = lstgo.Any(x => x.CurrencyID == c.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == c.Key.ReferenceID)) ?
                       lstgo.Where(x => x.CurrencyID == c.Key.CurrencyID && x.VoucherDetailForeignCurrencys.Any(d => d.ReferenceID == c.Key.ReferenceID)).OrderByDescending(d => d.DateReevaluate)
                       .FirstOrDefault().VoucherDetailForeignCurrencys.FirstOrDefault(x => x.ReferenceID == c.Key.ReferenceID).NewExchangeRate : 0,
                       DifferAmount = 0,
                       SoThu = 0,
                       SoThuQD = 0,
                       DiscountAmount = 0,
                       DiscountAmountOriginal = 0,
                       CurrencyID = c.Key.CurrencyID,
                       SaleInvoiceID = c.Key.ReferenceID
                   }).ToList()
                 }).ToList();

            try
            {
                foreach (CollectionCustomer payVendor in lstGroupAccountingObjects)
                {

                    //if (payVendor.SoConPhaiThu != 0)
                    //{
                    for (int i = payVendor.ReceiptDebits.Count - 1; i >= 0; i--)
                    {
                        if (payVendor.ReceiptDebits[i].LastExchangeRate != 0)
                        {
                            payVendor.ReceiptDebits[i].SoChuaThuQD = payVendor.ReceiptDebits[i].SoChuaThu * payVendor.ReceiptDebits[i].LastExchangeRate;
                        }
                        if (payVendor.ReceiptDebits[i].SoPhaiThu < 0)
                        {
                            payVendor.ReceiptDebits.RemoveAt(i);
                        }
                        else
                        {
                            payVendor.ReceiptDebits[i].SoDaThu = ((payVendor.ReceiptDebits[i].SoPhaiThu - payVendor.ReceiptDebits[i].SoChuaThu) * payVendor.ReceiptDebits[i].RefVoucherExchangeRate ?? 1);
                        }


                    }
                    //payVendor.SoDaThu = payVendor.ReceiptDebits.Sum(x => x.SoDaThu);
                    payVendor.SoPhatSinh = payVendor.ReceiptDebits.Where(x => x.TypeID != 701 && (x.Date >= beginDate && x.Date <= endDate)).Sum(x => x.SoPhaiThuQD);
                    payVendor.SoConPhaiThu = payVendor.SoDuDauNam + payVendor.SoPhatSinh - payVendor.SoDaThu;

                    //payVendor.SoConPhaiThu = payVendor.ReceiptDebits.Sum(x => x.SoChuaThuQD);
                    //payVendor.SoPhatSinh = payVendor.SoConPhaiThu + payVendor.SoDaThu - payVendor.SoDuDauNam;
                    payVendor.ReceiptDebits.OrderByDescending(c => c.PostedDate).ThenBy(c => c.SoChuaThu).ToList();
                    //}
                }
            }
            catch (Exception ex)
            {

            }
            return lstGroupAccountingObjects.Where(x => (x.SoDuDauNam != 0 || x.SoPhatSinh != 0 || x.SoDaThu != 0)).ToList();

        }
        #endregion        

        #region Thông báo công nợ Chuongnv
        public List<ListLibitiesReport> ListLibitiesReport(DateTime dtNgay, DateTime dtDenNgay, Currency currency)
        {
            // danh sách đã thu tiền trong năm nay
            int lamtrondg = 0;
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
            DateTime dt = dtDenNgay.AddDays(1);
            List<MBDepositDetailCustomer> lstMbDepositDetailCustomers = IMBDepositDetailCustomerService.Query.Where(c => _iMBDepositService.Query.Any(d => d.PostedDate <= dt)).ToList();
            List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = IMCReceiptDetailCustomerService.Query.Where(c => _iMCReceiptService.Query.Any(d => d.PostedDate <= dt)).ToList();
            List<ExceptVoucher> lstExceptVoucher = _IExceptVoucherService.GetAll();
            List<int> dsTypeSaInvoice = new List<int>() { 320, 323, 600, 701 };//danh sách type của bán hàng            
            List<int> dsType = new List<int>() { 101, 161 };//danh sách type của trả tiền
            List<Domain.Type> lstTypes = _iTypeService.GetAll();
            List<PaymentClause> lstPaymentClauses = IoC.Resolve<IPaymentClauseService>().GetIsActive(true);
            List<AccountingObject> lstAccountingObjects = GetAccountingObjects(0);//danh sách khách hàng
            List<AccountingObject> lstAccountingObjectsEmployee = GetAccountingObjects(2);//danh sách nhân viên
            List<Account> lstAccounts = _IAccountService.GetByDetailType(1);//danh sách tài khoản theo dõi chi tiết theo khách hàng
            List<string> lstStringAccount = new List<string> { "131", "1388", "6421" };
            List<GeneralLedger> lstGeneralLedger =
            _IGeneralLedgerService.Query.Where(c => c.AccountingObjectID != null && c.Account != null && c.CurrencyID != null).ToList().Where(c => lstAccountingObjects.Any(d => d.ID == c.AccountingObjectID) && (lstStringAccount.Any(d => c.Account.StartsWith(d))) && c.CurrencyID == currency.ID).ToList();
            List<SAInvoice> lstSaInvoice = _iSAInvoiceService.Query.Where(c => c.AccountingObjectID != null && c.TypeID != null && c.Recorded != null && c.CurrencyID != null).ToList().Where(c => lstAccountingObjects.Any(d => d.ID == c.AccountingObjectID) && (c.TypeID == 320 || c.TypeID == 323) && c.Recorded && c.CurrencyID == currency.ID).ToList();
            List<ListLibitiesReport> lst =
                (from c in lstGeneralLedger.ToList()
                 group c by c.AccountingObjectID into g
                 let guid = g.Key
                 where guid != null
                 select new ListLibitiesReport()
                 {
                     ID = guid.Value,
                     MaKhanhHang = lstAccountingObjects.Count(c => c.ID == guid.Value) == 1 ? lstAccountingObjects.Single(c => c.ID == guid.Value).AccountingObjectCode : "",
                     TenKhachHang = lstAccountingObjects.Count(c => c.ID == guid.Value) == 1 ? lstAccountingObjects.Single(c => c.ID == guid.Value).AccountingObjectName : "",
                     Address = lstAccountingObjects.Count(c => c.ID == guid.Value) == 1 ? lstAccountingObjects.Single(c => c.ID == guid.Value).Address : "",
                     SoPhatSinh = (lstGeneralLedger.Where(c => c.TypeID == 701 && c.AccountingObjectID == guid.Value).Sum(c => Math.Round(c.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(c.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero))),
                     SoDaThu = 0,
                     CongNoDenNgay = dtDenNgay.ToString("dd/MM/yyyy"),
                     CurrencyID = currency.ID,
                     TaxCode = lstAccountingObjects.Count(c => c.ID == guid.Value) == 1 ? lstAccountingObjects.Single(c => c.ID == guid.Value).TaxCode : "",
                     bill =
                  (from d in lstGeneralLedger.Where(c => dsTypeSaInvoice.Any(d => d == c.TypeID) && c.AccountingObjectID == guid.Value && c.PostedDate < dt).ToList()
                   group d by new { d.ReferenceID, d.Date, d.No, d.InvoiceNo, d.PostedDate, d.TypeID, d.AccountingObjectID, d.EmployeeID, d.CurrencyID, d.Reason, d.ExchangeRate, }
                                   into d
                   select new ListLibitiesReportDetail()
                   {
                       Date = d.Key.Date,
                       No = d.Key.No,
                       Reason = d.Key.No == "OPN" ? "Công nợ đầu kỳ" : d.Key.Reason,
                       PostedDate = d.Key.PostedDate,
                       SoDaThu = lstGeneralLedger.Where(x => dsType.Any(e => e == x.TypeID) && x.AccountingObjectID == guid.Value).Sum(t => Math.Round(t.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero)),
                       SoChuaThu = (decimal)(d.Sum(x => Math.Round(x.DebitAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(x.CreditAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) -
              (decimal)(lstMbDepositDetailCustomers.Where(b => b.SaleInvoiceID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) +
                                  lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) +
                                  lstExceptVoucher.Where(b => b.GLVoucherID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))),
                       SoChuaThuQD = (decimal)((d.Sum(x => Math.Round(x.DebitAmount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(x.CreditAmount, lamtrondg, MidpointRounding.AwayFromZero))) -
              (decimal)(lstMbDepositDetailCustomers.Where(b => b.SaleInvoiceID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) +
                                  lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)) +
                                  lstExceptVoucher.Where(b => b.GLVoucherID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.Amount, lamtrondg, MidpointRounding.AwayFromZero)))),
                       LaiNo = (lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID) != null ? (((lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID).DueDate != null) && ((lstPaymentClauses.Count(c => c.ID == lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID).PaymentClauseID) == 1 ? lstPaymentClauses.FirstOrDefault(b => b.ID == lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID).PaymentClauseID).PaymentClauseCode : null) != null)) ?
                            (((d.Sum(x => Math.Round(x.DebitAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(x.CreditAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) -
                                    (decimal)(lstMbDepositDetailCustomers.Where(b => b.SaleInvoiceID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) +
                                  lstMcReceiptDetailCustomers.Where(b => b.SaleInvoiceID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) +
                                  lstExceptVoucher.Where(b => b.GLVoucherID == d.Key.ReferenceID).ToList().Sum(f => Math.Round(f.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)))) *
                                     (dtDenNgay - lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID).DueDate.Value).Days * ((lstPaymentClauses.Count(a => a.ID == lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID).PaymentClauseID)) == 1 ? lstPaymentClauses.FirstOrDefault(a => a.ID == lstSaInvoice.FirstOrDefault(x => x.ID == d.Key.ReferenceID).PaymentClauseID).OverdueInterestPercent : 0) / 100) ?? 0 : 0) : 0),
                       CurrencyID = d.Key.CurrencyID,
                   }).ToList()
                 }).ToList();
            foreach (ListLibitiesReport payVendor in lst)
            {

                payVendor.SoDaThu = payVendor.bill.Sum(c => c.SoDaThu) ?? 0;
                payVendor.NoPhaiTra = (payVendor.bill.Sum(c => c.SoChuaThu) ?? 0);
                payVendor.LaiNoPhaiTra = payVendor.bill.Sum(c => c.LaiNo) ?? 0;
                payVendor.TongTien = (payVendor.NoPhaiTra + payVendor.LaiNoPhaiTra) ?? 0;
                payVendor.bill.OrderByDescending(c => c.PostedDate).ThenBy(c => c.SoChuaThu).ToList();
            }
            for (int i = lst.Count - 1; i >= 0; i--)
            {
                if (lst[i].TongTien <= 0)
                {
                    lst.RemoveAt(i);
                }
            }
            return lst.OrderBy(x => x.TenKhachHang).ToList();
        }
        #endregion

        #region Xử lý chênh lệch tỷ giá
        public GOtherVoucher HandlingExchangeRate(DateTime toDate, SolveExchangeRate solveExchangeRate)
        {
            DateTime fromdate = toDate.AddMonths(1).AddDays(-1);
            List<Currency> listCurrency = _ICurrencyService.Query.Where(c => c.ID != "VND").ToList();
            List<int> listTypeGetExchangeRate = new List<int> { 101, 161, 320 };
            List<int> listTypeSetExchangeRate = new List<int> { 210, 240, 174, 144, 134, 118, 128 };
            List<AccountingObject> listAccountingObjects = GetAccountingObjects(3);
            List<AccountingObject> listAccountingObjectsSupplier = GetAccountingObjects(1);
            List<AccountingObject> listAccountingObjectsCustomer = GetAccountingObjects(0);

            List<Account> lstAccountsCustomer = _IAccountService.GetByDetailType(1);//danh sách tài khoản theo dõi chi tiết theo khách hàng
            List<Account> lstAccountsSupplier = _IAccountService.GetByDetailType(0);//danh sách tài khoản theo dõi chi tiết theo nhà cung cấp
            List<Account> listAccounts = new List<Account>();
            listAccounts.AddRange(lstAccountsCustomer);
            listAccounts.AddRange(lstAccountsSupplier);

            List<GeneralLedger> listGeneralLedgersAll = _IGeneralLedgerService.GetAll()
                .Where(c => c.PostedDate <= fromdate && c.PostedDate >= new DateTime(toDate.Year, 1, 1) && c.AccountingObjectID != null && c.Account != null).ToList()
                .Where(c => listAccountingObjects.Any(d => d.ID == c.AccountingObjectID) && listAccounts.Any(e => e.AccountNumber == c.Account) && listCurrency.Any(b => b.ID == c.CurrencyID)).ToList();

            if (listGeneralLedgersAll.Count <= 0)
                return new GOtherVoucher();



            // danh sách đã thu tiền trong năm nay
            List<MBDepositDetailCustomer> lstMbDepositDetailCustomers = _iMBDepositService.Query.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1)).ToList().SelectMany(c => c.MBDepositDetailCustomers).ToList();
            List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = _iMCReceiptService.Query.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1)).ToList().SelectMany(c => c.MCReceiptDetailCustomers).ToList();

            List<ExceptVoucher> lstExceptVoucher = _IExceptVoucherService.Query.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1)).ToList();

            List<MBTellerPaperDetailVendor> lstMbTellerPaper = _IMBTellerPaper.Query.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1)).SelectMany(c => c.MBTellerPaperDetailVendors).ToList();
            List<MBCreditCardDetailVendor> lstMbCreditCard = _IMBCreditCard.Query.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1)).SelectMany(c => c.MBCreditCardDetailVendors).ToList();
            List<MCPaymentDetailVendor> lstMcPayment = _IMCPaymentService.Query.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1)).SelectMany(c => c.MCPaymentDetailVendors).ToList();

            #region
            GOtherVoucher gOtherVoucher = new GOtherVoucher()
            {
                ID = Guid.NewGuid(),
                TypeID = 600,
                PostedDate = toDate,
                Date = toDate,
                //No = no, ra form thì thêm
                Reason = "Tỷ giá xuất quỹ",
                TotalAmount = 0,
                TotalAmountOriginal = 0,
                Recorded = false,
                Exported = true,
            };
            #endregion

            var firstOrDefault = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLai");
            var systemOption = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "TCKHAC_TKCLechLo");
            var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TyGia");



            if (firstOrDefault != null && systemOption != null && ddSoTyGia != null)
            {
                string accountTkcLechLai = firstOrDefault.Data;
                string accountTkcLechLo = systemOption.Data;
                int ngoaite = int.Parse(ddSoTyGia.Data);


                solveExchangeRate = new SolveExchangeRate()
                {
                    AccountInterest = accountTkcLechLai,
                    AccountDeficit = accountTkcLechLo,
                    PostedDate = solveExchangeRate.PostedDate,
                    Date = solveExchangeRate.Date,
                    No = solveExchangeRate.No,
                    Reason = "Xử lý chênh lệch tỷ giá"
                };
                foreach (GeneralLedger gl in listGeneralLedgersAll.Where(c => c.PostedDate >= new DateTime(toDate.Year, 1, 1) && c.TypeID != 701))
                {
                    if (listAccountingObjectsCustomer.Any(c => c.ID == gl.AccountingObjectID) &&
                        lstAccountsCustomer.Any(c => c.AccountNumber == gl.Account)) // khách hàng
                    {
                        decimal soDaThu = (lstMbDepositDetailCustomers.Where(
                            b => b.AccountingObjectID == gl.AccountingObjectID && b.CreditAccount == gl.Account &&
                                b.SaleInvoiceID == gl.DetailID).ToList().Sum(f => f.AmountOriginal) -
                         lstMcReceiptDetailCustomers.Where(
                             b => b.AccountingObjectID == gl.AccountingObjectID && b.SaleInvoiceID == gl.DetailID)
                             .ToList()
                             .Sum(f => f.AmountOriginal) -
                         lstExceptVoucher.Where(
                             d => d.AccountingObjectID == gl.AccountingObjectID && d.DebitAccount == gl.Account &&
                                 d.GLVoucherID == gl.ReferenceID).ToList().Sum(d => d.AmountOriginal));//nguyên tệ
                        decimal soDaThuVnd = (lstMbDepositDetailCustomers.Where(
                                b => b.AccountingObjectID == gl.AccountingObjectID && b.CreditAccount == gl.Account &&
                                    b.SaleInvoiceID == gl.ReferenceID).ToList().Sum(f => f.Amount) -
                             lstMcReceiptDetailCustomers.Where(
                                 b => b.AccountingObjectID == gl.AccountingObjectID && b.SaleInvoiceID == gl.ReferenceID)
                                 .ToList()
                                 .Sum(f => f.Amount) -
                             lstExceptVoucher.Where(
                                 d => d.AccountingObjectID == gl.AccountingObjectID && d.DebitAccount == gl.Account &&
                                     d.GLVoucherID == gl.ReferenceID).ToList().Sum(d => d.Amount));
                        decimal soNoVnd = (decimal)((gl.DebitAmount > soDaThu ? soDaThu : gl.DebitAmount) * gl.ExchangeRate);//nếu số tiền nguyên tệ đâ thu nhỏ hơn số tiền nợ nguyên tệ thì ta chỉ tính số tiền đã trả nguyên tệ
                        if (soDaThuVnd > soNoVnd) // dư có
                        {
                            SolveExchangeRateDetail solveExchangeRateDetail = new SolveExchangeRateDetail()
                            {
                                Status = false,
                                AccountNumber = gl.Account, // Tài khoản
                                Date = gl.PostedDate, // ngày chứng từ
                                VoucherNo = gl.No, // số chứng từ
                                InvoiceNo = gl.InvoiceNo,// số hóa đơn
                                CurrencyId = gl.CurrencyID, // loại tiền tệ
                                AmountBanlace = soDaThuVnd - soNoVnd,// Số dư có
                                AmountDeficit = 0, // số dư nợ
                                AccountingObjectID = gl.AccountingObjectID ?? new Guid(),
                                AccountingObjectCode = listAccountingObjectsCustomer.Count(c => c.ID == gl.AccountingObjectID) == 1 ? listAccountingObjectsCustomer.Single(c => c.ID == gl.AccountingObjectID).AccountingObjectCode : ""
                            };
                            solveExchangeRate.ListSolveExchangeRateDetails.Add(solveExchangeRateDetail);
                        }
                        else if (soDaThuVnd < soNoVnd)
                        {
                            SolveExchangeRateDetail solveExchangeRateDetail = new SolveExchangeRateDetail()
                            {
                                Status = false,
                                AccountNumber = gl.Account, // Tài khoản
                                Date = gl.PostedDate, // ngày chứng từ
                                VoucherNo = gl.No, // số chứng từ
                                InvoiceNo = gl.InvoiceNo,// số hóa đơn
                                CurrencyId = gl.CurrencyID, // loại tiền tệ
                                AmountBanlace = 0,// Số dư có
                                AmountDeficit = soNoVnd - soDaThuVnd, // số dư nợ
                                AccountingObjectID = gl.AccountingObjectID ?? new Guid(),
                                AccountingObjectCode = listAccountingObjectsCustomer.Count(c => c.ID == gl.AccountingObjectID) == 1 ? listAccountingObjectsCustomer.Single(c => c.ID == gl.AccountingObjectID).AccountingObjectCode : ""
                            };
                            solveExchangeRate.ListSolveExchangeRateDetails.Add(solveExchangeRateDetail);
                        }

                    }
                    else if (listAccountingObjectsSupplier.Any(c => c.ID == gl.AccountingObjectID) &&
                        lstAccountsSupplier.Any(c => c.AccountNumber == gl.Account)) // nhà cung cấp
                    {
                        decimal soDaTra = (decimal)(lstMbCreditCard.Where(
                            b => b.AccountingObjectID == gl.AccountingObjectID && b.DebitAccount == gl.Account &&
                                b.PPInvoiceID == gl.DetailID).ToList().Sum(f => f.Amount)
                                          -
                                          lstMbTellerPaper.Where(
                                              b => b.AccountingObjectID == gl.AccountingObjectID && b.PPInvoiceID == gl.DetailID)
                                              .ToList()
                                              .Sum(f => f.Amount)
                                          -
                                          lstMcPayment.Where(
                                              b => b.AccountingObjectID == gl.AccountingObjectID && b.PPInvoiceID == gl.DetailID)
                                              .ToList()
                                              .Sum(f => f.Amount)
                                          -
                                          lstExceptVoucher.Where(
                                              d =>
                                                  d.AccountingObjectID == gl.AccountingObjectID && d.DebitAccount == gl.Account &&
                                                  d.GLVoucherID == gl.ReferenceID).ToList().Sum(d => d.Amount) ?? 0);
                        decimal soDaTraVnd = (decimal)(lstMbCreditCard.Where(
                            b => b.AccountingObjectID == gl.AccountingObjectID && b.DebitAccount == gl.Account &&
                                b.PPInvoiceID == gl.DetailID).ToList().Sum(f => f.AmountOriginal)
                                          -
                                          lstMbTellerPaper.Where(
                                              b => b.AccountingObjectID == gl.AccountingObjectID && b.PPInvoiceID == gl.DetailID)
                                              .ToList()
                                              .Sum(f => f.AmountOriginal)
                                          -
                                          lstMcPayment.Where(
                                              b => b.AccountingObjectID == gl.AccountingObjectID && b.PPInvoiceID == gl.DetailID)
                                              .ToList()
                                              .Sum(f => f.AmountOriginal)
                                          -
                                          lstExceptVoucher.Where(
                                              d =>
                                                  d.AccountingObjectID == gl.AccountingObjectID && d.DebitAccount == gl.Account &&
                                                  d.GLVoucherID == gl.ReferenceID).ToList().Sum(d => d.AmountOriginal) ?? 0);
                        decimal soNoVnd = (decimal)((gl.CreditAmount > soDaTra ? soDaTra : gl.CreditAmount) * gl.ExchangeRate);
                        if (soDaTraVnd < soNoVnd) // dư có
                        {
                            SolveExchangeRateDetail solveExchangeRateDetail = new SolveExchangeRateDetail()
                            {
                                Status = false,
                                AccountNumber = gl.Account, // Tài khoản
                                Date = gl.PostedDate, // ngày chứng từ
                                VoucherNo = gl.No, // số chứng từ
                                InvoiceNo = gl.InvoiceNo,// số hóa đơn
                                CurrencyId = gl.CurrencyID, // loại tiền tệ
                                AmountBanlace = soNoVnd - soDaTraVnd,// Số dư có
                                AmountDeficit = 0, // số dư nợ
                                AccountingObjectID = gl.AccountingObjectID ?? new Guid(),
                                AccountingObjectCode = listAccountingObjectsCustomer.Count(c => c.ID == gl.AccountingObjectID) == 1 ? listAccountingObjectsCustomer.Single(c => c.ID == gl.AccountingObjectID).AccountingObjectCode : ""
                            };
                            solveExchangeRate.ListSolveExchangeRateDetails.Add(solveExchangeRateDetail);
                        }
                        else if (soDaTraVnd > soNoVnd)
                        {
                            SolveExchangeRateDetail solveExchangeRateDetail = new SolveExchangeRateDetail()
                            {
                                Status = false,
                                AccountNumber = gl.Account, // Tài khoản
                                Date = gl.PostedDate, // ngày chứng từ
                                VoucherNo = gl.No, // số chứng từ
                                InvoiceNo = gl.InvoiceNo,// số hóa đơn
                                CurrencyId = gl.CurrencyID, // loại tiền tệ
                                AmountBanlace = 0,// Số dư có
                                AmountDeficit = soDaTraVnd - soNoVnd, // số dư nợ
                                AccountingObjectID = gl.AccountingObjectID ?? new Guid(),
                                AccountingObjectCode = listAccountingObjectsCustomer.Count(c => c.ID == gl.AccountingObjectID) == 1 ? listAccountingObjectsCustomer.Single(c => c.ID == gl.AccountingObjectID).AccountingObjectCode : ""
                            };
                            solveExchangeRate.ListSolveExchangeRateDetails.Add(solveExchangeRateDetail);
                        }

                    }

                }
                #region
                //var t = (from generalLedger in listGeneralLedgersAll
                //         group generalLedger by generalLedger.AccountingObjectID into g
                //         select new
                //         {
                //             KhachHang = (g.Where(c => c.PostedDate >=  new DateTime(toDate.Year,1,1) && c.TypeID != 701).Select(c => new
                //             {
                //                 ExchangeRate = c.ExchangeRate,
                //                 CheckColumn = false,
                //                 Date = c.Date,
                //                 No = c.No,
                //                 InvoiceNo = c.InvoiceNo ?? "",
                //                 PostedDate = c.PostedDate,
                //                 TypeID = c.TypeID,
                //                 SoPhatSinh = c.DebitAmount,
                //                 SoDaThu = (decimal)(lstMbDepositDetailCustomers.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.CreditAccount == c.Account && b.SaleInvoiceID == c.DetailID).ToList().Sum(f => f.Amount) -
                //                                     lstMcReceiptDetailCustomers.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.SaleInvoiceID == c.DetailID).ToList().Sum(f => f.Amount) -
                //                                     lstExceptVoucher.Where(d => d.AccountingObjectID == c.AccountingObjectID && d.DebitAccount == c.Account && d.GLVoucherID == c.ReferenceID).ToList().Sum(d => d.Amount)),
                //                 SoPhaiThu = c.DebitAmount,
                //                 SoPhaiThuQD = (decimal)(c.DebitAmount * c.ExchangeRate),
                //                 SoChuaThu =
                //                 c.DebitAmount - (decimal)
                //                 (
                //                     lstMbDepositDetailCustomers.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.CreditAccount == c.Account && b.SaleInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                     lstMcReceiptDetailCustomers.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.SaleInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                     lstExceptVoucher.Where(d => d.AccountingObjectID == c.AccountingObjectID && d.DebitAccount == c.Account && d.GLVoucherID == c.ReferenceID).ToList().Sum(d => d.Amount)
                //                 )
                //                 ,

                //                 SoChuaThuQD = (decimal)
                //                 (
                //                 c.DebitAmount - (decimal)
                //                 (
                //                     lstMbDepositDetailCustomers.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.CreditAccount == c.Account && b.SaleInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                     lstMcReceiptDetailCustomers.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.SaleInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                     lstExceptVoucher.Where(d => d.AccountingObjectID == c.AccountingObjectID && d.DebitAccount == c.Account && d.GLVoucherID == c.ReferenceID).ToList().Sum(d => d.Amount)
                //                 )
                //                 ) * c.ExchangeRate,

                //                 CreditAccount = c.Account,
                //                 CurrencyID = c.CurrencyID,
                //             })).ToList(),
                //             NhaCungCap = (g.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + toDate) && c.TypeID != 701).Select(c => new
                //             {
                //                 ExchangeRate = c.ExchangeRate,
                //                 CheckColumn = false,
                //                 Date = c.PostedDate,
                //                 No = c.No,
                //                 InvoiceNo = c.InvoiceNo,
                //                 SoPhatSinh = c.CreditAmount,
                //                 SoPhatSinhQĐ = c.CreditAmountOriginal,
                //                 SoDaTra =
                //                 c.CreditAmount -
                //                 (
                //                    lstMbCreditCard.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.DebitAccount == c.Account && b.PPInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                    lstMbTellerPaper.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.PPInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                    lstMcPayment.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.PPInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                    lstExceptVoucher.Where(d => d.AccountingObjectID == c.AccountingObjectID && d.DebitAccount == c.Account && d.GLVoucherID == c.ReferenceID).ToList().Sum(d => d.Amount)
                //                 ),
                //                 SoDaTraQĐ =
                //                 (c.CreditAmount -
                //                 (
                //                    lstMbCreditCard.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.DebitAccount == c.Account && b.PPInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                    lstMbTellerPaper.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.PPInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                    lstMcPayment.Where(b => b.AccountingObjectID == c.AccountingObjectID && b.PPInvoiceID == c.DetailID).ToList().Sum(f => f.Amount)
                //                 -
                //                    lstExceptVoucher.Where(d => d.AccountingObjectID == c.AccountingObjectID && d.DebitAccount == c.Account && d.GLVoucherID == c.ReferenceID).ToList().Sum(d => d.Amount)
                //                 )) * c.ExchangeRate,
                //                 Account = c.Account,
                //                 CurrencyID = c.CurrencyID
                //             })).ToList(),
                //             Bill2 = g.Where(c => c.PostedDate >= DateTime.Parse("1/1/" + toDate) && c.TypeID != 701).Select(c => new SolveExchangeRateDetail
                //             {
                //                 Status = false,
                //                 AccountNumber = c.Account,
                //                 Date = c.PostedDate,
                //                 VoucherNo = c.No,
                //                 CurrencyId = c.CurrencyID,
                //                 AmountBanlace = 0,
                //                 AmountDeficit = 0,
                //                 AccountingObjectID = c.AccountingObjectID ?? new Guid()
                //             }).ToList()
                //         }
                //    ).ToList();

                #endregion

                #region

                //foreach (Currency currency in listCurrency)
                //{
                //    if (currency.ID == "VND")
                //        continue;



                //    #region tạo chi tiết chứng từ nghiệp vụ khác
                //    decimal updatingExchangeRate =
                //        ((decimal)(listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < toDate).Sum(c => c.DebitAmount) //nhập kỳ trước
                //                   - listGeneralLedgersSet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < toDate).Sum(c => c.CreditAmount) //xuất kỳ trước
                //                   + listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate >= toDate).Sum(c => c.DebitAmount) //nhập trong kỳ này

                //            )) /
                //        ((decimal)(listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < toDate).Sum(c => c.DebitAmountOriginal) //nhập kỳ trước
                //                        - listGeneralLedgersSet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < toDate).Sum(c => c.CreditAmountOriginal) //xuất kỳ trước
                //                        + listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate >= toDate).Sum(c => c.DebitAmountOriginal) //nhập trong kỳ này
                //                        )
                //             == 0
                //            ? 1
                //            : (decimal)(listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < toDate).Sum(c => c.DebitAmountOriginal) //nhập kỳ trước
                //                        - listGeneralLedgersSet.Where(c => c.CurrencyID == currency.ID && c.PostedDate < toDate).Sum(c => c.CreditAmountOriginal) //xuất kỳ trước
                //                        + listGeneralLedgersGet.Where(c => c.CurrencyID == currency.ID && c.PostedDate >= toDate).Sum(c => c.DebitAmountOriginal) //nhập trong kỳ này
                //                ));
                //    updatingExchangeRate = Math.Round(updatingExchangeRate, ngoaite);
                //    Currency currency1 = currency;



                //    var group = (from t in listGeneralLedgersSet.Where(c => c.CurrencyID == currency1.ID && c.PostedDate >= toDate)
                //                 where t.CreditAmount > 0 && t.CreditAmountOriginal > 0
                //                 group t by t.Account
                //                     into g
                //                     select new { g.Key, g }).ToList();
                //    foreach (var t in @group)
                //    {
                //        decimal? amount =
                //            t.g.Select(c => new { sum = (updatingExchangeRate - c.ExchangeRate) * c.CreditAmount })
                //                .Sum(c => c.sum);
                //        GOtherVoucherDetail gOtherVoucherDetail = new GOtherVoucherDetail
                //        {
                //            GOtherVoucherID = gOtherVoucher.ID,
                //            Description = "Xử lý chênh lệch tỷ giá",
                //            DebitAccount = t.Key,
                //            CreditAccount = amount > 0 ? accountTkcLechLai : accountTkcLechLo,
                //            CurrencyID = currency.ID,
                //            ExchangeRate = updatingExchangeRate,
                //            Amount = 0,
                //            AmountOriginal = Math.Abs(amount ?? 0),
                //            IsMatch = false
                //        };
                //        gOtherVoucher.GOtherVoucherDetails.Add(gOtherVoucherDetail);
                //    }
                //    #endregion
                //}

                #endregion


            }
            gOtherVoucher.TotalAmountOriginal = gOtherVoucher.GOtherVoucherDetails.Sum(c => c.AmountOriginal);
            return gOtherVoucher;
        }

        #endregion
        public List<AccountingObject> GetListAccountingObjectOrderCodeByDepartment(Guid? id)
        {
            return Query.Where(x => x.DepartmentID == id).OrderBy(k => k.AccountingObjectCode).ToList();
        }
    }

}