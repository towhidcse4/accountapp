using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;


namespace Accounting.Core.ServiceImp
{
    public class EMDevidendDetailService: BaseService<EMDevidendDetail ,Guid>,IEMDevidendDetailService
    {
        public EMDevidendDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}