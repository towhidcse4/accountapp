﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class MCReceiptDetailCustomerService : BaseService<MCReceiptDetailCustomer, Guid>, IMCReceiptDetailCustomerService
    {
        public MCReceiptDetailCustomerService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// Lấy danh sách các MCReceiptDetailCustomer theo SaleInvoiceID
        /// [DUYTN]
        /// </summary>
        /// <param name="SaleInvoiceID">là SaleInvoiceID truyền vào</param>
        /// <returns>tập các MCReceiptDetailCustomer, null nếu bị lỗi</returns>
        public List<MCReceiptDetailCustomer> GetAll_BySaleInvoiceID(Guid SaleInvoiceID)
        {
            return Query.Where(k => k.SaleInvoiceID == SaleInvoiceID).ToList();
        }

        /// <summary>
        /// Đếm tổng số dòng dữ liệu MCReceiptDetailCustomer theo ByRemainingAmount
        /// [DUYTN]
        /// </summary>
        /// <param name="RemainingAmount">là ByRemainingAmount truyền vào</param>
        /// <returns>tập các MCReceiptDetailCustomer, null nếu bị lỗi</returns>
        public int CountByRemainingAmount(int RemainingAmount)
        {
            return Query.Count(p => p.ReceipableAmount == RemainingAmount);
        }
    }

}