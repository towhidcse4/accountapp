﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;
using FX.Core;
using System.ComponentModel;

namespace Accounting.Core.ServiceImp
{
    public class RSInwardOutwardService : BaseService<RSInwardOutward, Guid>, IRSInwardOutwardService
    {
        public RSInwardOutwardService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IRepositoryLedgerService _IRepositoryLedgerService { get { return IoC.Resolve<IRepositoryLedgerService>(); } }
        public IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public ISAInvoiceService _ISAInvoiceService { get { return IoC.Resolve<ISAInvoiceService>(); } }
        public ISAReturnService _ISAReturnService { get { return IoC.Resolve<ISAReturnService>(); } }
        public ISAReturnDetailService _ISAReturnDetailService { get { return IoC.Resolve<ISAReturnDetailService>(); } }
        public IRSTransferService _IRSTransferService { get { return IoC.Resolve<IRSTransferService>(); } }


        public RSInwardOutward getMCRSInwardOutwardbyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
        /// <summary>
        /// Lấy danh sách Recorded từ RSInwardOutward
        ///[Longtx]
        /// </summary>
        /// <returns>Trả về kiểu bool</returns>;
        public List<bool> GetRecorded()
        {
            return Query.Select(p => p.Recorded).ToList();
        }

        #region Lưu bảng nhập xuất kho

        #region Form Hàng mua trả lại
        /// <summary>
        /// Lấy DS để lưu vào bảng RSInwardOutward [Form Hàng mua trả lại] 
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="tempRSOutward">truyền bổ sung các trường [ContactName]|[Reason]|[MaterialQuantity]</param>
        /// <returns></returns>
        public RSInwardOutward GetSaveRSInwardOutward(PPDiscountReturn temp, RSInwardOutward tempRSOutward, bool isAdd = true, RSInwardOutward tempRSInwardOutward = null)
        {

            if (isAdd)
                tempRSInwardOutward = new RSInwardOutward { ID = Guid.NewGuid() };
            else if (tempRSInwardOutward == null)
                tempRSInwardOutward = getMCRSInwardOutwardbyNo(temp.OutwardNo);
            tempRSInwardOutward.BranchID = null;
            tempRSInwardOutward.TypeID = temp.TypeID;
            tempRSInwardOutward.Date = (DateTime)temp.ODate;
            tempRSInwardOutward.PostedDate = (DateTime)temp.OPostedDate;
            tempRSInwardOutward.No = temp.OutwardNo;
            tempRSInwardOutward.AccountingObjectID = temp.AccountingObjectID;
            tempRSInwardOutward.AccountingObjectName = temp.AccountingObjectName;
            tempRSInwardOutward.AccountingObjectAddress = temp.AccountingObjectAddress;
            tempRSInwardOutward.ContactName = tempRSOutward.ContactName; //
            tempRSInwardOutward.Reason = tempRSOutward.Reason; //
            tempRSInwardOutward.CurrencyID = temp.CurrencyID;
            tempRSInwardOutward.ExchangeRate = temp.ExchangeRate;
            tempRSInwardOutward.EmployeeID = temp.EmployeeID;
            tempRSInwardOutward.OriginalNo = temp.OriginalNo;
            tempRSInwardOutward.TotalAmount = temp.TotalAmount;
            tempRSInwardOutward.TotalAmountOriginal = temp.TotalAmountOriginal;
            tempRSInwardOutward.MaterialQuantity = tempRSOutward.MaterialQuantity; //
            tempRSInwardOutward.TemplateID = temp.TemplateID;
            tempRSInwardOutward.Recorded = temp.Recorded;
            tempRSInwardOutward.Exported = temp.Exported;
            tempRSInwardOutward.IsInwardOutwardType = 1;
            tempRSInwardOutward.IsOutwardSAInvoice = false;
            return tempRSInwardOutward;
        }
        /// <summary>
        /// Hàm thêm vào bảng RSInwardOutward và bảng RSInwardOutwardDetail của Form hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="listTempDetail">Ds PPDiscountReturnDetail theo đối tượng PPDiscountReturn</param>
        /// <param name="tempRSOutward">truyền bổ sung các trường [ContactName]|[Reason]|[MaterialQuantity]</param>
        public void AddRSInwardOutward(RSInwardOutward tempRSOutward)
        {
            CreateNew(tempRSOutward);
        }
        /// <summary>
        /// Hàm thêm vào bảng RSInwardOutward và bảng RSInwardOutwardDetail của Form hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="listTempDetail">Ds PPDiscountReturnDetail theo đối tượng PPDiscountReturn</param>
        /// <param name="tempRSOutward">truyền bổ sung các trường [ContactName]|[Reason]|[MaterialQuantity]</param>
        public void EditRSInwardOutward(RSInwardOutward tempRSOutward)
        {
            Update(tempRSOutward);
        }



        #endregion

        #endregion

        #region Cập nhật giá nhập kho thành phẩm
        public bool GetListInwardOutwardForUpdatingPrice(List<MaterialGoods> lstMaterialGoodses, List<Repository> lstRepositories, DateTime fromDate, DateTime toDate, bool isdd)
        {
            List<int> lstTypeIW = new List<int>() { 400, 401 };
            List<RSInwardOutward> lstRSInwardOutwards = (from iw in Query.ToList()
                                                         where iw.PostedDate >= fromDate && iw.PostedDate <= toDate
                                                               && iw.Recorded && iw.RSInwardOutwardDetails.Any(
                                                                   c => c.DebitAccount.StartsWith("15") && c.CreditAccount.StartsWith("154")
                                                                         && c.MaterialGoodsID != null &&
                                                                        (lstRepositories.Any(d => d.ID == c.RepositoryID) || c.RepositoryID == null) &&
                                                                        lstMaterialGoodses.Any(d => d.ID == c.MaterialGoodsID) && lstTypeIW.Any(d => d == c.TypeID))
                                                         select iw).ToList();
            BeginTran();
            try
            {
                foreach (RSInwardOutward inwardOutward in lstRSInwardOutwards)
                {
                    foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                    {
                        if (lstMaterialGoodses.Any(c => c.ID == inwardOutwardDetail.MaterialGoodsID) && (lstRepositories.Any(d => d.ID == inwardOutwardDetail.RepositoryID) || inwardOutwardDetail.RepositoryID == null))
                        {
                            inwardOutwardDetail.UnitPriceOriginal =
                          lstMaterialGoodses.Count(c => c.ID == inwardOutwardDetail.MaterialGoodsID) == 1
                              ? lstMaterialGoodses.Single(c => c.ID == inwardOutwardDetail.MaterialGoodsID).Dgchuyendoi
                              : inwardOutwardDetail.UnitPriceOriginal;
                            inwardOutwardDetail.UnitPrice = inwardOutwardDetail.UnitPriceOriginal * (inwardOutward.ExchangeRate ?? 1);
                            inwardOutwardDetail.AmountOriginal = inwardOutwardDetail.UnitPriceOriginal * (inwardOutwardDetail.Quantity ?? 1);
                            inwardOutwardDetail.Amount = inwardOutwardDetail.AmountOriginal * (inwardOutward.ExchangeRate ?? 1);
                        }
                    }
                    inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                    inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                    Update(inwardOutward);

                    List<GeneralLedger> lstgeneralLedger = _IGeneralLedgerService.Query.Where(c => c.ReferenceID == inwardOutward.ID && c.TypeID == inwardOutward.TypeID).ToList();
                    foreach(var generalLedger in lstgeneralLedger)
                    {
                        if (generalLedger != null &&
                        (generalLedger.DetailID != null
                        && lstRSInwardOutwards.Count(c => Enumerable.Count<RSInwardOutwardDetail>(c.RSInwardOutwardDetails, b => b.ID == generalLedger.DetailID) == 1) == 1))
                        {
                            RSInwardOutwardDetail rsInwardOutwardDetail =
                                lstRSInwardOutwards.Single(c => c.ID == generalLedger.ReferenceID && c.TypeID == generalLedger.TypeID).RSInwardOutwardDetails.Single(c => c.ID == generalLedger.DetailID);
                            if (rsInwardOutwardDetail.CreditAccount == generalLedger.Account)
                            {
                                generalLedger.CreditAmountOriginal = rsInwardOutwardDetail.AmountOriginal;
                                generalLedger.CreditAmount = rsInwardOutwardDetail.Amount;
                            }
                            else if (rsInwardOutwardDetail.DebitAccount == generalLedger.Account)
                            {
                                generalLedger.DebitAmountOriginal = rsInwardOutwardDetail.AmountOriginal;
                                generalLedger.DebitAmount = rsInwardOutwardDetail.Amount;
                            }
                            _IGeneralLedgerService.Update(generalLedger);
                        }
                    }                    
                    RepositoryLedger repositoryLedger =
                        _IRepositoryLedgerService.Query.FirstOrDefault(c => c.ReferenceID == inwardOutward.ID && c.TypeID == inwardOutward.TypeID);
                    if (repositoryLedger != null && lstMaterialGoodses.Any(c => c.ID == repositoryLedger.MaterialGoodsID) && lstRepositories.Any(c => c.ID == repositoryLedger.RepositoryID))
                    {
                        repositoryLedger.UnitPrice = lstMaterialGoodses.Count(c => c.ID == repositoryLedger.MaterialGoodsID) == 1
                                  ? lstMaterialGoodses.Single(c => c.ID == repositoryLedger.MaterialGoodsID).Dgchuyendoi
                                  : repositoryLedger.UnitPrice;
                        repositoryLedger.IWAmount = repositoryLedger.UnitPrice * repositoryLedger.IWQuantity;
                        _IRepositoryLedgerService.Update(repositoryLedger);
                    }
                }
                #region Update Xuất kho
                if (isdd)
                {
                    List<int> lstTypeOW = new List<int>() { 410, 411, 412, 415, 414 };
                    List<int> lstTypeSA = new List<int>() { 320, 321, 322, 323, 324, 325, 420 };
                    List<RSInwardOutward> lstRSOutwards = (from iw in Query.ToList()
                                                           where iw.PostedDate >= fromDate && iw.PostedDate <= toDate
                                                                 && iw.Recorded && iw.RSInwardOutwardDetails.Any(
                                                                     c => c.MaterialGoodsID != null &&
                                                                          (lstRepositories.Any(d => d.ID == c.RepositoryID)) &&
                                                                          lstMaterialGoodses.Any(d => d.ID == c.MaterialGoodsID)) && lstTypeOW.Any(d => d == iw.TypeID)
                                                           select iw).ToList();
                    List<SAInvoice> lstSAs = (from sa in _ISAInvoiceService.Query.ToList()
                                              where sa.PostedDate >= fromDate && sa.PostedDate <= toDate
                                                    && sa.Recorded && sa.SAInvoiceDetails.Any(
                                                        c => c.MaterialGoodsID != null &&
                                                             (lstRepositories.Any(d => d.ID == c.RepositoryID)) &&
                                                             lstMaterialGoodses.Any(d => d.ID == c.MaterialGoodsID))
                                              select sa).ToList();
                    List<RSTransfer> lstTranfers = (from ts in _IRSTransferService.Query.ToList()
                                                    where ts.PostedDate >= fromDate && ts.PostedDate <= toDate
                                                          && ts.Recorded && ts.RSTransferDetails.Any(
                                                              c => c.MaterialGoodsID != null &&
                                                                   (lstRepositories.Any(d => d.ID == c.FromRepositoryID)) &&
                                                                   lstMaterialGoodses.Any(d => d.ID == c.MaterialGoodsID))
                                                    select ts).ToList();
                    List<GeneralLedger> lstGLs = (from gl in _IGeneralLedgerService.Query.ToList()
                                                  where gl.PostedDate >= fromDate && gl.PostedDate <= toDate &&
                                                 (lstTypeOW.Any(d => d == gl.TypeID) || lstTypeSA.Any(d => d == gl.TypeID))
                                                  select gl).ToList();
                    List<RepositoryLedger> lstRLs = (from rl in _IRepositoryLedgerService.Query.ToList()
                                                     where rl.PostedDate >= fromDate && rl.PostedDate <= toDate
                                                           && rl.MaterialGoodsID != null && (lstRepositories.Any(d => d.ID == rl.RepositoryID)) &&
                                                           lstMaterialGoodses.Any(d => d.ID == rl.MaterialGoodsID) &&
                                                    (lstTypeOW.Any(d => d == rl.TypeID) || lstTypeSA.Any(d => d == rl.TypeID))
                                                     select rl).ToList();
                    List<RSInwardOutward> lstRSInwardOutward = Query.Where(c => c.TypeID == 403).ToList();
                    List<GeneralLedger> lstGeneralLedgersreturn = _IGeneralLedgerService.Query.Where(c => c.TypeID == 330).ToList();
                    List<RepositoryLedger> lstRepositoriesledger = _IRepositoryLedgerService.Query.ToList().Where(c => c.TypeID == 330 || c.TypeID == 403).OrderBy(c => c.PostedDate).ToList();

                    #region RSInwardOutward
                    foreach (RSInwardOutward inwardOutward in lstRSOutwards)
                    {
                        if (lstMaterialGoodses.Any(c => inwardOutward.RSInwardOutwardDetails.Any(d => d.MaterialGoodsID == c.ID)))
                        {
                            foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                            {

                                if (lstMaterialGoodses.Any(c => c.ID == inwardOutwardDetail.MaterialGoodsID))
                                {
                                    inwardOutwardDetail.UnitPriceOriginal =
                                  lstMaterialGoodses.Count(c => c.ID == inwardOutwardDetail.MaterialGoodsID) == 1
                              ? lstMaterialGoodses.Single(c => c.ID == inwardOutwardDetail.MaterialGoodsID).Dgchuyendoi
                              : inwardOutwardDetail.UnitPriceOriginal;
                                    inwardOutwardDetail.UnitPrice = inwardOutwardDetail.UnitPriceOriginal * (inwardOutward.ExchangeRate ?? 1);
                                    inwardOutwardDetail.AmountOriginal = inwardOutwardDetail.UnitPriceOriginal * (inwardOutwardDetail.Quantity ?? 1);
                                    inwardOutwardDetail.Amount = inwardOutwardDetail.AmountOriginal * (inwardOutward.ExchangeRate ?? 1);
                                }
                            }
                            inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                            inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                            Update(inwardOutward);
                        }
                    }
                    #endregion

                    #region RepositoryLedger
                    foreach (RepositoryLedger repositoryLedger in lstRLs)
                    {
                        if (lstMaterialGoodses.Any(c => c.ID == repositoryLedger.MaterialGoodsID))
                        {
                            repositoryLedger.UnitPrice = lstMaterialGoodses.Count(c => c.ID == repositoryLedger.MaterialGoodsID) == 1
                              ? lstMaterialGoodses.Single(c => c.ID == repositoryLedger.MaterialGoodsID).Dgchuyendoi
                              : repositoryLedger.UnitPrice;
                            repositoryLedger.OWAmount = repositoryLedger.UnitPrice * repositoryLedger.OWQuantity;

                            _IRepositoryLedgerService.Update(repositoryLedger);
                        }
                    }
                    #endregion

                    #region RSTranfer
                    foreach (RSTransfer tranfer in lstTranfers)
                    {
                        if (lstMaterialGoodses.Any(c => tranfer.RSTransferDetails.Any(d => d.MaterialGoodsID == c.ID)))
                        {
                            foreach (RSTransferDetail tranferDetail in tranfer.RSTransferDetails)
                            {

                                if (lstMaterialGoodses.Any(c => c.ID == tranferDetail.MaterialGoodsID))
                                {
                                    tranferDetail.UnitPriceOriginal =
                                  lstMaterialGoodses.Count(c => c.ID == tranferDetail.MaterialGoodsID) == 1
                              ? lstMaterialGoodses.Single(c => c.ID == tranferDetail.MaterialGoodsID).Dgchuyendoi
                              : tranferDetail.UnitPriceOriginal;
                                    tranferDetail.UnitPrice = tranferDetail.UnitPriceOriginal;// *(tranferDetail.ExchangeRate ?? 1);
                                    tranferDetail.AmountOriginal = tranferDetail.UnitPriceOriginal * (tranferDetail.Quantity ?? 1);
                                    tranferDetail.Amount = tranferDetail.AmountOriginal;// *(tranferDetail.ExchangeRate ?? 1);

                                    var a = _IRepositoryLedgerService.Query.FirstOrDefault(x => x.DetailID == tranferDetail.ID && x.IWQuantity > 0);
                                    if (a != null)
                                    {
                                        a.IWAmount = tranferDetail.Amount;
                                        a.IWAmountBalance = tranferDetail.Amount;
                                        a.UnitPrice = tranferDetail.UnitPrice;
                                    }
                                    _IRepositoryLedgerService.Update(a);
                                }

                            }
                            tranfer.TotalAmount = tranfer.RSTransferDetails.Sum(c => c.Amount);
                            tranfer.TotalAmountOriginal = tranfer.RSTransferDetails.Sum(c => c.AmountOriginal);
                            _IRSTransferService.Update(tranfer);
                        }
                    }
                    #endregion

                    #region SAInvoice
                    foreach (SAInvoice sAInvoice in lstSAs)
                    {
                        if (lstMaterialGoodses.Any(c => sAInvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                        {
                            foreach (SAInvoiceDetail sAInvoiceDetails in sAInvoice.SAInvoiceDetails)
                            {

                                if (lstMaterialGoodses.Any(c => c.ID == sAInvoiceDetails.MaterialGoodsID))
                                {
                                    sAInvoiceDetails.OWPriceOriginal =
                                  lstMaterialGoodses.Count(c => c.ID == sAInvoiceDetails.MaterialGoodsID) == 1
                              ? lstMaterialGoodses.Single(c => c.ID == sAInvoiceDetails.MaterialGoodsID).Dgchuyendoi
                              : sAInvoiceDetails.OWPriceOriginal;
                                    sAInvoiceDetails.OWPrice = sAInvoiceDetails.OWPriceOriginal;
                                    sAInvoiceDetails.OWAmountOriginal = sAInvoiceDetails.OWPriceOriginal * (sAInvoiceDetails.Quantity ?? 1);
                                    sAInvoiceDetails.OWAmount = sAInvoiceDetails.OWAmountOriginal;

                                    SAReturnDetail sAReturnDetails = _ISAReturnDetailService.Query.FirstOrDefault(c => c.SAInvoiceDetailID == sAInvoiceDetails.ID);//update hang mua trả lại
                                    if (sAReturnDetails != null)
                                    {
                                        var sAReturn1 = _ISAReturnService.Query.FirstOrDefault(c => c.ID == sAReturnDetails.SAReturnID && c.Recorded && c.AutoOWAmountCal == 0);
                                        if (sAReturn1 != null)
                                        {
                                            sAReturnDetails.OWPriceOriginal = sAInvoiceDetails.OWPriceOriginal;
                                            sAReturnDetails.OWPrice = sAInvoiceDetails.OWPriceOriginal;
                                            sAReturnDetails.OWAmountOriginal = sAInvoiceDetails.OWPriceOriginal * (sAReturnDetails.Quantity ?? 1);
                                            sAReturnDetails.OWAmount = sAReturnDetails.OWAmountOriginal;
                                            _ISAReturnDetailService.Update(sAReturnDetails);
                                            sAReturn1.TotalOWAmount = sAReturn1.SAReturnDetails.Sum(c => c.OWAmount);
                                            sAReturn1.TotalOWAmountOriginal = sAReturn1.SAReturnDetails.Sum(c => c.OWAmountOriginal);
                                            _ISAReturnService.Update(sAReturn1);
                                            var rsInward = lstRSInwardOutward.FirstOrDefault(x => x.ID == sAReturn1.ID);
                                            if (rsInward != null)
                                            {
                                                rsInward.TotalAmount = sAReturn1.TotalOWAmount;
                                                rsInward.TotalAmountOriginal = sAReturn1.TotalOWAmountOriginal;
                                                Update(rsInward);
                                            }
                                            var lstGeneralLedgerReturn = lstGeneralLedgersreturn.Where(x => x.DetailID == sAReturnDetails.ID);
                                            foreach (GeneralLedger g in lstGeneralLedgerReturn)
                                            {
                                                if (g.Account == "632")
                                                {
                                                    g.DebitAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                    g.DebitAmount = sAReturnDetails.OWAmount;
                                                }
                                                else if (g.AccountCorresponding == "632")
                                                {
                                                    g.CreditAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                    g.CreditAmount = sAReturnDetails.OWAmount;
                                                }
                                                _IGeneralLedgerService.Update(g);
                                            }
                                            var ReposReturn = lstRepositoriesledger.FirstOrDefault(x => x.DetailID == sAReturnDetails.ID);
                                            if (ReposReturn != null)
                                            {
                                                ReposReturn.UnitPrice = sAReturnDetails.OWPrice;
                                                ReposReturn.IWAmount = sAReturnDetails.OWAmount;
                                                ReposReturn.IWAmountBalance = sAReturnDetails.OWAmount;
                                                _IRepositoryLedgerService.Update(ReposReturn);
                                            }

                                        }

                                    }
                                }

                            }
                            sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmount);
                            sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmountOriginal);
                            _ISAInvoiceService.Update(sAInvoice);
                            if (sAInvoice.OutwardNo != null && !string.IsNullOrEmpty(sAInvoice.OutwardNo))
                            {
                                var outWard = Query.FirstOrDefault(c => c.ID == sAInvoice.ID);
                                if (outWard != null)
                                {
                                    outWard.TotalAmount = sAInvoice.TotalCapitalAmount;
                                    outWard.TotalAmountOriginal = sAInvoice.TotalCapitalAmountOriginal;
                                    Update(outWard);
                                }
                            }
                        }
                    }
                    #endregion

                    #region GeneralLedger
                    foreach (GeneralLedger g in lstGLs)
                    {
                        if (g.DetailID != null && lstRSOutwards.Count(c => c.ID == g.ReferenceID && c.TypeID == g.TypeID
                            && Enumerable.Count<RSInwardOutwardDetail>(c.RSInwardOutwardDetails, b => b.ID == g.DetailID) == 1) == 1)
                        {
                            RSInwardOutwardDetail rsInwardOutwardDetail =
                                lstRSOutwards.Single(c => c.ID == g.ReferenceID && c.TypeID == g.TypeID).RSInwardOutwardDetails.Single(c => c.ID == g.DetailID);
                            if (rsInwardOutwardDetail.CreditAccount == g.Account)
                            {
                                g.CreditAmountOriginal = rsInwardOutwardDetail.AmountOriginal;
                                g.CreditAmount = rsInwardOutwardDetail.Amount;
                            }
                            else if (rsInwardOutwardDetail.DebitAccount == g.Account)
                            {
                                g.DebitAmountOriginal = rsInwardOutwardDetail.AmountOriginal;
                                g.DebitAmount = rsInwardOutwardDetail.Amount;
                            }
                            _IGeneralLedgerService.Update(g);
                        }
                        else if (g.DetailID != null && lstTranfers.Count(c => c.ID == g.ReferenceID && c.TypeID == g.TypeID
                             && Enumerable.Count<RSTransferDetail>(c.RSTransferDetails, b => b.ID == g.DetailID) == 1) == 1)
                        {
                            RSTransferDetail RSTransferDetails =
                                lstTranfers.Single(c => c.ID == g.ReferenceID && c.TypeID == g.TypeID).RSTransferDetails.Single(c => c.ID == g.DetailID);
                            if (RSTransferDetails.CreditAccount == g.Account)
                            {
                                g.CreditAmountOriginal = RSTransferDetails.AmountOriginal;
                                g.CreditAmount = RSTransferDetails.Amount;
                            }
                            else if (RSTransferDetails.DebitAccount == g.Account)
                            {
                                g.DebitAmountOriginal = RSTransferDetails.AmountOriginal;
                                g.DebitAmount = RSTransferDetails.Amount;
                            }
                            _IGeneralLedgerService.Update(g);
                        }
                    }
                    #endregion

                    #region GeneralLedger cho bán hàng
                    foreach (GeneralLedger g in lstGLs.Where(x => (x.Account == "632" || x.AccountCorresponding == "632") && (new int[] { 320, 321, 322, 323, 324, 325 }.Any(a => a == x.TypeID))))
                    {
                        SAInvoiceDetail saDetail = new SAInvoiceDetail();
                        try
                        {
                            SAInvoice sa = lstSAs.FirstOrDefault(c => c.ID == g.ReferenceID && c.TypeID == g.TypeID);
                            if (sa != null) saDetail = sa.SAInvoiceDetails.FirstOrDefault(c => c.ID == g.DetailID);
                            if (g.Account == "632")
                            {
                                g.DebitAmountOriginal = saDetail.OWAmountOriginal;
                                g.DebitAmount = saDetail.OWAmount;
                            }
                            else
                            {
                                g.CreditAmountOriginal = saDetail.OWAmountOriginal;
                                g.CreditAmount = saDetail.OWAmount;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        _IGeneralLedgerService.Update(g);
                    }
                    #endregion
                    #endregion
                }
                CommitTran();
                return true;

            }
            catch (Exception)
            {
                RolbackTran();

                return false;
            }
        }

        public List<TIOriginalVoucher> getOriginalVoucher(DateTime dteDateFrom, DateTime dteDateTo, string CurrencyID = null)
        {
            List<RSInwardOutward> lst = new List<RSInwardOutward>();
            if(CurrencyID == null) lst = Query.Where(x => x.Date >= dteDateFrom && x.Date <= dteDateTo && x.TypeID >= 410 && x.TypeID <= 415
                    && x.Recorded).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            else lst = Query.Where(x => x.Date >= dteDateFrom && x.Date <= dteDateTo && x.TypeID >= 410 && x.TypeID <= 415
                    && x.Recorded && x.CurrencyID == CurrencyID ).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<TIOriginalVoucher> result = new List<TIOriginalVoucher>();
            foreach (RSInwardOutward io in lst)
            {
                TIOriginalVoucher ov = new TIOriginalVoucher()
                {
                    Amount = io.TotalAmountOriginal,
                    PostedDate = io.PostedDate,
                    Date = io.Date,
                    No = io.No,
                    Reason = io.Reason,
                    ID = io.ID

                };
                result.Add(ov);
            }
            return result;
        }        

        public List<TIOriginalVoucher> FindByListID(string originalVoucher)
        {
            if (originalVoucher.Length == 0) return new List<TIOriginalVoucher>();
            string[] ss = originalVoucher.Substring(0, originalVoucher.Length - 1).Split(';');
            List<Guid> guids = new List<Guid>();
            foreach (string s in ss)
            {
                guids.Add(Guid.Parse(s));
            }
            return Query.Where(haha => guids.Contains(haha.ID)).Select(hihi => new TIOriginalVoucher
            {
                PostedDate = hihi.PostedDate,
                Date = hihi.Date,
                No = hihi.No,
                Reason = hihi.Reason,
                Amount = hihi.TotalAmountOriginal,
                ID = hihi.ID
            }).ToList();
        }
        #endregion


    }


}