using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBTellerPaperDetailVendorService: BaseService<MBTellerPaperDetailVendor ,Guid>,IMBTellerPaperDetailVendorService
    {
        public MBTellerPaperDetailVendorService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}