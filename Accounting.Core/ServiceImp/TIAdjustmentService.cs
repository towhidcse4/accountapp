﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class TIAdjustmentService : BaseService<TIAdjustment, Guid>, ITIAdjustmentService
    {
        public TIAdjustmentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public TIAdjustment GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
    }
}
