﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMPublishPeriodDetailService : BaseService<EMPublishPeriodDetail, Guid>, IEMPublishPeriodDetailService
    {
        public EMPublishPeriodDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public BindingList<EMPublishPeriodDetail> GetBindingListEMPulishPeriodDetailID(Guid? inputID)
        {
            BindingList<EMPublishPeriodDetail> bdEMPublishPeriodDetail = new BindingList<EMPublishPeriodDetail>(Query.Where(p => p.EMPublishPeriodID == inputID).ToList());
            return bdEMPublishPeriodDetail;
        }

        public List<EMPublishPeriodDetail> GetListEMPublishPeriodDetail(Guid? Id)
        {
            List<EMPublishPeriodDetail> listPpd = Query.Where(a => a.EMPublishPeriodID == Id).ToList();
            return listPpd;
        }

        public EMPublishPeriodDetail GetEMPublishPeriodDetailIDStockCategoryID(Guid? Id, Guid? StockCategoryId)
        {
            EMPublishPeriodDetail select = Query.Where(a => a.EMPublishPeriodID == Id && a.StockCategoryID == StockCategoryId).Single();
            return select;
        }
    }

}


