﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CostSetService : BaseService<CostSet, Guid>, ICostSetService
    {
        public CostSetService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListOrderFixCodeChild(CostSet costSet)
        {
            List<string> lstOrderFixCodeChild = Query.Where(a => a.ParentID == costSet.ParentID)
                        .Select(a => a.OrderFixCode).ToList();
            return lstOrderFixCodeChild;
        }

        public List<CostSet> GetListCostSetOrderFixCode(string orderFixCode)
        {
            List<CostSet> lstChildCheck =
                                Query.Where(p => p.OrderFixCode.StartsWith(orderFixCode)).ToList();
            return lstChildCheck;
        }

        public List<CostSet> GetListCostSetParentID(Guid? parentID)
        {
            List<CostSet> listChildNewParent = Query.Where(
                                a => a.ParentID == parentID).ToList();
            return listChildNewParent;
        }

        public List<CostSet> GetCostSetGrade(int grade)
        {
            List<CostSet> listChildGradeNo1 = Query.Where(
                                a => a.Grade == grade).ToList();
            return listChildGradeNo1;
        }

        public int CountChildCostSet(Guid? parentID)
        {
            int checkChildOldParent = Query.Count(p => p.ParentID == parentID);
            return checkChildOldParent;
        }

        public List<CostSet> GetListCostSetIsActive(bool isActive)
        {
            List<CostSet> list = Query.Where(c => c.IsActive == isActive).ToList();
            return list;
        }
        /// <summary>
        /// Lấy danh sách CostSetCode từ GetCostSetCodeBool khi IsActive =true
        /// [Longtx]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<CostSet> GetCostSetCodeBool(bool isActive)
        {
            return Query.OrderBy(p => p.CostSetCode).Where(p => p.IsActive == isActive).ToList();
        }


        /// <summary>
        /// Lấy danh sách các CostSet theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách CostSet được sắp xếp theo dạng cây cha con</returns>
        public List<CostSet> GetByActive_OrderByTreeIsParentNode(bool isActive)
        {
            List<CostSet> get = new List<CostSet>();
            List<CostSet> lst = Query.ToList();
            List<CostSet> parent = lst.Where(x => x.IsActive == isActive && x.ParentID == null).OrderBy(x => x.CostSetCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode) && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }

        /// <summary>
        /// Lấy danh sách các CostSet và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách CostSet được sắp xếp theo dạng cây cha con</returns>
        public List<CostSet> GetAll_OrderByTreeIsParentNode()
        {
            List<CostSet> get = new List<CostSet>();
            List<CostSet> lst = Query.ToList();
            List<CostSet> parent = lst.Where(x => x.ParentID == null).OrderBy(x => x.CostSetCode).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }
        public List<CostSet> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.CostSetCode).ToList();
        }
        public Guid? GetGuidCostSetByCode(string code)
        {
            Guid? id = null;
            CostSet cs = Query.Where(p => p.CostSetCode == code).SingleOrDefault();
            if (cs != null)
            {
                id = cs.ID;
            }
            return id;
        }

        public List<SelectCostSet> GetListByType(int costsetType)
        {
            List<SelectCostSet> lst = new List<SelectCostSet>();
            List<CostSet> lstCostSet = Query.Where(x => x.IsActive == true).ToList();
            List<SelectCostSet> lstcostset = (from g in lstCostSet
                                          where g.CostSetType == costsetType
                                           group g by new { g.ID, g.CostSetCode, g.CostSetName, g.CostSetType, g.CostSetTypeView }
                        into t
                                           select new SelectCostSet()
                                           {
                                               ID = t.Key.ID,
                                               CostSetCode = t.Key.CostSetCode,
                                               CostSetName = t.Key.CostSetName,
                                               CostSetType = t.Key.CostSetType,
                                               CostSetTypeView = t.Key.CostSetTypeView
                                           }).ToList();
            lst.AddRange(lstcostset);
            return lst;
        }
    }

}