using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class RSTransferService : BaseService<RSTransfer, Guid>, IRSTransferService
    {
        public RSTransferService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public RSTransfer getRSTransferbyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
    }

}