﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.FSACalculateTheCost;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using NHibernate;

namespace Accounting.Core.ServiceImp
{
    public class MaterialGoodsService : BaseService<MaterialGoods, Guid>, IMaterialGoodsService
    {
        public MaterialGoodsService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private IViewVouchersCloseBookService _IVoucherPatternsReportService { get { return IoC.Resolve<IViewVouchersCloseBookService>(); } }
        public IRepositoryLedgerService _IRepositoryLedgerService { get { return IoC.Resolve<IRepositoryLedgerService>(); } }
        public IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public IMaterialGoodsCategoryService _IMaterialGoodsCategoryService { get { return IoC.Resolve<IMaterialGoodsCategoryService>(); } }
        public IRepositoryService _IRepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public ISystemOptionService ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public ISAOrderDetailService _ISAOrderDetailService { get { return IoC.Resolve<ISAOrderDetailService>(); } }
        public IPPOrderDetailService _IPPOrderDetailService { get { return IoC.Resolve<IPPOrderDetailService>(); } }
        public ITIIncrementDetailService _ITIIncrementDetailService { get { return IoC.Resolve<ITIIncrementDetailService>(); } }
        public ITIInitService _ITIInitService { get { return IoC.Resolve<ITIInitService>(); } }
        public ITIIncrementService _ITIIncrementService { get { return IoC.Resolve<ITIIncrementService>(); } }
        public ITIDecrementService _ITIDecrementService { get { return IoC.Resolve<ITIDecrementService>(); } }
        public ITIDecrementDetailService _ITIDecrementDetailService { get { return IoC.Resolve<ITIDecrementDetailService>(); } }
        public ITIAdjustmentService _ITIAdjustmentService { get { return IoC.Resolve<ITIAdjustmentService>(); } }
        public ITIAdjustmentDetailService _ITIAdjustmentDetailService { get { return IoC.Resolve<ITIAdjustmentDetailService>(); } }
        public ITIAllocationService ITIAllocationService { get { return IoC.Resolve<ITIAllocationService>(); } }
        public ITIAllocationDetailService ITIAllocationDetailService { get { return IoC.Resolve<ITIAllocationDetailService>(); } }
        public IToolLedgerService _IToolLedgerService { get { return IoC.Resolve<IToolLedgerService>(); } }


        public ISession GetSessionState()
        {
            return NHibernateSession;
        }
        /// <summary>
        /// Lấy ra danh sách MaterialGoods theo MaterialGoodType
        /// [DUYTN]
        /// </summary>
        /// <param name="materialGoodType">MaterialGoodType truyền vào</param>
        /// <returns>Danh sách các đối tượng MaterialGoods, null nếu bị lỗi</returns>
        public List<MaterialGoods> GetAll_ByMaterialGoodsType(int materialGoodType)
        {
            //List<MaterialGoods> cbb = (from a in Query where a.MaterialGoodsType == materialGoodType select a).ToList();
            return Query.Where(p => p.MaterialGoodsType == materialGoodType).ToList(); ;
        }

        /// <summary>
        /// Lấy ra danh sách các MaterialGoods với so sách khác theo MaterialToolType 
        /// [DUYTN]
        /// </summary>
        /// <param name="Other_MaterialToolType">là tham số Other_MaterialToolType và là số nguyên</param>
        /// <returns>Danh sách các đối tượng MaterialGoods, null nếu bị lỗi</returns>
        public List<MaterialGoods> GetAll_NotValueByMaterialToolType(int Other_MaterialToolType)
        {
            return Query.Where(p => p.MaterialToolType != Other_MaterialToolType).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách các MaterialGoods với so sách khác theo ID
        /// [DUYTN]
        /// </summary>
        /// <param name="ID">là ID của một đối tượng MaterialGoods</param>
        /// <returns>Danh sách các đối tượng MaterialGoods, null nếu bị lỗi</returns>
        public List<MaterialGoods> GetAll_NotValueByID(Guid ID)
        {
            return Query.Where(p => p.ID != ID).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCode
        /// [DUYTN]
        /// </summary>
        /// <returns>Danh sách MaterialGoodsCode dạng chuỗi, null nếu bị lỗi</returns>
        public List<string> GetMaterialGoodsCode()
        {
            return Query.Select(p => p.MaterialGoodsCode).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoods theo list MaterialGoodsType
        /// [DUYTN], sửa ngày 07/04/2014/
        /// </summary>
        /// <param name="MaterialGoodsType">Danh sách các MaterialGoodsType cần lấy (0,1,2,3,...)</param>
        /// <returns>tập các MaterialGoods, null nếu bị lỗi</returns>
        public List<MaterialGoods> GetAll_ByMaterialGoodsType(List<int> MaterialGoodsType)
        {
            //return Query.Where(c => c.MaterialGoodsType == MaterialGoodsType[0] || c.MaterialGoodsType == MaterialGoodsType[1]|| c.MaterialGoodsType==MaterialGoodsType[2]).ToList();
            return Query.Where(c => MaterialGoodsType.Contains(c.MaterialGoodsType)).ToList();
        }

        /// <summary>
        /// Lấy một đối tượng MaterialGoods theo MaterialGoodsCode
        /// [DUYTN], sửa ngày 07/04/2014
        /// </summary>
        /// <param name="MaterialGoodsCode">là MaterialGoodsCode truyền vào</param>
        /// <returns>Một MaterialGoodsCode, null nếu bị lỗi</returns>
        public MaterialGoods GetAll_ByMaterialGoodsCode(string MaterialGoodsCode)
        {
            //return Query.Where(c => c.MaterialGoodsCode.Equals(MaterialGoodsCode)).Select(k => k.MaterialGoodsName).ToString();
            return Query.Where(c => c.MaterialGoodsCode.Equals(MaterialGoodsCode)).FirstOrDefault();
        }

        /// <summary>
        /// Lấy ra danh sách MarerialGoodsCode có IsActive==true
        /// [Longtx]
        /// </summary>
        /// <returns></returns>
        public List<MaterialGoods> GetByMateriaGoodCode()
        {
            return Query.OrderBy(p => p.MaterialGoodsCode).Where(p => p.IsActive == true).ToList();
        }

        /// <summary>
        /// Lấy danh sách Id từ MaterialGood
        /// [Longtx]
        /// </summary>
        /// <param name="materialGoodID">là MaterialGoodsCode truyền vào</param>
        /// <returns>tập Id ở bảng materialGoods</returns>
        public List<MaterialGoods> GetListMaterialGood(Guid? materialGoodID)
        {
            return Query.Where(p => p.ID == materialGoodID).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MarerialGoodsCode từ MaterialGoods có IsActive==true
        /// [Longtx]
        /// </summary>
        /// <returns></returns>
        public List<MaterialGoods> GetByMateriaGoodCode(bool isActive)
        {
            return Query.OrderBy(p => p.MaterialGoodsCode).Where(p => p.IsActive == isActive).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoods có kèm Số lượng tồn 
        /// [Huy Anh]
        /// </summary>
        /// <param name="postedDate"></param>
        /// <param name="branchID"></param>
        /// <param name="isLeftJoin"></param>
        /// <returns></returns>
        public List<MaterialGoodsCustom> GetMateriaGoodByAndRepositoryLedger(DateTime postedDate, Guid? branchID, bool isLeftJoin = true)
        {
            var resul = (from g in Query.ToList()
                         join e in
                             _IRepositoryLedgerService.Query.Where(
                                 p => p.PostedDate <= postedDate && p.BranchID == branchID || p.BranchID == null) on g.ID
                             equals e.MaterialGoodsID into j1
                         from j2 in isLeftJoin ? j1.DefaultIfEmpty() : j1
                         group j2 by g
                             into joined
                         select new
                         {
                             joined,
                             SumIWQuantity = joined.Sum(x => x == null ? 0 : ((x.IWQuantity ?? 0) - (x.OWQuantity ?? 0))),
                             SumIWAmount = joined.Sum(x => x == null ? 0 : ((x.IWAmount ?? 0) - (x.OWAmount ?? 0)))
                         }).ToList();

            List<MaterialGoodsCustom> list = new List<MaterialGoodsCustom>();
            foreach (var row in resul)
            {
                MaterialGoodsCustom _materialGoods = new MaterialGoodsCustom();
                PropertyInfo[] propertyInfo = _materialGoods.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.joined.Key.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(_materialGoods, propItem.GetValue(row.joined.Key, null), null);
                    }
                }
                _materialGoods.SumIWQuantity = row.SumIWQuantity;
                _materialGoods.SumIWAmount = row.SumIWAmount;
                list.Add(_materialGoods);
            }
            return list.OrderBy(p => p.MaterialGoodsCode).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoods có kèm Số lượng tồn 
        /// [Huy Anh]
        /// </summary>
        /// <param name="MaterialToolType"> </param>
        /// <param name="isLeftJoin"></param>
        /// <returns></returns>
        public List<MaterialGoodsCustom> GetMateriaGoodByMaterialToolTypeAndRepositoryLedger(int MaterialToolType, bool isLeftJoin = true)
        {
            var resul = (from g in Query.Where(p => p.MaterialToolType != MaterialToolType).ToList()
                         join e in
                             _IRepositoryLedgerService.Query.ToList() on g.ID
                             equals e.MaterialGoodsID into j1
                         from j2 in isLeftJoin ? j1.DefaultIfEmpty() : j1
                         group j2 by g
                             into joined
                         select new
                         {
                             joined,
                             SumIWQuantity = joined.Sum(x => x == null ? 0 : ((x.IWQuantity ?? 0) - (x.OWQuantity ?? 0))),
                             SumIWAmount = joined.Sum(x => x == null ? 0 : ((x.IWAmount ?? 0) - (x.OWAmount ?? 0)))
                         }).ToList();
            List<MaterialGoodsCustom> list = new List<MaterialGoodsCustom>();
            foreach (var row in resul)
            {
                MaterialGoodsCustom _materialGoods = new MaterialGoodsCustom();
                PropertyInfo[] propertyInfo = _materialGoods.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.joined.Key.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(_materialGoods, propItem.GetValue(row.joined.Key, null), null);
                    }
                }
                _materialGoods.SumIWQuantity = row.SumIWQuantity;
                _materialGoods.SumIWAmount = row.SumIWAmount;
                list.Add(_materialGoods);
            }
            return list.OrderBy(p => p.MaterialGoodsCode).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoods kèm số lượng tồn theo từng kho
        /// [DuyNT]
        /// </summary>
        /// <param name="postedDate"></param>
        /// <returns>Trả về danh sách MaterialGoodsCustom ,null nếu lỗi</returns>
        public List<MaterialGoodsCustom> GetMateriaGoodAndRepositoryLedger(DateTime postedDate)
        {
            List<MaterialGoodsCategory> lstMaterialGoodsCategories = new List<MaterialGoodsCategory>();
            lstMaterialGoodsCategories = _IMaterialGoodsCategoryService.GetAll();
            List<Repository> lstRepositories = new List<Repository>();
            lstRepositories = _IRepositoryService.GetAll();
            List<RepositoryLedger> lstrepgl = _IRepositoryLedgerService.Query.Where(p => p.PostedDate <= postedDate).ToList();
            List<MaterialGoods> lstMaterialGoodses = Query.ToList();
            var resul = (from m in lstrepgl
                         group m by new { m.RepositoryID, m.MaterialGoodsID } into g
                         select new
                         {
                             RepositoryID = g.Key.RepositoryID,
                             MaterialGoodsID = g.Key.MaterialGoodsID,
                             SumIWQuantity = g.Sum(c => (c.IWQuantity ?? 0) - (c.OWQuantity ?? 0)),
                             SumIWAmount = g.Sum(c => (c.IWAmount ?? 0) - (c.OWAmount ?? 0)),
                         }).ToList(); //lấy danh sách VTHH có cùng kho.
            //List<MaterialGoodsCustom> list = new List<MaterialGoodsCustom>();
            //var repository = (from a in lstRepositories
            //                  from b in lstMaterialGoodses
            //                  select new
            //                  {
            //                      Repositorys = a,
            //                      MaterialGoods = b,
            //                  }).ToList();
            //foreach (var a in repository)
            //{
            //    MaterialGoodsCustom materialGoodsCustom = new MaterialGoodsCustom();
            //    MaterialGoods materialGoods = a.MaterialGoods;
            //    if (resul.Count(c => c.RepositoryID == a.Repositorys.ID && c.MaterialGoodsID == a.MaterialGoods.ID) == 1)
            //    {
            //        materialGoodsCustom.SumIWQuantity = resul.Single(c => c.RepositoryID == a.Repositorys.ID && c.MaterialGoodsID == a.MaterialGoods.ID).SumIWQuantity;
            //        materialGoodsCustom.SumIWAmount = resul.Single(c => c.RepositoryID == a.Repositorys.ID && c.MaterialGoodsID == a.MaterialGoods.ID).SumIWAmount;
            //    }
            //    else
            //    {
            //        materialGoodsCustom.SumIWQuantity = 0;
            //        materialGoodsCustom.SumIWAmount = 0;
            //    }
            //    PropertyInfo[] propertyInfo = materialGoodsCustom.GetType().GetProperties();
            //    foreach (PropertyInfo info in propertyInfo)
            //    {
            //        PropertyInfo propItem = materialGoods.GetType().GetProperty(info.Name);
            //        if (propItem != null && propItem.CanWrite)
            //        {
            //            info.SetValue(materialGoodsCustom, propItem.GetValue(materialGoods, null), null);
            //        }
            //    }
            //    materialGoodsCustom.InventoryIWAmount = materialGoodsCustom.SumIWAmount;
            //    materialGoodsCustom.InventoryIWQuantity = materialGoodsCustom.SumIWQuantity;
            //    materialGoodsCustom.Repository = a.Repositorys;
            //    materialGoodsCustom.RepositoryID = a.Repositorys.ID;
            //    materialGoodsCustom.RepositoryCode = a.Repositorys.RepositoryCode;
            //    list.Add(materialGoodsCustom);
            //}
            //return list./*Where(c => c.SumIWQuantity > 0).*/OrderBy(c => c.RepositoryCode).ThenBy(c => c.MaterialGoodsCode).ToList();
            List<MaterialGoodsCustom> list = new List<MaterialGoodsCustom>();
            foreach (var a in lstMaterialGoodses)
            {
                MaterialGoodsCustom materialGoodsCustom = new MaterialGoodsCustom();
                if (resul.Count(c => /*c.RepositoryID == a.RepositoryID &&*/ c.MaterialGoodsID == a.ID) == 1)
                {
                    materialGoodsCustom.SumIWQuantity = resul.Single(c =>/* c.RepositoryID == a.RepositoryID &&*/ c.MaterialGoodsID == a.ID).SumIWQuantity;
                    materialGoodsCustom.SumIWAmount = resul.Single(c => /*c.RepositoryID == a.RepositoryID &&*/ c.MaterialGoodsID == a.ID).SumIWAmount;
                }
                else
                {
                    materialGoodsCustom.SumIWQuantity = 0;
                    materialGoodsCustom.SumIWAmount = 0;
                }
                PropertyInfo[] propertyInfo = materialGoodsCustom.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = a.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(materialGoodsCustom, propItem.GetValue(a, null), null);
                    }
                }
                materialGoodsCustom.InventoryIWAmount = materialGoodsCustom.SumIWAmount;
                materialGoodsCustom.InventoryIWQuantity = materialGoodsCustom.SumIWQuantity;
                materialGoodsCustom.Repository = lstRepositories.FirstOrDefault(x => x.ID == a.RepositoryID);
                materialGoodsCustom.RepositoryID = a.RepositoryID;
                materialGoodsCustom.RepositoryCode = a.RepositoryCode;
                list.Add(materialGoodsCustom);
            }
            return list.OrderBy(c => c.RepositoryCode).ThenBy(c => c.MaterialGoodsCode).ToList();
        }

        /// <summary>
        /// Lấy danh sách Vật tư hàng hóa đang hoạt động
        /// </summary>
        /// <param name="isActive">Trạng thái hoạt động (true/false)</param>
        /// <returns></returns>
        public List<MaterialGoods> GetByIsActive(bool isActive)
        {
            try
            {
                return Query.Where(c => c.IsActive == isActive).ToList().OrderBy(c => c.MaterialGoodsCode).ToList();
            }
            catch (Exception)
            {
                return new List<MaterialGoods> { new MaterialGoods() };
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách MaterialGoods theo MaterialGoodsCategory
        /// </summary>
        /// <param name="materialGoodsCategory"></param>
        /// <returns></returns>
        public List<MaterialGoods> GetListByMaterialGoodsCategory(Guid? materialGoodsCategory)
        {
            return Query.Where(m => m.MaterialGoodsCategoryID == materialGoodsCategory).ToList();
        }
        public List<string> CheckExistMaterialGoods(IEnumerable<Guid> listCheck)
        {
            List<string> dsRepositoryCode = new List<string>();
            var ds = from materialGoodse in Query.ToList()
                     where listCheck.Any(c => c == materialGoodse.ID)
                     select materialGoodse;

            List<int> lstType = new List<int>() { 702, 200, 310, 300, 340 };
            //Điều kiện khác TypeID của Số dư của TK theo dõi theo VTHH,Đơn mua hàng,Báo giá,Đơn đặt hàng,Giảm giá hàng bán)
            List<RepositoryLedger> lstRepositoryLedger = new List<RepositoryLedger>();
            lstRepositoryLedger = _IRepositoryLedgerService.Query.ToList();
            if (lstRepositoryLedger.Count > 0)
            {
                try
                {
                    var query = (from resLedger in lstRepositoryLedger
                                 join mG in ds on resLedger.MaterialGoodsID equals mG.ID
                                 where lstType.All(c => resLedger.TypeID != c)
                                 select new { mG.RepositoryCode }).ToList();
                    var d = (from g in query
                             group g by new { g.RepositoryCode }).ToList();
                    dsRepositoryCode = d.Select(c => c.Key.RepositoryCode).ToList();
                }
                catch (Exception)
                {
                    return dsRepositoryCode;
                }


            }
            return dsRepositoryCode;
        }

        public List<MaterialGoods> GetOrderby()
        {
            return Query.OrderBy(p => p.MaterialGoodsName).ToList();
        }

        public Guid? GetGuidMaterialGoodsByCode(string code)
        {
            Guid? id = null;
            MaterialGoods mg = Query.Where(p => p.MaterialGoodsCode == code).SingleOrDefault();
            if (mg != null)
            {
                id = mg.ID;
            }
            return id;
        }

        #region Dũng Tính giá bán trong phân hệ bán hàng
        /// <summary>
        /// [DungNA]
        /// Danh sách tính giá bán phân hệ bán hàng
        /// </summary>
        /// <returns></returns>
        public List<CalculateCost> GetListCalculateCosts()
        {
            List<MaterialGoodsCategory> lstMaterialGoodsCategories = _IMaterialGoodsCategoryService.Query.ToList();
            List<MaterialGoods> lstMaterialGoodses = Query.ToList();
            List<CalculateCost> lsCalculateCosts = new List<CalculateCost>();
            lsCalculateCosts = Utils.Convert_FromParent_ToChild(lstMaterialGoodses, lsCalculateCosts);
            foreach (var x in lsCalculateCosts)
            {
                //x.MaterialGoodsCategory = (x.MaterialGoodsCategoryID != null && lstMaterialGoodsCategories.Count(b => x.MaterialGoodsCategory != null && b.ID == x.MaterialGoodsCategoryID) == 1) ? lstMaterialGoodsCategories.Single(b => x.MaterialGoodsCategory != null && b.ID == x.MaterialGoodsCategoryID).MaterialGoodsCategoryCode : "";
                x.Status = false;

            }
            return lsCalculateCosts;
            //select new MaterialGoods()
            // {
            //     //ID = a.ID,
            //     //MaterialGoodsCategory = (a.MaterialGoodsCategoryID != null && lstMaterialGoodsCategories.Count(b => b.ID == a.MaterialGoodsCategoryID)==1) ? lstMaterialGoodsCategories.Single(b => b.ID == a.MaterialGoodsCategoryID).MaterialGoodsCategoryCode : "",
            //     //MaterialGoodsCode = a.MaterialGoodsCode,
            //     //MaterialGoodsName = a.MaterialGoodsName,
            //     //PurchasePrice = Convert.ToDecimal(a.PurchasePrice ?? 0),
            //     //LastPurchasePriceAfterTax = Convert.ToDecimal(a.LastPurchasePriceAfterTax ?? 0),
            //     //FixedSalePrice = Convert.ToDecimal(a.FixedSalePrice ?? 0),
            //     //SalePrice = Convert.ToDecimal(a.SalePrice ?? 0),
            //     //SalePrice2 = Convert.ToDecimal(a.SalePrice2 ?? 0),
            //     //SalePrice3 = Convert.ToDecimal(a.SalePrice3 ?? 0),
            //     a

            // }).ToList();

        }

        public List<VoucherMaterialGoods> GetlistVoucherMaterialGoodsView()
        {
            var result = new List<VoucherMaterialGoods>();
            List<ViewVouchersCloseBook> lstVoucher = _IVoucherPatternsReportService.GetAll().Where(p => p.MaterialGoodsID != null).ToList();
            var voucherQuery = from voucher in lstVoucher
                               select new { voucher.ID, voucher.MaterialGoodsID, voucher.TypeID, voucher.No, voucher.Date, voucher.PostedDate, voucher.Reason, voucher.TotalAmount };
            foreach (var item in voucherQuery)
            {
                var temp = new VoucherMaterialGoods()
                {
                    ID = item.ID,
                    MaterialGoodsId = item.MaterialGoodsID,
                    TypeID = item.TypeID,
                    Date = item.Date,
                    No = item.No,
                    PostedDate = Convert.ToDateTime(item.PostedDate),
                    Reason = item.Reason,
                    TotalAmount = item.TotalAmount ?? 0
                };
                result.Add(temp);
            }
            return result;
        }
        #endregion

        public List<MaterialGoods> GetMaterialTools()
        {
            return Query.Where(o => o.MaterialToolType == 1 || o.MaterialToolType == 2).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách Công cụ dụng cụ có kèm Số lượng tồn 
        /// [Huy Anh]
        /// </summary>
        /// <param name="postedDate"></param>
        /// <param name="branchID"></param>
        /// <param name="isLeftJoin"></param>
        /// <returns></returns>
        public List<MaterialGoodsCustom> GetMateriaToolsByAndRepositoryLedger(DateTime postedDate, Guid? branchID, bool isLeftJoin = true)
        {
            var resul = (from g in Query.Where(i => i.MaterialToolType == 1 || i.MaterialToolType == 2).ToList()
                         join e in
                             _IRepositoryLedgerService.Query.Where(
                                 p => p.PostedDate <= postedDate && p.BranchID == branchID || p.BranchID == null) on g.ID
                             equals e.MaterialGoodsID into j1
                         from j2 in isLeftJoin ? j1.DefaultIfEmpty() : j1
                         group j2 by g
                             into joined
                         select new
                         {
                             joined,
                             SumIWQuantity = joined.Sum(x => x == null ? 0 : ((x.IWQuantity ?? 0) - (x.OWQuantity ?? 0))),
                             SumIWAmount = joined.Sum(x => x == null ? 0 : ((x.IWAmount ?? 0) - (x.OWAmount ?? 0)))
                         }).ToList();
            List<MaterialGoodsCustom> list = new List<MaterialGoodsCustom>();
            foreach (var row in resul)
            {
                MaterialGoodsCustom _materialGoods = new MaterialGoodsCustom();
                PropertyInfo[] propertyInfo = _materialGoods.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.joined.Key.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(_materialGoods, propItem.GetValue(row.joined.Key, null), null);
                    }
                }
                _materialGoods.SumIWQuantity = row.SumIWQuantity;
                _materialGoods.SumIWAmount = row.SumIWAmount;
                list.Add(_materialGoods);
            }
            return list.OrderBy(p => p.MaterialGoodsCode).ToList();
        }

        public List<MaterialGoods> GetListBySAOrderID(Guid id)
        {
            var listSAOrderDetail = _ISAOrderDetailService.GetAll().Where(x => x.SAOrderID == id);
            var listMaterialGood = Query.ToList();
            var query = (from a in listSAOrderDetail
                         join b in listMaterialGood
                         on a.MaterialGoodsID equals b.ID
                         select b).ToList();
            return query;
        }
        public List<MaterialGoods> GetListByPPOrderID(Guid id)
        {
            var listPPOrderDetail = _IPPOrderDetailService.GetAll().Where(x => x.PPOrderID == id);
            var listMaterialGood = Query.ToList();
            var query = (from a in listPPOrderDetail
                         join b in listMaterialGood
                         on a.MaterialGoodsID equals b.ID
                         select b).ToList();
            return query;
        }
        public bool ExistsMaterialCode(string req)
        {
            if (Query.Any(o => o.MaterialGoodsCode.ToLower().Equals(req.ToLower())))
                return true;
            return false;
        }

        public bool CheckExistMaterialGoods(string code, Guid iD)
        {
            if (Query.Any(o => o.MaterialGoodsCode.ToLower().Equals(code.ToLower()) && o.ID != iD))
                return true;
            return false;
        }

        public bool CheckDeleteMaterialTools(Guid iD)
        {
            return _ITIIncrementDetailService.Query.Any(fai => fai.ToolsID == iD)
                     || _ITIInitService.Query.Any(fai => fai.MaterialGoodsID == iD);
        }

        public List<MaterialGoods> GetByMaterialTools()
        {
            List<MaterialGoods> result = Query.Where(a => a.MaterialGoodsType == 1 && !_ITIInitService.Query.Any(b => b.MaterialGoodsID == a.ID)).ToList();
            foreach (MaterialGoods fa in result)
            {
                TIIncrementDetail detail = _ITIIncrementDetailService.FindByMaterialGoodsID(fa.ID);
                var data = _ITIIncrementDetailService.Query.Join(_IToolLedgerService.Query, a => a.TIIncrementID, b => b.ReferenceID, (a, b) => new { MaterialGoodsID = a.ToolsID, a.UnitPriceOriginal, b.ToolsID })
                                                .Where(c => c.MaterialGoodsID == fa.ID && c.ToolsID != null).FirstOrDefault();
                if (data != null)
                {
                    fa.UnitPrice = data.UnitPriceOriginal;
                    fa.Amount = data.UnitPriceOriginal * fa.Quantity;
                    fa.AllocatedAmount = fa.AllocationTimes != 0 ? fa.Amount / fa.AllocationTimes : 0;
                }

            }
            return result;
        }

        public List<VoucherTools> GetListVouchers(Guid ToolsID)
        {
            var result = _ITIIncrementService.Query
                        .Join(_ITIIncrementDetailService.Query, de => de.ID, ded => ded.TIIncrementID, (de, ded)
                            => new VoucherTools { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalAmount, ToolsID = ded.ToolsID })
                        .Where(c => c.ToolsID == ToolsID).ToList();

            var result2 = _ITIDecrementService.Query
                        .Join(_ITIDecrementDetailService.Query, de => de.ID, ded => ded.TIDecrementID, (de, ded)
                            => new VoucherTools { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalRemainingAmount, ToolsID = ded.ToolsID })
                        .Where(c => c.ToolsID == ToolsID).ToList();

            var result3 = _ITIAdjustmentService.Query
                        .Join(_ITIAdjustmentDetailService.Query, de => de.ID, ded => ded.TIAdjustmentID, (de, ded)
                            => new VoucherTools { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalAmount, ToolsID = ded.ToolsID })
                        .Where(c => c.ToolsID == ToolsID).ToList();

            var result4 = ITIAllocationService.Query
                        .Join(ITIAllocationDetailService.Query, de => de.ID, ded => ded.TIAllocationID, (de, ded)
                            => new VoucherTools { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalAmount, ToolsID = ded.ToolsID })
                        .Where(c => c.ToolsID == ToolsID).ToList();
            result.AddRange(result2);
            result.AddRange(result3);
            result.AddRange(result4);
            return result;

        }
        public List<TongHopTonKho> getMaterialGood(DateTime from, DateTime to, List<Repository> lstrepos, MaterialGoodsCategory mtc)
        {
            var ddSoGia = ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);
            DateTime fromdate = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0);
            DateTime todate = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);
            List<MaterialGoods> lstMate = Query.Where(x => x.MaterialGoodsCategoryID == mtc.ID).ToList();
            List<RepositoryLedger> lstRL = _IRepositoryLedgerService.Query.ToList().Where(x => lstrepos.Any(d => d.ID == x.RepositoryID) && lstMate.Any(d => d.ID == x.MaterialGoodsID)).ToList();

            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query;
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query;           

            var list_genera1 = (from a in lstRL
                                join i in listSAInvoice on a.ReferenceID equals i.ID
                                join b in listSABill on i.BillRefID equals b.ID
                                join d in list_delete on b.ID equals d.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in lstRL
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            lstRL = (from a in lstRL
                   where !list_genera1.Any(t => t.ID == a.ID)
                   select a).ToList();

            lstRL = (from a in lstRL
                     where !list_genera2.Any(t => t.ID == a.ID)
                     select a).ToList();

            List<TongHopTonKho> lst = new List<TongHopTonKho>();
            foreach (var x in lstrepos)
            {
                TongHopTonKho model = new TongHopTonKho();
                model.RepositoryCode = x.RepositoryCode;
                model.RepositoryName = x.RepositoryName;
                model.TongHopTonKhoDetails = (from a in lstRL
                                              where a.RepositoryID == x.ID
                                              group a by new { a.MaterialGoodsID, a.RepositoryID }
                                              into g
                                              select new TongHopTonKhoDetail()
                                              {
                                                  MaterialGoodID = g.Key.MaterialGoodsID,
                                                  MaterialGoodCode = lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID) == null ? "" : lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID).MaterialGoodsCode,
                                                  MaterialGoodName = lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID) == null ? "" : lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID).MaterialGoodsName,
                                                  Unit = lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID) == null ? "" : lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID).Unit,
                                                  SLDK = ((g.Where(d => d.PostedDate < fromdate).Sum(t => t.IWQuantity) ?? 0) - (g.Where(d => d.PostedDate < fromdate).Sum(t => t.OWQuantity) ?? 0)),
                                                  SLIWGK = (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.IWQuantity) ?? 0),
                                                  SLOWGK = (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.OWQuantity) ?? 0),
                                                  SLCK = (((g.Where(d => d.PostedDate < fromdate).Sum(t => t.IWQuantity) ?? 0) - (g.Where(d => d.PostedDate < fromdate).Sum(t => t.OWQuantity) ?? 0))
                                                  + ((g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.IWQuantity) ?? 0) - (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.OWQuantity) ?? 0))),
                                                  DK = ((g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.IWAmount??0, lamTronGia))) - (g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.OWAmount??0, lamTronGia)))),
                                                  IWGK = g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia)),
                                                  OWGK = g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia)),
                                                  CK = (((g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia))) - (g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia))))
                                                  + ((g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia))) - (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia))))),
                                              }).ToList();
                model.SumSLDK = model.TongHopTonKhoDetails.Sum(d => d.SLDK);
                model.SumSLIWGK = model.TongHopTonKhoDetails.Sum(d => d.SLIWGK);
                model.SumSLOWGK = model.TongHopTonKhoDetails.Sum(d => d.SLOWGK);
                model.SumSLCK = model.TongHopTonKhoDetails.Sum(d => d.SLCK);
                model.SumDK = model.TongHopTonKhoDetails.Sum(d => Math.Round(d.DK,lamTronGia));
                model.SumIWGK = model.TongHopTonKhoDetails.Sum(d => Math.Round(d.IWGK, lamTronGia));
                model.SumOWGK = model.TongHopTonKhoDetails.Sum(d => Math.Round(d.OWGK, lamTronGia));
                model.SumCK = model.TongHopTonKhoDetails.Sum(d => Math.Round(d.CK, lamTronGia));
                lst.Add(model);
            }
            return lst;
        }
        public List<TongHopTonKho> getMaterialGood(DateTime from, DateTime to, List<Repository> lstrepos)
        {
            var ddSoGia = ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);
            DateTime fromdate = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0);
            DateTime todate = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);
            List<MaterialGoods> lstMate = GetAll();
            List<RepositoryLedger> lstRL = _IRepositoryLedgerService.Query.ToList().Where(x => lstrepos.Any(d => d.ID == x.RepositoryID) && lstMate.Any(d => d.ID == x.MaterialGoodsID)).ToList();
            
            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query;
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query;
            var list_genera1 = (from a in lstRL
                                join i in listSAInvoice on a.ReferenceID equals i.ID
                                join b in listSABill on i.BillRefID equals b.ID
                                join d in list_delete on b.ID equals d.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in lstRL
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            lstRL = (from a in lstRL
                     where !list_genera1.Any(t => t.ID == a.ID)
                     select a).ToList();

            lstRL = (from a in lstRL
                     where !list_genera2.Any(t => t.ID == a.ID)
                     select a).ToList();
            List<TongHopTonKho> lst = new List<TongHopTonKho>();
            foreach (var x in lstrepos)
            {
                TongHopTonKho model = new TongHopTonKho();
                model.RepositoryCode = x.RepositoryCode;
                model.RepositoryName = x.RepositoryName;
                model.TongHopTonKhoDetails = (from a in lstRL
                                              where a.RepositoryID == x.ID
                                              group a by new { a.MaterialGoodsID, a.RepositoryID }
                                              into g
                                              select new TongHopTonKhoDetail()
                                              {
                                                  MaterialGoodID = g.Key.MaterialGoodsID,
                                                  MaterialGoodCode = lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID) == null ? "" : lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID).MaterialGoodsCode,
                                                  MaterialGoodName = lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID) == null ? "" : lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID).MaterialGoodsName,
                                                  Unit = lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID) == null ? "" : lstMate.FirstOrDefault(c => c.ID == g.Key.MaterialGoodsID).Unit,
                                                  SLDK = ((g.Where(d => d.PostedDate < fromdate).Sum(t => t.IWQuantity) ?? 0) - (g.Where(d => d.PostedDate < fromdate).Sum(t => t.OWQuantity) ?? 0)),
                                                  SLIWGK = (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.IWQuantity) ?? 0),
                                                  SLOWGK = (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.OWQuantity) ?? 0),
                                                  SLCK = (((g.Where(d => d.PostedDate < fromdate).Sum(t => t.IWQuantity) ?? 0) - (g.Where(d => d.PostedDate < fromdate).Sum(t => t.OWQuantity) ?? 0))
                                                  + ((g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.IWQuantity) ?? 0) - (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => t.OWQuantity) ?? 0))),
                                                  DK = ((g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia))) - (g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia)))),
                                                  IWGK = g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia)),
                                                  OWGK = g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia)),
                                                  CK = (((g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia))) - (g.Where(d => d.PostedDate < fromdate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia))))
                                                  + ((g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.IWAmount ?? 0, lamTronGia))) - (g.Where(d => d.PostedDate >= fromdate && d.PostedDate < todate).Sum(t => Math.Round(t.OWAmount ?? 0, lamTronGia))))),
                                              }).ToList();
                model.SumSLDK = model.TongHopTonKhoDetails.Sum(d => d.SLDK);
                model.SumSLIWGK = model.TongHopTonKhoDetails.Sum(d => d.SLIWGK);
                model.SumSLOWGK = model.TongHopTonKhoDetails.Sum(d => d.SLOWGK);
                model.SumSLCK = model.TongHopTonKhoDetails.Sum(d => d.SLCK);
                model.SumDK = model.TongHopTonKhoDetails.Sum(d => d.DK);
                model.SumIWGK = model.TongHopTonKhoDetails.Sum(d => d.IWGK);
                model.SumOWGK = model.TongHopTonKhoDetails.Sum(d => d.OWGK);
                model.SumCK = model.TongHopTonKhoDetails.Sum(d => d.CK);
                if (model.TongHopTonKhoDetails.Count > 0)
                    lst.Add(model);
            }
            return lst;
        }
    }
}
