using System;
using System.Data.SqlClient;
using System.Linq.Dynamic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class SysRoleService : BaseService<SysRole, Guid>, ISysRoleService
    {
        ISysUserroleService _ISysUserroleService { get { return IoC.Resolve<ISysUserroleService>(); } }
        public SysRoleService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<SysRole> GetByRoleCode()
        {
            return Query.OrderBy(p => p.RoleCode).ToList();
        }

        public List<Guid> GetRoleByUserID(Guid userID)
        {
            List<SysUserrole> lstUserRole = _ISysUserroleService.GetAll().Where(p => p.UserID == userID).ToList();
            List<SysRole> lstRole = Query.ToList();
            var query = from a in lstRole
                        join b in lstUserRole
                        on a.RoleID equals b.RoleID
                        select a;
            List<SysRole> lstSysRole = query.ToList();
            List<Guid> lst = new List<Guid>();
            foreach(var item in lstSysRole)
            {
                lst.Add(item.RoleID);
            }

            return lst;
        }
    }

}