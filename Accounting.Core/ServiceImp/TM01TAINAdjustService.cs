using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM01TAINAdjustService: BaseService<TM01TAINAdjust ,Guid>,ITM01TAINAdjustService
    {
        public TM01TAINAdjustService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}