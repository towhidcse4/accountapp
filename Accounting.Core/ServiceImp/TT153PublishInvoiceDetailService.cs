using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153PublishInvoiceDetailService: BaseService<TT153PublishInvoiceDetail ,Guid>,ITT153PublishInvoiceDetailService
    {
        public TT153PublishInvoiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<TT153PublishInvoiceDetail> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportId)
        {
            return Query.Where(n =>From <= n.StartUsing && n.StartUsing <= To && n.TT153ReportID == TT153ReportId).ToList();
        }

        public List<TT153PublishInvoiceDetail> GetAllToDate(DateTime dateTime, Guid TT153ReportId)
        {
            return Query.Where(n => n.StartUsing < dateTime && n.TT153ReportID == TT153ReportId).ToList();
        }

        public List<TT153PublishInvoiceDetail> GetAllWithTT153ReportID(Guid TT153ReportID)
        {
            return Query.Where(n => n.TT153ReportID == TT153ReportID).ToList();
        }

        public int GetWithMaxToNo(Guid TT153ReportID)
        {
            return Query.Where(n => n.TT153ReportID == TT153ReportID).ToList().Select(n => int.Parse(n.ToNo)).Max();
        }

        public int GetWithMinFromNo(Guid TT153ReportID)
        {
            return Query.Where(n => n.TT153ReportID == TT153ReportID).ToList().Select(n => int.Parse(n.FromNo)).Min();
        }
    }

}