﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class ViewGLPayExceedCashService : BaseService<ViewGLPayExceedCash, Guid>, IViewGLPayExceedCashService
    {
        public ViewGLPayExceedCashService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private IRepositoryLedgerService _IRepositoryLedgerService
        {
            get { return IoC.Resolve<IRepositoryLedgerService>(); }
        }
        public List<ViewGLPayExceedCash> GetAll_OrderBy()
        {
            return (from a in Query
                    group a by a.Account into g
                    select new ViewGLPayExceedCash
                    {
                        Account = g.Key,
                        DebitAmount = g.Sum(t=>t.DebitAmount),
                        DebitAmountOriginal = g.Sum(t => t.DebitAmountOriginal),
                        CreditAmount = g.Sum(t => t.CreditAmount),
                        CreditAmountOriginal = g.Sum(t => t.CreditAmountOriginal),
                    }).ToList();
        }
    }
}
