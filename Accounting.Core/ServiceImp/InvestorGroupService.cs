using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class InvestorGroupService : BaseService<InvestorGroup, Guid>, IInvestorGroupService
    {
        public InvestorGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<InvestorGroup> OrderByGroupName()
        {
            return Query.OrderBy(c => c.InvestorGroupName).ToList();
        }
        public List<InvestorGroup> GetListByOrderFixCode(string stringStartWith)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(stringStartWith)).ToList();
        }
        public List<InvestorGroup> GetListByParentID(Guid ParentID)
        {
            return Query.Where(t => t.ParentID == ParentID).ToList();
        }
        public List<InvestorGroup> GetListByGrade(int grade)
        {
            return Query.Where(a => a.Grade == grade).ToList();
        }
        public List<InvestorGroup> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.InvestorGroupCode).ToList();
        }
    }

}