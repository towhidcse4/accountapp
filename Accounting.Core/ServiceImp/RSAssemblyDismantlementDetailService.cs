﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using System.Linq;
using FX.Data;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class RSAssemblyDismantlementDetailService : BaseService<RSAssemblyDismantlementDetail, Guid>, IRSAssemblyDismantlementDetailService
    {
        public RSAssemblyDismantlementDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.IList<RSAssemblyDismantlementDetail> getRSAssemblyDismantlementDetailbyID(Guid id)
        {
            return Query.Where(k => k.RSAssemblyDismantlementID == id).ToList();
        }
        /// <summary>
        /// Lấy danh sách RSAssemblyDismantlementDetailbyID từ RSAssemblyDismantlementDetail
        /// [Longtx]
        /// </summary>
        /// <param name="AssemblyDismantlementID"></param>
        /// <returns></returns>
        public List<RSAssemblyDismantlementDetail> GetListRSAssemblyDismantlementDetailbyID(Guid AssemblyDismantlementID)
        {
            return Query.Where(p => p.RSAssemblyDismantlementID == AssemblyDismantlementID).ToList(); ;
        }

    }

}