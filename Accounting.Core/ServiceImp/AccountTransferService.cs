using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class AccountTransferService : BaseService<AccountTransfer, Guid>, IAccountTransferService
    {
        public AccountTransferService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.List<string> GetListFromAccountFull()
        {
            return Query.Select(p => p.FromAccount).ToList();
        }

        public System.Collections.Generic.List<string> GetListToAccountFull()
        {
            return Query.Select(p => p.ToAccount).ToList();
        }
        public List<AccountTransfer> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.AccountTransferCode).ToList();
        }
    }

}