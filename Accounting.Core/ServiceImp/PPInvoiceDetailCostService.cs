﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class PPInvoiceDetailCostService : BaseService<PPInvoiceDetailCost, Guid>, IPPInvoiceDetailCostService
    {
        public PPInvoiceDetailCostService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public decimal GetAmountByPPService(Guid pps)
        {
            decimal? amount = 0;
            amount = Query.Where(x => x.PPServiceID == pps).Sum(x => x.AmountPB);
            return amount??0;
        }
        public decimal GetAmountByPPInvoice(Guid pps, Guid ppi, bool type)
        {
            var amount = Query.Where(x => x.PPInvoiceID != ppi && x.PPServiceID == pps && x.CostType == type).Sum(x=>x.AmountPB);
            return amount ?? 0;
        }
    }
}