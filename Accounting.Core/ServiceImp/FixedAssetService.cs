﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using NHibernate;

namespace Accounting.Core.ServiceImp
{
    public class FixedAssetService : FX.Data.BaseService<FixedAsset, Guid>, IFixedAssetService
    {
        public FixedAssetService(String sessionFactoryConfigPath) : base(sessionFactoryConfigPath) { }
        public IFAIncrementService _IFAIncrementService { get { return IoC.Resolve<IFAIncrementService>(); } }
        public IFAIncrementDetailService _IFAIncrementDetailService { get { return IoC.Resolve<IFAIncrementDetailService>(); } }
        public IFADecrementService _IFADecrementService { get { return IoC.Resolve<IFADecrementService>(); } }
        public IFADecrementDetailService _IFADecrementDetailService { get { return IoC.Resolve<IFADecrementDetailService>(); } }
        public IFADepreciationService _IFADepreciationService { get { return IoC.Resolve<IFADepreciationService>(); } }
        public IFADepreciationDetailService _IFADepreciationDetailService { get { return IoC.Resolve<IFADepreciationDetailService>(); } }
        public IFAAdjustmentService _IFAAdjustmentService { get { return IoC.Resolve<IFAAdjustmentService>(); } }
        public ISAReturnDetailService _ISAReturnDetailService { get { return IoC.Resolve<ISAReturnDetailService>(); } }
        public IFixedAssetLedgerService _IFixedAssetLedgerService { get { return IoC.Resolve<IFixedAssetLedgerService>(); } }
        public IFAInitService _IFAInitService { get { return IoC.Resolve<IFAInitService>(); } }
        public IFAAdjustmentDetailService _IFAAdjustmentDetailService { get { return IoC.Resolve<IFAAdjustmentDetailService>(); } }
        public IFATransferService _IFATransferService { get { return IoC.Resolve<IFATransferService>(); } }
        public IFATransferDetailService _IFATransferDetailService { get { return IoC.Resolve<IFATransferDetailService>(); } }
        public IFAAuditService _IFAAuditService { get { return IoC.Resolve<IFAAuditService>(); } }
        public IFAAuditDetailService _IFAAuditDetailService { get { return IoC.Resolve<IFAAuditDetailService>(); } }
        public IFADepreciationPostService _IFADepreciationPostService { get { return IoC.Resolve<IFADepreciationPostService>(); } }

        public ISession GetSessionState()
        {
            return NHibernateSession;
        }
        public int CheckExistFA(string fixedAssetCode, Guid id)
        {
            return Query.Where(c => c.FixedAssetCode.ToLower().Equals(fixedAssetCode.ToLower()) && c.ID != id).Count();
        }
        public List<FixedAsset> GetTimeUsed(Guid fixedAssetID)
        {
            return Query.Where(k => k.ID == fixedAssetID).ToList();
        }

        public List<FixedAsset> GetAll_OrderBy()
        {
            var faIncrementDetails = _IFAIncrementDetailService.GetAll();
            var result = Query.OrderBy(p => p.FixedAssetCode);
            foreach (var source in result)
            {
                var faIncrementDetail = faIncrementDetails.FirstOrDefault(x => x.FixedAssetID == source.ID);
                if (faIncrementDetail != null)
                    source.FAIncrementDebitAccount =
                        faIncrementDetail.DebitAccount;
            }
            return result.ToList();
        }

        public List<VoucherFixedAsset> GetListVoucherFixedAssets(Guid fixedAssetId)
        {
            //List<VoucherFixedAsset> result = new List<VoucherFixedAsset>();
            //var listFAIncrement = _IFAIncrementService.Query.ToList();
            //var listFAIncrementDetail = _IFAIncrementDetailService.Query.ToList();
            //var listFADecrement = _IFADecrementService.Query.ToList();
            //var listFADecrementDetail = _IFADecrementDetailService.Query.ToList();
            //var listFAAdjustment = _IFAAdjustmentService.Query.ToList();
            //var listFAAudit = _IFAAuditService.Query.ToList();
            var listFATranfer = _IFATransferService.Query.ToList();
            //var listFFADepreciation = _IFADepreciationService.Query.ToList();
            List<VoucherFixedAsset> result1=new List<VoucherFixedAsset>();
            //List<VoucherFixedAsset> result4 = new List<VoucherFixedAsset>();
            var result = _IFAIncrementService.Query
                        .Where(x => x.FAIncrementDetails.Any(c => c.FixedAssetID == fixedAssetId)).ToList(); 
            foreach (var item in result)
            {
                var temp = new VoucherFixedAsset
                {
                    ID = item.ID,
                    No = item.No,
                    TypeID = item.TypeID,
                    Date = item.Date,
                    PostedDate = item.PostedDate,
                    Reason = item.Reason,
                    TotalAmount = item.TotalOrgPrice ?? 0
                };

                result1.Add(temp);
            }

            //var result0 = _IFATransferService.Query
            //            .Where(x => x.FATransferDetails.Any(c => c.FixedAssetID == fixedAssetId)).ToList();
            //foreach (var item in result0)
            //{
            //    var temp = new VoucherFixedAsset
            //    {
            //        ID = item.ID,
            //        No = item.No,
            //        TypeID = item.TypeID,
            //        Date = item.Date,
            //        PostedDate = item.Date,
            //        Reason = item.Reason,
            //        TotalAmount = item.FATransferDetails.Where(c => c.FixedAssetID == fixedAssetId).ToList().Sum(x => x.Amount) ?? 0
            //    };

            //    result4.Add(temp);
            //}
            //=> new VoucherFixedAsset { ID = x.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalOrgPrice ?? 0, FixedAssetID = ded.FixedAssetID };

            //var faIncrement = from faIncre in listFAIncrement
            //                  where faIncre.FAIncrementDetails.Any(x => x.FixedAssetID == fixedAssetId)
            //                  select new { faIncre.ID, faIncre.No, faIncre.TypeID, faIncre.Date, faIncre.PostedDate, faIncre.Reason, TotalAmount = faIncre.TotalOrgPrice ?? 0 };
            var result2 = _IFADecrementService.Query
                        .Join(_IFADecrementDetailService.Query, de => de.ID, ded => ded.FADecrementID, (de, ded)
                            => new VoucherFixedAsset { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalAmount , FixedAssetID = ded.FixedAssetID })
                        .Where(x => x.FixedAssetID == fixedAssetId).ToList();
            //var faDecrement = from faDecre in listFADecrement
            //                  where faDecre.FADecrementDetails.Any(x => x.FixedAssetID == fixedAssetId)
            //                  select new { faDecre.ID, faDecre.No, faDecre.TypeID, faDecre.Date, faDecre.PostedDate, faDecre.Reason, faDecre.TotalAmount };

            var result3 = _IFAAuditService.Query
                       .Join(_IFAAuditDetailService.Query, de => de.ID, ded => ded.FAAuditID, (de, ded)

                           => new VoucherFixedAsset { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Description, TotalAmount = 0, FixedAssetID = ded.FixedAssetID })
                       .Where(x => x.FixedAssetID == fixedAssetId).ToList();


            //var faAudit = from faDecre in listFAAudit
            //              where faDecre.FAAuditDetails.Any(x => x.FixedAssetID == fixedAssetId)
            //              select new { faDecre.ID, faDecre.No, faDecre.TypeID, faDecre.Date, faDecre.PostedDate, Reason = faDecre.Description, TotalAmount = faDecre.FAAuditDetails.Where(c => c.FixedAssetID == fixedAssetId).ToList().Sum(x => x.DepreciationAmount) ?? 0 };
            //var result4 = _IFATransferService.Query
            //          .Join(_IFATransferDetailService.Query, de => de.ID, ded => ded.FATransferID, (de, ded)
            //              => new VoucherFixedAsset { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.Date, Reason = de.Reason, TotalAmount =  0, FixedAssetID = ded.FixedAssetID })
            //          .Where(x => x.FixedAssetID == fixedAssetId).ToList();
            var faTranfer = from faDecre in listFATranfer
                            where faDecre.FATransferDetails.Any(x => x.FixedAssetID == fixedAssetId)
                            select new VoucherFixedAsset { ID = faDecre.ID, No = faDecre.No, TypeID = faDecre.TypeID, Date = faDecre.Date, PostedDate = faDecre.Date, Reason = faDecre.Reason, TotalAmount = faDecre.FATransferDetails.Where(c => c.FixedAssetID == fixedAssetId).ToList().Sum(x => x.Amount) ?? 0, FixedAssetID = fixedAssetId };
            var result5 = _IFADepreciationService.Query
                     .Join(_IFADepreciationDetailService.Query, de => de.ID, ded => ded.FADepreciationID, (de, ded)
                         => new VoucherFixedAsset { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.Date, Reason = de.Reason, TotalAmount = de.TotalAmount, FixedAssetID = ded.FixedAssetID })
                     .Where(x => x.FixedAssetID == fixedAssetId).ToList();
            //var faDepreciation = from faDecre in listFFADepreciation
            //                     where faDecre.FADepreciationDetails.Any(x=>x.FixedAssetID == fixedAssetId)
            //                     select new { faDecre.ID, faDecre.No, faDecre.TypeID, faDecre.Date, faDecre.PostedDate, faDecre.Reason, faDecre.TotalAmount };
            var result6 = _IFAAdjustmentService.Query
                    .Join(_IFAAdjustmentDetailService.Query, de => de.ID, ded => ded.FAAdjustmentID, (de, ded)
                        => new VoucherFixedAsset { ID = de.ID, No = de.No, TypeID = de.TypeID, Date = de.Date, PostedDate = de.PostedDate, Reason = de.Reason, TotalAmount = de.TotalAmount, FixedAssetID = de.FixedAssetID })
                    .Where(x => x.FixedAssetID == fixedAssetId).ToList();
            //var faAdjustment = from faAdjust in listFAAdjustment
            //                   where faAdjust.FixedAssetID == fixedAssetId
            //                   select
            //                       new { faAdjust.ID, faAdjust.No, faAdjust.TypeID, faAdjust.Date, faAdjust.PostedDate, faAdjust.Reason, faAdjust.TotalAmount };

            //var unionall = faIncrement.Union(faDecrement).Union(faAdjustment).Union(faDepreciation).Union(faTranfer).Union(faAudit).OrderBy(p => p.No);

            //foreach (var item in unionall)
            //{
            //    var temp = new VoucherFixedAsset
            //    {
            //        ID = item.ID,
            //        No = item.No,
            //        TypeID = item.TypeID,
            //        Date = item.Date,
            //        PostedDate = item.PostedDate,
            //        Reason = item.Reason,
            //        TotalAmount = item.TotalAmount
            //    };

            //    result.Add(temp);
            //}
            result1.AddRange(result2);
            result1.AddRange(result3);
            result1.AddRange(faTranfer);
            result1.AddRange(result5);
            result1.AddRange(result6);

            return result1.OrderByDescending(c => c.PostedDate).ThenByDescending(c=>c.Date).ToList();
        }
        public List<FixedAsset> GetAll_OrderByFixedAssetCode()
        {
            return Query.OrderBy(p => p.FixedAssetCode).ToList();
        }
        public Guid? GetGuidByFixedAssetCode(string ma)
        {
            Guid? id = null;
            FixedAsset dp = Query.Where(p => p.FixedAssetCode == ma).SingleOrDefault();
            if (dp != null)
            {
                id = dp.ID;
            }
            return id;
        }
        public FixedAsset GetFixedAssetByCode(string ma)
        {
            
            FixedAsset dp = Query.Where(p => p.FixedAssetCode == ma).SingleOrDefault();
            if (dp != null)
            {
                return dp;
            }
            return null;
        }
        public List<FixedAsset> GetFAForDepreciation(DateTime postedDate)
        {
            var postedDate2 = postedDate.AddMonths(-1);
            var lst = new List<FixedAsset>();
            try
            {
                var temp = Query.Where(fix =>!fix.IsActive && fix.DepreciationDate.HasValue && postedDate >= fix.DepreciationDate.Value
                                        && (
                                            _IFAIncrementDetailService.Query.Any(fin => fin.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fin.FAIncrementID && fal.PostedDate <= postedDate))
                                            ||
                                            _IFAInitService.Query.Any(fai => fai.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fai.ID && fal.PostedDate <= postedDate))
                                            )
                                        && !_IFADecrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fde.FADecrementID && fal.PostedDate <= postedDate2))).OrderBy(fix => fix.FixedAssetCode).ToList();
                foreach (var fix in temp)
                {
                    if (_IFixedAssetLedgerService.Query.Where(fdep => fdep.FixedAssetID == fix.ID && postedDate >= fix.DepreciationDate.Value)
                            .OrderByDescending(fal => fal.PostedDate).FirstOrDefault().UsedTimeRemain != 0)
                    {
                        lst.Add(fix);
                    }
                }
                foreach (FixedAsset fix in lst)
                {
                    FixedAssetLedger fixedAssetLedger = _IFixedAssetLedgerService.Query.Where(fal => fal.FixedAssetID == fix.ID && fal.PostedDate <= postedDate)
                        .OrderByDescending(fal => fal.PostedDate)
                        .FirstOrDefault();
                    fix.AcDepreciationAmount = fixedAssetLedger.AcDepreciationAmount;
                    fix.DepartmentID = fixedAssetLedger.DepartmentID;
                    fix.OriginalPrice = fixedAssetLedger.OriginalPrice;
                    fix.RemainingAmount = fixedAssetLedger.RemainingAmount;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<Guid> GetFAIDForDepreciation(DateTime postedDate2)
        {
            DateTime postedDate = new DateTime(postedDate2.Year, postedDate2.Month, postedDate2.Day);
            var lst = new List<Guid>();
            try
            {
                var temp = Query.Where(fix => fix.DepreciationDate.HasValue && postedDate >= fix.DepreciationDate.Value
                                        && (
                                            _IFAIncrementDetailService.Query.Any(fin => fin.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fin.FAIncrementID && fal.PostedDate <= postedDate))
                                            ||
                                            _IFAInitService.Query.Any(fai => fai.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fai.ID && fal.PostedDate <= postedDate))
                                            )
                                        && !_IFADecrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fde.FADecrementID && fal.PostedDate >= postedDate))).OrderBy(fix => fix.FixedAssetCode).ToList();
                foreach (var fix in temp)
                {
                    if (_IFADepreciationPostService.Query.Where(fdep => fdep.FixedAssetID == fix.ID).ToList().Sum(fdep => fdep.Amount) < (fix.OriginalPrice ?? 0))
                    {
                        lst.Add(fix.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public IList<VoucherFixedAsset> FindAllFAUnionInit()
        {
            List<VoucherFixedAsset> result = new List<VoucherFixedAsset>();
            var fixedAssets = Query.Where(fix => !_IFADecrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID)).ToList();
            var faInits = _IFAInitService.GetAll().ToList();
            var union1 = from fixedasset in fixedAssets
                         select
                             new { fixedasset.ID, fixedasset.FixedAssetName, fixedasset.FixedAssetCode };
            var union2 = from fixedasset in faInits
                         select
                             new { fixedasset.ID, fixedasset.FixedAssetName, fixedasset.FixedAssetCode };
            var union = union1.Union(union2);
            foreach (var item in union)
            {
                var temp = new VoucherFixedAsset
                {
                    ID = item.ID,
                    FixedAssetCode = item.FixedAssetCode,
                    FixedAssetName = item.FixedAssetCode
                };

                result.Add(temp);
            }
            return result;
        }

        public List<FixedAsset> FindByNoIsNull(bool isNull)
        {
            try
            {
                if (isNull)
                {
                    return Query.Where(fix => fix.No == null).OrderBy(fix => fix.FixedAssetCode).ToList();
                    //return (from fixedasset in Query where fixedasset.No == null select fixedasset).ToList();
                }
                else
                {
                    return Query.Where(fix => fix.No != null).OrderBy(fix => fix.FixedAssetCode).ToList();
                    //return (from fixedasset in Query.ToList() select fixedasset).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<Guid> GetFixedAssetWithoutIncrement(string id)
        {

            var fixedAssets = Query.Where(fix => !_IFAIncrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID)
                                                    && !_IFAInitService.Query.Any(fde => fde.FixedAssetID == fix.ID)).Select(fix => fix.ID).ToList();
            if (id != null)
            {
                var includeFa = _IFAIncrementDetailService.Query.Where(fai => fai.FixedAssetID == new Guid(id)).Select(fai => fai.ID).ToList();
                fixedAssets.AddRange(includeFa);
            }
            return fixedAssets;
        }

        public List<Guid> GetFixedAssetWithoutDecrement(string id, DateTime PostedDate)
        {
            // lây ra danh sách danh mục đã ghi tăng ghi sổ và chưa ghi giảm chưa ghi sổ
            var pd = new DateTime(PostedDate.Year, PostedDate.Month, PostedDate.Day);
            var fixedAssets = Query.Where(fix => (_IFAIncrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fde.FAIncrementID && fal.PostedDate <= pd))
                                                    || _IFAInitService.Query.Any(fde => fde.FixedAssetID == fix.ID))
                                                    && !_IFADecrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fde.FADecrementID && fal.PostedDate <= pd))
                                                    ).Select(fix => fix.ID).ToList();
            if (id != null)
            {
                var includeFa = _IFADecrementDetailService.Query.Where(fai => fai.FixedAssetID == new Guid(id)).Select(fai => fai.ID).ToList();
                fixedAssets.AddRange(includeFa);
            }
            return fixedAssets.ToList();
        }

        public List<Guid> GetFixedAssetWithoutDecrementAndLedger(string id)
        {

            var fixedAssets = Query.Where(fix => !_IFADecrementDetailService.Query.Any(fde => fde.FixedAssetID == fix.ID && _IFixedAssetLedgerService.Query.Any(fal => fal.FixedAssetID == fix.ID && fal.ReferenceID == fde.FADecrementID))).Select(fix => fix.ID).ToList();
            if (id != null)
            {
                var includeFa = _IFADecrementDetailService.Query.Where(fai => fai.FixedAssetID == new Guid(id)).Select(fai => fai.ID).ToList();
                fixedAssets.AddRange(includeFa);
            }
            return fixedAssets;
        }

        public bool CheckDeleteIncrement(List<Guid?> fixedassetid)
        {
            return fixedassetid.Count > 0 ? _IFADecrementDetailService.Query.Any(fde => fixedassetid.Contains(fde.FixedAssetID))
                                       || _IFAAdjustmentService.Query.Any(fde => fixedassetid.Contains(fde.FixedAssetID))
                                       || _IFADepreciationDetailService.Query.Any(fde => fixedassetid.Contains(fde.FixedAssetID))
                                       || _IFAAuditDetailService.Query.Any(fde => fixedassetid.Contains(fde.FixedAssetID))
                                       || _IFATransferDetailService.Query.Any(fde => fixedassetid.Contains(fde.FixedAssetID)) : false;
        }

        public bool CheckDeleteFixedAsset(Guid fixedassetid)
        {
            return _IFAIncrementDetailService.Query.Any(fai => fai.FixedAssetID == fixedassetid)
                     || _IFAInitService.Query.Any(fai => fai.FixedAssetID == fixedassetid);
        }
    }
}
