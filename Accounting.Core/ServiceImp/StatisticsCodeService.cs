﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class StatisticsCodeService : BaseService<StatisticsCode, Guid>, IStatisticsCodeService
    {
        public StatisticsCodeService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// sắp xếp tăng dần theo tên mã thống kê [khanhtq]
        /// </summary>
        /// <returns></returns>
        public List<StatisticsCode> OrderByCode()
        {
            return Query.OrderBy(c => c.StatisticsCodeName).ToList();
        }
        /// <summary>
        /// Lấy danh sách Trường xử lý theo ID của mã thống kê cha [khanhtq]
        /// </summary>
        /// <param name="ParentID"></param>
        /// <returns></returns>
        public List<string> GListOrderFixCodeByParentID(Guid? ParentID)
        {
            return Query.Where(a => a.ParentID == ParentID).Select(a => a.OrderFixCode).ToList();
        }
        /// <summary>
        /// lấy danh sách Mã thống kê theo Trường xử lý [khanhtq]
        /// </summary>
        /// <param name="stringStart">Chuỗi bắt đầu của trường xử lý cần lấy</param>
        /// <returns>danh sách Mã thống kê</returns>
        public List<StatisticsCode> GListByOrderFixCode(string stringStart)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(stringStart)).ToList();
        }
        /// <summary>
        /// Lấy danh sách mã thông kê theo ID của mã thông kê cha [khanhtq]
        /// </summary>
        /// <param name="ParentID">ID mã thống kê cha</param>
        /// <returns>Danh sách mã thống kê</returns>
        public List<StatisticsCode> GListByParrentID(Guid ParentID)
        {
            return Query.Where(p => p.ParentID == ParentID).ToList();
        }
        /// <summary>
        /// Lấy danh sách mã thống kê theo Bậc mã thống kê
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public List<StatisticsCode> GListByGrade(int Grade)
        {
            return Query.Where(a => a.Grade == Grade).ToList();
        }
        /// <summary>
        /// //Lấy về danh sách các con của Mã thống kê
        /// </summary>
        /// <param name="OrderFixCode"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<StatisticsCode> GListChilds(string OrderFixCode, Guid ID)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(OrderFixCode) && p.ID != ID).ToList();
        }
        /// <summary>
        /// Lấy danh sách Mã thống kê
        /// </summary>
        /// <returns></returns>
        public List<string> GListStatisticsCode_()
        {
            return Query.Select(c => c.StatisticsCode_).ToList();
        }

        /// <summary>
        /// Lấy danh sách StatisticsCode từ StatisticsCode theo IsActive= true
        /// [Longtx]
        /// </summary>
        /// <param name="isActive">StatisticsCode</param>
        /// <returns>danh sách StatisticsCode có IsActive= true </returns>

        public List<StatisticsCode> GetStatisticsCodeBool(bool isActive)
        {
            return GetAll().OrderBy(p => p.StatisticsCode_).Where(p => p.IsActive == isActive).ToList();
        }



        /// <summary>
        /// Lấy danh sách các StatisticsCode theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách StatisticsCode được sắp xếp theo dạng cây cha con</returns>
        public List<StatisticsCode> GetByActive_OrderByTreeIsParentNode(bool isActive)
        {
            List<StatisticsCode> get = new List<StatisticsCode>();
            List<StatisticsCode> lst = Query.ToList();
            List<StatisticsCode> parent = lst.Where(x => x.IsActive == isActive && x.ParentID == null).OrderBy(x => x.StatisticsCode_).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode) && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }

        /// <summary>
        /// Lấy danh sách các StatisticsCode và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách StatisticsCode được sắp xếp theo dạng cây cha con</returns>
        public List<StatisticsCode> GetAll_OrderByTreeIsParentNode()
        {
            List<StatisticsCode> get = new List<StatisticsCode>();
            List<StatisticsCode> lst = Query.ToList();
            List<StatisticsCode> parent = lst.Where(x => x.ParentID == null).OrderBy(x => x.StatisticsCode_).ToList();
            foreach (var item in parent)
            {
                get.Add(item);
                get.AddRange(lst.Where(p => p.OrderFixCode.StartsWith(item.OrderFixCode + "-") && p.ID != item.ID).OrderBy(p => p.OrderFixCode).ToList());
            }
            return get;
        }

        public Guid GetGuidStatisticsCodeByCode(string code)
        {
            Guid id = Guid.Empty;
            StatisticsCode ag = Query.Where(p => p.StatisticsCode_ == code).SingleOrDefault();
            if (ag != null)
            {
                id = ag.ID;
            }
            return id;
        }
    }

}