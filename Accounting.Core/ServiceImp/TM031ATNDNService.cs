using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM031ATNDNService: BaseService<TM031ATNDN ,Guid>,ITM031ATNDNService
    {
        public TM031ATNDNService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}