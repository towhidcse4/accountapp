using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentPersonalIncomeService : BaseService<MCPaymentPersonalIncome, Guid>, IMCPaymentPersonalIncomeService
    {
        public MCPaymentPersonalIncomeService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}