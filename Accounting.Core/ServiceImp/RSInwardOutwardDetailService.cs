﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;


namespace Accounting.Core.ServiceImp
{
    public class RSInwardOutwardDetailService : BaseService<RSInwardOutwardDetail, Guid>, IRSInwardOutwardDetailService
    {
        public RSInwardOutwardDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public IList<RSInwardOutwardDetail> getRSInwardOutwardDetaillbyID(Guid id)
        {
            return Query.Where(k => k.RSInwardOutwardID == id).ToList();
        }
        /// <summary>
        /// Lấy danh sách RSInwardOutwardID từ RSInwardOutwardDetail
        /// [Longtx] 
        /// </summary>
        /// <param name="InwardOutwarID"></param>
        /// <returns></returns>
        public List<RSInwardOutwardDetail> GetListTempDetailID(Guid InwardOutwarID)
        {
            return Query.Where(p => p.RSInwardOutwardID == InwardOutwarID).ToList();
        }

        #region Lưu bảng RSInwardOutwardDetail
        #region Form Hàng mua trả lại

        /// <summary>
        /// Lấy DS lưu bảng RSInwardOutwardDetail
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="listTempDetail">Ds PPDiscountReturnDetail theo đối tượng PPDiscountReturn</param>
        /// <param name="rsOutwardID">ID phiếu nhập kho</param>
        /// <param name="isAdd"></param>
        /// <returns></returns>
        public List<RSInwardOutwardDetail> GetListSaveRSInwardOutwardDetail(PPDiscountReturn temp,
                                                                            List<PPDiscountReturnDetail> listTempDetail,
                                                                            Guid rsOutwardID, bool isAdd = true, List<RSInwardOutwardDetail> listRSOutwardDetail = null)
        {
            if (isAdd)
                listRSOutwardDetail = new List<RSInwardOutwardDetail>();
            else if (listRSOutwardDetail == null)
                listRSOutwardDetail = getRSInwardOutwardDetaillbyID(rsOutwardID).ToList();
            for (int i = 0; i < listTempDetail.Count; i++)
            {
                RSInwardOutwardDetail rsOutwardDetailTemp = new RSInwardOutwardDetail();
                if (isAdd)
                {
                    listRSOutwardDetail.Add(rsOutwardDetailTemp);
                    listRSOutwardDetail[i].ID = Guid.NewGuid();
                }
                listRSOutwardDetail[i].RSInwardOutwardID = rsOutwardID;
                listRSOutwardDetail[i].MaterialGoodsID = listTempDetail[i].MaterialGoodsID;
                listRSOutwardDetail[i].Description = listTempDetail[i].Description;
                listRSOutwardDetail[i].RepositoryID = listTempDetail[i].RepositoryID;
                listRSOutwardDetail[i].DebitAccount = listTempDetail[i].DebitAccount;
                listRSOutwardDetail[i].CreditAccount = listTempDetail[i].CreditAccount;
                listRSOutwardDetail[i].Unit = listTempDetail[i].Unit;
                listRSOutwardDetail[i].Quantity = listTempDetail[i].Quantity;
                listRSOutwardDetail[i].QuantityConvert = listTempDetail[i].QuantityConvert;
                listRSOutwardDetail[i].UnitPrice = listTempDetail[i].UnitPrice == null ? 0 : (decimal)listTempDetail[i].UnitPrice;
                listRSOutwardDetail[i].UnitPriceOriginal = listTempDetail[i].UnitPriceOriginal == null ? 0 : (decimal)listTempDetail[i].UnitPriceOriginal;
                listRSOutwardDetail[i].UnitPriceConvert = listTempDetail[i].UnitPriceConvert == null ? 0 : (decimal)listTempDetail[i].UnitPriceConvert;
                listRSOutwardDetail[i].UnitPriceConvertOriginal = listTempDetail[i].UnitPriceConvertOriginal == null ? 0 : (decimal)listTempDetail[i].UnitPriceConvertOriginal;
                listRSOutwardDetail[i].Amount = listTempDetail[i].Amount == null ? 0 : (decimal)listTempDetail[i].Amount;
                listRSOutwardDetail[i].AmountOriginal = listTempDetail[i].AmountOriginal == null ? 0 : (decimal)listTempDetail[i].AmountOriginal;
                listRSOutwardDetail[i].EmployeeID = temp.EmployeeID;
                listRSOutwardDetail[i].BudgetItemID = listTempDetail[i].BudgetItemID;
                listRSOutwardDetail[i].CostSetID = listTempDetail[i].CostSetID;
                listRSOutwardDetail[i].ContractID = listTempDetail[i].ContractID;
                listRSOutwardDetail[i].StatisticsCodeID = listTempDetail[i].StatisticsCodeID;
                listRSOutwardDetail[i].ExpiryDate = listTempDetail[i].ExpiryDate;
                listRSOutwardDetail[i].LotNo = listTempDetail[i].LotNo;
                listRSOutwardDetail[i].UnitConvert = listTempDetail[i].UnitConvert;
                listRSOutwardDetail[i].ConvertRate = listTempDetail[i].ConvertRate;
                listRSOutwardDetail[i].DepartmentID = listTempDetail[i].DepartmentID;
                listRSOutwardDetail[i].ExpenseItemID = listTempDetail[i].ExpenseItemID;
                listRSOutwardDetail[i].IsIrrationalCost = false;
            }
            return listRSOutwardDetail;
        }

        #endregion
        #endregion
    }

}