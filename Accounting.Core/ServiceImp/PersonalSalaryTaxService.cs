using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class PersonalSalaryTaxService : BaseService<PersonalSalaryTax, Guid>, IPersonalSalaryTaxService
    {
        public PersonalSalaryTaxService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}