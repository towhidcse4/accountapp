﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class AppUserService : BaseService<AppUser, Guid>, IAppUserService
    {
        public AppUserService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }       
        
        public IUserGroupService _IUserGroupService { get { return IoC.Resolve<IUserGroupService>(); } }
        public IRoleService _IRoleService { get { return IoC.Resolve<IRoleService>(); } }
        public IPermissionService _IPermissionService { get { return IoC.Resolve<IPermissionService>(); } }
        public IRolePermissionService _IRolePermissionService { get { return IoC.Resolve<IRolePermissionService>(); } }
        public IUserRoleService _IUserRoleService { get { return IoC.Resolve<IUserRoleService>(); } }

        public AuthenticatedUser CheckUserAuthentication(string userName, string password)
        {
            AuthenticatedUser retVal = new AuthenticatedUser();
            AppUser user = Query.Where(u => u.UserName == userName && u.Password == password).FirstOrDefault();
            if (user != null)
            {
                retVal.UserName = user.UserName;
                retVal.Password = user.Password;
                retVal.GroupName = _IUserGroupService.Query.Where(g => g.UserGroupId == user.GroupUserId).Select(g => g.GroupName).FirstOrDefault();
                retVal.FirstName = user.FirstName;
                retVal.LastName = user.Lastname;

                List<UserRole> lstUserRole = _IUserRoleService.Query.Where(ur => ur.UserId == user.UserId).ToList();
                List<RolePermission> lstUserRolePermission = new List<RolePermission>();
                foreach (UserRole ur in lstUserRole)
                {
                    List<RolePermission> lstPermission = _IRolePermissionService.Query.Where(rp => rp.RoleId == ur.RoleId).ToList();
                    //lstUserRolePermission.Concat(lstPermission);
                    lstUserRolePermission.AddRange(lstPermission);
                }

                Hashtable role = new Hashtable();
                foreach (UserRole r in lstUserRole)
                {
                    role.Add(r.RoleId, r.RoleId);
                }
                retVal.Roles = role;

                Hashtable permission = new Hashtable();
                foreach (RolePermission p in lstUserRolePermission)
                {
                    permission.Add(p.PermissionId, p.PermissionId);
                }
                retVal.Permissions = permission;
            }

            return retVal;
        }
        
    }

}


//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections;
//using FX.Data;
//using FX.Core;
//using Accounting.Core;
//using Accounting.Core.IService;

//namespace Accounting.Core.ServiceImp
//{
//    public class AppUserService:BaseService<AppUser,Guid>, IAppUserService
//    {
//        public AppUserService(string sessionFactoryConfigPath):base(sessionFactoryConfigPath){}
//        public IUserGroupService _IUserGroupService { get { return IoC.Resolve<IUserGroupService>(); } }
//        public IRoleService _IRoleService { get { return IoC.Resolve<IRoleService>(); } }
//        public IPermissionService _IPermissionService { get { return IoC.Resolve<IPermissionService>(); } }
//        public IRolePermissionService _IRolePermissionService { get { return IoC.Resolve<IRolePermissionService>(); } }
//        public IUserRoleService _IUserRoleService { get { return IoC.Resolve<IUserRoleService>(); } }

//        public AuthenticatedUser CheckUserAuthentication(string userName, string password)
//        {
//            AuthenticatedUser retVal = new AuthenticatedUser();
//            AppUser user = Query.Where(u => u.UserName == userName && u.Password == password).FirstOrDefault();
//            if (user != null)
//            {
//                List<UserRole> lstUserRole = _IUserRoleService.Query.Where(ur => ur.UserId == user.UserId).ToList();
//                List<RolePermission> lstUserRolePermission = new List<RolePermission>();
//                foreach (UserRole ur in lstUserRole)
//                {
//                    lstUserRolePermission.Concat(_IRolePermissionService.Query.Where(rp => rp.RoleId == ur.RoleId).ToList());
//                }

//                Hashtable role = new Hashtable();
//                foreach (UserRole r in lstUserRole)
//                {
//                    role.Add(r.RoleId, r.RoleId);
//                }
//                retVal.Roles = role;

//                Hashtable permission = new Hashtable();
//                foreach (RolePermission p in lstUserRolePermission)
//                {
//                    permission.Add(p.PermissionId, p.PermissionId);
//                }
//                retVal.Permissions = permission;
//            }

//            return retVal;
//        }
//    }
//}
