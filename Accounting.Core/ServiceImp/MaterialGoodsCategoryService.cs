﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MaterialGoodsCategoryService : BaseService<MaterialGoodsCategory, Guid>, IMaterialGoodsCategoryService
    {
        public MaterialGoodsCategoryService (string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        //public List<MaterialGoodsCategory> getMaterialGoodsCategorysbyLikes(MaterialGoodsCategory _MaterialGoodsCategory)
        //{
        //    return Query.Where(k => k.MaterialGoodsCode.StartsWith(_MaterialGoodsCategory.MaterialGoodsCode) && k.IsParentNode == false).ToList();
        //}

        //public MaterialGoodsCategory getMaterialGoodsCategorysby_MaterialGoodsCode(string _MaterialGoodsCode)
        //{
        //    return Query.SingleOrDefault(k => k.MaterialGoodsCode.Equals(_MaterialGoodsCode));
        //}

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCategory các MaterialGoodsCategory cuối cùng
        /// theo một đối tượng _MaterialGoodsCategory và IsParentNode
        /// [DUYTN]
        /// </summary>
        /// <param name="_MaterialGoodsCategory"> là đối tượng MaterialGoodsCategory</param>
        /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
        public List<MaterialGoodsCategory> getMaterialGoodsCategorysbyLikes(MaterialGoodsCategory _MaterialGoodsCategory, Boolean IsParentNode)//IsParentNode = false
        {
            return Query.Where(k => k.MaterialGoodsCategoryCode.StartsWith(_MaterialGoodsCategory.MaterialGoodsCategoryCode) && k.IsParentNode == IsParentNode).ToList();
        }

        /// <summary>
        /// Lấy ra một đối tượng MaterialGoodsCategory theo MaterialGoodsCode
        /// [DUYTN]
        /// </summary>
        /// <param name="_MaterialGoodsCode">là MaterialGoodsCode được truyền vào</param>
        /// <returns>Một đối tượng MaterialGoodsCategory, null nếu bị lỗi</returns>
        public MaterialGoodsCategory getMaterialGoodsCategorysbyAccountNumber(string _MaterialGoodsCode)
        {
            return Query.SingleOrDefault(k => k.MaterialGoodsCategoryCode.Equals(_MaterialGoodsCode));
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCategory theo IsActive
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
        public List<MaterialGoodsCategory> GetAll_ByIsParentNode(bool IsParentNode)
        {
            return Query.Where(p => p.IsParentNode == IsParentNode).ToList();
        }
        public List<MaterialGoodsCategory> GetAll_ByIsActive(bool IsActive)
        {
            return Query.Where(p => p.IsActive == true).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCategory theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
        public List<MaterialGoodsCategory> GetAll_ByParentID(Guid ParentID)
        {
            return Query.Where(p => p.ParentID == ParentID).ToList();
        }

        /// <summary>
        /// Đếm số dòng dữ liệu theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID lọc</param>
        /// <returns>Số dòng, null nếu bị lỗi</returns>
        public int CountByParentID(Guid? ParentID)
        {
            return Query.Count(p => p.ParentID == ParentID);
        }

        /// <summary>
        /// Lấy ra danh sách OrderFixCode theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID"> là ParentID truyền vào</param>
        /// <returns>tập các OrderFixCode, null nếu bị lỗi</returns>
        public List<string> GetOrderFixCode_ByParentID(Guid? ParentID)
        {
            return Query.Where(a => a.ParentID == ParentID).Select(a => a.OrderFixCode).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCategory (các con cuối cùng)
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode">là OrderFixCode vị trí của một đối tượng MaterialGoodsCategory </param>
        /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
        public List<MaterialGoodsCategory> GetStartsWith_OrderFixCode(string OrderFixCode)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(OrderFixCode)).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCategory theo Grade
        /// [DUYTN]
        /// </summary>
        /// <param name="Grade"> là tham số bậc của Grade, i là số nguyên</param>
        /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
        public List<MaterialGoodsCategory> GetAll_ByGrade(int Grade)
        {
            return Query.Where(a => a.Grade == Grade).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các MaterialGoodsCode, null nếu bị lỗi</returns>
        public List<string> GetMaterialGoodsCode()
        {
            return Query.Select(p => p.MaterialGoodsCategoryCode).ToList();
        }
        public List<MaterialGoodsCategory> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.MaterialGoodsCategoryCode).ToList();
        }
        public string GetMaterialGoodsCategoryCodeByGuid(Guid? guid)
        {
            return Query.Where(p => p.ID == guid).FirstOrDefault().MaterialGoodsCategoryCode;
        }
        public Guid? GetGuidMaterialGoodsCategoryByCode(string code)
        {
            Guid? id = null;
            MaterialGoodsCategory mgC = Query.Where(p => p.MaterialGoodsCategoryCode == code).SingleOrDefault();
            if (mgC != null)
            {
                id = mgC.ID;
            }
            return id;
        }
    }
}

