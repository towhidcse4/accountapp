﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class SynthesisReportService : BaseService<SynthesisReport, Guid>, ISynthesisReportService
    {
        public SynthesisReportService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private static IFormulaOperandUsingService FormulaOperandUsingService
        {
            get { return IoC.Resolve<IFormulaOperandUsingService>(); }
        }
        private static IGeneralLedgerService IGeneralLedgerService
        {
            get { return IoC.Resolve<IGeneralLedgerService>(); }
        }
        private static List<FormulaOperandUsing> _lstFormulaOperandUsing = new List<FormulaOperandUsing>();
        public static List<FormulaOperandUsing> LstFormulaOperandUsing
        {
            get
            {
                if (_lstFormulaOperandUsing.Count == 0)
                    _lstFormulaOperandUsing = FormulaOperandUsingService.GetAll();
                return _lstFormulaOperandUsing;
            }
            set { _lstFormulaOperandUsing = value; }
        }

        /// <summary>
        /// Lấy ra danh sách các dòng báo cáo được phép chỉnh sửa
        /// [DuyNT]
        /// </summary>
        /// <param name="isDefault">được phép chỉnh sửa (mặc định bằng true)</param>
        /// <returns>null nếu lỗi</returns>
        public List<SynthesisReport> GetAll_ByIsDefault(bool isDefault = true)
        {
            return Query.Where(c => c.IsDefault == isDefault).OrderBy(c => c.ItemIndex).ToList();
        }
        /// <summary>
        /// Lấy ra danh sách báo cáo BalanceSheet bảng cân đối kết toán
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns>null nếu lỗi</returns>
        public List<Assets> ReportBalanceSheet(DateTime fromDate, DateTime toDate)
        {
            List<Assets> dsAssetses = new List<Assets>();
            var dsSynthesisReport = GetAll_ByIsDefault().Where(c => c.TypeID == 1).ToList();
            foreach (var row in dsSynthesisReport)
            {
                Assets assets = new Assets();
                PropertyInfo[] propertyInfo = assets.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(assets, propItem.GetValue(row, null), null);
                    }
                }
                if (row.IsUsedFormula || !string.IsNullOrEmpty(row.Formula1))
                {
                    ValueSynthesis(assets, fromDate, toDate);
                }
                if (Convert.ToInt32(assets.ItemIndex) < 35)
                {
                    assets.GroupID = 1;
                    assets.GroupName = "TÀI SẢN";
                }
                else if (Convert.ToInt32(assets.ItemIndex) >= 35 && Convert.ToInt32(assets.ItemIndex) <= 65)
                {
                    assets.GroupID = 2;
                    assets.GroupName = "NGUỒN VỐN";
                }
                else if (Convert.ToInt32(assets.ItemIndex) > 65)
                {
                    assets.GroupID = 3;
                    assets.GroupName = "CÁC CHỈ TIÊU NGOÀI BẢNG CÂN ĐỐI KẾ TOÁN";
                }
                dsAssetses.Add(assets);
            }
            #region một số công thức khác
            foreach (Assets synthesisReport in dsAssetses)
            {
                if (synthesisReport.ItemCode == "100" && !synthesisReport.IsUsedFormula)//tài sản ngắn hạn
                {
                    List<string> ds = new List<string>() { "110", "120", "130", "140", "150" };
                    synthesisReport.AmountBeginning = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountBeginning)).Sum();
                    synthesisReport.AmountEnding = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountEnding)).Sum();
                }
                else if (synthesisReport.ItemCode == "200" && !synthesisReport.IsUsedFormula) //tài sản dài hạn
                {
                    List<string> ds = new List<string>() { "210", "220", "230", "240" };
                    synthesisReport.AmountBeginning = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountBeginning)).Sum();
                    synthesisReport.AmountEnding = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountEnding)).Sum();
                }
                else if (synthesisReport.ItemCode == "250" && !synthesisReport.IsUsedFormula)//tổng cộng tài sản
                {
                    List<string> ds = new List<string>() { "100", "200" };
                    synthesisReport.AmountBeginning = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountBeginning)).Sum();
                    synthesisReport.AmountEnding = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountEnding)).Sum();
                }
                else if (synthesisReport.ItemCode == "300" && !synthesisReport.IsUsedFormula)//nợ phải trả
                {
                    List<string> ds = new List<string>() { "310", "330" };
                    synthesisReport.AmountBeginning = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountBeginning)).Sum();
                    synthesisReport.AmountEnding = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountEnding)).Sum();
                }
                else if (synthesisReport.ItemCode == "400" && !synthesisReport.IsUsedFormula)//vốn chủ sở hữu
                {
                    synthesisReport.AmountBeginning = (dsAssetses.Where(c => c.ItemCode == "410").Select(c => c.AmountBeginning)).Sum();
                    synthesisReport.AmountEnding = (dsAssetses.Where(c => c.ItemCode == "410").Select(c => c.AmountEnding)).Sum();
                }
                else if (synthesisReport.ItemCode == "440" && !synthesisReport.IsUsedFormula)//tổng cộng vốn
                {
                    List<string> ds = new List<string>() { "300", "400" };
                    synthesisReport.AmountBeginning = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountBeginning)).Sum();
                    synthesisReport.AmountEnding = (dsAssetses.Where(c => ds.Contains(c.ItemCode)).Select(c => c.AmountEnding)).Sum();
                }
            }
            #endregion
            return dsAssetses;
        }
        /// <summary>
        /// Lấy ra danh sách báo cáo IncomeStatement Báo cáo kết quả kinh doanh
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns>null nếu lỗi</returns>
        public List<Assets> ReportIncomeStatement(DateTime fromDate, DateTime toDate)
        {
            List<Assets> dsAssetses = new List<Assets>();
            var dsSynthesisReport = GetAll_ByIsDefault().Where(c => c.TypeID == 2).ToList();
            foreach (var row in dsSynthesisReport)
            {
                Assets assets = new Assets();
                PropertyInfo[] propertyInfo = assets.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(assets, propItem.GetValue(row, null), null);
                    }
                }
                if (row.IsUsedFormula || !string.IsNullOrEmpty(row.Formula1))
                {
                    ValueSynthesis(assets, fromDate, toDate);
                }
                dsAssetses.Add(assets);
            }
            return dsAssetses;
        }

        private Assets ValueSynthesis(Assets assets, DateTime fromDate, DateTime toDate)
        {
            #region xử lý chuỗi công thức lấy ra kết quả
            List<string> keystart = LstFormulaOperandUsing.Where(c => c.TypeID == assets.TypeID).Select(c => c.FormulaOperandUsingName + "[").ToList();
            Dictionary<string, string> dictionaryCuoiNam = new Dictionary<string, string>();
            Dictionary<string, string> dictionaryDauNam = new Dictionary<string, string>();
            string chuoiCongThuc = FormatExpression(assets.Formula1);
            if (!string.IsNullOrEmpty(chuoiCongThuc))
                for (int i = 0; i < keystart.Count(); i++)
                {
                    int viTriTimKiem = chuoiCongThuc.IndexOf(keystart[i], System.StringComparison.Ordinal);
                    while (viTriTimKiem != -1)
                    {
                        int viTriTimThay = chuoiCongThuc.IndexOf(keystart[i], viTriTimKiem, StringComparison.Ordinal);//vị trí đầu tiên
                        int viTriKetThucKey1 = viTriTimThay + keystart[i].Length;//vị trí chứa toàn bộ key
                        #region tìm thấy giá trị trùng key
                        int viTriCoDauCach = 0;
                        viTriCoDauCach = chuoiCongThuc.IndexOf(" ", viTriKetThucKey1, StringComparison.Ordinal);//vị trị có dấu cách, kết thúc key 1
                        if (viTriCoDauCach == 0) viTriCoDauCach = chuoiCongThuc.IndexOf(")", viTriKetThucKey1, StringComparison.Ordinal);//hoặc vị trị có ), kết thúc key 1
                        if (viTriKetThucKey1 >= 0 && viTriKetThucKey1 <= chuoiCongThuc.Length)
                        {
                            string chuoiLayRa = "";
                            if (viTriCoDauCach > 0)//nếu vị trí có dấu cách (có vị trí cuối cùng)
                                chuoiLayRa = chuoiCongThuc.Substring(viTriTimThay, viTriCoDauCach - viTriTimThay);
                            else if (viTriCoDauCach <= 0)//nếu không có vị trí cuối cùng
                                chuoiLayRa = chuoiCongThuc.Substring(viTriTimThay);
                            chuoiLayRa = chuoiLayRa.Replace(")", "");
                            if (chuoiLayRa.Length > 0)//chuỗi lấy ra khác chuỗi rỗng
                            {
                                decimal giaTriTuongUngDauNam = 0;
                                decimal giaTriTuongUngCuoiNam = 0;
                                string giaTriGanCuoiNam = "";
                                string giaTriGanDauNam = "";
                                #region cắt một chuỗi công thức thành các tham số đưa vào hàm
                                string accountNumber =
                                    chuoiLayRa.Substring(chuoiLayRa.IndexOf("[", StringComparison.Ordinal) + 1)
                                        .Replace("]", "");

                                List<string> listAccountNumber = accountNumber.Split(';').ToList();
                                if (listAccountNumber.Count > 0 && listAccountNumber.Count < 2)//nếu có một tham số
                                {
                                    string nameFun = keystart[i].Replace("[", "");
                                    UltilsSynthesisReport objReport = new UltilsSynthesisReport();
                                    giaTriTuongUngCuoiNam = (decimal)objReport.GetType().GetMethod(nameFun).Invoke(objReport, new object[] { fromDate, toDate, listAccountNumber[0] });
                                    giaTriTuongUngDauNam = (decimal)objReport.GetType().GetMethod("SD_DK").Invoke(objReport, new object[] { fromDate, toDate, listAccountNumber[0] });
                                }
                                else if (listAccountNumber.Count > 1 && listAccountNumber.Count < 3)//nếu có hai tham số
                                {
                                    string nameFun = keystart[i].Replace("[", "");
                                    UltilsSynthesisReport objReport = new UltilsSynthesisReport();
                                    if (LstFormulaOperandUsing.Single(c => c.TypeID == assets.TypeID && c.FormulaOperandUsingName == keystart[i].Replace("[","")).Description.ToLower().Contains("hoạt động"))
                                    {
                                        giaTriTuongUngCuoiNam = (decimal)objReport.GetType().GetMethod(nameFun).Invoke(objReport, new object[] { fromDate, toDate, listAccountNumber[0], listAccountNumber[1], CompareValueGeneralLedger(fromDate,toDate) });
                                        giaTriTuongUngDauNam = (decimal)objReport.GetType().GetMethod(nameFun + "_NT").Invoke(objReport, new object[] { fromDate, toDate, listAccountNumber[0], listAccountNumber[1] });
                                    }
                                    else
                                    {
                                        giaTriTuongUngCuoiNam = (decimal)objReport.GetType().GetMethod(nameFun).Invoke(objReport, new object[] { fromDate, toDate, listAccountNumber[0], listAccountNumber[1] });
                                        giaTriTuongUngDauNam = (decimal)objReport.GetType().GetMethod(nameFun + "_NT").Invoke(objReport, new object[] { fromDate, toDate, listAccountNumber[0], listAccountNumber[1] });
                                    }                            
                                }
                                #endregion
                                giaTriGanCuoiNam = giaTriTuongUngCuoiNam.ToString();
                                giaTriGanDauNam = giaTriTuongUngDauNam.ToString();
                                if (!dictionaryCuoiNam.Keys.ToList().Contains(chuoiLayRa))
                                {
                                    dictionaryCuoiNam.Add(chuoiLayRa, giaTriGanCuoiNam);
                                    dictionaryDauNam.Add(chuoiLayRa, giaTriGanDauNam);
                                }
                            }
                        }
                        #endregion
                        //vị trí tiếp theo cần tìm
                        viTriTimKiem = chuoiCongThuc.IndexOf(keystart[i], viTriTimKiem + viTriKetThucKey1 - viTriTimThay, StringComparison.Ordinal);
                    }
                }
            List<string> list = dictionaryCuoiNam.Keys.ToList();
            string chuoiCongThucCuoiNam = list.Aggregate(chuoiCongThuc, (current, s) => current.Replace(s, dictionaryCuoiNam[s].ToString()));
            //foreach (string s in list)
            //    chuoiCongThuc = chuoiCongThuc.Replace(s, dictionary[s].ToString());
            chuoiCongThucCuoiNam = "0 + " + chuoiCongThucCuoiNam;
            list = dictionaryDauNam.Keys.ToList();
            string chuoiCongThucDauNam = list.Aggregate(chuoiCongThuc, (current, s) => current.Replace(s, dictionaryDauNam[s].ToString()));
            chuoiCongThucDauNam = "0 + " + chuoiCongThucDauNam;
            #endregion
            assets.AmountBeginning = EvaluatePostfix(Infix2Postfix(FormatExpression(chuoiCongThucCuoiNam))) * assets.FormulaOfIndicator;
            assets.AmountEnding = EvaluatePostfix(Infix2Postfix(FormatExpression(chuoiCongThucDauNam))) * assets.FormulaOfIndicator;
            return assets;
        }

        /// <summary>
        /// Lấy ra danh sách báo cáo CashFlowStatement Báo cáo lưu chuyển tiền tệ
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<Assets> ReportCashFlowStatement(DateTime fromDate, DateTime toDate)
        {
            List<Assets> dsAssetses = new List<Assets>();
            var dsSynthesisReport = GetAll_ByIsDefault().Where(c => c.TypeID == 3).ToList();
            foreach (var row in dsSynthesisReport)
            {
                Assets assets = new Assets();
                PropertyInfo[] propertyInfo = assets.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(assets, propItem.GetValue(row, null), null);
                    }
                }
                if (row.IsUsedFormula || !string.IsNullOrEmpty(row.Formula1))
                {
                    ValueSynthesis(assets, fromDate, toDate);
                }
                dsAssetses.Add(assets);
            }
            return dsAssetses;
        }

        private IEnumerable<GeneralLedgerChild> GetGeneralLedgerChild()
        {
            var dsSynthesisReport = GetAll_ByIsDefault().Where(c => c.TypeID == 3).ToList();
            foreach (var row in dsSynthesisReport)
            {
                _dsGeneralLedgerChild.AddRange(ValueGeneralLedgerChild(row));
            }
            return _dsGeneralLedgerChild;
        }

        private List<GeneralLedgerChild> _dsGeneralLedgerChild = new List<GeneralLedgerChild>();
        public List<GeneralLedgerChild> DsGeneralLedgerChild
        {
            get
            {
                if (_dsGeneralLedgerChild.Count == 0)
                    _dsGeneralLedgerChild = GetGeneralLedgerChild().ToList();
                return _dsGeneralLedgerChild;
            }
            set { _dsGeneralLedgerChild = value; }
        }

        
        //danh sách hoạt động theo công thức của bảng báo cáo lưu chuyển tiền tệ
        /// <summary>
        /// Danh sách tất cả tài khoản theo công thức lưu chuyển tiền tệ
        /// </summary>
        /// <param name="synthesisReport"></param>
        /// <returns></returns>
        private IEnumerable<GeneralLedgerChild> ValueGeneralLedgerChild(SynthesisReport synthesisReport)
        {
            List<GeneralLedgerChild> dsTraVe = new List<GeneralLedgerChild>();
            #region xử lý chuỗi công thức lấy ra kết quả
            List<string> keystart = LstFormulaOperandUsing.Where(c => c.TypeID == synthesisReport.TypeID && c.Description.ToLower().Contains("hoạt động")).Select(c => c.FormulaOperandUsingName + "[").ToList();
            string chuoiCongThuc = FormatExpression(synthesisReport.Formula1);
            if (!string.IsNullOrEmpty(chuoiCongThuc))
                for (int i = 0; i < keystart.Count(); i++)
                {
                    int viTriTimKiem = chuoiCongThuc.IndexOf(keystart[i], StringComparison.Ordinal);
                    while (viTriTimKiem != -1)
                    {
                        int viTriTimThay = chuoiCongThuc.IndexOf(keystart[i], viTriTimKiem, StringComparison.Ordinal);//vị trí đầu tiên
                        int viTriKetThucKey1 = viTriTimThay + keystart[i].Length;//vị trí chứa toàn bộ key
                        #region tìm thấy giá trị trùng key
                        int viTriCoDauCach = 0;
                        viTriCoDauCach = chuoiCongThuc.IndexOf(" ", viTriKetThucKey1, StringComparison.Ordinal);//vị trị có dấu cách, kết thúc key 1
                        if (viTriCoDauCach == 0) viTriCoDauCach = chuoiCongThuc.IndexOf(")", viTriKetThucKey1, StringComparison.Ordinal);//hoặc vị trị có ), kết thúc key 1
                        if (viTriKetThucKey1 >= 0 && viTriKetThucKey1 <= chuoiCongThuc.Length)
                        {
                            string chuoiLayRa = "";
                            if (viTriCoDauCach > 0)//nếu vị trí có dấu cách (có vị trí cuối cùng)
                                chuoiLayRa = chuoiCongThuc.Substring(viTriTimThay, viTriCoDauCach - viTriTimThay);
                            else if (viTriCoDauCach <= 0)//nếu không có vị trí cuối cùng
                                chuoiLayRa = chuoiCongThuc.Substring(viTriTimThay);
                            chuoiLayRa = chuoiLayRa.Replace(")", "");
                            if (chuoiLayRa.Length > 0)//chuỗi lấy ra khác chuỗi rỗng
                            {
                                #region cắt một chuỗi công thức thành các tham số đưa vào hàm
                                string accountNumber =
                                    chuoiLayRa.Substring(chuoiLayRa.IndexOf("[", StringComparison.Ordinal) + 1)
                                        .Replace("]", "");

                                List<string> listAccountNumber = accountNumber.Split(';').ToList();
                                if (listAccountNumber.Count > 1 && listAccountNumber.Count < 3)//nếu có một tham số
                                {
                                    string nameFun = keystart[i].Replace("[", "");
                                    dsTraVe.AddRange(UltilsSynthesisReport.Ds_PS_DU_HoatDong(listAccountNumber[0], listAccountNumber[1]));
                                }
                                #endregion
                            }
                        }
                        #endregion
                        //vị trí tiếp theo cần tìm
                        viTriTimKiem = chuoiCongThuc.IndexOf(keystart[i], viTriTimKiem + viTriKetThucKey1 - viTriTimThay, StringComparison.Ordinal);
                    }
                }
            #endregion
            return dsTraVe;
        }

        private IEnumerable<GeneralLedgerChild> CompareValueGeneralLedger(DateTime fromDate, DateTime toDate)
        {
            List<CashFlowStatementReport> dsCashFlowStatementReports = (IoC.Resolve<ICashFlowStatementReportService>()).GetAll();
            List<GeneralLedger> dsGeneralLedgers = IGeneralLedgerService.GetAll()
                                                   .Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).ToList();
            var ds = from b in dsGeneralLedgers
                     join a in dsCashFlowStatementReports on b.ID equals a.GeneralLedgerID
                     select new { a, b };
            List<GeneralLedgerChild> dsLayTheoCashFlowStatement = new List<GeneralLedgerChild>();
            foreach (var row in ds)
            {
                GeneralLedgerChild generalLedgerChild = new GeneralLedgerChild();
                PropertyInfo[] propertyInfo = generalLedgerChild.GetType().GetProperties();
                foreach (PropertyInfo info in propertyInfo)
                {
                    PropertyInfo propItem = row.b.GetType().GetProperty(info.Name);
                    if (propItem != null && propItem.CanWrite)
                    {
                        info.SetValue(generalLedgerChild, propItem.GetValue(row.b, null), null);
                    }
                }
                generalLedgerChild.GeneralLedgerID = row.a.GeneralLedgerID;
                generalLedgerChild.ID = row.a.ID;
                generalLedgerChild.CategoriesActive = row.a.CategoriesActive;
                dsLayTheoCashFlowStatement.Add(generalLedgerChild);
            }
            List<GeneralLedgerChild> dsLayTheoCongThucTrongGeneralLedgers = DsGeneralLedgerChild.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).ToList();

            List<GeneralLedgerChild> dsTraVe = new List<GeneralLedgerChild>();
            foreach (GeneralLedgerChild layTheoCashFlowStatement in dsLayTheoCashFlowStatement)
            {
                foreach (GeneralLedgerChild layTheoCongThucTrongGeneralLedger in dsLayTheoCongThucTrongGeneralLedgers)
                {
                    dsTraVe.Add(layTheoCashFlowStatement.GeneralLedgerID == layTheoCongThucTrongGeneralLedger.GeneralLedgerID
                        ? layTheoCashFlowStatement : layTheoCongThucTrongGeneralLedger);
                }
            }
            return dsTraVe;
        }

        #region thuật toán balan
        public static string FormatExpression(string expression)
        {
            expression = expression.Replace(" ", "");
            expression = Regex.Replace(expression, @"(\+|\-|\*|\/|\%){3,}", match => match.Value[0].ToString());
            expression = Regex.Replace(expression, @"(\+|\-|\*|\/|\%)(\+|\*|\/|\%)", match => match.Value[0].ToString());
            expression = Regex.Replace(expression, @"\+|\-|\*|\/|\%|\)|\(", match => String.Format(" {0} ", match.Value));
            expression = expression.Replace("  ", " ");
            expression = expression.Trim();
            return expression;
        }
        public static int GetPriority(string op)
        {
            if (op == "sqrt")
                return 3;
            if (op == "*" || op == "/" || op == "%")
                return 2;
            if (op == "+" || op == "-")
                return 1;
            return 0;
        }
        public static bool IsOperator(string str)
        {
            return Regex.Match(str, @"^(\+|\-|\*|\/|\%|sqrt)$").Success;
        }
        public static bool IsOperand(string str)
        {
            return Regex.Match(str, @"^\d+$|^([a-z]|[A-Z])$").Success;
        }
        public static string Infix2Postfix(string infix)
        {
            infix = FormatExpression(infix);
            string[] tokens = infix.Split(' ').ToArray();
            Stack<string> stack = new Stack<string>();
            StringBuilder postfix = new StringBuilder();
            for (int i = 0; i < tokens.Length; i++)
            {
                string token = tokens[i];
                if (IsOperator(token))
                {
                    if (i > 0 && IsOperator(tokens[i - 1]))
                    {
                        if (token == "-")
                        {
                            postfix.Append(token + tokens[i + 1]).Append(" ");
                            i++;
                        }
                        else if (token == "sqrt")
                        {
                            stack.Push(token);
                        }
                    }
                    else
                    {
                        while (stack.Count > 0 && GetPriority(token) <= GetPriority(stack.Peek()))
                            postfix.Append(stack.Pop()).Append(" ");
                        stack.Push(token);
                    }
                }
                else if (token == "(")
                    stack.Push(token);
                else if (token == ")")
                {
                    string x = stack.Pop();
                    while (x != "(")
                    {
                        postfix.Append(x).Append(" ");
                        x = stack.Pop();
                    }
                }
                else// (IsOperand(s))
                {
                    postfix.Append(token).Append(" ");
                }
            }
            while (stack.Count > 0)
                postfix.Append(stack.Pop()).Append(" ");
            return postfix.ToString();
        }
        public static decimal EvaluatePostfix(string postfix)
        {
            Stack<decimal> stack = new Stack<decimal>();
            postfix = postfix.Trim();
            IEnumerable<string> enumer = postfix.Split(' ');
            foreach (string s in enumer)
            {
                if (IsOperator(s))
                {
                    decimal x = stack.Pop();

                    decimal y = stack.Pop();
                    switch (s)
                    {
                        case "+": y += x; break;
                        case "-": y -= x; break;
                        case "*": y *= x; break;
                        case "/": y /= x; break;
                        case "%": y %= x; break;
                    }
                    stack.Push(y);
                }
                else  // IsOperand
                {
                    stack.Push(decimal.Parse(s));
                }
            }
            return stack.Pop();
        }
        #endregion
    }

}