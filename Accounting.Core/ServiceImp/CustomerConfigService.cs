using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CustomerConfigService : BaseService<CustomerConfig, String>, ICustomerConfigService
    {
        public CustomerConfigService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}