﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;


namespace Accounting.Core.ServiceImp
{
    public class EMDevidendService: BaseService<EMDevidend ,Guid>,IEMDevidendService
    {
        public EMDevidendService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public IEMDevidendService _IEMDevidendService { get { return IoC.Resolve<IEMDevidendService>(); } }
        public IEMShareHolderService _IEMShareHolderService { get { return IoC.Resolve<IEMShareHolderService>(); } }
        public IEMDevidendDetailService _IEMDevidendDetailService { get { return IoC.Resolve<IEMDevidendDetailService>(); } }
        public IShareHolderGroupService _IShareHolderGroupService { get { return IoC.Resolve<IShareHolderGroupService>(); } }
        #region DungNA Báo cáo cổ tức phải trả Ngày Xong 18/7/2014       
        /// <summary>
        /// [DungNA]
        /// Báo cáo cổ tức phải trả
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to"> Đến Ngày</param>
        /// <param name="EmshareHolderGroup">Nhóm Cổ đông</param>
        /// <param name="LockDate">Ngày Chốt</param>
        /// <returns>Danh sách cổ tức phải trả cho nhóm cổ đông</returns>
        public List<DividendPayable> RPDividends312(DateTime from, DateTime to, List<Guid> IDEmshareHolderGroup, DateTime LockDate)
        {
            List<DividendPayable> Output = new List<DividendPayable>();
            foreach (Guid x in IDEmshareHolderGroup)
            {
                List<DividendPayable> aaa = Rpd(from, to, x, LockDate);
                Output.AddRange(aaa);
            }
            return Output;
        }
        /// <summary>
        /// [DungNA]
        /// Báo cáo cổ tức phải trả
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="IDEmshareHolderGroup">Nhóm cổ đông</param>
        /// <param name="LockDate">Ngày chốt</param>
        /// <returns></returns>
        public List<DividendPayable> Rpd(DateTime froms, DateTime to, Guid IDEmshareHolderGroup, DateTime LockDate)
        {
            var kq = (from a in _IEMDevidendService.Query.ToList()
                      join b in _IEMDevidendDetailService.Query.ToList() on a.ID equals b.EMDevidendID
                      join c in _IEMShareHolderService.Query.ToList() on b.EMShareHolderID equals c.ID
                      join d in _IShareHolderGroupService.Query.ToList() on c.ShareHolderCategoryID equals d.ID
                      where a.LockDate != null 
                      && c.ShareHolderCategoryID != null                      
                      && a.LockDate == LockDate
                      && a.LockDate >= froms
                      && (IDEmshareHolderGroup == Guid.Empty || c.ShareHolderCategoryID == IDEmshareHolderGroup)
                      select new DividendPayable
                      {
                          LockDate = a.LockDate,//Ngày chốt
                          DevidendPerSharePercent = a.DevidendPerSharePercent,//Tỷ lệ cổ tức
                          ShareHolderGroupCode = d.ShareHolderGroupCode,//Mã nhóm cổ đông
                          ShareHolderGroupName = d.ShareHolderGroupName,//Tên nhóm cổ đông
                          Description = a.Description,//Đợt chia cổ tức
                          ShareHolderCode = c.ShareHolderCode,//Mã cổ đông
                          ShareHolderName = c.ShareHolderName,//Tên cổ đông
                          ContactIdentificationNo = c.ContactIdentificationNo,//Số CMT cổ đông
                          ContactIssueDate = c.ContactIssueDate,//Ngày cấp số CMT
                          HoldQuantiy = b.HoldQuantiy,//Số cổ phần nắm giữ
                          AmountPayable = b.AmountPayable,//Số tiền

                      }).ToList();
            return kq;

        }

        #endregion
    }

}