using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class VoucherDetailForeignCurrencyService : BaseService<VoucherDetailForeignCurrency, int>, IVoucherDetailForeignCurrencyService
    {
        public VoucherDetailForeignCurrencyService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }
}