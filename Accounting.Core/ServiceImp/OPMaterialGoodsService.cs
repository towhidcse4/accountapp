﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain.obj;
using FX.Core;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class OPMaterialGoodsService : BaseService<OPMaterialGoods, Guid>, IOPMaterialGoodsService
    {
        public OPMaterialGoodsService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        #region Mở Service
        private IMaterialGoodsService MaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }
        private IGeneralLedgerService _IGeneralLedgerService
        {
            get { return IoC.Resolve<IGeneralLedgerService>(); }
        }
        private IRepositoryLedgerService _IRepositoryLedgerService
        {
            get { return IoC.Resolve<IRepositoryLedgerService>(); }
        }
        private IRSInwardOutwardService _IRSInwardOutwardService
        {
            get { return IoC.Resolve<IRSInwardOutwardService>(); }
        }
        private IRepositoryService _IRepositoryService
        {
            get { return IoC.Resolve<IRepositoryService>(); }
        }
        #endregion
        public List<OPMaterialGoodsByOrther> GetListOPMaterialGoodsByOrther(string accountNumber)
        {
            List<Repository> lstRepositories = _IRepositoryService.GetAll();
            List<OPMaterialGoodsByOrther> lst = (from materialGoods in MaterialGoodsService.GetByIsActive(true)
                                                 join opMaterialGoodse in Query.Where(d => d.AccountNumber == accountNumber).ToList() on materialGoods.ID equals opMaterialGoodse.MaterialGoodsID into
                                                     groupJoin
                                                 from i in groupJoin.DefaultIfEmpty()
                                                 select new OPMaterialGoodsByOrther()
                                                 {
                                                     MaterialGoodsCode = materialGoods.MaterialGoodsCode,
                                                     MaterialGoodsName = materialGoods.MaterialGoodsName,
                                                     MaterialGoodsCategoryID = materialGoods.MaterialGoodsCategoryID ?? Guid.Empty,
                                                     RepositoryCode = i == null ? "" : lstRepositories.Single(c => c.ID == i.RepositoryID).RepositoryCode,
                                                     CurrencyID = i == null ? "VND" : i.CurrencyID ?? "VND",
                                                     ID = i == null ? Guid.Empty : i.ID,
                                                     PostedDate = i == null ? (DateTime.Now) : i.PostedDate,
                                                     AccountNumber = i == null ? "" : i.AccountNumber ?? "",
                                                     MaterialGoodsID = i == null ? materialGoods.ID : i.MaterialGoodsID,//123
                                                     RepositoryID = i == null ? Guid.Empty : i.RepositoryID,//123
                                                     ExchangeRate = i == null ? 1 : i.ExchangeRate ?? 1,
                                                     UnitPrice = i == null ? 0 : i.UnitPrice,//123
                                                     UnitPriceOriginal = i == null ? 0 : i.UnitPriceOriginal,//123
                                                     Quantity = i == null ? 0 : i.Quantity ?? 0,
                                                     Amount = i == null ? 0 : i.Amount,//123
                                                     AmountOriginal = i == null ? 0 : i.AmountOriginal,//123
                                                     ExpiryDate = i == null ? null : i.ExpiryDate,
                                                     LotNo = i == null ? "" : i.LotNo ?? "",
                                                     Unit = i == null ? "" : i.Unit ?? "",
                                                     UnitConvert = i == null ? "" : i.UnitConvert ?? "",
                                                     QuantityConvert = i == null ? 0 : i.QuantityConvert ?? 0,
                                                     UnitPriceConvert = i == null ? 0 : i.UnitPriceConvertOriginal,//123
                                                     UnitPriceConvertOriginal = i == null ? 0 : i.UnitPriceConvertOriginal,//123
                                                     CostSetID = i == null ? null : i.CostSetID ?? null,
                                                     ContractID = i == null ? null : i.ContractID ?? null,
                                                     BankAccountDetailID = i == null ? null : i.BankAccountDetailID ?? null,
                                                     ContractName = i == null ? "" : i.ContractName ?? "",
                                                     CostSetName = i == null ? "" : i.CostSetName ?? "",
                                                     BankName = i == null ? "" : i.BankName ?? "",
                                                     ExpenseItemID = i == null ? null : i.ExpenseItemID ?? null,
                                                     ConvertRate = i == null ? 0 : i.ConvertRate ?? 0,
                                                     OrderPriority = i == null ? 0 : i.OrderPriority,//123,
                                                     TypeID = 702
                                                 }).ToList().OrderBy(c => c.MaterialGoodsCode).ToList();
            return lst;
        }
        public List<OPMaterialGoodsByOrther> GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty()
        {
            List<Repository> lstRepositories = _IRepositoryService.GetAll();
            List<OPMaterialGoodsByOrther> lst = (from materialGoods in MaterialGoodsService.GetByIsActive(true)
                                                 join opMaterialGoodse in Query on materialGoods.ID equals opMaterialGoodse.MaterialGoodsID
                                                 select new OPMaterialGoodsByOrther()
                                                 {
                                                     MaterialGoodsCode = materialGoods.MaterialGoodsCode,
                                                     MaterialGoodsName = materialGoods.MaterialGoodsName,
                                                     RepositoryCode = lstRepositories.Single(c => c.ID == opMaterialGoodse.RepositoryID).RepositoryCode,
                                                     MaterialGoodsCategoryID = materialGoods.MaterialGoodsCategoryID ?? Guid.Empty,
                                                     CurrencyID = opMaterialGoodse.CurrencyID,
                                                     ID = opMaterialGoodse.ID,
                                                     PostedDate = opMaterialGoodse.PostedDate,
                                                     AccountNumber = opMaterialGoodse.AccountNumber,
                                                     MaterialGoodsID = opMaterialGoodse.MaterialGoodsID,//123
                                                     RepositoryID = opMaterialGoodse.RepositoryID,//123
                                                     ExchangeRate = opMaterialGoodse.ExchangeRate,
                                                     UnitPrice = opMaterialGoodse.UnitPrice,//123
                                                     UnitPriceOriginal = opMaterialGoodse.UnitPriceOriginal,//123
                                                     Quantity = opMaterialGoodse.Quantity,
                                                     Amount = opMaterialGoodse.Amount,//123
                                                     AmountOriginal = opMaterialGoodse.AmountOriginal,//123
                                                     ExpiryDate = opMaterialGoodse.ExpiryDate,
                                                     LotNo = opMaterialGoodse.LotNo,
                                                     Unit = opMaterialGoodse.Unit,
                                                     UnitConvert = opMaterialGoodse.UnitConvert,
                                                     QuantityConvert = opMaterialGoodse.QuantityConvert,
                                                     UnitPriceConvert = opMaterialGoodse.UnitPriceConvertOriginal,//123
                                                     UnitPriceConvertOriginal = opMaterialGoodse.UnitPriceConvertOriginal,//123
                                                     CostSetID = opMaterialGoodse.CostSetID,
                                                     ContractID = opMaterialGoodse.ContractID,
                                                     BankAccountDetailID = opMaterialGoodse.BankAccountDetailID ?? null,
                                                     ContractName = opMaterialGoodse.ContractName,
                                                     CostSetName = opMaterialGoodse.CostSetName,
                                                     BankName = opMaterialGoodse.BankName,
                                                     ExpenseItemID = opMaterialGoodse.ExpenseItemID,
                                                     ConvertRate = opMaterialGoodse.ConvertRate,
                                                     OrderPriority = opMaterialGoodse.OrderPriority//123
                                                 }).ToList();
            return lst;
        }

        public void SaveOpAccountAndGeneralLedger(IEnumerable<OPMaterialGoodsByOrther> lst)
        {
            List<OPMaterialGoods> lstOPMaterialGoods = new List<OPMaterialGoods>();
            Utils.Convert_FromParent_ToChild(lst, lstOPMaterialGoods);
            SaveLedgerOpMaterialGoods(lstOPMaterialGoods);
        }

        public void SaveLedgerOpMaterialGoods(IEnumerable<OPMaterialGoods> input)
        {
            //Lấy ra danh sách OPAccount
            List<OPMaterialGoods> listOpMaterialGoodses = new List<OPMaterialGoods>();
            Utils.Convert_FromParent_ToChild(GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty(), listOpMaterialGoodses);
            try
            {
                BeginTran();
                foreach (var opMaterialGoodsInput in input)
                {
                    if (listOpMaterialGoodses.Count(c => c.ID == opMaterialGoodsInput.ID && c.RepositoryID == opMaterialGoodsInput.RepositoryID && c.AccountNumber == opMaterialGoodsInput.AccountNumber) == 1)
                    {
                        //Kiểm tra xem TK có thuộc danh sách hay không
                        OPMaterialGoods opMaterialGoods =
                            listOpMaterialGoodses.Single(c => c.ID == opMaterialGoodsInput.ID);
                        //Có trong danh sách(Sửa)
                        if (opMaterialGoods.ID == opMaterialGoodsInput.ID)
                        {
                            //Trường hợp sửa
                            if ((opMaterialGoodsInput.Amount != 0) // Hautv Edit
                                && (opMaterialGoodsInput.Quantity != 0))
                            {
                                //Xóa chứng từ trong sổ cái và sổ vật tư
                                DeleteLedgerOpMaterialGoods(opMaterialGoodsInput);
                                //Update chứng từ trong bảng OPAccount
                                Update(Utils.GetAgainObject(opMaterialGoodsInput));
                                //Insert chứng từ vào sổ cái
                                GeneralLedger generalLedger = GenGeneralLedgers(opMaterialGoodsInput, opMaterialGoodsInput.PostedDate);
                                _IGeneralLedgerService.CreateNew(generalLedger);
                                RepositoryLedger repositoryLedger = GenRepositoryLedger(opMaterialGoodsInput, opMaterialGoodsInput.PostedDate);
                                _IRepositoryLedgerService.CreateNew(repositoryLedger);
                            }
                            else if ((opMaterialGoodsInput.Amount == 0) &&(opMaterialGoodsInput.Quantity == 0))
                            {
                                //Xóa chứng từ trong sổ cái và sổ vật tư
                                DeleteLedgerOpMaterialGoods(opMaterialGoodsInput);
                                //Update chứng từ trong bảng OPAccount
                                Delete(Utils.GetAgainObject(opMaterialGoodsInput));
                            }
                        }
                    }
                    else
                    {
                        //Thêm mới vào bảng OPAccount
                        if ((opMaterialGoodsInput.Amount != 0) && (opMaterialGoodsInput.Quantity != 0))
                        {
                            CreateNew(opMaterialGoodsInput);
                            //Insert chứng từ vào sổ cái
                            GeneralLedger generalLedger = GenGeneralLedgers(opMaterialGoodsInput, DateTime.Now);
                            _IGeneralLedgerService.CreateNew(generalLedger);
                            RepositoryLedger repositoryLedger = GenRepositoryLedger(opMaterialGoodsInput, DateTime.Now);
                            _IRepositoryLedgerService.CreateNew(repositoryLedger);
                        }
                    }
                }
                //foreach (OPMaterialGoods opMaterialGoodse in listOpMaterialGoodses)
                //{
                //    if (input.All(c => opMaterialGoodse.ID != c.ID && opMaterialGoodse.RepositoryID == c.RepositoryID && opMaterialGoodse.AccountNumber == c.AccountNumber))
                //    {
                //        //Trường hợp xóa
                //        //Xóa chứng từ trong sổ cái và sổ vật tư
                //        DeleteLedgerOpMaterialGoods(opMaterialGoodse);
                //        //Xóa chứng từ trong bảng OPAccount
                //        Delete(opMaterialGoodse.ID);
                //    }
                //}
                CommitTran();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLedgerOpMaterialGoods(OPMaterialGoods opMgInput)
        {
            _IRepositoryLedgerService.Delete(_IRepositoryLedgerService.Getbykey(opMgInput.ID));
            _IGeneralLedgerService.Delete(_IGeneralLedgerService.Getbykey(opMgInput.ID));
            if ((opMgInput.TypeID == 330) || (opMgInput.TypeID == 340))
            {
                RSInwardOutward iwVoucher = _IRSInwardOutwardService.Query.FirstOrDefault(p => p.ID == opMgInput.ID);
                if (iwVoucher != null)
                {
                    Guid? iwIdGuid = iwVoucher.RSInwardID;
                    if (iwIdGuid != null)
                    {
                        _IRepositoryLedgerService.Delete(_IRepositoryLedgerService.Getbykey((Guid)iwIdGuid));
                        _IGeneralLedgerService.Delete(_IGeneralLedgerService.Getbykey((Guid)iwIdGuid));
                    }
                }
            }
        }

        /// <summary>
        /// Tạo chứng từ sổ cái cho OpAccount
        /// </summary>
        /// <param name="opMaterialGoods">đối tượng số dư đầu kỳ tài khoản</param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        protected GeneralLedger GenGeneralLedgers(OPMaterialGoods opMaterialGoods, DateTime startDate)
        {
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = opMaterialGoods.ID,
                BranchID = null,
                ReferenceID = opMaterialGoods.ID,
                TypeID = 702,
                Date = opMaterialGoods.PostedDate,
                PostedDate = opMaterialGoods.PostedDate,
                No = "OPN",
                InvoiceDate = null,
                InvoiceNo = null,
                Account = opMaterialGoods.AccountNumber,
                AccountCorresponding = "",
                BankAccountDetailID = opMaterialGoods.BankAccountDetailID,
                CurrencyID = opMaterialGoods.CurrencyID,
                ExchangeRate = opMaterialGoods.ExchangeRate,
                DebitAmount = opMaterialGoods.Amount,
                DebitAmountOriginal = opMaterialGoods.Amount,
                CreditAmount = 0,
                CreditAmountOriginal = 0,
                Reason = "",
                Description = "",
                AccountingObjectID = null,
                EmployeeID = null,
                BudgetItemID = null,
                CostSetID = opMaterialGoods.CostSetID,
                ContractID = opMaterialGoods.ContractID,
                StatisticsCodeID = null,
                InvoiceSeries = null,
                ContactName = null,
                DetailID = null,
                RefNo = "OPN",
                RefDate = null,
                DepartmentID = null,
                ExpenseItemID = opMaterialGoods.ExpenseItemID,
                IsIrrationalCost = false,
            };

            return genTemp;
        }

        protected RepositoryLedger GenRepositoryLedger(OPMaterialGoods opMaterialGoods, DateTime startDate)
        {
            RepositoryLedger reposTemp = new RepositoryLedger
            {
                ID = opMaterialGoods.ID,
                BranchID = null,
                ReferenceID = opMaterialGoods.ID,
                TypeID = opMaterialGoods.TypeID,
                Date = opMaterialGoods.PostedDate,
                PostedDate = opMaterialGoods.PostedDate,
                No = "OPN",
                Account = opMaterialGoods.AccountNumber,
                AccountCorresponding = "",
                RepositoryID = opMaterialGoods.RepositoryID,
                MaterialGoodsID = opMaterialGoods.MaterialGoodsID,
                Unit = opMaterialGoods.Unit,
                UnitPrice = opMaterialGoods.UnitPrice,
                IWQuantity = opMaterialGoods.Quantity,
                OWQuantity = 0,
                IWAmount = opMaterialGoods.Amount,
                OWAmount = 0,
                IWQuantityBalance = opMaterialGoods.Quantity,
                IWAmountBalance = opMaterialGoods.Amount,
                Reason = null,
                Description = null,
                OWPurpose = null,
                ExpiryDate = opMaterialGoods.ExpiryDate,
                LotNo = opMaterialGoods.LotNo,
                CostSetID = opMaterialGoods.CostSetID,
                UnitConvert = opMaterialGoods.UnitConvert,
                UnitPriceConvert = opMaterialGoods.UnitPriceConvert,
                IWQuantityConvert = opMaterialGoods.QuantityConvert,
                OWQuantityConvert = opMaterialGoods.QuantityConvert,
                StatisticsCodeID = null,
                ConvertRate = opMaterialGoods.ConvertRate,
                DetailID = Guid.NewGuid(),
                RefDateTime = opMaterialGoods.PostedDate.Date.AddDays(+1).AddMilliseconds(-3)
            };

            return reposTemp;
        }

        /// <summary>
        /// Lấy ra danh sách vật tư hàng hóa có số dư đầu kỳ theo tài khoản
        /// </summary>
        /// <returns></returns>
        public List<OPMaterialGoodsByOrther> GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty_ByAccountNumber(string accountNumber)
        {
            return
                GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty().Where(c => c.AccountNumber == accountNumber).ToList();
        }
    }

}