using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class SAReturnDetailCustomerService: BaseService<SAReturnDetailCustomer ,Guid>,ISAReturnDetailCustomerService
    {
        public SAReturnDetailCustomerService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}