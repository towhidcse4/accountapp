using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class SAOrderService: BaseService<SAOrder ,Guid>,ISAOrderService
    {
        ISAOrderDetailService _ISAOrderDetailService { get { return IoC.Resolve<ISAOrderDetailService>(); } }
        ISAOrderService _ISAOrderService { get { return IoC.Resolve<ISAOrderService>(); } }
        public SAOrderService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public SAOrder GetSAOrderByID(Guid ID)
        {
            return Query.Where(x => x.ID == ID).SingleOrDefault();
        }

        public List<SAOrder> GetAllOrderByNo()
        {
            return Query.OrderByDescending(x => x.No).ToList();
        }

        public List<SAOrder> GetAllOrderByNoAndNotInContract(Guid ID)
        {
            List<SAOrderDetail> lstSAOrderDetail = _ISAOrderDetailService.GetAll().Where(x => x.ContractID == null).ToList();
            List<SAOrder> lstSAOrder = new List<SAOrder>();
            if (ID == Guid.Empty)  lstSAOrder = _ISAOrderService.GetAll();
            else  lstSAOrder = _ISAOrderService.GetAll().Where(x=>x.AccountingObjectID == ID).ToList();
            var query = from saorder in lstSAOrder
                        join saorderdetail in lstSAOrderDetail on saorder.ID equals saorderdetail.SAOrderID
                        select saorder;
                return query.GroupBy(x => x.ID).Select(x => x.First()).ToList().OrderByDescending(x=>x.Date).ToList();  
        }

        public List<SAOrder> GetOrderByContractID(Guid contractID)
        {
            List<SAOrderDetail> lstSAOrderDetail = _ISAOrderDetailService.GetAll().Where(x => x.ContractID == contractID).ToList();
            List<SAOrder> lstSAOrder = Query.ToList();
            var query = from saorder in lstSAOrder
                        join saorderdetail in lstSAOrderDetail on saorder.ID equals saorderdetail.SAOrderID
                        select saorder;
            return query.GroupBy(x => x.ID).Select(x => x.First()).ToList();
        }

        public List<SAOrder> GetOrderByContract(Guid contractID)
        {
            List<SAOrder> lstSAOrder1 = _ISAOrderService.GetAllOrderByNoAndNotInContract(Guid.Empty);
            List<SAOrder> lstSAOrder2 = _ISAOrderService.GetOrderByContractID(contractID);
            lstSAOrder1.AddRange(lstSAOrder2);
            return lstSAOrder1.OrderByDescending(x=>x.Date).ToList();
        }

        public SAOrder GetOrderByNo(string no)
        {
            return Query.Where(x => x.No == no).FirstOrDefault();
        }

        public Guid GetIDByNo(string no)
        {
            SAOrder saorder = Query.Where(x => x.No == no).FirstOrDefault();
            return saorder.ID;
        }


        public List<Guid> GetOrderIDByContractID(Guid contractID)
        {
            List<SAOrderDetail> lstSAOrderDetail = _ISAOrderDetailService.GetAll().Where(x => x.ContractID == contractID).ToList();
            List<SAOrder> lstSAOrder = Query.ToList();
            var query = from saorder in lstSAOrder
                        join saorderdetail in lstSAOrderDetail on saorder.ID equals saorderdetail.SAOrderID
                        select saorder;
            List<SAOrder> lst = query.GroupBy(x => x.ID).Select(x => x.First()).ToList();
            List<Guid> lstID = new List<Guid>();
            foreach (var item in lst)
            {
                lstID.Add(item.ID);
            }
            return lstID;
        }
    }

}