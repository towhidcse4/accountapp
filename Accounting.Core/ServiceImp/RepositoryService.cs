﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class RepositoryService : BaseService<Repository, Guid>, IRepositoryService
    {
        public RepositoryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// lấy ra danh sách mã kho 
        /// [longtx]
        /// </summary>
        /// <returns></returns>
        public System.Collections.Generic.List<string> GetRepositoryCode()
        {
            return Query.Select(p => p.RepositoryCode).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách Repository theo IsActive
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>Tập các Repository, null nếu bị lỗi</returns>
        public List<Repository> GetAll_ByIsActive(bool IsActive)
        {
            return Query.Where(p => p.IsActive == IsActive).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách Repository theo IsActive và sắp xếp
        /// [haipt]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>Tập các Repository, null nếu bị lỗi</returns>
        public List<Repository> GetAll_ByOrderbyIsActive(bool IsActive)
        {
            return GetAll_ByIsActive(true).OrderBy(p => p.RepositoryName).ToList();
        }

        public List<Repository> GetOrderby()
        {
            return Query.OrderBy(p => p.RepositoryName).ToList();
        }
        /// <summary>
        /// Lấy ra danh sách RepositoryCode từ Repository theo IsActive == true
        /// [Longtx]
        /// </summary>
        /// <returns>Tập các Repository có isActive==true</returns>
        public List<Repository> GetByRepositoryCode(bool isActive)
        {
            return Query.OrderBy(p => p.RepositoryCode).Where(p => p.IsActive == isActive).ToList();
        }

        /// <summary>
        /// Lấy ra Repository theo ID
        /// [Haipt]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Repository> GetRepositoryById(Guid? id)
        {
            return Query.Where(p => p.ID == id).ToList();
        }
        public List<Repository> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.RepositoryCode).ToList();
        }
        public string GetRespositoryCodeByGuid(Guid? guid)
        {
            return Query.Where(p => p.ID == guid).FirstOrDefault().RepositoryCode;
        }
        public Guid? GetGuidRespositoryByCode(string code)
        {
            Guid? id = null;
            Repository rs = Query.Where(p => p.RepositoryCode == code).SingleOrDefault();
            if (rs != null)
            {
                id = rs.ID;
            }
            return id;
        }
    }
}