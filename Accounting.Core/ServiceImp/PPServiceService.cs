﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Core;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class PPServiceService: BaseService<PPService ,Guid>,IPPServiceService
    {
        ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public PPServiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public PPService GetPPServicebyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        public List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null)
        {
            List<PPService> lst = new List<PPService>();
            if(currencyID == null) lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                     && x.Recorded ).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            else lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                    && x.Recorded && x.CurrencyID == currencyID).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<TIOriginalVoucher> result = new List<TIOriginalVoucher>();
            foreach (PPService io in lst)
            {
                TIOriginalVoucher ov = new TIOriginalVoucher()
                {
                    Amount = io.TotalAmountOriginal,
                    PostedDate = io.PostedDate,
                    Date = io.Date,
                    No = io.No,
                    Reason = io.Reason,
                    ID = io.ID

                };
                result.Add(ov);
            }
            return result;
        }

        public List<TIOriginalVoucher> FindByListID(string originalVoucher)
        {
            string[] ss = originalVoucher.Substring(0, originalVoucher.Length - 1).Split(';');
            List<Guid> guids = new List<Guid>();
            foreach (string s in ss)
            {
                guids.Add(Guid.Parse(s));
            }
            return Query.Where(haha => guids.Contains(haha.ID)).Select(hihi => new TIOriginalVoucher
            {
                PostedDate = hihi.PostedDate,
                Date = hihi.Date,
                No = hihi.No,
                Reason = hihi.Reason,
                Amount = hihi.TotalAmountOriginal,
                ID = hihi.ID
            }).ToList();
        }

        public List<PPService> GetByBillReceived(bool billReceived, Guid? accountingObjectID, bool isRecorded, DateTime dtBegin, DateTime dtEnd)
        {
            var qr = accountingObjectID.HasValue ? Query.Where(i => i.AccountingObjectID == accountingObjectID.Value) : Query;
            qr = qr.Where(i => i.BillReceived == billReceived && i.Recorded == isRecorded && i.PostedDate >= dtBegin && i.PostedDate <= dtEnd);
            return qr.ToList();
        }
    }

}