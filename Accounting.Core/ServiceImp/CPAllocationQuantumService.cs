using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class CPAllocationQuantumService: BaseService<CPAllocationQuantum ,Guid>,ICPAllocationQuantumService
    {
        public CPAllocationQuantumService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}