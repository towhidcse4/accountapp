using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CPUncompleteService: BaseService<CPUncomplete ,Guid>,ICPUncompleteService
    {
        public CPUncompleteService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<CPUncomplete> GetList()
        {
            return Query.ToList();
        }
    }

}