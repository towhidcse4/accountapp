﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CurrencyService : BaseService<Currency, String>, ICurrencyService
    {
        public CurrencyService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<Currency> GetListCurrencyID(string Id)
        {
            List<Currency> lstCurrency = Query.Where(c => c.ID.Equals(Id)).ToList();
            return lstCurrency;
        }
        public List<Currency> GetIsActive(bool isActive)
        {
            return Query.Where(c => c.IsActive == isActive).OrderBy(c => c.ID).ToList();
        }

        /// <summary>
        /// Lấy danh sách CurrencyID đang hoạt động và so sánh kí tự viết hoa và kí tự thường
        /// [Longtx] 
        /// </summary>
        /// <param name="currencyLower"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public List<Currency> GetByListIdCurrenctLower(string currencyLower, bool isActive)
        {
            return Query.Where(p => p.ID.ToLower() == currencyLower && p.IsActive == isActive).ToList();
        }

        public int CountListCurrencyID(string Id)
        {
            return GetListCurrencyID(Id).Count;
        }

        public List<Currency> OrderByID()
        {
            return Query.OrderBy(p => p.ID).ToList();
        }
    }

}