using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class PaymentClauseService : BaseService<PaymentClause, Guid>, IPaymentClauseService
    {
        public PaymentClauseService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<PaymentClause> GetIsActive(bool IsActive)
        {
            return Query.OrderBy(p => p.PaymentClauseCode).Where(p => p.IsActive == IsActive).ToList();
        }
        public List<PaymentClause> OrderByCode()
        {
            return Query.OrderBy(c => c.PaymentClauseCode).ToList();
        }
        public List<string> GetCode()
        {
            return Query.Select(c => c.PaymentClauseCode).ToList();
        }

        public PaymentClause GetByID(Guid id)
        {
            return (PaymentClause)Query.Where(c => c.ID == id && c.IsActive == true);
        }

        public Guid GetGuidPaymentClauseByCode(string code)
        {
            Guid id = Guid.Empty;
            PaymentClause ag = Query.Where(p => p.PaymentClauseCode == code).SingleOrDefault();
            if (ag != null)
            {
                id = ag.ID;
            }
            return id;
        }
    }

}