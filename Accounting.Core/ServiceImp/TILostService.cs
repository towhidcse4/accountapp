﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TILostService : BaseService<TILost, Guid>, ITILostService
    {
        ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public TILostService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListTILostCode()
        {
            List<string> list = Query.Select(p => p.No).ToList();
            return list;
        }
    }
}
