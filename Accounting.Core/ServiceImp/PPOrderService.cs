using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class PPOrderService: BaseService<PPOrder ,Guid>,IPPOrderService
    {
        IPPOrderDetailService _IPPOrderDetailService { get { return IoC.Resolve<IPPOrderDetailService>(); } }
        IPPOrderService _IPPOrderService { get { return IoC.Resolve<IPPOrderService>(); } }
        public PPOrderService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public PPOrder GetPPOrderbyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        public PPOrder GetPPOrderByID(Guid id)
        {
            return Query.SingleOrDefault(x => x.ID == id);
        }

        public List<PPOrder> GetListOrderByNo()
        {
            return Query.OrderByDescending(k => k.No).ToList();
        }

        public List<PPOrder> GetAllOrderByNoAndNotInContract(Guid ID)
        {
            List<PPOrderDetail> lstPPOrderDetail = _IPPOrderDetailService.GetAll().Where(x => x.ContractID == null).ToList();
            List<PPOrder> lstPPOrder = new List<PPOrder>();
            if (ID == Guid.Empty) lstPPOrder = _IPPOrderService.GetAll();
            else lstPPOrder = _IPPOrderService.GetAll().Where(x => x.AccountingObjectID == ID).ToList();
            var query = from pporder in lstPPOrder
                        join pporderdetail in lstPPOrderDetail on pporder.ID equals pporderdetail.PPOrderID
                        select pporder;
            return query.GroupBy(x => x.ID).Select(x => x.First()).ToList().OrderByDescending(x=>x.Date).ToList();
        }

        public List<PPOrder> GetOrderByContractID(Guid contractID)
        {
            List<PPOrderDetail> lstPPOrderDetail = _IPPOrderDetailService.GetAll().Where(x => x.ContractID == contractID).ToList();
            List<PPOrder> lstPPOrder = _IPPOrderService.GetAll();
            var query = from pporder in lstPPOrder
                        join pporderdetail in lstPPOrderDetail on pporder.ID equals pporderdetail.PPOrderID
                        select pporder;
            return query.GroupBy(x => x.ID).Select(x => x.First()).ToList();
        }

        public List<PPOrder> GetOrderByContract(Guid contractID)
        {
            List<PPOrder> lstSAOrder1 = _IPPOrderService.GetAllOrderByNoAndNotInContract(Guid.Empty);
            List<PPOrder> lstSAOrder2 = _IPPOrderService.GetOrderByContractID(contractID);
            lstSAOrder1.AddRange(lstSAOrder2);
            return lstSAOrder1.OrderByDescending(x=>x.Date).ToList();
        }

        public PPOrder GetOrderByNo(string no)
        {
            return Query.Where(x => x.No == no).FirstOrDefault();
        }

        public Guid GetIDByNo(string no)
        {
            PPOrder pporder = Query.Where(x => x.No == no).FirstOrDefault();
            return pporder.ID;
        }

        public List<Guid> GetOrderIDByContractID(Guid contractID)
        {
            List<PPOrderDetail> lstPPOrderDetail = _IPPOrderDetailService.GetAll().Where(x => x.ContractID == contractID).ToList();
            List<PPOrder> lstPPOrder = Query.ToList();
            var query = from pporder in lstPPOrder
                        join pporderdetail in lstPPOrderDetail on pporder.ID equals pporderdetail.PPOrderID
                        select pporder;
            List<PPOrder> lst = query.GroupBy(x => x.ID).Select(x => x.First()).ToList();
            List<Guid> lstID = new List<Guid>();
            foreach (var item in lst)
            {
                lstID.Add(item.ID);
            }
            return lstID;
        }
    }

}