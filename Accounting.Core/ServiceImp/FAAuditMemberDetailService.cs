using System;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FAAuditMemberDetailService: BaseService<FAAuditMemberDetail ,Guid>, IFAAuditMemberDetailService
    {
        public FAAuditMemberDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}