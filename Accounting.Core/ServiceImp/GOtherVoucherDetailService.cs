﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class GOtherVoucherDetailService : BaseService<GOtherVoucherDetail, Guid>, IGOtherVoucherDetailService
    {
        public GOtherVoucherDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<GOtherVoucherDetail> GetByGOtherVoucherID(Guid ID)
        {
            return Query.Where(p => p.GOtherVoucherID == ID).ToList();
        }
    }

}