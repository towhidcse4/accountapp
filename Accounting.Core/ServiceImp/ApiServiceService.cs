using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class ApiServiceService: BaseService<ApiService ,Guid>,IApiServiceService
    {
        public ApiServiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}