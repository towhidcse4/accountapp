﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class ContractStateService : BaseService<ContractState, int>, IContractStateService
    {
        public ContractStateService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath) { }

        public ContractState getContractStatebyName(string _ContractStateName)
        {
            return Query.SingleOrDefault(k => k.ContractStateName.Equals(_ContractStateName));
        }

        public List<ContractState> GetListContractStateOrder()
        {
            List<ContractState> list = GetAll().OrderByDescending(c => c.ContractStateCode).Reverse().ToList();
            return list;
        }

        public List<string> GetListContractStateCode()
        {
            List<string> lst = Query.Select(c => c.ContractStateCode).ToList();
            return lst;
        }
    }
}
