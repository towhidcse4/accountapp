﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TIDecrementDetailService :BaseService<TIDecrementDetail,Guid>,ITIDecrementDetailService
    {
        public TIDecrementDetailService(string sessionFactoryConfigPath):base(sessionFactoryConfigPath)
        { }

        public ITIDecrementService _ITIDecrementService { get { return IoC.Resolve<ITIDecrementService>(); } }

        public List<TIDecrementDetail> GetByTIDecrementDetailID(Guid TIDecrementID)
        {
            return Query.Where(k => k.TIDecrementID ==TIDecrementID).ToList();
        }

        public decimal GetNotDecrementQuantity(Guid iD, decimal quantity, DateTime PostedDate)
        {
            return quantity - (Query.Where(a => a.ToolsID == iD 
                                    && _ITIDecrementService.Query.Any(b => b.Recorded && b.ID == a.TIDecrementID && b.PostedDate <= PostedDate)).ToList().Sum(a => a.DecrementQuantity) ?? 0);
        }

        public decimal GetRemainingAmount(Guid? materialGoodsID, DateTime postedDate)
        {
            var d = Query.Where(a => a.ToolsID == materialGoodsID && _ITIDecrementService.Query.Any(b => b.Recorded && b.ID == a.TIDecrementID && b.PostedDate.Month == postedDate.Month && b.PostedDate.Year == postedDate.Year)).FirstOrDefault();
            return d != null ? d.RemainingAmount : 0;
        }
    }
}
