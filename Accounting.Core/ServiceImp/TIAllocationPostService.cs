using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class TIAllocationPostService: BaseService<TIAllocationPost ,Guid>,ITIAllocationPostService
    {
        public TIAllocationPostService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}