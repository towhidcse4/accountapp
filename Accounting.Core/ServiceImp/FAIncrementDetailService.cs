using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using NHibernate;

namespace Accounting.Core.ServiceImp
{
    public class FAIncrementDetailService : BaseService<FAIncrementDetail, Guid>, IFAIncrementDetailService
    {
        public FAIncrementDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public ISession GetSession()
        {
            return NHibernateSession;
        }
        public FAIncrementDetail FindByFixedAssetID(Guid iD)
        {
            return Query.Where(p => p.FixedAssetID == iD).FirstOrDefault();
        }
    }

}