﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FADepreciatioAllocationService : BaseService<FADepreciationAllocation, Guid>, IFADepreciatioAllocationService
    {
        public FADepreciatioAllocationService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}