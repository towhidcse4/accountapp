﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;


namespace Accounting.Core.ServiceImp
{
    public class FixedAssetLedgerService : BaseService<FixedAssetLedger, Guid>, IFixedAssetLedgerService
    {
        public FixedAssetLedgerService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {

        }
        public IFixedAssetCategoryService _IFixedAssetCategoryService { get { return IoC.Resolve<IFixedAssetCategoryService>(); } }
        public IFixedAssetService _IFixedAssetService { get { return IoC.Resolve<IFixedAssetService>(); } }
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public IFAInitService _IFAInitService { get { return IoC.Resolve<IFAInitService>(); } }
        public IFADecrementDetailService _IFADecrementDetailService { get { return IoC.Resolve<IFADecrementDetailService>(); } }
        public IFAIncrementDetailService _IFAIncrementDetailService { get { return IoC.Resolve<IFAIncrementDetailService>(); } }
        public IFADecrementService _IFADecrementService { get { return IoC.Resolve<IFADecrementService>(); } }
        public IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }
        public IFixedAssetAccessoriesService _IFixedAssetAccessoriesService { get { return IoC.Resolve<IFixedAssetAccessoriesService>(); } } //Bảng phụ tùng kèm theo
        public List<FixedAssetLedger> GetByReferenceID(Guid Id)
        {
            return Query.Where(p => p.ReferenceID == Id).ToList();
        }
        #region Thẻ tài giản cố định S12SDNN DungNA
        /// <summary>
        /// [Dungna]
        /// Thẻ tài sản cố định
        /// </summary>
        /// <param name="PostedDate">Dữ liệu đầu vào là Ngày tự chọn để lọc cột PostedDate</param>
        /// <param name="ListIDFA"> Danh sách list trong bảng tài sản cố định</param>
        /// <returns>Danh sách các đối tượng tài sản cố định và list các con của nó</returns>
        public S12DNN ReportS12DNN(DateTime PostedDate2, List<Guid> ListIDFA)
        {
            try
            {
                DateTime postedDate = new DateTime(PostedDate2.Year, PostedDate2.Month, PostedDate2.Day, 23, 59, 59);
                S12DNN outData = new S12DNN();
                outData.Infos = new List<RFAInfo>();
                outData.Vouchers = new List<RFAVoucher>();
                outData.Attacments = new List<RFAAttackment>();
                foreach (Guid fa in ListIDFA)
                {
                    if (_IFixedAssetService.Query.FirstOrDefault(x => x.ID == fa).No == null && !_IFAIncrementDetailService.Query.Any(x => x.FixedAssetID == fa)) continue;
                    RFAInfo xxx = (from a in _IFixedAssetService.Query
                                   where a.FixedAssetName != null && a.FixedAssetCode != null && a.ID == fa
                                   select new RFAInfo
                                   {
                                       FixedAssetCode = a.FixedAssetCode,
                                       UsedDate = Convert.ToDateTime(a.UsedDate),
                                       DeliveryRecordNo = a.DeliveryRecordNo ?? "",
                                       DeliveryRecordDate = Convert.ToDateTime(a.DeliveryRecordDate),
                                       FixedAssetName = a.FixedAssetName,
                                       SerialNumber = a.SerialNumber ?? "",
                                       DepartmentID = a.DepartmentID ?? Guid.Empty,
                                       ProductionYear = a.ProductionYear != null && a.ProductionYear != 0 ? a.ProductionYear.ToString() : "",//Năm sản xuất                                  
                                       MadeIn = a.MadeIn ?? "",
                                       UseYear = Convert.ToDateTime(a.UsedDate) == DateTime.MinValue ? "" : Convert.ToDateTime(a.UsedDate).Year.ToString(),
                                   }).FirstOrDefault();
                    if (xxx.UseYear == "")
                    {
                        var year = (from a in _IFAInitService.Query
                                    where a.FixedAssetID == fa
                                    select new { year = Convert.ToDateTime(a.DepreciationDate).Year }).FirstOrDefault();
                        xxx.UseYear = year.year.ToString();
                    }

                    var decrement = (from a in Query
                                     join b in _IFADecrementService.Query on a.No equals b.No
                                     join c in _IFADecrementDetailService.Query on b.ID equals c.FADecrementID
                                     where c.FixedAssetID == fa && a.PostedDate < postedDate
                                     select new
                                     {
                                         b.PostedDate,
                                         b.Reason,
                                         a.No
                                     }).FirstOrDefault();
                    if (decrement != null)
                    {
                        xxx.DateDecrement = decrement.PostedDate;
                        xxx.NoDecrement = decrement.No;
                        xxx.DescriptionDecrement = decrement.Reason;
                    }

                    List<RFAVoucher> yyy = (from a in Query.ToList().OrderBy(a => a.PostedDate)
                                            join b in _IFixedAssetService.Query on a.FixedAssetID equals b.ID
                                            where a.PostedDate != null && b.FixedAssetName != null && b.FixedAssetCode != null && a.PostedDate < postedDate && b.ID == fa
                                                    && new int[] { 701, 119, 129, 132, 142, 172, 500, 510, 530, 540 }.Contains(a.TypeID)
                                            select new RFAVoucher
                                            {
                                                FixedAssetCode = b.FixedAssetCode,
                                                RefNumber = a.No,//Số hiệu chứng từ
                                                Hyperlink =  a.ReferenceID + ";" + a.TypeID,
                                                RefDate = a.PostedDate, // Ngày
                                                JournalMemo = a.Reason,// Diễn giải
                                                YearDepreciation = a.TypeID == 540 ? a.PostedDate.Year : (int?)null, // Trả ra năm giá trị hao mòn tương đương với năm postedDate
                                                OrgPrice = a.TypeID != 540 ? a.OriginalPrice : null,
                                                DepreciationValue = a.TypeID == 701 || a.TypeID == 530 ? a.AcDepreciationAmount : (a.TypeID == 540 ? a.MonthPeriodDepreciationAmount : (decimal?)null),
                                                TotalDepreciationValue = a.TypeID == 530 ? a.AcDepreciationAmount : (decimal?)null,
                                            }).ToList();

                    if (yyy.Count > 0)
                    {
                        xxx.No = yyy[0].RefNumber;
                        xxx.PostedDate = yyy[0].RefDate;
                        for (int i = 1; i < yyy.Count; i++)
                        {
                            if (yyy[i].RefNumber == yyy[i - 1].RefNumber)
                            {
                                yyy.Remove(yyy[i]);
                                i--;
                            }
                        }
                    }
                    outData.Vouchers.AddRange(yyy);
                    outData.Infos.Add(xxx);
                    List<RFAAttackment> zzz = (from a in _IFixedAssetAccessoriesService.Query
                                               join b in _IFixedAssetService.Query on a.FixedAssetID equals b.ID
                                               where b.FixedAssetName != null && b.FixedAssetCode != null && b.ID == fa
                                               select new RFAAttackment
                                               {
                                                   FixedAssetCode = b.FixedAssetCode,
                                                   FixedAssetAccessoriesName = a.FixedAssetAccessoriesName,
                                                   FixedAssetAccessoriesQuantity = a.FixedAssetAccessoriesQuantity,
                                                   FixedAssetAccessoriesUnit = a.FixedAssetAccessoriesUnit,
                                                   FixedAssetAccessoriesAmount = a.FixedAssetAccessoriesAmount,
                                               }).ToList();
                    outData.Attacments.AddRange(zzz);
                }
                return outData;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        #region Sổ tài sản sản cố định S10DNN DungNA
        /// <summary>
        /// [DungNA]
        /// Sổ tài sản cố định
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="ListFixedAssetCategoryID">Danh sách Loại tài sản cố định</param>
        /// <returns></returns>
        public List<S10DNN> RS10DNN(DateTime froms, DateTime to, List<Guid> ListFixedAssetID)
        {
            List<S10DNN> output = new List<S10DNN>();
            foreach (Guid x in ListFixedAssetID)
            {
                List<S10DNN> list = GetRS10DNN(froms, to, x);
                output.AddRange(list);
            }
            return output.OrderByDescending(a => a.OrderDate).ToList();

        }
        /// <summary>
        /// [DungNA]
        /// Lấy ra đối tượng tài sản cố định
        /// </summary>
        /// <param name="froms">từ ngày</param>
        /// <param name="to">đến ngày</param>
        /// <param name="FixedAssetCategoryID">Loại tài sản cố định</param>
        /// <returns></returns>
        public List<S10DNN> GetRS10DNN(DateTime froms, DateTime tos, Guid FixedAssetID)
        {
            try
            {
                DateTime from2 = new DateTime(froms.Year, froms.Month, froms.Day);
                DateTime to = new DateTime(tos.Year, tos.Month, tos.Day);
                var fa = _IFixedAssetService.Getbykey(FixedAssetID);
                var fac = _IFixedAssetCategoryService.Getbykey(fa.FixedAssetCategoryID);

                var opn = (from a in Query
                           join b in _IFixedAssetService.Query on a.FixedAssetID equals b.ID
                           where a.PostedDate < from2 && a.FixedAssetID == FixedAssetID
                            && a.TypeID != 550
                           orderby a.PostedDate descending
                           select new S10DNN
                           {
                               IncRefNo = a.Nos,
                               Hyperlink = a.ReferenceID + ";" + a.TypeID,
                               IncRefDate = a.IncrementDate ?? DateTime.Now,
                               MadeIn = a.TypeID == 701 || a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 530 || a.TypeID == 540 ? fa.MadeIn : "",
                               UsedDate = b.UsedDate,
                               SerialNumber = a.TypeID == 701 || a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 530 || a.TypeID == 540 ? fa.SerialNumber : "",
                               Price = a.TypeID == 530 ? (a.DifferOrgPrice ?? 0) : a.OriginalPrice,
                               DepreciationRate = a.TypeID == 520 ? null : a.TypeID == 701 || a.TypeID == 530 || a.TypeID == 540 || a.TypeID == 500 || a.TypeID == 510 ? (Math.Round(a.MonthDepreciationRate ?? 0, 2) / 100) : (decimal?)null,
                               DepreciationAmount = 0,
                               DepreciationCalc = a.AcDepreciationAmount,
                               DiffDepreciation = null,
                               OrderDate = a.PostedDate,
                               OrderType = 2
                           }).FirstOrDefault();
                if (opn != null && opn.Hyperlink.Contains("520")) opn = null;
                var R23 = (from a in Query
                           join b in _IFixedAssetService.Query on a.FixedAssetID equals b.ID
                           where a.PostedDate >= from2 && a.PostedDate <= to && a.FixedAssetID == FixedAssetID
                            && a.TypeID != 550
                           orderby a.PostedDate
                           select new S10DNN
                           {
                               IncRefNo = /*(a.TypeID == 701 || a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 530 || a.TypeID == 540) ? */a.TypeID != 520 ? a.No : "",
                               Hyperlink = a.ReferenceID + ";" + a.TypeID,
                               IncRefDate = /*a.TypeID == 701 || a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 530 || a.TypeID == 540 ?*/a.TypeID != 520 ? a.PostedDate : DateTime.MinValue,
                               MadeIn = /*a.TypeID == 701 || a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 530 || a.TypeID == 540 ? */a.TypeID != 520 ? fa.MadeIn : "",
                               UsedDate = b.UsedDate,
                               SerialNumber = /*a.TypeID == 701 || a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 530 || a.TypeID == 540 ?*/a.TypeID != 520 ? fa.SerialNumber : "",
                               Price = a.TypeID == 530 ? (a.DifferOrgPrice ?? 0) : a.TypeID != 520 && a.TypeID != 540 ? a.OriginalPrice : null,
                               DepreciationRate = a.TypeID == 520 ? null : a.TypeID == 701 || a.TypeID == 530 || a.TypeID == 540 || a.TypeID == 500 || a.TypeID == 510 ? (Math.Round(a.MonthDepreciationRate ?? 0, 2) / 100) : (decimal?)null,
                               DepreciationAmount = a.TypeID == 540 ? a.MonthPeriodDepreciationAmount : (a.TypeID == 530 || a.TypeID == 701 ? a.AcDepreciationAmount : null),
                               DepreciationCalc = a.TypeID == 701 || a.TypeID == 530 ? (a.AcDepreciationAmount ?? 0) : (a.TypeID == 540 ? a.MonthPeriodDepreciationAmount : (decimal?)null),
                               DiffDepreciation = a.TypeID == 530 ? a.DifferAcDepreciationAmount : null,
                               DesRefNo = a.TypeID == 520 ? a.No : "",
                               DesRefDate = a.TypeID == 520 ? a.PostedDate : DateTime.MinValue,
                               Reason = a.TypeID == 520 ? a.Reason : "",
                               OrderDate = a.PostedDate,
                               OrderType = a.TypeID == 701 || a.TypeID == 530 ? 1 : 0
                           }).ToList();
                if (opn != null)
                    R23.Insert(0, opn);
                for (int i = 0; i < R23.Count; i++)
                {
                    if (i + 1 != R23.Count && R23[i].IncRefNo == R23[i + 1].IncRefNo)
                    {
                        R23.Remove(R23[i]);
                        i--;
                    }
                    else
                    {
                        R23[i].FACategoryCode = fac.FixedAssetCategoryCode;
                        R23[i].FACategoryName = fac.FixedAssetCategoryName;
                        R23[i].FACode = fa.FixedAssetCode;
                        R23[i].FAName = fa.FixedAssetName;
                    }
                }
                return R23;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Sổ theo dõi tài sản cố định S11DNN [DungNA]
        /// <summary>
        /// [DungNA]
        /// Sổ theo dõi tài sản cố định
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to"> Đến Ngày </param>
        /// <param name="ListDepartmentID">List dánh sách phòng ban</param>
        /// <param name="CheckPrints">In theo số lượng khai báo trong tài sản cố định</param>
        /// <returns></returns>
        public List<S11DNN> RS11DNN(DateTime from, DateTime to, List<Guid> ListDepartmentID, bool CheckPrints)
        {
            List<S11DNN> output = new List<S11DNN>();
            foreach (Guid x in ListDepartmentID)
            {
                List<S11DNN> list = GetS11DNN(from, to, x, CheckPrints);
                if(list != null)
                    output.AddRange(list);
            }
            return output;
        }
        /// <summary>
        /// [DungNA]
        /// Lấy ra đối tượng theo dõi tài sản cố định
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to"> Đến Ngày </param>
        /// <param name="DepartmentID"> ID phòng ban</param>
        /// <param name="CheckPrints">In theo số lượng khai báo trong tài sản cố định</param>
        /// <returns></returns>
        public List<S11DNN> GetS11DNN(DateTime froms, DateTime tos, Guid DepartmentID, bool CheckPrints)
        {
            try
            {
                DateTime from2 = new DateTime(froms.Year, froms.Month, froms.Day);
                DateTime to = new DateTime(tos.Year, tos.Month, tos.Day);
                Department department = _IDepartmentService.Getbykey(DepartmentID);
                var DN22 = (from a in Query
                            where /*a.PostedDate >= from2 &&*/ a.PostedDate <= to && a.DepartmentID == DepartmentID
                            && (a.TypeID == 500 || a.TypeID == 510 || a.TypeID == 701 || a.TypeID == 119
                                || a.TypeID == 129 || a.TypeID == 132 || a.TypeID == 142 || a.TypeID == 172 || a.TypeID == 550)
                            select new S11DNN
                            {
                                No = a.No,
                                Hyperlink =  a.ReferenceID + ";" + a.TypeID,
                                PostedDate = a.PostedDate,
                                Quantity = 1,
                                OriginalPriceDebitAmount = (decimal)a.OriginalPrice,
                                FAID = a.FixedAssetID
                            }).ToList();

                //var dc = (from a in Query
                //          where a.PostedDate >= from2 && a.PostedDate <= to && a.DepartmentID == DepartmentID
                //          && (a.TypeID == 550)
                //          select new S11DNN
                //          {
                //              No = a.No,
                //              Hyperlink = a.TypeID + ";" + a.ReferenceID,
                //              PostedDate = a.PostedDate,
                //              Quantity = 1,
                //              OriginalPriceDebitAmount = (decimal)a.OriginalPrice,
                //              FAID = a.FixedAssetID
                //          }).ToList();
                //DN22.AddRange(dc);
                foreach (var x in DN22)
                {
                    x.FAName = _IFixedAssetService.Getbykey(x.FAID ?? Guid.Empty).FixedAssetName;
                    x.Moneys = x.Quantity * x.OriginalPriceDebitAmount;
                    x.DepartmentCode = department.DepartmentCode;
                    x.DepartmentName = department.DepartmentName;
                    var DN222 = (from a in Query
                                 where a.PostedDate >= from2 && a.PostedDate <= to && a.DepartmentID == DepartmentID
                                 && a.TypeID == 520 && a.FixedAssetID == x.FAID
                                 select new S11DNN
                                 {
                                     Hyperlink =   a.ReferenceID + ";" + a.TypeID,
                                     DesRefNo = a.No,
                                     DesPostedDate = a.PostedDate,
                                     Reason = a.Reason,
                                     DesQuantity = "1",
                                     DesMoneys = a.OriginalPrice,
                                 }).FirstOrDefault();
                    if (DN222 != null)
                    {
                        x.Hyperlink = DN222.Hyperlink;
                        x.DesRefNo = DN222.DesRefNo;
                        x.DesPostedDate = DN222.DesPostedDate;
                        x.Reason = DN222.Reason;
                        x.DesQuantity = DN222.DesQuantity;
                        x.DesMoneys = DN222.DesMoneys;
                    }
                }
                var codes = DN22.Select(a => a.FAID).ToList();
                var DN223 = (from a in Query
                             where a.PostedDate >= from2 && a.PostedDate <= to && a.DepartmentID == DepartmentID
                             && a.TypeID == 520 && !codes.Contains(a.FixedAssetID)
                             select new S11DNN
                             {
                                 Hyperlink =  a.ReferenceID + ";" + a.TypeID,
                                 DesRefNo = a.No,
                                 DesPostedDate = a.PostedDate,
                                 Reason = a.Reason,
                                 DesQuantity = "1",
                                 DesMoneys = a.OriginalPrice,
                                 FAID = a.FixedAssetID
                             }).ToList();
                foreach (var x in DN223)
                {
                    x.FAName = _IFixedAssetService.Getbykey(x.FAID ?? Guid.Empty).FixedAssetName;
                    x.DepartmentCode = department.DepartmentCode;
                    x.DepartmentName = department.DepartmentName;
                }

                DN22.AddRange(DN223);
                return DN22.OrderBy(c => c.PostedDate).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        #endregion
        public int FindLastOrderPriority()
        {
            List<FixedAssetLedger> lst = Query.ToList();
            return lst.Count > 0 ? lst.Max(opfa => opfa.OrderPriority) : 0;

        }

        public FixedAssetLedger FindByFixedAssetID(Guid iD)
        {
            try
            {
                var lst = Query.Where(fai => fai.FixedAssetID == iD).OrderByDescending(fix => fix.OrderPriority).ToList();
                return lst.Count > 0 ? lst[0] : null;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
        }

        public int CheckLedgerForDecrement(Guid FixedAssetID, DateTime postedDate, bool isCheckPostedDate)
        {
            if (isCheckPostedDate)
            {
                if (Query.Any(fal => fal.FixedAssetID == FixedAssetID
                                        && fal.PostedDate <= postedDate
                                        && fal.TypeID == 520))
                    return 1;
            }
            else
                if (Query.Any(fal => fal.FixedAssetID == FixedAssetID
                                        && fal.TypeID == 520))
                return 1;
            if (Query.Any(fal => fal.FixedAssetID == FixedAssetID
                                    && fal.PostedDate >= postedDate
                                    && (fal.TypeID == 530 || fal.TypeID == 550 || fal.TypeID == 540)))
                return 2;
            else return 0;
        }

        public bool CheckLedgerForAdjustment(Guid FixedAssetID, DateTime postedDate)
        {
            return Query.Any(fal => fal.FixedAssetID == FixedAssetID
                                    && fal.PostedDate >= postedDate
                                    && (fal.TypeID == 520 || fal.TypeID == 550 || fal.TypeID == 540));
        }

        public bool CheckLedgerForDepreciation(Guid FixedAssetID, DateTime postedDate)
        {
            // kiểm tra điều chỉnh và điều chuyển giữa tháng
            // nếu có và ngày hạch toán của lần ghi sổ này lớn hơn ngày hạch toán của điều chỉnh hoặc điều chuyển giữa tháng
            // thì phải có 1 chứng từ khâu hao nữa, có ngày hạch toán nhỏ hơn ngày hạch toán của điều chỉnh hoặc điều chuyển giữa tháng

            var PostedDates = Query.Where(fal => (fal.TypeID == 530 || fal.TypeID == 550) && fal.PostedDate.Month == postedDate.Month
                                                && fal.PostedDate.Year == postedDate.Year && fal.PostedDate.Day < postedDate.Day).Select(fal => fal.PostedDate).ToList();
            if (PostedDates != null)
            {
                bool IsDepreciation = false;
                foreach (DateTime PostedDate in PostedDates)
                {
                    IsDepreciation = Query.Any(fal => fal.TypeID == 540 && fal.PostedDate.Month == postedDate.Month
                                                        && fal.PostedDate.Year == postedDate.Year && fal.PostedDate.Day <= postedDate.Day);
                    // Nếu chưa có bản ghi tính khấu hao thì trả về lỗi
                    if (!IsDepreciation)
                    {
                        return true;
                    }
                }

            }
            DateTime date = new DateTime(postedDate.Year, postedDate.Month, 1).AddDays(-1);
            // Kiểm tra xem đã có chứng từ giảm trước ngày tính khấu hao chưa,
            // nếu có trả về lỗi
            return Query.Any(fal => fal.FixedAssetID == FixedAssetID
                                    && fal.PostedDate <= date
                                    && fal.TypeID == 520);
        }

        public decimal GetTotalAcDepreciationAmount(Guid fixedassetid, DateTime postedDate, bool? gg = null)
        {
            decimal khauhao = 0;
            decimal day = postedDate.Day - 1;
            decimal dayinmonth = DateTime.DaysInMonth(postedDate.Year, postedDate.Month);
            FAInit fAInit = _IFAInitService.FindByFixedAssetID(fixedassetid);
            FixedAsset fixedAsset = _IFixedAssetService.Getbykey(fixedassetid);
            FixedAssetLedger faadjustment = Query.Where(fal => fal.FixedAssetID == fixedassetid && fal.PostedDate <= postedDate && fal.TypeID == 530).FirstOrDefault();
            List<FixedAssetLedger> lstDepreciation = Query.Where(fal => fal.FixedAssetID == fixedassetid && fal.PostedDate <= postedDate && fal.TypeID == 540).ToList().OrderByDescending(x => x.PostedDate).ToList();
            decimal total = faadjustment != null ? (faadjustment.AcDepreciationAmount ?? 0) : fAInit != null && fixedAsset != null ? fAInit.AcDepreciationAmount ?? 0 : 0;
            //if (faadjustment != null)
            //{
            //    return total + (Query.Where(fal => fal.FixedAssetID == fixedassetid && fal.PostedDate <= postedDate && fal.TypeID == 540 && fal.PostedDate >= faadjustment.PostedDate).Sum(fal => fal.MonthPeriodDepreciationAmount) ?? 0);
            //}
            //else
            //{
            //    return total + (Query.Where(fal => fal.FixedAssetID == fixedassetid && fal.PostedDate <= postedDate && fal.TypeID == 540).Sum(fal => fal.MonthPeriodDepreciationAmount) ?? 0);
            //}
            if (faadjustment != null)
            {
                if (day > 0)
                {
                    decimal value = fixedAsset.MonthPeriodDepreciationAmount ?? 0;
                    khauhao = (value / dayinmonth) * day;
                }
                return total + ((gg ?? false) ? khauhao : 0 ) + (Query.Where(fal => fal.FixedAssetID == fixedassetid && fal.PostedDate <= postedDate && fal.TypeID == 540 && fal.PostedDate >= faadjustment.PostedDate).Sum(fal => fal.MonthPeriodDepreciationAmount) ?? 0);
            }
            else
            {
                if (day > 0)
                {
                    decimal value = fixedAsset.MonthPeriodDepreciationAmount ?? 0;
                    khauhao = (value / dayinmonth) * day;
                }
                return total + ((gg ?? false) ? khauhao : 0) + (Query.Where(fal => fal.FixedAssetID == fixedassetid && fal.PostedDate <= postedDate && fal.TypeID == 540).Sum(fal => fal.MonthPeriodDepreciationAmount) ?? 0);
            }
        }

        public decimal FindmonthDepreciationAmount(Guid iD, DateTime postedDate)
        {
            return Query.Where(fal => new int[] { 701, 530, 500, 510, 119, 129, 132, 142, 172 }.Contains(fal.TypeID) && fal.FixedAssetID == iD && fal.PostedDate <= postedDate).OrderByDescending(fal => fal.PostedDate).Select(fal => fal.MonthPeriodDepreciationAmount).FirstOrDefault() ?? 0;
        }

        public FixedAssetLedger findByRefIdAndFixedAssetId(Guid fAIncrementID, Guid iD)
        {
            return Query.Where(fal => fal.ReferenceID == fAIncrementID && fal.FixedAssetID == iD).FirstOrDefault();
        }
    }
}

