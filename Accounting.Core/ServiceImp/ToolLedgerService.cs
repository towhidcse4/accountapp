﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class ToolLedgerService : BaseService<ToolLedger, Guid>, IToolLedgerService
    {
        //IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        IDepartmentService _IDepartmentService { get { return IoC.Resolve<IDepartmentService>(); } }
        ITIInitService _ITIInitService { get { return IoC.Resolve<ITIInitService>(); } }
        ITIInitDetailService _ITIInitDetailService { get { return IoC.Resolve<ITIInitDetailService>(); } }
        ITITransferDetailService _ITITransferDetailService { get { return IoC.Resolve<ITITransferDetailService>(); } }

        public ToolLedgerService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public int CheckLedgerForDecrement(Guid ToolsID, DateTime postedDate, bool isCheckPostedDate)
        {
            var MaterialGoods = _ITIInitService.Getbykey(ToolsID);
            if (isCheckPostedDate)
            {
                var TotalDecrementQuantity = Query.Where(fal => fal.ToolsID == ToolsID
                                        && fal.PostedDate <= postedDate
                                        && fal.TypeID == 431).ToList().Sum(a => a.DecrementQuantity);
                if (TotalDecrementQuantity >= MaterialGoods.Quantity)
                    return 1;
            }
            else
            {
                var TotalDecrementQuantity = Query.Where(fal => fal.ToolsID == ToolsID
                                                && fal.TypeID == 431).ToList().Sum(a => a.DecrementQuantity);
                if (TotalDecrementQuantity >= MaterialGoods.Quantity)
                    return 1;
            }

            if (Query.Any(fal => fal.ToolsID == ToolsID
                                    && fal.PostedDate >= postedDate
                                    && (fal.TypeID == 432 || fal.TypeID == 433 || fal.TypeID == 434)))
                return 2;
            else return 0;
        }

        public List<ToolLedger> FindByMaterialGoodsID(Guid iD)
        {
            return Query.Where(o => o.ToolsID == iD).ToList();
        }

        public List<ToolLedger> GetByListReferenceID(Guid temp)
        {
            return Query.Where(p => p.ReferenceID == temp).ToList();
        }

        public ToolLedger GrtByInitID(Guid req)
        {
            return Query.FirstOrDefault(o => o.ToolsID == req);
        }

        public bool CheckLedgerForAdjustment(Guid ToolsID, DateTime postedDate)
        {
            return Query.Any(fal => fal.ToolsID == ToolsID
                                    && fal.PostedDate >= postedDate
                                    && (fal.TypeID == 431 || fal.TypeID == 433 || fal.TypeID == 434));
        }

        public bool CheckLedgerForDepreciation(Guid guid, DateTime postedDate)
        {
            // kiểm tra điều chỉnh và điều chuyển giữa tháng
            // nếu có và ngày hạch toán của lần ghi sổ này lớn hơn ngày hạch toán của điều chỉnh hoặc điều chuyển giữa tháng
            // thì phải có 1 chứng từ khâu hao nữa, có ngày hạch toán nhỏ hơn ngày hạch toán của điều chỉnh hoặc điều chuyển giữa tháng

            var PostedDates = Query.Where(fal => (/*fal.TypeID == 432 ||*/ fal.TypeID == 433) && fal.PostedDate.Month == postedDate.Month
                                                && fal.PostedDate.Year == postedDate.Year && fal.PostedDate.Day < postedDate.Day).Select(fal => fal.PostedDate).ToList();
            if (PostedDates != null)
            {
                bool IsDepreciation = false;
                foreach (DateTime PostedDate in PostedDates)
                {
                    IsDepreciation = Query.Any(fal => fal.TypeID == 434 && fal.PostedDate.Month == postedDate.Month
                                                        && fal.PostedDate.Year == postedDate.Year && fal.PostedDate.Day <= postedDate.Day);
                    // Nếu chưa có bản ghi tính khấu hao thì trả về lỗi
                    if (!IsDepreciation)
                    {
                        return true;
                    }
                }

            }
            return false;
        }

        public bool CheckLedgerForTransfer(Guid ToolsID, DateTime postedDate, Guid? fromDepartment)
        {
            return Query.Any(fal => fal.ToolsID == ToolsID
                                    && fal.PostedDate >= postedDate
                                    && (fal.TypeID == 431 || (fal.TypeID == 433 && fal.DepartmentID == fromDepartment) || fal.TypeID == 434));
        }

        public List<S11DNN> RS11DNN(DateTime from, DateTime to, List<Guid> ListDepartmentID, bool CheckPrints)
        {
            List<S11DNN> output = new List<S11DNN>();
            foreach (Guid x in ListDepartmentID)
            {
                List<S11DNN> list = GetS11DNN(from, to, x, CheckPrints);
                output.AddRange(list);
            }
            return output;
        }

        public List<S11DNN> GetS11DNN(DateTime froms, DateTime tos, Guid DepartmentID, bool CheckPrints)
        {
            try
            {
                DateTime from2 = new DateTime(froms.Year, froms.Month, froms.Day);
                DateTime to = new DateTime(tos.Year, tos.Month, tos.Day);
                Department department = _IDepartmentService.Getbykey(DepartmentID);

                //var opn = (from a in Query
                //           join b in _ITIInitService.Query on a.ToolsID equals b.ID
                //           join c in _ITIInitDetailService.Query on a.ToolsID equals c.TIInitID
                //           where a.DepartmentID == DepartmentID && a.PostedDate < from2
                //                       && c.ObjectID == DepartmentID && (a.TypeID == 430 || a.TypeID == 907
                //                       || a.TypeID == 902 || a.TypeID == 903 || a.TypeID == 904 || a.TypeID == 905
                //                       || a.TypeID == 906 || a.TypeID == 703)
                //           orderby a.PostedDate descending
                //           select new S11DNN
                //           {
                //               No = a.Nos,
                //               PostedDate = a.IncrementDate ?? DateTime.Now,
                //               Quantity = a.IncrementQuantity ?? 0,
                //               Moneys = (decimal)a.IncrementAmount,
                //               OriginalPriceDebitAmount = b.UnitPrice,
                //               FAID = a.ToolsID,
                //               FAName = b.ToolsName,
                //               FACode = b.ToolsCode,
                //               Unit = b.Unit,
                //           }).FirstOrDefault();

                var s11dnns = (from a in Query
                               join b in _ITIInitService.Query on a.ToolsID equals b.ID
                               join c in _ITIInitDetailService.Query on a.ToolsID equals c.TIInitID
                               where a.DepartmentID == DepartmentID /*&& a.PostedDate >= from2*/ && a.PostedDate <= to
                                       && c.ObjectID == DepartmentID && (a.TypeID == 430 || a.TypeID == 907
                                       || a.TypeID == 902 || a.TypeID == 903 || a.TypeID == 904 || a.TypeID == 905
                                       || a.TypeID == 906 || a.TypeID == 703 )
                               orderby b.ToolsCode
                               select new S11DNN
                               {   Hyperlink= a.ReferenceID + ";" + a.TypeID,
                                   No = a.No,
                                   PostedDate = a.PostedDate,
                                   Quantity = a.IncrementQuantity ?? 0,
                                   Moneys = /*(decimal)a.IncrementAmount*/((a.IncrementQuantity ?? 0) * b.UnitPrice),
                                   OriginalPriceDebitAmount = b.UnitPrice,
                                   FAID = a.ToolsID,
                                   FAName = b.ToolsName,
                                   FACode = b.ToolsCode,
                                   Unit = b.Unit,
                               }).ToList();
                //if (opn != null)
                //    s11dnns.Insert(0, opn);
                var s11dnns2 = (from a in Query
                                join b in _ITITransferDetailService.Query on a.ToolsID equals b.ToolsID
                                join c in _ITIInitService.Query on a.ToolsID equals c.ID
                                where a.DepartmentID == DepartmentID && a.PostedDate >= from2 && a.PostedDate <= to
                                       && a.TypeID == 433 && b.ToDepartmentID == DepartmentID && a.IncrementQuantity != 0
                               select new S11DNN
                               {
                                   Hyperlink = a.ReferenceID + ";" + a.TypeID,
                                   No = a.No,
                                   PostedDate = a.PostedDate,
                                   Quantity = a.IncrementQuantity ?? 0,
                                   Moneys = /*(decimal)a.IncrementAmount*/ ((a.IncrementQuantity ?? 0) * (a.UnitPrice ?? 0)),
                                   OriginalPriceDebitAmount = a.UnitPrice ?? 0,
                                   FAID = a.ToolsID,
                                   FAName = c.ToolsName,
                                   Unit = c.Unit,
                               }).ToList();
                s11dnns.AddRange(s11dnns2);
                foreach (var x in s11dnns)
                {
                    x.DepartmentID = DepartmentID;
                    x.DepartmentCode = department.DepartmentCode;
                    x.DepartmentName = department.DepartmentName;
                    
                }
                s11dnns = s11dnns.OrderBy(x => x.PostedDate).ToList();
                //var codes = s11dnns.Select(a => a.FAID).ToList();
                var DN223 = (from a in Query
                             where a.PostedDate >= from2 && a.PostedDate <= to && a.DepartmentID == DepartmentID
                             && a.TypeID == 431 
                             //&& !codes.Contains(a.ToolsID)
                             select new S11DNN
                             {
                                 Hyperlink = a.ReferenceID + ";" + a.TypeID,
                                 DesRefNo = a.No,
                                 DesPostedDate = a.PostedDate,
                                 Reason = a.Reason,
                                 DesQuantity = (a.DecrementQuantity ?? 0).ToString(),
                                 DesMoneys = a.RemainingAmount,
                                 FAID = a.ToolsID
                             }).ToList();
                foreach (var x in DN223)
                {
                    x.FAName = _ITIInitService.Getbykey(x.FAID ?? Guid.Empty).ToolsName;
                    x.DepartmentCode = department.DepartmentCode;
                    x.DepartmentName = department.DepartmentName;
                }
                s11dnns.AddRange(DN223);
                return s11dnns.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<TIInit> FindAllotionTools(DateTime fromDate, DateTime toDate, List<Guid> lstID)
        {
            List<TIInit> result = _ITIInitService.Query.ToList().Where(fix => lstID.Any(d => d == fix.ID)).ToList().CloneObject();
            List<ToolLedger> lsttg = Query.Where(fal => fal.PostedDate <= toDate).ToList().Where(x => lstID.Any(d => d == x.ToolsID) && (x.TypeID == 434 || x.TypeID == 431 || x.TypeID == 703 || x.TypeID == 430)).ToList();
            foreach (var x in result)
            {
                ToolLedger toolLedger = lsttg.Where(fal => fal.ToolsID == x.ID && fal.TypeID == 434).OrderByDescending(fal => fal.PostedDate).FirstOrDefault();
                ToolLedger decre = lsttg.Where(fal => fal.ToolsID == x.ID && fal.TypeID == 431).OrderByDescending(fal => fal.PostedDate).FirstOrDefault();
                ToolLedger incre = lsttg.Where(fal => fal.ToolsID == x.ID && fal.TypeID == 430).OrderByDescending(fal => fal.PostedDate).FirstOrDefault();
                if (toolLedger != null)
                {
                    x.AllocatedTimes = (Convert.ToDecimal(x.AllocationTimes ?? 0) - toolLedger.RemainingAllocaitonTimes);
                    x.RemainAllocationTimes = Convert.ToInt32(toolLedger.RemainingAllocaitonTimes);
                    x.RemainAmount = toolLedger.RemainingAmount;
                    x.AllocationAmount = x.Amount - toolLedger.RemainingAmount;
                    if (x.DeclareType == 1) x.IncrementDate = x.PostedDate;
                }
                else
                {
                    if (x.DeclareType == 0)
                    {
                        x.AllocatedTimes = null;
                        x.RemainAllocationTimes = null;
                        x.RemainAmount = 0;
                        x.AllocationAmount = null;
                    }
                    else
                    {
                        x.AllocatedTimes = (Convert.ToDecimal(x.AllocationTimes ?? 0) - Convert.ToDecimal(x.RemainAllocationTimes ?? 0));
                        x.IncrementDate = x.PostedDate;
                    }
                }
                if (decre != null) x.DecrementDate = decre.PostedDate;
                if (incre != null) x.IncrementDate = incre.PostedDate;
            }
            result = result.Where(x => x.IncrementDate <= toDate).ToList();
            return result;
        }
    }

}