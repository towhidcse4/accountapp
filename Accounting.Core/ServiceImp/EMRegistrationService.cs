﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMRegistrationService : BaseService<EMRegistration, Guid>, IEMRegistrationService
    {
        public EMRegistrationService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<EMRegistration> GetListEMRegistrationID(Guid? tempID)
        {
            List<EMRegistration> list = Query.Where(a => a.EMPublishPeriodID == tempID).ToList();
            return list;
        }

        public List<string> GetListEMRegistrationCode()
        {
            List<string> hh = Query.Select(a => a.RegistrationCode).ToList();
            return hh;
        }
    }

}


