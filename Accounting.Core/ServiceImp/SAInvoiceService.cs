﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.OverdueInterest;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using NHibernate;

namespace Accounting.Core.ServiceImp
{
    public class SAInvoiceService : BaseService<SAInvoice, Guid>, ISAInvoiceService
    {
        public SAInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public IGeneralLedgerService IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public IAccountService IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        public IAccountingObjectService IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IPaymentClauseService IPaymentClauseService { get { return IoC.Resolve<IPaymentClauseService>(); } }
        public IPPInvoiceService IPPInvoiceService { get { return IoC.Resolve<IPPInvoiceService>(); } }
        public IPPServiceService IPPServiceService { get { return IoC.Resolve<IPPServiceService>(); } }
        public IExceptVoucherService IExceptVoucherService { get { return IoC.Resolve<IExceptVoucherService>(); } }
        public ICurrencyService ICurrencyService { get { return IoC.Resolve<ICurrencyService>(); } }
        private ISAInvoiceDetailService _ISAInvoiceDetailService { get { return IoC.Resolve<ISAInvoiceDetailService>(); } }
        ISAReturnService SaReturnService
        {
            get { return IoC.Resolve<ISAReturnService>(); }
        }
        public ISystemOptionService _ISystemOptionService
        {
            get { return IoC.Resolve<ISystemOptionService>(); }
        }
        IMaterialGoodsService MaterialGoodsService
        {
            get { return IoC.Resolve<IMaterialGoodsService>(); }
        }

        public List<SAInvoice> GetSaInvoices(int year, List<int> dsTypeID)
        {
            return Query.Where(k => (k.Date.Year == year) && (dsTypeID.Contains(k.TypeID))).ToList();
        }
        //HUYPD
        public ISession GetSession()
        {
            return NHibernateSession;
        }
        /// <summary>
        /// Tạo mới chứng từ
        /// </summary>
        /// <param name="newSaInvoice">chứng từ mới</param>
        /// <param name="saInvoiceDetails">danh sách chi tiết về chứng từ</param>
        /// <param name="errMessage">Lỗi</param>
        /// <returns>true (thành công) or false (thất bại)</returns>
        public bool CreateNew(SAInvoice newSaInvoice, IList<SAInvoiceDetail> saInvoiceDetails, out string errMessage)
        {
            BeginTran();
            errMessage = string.Empty;
            try
            {
                if (saInvoiceDetails == null || saInvoiceDetails.Count == 0)
                {
                    errMessage = "Chưa thêm thông tin chi tiết về chứng từ!";
                    return false;
                }
                newSaInvoice.SAInvoiceDetails = saInvoiceDetails;
                Save(newSaInvoice);
                CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                errMessage = ex.Message;
                return false;
            }
        }
        /// <summary>
        /// Lấy ra danh sách các SAInvoice theo AccountingObjectID và PostedDate
        /// [DUYTN]
        /// </summary>
        /// <param name="AccountingObjectID">là AccountingObjectID truyền vào cần tìm</param>
        /// <param name="PostedDate">là PostedDate truyền vào cần lấy(chú ý truyền vào dạng datetime)</param>
        /// <returns>tập các , null nếu bị lỗi</returns>
        public List<SAInvoice> GetAll_ByAccountingObjectID_AndPostedDate(Guid AccountingObjectID, DateTime PostedDate)
        {
            return Query.Where(k => k.AccountingObjectID == AccountingObjectID && k.PostedDate.Date <= PostedDate.Date).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách các SAInvoice theo TypeID
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là tham số TypeID truyền vào</param>
        /// <returns>tập các SAInvoice, null nếu bị lỗi</returns>
        public List<SAInvoice> GetAll_ByTypeID(int TypeID)
        {
            return Query.Where(k => k.TypeID == TypeID).ToList();
        }

        /// <summary>
        /// danh sách các hóa đơn_SAInvoice theo TypeID của [đối tượng hiện tại] và có [ngày hoạch toán] nhỏ hơn [ngày hiện tại - Ngày tính lãi nợ]
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là TypeID truyền vào</param>
        /// <param name="DateTime">là Ngày tính lãi nợ</param>
        /// <param name="AccountingObjectID">là AccountingObjectID của đối tượng AccountingObject cần tìm</param>
        /// <returns></returns>
        public List<SAInvoice> GetListInVoid(int TypeID, DateTime DateTime, Guid AccountingObjectID)
        {
            return GetAll_ByTypeID(TypeID).Where(k => (k.PostedDate.Date <= DateTime.Date)
                && (k.DueDate != null) && (k.DueDate.GetValueOrDefault().Date <= DateTime.Date)
                && (k.AccountingObjectID == AccountingObjectID)).ToList();
        }

        public List<OverdueInterest> GetOverdueInterest(Guid AccountingObjectID, string AccountNumber, DateTime ngayTinhLai)
        {
            List<Account> lstAccount = IAccountService.GetCustomerAcctList(true, "1");
            List<GeneralLedger> lstG = IGeneralLedgerService.GetGLByListID(lstAccount.Select(a => a.AccountNumber).ToList());
            List<int> custObjType = new List<int>();
            custObjType.Add(0);
            List<AccountingObject> lstAccountingObj = IAccountingObjectService.GetAll_ByListObjectType(custObjType).Where(ao => ao.IsActive == true).Where(ao => ao.ID == AccountingObjectID).ToList();
            List<ExceptVoucher> lstExVoucher = IExceptVoucherService.Query.Where(e => e.AccountingObjectID == AccountingObjectID).ToList();
            List<Currency> lstCurr = ICurrencyService.Query.Where(c => c.IsActive == true).ToList();

            var lstAccountGL = from g in lstG
                               join acct in lstAccount on g.Account equals acct.AccountNumber
                               join acctObj in lstAccountingObj on g.AccountingObjectID equals acctObj.ID
                               join exVoucher in lstExVoucher on g.No equals exVoucher.No
                               join curr in lstCurr on g.CurrencyID equals curr.ID
                               group new { g, acct, acctObj, exVoucher } by new
                               {
                                   g.Account,
                                   g.No,
                                   g.AccountingObjectID,
                                   g.DebitAmount,
                                   g.DebitAmountOriginal,
                                   g.CreditAmount,
                                   g.CreditAmountOriginal,
                                   g.CurrencyID,
                                   curr.ExchangeRate,
                                   acctObj.AccountingObjectName,
                                   acctObj.AccountingObjectCode,
                                   exVoucher.Amount
                               }
                                   into joinAcctGl
                               where joinAcctGl.Key.Account == AccountNumber && joinAcctGl.Key.AccountingObjectID == AccountingObjectID
                               select new
                               {
                                   AccountNumber = joinAcctGl.Key.Account,
                                   InvoiceNo = joinAcctGl.Key.No,
                                   joinAcctGl.Key.AccountingObjectID,
                                   joinAcctGl.Key.AccountingObjectCode,
                                   joinAcctGl.Key.AccountingObjectName,
                                   joinAcctGl.Key.CurrencyID,
                                   joinAcctGl.Key.DebitAmount,
                                   joinAcctGl.Key.DebitAmountOriginal,
                                   joinAcctGl.Key.CreditAmount,
                                   joinAcctGl.Key.CreditAmountOriginal,
                                   ExceptVoucherAmount = joinAcctGl.Key.Amount,
                                   ExchangeRate = joinAcctGl.Key.ExchangeRate
                               };

            List<PPInvoice> lstInvoice = IPPInvoiceService.Query.Where(i => i.AccountingObjectID == AccountingObjectID).ToList();
            List<PPService> lstService = IPPServiceService.Query.Where(s => s.AccountingObjectID == AccountingObjectID).ToList();
            List<PaymentClause> lstPaymentClause = IPaymentClauseService.Query.Where(p => p.IsActive == true).ToList();

            var lstInvoiceInfo = (from i in lstInvoice
                                  join p in lstPaymentClause on i.PaymentClauseID equals p.ID
                                  group new { i, p } by new
                                  {
                                      i.AccountingObjectID,
                                      i.DueDate,
                                      i.PaymentClauseID,
                                      p.OverdueInterestPercent
                                  }
                                      into joinInvoice
                                  select new
                                  {
                                      joinInvoice.Key.AccountingObjectID,
                                      joinInvoice.Key.DueDate,
                                      joinInvoice.Key.OverdueInterestPercent
                                  }).Union(from s in lstService
                                           join p in lstPaymentClause on s.PaymentClauseID equals p.ID
                                           group new { s, p } by new
                                           {
                                               s.AccountingObjectID,
                                               s.DueDate,
                                               s.PaymentClauseID,
                                               p.OverdueInterestPercent
                                           }
                                                   into joinService
                                           select new
                                           {
                                               joinService.Key.AccountingObjectID,
                                               joinService.Key.DueDate,
                                               joinService.Key.OverdueInterestPercent
                                           }).ToList();

            var lstCustOverDue = from a in lstAccountGL
                                 join i in lstInvoiceInfo on a.AccountingObjectID equals i.AccountingObjectID
                                 group new { a, i } by new
                                 {
                                     a.AccountNumber,
                                     a.InvoiceNo,
                                     a.AccountingObjectID,
                                     a.AccountingObjectCode,
                                     a.AccountingObjectName,
                                     a.CurrencyID,
                                     a.ExchangeRate,
                                     a.DebitAmount,
                                     a.DebitAmountOriginal,
                                     a.CreditAmount,
                                     a.CreditAmountOriginal,
                                     a.ExceptVoucherAmount,
                                     i.OverdueInterestPercent,
                                     i.DueDate
                                 }
                                     into joined
                                 where joined.Key.DueDate < DateTime.Now.Date
                                        && ngayTinhLai > joined.Key.DueDate
                                        && joined.Sum(j => (joined.Key.DebitAmount - joined.Key.CreditAmount - joined.Key.ExceptVoucherAmount)) > 0
                                 select new
                                 {
                                     MaKhachHang = (Guid)joined.Key.AccountingObjectID,
                                     TenKhachHang = joined.Key.AccountingObjectName,
                                     SoChungTu = joined.Key.InvoiceNo,
                                     LoaiTien = joined.Key.CurrencyID,
                                     TiGia = joined.Key.ExchangeRate,
                                     NoQuaHan = joined.Sum(j => (joined.Key.DebitAmount - joined.Key.CreditAmount - joined.Key.ExceptVoucherAmount)),
                                     NoQuaHanQd = joined.Sum(j => (joined.Key.DebitAmount - joined.Key.CreditAmount - joined.Key.ExceptVoucherAmount)) * joined.Key.ExchangeRate,
                                     TienPhat = joined.Sum(j => (joined.Key.DebitAmount - joined.Key.CreditAmount - joined.Key.ExceptVoucherAmount)) * (joined.Key.OverdueInterestPercent == null ? 0 : Convert.ToDecimal(joined.Key.OverdueInterestPercent) * (ngayTinhLai - joined.Key.DueDate.Value).Days),
                                     TienPhatQd = joined.Sum(j => (joined.Key.DebitAmount - joined.Key.CreditAmount - joined.Key.ExceptVoucherAmount)) * (joined.Key.OverdueInterestPercent == null ? 0 : joined.Key.OverdueInterestPercent) * joined.Key.ExchangeRate * (ngayTinhLai - joined.Key.DueDate.Value).Days,
                                     TkPhaiThu = joined.Key.AccountNumber
                                 };
            List<OverdueInterest> lstOverDueInterest = new List<OverdueInterest>();
            foreach (var custOverDue in lstCustOverDue)
            {
                OverdueInterest custOverDueInterest = new OverdueInterest
                {
                    ID = custOverDue.MaKhachHang,
                    TenKhachHang = custOverDue.TenKhachHang,
                    SoChungTu = custOverDue.SoChungTu,
                    LoaiTien = custOverDue.LoaiTien,
                    NoQuaHan = custOverDue.NoQuaHan,
                    NoQuaHanQd = Convert.ToDecimal(custOverDue.NoQuaHanQd),
                    TienPhat = Convert.ToDecimal(custOverDue.TienPhat),
                    TienPhatQd = Convert.ToDecimal(custOverDue.TienPhatQd),
                    TkPhaiThu = custOverDue.TkPhaiThu
                };
                lstOverDueInterest.Add(custOverDueInterest);
            }

            return lstOverDueInterest;
        }
        /// <summary>
        /// Sổ chi tiết bán hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="materialGoodsIDs">danh sách mã tài sản cố định</param>
        /// <returns></returns>
        public List<S17DNN> ReportS35DN(DateTime fromDate, DateTime toDate, IEnumerable<Guid> materialGoodsIDs)
        {
            var lstReportSaInvoiceDetail = new List<S17DNN>();
            var lstSaReturn = SaReturnService.GetAll().Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate
                                                                  &&
                                                                  c.SAReturnDetails.Any(d => materialGoodsIDs.Contains((Guid)d.MaterialGoodsID)))
                                                                 .Select(c => new
                                                                 {
                                                                     SaReturn = c,
                                                                     c.SAReturnDetails
                                                                 }).ToList();
            foreach (var materialGoodID in materialGoodsIDs)
            {
                Guid id = materialGoodID;
                var lstSaInvoiceDetail = (from sainvoice in Query.ToList()
                                          from sainvoiceDetail in sainvoice.SAInvoiceDetails
                                          let materialGoodsID = sainvoiceDetail.MaterialGoodsID
                                          // ReSharper disable once ImplicitlyCapturedClosure
                                          where materialGoodsID != null
                                          where materialGoodsID == id
                                                                      && sainvoice.PostedDate >= fromDate && sainvoice.PostedDate <= toDate
                                                                      && sainvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == materialGoodID)
                                          select new S17DNN
                                          {
                                              MaterialGoodsID = (Guid)materialGoodsID,
                                              MaterialGoodsName = MaterialGoodsService.Getbykey((Guid)materialGoodsID).MaterialGoodsName,
                                              SAInvoiceID = sainvoice.ID,
                                              Hyperlink = sainvoice.ID + ";" + sainvoice.TypeID,
                                              PostedDate = sainvoice.PostedDate,
                                              Date = sainvoice.Date,
                                              RefNo = sainvoice.NumberAttach ?? sainvoice.No,
                                              Amount = sainvoice.TotalAmount,
                                              CreditAccount = sainvoiceDetail.CreditAccount,
                                              Description = sainvoice.Reason,
                                              Quantity = sainvoiceDetail.Quantity ?? 0,
                                              UnitPrice = sainvoiceDetail.UnitPrice,
                                              OutwardAmount = sainvoiceDetail.OWAmount -
                                              (from a in lstSaReturn.SelectMany(b => b.SAReturnDetails)
                                               where a.SAInvoiceID == sainvoice.ID
                                               select a.OWAmount).FirstOrDefault(),
                                              DiscountAmount = sainvoiceDetail.DiscountAmount,
                                              VATRate = 0
                                          }).ToList();
                lstReportSaInvoiceDetail.AddRange(lstSaInvoiceDetail);
                var lstReportDetail = (from b in lstSaReturn.Select(c => c.SaReturn)
                                       from c in b.SAReturnDetails
                                       let saInvoiceID = c.SAInvoiceID
                                       where saInvoiceID != null
                                       //where lstSaInvoiceDetail.Any(k => k.SAInvoiceID == saInvoiceID)
                                       //                   && lstSaInvoiceDetail.Any(d => d.MaterialGoodsID == c.MaterialGoodsID)
                                       select new S17DNN
                                       {
                                           MaterialGoodsID = (Guid)c.MaterialGoodsID,
                                           MaterialGoodsName = MaterialGoodsService.Getbykey((Guid)c.MaterialGoodsID).MaterialGoodsName,
                                           SAInvoiceID = (Guid)saInvoiceID,
                                           Hyperlink = b.ID + ";" + b.TypeID,
                                           PostedDate = b.PostedDate,
                                           Date = b.Date,
                                           RefNo = b.No,
                                           Amount = b.TotalAmount,
                                           CreditAccount = c.CreditAccount,
                                           Description = b.Reason,
                                           Quantity = 0,
                                           UnitPrice = 0,
                                           OutwardAmount = 0,
                                           DiscountAmount = c.DiscountAmount,
                                           VATRate = 0
                                       }).ToList();
                lstReportSaInvoiceDetail.AddRange(lstReportDetail);
            }
            return lstReportSaInvoiceDetail;
        }

        /// <summary>
        /// Sổ nhật ký bán hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <returns></returns>
        public List<S03A4DNN> ReportS03A4Dns(DateTime fromDate, DateTime toDate)
        {
            return (from a in Query
                    from b in a.SAInvoiceDetails
                    where a.PostedDate >= fromDate && a.PostedDate <= toDate
                    select new S03A4DNN
                    {
                        PostedDate = a.PostedDate,
                        Date = a.Date,
                        No = a.OriginalNo ?? a.No,
                        Description = b.Description,
                        Amount5111 = b.CreditAccount.StartsWith("5111") ? b.Amount : 0,
                        Amount5112 = b.CreditAccount.StartsWith("5112") ? b.Amount : 0,
                        Amount5113 = b.CreditAccount.StartsWith("5113") ? b.Amount : 0,
                        AmountOther = b.CreditAccount.StartsWith("5118") ? b.Amount : 0,
                        Amount = b.Amount,
                        Hyperlink = a.TypeID + ";" + a.ID
                    }).ToList();
        }
        private IMBDepositDetailCustomerService _iMBDepositDetailCustomerService { get { return IoC.Resolve<IMBDepositDetailCustomerService>(); } }
        private IMCReceiptDetailCustomerService _iMCReceiptDetailCustomerService { get { return IoC.Resolve<IMCReceiptDetailCustomerService>(); } }
        private ISAReturnDetailCustomerService _iSAReturnDetailCustomerService { get { return IoC.Resolve<ISAReturnDetailCustomerService>(); } }
        /// <summary>
        /// Hàm tính lãi nợ
        /// </summary>
        /// <param name="lstAccountingObjects">danh sách các nhà cung cấp cần tính lãi nợ</param>
        /// <param name="lstAccounts">danh sách các tài khoản phải thu</param>
        /// <param name="dtNgayTinhLai">ngày tính lãi</param>
        /// <returns></returns>
        public List<OverdueInterest> GetOverdueInterests(List<AccountingObject> lstAccountingObjects,
            IEnumerable<Account> lstAccounts, DateTime dtNgayTinhLai)
        {
            #region code cũ
            //List<OverdueInterest> lstOverdueInterest = new List<OverdueInterest>();
            //var lstSaInvoice = (from overdueInterest in Query.Where(c => c.TypeID == 320
            //                                                                         && c.PaymentClauseID != null
            //                                                                         && c.PaymentClauseID != Guid.Empty
            //                                                                         && c.DueDate != null
            //                                                                         && c.ExchangeRate != null
            //                                                                         && c.DueDate < dtNgayTinhLai &&
            //                                                                         lstAccountingObjects.Any(
            //                                                                             d =>
            //                                                                                 d.ID ==
            //                                                                                 c.AccountingObjectID)).ToList()
            //              where overdueInterest.SaInvoiceDetails.Any(b => lstAccounts.Any(c => c.AccountNumber == b.DebitAccount))
            //              let saInvoiceDetails = overdueInterest.SaInvoiceDetails.GroupBy(c => c.DebitAccount).FirstOrDefault()
            //              where saInvoiceDetails != null
            //              select new
            //              {
            //                  MaKhachHang = (Guid)overdueInterest.AccountingObjectID,
            //                  TenKhachHang = overdueInterest.AccountingObjectName,
            //                  SoChungTu = overdueInterest.No,
            //                  LoaiTien = overdueInterest.CurrencyID,
            //                  NoQuaHan = overdueInterest.TotalAmount,
            //                  TyGia = overdueInterest.ExchangeRate,
            //                  NoQuaHanQd = overdueInterest.TotalAmountOriginal,
            //                  TienPhat = (decimal)(overdueInterest.DueDate != null ?
            //                      overdueInterest.TotalAmount * (overdueInterest.DueDate.Value - dtNgayTinhLai).Days
            //                      * _iPaymentClauseService.Getbykey((Guid)overdueInterest.PaymentClauseID).OverdueInterestPercent : 0),
            //                  TienPhatQd = (decimal)((overdueInterest.DueDate != null ?
            //                      overdueInterest.TotalAmount * (overdueInterest.DueDate.Value - dtNgayTinhLai).Days
            //                      * _iPaymentClauseService.Getbykey((Guid)overdueInterest.PaymentClauseID).OverdueInterestPercent : 0) * overdueInterest.ExchangeRate),
            //                  TkPhaiThu = saInvoiceDetails.Key,
            //                  SAInvoiceID = overdueInterest.ID,
            //                  SoNgayQuaHan = (overdueInterest.DueDate.Value - dtNgayTinhLai),
            //                  DieuKhoanThanhToan = overdueInterest.PaymentClauseID
            //              }).ToList();
            ////danh sách những hóa đơn bán hàng chưa trả tiền, có điều khoản thanh toán
            //var lstExceptVoucher = (from a in _iExceptVoucherService.Query.ToList()
            //               where lstSaInvoice.Any(c => c.SAInvoiceID == a.GLVoucherID)
            //               && lstSaInvoice.Any(c => c.MaKhachHang == a.AccountingObjectID)
            //               && lstSaInvoice.Any(c => c.TkPhaiThu == a.DebitAccount)
            //               group a by new { a.GLVoucherID, a.DebitAccount, a.AccountingObjectID } into t
            //               select new
            //               {
            //                   Amount = t.Sum(c => c.Amount),
            //                   AmountOriginal = t.Sum(c => c.AmountOriginal),
            //                   DebitAccount = t.Key.DebitAccount,
            //                   AccountingObjectID = t.Key.AccountingObjectID,
            //                   SAInvoiceID = t.Key.GLVoucherID
            //               }).ToList();
            ////danh sách đối trừ chứng từ

            //foreach (var ak in lstSaInvoice)
            //{
            //    if (lstExceptVoucher.Count > 0)
            //    {
            //        foreach (var al in lstExceptVoucher)
            //        {
            //            if (ak.SAInvoiceID == al.SAInvoiceID && al.Amount < ak.NoQuaHan)//nếu tồn tại trong bảng ExceptVoucher thì tính lại nợ quá hạn
            //            {
            //                OverdueInterest ov = new OverdueInterest
            //                {
            //                    LoaiTien = ak.LoaiTien,
            //                    MaKhachHang = ak.MaKhachHang,
            //                    NoQuaHan = ak.NoQuaHan - al.Amount
            //                };
            //                ov.NoQuaHanQd = (decimal)(ov.NoQuaHan * ak.TyGia);
            //                ov.SoChungTu = ak.SoChungTu;
            //                ov.TenKhachHang = ak.TenKhachHang;
            //                ov.TienPhat = (decimal)(ov.NoQuaHan * ak.SoNgayQuaHan.Days*_iPaymentClauseService.Getbykey((Guid)ak.DieuKhoanThanhToan) .OverdueInterestPercent);
            //                ov.TienPhatQd = (decimal)(ov.TienPhat * ak.TyGia);
            //                ov.TkPhaiThu = ak.TkPhaiThu;
            //                lstOverdueInterest.Add(ov);
            //            }
            //        }
            //    }
            //    if (lstExceptVoucher.All(c => c.SAInvoiceID != ak.SAInvoiceID))//nếu không tồn tại trong bảng ExceptVoucher thì giữ nguyên lãi nợ
            //    {
            //        OverdueInterest ov = new OverdueInterest
            //        {
            //            LoaiTien = ak.LoaiTien,
            //            MaKhachHang = ak.MaKhachHang,
            //            NoQuaHan = ak.NoQuaHan,
            //        };
            //        ov.NoQuaHanQd = (decimal)(ov.NoQuaHan * ak.TyGia);
            //        ov.SoChungTu = ak.SoChungTu;
            //        ov.TenKhachHang = ak.TenKhachHang;
            //        ov.TienPhat = ak.TienPhat;
            //        ov.TienPhatQd = (decimal)(ov.TienPhat * ak.TyGia);
            //        ov.TkPhaiThu = ak.TkPhaiThu;
            //        lstOverdueInterest.Add(ov);
            //    }
            //}
            //return lstOverdueInterest;
            #endregion
            try
            {
                int lamtrondg = 0;
                var ddSoTyGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
                if (ddSoTyGia != null) lamtrondg = int.Parse(ddSoTyGia.Data);
                List<ExceptVoucher> lstExceptVouchers = IExceptVoucherService.GetAll();
                List<MBDepositDetailCustomer> lstMbDepositDetailCustomers = _iMBDepositDetailCustomerService.GetAll();
                List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = _iMCReceiptDetailCustomerService.GetAll();
                List<SAReturnDetailCustomer> lstSaReturnDetailCustomers = _iSAReturnDetailCustomerService.GetAll();
                List<PaymentClause> lstPaymentClauses = IPaymentClauseService.GetIsActive(true);
                List<SAInvoice> lstSaInvoices = (Query.Where(c => (c.TypeID == 320 || c.TypeID == 323) && c.PaymentClauseID != null
                                                && c.PaymentClauseID != Guid.Empty
                                                && c.DueDate != null
                                                && c.ExchangeRate != null
                                                && c.DueDate < dtNgayTinhLai)).ToList().Where(c => lstAccountingObjects.Any(d => d.ID == c.AccountingObjectID)).ToList(); ;
                List<OverdueInterest> lstOverdueInterests = (from d in lstSaInvoices
                                                             from b in d.SAInvoiceDetails
                                                             where
                                                                 lstAccounts.Any(c => c.AccountNumber == b.DebitAccount) &&
                                                                 lstAccountingObjects.Any(c => c.ID == b.AccountingObjectID)
                                                                 && d.Recorded
                                                             select new OverdueInterest
                                                             {
                                                                 LoaiTien = d.CurrencyID,
                                                                 ID = (Guid)d.AccountingObjectID,
                                                                 MaKhachHang = lstAccountingObjects.Count(c => c.ID == (Guid)d.AccountingObjectID) == 1 ? lstAccountingObjects.Single(c => c.ID == (Guid)d.AccountingObjectID).AccountingObjectCode : "",
                                                                 SoChungTu = d.No,
                                                                 NoQuaHan =
                                                                     ((Math.Round(b.Amount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(b.DiscountAmount, lamtrondg, MidpointRounding.AwayFromZero) + Math.Round(b.VATAmount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                      lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                      lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                      lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))
                                                                      - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))),
                                                                 NoQuaHanQd =
                                                                     (decimal)
                                                                         (((Math.Round(b.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(b.DiscountAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero) + Math.Round(b.VATAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                           lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                           lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                           lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))
                                                                           - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)))),
                                                                 TenKhachHang = d.AccountingObjectName,
                                                                 TienPhat =
                                                                     (decimal)
                                                                         (d.DueDate != null
                                                                             ? (((Math.Round(b.Amount, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(b.DiscountAmount, lamtrondg, MidpointRounding.AwayFromZero) + Math.Round(b.VATAmount, lamtrondg, MidpointRounding.AwayFromZero))
                                                                             - lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))
                                                                             - lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))
                                                                                 - lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))
                                                                                 - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => Math.Round(c.Amount, lamtrondg, MidpointRounding.AwayFromZero))) *
                                                                                ((dtNgayTinhLai - d.DueDate.Value).Days * (lstPaymentClauses.Count(c => c.ID == d.PaymentClauseID) == 1 ? lstPaymentClauses.Single(c => c.ID == d.PaymentClauseID).OverdueInterestPercent : 0) / 100))
                                                                             : 0),
                                                                 TienPhatQd =
                                                                     (decimal)
                                                                         ((d.DueDate != null
                                                                             ? (((Math.Round(b.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero) - Math.Round(b.DiscountAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero) + Math.Round(b.VATAmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                                 lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                                 lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero)) -
                                                                                 lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))
                                                                                 - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => Math.Round(c.AmountOriginal, lamtrondg, MidpointRounding.AwayFromZero))) *
                                                                                ((dtNgayTinhLai - d.DueDate.Value).Days * (lstPaymentClauses.Count(c => c.ID == d.PaymentClauseID) == 1 ? lstPaymentClauses.Single(c => c.ID == d.PaymentClauseID).OverdueInterestPercent : 0) / 100))
                                                                             : 0)),
                                                                 TkPhaiThu = b.DebitAccount
                                                             }).ToList();
                //return (from i in lstOverdueInterests 
                //        group i by new { i.MaKhachHang,i.LoaiTien} into g
                //        from d in g
                //        select new OverdueInterest()
                //        {
                //            LoaiTien = g.Key.LoaiTien,
                //            MaKhachHang = d.SoChungTu,
                //        }
                //)
                return lstOverdueInterests.Where(c => c.NoQuaHan > 0).ToList();
            }
            catch(Exception ex)
            {
                return new List<OverdueInterest>();
            }
        }

        /// <summary>
        /// Thông báo công nợ phải trả
        /// [DuyNT]
        /// </summary>
        /// <param name="dtDenNgay">Ngày tính công nợ</param>
        /// <param name="ngayThongBao"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public List<ListLibitiesReport> GetListLibitiesReport(DateTime dtDenNgay, DateTime ngayThongBao, Currency currency)
        {
            List<AccountingObject> lstAccountingObjects = IAccountingObjectService.GetAccountingObjects(0);
            List<Account> lstAccounts = IAccountService.GetByDetailType(1);
            List<ExceptVoucher> lstExceptVouchers = IExceptVoucherService.GetAll();
            List<MBDepositDetailCustomer> lstMbDepositDetailCustomers = _iMBDepositDetailCustomerService.GetAll();
            List<MCReceiptDetailCustomer> lstMcReceiptDetailCustomers = _iMCReceiptDetailCustomerService.GetAll();
            List<SAReturnDetailCustomer> lstSaReturnDetailCustomers = _iSAReturnDetailCustomerService.GetAll();
            List<PaymentClause> lstPaymentClauses = IPaymentClauseService.GetIsActive(true);
            List<SAInvoice> lstSaInvoices =
                Query.Where(c => (c.TypeID == 320 || c.TypeID == 323) && c.PostedDate <= dtDenNgay && c.Recorded).ToList()
                    .Where(c => lstAccountingObjects.Any(d => d.ID == c.AccountingObjectID)).ToList();
            List<ListLibitiesReport> lst = (from d in lstSaInvoices
                                            from b in d.SAInvoiceDetails
                                            where lstAccounts.Any(c => c.AccountNumber == b.DebitAccount) &&
                                                lstAccountingObjects.Any(c => c.ID == b.AccountingObjectID) && (d.CurrencyID == currency.ID || currency.ID == "<<Tất cả>>")
                                            select new ListLibitiesReport
                                            {
                                                Status = false,
                                                ID = d.AccountingObjectID ?? Guid.Empty,
                                                NgayThongBao = ngayThongBao,
                                                CongNoDenNgay = dtDenNgay.ToString("dd/MM/yyyy"),
                                                CongNoTuNgay = d.DueDate ?? null,
                                                MaKhanhHang = lstAccountingObjects.Any(c => c.ID == d.AccountingObjectID) ? lstAccountingObjects.Single(c => c.ID == d.AccountingObjectID).AccountingObjectCode : "",
                                                TenKhachHang = d.AccountingObjectName,
                                                Address = d.AccountingObjectAddress,
                                                TaxCode = d.CompanyTaxCode,
                                                NoPhaiTra = (b.Amount -
                                                      lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount) -
                                                      lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => c.Amount) -
                                                      lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount)
                                                      - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => c.Amount)),
                                                LaiNoPhaiTra = (decimal)
                                                         ((d.DueDate != null && d.PaymentClauseID != null && d.PaymentClauseID != Guid.Empty)
                                                             ? ((b.Amount -
                                                                 lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount) -
                                                                 lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => c.Amount) -
                                                                 lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount)
                                                                 - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => c.Amount)) *
                                                                (dtDenNgay - d.DueDate.Value).Days * (lstPaymentClauses.Count(c => c.ID == d.PaymentClauseID) == 1 ? lstPaymentClauses.Single(c => c.ID == d.PaymentClauseID).OverdueInterestPercent : 0) / 100)
                                                             : 0),
                                                TongTien = (b.Amount -
                                                      lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount) -
                                                      lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => c.Amount) -
                                                      lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount)
                                                      - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => c.Amount)) +
                                                      (decimal)(d.DueDate != null ? ((b.Amount -
                                                                 lstMbDepositDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount) -
                                                                 lstSaReturnDetailCustomers.Where(c => c.SAInvoiceID == d.ID).Sum(c => c.Amount) -
                                                                 lstMcReceiptDetailCustomers.Where(c => c.SaleInvoiceID == d.ID).Sum(c => c.Amount)
                                                                 - lstExceptVouchers.Where(c => c.GLVoucherID == d.ID).Sum(c => c.Amount)) *
                                                                (dtDenNgay - d.DueDate.Value).Days * (lstPaymentClauses.Count(c => c.ID == d.PaymentClauseID) == 1 ? lstPaymentClauses.Single(c => c.ID == d.PaymentClauseID).OverdueInterestPercent : 0) / 100)
                                                             : 0),
                                            }).ToList();

            return (from d in lst
                    group d by
                        new { d.Status, d.NgayThongBao, d.CongNoDenNgay, d.MaKhanhHang, d.TenKhachHang, d.ID }
                        into g
                    select new ListLibitiesReport
                    {
                        Status = g.Key.Status,
                        ID = g.Key.ID,
                        NgayThongBao = g.Key.NgayThongBao,
                        CongNoDenNgay = dtDenNgay.ToString("dd/MM/yyyy"),
                        CongNoTuNgay = g.FirstOrDefault(c => c.MaKhanhHang == g.Key.MaKhanhHang).CongNoTuNgay,
                        Address = g.FirstOrDefault(c => c.MaKhanhHang == g.Key.MaKhanhHang).Address,
                        TaxCode = g.FirstOrDefault(c => c.MaKhanhHang == g.Key.MaKhanhHang).TaxCode,
                        MaKhanhHang = g.Key.MaKhanhHang,
                        TenKhachHang = g.Key.TenKhachHang,
                        NoPhaiTra = g.Sum(c => c.NoPhaiTra),
                        LaiNoPhaiTra = g.Sum(c => c.LaiNoPhaiTra),
                        TongTien = g.Sum(c => c.TongTien)
                    }).ToList();
        }

        public int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            var lst = Query.Where(n => n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null).ToList();
            if (lst.Count > 0) return lst.ToList().Select(n => int.Parse(n.InvoiceNo)).Max();
            else return 0;
        }

        public List<SAInvoice> GetListSAInVoiceTT153(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            return Query.Where(n => n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != null && n.InvoiceNo != "").ToList();
        }
        public decimal DoanhSo(Guid ID, DateTime fromDate, DateTime toDate)
        {
            List<SAInvoiceDetail> lst = _ISAInvoiceDetailService.Query.Where(b => b.CreditAccount.StartsWith("511") && b.CostSetID == ID && Query.Any(c => b.SAInvoiceID == c.ID && c.Recorded && c.PostedDate >= fromDate && c.PostedDate <= toDate)).ToList();
            return (lst.Sum(c => c.Amount) - lst.Sum(c => c.DiscountAmount) + lst.Sum(c => c.VATAmount));
        }
        public decimal DoanhSoHD(Guid ID, DateTime fromDate, DateTime toDate)
        {
            List<SAInvoiceDetail> lst = _ISAInvoiceDetailService.Query.Where(b => b.CreditAccount.StartsWith("511") && b.ContractID == ID && Query.Any(c => b.SAInvoiceID == c.ID && c.Recorded && c.PostedDate >= fromDate && c.PostedDate <= toDate)).ToList();
            return (lst.Sum(c => c.Amount) - lst.Sum(c => c.DiscountAmount) + lst.Sum(c => c.VATAmount));
        }
        public List<SAInvoiceDetail> DoanhSoHD(List<Guid> ID, DateTime fromDate, DateTime toDate)
        {
            List<SAInvoiceDetail> lst = _ISAInvoiceDetailService.Query.Where(b => b.CreditAccount.StartsWith("511") && ID.Contains(b.ContractID ?? Guid.Empty) && Query.Any(c => b.SAInvoiceID == c.ID && c.Recorded && c.PostedDate >= fromDate && c.PostedDate <= toDate)).ToList();
            return (from a in lst
                    group a by new { a.ContractID } into t
                    select new SAInvoiceDetail
                    {
                        ContractID = t.Key.ContractID,
                        Amount = t.Sum(x => x.Amount),
                        VATAmount = t.Sum(x => x.VATAmount),
                        DiscountAmount = t.Sum(x => x.DiscountAmount),
                    }).ToList();
        }
        public List<SAInvoiceDetail> DoanhSoCTDH(List<Guid> ID, DateTime fromDate, DateTime toDate)
        {
            List<SAInvoiceDetail> lst = _ISAInvoiceDetailService.Query.Where(b => b.CreditAccount.StartsWith("511") && ID.Contains(b.CostSetID ?? Guid.Empty) && Query.Any(c => b.SAInvoiceID == c.ID && c.Recorded && c.PostedDate >= fromDate && c.PostedDate <= toDate)).ToList();
            return (from a in lst
                    group a by new { a.CostSetID } into t
                    select new SAInvoiceDetail
                    {
                        CostSetID = t.Key.CostSetID,
                        Amount = t.Sum(x => x.Amount),
                        VATAmount = t.Sum(x => x.VATAmount),
                        DiscountAmount = t.Sum(x => x.DiscountAmount),
                    }).ToList();
        }

        public List<SAInvoice> GetListSAInVoiceTT153inPeriod(DateTime From, DateTime To, Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate)
        {
            return Query.Where(n => n.Date <= To && n.Date >= From && n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceNo != null && n.InvoiceNo != "").ToList();
        }
    }

}