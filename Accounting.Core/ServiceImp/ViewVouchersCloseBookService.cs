﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class ViewVouchersCloseBookService : BaseService<ViewVouchersCloseBook, Guid>, IViewVouchersCloseBookService
    {
        public ViewVouchersCloseBookService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<ViewVouchersCloseBook> GetAllVoucherUnRecorded(DateTime newDBDateClosed, Guid? branchId = null)
        {
            if (branchId.HasValue)
                return Query.Where(v => v.BranchID == branchId.Value && v.PostedDate <= newDBDateClosed && v.Recorded == false).ToList();
            return Query.Where(v => v.PostedDate <= newDBDateClosed && v.Recorded == false).ToList();
        }

        public List<ViewVouchersCloseBook> GetAllVoucherAccountingObject()
        {
            return Query.Where(o => o.AccountingObjectID != null || o.EmployeeID != null).OrderBy(p => p.No).ToList();
        }

        public ViewVouchersCloseBook GetByNo(string No)
        {
            return Query.FirstOrDefault(o => o.No == No);
        }

        public bool CheckNoExist(string No)
        {
            return Query.Any(o => o.No == No);
        }
    }
}
