using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153DestructionInvoiceService: BaseService<TT153DestructionInvoice ,Guid>,ITT153DestructionInvoiceService
    {
        public TT153DestructionInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<TT153DestructionInvoiceDetail> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportID)
        {
            List<TT153DestructionInvoice> lst = Query.Where(n => n.Date <= To && n.Date >= From).ToList();
            List<TT153DestructionInvoiceDetail> lstDetail = new List<TT153DestructionInvoiceDetail>();
            foreach (TT153DestructionInvoice item in lst)
            {
                foreach (TT153DestructionInvoiceDetail itemDetail in item.TT153DestructionInvoiceDetails)
                {
                    if (itemDetail.TT153ReportID == TT153ReportID)
                    {
                        lstDetail.Add(itemDetail);
                        break;
                    }
                }
            }
            return lstDetail;
        }

        public List<TT153DestructionInvoiceDetail> GetAllToDate(DateTime To, Guid TT153ReportID)
        {
            List<TT153DestructionInvoice> lst = Query.Where(n => n.Date < To).ToList();
            List<TT153DestructionInvoiceDetail> lstDetail = new List<TT153DestructionInvoiceDetail>();
            foreach (TT153DestructionInvoice item in lst)
            {
                foreach (TT153DestructionInvoiceDetail itemDetail in item.TT153DestructionInvoiceDetails)
                {
                    if (itemDetail.TT153ReportID == TT153ReportID)
                    {
                        lstDetail.Add(itemDetail);
                        break;
                    }
                }
            }
            return lstDetail;
        }
    }

}