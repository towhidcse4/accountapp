﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class TransportMethodService : BaseService<TransportMethod, Guid>, ITransportMethodService
    {
        public TransportMethodService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// Lấy danh sách Phương thức vẩn chuyển [khanhtq]
        /// </summary>
        /// <returns>Danh sách phương thức vận chuyển sắp xếp và sắp xếp theo mã </returns>
        public List<TransportMethod> OrderByCode()
        {
            return Query.OrderBy(c=>c.TransportMethodCode).ToList();
        }
        /// <summary>
        /// Danh sách mã phương thức vận chuyển
        /// </summary>
        /// <returns>Danh sách mã phương thức vận chuyển</returns>
        public List<string> GetCode()
        {
            return Query.Select(c => c.TransportMethodCode).ToList();
        }
        /// <summary>
        /// lấy danh sách phương thức vận chuyển
        /// </summary>
        /// <returns></returns>
        public List<TransportMethod> GetActive()
        {
            return Query.Where(o => o.IsActive == true).OrderBy(c => c.TransportMethodName).ToList();
        }
    }

}