﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class AccountingObjectGroupService : BaseService<AccountingObjectGroup, Guid>, IAccountingObjectGroupService
    {
        public AccountingObjectGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.List<AccountingObjectGroup> GetListByIsActiveOrderCode(bool IsActive)
        {
            return Query.OrderBy(p => p.AccountingObjectGroupCode).Where(p => p.IsActive == IsActive).ToList();
        }

        /// <summary>
        /// Lấy danh sách các AccountingObjectGroup theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        public List<AccountingObjectGroup> GetAll_ByParentID(Guid ParentID)
        {
            return Query.Where(p => p.ParentID == ParentID).ToList();
        }

        /// <summary>
        /// Lấy tổng số dòng theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID"> là ParentID truyền vào</param>
        /// <returns>Tổng số dòng chứa cùng một ParentID, null nếu bị lỗi</returns>
        public int CountByParentID(Guid? ParentID)
        {
            return Query.Count(p => p.ParentID == ParentID);
        }

        /// <summary>
        /// Lấy danh sách AccountingObjectGroup sắp xếp tăng dần theo AccountingObjectGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        public List<AccountingObjectGroup> GetAll_OrderByAccountingObjectGroupCode()
        {
            return Query.OrderBy(c => c.AccountingObjectGroupCode).ToList();
        }

        /// <summary>
        /// Lấy danh sách AccountingObjectGroup sắp xếp tăng dần theo AccountingObjectGroupName
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        public List<AccountingObjectGroup> GetAll_OrderByAccountingObjectGroupName()
        {
            return Query.OrderBy(c => c.AccountingObjectGroupName).ToList();
        }

        /// <summary>
        /// Lấy danh sách các OrderFixCode theo ParentID
        /// [DUYTN] 
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các OrderFixCode, null nếu bị lỗi</returns>
        public List<string> GetOrderFixCode_ByParentID(Guid? ParentID)
        {
            return Query.Where(a => a.ParentID == ParentID).Select(a => a.OrderFixCode).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách AccountingObjectGroup (các con cuối cùng) theo OrderFixCode
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode">là OrderFixCode vị trí của một đối tượng AccountingObjectGroup </param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        public List<AccountingObjectGroup> GetStartsWith_OrderFixCode(string OrderFixCode)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(OrderFixCode)).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách AccountingObjectGroup (các con cuối cùng) theo OrderFixCode và có giá trị khác ID
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode">là OrderFixCode vị trí của một đối tượng AccountingObjectGroup </param>
        /// <param name="ID">là ID của một đối tượng AccountingObjectGroup </param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        public List<AccountingObjectGroup> GetStartsWith_OrderFixCode_NotValueByID(string OrderFixCode, Guid ID)
        {
            return GetStartsWith_OrderFixCode(OrderFixCode).Where(p => p.ID != ID).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách các AccountingObjectGroup theo Grade
        /// [DUYTN]
        /// </summary>
        /// <param name="Grade">là Grade tham số kiểu int truyền vào</param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        public List<AccountingObjectGroup> GetAll_ByGrade(int Grade)
        {
            return Query.Where(a => a.Grade == Grade).ToList();
        }

        /// <summary>
        /// Lấy ra danh sách các AccountingObjectGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectGroupCode, null nếu bị lỗi</returns>
        public List<string> GetAccountingObjectGroupCode()
        {
            return Query.Select(c => c.AccountingObjectGroupCode).ToList();
        }
        public Guid? GetGuidAccountingObjectGroupByCode(string code)
        {
            Guid? id = null;
            AccountingObjectGroup ag = Query.Where(p => p.AccountingObjectGroupCode == code).SingleOrDefault();
            if (ag != null)
            {
                id = ag.ID;
            }
            return id;
        }
    }

}