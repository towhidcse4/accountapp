using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBCreditCardService: BaseService<MBCreditCard ,Guid>,IMBCreditCardService
    {
        public MBCreditCardService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public MBCreditCard GetByNo(string no)
        {
            try
            {
                return Query.FirstOrDefault(m => m.No == no);
            }
            catch
            {
                return null;
            }
        }
    }

}