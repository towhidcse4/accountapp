﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class ViewVoucherInvisibleService : BaseService<ViewVoucherInvisible, Guid>, IViewVoucherInvisibleService
    {
        public ViewVoucherInvisibleService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public ITypeGroupService _ITypeGroupService { get { return IoC.Resolve<ITypeGroupService>(); } }
        public List<ViewVoucherInvisible> GetAllVoucherUnRecorded(DateTime newDBDateClosed, Guid? branchId = null)
        {
            if (branchId.HasValue)
                return Query.Where(v => v.BranchID == branchId.Value && v.PostedDate <= newDBDateClosed && v.Recorded == false).ToList();
            return Query.Where(v => v.PostedDate <= newDBDateClosed && v.Recorded == false).ToList();
        }

        public ViewVoucherInvisible GetByNo(string No)
        {
            return Query.FirstOrDefault(o => o.No == No);
        }
        public ViewVoucherInvisible GetByRefID(Guid refID)
        {
            return Query.FirstOrDefault(o => o.RefID == refID);
        }

        public bool CheckNoExist(string No)
        {
            return Query.Any(o => o.No == No);
        }

        public List<ViewVoucherInvisible> SearchVoucherNo(string partNo, DateTime startDate, DateTime endDate, Guid? branchID = null)
        {
            var query = Query;

            if (!string.IsNullOrEmpty(partNo))
                query = query.Where(o => o.No.Contains(partNo));

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                query = query.Where(o => o.Date >= startDate);
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                query = query.Where(o => o.Date <= endDate);
            }

            return query.OrderBy(o => o.No).ToList();
        }

        public List<ViewVoucherReset> GetPureVoucher(int TypeGroupID, DateTime startDate, DateTime endDate)
        {
            var query = Query;

            // lấy chứng từ theo TypeGroupID và tên bảng tránh trường hợp trùng số nhưng khác bảng
            if (TypeGroupID == 10)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "MCReceipt");
            if (TypeGroupID == 11)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "MCPayment");
            if (TypeGroupID == 12 || TypeGroupID == 13)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "MBTellerPaper");
            if (TypeGroupID == 16)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "MBDeposit");
            if (TypeGroupID == 17)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "MBCreditCard");
            if (TypeGroupID == 21)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "PPInvoice");
            if (TypeGroupID == 40 || TypeGroupID == 41)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "RSInwardOutward");
            if (TypeGroupID == 50)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "FAIncrement");
            if (TypeGroupID == 430)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "TIIncrement");
            else
                query = query.Where(o => o.TypeGroupID == TypeGroupID);

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                query = query.Where(o => o.Date >= startDate);
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                query = query.Where(o => o.Date <= endDate);
            }

            var group = query.OrderBy(o => o.Date).ThenBy(o => o.No)
                .GroupBy(
                p => new
                {
                    p.RefID,
                    p.No,
                    p.Date,
                    p.PostedDate,
                    p.TypeGroupID,
                    p.Recorded
                })
                .Select(
                o => new ViewVoucherReset
                {
                    ID = o.Key.RefID,
                    No = o.Key.No,
                    Date = o.Key.Date,
                    PostedDate = o.Key.PostedDate,
                    TypeGroupID = o.Key.TypeGroupID,
                    Recorded = o.Key.Recorded,
                    NoCurrentValue = Utils.GetNoValue(o.Key.No),
                    Checked = true
                });

            return group.ToList();
        }
        public List<RefVoucherRSInwardOutward> GetVoucherFromView(int TypeGroupID, DateTime startDate, DateTime endDate)
        {
            var query = Query;

            if (TypeGroupID == 11)
                query = query.Where(o => (o.TypeGroupID == TypeGroupID || o.TypeID == 119 || o.TypeID == 902) && o.RefTable == "MCPayment");
            else if (TypeGroupID == 43)
                query = query.Where(o => o.TypeGroupID == 32);
            else if (TypeGroupID == 12)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "MBTellerPaper");
            else if (TypeGroupID == 13)
                query = query.Where(o => (o.TypeGroupID == 13 || o.TypeGroupID == 14) && o.RefTable == "MBTellerPaper");
            else if (TypeGroupID == 21)
                query = query.Where(o => (o.TypeGroupID == 21 || o.TypeGroupID == 11 || o.TypeGroupID == 12 || o.TypeGroupID == 13 || o.TypeGroupID == 14 || o.TypeGroupID == 17) && o.RefTable == "PPInvoice");
            else if (TypeGroupID == 22)
                query = query.Where(o => (o.TypeGroupID == 22 || o.TypeGroupID == 23) && o.RefTable == "PPDiscountReturn");
            else if (TypeGroupID == 24)
                query = query.Where(o => (o.TypeGroupID == 24 || o.TypeGroupID == 11 || o.TypeGroupID == 12 || o.TypeGroupID == 13 || o.TypeGroupID == 14 || o.TypeGroupID == 17) && o.RefTable == "PPService");
            else if (TypeGroupID == 32 || TypeGroupID == 323)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "SAInvoice");
            else if (TypeGroupID == 33)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "SAReturn");
            else if (TypeGroupID == 90)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "RSAssemblyDismantlement_new");
            else if (TypeGroupID == 430)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "TIIncrement");
            else if (TypeGroupID == 50)
                query = query.Where(o => o.TypeGroupID == TypeGroupID && o.RefTable == "FAIncrement");
            else
                query = query.Where(o => o.TypeGroupID == TypeGroupID);

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                query = query.Where(o => o.PostedDate >= startDate);
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                query = query.Where(o => o.PostedDate <= endDate);
            }

            var group = query.Where(n => n.PostedDate != null).OrderByDescending(o => (o.PostedDate ?? DateTime.Now).Date).ThenByDescending(o => o.No)
                .Select(
                o => new RefVoucherRSInwardOutward
                {
                    RefID2 = o.RefID,
                    No = o.No,
                    Date = (o.Date ?? DateTime.Now).Date,
                    PostedDate = (o.PostedDate ?? DateTime.Now).Date,
                    Reason = o.Reason,
                    TypeVoucher = o.TypeGroupID,
                    TypeID = o.TypeID
                });

            return group.ToList();
        }
        public List<RefVoucherRSInwardOutward> GetVoucher(int TypeGroupID, DateTime startDate, DateTime endDate)
        {
            var query = Query;

            query = query.Where(o => o.TypeGroupID == TypeGroupID);

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                query = query.Where(o => o.PostedDate >= startDate);
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                query = query.Where(o => o.PostedDate <= endDate);
            }

            var group = query.Where(n => n.PostedDate != null).OrderByDescending(o => (o.PostedDate ?? DateTime.Now).Date).ThenByDescending(o => o.No)
                .Select(
                o => new RefVoucherRSInwardOutward
                {
                    RefID2 = o.RefID,
                    No = o.No,
                    Date = (o.Date ?? DateTime.Now).Date,
                    PostedDate = (o.PostedDate ?? DateTime.Now).Date,
                    Reason = o.Reason,
                    TypeVoucher = o.TypeGroupID,
                    TypeID = o.TypeID
                });

            return group.ToList();
        }
        public List<RefVoucher> GetVoucherTest(int TypeGroupID, DateTime startDate, DateTime endDate)
        {
            var query = Query;

            query = query.Where(o => o.TypeGroupID == TypeGroupID);

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                query = query.Where(o => o.PostedDate >= startDate);
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                query = query.Where(o => o.PostedDate <= endDate);
            }

            var group = query.Where(n => n.PostedDate != null).OrderByDescending(o => (o.PostedDate ?? DateTime.Now).Date).ThenByDescending(o => o.No)
                .Select(
                o => new RefVoucher
                {
                    RefID2 = o.RefID,
                    No = o.No,
                    Date = (o.Date ?? DateTime.Now).Date,
                    PostedDate = (o.PostedDate ?? DateTime.Now).Date,
                    Reason = o.Reason,
                    TypeVoucher = o.TypeGroupID,
                    TypeID = o.TypeID
                });

            return group.ToList();
        }

        public List<ViewVoucherReset> GetVoucherInList(List<string> listNo, DateTime startDate, DateTime endDate)
        {
            var query = Query;

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                //query = query.w(o => !(o.PostedDate >= startDate));
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                //query = query.Where(o => !(o.PostedDate <= endDate));
            }

            query = query.Where(o => listNo.Contains(o.No) && (o.PostedDate < startDate || o.PostedDate > endDate));

            var group = query.OrderBy(o => o.PostedDate).ThenBy(o => o.No)
                .GroupBy(
                p => new
                {
                    p.RefID,
                    p.No,
                    p.Date,
                    p.PostedDate,
                    p.TypeGroupID,
                    p.Recorded
                })
                .Select(
                o => new ViewVoucherReset
                {
                    ID = o.Key.RefID,
                    No = o.Key.No,
                    Date = o.Key.Date,
                    PostedDate = o.Key.PostedDate,
                    TypeGroupID = o.Key.TypeGroupID,
                    Recorded = o.Key.Recorded,
                    NoCurrentValue = Utils.GetNoValue(o.Key.No),
                    Checked = true
                });

            return group.ToList();
        }

        public List<ViewVoucherInvisible> GetVoucherInvisible(int? TypeGroupID, DateTime startDate, DateTime endDate)
        {
            var query = Query;

            if (TypeGroupID != null)
            {
                query = query.Where(o => o.TypeGroupID == TypeGroupID);
            }

            if (startDate != null)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                startDate = startDate.Date + ts;
                query = query.Where(o => o.Date >= startDate);
            }

            if (endDate != null)
            {
                TimeSpan ts = new TimeSpan(23, 59, 59);
                endDate = endDate.Date + ts;
                query = query.Where(o => o.Date <= endDate);
            }

            return query.OrderBy(o => o.Date).ThenBy(o => o.No).ToList();
        }
    }
}
