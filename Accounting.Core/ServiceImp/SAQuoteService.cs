using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class SAQuoteService: BaseService<SAQuote, Guid>,ISAQuoteService
    {
        public SAQuoteService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<SAQuote> OrderByCode()
        {
            return Query.OrderByDescending(p => p.No).ToList();
        }
    }

}