using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM02TAINService: BaseService<TM02TAIN ,Guid>,ITM02TAINService
    {
        public TM02TAINService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}