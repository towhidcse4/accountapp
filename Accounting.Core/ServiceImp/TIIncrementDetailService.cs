﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    class TIIncrementDetailService : BaseService<TIIncrementDetail, Guid>, ITIIncrementDetailService
    {
       public TIIncrementDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
       { }

        public IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public ITIInitService _ITIInitService { get { return IoC.Resolve<ITIInitService>(); } }
        public ITIIncrementService _ITIIncrementService { get { return IoC.Resolve<ITIIncrementService>(); } }

        public bool CheckDeleteTools(Guid iD)
        {
            return _ITIInitService.Query.Any(fai => fai.MaterialGoodsID == iD);
        }

        public bool CheckQuantity(Guid? toolsID, decimal quantity)
        {
            decimal totalQuantity = _ITIInitService.Query.Where(mg => mg.ID == toolsID).FirstOrDefault().Quantity ?? 0;
            var recordedQuantity = Query.Where(tiid => tiid.ToolsID == toolsID && _ITIIncrementService.Query.Any(tii => tii.ID == tiid.TIIncrementID && tii.Recorded)).ToList();
                          //.Sum(tiid => tiid.Quantity);
            return recordedQuantity.Sum(a => a.Quantity) + quantity > totalQuantity;
        }

        public TIIncrementDetail FindByMaterialGoodsID(Guid iD)
        {
            return Query.Where(p => p.ToolsID == iD).FirstOrDefault();
        }

        public decimal GetNotRecoredQuantity(Guid? toolsID)
        {
            decimal? totalQuantity = _ITIInitService.Query.Where(mg => mg.ID == toolsID).FirstOrDefault().Quantity;
            var recordedQuantity = Query.Where(tiid => tiid.ToolsID == toolsID && _ITIIncrementService.Query.Any(tii => tii.ID == tiid.TIIncrementID && tii.Recorded)).ToList();
            //.Sum(tiid => tiid.Quantity);
            return (totalQuantity ?? 0) - recordedQuantity.Sum(a => a.Quantity);
        }
    }
}
