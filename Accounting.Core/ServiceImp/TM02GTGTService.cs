using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM02GTGTService: BaseService<TM02GTGT ,Guid>,ITM02GTGTService
    {
        public TM02GTGTService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}