using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153ReportService : BaseService<TT153Report, Guid>, ITT153ReportService
    {
        public TT153ReportService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
        ISABillService _ISABillService = IoC.Resolve<ISABillService>();
        ISAReturnService _ISAReturnService = IoC.Resolve<ISAReturnService>();
        IPPDiscountReturnService _IPPDiscountReturnService = IoC.Resolve<IPPDiscountReturnService>();
        public TT153Report GetbySAInvoiceID(Guid SAInvoiceID)
        {
            SAInvoice sAInvoice = _ISAInvoiceService.Getbykey(SAInvoiceID);
            if (sAInvoice == null)
            {
                SABill sABill = _ISABillService.Getbykey(SAInvoiceID);
                if (sABill == null)
                {
                    SAReturn sAReturn = _ISAReturnService.Getbykey(SAInvoiceID);
                    if (sAReturn == null)
                    {
                        PPDiscountReturn pPDiscountReturn = _IPPDiscountReturnService.Getbykey(SAInvoiceID);
                        return Query.Where(n => n.InvoiceForm == pPDiscountReturn.InvoiceForm && n.InvoiceTemplate == pPDiscountReturn.InvoiceTemplate && n.InvoiceTypeID == pPDiscountReturn.InvoiceTypeID).FirstOrDefault();
                    }
                    else
                    {
                        return Query.Where(n => n.InvoiceForm == sAReturn.InvoiceForm && n.InvoiceTemplate == sAReturn.InvoiceTemplate && n.InvoiceTypeID == sAReturn.InvoiceTypeID).FirstOrDefault();
                    }
                }
                else
                {
                    return Query.Where(n => n.InvoiceForm == sABill.InvoiceForm && n.InvoiceTemplate == sABill.InvoiceTemplate && n.InvoiceTypeID == sABill.InvoiceTypeID).FirstOrDefault();
                }
            }
            else
            {
                return Query.Where(n => n.InvoiceForm == sAInvoice.InvoiceForm && n.InvoiceTemplate == sAInvoice.InvoiceTemplate && n.InvoiceTypeID == sAInvoice.InvoiceTypeID).FirstOrDefault();
            }
        }
    }

}