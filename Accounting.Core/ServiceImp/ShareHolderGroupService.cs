﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class ShareHolderGroupService : BaseService<ShareHolderGroup, Guid>, IShareHolderGroupService
    {
        public ShareHolderGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// Lấy ra danh sách tất cả các ShareHolderGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>Danh sách ShareHolderGroupCode dạng chuỗi, null nếu bị lỗi</returns>
        public List<string> GetShareHolderGroupCode()
        {
            return Query.Select(Duy => Duy.ShareHolderGroupCode).ToList();
        }
    }

}