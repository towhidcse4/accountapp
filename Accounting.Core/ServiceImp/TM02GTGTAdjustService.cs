using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM02GTGTAdjustService: BaseService<TM02GTGTAdjust ,Guid>,ITM02GTGTAdjustService
    {
        public TM02GTGTAdjustService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}