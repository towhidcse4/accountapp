﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class EMAllocationRequiredService : BaseService<EMAllocationRequired, Guid>, IEMAllocationRequiredService
    {
        public EMAllocationRequiredService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IBudgetItemService _IBudgetItemService { get { return IoC.Resolve<IBudgetItemService>(); } }
        public IEMEstimateService _IEMEstimateService { get { return IoC.Resolve<IEMEstimateService>(); } }
        public IEMAllocationService _IEMAllocationService { get { return IoC.Resolve<IEMAllocationService>(); } }
        public IEMAllocationRequiredDetailService _IEMAllocationRequiredDetailService { get { return IoC.Resolve<IEMAllocationRequiredDetailService>(); } }
        public IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        #region Tình hình cấp phát và sử dụng ngân sách DungNA
        /// <summary>
        /// [DungNA]
        /// Báo Cáo tình hình cấp phát và sử dụng ngân sách 
        /// </summary>
        /// <param name="Months">Tháng Đầu Vào</param>
        /// <param name="Year">Năm Đầu vào</param>
        /// <param name="BudgetItemID">Mã mục Thu CHi</param>
        /// <returns></returns>
        public List<BUAllocationAndUse> GetRPAandB(List<int> Months, int Year, Guid BudgetItemID)
        {
            var RPA = (from a in _IEMEstimateService.Query.ToList() // Bảng Dự toán
                       join b in _IBudgetItemService.Query.ToList() on a.BudgetItemID equals b.ID //Bảng Thu/Chi
                       join c in _IEMAllocationRequiredDetailService.Query.ToList() on b.ID equals c.BudgetItemID // Bảng Chi tiết yêu cầu cấp chi phí
                       join d in _IEMAllocationService.Query.ToList() on b.ID equals d.EMAllocationRequiredID  // Bảng Yêu cầu cấp chi phí                     
                       where
                       b.BudgetItemType == 0 //Xác định đâu là loại thu và chi trong bảng BudgetItem
                       && a.Type == 0  //Xác định đâu là dự toán thu và chi  trong bảng   EMEstimate                    
                       && d.BudgetYear == Year //năm đầu vào từ bảng    EMAllocation                
                       && a.EstimateBudgetYear == Year  //Năm dự toán so sánh với   năm đầu vào trong bảng dự toán    IEMEstimateService            
                       //Vẫn cần thêm điều kiện
                       select new BUAllocationAndUse
                       {
                           IsParent = b.IsParentNode,// xác định cha = true false
                           BudgetItemName = b.BudgetItemName,//Mục Chi
                           AllowcationAmount = c.RequestAmount,//Số yêu cầu
                           UsedAmount = c.AprovedAmount,//Số đượ duyệt
                           Amount = c.Amount,//Số đã cấp phát
                           ApprovedAmount = Convert.ToDecimal((from f in _IGeneralLedgerService.Query.ToList()
                                                               where Months.Contains(f.PostedDate.Month) && f.PostedDate.Year == Year
                                                               && (f.Account.StartsWith("111") || f.Account.StartsWith("112"))
                                                               && f.BudgetItemID == b.ID && f.BudgetItemID != null && f.PostedDate != null
                                                               select new
                                                               {
                                                                   a = f.CreditAmount - f.DebitAmount
                                                               }).FirstOrDefault()),
                           PlanAmount = 0,//Để mặc định giá trị dự toán là 0
                           Thang1 = a.AmountMonth1,
                           Thang2 = a.AmountMonth2,
                           Thang3 = a.AmountMonth3,
                           Thang4 = a.AmountMonth4,
                           Thang5 = a.AmountMonth5,
                           Thang6 = a.AmountMonth6,
                           Thang7 = a.AmountMonth7,
                           Thang8 = a.AmountMonth8,
                           Thang9 = a.AmountMonth9,
                           Thang10 = a.AmountMonth10,
                           Thang11 = a.AmountMonth11,
                           Thang12 = a.AmountMonth12,
                       }).ToList();
            foreach (var x in RPA)
            {
                if (Months.Contains(1))
                    x.PlanAmount += x.Thang1;
                if (Months.Contains(2))
                    x.PlanAmount += x.Thang2;
                if (Months.Contains(3))
                    x.PlanAmount += x.Thang3;
                if (Months.Contains(4))
                    x.PlanAmount += x.Thang4;
                if (Months.Contains(5))
                    x.PlanAmount += x.Thang5;
                if (Months.Contains(6))
                    x.PlanAmount += x.Thang6;
                if (Months.Contains(7))
                    x.PlanAmount += x.Thang7;
                if (Months.Contains(8))
                    x.PlanAmount += x.Thang8;
                if (Months.Contains(9))
                    x.PlanAmount += x.Thang9;
                if (Months.Contains(10))
                    x.PlanAmount += x.Thang10;
                if (Months.Contains(11))
                    x.PlanAmount += x.Thang11;
                if (Months.Contains(12))
                    x.PlanAmount += x.Thang12;
                x.UnUsedAmount = (x.Amount - x.ApprovedAmount); //Số chưa chia
                x.LeftOverPlanAmount = (x.PlanAmount - x.ApprovedAmount); //Số còn lại so với dự toán
                x.Percentage = (x.ApprovedAmount / x.PlanAmount);//Tỷ Lệ %

            }
            return RPA;
        }
        #endregion
    }
}
