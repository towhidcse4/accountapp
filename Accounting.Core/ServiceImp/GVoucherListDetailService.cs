﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;
using Accounting.Core.Domain.obj.Report;

namespace Accounting.Core.ServiceImp
{
    public class GVoucherListDetailService : BaseService<GVoucherListDetail, Guid>, IGVoucherListDetailService
    {
        public GVoucherListDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IGVoucherListService IGVoucherListService { get { return IoC.Resolve<IGVoucherListService>(); } }
        public IAccountService IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        public List<GVoucherListDetail> GetByGvoucherListId(Guid gVoucherListId)
        {
            try
            {
                return Query.Where(t => t.GVoucherListID == gVoucherListId).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<S02C1DNN> GetS02C1DNN(DateTime bgdt, DateTime endt, List<string> lstAcc, bool chk)
        {
            DateTime enddt = new DateTime(endt.Year, endt.Month, endt.Day, 23, 59, 59);            
            try
            {               
                List<GVoucherList> gVoucherList = IGVoucherListService.Query.Where(x=>x.Date < enddt).ToList();
                List<Account> lstAccount = IAccountService.GetAll();
                List<GVoucherListDetail> gVoucherListDetails = gVoucherList.SelectMany(x=>x.GVoucherListDetails).Where(c=>c.VoucherCreditAccount != null && c.VoucherDebitAccount != null).ToList()
               .Where(x => lstAcc.Any(d => x.VoucherCreditAccount.StartsWith(d)) || lstAcc.Any(d => x.VoucherDebitAccount.StartsWith(d))).ToList();
                if (!chk)
                {
                    List<GVoucherListDetail> listDetails = (from a in gVoucherListDetails
                                                            join b in gVoucherList on a.GVoucherListID equals b.ID
                                                            select new GVoucherListDetail
                                                            {
                                                                ID = a.ID,
                                                                GVoucherListID = a.GVoucherListID,
                                                                Date = b.Date,
                                                                No = b.No,                     
                                                                VoucherID=b.ID,//trung nq sửa để lấy chi tiết chứng từ
                                                                VoucherTypeID=b.TypeID,
                                                                Description = a.VoucherDescription,
                                                                VoucherDebitAccount = a.VoucherDebitAccount,
                                                                VoucherCreditAccount = a.VoucherCreditAccount,
                                                                VoucherAmount = a.VoucherAmount
                                                            }).ToList();
                    List<S02C1DNN> lstS02 = new List<S02C1DNN>();
                    foreach (var acc in lstAcc)
                    {
                        var lst = (from a in listDetails
                                   where (a.VoucherCreditAccount.StartsWith(acc) || a.VoucherDebitAccount.StartsWith(acc))
                                   select new S02C1DNN
                                   {
                                       PostedDate = a.Date,
                                       RefDate = a.Date,
                                       RefNo = a.No,
                                       Hyperlink = a.VoucherID + ";" + a.VoucherTypeID,
                                       Note = a.Description,
                                       AccountNumber = acc,
                                       AccountCategoryKind = lstAccount.FirstOrDefault(x=>x.AccountNumber == acc).AccountGroupKind,
                                       DetailAccountNumber = a.VoucherCreditAccount.StartsWith(acc) ? a.VoucherDebitAccount : a.VoucherCreditAccount,
                                       DebitAmount = a.VoucherDebitAccount.StartsWith(acc) ? (decimal)a.VoucherAmount : 0,
                                       CreditAmount = a.VoucherCreditAccount.StartsWith(acc) ? (decimal)a.VoucherAmount : 0,
                                   }).ToList();
                        if (lst.Count > 0) lstS02.AddRange(lst);
                    }
                    return lstS02.OrderBy(x => x.RefDate).ToList();
                }
                else
                {
                    List<GVoucherListDetail> listDetails = (from a in gVoucherListDetails
                                                            join b in gVoucherList on a.GVoucherListID equals b.ID
                                                            select new GVoucherListDetail
                                                            {
                                                                ID = a.ID,
                                                                GVoucherListID = a.GVoucherListID,
                                                                Date = b.Date,
                                                                No = b.No,
                                                                VoucherID = b.ID,
                                                                VoucherTypeID = b.TypeID,
                                                                Description = b.Description,
                                                                VoucherDebitAccount = a.VoucherDebitAccount,
                                                                VoucherCreditAccount = a.VoucherCreditAccount,
                                                                VoucherAmount = a.VoucherAmount
                                                            }).ToList();
                    List<S02C1DNN> lstS02 = new List<S02C1DNN>();
                    foreach (var acc in lstAcc)
                    {
                        var lst = (from a in listDetails
                                   where (a.VoucherCreditAccount.StartsWith(acc) || a.VoucherDebitAccount.StartsWith(acc))
                                   group a by new { a.Description, a.Date, a.No, a.VoucherID,a.VoucherTypeID,a.VoucherCreditAccount, a.VoucherDebitAccount}
                                   into g
                                   select new S02C1DNN
                                   {
                                       PostedDate = g.Key.Date,
                                       RefDate = g.Key.Date,
                                       RefNo = g.Key.No,
                                       Hyperlink = g.Key.VoucherID + ";" + g.Key.VoucherTypeID,
                                       Note = g.Key.Description,
                                       AccountNumber = acc,
                                       AccountCategoryKind = lstAccount.FirstOrDefault(x => x.AccountNumber == acc).AccountGroupKind,
                                       DetailAccountNumber = g.Key.VoucherCreditAccount.StartsWith(acc) ? g.Key.VoucherDebitAccount : g.Key.VoucherCreditAccount,
                                       DebitAmount = g.Key.VoucherDebitAccount.StartsWith(acc) ? g.Sum(t=> (decimal)t.VoucherAmount) : 0,
                                       CreditAmount = g.Key.VoucherCreditAccount.StartsWith(acc) ? g.Sum(t=> (decimal)t.VoucherAmount) : 0,
                                   }).ToList();
                        if (lst.Count > 0) lstS02.AddRange(lst);
                    }

                    return lstS02.OrderBy(x => x.RefDate).ToList();
                }
            }
            catch(Exception ex)
            {

            }
            
            return new List<S02C1DNN>();
        }
    }

}