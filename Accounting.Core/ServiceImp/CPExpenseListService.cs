using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CPExpenseListService: BaseService<CPExpenseList ,Guid>,ICPExpenseListService
    {
        public CPExpenseListService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<CPExpenseList> GetListByTypeVoucher(int typeVoucher)
        {
            return Query.Where(x => x.TypeVoucher == typeVoucher).ToList(); 
        }
    }

}