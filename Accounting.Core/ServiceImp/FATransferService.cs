﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{

    public class FATransferService : BaseService<FATransfer, Guid>, IFATransferService
    {
        public FATransferService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public FATransfer GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
    }
}
