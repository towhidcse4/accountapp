﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.ServiceImp
{
    public class TIAdjustmentDetailService : BaseService<TIAdjustmentDetail, Guid>, ITIAdjustmentDetailService
    {
        public TIAdjustmentDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<TIAdjustmentDetail> GetByTIAdjustmentID(Guid TIAdjustmentID)
        {
            return Query.Where(k => k.TIAdjustmentID == TIAdjustmentID).ToList();
        }


    }
}
