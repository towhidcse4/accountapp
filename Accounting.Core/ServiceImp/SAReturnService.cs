using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class SAReturnService : BaseService<SAReturn, Guid>, ISAReturnService
    {
        public SAReturnService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public SAReturn GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }

        public List<SAReturn> GetListSAReturnTT153(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            return Query.Where(n => n.TypeID == 340 && n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != null && n.InvoiceNo != "").ToList();
        }

        public int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries)
        {
            var lst = Query.Where(n => n.TypeID == 340 && n.InvoiceTypeID == InvoiceTypeID && n.InvoiceForm == InvoiceForm && n.InvoiceTemplate == InvoiceTemplate && n.InvoiceSeries == InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null).ToList();
            if (lst.Count > 0) return lst.ToList().Select(n => int.Parse(n.InvoiceNo)).Max();
            else return 0;
        }
    }

}