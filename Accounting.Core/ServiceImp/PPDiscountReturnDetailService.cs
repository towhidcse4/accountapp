using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class PPDiscountReturnDetailService: BaseService<PPDiscountReturnDetail ,Guid>,IPPDiscountReturnDetailService
    {
        private static IPPDiscountReturnService _IPPDiscountReturnService
        {
            get { return IoC.Resolve<IPPDiscountReturnService>(); }
        }
        public PPDiscountReturnDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IList<PPDiscountReturnDetail> GetPPDiscountReturnDetailbyID(Guid id)
        {
            return Query.Where(k => k.PPDiscountReturnID == id).ToList();
        }
        public decimal GetPPDiscountReturnDetailQuantity(Guid materialGoodsID, Guid contractID)
        {
            List<PPDiscountReturnDetail> lstPPInvoiceDetail = Query.Where(X => X.MaterialGoodsID == materialGoodsID && X.ContractID == contractID && _IPPDiscountReturnService.Query.Any(d => d.ID == X.PPDiscountReturnID && d.Recorded)).ToList();
            decimal quantity = lstPPInvoiceDetail.Count == 0 ? 0 : (lstPPInvoiceDetail.Sum(x => x.Quantity) ?? 0);
            return (decimal)quantity;
        }
    }

}