﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{

    public class TIAllocationDetailService : BaseService<TIAllocationDetail, Guid>, ITIAllocationDetailService
    {
        public TIAllocationDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        IFixedAssetService _IFixedAssetService = FX.Core.IoC.Resolve<IFixedAssetService>();
        public IFAAdjustmentService _IFAAdjustmentService { get { return FX.Core.IoC.Resolve<IFAAdjustmentService>(); } }
        public List<TIAllocationDetail> GetTIAllocationDetails(Guid TIAllocationId)
        {
            return Query.Where(k => k.TIAllocationID == TIAllocationId).ToList();
        }
        
    }
}
