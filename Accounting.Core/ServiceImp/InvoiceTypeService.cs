﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
   public class InvoiceTypeService : BaseService<InvoiceType,Guid>, IInvoiceTypeService
    {
       public InvoiceTypeService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
       /// <summary>
       /// Lấy danh sách Mẫu số hóa đơn đang hoạt động
       /// [Longtx]
       /// </summary>
       /// <param name="isActive"></param>
       /// <returns></returns>
       public List<InvoiceType> GetIsActive(bool isActive)
       {
           return Query.Where(p => p.IsActive == isActive).OrderBy(p=>p.InvoiceTypeCode).ToList();
       }

       public List<InvoiceType> GetAll_OrderBy()
       {
           return Query.OrderBy(p => p.InvoiceTypeCode).ToList();
       }
    }
}
