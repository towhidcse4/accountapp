using System;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class FAAdjustmentService : BaseService<FAAdjustment, Guid>, IFAAdjustmentService
    {
        public FAAdjustmentService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public FAAdjustment GetByNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
    }

}