﻿using System;
using System.Linq.Dynamic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
namespace Accounting.Core.ServiceImp
{
    public class RepositoryLedgerService : BaseService<RepositoryLedger, Guid>, IRepositoryLedgerService
    {
        public RepositoryLedgerService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IRSInwardOutwardService _IRSInwardOutwardService { get { return IoC.Resolve<IRSInwardOutwardService>(); } }
        public IRSInwardOutwardDetailService _IRSInwardOutwardDetailService { get { return IoC.Resolve<IRSInwardOutwardDetailService>(); } }
        public IRSAssemblyDismantlementService _IRSAssemblyDismantlementService { get { return IoC.Resolve<IRSAssemblyDismantlementService>(); } }
        public ISAInvoiceService _ISAInvoiceService { get { return IoC.Resolve<ISAInvoiceService>(); } }
        public ISAInvoiceDetailService _ISAInvoiceDetailService { get { return IoC.Resolve<ISAInvoiceDetailService>(); } }
        public ISAReturnService _ISAReturnService { get { return IoC.Resolve<ISAReturnService>(); } }
        public ISAReturnDetailService _ISAReturnDetailService { get { return IoC.Resolve<ISAReturnDetailService>(); } }
        public IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public IRepositoryService _IRepositoryService { get { return IoC.Resolve<IRepositoryService>(); } }
        public IOPMaterialGoodsService _IOPMaterialGoodsService { get { return IoC.Resolve<IOPMaterialGoodsService>(); } }
        public IMaterialGoodsService _IMaterialGoodsService { get { return IoC.Resolve<IMaterialGoodsService>(); } }
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public IRSTransferService _IRSTranferService { get { return IoC.Resolve<IRSTransferService>(); } }
        public IRSTransferDetailService _IRSTransferDetailService { get { return IoC.Resolve<IRSTransferDetailService>(); } }
        public IPPDiscountReturnDetailService _IPPDiscountReturnDetailService { get { return IoC.Resolve<IPPDiscountReturnDetailService>(); } }
        public IPPDiscountReturnService _IPPDiscountReturnService { get { return IoC.Resolve<IPPDiscountReturnService>(); } }
        /// <summary>
        /// Lấy danh sách ReferenceID từ RepositoryLedger
        /// </summary>
        /// <param name="selectID"></param>
        /// <returns>tập ReferenceID</returns>
        public List<RepositoryLedger> GetByListReferenceID(Guid selectID)
        {
            return Query.Where(p => p.ReferenceID == selectID).ToList();
        }
        public List<RepositoryLedger> GetByMaterialGoodsAndLotNo(Guid ID)
        {
            List<RepositoryLedger> lst = Query.Where(p => p.MaterialGoodsID == ID && p.LotNo != null).ToList();
            return (from a in lst
                    group a by new { a.MaterialGoodsID, a.LotNo } into g
                    select new RepositoryLedger
                    {
                        MaterialGoodsID = g.Key.MaterialGoodsID,
                        LotNo = g.Key.LotNo,
                        IWQuantity = (g.Sum(x => x.IWQuantity) - g.Sum(x => x.OWQuantity))
                    }).ToList();
        }
        public List<RepositoryLedger> GetByMaterialGoods(Guid ID)
        {
            List<RepositoryLedger> lst = Query.Where(p => p.MaterialGoodsID == ID && p.IWQuantity > 0).OrderBy(x => x.Date).ToList();
            List<SAInvoiceDetail> lstSA = _ISAInvoiceDetailService.Query.Where(p => _ISAInvoiceService.Query.Any(x => x.ID == p.SAInvoiceID && x.Recorded) && p.MaterialGoodsID == ID && p.RSInwardDetailID != null).ToList();
            List<RSInwardOutwardDetail> lstRS = _IRSInwardOutwardDetailService.Query.Where(p => _IRSInwardOutwardService.Query.Any(x => x.ID == p.RSInwardOutwardID && x.Recorded) && p.MaterialGoodsID == ID && p.RSInwardDetailID != null).ToList();
            List<RSTransferDetail> lstRST = _IRSTransferDetailService.Query.Where(p => _IRSTranferService.Query.Any(x => x.ID == p.RSTransferID && x.Recorded) && p.MaterialGoodsID == ID && p.RSInwardDetailID != null).ToList();
            foreach (var x in lst)
            {
                x.QuantityT = (x.IWQuantity ?? 0) - (lstSA.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Quantity) ?? 0) -
                    (lstRS.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Quantity) ?? 0) - (lstRST.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Quantity) ?? 0);
                x.IWAmountT = (x.IWAmount ?? 0) - (lstSA.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.OWAmount)) -
                    (lstRS.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Amount)) - (lstRST.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Amount));
            }
            return lst.Where(x => x.QuantityT > 0).ToList();
        }
        public RepositoryLedger GetByMaterialGoods(Guid ID, Guid RSID)
        {
            RepositoryLedger x = Query.FirstOrDefault(p => p.MaterialGoodsID == ID && p.IWQuantity > 0 && p.DetailID == RSID);
            if (x != null)
            {
                List<SAInvoiceDetail> lstSA = _ISAInvoiceDetailService.Query.Where(p => p.MaterialGoodsID == ID && p.RSInwardDetailID == RSID && _ISAInvoiceService.Query.Any(c => c.ID == p.SAInvoiceID && c.Recorded)).ToList();
                List<RSInwardOutwardDetail> lstRS = _IRSInwardOutwardDetailService.Query.Where(p => p.MaterialGoodsID == ID && p.RSInwardDetailID == RSID && _IRSInwardOutwardService.Query.Any(c => c.ID == p.RSInwardOutwardID && c.Recorded)).ToList();
                List<RSTransferDetail> lstRST = _IRSTransferDetailService.Query.Where(p => p.MaterialGoodsID == ID && p.RSInwardDetailID == RSID && _IRSTranferService.Query.Any(c => c.ID == p.RSTransferID && c.Recorded)).ToList();
                x.QuantityT = (x.IWQuantity ?? 0) - (lstSA.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Quantity) ?? 0) -
                        (lstRS.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Quantity) ?? 0) - (lstRST.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Quantity) ?? 0);
                x.IWAmountT = (x.IWAmount ?? 0) - (lstSA.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.OWAmount)) -
                        (lstRS.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Amount)) - (lstRST.Where(c => c.RSInwardDetailID == x.DetailID).Sum(d => d.Amount));
            }
            return x;
        }
        public decimal GetQuantityByRSIWID(Guid ID)
        {
            decimal sln = Query.FirstOrDefault(x => x.DetailID == ID).IWQuantity ?? 0;
            decimal slx1 = (_ISAInvoiceDetailService.Query.Where(x => x.RSInwardDetailID == ID && _ISAInvoiceService.Query.Any(c => c.ID == x.SAInvoiceID && c.Recorded)).Sum(t => t.Quantity) ?? 0);
            decimal slx2 = (_IRSInwardOutwardDetailService.Query.Where(x => x.RSInwardDetailID == ID && _IRSInwardOutwardService.Query.Any(c => c.ID == x.RSInwardOutwardID && c.Recorded)).Sum(t => t.Quantity) ?? 0);
            decimal slx3 = (_IRSTransferDetailService.Query.Where(x => x.RSInwardDetailID == ID && _IRSTranferService.Query.Any(c => c.ID == x.RSTransferID && c.Recorded)).Sum(t => t.Quantity) ?? 0);
            decimal slt = sln - slx1 - slx2 - slx3;
            return slt;

        }
        #region Lưu sổ kho

        #region Form Hàng mua trả lại
        /// <summary>
        /// Lấy DS lưu vào sổ kho 
        /// Form Hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GetListSaveRepositoryLedger(PPDiscountReturn temp, List<PPDiscountReturnDetail> listTempDetail, RepositoryLedger tempRepos)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            for (int i = 0; i < listTempDetail.Count; i++)
            {
                RepositoryLedger reposTemp = new RepositoryLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = temp.ID,
                    Date = (DateTime)temp.ODate,
                    PostedDate = (DateTime)temp.OPostedDate,
                    No = temp.OutwardNo,
                    Account = listTempDetail[i].CreditAccount,
                    AccountCorresponding = listTempDetail[i].DebitAccount,
                    RepositoryID = listTempDetail[i].ID,
                    MaterialGoodsID = (Guid)listTempDetail[i].MaterialGoodsID,
                    Unit = listTempDetail[i].Unit,
                    UnitPrice = (decimal)listTempDetail[i].UnitPrice,
                    OWQuantity = listTempDetail[i].Quantity,
                    OWAmount = listTempDetail[i].Amount,
                    IWQuantityBalance = 0,
                    IWAmountBalance = 0,
                    Reason = tempRepos.Reason,
                    Description = temp.Reason,
                    OWPurpose = null,
                    ExpiryDate = listTempDetail[i].ExpiryDate,
                    LotNo = listTempDetail[i].LotNo,
                    CostSetID = listTempDetail[i].CostSetID,
                    UnitConvert = listTempDetail[i].UnitConvert,
                    UnitPriceConvert = (decimal)listTempDetail[i].UnitPriceConvert,
                    OWQuantityConvert = listTempDetail[i].QuantityConvert,
                    StatisticsCodeID = listTempDetail[i].StatisticsCodeID,
                    ConvertRate = listTempDetail[i].ConvertRate
                };
                listReposTemp.Add(reposTemp);
            }
            return listReposTemp;
        }
        /// <summary>
        /// Hàm lưu sổ kho của form Hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        public void SaveRepositoryLedger(PPDiscountReturn temp, List<PPDiscountReturnDetail> listTempDetail,
                                         RepositoryLedger tempRepos)
        {
            foreach (var repositoryLedger in GetListSaveRepositoryLedger(temp, listTempDetail, tempRepos))
            {
                CreateNew(repositoryLedger);
            }
        }
        #endregion

        #region Form Điều chỉnh tồn kho
        /// <summary>
        /// Lấy DS lưu vào sổ kho 
        /// Form Điều chỉnh tồn kho
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        /// <returns></returns>
        public List<RepositoryLedger> GetListSaveRepositoryLedger(RSInwardOutward temp, List<RSInwardOutwardDetail> listTempDetail, RepositoryLedger tempRepos, bool isInward)
        {
            List<RepositoryLedger> listReposTemp = new List<RepositoryLedger>();
            for (int i = 0; i < listTempDetail.Count; i++)
            {
                RepositoryLedger reposTemp = new RepositoryLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = temp.ID,
                    Date = temp.Date,
                    PostedDate = temp.PostedDate,
                    No = temp.No,
                    Account = listTempDetail[i].CreditAccount,
                    AccountCorresponding = listTempDetail[i].DebitAccount,
                    RepositoryID = listTempDetail[i].ID,
                    MaterialGoodsID = (Guid)listTempDetail[i].MaterialGoodsID,
                    Unit = listTempDetail[i].Unit,
                    UnitPrice = listTempDetail[i].UnitPrice == null ? 0 : listTempDetail[i].UnitPrice,
                    IWAmount = isInward ? listTempDetail[i].Amount : (decimal?)null,
                    IWQuantity = isInward ? listTempDetail[i].Quantity : null,
                    OWQuantity = isInward ? null : listTempDetail[i].Quantity,
                    OWAmount = isInward ? (decimal?)null : listTempDetail[i].Amount,
                    IWQuantityBalance = 0,
                    IWAmountBalance = 0,
                    Reason = tempRepos.Reason,
                    Description = temp.Reason,
                    OWPurpose = null,
                    ExpiryDate = listTempDetail[i].ExpiryDate,
                    LotNo = listTempDetail[i].LotNo,
                    CostSetID = listTempDetail[i].CostSetID,
                    UnitConvert = listTempDetail[i].UnitConvert,
                    UnitPriceConvert = listTempDetail[i].UnitPriceConvert,
                    OWQuantityConvert = listTempDetail[i].QuantityConvert,
                    StatisticsCodeID = listTempDetail[i].StatisticsCodeID,
                    ConvertRate = listTempDetail[i].ConvertRate
                };
                listReposTemp.Add(reposTemp);
            }
            return listReposTemp;
        }
        /// <summary>
        /// Hàm lưu sổ kho của form Điều chỉnh tồn kho
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        public void SaveRepositoryLedger(RSInwardOutward temp, List<RSInwardOutwardDetail> listTempDetail,
                                         RepositoryLedger tempRepos, bool isInward)
        {
            foreach (var repositoryLedger in GetListSaveRepositoryLedger(temp, listTempDetail, tempRepos, isInward))
            {
                CreateNew(repositoryLedger);
            }
        }

        #endregion
        #endregion

        #region Tính giá xuất kho
        /// <summary>
        /// TÍnh giá xuất kho theo phương pháp bình quân cuối kỳ
        /// </summary>
        /// <param name="lstMaterialGoods">danh sách VTHH cần tính giá</param>>
        /// <param name="fromDate">ngày bắt đầu kỳ tính giá</param>
        /// <param name="toDate">ngày kết thúc kỳ tính giá</param>
        public bool PricingAndUpdateOW_AverageForEndOfPeriods(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate, Guid ID)
        {
            #region Tính giá xuất DuyNT
            var lstgg = _IPPDiscountReturnDetailService.Query.ToList().Where(x => lstMaterialGoods.Any(d => d.ID == x.MaterialGoodsID) && x.ConfrontDetailID != null && (x.RepositoryID == ID) && _IPPDiscountReturnService.Query.Any(c => c.ID == x.PPDiscountReturnID && c.TypeID == 230 && c.Recorded && c.PostedDate <= toDate)).ToList();
            List<SystemOption> lstsys = _ISystemOptionService.Query.Where(c => c.Code == "DDSo_TienVND" || c.Code == "DDSo_NgoaiTe").ToList();
            var ddSoGia = lstsys.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null) lamTronGia = Convert.ToInt32(ddSoGia.Data);
            var ddSoGia1 = lstsys.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
            int lamTronGia1 = 0;
            if (ddSoGia != null) lamTronGia1 = Convert.ToInt32(ddSoGia1.Data);
            var lstrepos = Query.ToList();
            var lstRepositoryLedgers = (from l in lstrepos.Where(c => lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID) && (c.RepositoryID == ID) && !(new int[] { 330 }.Any(d => d == c.TypeID))).ToList()
                                        group l by l.MaterialGoodsID into g
                                        select new
                                        {
                                            g.Key,
                                            g,
                                            SumAmount =
                                          (((g.Where(c => c.PostedDate < fromDate && (c.ID != null)).Sum(c => Math.Round(c.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero))//nhập kho kỳ trước
                                               - g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWAmount)//xuất kho kỳ trước
                                               + g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (c.ID != null)).Sum(c => Math.Round(c.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero))//nhập kho trong kỳ này
                                             ) ?? 0) - (lstgg.Where(x => x.MaterialGoodsID == g.Key).Sum(b => Math.Round(b.Amount ?? 0, lamTronGia, MidpointRounding.AwayFromZero)))),
                                            SumQuantity =
                                       (((g.Where(c => c.PostedDate < fromDate && (c.ID != null)).Sum(c => c.IWQuantity)
                                               - g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWQuantity)
                                               + g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (c.ID != null)).Sum(c => c.IWQuantity))) == 0 ? 1 :
                                       (g.Where(c => c.PostedDate < fromDate && (c.ID != null)).Sum(c => c.IWQuantity)//nhập kho kỳ trước
                                               - g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWQuantity)//xuất kho kỳ trước
                                               + g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (c.ID != null)).Sum(c => c.IWQuantity)//nhập kho trong kỳ này

                                        ) ?? 0)
                                        }).ToList();
            var updatingUnitPrice = (from m in lstMaterialGoods
                                     join l in lstRepositoryLedgers on m.ID equals l.Key into j
                                     from k in j
                                     select new
                                     {
                                         MaterialGoodID = m.ID,
                                         sumBeginIWQuantityOriginal = (j.Sum(c => c.SumAmount) / (j.Sum(c => c.SumQuantity) == 0 ? 1 : (j.Sum(c => c.SumQuantity))))
                                     }).ToList();
            #endregion

            #region Lấy các chứng từ cần cập nhật giá xuất

            List<int> lstTypeOW = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420, 421, 422 };
            #region RepositoryLedger - GeneralLedger
            List<RepositoryLedger> lstRepositoryLedgersOW =
                lstRepositoryLedgers.SelectMany(c => c.g).Where(c => lstTypeOW.Any(d => c.TypeID == d) && c.PostedDate >= fromDate && c.PostedDate <= toDate).OrderBy(c => c.RefDateTime).ToList();

            List<GeneralLedger> lstGeneralLedgers =
                _IGeneralLedgerService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (lstTypeOW.Contains(c.TypeID)) || c.TypeID == 330).ToList().OrderBy(c => c.PostedDate).ToList();
            #endregion

            #region RSInwardOutward - RSInwardOutwardDetail
            List<RSInwardOutward> lstRSInwardOutwards = _IRSInwardOutwardService.Query.Where(
                    c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
            List<RSAssemblyDismantlement> rSAssemblyDismantlements = _IRSAssemblyDismantlementService.GetAll();
            #endregion

            #region RSTranfer - RSTranferDetail
            List<int> lstTypeTranfer = new List<int>() { 420, 421, 422 };
            List<RSTransfer> lstRSTranfer = _IRSTranferService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
            List<RepositoryLedger> lstRepositoriesledger = lstrepos.Where(c => (lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID)
            && (c.TypeID == 420 || c.TypeID == 421 || c.TypeID == 422) && c.PostedDate >= fromDate && c.PostedDate <= toDate) || c.TypeID == 403).OrderBy(c => c.PostedDate).ToList();
            #endregion

            #region SAInvoice
            List<int> lstTypeSAInvoices = new List<int>() { 412, 415, 320, 321, 322, 323, 324, 325 };
            List<SAInvoice> lstSAInvoices = _ISAInvoiceService.Query.Where(
                    c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
            List<SAReturn> lstSAReturns = _ISAReturnService.Query.Where(c => c.Recorded && c.AutoOWAmountCal == 0).ToList();
            List<SAReturnDetail> lstSAReturnDetails = _ISAReturnDetailService.Query.Where(c => _ISAReturnService.Query.Any(d => d.Recorded && d.AutoOWAmountCal == 0 && d.ID == c.SAReturnID)).ToList();
            List<GeneralLedger> lstGeneralLedgersreturn = lstGeneralLedgers.Where(c => c.TypeID == 330).ToList();
            List<RSInwardOutward> lstRSInwardOutward = _IRSInwardOutwardService.Query.Where(c => c.TypeID == 403).ToList();
            #endregion

            #endregion

            #region tạo list kiểm tra số lượng tồn trong kho
            //dùng tạm một đối tượng OPMaterialGoods Để tính (vì sau mỗi lần xuất kho tính lại đơn giá thì cần tính lại số lượng tồn mà var không thể cập nhật được)
            List<OPMaterialGoods> lstCheck =
                 (from l in lstRepositoryLedgers
                  select new OPMaterialGoods
                  {
                      MaterialGoodsID = l.Key,
                      Quantity = l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.IWQuantity) //nhập kho kỳ trước
                              - l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWQuantity) //xuất kho kỳ trước
                              + l.g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).Sum(c => c.IWQuantity), //nhập kho trong kỳ
                      Amount = (l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.IWAmount) //nhập kho kỳ trước
                                - l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWAmount) //xuất kho kỳ trước
                                + l.g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).Sum(c => c.IWAmount)) ?? 0, //nhập kho trong kỳ
                  }).ToList();
            #endregion

            #region Cập nhật các chứng từ Chuongnv
            BeginTran();
            try
            {
                #region RepositoryLedger
                foreach (RepositoryLedger rep in lstRepositoryLedgersOW)
                {
                    if (lstMaterialGoods.Any(c => c.ID == rep.MaterialGoodsID))
                    {
                        lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Quantity -= rep.OWQuantity;
                        rep.UnitPrice = updatingUnitPrice.Count(c => c.MaterialGoodID == rep.MaterialGoodsID) == 1
                                  ? updatingUnitPrice.Single(c => c.MaterialGoodID == rep.MaterialGoodsID).sumBeginIWQuantityOriginal
                                  : rep.UnitPrice;
                        if (lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Quantity == 0)
                        {
                            rep.OWAmount = lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Amount;
                        }
                        else
                        {
                            rep.OWAmount = Math.Round((rep.UnitPrice * rep.OWQuantity) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                            lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Amount -= rep.OWAmount ?? 0;
                        }

                        Update(rep);
                        if (rep.TypeID == 410 || rep.TypeID == 414)
                        {
                            #region RSInwardOutward
                            var inwardOutward = lstRSInwardOutwards.FirstOrDefault(x => x.ID == rep.ReferenceID);
                            if (inwardOutward != null)
                            {
                                if (lstMaterialGoods.Any(c => inwardOutward.RSInwardOutwardDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                {
                                    foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                                    {

                                        if (inwardOutwardDetail.MaterialGoodsID == rep.MaterialGoodsID && inwardOutwardDetail.RepositoryID == ID)
                                        {
                                            inwardOutwardDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                            inwardOutwardDetail.UnitPrice = rep.UnitPrice;
                                            inwardOutwardDetail.AmountOriginal = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                            inwardOutwardDetail.Amount = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0), lamTronGia, MidpointRounding.AwayFromZero);
                                        }

                                    }
                                    inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                                    inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                                    _IRSInwardOutwardService.Update(inwardOutward);
                                }
                                if (inwardOutward.RSAssemblyDismantlementID != null)
                                {
                                    var RSAssemblyDismantlement = rSAssemblyDismantlements.FirstOrDefault(c => c.ID == inwardOutward.RSAssemblyDismantlementID);
                                    if (RSAssemblyDismantlement != null && lstMaterialGoods.Any(c => RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (RSAssemblyDismantlementDetail RSAssemblyDismantlementDetail in RSAssemblyDismantlement.RSAssemblyDismantlementDetails)
                                        {

                                            if (RSAssemblyDismantlementDetail.MaterialGoodsID == rep.MaterialGoodsID && RSAssemblyDismantlementDetail.RepositoryID == ID)
                                            {
                                                RSAssemblyDismantlementDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                RSAssemblyDismantlementDetail.UnitPrice = rep.UnitPrice;
                                                RSAssemblyDismantlementDetail.AmountOriginal = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                RSAssemblyDismantlementDetail.Amount = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0),
                                                     lamTronGia, MidpointRounding.AwayFromZero);
                                            }

                                        }
                                        RSAssemblyDismantlement.Amount = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.Amount);
                                        RSAssemblyDismantlement.AmountOriginal = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.AmountOriginal);
                                        _IRSAssemblyDismantlementService.Update(RSAssemblyDismantlement);
                                    }
                                }
                                if (inwardOutward.SAInvoiceID != null)
                                {
                                    var Sainvoice = lstSAInvoices.FirstOrDefault(x => x.ID == inwardOutward.SAInvoiceID);
                                    if (Sainvoice != null && lstMaterialGoods.Any(c => Sainvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (SAInvoiceDetail sAInvoiceDetail in Sainvoice.SAInvoiceDetails)
                                        {
                                            if (sAInvoiceDetail.MaterialGoodsID == rep.MaterialGoodsID && sAInvoiceDetail.RepositoryID == ID)
                                            {
                                                sAInvoiceDetail.OWPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                sAInvoiceDetail.OWPrice = rep.UnitPrice;
                                                sAInvoiceDetail.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                sAInvoiceDetail.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0),
                                                    lamTronGia, MidpointRounding.AwayFromZero);
                                            }
                                        }
                                        Sainvoice.TotalCapitalAmount = Sainvoice.SAInvoiceDetails.Sum(c => c.Amount);
                                        Sainvoice.TotalCapitalAmountOriginal = Sainvoice.SAInvoiceDetails.Sum(c => c.AmountOriginal);
                                        _ISAInvoiceService.Update(Sainvoice);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (new int[] { 412 }.Any(a => a == rep.TypeID))
                        {
                            #region SAInvoice
                            var sAInvoice = lstSAInvoices.FirstOrDefault(x => x.ID == rep.ReferenceID);
                            if (sAInvoice != null && lstMaterialGoods.Any(c => sAInvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                            {
                                foreach (SAInvoiceDetail sAInvoiceDetails in sAInvoice.SAInvoiceDetails)
                                {
                                    if (sAInvoiceDetails.MaterialGoodsID == rep.MaterialGoodsID && sAInvoiceDetails.RepositoryID == ID)
                                    {
                                        sAInvoiceDetails.OWPriceOriginal = rep.UnitPrice / (sAInvoice.ExchangeRate ?? 1);
                                        sAInvoiceDetails.OWPrice = rep.UnitPrice;
                                        sAInvoiceDetails.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                            (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                        sAInvoiceDetails.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0),
                                             lamTronGia, MidpointRounding.AwayFromZero);
                                        SAReturnDetail sAReturnDetails = lstSAReturnDetails.FirstOrDefault(c => c.SAInvoiceDetailID == sAInvoiceDetails.ID);//update hang mua trả lại
                                        if (sAReturnDetails != null)
                                        {
                                            var sAReturn = lstSAReturns.FirstOrDefault(c => c.ID == sAReturnDetails.SAReturnID && c.Recorded && c.AutoOWAmountCal == 0);
                                            if (sAReturn != null)
                                            {
                                                sAReturnDetails.OWPriceOriginal = rep.UnitPrice / (sAReturn.ExchangeRate ?? 1);
                                                sAReturnDetails.OWPrice = rep.UnitPrice;
                                                if (sAInvoiceDetails.Quantity == sAReturnDetails.Quantity)
                                                {
                                                    sAReturnDetails.OWAmountOriginal = Math.Round((rep.OWAmount ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                                                                    (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    sAReturnDetails.OWAmount = Math.Round((rep.OWAmount ?? 0),
                                                                                lamTronGia, MidpointRounding.AwayFromZero);
                                                }
                                                else
                                                {
                                                    sAReturnDetails.OWAmountOriginal = Math.Round(sAInvoiceDetails.OWPriceOriginal * (sAReturnDetails.Quantity ?? 1),
                                                                                    (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    sAReturnDetails.OWAmount = Math.Round(sAInvoiceDetails.OWPrice * (sAReturnDetails.Quantity ?? 1),
                                                                                lamTronGia, MidpointRounding.AwayFromZero);
                                                }
                                                _ISAReturnDetailService.Update(sAReturnDetails);
                                                sAReturn.TotalOWAmount = sAReturn.SAReturnDetails.Sum(c => c.OWAmount);
                                                sAReturn.TotalOWAmountOriginal = sAReturn.SAReturnDetails.Sum(c => c.OWAmountOriginal);
                                                _ISAReturnService.Update(sAReturn);
                                                var rsInward = lstRSInwardOutward.FirstOrDefault(c => c.ID == sAReturn.ID);
                                                if (rsInward != null)
                                                {
                                                    rsInward.TotalAmount = sAReturn.TotalOWAmount;
                                                    rsInward.TotalAmountOriginal = sAReturn.TotalOWAmountOriginal;
                                                    _IRSInwardOutwardService.Update(rsInward);
                                                }
                                                var lstGeneralLedgerReturn = lstGeneralLedgersreturn.Where(c => c.DetailID == sAReturnDetails.ID);
                                                foreach (GeneralLedger g in lstGeneralLedgerReturn)
                                                {
                                                    if (g.Account == "632")
                                                    {
                                                        g.CreditAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                        g.CreditAmount = sAReturnDetails.OWAmount;

                                                    }
                                                    else if (g.AccountCorresponding == "632")
                                                    {
                                                        g.DebitAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                        g.DebitAmount = sAReturnDetails.OWAmount;
                                                    }
                                                    _IGeneralLedgerService.Update(g);
                                                }
                                                var ReposReturn = lstRepositoriesledger.FirstOrDefault(c => c.DetailID == sAReturnDetails.ID);
                                                if (ReposReturn != null)
                                                {
                                                    ReposReturn.UnitPrice = sAReturnDetails.OWPrice;
                                                    ReposReturn.IWAmount = sAReturnDetails.OWAmount;
                                                    ReposReturn.IWAmountBalance = sAReturnDetails.OWAmount;
                                                    Update(ReposReturn);
                                                }
                                            }
                                        }
                                    }
                                    sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmount);
                                    sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmountOriginal);
                                    _ISAInvoiceService.Update(sAInvoice);
                                }
                                if (sAInvoice.OutwardNo != null && !string.IsNullOrEmpty(sAInvoice.OutwardNo))
                                {
                                    var outWard = lstRSInwardOutwards.FirstOrDefault(x => x.ID == sAInvoice.ID);//_IRSInwardOutwardService.Query.FirstOrDefault(c => c.ID == sAInvoice.ID);
                                    if (outWard != null)
                                    {
                                        outWard.TotalAmount = sAInvoice.TotalCapitalAmount;
                                        outWard.TotalAmountOriginal = sAInvoice.TotalCapitalAmountOriginal;
                                        _IRSInwardOutwardService.Update(outWard);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (rep.TypeID == 420 || rep.TypeID == 421 || rep.TypeID == 422)
                        {
                            #region RSTranfer
                            var tranfer = lstRSTranfer.FirstOrDefault(x => x.ID == rep.ReferenceID);

                            if (tranfer != null && lstMaterialGoods.Any(c => tranfer.RSTransferDetails.Any(d => d.MaterialGoodsID == c.ID)))
                            {
                                foreach (RSTransferDetail tranferDetail in tranfer.RSTransferDetails)
                                {
                                    if (tranferDetail.MaterialGoodsID == rep.MaterialGoodsID && tranferDetail.FromRepositoryID == ID)
                                    {
                                        tranferDetail.UnitPriceOriginal = rep.UnitPrice / (tranfer.ExchangeRate ?? 1);
                                        tranferDetail.UnitPrice = rep.UnitPrice;
                                        tranferDetail.AmountOriginal = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0) / (tranfer.ExchangeRate ?? 1),
                                           (tranfer.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                        tranferDetail.Amount = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0),
                                             lamTronGia, MidpointRounding.AwayFromZero);
                                        if (lstrepos.Any(x => x.DetailID == tranferDetail.ID && x.IWQuantity > 0))
                                        {
                                            var inwardTranfer = lstrepos.FirstOrDefault(x => x.DetailID == tranferDetail.ID && x.IWQuantity > 0);
                                            inwardTranfer.UnitPrice = tranferDetail.UnitPrice;
                                            inwardTranfer.IWAmount = tranferDetail.Amount;
                                            Update(inwardTranfer);
                                        }
                                    }

                                }
                                tranfer.TotalAmount = tranfer.RSTransferDetails.Sum(c => c.Amount);
                                tranfer.TotalAmountOriginal = tranfer.RSTransferDetails.Sum(c => c.AmountOriginal);
                                _IRSTranferService.Update(tranfer);
                            }
                            #endregion
                        }
                        #region GeneralLedger
                        string OldID = "";
                        var lstGl = lstGeneralLedgers.Where(x => x.DetailID == rep.DetailID).ToList();//_IGeneralLedgerService.GetByDetailID(rep.DetailID ?? Guid.Empty);
                        foreach (GeneralLedger g in lstGl)
                        {
                            if (lstGl.Count >= 1)
                            {
                                OldID = lstGl[0].ID.ToString();
                            }
                            if (new int[] { 320, 321, 322, 323, 324, 325, 412 }.Any(u => u == g.TypeID))
                            {
                                if (g.Account == "632")
                                {
                                    g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                    g.DebitAmount = rep.OWAmount ?? 0;
                                }
                                else if (g.AccountCorresponding == "632")
                                {
                                    g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                    g.CreditAmount = rep.OWAmount ?? 0;
                                }
                            }
                            else if (new int[] { 410, 414 }.Any(u => u == g.TypeID))
                            {
                                if (lstRSInwardOutwards.Any(x => x.ID == g.ReferenceID) && lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.Count > 0)
                                {
                                    var rsOutDetail = lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                    if (rsOutDetail != null)
                                    {
                                        if (g.Account == rsOutDetail.CreditAccount)
                                        {
                                            g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = rep.OWAmount ?? 0;
                                        }
                                        else if (g.Account == rsOutDetail.DebitAccount)
                                        {
                                            g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = rep.OWAmount ?? 0;
                                        }
                                    }
                                }
                            }
                            else if (new int[] { 420, 421, 422 }.Any(u => u == g.TypeID))
                            {
                                if (lstRSTranfer.Any(x => x.ID == g.ReferenceID) && lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.Count > 0)
                                {
                                    var rsOutDetail = lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.FirstOrDefault(x => x.ID == g.DetailID && x.FromRepositoryID == rep.RepositoryID);
                                    if (rsOutDetail != null)
                                    {
                                        if (rsOutDetail.DebitAccount == rsOutDetail.CreditAccount)//trungnq thêm trường hợp này để check trường hợp tính giá xuất kho khi 2 tài khoản nợ có = nhau bug 6309
                                        {
                                            if (rep.OWAmount != null)
                                            {
                                                if (rep.OWAmount != 0)
                                                {
                                                    if (g.ID.ToString() == OldID)
                                                    {
                                                        g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                        g.CreditAmount = rep.OWAmount ?? 0;

                                                    }
                                                    else
                                                    {
                                                        g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                        g.DebitAmount = rep.OWAmount ?? 0;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        if (g.Account == rsOutDetail.CreditAccount)
                                        {
                                            g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = rep.OWAmount ?? 0;
                                        }
                                        else if (g.Account == rsOutDetail.DebitAccount)
                                        {
                                            g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = rep.OWAmount ?? 0;
                                        }
                                    }
                                }
                            }
                            _IGeneralLedgerService.Update(g);
                        }
                        #endregion
                    }
                }
                #endregion

                CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                return false;
            }
            #endregion
        }
        public bool PricingAndUpdateOW_AverageForEndOfPeriods(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate)
        {
            #region Tính giá xuất DuyNT
            var lstgg = _IPPDiscountReturnDetailService.Query.ToList().Where(x => lstMaterialGoods.Any(d => d.ID == x.MaterialGoodsID) && x.ConfrontDetailID != null && _IPPDiscountReturnService.Query.Any(c => c.ID == x.PPDiscountReturnID && c.TypeID == 230 && c.Recorded && c.PostedDate <= toDate)).ToList();
            List<SystemOption> lstsys = _ISystemOptionService.Query.Where(c => c.Code == "DDSo_TienVND" || c.Code == "DDSo_NgoaiTe").ToList();
            var ddSoGia = lstsys.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null) lamTronGia = Convert.ToInt32(ddSoGia.Data);
            var ddSoGia1 = lstsys.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
            int lamTronGia1 = 0;
            if (ddSoGia != null) lamTronGia1 = Convert.ToInt32(ddSoGia1.Data);
            var lstrepos = Query.ToList();
            var lstRepositoryLedgers = (from l in lstrepos.Where(c => lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID) && !(new int[] { 330 }.Any(d => d == c.TypeID))).ToList()
                                        group l by l.MaterialGoodsID into g
                                        select new
                                        {
                                            g.Key,
                                            g,
                                            SumAmount =
                                          (((g.Where(c => c.PostedDate < fromDate && (c.ID != null)).Sum(c => Math.Round(c.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero))//nhập kho kỳ trước
                                               - g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWAmount)//xuất kho kỳ trước
                                               + g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (c.ID != null)).Sum(c => Math.Round(c.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero))//nhập kho trong kỳ này
                                             ) ?? 0) - (lstgg.Where(x => x.MaterialGoodsID == g.Key).Sum(b => Math.Round(b.Amount ?? 0, lamTronGia, MidpointRounding.AwayFromZero)))),
                                            SumQuantity =
                                       (((g.Where(c => c.PostedDate < fromDate).Sum(c => c.IWQuantity)
                                               - g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWQuantity)
                                               + g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).Sum(c => c.IWQuantity))) == 0 ? 1 :
                                       (g.Where(c => c.PostedDate < fromDate).Sum(c => c.IWQuantity)//nhập kho kỳ trước
                                               - g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWQuantity)//xuất kho kỳ trước
                                               + g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).Sum(c => c.IWQuantity)//nhập kho trong kỳ này

                                        ) ?? 0)
                                        }).ToList();
            var updatingUnitPrice = (from m in lstMaterialGoods
                                     join l in lstRepositoryLedgers on m.ID equals l.Key into j
                                     from k in j
                                     select new
                                     {
                                         MaterialGoodID = m.ID,
                                         sumBeginIWQuantityOriginal = (j.Sum(c => c.SumAmount) / (j.Sum(c => c.SumQuantity) == 0 ? 1 : (j.Sum(c => c.SumQuantity))))
                                     }).ToList();


            #endregion

            #region Lấy các chứng từ cần cập nhật giá xuất DuyNT

            List<int> lstTypeOW = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420, 421, 422 };
            #region RepositoryLedger - GeneralLedger
            List<RepositoryLedger> lstRepositoryLedgersOW =
                lstRepositoryLedgers.SelectMany(c => c.g).Where(c => lstTypeOW.Any(d => c.TypeID == d) && c.PostedDate >= fromDate && c.PostedDate <= toDate).OrderBy(c => c.RefDateTime).ToList();

            List<GeneralLedger> lstGeneralLedgers =
                _IGeneralLedgerService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (lstTypeOW.Contains(c.TypeID)) || c.TypeID == 330).ToList().OrderBy(c => c.PostedDate).ToList();
            #endregion

            #region RSInwardOutward - RSInwardOutwardDetail
            List<RSInwardOutward> lstRSInwardOutwards = _IRSInwardOutwardService.Query.Where(
                    c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
            List<RSAssemblyDismantlement> rSAssemblyDismantlements = _IRSAssemblyDismantlementService.GetAll();
            #endregion

            #region RSTranfer - RSTranferDetail
            List<int> lstTypeTranfer = new List<int>() { 420, 421, 422 };
            List<RSTransfer> lstRSTranfer = _IRSTranferService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
            List<RepositoryLedger> lstRepositoriesledger = lstrepos.Where(c => (lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID)
            && (c.TypeID == 420 || c.TypeID == 421 || c.TypeID == 422) && c.PostedDate >= fromDate && c.PostedDate <= toDate) || c.TypeID == 403).OrderBy(c => c.PostedDate).ToList();
            #endregion

            #region SAInvoice
            List<int> lstTypeSAInvoices = new List<int>() { 412, 415, 320, 321, 322, 323, 324, 325 };
            List<SAInvoice> lstSAInvoices = _ISAInvoiceService.Query.Where(
                    c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
            List<SAReturn> lstSAReturns = _ISAReturnService.Query.Where(c => c.Recorded && c.AutoOWAmountCal == 0).ToList();
            List<SAReturnDetail> lstSAReturnDetails = _ISAReturnDetailService.Query.Where(c => _ISAReturnService.Query.Any(d => d.Recorded && d.AutoOWAmountCal == 0 && d.ID == c.SAReturnID)).ToList();
            List<GeneralLedger> lstGeneralLedgersreturn = lstGeneralLedgers.Where(c => c.TypeID == 330).ToList();
            List<RSInwardOutward> lstRSInwardOutward = _IRSInwardOutwardService.Query.Where(c => c.TypeID == 403).ToList();
            #endregion

            #endregion

            #region tạo list kiểm tra số lượng tồn trong kho
            //dùng tạm một đối tượng OPMaterialGoods Để tính (vì sau mỗi lần xuất kho tính lại đơn giá thì cần tính lại số lượng tồn mà var không thể cập nhật được)
            List<OPMaterialGoods> lstCheck =
                 (from l in lstRepositoryLedgers
                  select new OPMaterialGoods
                  {
                      MaterialGoodsID = l.Key,
                      Quantity = l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.IWQuantity) //nhập kho kỳ trước
                              - l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWQuantity) //xuất kho kỳ trước
                              + l.g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).Sum(c => c.IWQuantity), //nhập kho trong kỳ
                      Amount = (l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.IWAmount) //nhập kho kỳ trước
                                - l.g.Where(c => c.PostedDate < fromDate).Sum(c => c.OWAmount) //xuất kho kỳ trước
                                + l.g.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate).Sum(c => c.IWAmount)) ?? 0, //nhập kho trong kỳ
                  }).ToList();
            #endregion

            #region Cập nhật các chứng từ Chuongnv
            BeginTran();
            try
            {
                #region RepositoryLedger
                foreach (RepositoryLedger rep in lstRepositoryLedgersOW)
                {
                    if (lstMaterialGoods.Any(c => c.ID == rep.MaterialGoodsID))
                    {
                        lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Quantity -= rep.OWQuantity;
                        rep.UnitPrice = updatingUnitPrice.Count(c => c.MaterialGoodID == rep.MaterialGoodsID) == 1
                                  ? updatingUnitPrice.Single(c => c.MaterialGoodID == rep.MaterialGoodsID).sumBeginIWQuantityOriginal
                                  : rep.UnitPrice;
                        if (lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Quantity == 0)
                        {
                            rep.OWAmount = lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Amount;
                        }
                        else
                        {
                            rep.OWAmount = Math.Round((rep.UnitPrice * rep.OWQuantity) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                            lstCheck.FirstOrDefault(x => x.MaterialGoodsID == rep.MaterialGoodsID).Amount -= rep.OWAmount ?? 0;
                        }

                        Update(rep);
                        if (rep.TypeID == 410 || rep.TypeID == 414)
                        {
                            #region RSInwardOutward
                            var inwardOutward = lstRSInwardOutwards.FirstOrDefault(x => x.ID == rep.ReferenceID);
                            if (inwardOutward != null)
                            {
                                if (lstMaterialGoods.Any(c => inwardOutward.RSInwardOutwardDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                {
                                    foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                                    {

                                        if (inwardOutwardDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                        {
                                            inwardOutwardDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                            inwardOutwardDetail.UnitPrice = rep.UnitPrice;
                                            inwardOutwardDetail.AmountOriginal = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                            inwardOutwardDetail.Amount = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0), lamTronGia, MidpointRounding.AwayFromZero);
                                        }

                                    }
                                    inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                                    inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                                    _IRSInwardOutwardService.Update(inwardOutward);
                                }
                                if (inwardOutward.RSAssemblyDismantlementID != null)
                                {
                                    var RSAssemblyDismantlement = rSAssemblyDismantlements.FirstOrDefault(c => c.ID == inwardOutward.RSAssemblyDismantlementID);
                                    if (RSAssemblyDismantlement != null && lstMaterialGoods.Any(c => RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (RSAssemblyDismantlementDetail RSAssemblyDismantlementDetail in RSAssemblyDismantlement.RSAssemblyDismantlementDetails)
                                        {

                                            if (RSAssemblyDismantlementDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                            {
                                                RSAssemblyDismantlementDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                RSAssemblyDismantlementDetail.UnitPrice = rep.UnitPrice;
                                                RSAssemblyDismantlementDetail.AmountOriginal = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                RSAssemblyDismantlementDetail.Amount = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0),
                                                     lamTronGia, MidpointRounding.AwayFromZero);
                                            }

                                        }
                                        RSAssemblyDismantlement.Amount = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.Amount);
                                        RSAssemblyDismantlement.AmountOriginal = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.AmountOriginal);
                                        _IRSAssemblyDismantlementService.Update(RSAssemblyDismantlement);
                                    }
                                }
                                if (inwardOutward.SAInvoiceID != null)
                                {
                                    var Sainvoice = lstSAInvoices.FirstOrDefault(x => x.ID == inwardOutward.SAInvoiceID);
                                    if (Sainvoice != null && lstMaterialGoods.Any(c => Sainvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (SAInvoiceDetail sAInvoiceDetail in Sainvoice.SAInvoiceDetails)
                                        {
                                            if (sAInvoiceDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                            {
                                                sAInvoiceDetail.OWPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                sAInvoiceDetail.OWPrice = rep.UnitPrice;
                                                sAInvoiceDetail.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                sAInvoiceDetail.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0),
                                                    lamTronGia, MidpointRounding.AwayFromZero);
                                            }
                                        }
                                        Sainvoice.TotalCapitalAmount = Sainvoice.SAInvoiceDetails.Sum(c => c.Amount);
                                        Sainvoice.TotalCapitalAmountOriginal = Sainvoice.SAInvoiceDetails.Sum(c => c.AmountOriginal);
                                        _ISAInvoiceService.Update(Sainvoice);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (new int[] { 412 }.Any(a => a == rep.TypeID))
                        {
                            #region SAInvoice
                            var sAInvoice = lstSAInvoices.FirstOrDefault(x => x.ID == rep.ReferenceID);
                            if (sAInvoice != null && lstMaterialGoods.Any(c => sAInvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                            {
                                foreach (SAInvoiceDetail sAInvoiceDetails in sAInvoice.SAInvoiceDetails)
                                {
                                    if (sAInvoiceDetails.MaterialGoodsID == rep.MaterialGoodsID)
                                    {
                                        sAInvoiceDetails.OWPriceOriginal = rep.UnitPrice / (sAInvoice.ExchangeRate ?? 1);
                                        sAInvoiceDetails.OWPrice = rep.UnitPrice;
                                        sAInvoiceDetails.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                            (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                        sAInvoiceDetails.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0),
                                             lamTronGia, MidpointRounding.AwayFromZero);
                                        SAReturnDetail sAReturnDetails = lstSAReturnDetails.FirstOrDefault(c => c.SAInvoiceDetailID == sAInvoiceDetails.ID);//update hang mua trả lại
                                        if (sAReturnDetails != null)
                                        {
                                            var sAReturn = lstSAReturns.FirstOrDefault(c => c.ID == sAReturnDetails.SAReturnID && c.Recorded && c.AutoOWAmountCal == 0);
                                            if (sAReturn != null)
                                            {
                                                sAReturnDetails.OWPriceOriginal = rep.UnitPrice / (sAReturn.ExchangeRate ?? 1);
                                                sAReturnDetails.OWPrice = rep.UnitPrice;
                                                if (sAInvoiceDetails.Quantity == sAReturnDetails.Quantity)
                                                {
                                                    sAReturnDetails.OWAmountOriginal = Math.Round((rep.OWAmount ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                                                                    (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    sAReturnDetails.OWAmount = Math.Round((rep.OWAmount ?? 0),
                                                                                lamTronGia, MidpointRounding.AwayFromZero);
                                                }
                                                else
                                                {
                                                    sAReturnDetails.OWAmountOriginal = Math.Round(sAInvoiceDetails.OWPriceOriginal * (sAReturnDetails.Quantity ?? 1),
                                                                                    (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    sAReturnDetails.OWAmount = Math.Round(sAInvoiceDetails.OWPrice * (sAReturnDetails.Quantity ?? 1),
                                                                                lamTronGia, MidpointRounding.AwayFromZero);
                                                }
                                                _ISAReturnDetailService.Update(sAReturnDetails);
                                                sAReturn.TotalOWAmount = sAReturn.SAReturnDetails.Sum(c => c.OWAmount);
                                                sAReturn.TotalOWAmountOriginal = sAReturn.SAReturnDetails.Sum(c => c.OWAmountOriginal);
                                                _ISAReturnService.Update(sAReturn);
                                                var rsInward = lstRSInwardOutward.FirstOrDefault(c => c.ID == sAReturn.ID);
                                                if (rsInward != null)
                                                {
                                                    rsInward.TotalAmount = sAReturn.TotalOWAmount;
                                                    rsInward.TotalAmountOriginal = sAReturn.TotalOWAmountOriginal;
                                                    _IRSInwardOutwardService.Update(rsInward);
                                                }
                                                var lstGeneralLedgerReturn = lstGeneralLedgersreturn.Where(c => c.DetailID == sAReturnDetails.ID);
                                                foreach (GeneralLedger g in lstGeneralLedgerReturn)
                                                {
                                                    if (g.Account == "632")
                                                    {
                                                        g.CreditAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                        g.CreditAmount = sAReturnDetails.OWAmount;

                                                    }
                                                    else if (g.AccountCorresponding == "632")
                                                    {
                                                        g.DebitAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                        g.DebitAmount = sAReturnDetails.OWAmount;
                                                    }
                                                    _IGeneralLedgerService.Update(g);
                                                }
                                                var ReposReturn = lstRepositoriesledger.FirstOrDefault(c => c.DetailID == sAReturnDetails.ID);
                                                if (ReposReturn != null)
                                                {
                                                    ReposReturn.UnitPrice = sAReturnDetails.OWPrice;
                                                    ReposReturn.IWAmount = sAReturnDetails.OWAmount;
                                                    ReposReturn.IWAmountBalance = sAReturnDetails.OWAmount;
                                                    Update(ReposReturn);
                                                }
                                            }
                                        }
                                    }
                                    sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmount);
                                    sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmountOriginal);
                                    _ISAInvoiceService.Update(sAInvoice);
                                }
                                if (sAInvoice.OutwardNo != null && !string.IsNullOrEmpty(sAInvoice.OutwardNo))
                                {
                                    var outWard = lstRSInwardOutwards.FirstOrDefault(x => x.ID == sAInvoice.ID);//_IRSInwardOutwardService.Query.FirstOrDefault(c => c.ID == sAInvoice.ID);
                                    if (outWard != null)
                                    {
                                        outWard.TotalAmount = sAInvoice.TotalCapitalAmount;
                                        outWard.TotalAmountOriginal = sAInvoice.TotalCapitalAmountOriginal;
                                        _IRSInwardOutwardService.Update(outWard);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (rep.TypeID == 420 || rep.TypeID == 421 || rep.TypeID == 422)
                        {
                            #region RSTranfer
                            var tranfer = lstRSTranfer.FirstOrDefault(x => x.ID == rep.ReferenceID);

                            if (tranfer != null && lstMaterialGoods.Any(c => tranfer.RSTransferDetails.Any(d => d.MaterialGoodsID == c.ID)))
                            {
                                foreach (RSTransferDetail tranferDetail in tranfer.RSTransferDetails)
                                {
                                    if (tranferDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                    {
                                        tranferDetail.UnitPriceOriginal = rep.UnitPrice / (tranfer.ExchangeRate ?? 1);
                                        tranferDetail.UnitPrice = rep.UnitPrice;
                                        tranferDetail.AmountOriginal = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0) / (tranfer.ExchangeRate ?? 1),
                                           (tranfer.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                        tranferDetail.Amount = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0),
                                             lamTronGia, MidpointRounding.AwayFromZero);
                                        if (lstrepos.Any(x => x.DetailID == tranferDetail.ID && x.IWQuantity > 0))
                                        {
                                            var inwardTranfer = lstrepos.FirstOrDefault(x => x.DetailID == tranferDetail.ID && x.IWQuantity > 0);
                                            inwardTranfer.UnitPrice = tranferDetail.UnitPrice;
                                            inwardTranfer.IWAmount = tranferDetail.Amount;
                                            Update(inwardTranfer);
                                        }
                                    }

                                }
                                tranfer.TotalAmount = tranfer.RSTransferDetails.Sum(c => c.Amount);
                                tranfer.TotalAmountOriginal = tranfer.RSTransferDetails.Sum(c => c.AmountOriginal);
                                _IRSTranferService.Update(tranfer);
                            }
                            #endregion
                        }
                        #region GeneralLedger
                        var lstGl = lstGeneralLedgers.Where(x => x.DetailID == rep.DetailID).ToList();//_IGeneralLedgerService.GetByDetailID(rep.DetailID ?? Guid.Empty);
                        string OldID = "";
                        string NewID = "";
                        foreach (GeneralLedger g in lstGl)
                        {
                            NewID = g.ID.ToString();
                            if (lstGl.Count >= 1)
                            {
                                OldID = lstGl[0].ID.ToString();
                            }
                            if (new int[] { 320, 321, 322, 323, 324, 325, 412 }.Any(u => u == g.TypeID))
                            {
                                if (g.Account == "632")
                                {
                                    g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                    g.DebitAmount = rep.OWAmount ?? 0;
                                }
                                else if (g.AccountCorresponding == "632")
                                {
                                    g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                    g.CreditAmount = rep.OWAmount ?? 0;
                                }
                            }
                            else if (new int[] { 410, 414 }.Any(u => u == g.TypeID))
                            {
                                if (lstRSInwardOutwards.Any(x => x.ID == g.ReferenceID) && lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.Count > 0)
                                {
                                    var rsOutDetail = lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                    if (rsOutDetail != null)
                                    {
                                        if (g.Account == rsOutDetail.CreditAccount)
                                        {
                                            g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = rep.OWAmount ?? 0;
                                        }
                                        else if (g.Account == rsOutDetail.DebitAccount)
                                        {
                                            g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = rep.OWAmount ?? 0;
                                        }
                                    }
                                }
                            }
                            else if (new int[] { 420, 421, 422 }.Any(u => u == g.TypeID))
                            {
                                if (lstRSTranfer.Any(x => x.ID == g.ReferenceID) && lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.Count > 0)
                                {
                                    var rsOutDetail = lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                    if (rsOutDetail != null)
                                    {
                                        if (rsOutDetail.DebitAccount == rsOutDetail.CreditAccount)//trungnq thêm trường hợp này để check trường hợp tính giá xuất kho khi 2 tài khoản nợ có = nhau bug 6309
                                        {
                                            if (rep.OWAmount != null)
                                            {
                                                if (rep.OWAmount != 0)
                                                {
                                                    if (g.ID.ToString() == OldID)
                                                    {
                                                        g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                        g.CreditAmount = rep.OWAmount ?? 0;

                                                    }
                                                    else
                                                    {
                                                        g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                        g.DebitAmount = rep.OWAmount ?? 0;
                                                    }
                                                }
                                            }
                                        }
                                        else if (g.Account == rsOutDetail.CreditAccount)
                                        {
                                            g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = rep.OWAmount ?? 0;
                                        }
                                        else if (g.Account == rsOutDetail.DebitAccount)
                                        {
                                            g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = rep.OWAmount ?? 0;
                                        }
                                    }
                                }
                            }
                            _IGeneralLedgerService.Update(g);
                        }
                        #endregion
                    }
                }
                #endregion

                CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                return false;
            }
            #endregion
        }
        /// <summary>
        /// TÍnh giá xuất kho theo phương pháp bình quân tức thời
        /// </summary>
        /// <param name="lstMaterialGoods">danh sách VTHH cần tính giá</param>>
        /// <param name="fromDate">ngày bắt đầu kỳ tính giá</param>
        /// <param name="toDate">ngày kết thúc kỳ tính giá</param>
        public bool PricingAndUpdateOW_AverageForInstantaneous(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate)
        {
            try
            {
                #region Lấy các chứng từ cần cập nhật giá xuất DuyNT các chứng từ này phát sinh trong kỳ
                List<SystemOption> lstsys = _ISystemOptionService.Query.Where(c => c.Code == "DDSo_TienVND" || c.Code == "DDSo_NgoaiTe").ToList();
                var ddSoGia = lstsys.FirstOrDefault(c => c.Code == "DDSo_TienVND");
                int lamTronGia = 0;
                if (ddSoGia != null) lamTronGia = Convert.ToInt32(ddSoGia.Data);
                var ddSoGia1 = lstsys.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
                int lamTronGia1 = 0;
                if (ddSoGia != null) lamTronGia1 = Convert.ToInt32(ddSoGia1.Data);

                //danh sách trong kho ban đầu tính giá               
                List<int> lstTypeIW = new List<int>() { 400, 401, 402, 403, 404, 210, 260, 261, 262, 263, 264, 420, 421, 422 };
                List<int> lstTypeOW = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420, 421, 422 };
                #endregion

                #region Lấy các chứng từ cần cập nhật giá xuất DuyNT

                #region RepositoryLedger - GeneralLedger  
                List<RepositoryLedger> lstrepos = GetAll();
                List<RepositoryLedger> lstRepositoryLedgers = lstrepos.Where(c => lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID))
                    .OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime).ToList();

                List<GeneralLedger> lstGeneralLedgers =
                    _IGeneralLedgerService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (lstTypeOW.Contains(c.TypeID)) || c.TypeID == 330).ToList().OrderBy(c => c.PostedDate).ToList();
                #endregion

                #region RSInwardOutward - RSInwardOutwardDetail
                List<RSInwardOutward> lstRSInwardOutwards = _IRSInwardOutwardService.Query.Where(
                        c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                List<RSAssemblyDismantlement> rSAssemblyDismantlements = _IRSAssemblyDismantlementService.GetAll();
                #endregion

                #region RSTranfer - RSTranferDetail
                List<int> lstTypeTranfer = new List<int>() { 420, 421, 422 };
                List<RSTransfer> lstRSTranfer = _IRSTranferService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                List<RepositoryLedger> lstRepositoriesledger = lstrepos.Where(c => (lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID)
                && (c.TypeID == 420 || c.TypeID == 421 || c.TypeID == 422) && c.PostedDate >= fromDate && c.PostedDate <= toDate) || c.TypeID == 403).OrderBy(c => c.PostedDate).ToList();
                #endregion

                #region SAInvoice
                List<int> lstTypeSAInvoices = new List<int>() { 412, 415, 320, 321, 322, 323, 324, 325 };
                List<SAInvoice> lstSAInvoices = _ISAInvoiceService.Query.Where(
                        c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                List<SAReturn> lstSAReturns = _ISAReturnService.Query.Where(c => c.Recorded && c.AutoOWAmountCal == 0).ToList();
                List<SAReturnDetail> lstSAReturnDetails = _ISAReturnDetailService.Query.Where(c => _ISAReturnService.Query.Any(d => d.Recorded && d.AutoOWAmountCal == 0 && d.ID == c.SAReturnID)).ToList();
                List<GeneralLedger> lstGeneralLedgersreturn = lstGeneralLedgers.Where(c => c.TypeID == 330).ToList();
                List<RSInwardOutward> lstRSInwardOutward = _IRSInwardOutwardService.Query.Where(c => c.TypeID == 403).ToList();
                #endregion

                #region PPDiscountRetur               
                List<PPDiscountReturnDetail> lstPPDiscountReturnDetailsAll = _IPPDiscountReturnDetailService.Query.Where(c => _IPPDiscountReturnService.Query.Any(d => d.Recorded && d.TypeID == 230 && d.ID == c.PPDiscountReturnID) && c.ConfrontDetailID != null).ToList();
                #endregion

                #endregion
                foreach (MaterialGoods materialGood in lstMaterialGoods)
                {
                    #region Tính giá xuất cho một VTHH
                    //DS chứng từ VTHH đầu kỳ
                    List<RepositoryLedger> lstRLIWDK = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && c.IWAmount > 0 && c.IWQuantity > 0 && c.TypeID == 702).ToList();//danh sách nhập kho
                    decimal GTDK = lstRLIWDK.Sum(x => Math.Round(x.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero));
                    decimal SLDK = lstRLIWDK.Sum(x => x.IWQuantity) ?? 0;
                    //DS chứng từ VTHH trong kỳ
                    List<RepositoryLedger> lstRLOW = new List<RepositoryLedger>();
                    lstRLOW = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && lstTypeOW.Any(d => d == c.TypeID) && c.OWQuantity > 0 &&
                     c.PostedDate >= fromDate && c.PostedDate <= toDate).OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime.Value.ToOADate()).ToList();//danh sách xuất kho
                    #region Tính giá                  
                    decimal slton = SLDK;
                    decimal gtton = GTDK;
                    decimal dongia = SLDK == 0 ? 0 : GTDK / SLDK;
                    for (int i = 0; i < lstRLOW.Count; i++)
                    {
                        List<RepositoryLedger> lstIW = new List<RepositoryLedger>();
                        lstIW = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && lstTypeIW.Any(d => d == c.TypeID) && c.IWQuantity > 0 &&
                          (c.PostedDate <= lstRLOW[i].PostedDate && c.RefDateTime <= lstRLOW[i].RefDateTime) && (i != 0 ? (c.PostedDate >= lstRLOW[i - 1].PostedDate && c.RefDateTime >= lstRLOW[i - 1].RefDateTime) : true)).ToList();
                        List<PPDiscountReturnDetail> lstPPDiscountReturnDetails = lstPPDiscountReturnDetailsAll.Where(c => (lstIW.Any(d => d.DetailID == c.ConfrontDetailID))).ToList();

                        slton = slton + (lstIW.Sum(x => x.IWQuantity) ?? 0) - (i != 0 ? lstRLOW[i - 1].OWQuantity ?? 0 : 0);
                        gtton = (gtton + (lstIW.Sum(x => Math.Round(x.IWAmount ?? 0, lamTronGia))) - (i != 0 ? Math.Round(lstRLOW[i - 1].OWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero) : 0)
                            - lstPPDiscountReturnDetails.Sum(x => Math.Round(x.Amount ?? 0, lamTronGia)));
                        dongia = slton == 0 ? 0 : gtton / slton;
                        //if (i == 0 && lstIW.Count == 0) dongia = lstRLOW[0].UnitPrice;
                        lstRLOW[i].UnitPrice = dongia;
                        lstRLOW[i].OWAmount = Math.Round((dongia * lstRLOW[i].OWQuantity ?? 0), lamTronGia, MidpointRounding.AwayFromZero);
                    }
                    #endregion

                    #region Update
                    BeginTran();
                    foreach (var x in lstRLOW)
                    {
                        Update(x);
                        if (x.TypeID == 410 || x.TypeID == 414)
                        {
                            #region RSInwardOutward
                            var inwardOutward = lstRSInwardOutwards.FirstOrDefault(c => c.ID == x.ReferenceID);
                            if (inwardOutward != null)
                            {
                                if (lstMaterialGoods.Any(c => inwardOutward.RSInwardOutwardDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                {
                                    foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                                    {

                                        if (inwardOutwardDetail.MaterialGoodsID == x.MaterialGoodsID)
                                        {
                                            inwardOutwardDetail.UnitPriceOriginal = x.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                            inwardOutwardDetail.UnitPrice = x.UnitPrice;
                                            inwardOutwardDetail.AmountOriginal = Math.Round(inwardOutwardDetail.UnitPriceOriginal * (inwardOutwardDetail.Quantity ?? 1),
                                                (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                            inwardOutwardDetail.Amount = Math.Round(inwardOutwardDetail.UnitPrice * (inwardOutwardDetail.Quantity ?? 1), lamTronGia, MidpointRounding.AwayFromZero);
                                        }

                                    }
                                    inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                                    inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                                    _IRSInwardOutwardService.Update(inwardOutward);
                                }
                                if (inwardOutward.RSAssemblyDismantlementID != null)
                                {
                                    var RSAssemblyDismantlement = rSAssemblyDismantlements.FirstOrDefault(c => c.ID == inwardOutward.RSAssemblyDismantlementID);
                                    if (RSAssemblyDismantlement != null && lstMaterialGoods.Any(c => RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (RSAssemblyDismantlementDetail RSAssemblyDismantlementDetail in RSAssemblyDismantlement.RSAssemblyDismantlementDetails)
                                        {

                                            if (RSAssemblyDismantlementDetail.MaterialGoodsID == x.MaterialGoodsID)
                                            {
                                                RSAssemblyDismantlementDetail.UnitPriceOriginal = x.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                RSAssemblyDismantlementDetail.UnitPrice = x.UnitPrice;
                                                RSAssemblyDismantlementDetail.AmountOriginal = Math.Round(RSAssemblyDismantlementDetail.UnitPriceOriginal * (RSAssemblyDismantlementDetail.Quantity ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                RSAssemblyDismantlementDetail.Amount = Math.Round(RSAssemblyDismantlementDetail.UnitPrice * (RSAssemblyDismantlementDetail.Quantity ?? 1),
                                                     lamTronGia, MidpointRounding.AwayFromZero);
                                            }

                                        }
                                        RSAssemblyDismantlement.Amount = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.Amount);
                                        RSAssemblyDismantlement.AmountOriginal = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.AmountOriginal);
                                        _IRSAssemblyDismantlementService.Update(RSAssemblyDismantlement);
                                    }
                                }
                                if (inwardOutward.SAInvoiceID != null)
                                {
                                    var Sainvoice = lstSAInvoices.FirstOrDefault(c => c.ID == inwardOutward.SAInvoiceID);//_ISAInvoiceService.Getbykey(inwardOutward.SAInvoiceID ?? Guid.Empty);
                                    if (Sainvoice != null && lstMaterialGoods.Any(c => Sainvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (SAInvoiceDetail sAInvoiceDetail in Sainvoice.SAInvoiceDetails)
                                        {
                                            if (sAInvoiceDetail.MaterialGoodsID == x.MaterialGoodsID)
                                            {
                                                sAInvoiceDetail.OWPriceOriginal = x.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                sAInvoiceDetail.OWPrice = x.UnitPrice;
                                                sAInvoiceDetail.OWAmountOriginal = Math.Round(sAInvoiceDetail.OWPriceOriginal * (sAInvoiceDetail.Quantity ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                sAInvoiceDetail.OWAmount = Math.Round(sAInvoiceDetail.OWPrice * (sAInvoiceDetail.Quantity ?? 1),
                                                    lamTronGia, MidpointRounding.AwayFromZero); ;
                                            }
                                        }
                                        Sainvoice.TotalCapitalAmount = Sainvoice.SAInvoiceDetails.Sum(c => c.Amount);
                                        Sainvoice.TotalCapitalAmountOriginal = Sainvoice.SAInvoiceDetails.Sum(c => c.AmountOriginal);
                                        _ISAInvoiceService.Update(Sainvoice);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (new int[] { 412 }.Any(a => a == x.TypeID))
                        {
                            #region SAInvoice
                            var sAInvoice = lstSAInvoices.FirstOrDefault(c => c.ID == x.ReferenceID);
                            if (sAInvoice != null && lstMaterialGoods.Any(c => sAInvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                            {
                                foreach (SAInvoiceDetail sAInvoiceDetails in sAInvoice.SAInvoiceDetails)
                                {
                                    if (sAInvoiceDetails.MaterialGoodsID == x.MaterialGoodsID)
                                    {
                                        sAInvoiceDetails.OWPriceOriginal = x.UnitPrice / (sAInvoice.ExchangeRate ?? 1);
                                        sAInvoiceDetails.OWPrice = x.UnitPrice;
                                        sAInvoiceDetails.OWAmountOriginal = Math.Round(sAInvoiceDetails.OWPriceOriginal * (sAInvoiceDetails.Quantity ?? 1),
                                            (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                        sAInvoiceDetails.OWAmount = Math.Round(sAInvoiceDetails.OWPrice * (sAInvoiceDetails.Quantity ?? 1),
                                             lamTronGia, MidpointRounding.AwayFromZero);
                                        SAReturnDetail sAReturnDetails = lstSAReturnDetails.FirstOrDefault(d => d.SAInvoiceDetailID == sAInvoiceDetails.ID);//update hang mua trả lại
                                        if (sAReturnDetails != null)
                                        {
                                            var sAReturn1 = lstSAReturns.FirstOrDefault(c => c.ID == sAReturnDetails.SAReturnID && c.Recorded && c.AutoOWAmountCal == 0);
                                            if (sAReturn1 != null)
                                            {
                                                sAReturnDetails.OWPriceOriginal = x.UnitPrice / (sAReturn1.ExchangeRate ?? 1);
                                                sAReturnDetails.OWPrice = x.UnitPrice;

                                                sAReturnDetails.OWAmountOriginal = Math.Round(sAInvoiceDetails.OWPriceOriginal * (sAReturnDetails.Quantity ?? 1),
                                            (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                sAReturnDetails.OWAmount = Math.Round(sAInvoiceDetails.OWPrice * (sAReturnDetails.Quantity ?? 1),
                                             lamTronGia, MidpointRounding.AwayFromZero);

                                                _ISAReturnDetailService.Update(sAReturnDetails);

                                                sAReturn1.TotalOWAmount = sAReturn1.SAReturnDetails.Sum(c => c.OWAmount);
                                                sAReturn1.TotalOWAmountOriginal = sAReturn1.SAReturnDetails.Sum(c => c.OWAmountOriginal);

                                                _ISAReturnService.Update(sAReturn1);
                                                var rsInward = lstRSInwardOutward.FirstOrDefault(c => c.ID == sAReturn1.ID);
                                                if (rsInward != null)
                                                {
                                                    rsInward.TotalAmount = sAReturn1.TotalOWAmount;
                                                    rsInward.TotalAmountOriginal = sAReturn1.TotalOWAmountOriginal;
                                                    _IRSInwardOutwardService.Update(rsInward);
                                                }
                                                var lstGeneralLedgerReturn = lstGeneralLedgersreturn.Where(c => c.DetailID == sAReturnDetails.ID);
                                                foreach (GeneralLedger g in lstGeneralLedgerReturn)
                                                {
                                                    if (g.Account == "632")
                                                    {
                                                        g.CreditAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                        g.CreditAmount = sAReturnDetails.OWAmount;

                                                    }
                                                    else if (g.AccountCorresponding == "632")
                                                    {
                                                        g.DebitAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                        g.DebitAmount = sAReturnDetails.OWAmount;
                                                    }
                                                    _IGeneralLedgerService.Update(g);
                                                }
                                                var ReposReturn = lstRepositoriesledger.FirstOrDefault(c => c.DetailID == sAReturnDetails.ID);
                                                if (ReposReturn != null)
                                                {
                                                    ReposReturn.UnitPrice = sAReturnDetails.OWPrice;
                                                    ReposReturn.IWAmount = sAReturnDetails.OWAmount;
                                                    ReposReturn.IWAmountBalance = sAReturnDetails.OWAmount;
                                                    Update(ReposReturn);
                                                }
                                            }

                                        }
                                    }

                                }
                                sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmount);
                                sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmountOriginal);
                                _ISAInvoiceService.Update(sAInvoice);
                            }
                            if (sAInvoice.OutwardNo != null && !string.IsNullOrEmpty(sAInvoice.OutwardNo))
                            {
                                var outWard = _IRSInwardOutwardService.Query.FirstOrDefault(c => c.ID == sAInvoice.ID);
                                if (outWard != null)
                                {
                                    outWard.TotalAmount = sAInvoice.TotalCapitalAmount;
                                    outWard.TotalAmountOriginal = sAInvoice.TotalCapitalAmountOriginal;
                                    _IRSInwardOutwardService.Update(outWard);
                                }

                            }
                            #endregion
                        }
                        else if (x.TypeID == 420 || x.TypeID == 421 || x.TypeID == 422)
                        {
                            #region RSTranfer
                            var tranfer = lstRSTranfer.FirstOrDefault(c => c.ID == x.ReferenceID);
                            if (tranfer != null && lstMaterialGoods.Any(c => tranfer.RSTransferDetails.Any(d => d.MaterialGoodsID == c.ID)))
                            {
                                foreach (RSTransferDetail tranferDetail in tranfer.RSTransferDetails)
                                {

                                    if (tranferDetail.MaterialGoodsID == x.MaterialGoodsID)
                                    {
                                        tranferDetail.UnitPriceOriginal = x.UnitPrice / (tranfer.ExchangeRate ?? 1);
                                        tranferDetail.UnitPrice = x.UnitPrice;
                                        tranferDetail.AmountOriginal = Math.Round(tranferDetail.UnitPriceOriginal * (tranferDetail.Quantity ?? 1),
                                            (tranfer.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                        tranferDetail.Amount = Math.Round(tranferDetail.UnitPrice * (tranferDetail.Quantity ?? 1),
                                             lamTronGia, MidpointRounding.AwayFromZero);
                                    }

                                }
                                tranfer.TotalAmount = tranfer.RSTransferDetails.Sum(c => c.Amount);
                                tranfer.TotalAmountOriginal = tranfer.RSTransferDetails.Sum(c => c.AmountOriginal);
                                _IRSTranferService.Update(tranfer);
                            }
                            #endregion
                        }
                        #region GeneralLedger
                        string OldID = "";
                        var lstgl = lstGeneralLedgers.Where(c => c.DetailID == x.DetailID).ToList();
                        foreach (GeneralLedger g in lstgl)
                        {
                            if (lstgl.Count >= 1)
                            {
                                OldID = lstgl[0].ID.ToString();
                            }
                            if (new int[] { 320, 321, 322, 323, 324, 325, 412 }.Any(u => u == g.TypeID))
                            {
                                if (g.Account == "632")
                                {
                                    g.DebitAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                    g.DebitAmount = x.OWAmount ?? 0;
                                }
                                else if (g.AccountCorresponding == "632")
                                {
                                    g.CreditAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                    g.CreditAmount = x.OWAmount ?? 0;
                                }
                            }
                            else if (new int[] { 410, 414 }.Any(u => u == g.TypeID))
                            {
                                if (lstRSInwardOutwards.Any(c => c.ID == g.ReferenceID) && lstRSInwardOutwards.FirstOrDefault(c => c.ID == g.ReferenceID).RSInwardOutwardDetails.Count > 0)
                                {
                                    var rsOutDetail = lstRSInwardOutwards.FirstOrDefault(c => c.ID == g.ReferenceID).RSInwardOutwardDetails.FirstOrDefault(c => c.ID == g.DetailID);
                                    if (rsOutDetail != null)
                                    {
                                        if (g.Account == rsOutDetail.CreditAccount)
                                        {
                                            g.CreditAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = x.OWAmount ?? 0;
                                        }
                                        else if (g.Account == rsOutDetail.DebitAccount)
                                        {
                                            g.DebitAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = x.OWAmount ?? 0;
                                        }
                                    }
                                }
                            }
                            else if (new int[] { 420, 421, 422 }.Any(u => u == g.TypeID))
                            {
                                if (lstRSTranfer.Any(c => c.ID == g.ReferenceID) && lstRSTranfer.FirstOrDefault(c => c.ID == g.ReferenceID).RSTransferDetails.Count > 0)
                                {
                                    var rsOutDetail = lstRSTranfer.FirstOrDefault(c => c.ID == g.ReferenceID).RSTransferDetails.FirstOrDefault(c => c.ID == g.DetailID);
                                    if (rsOutDetail != null)
                                    {
                                        if (rsOutDetail.DebitAccount == rsOutDetail.CreditAccount)//trungnq thêm trường hợp này để check trường hợp tính giá xuất kho khi 2 tài khoản nợ có = nhau bug 6309
                                        {
                                            if (x.OWAmount != null)
                                            {
                                                if (x.OWAmount != 0)
                                                {
                                                    if (g.ID.ToString() == OldID)
                                                    {
                                                        g.CreditAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                        g.CreditAmount = x.OWAmount ?? 0;

                                                    }
                                                    else
                                                    {
                                                        g.DebitAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                        g.DebitAmount = x.OWAmount ?? 0;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        if (g.Account == rsOutDetail.CreditAccount)
                                        {
                                            g.CreditAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = x.OWAmount ?? 0;
                                        }
                                        else if (g.Account == rsOutDetail.DebitAccount)
                                        {
                                            g.DebitAmountOriginal = Math.Round((x.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = x.OWAmount ?? 0;
                                        }
                                    }
                                }
                            }
                            _IGeneralLedgerService.Update(g);
                        }
                        #endregion

                    }
                    CommitTran();
                    #endregion

                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                return false;
            }
        }
        /// <summary>
        /// TÍnh giá xuất kho theo phương pháp nhập trước xuất trước
        /// </summary>
        /// <param name="lstMaterialGoods">danh sách VTHH cần tính giá</param>>
        /// <param name="fromDate">ngày bắt đầu kỳ tính giá</param>
        /// <param name="toDate">ngày kết thúc kỳ tính giá</param>
        public bool PricingAndUpdateOW_InFirstOutFirst(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate)
        {
            try
            {
                #region Lấy các chứng từ cần cập nhật giá xuất DuyNT các chứng từ này phát sinh trong kỳ
                List<SystemOption> lstsys = _ISystemOptionService.Query.Where(c => c.Code == "DDSo_TienVND" || c.Code == "DDSo_NgoaiTe").ToList();
                var ddSoGia = lstsys.FirstOrDefault(c => c.Code == "DDSo_TienVND");
                int lamTronGia = 0;
                if (ddSoGia != null) lamTronGia = Convert.ToInt32(ddSoGia.Data);
                var ddSoGia1 = lstsys.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
                int lamTronGia1 = 0;
                if (ddSoGia != null) lamTronGia1 = Convert.ToInt32(ddSoGia1.Data);
                //danh sách trong kho ban đầu tính giá            
                List<int> lstTypeIW = new List<int>() { 400, 401, 402, 403, 404, 210, 260, 261, 262, 263, 264, 420, 421, 422 };
                List<int> lstTypeOW = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420, 421, 422 };
                #endregion
                #region Lấy các chứng từ cần cập nhật giá xuất DuyNT

                #region RepositoryLedger - GeneralLedger  
                List<RepositoryLedger> lstrepos = GetAll();
                List<RepositoryLedger> lstRepositoryLedgers = lstrepos.Where(c => lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID))
                    .OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime).ToList();

                List<GeneralLedger> lstGeneralLedgers =
                    _IGeneralLedgerService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (lstTypeOW.Contains(c.TypeID)) || c.TypeID == 330).ToList().OrderBy(c => c.PostedDate).ToList();
                #endregion

                #region RSInwardOutward - RSInwardOutwardDetail
                List<RSInwardOutward> lstRSInwardOutwards = _IRSInwardOutwardService.Query.Where(
                        c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                List<RSAssemblyDismantlement> rSAssemblyDismantlements = _IRSAssemblyDismantlementService.GetAll();
                #endregion

                #region RSTranfer - RSTranferDetail
                List<int> lstTypeTranfer = new List<int>() { 420, 421, 422 };
                List<RSTransfer> lstRSTranfer = _IRSTranferService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                List<RepositoryLedger> lstRepositoriesledger = lstrepos.Where(c => (lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID)
                && (c.TypeID == 420 || c.TypeID == 421 || c.TypeID == 422) && c.PostedDate >= fromDate && c.PostedDate <= toDate) || c.TypeID == 403).OrderBy(c => c.PostedDate).ToList();
                #endregion

                #region SAInvoice
                List<int> lstTypeSAInvoices = new List<int>() { 412, 415, 320, 321, 322, 323, 324, 325 };
                List<SAInvoice> lstSAInvoices = _ISAInvoiceService.Query.Where(
                        c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                List<SAReturn> lstSAReturns = _ISAReturnService.Query.Where(c => c.Recorded && c.AutoOWAmountCal == 0).ToList();
                List<SAReturnDetail> lstSAReturnDetails = _ISAReturnDetailService.Query.Where(c => _ISAReturnService.Query.Any(d => d.Recorded && d.AutoOWAmountCal == 0 && d.ID == c.SAReturnID)).ToList();
                List<GeneralLedger> lstGeneralLedgersreturn = lstGeneralLedgers.Where(c => c.TypeID == 330).ToList();
                List<RSInwardOutward> lstRSInwardOutward = _IRSInwardOutwardService.Query.Where(c => c.TypeID == 403).ToList();
                #endregion

                #region PPDiscountRetur               
                List<PPDiscountReturnDetail> lstPPDiscountReturnDetailsAll = _IPPDiscountReturnDetailService.Query.Where(c => _IPPDiscountReturnService.Query.Any(d => d.Recorded && d.TypeID == 230 && d.ID == c.PPDiscountReturnID) && c.ConfrontDetailID != null).ToList();
                #endregion
                List<string> lstFail = new List<string>();
                bool ctn = true;
                #endregion
                foreach (MaterialGoods materialGood in lstMaterialGoods)
                {
                    #region Tính giá xuất cho một VTHH
                    //DS chứng từ VTHH đầu kỳ
                    List<RepositoryLedger> lstRLIWDK = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && c.IWAmount > 0 && c.IWQuantity > 0 && c.TypeID == 702).ToList();
                    decimal GTDK = lstRLIWDK.Sum(x => Math.Round(x.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero));
                    decimal SLDK = lstRLIWDK.Sum(x => x.IWQuantity) ?? 0;
                    decimal DGDK = SLDK > 0 ? GTDK / SLDK : 0;
                    //DS chứng từ VTHH trong kỳ
                    List<RepositoryLedger> lstRLOW = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && lstTypeOW.Any(d => d == c.TypeID) && c.OWQuantity > 0 &&
                         c.PostedDate <= toDate).OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime.Value.ToOADate()).ToList();//danh sách xuất kho
                    List<RepositoryLedger> lstRLIW1 = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && lstTypeIW.Any(d => d == c.TypeID) && c.IWQuantity > 0 &&
                         c.PostedDate <= toDate).OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime.Value.ToOADate()).ToList();//danh sách nhập kho
                    List<RepositoryLedger> lstRLIW = lstRLIW1.CloneObject();
                    if (lstRLIWDK.Count() == 0 && lstRLIW.Count() == 0 && lstRLOW.Count() > 0)
                    {
                        if (DialogResult.Yes == MessageBox.Show("VTHH " + materialGood.MaterialGoodsCode + " bị tồn kho âm./n Nếu ấn 'Đồng ý' hệ thống sẽ bỏ qua những hàng hóa bị âm kho, chỉ thực hiện cho những hàng hóa không bị âm số lượng tồn. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            lstFail.Add(materialGood.MaterialGoodsCode);
                            continue;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    foreach (RepositoryLedger repositoryLedger in lstRLIW)
                    {
                        List<PPDiscountReturnDetail> pPDiscountReturnDetail = lstPPDiscountReturnDetailsAll.Where(c => (c.ConfrontDetailID ?? Guid.NewGuid()) == repositoryLedger.DetailID).ToList();
                        if (pPDiscountReturnDetail.Count() > 0)
                        {
                            repositoryLedger.IWAmount = repositoryLedger.IWAmount - pPDiscountReturnDetail.Sum(t => t.Amount);
                        }
                    }
                    #region Tính giá                  

                    int j = 0;
                    decimal sl = lstRLIW.Count > 0 ? lstRLIW[0].IWQuantity ?? 0 : 0;
                    decimal gt = lstRLIW.Count > 0 ? Math.Round(lstRLIW[0].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero) : 0;
                    for (int i = 0; i < lstRLOW.Count; i++)
                    {
                        bool check = true;
                        try
                        {
                            if (SLDK > 0)
                            {
                                if (lstRLOW[i].OWQuantity <= SLDK)
                                {
                                    SLDK = SLDK - lstRLOW[i].OWQuantity ?? 0;
                                    lstRLOW[i].UnitPrice = DGDK;
                                    if (SLDK == 0)
                                    {
                                        lstRLOW[i].OWAmount = GTDK;
                                        GTDK = GTDK - Math.Round(lstRLOW[i].OWAmount ?? 0, lamTronGia);
                                    }
                                    else
                                    {
                                        lstRLOW[i].OWAmount = lstRLOW[i].OWQuantity * lstRLOW[i].UnitPrice;
                                        GTDK = GTDK - Math.Round(lstRLOW[i].OWAmount ?? 0, lamTronGia);
                                    }
                                }
                                else
                                {
                                    while (sl < (lstRLOW[i].OWQuantity - SLDK))
                                    {
                                        if (lstFail.Contains(materialGood.MaterialGoodsCode))
                                        {
                                            ctn = false;
                                            break;
                                        }
                                        j++;
                                        if (lstRLIW.Count() < j)
                                        {
                                            if (DialogResult.Yes == MessageBox.Show("VTHH " + materialGood.MaterialGoodsCode + " bị tồn kho âm./n Nếu ấn 'Đồng ý' hệ thống sẽ bỏ qua những hàng hóa bị âm kho, chỉ thực hiện cho những hàng hóa không bị âm số lượng tồn. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                            {
                                                lstFail.Add(materialGood.MaterialGoodsCode);
                                                ctn = false;
                                                break;
                                            }
                                            else
                                            {
                                                return false;
                                            }
                                        }
                                        sl = sl + lstRLIW[j].IWQuantity ?? 0;
                                        gt = gt + Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    }
                                    if (!ctn)
                                    {
                                        continue;
                                    }
                                    lstRLIW[j].IWQuantity = sl - (lstRLOW[i].OWQuantity - SLDK);
                                    lstRLIW[j].IWAmount = Math.Round((lstRLIW[j].IWQuantity * lstRLIW[j].UnitPrice) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    lstRLOW[i].OWAmount = gt - Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero) + GTDK;
                                    lstRLOW[i].UnitPrice = (lstRLOW[i].OWAmount ?? 0) / (lstRLOW[i].OWQuantity ?? 1);
                                    sl = lstRLIW[j].IWQuantity ?? 0;
                                    gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    SLDK = 0;
                                    GTDK = 0;
                                }
                            }
                            else
                            {
                                while (sl < lstRLOW[i].OWQuantity)
                                {
                                    if (lstFail.Contains(materialGood.MaterialGoodsCode))
                                    {
                                        ctn = false;
                                        break;
                                    }
                                    j++;
                                    if (lstRLIW.Count() < j)
                                    {
                                        if (DialogResult.Yes == MessageBox.Show("VTHH " + materialGood.MaterialGoodsCode + " bị tồn kho âm./n Nếu ấn 'Đồng ý' hệ thống sẽ bỏ qua những hàng hóa bị âm kho, chỉ thực hiện cho những hàng hóa không bị âm số lượng tồn. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                        {
                                            lstFail.Add(materialGood.MaterialGoodsCode);
                                            ctn = false;
                                            break;
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                    }
                                    sl = sl + lstRLIW[j].IWQuantity ?? 0;
                                    gt = gt + Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    check = false;
                                }
                                if (!ctn)
                                {
                                    continue;
                                }
                                if (check)
                                {
                                    lstRLOW[i].UnitPrice = lstRLIW[j].UnitPrice;
                                    lstRLIW[j].IWQuantity = sl - lstRLOW[i].OWQuantity;
                                    if (lstRLIW[j].IWQuantity == 0)
                                    {
                                        lstRLOW[i].OWAmount = gt;
                                        lstRLIW[j].IWAmount = gt - lstRLOW[i].OWAmount;
                                        if (lstRLIW.Count - 1 > j)
                                        {
                                            j++;
                                            sl = lstRLIW[j].IWQuantity ?? 0;
                                            gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            sl = 0;
                                            gt = 0;
                                        }
                                    }
                                    else
                                    {
                                        lstRLOW[i].OWAmount = Math.Round((lstRLOW[i].UnitPrice * lstRLOW[i].OWQuantity) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        lstRLIW[j].IWAmount = gt - lstRLOW[i].OWAmount;
                                        sl = lstRLIW[j].IWQuantity ?? 0;
                                        gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    }
                                }
                                else
                                {
                                    lstRLIW[j].IWQuantity = sl - lstRLOW[i].OWQuantity;
                                    lstRLIW[j].IWAmount = Math.Round((lstRLIW[j].IWQuantity * lstRLIW[j].UnitPrice) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    lstRLOW[i].OWAmount = gt - Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    lstRLOW[i].UnitPrice = (lstRLOW[i].OWAmount ?? 0) / (lstRLOW[i].OWQuantity ?? 1);
                                    sl = lstRLIW[j].IWQuantity ?? 0;
                                    gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            return false;
                        }
                    }
                    #endregion
                    if (!ctn)
                    {
                        continue;
                    }
                    #region Update
                    BeginTran();
                    foreach (var rep in lstRLOW)
                    {
                        if (rep.PostedDate >= fromDate)
                        {
                            Update(rep);
                            if (rep.TypeID == 410 || rep.TypeID == 414)
                            {
                                #region RSInwardOutward
                                var inwardOutward = lstRSInwardOutwards.FirstOrDefault(x => x.ID == rep.ReferenceID);
                                if (inwardOutward != null)
                                {
                                    if (lstMaterialGoods.Any(c => inwardOutward.RSInwardOutwardDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                                        {

                                            if (inwardOutwardDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                            {
                                                inwardOutwardDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                inwardOutwardDetail.UnitPrice = rep.UnitPrice;
                                                inwardOutwardDetail.AmountOriginal = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                    (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                inwardOutwardDetail.Amount = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0), lamTronGia, MidpointRounding.AwayFromZero);
                                            }

                                        }
                                        inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                                        inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                                        _IRSInwardOutwardService.Update(inwardOutward);
                                    }
                                    if (inwardOutward.RSAssemblyDismantlementID != null)
                                    {
                                        var RSAssemblyDismantlement = rSAssemblyDismantlements.FirstOrDefault(c => c.ID == inwardOutward.RSAssemblyDismantlementID);
                                        if (RSAssemblyDismantlement != null && lstMaterialGoods.Any(c => RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                        {
                                            foreach (RSAssemblyDismantlementDetail RSAssemblyDismantlementDetail in RSAssemblyDismantlement.RSAssemblyDismantlementDetails)
                                            {

                                                if (RSAssemblyDismantlementDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                                {
                                                    RSAssemblyDismantlementDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                    RSAssemblyDismantlementDetail.UnitPrice = rep.UnitPrice;
                                                    RSAssemblyDismantlementDetail.AmountOriginal = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                        (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    RSAssemblyDismantlementDetail.Amount = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0),
                                                         lamTronGia, MidpointRounding.AwayFromZero);
                                                }

                                            }
                                            RSAssemblyDismantlement.Amount = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.Amount);
                                            RSAssemblyDismantlement.AmountOriginal = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.AmountOriginal);
                                            _IRSAssemblyDismantlementService.Update(RSAssemblyDismantlement);
                                        }
                                    }
                                    if (inwardOutward.SAInvoiceID != null)
                                    {
                                        var Sainvoice = lstSAInvoices.FirstOrDefault(x => x.ID == inwardOutward.SAInvoiceID);
                                        if (Sainvoice != null && lstMaterialGoods.Any(c => Sainvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                        {
                                            foreach (SAInvoiceDetail sAInvoiceDetail in Sainvoice.SAInvoiceDetails)
                                            {
                                                if (sAInvoiceDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                                {
                                                    sAInvoiceDetail.OWPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                    sAInvoiceDetail.OWPrice = rep.UnitPrice;
                                                    sAInvoiceDetail.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                        (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    sAInvoiceDetail.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0),
                                                       lamTronGia, MidpointRounding.AwayFromZero);
                                                }
                                            }
                                            Sainvoice.TotalCapitalAmount = Sainvoice.SAInvoiceDetails.Sum(c => c.Amount);
                                            Sainvoice.TotalCapitalAmountOriginal = Sainvoice.SAInvoiceDetails.Sum(c => c.AmountOriginal);
                                            _ISAInvoiceService.Update(Sainvoice);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (new int[] { 412 }.Any(a => a == rep.TypeID))
                            {
                                #region SAInvoice
                                var sAInvoice = lstSAInvoices.FirstOrDefault(x => x.ID == rep.ReferenceID);
                                if (sAInvoice != null && lstMaterialGoods.Any(c => sAInvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                {
                                    foreach (SAInvoiceDetail sAInvoiceDetails in sAInvoice.SAInvoiceDetails)
                                    {
                                        if (sAInvoiceDetails.MaterialGoodsID == rep.MaterialGoodsID)
                                        {
                                            sAInvoiceDetails.OWPriceOriginal = rep.UnitPrice / (sAInvoice.ExchangeRate ?? 1);
                                            sAInvoiceDetails.OWPrice = rep.UnitPrice;
                                            sAInvoiceDetails.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                                (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                            sAInvoiceDetails.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0),
                                                 lamTronGia, MidpointRounding.AwayFromZero);
                                            SAReturnDetail sAReturnDetails = lstSAReturnDetails.FirstOrDefault(c => c.SAInvoiceDetailID == sAInvoiceDetails.ID);//update hang mua trả lại
                                            if (sAReturnDetails != null)
                                            {
                                                var sAReturn = lstSAReturns.FirstOrDefault(c => c.ID == sAReturnDetails.SAReturnID && c.Recorded && c.AutoOWAmountCal == 0 && c.SAReturnDetails.Any(d => d.SAInvoiceDetailID == sAInvoiceDetails.ID));
                                                if (sAReturn != null)
                                                {
                                                    sAReturnDetails.OWPriceOriginal = rep.UnitPrice / (sAReturn.ExchangeRate ?? 1);
                                                    sAReturnDetails.OWPrice = rep.UnitPrice;
                                                    if (sAInvoiceDetails.Quantity == sAReturnDetails.Quantity)
                                                    {
                                                        sAReturnDetails.OWAmountOriginal = Math.Round((rep.OWAmount ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                                                                        (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                        sAReturnDetails.OWAmount = Math.Round((rep.OWAmount ?? 0),
                                                                                    lamTronGia, MidpointRounding.AwayFromZero);
                                                    }
                                                    else
                                                    {
                                                        sAReturnDetails.OWAmountOriginal = Math.Round(sAInvoiceDetails.OWPriceOriginal * (sAReturnDetails.Quantity ?? 1),
                                                                                        (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                        sAReturnDetails.OWAmount = Math.Round(sAInvoiceDetails.OWPrice * (sAReturnDetails.Quantity ?? 1),
                                                                                    lamTronGia, MidpointRounding.AwayFromZero);
                                                    }
                                                    _ISAReturnDetailService.Update(sAReturnDetails);
                                                    sAReturn.TotalOWAmount = sAReturn.SAReturnDetails.Sum(c => c.OWAmount);
                                                    sAReturn.TotalOWAmountOriginal = sAReturn.SAReturnDetails.Sum(c => c.OWAmountOriginal);
                                                    _ISAReturnService.Update(sAReturn);
                                                    var rsInward = lstRSInwardOutward.FirstOrDefault(c => c.ID == sAReturn.ID);
                                                    if (rsInward != null)
                                                    {
                                                        rsInward.TotalAmount = sAReturn.TotalOWAmount;
                                                        rsInward.TotalAmountOriginal = sAReturn.TotalOWAmountOriginal;
                                                        _IRSInwardOutwardService.Update(rsInward);
                                                    }
                                                    var lstGeneralLedgerReturn = lstGeneralLedgersreturn.Where(c => c.DetailID == sAReturnDetails.ID);
                                                    foreach (GeneralLedger g in lstGeneralLedgerReturn)
                                                    {
                                                        if (g.Account == "632")
                                                        {
                                                            g.CreditAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                            g.CreditAmount = sAReturnDetails.OWAmount;

                                                        }
                                                        else if (g.AccountCorresponding == "632")
                                                        {
                                                            g.DebitAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                            g.DebitAmount = sAReturnDetails.OWAmount;
                                                        }
                                                        _IGeneralLedgerService.Update(g);
                                                    }
                                                    var ReposReturn = lstRepositoriesledger.FirstOrDefault(c => c.DetailID == sAReturnDetails.ID);
                                                    if (ReposReturn != null)
                                                    {
                                                        ReposReturn.UnitPrice = sAReturnDetails.OWPrice;
                                                        ReposReturn.IWAmount = sAReturnDetails.OWAmount;
                                                        ReposReturn.IWAmountBalance = sAReturnDetails.OWAmount;
                                                        Update(ReposReturn);
                                                    }
                                                }
                                            }
                                        }
                                        sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmount);
                                        sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmountOriginal);
                                        _ISAInvoiceService.Update(sAInvoice);
                                    }
                                    if (sAInvoice.OutwardNo != null && !string.IsNullOrEmpty(sAInvoice.OutwardNo))
                                    {
                                        var outWard = lstRSInwardOutwards.FirstOrDefault(x => x.ID == sAInvoice.ID);//_IRSInwardOutwardService.Query.FirstOrDefault(c => c.ID == sAInvoice.ID);
                                        if (outWard != null)
                                        {
                                            outWard.TotalAmount = sAInvoice.TotalCapitalAmount;
                                            outWard.TotalAmountOriginal = sAInvoice.TotalCapitalAmountOriginal;
                                            _IRSInwardOutwardService.Update(outWard);
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (rep.TypeID == 420 || rep.TypeID == 421 || rep.TypeID == 422)
                            {
                                #region RSTranfer
                                var tranfer = lstRSTranfer.FirstOrDefault(x => x.ID == rep.ReferenceID);

                                if (tranfer != null && lstMaterialGoods.Any(c => tranfer.RSTransferDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                {
                                    foreach (RSTransferDetail tranferDetail in tranfer.RSTransferDetails)
                                    {
                                        if (tranferDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                        {
                                            tranferDetail.UnitPriceOriginal = rep.UnitPrice / (tranfer.ExchangeRate ?? 1);
                                            tranferDetail.UnitPrice = rep.UnitPrice;
                                            tranferDetail.AmountOriginal = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0) / (tranfer.ExchangeRate ?? 1),
                                               (tranfer.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                            tranferDetail.Amount = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0),
                                                 lamTronGia, MidpointRounding.AwayFromZero);
                                        }

                                    }
                                    tranfer.TotalAmount = tranfer.RSTransferDetails.Sum(c => c.Amount);
                                    tranfer.TotalAmountOriginal = tranfer.RSTransferDetails.Sum(c => c.AmountOriginal);
                                    _IRSTranferService.Update(tranfer);
                                }
                                #endregion
                            }
                            #region GeneralLedger
                            string OldID = "";
                            var lstGl = lstGeneralLedgers.Where(x => x.DetailID == rep.DetailID).ToList();//_IGeneralLedgerService.GetByDetailID(rep.DetailID ?? Guid.Empty);
                            foreach (GeneralLedger g in lstGl)
                            {
                                if (lstGl.Count >= 1)
                                {
                                    OldID = lstGl[0].ID.ToString();
                                }
                                if (new int[] { 320, 321, 322, 323, 324, 325, 412 }.Any(u => u == g.TypeID))
                                {
                                    if (g.Account == "632")
                                    {
                                        g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                        g.DebitAmount = rep.OWAmount ?? 0;
                                    }
                                    else if (g.AccountCorresponding == "632")
                                    {
                                        g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                        g.CreditAmount = rep.OWAmount ?? 0;
                                    }
                                }
                                else if (new int[] { 410, 414 }.Any(u => u == g.TypeID))
                                {
                                    if (lstRSInwardOutwards.Any(x => x.ID == g.ReferenceID) && lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.Count > 0)
                                    {
                                        var rsOutDetail = lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                        if (rsOutDetail != null)
                                        {
                                            if (g.Account == rsOutDetail.CreditAccount)
                                            {
                                                g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                g.CreditAmount = rep.OWAmount ?? 0;
                                            }
                                            else if (g.Account == rsOutDetail.DebitAccount)
                                            {
                                                g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                g.DebitAmount = rep.OWAmount ?? 0;
                                            }
                                        }
                                    }
                                }
                                else if (new int[] { 420, 421, 422 }.Any(u => u == g.TypeID))
                                {
                                    if (lstRSTranfer.Any(x => x.ID == g.ReferenceID) && lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.Count > 0)
                                    {
                                        var rsOutDetail = lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                        if (rsOutDetail != null)
                                        {
                                            if (rsOutDetail.DebitAccount == rsOutDetail.CreditAccount)//trungnq thêm trường hợp này để check trường hợp tính giá xuất kho khi 2 tài khoản nợ có = nhau bug 6309
                                            {
                                                if (rep.OWAmount != null)
                                                {
                                                    if (rep.OWAmount != 0)
                                                    {
                                                        if (g.ID.ToString() == OldID)
                                                        {
                                                            g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                            g.CreditAmount = rep.OWAmount ?? 0;

                                                        }
                                                        else
                                                        {
                                                            g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                            g.DebitAmount = rep.OWAmount ?? 0;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            if (g.Account == rsOutDetail.CreditAccount)
                                            {
                                                g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                g.CreditAmount = rep.OWAmount ?? 0;
                                            }
                                            else if (g.Account == rsOutDetail.DebitAccount)
                                            {
                                                g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                g.DebitAmount = rep.OWAmount ?? 0;
                                            }
                                        }
                                    }
                                }
                                _IGeneralLedgerService.Update(g);
                            }
                            #endregion
                        }
                    }
                    CommitTran();
                    #endregion

                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                return false;
            }
        }

        public bool PricingAndUpdateOW_InFirstOutFirst(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate, Guid repositoryID)
        {
            try
            {
                #region Lấy các chứng từ cần cập nhật giá xuất DuyNT các chứng từ này phát sinh trong kỳ
                List<SystemOption> lstsys = _ISystemOptionService.Query.Where(c => c.Code == "DDSo_TienVND" || c.Code == "DDSo_NgoaiTe").ToList();
                var ddSoGia = lstsys.FirstOrDefault(c => c.Code == "DDSo_TienVND");
                int lamTronGia = 0;
                if (ddSoGia != null) lamTronGia = Convert.ToInt32(ddSoGia.Data);
                var ddSoGia1 = lstsys.FirstOrDefault(c => c.Code == "DDSo_NgoaiTe");
                int lamTronGia1 = 0;
                if (ddSoGia != null) lamTronGia1 = Convert.ToInt32(ddSoGia1.Data);
                //danh sách trong kho ban đầu tính giá            
                List<int> lstTypeIW = new List<int>() { 400, 401, 402, 403, 404, 210, 260, 261, 262, 263, 264, 420, 421, 422 };
                List<int> lstTypeOW = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420, 421, 422 };
                #endregion
                #region Lấy các chứng từ cần cập nhật giá xuất DuyNT

                #region RepositoryLedger - GeneralLedger  
                List<RepositoryLedger> lstrepos = Query.Where(x => x.RepositoryID == repositoryID).ToList();
                if (lstrepos.Count() > 0)
                {
                    List<RepositoryLedger> lstRepositoryLedgers = lstrepos.Where(c => lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID))
                    .OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime).ToList();

                    List<GeneralLedger> lstGeneralLedgers =
                        _IGeneralLedgerService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && (lstTypeOW.Contains(c.TypeID)) || c.TypeID == 330).ToList().OrderBy(c => c.PostedDate).ToList();
                    #endregion

                    #region RSInwardOutward - RSInwardOutwardDetail
                    List<RSInwardOutward> lstRSInwardOutwards = _IRSInwardOutwardService.Query.Where(
                            c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                    List<RSAssemblyDismantlement> rSAssemblyDismantlements = _IRSAssemblyDismantlementService.GetAll();
                    #endregion

                    #region RSTranfer - RSTranferDetail
                    List<int> lstTypeTranfer = new List<int>() { 420, 421, 422 };
                    List<RSTransfer> lstRSTranfer = _IRSTranferService.Query.Where(c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                    List<RepositoryLedger> lstRepositoriesledger = lstrepos.Where(c => (lstMaterialGoods.Any(d => d.ID == c.MaterialGoodsID)
                    && (c.TypeID == 420 || c.TypeID == 421 || c.TypeID == 422) && c.PostedDate >= fromDate && c.PostedDate <= toDate) || c.TypeID == 403).OrderBy(c => c.PostedDate).ToList();
                    #endregion

                    #region SAInvoice
                    List<int> lstTypeSAInvoices = new List<int>() { 412, 415, 320, 321, 322, 323, 324, 325 };
                    List<SAInvoice> lstSAInvoices = _ISAInvoiceService.Query.Where(
                            c => c.PostedDate >= fromDate && c.PostedDate <= toDate && c.Recorded).ToList();
                    List<SAReturn> lstSAReturns = _ISAReturnService.Query.Where(c => c.Recorded && c.AutoOWAmountCal == 0).ToList();
                    List<SAReturnDetail> lstSAReturnDetails = _ISAReturnDetailService.Query.Where(c => _ISAReturnService.Query.Any(d => d.Recorded && d.AutoOWAmountCal == 0 && d.ID == c.SAReturnID)).ToList();
                    List<GeneralLedger> lstGeneralLedgersreturn = lstGeneralLedgers.Where(c => c.TypeID == 330).ToList();
                    List<RSInwardOutward> lstRSInwardOutward = _IRSInwardOutwardService.Query.Where(c => c.TypeID == 403).ToList();
                    #endregion

                    #region PPDiscountRetur               
                    List<PPDiscountReturnDetail> lstPPDiscountReturnDetailsAll = _IPPDiscountReturnDetailService.Query.Where(c => _IPPDiscountReturnService.Query.Any(d => d.Recorded && d.TypeID == 230 && d.ID == c.PPDiscountReturnID) && c.ConfrontDetailID != null).ToList();
                    #endregion
                    List<string> lstFail = new List<string>();
                    bool ctn = true;
                    #endregion
                    foreach (MaterialGoods materialGood in lstMaterialGoods)
                    {
                        ctn = true;
                        #region Tính giá xuất cho một VTHH
                        //DS chứng từ VTHH đầu kỳ
                        List<RepositoryLedger> lstRLIWDK = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && c.IWAmount > 0 && c.IWQuantity > 0 && c.TypeID == 702).ToList();
                        decimal GTDK = lstRLIWDK.Sum(x => Math.Round(x.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero));
                        decimal SLDK = lstRLIWDK.Sum(x => x.IWQuantity) ?? 0;
                        decimal DGDK = SLDK > 0 ? GTDK / SLDK : 0;
                        //DS chứng từ VTHH trong kỳ
                        List<RepositoryLedger> lstRLOW = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && lstTypeOW.Any(d => d == c.TypeID) && c.OWQuantity > 0 &&
                             c.PostedDate <= toDate).OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime.Value.ToOADate()).ToList();//danh sách xuất kho
                        List<RepositoryLedger> lstRLIW1 = lstRepositoryLedgers.Where(c => c.MaterialGoodsID == materialGood.ID && lstTypeIW.Any(d => d == c.TypeID) && c.IWQuantity > 0 &&
                             c.PostedDate <= toDate).OrderBy(c => c.PostedDate).ThenBy(c => c.RefDateTime.Value.ToOADate()).ToList();//danh sách nhập kho
                        List<RepositoryLedger> lstRLIW = lstRLIW1.CloneObject();
                        if (lstFail.Contains(materialGood.MaterialGoodsCode))
                        {
                            continue;
                        }
                        if (lstRLIWDK.Count() == 0 && lstRLIW.Count() == 0 && lstRLOW.Count() > 0)
                        {
                            if (DialogResult.Yes == MessageBox.Show("VTHH " + materialGood.MaterialGoodsCode + " bị tồn kho âm./n Nếu ấn 'Đồng ý' hệ thống sẽ bỏ qua những hàng hóa bị âm kho, chỉ thực hiện cho những hàng hóa không bị âm số lượng tồn. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                lstFail.Add(materialGood.MaterialGoodsCode);
                                continue;
                            }
                            else
                            {
                                return false;
                            }

                        }
                        foreach (RepositoryLedger repositoryLedger in lstRLIW)
                        {
                            List<PPDiscountReturnDetail> pPDiscountReturnDetail = lstPPDiscountReturnDetailsAll.Where(c => (c.ConfrontDetailID ?? Guid.NewGuid()) == repositoryLedger.DetailID).ToList();
                            if (pPDiscountReturnDetail.Count() > 0)
                            {
                                repositoryLedger.IWAmount = repositoryLedger.IWAmount - pPDiscountReturnDetail.Sum(t => t.Amount);
                            }
                        }
                        #region Tính giá                                         
                        int j = 0;
                        decimal sl = lstRLIW.Count > 0 ? lstRLIW[0].IWQuantity ?? 0 : 0;
                        decimal gt = lstRLIW.Count > 0 ? Math.Round(lstRLIW[0].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero) : 0;
                        for (int i = 0; i < lstRLOW.Count; i++)
                        {
                            bool check = true;
                            try
                            {
                                if (SLDK > 0)
                                {
                                    if (lstRLOW[i].OWQuantity <= SLDK)
                                    {
                                        SLDK = SLDK - lstRLOW[i].OWQuantity ?? 0;
                                        lstRLOW[i].UnitPrice = DGDK;
                                        if (SLDK == 0)
                                        {
                                            lstRLOW[i].OWAmount = GTDK;
                                            GTDK = GTDK - Math.Round(lstRLOW[i].OWAmount ?? 0, lamTronGia);
                                        }
                                        else
                                        {
                                            lstRLOW[i].OWAmount = lstRLOW[i].OWQuantity * lstRLOW[i].UnitPrice;
                                            GTDK = GTDK - Math.Round(lstRLOW[i].OWAmount ?? 0, lamTronGia);
                                        }
                                    }
                                    else
                                    {
                                        while (sl < (lstRLOW[i].OWQuantity - SLDK))
                                        {
                                            if (lstFail.Contains(materialGood.MaterialGoodsCode))
                                            {
                                                ctn = false;
                                                break;
                                            }
                                            j++;
                                            if (lstRLIW.Count() < j)
                                            {
                                                if (DialogResult.Yes == MessageBox.Show("VTHH " + materialGood.MaterialGoodsCode + " bị tồn kho âm./n Nếu ấn 'Đồng ý' hệ thống sẽ bỏ qua những hàng hóa bị âm kho, chỉ thực hiện cho những hàng hóa không bị âm số lượng tồn. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                                {
                                                    lstFail.Add(materialGood.MaterialGoodsCode);
                                                    ctn = false;
                                                    break;
                                                }
                                                else
                                                {
                                                    return false;
                                                }
                                            }
                                            sl = sl + lstRLIW[j].IWQuantity ?? 0;
                                            gt = gt + Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        }
                                        if (!ctn)
                                        {
                                            continue;
                                        }
                                        lstRLIW[j].IWQuantity = sl - (lstRLOW[i].OWQuantity - SLDK);
                                        lstRLIW[j].IWAmount = Math.Round((lstRLIW[j].IWQuantity * lstRLIW[j].UnitPrice) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        lstRLOW[i].OWAmount = gt - (Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero) - GTDK);
                                        lstRLOW[i].UnitPrice = (lstRLOW[i].OWAmount ?? 0) / (lstRLOW[i].OWQuantity ?? 1);
                                        sl = lstRLIW[j].IWQuantity ?? 0;
                                        gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        SLDK = 0;
                                        GTDK = 0;
                                    }
                                }
                                else
                                {
                                    while (sl < lstRLOW[i].OWQuantity)
                                    {
                                        if (lstFail.Contains(materialGood.MaterialGoodsCode))
                                        {
                                            ctn = false;
                                            break;
                                        }
                                        j++;
                                        if (lstRLIW.Count() < j)
                                        {
                                            if (DialogResult.Yes == MessageBox.Show("VTHH " + materialGood.MaterialGoodsCode + " bị tồn kho âm./n Nếu ấn 'Đồng ý' hệ thống sẽ bỏ qua những hàng hóa bị âm kho, chỉ thực hiện cho những hàng hóa không bị âm số lượng tồn. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                            {
                                                lstFail.Add(materialGood.MaterialGoodsCode);
                                                ctn = false;
                                                break;
                                            }
                                            else
                                            {
                                                return false;
                                            }
                                        }
                                        sl = sl + lstRLIW[j].IWQuantity ?? 0;
                                        gt = gt + Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        check = false;
                                    }
                                    if (!ctn)
                                    {
                                        continue;
                                    }
                                    if (check)
                                    {
                                        lstRLOW[i].UnitPrice = (lstRLIW[j].IWQuantity == 0 ? 0 : ((lstRLIW[j].IWAmount ?? 0) / (lstRLIW[j].IWQuantity ?? 1)));
                                        lstRLIW[j].IWQuantity = sl - lstRLOW[i].OWQuantity;
                                        if (lstRLIW[j].IWQuantity == 0)
                                        {
                                            lstRLOW[i].OWAmount = gt;
                                            lstRLIW[j].IWAmount = gt - lstRLOW[i].OWAmount;
                                            if (lstRLIW.Count - 1 > j)
                                            {
                                                j++;
                                                sl = lstRLIW[j].IWQuantity ?? 0;
                                                gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                            }
                                            else
                                            {
                                                sl = 0;
                                                gt = 0;
                                            }
                                        }
                                        else
                                        {
                                            lstRLOW[i].OWAmount = Math.Round((lstRLOW[i].UnitPrice * lstRLOW[i].OWQuantity) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                            lstRLIW[j].IWAmount = gt - lstRLOW[i].OWAmount;
                                            sl = lstRLIW[j].IWQuantity ?? 0;
                                            gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        }
                                    }
                                    else
                                    {
                                        lstRLIW[j].IWQuantity = sl - lstRLOW[i].OWQuantity;
                                        lstRLIW[j].IWAmount = Math.Round((lstRLIW[j].IWQuantity * lstRLIW[j].UnitPrice) ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        lstRLOW[i].OWAmount = gt - Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                        lstRLOW[i].UnitPrice = (lstRLOW[i].OWAmount ?? 0) / (lstRLOW[i].OWQuantity ?? 1);
                                        sl = lstRLIW[j].IWQuantity ?? 0;
                                        gt = Math.Round(lstRLIW[j].IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero);
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                return false;
                            }
                        }
                        #endregion
                        if (!ctn)
                        {
                            continue;
                        }
                        #region Update
                        BeginTran();
                        foreach (var rep in lstRLOW)
                        {
                            if (rep.PostedDate >= fromDate)
                            {
                                Update(rep);
                                if (rep.TypeID == 410 || rep.TypeID == 414)
                                {
                                    #region RSInwardOutward
                                    var inwardOutward = lstRSInwardOutwards.FirstOrDefault(x => x.ID == rep.ReferenceID);
                                    if (inwardOutward != null)
                                    {
                                        if (lstMaterialGoods.Any(c => inwardOutward.RSInwardOutwardDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                        {
                                            foreach (RSInwardOutwardDetail inwardOutwardDetail in inwardOutward.RSInwardOutwardDetails)
                                            {

                                                if (inwardOutwardDetail.MaterialGoodsID == rep.MaterialGoodsID && inwardOutwardDetail.RepositoryID == repositoryID)
                                                {
                                                    inwardOutwardDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                    inwardOutwardDetail.UnitPrice = rep.UnitPrice;
                                                    inwardOutwardDetail.AmountOriginal = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                        (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                    inwardOutwardDetail.Amount = Math.Round((rep.UnitPrice * inwardOutwardDetail.Quantity ?? 0), lamTronGia, MidpointRounding.AwayFromZero);
                                                }

                                            }
                                            inwardOutward.TotalAmount = inwardOutward.RSInwardOutwardDetails.Sum(c => c.Amount);
                                            inwardOutward.TotalAmountOriginal = inwardOutward.RSInwardOutwardDetails.Sum(c => c.AmountOriginal);
                                            _IRSInwardOutwardService.Update(inwardOutward);
                                        }
                                        if (inwardOutward.RSAssemblyDismantlementID != null)
                                        {
                                            var RSAssemblyDismantlement = rSAssemblyDismantlements.FirstOrDefault(c => c.ID == inwardOutward.RSAssemblyDismantlementID);
                                            if (RSAssemblyDismantlement != null && lstMaterialGoods.Any(c => RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                            {
                                                foreach (RSAssemblyDismantlementDetail RSAssemblyDismantlementDetail in RSAssemblyDismantlement.RSAssemblyDismantlementDetails)
                                                {

                                                    if (RSAssemblyDismantlementDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                                    {
                                                        RSAssemblyDismantlementDetail.UnitPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                        RSAssemblyDismantlementDetail.UnitPrice = rep.UnitPrice;
                                                        RSAssemblyDismantlementDetail.AmountOriginal = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                            (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                        RSAssemblyDismantlementDetail.Amount = Math.Round((rep.UnitPrice * RSAssemblyDismantlementDetail.Quantity ?? 0),
                                                             lamTronGia, MidpointRounding.AwayFromZero);
                                                    }

                                                }
                                                RSAssemblyDismantlement.Amount = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.Amount);
                                                RSAssemblyDismantlement.AmountOriginal = RSAssemblyDismantlement.RSAssemblyDismantlementDetails.Sum(c => c.AmountOriginal);
                                                _IRSAssemblyDismantlementService.Update(RSAssemblyDismantlement);
                                            }
                                        }
                                        if (inwardOutward.SAInvoiceID != null)
                                        {
                                            var Sainvoice = lstSAInvoices.FirstOrDefault(x => x.ID == inwardOutward.SAInvoiceID);
                                            if (Sainvoice != null && lstMaterialGoods.Any(c => Sainvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                            {
                                                foreach (SAInvoiceDetail sAInvoiceDetail in Sainvoice.SAInvoiceDetails)
                                                {
                                                    if (sAInvoiceDetail.MaterialGoodsID == rep.MaterialGoodsID)
                                                    {
                                                        sAInvoiceDetail.OWPriceOriginal = rep.UnitPrice / (inwardOutward.ExchangeRate ?? 1);
                                                        sAInvoiceDetail.OWPrice = rep.UnitPrice;
                                                        sAInvoiceDetail.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0) / (inwardOutward.ExchangeRate ?? 1),
                                                            (inwardOutward.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                        sAInvoiceDetail.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetail.Quantity ?? 0),
                                                           lamTronGia, MidpointRounding.AwayFromZero);
                                                    }
                                                }
                                                Sainvoice.TotalCapitalAmount = Sainvoice.SAInvoiceDetails.Sum(c => c.Amount);
                                                Sainvoice.TotalCapitalAmountOriginal = Sainvoice.SAInvoiceDetails.Sum(c => c.AmountOriginal);
                                                _ISAInvoiceService.Update(Sainvoice);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (new int[] { 412 }.Any(a => a == rep.TypeID))
                                {
                                    #region SAInvoice
                                    var sAInvoice = lstSAInvoices.FirstOrDefault(x => x.ID == rep.ReferenceID);
                                    if (sAInvoice != null && lstMaterialGoods.Any(c => sAInvoice.SAInvoiceDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (SAInvoiceDetail sAInvoiceDetails in sAInvoice.SAInvoiceDetails)
                                        {
                                            if (sAInvoiceDetails.MaterialGoodsID == rep.MaterialGoodsID && sAInvoiceDetails.RepositoryID == repositoryID)
                                            {
                                                sAInvoiceDetails.OWPriceOriginal = rep.UnitPrice / (sAInvoice.ExchangeRate ?? 1);
                                                sAInvoiceDetails.OWPrice = rep.UnitPrice;
                                                sAInvoiceDetails.OWAmountOriginal = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                                    (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                sAInvoiceDetails.OWAmount = Math.Round((rep.UnitPrice * sAInvoiceDetails.Quantity ?? 0),
                                                     lamTronGia, MidpointRounding.AwayFromZero);
                                                SAReturnDetail sAReturnDetails = lstSAReturnDetails.FirstOrDefault(c => c.SAInvoiceDetailID == sAInvoiceDetails.ID);//update hang mua trả lại
                                                if (sAReturnDetails != null)
                                                {
                                                    var sAReturn = lstSAReturns.FirstOrDefault(c => c.ID == sAReturnDetails.SAReturnID && c.Recorded && c.AutoOWAmountCal == 0 && c.SAReturnDetails.Any(d => d.SAInvoiceDetailID == sAInvoiceDetails.ID));
                                                    if (sAReturn != null)
                                                    {
                                                        sAReturnDetails.OWPriceOriginal = rep.UnitPrice / (sAReturn.ExchangeRate ?? 1);
                                                        sAReturnDetails.OWPrice = rep.UnitPrice;
                                                        if (sAInvoiceDetails.Quantity == sAReturnDetails.Quantity)
                                                        {
                                                            sAReturnDetails.OWAmountOriginal = Math.Round((rep.OWAmount ?? 0) / (sAInvoice.ExchangeRate ?? 1),
                                                                                            (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                            sAReturnDetails.OWAmount = Math.Round((rep.OWAmount ?? 0),
                                                                                        lamTronGia, MidpointRounding.AwayFromZero);
                                                        }
                                                        else
                                                        {
                                                            sAReturnDetails.OWAmountOriginal = Math.Round(sAInvoiceDetails.OWPriceOriginal * (sAReturnDetails.Quantity ?? 1),
                                                                                            (sAInvoice.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                            sAReturnDetails.OWAmount = Math.Round(sAInvoiceDetails.OWPrice * (sAReturnDetails.Quantity ?? 1),
                                                                                        lamTronGia, MidpointRounding.AwayFromZero);
                                                        }
                                                        _ISAReturnDetailService.Update(sAReturnDetails);
                                                        sAReturn.TotalOWAmount = sAReturn.SAReturnDetails.Sum(c => c.OWAmount);
                                                        sAReturn.TotalOWAmountOriginal = sAReturn.SAReturnDetails.Sum(c => c.OWAmountOriginal);
                                                        _ISAReturnService.Update(sAReturn);
                                                        var rsInward = lstRSInwardOutward.FirstOrDefault(c => c.ID == sAReturn.ID);
                                                        if (rsInward != null)
                                                        {
                                                            rsInward.TotalAmount = sAReturn.TotalOWAmount;
                                                            rsInward.TotalAmountOriginal = sAReturn.TotalOWAmountOriginal;
                                                            _IRSInwardOutwardService.Update(rsInward);
                                                        }
                                                        var lstGeneralLedgerReturn = lstGeneralLedgersreturn.Where(c => c.DetailID == sAReturnDetails.ID);
                                                        foreach (GeneralLedger g in lstGeneralLedgerReturn)
                                                        {
                                                            if (g.Account == "632")
                                                            {
                                                                g.CreditAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                                g.CreditAmount = sAReturnDetails.OWAmount;

                                                            }
                                                            else if (g.AccountCorresponding == "632")
                                                            {
                                                                g.DebitAmountOriginal = sAReturnDetails.OWAmountOriginal;
                                                                g.DebitAmount = sAReturnDetails.OWAmount;
                                                            }
                                                            _IGeneralLedgerService.Update(g);
                                                        }
                                                        var ReposReturn = lstRepositoriesledger.FirstOrDefault(c => c.DetailID == sAReturnDetails.ID);
                                                        if (ReposReturn != null)
                                                        {
                                                            ReposReturn.UnitPrice = sAReturnDetails.OWPrice;
                                                            ReposReturn.IWAmount = sAReturnDetails.OWAmount;
                                                            ReposReturn.IWAmountBalance = sAReturnDetails.OWAmount;
                                                            Update(ReposReturn);
                                                        }
                                                    }
                                                }
                                            }
                                            sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmount);
                                            sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(c => c.OWAmountOriginal);
                                            _ISAInvoiceService.Update(sAInvoice);
                                        }
                                        if (sAInvoice.OutwardNo != null && !string.IsNullOrEmpty(sAInvoice.OutwardNo))
                                        {
                                            var outWard = lstRSInwardOutwards.FirstOrDefault(x => x.ID == sAInvoice.ID);//_IRSInwardOutwardService.Query.FirstOrDefault(c => c.ID == sAInvoice.ID);
                                            if (outWard != null)
                                            {
                                                outWard.TotalAmount = sAInvoice.TotalCapitalAmount;
                                                outWard.TotalAmountOriginal = sAInvoice.TotalCapitalAmountOriginal;
                                                _IRSInwardOutwardService.Update(outWard);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (rep.TypeID == 420 || rep.TypeID == 421 || rep.TypeID == 422)
                                {
                                    #region RSTranfer
                                    var tranfer = lstRSTranfer.FirstOrDefault(x => x.ID == rep.ReferenceID);

                                    if (tranfer != null && lstMaterialGoods.Any(c => tranfer.RSTransferDetails.Any(d => d.MaterialGoodsID == c.ID)))
                                    {
                                        foreach (RSTransferDetail tranferDetail in tranfer.RSTransferDetails)
                                        {
                                            if (tranferDetail.MaterialGoodsID == rep.MaterialGoodsID && tranferDetail.FromRepositoryID == repositoryID)
                                            {
                                                tranferDetail.UnitPriceOriginal = rep.UnitPrice / (tranfer.ExchangeRate ?? 1);
                                                tranferDetail.UnitPrice = rep.UnitPrice;
                                                tranferDetail.AmountOriginal = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0) / (tranfer.ExchangeRate ?? 1),
                                                   (tranfer.ExchangeRate ?? 1) == 1 ? lamTronGia : lamTronGia1, MidpointRounding.AwayFromZero);
                                                tranferDetail.Amount = Math.Round((rep.UnitPrice * tranferDetail.Quantity ?? 0),
                                                     lamTronGia, MidpointRounding.AwayFromZero);
                                            }

                                        }
                                        tranfer.TotalAmount = tranfer.RSTransferDetails.Sum(c => c.Amount);
                                        tranfer.TotalAmountOriginal = tranfer.RSTransferDetails.Sum(c => c.AmountOriginal);
                                        _IRSTranferService.Update(tranfer);
                                    }
                                    #endregion
                                }
                                #region GeneralLedger
                                string OldID = "";
                                var lstGl = lstGeneralLedgers.Where(x => x.DetailID == rep.DetailID).ToList();//_IGeneralLedgerService.GetByDetailID(rep.DetailID ?? Guid.Empty);
                                foreach (GeneralLedger g in lstGl)
                                {
                                    if (lstGl.Count >= 1)
                                    {
                                        OldID = lstGl[0].ID.ToString();
                                    }
                                    if (new int[] { 320, 321, 322, 323, 324, 325, 412 }.Any(u => u == g.TypeID))
                                    {
                                        if (g.Account == "632")
                                        {
                                            g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.DebitAmount = rep.OWAmount ?? 0;
                                        }
                                        else if (g.AccountCorresponding == "632")
                                        {
                                            g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                            g.CreditAmount = rep.OWAmount ?? 0;
                                        }
                                    }
                                    else if (new int[] { 410, 414 }.Any(u => u == g.TypeID))
                                    {
                                        if (lstRSInwardOutwards.Any(x => x.ID == g.ReferenceID) && lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.Count > 0)
                                        {
                                            var rsOutDetail = lstRSInwardOutwards.FirstOrDefault(x => x.ID == g.ReferenceID).RSInwardOutwardDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                            if (rsOutDetail != null)
                                            {
                                                if (g.Account == rsOutDetail.CreditAccount)
                                                {
                                                    g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                    g.CreditAmount = rep.OWAmount ?? 0;
                                                }
                                                else if (g.Account == rsOutDetail.DebitAccount)
                                                {
                                                    g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                    g.DebitAmount = rep.OWAmount ?? 0;
                                                }
                                            }
                                        }
                                    }
                                    else if (new int[] { 420, 421, 422 }.Any(u => u == g.TypeID))
                                    {
                                        if (lstRSTranfer.Any(x => x.ID == g.ReferenceID) && lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.Count > 0)
                                        {
                                            var rsOutDetail = lstRSTranfer.FirstOrDefault(x => x.ID == g.ReferenceID).RSTransferDetails.FirstOrDefault(x => x.ID == g.DetailID);
                                            if (rsOutDetail != null)
                                            {
                                                if (rsOutDetail.DebitAccount == rsOutDetail.CreditAccount)//trungnq thêm trường hợp này để check trường hợp tính giá xuất kho khi 2 tài khoản nợ có = nhau bug 6309
                                                {
                                                    if (rep.OWAmount != null)
                                                    {
                                                        if (rep.OWAmount != 0)
                                                        {
                                                            if (g.ID.ToString() == OldID)
                                                            {
                                                                g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                                g.CreditAmount = rep.OWAmount ?? 0;

                                                            }
                                                            else
                                                            {
                                                                g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                                g.DebitAmount = rep.OWAmount ?? 0;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                if (g.Account == rsOutDetail.CreditAccount)
                                                {
                                                    g.CreditAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                    g.CreditAmount = rep.OWAmount ?? 0;
                                                }
                                                else if (g.Account == rsOutDetail.DebitAccount)
                                                {
                                                    g.DebitAmountOriginal = Math.Round((rep.OWAmount / g.ExchangeRate ?? 1), lamTronGia1, MidpointRounding.AwayFromZero);
                                                    g.DebitAmount = rep.OWAmount ?? 0;
                                                }
                                            }
                                        }
                                    }
                                    _IGeneralLedgerService.Update(g);
                                }
                                #endregion
                            }
                        }
                        CommitTran();
                        #endregion

                        #endregion
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                return false;
            }
        }
        #endregion

        #region RepositoryLedgerReport:Dungna(ReportsS12DN)
        /// <summary>
        /// Báo cáo thẻ kho
        /// [DungNA]
        /// </summary>
        /// <param name="Date">Từ Ngày</param>
        /// <param name="PostedDate">Đến Ngày</param>
        /// <param name="MaterialGoodsCode">Mã Sản phẩm</param>        
        /// <returns></returns>         
        public List<S09DNN> ReportS09DNN(DateTime Date, DateTime PostedDate, List<Guid> MG, Guid resID)
        {
            List<S09DNN> outData = new List<S09DNN>();
            foreach (Guid x in MG)
            {
                List<S09DNN> ddd = GetListS09DNN(Date, PostedDate, x, resID);
                outData.AddRange(ddd);
            }
            return outData;
        }
        public List<S09DNN> GetListS09DNN(DateTime Date, DateTime PostedDate, Guid MaterialGoodsID, Guid resID)
        {
            Date = new DateTime(Date.Year, Date.Month, Date.Day, 0, 0, 0);
            PostedDate = new DateTime(PostedDate.Year, PostedDate.Month, PostedDate.Day, 23, 59, 59);
            var listkq = new List<S09DNN>();
            List<S09DNN> thongke = null;
            var list_repo = Query.Where(o => o.MaterialGoodsID == MaterialGoodsID && o.PostedDate >= Date && o.PostedDate <= PostedDate && o.No != "OPN" && ((((o.IWQuantity == null) ? 0 : o.IWQuantity) != 0) || (((o.OWQuantity == null) ? 0 : o.OWQuantity) != 0))).ToList();

            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query;
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query;
            var list_genera1 = (from a in list_repo
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in list_repo
                                join i in listSAInvoice on a.ReferenceID equals i.ID
                                //join b in listSABill on i.BillRefID equals b.ID
                                join d in list_delete on i.BillRefID equals d.SAInvoiceID
                                select a).ToList();

            list_repo = (from a in list_repo
                         where !list_genera1.Any(t => t.ID == a.ID) && !list_genera2.Any(t => t.ID == a.ID)
                         select a).ToList();

            if (resID != Guid.Empty)
            {
                thongke = (from a in list_repo
                           join c in _IMaterialGoodsService.Query.ToList() on a.MaterialGoodsID equals c.ID
                           where c.MaterialGoodsCode != null
                           && c.MaterialGoodsName != null
                           && (resID == Guid.Empty ? true : a.RepositoryID == resID)
                           orderby a.RefDateTime
                           select new S09DNN
                           {
                               MaterialGoodsCode = c.MaterialGoodsCode,
                               MaterialGoodsName = c.MaterialGoodsName,
                               Unit = c.Unit ?? "",
                               Date = a.Date,
                               InPut = (string.IsNullOrEmpty(a.IWQuantity.ToString()) || a.IWQuantity == 0) ? "" : a.No,//Nhập
                               OutPut = (string.IsNullOrEmpty(a.OWQuantity.ToString()) || a.OWQuantity == 0) ? "" : a.No,//Xuất
                               Hyperlink = a.ReferenceID + ";" + a.TypeID,
                               Description = a.Reason,
                               PostedDate = a.PostedDate,
                               IWQuantity = a.IWQuantity ?? decimal.Parse("0"),//Số lượng nhập
                               OWQuantity = a.OWQuantity ?? decimal.Parse("0"),//Số lượng xuất
                               SignatureAccountancy = "",
                           }).ToList();
            }
            else
            {
                thongke = (from a in list_repo
                           join c in _IMaterialGoodsService.Query.ToList() on a.MaterialGoodsID equals c.ID
                           where c.MaterialGoodsCode != null
                           && c.MaterialGoodsName != null
                           && (resID == Guid.Empty ? true : a.RepositoryID == resID)
                           orderby a.RefDateTime
                           select new S09DNN
                           {
                               MaterialGoodsCode = c.MaterialGoodsCode,
                               MaterialGoodsName = c.MaterialGoodsName,
                               Unit = c.Unit ?? "",
                               Date = a.Date,
                               InPut = (string.IsNullOrEmpty(a.IWQuantity.ToString()) || a.IWQuantity == 0) ? "" : a.No,//Nhập
                               OutPut = (string.IsNullOrEmpty(a.OWQuantity.ToString()) || a.OWQuantity == 0) ? "" : a.No,//Xuất
                               Hyperlink = a.ReferenceID + ";" + a.TypeID,
                               Description = a.Reason,
                               PostedDate = a.PostedDate,
                               IWQuantity = a.IWQuantity ?? decimal.Parse("0"),//Số lượng nhập
                               OWQuantity = a.OWQuantity ?? decimal.Parse("0"),//Số lượng xuất
                               SignatureAccountancy = "",
                           }).ToList();
            }
            var MaterialGoods = _IMaterialGoodsService.Getbykey(MaterialGoodsID);
            foreach (var x in thongke)
            {
                var addlist = new S09DNN();
                addlist.MaterialGoodsName = x.MaterialGoodsName;
                addlist.Unit = x.Unit;
                addlist.MaterialGoodsCode = x.MaterialGoodsCode;
                addlist.Date = x.Date;
                addlist.InPut = x.InPut;
                addlist.OutPut = x.OutPut;
                addlist.Description = x.Description;
                addlist.PostedDate = x.PostedDate;
                addlist.IWQuantity = x.IWQuantity;
                addlist.OWQuantity = x.OWQuantity;
                addlist.SignatureAccountancy = "";
                addlist.Hyperlink = x.Hyperlink;
                addlist.OrderType = 0;
                listkq.Add(addlist);
            }
            S09DNN OpeningBalance = new S09DNN();//Số dư đầu kỳ            
            OpeningBalance.OpeningBalance = GetOpeningBalance(Date, MaterialGoodsID, resID);
            OpeningBalance.OrderType = -1;
            OpeningBalance.MaterialGoodsCode = MaterialGoods.MaterialGoodsCode;
            OpeningBalance.MaterialGoodsName = MaterialGoods.MaterialGoodsName;
            OpeningBalance.Unit = MaterialGoods.Unit;
            listkq.Add(OpeningBalance);
            return listkq.OrderBy(c => c.Date).ThenBy(d => d.No).ToList();
        }
        /// <summary>
        /// [DungNA]
        /// Hàm Lấy số dư đầu kỳ cho sổ kho
        /// </summary>
        /// <param name="PostedDate">Ngày chứng từ trở về trước</param>
        /// <param name="MaterialGoodsID">ID vật tư hàng hóa</param>
        /// <returns></returns>
        public decimal GetOpeningBalance(DateTime Date, Guid MaterialGoodsID, Guid resID)
        {
            decimal rp = 0;

            try
            {
                if (resID != Guid.Empty)
                {
                    var SoTonDauKy = (from a in Query
                                      where (a.PostedDate < Date || a.No == "OPN")
                                     && a.MaterialGoodsID == MaterialGoodsID && a.RepositoryID == resID
                                      select new
                                      {
                                          OpeningBalance = a.IWQuantity - a.OWQuantity,
                                      }).Sum(c => c.OpeningBalance);
                    rp = SoTonDauKy ?? 0;
                }
                else
                {
                    var SoTonDauKy1 = (from a in Query
                                       where (a.PostedDate < Date || a.No == "OPN")
                                      && a.MaterialGoodsID == MaterialGoodsID
                                       select new
                                       {
                                           OpeningBalance = a.IWQuantity - a.OWQuantity,
                                       }).Sum(c => c.OpeningBalance);
                    rp = SoTonDauKy1 ?? 0;
                }

            }
            catch (Exception ex) { };

            return rp;
        }
        #endregion
        #region Sổ chi tiết vật liệu hàng hóa [DungNA] RS10DN
        /// <summary>
        /// [DungNA]
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to">Đên Ngày</param>
        /// <param name="RepositoryID">Mã Kho</param>
        /// <param name="MaterialGoodsID">Tên hàng hóa</param>
        /// <param name="TotalGroup">Check Cộng Gộp</param>
        /// <returns></returns>
        public List<S07DNN> RS07DNN(DateTime from, DateTime to, Guid RepositoryID, List<Guid> MaterialGoodsID, bool TotalGroup)
        {
            var ddSoGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);
            var lstma = _IMaterialGoodsService.GetAll();
            var lstreps = _IRepositoryService.GetAll();
            List<S07DNN> output = new List<S07DNN>();
            foreach (Guid x in MaterialGoodsID)
            {
                List<S07DNN> list1 = GetRS07DNN(from, to, RepositoryID, x, TotalGroup, lamTronGia, lstma, lstreps);
                output.AddRange(list1);
            }
            return output;
        }
        public List<S07DNN> GetRS07DNN(DateTime fromdate, DateTime todate, Guid RepositoryID, Guid MaterialGoodsID, bool TotalGroup, int lamTronGia, List<MaterialGoods> lstma, List<Repository> lstreps)
        {
            DateTime froms = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day, 0, 0, 0);
            DateTime to = new DateTime(todate.Year, todate.Month, todate.Day, 23, 59, 59);

            var list_repo = Query.Where(o => o.No != null
                      && o.No != "OPN"
                      && o.AccountCorresponding != null
                      && o.Unit != null
                      && o.UnitPrice != null
                      && o.RepositoryID != null
                      && o.MaterialGoodsID != null
                      && o.PostedDate >= froms
                      && o.PostedDate <= to).ToList();

            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query.ToList();
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query.Where(t => t.BillRefID != null).ToList();

            var list_genera1 = (from a in list_repo
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in list_repo
                                from SA in listSAInvoice.Where(t => t.ID == a.ReferenceID) //on GL.ReferenceID equals SA.ID
                                //from b in listSABill.Where(t => t.ID == SA.BillRefID) //on SA.BillRefID equals b.ID
                                from d in list_delete.Where(t => t.SAInvoiceID == SA.BillRefID) //on b.ID equals d.SAInvoiceID
                                select a).ToList();

            list_repo = (from a in list_repo
                         where !list_genera1.Any(t => t.ID == a.ID) && !list_genera2.Any(t => t.ID == a.ID)
                         select a).ToList();

            var listkq = new List<S07DNN>();
            var RS10 = (from a in list_repo
                        join b in lstma on a.MaterialGoodsID equals b.ID
                        join d in lstreps on a.RepositoryID equals d.ID
                        where d.RepositoryCode != null
                       && d.RepositoryName != null
                       && b.MaterialGoodsCode != null
                       && b.MaterialGoodsName != null
                       //&& a.Description != null
                       && (RepositoryID == Guid.Empty ? true : a.RepositoryID == RepositoryID)
                       && b.ID == MaterialGoodsID
                        orderby a.RefDateTime
                        select new S07DNN
                        {
                            RepositoryCode = d.RepositoryCode,
                            RepositoryName = d.RepositoryName,
                            Currency = "Việt Nam Đồng",
                            MaterialGoodsCode = b.MaterialGoodsCode,
                            MaterialGoodsName = b.MaterialGoodsName,
                            No = a.No,
                            RefID = a.ReferenceID,
                            RefType = a.TypeID,
                            PostedDate = a.PostedDate,
                            Description = a.Description,
                            Reason = a.Reason,
                            Account = d.DefaultAccount,
                            AccountCorresponding = a.AccountCorresponding,
                            Unit = a.Unit,
                            UnitPrice = a.UnitPrice,
                            IWQuantity = a.IWQuantity ?? 0,
                            IWValue = Math.Round(a.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero),
                            OWQuantity = a.OWQuantity ?? 0,
                            OWValue = Math.Round(a.OWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero),
                            CurrencyID = "VND"
                        }).ToList();
            List<S07DNN> OpeningBalance = GetOpeningBalanceS07DNN(MaterialGoodsID, RepositoryID, froms, to, lamTronGia, lstma, lstreps);

            if (TotalGroup == false)
            {
                List<string> lstRSCode = RS10.Select(o => o.RepositoryCode).Distinct().ToList();
                List<string> lstOPCode = OpeningBalance.Select(o => o.RepositoryCode).Distinct().ToList();

                foreach (var item in lstOPCode)
                {
                    if (!lstRSCode.Contains(item))
                    {
                        lstRSCode.Add(item);
                    }
                }

                foreach (var code in lstRSCode)
                {
                    List<S07DNN> ListByCode = RS10.Where(o => o.RepositoryCode == code).ToList();
                    List<S07DNN> ListOPByCode = OpeningBalance.Where(o => o.RepositoryCode == code).ToList();
                    ListByCode.AddRange(ListOPByCode);
                    ListByCode = ListByCode.OrderBy(o => o.PostedDate).ToList();
                    decimal ClosingQuantity = 0;
                    decimal ClosingAmount = 0;

                    foreach (var x in ListByCode)
                    {
                        var addlist = new S07DNN();
                        addlist.RepositoryCode = x.RepositoryCode;
                        addlist.RepositoryName = x.RepositoryName;
                        addlist.Currency = x.Currency;
                        addlist.MaterialGoodsCode = x.MaterialGoodsCode;
                        addlist.MaterialGoodsName = x.MaterialGoodsName;
                        addlist.No = x.No;
                        addlist.RefID = x.RefID;
                        addlist.RefType = x.RefType;
                        addlist.PostedDate = x.PostedDate;
                        addlist.Reason = x.Reason;
                        addlist.Account = x.Account;
                        addlist.AccountCorresponding = x.AccountCorresponding;
                        addlist.Unit = x.Unit;
                        addlist.UnitPrice = x.UnitPrice;
                        addlist.IWQuantity = x.IWQuantity;
                        addlist.IWValue = x.IWValue;
                        addlist.OWQuantity = x.OWQuantity;
                        addlist.OWValue = x.OWValue;
                        addlist.CurrencyID = x.CurrencyID;
                        addlist.IWOPAmount = x.IWOPAmount;
                        addlist.IWOPQuantity = x.IWOPQuantity;
                        addlist.OWOPAmount = x.OWOPAmount;
                        addlist.OWOPQuantity = x.OWOPQuantity;

                        if (x.No != null)
                        {
                            ClosingQuantity = ClosingQuantity + addlist.IWQuantity - addlist.OWQuantity;
                            ClosingAmount = ClosingAmount + addlist.IWValue - addlist.OWValue /*((addlist.IWQuantity - addlist.OWQuantity) * addlist.UnitPrice)*/;
                        }
                        else
                        {
                            ClosingQuantity = ClosingQuantity + x.ClosingQuantity;
                            ClosingAmount = ClosingAmount + x.ClosingValue;
                        }

                        addlist.ClosingQuantity = ClosingQuantity;
                        addlist.ClosingValue = ClosingAmount;
                        addlist.OrderType = x.OrderType;
                        listkq.Add(addlist);

                    }
                }


            }
            else   //Sử dụng group by 2 cột để lọc giá trị gộp các bút toán giống nhau
            {
                var ReportGroup = from p in RS10
                                  group p by new
                                  {
                                      p.No,
                                      p.PostedDate,
                                      p.Account,
                                      //p.AccountCorresponding,
                                      p.UnitPrice,
                                      p.Currency,
                                      p.CurrencyID,
                                      p.MaterialGoodsName,
                                      p.MaterialGoodsCode,
                                      p.RepositoryCode,
                                      p.RepositoryName,
                                  } into rp
                                  select new S07DNN
                                  {
                                      No = rp.Key.No,
                                      PostedDate = rp.Key.PostedDate,
                                      Account = rp.Key.Account,

                                      //AccountCorresponding = rp.Key.AccountCorresponding,
                                      UnitPrice = rp.Key.UnitPrice,
                                      Reason = rp.Select(c => c.Reason).ToString(),
                                      IWQuantity = rp.Sum(c => c.IWQuantity),
                                      IWValue = rp.Sum(c => c.IWValue),
                                      OWQuantity = rp.Sum(c => c.OWQuantity),
                                      OWValue = rp.Sum(c => c.OWValue),
                                      CurrencyID = rp.Key.CurrencyID,
                                      MaterialGoodsCode = rp.Key.MaterialGoodsCode,
                                      MaterialGoodsName = rp.Key.MaterialGoodsName,
                                      RepositoryCode = rp.Key.RepositoryCode,
                                      RepositoryName = rp.Key.RepositoryName
                                  };
                // sắp xếp, tính toán các trường tồn
                List<string> lstRSCode = ReportGroup.Select(o => o.RepositoryCode).Distinct().ToList();

                foreach (var code in lstRSCode)
                {
                    List<S07DNN> ListByCode = ReportGroup.Where(o => o.RepositoryCode == code).ToList();
                    List<S07DNN> ListOPByCode = OpeningBalance.Where(o => o.RepositoryCode == code).ToList();
                    ListByCode.AddRange(ListOPByCode);
                    ListByCode = ListByCode.OrderBy(o => o.PostedDate).ToList();
                    decimal ClosingQuantity = 0;
                    decimal ClosingAmount = 0;

                    foreach (var x in ListByCode)
                    {
                        var addlist = new S07DNN();
                        addlist.RepositoryCode = x.RepositoryCode;
                        addlist.RepositoryName = x.RepositoryName;
                        addlist.Currency = x.Currency;
                        addlist.MaterialGoodsCode = x.MaterialGoodsCode;
                        addlist.MaterialGoodsName = x.MaterialGoodsName;
                        addlist.No = x.No;
                        addlist.RefType = x.RefType;
                        addlist.RefID = x.RefID;
                        addlist.PostedDate = x.PostedDate;
                        addlist.Reason = x.Reason;
                        addlist.Account = x.Account;
                        addlist.AccountCorresponding = x.AccountCorresponding;
                        addlist.Unit = x.Unit;
                        addlist.UnitPrice = x.UnitPrice;
                        addlist.IWQuantity = x.IWQuantity;
                        addlist.IWValue = x.IWValue;
                        addlist.OWQuantity = x.OWQuantity;
                        addlist.OWValue = x.OWValue;
                        addlist.CurrencyID = x.CurrencyID;
                        addlist.IWOPAmount = x.IWOPAmount;
                        addlist.IWOPQuantity = x.IWOPQuantity;
                        addlist.OWOPAmount = x.OWOPAmount;
                        addlist.OWOPQuantity = x.OWOPQuantity;

                        if (x.No != null)
                        {
                            ClosingQuantity = ClosingQuantity + addlist.IWQuantity - addlist.OWQuantity;
                            ClosingAmount = ClosingAmount + ((addlist.IWQuantity - addlist.OWQuantity) * addlist.UnitPrice);
                        }
                        else
                        {
                            ClosingQuantity = ClosingQuantity + x.ClosingQuantity;
                            ClosingAmount = ClosingAmount + x.ClosingValue;
                        }

                        addlist.ClosingQuantity = ClosingQuantity;
                        addlist.ClosingValue = ClosingAmount;
                        addlist.OrderType = x.OrderType;
                        listkq.Add(addlist);

                    }
                }
            }

            return listkq;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MaterialGoodsID"></param>
        /// <param name="RepositoryID"></param>
        /// <returns></returns>
        public List<S07DNN> GetOpeningBalanceS07DNN(Guid MaterialGoodsID, Guid RepositoryID, DateTime fromdate, DateTime todate, int lamTronGia, List<MaterialGoods> lstma, List<Repository> lstreps)//Số dư đầu kỳ
        {
            var listkq = new List<S07DNN>();
            var RS10 = (from a in Query.Where(o => (o.No == "OPN" || o.PostedDate < fromdate)
                      && o.AccountCorresponding != null
                      && o.Unit != null
                      && o.UnitPrice != null
                      && o.RepositoryID != null
                      && o.MaterialGoodsID != null).ToList()
                        join b in lstma on a.MaterialGoodsID equals b.ID
                        join d in lstreps on a.RepositoryID equals d.ID
                        where d.RepositoryCode != null
                       && d.RepositoryName != null
                       && b.MaterialGoodsCode != null
                       && b.MaterialGoodsName != null
                       //&& a.Description != null
                       && (RepositoryID == Guid.Empty ? true : a.RepositoryID == RepositoryID)
                       && b.ID == MaterialGoodsID
                        orderby a.PostedDate
                        select new S07DNN
                        {
                            RepositoryCode = d.RepositoryCode,
                            RepositoryName = d.RepositoryName,
                            Currency = "Việt Nam Đồng",
                            MaterialGoodsCode = b.MaterialGoodsCode,
                            MaterialGoodsName = b.MaterialGoodsName,
                            No = a.No,
                            //RefType=a.TypeID,
                            //RefID=a.ID,
                            PostedDate = a.PostedDate,
                            Description = a.Description,
                            Reason = a.Reason,
                            Account = d.DefaultAccount,
                            AccountCorresponding = a.AccountCorresponding,
                            Unit = a.Unit,
                            UnitPrice = a.UnitPrice,
                            IWQuantity = a.IWQuantity ?? 0,
                            OWQuantity = a.OWQuantity ?? 0,
                            IWValue = Math.Round(a.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero),
                            OWValue = Math.Round((a.OWAmount ?? 0), lamTronGia, MidpointRounding.AwayFromZero),
                            CurrencyID = "VND"
                        }).ToList();
            var ReportGroup = from p in RS10
                              group p by new
                              {
                                  p.Account,
                                  p.RepositoryCode,
                                  p.RepositoryName,
                                  p.MaterialGoodsCode,
                                  p.MaterialGoodsName,
                                  p.Currency,
                                  p.CurrencyID
                              } into rp
                              select new S07DNN
                              {
                                  Account = rp.Key.Account,
                                  Reason = "Số tồn đầu kỳ",
                                  IWQuantity = rp.Sum(o => o.IWQuantity),
                                  IWValue = rp.Sum(o => o.IWValue),
                                  OWQuantity = rp.Sum(o => o.OWQuantity),
                                  OWValue = rp.Sum(o => o.OWValue),
                                  ClosingQuantity = rp.Sum(o => o.IWQuantity) - rp.Sum(o => o.OWQuantity),
                                  ClosingValue = rp.Sum(o => o.IWValue) - rp.Sum(o => o.OWValue),
                                  CurrencyID = rp.Key.CurrencyID,
                                  Currency = rp.Key.Currency,
                                  MaterialGoodsCode = rp.Key.MaterialGoodsCode,
                                  MaterialGoodsName = rp.Key.MaterialGoodsName,
                                  RepositoryCode = rp.Key.RepositoryCode,
                                  RepositoryName = rp.Key.RepositoryName,
                                  OrderType = -1,
                              };
            foreach (var y in ReportGroup)
            {
                var addlist = new S07DNN();
                addlist.RepositoryCode = y.RepositoryCode;
                addlist.RepositoryName = y.RepositoryName;
                addlist.Currency = y.Currency;
                addlist.MaterialGoodsCode = y.MaterialGoodsCode;
                addlist.MaterialGoodsName = y.MaterialGoodsName;
                addlist.No = y.No;
                //addlist.RefID = y.RefID;
                //addlist.RefType = y.RefType;
                addlist.PostedDate = y.PostedDate;
                addlist.Reason = y.Reason;
                addlist.Account = y.Account;
                addlist.AccountCorresponding = y.AccountCorresponding;
                addlist.Unit = y.Unit;
                addlist.UnitPrice = y.UnitPrice;
                addlist.IWOPQuantity = y.IWQuantity;
                addlist.IWOPAmount = y.IWValue;
                addlist.OWOPQuantity = y.OWQuantity;
                addlist.OWOPAmount = y.OWValue;
                addlist.ClosingQuantity = y.ClosingQuantity;
                addlist.ClosingValue = y.ClosingValue;
                addlist.CurrencyID = y.CurrencyID;
                addlist.OrderType = y.OrderType;

                listkq.Add(addlist);
            }

            return listkq;
        }
        #endregion

        public List<MaterialGoodsReport> getMaterialGoodsInLedger(DateTime fromDate, DateTime toDate, Guid? repositoryID, Guid? materialGoodsCategoryID)
        {
            List<Guid> listMaterialGoodsInLedgerID = new List<Guid>();
            List<MaterialGoodsReport> listMaterialGoodsInLedger = new List<MaterialGoodsReport>();
            // lấy danh sách ID các vật tư, hàng hóa được lưu kho
            var query = Query;
            if (fromDate != DateTime.MinValue)
            {
                query = query.Where(o => o.PostedDate >= fromDate);
            }

            if (toDate != DateTime.MinValue)
            {
                DateTime endDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 29, 59, 59);
                query = query.Where(o => o.PostedDate <= endDate);
            }

            if (repositoryID != null && repositoryID != Guid.Empty)
            {
                query = query.Where(o => o.RepositoryID == repositoryID);
            }

            listMaterialGoodsInLedgerID = query.Select(o => o.MaterialGoodsID).Distinct().ToList();

            // lấy danh sách vật tư hàng hóa
            var materialQuery = _IMaterialGoodsService.Query.Where(o => listMaterialGoodsInLedgerID.Contains(o.ID));

            if (materialGoodsCategoryID != null && materialGoodsCategoryID != Guid.Empty)
            {
                materialQuery = materialQuery.Where(o => o.MaterialGoodsCategoryID == materialGoodsCategoryID);
            }

            listMaterialGoodsInLedger = materialQuery.OrderBy(o => o.MaterialGoodsCode).Select(o => new MaterialGoodsReport()
            {
                ID = o.ID,
                MaterialGoodsCode = o.MaterialGoodsCode,
                MaterialGoodsName = o.MaterialGoodsName,
                RepositoryID = repositoryID ?? Guid.Empty,
                MaterialGoodsCategoryID = o.MaterialGoodsCategoryID ?? Guid.Empty,
                MaterialGoodsGSTID = o.MaterialGoodsGSTID ?? Guid.Empty
            }).ToList();

            return listMaterialGoodsInLedger;
        }

        public List<S07DNN16> RS07DNN16(DateTime from, DateTime to, List<string> AccountNumbers)
        {
            var ddSoGia = _ISystemOptionService.Query.FirstOrDefault(c => c.Code == "DDSo_TienVND");
            int lamTronGia = 0;
            if (ddSoGia != null)
                lamTronGia = Convert.ToInt32(ddSoGia.Data);
            List<S07DNN16> output = new List<S07DNN16>();
            foreach (string x in AccountNumbers)
            {
                List<S07DNN16> list1 = GetRS07DNN16(from, to, x, lamTronGia);
                output.AddRange(list1);
            }
            return output;
        }

        public List<S07DNN16> GetRS07DNN16(DateTime fromdate, DateTime todate, string AccountNumber, int lamTronGia)
        {
            DateTime to = new DateTime(todate.Year, todate.Month, todate.Day, 23, 59, 59);
            DateTime froms = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day, 0, 0, 0);
            var listkq = new List<S07DNN16>();
            var lst = Query.Where(o => o.No != null && o.Account != null && o.RepositoryID != null && o.MaterialGoodsID != null && o.Account.StartsWith(AccountNumber)).ToList();

            var list_delete = IoC.Resolve<ITT153DeletedInvoiceService>().Query.ToList();
            var listSABill = IoC.Resolve<ISABillDetailService>().Query;
            var listSAInvoice = IoC.Resolve<ISAInvoiceService>().Query;
            var list_genera1 = (from a in lst
                                join b in list_delete on a.ReferenceID equals b.SAInvoiceID
                                select a).ToList();
            var list_genera2 = (from a in lst
                                join i in listSAInvoice on a.ReferenceID equals i.ID
                                //join b in listSABill on i.BillRefID equals b.ID
                                join d in list_delete on i.BillRefID equals d.SAInvoiceID
                                select a).ToList();
            lst = (from a in lst
                   where !list_genera1.Any(t => t.ID == a.ID) && !list_genera2.Any(t => t.ID == a.ID)
                   select a).ToList();

            var RS10 = (from a in lst
                        join b in _IMaterialGoodsService.Query.ToList() on a.MaterialGoodsID equals b.ID
                        join d in _IRepositoryService.Query.ToList() on a.RepositoryID equals d.ID
                        where d.RepositoryCode != null
                       && d.RepositoryName != null
                       && b.MaterialGoodsCode != null
                       && b.MaterialGoodsName != null
                        //&& AccountNumber.StartsWith(d.DefaultAccount)
                        orderby a.RefDateTime
                        select new S07DNN16
                        {
                            No = a.No,
                            RepositoryCode = d.RepositoryCode,
                            RepositoryName = d.RepositoryName,
                            Currency = "Việt Nam Đồng",
                            MaterialGoodsCode = b.MaterialGoodsCode,
                            MaterialGoodsName = b.MaterialGoodsName,
                            PostedDate = a.PostedDate,
                            Description = a.Description,
                            Reason = a.Reason,
                            Account = a.Account,
                            AccountCorresponding = a.AccountCorresponding,
                            Unit = a.Unit,
                            UnitPrice = a.UnitPrice,
                            IWQuantity = a.IWQuantity ?? 0,
                            IWValue = Math.Round(a.IWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero),
                            OWQuantity = a.OWQuantity ?? 0,
                            OWValue = Math.Round(a.OWAmount ?? 0, lamTronGia, MidpointRounding.AwayFromZero),
                            CurrencyID = "VND"
                        }).ToList();
            // nhóm các bút toán giống nhau
            var ReportGroup = from p in RS10
                              group p by new
                              {
                                  p.MaterialGoodsCode,
                                  p.Account,
                                  p.MaterialGoodsName,
                                  p.CurrencyID
                              } into rp
                              select new S07DNN16
                              {
                                  MaterialGoodsCode = rp.Key.MaterialGoodsCode,
                                  MaterialGoodsName = rp.Key.MaterialGoodsName,
                                  Account = rp.Key.Account,
                                  CurrencyID = rp.Key.CurrencyID,
                                  IWQuantity = rp.Where(o => o.PostedDate <= to && o.PostedDate >= froms && o.No != "OPN").Sum(c => c.IWQuantity),
                                  IWValue = rp.Where(o => o.PostedDate <= to && o.PostedDate >= froms && o.No != "OPN").Sum(c => Math.Round(c.IWValue, lamTronGia, MidpointRounding.AwayFromZero)),
                                  OWQuantity = rp.Where(o => o.PostedDate <= to && o.PostedDate >= froms && o.No != "OPN").Sum(c => c.OWQuantity),
                                  OWValue = rp.Where(o => o.PostedDate <= to && o.PostedDate >= froms && o.No != "OPN").Sum(c => Math.Round(c.OWValue, lamTronGia, MidpointRounding.AwayFromZero)),
                                  OPQuantity = rp.Where(o => o.PostedDate < froms || o.No == "OPN").Sum(c => c.IWQuantity) - rp.Where(o => o.PostedDate < froms || o.No == "OPN").Sum(c => c.OWQuantity),
                                  OPAmount = rp.Where(o => o.PostedDate < froms || o.No == "OPN").Sum(c => Math.Round(c.IWValue, lamTronGia, MidpointRounding.AwayFromZero)) - rp.Where(o => o.PostedDate < froms || o.No == "OPN").Sum(c => Math.Round(c.OWValue, lamTronGia, MidpointRounding.AwayFromZero)),
                              };
            foreach (var y in ReportGroup)
            {
                var addlist = new S07DNN16();
                addlist.RepositoryCode = y.RepositoryCode;
                addlist.RepositoryName = y.RepositoryName;
                addlist.Currency = y.Currency;
                addlist.MaterialGoodsCode = y.MaterialGoodsCode;
                addlist.MaterialGoodsName = y.MaterialGoodsName;
                addlist.PostedDate = y.PostedDate;
                addlist.Description = y.Reason;
                addlist.Account = y.Account;
                addlist.AccountCorresponding = y.AccountCorresponding;
                addlist.Unit = y.Unit;
                addlist.UnitPrice = y.UnitPrice;
                addlist.IWQuantity = y.IWQuantity;
                addlist.IWValue = y.IWValue;
                addlist.OWQuantity = y.OWQuantity;
                addlist.OWValue = y.OWValue;
                addlist.ClosingQuantity = y.OPQuantity + addlist.IWQuantity - addlist.OWQuantity;
                addlist.ClosingValue = y.OPAmount + addlist.IWValue - addlist.OWValue;
                addlist.OPAmount = y.OPAmount;
                addlist.CurrencyID = y.CurrencyID;

                listkq.Add(addlist);
            }
            //S07DNN16 OpeningBalance = GetOpeningBalanceS07DNN(MaterialGoodsID, RepositoryID);
            //OpeningBalance.MaterialGoodsCode = _IMaterialGoodsService.Query.Select(c => c.MaterialGoodsCode).ToString();
            //listkq.Add(OpeningBalance); // add thêm dòng sô dư đầu kỳ                                                
            return listkq.OrderBy(o => o.MaterialGoodsCode).ToList();
        }
    }

}