﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class TypeGroupService : BaseService<TypeGroup, int>, ITypeGroupService
    {
        public TypeGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}