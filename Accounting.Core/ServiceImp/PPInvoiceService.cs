﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Accounting.Core.Domain;
using FX.Core;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class PPInvoiceService : BaseService<PPInvoice, Guid>, IPPInvoiceService
    {
        ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        IPPDiscountReturnService _IPPDiscountReturnService { get { return IoC.Resolve<IPPDiscountReturnService>(); } }
        public PPInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public PPInvoice GetPPInvoicebyNo(string no)
        {
            return Query.SingleOrDefault(k => k.No == no);
        }
        public List<PPInvoice> GetByBillReceived(bool billReceived, Guid? accountingObjectID, bool isRecorded,
                                         DateTime dtBegin, DateTime dtEnd)
        {
            var qr = accountingObjectID.HasValue ? Query.Where(i => i.AccountingObjectID == accountingObjectID.Value) : Query;
            qr = qr.Where(i => i.BillReceived == billReceived && i.Recorded == isRecorded && i.PostedDate >= dtBegin && i.PostedDate <= dtEnd);

            return qr.ToList();
        }
        public List<PPInvoice> GetByBillReceived(bool billReceived, Guid? accountingObjectID, bool isRecorded,
                                         DateTime dtBegin, DateTime dtEnd, bool? isImport)
        {
            var qr = accountingObjectID.HasValue ? Query.Where(i => i.AccountingObjectID == accountingObjectID.Value) : Query;
            qr = qr.Where(i => i.BillReceived == billReceived && i.Recorded == isRecorded && i.PostedDate >= dtBegin && i.PostedDate <= dtEnd);

            if (isImport != null)
            {
                qr = qr.Where(i => i.IsImportPurchase == isImport);
            }

            return qr.ToList();
        }
        IEnumerable<PurchaseDetailSupplerInventoryItem> ReportPurchaseDetailSupplerInventoryItem(DateTime fromDate, DateTime toDate, Guid accountingObjectID,
            List<Guid> materialGoodsID, bool accountingObjectIDinPpInvoices)
        {
            List<PPInvoice> lstPpInvoices = Query.ToList();
            var lstPpDiscountReturnDetailsDiscounts = _IPPDiscountReturnService.GetAll().Where(c => c.TypeID == 220 && c.PostedDate >= fromDate && c.PostedDate <= toDate).SelectMany(c => c.PPDiscountReturnDetails).ToList();
            //lấy ra danh sách hàng trả lại theo typeid và thời gian từ ngày - đến ngày
            var lstPpDiscountReturnDetailsReturn = _IPPDiscountReturnService.GetAll().Where(c => c.TypeID == 230 && c.PostedDate >= fromDate && c.PostedDate <= toDate).SelectMany(c => c.PPDiscountReturnDetails).ToList();
            //lấy ra toàn bộ ds chi tiết hóa đơn bán
            List<PPInvoiceDetail> lstPpInvoiceDetails = lstPpInvoices.SelectMany(c => c.PPInvoiceDetails).ToList();
            var lstrReportPPinvoices = new List<PurchaseDetailSupplerInventoryItem>();
            //lấy ra danh sách id hóa đơn bán và các thông tin đi kèm theo nhà cung cấp, từ ngày - đến ngày
            var no = from i in lstPpInvoices
                     where (accountingObjectIDinPpInvoices || i.AccountingObjectID.Equals(accountingObjectID))
                     && i.PostedDate >= fromDate && i.PostedDate <= toDate
                     select new { i.ID, i.No, i.AccountingObjectID, i.NumberAttach, i.PostedDate, i.TypeID, i.AccountingObjectName };
            //lấy từng id ra để so sánh
            foreach (var n in no)
            {
                var n1 = n;
                //lấy ra danh sách nhóm mã mặt hàng và tổng giá trị được giảm giá của mặt hàng đó trong ds hàng giảm giá
                //theo mã hóa đơn và mặt hàng đó có tồn tại trong ds mã mặt hàng truyền vào
                //check null
                var sumDiscountsTotalAmount = (from gg in lstPpDiscountReturnDetailsDiscounts
                                               where
                                                   gg.MaterialGoodsID != null && gg.ConfrontID != null &&
                                                   materialGoodsID.Contains((Guid)gg.MaterialGoodsID)
                                                   && gg.ConfrontID.Equals(n1.ID)
                                               group gg by new { gg.MaterialGoodsID, gg.ConfrontID }
                                                   into t
                                                   select new
                                                   {
                                                       DiscountsTotalAmount = t.Sum(t1 => t1.Amount),
                                                       MaterialGoodsIDs = t.Key.MaterialGoodsID
                                                   }).ToList();
                //lấy ra danh sách nhóm mã mặt hàng ,tổng giá trị hàng trả lại và số lượng trả lại của mặt hàng đó trong ds hàng trả lại
                // theo mã hóa đơn và mặt hàng đó có tồn tại trong ds mã mặt hàng truyền vào
                //check null
                var sumDiscountReturn = (from tl in lstPpDiscountReturnDetailsReturn
                                         where tl.MaterialGoodsID != null && tl.ConfrontID != null &&
                                             materialGoodsID.Contains((Guid)tl.MaterialGoodsID)
                                             && tl.ConfrontID.Equals(n1.ID)
                                         group tl by new { tl.MaterialGoodsID, tl.ConfrontID }
                                             into t
                                             select new
                                             {
                                                 MaterialGoodsIDs = t.Key.MaterialGoodsID,
                                                 ReturnTotalAmount = t.Sum(t1 => t1.Amount),
                                                 QuantityReturn = t.Sum(t1 => t1.Quantity)
                                             }).ToList();
                //lấy ra đối tượng cần in báo cáo
                var ak = (from id in lstPpInvoiceDetails //lấy từng đối tượng chi tiết hóa đơn trong ds chi tiết hóa đơn (mỗi đối tượng chỉ chứa một mã mặt hàng và id hóa đơn tương ứng)
                          where n1 != null //check null
                          let quantity = id.Quantity //đặt số lượng trong hóa đơn bằng quantity
                          where materialGoodsID != null && (materialGoodsID.Contains((Guid)id.MaterialGoodsID)
                                                            && n1.ID.Equals(id.PPInvoiceID))
                                && (accountingObjectIDinPpInvoices == false || id.AccountingObjectID.Equals(accountingObjectID))
                          //lọc trong danh sách chi tiết hóa đơn bán theo mã hóa đơn và mặt hàng đó có tồn tại trong ds mã mặt hàng truyền vào
                          where quantity != null //check null
                          let discountsTotalAmount = sumDiscountsTotalAmount.Where(c => c.MaterialGoodsIDs == id.MaterialGoodsID).Select(c => c.DiscountsTotalAmount).FirstOrDefault()
                          //lấy ra tổng giá trị giảm giá theo mã mặt hàng
                          where discountsTotalAmount != null
                          let lstdiscountReturn = sumDiscountReturn
                          where lstdiscountReturn != null
                          let returnTotalAmount = lstdiscountReturn.Where(c => c.MaterialGoodsIDs == id.MaterialGoodsID).Select(c => c.ReturnTotalAmount).FirstOrDefault()
                          //lấy ra tổng giá trị trả lại theo mã mặt hàng
                          where returnTotalAmount != null
                          let quantityReturn = lstdiscountReturn.Where(c => c.MaterialGoodsIDs == id.MaterialGoodsID).Select(c => c.QuantityReturn).FirstOrDefault()
                          //lấy ra tổng số lượng hàng trả lại theo mã mặt hàng
                          where quantityReturn != null
                          select new PurchaseDetailSupplerInventoryItem()
                          {
                              AccountingObjectCode =
                                  _IAccountingObjectService.Getbykey((Guid)n1.AccountingObjectID).AccountingObjectCode,
                              AccountingObjectName = n1.AccountingObjectName,
                              PostedDate = n1.PostedDate,
                              NumberAttach = n1.NumberAttach ?? n1.No,
                              TypeName = _ITypeService.Getbykey(n1.TypeID).TypeName,
                              Description = id.Description,
                              Quantity = (decimal)quantity,
                              UnitPrice = id.UnitPrice,
                              Amount = id.Amount,
                              DiscountAmount = id.DiscountAmount,
                              DiscountTotalAmount = (decimal)discountsTotalAmount,
                              ReturnTotalAmount = (decimal)returnTotalAmount,
                              ReturnQuantity = (decimal)quantityReturn,
                              MaterialGoodsCode = id.MaterialGoodsCode,
                              MaterialGoodsName = id.MaterialGoodsName,
                          }).ToList();
                //đưa danh sách tìm được vào list kết quả
                lstrReportPPinvoices.AddRange(ak.ToList());
            }
            foreach (var detailedBooksForPurchasese in lstrReportPPinvoices)
            {
                detailedBooksForPurchasese.AddHyperLink();
            }
            return lstrReportPPinvoices;
        }

        /// <summary>
        /// Sổ chi tiết mua hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="accountingObjectID">id nhà cung cấp</param>
        /// <param name="materialGoodsID">ds id mặt hàng</param>
        /// <param name="accountingObjectIDinPpInvoices">bằng true - không lấy theo đối tượng trên thông tin chung</param>
        /// <returns></returns>
        public List<PurchaseDetailSupplerInventoryItem> ReportPurchaseDetailSupplerInventoryItem(DateTime fromDate, DateTime toDate, IEnumerable<Guid> accountingObjectID,
            List<Guid> materialGoodsID, bool accountingObjectIDinPpInvoices)
        {
            var lstrReportPPinvoices = new List<PurchaseDetailSupplerInventoryItem>();
            foreach (Guid guid in accountingObjectID)
            {
                var reportPPinvoices = ReportPurchaseDetailSupplerInventoryItem(fromDate, toDate, guid, materialGoodsID, accountingObjectIDinPpInvoices);
                lstrReportPPinvoices.AddRange(reportPPinvoices);
            }
            return lstrReportPPinvoices;
        }

        /// <summary>
        /// Sổ nhật ký mua hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <returns></returns>
        public List<S03A3DNN> ReportS03A3Dn(DateTime fromDate, DateTime toDate)
        {
            var creditAccountRawMaterials = new List<string>() { "152", "153" };
            return (from a in Query
                    from b in a.PPInvoiceDetails
                    where a.PostedDate >= fromDate && a.PostedDate <= toDate
                    select new S03A3DNN()
                    {
                        PostedDate = a.PostedDate,
                        Date = a.Date,
                        No = a.OriginalNo ?? a.No,
                        Description = b.Description,
                        Amount156 = b.CreditAccount.StartsWith("156") ? b.Amount : 0,
                        Amount152 =
                            creditAccountRawMaterials.Contains(b.CreditAccount.Substring(0, 3)) ? b.Amount : 0,
                        AccountOther =
                            (!b.CreditAccount.StartsWith("156") &&
                             !creditAccountRawMaterials.Contains(b.CreditAccount.Substring(0, 3)))
                                ? b.CreditAccount
                                : "",
                        AmountOther =
                            (!b.CreditAccount.StartsWith("156") &&
                             !creditAccountRawMaterials.Contains(b.CreditAccount.Substring(0, 3)))
                                ? b.Amount
                                : 0,
                        PayableAmount = b.Amount,
                        Hyperlink = a.TypeID + ";" + a.ID
                    }).ToList();
        }

        public List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null)
        {
            List<PPInvoice> lst = new List<PPInvoice>();
            if(currencyID == null) lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                     && x.Recorded ).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            else lst = Query.Where(x => x.Date >= dateTime1 && x.Date <= dateTime2
                    && x.Recorded &&  x.CurrencyID == currencyID).OrderByDescending(s => s.PostedDate).ThenByDescending(s => s.No).ToList();
            List<TIOriginalVoucher> result = new List<TIOriginalVoucher>();
            foreach (PPInvoice io in lst)
            {
                TIOriginalVoucher ov = new TIOriginalVoucher()
                {
                    Amount = io.TotalAmountOriginal,
                    PostedDate = io.PostedDate,
                    Date = io.Date,
                    No = io.No,
                    Reason = io.Reason,
                    ID = io.ID

                };
                result.Add(ov);
            }
            return result;
        }

        public List<TIOriginalVoucher> FindByListID(string originalVoucher)
        {
            string[] ss = originalVoucher.Substring(0, originalVoucher.Length - 1).Split(';');
            List<Guid> guids = new List<Guid>();
            foreach (string s in ss)
            {
                guids.Add(Guid.Parse(s));
            }
            return Query.Where(haha => guids.Contains(haha.ID)).Select(hihi => new TIOriginalVoucher
            {
                PostedDate = hihi.PostedDate,
                Date = hihi.Date,
                No = hihi.No,
                Reason = hihi.Reason,
                Amount = hihi.TotalAmountOriginal,
                ID = hihi.ID
            }).ToList();
        }
    }
}