using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class BankService : BaseService<Bank, Guid>, IBankService
    {
        public BankService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<Bank> GetListBankIsActive(bool isActive)
        {
            List<Bank> lstBank = Query.Where(c => c.IsActive == isActive).ToList();
            return lstBank;
        }

        public List<string> GetListBankCode()
        {
            List<string> list = Query.Select(p => p.BankCode).ToList();
            return list;
        }

        public List<Bank> GetOrderby()
        {
            return Query.OrderBy(p => p.BankName).ToList();
        }
        public List<Bank> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.BankCode).ToList();
        }
    }

}