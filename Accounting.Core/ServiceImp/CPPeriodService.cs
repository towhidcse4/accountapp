﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class CPPeriodService : BaseService<CPPeriod, Guid>, ICPPeriodService
    {
        public CPPeriodService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private ICPPeriodService ICPPeriodService { get { return IoC.Resolve<ICPPeriodService>(); } }
        private ICPPeriodDetailService ICPPeriodDetailService { get { return IoC.Resolve<ICPPeriodDetailService>(); } }
        private ICPAcceptanceService ICPAcceptanceService { get { return IoC.Resolve<ICPAcceptanceService>(); } }
        private ICPAcceptanceDetailService ICPAcceptanceDetailService { get { return IoC.Resolve<ICPAcceptanceDetailService>(); } }
        private ICPOPNService ICPOPNService { get { return IoC.Resolve<ICPOPNService>(); } }
        private IExpenseItemService IExpenseItemService { get { return IoC.Resolve<IExpenseItemService>(); } }
        public List<CPCostingPeriod> GetCPCostingPeriod()
        {
            List<CPCostingPeriod> lst = new List<CPCostingPeriod>();
            return lst;
        }
        public List<CostPeriod> GetAllCostPeriod(int type)
        {
            List<CPPeriod> LstCPPeriod = ICPPeriodService.Query.Where(x => x.Type == type).OrderBy(c=>c.FromDate).ToList();
            List<CPAcceptance> LstCPAcceptance = ICPAcceptanceService.GetAll();
            List<CPOPN> LstOPN = ICPOPNService.GetAll();
            List<CostPeriod> lst = new List<CostPeriod>();
            for (int i = 0; i < LstCPPeriod.Count; i++)
            {
                CostPeriod a = new CostPeriod();
                a.ID = LstCPPeriod[i].ID;
                a.FromDate = LstCPPeriod[i].FromDate;
                a.ToDate = LstCPPeriod[i].ToDate;
                a.CostingPeriodName = LstCPPeriod[i].Name;
                a.AcceptedAmount = LstCPPeriod[i].CPAcceptances.Count > 0 ? LstCPPeriod[i].CPAcceptances.Sum(c=>c.TotalAmount) : 0;               
                decimal b = LstCPPeriod[i].CPExpenseLists.ToList().Where(c => c.TypeVoucher == 0).Sum(c => c.Amount);
                decimal d = LstCPPeriod[i].CPExpenseLists.ToList().Where(c => c.TypeVoucher == 1).Sum(c => c.Amount);
                decimal e = LstCPPeriod[i].CPAllocationGeneralExpenses.Sum(c => c.AllocatedAmount);
                a.ExpensesDuringPeriod = (b - d + e);                              
                a.AccumulatedAllocateAmount = AccumulatedAllocateAmount(LstCPPeriod[i].CPPeriodDetails.Select(x=> LstCPPeriod[i].Type != 5 ? x.CostSetID : x.ContractID).ToList(), LstCPPeriod.Where(x=>x.ToDate < LstCPPeriod[i].ToDate).ToList(), LstOPN, LstCPPeriod[i].Type);
                a.UnAcceptedAmount = ((a.ExpensesDuringPeriod + a.AccumulatedAllocateAmount) - a.AcceptedAmount);
                lst.Add(a);
            }
            return lst.OrderByDescending(x=>x.FromDate).ToList();
        }
        public decimal AccumulatedAllocateAmount(List<Guid> lstID, List<CPPeriod> lstCPPeriod, List<CPOPN> LstOPN, int type)
        {
            decimal total = 0;           
            foreach (var id in lstID)
            {
                foreach(var md in lstCPPeriod)
                {
                    decimal a = md.CPExpenseLists.Where(c => (type != 5 ? c.CostSetID == id : c.ContractID == id) && c.TypeVoucher == 0).Sum(c => c.Amount);
                    decimal b = md.CPExpenseLists.Where(c => (type != 5 ? c.CostSetID == id : c.ContractID == id) && c.TypeVoucher == 1).Sum(c => c.Amount);
                    decimal h = 0;
                    foreach (var allo in md.CPAllocationGeneralExpenses.Where(C => C.AllocatedRate > 0).ToList())
                    {
                        h = h + allo.CPAllocationGeneralExpenseDetails.Where(c => (type != 5 ? c.CostSetID == id : c.ContractID == id)).Sum(c => c.AllocatedAmount);
                    }
                    decimal lk = 0;
                    if(md.CPAcceptances.Count > 0)
                    {
                        foreach (var item in md.CPAcceptances)
                        {
                            var g = item.CPAcceptanceDetails.FirstOrDefault(x => (type != 5 ? x.CostSetID == id : x.ContractID == id));
                            if (g != null) lk = lk + g.TotalAcceptedAmount;
                        }
                    }
                    total = total + (a - b + h - lk);
                }
                if(LstOPN.Any(x=> (type != 5 ? x.CostSetID == id : x.ContractID == id)))
                {
                    total = total + LstOPN.FirstOrDefault(x => (type != 5 ? x.CostSetID == id : x.ContractID == id)).NotAcceptedAmount??0;
                }
            }
            return total;
        }
        public List<RCost> GetListRCost(List<CostSet> lst, Guid ID)
        {
            List<RCost> lstCost = new List<RCost>();
            CPPeriod CPPeriod = ICPPeriodService.Query.FirstOrDefault(x => x.ID == ID);
            List<CPPeriod> lstLuyKe = ICPPeriodService.Query.Where(x => x.ToDate < CPPeriod.FromDate && x.Type == CPPeriod.Type).OrderByDescending(x => x.ToDate).ToList();
            List<CPOPN> LstOPN = ICPOPNService.Query.ToList().Where(x => lst.Any(d => d.ID == x.CostSetID)).ToList();
            List<ExpenseItem> LstExpen = IExpenseItemService.GetAll();
            foreach (var x in lst)
            {
                decimal allo1 = 0;
                decimal allo2 = 0;
                decimal allo3 = 0;
                decimal exp01 = 0;
                decimal exp02 = 0;
                decimal exp03 = 0;
                decimal exp11 = 0;
                decimal exp12 = 0;
                decimal exp13 = 0;
                decimal acp = 0;
                RCost md = new RCost();
                md.ID = x.ID;
                md.FromDate = CPPeriod.FromDate;
                md.ToDate = CPPeriod.ToDate;
                md.CostSetName = x.CostSetName;
                if (lstLuyKe.Count > 0)
                {
                    md.UncompleteFirstMaterial = 0;//2.1
                    md.UncompleteFirstLabor = 0;//3.1
                    md.UncompleteFirstCost = 0;//4.1
                    var NotAccLK = lstLuyKe[0].CPAcceptances.Count > 0 ? lstLuyKe[0].CPAcceptances.OrderBy(c => c.OrderPriority).ToList()[0].CPAcceptanceDetails.FirstOrDefault(d => d.CostSetID == x.ID).Amount : 0;// chi phí phát sinh trong kỳ của kỳ trước của Costset
                    decimal accLK = 0;
                    foreach(var item in lstLuyKe[0].CPAcceptances)
                    {
                        var AcceptanceDetail = item.CPAcceptanceDetails.FirstOrDefault(u => u.CostSetID == x.ID);
                        if (AcceptanceDetail != null) accLK = accLK + AcceptanceDetail.TotalAcceptedAmount;
                    }
                    md.UncompleteFirstAmount = (NotAccLK - accLK);//1.1                   
                }
                else
                {
                    md.UncompleteFirstMaterial = LstOPN.Any(d => d.CostSetID == x.ID) ? LstOPN.FirstOrDefault(d => d.CostSetID == x.ID).DirectMaterialAmount ?? 0 : 0;
                    md.UncompleteFirstLabor = LstOPN.Any(d => d.CostSetID == x.ID) ? LstOPN.FirstOrDefault(d => d.CostSetID == x.ID).DirectLaborAmount ?? 0 : 0;
                    md.UncompleteFirstCost = LstOPN.Any(d => d.CostSetID == x.ID) ? LstOPN.FirstOrDefault(d => d.CostSetID == x.ID).GeneralExpensesAmount ?? 0 : 0;
                    md.UncompleteFirstAmount = LstOPN.Any(d => d.CostSetID == x.ID) ? LstOPN.FirstOrDefault(d => d.CostSetID == x.ID).TotalCostAmount ?? 0 : 0;
                }
                var lstAllo = CPPeriod.CPAllocationGeneralExpenses.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 0).ToList();
                if (lstAllo.Count != 0)
                {
                    foreach(var item in lstAllo)
                    {
                        allo1 = allo1 + item.CPAllocationGeneralExpenseDetails.Where(a => a.CostSetID == x.ID).Sum(t => t.AllocatedAmount);
                    }
                }
                var lstexp01 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 0 && d.CostSetID == x.ID).ToList();
                if(lstexp01.Count != 0)
                {
                    exp01 = lstexp01.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 0).Sum(t => t.Amount);
                }
                var lstexp11 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 1 && d.CostSetID == x.ID).ToList();
                if (lstexp11.Count != 0)
                {
                    exp11 = lstexp11.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 0).Sum(t => t.Amount);
                }
                md.IncurredMaterial =   (exp01 - exp11 + allo1); //2.2

                var lstAllo2 = CPPeriod.CPAllocationGeneralExpenses.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 1).ToList();
                if (lstAllo2 != null)
                {
                    foreach (var item in lstAllo2)
                    {
                        allo2 = allo2 + item.CPAllocationGeneralExpenseDetails.Where(a => a.CostSetID == x.ID).Sum(t => t.AllocatedAmount);
                    }
                }
                var lstexp02 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 0 && d.CostSetID == x.ID).ToList();
                if (lstexp02.Count != 0)
                {
                    exp02 = lstexp02.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 1).Sum(t => t.Amount);
                }
                var lstexp12 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 1 && d.CostSetID == x.ID).ToList();
                if (lstexp12.Count != 0)
                {
                    exp12 = lstexp12.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 1).Sum(t => t.Amount);
                }
                md.IncurredLabor = (exp02 - exp12 + allo2);// 3.2

                var lstAllo3 = CPPeriod.CPAllocationGeneralExpenses.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 2).ToList();
                if (lstAllo3 != null)
                {
                    foreach (var item in lstAllo3)
                    {
                        allo3 = allo3 + item.CPAllocationGeneralExpenseDetails.Where(a => a.CostSetID == x.ID).Sum(t => t.AllocatedAmount);
                    }
                }
                var lstexp03 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 0 && d.CostSetID == x.ID).ToList();
                if (lstexp03.Count != 0)
                {
                    exp03 = lstexp03.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 2).Sum(t => t.Amount);
                }
                var lstexp13 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 1 && d.CostSetID == x.ID).ToList();
                if (lstexp13.Count != 0)
                {
                    exp13 = lstexp13.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 2).Sum(t => t.Amount);
                }
                md.IncurredCost = (exp03 - exp13 + allo3); //4.2
                md.IncurredAmount = md.IncurredMaterial + md.IncurredLabor + md.IncurredCost; //1.2
                md.UncompleteLastMaterial = md.UncompleteFirstMaterial + md.IncurredMaterial; //2.3
                md.UncompleteLastLabor = md.UncompleteFirstLabor + md.IncurredLabor; //3.3
                md.UncompleteLastCost = md.UncompleteFirstCost + md.IncurredCost; //4.3
                md.UncompleteLastAmount = md.UncompleteFirstAmount + md.IncurredAmount;//1.3
                if(CPPeriod.CPAcceptances.Count > 0)
                {
                    foreach (var item in CPPeriod.CPAcceptances)
                    {
                        acp = acp + item.CPAcceptanceDetails.FirstOrDefault(d => d.CostSetID == x.ID).TotalAcceptedAmount;
                    }
                }
                md.AcceptedAmount = acp;//1.4
                md.UnAcceptedAmount = CPPeriod.CPAcceptances.Count > 0 ? CPPeriod.CPAcceptances.FirstOrDefault(c=>c.OrderPriority == 0).CPAcceptanceDetails.FirstOrDefault(d => d.CostSetID == x.ID).Amount - md.AcceptedAmount : 0;//1.5
                lstCost.Add(md);
            }
            return lstCost;
        }
        public List<RCost> GetListRCost(List<EMContract> lst, Guid ID)
        {
            List<RCost> lstCost = new List<RCost>();
            CPPeriod CPPeriod = ICPPeriodService.Query.FirstOrDefault(x => x.ID == ID);
            List<CPPeriod> lstLuyKe = ICPPeriodService.Query.Where(x => x.ToDate < CPPeriod.FromDate).OrderByDescending(x => x.ToDate).ToList();
            List<CPOPN> LstOPN = ICPOPNService.Query.ToList().Where(x => lst.Any(d => d.ID == x.ContractID)).ToList();
            List<ExpenseItem> LstExpen = IExpenseItemService.GetAll();
            foreach (var x in lst)
            {
                decimal allo1 = 0;
                decimal allo2 = 0;
                decimal allo3 = 0;
                decimal exp01 = 0;
                decimal exp02 = 0;
                decimal exp03 = 0;
                decimal exp11 = 0;
                decimal exp12 = 0;
                decimal exp13 = 0;
                decimal acp = 0;
                RCost md = new RCost();
                md.ID = x.ID;
                md.FromDate = CPPeriod.FromDate;
                md.ToDate = CPPeriod.ToDate;
                md.CostSetName = x.Code;
                if (lstLuyKe.Count > 0)
                {
                    md.UncompleteFirstMaterial = 0;//2.1
                    md.UncompleteFirstLabor = 0;//3.1
                    md.UncompleteFirstCost = 0;//4.1
                    var NotAccLK = lstLuyKe[0].CPAcceptances.Count > 0 ? lstLuyKe[0].CPAcceptances.OrderBy(c => c.OrderPriority).ToList()[0].CPAcceptanceDetails.FirstOrDefault(d => d.ContractID == x.ID).Amount : 0;// chi phí phát sinh trong kỳ của kỳ trước của Costset
                    decimal accLK = 0;
                    foreach (var item in lstLuyKe[0].CPAcceptances)
                    {
                        var AcceptanceDetail = item.CPAcceptanceDetails.FirstOrDefault(u => u.ContractID == x.ID);
                        if (AcceptanceDetail != null) accLK = accLK + AcceptanceDetail.TotalAcceptedAmount;
                    }
                    md.UncompleteFirstAmount = (NotAccLK - accLK);
                }
                else
                {
                    md.UncompleteFirstMaterial = LstOPN.Any(d => d.ContractID == x.ID) ? LstOPN.FirstOrDefault(d => d.ContractID == x.ID).DirectMaterialAmount ?? 0 : 0;
                    md.UncompleteFirstLabor = LstOPN.Any(d => d.ContractID == x.ID) ? LstOPN.FirstOrDefault(d => d.ContractID == x.ID).DirectLaborAmount ?? 0 : 0;
                    md.UncompleteFirstCost = LstOPN.Any(d => d.ContractID == x.ID) ? LstOPN.FirstOrDefault(d => d.ContractID == x.ID).GeneralExpensesAmount ?? 0 : 0;
                    md.UncompleteFirstCost = LstOPN.Any(d => d.ContractID == x.ID) ? LstOPN.FirstOrDefault(d => d.ContractID == x.ID).TotalCostAmount ?? 0 : 0;
                }
                var lstAllo = CPPeriod.CPAllocationGeneralExpenses.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 0).ToList();
                if (lstAllo.Count != 0)
                {
                    foreach (var item in lstAllo)
                    {
                        allo1 = allo1 + item.CPAllocationGeneralExpenseDetails.Where(a => a.ContractID == x.ID).Sum(t => t.AllocatedAmount);
                    }
                }
                var lstexp01 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 0 && d.ContractID == x.ID).ToList();
                if (lstexp01.Count != 0)
                {
                    exp01 = lstexp01.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 0).Sum(t => t.Amount);
                }
                var lstexp11 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 1 && d.ContractID == x.ID).ToList();
                if (lstexp11.Count != 0)
                {
                    exp11 = lstexp11.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 0).Sum(t => t.Amount);
                }
                md.IncurredMaterial = (exp01 - exp11 + allo1); //2.2

                var lstAllo2 = CPPeriod.CPAllocationGeneralExpenses.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 1).ToList();
                if (lstAllo2 != null)
                {
                    foreach (var item in lstAllo2)
                    {
                        allo2 = allo2 + item.CPAllocationGeneralExpenseDetails.Where(a => a.ContractID == x.ID).Sum(t => t.AllocatedAmount);
                    }
                }
                var lstexp02 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 0 && d.ContractID == x.ID).ToList();
                if (lstexp02.Count != 0)
                {
                    exp02 = lstexp02.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 1).Sum(t => t.Amount);
                }
                var lstexp12 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 1 && d.ContractID == x.ID).ToList();
                if (lstexp12.Count != 0)
                {
                    exp12 = lstexp12.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 1).Sum(t => t.Amount);
                }
                md.IncurredLabor = (exp02 - exp12 + allo2);// 3.2

                var lstAllo3 = CPPeriod.CPAllocationGeneralExpenses.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 2).ToList();
                if (lstAllo3 != null)
                {
                    foreach (var item in lstAllo3)
                    {
                        allo3 = allo3 + item.CPAllocationGeneralExpenseDetails.Where(a => a.ContractID == x.ID).Sum(t => t.AllocatedAmount);
                    }
                }
                var lstexp03 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 0 && d.ContractID == x.ID).ToList();
                if (lstexp03.Count != 0)
                {
                    exp03 = lstexp03.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 2).Sum(t => t.Amount);
                }
                var lstexp13 = CPPeriod.CPExpenseLists.ToList().Where(d => d.TypeVoucher == 1 && d.ContractID == x.ID).ToList();
                if (lstexp13.Count != 0)
                {
                    exp13 = lstexp13.Where(d => LstExpen.FirstOrDefault(a => a.ID == d.ExpenseItemID).ExpenseType == 2).Sum(t => t.Amount);
                }
                md.IncurredCost = (exp03 - exp13 + allo3); //4.2
                md.IncurredAmount = md.IncurredMaterial + md.IncurredLabor + md.IncurredCost; //1.2
                md.UncompleteLastMaterial = md.UncompleteFirstMaterial + md.IncurredMaterial; //2.3
                md.UncompleteLastLabor = md.UncompleteFirstLabor + md.IncurredLabor; //3.3
                md.UncompleteLastCost = md.UncompleteFirstCost + md.IncurredCost; //4.3
                md.UncompleteLastAmount = md.UncompleteFirstAmount + md.IncurredAmount;//1.3
                if (CPPeriod.CPAcceptances.Count > 0)
                {
                    foreach (var item in CPPeriod.CPAcceptances)
                    {
                        acp = acp + item.CPAcceptanceDetails.FirstOrDefault(d => d.ContractID == x.ID).TotalAcceptedAmount;
                    }
                }
                md.AcceptedAmount = acp;//1.4
                md.UnAcceptedAmount = CPPeriod.CPAcceptances.Count > 0 ? CPPeriod.CPAcceptances[0].CPAcceptanceDetails.FirstOrDefault(d => d.ContractID == x.ID).Amount - md.AcceptedAmount : 0;//1.5
                lstCost.Add(md);
            }
            return lstCost;
        }
    }

}