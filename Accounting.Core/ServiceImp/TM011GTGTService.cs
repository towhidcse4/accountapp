using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM011GTGTService: BaseService<TM011GTGT ,Guid>,ITM011GTGTService
    {
        public TM011GTGTService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}