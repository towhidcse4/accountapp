using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class EMContractDetailRevenueService: BaseService<EMContractDetailRevenue, Guid>, IEMContractDetailRevenueService
    {
        public EMContractDetailRevenueService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<EMContractDetailRevenue> GetByContractID(Guid contractID)
        {
            return Query.Where(x => x.ContractID == contractID).ToList();
        }
    }

}