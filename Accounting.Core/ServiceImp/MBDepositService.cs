using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBDepositService: BaseService<MBDeposit ,Guid>,IMBDepositService
    {
        public MBDepositService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public MBDeposit GetByNo(string no)
        {
            try
            {
                return Query.FirstOrDefault(m => m.No == no);
            }
            catch
            {
                return null;
            }
        }
    }

}