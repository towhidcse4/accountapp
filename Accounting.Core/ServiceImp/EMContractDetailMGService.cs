using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class EMContractDetailMGService: BaseService<EMContractDetailMG ,Guid>,IEMContractDetailMGService
    {
        public EMContractDetailMGService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<EMContractDetailMG> GetEMContractDetailMGByContractID(Guid contractID)
        {
            return Query.Where(p => p.ContractID == contractID).ToList();
        }

        public EMContractDetailMG GetEMContractDetailMGByContractIDandMID(Guid contractID, Guid materialID)
        {
            return Query.FirstOrDefault(p => p.ContractID == contractID && p.MaterialGoodsID == materialID);
        }

        public EMContractDetailMG GetByID(Guid ID)
        {
            return Query.Where(x => x.ID == ID).SingleOrDefault();
        }
    }

}