using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153LostInvoiceService : BaseService<TT153LostInvoice, Guid>, ITT153LostInvoiceService
    {
        public TT153LostInvoiceService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        ITT153ReportService _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
        ISAInvoiceService _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
        ISABillService _ISABillService = IoC.Resolve<ISABillService>();
        ISAReturnService _ISAReturnService = IoC.Resolve<ISAReturnService>();
        IPPDiscountReturnService _IPPDiscountReturnService = IoC.Resolve<IPPDiscountReturnService>();
        public List<int> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportID)
        {
            List<int> lrs = new List<int>();
            TT153Report tT153Report = _ITT153ReportService.Getbykey(TT153ReportID);
            List<TT153LostInvoice> lst = Query.Where(n => n.Date <= To && n.Date >= From).ToList();
            foreach (var itemL in lst)
            {
                foreach (TT153LostInvoiceDetail itemDetail in itemL.TT153LostInvoiceDetails.Where(n => n.TT153ReportID == TT153ReportID))
                {
                    foreach (var item in _ISAInvoiceService.Query.Where(n => n.InvoiceNo != null && n.InvoiceNo != "" && n.InvoiceDate >= From && n.InvoiceDate <= To && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceSeries == tT153Report.InvoiceSeries))
                    {
                        if (int.Parse(item.InvoiceNo) <= int.Parse(itemDetail.ToNo) && int.Parse(item.InvoiceNo) >= int.Parse(itemDetail.FromNo))
                        {
                            lrs.Add(int.Parse(item.InvoiceNo));
                        }
                    }
                }

            }
            foreach (var itemL in lst)
            {
                foreach (TT153LostInvoiceDetail itemDetail in itemL.TT153LostInvoiceDetails.Where(n => n.TT153ReportID == TT153ReportID))
                {
                    foreach (var item in _IPPDiscountReturnService.Query.Where(n => n.TypeID == 220 && n.InvoiceNo != null && n.InvoiceNo != "" && n.InvoiceDate >= From && n.InvoiceDate <= To && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceSeries == tT153Report.InvoiceSeries))
                    {
                        if (int.Parse(item.InvoiceNo) <= int.Parse(itemDetail.ToNo) && int.Parse(item.InvoiceNo) >= int.Parse(itemDetail.FromNo))
                        {
                            lrs.Add(int.Parse(item.InvoiceNo));
                        }
                    }
                }

            }
            foreach (var itemL in lst)
            {
                foreach (TT153LostInvoiceDetail itemDetail in itemL.TT153LostInvoiceDetails.Where(n => n.TT153ReportID == TT153ReportID))
                {
                    foreach (var item in _ISABillService.Query.Where(n => n.InvoiceNo != null && n.InvoiceNo != "" && n.InvoiceDate >= From && n.InvoiceDate <= To && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceSeries == tT153Report.InvoiceSeries))
                    {
                        if (int.Parse(item.InvoiceNo) <= int.Parse(itemDetail.ToNo) && int.Parse(item.InvoiceNo) >= int.Parse(itemDetail.FromNo))
                        {
                            lrs.Add(int.Parse(item.InvoiceNo));
                        }
                    }
                }

            }
            foreach (var itemL in lst)
            {
                foreach (TT153LostInvoiceDetail itemDetail in itemL.TT153LostInvoiceDetails.Where(n => n.TT153ReportID == TT153ReportID))
                {
                    foreach (var item in _ISAReturnService.Query.Where(n => n.TypeID == 340 && n.InvoiceNo != null && n.InvoiceNo != "" && n.InvoiceDate >= From && n.InvoiceDate <= To && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceSeries == tT153Report.InvoiceSeries))
                    {
                        if (int.Parse(item.InvoiceNo) <= int.Parse(itemDetail.ToNo) && int.Parse(item.InvoiceNo) >= int.Parse(itemDetail.FromNo))
                        {
                            lrs.Add(int.Parse(item.InvoiceNo));
                        }
                    }
                }

            }
            //List<int> lstDetail = new List<int>();
            //foreach (TT153LostInvoice item in lst)
            //{
            //    foreach (TT153LostInvoiceDetail itemDetail in item.TT153LostInvoiceDetails)
            //    {
            //        if (itemDetail.TT153ReportID == TT153ReportID)
            //        {
            //            lstDetail.Add(itemDetail);
            //            break;
            //        }
            //    }
            //}
            return lrs;
        }
    }

}