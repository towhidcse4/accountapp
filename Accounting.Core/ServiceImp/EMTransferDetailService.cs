﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMTransferDetailService : BaseService<EMTransferDetail, Guid>, IEMTransferDetailService
    {
        public EMTransferDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<EMTransferDetail> GetListEMTransferDetailEMTransferID(Guid? emTransferID)
        {
            List<EMTransferDetail> listPpd = Query.Where(a => a.EMTransferID == emTransferID).ToList();
            return listPpd;
        }

        public EMTransferDetail GetEMTransferDetailStockCategoryID(Guid? stockCategoryID)
        {
            EMTransferDetail select = Query.Where(a => a.StockCategoryID == stockCategoryID).Single();
            return select;
        }
    }
}
