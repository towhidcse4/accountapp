using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class SAReturnDetailService: BaseService<SAReturnDetail ,Guid>,ISAReturnDetailService
    {
        private static ISAReturnService ISAReturnService
        {
            get { return IoC.Resolve<ISAReturnService>(); }
        }
        public SAReturnDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
        public decimal GetSAReturnDetailQuantity(Guid materialGoodsID, Guid contractID)
        {
            List<SAReturnDetail> lstSADetail = Query.Where(X => X.MaterialGoodsID == materialGoodsID && X.ContractID == contractID && ISAReturnService.Query.Any(d => d.ID == X.SAReturnID && d.Recorded)).ToList();
            decimal quantity = lstSADetail.Count == 0 ? 0 : (lstSADetail.Sum(x => x.Quantity) ?? 0);
            return (decimal)quantity;
        }
    }

}