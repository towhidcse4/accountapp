using System;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCReceiptDetailTaxService : BaseService<MCReceiptDetailTax, Guid>, IMCReceiptDetailTaxService
    {
        public MCReceiptDetailTaxService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public System.Collections.Generic.IList<MCReceiptDetailTax> getMCReceiptDetailTaxbyID(Guid id)
        {
            return Query.Where(k => k.MCReceiptID == id).ToList();
        }
    }

}