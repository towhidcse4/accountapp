﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    class TILostDetailService : BaseService<TILostDetail, Guid>, ITILostDetailService
    {
       public TILostDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
       { }
    }
}
