using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class TIAllocationAllocatedService: BaseService<TIAllocationAllocated ,Guid>,ITIAllocationAllocatedService
    {
        public TIAllocationAllocatedService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}