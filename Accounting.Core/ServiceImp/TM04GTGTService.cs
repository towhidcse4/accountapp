using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class TM04GTGTService: BaseService<TM04GTGT ,Guid>,ITM04GTGTService
    {
        public TM04GTGTService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}