using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CPPeriodDetailService: BaseService<CPPeriodDetail ,Guid>,ICPPeriodDetailService
    {
        public CPPeriodDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        public List<CPPeriodDetail> GetList()
        {
            return Query.ToList();
        }
    }

}