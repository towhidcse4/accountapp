using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class PSTimeSheetService: BaseService<PSTimeSheet ,Guid>,IPSTimeSheetService
    {
        public PSTimeSheetService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}