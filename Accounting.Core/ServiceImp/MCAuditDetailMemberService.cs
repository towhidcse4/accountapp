﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCAuditDetailMemberService : BaseService<MCAuditDetailMember, Guid>, IMCAuditDetailMemberService
    {
        public MCAuditDetailMemberService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }
}