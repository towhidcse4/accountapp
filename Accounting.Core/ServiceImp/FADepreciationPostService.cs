using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class FADepreciationPostService: BaseService<FADepreciationPost ,Guid>,IFADepreciationPostService
    {
        public FADepreciationPostService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}