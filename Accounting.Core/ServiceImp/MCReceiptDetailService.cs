using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCReceiptDetailService : BaseService<MCReceiptDetail, Guid>, IMCReceiptDetailService
    {
        public MCReceiptDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<MCReceiptDetail> GetBymCReceiptId(Guid mCReceiptId)
        {
            return Query.Where(k => k.MCReceiptID == mCReceiptId).ToList();
        }

        public List<MCReceiptDetail> GetMCReceiptDetailByContract(Guid contractID)
        {
            return Query.Where(k => k.ContractID == contractID).ToList();
        }
    }

}