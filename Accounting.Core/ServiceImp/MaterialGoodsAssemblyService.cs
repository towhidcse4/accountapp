﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;
namespace Accounting.Core.ServiceImp
{
    class MaterialGoodsAssemblyService : BaseService<MaterialGoodsAssembly, Guid>, IMaterialGoodsAssemblyService
    {
        public MaterialGoodsAssemblyService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        /// <summary>
        /// Lấy danh sách MaterialGoodsAssembly theo MaterialGoodsID
        /// [DUYTN]
        /// </summary>
        /// <param name="MaterialGoodsID">là MaterialGoodsID cần truyền vào</param>
        /// <returns>Tập các MaterialGoodsAssembly, null nếu bị lỗi</returns>
        public List<MaterialGoodsAssembly> GetAll_ByMaterialGoodsID(Guid MaterialGoodsID)
        {
            return Query.Where(k => k.MaterialGoodsID == MaterialGoodsID).ToList();
        }
    }
}
