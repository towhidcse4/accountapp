using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCPaymentDetailSalaryService : BaseService<MCPaymentDetailSalary, Guid>, IMCPaymentDetailSalaryService
    {
        public MCPaymentDetailSalaryService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}