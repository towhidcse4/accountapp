﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
namespace Accounting.Core.ServiceImp
{
    public class EMShareHolderService : BaseService<EMShareHolder, Guid>, IEMShareHolderService
    {
        public EMShareHolderService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }
}
