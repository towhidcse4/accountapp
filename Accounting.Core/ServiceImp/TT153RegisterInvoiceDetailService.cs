using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153RegisterInvoiceDetailService: BaseService<TT153RegisterInvoiceDetail ,Guid>,ITT153RegisterInvoiceDetailService
    {
        public TT153RegisterInvoiceDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}