using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class RegistrationGroupService : BaseService<RegistrationGroup, Guid>, IRegistrationGroupService
    {
        public RegistrationGroupService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<RegistrationGroup> GetParentId(Guid? idParentGuid)
        {
            return Query.Where(p => p.ParentID == idParentGuid).ToList();
        }


        public int GetCountByParenId(Guid? countparentIDgGuid)
        {
            return Query.Count(p => p.ParentID == countparentIDgGuid);
        }



        public List<RegistrationGroup> GetNewParent(Guid newParentGuid)
        {
            return Query.Where(a => a.ParentID == newParentGuid).ToList();
        }


        public List<RegistrationGroup> GetGarde(int grade)
        {
            return Query.Where(a => a.Grade == grade).ToList();
        }

        public List<RegistrationGroup> GetOrderChild(string lstChild)
        {
            return Query.Where(p => p.OrderFixCode.StartsWith(lstChild)).ToList();
        }

        public List<RegistrationGroup> GetByRegistrationGroupsCode(string registrationGroupCode)
        {
            return Query.Where(c => c.RegistrationGroupCode.Equals(registrationGroupCode)).ToList();
        }


        public List<string> GetParenIdOrderFixbyParenID(Guid? parentId)
        {
            return Query.Where(a => a.ParentID == parentId).Select(a => a.OrderFixCode).ToList();
        }
        public List<RegistrationGroup> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.RegistrationGroupCode).ToList();
        }
    }

}