﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class EMPublishPeriodService : BaseService<EMPublishPeriod, Guid>, IEMPublishPeriodService
    {
        public EMPublishPeriodService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<string> GetListPublishPeriodCode()
        {
            List<string> hh = Query.Select(a => a.PeriodCode).ToList();
            return hh;
        }
    }

}


