﻿using System;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Core.ServiceImp
{
    public class RSTransferDetailService : BaseService<RSTransferDetail, Guid>, IRSTransferDetailService
    {
        public RSTransferDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        /// <summary>
        /// Lấy về danh sách RSTransferID
        /// </summary>
        /// <param name="tempId"></param>
        /// <returns>tập các ID</returns>
        public List<RSTransferDetail> GetByRSTransferID(Guid tempId)
        {
            return Query.Where(p => p.RSTransferID == tempId).ToList();
        }
    }

}