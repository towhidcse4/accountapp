using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class CashFlowStatementReportService : BaseService<CashFlowStatementReport, Guid>, ICashFlowStatementReportService
    {
        public CashFlowStatementReportService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
    }

}