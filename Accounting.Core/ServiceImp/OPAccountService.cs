﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.AllOPAccount;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using PerpetuumSoft.Framework.Expressions;

namespace Accounting.Core.ServiceImp
{
    public class OPAccountService : BaseService<OPAccount, Guid>, IOPAccountService
    {
        public OPAccountService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        #region Mở Services
        private IAccountService _IAccountService
        {
            get { return IoC.Resolve<IAccountService>(); }
        }
        private IGeneralLedgerService _IGeneralLedgerService
        {
            get { return IoC.Resolve<IGeneralLedgerService>(); }
        }
        private ICurrencyService _ICurrencyService
        {
            get { return IoC.Resolve<ICurrencyService>(); }
        }
        private IOPAccountService _IOPAccountService
        {
            get { return IoC.Resolve<IOPAccountService>(); }
        }
        private IAccountingObjectService _IAccountingObjectService
        {
            get { return IoC.Resolve<IAccountingObjectService>(); }
        }
        private IBankAccountDetailService _IBankAccountDetailService
        {
            get { return IoC.Resolve<IBankAccountDetailService>(); }
        }
        #endregion
        /// <summary>
        /// Lấy danh sách tài khoản có số dư đầu kỳ
        /// </summary>
        /// <param name="startDate">Ngày hạch toán</param>
        /// <returns></returns>
        public IEnumerable<AllOpAccount> GetAllOpAccounts(DateTime startDate)
        {
            //Danh sách các type id chính xác của opn
            List<int> dsTypeID = new List<int>() { 700, 701, 702 };
            List<AllOpAccount> listAllOpAccount = new List<AllOpAccount>();
            //Lấy ra danh sách tài khoản có số dư ban đầu
            var listAccount = _IAccountService.GetListActiveKindOrderBy(true).Where(c => c.DetailType.Split(';').All(d => d != "10"));
            //Lấy ra danh sách sổ cái
            var listGen = (from listGenFilter in _IGeneralLedgerService.GetAllGL()
                           where
                           //listGenFilter.PostedDate <= startDate.AddDays(-1) &&
                           listGenFilter.Account != ""
                                   && listGenFilter.TypeID.ToString().StartsWith("70")
                           group listGenFilter by listGenFilter.Account
                               into listGenFilterGroup
                           select new
                           {
                               AccountNumber = listGenFilterGroup.Key,
                               SumDebitAmount = listGenFilterGroup.Sum(d => d.DebitAmount),
                               SumCreditAmount = listGenFilterGroup.Sum(d => d.CreditAmount),
                               SumDebitAmountOriginal = listGenFilterGroup.Sum(d => d.DebitAmountOriginal),
                               SumCreditAmountOriginal = listGenFilterGroup.Sum(d => d.CreditAmountOriginal),
                           }).ToList();
            //Tổng hợp dữ liệu để lấy ra danh sách tài khoản tương ứng với số dư
            var listReturn = (from account in listAccount
                              join gen in listGen on account.AccountNumber equals gen.AccountNumber into accGroup
                              from item in accGroup.DefaultIfEmpty()
                              select new
                              {
                                  account,
                                  SumDebitAmount = accGroup.Sum(p => p.SumDebitAmount),
                                  SumCreditAmount = accGroup.Sum(p => p.SumCreditAmount),
                                  SumDebitAmountOriginal = accGroup.Sum(p => p.SumDebitAmountOriginal),
                                  SumCreditAmountOriginal = accGroup.Sum(d => d.SumCreditAmountOriginal)
                              });
            foreach (var itemParent in listReturn)
            {
                AllOpAccount opAccount = new AllOpAccount();
                PropertyInfo[] propertyInfoChild = opAccount.GetType().GetProperties();
                foreach (PropertyInfo infoChild in propertyInfoChild)
                {
                    PropertyInfo propItemChung = itemParent.account.GetType().GetProperty(infoChild.Name);
                    if (propItemChung != null && propItemChung.CanWrite)
                    {
                        infoChild.SetValue(opAccount, propItemChung.GetValue(itemParent.account, null), null);
                    }
                }
                opAccount.SumDebitAmount = itemParent.SumDebitAmount;
                opAccount.SumCreditAmount = itemParent.SumCreditAmount;
                listAllOpAccount.Add(opAccount);
            }
            return listAllOpAccount;
        }

        /// <summary>
        /// Danh sách opaccout theo typeid
        /// </summary>
        /// <param name="type">
        /// 701 theo đối tượng (khách hàng - nhà cung cấp - nhân viên)
        /// 700 mặc định
        /// </param>
        /// <returns></returns>
        public List<OPAccount> GetAll_ByTypeID(int type)
        {
            return Query.Where(c => c.TypeID == type).ToList();
        }

        /// <summary>
        /// Lấy danh sách số dư đầu kỳ tài khoản với các tài khoản khác tài khoản VTHH hoặc đối tượng
        /// </summary>
        /// <returns>null nếu bị lỗi</returns>
        public List<OpAccountDetailByOrther> GetListOpAccountDetailByOrthers()
        {
            List<string> dsDieuKien = new List<string>() { "-1", "4", "6", "7", "8", "9" };
            return (from acc in
                        _IAccountService.Query.Where(c => c.IsActive).ToList()/*GetAllChildrenAccount().Where(p =>*/ /*p.AccountGroupKind != 3 &&*//* (p.ParentID != null || p.IsParentNode == false))*///(p => dsDieuKien.Any(e => p.DetailType.Contains(e)) && 
                         .OrderBy(c => c.AccountNumber)
                         .ToList().Where(dc => dsDieuKien.Any(e => dc.DetailType.Contains(e)))
                    join opa in GetAll_ByTypeID(700) on acc.AccountNumber equals opa.AccountNumber into groupJoin
                    from item in groupJoin.DefaultIfEmpty()
                    orderby acc.AccountNumber
                    select new OpAccountDetailByOrther
                    {
                        CurrencyID = item != null ? item.CurrencyID : acc.DetailType.Contains("8") ? "" : "VND",
                        ExchangeRate = (decimal)((item != null && item.CurrencyID != null) ? /*_ICurrencyService.Query.Single(c => c.ID == item.CurrencyID).ExchangeRate*/item.ExchangeRate : 1),
                        AccountName = acc.AccountName,
                        ParentID = acc.ParentID,
                        DetailType = acc.DetailType,
                        AccountGroupKind = acc.AccountGroupKind,
                        ID = item != null ? item.ID : Guid.Empty,
                        TypeID = item != null ? item.TypeID : 700,
                        PostedDate = item != null ? item.PostedDate : DateTime.Now,
                        AccountNumber = acc.AccountNumber,
                        DebitAmount = item != null ? item.DebitAmount : 0,
                        DebitAmountOriginal = item != null ? item.DebitAmountOriginal : 0,
                        CreditAmount = item != null ? item.CreditAmount : 0,
                        CreditAmountOriginal = item != null ? item.CreditAmountOriginal : 0,
                        BankAccountDetailID = item != null ? item.BankAccountDetailID : null,
                        AccountingObjectID = item != null ? item.AccountingObjectID : null,
                        EmployeeID = item != null ? item.EmployeeID : null,
                        ContractID = item != null ? item.ContractID : null,
                        CostSetID = item != null ? item.CostSetID : null,
                        ExpenseItemID = item != null ? item.ExpenseItemID : null,
                        OrderPriority = item != null ? item.OrderPriority : 0
                    }).ToList();

        }
        public List<OpAccountDetailByOrther> GetListOpAccountDetailByOrthers_NotDefaultIfEmpty()
        {
            List<BankAccountDetail> lst = _IBankAccountDetailService.GetAll();
            List<OPAccount> lstOPAccounts = GetAll_ByTypeID(700);
            if (lstOPAccounts.Count > 0)
                return (from acc in
                            _IAccountService.Query.Where(p => p.AccountGroupKind != 3)
                             .OrderBy(c => c.AccountNumber).ToList()
                        join opa in lstOPAccounts on acc.AccountNumber equals opa.AccountNumber
                        orderby acc.AccountNumber
                        select new OpAccountDetailByOrther
                        {
                            CurrencyID = opa.CurrencyID,
                            ExchangeRate = opa.ExchangeRate,
                            AccountName = acc.AccountName,
                            ParentID = acc.ParentID,
                            DetailType = acc.DetailType,
                            AccountGroupKind = acc.AccountGroupKind,
                            ID = opa.ID,
                            TypeID = opa.TypeID,
                            PostedDate = opa.PostedDate,
                            AccountNumber = opa.AccountNumber,
                            DebitAmount = opa.DebitAmount,
                            DebitAmountOriginal = opa.DebitAmountOriginal,
                            CreditAmount = opa.CreditAmount,
                            CreditAmountOriginal = opa.CreditAmountOriginal,
                            BankAccountDetailID = opa.BankAccountDetailID,
                            AccountingObjectID = opa.AccountingObjectID,
                            EmployeeID = opa.EmployeeID,
                            ContractID = opa.ContractID,
                            CostSetID = opa.CostSetID,
                            ExpenseItemID = opa.ExpenseItemID,
                            OrderPriority = opa.OrderPriority,
                            BankAccount = lst.Count(c => c.ID == opa.BankAccountDetailID) == 1 ? lst.Single(c => c.ID == opa.BankAccountDetailID).BankAccount : "",
                            BankName = lst.Count(c => c.ID == opa.BankAccountDetailID) == 1 ? lst.Single(c => c.ID == opa.BankAccountDetailID).BankName : "",
                        }).ToList();
            else return new List<OpAccountDetailByOrther>();
        }
        /// <summary>
        /// Lấy danh sách số dư đầu kỳ tài khoản theo đối tượng
        /// </summary>
        /// <param name="account">tài khoản cần lấy</param>
        /// <returns>trả về null nếu lỗi</returns>
        public List<OpAccountObjectByOrther> GetListOpAccountObjectByOrthers(Account account)
        {
            List<string> ds = account.DetailType.Split(';').ToList();
            int i = -1;
            if (ds.Contains("0"))//khách hàng
            {
                i = 1;
            }
            else if (ds.Contains("1"))//nhà cung cấp
            {
                i = 0;
            }
            else if (ds.Contains("2"))//nhân viên
            {
                i = 2;
            }
            else if (ds.Contains("11"))//nhân viên
            {
                i = 5;//đối tượng khác trungnq sửa bug 6190
            }
            return (from acc in _IAccountingObjectService.GetAccountingObjects(i, true)
                    join opa in GetAll_ByTypeID(701) on acc.ID equals opa.AccountingObjectID into groupJoin
                    from item in groupJoin.DefaultIfEmpty()
                    orderby acc.AccountingObjectCode
                    select new OpAccountObjectByOrther()
                    {
                        CurrencyID = item != null ? item.CurrencyID : "",
                        ExchangeRate = (decimal)((item != null && item.CurrencyID != null) ? /*_ICurrencyService.Query.Single(c => c.ID == item.CurrencyID)*/item.ExchangeRate : 1),
                        ID = item != null ? item.ID : Guid.Empty,
                        TypeID = item != null ? item.TypeID : 701,
                        PostedDate = item != null ? item.PostedDate : DateTime.Now,
                        AccountNumber = item != null ? item.AccountNumber : account.AccountNumber,
                        DebitAmount = item != null ? item.DebitAmount : 0,
                        DebitAmountOriginal = item != null ? item.DebitAmountOriginal : 0,
                        CreditAmount = item != null ? item.CreditAmount : 0,
                        CreditAmountOriginal = item != null ? item.CreditAmountOriginal : 0,
                        BankAccountDetailID = item != null ? item.BankAccountDetailID : null,
                        AccountingObjectID = item != null ? item.AccountingObjectID : acc.ID,
                        EmployeeID = item != null ? item.EmployeeID : null,
                        ContractID = item != null ? item.ContractID : null,
                        CostSetID = item != null ? item.CostSetID : null,
                        ExpenseItemID = item != null ? item.ExpenseItemID : null,
                        OrderPriority = item != null ? item.OrderPriority : 0,
                        AccountingObjectCode = acc.AccountingObjectCode,
                        AccountingObjectName = acc.AccountingObjectName
                    }).ToList();
        }
        public List<OpAccountObjectByOrther> GetListOpAccountObjectByOrthers_NotDefaultIfEmpty()
        {
            List<OPAccount> lstOPAccounts = GetAll_ByTypeID(701);
            if (lstOPAccounts.Count > 0)
                return (from acc in _IAccountingObjectService.GetAll()
                        join opa in lstOPAccounts on acc.ID equals opa.AccountingObjectID
                        orderby acc.AccountingObjectCode
                        select new OpAccountObjectByOrther()
                        {
                            CurrencyID = opa.CurrencyID,
                            ExchangeRate = opa.ExchangeRate,
                            ID = opa.ID,
                            TypeID = opa.TypeID,
                            PostedDate = opa.PostedDate,
                            AccountNumber = opa.AccountNumber,
                            DebitAmount = opa.DebitAmount,
                            DebitAmountOriginal = opa.DebitAmountOriginal,
                            CreditAmount = opa.CreditAmount,
                            CreditAmountOriginal = opa.CreditAmountOriginal,
                            BankAccountDetailID = opa.BankAccountDetailID,
                            AccountingObjectID = opa.AccountingObjectID,
                            EmployeeID = opa.EmployeeID,
                            ContractID = opa.ContractID,
                            CostSetID = opa.CostSetID,
                            ExpenseItemID = opa.ExpenseItemID,
                            OrderPriority = opa.OrderPriority,
                            AccountingObjectCode = acc.AccountingObjectCode,
                            AccountingObjectName = acc.AccountingObjectName
                        }).ToList();
            return new List<OpAccountObjectByOrther>();
        }

        /// <summary>
        /// Chuyển danh sách số dư đầu kỳ tài khoản và lưu vào CSDL
        /// </summary>
        /// <param name="lstOpAccount">danh sách số dư đầu kỳ theo tài khoản</param>
        public void SaveOpAccountAndGeneralLedger(IEnumerable<OpAccountDetailByOrther> lstOpAccount)
        {
            List<OPAccount> lst = new List<OPAccount>();
            Utils.Convert_FromParent_ToChild(lstOpAccount, lst);
            const int typeID = 700;
            SaveLedgerOpAccount(lst, typeID);
        }
        public void SaveOpAccountAndGeneralLedger(IEnumerable<OpAccountObjectByOrther> lstOpAccount)
        {
            List<OPAccount> lst = new List<OPAccount>();
            Utils.Convert_FromParent_ToChild(lstOpAccount, lst);
            const int typeID = 701;
            SaveLedgerOpAccount(lst, typeID);
        }
        public void SaveLedgerOpAccount(List<OPAccount> listOpAccountUpdate, int typeID)
        {
            //Lấy ra danh sách OPAccount theo đúng type id
            List<OPAccount> listOpAccountDb = Query.Where(c => c.TypeID == typeID).ToList();
            try
            {
                BeginTran();
                foreach (var opAccountUpdate in listOpAccountUpdate)
                {
                    if (listOpAccountDb.Any(p => p.ID == opAccountUpdate.ID))
                    {
                        //Kiểm tra xem TK có thuộc danh sách hay không
                        foreach (var accountDb in listOpAccountDb)
                        {
                            //Có trong danh sách(Sửa hoặc Xóa)
                            if ((accountDb.ID == opAccountUpdate.ID) && ((accountDb.DebitAmount != opAccountUpdate.DebitAmount) || (accountDb.CreditAmount != opAccountUpdate.CreditAmount)
                                    || (accountDb.CostSetID != opAccountUpdate.CostSetID) || (accountDb.ContractID != opAccountUpdate.ContractID)
                                    || (accountDb.BankAccountDetailID != opAccountUpdate.BankAccountDetailID) || (accountDb.ExpenseItemID != opAccountUpdate.ExpenseItemID)))
                            {
                                //Trường hợp sửa
                                if ((opAccountUpdate.DebitAmount != 0) || (opAccountUpdate.DebitAmountOriginal != 0)
                                    || (opAccountUpdate.CreditAmount != 0) || (opAccountUpdate.CreditAmountOriginal != 0))
                                {
                                    //Xóa chứng từ trong sổ cái
                                    var saved = _IGeneralLedgerService.Getbykey(opAccountUpdate.ID);
                                    if (saved != null && saved.ID != Guid.Empty)
                                    {
                                        _IGeneralLedgerService.Delete(_IGeneralLedgerService.Getbykey(opAccountUpdate.ID));
                                    }
                                    //Update chứng từ trong bảng OPAccount
                                    _IOPAccountService.Update(Utils.GetAgainObject(opAccountUpdate));
                                    //Insert chứng từ vào sổ cái
                                    GeneralLedger generalLedger = GenGeneralLedgers(opAccountUpdate, opAccountUpdate.PostedDate, typeID);
                                    _IGeneralLedgerService.CreateNew(generalLedger);
                                }
                                else
                                {
                                    //Xóa chứng từ trong sổ cái
                                    var saved = _IGeneralLedgerService.Getbykey(opAccountUpdate.ID);
                                    if (saved != null && saved.ID != Guid.Empty)
                                    {
                                        _IGeneralLedgerService.Delete(_IGeneralLedgerService.Getbykey(opAccountUpdate.ID));
                                    }
                                    //Update chứng từ trong bảng OPAccount
                                    _IOPAccountService.Delete(Utils.GetAgainObject(opAccountUpdate));
                                }
                            }
                        }
                    }
                    else
                    {
                        if(opAccountUpdate.CreditAmount != 0 || opAccountUpdate.DebitAmount != 0)
                        {
                            //Thêm mới vào bảng OPAccount
                            _IOPAccountService.CreateNew(opAccountUpdate);
                            //Insert chứng từ vào sổ cái
                            GeneralLedger generalLedger = GenGeneralLedgers(opAccountUpdate, opAccountUpdate.PostedDate, typeID);
                            _IGeneralLedgerService.CreateNew(generalLedger);
                        }
                    }

                }
                foreach (OPAccount opAccount in listOpAccountDb)
                {
                    if (listOpAccountUpdate.All(c => opAccount.ID != c.ID && c.AccountNumber == opAccount.AccountNumber))
                    {
                        //Trường hợp xóa

                        //Xóa chứng từ trong sổ cái
                        var data = _IGeneralLedgerService.Getbykey(opAccount.ID);
                        if (data != null)
                            _IGeneralLedgerService.Delete(data);
                        //Xóa chứng từ trong bảng OPAccount
                        var data2 = _IOPAccountService.Getbykey(opAccount.ID);
                        if (data2 != null)
                            _IOPAccountService.Delete(data2);
                    }
                }


                CommitTran();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                RolbackTran();
            }
        }

        /// <summary>
        /// Tạo chứng từ sổ cái cho OpAccount
        /// </summary>
        /// <param name="opAccount">đối tượng số dư đầu kỳ tài khoản</param>
        /// <param name="startDate"></param>
        /// <param name="typeID">typeid tuong ung</param>
        /// <returns></returns>
        protected GeneralLedger GenGeneralLedgers(OPAccount opAccount, DateTime startDate, int typeID)
        {
            GeneralLedger genTemp = new GeneralLedger
            {
                ID = opAccount.ID,
                BranchID = null,
                ReferenceID = opAccount.ID,
                TypeID = typeID,
                Date = startDate,
                PostedDate = opAccount.PostedDate,
                No = "OPN",
                InvoiceDate = null,
                InvoiceNo = null,
                Account = opAccount.AccountNumber,
                AccountCorresponding = "",
                BankAccountDetailID = opAccount.BankAccountDetailID,
                CurrencyID = opAccount.CurrencyID,
                ExchangeRate = opAccount.ExchangeRate,
                DebitAmount = opAccount.DebitAmount,
                DebitAmountOriginal = opAccount.DebitAmountOriginal,
                CreditAmount = opAccount.CreditAmount,
                CreditAmountOriginal = opAccount.CreditAmountOriginal,
                Reason = "",
                Description = "",
                AccountingObjectID = opAccount.AccountingObjectID,
                EmployeeID = opAccount.EmployeeID,
                BudgetItemID = null,
                CostSetID = opAccount.CostSetID,
                ContractID = opAccount.ContractID,
                StatisticsCodeID = null,
                InvoiceSeries = null,
                ContactName = null,
                DetailID = opAccount.ID,
                RefNo = "OPN",
                RefDate = startDate,
                DepartmentID = null,
                ExpenseItemID = opAccount.ExpenseItemID,
                IsIrrationalCost = false,
            };

            return genTemp;
        }

        public OPAccount FindByAccountNumber(string originalPriceAccount)
        {
            var lst = Query.Where(opa => opa.AccountNumber == originalPriceAccount && opa.TypeID == 701).ToList();
            if (lst.Count > 0)
            {
                return lst[0];
            }
            return null;
        }

        public int FindLastPriority()
        {
            List<OPAccount> lst = Query.ToList();
            return lst.Count > 0 ? lst.Max(opfa => opfa.OrderPriority) : 0;
        }
    }
}

