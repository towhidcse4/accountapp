using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class InvestorService : BaseService<Investor, Guid>, IInvestorService
    {
        public InvestorService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public List<string> GetCode()
        {
            return Query.Select(a => a.InvestorCode).ToList();
        }
        public List<Investor> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.InvestorCode).ToList();
        }
    }
}