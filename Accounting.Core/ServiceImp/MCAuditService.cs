﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class MCAuditService : BaseService<MCAudit, Guid>, IMCAuditService
    {
        public MCAuditService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        private IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public decimal SoDuSoQuy(DateTime date, string Currency)
        {
            List<GeneralLedger> lstgl =  _IGeneralLedgerService.Query.Where(x => x.PostedDate <= date && x.CurrencyID == Currency).ToList().Where(x=> Currency == "VND" ? x.Account == "1111" : x.Account == "1112").ToList();
            return Currency == "VND" ? lstgl.Sum(x=>x.DebitAmount - x.CreditAmount): lstgl.Sum(x => x.DebitAmountOriginal - x.CreditAmountOriginal);
        }
    }
}