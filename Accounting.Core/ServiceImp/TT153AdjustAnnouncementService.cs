using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using System.Linq;
using FX.Core;

namespace Accounting.Core.ServiceImp
{
    public class TT153AdjustAnnouncementService: BaseService<TT153AdjustAnnouncement ,Guid>,ITT153AdjustAnnouncementService
    {
        public TT153AdjustAnnouncementService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}