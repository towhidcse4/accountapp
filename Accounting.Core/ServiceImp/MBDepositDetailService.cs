using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MBDepositDetailService: BaseService<MBDepositDetail ,Guid>,IMBDepositDetailService
    {
        public MBDepositDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}