﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class MaterialQuantumDetailService: BaseService<MaterialQuantumDetail ,Guid>,IMaterialQuantumDetailService
    {
        public MaterialQuantumDetailService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}

        /// <summary>
        /// Lấy ra danh sách MaterialQuantumDetail theo MaterialQuantumID
        /// [DUYTN]
        /// </summary>
        /// <param name="MaterialQuantumID">là MaterialQuantumID truyền vào</param>
        /// <returns>tập các MaterialQuantumDetail, null nếu bị lỗi</returns>
        public List<MaterialQuantumDetail> GetAll_ByMaterialQuantumID(Guid MaterialQuantumID)
        {
            return Query.Where(p => p.MaterialQuantumID == MaterialQuantumID).ToList();
        }
    }

}