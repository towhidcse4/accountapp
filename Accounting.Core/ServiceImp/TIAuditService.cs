using System;
using FX.Data;
using Accounting.Core.Domain;
using Accounting.Core.IService;

namespace Accounting.Core.ServiceImp
{
    public class TIAuditService: BaseService<TIAudit ,Guid>,ITIAuditService
    {
        public TIAuditService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {}
    }

}