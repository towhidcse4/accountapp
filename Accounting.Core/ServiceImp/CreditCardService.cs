﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;

namespace Accounting.Core.ServiceImp
{
    public class CreditCardService : BaseService<CreditCard, Guid>, ICreditCardService
    {
        public CreditCardService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }

        public List<CreditCard> GetListCreditCardCreditCardNumber(string creditCardNumber)
        {
            List<CreditCard> lstCreditCard = Query.Where(c => c.CreditCardNumber.Equals(creditCardNumber)).ToList();
            return lstCreditCard;
        }
        public CreditCard GetByNumber(string creditCardNumber)
        {
            return Query.Single(c => c.CreditCardNumber == creditCardNumber);
        }

        public int CountListCreditCardCreditCardNumber(string creditCardNumber)
        {
            return GetListCreditCardCreditCardNumber(creditCardNumber).Count;
        }

        /// <summary>
        /// Hàm lấy ra list creditcard đang hoạt động
        /// </summary>
        /// <param name="isActive">creditcard còn hoạt động = true, không còn = false</param>
        /// <returns></returns>
        public List<CreditCard> GetAll_IsActive(bool isActive)
        {
            return Query.Where(c => c.IsActive == isActive).OrderBy(c => c.CreditCardNumber).ToList();
        }

        public List<CreditCard> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.CreditCardNumber).ToList();
        }
    }

}