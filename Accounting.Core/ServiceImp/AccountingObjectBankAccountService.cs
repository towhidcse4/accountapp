﻿using System;
using System.Data.SqlClient;
using System.Linq.Dynamic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Data;
using FX.Core;
using System.Linq;
using System.Collections.Generic;

namespace Accounting.Core.ServiceImp
{
    public class AccountingObjectBankAccountService : BaseService<AccountingObjectBankAccount, Guid>, IAccountingObjectBankAccountService
    {
        public AccountingObjectBankAccountService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        public IAccountService _IAccountService { get { return IoC.Resolve<IAccountService>(); } }
        public IMBTellerPaperService _IMBTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        public IMBTellerPaperDetailService _IMBTellerPaperDetailService { get { return IoC.Resolve<IMBTellerPaperDetailService>(); } }
        public IMBInternalTransferService _IMBInternalTransferService { get { return IoC.Resolve<IMBInternalTransferService>(); } }
        public IMBInternalTransferDetailService _IMBInternalTransferDetailService { get { return IoC.Resolve<IMBInternalTransferDetailService>(); } }
        public IGOtherVoucherService _IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        public IGOtherVoucherDetailService _IGOtherVoucherDetailService { get { return IoC.Resolve<IGOtherVoucherDetailService>(); } }
        public IMBCreditCardService _IMBCreditCardService { get { return IoC.Resolve<IMBCreditCardService>(); } }
        public IMBCreditCardDetailService _IMBCreditCardDetailService { get { return IoC.Resolve<IMBCreditCardDetailService>(); } }
        public IMBDepositService _IMBDepositService { get { return IoC.Resolve<IMBDepositService>(); } }
        public IMBDepositDetailService _IMBDepositDetailService { get { return IoC.Resolve<IMBDepositDetailService>(); } }
        public IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        public IMCPaymentDetailService _IMCPaymentDetailService { get { return IoC.Resolve<IMCPaymentDetailService>(); } }
        public IMCReceiptService _IMCReceiptService { get { return IoC.Resolve<IMCReceiptService>(); } }
        public IMCReceiptDetailService _IMCReceiptDetailService { get { return IoC.Resolve<IMCReceiptDetailService>(); } }
        public IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        public IAccountingObjectBankAccountService _IAccountingObjectBankAccountService { get { return IoC.Resolve<IAccountingObjectBankAccountService>(); } }
        public IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public ITypeService _ITypeService { get { return IoC.Resolve<ITypeService>(); } }
        public IBankAccountDetailService _IBankAccountDetailService { get { return IoC.Resolve<IBankAccountDetailService>(); } }
        List<GeneralLedger> lstSoCai = new List<GeneralLedger>();
        public List<AccountingObjectBankAccount> GetByAccountingObject(AccountingObject ObjAccounting)
        {
            try
            {
                return Query.Where(a => a.AccountingObjectID == ObjAccounting.ID).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<AccountingObjectBankAccount> GetAll_OrderBy()
        {
            return Query.OrderBy(p => p.BankAccount).ToList();
        }

        /// <summary>
        /// Lấy danh sách Accounting ObjectBankAccount By AccountingObjectID 
        /// </summary>
        /// <param name="accountingObjectID">AccountObject ID</param>
        /// <returns>Danh sách AccountingObjectBankAccount </returns>
        public List<AccountingObjectBankAccount> GetByAccountingObjectID(Guid accountingObjectID)
        {
            try
            {
                return Query.Where(a => a.AccountingObjectID == accountingObjectID /*&& a.IsSelect == true*/).ToList(); // Hautv edit
            }
            catch
            {
                return null;
            }
        }

        public List<AccountingObjectBankAccount> GetByAccountingObjectType(Guid accoutingObjectID, List<int> objectType)
        {
            try
            {
                List<AccountingObjectBankAccount> lstAccountingObjectBankAccount = _IAccountingObjectBankAccountService.GetAll();
                List<AccountingObject> lstAccountingObject = _IAccountingObjectService.GetAll_ByListObjectType(objectType);
                var query = (from a in lstAccountingObjectBankAccount
                             join b in lstAccountingObject on a.AccountingObjectID equals b.ID
                             where b.ID == accoutingObjectID
                             select a).ToList();
                return query;
            }
            catch
            {
                return null;
            }
        }

        #region From FMcompare
        #region Dũng Lấy danh sách các chứng từ thu tiền để đối chiếu giao dịch với ngân hàng
        /// <summary>
        /// Lấy danh sách các chứng từ thu tiền để đối chiếu giao dịch với ngân hàng
        /// [DungNA]
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="CurrencyID">Mã tiền tệ</param>
        /// <param name="MatchDate">Ngày đối chiếu</param>
        /// <param name="isMatched">true/false: đã khớp/chưa khớp</param>
        /// <returns>Danh sách CollectionVoucher </returns>
        public List<CollectionVoucher> GetCollectionVoucher(Guid bankAccountDetailId, string currencyId, DateTime matchDate, bool isMatched)
        {
            matchDate = new DateTime(matchDate.Year, matchDate.Month, matchDate.Day, 23, 59, 59);
            List<MBTellerPaper> lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.Recorded == true).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.ToList();
            List<MBDeposit> lstMBDeposit = _IMBDepositService.Query.Where(c => c.Recorded == true).ToList();
            List<MBDepositDetail> lstMbDepositDetails = _IMBDepositDetailService.Query.ToList();
            List<MCPaymentDetail> lstMCPaymentDetail = _IMCPaymentDetailService.Query.ToList();
            List<MCReceiptDetail> lsMcReceiptDetails = _IMCReceiptDetailService.Query.ToList();
            List<Account> lstAccounts = _IAccountService.GetByDetailType(6);
            List<AccountingObject> lstAccountingObject = _IAccountingObjectService.Query.Where(a => a.IsActive == true).ToList();//Đối tượng nộp
            List<CollectionVoucher> lstCollectionVoucher = new List<CollectionVoucher>();//List đối tượng chứng từ giao dịch
            List<GeneralLedger> lstSoCai = new List<GeneralLedger>();
            List<string> listAccountName = lstAccounts.Select(c => c.AccountNumber).ToList();
            if (bankAccountDetailId != Guid.Empty && currencyId != string.Empty && matchDate > DateTime.MinValue)
            {
                var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.CurrencyID != null
                                 && a.Account != null
                                 && a.PostedDate != null
                                 && a.BankAccountDetailID == bankAccountDetailId
                                 && a.PostedDate <= matchDate
                                 && a.CurrencyID == currencyId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 //&& a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.DebitAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                lstSoCai = query.ToList();
            }
            else if (matchDate > DateTime.MinValue && currencyId == string.Empty)
            {
                var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == bankAccountDetailId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 //&& a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.DebitAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                lstSoCai = query.ToList();
            }
            else
            {
                var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == bankAccountDetailId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 //&& a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.DebitAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                lstSoCai = query.ToList();
            }

            var lstMbInternalTransferDetailsLG = (from a in lstSoCai
                                                  join b in lstMbInternalTransferDetails on a.DetailID equals b.ID
                                                  //join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                                  where b.IsMatchTo != null && b.IsMatchTo == isMatched
                                                  select new
                                                  {
                                                      a.DetailID,
                                                      a.TypeID,
                                                      a.Date,
                                                      Nos = (a.No != null ? a.No : ""),
                                                      //AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                      AccountingObjectNames = "",
                                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                      Amountt = a.DebitAmount,
                                                      AmountOriginalt = a.DebitAmountOriginal,
                                                      Description = a.Reason,
                                                      BankAccountDetailId = a.BankAccountDetailID,
                                                      b.MatchDateTo,
                                                  }).ToList();

            var lstMBDepositLG = (from a in lstSoCai
                                  join b in lstMBDeposit on a.ReferenceID equals b.ID
                                  join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                  where b.IsMatch != null && b.IsMatch == isMatched
                                  select new
                                  {
                                      a.ReferenceID,
                                      a.TypeID,
                                      a.Date,
                                      Nos = (a.No != null ? a.No : ""),
                                      AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                      Amountt = a.DebitAmount,
                                      AmountOriginalt = a.DebitAmountOriginal,
                                      Description = a.Reason,
                                      BankAccountDetailId = a.BankAccountDetailID,
                                      b.MatchDate,
                                  }).ToList();

            var lstMCPaymentDetailLG = (from a in lstSoCai
                                        join b in lstMCPaymentDetail on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.DebitAmount,
                                            AmountOriginalt = a.DebitAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lsMcReceiptDetailsLG = (from a in lstSoCai
                                        join b in lsMcReceiptDetails on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.DebitAmount,
                                            AmountOriginalt = a.DebitAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lstMbTellerPapersLG = (from a in lstSoCai
                                       join b in lstMbTellerPapers on a.ReferenceID equals b.ID
                                       join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                       where b.IsMatch != null && b.IsMatch == isMatched
                                       select new
                                       {
                                           a.ReferenceID,
                                           a.TypeID,
                                           a.Date,
                                           Nos = (a.No != null ? a.No : ""),
                                           AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                           Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                           Amountt = a.DebitAmount,
                                           AmountOriginalt = a.DebitAmountOriginal,
                                           Description = a.Reason,
                                           BankAccountDetailId = a.BankAccountDetailID,
                                           b.MatchDate,
                                       }).ToList();

            var lstGOtherVoucherDetailsLG = (from a in lstSoCai
                                             join b in lstGOtherVoucherDetails on a.DetailID equals b.ID
                                             join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                             where b.IsMatch != null && b.IsMatch == isMatched
                                             select new
                                             {
                                                 a.DetailID,
                                                 a.TypeID,
                                                 a.Date,
                                                 Nos = (a.No != null ? a.No : ""),
                                                 AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                 Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                 Amountt = a.DebitAmount,
                                                 AmountOriginalt = a.DebitAmountOriginal,
                                                 Description = a.Reason,
                                                 BankAccountDetailId = a.BankAccountDetailID,
                                                 b.MatchDate,
                                             }).ToList();
            #region Add List


            foreach (var d in lstGOtherVoucherDetailsLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbInternalTransferDetailsLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDateTo??DateTime.MinValue);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMBDepositLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMCPaymentDetailLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lsMcReceiptDetailsLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbTellerPapersLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            #endregion
            if(isMatched) return lstCollectionVoucher.OrderBy(z => z.Date).ThenBy(x => x.No).ToList();
            else
            {
                return lstCollectionVoucher.GroupBy(x => new { x.No, x.TypeId, x.AccountingObjectName, x.BankAccountDetailId, x.Description, x.Date, x.Rate }, (key, group) => new CollectionVoucher()
                {
                    No = key.No,
                    TypeId = key.TypeId,
                    AccountingObjectName = key.AccountingObjectName,
                    Description = key.Description,
                    Date = key.Date,
                    Rate = key.Rate,
                    BankAccountDetailId = key.BankAccountDetailId,
                    Amount = group.Sum(c => c.Amount),
                    AmountOriginal = group.Sum(c => c.AmountOriginal),
                }).ToList().OrderBy(z => z.Date).ThenBy(x => x.No).ToList();//Danh sách đã được group
            }
            //return lstCollectionVoucher.OrderBy(z=>z.Date).ThenBy(x=>x.No).ToList();
        }
        public List<CollectionVoucher> GetCollectionVoucherList(List<Guid> ListBankAccountDetailID, string currencyId, DateTime matchDate, bool isMatched)
        {
            List<MBTellerPaper> lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.Recorded == true).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.ToList();
            List<MBDeposit> lstMBDeposit = _IMBDepositService.Query.Where(c => c.Recorded == true).ToList();
            List<MCPaymentDetail> lstMCPaymentDetail = _IMCPaymentDetailService.Query.ToList();
            List<MCReceiptDetail> lsMcReceiptDetails = _IMCReceiptDetailService.Query.ToList();
            List<Account> lstAccounts = _IAccountService.GetByDetailType(6);
            List<AccountingObject> lstAccountingObject = _IAccountingObjectService.Query.Where(a => a.IsActive == true).ToList();//Đối tượng nộp
            List<CollectionVoucher> lstCollectionVoucher = new List<CollectionVoucher>();//List đối tượng chứng từ giao dịch
            List<GeneralLedger> lstSoCai = new List<GeneralLedger>();
            List<string> listAccountName = lstAccounts.Select(c => c.AccountNumber).ToList();
            foreach (var x in ListBankAccountDetailID)
            {
                if (currencyId != string.Empty && matchDate >= DateTime.MinValue)
                {
                    var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.CurrencyID != null
                                 && a.Account != null
                                 && a.PostedDate != null
                                 && a.BankAccountDetailID == x
                                 && a.PostedDate <= matchDate
                                 && a.CurrencyID == currencyId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 && a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.DebitAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                    lstSoCai.AddRange(query.ToList());
                }
                else if (currencyId == string.Empty && matchDate >= DateTime.MinValue)
                {
                    var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                && a.BankAccountDetailID != null
                                && a.Account != null
                                && a.BankAccountDetailID == x
                                && a.Account.StartsWith("112")
                                && a.DebitAmount > 0
                                && a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                && listAccountName.Contains(a.Account)
                                && !(a.TypeID >= 700 && a.TypeID <= 709));
                    lstSoCai.AddRange(query.ToList());
                }
                else
                {
                    var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == x
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 && a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.DebitAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                    lstSoCai.AddRange(query.ToList());
                }
            }


            var lstMbInternalTransferDetailsLG = (from a in lstSoCai
                                                  join b in lstMbInternalTransferDetails on a.DetailID equals b.ID
                                                  where b.IsMatchTo != null && b.IsMatchTo == isMatched
                                                  select new
                                                  {
                                                      a.DetailID,
                                                      a.TypeID,
                                                      a.Date,
                                                      Nos = (a.No != null ? a.No : ""),
                                                      AccountingObjectNames = "",
                                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                      Amountt = a.DebitAmount,
                                                      AmountOriginalt = a.DebitAmountOriginal,
                                                      Description = a.Reason,
                                                      BankAccountDetailId = a.BankAccountDetailID,
                                                      b.MatchDateTo,
                                                  }).ToList();

            var lstMBDepositLG = (from a in lstSoCai
                                  join b in lstMBDeposit on a.ReferenceID equals b.ID
                                  join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                  where b.IsMatch != null && b.IsMatch == isMatched
                                  select new
                                  {
                                      a.ReferenceID,
                                      a.TypeID,
                                      a.Date,
                                      Nos = (a.No != null ? a.No : ""),
                                      AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                      Amountt = a.DebitAmount,
                                      AmountOriginalt = a.DebitAmountOriginal,
                                      Description = a.Reason,
                                      BankAccountDetailId = a.BankAccountDetailID,
                                      b.MatchDate,
                                  }).ToList();

            var lstMCPaymentDetailLG = (from a in lstSoCai
                                        join b in lstMCPaymentDetail on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.DebitAmount,
                                            AmountOriginalt = a.DebitAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lsMcReceiptDetailsLG = (from a in lstSoCai
                                        join b in lsMcReceiptDetails on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.DebitAmount,
                                            AmountOriginalt = a.DebitAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lstMbTellerPapersLG = (from a in lstSoCai
                                       join b in lstMbTellerPapers on a.ReferenceID equals b.ID
                                       join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                       where b.IsMatch != null && b.IsMatch == isMatched
                                       select new
                                       {
                                           a.ReferenceID,
                                           a.TypeID,
                                           a.Date,
                                           Nos = (a.No != null ? a.No : ""),
                                           AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                           Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                           Amountt = a.DebitAmount,
                                           AmountOriginalt = a.DebitAmountOriginal,
                                           Description = a.Reason,
                                           BankAccountDetailId = a.BankAccountDetailID,
                                           b.MatchDate,
                                       }).ToList();

            var lstGOtherVoucherDetailsLG = (from a in lstSoCai
                                             join b in lstGOtherVoucherDetails on a.DetailID equals b.ID
                                             join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                             where b.IsMatch != null && b.IsMatch == isMatched
                                             select new
                                             {
                                                 a.DetailID,
                                                 a.TypeID,
                                                 a.Date,
                                                 Nos = (a.No != null ? a.No : ""),
                                                 AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                 Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                 Amountt = a.DebitAmount,
                                                 AmountOriginalt = a.DebitAmountOriginal,
                                                 Description = a.Reason,
                                                 BankAccountDetailId = a.BankAccountDetailID,
                                                 b.MatchDate,
                                             }).ToList();
            #region Add List


            foreach (var d in lstGOtherVoucherDetailsLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbInternalTransferDetailsLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = d.MatchDateTo??DateTime.MinValue;
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMBDepositLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMCPaymentDetailLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lsMcReceiptDetailsLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbTellerPapersLG)
            {
                CollectionVoucher addlist = new CollectionVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            #endregion
            //return lstCollectionVoucher.GroupBy(x => new { x.No, x.TypeId }, (key, group) => new CollectionVoucher()
            //{
            //    No = key.No,
            //    TypeId = key.TypeId,
            //    Amount = group.Sum(c => c.Amount),
            //    AmountOriginal = group.Sum(c => c.AmountOriginal),
            //}).ToList();//Danh sách đã được group
            return lstCollectionVoucher.ToList();
        }
        #endregion
        #region Dũng Lấy danh sách các chứng từ chi tiền để đối chiếu giao dịch với ngân hàng
        /// <summary>
        /// Lấy danh sách các chứng từ chi tiền để đối chiếu giao dịch với ngân hàng
        /// [DungNA]
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="CurrencyID">Mã tiền tệ</param>
        /// <param name="MatchDate">Ngày đối chiếu</param>
        /// <param name="isMatched">true/false: đã khớp/chưa khớp</param>
        /// <returns>Danh sách SpendingVoucher </returns>
        public List<SpendingVoucher> GetSpendingVoucher(Guid bankAccountDetailId, string currencyId, DateTime matchDate, bool isMatched)
        {
            List<MBTellerPaper> lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.Recorded == true).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.ToList();
            List<MBDeposit> lstMBDeposit = _IMBDepositService.Query.Where(c => c.Recorded == true).ToList();
            List<MCPaymentDetail> lstMCPaymentDetail = _IMCPaymentDetailService.Query.ToList();
            List<MCReceiptDetail> lsMcReceiptDetails = _IMCReceiptDetailService.Query.ToList();
            List<Account> lstAccounts = _IAccountService.GetByDetailType(6);
            List<AccountingObject> lstAccountingObject = _IAccountingObjectService.Query.Where(a => a.IsActive == true).ToList();//Đối tượng nộp
            List<SpendingVoucher> lstCollectionVoucher = new List<SpendingVoucher>();//List đối tượng chứng từ giao dịch
            List<GeneralLedger> lstSoCai = new List<GeneralLedger>();
            List<string> listAccountName = lstAccounts.Select(c => c.AccountNumber).ToList();
            if (bankAccountDetailId != Guid.Empty && currencyId != string.Empty && matchDate > DateTime.MinValue)
            {
                var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.CurrencyID != null
                                 && a.Account != null
                                 && a.PostedDate != null
                                 && a.BankAccountDetailID == bankAccountDetailId
                                 && a.PostedDate <= matchDate
                                 && a.CurrencyID == currencyId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 //&& a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.CreditAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                lstSoCai = query.ToList();
            }
            else if (matchDate > DateTime.MinValue && currencyId == string.Empty)
            {
                var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == bankAccountDetailId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 //&& a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.CreditAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                lstSoCai = query.ToList();
            }
            else
            {
                var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == bankAccountDetailId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 //&& a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.CreditAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                lstSoCai = query.ToList();
            }

            var lstMbInternalTransferDetailsLG = (from a in lstSoCai
                                                  join b in lstMbInternalTransferDetails on a.DetailID equals b.ID
                                                  //join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                                  where b.IsMatch != null && b.IsMatch == isMatched
                                                  select new
                                                  {
                                                      a.DetailID,
                                                      a.TypeID,
                                                      a.Date,
                                                      Nos = (a.No != null ? a.No : ""),
                                                      AccountingObjectNames = "",
                                                      //AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                      Amountt = a.CreditAmount,
                                                      AmountOriginalt = a.CreditAmountOriginal,
                                                      Description = a.Reason,
                                                      BankAccountDetailId = a.BankAccountDetailID,
                                                      b.MatchDate,
                                                  }).ToList();

            var lstMBDepositLG = (from a in lstSoCai
                                  join b in lstMBDeposit on a.ReferenceID equals b.ID
                                  join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                  where b.IsMatch != null && b.IsMatch == isMatched
                                  select new
                                  {
                                      a.ReferenceID,
                                      a.TypeID,
                                      a.Date,
                                      Nos = (a.No != null ? a.No : ""),
                                      AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                      Amountt = a.CreditAmount,
                                      AmountOriginalt = a.CreditAmountOriginal,
                                      Description = a.Reason,
                                      BankAccountDetailId = a.BankAccountDetailID,
                                      b.MatchDate,
                                  }).ToList();

            var lstMCPaymentDetailLG = (from a in lstSoCai
                                        join b in lstMCPaymentDetail on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.CreditAmount,
                                            AmountOriginalt = a.CreditAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lsMcReceiptDetailsLG = (from a in lstSoCai
                                        join b in lsMcReceiptDetails on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.CreditAmount,
                                            AmountOriginalt = a.CreditAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lstMbTellerPapersLG = (from a in lstSoCai
                                       join b in lstMbTellerPapers on a.ReferenceID equals b.ID
                                       join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                       where b.IsMatch != null && b.IsMatch == isMatched
                                       select new
                                       {
                                           a.ReferenceID,
                                           a.TypeID,
                                           a.Date,
                                           Nos = (a.No != null ? a.No : ""),
                                           AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                           Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                           Amountt = a.CreditAmount,
                                           AmountOriginalt = a.CreditAmountOriginal,
                                           Description = a.Reason,
                                           BankAccountDetailId = a.BankAccountDetailID,
                                           b.MatchDate,
                                       }).ToList();

            var lstGOtherVoucherDetailsLG = (from a in lstSoCai
                                             join b in lstGOtherVoucherDetails on a.DetailID equals b.ID
                                             join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                             where b.IsMatch != null && b.IsMatch == isMatched
                                             select new
                                             {
                                                 a.DetailID,
                                                 a.TypeID,
                                                 a.Date,
                                                 Nos = (a.No != null ? a.No : ""),
                                                 AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                 Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                 Amountt = a.CreditAmount,
                                                 AmountOriginalt = a.CreditAmountOriginal,
                                                 Description = a.Reason,
                                                 BankAccountDetailId = a.BankAccountDetailID,
                                                 b.MatchDate,
                                             }).ToList();
            #region Add List
            foreach (var d in lstGOtherVoucherDetailsLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbInternalTransferDetailsLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMBDepositLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMCPaymentDetailLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lsMcReceiptDetailsLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbTellerPapersLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            #endregion
            if (isMatched)
            {
                return (from a in lstCollectionVoucher
                        group a by a.Id into row
                        select new SpendingVoucher()
                        {
                            Id = row.Select(o => o.Id).FirstOrDefault(),
                            TypeId = row.Select(o => o.TypeId).FirstOrDefault(),
                            Date = row.Select(o => o.Date).FirstOrDefault(),
                            No = row.Select(o => o.No).FirstOrDefault(),
                            AccountingObjectName = row.Select(o => o.AccountingObjectName).FirstOrDefault(),
                            Rate = row.Select(o => o.Rate).FirstOrDefault(),
                            Amount = row.Select(o => o.Amount).FirstOrDefault(),
                            AmountOriginal = row.Select(o => o.AmountOriginal).FirstOrDefault(),
                            Description = row.Select(o => o.Description).FirstOrDefault(),
                            MathDate = row.Select(o => o.MathDate).FirstOrDefault(),
                            BankAccountDetailId = row.Select(o => o.BankAccountDetailId).FirstOrDefault(),
                        }).OrderBy(x => x.Date).ThenBy(z => z.No).ToList();
            }
            else
            {
                return lstCollectionVoucher.GroupBy(x => new { x.No, x.TypeId, x.AccountingObjectName, x.BankAccountDetailId, x.Description, x.Date, x.Rate }, (key, group) => new SpendingVoucher()
                {
                    No = key.No,
                    TypeId = key.TypeId,
                    AccountingObjectName = key.AccountingObjectName,
                    Description = key.Description,
                    Date = key.Date,
                    Rate = key.Rate,
                    BankAccountDetailId = key.BankAccountDetailId,
                    Amount = group.Sum(c => c.Amount),
                    AmountOriginal = group.Sum(c => c.AmountOriginal),
                }).ToList().OrderBy(z => z.Date).ThenBy(x => x.No).ToList();
            }
            

        }
        public List<SpendingVoucher> GetSpendingVoucherList(List<Guid> ListBankAccountDetailID, string currencyId, DateTime matchDate, bool isMatched)
        {
            List<MBTellerPaper> lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.Recorded == true).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.ToList();
            List<MBDeposit> lstMBDeposit = _IMBDepositService.Query.Where(c => c.Recorded == true).ToList();
            List<MBDepositDetail> lstMbDepositDetails = _IMBDepositDetailService.Query.ToList();
            List<MCPaymentDetail> lstMCPaymentDetail = _IMCPaymentDetailService.Query.ToList();
            List<MCReceiptDetail> lsMcReceiptDetails = _IMCReceiptDetailService.Query.ToList();
            List<Account> lstAccounts = _IAccountService.GetByDetailType(6);
            List<AccountingObject> lstAccountingObject = _IAccountingObjectService.Query.Where(a => a.IsActive == true).ToList();//Đối tượng nộp
            List<SpendingVoucher> lstCollectionVoucher = new List<SpendingVoucher>();//List đối tượng chứng từ giao dịch
            List<GeneralLedger> lstSoCai = new List<GeneralLedger>();
            List<string> listAccountName = lstAccounts.Select(c => c.AccountNumber).ToList();
            foreach (var x in ListBankAccountDetailID)
            {
                if (currencyId != string.Empty && matchDate >= DateTime.MinValue)
                {
                    var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.CurrencyID != null
                                 && a.Account != null
                                 && a.PostedDate != null
                                 && a.BankAccountDetailID == x
                                 && a.PostedDate <= matchDate
                                 && a.CurrencyID == currencyId
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 && a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.CreditAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                    lstSoCai.AddRange(query.ToList());
                }
                else if (currencyId == string.Empty && matchDate >= DateTime.MinValue)
                {
                    var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == x
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 && a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.CreditAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                    lstSoCai.AddRange(query.ToList());

                }
                else
                {
                    var query = _IGeneralLedgerService.Query.Where(a => a.BankAccountDetailID != Guid.Empty
                                 && a.BankAccountDetailID != null
                                 && a.Account != null
                                 && a.BankAccountDetailID == x
                                 && a.Account.StartsWith("112")
                                 && listAccountName.Contains(a.Account)
                                 && a.Account.StartsWith("112") != a.AccountCorresponding.StartsWith("112")
                                 && a.CreditAmount > 0
                                 && !(a.TypeID >= 700 && a.TypeID <= 709));
                    lstSoCai.AddRange(query.ToList());
                }
            }


            var lstMbInternalTransferDetailsLG = (from a in lstSoCai
                                                  join b in lstMbInternalTransferDetails on a.DetailID equals b.ID
                                                  join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                                  where b.IsMatch != null && b.IsMatch == isMatched
                                                  select new
                                                  {
                                                      a.DetailID,
                                                      a.TypeID,
                                                      a.Date,
                                                      Nos = (a.No != null ? a.No : ""),
                                                      AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                      Amountt = a.CreditAmount,
                                                      AmountOriginalt = a.CreditAmountOriginal,
                                                      Description = a.Reason,
                                                      BankAccountDetailId = a.BankAccountDetailID,
                                                      b.MatchDate,
                                                  }).ToList();

            var lstMBDepositLG = (from a in lstSoCai
                                  join b in lstMBDeposit on a.ReferenceID equals b.ID
                                  join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                  where b.IsMatch != null && b.IsMatch == isMatched
                                  select new
                                  {
                                      a.ReferenceID,
                                      a.TypeID,
                                      a.Date,
                                      Nos = (a.No != null ? a.No : ""),
                                      AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                      Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                      Amountt = a.CreditAmount,
                                      AmountOriginalt = a.CreditAmountOriginal,
                                      Description = a.Reason,
                                      BankAccountDetailId = a.BankAccountDetailID,
                                      b.MatchDate,
                                  }).ToList();

            var lstMCPaymentDetailLG = (from a in lstSoCai
                                        join b in lstMCPaymentDetail on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.CreditAmount,
                                            AmountOriginalt = a.CreditAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lsMcReceiptDetailsLG = (from a in lstSoCai
                                        join b in lsMcReceiptDetails on a.DetailID equals b.ID
                                        join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                        where b.IsMatch != null && b.IsMatch == isMatched
                                        select new
                                        {
                                            a.DetailID,
                                            a.TypeID,
                                            a.Date,
                                            Nos = (a.No != null ? a.No : ""),
                                            AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                            Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                            Amountt = a.CreditAmount,
                                            AmountOriginalt = a.CreditAmountOriginal,
                                            Description = a.Reason,
                                            BankAccountDetailId = a.BankAccountDetailID,
                                            b.MatchDate,
                                        }).ToList();

            var lstMbTellerPapersLG = (from a in lstSoCai
                                       join b in lstMbTellerPapers on a.ReferenceID equals b.ID
                                       join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                       where b.IsMatch != null && b.IsMatch == isMatched
                                       select new
                                       {
                                           a.ReferenceID,
                                           a.TypeID,
                                           a.Date,
                                           Nos = (a.No != null ? a.No : ""),
                                           AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                           Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                           Amountt = a.CreditAmount,
                                           AmountOriginalt = a.CreditAmountOriginal,
                                           Description = a.Reason,
                                           BankAccountDetailId = a.BankAccountDetailID,
                                           b.MatchDate,
                                       }).ToList();

            var lstGOtherVoucherDetailsLG = (from a in lstSoCai
                                             join b in lstGOtherVoucherDetails on a.DetailID equals b.ID
                                             join c in lstAccountingObject on a.AccountingObjectID equals c.ID
                                             where b.IsMatch != null && b.IsMatch == isMatched
                                             select new
                                             {
                                                 a.DetailID,
                                                 a.TypeID,
                                                 a.Date,
                                                 Nos = (a.No != null ? a.No : ""),
                                                 AccountingObjectNames = (c.AccountingObjectName != null ? c.AccountingObjectName : ""),
                                                 Rate = Convert.ToDecimal(a.ExchangeRate != 0 ? a.ExchangeRate : 0),
                                                 Amountt = a.CreditAmount,
                                                 AmountOriginalt = a.CreditAmountOriginal,
                                                 Description = a.Reason,
                                                 BankAccountDetailId = a.BankAccountDetailID,
                                                 b.MatchDate,
                                             }).ToList();
            #region Add List
            foreach (var d in lstGOtherVoucherDetailsLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbInternalTransferDetailsLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMBDepositLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;

                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMCPaymentDetailLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lsMcReceiptDetailsLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.DetailID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            foreach (var d in lstMbTellerPapersLG)
            {
                SpendingVoucher addlist = new SpendingVoucher();
                addlist.Id = d.ReferenceID;
                addlist.TypeId = d.TypeID;
                addlist.Date = d.Date;
                addlist.No = d.Nos;
                addlist.AccountingObjectName = d.AccountingObjectNames;
                addlist.Rate = d.Rate;
                addlist.Amount = d.Amountt;
                addlist.AmountOriginal = d.AmountOriginalt;
                addlist.Description = d.Description;
                addlist.MathDate = Convert.ToDateTime(d.MatchDate);
                addlist.BankAccountDetailId = (Guid)d.BankAccountDetailId;
                lstCollectionVoucher.Add(addlist);
            }
            #endregion
            //return lstCollectionVoucher.GroupBy(x => new { x.No, x.TypeId }, (key, group) => new SpendingVoucher()
            //{
            //    No = key.No,
            //    TypeId = key.TypeId,
            //    Amount = group.Sum(c => c.Amount),
            //    AmountOriginal = group.Sum(c => c.AmountOriginal),
            //}).ToList();//Danh sách đã được group
            return (from a in lstCollectionVoucher
                    group a by a.Id into row
                    select new SpendingVoucher()
                    {
                        Id = row.Select(o => o.Id).FirstOrDefault(),
                        TypeId = row.Select(o => o.TypeId).FirstOrDefault(),
                        Date = row.Select(o => o.Date).FirstOrDefault(),
                        No = row.Select(o => o.No).FirstOrDefault(),
                        AccountingObjectName = row.Select(o => o.AccountingObjectName).FirstOrDefault(),
                        Rate = row.Select(o => o.Rate).FirstOrDefault(),
                        Amount = row.Select(o => o.Amount).FirstOrDefault(),
                        AmountOriginal = row.Select(o => o.AmountOriginal).FirstOrDefault(),
                        Description = row.Select(o => o.Description).FirstOrDefault(),
                        MathDate = row.Select(o => o.MathDate).FirstOrDefault(),
                        BankAccountDetailId = row.Select(o => o.BankAccountDetailId).FirstOrDefault(),
                    }).ToList();
        }
        #endregion
        #region Dũng Lấy số dư đầu kỳ
        /// <summary>
        /// [DungNA]
        /// Lấy số dư đầu kỳ bởi các chứng từ đã đối chiếu
        /// </summary>
        /// <param name="bankAccountDetailId">ID số tài khoản ngân hàng</param>
        /// <param name="currencyId">Loại Tiền Tệ</param>
        /// <param name="matchDate">Ngày đối chiếu</param>
        /// <returns></returns>
        public decimal GetMatchedOverBalance(Guid bankAccountDetailId, string currencyId, DateTime matchDate)
        {
            List<GeneralLedger> lstGeneralLedgers =
                _IGeneralLedgerService.Query.Where(
                    c =>
                        c.BankAccountDetailID == bankAccountDetailId && c.CurrencyID == currencyId &&
                        c.PostedDate <= matchDate && c.Account.StartsWith("112") && c.No == "OPN").ToList();
            decimal opn = lstGeneralLedgers.Sum(c => c.DebitAmountOriginal);
            List<CollectionVoucher> lstCollectionVoucher = GetCollectionVoucher(bankAccountDetailId, currencyId, matchDate, true); // chứng từ thu đã thực hiện đối chiếu
            List<SpendingVoucher> lstSpendingVoucher = GetSpendingVoucher(bankAccountDetailId, currencyId, matchDate, true); // chứng từ chi đã thực hiện đối chiếu
            decimal collectionMatchedOverBalance = lstCollectionVoucher.Sum(x => x.AmountOriginal);
            decimal spendingMatchedOverBalance = lstSpendingVoucher.Sum(x => x.AmountOriginal);
            return opn + collectionMatchedOverBalance - spendingMatchedOverBalance;
        }
        #endregion
        #region Dũng Hàm lấy danh sách các chứng từ đã đối chiếu
        /// <summary>
        /// Lấy danh sách các chứng từ đã đối trừ
        /// [DungNA]
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>        
        /// <returns>Danh sách ComparedVoucher </returns>
        public List<ComparedVoucher> GetComparedVoucher(List<Guid> bankAccountDetailId)
        {
            List<CollectionVoucher> lstCollectionVoucher = GetCollectionVoucherList(bankAccountDetailId, string.Empty, DateTime.MinValue, true);
            List<Accounting.Core.Domain.Type> lstType = _ITypeService.Query.Where(t => t.ID > 0).ToList();
            var lstCollVoucher = from c in lstCollectionVoucher
                                 join t in lstType on c.TypeId equals t.ID
                                 select new
                                 {
                                     c.Id,
                                     t.TypeName,
                                     c.Date,
                                     c.No,
                                     c.AccountingObjectName,
                                     c.Rate,
                                     c.Amount,
                                     c.AmountOriginal,
                                     c.Description
                                 };

            List<SpendingVoucher> lstSpendingVoucher = GetSpendingVoucherList(bankAccountDetailId, string.Empty, DateTime.MinValue, true);
            var lstSpendVoucher = from s in lstSpendingVoucher
                                  join t in lstType on s.TypeId equals t.ID
                                  select new
                                  {
                                      s.Id,
                                      t.TypeName,
                                      s.Date,
                                      s.No,
                                      s.AccountingObjectName,
                                      s.Rate,
                                      s.Amount,
                                      s.AmountOriginal,
                                      s.Description
                                  };

            List<ComparedVoucher> lstComparedVoucher = new List<ComparedVoucher>();
            foreach (var c in lstCollVoucher)
            {
                ComparedVoucher comparedVoucher = new ComparedVoucher();
                comparedVoucher.Date = c.Date;
                comparedVoucher.No = c.No;
                comparedVoucher.AccountingObjectName = c.AccountingObjectName;
                comparedVoucher.Amount = c.Amount;
                comparedVoucher.TypeName = c.TypeName;
                comparedVoucher.Description = c.Description;
                lstComparedVoucher.Add(comparedVoucher);
            }

            foreach (var s in lstSpendVoucher)
            {
                ComparedVoucher comparedVoucher = new ComparedVoucher();
                comparedVoucher.Date = s.Date;
                comparedVoucher.No = s.No;
                comparedVoucher.AccountingObjectName = s.AccountingObjectName;
                comparedVoucher.Amount = s.Amount;
                comparedVoucher.TypeName = s.TypeName;
                comparedVoucher.Description = s.Description;
                lstComparedVoucher.Add(comparedVoucher);
            }

            return lstComparedVoucher;
        }
        #endregion
        #region Hàm bỏ đối chiếu
        public List<Compared> GetCompared(Guid bankAccountDetailId)
        {
            List<Compared> lstCompared = new List<Compared>();
            List<CollectionVoucher> lstCollectionVoucher = GetCollectionVoucher(bankAccountDetailId, string.Empty, DateTime.MinValue, true);
            List<BankAccountDetail> lstBankAccountDetail = _IBankAccountDetailService.Query.Where(b => b.ID == bankAccountDetailId).ToList();
            var lstCollVoucher = from c in lstCollectionVoucher
                                 join b in lstBankAccountDetail on c.BankAccountDetailId equals b.ID
                                 select new
                                 {
                                     c.BankAccountDetailId,
                                     c.MathDate,
                                     b.BankAccount,
                                     b.BankName
                                 };

            List<SpendingVoucher> lstSpendingVoucher = GetSpendingVoucher(bankAccountDetailId, string.Empty, DateTime.MinValue, true);
            var lstSpendVoucher = from s in lstSpendingVoucher
                                  join b in lstBankAccountDetail on s.BankAccountDetailId equals b.ID
                                  select new
                                  {
                                      s.BankAccountDetailId,
                                      s.MathDate,
                                      b.BankAccount,
                                      b.BankName
                                  };
            foreach (var c in lstCollVoucher)
            {
                Compared compared = new Compared();
                compared.BankAccountID = c.BankAccountDetailId;
                compared.DateCompare = c.MathDate;
                compared.BankAccount = c.BankAccount;
                compared.BankName = c.BankName;
                lstCompared.Add(compared);
            }

            foreach (var s in lstSpendVoucher)
            {
                Compared compared = new Compared();
                compared.BankAccountID = s.BankAccountDetailId;
                compared.DateCompare = s.MathDate;
                compared.BankAccount = s.BankAccount;
                compared.BankName = s.BankName;
                lstCompared.Add(compared);
            }

            return lstCompared;
        }
        public List<Compared> GetCompared(List<Guid> bankAccountDetailId)
        {
            List<Compared> lstCompared = new List<Compared>();
            List<CollectionVoucher> lstCollectionVoucher = GetCollectionVoucherList(bankAccountDetailId, string.Empty, DateTime.MinValue, true);
            //var listPhieuThu =
            //    lstCollectionVoucher.GroupBy(c => new { c.BankAccountDetailId, c.MathDate },
            //        (key, group) =>
            //            new CollectionVoucher() { BankAccountDetailId = key.BankAccountDetailId, MathDate = key.MathDate })
            //        .ToList();
            List<BankAccountDetail> lstBankAccountDetail = _IBankAccountDetailService.GetAll().Where(b => bankAccountDetailId.Any(c => c == b.ID)).ToList();
            var lstCollVoucher = from c in lstCollectionVoucher
                                 join b in lstBankAccountDetail on c.BankAccountDetailId equals b.ID
                                 select new
                                 {
                                     c.Id,
                                     c.BankAccountDetailId,
                                     c.MathDate,
                                     b.BankAccount,
                                     b.BankName
                                 };

            List<SpendingVoucher> lstSpendingVoucher = GetSpendingVoucherList(bankAccountDetailId, string.Empty, DateTime.MinValue, true);
            //var listphieuchi =
            //    lstSpendingVoucher.GroupBy(c => new { c.BankAccountDetailId, c.MathDate },
            //        (key, group) =>
            //            new SpendingVoucher() { BankAccountDetailId = key.BankAccountDetailId, MathDate = key.MathDate })
            //        .ToList();
            var lstSpendVoucher = from s in lstSpendingVoucher
                                  join b in lstBankAccountDetail on s.BankAccountDetailId equals b.ID
                                  select new
                                  {
                                      s.Id,
                                      s.BankAccountDetailId,
                                      s.MathDate,
                                      b.BankAccount,
                                      b.BankName
                                  };
            foreach (var c in lstCollVoucher)
            {
                Compared compared = new Compared();
                compared.Id = c.Id;
                compared.BankAccountID = c.BankAccountDetailId;
                compared.DateCompare = c.MathDate;
                compared.BankAccount = c.BankAccount;
                compared.BankName = c.BankName;
                lstCompared.Add(compared);
            }

            foreach (var s in lstSpendVoucher)
            {
                Compared compared = new Compared();
                compared.Id = s.Id;
                compared.BankAccountID = s.BankAccountDetailId;
                compared.DateCompare = s.MathDate;
                compared.BankAccount = s.BankAccount;
                compared.BankName = s.BankName;
                lstCompared.Add(compared);
            }

            return lstCompared.GroupBy(c => new { c.BankAccountID, c.BankAccount, c.DateCompare ,c.BankName}, (key, group) => new Compared()
            {
                BankAccountID = key.BankAccountID,
                BankAccount = key.BankAccount,
                DateCompare = key.DateCompare,
                BankName = key.BankName,
                lstIDGeneralLedger = group.Select(c => c.Id).ToList()
            }).ToList();
            //return lstCompared.ToList();
        }
        public bool Remove(Guid bankAccountDetailId, DateTime dateCompared, bool Status)
        {
            bool checkSTT = false;
            #region list phiếu thu
            List<MBTellerPaper> lstMbTellerPapers = new List<MBTellerPaper>();
            List<MBTellerPaperDetail> lstTellerPaperDetails = new List<MBTellerPaperDetail>();
            List<MBInternalTransfer> lstMBInternalTransfer = new List<MBInternalTransfer>();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = new List<MBInternalTransferDetail>();
            List<GOtherVoucher> lstGOtherVouchers = new List<GOtherVoucher>();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = new List<GOtherVoucherDetail>();
            List<MBCreditCard> lstMbCreditCards = new List<MBCreditCard>();
            List<MBCreditCardDetail> lstCreditCardDetails = new List<MBCreditCardDetail>();
            List<MBDeposit> lstMBDeposit = new List<MBDeposit>();
            List<MBDepositDetail> lstMbDepositDetails = new List<MBDepositDetail>();
            List<MCPayment> lstMCPayment = new List<MCPayment>();
            List<MCPaymentDetail> lstMCPaymentDetail = new List<MCPaymentDetail>();
            List<MCReceipt> lstMcReceipts = new List<MCReceipt>();
            List<MCReceiptDetail> lsMcReceiptDetails = new List<MCReceiptDetail>();
            #endregion
            #region List phiếu chi
            List<MBTellerPaper> lstMbTellerPapersSP = new List<MBTellerPaper>();
            List<MBTellerPaperDetail> lstTellerPaperDetailsSP = new List<MBTellerPaperDetail>();
            List<MBInternalTransfer> lstMBInternalTransferSP = new List<MBInternalTransfer>();
            List<MBInternalTransferDetail> lstMbInternalTransferDetailsSP = new List<MBInternalTransferDetail>();
            List<GOtherVoucher> lstGOtherVouchersSP = new List<GOtherVoucher>();
            List<GOtherVoucherDetail> lstGOtherVoucherDetailsSP = new List<GOtherVoucherDetail>();
            List<MBCreditCard> lstMbCreditCardsSP = new List<MBCreditCard>();
            List<MBCreditCardDetail> lstCreditCardDetailsSP = new List<MBCreditCardDetail>();
            List<MBDeposit> lstMBDepositSP = new List<MBDeposit>();
            List<MBDepositDetail> lstMbDepositDetailsSP = new List<MBDepositDetail>();
            List<MCPayment> lstMCPaymentSP = new List<MCPayment>();
            List<MCPaymentDetail> lstMCPaymentDetailSP = new List<MCPaymentDetail>();
            List<MCReceipt> lstMcReceiptsSP = new List<MCReceipt>();
            List<MCReceiptDetail> lsMcReceiptDetailsSP = new List<MCReceiptDetail>();
            #endregion

            base.BeginTran();
            #region Update Phiếu thu
            lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.MatchDate != null && c.IsMatch != null && c.BankAccountDetailID == bankAccountDetailId && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            if (lstMbTellerPapers.Count > 0)
            {
                foreach (MBTellerPaper x in lstMbTellerPapers)
                {
                    x.IsMatch = false;
                    _IMBTellerPaperService.Update(x);
                }
            }
            lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.Where(c => c.DebitAccount != null && c.FromBankAccountDetailID != null && c.FromBankAccountDetailID != Guid.Empty && c.MatchDate != null && c.IsMatch != null && c.DebitAccount.StartsWith("112") && c.FromBankAccountDetailID == bankAccountDetailId && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            if (lstMbInternalTransferDetails.Count > 0)
            {
                foreach (MBInternalTransferDetail x in lstMbInternalTransferDetails)
                {
                    x.IsMatch = false;
                    _IMBInternalTransferDetailService.Update(x);
                }
            }
            lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.Where(c => c.DebitAccount != null && c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.MatchDate != null && c.IsMatch != null && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            if (lstGOtherVoucherDetails.Count > 0)
            {
                foreach (GOtherVoucherDetail x in lstGOtherVoucherDetails)
                {
                    x.IsMatch = false;
                    _IGOtherVoucherDetailService.Update(x);
                }
            }
            lstMbCreditCards = _IMBCreditCardService.Query.Where(c => c.MatchDate != null && c.IsMatch != null && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            lstCreditCardDetails = _IMBCreditCardDetailService.Query.Where(c => c.DebitAccount != null && c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId).ToList();
            var listCreditCard = (from a in lstMbCreditCards
                                  join b in lstCreditCardDetails on a.ID equals b.MBCreditCardID
                                  select a).ToList();
            if (listCreditCard.Count > 0)
            {
                foreach (MBCreditCard x in listCreditCard)
                {
                    x.IsMatch = false;
                    _IMBCreditCardService.Update(x);
                }
            }
            lstMBDeposit = _IMBDepositService.Query.Where(c => c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.BankAccountDetailID == bankAccountDetailId && c.IsMatch == true && c.MatchDate == dateCompared).ToList();
            if (lstMBDeposit.Count > 0)
            {
                foreach (MBDeposit x in lstMBDeposit)
                {
                    x.IsMatch = false;
                    _IMBDepositService.Update(x);
                }
            }
            lstMCPaymentDetail = _IMCPaymentDetailService.Query.Where(c => c.DebitAccount != null && c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId && c.IsMatch == true && c.MatchDate == dateCompared).ToList();
            if (lstMCPaymentDetail.Count > 0)
            {
                foreach (MCPaymentDetail x in lstMCPaymentDetail)
                {
                    x.IsMatch = false;
                    _IMCPaymentDetailService.Update(x);
                }
            }
            lsMcReceiptDetails = _IMCReceiptDetailService.Query.Where(c => c.DebitAccount != null && c.MatchDate != null && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.BankAccountDetailID == bankAccountDetailId && c.IsMatch == true && c.MatchDate == dateCompared).ToList();
            if (lsMcReceiptDetails.Count > 0)
            {
                foreach (MCReceiptDetail x in lsMcReceiptDetails)
                {
                    x.IsMatch = false;
                    _IMCReceiptDetailService.Update(x);
                }
            }
            #endregion
            #region Update Phiếu chi
            lstMbTellerPapersSP = _IMBTellerPaperService.Query.Where(c => c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.BankAccountDetailID == bankAccountDetailId && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            if (lstMbTellerPapersSP.Count > 0)
            {
                foreach (MBTellerPaper x in lstMbTellerPapersSP)
                {
                    x.IsMatch = false;
                    _IMBTellerPaperService.Update(x);
                }
            }
            lstMbInternalTransferDetailsSP = _IMBInternalTransferDetailService.Query.Where(c => c.MatchDate != null && c.FromBankAccountDetailID != null && c.IsMatch != null && c.FromBankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.CreditAccount.StartsWith("112") && c.FromBankAccountDetailID == bankAccountDetailId && c.MatchDate != dateCompared && c.IsMatch == true).ToList();
            if (lstMbInternalTransferDetailsSP.Count > 0)
            {
                foreach (MBInternalTransferDetail x in lstMbInternalTransferDetailsSP)
                {
                    x.IsMatch = false;
                    _IMBInternalTransferDetailService.Update(x);
                }
            }
            lstGOtherVoucherDetailsSP = _IGOtherVoucherDetailService.Query.Where(c => c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            if (lstGOtherVoucherDetailsSP.Count > 0)
            {
                foreach (GOtherVoucherDetail x in lstGOtherVoucherDetailsSP)
                {
                    x.IsMatch = false;
                    _IGOtherVoucherDetailService.Update(x);
                }
            }
            lstMbCreditCardsSP = _IMBCreditCardService.Query.Where(c => c.MatchDate != null && c.IsMatch != null && c.MatchDate == dateCompared && c.IsMatch == true).ToList();
            lstCreditCardDetailsSP = _IMBCreditCardDetailService.Query.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId).ToList();
            var listCreditCardSP = (from a in lstMbCreditCardsSP
                                    join b in lstCreditCardDetailsSP on a.ID equals b.MBCreditCardID
                                    select a).ToList();
            if (listCreditCardSP.Count > 0)
            {
                foreach (MBCreditCard x in listCreditCardSP)
                {
                    x.IsMatch = false;
                    _IMBCreditCardService.Update(x);
                }
            }
            lstMBDepositSP = _IMBDepositService.Query.Where(c => c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.BankAccountDetailID == bankAccountDetailId && c.IsMatch == true && c.MatchDate == dateCompared).ToList();
            if (lstMBDepositSP.Count > 0)
            {
                foreach (MBDeposit x in lstMBDepositSP)
                {
                    x.IsMatch = false;
                    _IMBDepositService.Update(x);
                }
            }
            lstMCPaymentDetailSP = _IMCPaymentDetailService.Query.Where(c => c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId && c.IsMatch == true && c.MatchDate == dateCompared).ToList();
            if (lstMCPaymentDetailSP.Count > 0)
            {
                foreach (MCPaymentDetail x in lstMCPaymentDetailSP)
                {
                    x.IsMatch = false;
                    _IMCPaymentDetailService.Update(x);
                }
            }
            lsMcReceiptDetailsSP = _IMCReceiptDetailService.Query.Where(c => c.MatchDate != null && c.BankAccountDetailID != null && c.IsMatch != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == bankAccountDetailId && c.IsMatch == true && c.MatchDate == dateCompared).ToList();
            if (lsMcReceiptDetailsSP.Count > 0)
            {
                foreach (MCReceiptDetail x in lsMcReceiptDetailsSP)
                {
                    x.IsMatch = false;
                    _IMCReceiptDetailService.Update(x);
                }
            }
            #endregion
            base.CommitTran();
            return checkSTT;
        }
        public bool RemoveList(List<Compared> lstCompareds)
        {
            List<Compared> lstComparedsss = (from a in lstCompareds
                                             from b in a.lstIDGeneralLedger
                                             select new Compared()
                                             {
                                                 Id = b,
                                                 BankAccountID = a.BankAccountID,
                                                 DateCompare = a.DateCompare,
                                                 BankAccount = a.BankAccount,
                                                 BankName = a.BankName,
                                             }

                ).ToList();
            bool checkSTT = false;
            #region list phiếu thu

            List<MBTellerPaper> lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.IsMatch != null && c.IsMatch == true).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatchTo != null && c.IsMatchTo == true).ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            List<MBDeposit> lstMBDeposit = _IMBDepositService.Query.Where(c => c.IsMatch == true).ToList();
            List<MCPaymentDetail> lstMCPaymentDetail = _IMCPaymentDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            List<MCReceiptDetail> lsMcReceiptDetails = _IMCReceiptDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            #endregion

            #region List phiếu chi
            List<MBTellerPaper> lstMbTellerPapersSP = _IMBTellerPaperService.Query.Where(c => c.IsMatch != null && c.IsMatch == true).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetailsSP = _IMBInternalTransferDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetailsSP = _IGOtherVoucherDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            List<MBDeposit> lstMBDepositSP = _IMBDepositService.Query.Where(c => c.IsMatch != null && c.IsMatch == true).ToList();
            List<MCPaymentDetail> lstMCPaymentDetailSP = _IMCPaymentDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            List<MCReceiptDetail> lsMcReceiptDetailsSP = _IMCReceiptDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == true).ToList();
            #endregion
            base.BeginTran();
            foreach (Compared y in lstComparedsss)
            {

                #region Update Phiếu thu
                var lstMbTellerPapersSave1 = lstMbTellerPapers.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
                if (lstMbTellerPapersSave1.Count > 0)
                {
                    foreach (MBTellerPaper x in lstMbTellerPapersSave1)
                    {

                        x.IsMatch = false;
                        _IMBTellerPaperService.Update(x);
                    }
                }
               var lstMbInternalTransferDetails1 = lstMbInternalTransferDetails.Where(c => c.ToBankAccountDetailID != null && c.ToBankAccountDetailID != Guid.Empty && c.IsMatchTo != null && c.ToBankAccountDetailID == y.BankAccountID && c.IsMatchTo == true && c.MatchDateTo == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMbInternalTransferDetails1.Count > 0)
                {
                    foreach (MBInternalTransferDetail x in lstMbInternalTransferDetails1)
                    {

                        x.IsMatchTo = false;
                        _IMBInternalTransferDetailService.Update(x);
                    }
                }
               var lstGOtherVoucherDetails1 = lstGOtherVoucherDetails.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstGOtherVoucherDetails1.Count > 0)
                {
                    foreach (GOtherVoucherDetail x in lstGOtherVoucherDetails1)
                    {

                        x.IsMatch = false;
                        _IGOtherVoucherDetailService.Update(x);
                    }
                }

               var lstMBDeposit1 = lstMBDeposit.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMBDeposit1.Count > 0)
                {
                    foreach (MBDeposit x in lstMBDeposit1)
                    {

                        x.IsMatch = false;
                        _IMBDepositService.Update(x);
                    }
                }
               var lstMCPaymentDetail1 = lstMCPaymentDetail.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMCPaymentDetail1.Count > 0)
                {
                    foreach (MCPaymentDetail x in lstMCPaymentDetail1)
                    {

                        x.IsMatch = false;
                        _IMCPaymentDetailService.Update(x);
                    }
                }
               var lsMcReceiptDetails1 = lsMcReceiptDetails.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lsMcReceiptDetails1.Count > 0)
                {
                    foreach (MCReceiptDetail x in lsMcReceiptDetails1)
                    {

                        x.IsMatch = false;
                        _IMCReceiptDetailService.Update(x);
                    }
                }
                #endregion
                #region Update Phiếu chi
               var lstMbTellerPapersSP12 = lstMbTellerPapersSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMbTellerPapersSP12.Count > 0)
                {
                    foreach (MBTellerPaper x in lstMbTellerPapersSP12)
                    {

                        x.IsMatch = false;
                        _IMBTellerPaperService.Update(x);
                    }
                }
               var lstMbInternalTransferDetailsSP1 = lstMbInternalTransferDetailsSP.Where(c => c.FromBankAccountDetailID != null && c.FromBankAccountDetailID != Guid.Empty && c.IsMatch != null && c.FromBankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMbInternalTransferDetailsSP1.Count > 0)
                {
                    foreach (MBInternalTransferDetail x in lstMbInternalTransferDetailsSP1)
                    {

                        x.IsMatch = false;
                        _IMBInternalTransferDetailService.Update(x);
                    }
                }
               var lstGOtherVoucherDetailsSP1 = lstGOtherVoucherDetailsSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstGOtherVoucherDetailsSP1.Count > 0)
                {
                    foreach (GOtherVoucherDetail x in lstGOtherVoucherDetailsSP1)
                    {

                        x.IsMatch = false;
                        _IGOtherVoucherDetailService.Update(x);
                    }
                }
               var lstMBDepositSP1 = lstMBDepositSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMBDepositSP1.Count > 0)
                {
                    foreach (MBDeposit x in lstMBDepositSP1)
                    {

                        x.IsMatch = false;
                        _IMBDepositService.Update(x);
                    }
                }
               var lstMCPaymentDetailSP1 = lstMCPaymentDetailSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lstMCPaymentDetailSP1.Count > 0)
                {
                    foreach (MCPaymentDetail x in lstMCPaymentDetailSP1)
                    {

                        x.IsMatch = false;
                        _IMCPaymentDetailService.Update(x);
                    }
                }
               var lsMcReceiptDetailsSP1 = lsMcReceiptDetailsSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == true && c.MatchDate == y.DateCompare && c.ID == y.Id).ToList();
               if (lsMcReceiptDetailsSP1.Count > 0)
                {
                    foreach (MCReceiptDetail x in lsMcReceiptDetailsSP1)
                    {

                        x.IsMatch = false;
                        _IMCReceiptDetailService.Update(x);
                    }
                }
                #endregion

            }
            base.CommitTran();
            return checkSTT;
        }
        #endregion
        #region Hàm thực hiện đối chiếu
        public bool ComparedV(List<Compared> lstCompareds, DateTime dateCompareD)
        {
            bool checkSTT = false;
            #region list phiếu thu
            List<MBTellerPaper> lstMbTellerPapers = _IMBTellerPaperService.Query.Where(c => c.IsMatch != null && c.IsMatch == false).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetails = _IMBInternalTransferDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatchTo != null && c.IsMatchTo == false).ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetails = _IGOtherVoucherDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            List<MBDeposit> lstMBDeposit = _IMBDepositService.Query.Where(c => c.IsMatch == false).ToList();
            List<MCPaymentDetail> lstMCPaymentDetail = _IMCPaymentDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            List<MCReceiptDetail> lsMcReceiptDetails = _IMCReceiptDetailService.Query.Where(c => c.DebitAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            #endregion

            #region List phiếu chi
            List<MBTellerPaper> lstMbTellerPapersSP = _IMBTellerPaperService.Query.Where(c => c.IsMatch != null && c.IsMatch == false).ToList();
            List<MBInternalTransferDetail> lstMbInternalTransferDetailsSP = _IMBInternalTransferDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            List<GOtherVoucherDetail> lstGOtherVoucherDetailsSP = _IGOtherVoucherDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            List<MBDeposit> lstMBDepositSP = _IMBDepositService.Query.Where(c => c.IsMatch != null && c.IsMatch == false).ToList();
            List<MCPaymentDetail> lstMCPaymentDetailSP = _IMCPaymentDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            List<MCReceiptDetail> lsMcReceiptDetailsSP = _IMCReceiptDetailService.Query.Where(c => c.CreditAccount != null && c.IsMatch != null && c.IsMatch == false).ToList();
            #endregion
            foreach (Compared y in lstCompareds)
            {
                try
                {
                    base.BeginTran();
                    #region Update Phiếu thu
                    var lstMbTellerPapersCompare = lstMbTellerPapers.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMbTellerPapersCompare.Count > 0)
                    {
                        foreach (MBTellerPaper x in lstMbTellerPapersCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMBTellerPaperService.Update(x);
                        }
                    }
                    var lstMbInternalTransferDetailsCompare = lstMbInternalTransferDetails.Where(c => c.ToBankAccountDetailID != null && c.ToBankAccountDetailID != Guid.Empty && c.DebitAccount != null && c.IsMatchTo != null && c.DebitAccount.StartsWith("112") && c.ToBankAccountDetailID == y.BankAccountID && c.IsMatchTo == false && c.ID == y.Id).ToList();
                    if (lstMbInternalTransferDetailsCompare.Count > 0)
                    {
                        foreach (MBInternalTransferDetail x in lstMbInternalTransferDetailsCompare)
                        {
                            x.MatchDateTo = dateCompareD;
                            x.IsMatchTo = true;
                            _IMBInternalTransferDetailService.Update(x);
                        }
                    }
                    var lstGOtherVoucherDetailsCompare = lstGOtherVoucherDetails.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.DebitAccount != null && c.IsMatch != null && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstGOtherVoucherDetailsCompare.Count > 0)
                    {
                        foreach (GOtherVoucherDetail x in lstGOtherVoucherDetailsCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IGOtherVoucherDetailService.Update(x);
                        }
                    }

                    var lstMBDepositCompare = lstMBDeposit.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMBDepositCompare.Count > 0)
                    {
                        foreach (MBDeposit x in lstMBDepositCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMBDepositService.Update(x);
                        }
                    }
                    var lstMCPaymentDetailCompare = lstMCPaymentDetail.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.DebitAccount != null && c.IsMatch != null && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMCPaymentDetailCompare.Count > 0)
                    {
                        foreach (MCPaymentDetail x in lstMCPaymentDetailCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMCPaymentDetailService.Update(x);
                        }
                    }
                    var lsMcReceiptDetailsCompare = lsMcReceiptDetails.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.DebitAccount != null && c.IsMatch != null && c.DebitAccount.StartsWith("112") && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lsMcReceiptDetailsCompare.Count > 0)
                    {
                        foreach (MCReceiptDetail x in lsMcReceiptDetailsCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMCReceiptDetailService.Update(x);
                        }
                    }
                    #endregion
                    #region Update Phiếu chi
                    var lstMbTellerPapersSPCompare = lstMbTellerPapersSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMbTellerPapersSPCompare.Count > 0)
                    {
                        foreach (MBTellerPaper x in lstMbTellerPapersSPCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMBTellerPaperService.Update(x);
                        }
                    }
                    var lstMbInternalTransferDetailsSPCompare = lstMbInternalTransferDetailsSP.Where(c => c.FromBankAccountDetailID != null && c.FromBankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.IsMatch != null && c.CreditAccount.StartsWith("112") && c.FromBankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMbInternalTransferDetailsSPCompare.Count > 0)
                    {
                        foreach (MBInternalTransferDetail x in lstMbInternalTransferDetailsSPCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMBInternalTransferDetailService.Update(x);
                        }
                    }
                    var lstGOtherVoucherDetailsSPCompare = lstGOtherVoucherDetailsSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.IsMatch != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstGOtherVoucherDetailsSPCompare.Count > 0)
                    {
                        foreach (GOtherVoucherDetail x in lstGOtherVoucherDetailsSPCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IGOtherVoucherDetailService.Update(x);
                        }
                    }
                    var lstMBDepositSPCompare = lstMBDepositSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.IsMatch != null && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMBDepositSPCompare.Count > 0)
                    {
                        foreach (MBDeposit x in lstMBDepositSPCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMBDepositService.Update(x);
                        }
                    }
                    var lstMCPaymentDetailSPCompare = lstMCPaymentDetailSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.IsMatch != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lstMCPaymentDetailSPCompare.Count > 0)
                    {
                        foreach (MCPaymentDetail x in lstMCPaymentDetailSPCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMCPaymentDetailService.Update(x);
                        }
                    }
                    var lsMcReceiptDetailsSPCompare = lsMcReceiptDetailsSP.Where(c => c.BankAccountDetailID != null && c.BankAccountDetailID != Guid.Empty && c.CreditAccount != null && c.IsMatch != null && c.CreditAccount.StartsWith("112") && c.BankAccountDetailID == y.BankAccountID && c.IsMatch == false && c.ID == y.Id).ToList();
                    if (lsMcReceiptDetailsSPCompare.Count > 0)
                    {
                        foreach (MCReceiptDetail x in lsMcReceiptDetailsSPCompare)
                        {
                            x.MatchDate = dateCompareD;
                            x.IsMatch = true;
                            _IMCReceiptDetailService.Update(x);
                        }
                    }
                    #endregion
                    base.CommitTran();
                }
                catch (Exception)
                {
                    base.RolbackTran();

                    throw;
                }

            }
            return checkSTT;
        }
        #endregion
        #endregion



    }
}