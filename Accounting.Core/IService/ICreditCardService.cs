﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ICreditCardService : FX.Data.IBaseService<CreditCard, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách CreditCard
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        List<CreditCard> GetListCreditCardCreditCardNumber(string creditCardNumber);
        /// <summary>
        /// Lấy Thông tin thẻ tin dụng theo số thẻ [khanhtq]
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        CreditCard GetByNumber(string creditCardNumber);

        /// <summary>
        /// Đếm danh sách CreditCard theo CreditCardNumber
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        int CountListCreditCardCreditCardNumber(string creditCardNumber);

        /// <summary>
        /// Hàm lấy ra list creditcard đang hoạt động
        /// </summary>
        /// <param name="isActive">creditcard còn hoạt động = true, không còn = false</param>
        /// <returns></returns>
        List<CreditCard> GetAll_IsActive(bool isActive);

        List<CreditCard> GetAll_OrderBy();
    }
}
