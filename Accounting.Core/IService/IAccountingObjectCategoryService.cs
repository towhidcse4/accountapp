﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IAccountingObjectCategoryService : FX.Data.IBaseService<AccountingObjectCategory, Guid>
    {
        /// <summary>
        /// Lấy danh sách được tiêu chi active và order by AccountingObjectCategoryCode.
        /// </summary>
        /// <param name="isActive">true: false</param>
        /// <returns>Danh sách được order</returns>
        List<AccountingObjectCategory> GetByIsActiveOrderByCode(bool isActive);

        /// <summary>
        /// Lấy danh sách các AccountingObjectCategoryCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectCategoryCode, null nếu bị lỗi</returns>
        List<string> GetAccountingObjectCategoryCode();
        Guid? GetGuidAccountingObjectCategoryByCode(string code);
        List<AccountingObjectCategory> GetAll_OrderBy();
    }
}
