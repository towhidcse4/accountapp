using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ICustomerConfigService : FX.Data.IBaseService<CustomerConfig, String>
    {
    }
}
