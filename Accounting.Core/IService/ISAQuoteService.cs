using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ISAQuoteService : FX.Data.IBaseService<SAQuote, Guid>
    {
        List<SAQuote> OrderByCode();
    }
}
