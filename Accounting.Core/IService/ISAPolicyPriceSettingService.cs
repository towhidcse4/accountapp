﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ISAPolicyPriceSettingService:FX.Data.IBaseService<SAPolicyPriceSetting ,Guid>
   {
       /// <summary>
       /// [DungNA]
       /// Lấy danh sách chính sách giá giá bán
       /// </summary>
       /// <returns></returns>
       List<SAPolicyPriceSetting> GetPolicyPriceSettings();

       /// <summary>
       /// [DungNA]
       /// Lấy danh bật tư hàng hóa áp dụng
       /// </summary>
       /// <returns></returns>
       List<SAPolicyPriceTableHT> GetPolicyPriceTabless(Guid saPolicyPriceSettingID);

       /// <summary>
       /// [DungNA]
       /// Lấy ra danh sách khách hàng áp dụng
       /// </summary>
       /// <returns></returns>
       List<SAPolicySalePriceCustomerHT> GetSAPolicySalePriceCustomerHT(Guid saPolicyPriceSettingID);

       /// <summary>
       /// [DungNA]
       /// Lấy ra danh sách vật tư hàng hóa
       /// </summary>
       /// <returns></returns>
       List<MaterialGoodHT> GetMaterialGoodHts(bool isadd, Guid ID);

       /// <summary>
       /// [DungNA]
       /// Lấy danh sách nhóm giá bán
       /// </summary>
       /// <returns></returns>
       List<GroupSalePriceHT> GetGroupSalePriceHts();

       /// <summary>
       /// [DungNA]
       /// Lấy danh sách thiết lập bảng giá
       /// </summary>
       /// <returns></returns>
       List<PriceUpHT> GetListPriceUpHT(List<MaterialGoodHT> lsMaterialGoodHts);

       /// <summary>
       /// [DungNA]
       /// Chọn khách hàng cho các nhóm
       /// </summary>
       /// <returns></returns>
       List<SAPolicySalePriceCustomerHT> GetLstChoiceAccountingOBJ();
        decimal UnitPrice(Guid MID, Guid CID);
    }
}
