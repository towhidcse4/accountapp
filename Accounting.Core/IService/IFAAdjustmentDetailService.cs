﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFAAdjustmentDetailService:FX.Data.IBaseService<FAAdjustmentDetail, Guid>
    {
        List<FAAdjustmentDetail> GetByFAADjeusmenttId(Guid faAdjustmenID);
    }
}
