﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IPaymentClauseService : FX.Data.IBaseService<PaymentClause, Guid>
    {
        /// <summary>
        /// Lấy danh sách Điều khoản thanh toán đang hoạt động [khanhtq]
        /// </summary>
        /// <param name="isActive">Giá trị</param>
        /// <returns>Danh sách </returns>
        List<PaymentClause> GetIsActive(bool isActive);
        /// <summary>
        /// Sắp xếp theo mã điều khoản thanh toán [khanhtq]
        /// </summary>
        /// <returns>danh sách điều khoản thanh toán</returns>
        List<PaymentClause> OrderByCode();
        /// <summary>
        /// Lấy tất cả danh sách mã điều khoản thanh toán [khanhtq]
        /// </summary>
        /// <returns> danh sách mã điều khoản thanh toán</returns>
        List<string> GetCode();

        /// <summary>
        /// Lấy thông tin điều khoản thanh toán theo ID [PhongNT]
        /// </summary>
        /// <returns> danh sách mã điều khoản thanh toán</returns>
        PaymentClause GetByID(Guid id);
        Guid GetGuidPaymentClauseByCode(string code);
    }
}
