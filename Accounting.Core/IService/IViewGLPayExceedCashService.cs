﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface IViewGLPayExceedCashService : FX.Data.IBaseService<ViewGLPayExceedCash, Guid>
    {
        List<ViewGLPayExceedCash> GetAll_OrderBy();
    }
}
