﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IShareHolderGroupService : FX.Data.IBaseService<ShareHolderGroup, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách tất cả các ShareHolderGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>Danh sách ShareHolderGroupCode dạng chuỗi, null nếu bị lỗi</returns>
        List<string> GetShareHolderGroupCode();
    }
}
