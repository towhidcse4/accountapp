using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITypeGroupService : FX.Data.IBaseService<TypeGroup, int>
    {
    }
}
