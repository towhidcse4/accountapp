﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPInvoiceService:FX.Data.IBaseService<PPInvoice ,Guid>
   {
       PPInvoice GetPPInvoicebyNo(string no);

       /// <summary>
       /// Lấy danh sách mua hàng theo trạng thái nhận hóa đơn
       /// </summary>
       /// <param name="billReceived">đã nhận hóa đơn(true)/chưa nhận hóa đơn(false)</param>
       /// <param name="accountingObjectID">ID nhà cung cấp (Guid)</param>
       /// <param name="isImportPurchase">Trong nước(true)/Nhập khẩu(false)</param>
       /// <param name="dtBegin">từ ngày</param>
       /// <param name="dtEnd">đến ngày</param>
       /// <returns></returns>
       List<PPInvoice> GetByBillReceived(bool billReceived, Guid? accountingObjectID, bool isImportPurchase,
                                         DateTime dtBegin, DateTime dtEnd);
        /// <summary>
        /// Lấy danh sách mua hàng theo trạng thái nhận hóa đơn
        /// </summary>
        /// <param name="billReceived">đã nhận hóa đơn(true)/chưa nhận hóa đơn(false)</param>
        /// <param name="accountingObjectID">ID nhà cung cấp (Guid)</param>
        /// <param name="isImportPurchase">Trong nước(true)/Nhập khẩu(false)</param>
        /// <param name="dtBegin">từ ngày</param>
        /// <param name="dtEnd">đến ngày</param>
        /// <param name="isImport">là hóa đơn mua hàng nhập khẩu</param>
        /// <returns></returns>
        List<PPInvoice> GetByBillReceived(bool billReceived, Guid? accountingObjectID, bool isImportPurchase,
                                         DateTime dtBegin, DateTime dtEnd, bool? isImport);

        /// <summary>
        /// Sổ chi tiết mua hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="accountingObjectID">id nhà cung cấp</param>
        /// <param name="materialGoodsID">ds id mặt hàng</param>
        /// <param name="accountingObjectIDinPpInvoices">bằng true - không lấy theo đối tượng trên thông tin chung</param>
        List<PurchaseDetailSupplerInventoryItem> ReportPurchaseDetailSupplerInventoryItem(DateTime fromDate, DateTime toDate, IEnumerable<Guid> accountingObjectID, List<Guid> materialGoodsID, bool accountingObjectIDinPpInvoices);

       /// <summary>
       /// Sổ nhật ký mua hàng
       /// [DuyNT]
       /// </summary>
       /// <param name="fromDate">từ ngày</param>
       /// <param name="toDate">đến ngày</param>
       /// <returns></returns>
       List<S03A3DNN> ReportS03A3Dn(DateTime fromDate, DateTime toDate);
        List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
    }
}
