﻿using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IGenCodeService : FX.Data.IBaseService<GenCode, Guid>
    {
        GenCode getGenCode(int TypeGroupID);

        /// <summary>
        /// Khong thuc hien commit transaction de ben ngoai
        /// [kiendd]
        /// </summary>
        /// <param name="TypeGroupID"></param>
        /// <returns></returns>
        bool UpdateGenCodeForm(int TypeGroupID, string textNo, Guid VoucherID );

        /// <summary>
        /// Thực hiện cập nhật Gencode cho phần danh mục có gencode
        /// </summary>
        /// <param name="TypeGroupID"></param>
        /// <param name="textNo"></param>
        /// <returns></returns>
        bool UpdateGenCodeCatalog(int TypeGroupID, string textNo);
    }
}
