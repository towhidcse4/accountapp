﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IParametersService : FX.Data.IBaseService<Parameters, Guid>
    {
        /// <summary>
        /// Lấy tham số hệ thống kế toán bằng code của tham số
        /// </summary>
        /// <returns>Tham số hệ thống tương ứng</returns>
        Parameters GetByCode(string code);
    }
}
