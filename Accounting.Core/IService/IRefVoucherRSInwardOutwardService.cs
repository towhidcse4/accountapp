﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IRefVoucherRSInwardOutwardService : FX.Data.IBaseService<RefVoucherRSInwardOutward, Guid>
    {
        List<RefVoucherRSInwardOutward> GetByRefID2(Guid refID2);        
    }
}
