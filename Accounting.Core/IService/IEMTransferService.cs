﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMTransferService : FX.Data.IBaseService<EMTransfer, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách EMTransferCode
        /// </summary>
        /// <returns></returns>
        List<string> GetListEMTransferCode();
    }
}
