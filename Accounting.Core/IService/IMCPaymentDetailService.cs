﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IMCPaymentDetailService : FX.Data.IBaseService<MCPaymentDetail, Guid>
    {
        /// <summary>
        /// lấy danh sách Chi tiết hoạch toán phiếu thu theo phiếu chi
        /// </summary>
        /// <param name="mCPaymentId">ID của phiếu chi</param>
        /// <returns>Danh sách chi tiết phiêu chi</returns>
        List<MCPaymentDetail> GetByMcPaymentId(Guid mCPaymentId);
    }
}
