﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IEMDevidendService : FX.Data.IBaseService<EMDevidend, Guid>
    {
        /// <summary>
        /// [DungNA]
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to"> Đến Ngày</param>
        /// <param name="EmshareHolderGroup">Nhóm Cổ đông</param>
        /// <param name="LockDate">Ngày Chốt</param>
        /// <returns>Danh sách cổ tức phải trả cho nhóm cổ đông</returns>
       // List<Dividends312> RPDividends312(DateTime from, DateTime to, List<Guid> EmshareHolderGroup, DateTime LockDate);
    }
}
