﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface ISynthesisReportService : FX.Data.IBaseService<SynthesisReport, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách các dòng báo cáo được phép chỉnh sửa
        /// [DuyNT]
        /// </summary>
        /// <param name="isDefault">được phép chỉnh sửa (mặc định bằng true)</param>
        /// <returns>null nếu lỗi</returns>
        List<SynthesisReport> GetAll_ByIsDefault(bool isDefault = true);

        /// <summary>
        /// Lấy ra danh sách báo cáo BalanceSheet bảng cân đối kết toán
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns>null nếu lỗi</returns>
        List<Assets> ReportBalanceSheet(DateTime fromDate, DateTime toDate);

        /// <summary>
        /// Lấy ra danh sách báo cáo IncomeStatement Báo cáo kết quả kinh doanh
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns>null nếu lỗi</returns>
        List<Assets> ReportIncomeStatement(DateTime fromDate, DateTime toDate);

        /// <summary>
        /// Lấy ra danh sách báo cáo CashFlowStatement Báo cáo lưu chuyển tiền tệ
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        List<Assets> ReportCashFlowStatement(DateTime fromDate, DateTime toDate);
    }
}
