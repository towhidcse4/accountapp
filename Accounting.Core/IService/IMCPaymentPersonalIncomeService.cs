using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentPersonalIncomeService : FX.Data.IBaseService<MCPaymentPersonalIncome, Guid>
    {
    }
}
