﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRepositoryService : FX.Data.IBaseService<Repository, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách mã kho 
        /// [longtx]
        /// </summary>
        /// <returns>Danh sách</returns>
        List<string> GetRepositoryCode();

        /// <summary>
        /// Lấy ra danh sách Repository theo IsActive
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>Tập các Repository, null nếu bị lỗi</returns>
        List<Repository> GetAll_ByIsActive(bool IsActive);

        /// <summary>
        /// Lấy ra danh sách Repository theo IsActive và sắp xếp
        /// [haipt]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>Tập các Repository, null nếu bị lỗi</returns>
        List<Repository> GetAll_ByOrderbyIsActive(bool IsActive);

        List<Repository> GetOrderby();
        /// <summary>
        /// Lấy ra danh sách RepositoryCode từ Repository theo IsActive == true
        /// [Longtx]
        /// </summary>
        /// <returns>Tập các Repository có isActive==true</returns>
        List<Repository> GetByRepositoryCode(bool isActive);

        /// <summary>
        /// Lấy ra Repository theo ID
        /// [Haipt]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<Repository> GetRepositoryById(Guid? id);
        List<Repository> GetAll_OrderBy();

        /// <summary>
        /// Lấy ra ID của kho
        /// </summary>
        /// <param name="code">Mã kho</param>
        /// <returns></returns>
        Guid? GetGuidRespositoryByCode(string code);
        string GetRespositoryCodeByGuid(Guid? guid);
    }
}
