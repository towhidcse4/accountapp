using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ISABillService: FX.Data.IBaseService<SABill ,Guid>
    {
        int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
        List<SABill> GetListSABillTT153(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
    }
}
