﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentService : FX.Data.IBaseService<MCPayment, Guid>
    {
        MCPayment GetByNo(string no);
        /// <summary>
        /// Lấy danh sách Phiếu chi theo năm của ngày ghi sổ và sắp xếp thứ tự giảm dần theo ngày ghi sổ [khanhtq]
        /// </summary>
        /// <param name="postedYear">Năm ghi sổ</param>
        /// <returns>Danh sách phiếu chi</returns>
        List<MCPayment> OrderByPostedDate(int postedYear);
        List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
        MCPayment findByAuditID(Guid ID);
    }
}
