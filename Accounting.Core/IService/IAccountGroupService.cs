using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IAccountGroupService : FX.Data.IBaseService<AccountGroup, string>
    {
        List<AccountGroup> GetAll_Orderby();
    }
}
