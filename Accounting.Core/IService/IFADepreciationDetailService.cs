﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
     public interface IFADepreciationDetailService : FX.Data.IBaseService<FADepreciationDetail, Guid>
    {
         List<FADepreciationDetail> GetFADepreciationDetails(Guid faDepreciationId);

        List<FADepreciationDetail> FindByFixedAssetID(Guid? FixedAssetID);
    }

}
