﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IAppUserService : FX.Data.IBaseService<AppUser, Guid>
    {
        AuthenticatedUser CheckUserAuthentication(string userName, string password);
    }
}
