﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Accounting.Core.Domain;
using NHibernate;

namespace Accounting.Core.IService
{
    public interface IFixedAssetService:FX.Data.IBaseService<FixedAsset, Guid>
    {
        ISession GetSessionState();
        /// <summary>
        /// Đếm danh sách FixedAsset có cùng FixedAssetCode
        /// </summary>
        /// <param name="fixedAssetCode"></param>
        /// <returns></returns>
        int CheckExistFA(string fixedAssetCode, Guid id);

        List<FixedAsset> GetTimeUsed(Guid fixedAssetID);

        List<FixedAsset> GetAll_OrderBy();

        List<VoucherFixedAsset> GetListVoucherFixedAssets(Guid fixedAssetId);
        List<FixedAsset> GetAll_OrderByFixedAssetCode();
        List<FixedAsset> GetFAForDepreciation(DateTime postedDate);
        List<Guid> GetFAIDForDepreciation(DateTime postedDate);

        /// <summary>
        /// Lấy ra danh sách fixedasset của danh mục kết hợp với bảng khai báo
        /// </summary>
        /// <returns></returns>
        IList<VoucherFixedAsset> FindAllFAUnionInit();
        Guid? GetGuidByFixedAssetCode(string ma);
        /// <summary>
        /// Lấy ra danh sách fixedasset theo no
        /// </summary>
        /// <returns></returns>
        FixedAsset GetFixedAssetByCode(string ma);
        List<FixedAsset> FindByNoIsNull(bool isNull);

        /// <summary>
        ///  Lấy ra List các tài sản khai mới ở danh mục mà chưa ghi tăng lần nào
        /// </summary>
        /// <returns></returns>
        List<Guid> GetFixedAssetWithoutIncrement(string id);

        /// <summary>
        ///  Lấy ra List các tài sản khai mới ở danh mục mà đã ghi tăng chưa ghi giảm lần nào và ở khai báo và chưa ghi giảm
        /// </summary>
        /// <returns></returns>
        List<Guid> GetFixedAssetWithoutDecrement(string id, DateTime PostedDate);

        /// <summary>
        ///  Lấy ra những TSCĐ đã ghi giảm hoặc chưa ghi sổ
        /// </summary>
        /// <returns></returns>
        List<Guid> GetFixedAssetWithoutDecrementAndLedger(string id);

        /// <summary>
        /// Nếu là ghi tăng tài sản cố định, kiểm tra xem đã giảm, tính khấu hao, điều chuyển, điều chỉnh chưa
        /// Nếu đã thực hiện 1 trong những tác vụ trên thông báo :
        /// "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!"
        /// </summary>
        /// <returns></returns>
        bool CheckDeleteIncrement(List<Guid?> fixedassetid);

        /// <summary>
        /// Nếu là danh mục TSCĐ, kiểm tra xem đã ghi tăng chưa
        /// Nếu đã ghi tăng, thông báo
        /// "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!"
        /// /// </summary>
        /// <returns></returns>
        bool CheckDeleteFixedAsset(Guid fixedassetid);
    }
}
