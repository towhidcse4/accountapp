﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.AllOPAccount;

namespace Accounting.Core.IService
{
    public interface IOPAccountService : FX.Data.IBaseService<OPAccount, Guid>
    {
        IEnumerable<AllOpAccount> GetAllOpAccounts(DateTime startDate);
        /// <summary>
        /// Lấy danh sách tất cả các tài khoản(có thể chưa có hoặc có số dư đầu kỳ)
        /// </summary>
        /// <returns></returns>
        List<OpAccountDetailByOrther> GetListOpAccountDetailByOrthers();
        /// <summary>
        /// Lấy ra danh sácha các tài khoản có số dư đầu kỳ
        /// </summary>
        /// <returns></returns>
        List<OpAccountDetailByOrther> GetListOpAccountDetailByOrthers_NotDefaultIfEmpty();

        /// <summary>
        /// Chuyển danh sách số dư đầu kỳ tài khoản và lưu vào CSDL
        /// </summary>
        /// <param name="lstOpAccount">danh sách số dư đầu kỳ theo tài khoản</param>
        void SaveOpAccountAndGeneralLedger(IEnumerable<OpAccountDetailByOrther> lstOpAccount);
        void SaveOpAccountAndGeneralLedger(IEnumerable<OpAccountObjectByOrther> lstOpAccount);

        List<OpAccountObjectByOrther> GetListOpAccountObjectByOrthers(Account account);
        List<OpAccountObjectByOrther> GetListOpAccountObjectByOrthers_NotDefaultIfEmpty();

        /// <summary>
        /// Lấy ra OPAccount theo account number
        /// </summary>
        /// <returns></returns>
        OPAccount FindByAccountNumber(string originalPriceAccount);

        /// <summary>
        /// Lấy ra order priority cuối cùng
        /// </summary>
        /// <returns></returns>
        int FindLastPriority();
    }
}
