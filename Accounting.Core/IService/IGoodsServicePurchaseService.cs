﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IGoodsServicePurchaseService : FX.Data.IBaseService<GoodsServicePurchase, Guid>
    {
        List<string> GetListGoodServicePurchaseCode();

        List<GoodsServicePurchase> GetAll_OrderBy();
    }
}
