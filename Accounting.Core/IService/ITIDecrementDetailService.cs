﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITIDecrementDetailService :FX.Data.IBaseService<TIDecrementDetail,Guid>
    {
        List<TIDecrementDetail> GetByTIDecrementDetailID(Guid TIDecrementID);
        
        decimal GetNotDecrementQuantity(Guid iD, decimal quantity, DateTime postedDate);
        decimal GetRemainingAmount(Guid? materialGoodsID, DateTime postedDate);
    }
}
