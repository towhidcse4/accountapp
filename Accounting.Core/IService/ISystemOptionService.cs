using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ISystemOptionService:FX.Data.IBaseService<SystemOption ,int>
   {
       SystemOption GetByCode(string code);
   }
}
