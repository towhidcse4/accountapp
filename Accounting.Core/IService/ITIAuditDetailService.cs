using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface ITIAuditDetailService : FX.Data.IBaseService<TIAuditDetail, Guid>
    {
        IList<TIAuditDetail> FindAllFaInitAndIncrementTools(DateTime inventoryDate);
    }
}
