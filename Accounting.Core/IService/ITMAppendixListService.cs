using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITMAppendixListService: FX.Data.IBaseService<TMAppendixList ,Guid>
    {
        List<TMAppendixList> GetAppendixListByTypeGroup(int typeGroup);
    }
}
