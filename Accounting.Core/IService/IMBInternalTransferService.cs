﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IMBInternalTransferService : FX.Data.IBaseService<MBInternalTransfer, Guid>
    {
        /// <summary>
        /// Lấy ra đối tượng MBInternalTransfer theo No
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        MBInternalTransfer GetByNo(string input);

        MBInternalTransfer GetById(Guid input);
    }
}
