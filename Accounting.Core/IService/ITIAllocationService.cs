﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITIAllocationService :FX.Data.IBaseService<TIAllocation,Guid>
    {
        List<TIAllocation> GetAll_OrderBy();
        TIAllocation GetByNo(string no);
        decimal GetDepreciationByMonth(DateTime from, DateTime to, Guid MaterialGoodsID);
        decimal GetRemainingAmount(Guid MaterialGoodsID, DateTime dp, decimal? Unitprice, decimal numberOfTools);
        decimal GetRemaningAllocationTimes(Guid iD, DateTime dp, decimal allocationTime);
    }
}
