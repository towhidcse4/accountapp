﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.OverdueInterest;
using FX.Data;
using FX.Core;
using System.Linq;
using NHibernate;

namespace Accounting.Core.IService
{
    public interface ISAInvoiceService : FX.Data.IBaseService<SAInvoice, Guid>
    {
        /// <summary>
        /// Lấy danh sách chứng từ bán hàng
        /// </summary>
        /// <param name="year">năm hoạch toán (dữ liệu lọc theo năm hoạch toán)</param>
        /// <param name="dsTypeID">loại chứng từ bán hàng (chưa thu tiền - 320   |    thu tiền ngay - 321,322    |   ......)</param>
        /// <returns>danh sách chứng từ bán hàng</returns>
        List<SAInvoice> GetSaInvoices(int year, List<int> dsTypeID);
        ISession GetSession();
        bool CreateNew(SAInvoice newSaInvoice, IList<SAInvoiceDetail> saInvoiceDetails, out string errMessage);
        /// <summary>
        /// Lấy ra danh sách các SAInvoice theo AccountingObjectID và PostedDate
        /// [DUYTN]
        /// </summary>
        /// <param name="AccountingObjectID">là AccountingObjectID truyền vào cần tìm</param>
        /// <param name="PostedDate">là PostedDate truyền vào cần lấy(chú ý truyền vào dạng datetime)</param>
        /// <returns>tập các , null nếu bị lỗi</returns>
        List<SAInvoice> GetAll_ByAccountingObjectID_AndPostedDate(Guid AccountingObjectID, DateTime PostedDate);

        /// <summary>
        /// Lấy ra danh sách các SAInvoice theo TypeID
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là tham số TypeID truyền vào</param>
        /// <returns>tập các SAInvoice, null nếu bị lỗi</returns>
        List<SAInvoice> GetAll_ByTypeID(int TypeID);

        /// <summary>
        /// danh sách các hóa đơn_SAInvoice theo TypeID của [đối tượng hiện tại] và có [ngày hoạch toán] nhỏ hơn [ngày hiện tại]
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là TypeID truyền vào</param>
        /// <param name="DateTime">là Ngày tính lãi nợ</param>
        /// <param name="AccountingObjectID">là AccountingObjectID của đối tượng AccountingObject cần tìm</param>
        /// <returns></returns>
        List<SAInvoice> GetListInVoid(int TypeID, DateTime DateTime, Guid AccountingObjectID);

        /// <summary>
        /// danh sách các khách hàng có nợ bị tính lãi
        /// [PhongNT]
        /// </summary>
        /// <param name="Guid">AccountingObjectID</param>
        /// <param name="AccountNumber"></param>
        /// <param name="Datetime">ngày tính lãi</param>
        /// <returns></returns>
        List<OverdueInterest> GetOverdueInterest(Guid AccountingObjectID, string AccountNumber, DateTime ngayTinhLai);

        /// <summary>
        /// Sổ chi tiết bán hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="materialGoodsIDs">danh sách mã tài sản cố định</param>
        /// <returns></returns>
        List<S17DNN> ReportS35DN(DateTime fromDate, DateTime toDate, IEnumerable<Guid> materialGoodsIDs);

        /// <summary>
        /// Sổ nhật ký bán hàng
        /// [DuyNT]
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <returns></returns>
        List<S03A4DNN> ReportS03A4Dns(DateTime fromDate, DateTime toDate);

        /// <summary>
        /// Hàm tính lãi nợ
        /// [DuyNT]
        /// </summary>
        /// <param name="lstAccountingObjects">danh sách các nhà cung cấp cần tính lãi nợ</param>
        /// <param name="lstAccounts">danh sách các tài khoản phải thu</param>
        /// <param name="dtNgayTinhLai">ngày tính lãi</param>
        /// <returns></returns>
        List<OverdueInterest> GetOverdueInterests(List<AccountingObject> lstAccountingObjects,
            IEnumerable<Account> lstAccounts, DateTime dtNgayTinhLai);

        /// <summary>
        /// Thông báo công nợ phải trả
        /// [DuyNT]
        /// </summary>
        /// <param name="dtDenNgay">Ngày tính công nợ</param>
        /// <param name="ngayThongBao"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        List<ListLibitiesReport> GetListLibitiesReport(DateTime dtDenNgay, DateTime ngayThongBao, Currency currency);

        // Tính doanh số module giá thành
        List<SAInvoiceDetail> DoanhSoHD(List<Guid> ID, DateTime fromDate, DateTime toDate);
        List<SAInvoiceDetail> DoanhSoCTDH(List<Guid> ID, DateTime fromDate, DateTime toDate);
        decimal DoanhSo(Guid ID, DateTime fromDate, DateTime toDate);
        decimal DoanhSoHD(Guid ID, DateTime fromDate, DateTime toDate);
        int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
        List<SAInvoice> GetListSAInVoiceTT153(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
        List<SAInvoice> GetListSAInVoiceTT153inPeriod(DateTime From, DateTime To, Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate);
    }
}
