using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentDetailImportVATService : FX.Data.IBaseService<MCPaymentDetailImportVAT, Guid>
    {
    }
}
