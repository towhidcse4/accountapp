﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ICostSetService : FX.Data.IBaseService<CostSet, Guid>
    {
        /// <summary>
        /// Lấy danh sách chuỗi OrderFixCode của các đối tượng con có cùng cha
        /// </summary>
        /// <param name="costSet">Đối tượng CostSet truyền vào</param>
        /// <returns>Danh sách các OrderFixCode</returns>
        List<string> GetListOrderFixCodeChild(CostSet costSet);

        /// <summary>
        /// Lấy danh sách các đối tượng CostSet có cùng chung OrderFixCode
        /// </summary>
        /// <param name="orderFixCode">Chuỗi OrderFixCode truyền vào</param>
        /// <returns>Danh sách các CostSet</returns>
        List<CostSet> GetListCostSetOrderFixCode(string orderFixCode);
        
        /// <summary>
        /// Lấy ra danh sách các đối tượng CostSet có chung parentID
        /// </summary>
        /// <param name="parentID">ID của đối tượng cha</param>
        /// <returns>Danh sách các CostSet</returns>
        List<CostSet> GetListCostSetParentID(Guid? parentID);

        /// <summary>
        /// Lấy danh sách các CostSet theo bậc truyền vào
        /// </summary>
        /// <param name="grade">bậc của đối tượng</param>
        /// <returns>Danh sách các CostSet</returns>
        List<CostSet> GetCostSetGrade(int grade); 

        /// <summary>
        /// Đếm số lượng con của một đối tượng CostSet
        /// </summary>
        /// <param name="parentID">ID của đối tượng Cha</param>
        /// <returns>Số lượng con của một đối tượng</returns>
        int CountChildCostSet(Guid? parentID);

        /// <summary>
        /// Lấy ra danh sách CostSet theo IsActive
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<CostSet> GetListCostSetIsActive(bool isActive);

        /// <summary>
        /// Lấy danh sách CostSetCode từ GetCostSetCodeBool khi IsActive =true
        /// [Longtx]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<CostSet> GetCostSetCodeBool(bool isActive);

        
        /// <summary>
        /// Lấy danh sách các CostSet theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách CostSet được sắp xếp theo dạng cây cha con</returns>
        List<CostSet> GetByActive_OrderByTreeIsParentNode(bool isActive);

        /// <summary>
        /// Lấy danh sách các CostSet và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách CostSet được sắp xếp theo dạng cây cha con</returns>
        List<CostSet> GetAll_OrderByTreeIsParentNode();
        List<CostSet> GetAll_OrderBy();
        Guid? GetGuidCostSetByCode(string code);
        List<SelectCostSet> GetListByType(int costsetType);
    }
}
