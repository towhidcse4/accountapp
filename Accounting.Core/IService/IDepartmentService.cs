﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IDepartmentService : FX.Data.IBaseService<Department, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách Deparment có cùng cha
        /// </summary>
        /// <param name="parentID">ID của Department đối tượng cha</param>
        /// <returns>Danh sách Department con</returns>
        List<Department> GetListDepartmentParentID(Guid? parentID);

        /// <summary>
        /// Lấy danh sách Department
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<Department> GetListDepartmentIsActive(bool isActive);

        /// <summary>
        /// Lấy danh sách DepartmentCode từ Department theo IsActive = true
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<Department> GetListDepartmentBool(bool isActive);

        /// <summary>
        /// Lấy ra danh sách Department sắp xếp DepartmentCode
        /// </summary>
        /// <returns></returns>
        List<Department> GetListDeparmentOrderDepartmentCode();
        Guid? GetGuidBydepartmentCode(string ma);// lấy guid qua departmentCode
        /// <summary>
        /// Đếm danh sách Department theo parentId
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        int CountListDepartmentParentID(Guid? parentID);

        /// <summary>
        /// Lấy Danh sách OrderFixCode theo ParentId
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<string> GetListOrderFixCodeParentID(Guid? parentID);

        /// <summary>
        /// Lấy danh sách Department theo OrderFixCode
        /// </summary>
        /// <param name="orderFixCode"></param>
        /// <returns></returns>
        List<Department> GetListDepartmentOrderFixCode(string orderFixCode);

        /// <summary>
        /// Lấy ra danh sách Department theo grade
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        List<Department> GetListDepartmentGrade(int grade);

        /// <summary>
        /// Lấy danh sách Department theo OrderFixCode bỏ qua đối tượng cha
        /// </summary>
        /// <param name="orderFixCode"></param>
        /// <returns></returns>
        List<Department> GetListDepartmentOrderFixCodeNotParent(string orderFixCode, Guid? parentID);

        /// <summary>
        /// Lấy ra danh sách DepartmentCode
        /// </summary>
        /// <returns></returns>
        List<string> GetListDepartmentCode();

        /// <summary>
        /// Lấy danh sách các Department theo IsActive được sắp xếp tăng dần theo DepartmentCode
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>tập các Department, null nếu bị lỗi</returns>
        List<Department> GetAll_ByIsActive_OrderByDepartmentCode(bool IsActive);

        /// <summary>
        /// Lấy danh sách các Department theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách Department được sắp xếp theo dạng cây cha con</returns>
        List<Department> GetByActive_OrderByTreeIsParentNode(bool isActive);

        /// <summary>
        /// Lấy danh sách các Department và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách Department được sắp xếp theo dạng cây cha con</returns>
        List<Department> GetAll_OrderByTreeIsParentNode();

        /// <summary>
        /// Lấy ra ID của phòng ban
        /// </summary>
        /// <param name="code">Mã kho</param>
        /// <returns></returns>
        Guid? GetGuidDepartmentByCode(string code);
        string GetDepartmentCodeByGuid(Guid? guid);
        List<Department> GetListParent();
        List<Objects> GetObject();
    }
}
