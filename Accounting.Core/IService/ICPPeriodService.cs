using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPPeriodService: FX.Data.IBaseService<CPPeriod ,Guid>
    {
        List<CPCostingPeriod> GetCPCostingPeriod();
        List<CostPeriod> GetAllCostPeriod(int type);
        decimal AccumulatedAllocateAmount(List<Guid> lstID, List<CPPeriod> LstCPPeriod, List<CPOPN> LstOPN, int type);
        List<RCost> GetListRCost(List<CostSet> lst, Guid ID);
        List<RCost> GetListRCost(List<EMContract> lst, Guid ID);
    }
}
