using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface ISysRoleService : FX.Data.IBaseService<SysRole, Guid>
    {
        List<SysRole> GetByRoleCode();
        List<Guid> GetRoleByUserID(Guid userID);
    }
}
