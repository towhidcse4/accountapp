using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IPPDiscountReturnService : FX.Data.IBaseService<PPDiscountReturn, Guid>
    {
        PPDiscountReturn GetPPDiscountReturnbyNo(string no);
        int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
        List<PPDiscountReturn> GetListPPDiscountReturnTT153(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
    }
}
