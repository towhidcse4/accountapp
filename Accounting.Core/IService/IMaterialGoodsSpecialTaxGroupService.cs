﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IMaterialGoodsSpecialTaxGroupService : FX.Data.IBaseService<MaterialGoodsSpecialTaxGroup, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theo IsActive
        /// [DUYTN]
        /// </summary>
        /// <param name="IsActive">là IsActive truyền vào</param>
        /// <returns>Tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        List<MaterialGoodsSpecialTaxGroup> GetAll_ByIsActive(bool IsActive);

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID"> là ParentID truyền vào</param>
        /// <returns>tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        List<MaterialGoodsSpecialTaxGroup> GetAll_ByParentID(Guid ParentID);

        /// <summary>
        /// Số dòng theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>số dòng theo ParentID, null nếu lỗi</returns>
        int CountByParentID(Guid? ParentID);

        /// <summary>
        /// Lấy ra danh sách OrderFixCod theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các OrderFixCod, null nếu bị lỗi</returns>
        List<string> GetOrderFixCode_ByParentID(Guid? ParentID);

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theo OrderFixCode (các con cuối cùng)
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode"> là OrderFixCode truyền vào</param>
        /// <returns>tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        List<MaterialGoodsSpecialTaxGroup> GetStartsWith_ByOrderFixCode(string OrderFixCode);

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroup theo Grade
        /// [DUYTN]
        /// </summary>
        /// <param name="Grade"> là tham số bậc của Grade</param>
        /// <returns>tập các MaterialGoodsSpecialTaxGroup, null nếu bị lỗi</returns>
        List<MaterialGoodsSpecialTaxGroup> GetAll_ByGrade(int Grade);

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsSpecialTaxGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các MaterialGoodsSpecialTaxGroupCode, null nếu bị lỗi</returns>
        List<string> GetMaterialGoodsSpecialTaxGroupCode();
        string GetMaterialGoodsSpecialTaxGroupCodeByGuid(Guid? guid);
        List<MaterialGoodsSpecialTaxGroup> GetAll_OrderBy();
        Guid GetGuidMaterialGoodsSpecialTaxGroupByCode(string code);
    }
}
