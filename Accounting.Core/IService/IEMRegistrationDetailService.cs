﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMRegistrationDetailService : FX.Data.IBaseService<EMRegistrationDetail, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách binding EMRegistrationDetail theo ID
        /// [haipt]
        /// </summary>
        /// <param name="inputID"></param>
        /// <returns></returns>
        BindingList<EMRegistrationDetail> GetBinddingEMRegistrationDetailID(Guid? inputID);
    }
}
