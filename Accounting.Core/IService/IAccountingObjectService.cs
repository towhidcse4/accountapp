﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IAccountingObjectService : FX.Data.IBaseService<AccountingObject, Guid>
    {
        /// <summary>
        /// Lấy ra AccountingObject the mã
        /// </summary>
        /// <returns></returns>
        AccountingObject GetAccountingObjectByCode(string accountingObjectCode);

        /// <summary>
        /// Lấy ra AccountingObject theo ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        AccountingObject GetAccountingObjectById(Guid ID);

        /// <summary>
        /// Lấy ra danh sách tất cả AccountingObject và sắp xếp theo mã
        /// </summary>
        /// <returns></returns>
        List<AccountingObject> GetListAccountingObjectOrderCode();

        /// <summary>
        /// Lấy danh sách AccountingObject theo IsActive
        /// isActive = true: Lấy ra danh sách hoạt động
        /// isActive = false: lấy ra danh sách ngừng hoạt động
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<AccountingObject> GetListAccoungingObjectByIsActive(bool isActive);

        /// <summary>
        /// Lấy danh sách AccountingObject theo IsActive và sắp xếp theo Code
        /// isActive = true: Lấy ra danh sách hoạt động
        /// isActive = false: lấy ra danh sách ngừng hoạt động
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<AccountingObject> GetListAccoungingObjectByIsActiveOrderCode(bool isActive);

        /// <summary>
        /// Danh sach cac AccountingObjectCode
        /// </summary>
        /// <returns>Tra ve danh sach chuoi du lieu</returns>
        List<string> GetListAccountingObjectCode();

        /// <summary>
        /// Lấy danh sách các đối tượng nhân viên, khách hàng, nhà cung cấp
        /// [DUYTN]
        /// </summary>
        /// <param name="select">0: khách hàng, 1: nhà cung cấp, 2: nhân viên</param>
        /// <param name="isActive">là IsActive =true có hoạt động, false không hoạt động</param>
        /// <returns>tập các AccountingObject, null nếu bị lỗi</returns>
        List<AccountingObject> GetAccountingObjects(int select, Boolean isActive = true);
        List<AccountingObject> GetAccountingObjectsByType(int select);

        List<AccountingObject> GetAccountingObjectsGetInvoice(Boolean isActive = true);

        /// <summary>
        /// lấy khách hàng có phát sinh theo mã khách hàng: hàm tổng quát [PhongNT]
        /// </summary>
        /// <param name="customerCode">Mã khách hàng</param>
        /// <returns></returns>
        List<CustomerDebit> GetCustDrByCode(string customerCode);


        /// <summary>
        /// Lấy danh sách nhà cung cấp cần trả tiền và ngày hạch toán
        /// </summary>
        /// <param name="accountingDate">ngày hạch toán</param>
        /// <returns></returns>
        List<PayVendor> GetListPayVendor(DateTime accountingDate, DateTime? beginDate, DateTime? endDate);

        /// <summary>
        /// Lấy ra danh sách đối tượng kế toán theo isEmployee
        /// isEmployee = true: lấy ra nhân viên
        /// isEmployee = false: lấy ra các đối tượng không phải nhân viên
        /// </summary>
        /// <returns></returns>
        List<AccountingObject> GetListAccountingObjectByEmployee(bool isEmployee);

        /// <summary>
        /// Danh sách các AccountingObject theo danh sách ObjectType
        /// [DUYTN]
        /// </summary>
        /// <param name="objectType">là danh sách các ObjectType truyền vào (0, 1, 2 , ...)</param>
        /// <returns>tập các AccountingObject, null nếu bị lỗi</returns>
        List<AccountingObject> GetAll_ByListObjectType(List<int> objectType);

        /// <summary>
        /// Lấy danh sách khách hàng phải trả tiền theo ngày hạch toán
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        List<CollectionCustomer> GetListCustomerDebits(DateTime startDate, DateTime? beginDate, DateTime? endDate);

        List<ListLibitiesReport> ListLibitiesReport(DateTime startDate, DateTime dtDenNgay, Currency currency);

        GOtherVoucher HandlingExchangeRate(DateTime toDate, SolveExchangeRate solveExchangeRate);

        /// <summary>
        /// Lấy ra ID của đối tượng kế toán
        /// </summary>
        /// <param name="code">Mã đối tượng kế toán</param>
        /// <returns></returns>
        Guid? GetGuidAccountingObjectByCode(string code);

        /// <summary>
        /// Lấy ra danh sách
        /// </summary>
        /// <returns></returns>
        List<VoucherAccountingObject> GetListVoucherAccoungtingObjectView();

        List<AccountingObject> GetListAccountingObjectOrderCodeByDepartment(Guid? id);
    }
}
