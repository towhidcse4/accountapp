﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
namespace Accounting.Core.IService
{
    public interface ISaleDiscountPolicyService : IBaseService<SaleDiscountPolicy, Guid>
    {
        /// <summary>
        /// Lấy danh sách SaleDiscountPolicy theo MaterialGoodsID
        /// [DUYTN]
        /// </summary>
        /// <param name="MaterialGoodsID"> là MaterialGoodsID cần truyền vào</param>
        /// <returns>tập các SaleDiscountPolicy, null nếu bị lỗi</returns>
        List<SaleDiscountPolicy> GetAll_ByMaterialGoodsID(Guid MaterialGoodsID);
    }
}
