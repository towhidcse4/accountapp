using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IOPToolsService : FX.Data.IBaseService<OPTools, Guid>
    {
        OPTools GetByInitID(Guid req);
        OPTools FindByMaterialGoodsID(Guid iD);
    }
}
