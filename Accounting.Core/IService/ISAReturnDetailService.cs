using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
   public interface ISAReturnDetailService:FX.Data.IBaseService<SAReturnDetail ,Guid>
    {
        decimal GetSAReturnDetailQuantity(Guid materialGoodsID, Guid contractID);
    }
}
