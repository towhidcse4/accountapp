﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IGVoucherListService : FX.Data.IBaseService<GVoucherList, Guid>
    {
        List<GVoucherList> GetAll_OrderBy();
    }
}
