﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IInvestorService : FX.Data.IBaseService<Investor, Guid>
    {
        /// <summary>
        /// Lấy tất cả danh sách mã nhà đầu tư [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<string> GetCode();
        List<Investor> GetAll_OrderBy();
    }
}
