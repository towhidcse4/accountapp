﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IGOtherVoucherDetailService : FX.Data.IBaseService<GOtherVoucherDetail, Guid>
    {
        List<GOtherVoucherDetail> GetByGOtherVoucherID(Guid ID);
    }
}
