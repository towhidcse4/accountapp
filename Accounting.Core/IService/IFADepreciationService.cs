﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFADepreciationService :FX.Data.IBaseService<FADepreciation,Guid>
    {
        List<FADepreciation> GetAll_OrderBy();
        FADepreciation GetByNo(string no);
        decimal GetDepreciationByMonth(DateTime from, DateTime to, Guid fixedAssetId);
    }
}
