using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
   public interface ISAReturnDetailCustomerService:FX.Data.IBaseService<SAReturnDetailCustomer ,Guid>
    {
    }
}
