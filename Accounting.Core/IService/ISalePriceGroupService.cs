﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ISalePriceGroupService : FX.Data.IBaseService<SalePriceGroup, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách tất cả các SalePriceGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>Danh sách SalePriceGroupCode dạng chuỗi, null nếu bị lỗi</returns>
        List<string> GetSalePriceGroupCode();

        string GetCodeSalePriceGroupById(Guid Id);

        SalePriceGroup GetSalePriceGroupByCode(string code);
        List<SalePriceGroup> GetAll_OrderBy();
    }
}
