using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using FX.Data;

namespace Accounting.Core.IService
{
    public interface IEMContractDetailMGService : IBaseService<EMContractDetailMG, Guid>
    {
        List<EMContractDetailMG> GetEMContractDetailMGByContractID(Guid contractID);
        EMContractDetailMG GetEMContractDetailMGByContractIDandMID(Guid contractID, Guid materialID);
        EMContractDetailMG GetByID(Guid ID);
    }
}
