using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPExpenseListService: FX.Data.IBaseService<CPExpenseList ,Guid>
    {
        List<CPExpenseList> GetListByTypeVoucher(int typeVoucher);
    }
}
