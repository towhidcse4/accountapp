﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IBankAccountDetailService : FX.Data.IBaseService<BankAccountDetail, Guid>
    {
        /// <summary>
        /// Lấy ra ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        BankAccountDetail GetById(Guid ID);

        /// <summary>
        /// Lấy ra danh sách chuỗi BankAccount
        /// </summary>
        /// <returns></returns>
        List<string> GetListBankAccount();

        List<BankAccountDetail> GetIsActive(bool isActive);

        List<BankAccountDetail> GetAllOrderBy();

        Guid? GetGuildFromBankAccount(string bankAccount);


    }
}
