﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITIIncrementDetailService : FX.Data.IBaseService<TIIncrementDetail, Guid>
    {
        /// <summary>
        /// Lấy ra CCDC ghi tăng theo id
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        TIIncrementDetail FindByMaterialGoodsID(Guid iD);

        /// <summary>
        /// kiểm tra CCDC đã ghi tăng chưa, nếu đã ghi tăng thì không cho xóa
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        bool CheckDeleteTools(Guid iD);

        /// <summary>
        /// Kiêm tra số lượng vượt quá số lượng khai báo không
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        bool CheckQuantity(Guid? toolsID, decimal quantity);

        /// <summary>
        /// Lấy ra số lượng ccdc chưa ghi tăng
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        decimal GetNotRecoredQuantity(Guid? toolsID);
    }
}
