﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface IViewVouchersCloseBookService : FX.Data.IBaseService<ViewVouchersCloseBook, Guid>
    {
        List<ViewVouchersCloseBook> GetAllVoucherUnRecorded(DateTime newDBDateClosed, Guid? branchId = null);
        List<ViewVouchersCloseBook> GetAllVoucherAccountingObject();
        ViewVouchersCloseBook GetByNo(string No);
        bool CheckNoExist(string No);
    }
}
