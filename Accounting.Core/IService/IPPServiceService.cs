﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPServiceService:FX.Data.IBaseService<PPService ,Guid>
   {
       PPService GetPPServicebyNo(string no);
        List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
        List<PPService> GetByBillReceived(bool billReceived, Guid? accountingObjectID, bool isRecorded,
                                         DateTime dtBegin, DateTime dtEnd);
    }
}
