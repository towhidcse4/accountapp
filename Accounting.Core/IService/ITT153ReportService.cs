using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153ReportService:FX.Data.IBaseService<TT153Report ,Guid>
    {
        TT153Report GetbySAInvoiceID(Guid SAInvoiceID);
    }
}
