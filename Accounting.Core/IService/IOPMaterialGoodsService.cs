﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj;

namespace Accounting.Core.IService
{
    public interface IOPMaterialGoodsService : FX.Data.IBaseService<OPMaterialGoods, Guid>
    {
        /// <summary>
        /// Lấy danh sách các vật tư hàng hóa có hoặc chưa có số dư đầu kỳ
        /// </summary>
        /// <returns></returns>
        List<OPMaterialGoodsByOrther> GetListOPMaterialGoodsByOrther(string accountNumber);

        /// <summary>
        /// Lấy ra danh sách vật tư hàng hóa có số dư đầu kỳ
        /// </summary>
        /// <returns></returns>
        List<OPMaterialGoodsByOrther> GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty();

        /// <summary>
        /// Lấy ra danh sách vật tư hàng hóa có số dư đầu kỳ theo tài khoản
        /// </summary>
        /// <returns></returns>
        List<OPMaterialGoodsByOrther> GetListOPMaterialGoodsByOrther_NotDefaultIfEmpty_ByAccountNumber(string accountNumber);

        void SaveOpAccountAndGeneralLedger(IEnumerable<OPMaterialGoodsByOrther> lst);
    }
}
