using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153LostInvoiceDetailService:FX.Data.IBaseService<TT153LostInvoiceDetail ,Guid>
    {
        List<TT153LostInvoiceDetail> GetAllBySAInvoiceID(Guid SAInvoiceID);
        List<TT153LostInvoiceDetail> GetAllByTT153ReportID(Guid TT153ReportID);
    }
}
