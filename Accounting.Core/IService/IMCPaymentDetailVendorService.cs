using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentDetailVendorService : FX.Data.IBaseService<MCPaymentDetailVendor, Guid>
    {
        List<MCPaymentDetailVendor> GetByMcPaymentId(Guid mCPaymentId);
    }
}
