﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IAccountTransferService : FX.Data.IBaseService<AccountTransfer, Guid>
    {
        /// <summary>
        /// Lấy danh sách FromAccount
        /// </summary>
        /// <returns>Danh sach From Tài khoản</returns>
        List<string> GetListFromAccountFull();

        /// <summary>
        /// Lấy danh sách ToAccount
        /// </summary>
        /// <returns>Danh sach To Tài khoản</returns>
        List<string> GetListToAccountFull();
        List<AccountTransfer> GetAll_OrderBy();
    }
}
