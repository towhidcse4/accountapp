﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IMBTellerPaperService:FX.Data.IBaseService<MBTellerPaper ,Guid>
    {
       /// <summary>
       /// Lấy Danh sách séc/ ủy nhiệm chi theo số chứng từ
       /// </summary>
       /// <param name="no">Số chứng từ</param>
       /// <returns></returns>
       MBTellerPaper GetByNo(string no);
        List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
    }
}
