using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface ITIInitDetailService : FX.Data.IBaseService<TIInitDetail, Guid>
    {
        List<TIInitDetail> GetByTIInitID(Guid req);
    }
}
