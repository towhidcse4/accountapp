using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IPSSalarySheetService : FX.Data.IBaseService<PSSalarySheet, Guid>
    {
        List<PSSalarySheet> GetByGOtherVoucherIDIsNull();
    }
}
