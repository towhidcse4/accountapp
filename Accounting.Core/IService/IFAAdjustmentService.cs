using System;
using System.Linq;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFAAdjustmentService : FX.Data.IBaseService<FAAdjustment, Guid>
    {

        FAAdjustment GetByNo(string no);


    }
}
