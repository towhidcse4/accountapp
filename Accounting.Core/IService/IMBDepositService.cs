using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IMBDepositService : FX.Data.IBaseService<MBDeposit, Guid>
    {
        MBDeposit GetByNo(string no);
    }
}
