using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IToolLedgerService : FX.Data.IBaseService<ToolLedger, Guid>
    {
        ToolLedger GrtByInitID(Guid req);

        List<ToolLedger> FindByMaterialGoodsID(Guid iD);
        List<ToolLedger> GetByListReferenceID(Guid temp);
        int CheckLedgerForDecrement(Guid ToolsID, DateTime postedDate, bool isCheckPostedDate);
        bool CheckLedgerForAdjustment(Guid guid, DateTime dateTime);
        bool CheckLedgerForDepreciation(Guid guid, DateTime dateTime);
        bool CheckLedgerForTransfer(Guid guid, DateTime dateTime, Guid? fromDepartment);
        List<S11DNN> RS11DNN(DateTime value1, DateTime value2, List<Guid> lstGuidDepartment, bool @checked);
        List<TIInit> FindAllotionTools(DateTime fromDate, DateTime toDate, List<Guid> lstID);
    }
}
