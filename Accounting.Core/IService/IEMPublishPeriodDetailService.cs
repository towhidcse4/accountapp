﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMPublishPeriodDetailService : FX.Data.IBaseService<EMPublishPeriodDetail, Guid>
    {
        /// <summary>
        /// Lấy ra một bindingList EMPublishPeriodDetail theo ID
        /// [haipt]
        /// </summary>
        /// <param name="inputID"></param>
        /// <returns></returns>
        BindingList<EMPublishPeriodDetail> GetBindingListEMPulishPeriodDetailID(Guid? inputID);

        /// <summary>
        /// Lấy ra danh sách EMPublishPeriodDetail theo Id
        /// [haipt]
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        List<EMPublishPeriodDetail> GetListEMPublishPeriodDetail(Guid? Id);

        /// <summary>
        /// Lấy ra EMPublishPeriod theo ID và StockCategoryID
        /// [haipt]
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="StockCategoryId"></param>
        /// <returns></returns>
        EMPublishPeriodDetail GetEMPublishPeriodDetailIDStockCategoryID(Guid? Id, Guid? StockCategoryId);
    }
}
