﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface ITIAdjustmentDetailService : FX.Data.IBaseService<TIAdjustmentDetail, Guid>
    {

        List<TIAdjustmentDetail> GetByTIAdjustmentID(Guid TIAdjustmentID);
    }
}
