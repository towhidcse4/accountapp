﻿using System.Collections.Generic;
using Accounting.Core.Domain;
using System;

namespace Accounting.Core.IService
{
    public interface IInvestorGroupService : FX.Data.IBaseService<InvestorGroup, Guid>
    {
        List<InvestorGroup> OrderByGroupName();
        List<InvestorGroup> GetListByOrderFixCode(string stringStartWith);
        List<InvestorGroup> GetListByParentID(Guid ParentID);
        List<InvestorGroup> GetListByGrade(int grade);
        /// <summary>
        /// Lấy ra danh sách sắp xếp
        /// </summary>
        /// <returns></returns>
        List<InvestorGroup> GetAll_OrderBy();
    }
}
