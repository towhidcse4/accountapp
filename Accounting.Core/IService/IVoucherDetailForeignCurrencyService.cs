using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IVoucherDetailForeignCurrencyService : FX.Data.IBaseService<VoucherDetailForeignCurrency, int>
    {
    }
}
