using System;
using Accounting.Core.Domain;
using System.Collections.Generic;


namespace Accounting.Core.IService
{
    public interface IMCReceiptDetailService : FX.Data.IBaseService<MCReceiptDetail, Guid>
    {
        List<MCReceiptDetail> GetBymCReceiptId(Guid mCReceiptId);
        List<MCReceiptDetail> GetMCReceiptDetailByContract(Guid contractID);
    }
}
