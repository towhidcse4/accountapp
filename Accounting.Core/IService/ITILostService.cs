﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITILostService : FX.Data.IBaseService<TILost, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách chuỗi FAIncrementCode
        /// </summary>
        /// <returns></returns>
        List<string> GetListTILostCode();
    }
}
