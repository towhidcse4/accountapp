﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IExpenseItemService : FX.Data.IBaseService<ExpenseItem, Guid>
    {
        /// <summary>
        /// Lấy danh sách ExpenseItem cùng cha
        /// [haipt]
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<ExpenseItem> GetListExpenseItemParentID(Guid? parentID);
        Guid? GetGuidByExpenseItemCode(string ma);//lấy guid khi truyền vào ExpenseItemCode
        string GetExpenseItemCodeByGuid(Guid? ma);
        ExpenseItem GetExpenseItemByGuid(Guid ma);
        /// <summary>
        /// Lấy ra danh sách chuỗi ExpenseItemCode
        /// [haipt]
        /// </summary>
        /// <returns></returns>
        List<string> GetListExpenseItemCode();

        /// <summary>
        /// Lấy ra danh sách ExpenseItem theo OrderFixCode
        /// [haipt]
        /// </summary>
        /// <param name="orderFixCode"></param>
        /// <returns></returns>
        List<ExpenseItem> GetListExpenseItemOrderFixCode(string orderFixCode);

        /// <summary>
        /// Lấy ra danh sách orderFixCode của các ExpenseItem cùng cha
        /// [haipt]
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<string> GetListOrderFixCodeExpenseItemChild(Guid? parentID);

        /// <summary>
        /// Lấy ra danh sách ExpenseItem theo bậc
        /// [haipt]
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        List<ExpenseItem> GetListExpenseItemGrade(int grade);
        /// <summary>
        /// Lấy ExpenseItemCode từ ExpenseItem theo isActive= true
        /// [Longtx]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>        
        List<ExpenseItem> GetExpenseItemCodeBool(bool isActive);

        /// <summary>
        /// Đếm danh sách ExpenseItem theo ParentID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        int CountListExpenseItemParentID(Guid? parentID);


        /// <summary>
        /// Lấy danh sách các ExpenseItem theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách ExpenseItem được sắp xếp theo dạng cây cha con</returns>
        List<ExpenseItem> GetByActive_OrderByTreeIsParentNode(bool isActive);

        /// <summary>
        /// Lấy danh sách các ExpenseItem và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách ExpenseItem được sắp xếp theo dạng cây cha con</returns>
        List<ExpenseItem> GetAll_OrderByTreeIsParentNode();
        List<ExpenseItem> GetAll_OrderBy();
        Guid? GetGuidExpenseItemByCode(string code);
        List<ExpenseItem> GetListParent();
        List<ExpenseItem> ReportExpenseItem(List<string> acc, List<ExpenseItemTongHopCP> lst, DateTime from, DateTime to);
    }
}
