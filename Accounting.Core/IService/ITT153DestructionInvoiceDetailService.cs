using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153DestructionInvoiceDetailService:FX.Data.IBaseService<TT153DestructionInvoiceDetail ,Guid>
    {
        List<TT153DestructionInvoiceDetail> GetAllByTT153ReportID(Guid TT153ReportID);
        List<TT153DestructionInvoiceDetail> GetAllBySAInvoiceID(Guid SAInvoiceID);
    }
}
