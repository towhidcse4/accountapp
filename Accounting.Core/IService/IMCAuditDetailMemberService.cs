﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCAuditDetailMemberService : FX.Data.IBaseService<MCAuditDetailMember, Guid>
    {

    }
}
