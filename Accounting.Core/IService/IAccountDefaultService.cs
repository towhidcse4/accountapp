﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IAccountDefaultService : FX.Data.IBaseService<AccountDefault, Guid>
    {
        //List<Account> GetAccountDefaultByTypeID(int typeID, string columnName);

        /// <summary>
        /// Lấy ra danh sách các AccountDefault theo TypeID
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là TypeID truyền vào</param>
        /// <returns>tập các AccountDefault, null nếu bị lỗi</returns>
        List<AccountDefault> GetAll_ByTypeID(int TypeID);
        AccountDefault GetAccountDefaultByTypeIDAndColumnNameAndPPType(int typeID, string columnName, bool? ppType);
        AccountDefault GetAccountDefaultByTypeIDAndColumnName(int typeID, string columnName);

        /// <summary>
        /// Lấy DS tài khoản ngầm định theo loại chứng từ có tùy biến 
        /// [Huy Anh]
        /// </summary>
        /// <param name="_dsAccountDefault">DS bảng AccountDefault</param>
        /// <param name="typeId">Loại chứng từ</param>
        /// <param name="accountName">Tên tài khoản</param>
        /// <param name="_dsAccount">DS bảng Account nếu có</param>
        /// <param name="isImportPurchase">Trạng thái mua hàng(Nhập khẩu/ Trong nước)</param>
        /// <returns></returns>
        List<Account> GetAccountDefaultByTypeId(int typeId, string accountName,
                                                IList<AccountDefault> _dsAccountDefault = null,
                                                IList<Account> _dsAccount = null, bool isImportPurchase = false);
        List<AccountDefault_Object> GetAll_Sort();
        string DefaultDebitAccount();
        string DefaultCreditAccount();
        string DefaultVatAccount();
        /// <summary>
        /// Lấy ra dictionary chứa các TK mặc định
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> DefaultAccount();
    }
}
