﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ITemplateColumnService : FX.Data.IBaseService<TemplateColumn, Guid>
    {
        /// <summary>
        /// các "cột" trong một "bảng"
        /// </summary>
        /// <param name="TableID"></param>
        /// <returns></returns>
        List<TemplateColumn> getTemplateColumnsByTable(Guid TableID);

        /// <summary>
        /// Lấy mẫu chuẩn của tab Detail theo loại chứng từ
        /// </summary>
        /// <param name="typeId">Loại chứng từ</param>
        /// <returns></returns>
        List<TemplateColumn> GetTemplateDetailDefaultByTypeId(int typeId);
    }
}
