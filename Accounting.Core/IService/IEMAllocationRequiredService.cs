﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMAllocationRequiredService : FX.Data.IBaseService<EMAllocationRequired, Guid>
    {
        List<BUAllocationAndUse> GetRPAandB(List<int> Months, int Year, Guid BudgetItemID);
    }
}
