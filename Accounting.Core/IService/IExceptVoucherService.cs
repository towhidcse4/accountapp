﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Confront;

namespace Accounting.Core.IService
{
    public interface IExceptVoucherService : FX.Data.IBaseService<ExceptVoucher, Guid>
    {
        List<ExceptVoucher> GetListExceptVoucher(Guid accountingObjectId, string debitAccount);
        List<RemoveExpectVoucher> RemovExceptVouchers(Guid accountingObjectId, string debitAccount);
    }

}
