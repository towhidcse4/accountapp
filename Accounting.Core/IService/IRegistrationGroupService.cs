﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRegistrationGroupService : FX.Data.IBaseService<RegistrationGroup, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách các Id các đối tượng ở lớp cha
        /// [longtx]
        /// </summary>
        /// <param name="parentId">Id cua parent</param>
        /// <returns>Danh sách</returns>
        List<RegistrationGroup> GetParentId(Guid? parentId);
        /// <summary>
        /// lấy danh sách các 
        /// [longtx]
        /// </summary>
        /// <param name="countparentID"></param>
        /// <returns></returns>
        int GetCountByParenId(Guid? countparentID);
        /// <summary>
        /// lấy danh sách con trong lớp cha mới
        /// [longtx]
        /// </summary>
        /// <param name="newParent"></param>
        /// <returns>Danh sách RegistrationGroup</returns>
        List<RegistrationGroup> GetNewParent(Guid newParent);
        /// <summary>
        /// Lấy danh sách RegistrationGroup có bậc thứ n
        /// [longtx]
        /// </summary>
        /// <param name="grade">số bậc</param>
        /// <returns>Danh sách RegistrationGroup</returns>
        List<RegistrationGroup> GetGarde(int grade);
        /// <summary>
        /// Lấy danh sách các lớp con của đối tượng RegistrationGroup
        /// Các lớp con Có Order Fixcode StartWith = OrderFixCode . Gồm cả đối tượng cha.
        /// [longtx]
        /// </summary>
        /// <param name="lstChild">Chuỗi OrderFixCode</param>
        /// <returns>Danh sách RegistrationGroup </returns>
        List<RegistrationGroup> GetOrderChild(string lstChild);
        /// <summary>
        /// Lấy ra  danh sách RegistrationGroup  by nhóm đăng ký 
        /// [longtx]
        /// </summary>
        /// <param name="txtGroupCode"></param>
        /// <returns></returns>
        List<RegistrationGroup> GetByRegistrationGroupsCode(string registrationGroupCode);


        /// <summary>
        /// Lấy danh sách string orderFixcode theo điều kiện ParentID
        /// [longtx]
        /// </summary>
        /// <param name="parentID">ID của parent</param>
        /// <returns>Danh sách </returns>
        List<string> GetParenIdOrderFixbyParenID(Guid? parentID);
        List<RegistrationGroup> GetAll_OrderBy();
    }
}
