using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IOPFixedAssetService : FX.Data.IBaseService<OPFixedAsset, Guid>
    {
        int FindLastOrderPriority();
        OPFixedAsset FindByFixedAssetID(Guid iD);
    }
}
