using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRSTransferDetailService : FX.Data.IBaseService<RSTransferDetail, Guid>
    {
        List<RSTransferDetail> GetByRSTransferID(Guid tempId);
    }
}
