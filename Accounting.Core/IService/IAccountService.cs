﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IAccountService : FX.Data.IBaseService<Account, Guid>
    {
        /// <summary>
        /// Lấy các tài khoản con của một Account (các con cuối cùng)
        /// </summary>
        /// <param name="account">Tài khoản cha cần lấy (lọc theo account.AccountNumber)</param>
        /// <returns>Tập các con, cháu, chắt của nó, null nếu bị lỗi</returns>
        List<Account> GetChildrenAccount(Account account);

        /// <summary>
        /// Lấy các tài khoản con của một Account (các con cuối cùng)
        /// </summary>
        /// <param name="strdsAccount">Danh sách tài khoản cha cần lấy. VD: "111,1112" (lọc theo account.AccountNumber)</param>
        /// <returns>Tập các con, cháu, chắt của nó, null nếu bị lỗi</returns>
        List<Account> GetChildrenAccount(string strdsAccount);

        /// <summary>
        /// Lấy đối tượng Account từ giá trị string accountNumber
        /// </summary>
        /// <param name="accountNumber">string accountNumber cần lấy</param>
        /// <returns>đối tượng Account</returns>
        Account GetByAccountNumber(string accountNumber);

        /// <summary>
        /// lấy danh sách các tài khoản Account từ chuỗi AcountNumber
        /// </summary>
        /// <param name="strdsAccount">chuỗi chứa các AccountNumber. VD: "111,331,3331"</param>
        /// <returns>tập các Account, null nếu bị lỗi</returns>
        List<Account> GetByListAcountName(string strdsAccount);

        /// <summary>
        /// lấy danh sách các tài khoản Account từ chuỗi AcountNumber
        /// </summary>
        /// <param name="strdsAccount">danh sách các AccountName</param>
        /// <returns>tập các Account, null nếu bị lỗi</returns>
        List<Account> GetByListAcountName(List<string> strdsAccount);

        /// <summary>
        /// Lấy đầy đủ danh sách Account và order by theo AccountNumber
        /// </summary>
        /// <returns>list danh sách Account</returns>
        List<Account> GetListOrderBy();

        /// <summary>
        /// Lấy danh sách children Account 
        /// </summary>
        /// <param name="parentID">Guid: idParent</param>
        /// <returns>Danh sách Account</returns>
        List<Account> GetListChildren(Guid parentID);

        /// <summary>
        /// Lấy số lượng Children Account
        /// </summary>
        /// <param name="parentID">Guid: IdParent</param>
        /// <returns>Số lượng Account</returns>
        Int32 GetCountChildren(Guid parentID);

        /// <summary>
        /// lấy danh sách Account với cùng bậc
        /// </summary>
        /// <param name="grade">bậc của tài khoản</param>
        /// <returns>danh sách</returns>
        List<Account> GetListChildren(int grade);


        /// <summary>
        /// lấy danh sách Account có AccountNumber bắt đầu bằng giá tri
        /// </summary>
        /// <param name="start">Giá trị bắt đầu chuỗi AccountNumber</param>
        /// <returns>Danh sách Account thỏa mãn</returns>
        List<Account> GetListChildrenStartWith(string start);

        /// <summary>
        /// Lấy toàn bộ danh sách các AccountNumber
        /// </summary>
        /// <returns>Danh sách string</returns>
        List<string> GetFullListAccountNumber();

        /// <summary>
        /// Lấy ra danh sách Account theo isActive
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<Account> GetListAccountIsActive(bool isActive);

        /// <summary>
        /// Lấy ra danh sách Account theo AccountNumber và IsActive
        /// [haipt]
        /// </summary>
        /// <param name="listAccountNumber"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<Account> GetListAccountAccountNumberIsActive(string listAccountNumber, bool isActive);

        /// <summary>
        /// Lấy ra danh sách Account theo AccountNumber và IsActive với lever theo type
        /// type 0 -> lấy lever 2 trở lên, type 1 -> lấy bỏ lever 1 nếu có lever > 2
        /// [hieudt]
        /// </summary>
        /// <param name="listAccountNumber"></param>
        /// <param name="isActive"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        List<Account> GetListAccountAccountNumberIsActiveWithUpperLever(string listAccountNumber, bool isActive, int type);

        /// <summary>
        /// Lấy đầy đủ danh sách Account Active và order by theo AccountNumber
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns>list danh sách Account</returns>
        List<Account> GetListActiveOrderBy(bool isActive);
        /// <summary>
        /// Lấy danh sách AccoutNumber từ Account theo IsActive= true
        /// [Longtx]
        /// </summary>
        /// <param name="IsActive">AccoutNumber</param>
        /// <returns></returns>
        List<Account> GetAcountNumberBool(bool IsActive);

        /// <summary>
        /// Lấy danh sách tài khoản theo điều kiện là khách hàng
        /// [PhongNT]
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="detailType"></param>
        /// <returns></returns>
        List<Account> GetCustomerAcctList(bool isActive, string detailType);
        /// <summary>
        /// Lấy danh sách tài khoản theo loại đối tượng kế toán [khanhtq]
        /// </summary>
        /// <param name="detailType">
        /// 0=Nhà cung cấp,
        /// 1=Khách hàng,
        /// 2=Nhân viên,
        /// 3=Đối tượng tập hợp chi phí,
        /// 4=Hợp đồng,
        /// 5=Vật tư hàng hóa công cụ dụng cụ,
        /// 6=Tài khoản ngân hàng,
        /// 7=Khoản mục chi phí,
        /// 8=Chi tiết theo ngoại tệ,
        /// 9=Theo dõi phát sinh theo phòng ban,
        /// 10=Theo dõi phát sinh theo mục thu/chi</param>
        /// <returns>Danh sách tài khoản</returns>
        List<Account> GetByDetailType(int detailType, bool isActive = true);
        List<Account> GetByDetailType();

        /// <summary>
        /// Lấy đầy đủ danh sách Account Active các tài khoản có số dư và order by theo AccountNumber
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns>list danh sách Account</returns>
        List<Account> GetListActiveKindOrderBy(bool isActive);

        /// <summary>
        /// Lấy danh sách tài khoản chi tiết theo ngoại tệ
        /// </summary>
        /// <returns></returns>
        List<Account> GetAccounts_ByDetailTypeFollowCurrency();

        /// <summary>
        /// Lấy toàn bộ các tài khoản con (các con cuối cùng)
        /// </summary>
        /// <returns></returns>
        List<Account> GetAllChildrenAccount();

        List<AccountDefault_Account_Object> GetChildrenAccountWithCheck(string strdsAccount);
        /// <summary>
        /// Lấy danh sách các tài khoản cha
        /// </summary>
        /// <returns></returns>
        List<string> getListParentsAccount();
    }
}
