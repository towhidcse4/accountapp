﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.ServiceImp;

namespace Accounting.Core.IService
{
    public interface IFixedAssetLedgerService : FX.Data.IBaseService<FixedAssetLedger, Guid>
    {
        List<FixedAssetLedger> GetByReferenceID(Guid Id);
        /// <summary>
        /// [Dungna]
        /// Thẻ tài sản cố định
        /// </summary>
        /// <param name="PostedDate">Dữ liệu đầu vào là Ngày tự chọn để lọc cột PostedDate</param>
        /// <param name="ListIDFA"> Danh sách list trong bảng tài sản cố định</param>
        /// <returns>Danh sách các đối tượng tài sản cố định và list các con của nó</returns>
        S12DNN ReportS12DNN(DateTime PostedDate, List<Guid> ListIDFA);
        /// <summary>
        /// [DungNA]
        /// Sổ tài sản cố định
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="ListFixedAssetID">Danh sách Loại tài sản cố định</param>
        /// <returns></returns>
        List<S10DNN> RS10DNN(DateTime froms, DateTime to, List<Guid> ListFixedAssetID);
        /// <summary>
        /// [DungNA]
        /// Sổ theo dõi tài sản cố định
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to"> Đến Ngày </param>
        /// <param name="ListDepartmentID">List dánh sách phòng ban</param>
        /// <param name="CheckPrints">In theo số lượng khai báo trong tài sản cố định</param>
        /// <returns></returns>
        List<S11DNN> RS11DNN(DateTime from, DateTime to, List<Guid> ListDepartmentID, bool CheckPrints);
        int FindLastOrderPriority();
        FixedAssetLedger FindByFixedAssetID(Guid iD);

        /// <summary>
        /// Nếu là ghi giảm, kiểm tra xem đã điều chỉnh, điều chuyển có postedDate > postedDate của ghi giảm chưa
        /// đã tính khấu hao trong tháng chứ, nếu đã làm những điều trên không cho ghi sổ và thông báo
        /// "Ghi sổ thất bại! Tài sản này đã có phát sinh điều chỉnh, tính khấu hao hoặc điều chuyển sau ngày hạch toán của chứng từ ghi giảm này"
        /// </summary>
        /// <param name="FixedAssetID">fixedassetid</param>
        /// <returns></returns>
        int CheckLedgerForDecrement(Guid FixedAssetID, DateTime postedDate, bool isCheckPostedDate);

        /// <summary>
        /// Nếu là Điều chỉnh, kiểm tra xem đã ghi giảm, điều chuyển có postedDate > postedDate của ghi giảm chưa
        /// đã tính khấu hao trong tháng chứ, nếu đã làm những điều trên không cho ghi sổ và thông báo
        /// "Ghi sổ thất bại! Tài sản này đã có phát sinh điều chỉnh, tính khấu hao hoặc điều chuyển sau ngày hạch toán của chứng từ ghi giảm này"
        /// </summary>
        /// <param name="FixedAssetID">fixedassetid</param>
        /// <returns></returns>
        bool CheckLedgerForAdjustment(Guid FixedAssetID, DateTime dateTime);

        /// <summary>
        /// Nếu là tính khấu, kiểm tra xem đã ghi giảm, điều chuyển có postedDate > postedDate của ghi giảm chưa
        /// đã tính khấu hao trong tháng chưa, nếu đã làm những điều trên không cho ghi sổ và thông báo
        /// "Ghi sổ thất bại! Tài sản này đã có phát sinh điều chỉnh, tính khấu hao hoặc điều chuyển sau ngày hạch toán của chứng từ ghi giảm này"
        /// </summary>
        /// <param name="FixedAssetID">fixedassetid</param>
        /// <returns></returns>
        bool CheckLedgerForDepreciation(Guid FixedAssetID, DateTime dateTime);

        /// <summary>
        /// Tính tổng số tiền đã khấu hao
        /// </summary>
        /// <param name="FixedAssetID">fixedassetid</param>
        /// <returns></returns>
        decimal GetTotalAcDepreciationAmount(Guid fixedassetid, DateTime postedDate, bool? kk = null);

        /// <summary>
        /// Lấy giá tính khấu hao tháng order theo posteddate với các type id 701, 530 500, 510, 119, 129, 132, 142, 172
        /// </summary>
        /// <param name="FixedAssetID">fixedassetid</param>
        /// <returns></returns>
        decimal FindmonthDepreciationAmount(Guid iD, DateTime postedDate);


        /// <summary>
        /// Lấy ra bản ghi sổ của tài sản ghi tăng
        /// </summary>
        /// <param name="FixedAssetID">fixedassetid</param>
        /// <returns></returns>
        FixedAssetLedger findByRefIdAndFixedAssetId(Guid fAIncrementID, Guid iD);
    }
}
