﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
   public interface IMaterialGoodsAssemblyService : FX.Data.IBaseService<MaterialGoodsAssembly,Guid>
    {
       /// <summary>
       /// Lấy danh sách MaterialGoodsAssembly theo MaterialGoodsID
       /// [DUYTN]
       /// </summary>
       /// <param name="MaterialGoodsID">là MaterialGoodsID cần truyền vào</param>
       /// <returns>Tập các MaterialGoodsAssembly, null nếu bị lỗi</returns>
       List<MaterialGoodsAssembly> GetAll_ByMaterialGoodsID(Guid MaterialGoodsID);
    }
}
