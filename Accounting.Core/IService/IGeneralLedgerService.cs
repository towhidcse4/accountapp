﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
using Accounting.Core.ServiceImp;

namespace Accounting.Core.IService
{
    public interface IGeneralLedgerService : FX.Data.IBaseService<GeneralLedger, Guid>
    {
        List<GeneralLedger> GetListByReferenceID(Guid referenceID);

        /// <summary>
        /// Lấy danh sách ReferenceId ở bảng GeneralLedger
        /// [Longtx]
        /// </summary>
        /// <param name="Id">Tập các ID</param>
        /// <returns></returns>
        List<GeneralLedger> GetByReferenceID(Guid Id);
        List<GeneralLedger> GetByDetailID(Guid Id);
        /// <summary>
        /// Lấy danh sách GeneralLedger theo tham số là 1 tập AccountingObjectID
        /// [PhongNT]
        /// </summary>
        /// <param name="Id">Tập các ID</param>
        /// 29/04/2014
        /// <returns></returns>
        List<GeneralLedger> GetGLByListID(List<string> lstAcctNo);
        List<GeneralLedger> GetAllGL();

        /// <summary>
        /// Lấy danh sách chứng từ 
        /// [Chuongnv]
        /// </summary>
        /// <param name="AccountingObjectId"></param>
        /// <param name="accountNumber"></param>
        /// <param name="currencyID"></param>
        /// <param name="ngayHachToan"></param>
        /// 15/05/2014
        /// <returns></returns>
        Except GetVouchers(Guid accountingObjectId, string accountNumber, string currencyId);

        #region Lưu sổ cái Haipt
        //[H.A] thay thành trả ra trạng thái ghi sổ thành công hay ko
        bool SaveLedger<T>(T input);
        bool SaveLedgerNoTran<T>(T input, bool isMess = true);

        bool SaveLedger<T1, T2>(T1 input1, T2 input2);
        bool SaveLedgerNoTran<T1, T2>(T1 input1, T2 input2, bool isMess = true);
        #endregion

        #region Xóa chứng từ sổ cái
        //[H.A] thay thành trả ra trạng thái bỏ ghi thành công hay ko
        bool RemoveLedger<T>(T input);
        bool RemoveLedgerNoTran<T>(T input, bool isMess = true);
        
        bool RemoveLedger<T1, T2>(T1 input1, T2 input2);
        bool RemoveLedgerNoTran<T1, T2>(T1 input1, T2 input2, bool isMess = true);
        #endregion

        /// <summary>
        /// Bù trừ công nợ
        /// </summary>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="currency">loại tiền</param>
        /// <returns></returns>
        List<OffsetLiabilities> GetOffsetLiabilitieses(DateTime fromDate, DateTime toDate, Currency currency);

        /// <summary>
        /// Sổ quỹ
        /// [DuyTN] ngày 2-7-2013
        /// Report 
        /// </summary>
        /// <param name="date">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="currencyID">Mã loại tiền </param>
        /// <param name="account">Danh sách mã tài khoản</param>
        /// <returns>Một danh sách GeneralLedgerReport, null nếu bị lỗi</returns>
        List<S05aDNN> ReportS05aDNN(DateTime date, DateTime toDate, string currencyID, IEnumerable<string> account);

        /// <summary>
        /// Tổng hợp công nợ phải trả
        /// </summary>
        /// <param name="fromdate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <param name="currencyID">mã loại tiền</param>
        /// <param name="account">tài khoản</param>
        /// <param name="accountingObjectID">ds nhà cung cấp</param>
        /// <returns></returns>
        List<PayableReceiptableSummary> ReportsPayableReceiptableSummary(DateTime fromdate, DateTime toDate, string currencyID,
            IEnumerable<string> account, List<Guid> accountingObjectID);

        /// <summary>
        /// Báo cáo số tiền gửi ngân hàng
        /// [DungNA]
        /// </summary>
        /// <param name="Accoun1">Loại Tài Khoản</param>
        /// <param name="Date1">Ngày Tháng Ghi Sổ</param>
        /// <param name="PostedDate1">Ngày Tháng chứng từ</param>
        /// <param name="CurrencyID1">Loại Tiền</param>
        /// <param name="BankAccounDetailID1">Tài khoản Ngân Hàng</param>
        /// <param name="TotalGroup"> Checkbox cộng gộp</param>
        /// <returns></returns>
        List<S08DNN> ReportS06DNN(List<string> ListAccount, DateTime fromDate, DateTime ToDate, string CurrencyID1, Guid BankAccounDetailID1, bool TotalGroup);
        /// <summary>
        /// [DungNA]
        /// </summary>
        /// <param name="from">từ ngày</param>
        /// <param name="To">Đến Ngày</param>
        /// <param name="Account">Loại Tài Khoản</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="GroupAA"> Nút cộng gộp</param>
        /// <returns>Danh Sách các tài khoản tương ứng với loại tại khoản truyền vào</returns>
        List<S03A1DNN> RS03A1DNN(DateTime from, DateTime To, string Account, string CurrencyID, bool GroupAA);
        /// <summary>
        /// [DungNA]
        /// Sổ nhật ký thu tiền
        /// </summary>
        /// <param name="from">Từ ngày</param>
        /// <param name="To">Đến Ngày</param>
        /// <param name="Account"></param>
        /// <param name="CurrencyID"></param>
        /// <param name="GroupAA"></param>
        /// <returns></returns>
        List<S03A2DNN> RS03A2DNN(DateTime from, DateTime To, string Account, string CurrencyID, bool GroupAA);
        /// <summary>
        /// [DungNA]
        /// Bảng đối chiếu với ngân hàng
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="BankAccountDetailID">Mã tài khoản ngân hàng</param>
        /// <returns></returns>
        List<BankCompare> RBankCompare(DateTime froms, DateTime to, string CurrencyID, List<Guid> BankAccountDetailID);
        /// <summary>
        /// [DungNA]
        /// Báo cáo bảng kê số dư ngân hàng
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="ListAccount">Danh Sách Tài kHoản</param>
        /// <param name="CurrencyID">Loại Tiền Tệ</param>
        /// <returns></returns>
        List<BankBalance> RBankBlance(DateTime froms, DateTime to, List<string> ListAccount, string CurrencyID);
        GeneralLedger FindByAccountNumber(string originalPriceAccount);

        /// <summary>
        /// [DungNA]
        /// Sổ chi tiết tiền gửi ngân hàng bằng ngoại tệ
        /// </summary>
        /// <param name="froms">Từ ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="Account">Số tài khoản</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="ListBankAccountDetailID">Danh sách tài khoản ngân hàng</param>
        /// <param name="TotalGroup">Nút cộng gộp</param>
        /// <returns></returns>
        List<BankForeignCurrency> RForegignCurrencyCash(DateTime froms, DateTime to, string Account, string CurrencyID, List<Guid> ListBankAccountDetailID, bool TotalGroup);
        int? FindLastPriority();

        /// <summary>
        /// [DungNA]
        /// Sổ nhật ký chung
        /// </summary>
        /// <param name="froms">Từ ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="NoOrInvoiceNo">True hoặc Flase tương ứng với Số chứng từ hoặc Số hóa đơn</param>
        /// <param name="TotalGroup">Check cộng gộp các but toán giống nhau</param>
        /// <returns></returns>
        List<S03ADNN> RS03ADNN(DateTime froms, DateTime to, bool NoOrInvoiceNo, bool TotalGroup);
        /// <summary>
        /// [DungNa]
        /// Sổ cái tài khoản (Hình thức nhật ký chung)
        /// </summary>
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="NoOrInvoiceNo">True là số chứng từ còn false là số hóa đơn</param>
        /// <param name="CurrencyID">Loại Tiền</param>
        /// <param name="ListAccount">Danh sách tài khoản</param>
        /// <param name="TotalGroup">But toán cộng gộp</param>
        /// <returns></returns>
        List<S03BDNN> RS03BDNN(DateTime froms, DateTime to, bool NoOrInvoiceNo, string CurrencyID, List<string> ListAccount, bool TotalGroup);

        #region Hàm lấy số dư đầu kỳ báo cáo dùng chung
        /// <summary>
        /// [DungNA] And [Duy]
        /// Lấy số dư đầu kỳ báo cáo dùng chung
        /// </summary>
        /// <param name="fromDate">Từ Ngày (ngày bắt đầu báo cáo)</param>        
        /// <param name="currencyID">Loại Tiền</param>
        /// <param name="account">Số Tài khoản</param>
        /// <returns></returns>
        decimal GetOpeningBalanceDD(DateTime fromDate, string account, string currencyID = "");
        //Số dư đầu kỳ và số tồn đầu kỳ như nhau và số dư đầu năm
        #endregion
        #region Hàm lấy số dư cuối kỳ báo cáo dùng chung

        /// <summary>
        /// [DungNA] And [Duy]
        /// Lấy số dư cuối kỳ dùng chung
        /// </summary>
        /// <param name="toDate">Đến Ngày (ngày kết thúc báo cáo)</param>        
        /// <param name="currencyID">Loại Tiền</param>
        /// <param name="account">Số Tài khoản</param>
        /// <returns></returns>
        decimal GetClosingBalanceDD(DateTime toDate, string account, string currencyID = "");
        #endregion

        /// <summary>
        /// [DungNA]
        /// Form FSAExceptVoucher
        /// Danh sách chứng từ còn nợ
        /// </summary>
        /// <param name="accountingObjectId"> ID Nhà cung cấp</param>
        /// <param name="accountNumber"> Số tài khoản</param>
        /// <param name="currencyId"> ID Loại tiền</param>
        /// <returns></returns>
        Except GetVouchersSell(Guid accountingObjectId, string accountNumber, string currencyId);

        /// <summary>
        /// Lấy công nợ của khách hàng (Nghiệp vụ BÁN HÀNG)
        /// [kiendd]
        /// -- lọc theo đối tượng
        /// -- lọc theo ngày ghi sổ chứng từ nhỏ hơn hoặc bằng ngày hạch toán
        /// -- lọc tất cả những trường chi tiết theo khách hàng
        ///  Công nợ = tổng debit - tổng credit
        /// </summary>
        /// <param name="accountingObjectId">khách hàng cần lấy công nợ</param>
        /// <param name="postedDate">Ngày hạch toán</param>
        /// <returns></returns>
        decimal GetDebtCustomer(Guid accountingObjectId, DateTime postedDate);

        /// <summary>
        /// Lấy công nợ của NCC (Nghiệp vụ Mua HÀNG)
        /// [anhth]
        /// -- lọc theo đối tượng
        /// -- lọc theo ngày ghi sổ chứng từ nhỏ hơn hoặc bằng ngày hạch toán
        /// -- lọc tất cả những trường chi tiết theo khách hàng
        ///  Công nợ = tổng credit - tổng debit
        /// </summary>
        /// <param name="accountingObjectId">khách hàng cần lấy công nợ</param>
        /// <param name="postedDate">Ngày hạch toán</param>
        /// <returns></returns>
        decimal GetCredCustomer(Guid accountingObjectId, DateTime postedDate);

        #region Chứng từ ghi sổ
        /// <summary>
        /// Lấy ra danh sách chứng từ cần thao tác vào bảng chứng từ ghi sổ
        /// </summary>
        /// <param name="typeID">loại chứng từ cần lấy ra</param>
        /// <param name="fromDate">từ ngày</param>
        /// <param name="toDate">đến ngày</param>
        /// <returns></returns>
        List<GVoucherListDetail> GetRecordingVouchers(int typeID, DateTime fromDate, DateTime toDate);
        List<GVoucherListDetail> GetRecordingVouchers(List<int> lstTypeGroupID, DateTime fromDate, DateTime toDate);
        #endregion

        /// <summary>
        /// [Thudq]
        /// <param name="froms">Từ Ngày</param>
        /// <param name="to">Đến Ngày</param>
        /// <param name="EmployeeID">Nhân viên</param>
        /// <returns></returns>
        List<GeneralLeged_TypeName> GetListEmp(Guid emplID, DateTime fromdate, DateTime todate);

        /// <summary>
        /// Tính tỉ gía xuất qũy theo phương pháp bình quân cuối kỳ
        /// </summary>
        /// <param name="listCurrency">danh sách loại tiền</param>
        /// <param name="toDate">Đến ngày</param>
        /// <returns></returns>
        GOtherVoucher FormulaExchangeRate_AverageForEndOfPeriods(List<Currency> listCurrency, DateTime toDate);
        /// <summary>
        /// Đánh giá lại tài khoản ngoại tệ
        /// </summary>
        /// <param name="cu"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        List<GOtherVoucherDetailForeignCurrency> GetGOtherVoucherDetailForeignCurrency(Currency cu, DateTime dt, decimal newEx);
        List<GOtherVoucherDetailDebtPayment> GetGOtherVoucherDetailDebtPayment(Currency cu, DateTime dt, decimal newEx);
        //GOtherVoucher FormulaExchangeRate_AverageForInstantaneous<T>(List<Currency> listCurrency, DateTime toDate);
        List<EMContractSale> GetEMContractSaleDetailByContractID(Guid contractID, List<int> typeID);
        List<EMContractSale> GetEMContractHoaDonByContractID(Guid contractID, List<int> typeID);
        List<EMContractSale> GetEMContractSaleDetailByTypeID(List<int> typeID, DateTime fromdate, DateTime todate);
        List<EMContractSale> GetEMContractSaleThucChiDetailByContractID(Guid contractID, List<int> typeID);
        List<EMContractSale> GetEMContractSaleThucChiDetailByTypeID(List<int> typeID, DateTime fromdate, DateTime todate);
        /// <summary>
        /// Hàm lấy ra một đối tượng EMContractBuy
        /// [DuyTN] ngày 4-7-2013 Cho Long
        /// </summary>
        /// <param name="contractID"> mã hợp đồng</param>
        /// <param name="typeID"> danh sách các loại type id tương ứng</param>
        /// <returns>trả về một danh sách, null nếu bị lỗi </returns>
        List<EMContractBuy> GetEmContractBuys(Guid contractID, List<int> typeID);
        List<EMContractBuy> GetEmContractBuyInvoices(Guid contractID, List<int> typeID);
        List<GeneralLedger> GetGLByDetailID(Guid ID);

        FixedAssetLedger GetLastFixedAssetLedger(Guid? fixedAssetId, DateTime dp);
        /// <summary>
        /// Lấy dữ liệu kết chuyển lãi lỗ theo cặp tài khoản
        /// </summary>
        /// <returns></returns>
        List<GOtherVoucherDetail> getILTranferDetail(DateTime toDate);
        List<GeneralLedger> GetByContractID(Guid contractID);

        ToolLedger GetLastestToolLedger(Guid? iD, DateTime dp);

        // Tập hợp chi phí trực tiếp
        List<CPExpenseList> GetCPExpenseListsOnGL(DateTime fromdate, DateTime todate, List<Guid> costsetID);
        List<CPExpenseList> GetCPExpenseListsContractOnGL(DateTime fromdate, DateTime todate, List<Guid> costsetID);
        // Tập hợp các khoản giảm giá thành
        List<CPExpenseList> GetCPExpenseListsOnGL2(DateTime fromdate, DateTime todate, List<Guid> costsetID);
        List<CPExpenseList> GetCPExpenseListsContractOnGL2(DateTime fromdate, DateTime todate, List<Guid> costsetID);
        //chi phí phân bổ+
        List<CPAllocationGeneralExpense> GetCPAllocationGeneralExpenseOnGL(DateTime fromdate, DateTime todate);
        List<CPAllocationGeneralExpense> GetCPAllocationGeneralExpenseOnGLBefore(DateTime fromdate);//add by cuongpv
        List<CPAllocationGeneralExpense> GetCPAllocationGeneralExpenseContractOnGL(DateTime fromdate, DateTime todate);
        List<object> GetListObject_Xulychungtu<T>(T input, ref string msg, List<ViewGLPayExceedCash> lstViewGLPayExceedCash);
        List<object> ProcessRemoveLedger_XLCT<T>(T input);
    }
}
