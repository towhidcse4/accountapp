﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;

namespace  Accounting.Core.IService
{
   public interface IEMContractDetailPaymentService:FX.Data.IBaseService<EMContractDetailPayment ,Guid>
    {
       List<EMContractDetailPayment> getEMContractDetailPaymentbyIDEMContract(Guid id);
       EMContractDetailPayment GetByID(Guid id);
       Decimal TotalDetailPaymentByContractID(Guid contractId);
    }

}
