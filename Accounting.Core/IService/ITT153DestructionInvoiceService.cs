using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153DestructionInvoiceService:FX.Data.IBaseService<TT153DestructionInvoice ,Guid>
    {
        List<TT153DestructionInvoiceDetail> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportID);
        List<TT153DestructionInvoiceDetail> GetAllToDate(DateTime To, Guid TT153ReportID);
    }
}
