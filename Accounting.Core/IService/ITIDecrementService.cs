﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
//using Accounting.Core.Domain.obj.TIDecrements;

namespace Accounting.Core.IService
{
    public interface ITIDecrementService : FX.Data.IBaseService<TIDecrement, Guid>
    {
        //List<GGHachToan> GetListGG(string FACode);
        TIDecrement findByRefID(Guid iD);
    }
}
