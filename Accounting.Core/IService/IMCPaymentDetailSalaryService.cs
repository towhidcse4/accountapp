using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentDetailSalaryService : FX.Data.IBaseService<MCPaymentDetailSalary, Guid>
    {
    }
}
