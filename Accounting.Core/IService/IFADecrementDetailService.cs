﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFADecrementDetailService :FX.Data.IBaseService<FADecrementDetail,Guid>
    {
        List<FADecrementDetail> GetByFADecrementDetailID(Guid faDecrementID);
        DateTime? FindDecrementDate(Guid iD, DateTime startDate);
    }
}
