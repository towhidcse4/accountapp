using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IPersonalSalaryTaxService : FX.Data.IBaseService<PersonalSalaryTax, Guid>
    {
    }
}
