﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface IViewVoucherInvisibleService : FX.Data.IBaseService<ViewVoucherInvisible, Guid>
    {
        List<ViewVoucherInvisible> GetAllVoucherUnRecorded(DateTime newDBDateClosed, Guid? branchId = null);
        ViewVoucherInvisible GetByNo(string No);
        ViewVoucherInvisible GetByRefID(Guid refID);
        bool CheckNoExist(string No);
        List<ViewVoucherInvisible> SearchVoucherNo(string partNo, DateTime startDate, DateTime endDate, Guid? branchID = null);

        /// <summary>
        /// Lấy danh sách chứng từ gốc
        /// </summary>
        /// <returns></returns>
        List<ViewVoucherReset> GetPureVoucher(int TypeGroupID, DateTime StartDate, DateTime EndDate);
        List<RefVoucherRSInwardOutward> GetVoucher(int TypeGroupID, DateTime StartDate, DateTime EndDate);
        List<RefVoucherRSInwardOutward> GetVoucherFromView(int TypeGroupID, DateTime StartDate, DateTime EndDate);
        List<RefVoucher> GetVoucherTest(int TypeGroupID, DateTime startDate, DateTime endDate);
        List<ViewVoucherReset> GetVoucherInList(List<string> listNo, DateTime startDate, DateTime endDate);

        List<ViewVoucherInvisible> GetVoucherInvisible(int? TypeGroupID, DateTime StartDate, DateTime EndDate);

    }
}
