﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMPublishPeriodService : FX.Data.IBaseService<EMPublishPeriod, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách PublishPeriodCode
        /// [haipt]
        /// </summary>
        /// <returns></returns>
        List<string> GetListPublishPeriodCode();
    }
}
