﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IAccountingObjectGroupService : FX.Data.IBaseService<AccountingObjectGroup, Guid>
    {
        /// <summary>
        /// Lấy danh sách AccountingObjectGroup 
        /// </summary>
        /// <param name="IsActive">Được Active</param>
        /// <returns>Danh sách AccountingObjectGroup được order</returns>
        List<AccountingObjectGroup> GetListByIsActiveOrderCode(bool IsActive);

        /// <summary>
        /// Lấy danh sách các AccountingObjectGroup theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        List<AccountingObjectGroup> GetAll_ByParentID(Guid ParentID);

        /// <summary>
        /// Lấy tổng số dòng theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID"> là ParentID truyền vào</param>
        /// <returns>Tổng số dòng chứa cùng một ParentID, null nếu bị lỗi</returns>
        int CountByParentID(Guid? ParentID);

        /// <summary>
        /// Lấy danh sách AccountingObjectGroup sắp xếp tăng dần theo AccountingObjectGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        List<AccountingObjectGroup> GetAll_OrderByAccountingObjectGroupCode();

        /// <summary>
        /// Lấy danh sách AccountingObjectGroup sắp xếp tăng dần theo AccountingObjectGroupName
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        List<AccountingObjectGroup> GetAll_OrderByAccountingObjectGroupName();

        /// <summary>
        /// Lấy danh sách các OrderFixCode theo ParentID
        /// [DUYTN] 
        /// </summary>
        /// <param name="ParentID">là ParentID truyền vào</param>
        /// <returns>tập các OrderFixCode, null nếu bị lỗi</returns>
        List<string> GetOrderFixCode_ByParentID(Guid? ParentID);

        /// <summary>
        /// Lấy ra danh sách AccountingObjectGroup (các con cuối cùng) theo OrderFixCode
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode">là OrderFixCode vị trí của một đối tượng AccountingObjectGroup </param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        List<AccountingObjectGroup> GetStartsWith_OrderFixCode(string OrderFixCode);

        /// <summary>
        /// Lấy ra danh sách AccountingObjectGroup (các con cuối cùng) theo OrderFixCode và có giá trị khác ID
        /// [DUYTN]
        /// </summary>
        /// <param name="OrderFixCode">là OrderFixCode vị trí của một đối tượng AccountingObjectGroup </param>
        /// <param name="ID">là ID của một đối tượng AccountingObjectGroup </param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        List<AccountingObjectGroup> GetStartsWith_OrderFixCode_NotValueByID(string p, Guid ID);

        /// <summary>
        /// Lấy ra danh sách các AccountingObjectGroup theo Grade
        /// [DUYTN]
        /// </summary>
        /// <param name="Grade">là Grade tham số kiểu int truyền vào</param>
        /// <returns>tập các AccountingObjectGroup, null nếu bị lỗi</returns>
        List<AccountingObjectGroup> GetAll_ByGrade(int Grade);

        /// <summary>
        /// Lấy ra danh sách các AccountingObjectGroupCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các AccountingObjectGroupCode, null nếu bị lỗi</returns>
        List<string> GetAccountingObjectGroupCode();

        Guid? GetGuidAccountingObjectGroupByCode(string code);

    }
}
