﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IAccountingObjectBankAccountService : FX.Data.IBaseService<AccountingObjectBankAccount, Guid>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ObjAccounting"></param>
        /// <returns></returns>
        List<AccountingObjectBankAccount> GetByAccountingObject(AccountingObject ObjAccounting);

        List<AccountingObjectBankAccount> GetAll_OrderBy();

        /// <summary>
        /// Lấy danh sách Accounting ObjectBankAccount By AccountingObjectID 
        /// </summary>
        /// <param name="accountingObjectID">AccountObject ID</param>
        /// <returns>Danh sách AccountingObjectBankAccount </returns>
        List<AccountingObjectBankAccount> GetByAccountingObjectID(Guid accountingObjectID);

        /// <summary>
        /// Lấy danh sách các chứng từ thu tiền để đối chiếu giao dịch với ngân hàng
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="CurrencyID">Mã tiền tệ</param>
        /// <param name="MatchDate">Ngày đối chiếu</param>
        /// <param name="isMatched">true/false: đã khớp/chưa khớp</param>
        /// <returns>Danh sách CollectionVoucher </returns>
        List<CollectionVoucher> GetCollectionVoucher(Guid bankAccountDetailId, string currencyId, DateTime matchDate, bool isMatched);
        List<CollectionVoucher> GetCollectionVoucherList(List<Guid> ListBankAccountDetailID, string currencyId, DateTime matchDate, bool isMatched);

        /// <summary>
        /// Lấy danh sách các chứng từ chi tiền để đối chiếu giao dịch với ngân hàng
        /// [DungNA]
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="CurrencyID">Mã tiền tệ</param>
        /// <param name="MatchDate">Ngày đối chiếu</param>
        /// <param name="isMatched">true/false: đã khớp/chưa khớp</param>
        /// <returns>Danh sách SpendingVoucher </returns>
        List<SpendingVoucher> GetSpendingVoucher(Guid bankAccountDetailId, string currencyId, DateTime matchDate, bool isMatched);
        List<SpendingVoucher> GetSpendingVoucherList(List<Guid> ListBankAccountDetailID, string currencyId, DateTime matchDate, bool isMatched);
        /// <summary>
        /// Lấy số dư đầu kỳ sau đối chiếu giao dịch với ngân hàng
        /// [DungNA]
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="CurrencyID">Mã tiền tệ</param>
        /// <param name="MatchDate">Ngày đối chiếu</param>
        /// <returns>Danh sách SpendingVoucher </returns>
        decimal GetMatchedOverBalance(Guid bankAccountDetailId, string currencyId, DateTime matchDate);

        /// <summary>
        /// Lấy danh sách các chứng từ đã đối trừ
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>        
        /// <returns>Danh sách ComparedVoucher </returns>
        List<ComparedVoucher> GetComparedVoucher(List<Guid> bankAccountDetailId);

        /// <summary>
        /// Lấy thông tin các ngày đối chiếu theo tài khoản ngân hàng
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="dateCompared">Ngày đối chiếu</param>
        /// <returns>Danh sách Compared </returns>
         List<Compared> GetCompared(Guid bankAccountDetailId);

        List<Compared> GetCompared(List<Guid> bankAccountDetailId);
        /// <summary>
        /// Đảo trạng thái đã đối trừ của chứng từ
        /// </summary>
        /// <param name="BankAccountDetailID">ID của số tài khoản ngân hàng</param>
        /// <param name="dateCompared">Ngày đối chiếu</param>
        /// <returns>true/false </returns>
         bool Remove(Guid bankAccountDetailId, DateTime dateCompared, bool Status);
         bool RemoveList(List<Compared> lstCompareds );

        /// <summary>
        /// Hàm thực hiện đối chiếu 
        /// [DungNA]
        /// </summary>
        /// <param name="lstCompareds"></param>
        /// <param name="dateCompareD">Ngày đối chiếu</param>
        /// <returns></returns>
        bool ComparedV(List<Compared> lstCompareds, DateTime dateCompareD);
        List<AccountingObjectBankAccount> GetByAccountingObjectType(Guid accoutingObjectID, List<int> objectType);
    }
}
