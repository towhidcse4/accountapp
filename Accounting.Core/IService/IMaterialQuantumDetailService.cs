﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IMaterialQuantumDetailService:FX.Data.IBaseService<MaterialQuantumDetail ,Guid>
    {
        /// <summary>
        /// Lấy ra danh sách MaterialQuantumDetail theo MaterialQuantumID
        /// [DUYTN]
        /// </summary>
        /// <param name="MaterialQuantumID">là MaterialQuantumID truyền vào</param>
        /// <returns>tập các MaterialQuantumDetail, null nếu bị lỗi</returns>
       List<MaterialQuantumDetail> GetAll_ByMaterialQuantumID(Guid MaterialQuantumID);
    }
}
