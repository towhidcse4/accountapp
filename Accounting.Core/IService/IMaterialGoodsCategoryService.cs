﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
   public interface IMaterialGoodsCategoryService:FX.Data.IBaseService<MaterialGoodsCategory,Guid>
    {
        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCategory các MaterialGoodsCategory cuối cùng
        /// theo một đối tượng _MaterialGoodsCategory và IsParentNode
        /// [DUYTN]
        /// </summary>
        /// <param name="_MaterialGoodsCategory"> là đối tượng MaterialGoodsCategory</param>
        /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
       List<MaterialGoodsCategory> getMaterialGoodsCategorysbyLikes(MaterialGoodsCategory _MaterialGoodsCategory, Boolean IsParentNode);

       /// <summary>
       /// Lấy ra một đối tượng MaterialGoodsCategory theo MaterialGoodsCode
       /// </summary>
       /// <param name="_MaterialGoodsCode">là MaterialGoodsCode được truyền vào</param>
       /// <returns>Một đối tượng MaterialGoodsCategory, null nếu bị lỗi</returns>
       MaterialGoodsCategory getMaterialGoodsCategorysbyAccountNumber(string _MaterialGoodsCode);

       /// <summary>
       /// Lấy ra danh sách MaterialGoodsCategory theo IsActive
       /// [DUYTN]
       /// </summary>
       /// <param name="IsActive">là IsActive truyền vào</param>
       /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
       List<MaterialGoodsCategory> GetAll_ByIsActive(bool IsActive);

       /// <summary>
       /// Lấy ra danh sách MaterialGoodsCategory theo ParentID
       /// [DUYTN]
       /// </summary>
       /// <param name="ParentID">là ParentID truyền vào</param>
       /// <returns>tập các , null nếu bị lỗi</returns>
       List<MaterialGoodsCategory> GetAll_ByParentID(Guid ParentID);

       /// <summary>
       /// Đếm số dòng dữ liệu theo ParentID
       /// [DUYTN]
       /// </summary>
       /// <param name="ParentID">là ParentID lọc</param>
       /// <returns>Số dòng, null nếu bị lỗi</returns>
       int CountByParentID(Guid? ParentID);
        /// <summary>
        /// Lấy ra danh sách OrderFixCode theo ParentID
        /// [DUYTN]
        /// </summary>
        /// <param name="ParentID"> là ParentID truyền vào</param>
        /// <returns>tập các , null nếu bị lỗi</returns>
        List<MaterialGoodsCategory> GetAll_ByIsParentNode(bool IsParentNode);
        List<string> GetOrderFixCode_ByParentID(Guid? ParentID);

       /// <summary>
       /// Lấy ra danh sách MaterialGoodsCategory (các con cuối cùng)
       /// [DUYTN]
       /// </summary>
       /// <param name="OrderFixCode">là OrderFixCode vị trí của một đối tượng MaterialGoodsCategory </param>
       /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
       List<MaterialGoodsCategory> GetStartsWith_OrderFixCode(string OrderFixCode);

       /// <summary>
       /// Lấy ra danh sách MaterialGoodsCategory theo Grade
       /// [DUYTN]
       /// </summary>
       /// <param name="Grade"> là tham số bậc của Grade, i là số nguyên</param>
       /// <returns>tập các MaterialGoodsCategory, null nếu bị lỗi</returns>
       List<MaterialGoodsCategory> GetAll_ByGrade(int Grade);

       /// <summary>
       /// Lấy ra danh sách MaterialGoodsCode
       /// [DUYTN]
       /// </summary>
       /// <returns>tập các MaterialGoodsCode, null nếu bị lỗi</returns>
       List<string> GetMaterialGoodsCode();
       List<MaterialGoodsCategory> GetAll_OrderBy();
       /// <summary>
       /// Lấy ra ID của danh mục vật tư hàng hóa
       /// </summary>
       /// <param name="code">Mã danh mục vật tư hàng hóa</param>
       /// <returns></returns>
       Guid? GetGuidMaterialGoodsCategoryByCode(string code);
        string GetMaterialGoodsCategoryCodeByGuid(Guid? guid);
    }
}
