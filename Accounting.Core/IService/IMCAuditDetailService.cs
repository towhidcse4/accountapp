﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCAuditDetailService : FX.Data.IBaseService<MCAuditDetail, Guid>
    {

    }
}
