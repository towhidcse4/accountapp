﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMRegistrationService : FX.Data.IBaseService<EMRegistration, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách EMRegistration theo tempID
        /// [haipt]
        /// </summary>
        /// <param name="tempID"></param>
        /// <returns></returns>
        List<EMRegistration> GetListEMRegistrationID(Guid? tempID);

        /// <summary>
        /// Lấy ra danh sách EMRegistrationCode
        /// [haipt]
        /// </summary>
        /// <returns></returns>
        List<string> GetListEMRegistrationCode();
    }
}
