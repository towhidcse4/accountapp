﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFATransferDetailService : FX.Data.IBaseService<FATransferDetail, Guid>
    {
        
        List<FATransferDetail> GetByFATransferID(Guid fATransferID);
    }
}
