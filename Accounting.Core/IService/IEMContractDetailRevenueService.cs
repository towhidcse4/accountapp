using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
namespace Accounting.Core.IService
{
   public interface IEMContractDetailRevenueService: FX.Data.IBaseService<EMContractDetailRevenue, Guid>
    {
        List<EMContractDetailRevenue> GetByContractID(Guid contractID);
    }
}
