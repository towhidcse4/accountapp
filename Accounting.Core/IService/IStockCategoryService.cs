﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IStockCategoryService : FX.Data.IBaseService<StockCategory, Guid>
    {
        /// <summary>
        /// Lấy danh sách Danh mục cổ phần theo mã cổ phần [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<string> GetCode();
        List<StockCategory> GetAll_OrderBy();
    }
}
