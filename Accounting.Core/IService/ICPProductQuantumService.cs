using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPProductQuantumService: FX.Data.IBaseService<CPProductQuantum ,Guid>
    {
        List<CPMaterialProductQuantum> GetAllByType();
    }
}
