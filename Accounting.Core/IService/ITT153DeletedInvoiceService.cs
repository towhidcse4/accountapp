using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153DeletedInvoiceService:FX.Data.IBaseService<TT153DeletedInvoice ,Guid>
    {
        List<int> GetAllFromDateToDate(DateTime From, DateTime To, Guid InvoiceTypeID, int InvoiceForm, string InvoiceTemplate, string InvoiceSeries);
        List<SAInvoice> GetAllSAInvoice(Guid TT153ReportID);
    }
}
