﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCAuditService : FX.Data.IBaseService<MCAudit, Guid>
    {
        decimal SoDuSoQuy(DateTime date, string Currency);
    }
}
