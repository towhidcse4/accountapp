using Accounting.Core.Domain;
using System;

namespace Accounting.Core.IService
{
    public interface ICashFlowStatementReportService : FX.Data.IBaseService<CashFlowStatementReport, Guid>
    {
    }
}
