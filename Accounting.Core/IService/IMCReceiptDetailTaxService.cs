using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCReceiptDetailTaxService : FX.Data.IBaseService<MCReceiptDetailTax, Guid>
    {
        IList<MCReceiptDetailTax> getMCReceiptDetailTaxbyID(Guid id);
    }
}
