using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPOrderService:FX.Data.IBaseService<PPOrder ,Guid>
   {
       PPOrder GetPPOrderbyNo(string no);
       PPOrder GetPPOrderByID(Guid id);
       List<PPOrder> GetListOrderByNo();
       List<PPOrder> GetAllOrderByNoAndNotInContract(Guid ID);
        List<PPOrder> GetOrderByContractID(Guid contractID);
        PPOrder GetOrderByNo(string no);
        Guid GetIDByNo(string no);
        List<PPOrder> GetOrderByContract(Guid contractID);
        List<Guid> GetOrderIDByContractID(Guid contractID);
    }
}
