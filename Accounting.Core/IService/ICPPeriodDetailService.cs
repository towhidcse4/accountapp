using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPPeriodDetailService: FX.Data.IBaseService<CPPeriodDetail ,Guid>
    {
        List<CPPeriodDetail> GetList();
    }
}
