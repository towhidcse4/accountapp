﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ITransportMethodService : FX.Data.IBaseService<TransportMethod, Guid>
    {
        /// <summary>
        /// Sắp xếp danh sách Phương thức vận chuyển theo mã [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<TransportMethod> OrderByCode();
        /// <summary>
        /// Lấy tất cả danh sách mã phương thức vận chuyển [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<string> GetCode();
        /// <summary>
        /// lấy danh sách phương thức vận chuyển
        /// </summary>
        /// <returns></returns>
        List<TransportMethod> GetActive();
    }
}
