using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IPSSalarySheetDetailService : FX.Data.IBaseService<PSSalarySheetDetail, Guid>
    {
        IList<PSSalarySheetDetail> FindEmployeeByDepartment(List<Guid> departments, DateTime createDate, int typeID, int type, decimal WorkDayInMonth, List<PSTimeSheetDetail> psts = null, List<PSTimeSheetSummaryDetail> pstss = null);
    }
}
