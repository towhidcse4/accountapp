using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IPSTimeSheetDetailService : FX.Data.IBaseService<PSTimeSheetDetail, Guid>
    {
        List<PSTimeSheetDetail> FindEmployeeByDepartment(List<Guid> departments, int daysInMonth, List<int> t7, List<int> cn, decimal? WorkingHoursInDay, 
            Guid? baseOn, bool isNewEmployee, bool isUnemployee, bool st7, bool ct7, bool scn, bool ccn);
        
    }
}
