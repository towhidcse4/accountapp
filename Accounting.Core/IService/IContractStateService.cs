﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IContractStateService : FX.Data.IBaseService<ContractState,int>
    {       
        ContractState getContractStatebyName(string _ContractStateName);

        /// <summary>
        /// Lấy danh sách ContractState đã sắp xếp
        /// [hait]
        /// </summary>
        /// <returns></returns>
        List<ContractState> GetListContractStateOrder();

        /// <summary>
        /// Lấy danh sách ContractStateCode
        /// [haipt]
        /// </summary>
        /// <returns></returns>
        List<string> GetListContractStateCode();
    }
}
