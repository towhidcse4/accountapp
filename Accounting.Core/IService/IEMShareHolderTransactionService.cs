﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMShareHolderTransactionService : FX.Data.IBaseService<EMShareHolderTransaction, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách EMShareHolderTransaction theo EMShareHolderID
        /// [haipt]
        /// </summary>
        /// <param name="emShareHolderID"></param>
        /// <returns></returns>
        List<EMShareHolderTransaction> GetListEMShareHolderTransactionEMShareHolder(Guid? emShareHolderID);
    }
}
