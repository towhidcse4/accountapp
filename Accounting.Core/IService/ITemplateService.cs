﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ITemplateService : FX.Data.IBaseService<Template, Guid>
    {
        /// <summary>
        /// các "mẫu giao diện" trong một "nghiệp vụ"
        /// </summary>
        /// <param name="TypeID"></param>
        /// <returns></returns>
        List<Template> getTemplateinTypeForm(int TypeID);

        Template getTemplateStandard(int typeID);
    }
}
