using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPServiceDetailService:FX.Data.IBaseService<PPServiceDetail ,Guid>
   {
       IList<PPServiceDetail> GetPPServiceDetailbyID(Guid id);
    }
}
