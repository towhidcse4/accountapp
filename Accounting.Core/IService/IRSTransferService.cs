using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IRSTransferService : FX.Data.IBaseService<RSTransfer, Guid>
    {
        RSTransfer getRSTransferbyNo(string no);
    }
}
