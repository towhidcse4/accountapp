﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.FSACalculateTheCost;
using NHibernate;

namespace Accounting.Core.IService
{
    public interface IMaterialGoodsService : FX.Data.IBaseService<MaterialGoods, Guid>
    {
        ISession GetSessionState();
        /// <summary>
        /// Lấy ra danh sách MaterialGoods theo MaterialGoodType
        /// [DUYTN]
        /// </summary>
        /// <param name="materialGoodType">MaterialGoodType truyền vào</param>
        /// <returns>Danh sách các đối tượng MaterialGoods, null nếu bị lỗi</returns>
        List<MaterialGoods> GetAll_ByMaterialGoodsType(int materialGoodType);

        /// <summary>
        /// Lấy ra danh sách các MaterialGoods với so sách khác theo MaterialToolType
        /// [DUYTN]
        /// </summary>
        /// <param name="Other_MaterialToolType">là tham số Other_MaterialToolType và là số nguyên</param>
        /// <returns>Danh sách các đối tượng MaterialGoods, null nếu bị lỗi</returns>
        List<MaterialGoods> GetAll_NotValueByMaterialToolType(int Other_MaterialToolType);

        /// <summary>
        /// Lấy ra danh sách các MaterialGoods với so sách khác theo ID
        /// [DUYTN]
        /// </summary>
        /// <param name="ID">là ID của một đối tượng MaterialGoods</param>
        /// <returns>Danh sách các đối tượng MaterialGoods, null nếu bị lỗi</returns>
        List<MaterialGoods> GetAll_NotValueByID(Guid ID);

        /// <summary>
        /// Lấy ra danh sách MaterialGoodsCode
        /// [DUYTN]
        /// </summary>
        /// <returns>Danh sách MaterialGoodsCode dạng chuỗi, null nếu bị lỗi</returns>
        List<string> GetMaterialGoodsCode();

        /// <summary>
        /// Lấy ra danh sách MaterialGoods theo list MaterialGoodsType
        /// [DUYTN], sửa ngày 07/04/2014/
        /// </summary>
        /// <param name="MaterialGoodsType">Danh sách các MaterialGoodsType cần lấy (0,1,2,3,...)</param>
        /// <returns>tập các MaterialGoods, null nếu bị lỗi</returns>
        List<MaterialGoods> GetAll_ByMaterialGoodsType(List<int> MaterialGoodsType);

        /// <summary>
        /// Lấy một đối tượng MaterialGoods theo MaterialGoodsCode
        /// [DUYTN], sửa ngày 07/04/2014
        /// </summary>
        /// <param name="MaterialGoodsCode">là MaterialGoodsCode truyền vào</param>
        /// <returns>Một MaterialGoodsCode, null nếu bị lỗi</returns>
        MaterialGoods GetAll_ByMaterialGoodsCode(string MaterialGoodsCode);

        List<MaterialGoods> GetByMateriaGoodCode();

        List<MaterialGoods> GetListMaterialGood(Guid? materialGoodID);

        /// <summary>
        /// Lấy ra danh sách MarerialGoodsCode từ MaterialGoods có IsActive==true
        /// [Longtx]
        /// </summary>
        /// <returns></returns>
        List<MaterialGoods> GetByMateriaGoodCode(bool isActive);

        /// <summary>
        /// Lấy ra danh sách MaterialGoods có kèm Số lượng tồn 
        /// [Huy Anh]
        /// </summary>
        /// <param name="postedDate"></param>
        /// <param name="branchID"></param>
        /// <param name="isLeftJoin"></param>
        /// <returns>Trả về danh sách MaterialGoodsCustom ,null nếu lỗi</returns>
        List<MaterialGoodsCustom> GetMateriaGoodByAndRepositoryLedger(DateTime postedDate, Guid? branchID, bool isLeftJoin = true);

        List<MaterialGoodsCustom> GetMateriaGoodByMaterialToolTypeAndRepositoryLedger(int MaterialToolType,
                                                                                      bool isLeftJoin = true);
        /// <summary>
        /// Lấy ra danh sách MaterialGoods kèm số lượng tồn theo từng kho
        /// [DuyNT]
        /// </summary>
        /// <param name="postedDate"></param>
        /// <returns>Trả về danh sách MaterialGoodsCustom ,null nếu lỗi</returns>
        List<MaterialGoodsCustom> GetMateriaGoodAndRepositoryLedger(DateTime postedDate);

        /// <summary>
        /// Lấy danh sách Vật tư hàng hóa đang hoạt động [khanhtq]
        /// </summary>
        /// <param name="isActive">Trạng thái hoạt động (true/false)</param>
        /// <returns></returns>
        List<MaterialGoods> GetByIsActive(bool isActive);

        /// <summary>
        /// Lấy danh sách MaterialGoods theo MaterialGoodsCategory
        /// </summary>
        /// <param name="materialGoodsCategory"></param>
        /// <returns></returns>
        List<MaterialGoods> GetListByMaterialGoodsCategory(Guid? materialGoodsCategory);

        List<string> CheckExistMaterialGoods(IEnumerable<Guid> listCheck);

        /// <summary>
        /// [DungNA]
        /// Danh sách tính giá bán phân hệ bán hàng
        /// </summary>
        /// <returns></returns>
        List<CalculateCost> GetListCalculateCosts();

        /// <summary>
        /// Lấy ra danh sách sắp xếp
        /// </summary>
        /// <returns></returns>
        List<MaterialGoods> GetOrderby();

        List<VoucherMaterialGoods> GetlistVoucherMaterialGoodsView();

        Guid? GetGuidMaterialGoodsByCode(string code);

        List<MaterialGoods> GetMaterialTools();

        List<MaterialGoodsCustom> GetMateriaToolsByAndRepositoryLedger(DateTime postedDate, Guid? branchID, bool isLeftJoin = true);

        List<MaterialGoods> GetListBySAOrderID(Guid id);
        List<MaterialGoods> GetListByPPOrderID(Guid id);
        /// <summary>
        /// Check has exists
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        bool ExistsMaterialCode(string req);

        /// <summary>
        /// kiểm tra xem đã tồn tại material good hay chưa
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        bool CheckExistMaterialGoods(string code, Guid iD);

        /// <summary>
        /// Kiểm tra CCDC trước khi xóa
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        bool CheckDeleteMaterialTools(Guid iD);


        /// <summary>
        /// Lấy ra danh mục CCDC
        /// </summary>
        /// <param name="req">MaterialCode</param>
        /// <returns></returns>
        List<MaterialGoods> GetByMaterialTools();

        List<VoucherTools> GetListVouchers(Guid iD);
        List<TongHopTonKho> getMaterialGood(DateTime from, DateTime to, List<Repository> lstrepos, MaterialGoodsCategory mtc);
        List<TongHopTonKho> getMaterialGood(DateTime from, DateTime to, List<Repository> lstrepos);
    }
}
