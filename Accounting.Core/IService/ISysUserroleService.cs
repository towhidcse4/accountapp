using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface ISysUserroleService : FX.Data.IBaseService<SysUserrole, Guid>
    {
        List<SysUserrole> GetUserroleByUserID(Guid userID);
    }
}
