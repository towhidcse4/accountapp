﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMTransferDetailService : FX.Data.IBaseService<EMTransferDetail, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách EMTransferDetail theo EMTransferID
        /// </summary>
        /// <param name="emTransferID"></param>
        /// <returns></returns>
        List<EMTransferDetail> GetListEMTransferDetailEMTransferID(Guid? emTransferID);

        /// <summary>
        /// Lấy ra 1 EMTransferDetail theo StockCategoryID
        /// </summary>
        /// <param name="stockCategoryID"></param>
        /// <returns></returns>
        EMTransferDetail GetEMTransferDetailStockCategoryID(Guid? stockCategoryID);
    }
}
