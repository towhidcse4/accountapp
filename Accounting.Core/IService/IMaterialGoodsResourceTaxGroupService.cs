﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMaterialGoodsResourceTaxGroupService : FX.Data.IBaseService<MaterialGoodsResourceTaxGroup, Guid>
    { 
        List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroup(bool isActive);
        List<string> GetListMaterialGoodsResourceTaxGroupCode();
        List<string> GetListOrderFixCodeParentID(Guid? parentID);
        List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupParentID(Guid? parentID);
        int CountListMaterialGoodsResourceTaxGroupParentID(Guid? parentID);
        List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupOrderFixCode(string orderFixCode);
        List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupGrade(int grade);
        List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupOrderFixCodeNotParent(string orderFixCode, Guid? parentID);
        List<MaterialGoodsResourceTaxGroup> GetListMaterialGoodsResourceTaxGroupOrderFixCode();
        List<MaterialGoodsResourceTaxGroup> GetAll_OrderBy();
    }
}
