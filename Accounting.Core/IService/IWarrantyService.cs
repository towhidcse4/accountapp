﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IWarrantyService : FX.Data.IBaseService<Warranty, int>
    {
        /// <summary>
        /// Lấy danh sách các gói bảo hành
        /// </summary>
        /// <returns></returns>
        List<Warranty> getActive();
    }
}
