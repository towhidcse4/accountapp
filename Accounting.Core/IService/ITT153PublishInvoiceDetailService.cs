using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153PublishInvoiceDetailService:FX.Data.IBaseService<TT153PublishInvoiceDetail ,Guid>
    {
        int GetWithMaxToNo(Guid TT153ReportID);
        int GetWithMinFromNo(Guid TT153ReportID);
        List<TT153PublishInvoiceDetail> GetAllWithTT153ReportID(Guid TT153ReportID);
        List<TT153PublishInvoiceDetail> GetAllToDate(DateTime dateTime, Guid TT153ReportID);
        List<TT153PublishInvoiceDetail> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportID);

    }
}
