using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPUncompleteService: FX.Data.IBaseService<CPUncomplete ,Guid>
    {
        List<CPUncomplete> GetList();
    }
}
