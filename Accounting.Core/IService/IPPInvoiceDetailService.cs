using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPInvoiceDetailService:FX.Data.IBaseService<PPInvoiceDetail ,Guid>
    {
       IList<PPInvoiceDetail> GetPPInvoiceDetailbyID(Guid id);

       List<PPInvoiceDetail> GetListPPInvoiceDetailbyID(Guid InvoiceId);

        decimal GetPPInvoiceQuantity(Guid materialGoodsID, Guid contractID);
    }
}
