﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
     public interface ITIAllocationDetailService : FX.Data.IBaseService<TIAllocationDetail, Guid>
    {
         List<TIAllocationDetail> GetTIAllocationDetails(Guid TIAllocationId);
    }

}
