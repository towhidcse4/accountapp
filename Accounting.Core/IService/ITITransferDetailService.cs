﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITITransferDetailService : FX.Data.IBaseService<TITransferDetail, Guid>
    {
        
        List<TITransferDetail> GetByTITransferID(Guid TITransferID);
    }
}
