﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface IViewRSLotNoService : FX.Data.IBaseService<ViewRSLotNo, Guid>
    {
        List<ViewRSLotNo> GetAll_OrderBy();
        List<ViewRSLotNo> GetByMaterialGoodsID(Guid ID);
    }
}
