﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accounting.Core.Domain;
using NHibernate;

namespace Accounting.Core.IService
{
    public interface IFAIncrementDetailService : FX.Data.IBaseService<FAIncrementDetail, Guid>
    {
        // Tìm theo mã tải sản cố định
        ISession GetSession();
        FAIncrementDetail FindByFixedAssetID(Guid iD);
    }
}
