﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ICurrencyService : FX.Data.IBaseService<Currency, String>
    {
        /// <summary>
        /// Lấy ra danh sách Currency theo ID
        /// </summary>
        /// <param name="Id">ID truyền vào</param>
        /// <returns>Danh sách Currency</returns>
        List<Currency> GetListCurrencyID(string Id);

        /// <summary>
        /// Lấy danh sách các loại tiền tệ đang được sử dụng [khanhtq]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<Currency> GetIsActive(bool isActive);

        /// <summary>
        /// Lấy danh sách CurrencyID đang hoạt động và so sánh kí tự viết hoa và kí tự thường
        /// [Longtx] 
        /// </summary>
        /// <param name="currencyLower"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<Currency> GetByListIdCurrenctLower(string currencyLower, bool isActive);

        /// <summary>
        /// Đếm danh sách Currency theo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        int CountListCurrencyID(string Id);

        List<Currency> OrderByID();
    }
}
