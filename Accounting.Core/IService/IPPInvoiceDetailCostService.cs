﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IPPInvoiceDetailCostService : FX.Data.IBaseService<PPInvoiceDetailCost, Guid>
    {
        decimal GetAmountByPPService(Guid pps);

        decimal GetAmountByPPInvoice(Guid pps, Guid ppi, bool type);
    }
}