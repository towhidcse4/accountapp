﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFixedAssetCategoryService : FX.Data.IBaseService<FixedAssetCategory, Guid>
    {
        /// <summary>
        /// Lấy danh sách FixedAssetCategory theo isActive
        /// [haipt]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<FixedAssetCategory> GetListFixedAssetCategoryIsActive(bool isActive);

        /// <summary>
        /// Lấy danh sách FixedAssetCategory theo parentID
        /// [haipt]
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<FixedAssetCategory> GetListFixedAssetCategoryParentID(Guid? parentID);

        /// <summary>
        /// Lấy danh sách OrderFixCode theo ParentID
        /// [haipt]
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<string> GetListOrderFixCodeFixedAssetCategory(Guid? parentID);

        /// <summary>
        /// Lấy ra danh sách FixedAssetCategory theo OrderFixCode
        /// [haipt]
        /// </summary>
        /// <param name="orderFixCode"></param>
        /// <returns></returns>
        List<FixedAssetCategory> GetListFixedAssetCategoryOrderFixCode(string orderFixCode);

        /// <summary>
        /// Lấy ra danh sách FixedAssetCategory theo grade
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        List<FixedAssetCategory> GetListFixedAssetCategoryGrade(int grade);

        /// <summary>
        /// Đếm danh sách FixedAssetCategory theo parentId
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        int CountListFixedAssetCategoryParentID(Guid? parentID);

        /// <summary>
        /// Lấy ra danh sách sắp xếp
        /// </summary>
        /// <returns></returns>
        List<FixedAssetCategory> GetOrderby();
        List<FixedAssetCategory> GetAll_OrderBy();
        Guid? GetGuidFixedAssetCategoryByCode(string code);
    }
}
