﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRepositoryLedgerService : FX.Data.IBaseService<RepositoryLedger, Guid>
    {
        List<RepositoryLedger> GetByListReferenceID(Guid selectID);
        List<RepositoryLedger> GetByMaterialGoodsAndLotNo(Guid ID);
        List<RepositoryLedger> GetByMaterialGoods(Guid ID);
        RepositoryLedger GetByMaterialGoods(Guid ID, Guid RSID);
        decimal GetQuantityByRSIWID(Guid ID);
        #region Lưu sổ kho

        #region Form Hàng mua trả lại
        /// <summary>
        /// Lấy DS lưu vào sổ kho 
        /// Form Hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        /// <returns></returns>
        List<RepositoryLedger> GetListSaveRepositoryLedger(PPDiscountReturn temp,
                                                           List<PPDiscountReturnDetail> listTempDetail,
                                                           RepositoryLedger tempRepos);

        /// <summary>
        /// Hàm lưu sổ kho của form Hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        void SaveRepositoryLedger(PPDiscountReturn temp, List<PPDiscountReturnDetail> listTempDetail,
                                  RepositoryLedger tempRepos);
        #endregion


        #region Form Điều chỉnh tồn kho

        /// <summary>
        /// Lấy DS lưu vào sổ kho 
        /// Form Điều chỉnh tồn kho
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        /// <returns></returns>
        List<RepositoryLedger> GetListSaveRepositoryLedger(RSInwardOutward temp,
                                                                  List<RSInwardOutwardDetail> listTempDetail,
                                                                  RepositoryLedger tempRepo, bool isInward);

        /// <summary>
        /// Hàm lưu sổ kho của form Điều chỉnh tồn kho
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="listTempDetail"></param>
        /// <param name="tempRepos"></param>
        void SaveRepositoryLedger(RSInwardOutward temp, List<RSInwardOutwardDetail> listTempDetail,
                                         RepositoryLedger tempRepos, bool isInward);

        #endregion

        #endregion

        #region Tính và cập nhật giá xuất kho

        /// <summary>
        /// TÍnh giá xuất kho theo phương pháp bình quân cuối kỳ
        /// </summary>
        /// <param name="lstMaterialGoods">danh sách VTHH cần tính giá</param>>
        /// <param name="fromDate">ngày bắt đầu kỳ tính giá</param>
        /// <param name="toDate">ngày kết thúc kỳ tính giá</param>
        bool PricingAndUpdateOW_AverageForEndOfPeriods(List<MaterialGoods> lstMaterialGoods, DateTime fromDate,
            DateTime toDate, Guid ID);
        bool PricingAndUpdateOW_AverageForEndOfPeriods(List<MaterialGoods> lstMaterialGoods, DateTime fromDate,
            DateTime toDate);

        /// <summary>
        /// TÍnh giá xuất kho theo phương pháp bình quân tức thời
        /// </summary>
        /// <param name="lstMaterialGoods">danh sách VTHH cần tính giá</param>>
        /// <param name="fromDate">ngày bắt đầu kỳ tính giá</param>
        /// <param name="toDate">ngày kết thúc kỳ tính giá</param>
        bool PricingAndUpdateOW_AverageForInstantaneous(List<MaterialGoods> lstMaterialGoods, DateTime fromDate,
            DateTime toDate);
        /// <summary>
        /// TÍnh giá xuất kho theo phương pháp nhập trước xuất trước
        /// <param name="lstMaterialGoods">danh sách VTHH cần tính giá</param>>
        /// <param name="fromDate">ngày bắt đầu kỳ tính giá</param>
        /// <param name="toDate">ngày kết thúc kỳ tính giá</param>
        bool PricingAndUpdateOW_InFirstOutFirst(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate);

        bool PricingAndUpdateOW_InFirstOutFirst(List<MaterialGoods> lstMaterialGoods, DateTime fromDate, DateTime toDate, Guid repositoryID);
        #endregion

        /// <summary>
        /// Báo cáo thẻ kho
        /// [DungNA]
        /// </summary>
        /// <param name="Date">Từ Ngày</param>
        /// <param name="PostedDate">Đến Ngày</param>
        /// <param name="MaterialGoodsCode">Mã Sản phẩm</param>        
        /// <returns></returns>
        List<S09DNN> ReportS09DNN(DateTime Date, DateTime PostedDate, List<Guid> MG, Guid resID);
        /// <summary>
        /// [DungNA]
        /// </summary>
        /// <param name="from">Từ Ngày</param>
        /// <param name="to">Đên Ngày</param>
        /// <param name="RepositoryID">Mã Kho</param>
        /// <param name="MaterialGoodsID">Tên hàng hóa</param>
        /// <param name="TotalGroup">Check Cộng Gộp</param>
        /// <returns></returns>
        List<S07DNN> RS07DNN(DateTime from, DateTime to, Guid RepositoryID, List<Guid> MaterialGoodsID, bool TotalGroup);
        /// <summary>
        /// Lấy danh sách vật tư hàng hóa được ghi sổ
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="repositoryID"></param>
        /// <param name="materialGoodsCategoryID"></param>
        /// <returns></returns>
        List<MaterialGoodsReport> getMaterialGoodsInLedger(DateTime fromDate, DateTime toDate, Guid? repositoryID, Guid? materialGoodsCategoryID);
        /// <summary>
        /// Danh sách vật tư hàng hóa ghi sổ
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="AccountID"></param>
        /// <param name="TotalGroup"></param>
        /// <returns></returns>
        List<S07DNN16> RS07DNN16(DateTime from, DateTime to, List<string> AccountNumbers);
    }
}
