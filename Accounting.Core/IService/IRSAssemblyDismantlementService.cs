using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRSAssemblyDismantlementService : FX.Data.IBaseService<RSAssemblyDismantlement, Guid>
    {
        RSAssemblyDismantlement getRSAssemblyDismantlementbyNo(string no);

        List<bool> GetByRecorded();

        List<RSAssemblyDismantlement> getAllOrderbyDate();

        List<RSAssemblyDismantlement> getByType(int TypeID, DateTime fromDate, DateTime toDate);
    }
}
