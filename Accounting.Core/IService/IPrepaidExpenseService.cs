using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPrepaidExpenseService: FX.Data.IBaseService<PrepaidExpense ,Guid>
    {
        List<PrepaidExpense> GetAll_ByDate(DateTime Date);
        List<PrepaidExpense> GetAll_Order();
        List<PrepaidExpenseAllocation_Report> GetReport_ByDate(DateTime FDate, DateTime TDate);
        bool CheckDelete(Guid ID);
    }
}
