﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IRefVoucherService : FX.Data.IBaseService<RefVoucher, Guid>
    {
        List<RefVoucher> GetByRefID2(Guid refID2);
    }
}
