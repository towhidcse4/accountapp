﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.Report;

namespace Accounting.Core.IService
{
    public interface IGVoucherListDetailService : FX.Data.IBaseService<GVoucherListDetail, Guid>
    {
        List<GVoucherListDetail> GetByGvoucherListId(Guid gVoucherListId);
        List<S02C1DNN> GetS02C1DNN(DateTime bgdt, DateTime endt, List<string> lstAcc, bool chk);
    }
}
