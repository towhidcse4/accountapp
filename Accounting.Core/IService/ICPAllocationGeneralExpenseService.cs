using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPAllocationGeneralExpenseService: FX.Data.IBaseService<CPAllocationGeneralExpense ,Guid>
    {
        List<CPAllocationGeneralExpense> GetList();
    }
}
