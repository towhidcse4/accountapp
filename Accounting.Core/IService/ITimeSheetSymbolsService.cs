﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ITimeSheetSymbolsService : FX.Data.IBaseService<TimeSheetSymbols, Guid>
    {
        /// <summary>
        /// Sắp xếp danh sách Ký hiệu chấm công theo mã [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<TimeSheetSymbols> OrderByCode();
        /// <summary>
        /// Lấy tất cả danh sách mã ký hiệu chấm công [khanhtq]
        /// </summary>
        /// <returns></returns>
        List<string> GetCode();
        /// <summary>
        /// Lấy danh sách Ký hiệu chấm công mặc định
        /// </summary>
        /// <param name="isDefault"></param>
        /// <returns></returns>
        List<TimeSheetSymbols> GetIsDefault(bool isDefault);
    }
}
