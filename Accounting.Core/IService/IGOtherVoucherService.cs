﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IGOtherVoucherService : FX.Data.IBaseService<GOtherVoucher, Guid>
    {
        GOtherVoucher GetGOtherVoucherReturnbyNo(string No);
        bool GetGOtherVoucherGetByDate(DateTime dt);
        #region Nghiệp vụ Lưu

        /// <summary>
        /// Thêm mới GOtherVoucher
        /// </summary>
        /// <param name="GOtherVoucher">Đối tượng GOtherVoucher</param>
        /// <param name="TypeGroup"></param>
        void Add(GOtherVoucher GOtherVoucher, int TypeGroup);

        /// <summary>
        /// Lưu sửa GOtherVoucher
        /// </summary>
        /// <param name="GOtherVoucher">Đối tượng GOtherVoucher</param>
        void Edit(GOtherVoucher GOtherVoucher);

        /// <summary>
        /// Lưu sổ cái
        /// </summary>
        /// <param name="No"></param>
        /// <param name="_dsAccount">List Account nếu có (mặc định để trống)</param>
        void Saveleged(string No, IList<Account> _dsAccount = null);
        List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
        List<InsurancePayment> FindInsurancePayment(DateTime date);
        List<WagePayment> FindWagePayment(DateTime date);
        List<WagePaymentDetail> WagePaymentDetail(Guid id);

        #endregion
    }
}
