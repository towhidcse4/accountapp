﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.FADecrements;

namespace Accounting.Core.IService
{
    public interface IFADecrementService : FX.Data.IBaseService<FADecrement, Guid>
    {
        List<GGHachToan> GetListGG(string FACode);
        FADecrement findByRefID(Guid iD);
    }
}
