﻿using System;
using System.Linq;
using System.Collections.Generic;
using Accounting.Core.Domain;
using NHibernate;

namespace Accounting.Core.IService
{
    public interface ITIInitService : FX.Data.IBaseService<TIInit, Guid>
    {
        ISession GetSession();
        /// <summary>
        /// Check exists tools code
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        bool ExistsToolsCode(string req);

        List<TIInit> GetToolsCategory();
        List<TIInit> GetToolsInit();
        Guid? GetGuidBytiinitCode(string ma);
        TIInit GettiinitByCode(string ma);
        bool CheckDeleteTIInit(Guid iD);

        List<Guid> FindByType(int? type, string id, DateTime PostedDate);
        TIInit FindByMaterialGoodsIdAndDepartmentId(Guid iD);
        List<TIInit> FindAllotionTools(DateTime postedDate);        
        bool CheckDeleteIncrement(List<Guid?> tIIncrementIDs);
        DateTime GetDateIncrement(TIInit ID);
        List<TIInit> FindByDeclareType(int type);
        List<TIInitDetail> FindAllocationAllocated(Guid iD, DateTime postedDate);
        decimal FindQuantityForAdjustment(Guid toolsID, Guid fromDepartmentID, DateTime dp);
        List<Department> FindDepartment(Guid toolsID, DateTime dateTime);
    }
}
