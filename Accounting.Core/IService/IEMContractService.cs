﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IEMContractService : FX.Data.IBaseService<EMContract, Guid>
    {
        /// <summary>
        /// Lấy thông tin đối tượng EMContract bằng ID
        /// [PhongNT]
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        EMContract GetByID(Guid id);


        /// <summary>
        /// Lấy ra list các EMContract hoạt động
        /// [DuyTN]
        /// </summary>
        /// <param name="isActive">= true (còn hoạt động), =false (không còn hoạt động)</param>
        /// <returns></returns>
        List<EMContract> GetAll_IsActive(bool isActive);

        List<EMContract> GetAll_OrderBy();

        List<EMContract> GetAllContractBuy();
        List<string> GetCloseReason();
        List<EMContract> GetAllContractSale();
        List<EMContract> GetAllProject();
        Guid? GetGuidEMContractByCode(string code);
    }
}
