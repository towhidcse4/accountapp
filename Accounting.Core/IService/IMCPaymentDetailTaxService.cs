using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentDetailTaxService : FX.Data.IBaseService<MCPaymentDetailTax, Guid>
    {
        IList<MCPaymentDetailTax> getMCPaymentDetailTaxbyID(Guid id);
    }
}
