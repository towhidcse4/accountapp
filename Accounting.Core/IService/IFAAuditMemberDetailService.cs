﻿using Accounting.Core.Domain;
using System;

namespace Accounting.Core.IService
{
    public interface IFAAuditMemberDetailService : FX.Data.IBaseService<FAAuditMemberDetail, Guid>
    {
    }
}