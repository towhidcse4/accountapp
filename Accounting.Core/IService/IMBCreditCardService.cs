﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IMBCreditCardService:FX.Data.IBaseService<MBCreditCard ,Guid>
    {
       /// <summary>
       /// Lấy thông tin Thanh toán bằng thẻ tín dụng theo Số chứng từ [khanhtq]
       /// </summary>
       /// <param name="no">Số chứng từ</param>
       /// <returns>Thông tin thanh toán bằng thẻ tín dụng</returns>
       MBCreditCard GetByNo(string no);
    }
}
