using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRSAssemblyDismantlementDetailService : FX.Data.IBaseService<RSAssemblyDismantlementDetail, Guid>
    {
        IList<RSAssemblyDismantlementDetail> getRSAssemblyDismantlementDetailbyID(Guid id);

        List<RSAssemblyDismantlementDetail> GetListRSAssemblyDismantlementDetailbyID(Guid AssemblyDismantlementID);
    }
}
