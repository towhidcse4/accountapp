using System;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IMCPaymentDetailInsuranceService : FX.Data.IBaseService<MCPaymentDetailInsurance, Guid>
    {
    }
}
