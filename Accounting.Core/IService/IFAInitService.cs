using System;
using System.Collections.Generic;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IFAInitService : FX.Data.IBaseService<FAInit, Guid>
    {
        FAInit FindByFixedAssetID(Guid iD);

        bool CheckDeleteFAInit(Guid iD);
    }
}
