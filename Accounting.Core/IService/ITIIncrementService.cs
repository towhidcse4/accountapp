﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ITIIncrementService : FX.Data.IBaseService<TIIncrement, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách chuỗi FAIncrementCode
        /// </summary>
        /// <returns></returns>
        List<string> GetListTIIncrementCode();
        TIIncrement findByRefID(Guid iD);
    }
}
