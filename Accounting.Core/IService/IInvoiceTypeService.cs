﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IInvoiceTypeService : FX.Data.IBaseService<InvoiceType,Guid>
    {
        /// <summary>
        /// Lấy danh sách Mẫu số hóa đơn đang hoạt động
        /// [Longtx]
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        List<InvoiceType> GetIsActive(bool isActive);

        List<InvoiceType> GetAll_OrderBy();
    }
}
