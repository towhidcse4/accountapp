﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ITypeService : FX.Data.IBaseService<Accounting.Core.Domain.Type, int>
    {
        /// <summary>
        /// Lấy danh sách ID của loại chứng từ
        /// </summary>
        /// <returns></returns>
        List<int> GetListId();
        /// <summary>
        /// Lấy danh sách Loại chứng từ theo Danh sách ID
        /// </summary>
        /// <param name="lstId">Danh sách ID của loại chứng từ</param>
        /// <returns>Danh sách loại chứng từ</returns>
        List<Accounting.Core.Domain.Type> GetByListId(List<int> lstId);
        List<Accounting.Core.Domain.Type> GetAll_OrderBy();
    }
}
