﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IBankService : FX.Data.IBaseService<Bank, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách Bank theo isActive
        /// </summary>
        /// <param name="isActive">Trạng thái active</param>
        /// <returns></returns>
        List<Bank> GetListBankIsActive(bool isActive);

        /// <summary>
        /// Lấy ra danh sách chuỗi BankCode
        /// </summary>
        /// <returns></returns>
        List<string> GetListBankCode();

        /// <summary>
        /// Lấy danh sách sắp xếp
        /// </summary>
        /// <returns></returns>
        List<Bank> GetOrderby();
        List<Bank> GetAll_OrderBy();
    }
}
