using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
   public interface ISAOrderDetailService:FX.Data.IBaseService<SAOrderDetail ,Guid>
   {
       List<SAOrderDetail> GetSAOrderDetailByOrderID(Guid OrderID);
       List<SAOrderDetail> GetSAOrderDetailBySAOrderCode(Guid? SAOrderID, Guid? ContractID);
        List<SAOrderDetail> GetSAOrderDetailByContractID(Guid ContractID);
    }
}
