using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IVoucherPatternsReportService : FX.Data.IBaseService<VoucherPatternsReport, int>
    {
        List<VoucherPatternsReport> GetAll_ByTypeID(int typeID);
    }
}
