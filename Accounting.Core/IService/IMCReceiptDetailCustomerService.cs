﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IMCReceiptDetailCustomerService : FX.Data.IBaseService<MCReceiptDetailCustomer, Guid>
    {
        /// <summary>
        /// Lấy danh sách các MCReceiptDetailCustomer theo SaleInvoiceID
        /// [DUYTN]
        /// </summary>
        /// <param name="SaleInvoiceID">là SaleInvoiceID truyền vào</param>
        /// <returns>tập các MCReceiptDetailCustomer, null nếu bị lỗi</returns>
        List<MCReceiptDetailCustomer> GetAll_BySaleInvoiceID(Guid SaleInvoiceID);

        /// <summary>
        /// Đếm tổng số dòng dữ liệu MCReceiptDetailCustomer theo ByRemainingAmount
        /// [DUYTN]
        /// </summary>
        /// <param name="RemainingAmount">là ByRemainingAmount truyền vào</param>
        /// <returns>tập các MCReceiptDetailCustomer, null nếu bị lỗi</returns>
        int CountByRemainingAmount(int RemainingAmount);
    }
}
