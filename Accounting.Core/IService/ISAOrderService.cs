using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
   public interface ISAOrderService:FX.Data.IBaseService<SAOrder ,Guid>
    {
        SAOrder GetSAOrderByID(Guid ID);
        List<SAOrder> GetAllOrderByNo();
        List<SAOrder> GetAllOrderByNoAndNotInContract(Guid ID);
        List<SAOrder> GetOrderByContractID(Guid contractID);
        SAOrder GetOrderByNo(string no);
        Guid GetIDByNo(string no);
        List<SAOrder> GetOrderByContract(Guid contractID);
        List<Guid> GetOrderIDByContractID(Guid contractID);
    }
}
