﻿using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IBudgetItemService : FX.Data.IBaseService<BudgetItem, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách BudgetItem có cùng cha
        /// [haipt]
        /// </summary>
        /// <param name="parentID">ID cha</param>
        /// <returns></returns>
        List<BudgetItem> GetListBudgetItemParentID(Guid? parentID);

        /// <summary>
        /// Lấy ra danh sách BudgetItem theo BudgetItemType
        /// [haipt]
        /// </summary>
        /// <param name="budgetItemType"></param>
        /// <returns></returns>
        List<BudgetItem> GetListBudgetItemBudgetItemType(int budgetItemType);

        /// <summary>
        /// Lấy ra danh sách OrderFixCode của các BudgetItem có cùng cha
        /// [haipt]
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<string> GetListOrderFixCodeBudgetItemChild(Guid? parentID);

        /// <summary>
        /// Lấy danh sách BudgetItem chung OrderFixCode
        /// [haipt]
        /// </summary>
        /// <param name="orderFixCode"></param>
        /// <returns></returns>
        List<BudgetItem> GetListBudgetItemOrderFixCode(string orderFixCode);

        /// <summary>
        /// Lấy ra danh sách BudgetItem theo grade
        /// [haipt]
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        List<BudgetItem> GetListBudgetItemGrade(int grade);

        /// <summary>
        /// Lấy ra danh sách BudgetItemType
        /// [haipt]
        /// </summary>
        /// <returns></returns>
        List<int> GetListBudgetItemType();

        /// <summary>
        /// Lấy ra danh sách BudgetItemCode
        /// [haipt]
        /// </summary>
        /// <returns></returns>
        List<string> GetListBudgetItemCode();

        /// <summary>
        /// Lấy ra danh sách BudgetItem theo BudgetItemName với điều kiện startWith
        /// </summary>
        /// <param name="startWith">Chuỗi startWith</param>
        /// <returns></returns>
        List<BudgetItem> GetListBudgetItemBudgetItemNameStartWith(string startWith);
		
		List<BudgetItem> GetBudgetItemCode(bool isActive);

        /// <summary>
        /// Đếm số BudgetItem theo parentID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        int CountBudgetItemParentID(Guid? parentID);


        /// <summary>
        /// Lấy danh sách các BudgetItem theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách BudgetItem được sắp xếp theo dạng cây cha con</returns>
        List<BudgetItem> GetByActive_OrderByTreeIsParentNode(bool isActive);

        /// <summary>
        /// Lấy danh sách các BudgetItem và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách BudgetItem được sắp xếp theo dạng cây cha con</returns>
        List<BudgetItem> GetAll_OrderByTreeIsParentNode();
        List<BudgetItem> GetAll_OrderBy();
        Guid GetGuidBudgetItemByCode(string code);
    }
}
