using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IPSTimeSheetSummaryDetailService : FX.Data.IBaseService<PSTimeSheetSummaryDetail, Guid>
    {
        List<PSTimeSheetSummaryDetail> FindEmployeeByDepartment(IList<Guid> departments);
    }
}
