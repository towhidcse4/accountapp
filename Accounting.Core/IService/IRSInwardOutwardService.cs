﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;
using System.ComponentModel;

namespace Accounting.Core.IService
{
    public interface IRSInwardOutwardService : FX.Data.IBaseService<RSInwardOutward, Guid>
    {
        RSInwardOutward getMCRSInwardOutwardbyNo(string no);
        List<bool> GetRecorded();

        #region Lưu bảng nhập xuất kho

        #region Form Hàng mua trả lại

        /// <summary>
        /// Lấy DS để lưu vào bảng RSInwardOutward [Form Hàng mua trả lại] 
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="tempRSOutward">truyền bổ sung các trường [ContactName]|[Reason]|[MaterialQuantity]</param>
        /// <returns></returns>
        RSInwardOutward GetSaveRSInwardOutward(PPDiscountReturn temp, RSInwardOutward tempRSOutward, bool isAdd = true, RSInwardOutward tempRSInwardOutward = null);

        /// <summary>
        /// Hàm lưu vào bảng RSInwardOutward của Form hàng mua trả lại
        /// [Huy Anh]
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="tempRSOutward">truyền bổ sung các trường [ContactName]|[Reason]|[MaterialQuantity]</param>
        void AddRSInwardOutward(RSInwardOutward tempRSOutward);

        void EditRSInwardOutward(RSInwardOutward tempRSOutward);

        #endregion

        #endregion

        #region Cập nhật kho thành phẩm

        bool GetListInwardOutwardForUpdatingPrice(List<MaterialGoods> mgInput,
            List<Repository> reposInput, DateTime fromDate, DateTime toDate, bool isdd);


        #endregion

        List<TIOriginalVoucher> getOriginalVoucher(DateTime fromDate, DateTime toDate, String currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
    }
}
