﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IRSInwardOutwardDetailService : FX.Data.IBaseService<RSInwardOutwardDetail, Guid>
    {
        IList<RSInwardOutwardDetail> getRSInwardOutwardDetaillbyID(Guid id);

        /// <summary>
        /// Lấy danh sách RSInwardOutwardID từ RSInwardOutwardDetail
        /// [Longtx] 
        /// </summary>
        /// <param name="InwardOutwarID"></param>
        /// <returns></returns>
        List<RSInwardOutwardDetail> GetListTempDetailID(Guid InwardOutwarID);

        #region Lưu bảng RSInwardOutwardDetail

        #region Form Hàng mua trả lại

        /// <summary>
        /// Lấy DS lưu bảng RSInwardOutwardDetail
        /// </summary>
        /// <param name="temp">Đối tượng PPDiscountReturn</param>
        /// <param name="listTempDetail">Ds PPDiscountReturnDetail theo đối tượng PPDiscountReturn</param>
        /// <param name="rsOutwardID">ID phiếu nhập kho</param>
        /// <returns></returns>
        List<RSInwardOutwardDetail> GetListSaveRSInwardOutwardDetail(PPDiscountReturn temp,
                                                                     List<PPDiscountReturnDetail> listTempDetail,
                                                                     Guid rsOutwardID, bool isAdd = true, List<RSInwardOutwardDetail> listRSOutwardDetail = null);


        #endregion

        #endregion
    }
}
