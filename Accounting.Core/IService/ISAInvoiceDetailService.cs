using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ISAInvoiceDetailService:FX.Data.IBaseService<SAInvoiceDetail ,Guid>
    {
        decimal GetSAInvoiceQuantity(Guid materialGoodsID, Guid contractID);
    }
}
