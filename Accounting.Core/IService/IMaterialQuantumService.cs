﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IMaterialQuantumService : FX.Data.IBaseService<MaterialQuantum, Guid>
    {
        /// <summary>
        /// Lấy ra danh sách MaterialQuantum đã sắp xếp theo thứ tự tăng dần MaterialQuantumCode
        /// [DUYTN], sửa ngày 07/04/2014
        /// </summary>
        /// <returns>tập các MaterialQuantum, null nếu bị lỗi</returns>
        List<MaterialQuantum> GetOrderBy_MaterialQuantumCode();

        /// <summary>
        /// Lấy ra danh sách MaterialQuantumCode
        /// [DUYTN]
        /// </summary>
        /// <returns>tập các MaterialQuantumCode, null nếu bị lỗi</returns>
        List<string> GetMaterialQuantumCode();
    }
}
