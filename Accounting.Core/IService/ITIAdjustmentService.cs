﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface ITIAdjustmentService : FX.Data.IBaseService<TIAdjustment, Guid>
    {
        TIAdjustment GetByNo(string no);
    }
}
