﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IViewEInvoiceService : FX.Data.IBaseService<ViewEInvoice, Guid>
    {
    }
}
