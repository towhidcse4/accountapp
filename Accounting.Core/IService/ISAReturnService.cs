using System;
using System.Collections.Generic;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ISAReturnService : FX.Data.IBaseService<SAReturn, Guid>
    {
        SAReturn GetByNo(string no);
        int GetMaxToNo(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
        List<SAReturn> GetListSAReturnTT153(Guid InvoiceTypeID, int InvoiceForm, String InvoiceTemplate, string InvoiceSeries);
    }
}
