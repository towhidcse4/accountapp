using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPDiscountReturnDetailService:FX.Data.IBaseService<PPDiscountReturnDetail ,Guid>
   {
       IList<PPDiscountReturnDetail> GetPPDiscountReturnDetailbyID(Guid id);
        decimal GetPPDiscountReturnDetailQuantity(Guid materialGoodsID, Guid contractID);
    }
}
