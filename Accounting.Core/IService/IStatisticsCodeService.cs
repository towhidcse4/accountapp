﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IStatisticsCodeService : FX.Data.IBaseService<StatisticsCode, Guid>
    {
        List<StatisticsCode> OrderByCode();
        List<string> GListOrderFixCodeByParentID(Guid? ParentID);
        List<StatisticsCode> GListByOrderFixCode(string stringStart);
        List<StatisticsCode> GListByParrentID(Guid ParentID);
        List<StatisticsCode> GListByGrade(int Grade);
        List<StatisticsCode> GListChilds(string OrderFixCode, Guid ID);
        List<string> GListStatisticsCode_();

        /// <summary>
        /// Lấy danh sách StatisticsCode từ StatisticsCode theo IsActive= true
        /// [Longtx]
        /// </summary>
        /// <param name="isActive">StatisticsCode</param>
        /// <returns>danh sách StatisticsCode có IsActive= true </returns>
        List<StatisticsCode> GetStatisticsCodeBool(bool isActive);

        
        /// <summary>
        /// Lấy danh sách các StatisticsCode theo IsActive và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <param name="isActive">là IsActive được truyền vào (true or false) </param>
        /// <returns>List danh sách StatisticsCode được sắp xếp theo dạng cây cha con</returns>
        List<StatisticsCode> GetByActive_OrderByTreeIsParentNode(bool isActive);


        /// <summary>
        /// Lấy danh sách các StatisticsCode và được sắp xếp theo Cây cha con
        /// [Huy Anh]
        /// </summary>
        /// <returns>List danh sách StatisticsCode được sắp xếp theo dạng cây cha con</returns>
        List<StatisticsCode> GetAll_OrderByTreeIsParentNode();
        Guid GetGuidStatisticsCodeByCode(string code);
    }
}
