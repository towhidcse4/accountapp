﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface ITemplateDetailService : FX.Data.IBaseService<TemplateDetail, Guid>
    {
        /// <summary>
        /// các "bảng" trong một "mẫu giao diện"
        /// </summary>
        /// <param name="TemplateID"></param>
        /// <returns></returns>
        List<TemplateDetail> getTableinTemplate(Guid TemplateID);
    }
}

