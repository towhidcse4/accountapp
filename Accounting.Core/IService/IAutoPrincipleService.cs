﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IAutoPrincipleService : FX.Data.IBaseService<AutoPrinciple, Guid>
    {
        List<AutoPrinciple> GetAll_OrderBy();
        /// <summary>
        /// Lấy danh sách AutoPriceple đang active
        /// </summary>
        /// <returns></returns>
        List<AutoPrinciple> GetAll_Active();
        /// <summary>
        /// Lấy danh sách lý do theo type
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns></returns>
        List<AutoPrinciple> getByTypeID(int typeID);
    }
}
