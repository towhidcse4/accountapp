﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFATransferService : FX.Data.IBaseService<FATransfer, Guid>
    {
        FATransfer GetByNo(string no);
    }
}
