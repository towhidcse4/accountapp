﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFixedAssetDetailService:FX.Data.IBaseService<FixedAssetDetail, Guid>
    {
        IList<FixedAssetDetail> getFixedAssetDetailbyID(Guid id);
        List<FixedAssetDetail> getFixedAssetDetailbyIDlst(Guid id);
    }
}
