﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface IFixedAssetAccessoriesService : FX.Data.IBaseService<FixedAssetAccessories,Guid>
    {
    }
}
