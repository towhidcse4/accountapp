﻿using System;
using Accounting.Core.Domain;
using System.Collections.Generic;

namespace Accounting.Core.IService
{
    public interface IMCReceiptService : FX.Data.IBaseService<MCReceipt, Guid>
    {
        /// <summary>
        /// Lấy Phiếu thu theo số chứng từ
        /// </summary>
        /// <param name="no">Số chứng từ</param>
        /// <returns></returns>
        MCReceipt GetByNo(string no);

        //void CreateNew()
        /// <summary>
        /// Lấy ra danh sách các MCReceipt theo TypeID
        /// [DUYTN]
        /// </summary>
        /// <param name="TypeID">là TypeID truyền vào</param>
        /// <returns>tập các MCReceipt, null nếu bị lỗi</returns>
        List<MCReceipt> GetAll_ByTypeID(int TypeID);
        MCReceipt findByAuditID(Guid ID);
        List<TIOriginalVoucher> getOriginalVoucher(DateTime dateTime1, DateTime dateTime2, string currencyID = null);
        List<TIOriginalVoucher> FindByListID(string originalVoucher);
    }
}
