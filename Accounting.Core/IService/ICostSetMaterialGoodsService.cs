﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.Domain;

namespace Accounting.Core.IService
{
    public interface ICostSetMaterialGoodsService : FX.Data.IBaseService<CostSetMaterialGoods,Guid>
    {
        /// <summary>
        /// Lấy ra danh sách CostSetMaterialGoods theo CostSetID
        /// </summary>
        /// <param name="costSetID">CostSet ID truyền vào</param>
        /// <returns>Danh sách CostSetMaterialGoods</returns>
        List<CostSetMaterialGoods> GetCostSetMaterialGoodsCostSetID(Guid? costSetID);
    }
}
