using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ITT153LostInvoiceService:FX.Data.IBaseService<TT153LostInvoice ,Guid>
    {
        List<int> GetAllFromDateToDate(DateTime From, DateTime To, Guid TT153ReportID);
    }
}
