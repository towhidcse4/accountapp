﻿using Accounting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Core.IService
{
    public interface IViewVouchersCloseBook1Service : FX.Data.IBaseService<ViewVouchersCloseBook1, Guid>
    {
        List<ViewVouchersCloseBook1> GetAllVoucherUnRecorded1(DateTime newDBDateClosed, Guid? branchId = null);
        List<ViewVouchersCloseBook1> GetAllVoucherAccountingObject1();
        ViewVouchersCloseBook1 GetByNo1(string No);
        bool CheckNoExist1(string No);
    }
}
