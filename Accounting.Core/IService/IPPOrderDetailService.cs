using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface IPPOrderDetailService:FX.Data.IBaseService<PPOrderDetail ,Guid>
   {
       IList<PPOrderDetail> GetPPOrderDetailbyID(Guid id);
       List<PPOrderDetail> GetPPOrderDetailbyPPOrderID(Guid id);
       List<PPOrderDetail> GetPPOrderDetailbyContractID(Guid contractID);
        List<PPOrderDetail> GetPPOrderDetailByPPOrderCode(Guid PPOrderID, Guid? ContractID);
    }
}
