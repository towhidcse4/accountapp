﻿using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IDateTimeCacheHistoryService : FX.Data.IBaseService<DateTimeCacheHistory, Guid>
    {
        DateTimeCacheHistory GetDateTimeCacheHistoryByTypeID(int? typeID, Guid iDUser);
        DateTimeCacheHistory GetDateTimeCacheHistoryBySubSystemCode(string subSystemCode, Guid iDUser);
        List<DateTimeCacheHistory> GetAllDateTimeCacheHistory();
    }
}
