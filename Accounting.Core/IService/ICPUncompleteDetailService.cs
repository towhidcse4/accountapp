using System;
using System.Collections.Generic;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
   public interface ICPUncompleteDetailService: FX.Data.IBaseService<CPUncompleteDetail ,Guid>
    {
        List<CPUncompleteDetail> GetList();
    }
}
