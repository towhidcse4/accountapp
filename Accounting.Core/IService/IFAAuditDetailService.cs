using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Accounting.Core.Domain;
namespace Accounting.Core.IService
{
    public interface IFAAuditDetailService : FX.Data.IBaseService<FAAuditDetail, Guid>
    {
        List<FAAuditDetail> FindAllFaInitAndIncrementFixedAsset(DateTime postedDate, DateTime inventoryDate);
    }
}
