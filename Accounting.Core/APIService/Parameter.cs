﻿
namespace Accounting.Core
{
    public class Parameter
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public ParamType Type { get; set; }
    }
    public enum ParamType
    {
        //Cookie,
        //GetOrPost,
        //HttpHeader,
        //RequestBody,
        UrlSegment,
        QueryString,
    }
}
