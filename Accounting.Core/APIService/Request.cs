﻿using System.Collections.Generic;

namespace Accounting.Core
{
    public class Request
    {
        public string Ikey { get; set; }
        public string[] Ikeys { get; set; }
        public string Pattern { get; set; }
        public string Serial { get; set; }
        public string InvNo { get; set; }
        public string XmlData { get; set; }
        public int? ComId { get; set; }
        public string TaxCode { get; set; }
        public string Attachment { get; set; }
        public int? Convert { get; set; }
        public string SignDate { get; set; }
        public int Quantity { get; set; }
        public Dictionary<string, string> IkeyEmail { get; set; }
        public bool AutoNo { get; set; }
        public string CertString { get; set; }
        public Dictionary<string, string> Signature { get; set; }
        public Dictionary<string, string> IkeyDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int? Option { get; set; }
        public int? typeHDDC_VNPT { get; set; }
        public Request()
        {
            IkeyEmail = new Dictionary<string, string>();
            IkeyDate = new Dictionary<string, string>();
        }
    }
}
