﻿using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Deserializers;

namespace Accounting.Core
{
    public class RestApiService
    {
        private readonly EasyClient _client;
        private readonly NetworkCredential _credential;

        public RestApiService(string host, string username, string password)
        {
            if (string.IsNullOrWhiteSpace(host)) throw new ArgumentException("Host can not be empty", nameof(host));
            if (string.IsNullOrWhiteSpace(username)) throw new ArgumentException("Username can not be empty", nameof(username));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Password can not be empty", nameof(password));

            _credential = new NetworkCredential(username, password, host);
            try
            {
                _client = new EasyClient(host);
            }
            catch (Exception e)
            {
                
            }
            
           
        }
        
        #region synchronous
        public Response Post(string resource, Request request)
        {
            IRestResponse<Response> restResponse = CallApi<Response>(resource, Method.POST, request);
            return ExtractResponse(restResponse);
        }

        public Response Get(string resource, params Parameter[] parameters)
        {
            IRestResponse<Response> restResponse = CallApi<Response>(resource, Method.GET, parameters);
            return ExtractResponse(restResponse);
        }

        public ResponseFile Download(string resource, Request request)
        {
            var restReponse = CallApi(resource, Method.POST, request);
            ResponseFile response = new ResponseFile();
            response.HttpStatusCode = restReponse.StatusCode;
            if (restReponse.StatusCode == HttpStatusCode.OK)
            {
                response.RawBytes = restReponse.RawBytes;
                var value = restReponse.Headers.ToList().First(_ => _.Name == "Content-Disposition")?.ToString();
                //response.FileName = new ContentDisposition().FileName;
                response.FileName = value.Split(';').FirstOrDefault(n => n.Contains("filename=")).Replace("filename=","");
                response.Content = restReponse.Content;
                response.Message = restReponse.ErrorMessage;
            }
            else
            {
                response.Content = restReponse.Content;
                try
                {
                    var mes = JObject.Parse(restReponse.Content);
                    response.Message = mes["Message"].ToString();
                }
                catch (Exception ex)
                {
                    response.Message = "Error get Message";
                }
            }
            return response;
        }

        private IRestResponse<T> CallApi<T>(string resource, Method method, object data) where T: new()
        {
            if (string.IsNullOrWhiteSpace(resource)) throw new ArgumentException("Resource can not be empty", nameof(resource));
            if (_client == null) throw new Exception("EasyClient hasn't been initialized yet");

            var request = new RestRequest(resource) { Method = method };
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authentication", GenerateToken(method, _credential.UserName, _credential.Password));
            AddRequestData(request, data);
            return _client.Execute<T>(request);
        }

        private IRestResponse CallApi(string resource, Method method, object data)
        {
            if (string.IsNullOrWhiteSpace(resource)) throw new ArgumentException("Resource can not be empty", nameof(resource));
            if (_client == null) throw new Exception("EasyClient hasn't been initialized yet");

            var request = new RestRequest(resource) { Method = method };
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authentication", GenerateToken(method, _credential.UserName, _credential.Password));
            AddRequestData(request, data);
            return _client.Execute(request);
        } 
        #endregion

        #region asynchronous
        public async Task<Response> PostAsync(string resource, Request request)
        {
            IRestResponse<Response> restResponse = await CallApiAsync<Response>(resource, Method.POST, request);
            return ExtractResponse(restResponse);
        }

        public async Task<Response> GetAsync(string resource, params Parameter[] parameters)
        {
            IRestResponse<Response> restResponse = await CallApiAsync<Response>(resource, Method.GET, parameters);
            return ExtractResponse(restResponse);
        }

        private async Task<IRestResponse<T>> CallApiAsync<T>(string resource, Method method, object data) where T : new()
        {
            if (string.IsNullOrWhiteSpace(resource)) throw new ArgumentException("Resource can not be empty", nameof(resource));
            if (_client == null) throw new Exception("EasyClient hasn't been initialized yet");

            var request = new RestRequest(resource) { Method = method };
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authentication", GenerateToken(method, _credential.UserName, _credential.Password));
            AddRequestData(request, data);
            return await _client.ExecuteTaskAsync<T>(request);
        }

        private async Task<IRestResponse> CallApiAsync(string resource, Method method, object data)
        {
            if (string.IsNullOrWhiteSpace(resource)) throw new ArgumentException("Resource can not be empty", nameof(resource));
            if (_client == null) throw new Exception("EasyClient hasn't been initialized yet");

            var request = new RestRequest(resource) { Method = method };
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authentication", GenerateToken(method, _credential.UserName, _credential.Password));
            AddRequestData(request, data);
            return await _client.ExecuteTaskAsync(request);
        } 
        #endregion

        private static string GenerateToken(Method method, string username, string password)
        {
            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - epochStart;
            string timestamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();
            string nonce = Guid.NewGuid().ToString("N").ToLower();
            string signatureRawData = $"{method.ToString().ToUpper()}{timestamp}{nonce}";

            using (MD5 md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signatureRawData));
                var signature = Convert.ToBase64String(hash);
                return $"{signature}:{nonce}:{timestamp}:{username}:{password}";
            }
        }

        private static Response ExtractResponse(IRestResponse<Response> response)
        {
            //if (response.ErrorException != null)
            //{
            //    throw exception;
            //}



            if (response.Data == null)
            {
                string message = null;
                switch (response.StatusCode)
                {
                    case HttpStatusCode.RequestTimeout:
                    case HttpStatusCode.GatewayTimeout:
                        message = response.ErrorMessage;
                        break;
                    case HttpStatusCode.NotFound:
                        message = "Not Found";
                        break;
                    case 0: // consider as RequestTimeout
                        response.StatusCode = HttpStatusCode.RequestTimeout;
                        message = "Time out";
                        break;
                    default:
                        message = response.Content;
                        break;
                }
                response.Data = new Response
                {
                    Message = message,
                    Status = null
                };
            }



            response.Data.HttpStatusCode = response.StatusCode;

            return response.Data;
        }

        private static void AddRequestData(IRestRequest request, object data)
        {
            switch (data)
            {
                case IEnumerable parameters:
                    foreach (Parameter p in parameters)
                    {
                        if (p.Type == ParamType.UrlSegment)
                            request.AddUrlSegment(p.Name, p.Value);
                        request.AddQueryParameter(p.Name, p.Value);
                    }
                    break;
                case Parameter p:
                    if (p.Type == ParamType.UrlSegment)
                        request.AddUrlSegment(p.Name, p.Value);
                    request.AddQueryParameter(p.Name, p.Value);
                    break;
                default:
                    request.AddJsonBody(data);
                    break;
            }
        }
    }
}
