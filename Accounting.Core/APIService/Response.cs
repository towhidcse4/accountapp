﻿using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Accounting.Core
{
    public class Response
    {
        //[JsonIgnore]
        public HttpStatusCode HttpStatusCode { get; set; }

        public int? Status { get; set; }

        public string Message { get; set; }

        //[JsonConverter(typeof(ConcreteConverter<ResponseData>))]
        public ResponseData Data { get; set; }
    }
    public class ResponseData
    {
        public string Pattern { get; set; }
        public string Serial { get; set; }
        public Dictionary<string, string> KeyInvoiceNo { get; set; }
        public Dictionary<string, string> KeyInvoiceMsg { get; set; }
        public string Html { get; set; }
        public int? InvoiceStatus { get; set; }
        public List<int> InvoiceNo { get; set; }
        public Dictionary<string, string> DigestData { set; get; }
        public string Ikey { get; set; }
        public string XmlReport { get; set; }
        public string Invoices { get; set; }
    }

    public class ResponseFile
    {
        public byte[] RawBytes { get; set; }
        public string FileName { get; set; }
        public string Content { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public string Message { get; set; }

    }
}
