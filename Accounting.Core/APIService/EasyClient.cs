﻿using System;
using System.Net;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Deserializers;

namespace Accounting.Core
{
    public sealed class EasyClient : RestSharp.RestClient
    {
        public EasyClient(string baseUrl)
        {
            BaseUrl = new Uri(baseUrl);
        }

        public EasyClient(IDeserializer derializer, string baseUrl)
        {
            AddHandler("application/json", derializer);
            AddHandler("text/json", derializer);
            AddHandler("text/x-json", derializer);
            BaseUrl = new Uri(baseUrl);
        }

        public override IRestResponse Execute(IRestRequest request)
        {
            var response = base.Execute(request);
            TimeoutCheck(response);
            return response;
        }
        public override IRestResponse<T> Execute<T>(IRestRequest request)
        {
            var response = base.Execute<T>(request);
            TimeoutCheck(response);
            return response;
        }

        public override async Task<IRestResponse> ExecuteTaskAsync(IRestRequest request)
        {
            var response = await base.ExecuteTaskAsync(request);
            TimeoutCheck(response);
            return response;
        }
        public override async Task<IRestResponse<T>> ExecuteTaskAsync<T>(IRestRequest request)
        {
            var response = await base.ExecuteTaskAsync<T>(request);
            TimeoutCheck(response);
            return response;
        }

        private static void TimeoutCheck(IRestResponse response)
        {
            if (response.StatusCode == 0)
            {
                response.StatusCode = HttpStatusCode.RequestTimeout;
            }
        }

        //public T Get<T>(IRestRequest request) where T : new()
        //{
        //    var response = Execute<T>(request);
        //    if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //    {
        //        return response.Data;
        //    }
        //    else
        //    {
        //        //LogError(BaseUrl, request, response);
        //        return default(T);
        //    }
        //}
    }
}
