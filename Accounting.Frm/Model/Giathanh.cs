﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Model
{
   public class Giathanh
    {
        private string materialGoodsCode;

        public string MaterialGoodsCode
        {
            get { return materialGoodsCode; }
            set { materialGoodsCode = value; }
        }
        private string materialGoodsName;

        public string MaterialGoodsName
        {
            get { return materialGoodsName; }
            set { materialGoodsName = value; }
        }
        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
}
