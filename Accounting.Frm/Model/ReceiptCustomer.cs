﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Model
{
    class ReceiptCustomer
    {
        public List<Customer> GeCustomer()
        {
            Customer c1 = new Customer
                              {
                                  Code = "0001",
                                  Name = "Nguyễn Tuấn Anh",
                                  Address = "Hoàng Mai",
                                  Phone = "0977156899",
                                  Fax = "043-987549",
                                  Email = "diciem001@gmail.com",
                                  SoConPhaiThu = 500000,
                                  SoDaThu = 200000,
                                  SoDuDauNam = 5000000,
                                  SoPhatSinh = 1000000
                              };
            Receipt r1c1 = new Receipt
                              {
                                  VouchersDate = DateTime.Now,
                                  No = "0001",
                                  InvoiceNo = "2000",
                                  PostedDate = DateTime.Now,
                                  SoDaThu = 5000000,
                                  SoPhatSinh = 10000000,
                                  TypeId = 14,
                                  TypeName = "Hóa đơn bán hàng"
                              };
            Receipt r2c1 = new Receipt
            {
                VouchersDate = DateTime.Now,
                No = "0002",
                InvoiceNo = "2001",
                PostedDate = DateTime.Now,
                SoDaThu = 7500000,
                SoPhatSinh = 40000000,
                TypeId = 14,
                TypeName = "Phiếu thu tiền khách hàng"
            };
            c1.Receipts.Add(r1c1);
            c1.Receipts.Add(r2c1);
            Customer c2 = new Customer
                              {
                                  Code = "0002",
                                  Name = "Ngô Văn Tuấn",
                                  Address = "Cầu Giấy",
                                  Phone = "0912094460",
                                  Fax = "043-123456",
                                  Email = "diciem006@gmail.com",
                                  SoConPhaiThu = 550000,
                                  SoDaThu = 220000,
                                  SoDuDauNam = 5900000,
                                  SoPhatSinh = 2000000
                              };
            return new List<Customer> { c1, c2 };
        }
    }
    class Customer
    {
        private string _code;
        private string _name;
        private string _address;
        private string _phone;
        private string _fax;
        private string _email;
        private decimal _soDuDauNam;
        private decimal _soPhatSinh;
        private decimal _soDaThu;
        private decimal _soConPhaiThu;
        private List<Receipt> _receipts = new List<Receipt>();

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public virtual string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public virtual string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        public virtual string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public virtual decimal SoDuDauNam
        {
            get { return _soDuDauNam; }
            set { _soDuDauNam = value; }
        }

        public virtual decimal SoPhatSinh
        {
            get { return _soPhatSinh; }
            set { _soPhatSinh = value; }
        }

        public virtual decimal SoDaThu
        {
            get { return _soDaThu; }
            set { _soDaThu = value; }
        }

        public virtual decimal SoConPhaiThu
        {
            get { return _soConPhaiThu; }
            set { _soConPhaiThu = value; }
        }

        public virtual List<Receipt> Receipts
        {
            get { return _receipts; }
            set { _receipts = value; }
        }
    }

    class Receipt
    {
        private DateTime _vouchersDate; // ngày chứng từ
        private string _no; // số chứng từ
        private string _invoiceNo; // số hóa đơn
        private DateTime _postedDate; // ngày hạch toán
        private string _typeName; // loại chứng từ
        private int _typeId; // id chứng từ
        private decimal _soPhatSinh;
        private decimal _soDaThu;

        public virtual DateTime VouchersDate
        {
            get { return _vouchersDate; }
            set { _vouchersDate = value; }
        }

        public virtual string No
        {
            get { return _no; }
            set { _no = value; }
        }

        public virtual string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        public virtual DateTime PostedDate
        {
            get { return _postedDate; }
            set { _postedDate = value; }
        }

        public virtual string TypeName
        {
            get { return _typeName; }
            set { _typeName = value; }
        }

        public virtual int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        public virtual decimal SoDaThu
        {
            get { return _soDaThu; }
            set { _soDaThu = value; }
        }

        public virtual decimal SoPhatSinh
        {
            get { return _soPhatSinh; }
            set { _soPhatSinh = value; }
        }
    }
}
