﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Model
{
     public class tonghop
    {
        private string _Loaicophan;

        public string Loaicophan
        {
            get { return _Loaicophan; }
            set { _Loaicophan = value; }
        }

        private DateTime _Ngay;

        public DateTime Ngay
        {
            get { return _Ngay; }
            set { _Ngay = value; }
        }
        private decimal _Tang;

        public decimal Tang
        {
            get { return _Tang; }
            set { _Tang = value; }
        }
        private decimal _Giam;

        public decimal Giam
        {
            get { return _Giam; }
            set { _Giam = value; }
        }
        private decimal _Socophan;

        public decimal Socophan
        {
            get { return _Socophan; }
            set { _Socophan = value; }
        }
        private decimal _Giatricophan;

        public decimal Giatricophan
        {
            get { return _Giatricophan; }
            set { _Giatricophan = value; }
        }
        private string _Lydothaydoi;

        public string Lydothaydoi
        {
            get { return _Lydothaydoi; }
            set { _Lydothaydoi = value; }
        }
    }
}
