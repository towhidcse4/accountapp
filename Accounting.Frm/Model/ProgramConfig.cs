﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accounting.Core.IService;
using FX.Core;
using Accounting.Core.Domain;

namespace Accounting.Model
{
    public class ProgramConfig
    {
        private int _Number;
        public int Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        public ProgramConfig(List<CustomerConfig> listCustomerConfig)
        {
            Number = 4;
        }
    }
}
