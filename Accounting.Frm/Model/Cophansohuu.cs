﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Accounting.Model
{
    public class Cophansohuu
    {
        private string _Loaicophan;

        public string Loaicophan
         {
           get { return _Loaicophan; }
            set { _Loaicophan = value; }
         }
        private string _Hinhthucuudai;

        public string Hinhthucuudai
        {
            get { return _Hinhthucuudai; }
            set { _Hinhthucuudai = value; }
        }
        private string _Dieukienchuyennhuong;

        public string Dieukienchuyennhuong
        {
            get { return _Dieukienchuyennhuong; }
            set { _Dieukienchuyennhuong = value; }
        }
        private decimal _Socophansohuu;

        public decimal Socophansohuu
        {
            get { return _Socophansohuu; }
            set { _Socophansohuu = value; }
        }
        private decimal _Tongmenhgiacophan;

        public decimal Tongmenhgiacophan
        {
            get { return _Tongmenhgiacophan; }
            set { _Tongmenhgiacophan = value; }
        }
    }
}
