﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain.obj;
using Accounting.Core.IService;
using FX.Core;
using FX.Data;
using FX.Utils;

namespace Accounting
{
    public partial class TestFormRBAC : Form
    {
        public TestFormRBAC()
        {
            InitializeComponent();
        }

        AuthenticatedUser _AuthenticationUser = new AuthenticatedUser();

        public TestFormRBAC(AuthenticatedUser authUser)
        {
            InitializeComponent();
            _AuthenticationUser = authUser;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Mỗi chức năng cần giám sát quyền truy cập có 1 ID tương ứng với ID của permission(quyền truy cập) mà người dùng được phân cho
            // nếu người dùng có permission trùng với ID của 1 chức năng có nghĩa là người dùng được sử dụng chức năng đó
            Guid guid = new Guid("94101E20-FFD1-424D-BFEC-F70EB399B95F");
            //if (_AuthenticationUser.Permissions.ContainsValue(guid))
            if(HasPermission(guid))
            {
                MessageBox.Show("Hello user: " + _AuthenticationUser.UserName + "! Đây là " + button1.Text);
            }
        }

        public bool HasPermission(Guid permissionId)
        {
            return _AuthenticationUser.Permissions.ContainsValue(permissionId);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Mỗi chức năng cần giám sát quyền truy cập có 1 ID tương ứng với ID của permission(quyền truy cập) mà người dùng được phân cho
            // nếu người dùng có permission trùng với ID của 1 chức năng có nghĩa là người dùng được sử dụng chức năng đó
            Guid guid = new Guid("0f6e0f4d-08dd-45dc-80d8-544ade12272a");
            if (HasPermission(guid))
            {
                MessageBox.Show("Hello user: " + _AuthenticationUser.UserName + "! Đây là " + button1.Text);
            }
            else
            {
                MessageBox.Show("Sorry user: " + _AuthenticationUser.UserName + "! Bác không có quyền truy cập vào " + button2.Text);
            }
        }
    }
}
