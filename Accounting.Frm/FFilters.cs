﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FFilters<T> : CustormForm
    {
        object output;

        public object Output
        {
            get { return output; }
            set { output = value; }
        }

        private T _selected = (T)Activator.CreateInstance(typeof(T));

        public T Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        private string ValueMember;
        public FFilters(IList<T> listInput, string nameTable, string valueMember, string title, int typeM, int? accountingObjectType = null)
        {
            InitializeComponent();
            Text += @" " + title;
            ValueMember = valueMember;
            uGrid.DataSource = listInput;
            uGrid.ConfigGrid(nameTable, true, true);
            if (accountingObjectType != null)
            {
                foreach (var row in uGrid.Rows)
                {
                    row.ConfigComboRowAccounting(accountingObjectType);
                }
            }
            else
                uGrid.ConfigGridByStatusObject(typeM);
            if (nameTable == "MaterialGoodsCustom")
            {
                uGrid.DisplayLayout.Bands[0].Columns["sumIWQuantity"].FormatNumberic(5);
            }
            btnOk.Enabled = uGrid.Rows.Count > 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            if(ValueMember != null && !ValueMember.Trim().IsNullOrEmpty())
            {
                Output = uGrid.ActiveRow.Cells[ValueMember].Value;
            }

            Selected = (T)uGrid.ActiveRow.ListObject;
            Close();
        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (uGrid.Rows.Count == 0 || uGrid.ActiveRow == null) return;

            if (ValueMember != null && !ValueMember.Trim().IsNullOrEmpty())
            {
                Output = uGrid.ActiveRow.Cells[ValueMember].Value;
            }
                
            Selected = (T)uGrid.ActiveRow.ListObject;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
