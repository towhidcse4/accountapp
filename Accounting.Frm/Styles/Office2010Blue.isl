﻿<?xml version="1.0" encoding="utf-8"?>
<styleLibrary>
  <annotation>
    <lastModified>2010-10-01T12:58:15</lastModified>
  </annotation>
  <styleSets defaultStyleSet="Default">
    <styleSet name="Default" viewStyle="Office2010">
      <componentStyles>
        <componentStyle name="Inbox Panel">
          <properties>
            <property name="BorderStyle" colorCategory="{Default}">FixedSingle</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraActivityIndicator" useFlatMode="True" />
        <componentStyle name="UltraCalculator" useFlatMode="True" />
        <componentStyle name="UltraCalendarCombo" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraCombo" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraDayView" viewStyle="Office2007" />
        <componentStyle name="UltraExplorerBar">
          <properties>
            <property name="ExpansionButtonCollapsedImage" colorCategory="{Default}">AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAAQEAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAUIBgAAAIma9tgAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAaklEQVQYV2N09fL7zwAEu7ZuZATRIBAVn/KfjZ2dgQEkCcJe/sH/gYAhPDbpf1xK5v/UrIL/DCABT7+g//6hkf/DohP+xyal/0/OzAcrBEuCcGhU/P/oxLT/SRm5YAkUSRAnMS0HLgHiAwAKsUwRWvWVJgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</property>
            <property name="ExpansionButtonExpandedImage" colorCategory="{Default}">AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA+QAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAUIBgAAAIma9tgAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAYklEQVQYV02N2wqAIABD/bke6sGgCLrQRStLUZSKfn+VkPUw2HYGIwDIK6G32345gHqYwYTGYvYw8JDmJYq6R8ME+Gog7eEHJEooYpohLSpftHzBKC2UO+Hho/9XN0nMyuEC5J5X337cQ/IAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraFormManager">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2010</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraGanttView" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraGrid">
          <radioButtonGlyphInfo>Office2007RadioButtonGlyphInfo</radioButtonGlyphInfo>
        </componentStyle>
        <componentStyle name="UltraGridCellProxy" useFlatMode="True" />
        <componentStyle name="UltraMessageBoxManager" useFlatMode="True" />
        <componentStyle name="UltraMonthViewMulti" viewStyle="Office2007" useFlatMode="True" />
        <componentStyle name="UltraMonthViewSingle" viewStyle="Office2007" useFlatMode="True" />
        <componentStyle name="UltraNavigationBar" buttonStyle="Office2010Button" useFlatMode="True" />
        <componentStyle name="UltraOptionSet" useFlatMode="True">
          <radioButtonGlyphInfo>Office2007RadioButtonGlyphInfo</radioButtonGlyphInfo>
        </componentStyle>
        <componentStyle name="UltraProgressBar" useFlatMode="True" />
        <componentStyle name="UltraTabControl" buttonStyle="Office2010Button" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTabStripControl" buttonStyle="Office2010Button" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTilePanel" useFlatMode="True" />
        <componentStyle name="UltraTimelineView" viewStyle="Office2007" />
        <componentStyle name="UltraToolbarsManager">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2010</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTrackBar" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraTree" useFlatMode="True">
          <properties>
            <property name="NodeConnectorStyle" colorCategory="{Default}">None</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraWeekView" viewStyle="Office2007" />
      </componentStyles>
      <styles>
        <style role="ActivityIndicator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ActivityIndicator_MarqueeFill">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 1, 0, 1">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAANAEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAAoIBgAAAGYFdL4AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA6xAAAOsQGcjB/aAAAAnUlEQVQoU2NcfmXdfwYcACTBCJKrOdz8HxnXHm75D8LIYgzp+7P+w3DG/uz/GUA+BGfDxRni9if/h+CU//FoOHZf0v+4fcn/GUAMQpjB+oL7f+uLEGwDxh5wbH0eKA7EDBI31f+L38KOQXIgzKDwzOy/wlMofmb+XwEFA8WB8gyyr3X+o+BXOv/lgGIgDBNnEH0h9x+OXwLZyBgqBwAA1MNeqyZoegAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="Base">
          <states>
            <state name="Normal" themedElementAlpha="Transparent" />
          </states>
        </style>
        <style role="Button" buttonStyle="Office2010Button" />
        <style role="CalendarComboControlArea">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="CalendarComboDateButton">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_CalendarComboDateButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDateButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDateButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="CalendarComboDateButtonArea">
          <states>
            <state name="Normal" backColor="204, 219, 237" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="CalendarComboDropDown" borderStyle="TwoColor">
          <states>
            <state name="Normal" backColor="204, 219, 237" borderColor="133, 158, 191" backColor2="187, 206, 230" backGradientStyle="None" borderColor3DBase="133, 158, 191" backHatchStyle="None" borderColor2="White" />
          </states>
        </style>
        <style role="CalendarComboDropDownButton" borderStyle="Solid" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" foreColor="110, 112, 114" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ComboDropDownButton">
          <states>
            <state name="HotTracked" borderAlpha="Opaque" />
            <state name="Pressed" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="DayViewTimeSlotDescriptor">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DesktopAlertPinButton">
          <states>
            <state name="Normal" backColor="Transparent" borderAlpha="Transparent" backColor2="Transparent" />
            <state name="HotTracked" backColor="255, 242, 195" backColor2="255, 223, 130" />
            <state name="Pressed" backColor="252, 154, 64" backColor2="255, 179, 89" />
          </states>
        </style>
        <style role="DockAreaPaneContentArea">
          <states>
            <state name="Normal" backColor="207, 221, 238" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DockAreaSplitterBarHorizontal">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Vertical" imageBackgroundStretchMargins="0, 1, 0, 1" />
          </states>
        </style>
        <style role="DockAreaSplitterBarVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="DockControlPane">
          <states>
            <state name="Normal" backColor="239, 246, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DockManagerSplitterBar" borderStyle="Solid" />
        <style role="DockPaneCaption" borderStyle="Solid">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
            <state name="Active" foreColor="Black" />
          </states>
        </style>
        <style role="DockPaneCloseButton">
          <states>
            <state name="HotTracked" backColorAlpha="Opaque" borderAlpha="Opaque" />
            <state name="Pressed" backColorAlpha="Opaque" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="DockPanePinButton">
          <states>
            <state name="HotTracked" borderColor="244, 209, 82" backColorAlpha="Opaque" borderAlpha="Opaque" borderColor2="White" />
            <state name="Pressed" borderColor="194, 119, 43" backColorAlpha="Opaque" borderAlpha="Opaque" borderColor2="Transparent" />
          </states>
        </style>
        <style role="DockSlidingGroupHeader" buttonStyle="Office2010Button" />
        <style role="DropDownEditorButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DropDownResizeHandle">
          <states>
            <state name="Normal" backColor="236, 245, 253" foreColor="188, 198, 209" borderColor="167, 171, 176" backColor2="212, 229, 246" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="EditorControl">
          <states>
            <state name="Normal" borderColor="177, 192, 214" />
          </states>
        </style>
        <style role="ExplorerBarControlArea">
          <states>
            <state name="Normal" backColor="White" borderColor="114, 142, 173" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarGroupHeader" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="196, 213, 232" foreColor="30, 57, 91" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>Default_ExplorerBarGroupHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarGroupItemAreaInner">
          <states>
            <state name="Normal" backColor="White" backColor2="241, 245, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarGroupItemAreaOuter" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="253, 254, 254" borderColor="132, 157, 189" backColor2="241, 245, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarItem">
          <states>
            <state name="Normal" foreColor="30, 57, 91" />
          </states>
        </style>
        <style role="ExplorerBarItemScrollButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ExplorerBarGroupHeader_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationOverflowButtonArea">
          <states>
            <state name="Normal" backColor="196, 213, 232" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarNavigationOverflowQuickCustomizeButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationPaneCollapsedGroupArea">
          <states>
            <state name="Normal" backColor="196, 213, 232" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationPaneExpansionButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="207, 221, 238" backColor2="224, 235, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarNavigationSplitter">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="133, 158, 191" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="FilterProviderActionButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GanttViewControlArea">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GanttViewVerticalSplitterBar">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridAddNewBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="133, 158, 191" borderAlpha="Opaque" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="133, 158, 191" />
          </states>
        </style>
        <style role="GridAddNewBoxButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridBandHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCaption">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCardArea" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCardCaption">
          <states>
            <state name="Normal" backColor="231, 233, 236" backColor2="215, 217, 219" backGradientStyle="Vertical" />
            <state name="Selected" backColor="255, 231, 113" backColor2="255, 149, 9" />
          </states>
        </style>
        <style role="GridCell" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" borderColor="0, 25, 51" />
            <state name="Active" borderColor="Black" borderColor3DBase="Black" borderColor2="Black" />
          </states>
        </style>
        <style role="GridCellProxy">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="242, 247, 252" borderColor="171, 186, 208" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridColScrollRegionSplitBox">
          <states>
            <state name="Normal" backColor="238, 238, 239" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridColumnHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="255, 223, 107" borderColor="232, 191, 58" backColor2="255, 252, 230" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="GridControlArea">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" borderColor3DBase="160, 176, 199" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridFilterClearButton">
          <states>
            <state name="Normal" borderColor="160, 176, 199" />
          </states>
        </style>
        <style role="GridGroupByBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByBoxPrompt">
          <states>
            <state name="Normal" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByExpansionIndicator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByRow">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="133, 158, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByRowConnector">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" borderAlpha="Transparent" backGradientStyle="None" borderColor3DBase="160, 176, 199" backHatchStyle="None" borderColor2="160, 176, 199" />
          </states>
        </style>
        <style role="GridHeader" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="133, 158, 191" />
          </states>
        </style>
        <style role="GridRow" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="218, 220, 221" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" backColor="182, 202, 234" borderColor="Black" backGradientStyle="None" borderColor3DBase="Black" backHatchStyle="None" borderColor2="Black" />
          </states>
        </style>
        <style role="GridRowEditTemplatePanel">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridRowSelector" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="1, 1, 2, 1" borderColor2="White" />
          </states>
        </style>
        <style role="GridRowSelectorHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridScrollRegionSplitBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="238, 238, 239" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSpecialRowSeparator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="133, 158, 191" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="GridSummaryFooter" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSummaryFooterCaption" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSummaryValue">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GroupPaneSplitterBarVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="ListViewColumnHeader" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ListViewItem">
          <states>
            <state name="Selected" backColor="169, 193, 222" foreColor="0, 25, 51" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="255, 220, 45" foreColor="0, 36, 73" fontUnderline="False" backColor2="255, 255, 181" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="MessageBoxButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="MessageBoxButtonArea">
          <states>
            <state name="Normal" backColor="White" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MessageBoxFooter">
          <states>
            <state name="Normal" backColor="220, 232, 246" borderColor="132, 157, 189" backColor2="179, 196, 216" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="MessageBoxHeader">
          <states>
            <state name="Normal" foreColor="59, 59, 59" />
          </states>
        </style>
        <style role="MonthViewMultiControlArea">
          <states>
            <state name="Normal" backColor="192, 219, 255" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MonthViewSingleControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="141, 174, 217" />
          </states>
        </style>
        <style role="NavigationBarActionButton">
          <states>
            <state name="HotTracked" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarControlArea">
          <states>
            <state name="Normal" backColor="White" borderColor="139, 160, 188" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="NavigationBarLocationDropDownButton">
          <states>
            <state name="HotTracked" backColor="255, 220, 45" borderColor="255, 185, 45" backColor2="255, 255, 181" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarLocationTextButton">
          <states>
            <state name="HotTracked" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 192, 67" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarPreviousLocationsDropDownButton">
          <states>
            <state name="HotTracked" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 192, 67" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="ProgressBarFill">
          <states>
            <state name="Normal" backColor="21, 233, 30" borderColor="Transparent" imageBackgroundStyle="Stretched" backColorAlpha="Opaque" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="Transparent">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAArAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIAwAAAJ7JVaQAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAn1BMVEVev2On1K5ZwF8X2ygj1zIb3y4e6TAd6yxswHB8w4MV6B5Mx1V0wXlmv2kx0z4V6R4/z0oe4zMf5jRNx1Vgv2Qe5DRevmNjv2cf5zVdvmJfv2Niv2Z1wXoy0z9Az0tdvmN9w4Rfv2Rnv2ltwHBiv2d0wXpnv2od6ixAz0oy0z4e6DAf5zQc3y4Y2igx0j4f5DMk1jI/zkoe6ywg5zUf6TAMsnD5AAAAf0lEQVQoU23BBQ7DMBAEwC0zM6ZMcQxt//+23sk9yXIyg1IRtCIHhm7gSrYMzciZoRe4kRPDxruwhcDaO7KpQLGVl7C5QLkIOoGJgHNuKB5kz6C17osn2TFYa6s5yLKskgOlVC0HxpiRmAmkaToWy78P6pEX+aIRuZM32oGB+AH2LBw2ns/0HwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAArAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIAwAAAJ7JVaQAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAn1BMVEVev2On1K5ZwF8X2ygj1zIb3y4e6TAd6yxswHB8w4MV6B5Mx1V0wXlmv2kx0z4V6R4/z0oe4zMf5jRNx1Vgv2Qe5DRevmNjv2cf5zVdvmJfv2Niv2Z1wXoy0z9Az0tdvmN9w4Rfv2Rnv2ltwHBiv2d0wXpnv2od6ixAz0oy0z4e6DAf5zQc3y4Y2igx0j4f5DMk1jI/zkoe6ywg5zUf6TAMsnD5AAAAf0lEQVQoU23BBQ7DMBAEwC0zM6ZMcQxt//+23sk9yXIyg1IRtCIHhm7gSrYMzciZoRe4kRPDxruwhcDaO7KpQLGVl7C5QLkIOoGJgHNuKB5kz6C17osn2TFYa6s5yLKskgOlVC0HxpiRmAmkaToWy78P6pEX+aIRuZM32oGB+AH2LBw2ns/0HwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="ScheduleAppointment">
          <states>
            <state name="Normal" backColor="224, 233, 246" foreColor="Black" borderColor="141, 174, 217" backColor2="179, 201, 230" backGradientStyle="Vertical" />
            <state name="Selected" backColor="219, 230, 245" borderColor="Black" backColor2="181, 202, 230" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ScheduleCurrentDay">
          <states>
            <state name="Normal" borderColor="201, 92, 5" />
            <state name="Selected" borderColor="201, 92, 5" />
          </states>
        </style>
        <style role="ScheduleDay">
          <states>
            <state name="Selected" backColor="169, 193, 222" foreColor="Black" borderColor="169, 193, 222" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="169, 193, 222" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleNonWorkingDay">
          <states>
            <state name="Normal" backColor="239, 242, 247" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleTrailingDay">
          <states>
            <state name="Normal" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="ScrollBarButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ScrollBarButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ScrollBarButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScrollBarIntersection">
          <states>
            <state name="Normal" backColor="238, 238, 239" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="SpinButtonDownMaxValue">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAxwAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAYIBgAAAA8OhHYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAMElEQVQYV2MMz276z4AL/P//nwGkAB2DxBlABLoCmBhcEqYAJoGiE1kQrhObfTAxAPq9Xzz+TjnMAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownNextItem">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAvgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAQIBgAAAELGJX0AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAJ0lEQVQYV2MMz276z4AL/P//nwGkAB2DxBlABLoCmBhcEqYAJgGiAULmOnyM5bzdAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownNextPage">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAzgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAoIBgAAAHjMRA0AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAN0lEQVQoU2MMz276z4AL/P//nwGkAB2DxBlABLoCmBhcEqYAJoGiE1kQrhObfTAxDPtgEjR0EAD/LKxeDCG4WAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpMinValue">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAxAAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAYIBgAAAA8OhHYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAALUlEQVQYV2MMz276z4AL/P//nwEXRpEAmYKsEC4JkoBhmAKwJLIEsgKsEjAFAGENXzwwOSVTAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpPrevItem">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAvgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAQIBgAAAELGJX0AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAJ0lEQVQYV2P4//8/AwyHZzcBmQg+igRIElkBWBImiEyDxLFKwBQBAChVSA0EUeFOAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpPrevPage">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAyQAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAoIBgAAAHjMRA0AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAMklEQVQoU2P4//8/AwyHZzcBmQg+igRIElkBWBImiEyDxLFKwBTBjUW2C8ZGkRwMDgIAwza57/sbUA0AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="StatusBarPanel">
          <states>
            <state name="Normal" backColor="220, 232, 246" borderColor="194, 212, 232" backColor2="179, 196, 216" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TabClientArea">
          <states>
            <state name="Normal" backColor="239, 246, 253" backColor2="216, 228, 242" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TabClientAreaHorizontal">
          <states>
            <state name="Normal" borderColor="159, 178, 199" borderAlpha="Transparent" borderColor3DBase="159, 178, 199" />
          </states>
        </style>
        <style role="TabClientAreaVertical">
          <states>
            <state name="Normal" borderColor="159, 178, 199" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="TabControlCloseButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemAreaHorizontalBottom">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaHorizontalTop">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVerticalLeft">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVerticalRight">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemHorizontalBottom">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemHorizontalTop">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemHorizontalTop_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemHorizontalTop_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemHorizontalTop_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemVerticalLeft">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemVerticalRight">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileCloseButton">
          <states>
            <state name="HotTracked" backColor="255, 236, 160" backColor2="255, 246, 202">
              <resources>
                <name>Default_TileCloseButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileHeader">
          <states>
            <state name="Normal" backColor="191, 210, 234" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TileStateChangeButtonLarge">
          <states>
            <state name="HotTracked" borderAlpha="Opaque">
              <resources>
                <name>Default_TileStateChangeButtonLarge_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileStateChangeButtonNormal">
          <states>
            <state name="HotTracked" backColor="255, 227, 124" backColor2="255, 251, 226">
              <resources>
                <name>Default_TileStateChangeButtonNormal_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderDays">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderHours">
          <states>
            <state name="Normal" backColor="White" borderColor="141, 174, 217" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewColumnHeaderMonths">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderPrimaryInterval">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" borderColor="141, 174, 217" fontBold="True" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewColumnHeaderWeeks">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" backGradientStyle="None" borderColor3DBase="255, 223, 134" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderYears">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="141, 174, 217" />
          </states>
        </style>
        <style role="TimelineViewDateTimeIntervalLabel">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ToolTip">
          <states>
            <state name="Normal" backColor="White" borderColor="118, 118, 118" backColor2="201, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TrackBarButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" foreColor="107, 111, 115">
              <resources>
                <name>Default_TrackBarButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_TrackBarButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TrackBarMajorTickmarks">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarMinorTickmarks">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarThumbHorizontal" buttonStyle="Office2010Button">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_TrackBarThumbHorizontal_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarThumbHorizontal_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_TrackBarThumbHorizontal_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TrackBarThumbVertical">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_TrackBarThumbVertical_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarThumbVertical_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAALgEAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAl0lEQVQoU2M8VKHzn4EAsG2/zIii5FCN/v9Ph+LxYpAaIGCAYYZDDYb/v57L+//7RhVWDJIDYZA6hKYWk///n3QShQ+B1AJtZDjUYf7/36MOojFIPcOhbqv/P283/P/7sA0nBsmDMEgtxKZ+2/8fzhX9/3O/BSsGyYEwSB3CT5Pt/z89kIYXHwKqQQ296U7/DxHAyBpAbAB4N0OE6kvq2AAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="TrackBarTrackHorizontal">
          <states>
            <state name="Normal" backColor="138, 156, 184" imageBackgroundOrigin="Container" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarTrackVertical">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeCell">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="30, 57, 91" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeColumnHeader">
          <states>
            <state name="Normal" backColor="239, 245, 251" backColor2="225, 236, 250" backGradientStyle="Vertical" borderColor3DBase="132, 157, 189" />
          </states>
        </style>
        <style role="TreeControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="197, 214, 233" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeNode">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" backColor="169, 193, 222" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="169, 193, 222" foreColor="30, 57, 91" fontUnderline="False" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="169, 193, 222" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeNodeExpansionIndicator">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAygAAAAKJUE5HDQoaCgAAAA1JSERSAAAABQAAAAcIBgAAAMCnh+4AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsBAAALAQE2cE9bAAAAM0lEQVQYV2NctmzZfwYgiIyMZATRYAASBAEozQBkIgSRJeAqwcqhOlAEYUbgNhPZEpBFAM9eZgzIDLduAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
            <state name="Expanded">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAxQAAAAKJUE5HDQoaCgAAAA1JSERSAAAABQAAAAUIBgAAAI1vJuUAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsBAAALAQE2cE9bAAAALklEQVQYV2P8//8/AwyoqalBOCBBEFZVVf0fGRn5H0SjCMAFYSpAAiiCIAlkDABPYyldsbt3pwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="UltraButtonBase">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculator">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorButtonAction" buttonStyle="Flat">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonBase" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonImmediateAction">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonNumeric">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonOperator">
          <states>
            <state name="Normal" foreColor="30, 57, 91">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonPendingCalculations">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDown">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorDropDownHostCancelButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDownHostOkButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABOElEQVRYR+XYSVLCYBCG4Xgdt65ceSJv5CWEtZYiYwiRKRMzJGIVkhHYWfXZUWkO8aXq2WT3v9WdSnL1+OLeGobxIK4F0zWTw94bEsAygx2CzxOV3nQPOfuuDJB50QHBVgKQqdZ8/AZwowL+9khHAzhhAe/jSOcSYCMBIglARgOMNzlceQ6w0QCjdQ4nPNDRAMNVhrGsARsNMFhmGK0LOhqgv8gwXBV0NMD7PMVgmdPRAPYsRX+R09EA1iyBLWvARgN0pwl684yOBjAnCSxZAzbVmvf3MdQJEnSnKZ1LAD+BOUnpVM4T0PZjlFPApvL6vwItL0ZbpoCNBmh6e7RkCthcArgSQKaAjQZoOHs03ZiOBqg7X2jIFLA5B7CeeiHqMgVMnu2o/C2+Ll+EbsSb+BblTRYdOevdD0dmbYsWCMKBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraColorPicker">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraComboEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraComboEditPortion">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCurrencyEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraDateTimeEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraFontNameEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="UltraFormattedTextEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraGroupBoxContentArea">
          <states>
            <state name="Normal" backColor="White" borderColor="133, 158, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraGroupBoxExpansionIndicator">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraMaskedEdit">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraNumericEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraProgressBar" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" borderColor2="Transparent" />
          </states>
        </style>
        <style role="UltraSplitterButton">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Gray" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraSplitterHorizontal" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="UltraSplitterVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="UltraTabControl">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" borderColor3DBase="159, 178, 199" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTabStripControl">
          <states>
            <state name="Normal" borderColor3DBase="159, 178, 199" />
          </states>
        </style>
        <style role="UltraTextEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTile" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="144, 154, 166" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTimeSpanEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTimeZoneEditor">
          <states>
            <state name="Normal" backColor="237, 245, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ValueList">
          <states>
            <state name="Normal" borderColor="167, 171, 176" />
          </states>
        </style>
      </styles>
    </styleSet>
  </styleSets>
  <resources>
    <resource name="Default_CalendarComboDateButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAoQMAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAADCklEQVRYR9XY204TQRzH8eHS+BI+gk/gvfGA55fwKTyjAqJC8KzBA6KJERSNF3JBgohSWqBQMGpLOfewWyntzG5L/v7+s93SNtQGLqRL8gk7oex+GbozCw1EJCKBDtorB4VXPjJ7Doh9+882iLC/nWJfzlBerXgG93K3iA2fprxc1GSsr+65rdztxGejJFd6oNsDenSvjo8Pn8IgTHKpC554QJfu5W4R/3qS8pnfJBfuk1y8W//Qyb3cLeIjHP8T8R0k529XaMe4FL+mFn69e56bOG6DG9AKLdAM1+EaXIUmuAKX4RJcLLiAz+fhXHkTOrmXu0Xi2wnKr//AC3ChKC5Q79DJvdwtEt+PYzBLcg4zEsFM1Dt0ci93i+Qo4tMzTnQYv7p6h07u5W6Px/uO4ScJYeZx04Rxw9Q7dHJvEt0i6WssxOOu31H4dt9qPEnAk7Ud+u2MyY00FeIbPR5vjLkzzzcsz/42YSZ4NhxbrVZYzyMl5nA8h3X+n7Dy8epXSi8ozjX4bcPdwvAfdZbKKDaTKDaTmvh1vPFUwxtTqVsYV6rcDGttjrzx8T7EfW26l7uFEThS2GE7scve2QIeGRZquYfHilL8qMEeOJbYwzIKY7X0qNwyxkWPcbxJn0/3depeI8Dx4xxfeDBbfkqy6BmOq1PLz3HyEisvSBV145i9dKyyngqvMK4Qw7joNY5LrOKJl9vwAMm93C2MCcRn50muvoG35WIYx3qri/eS0vo2JXCceFfwnlTS1Y/jfrLKfCDLqOYjvub65JxP973RvdwtzInD+i8omfhMyhggZQ6SlRoiOz1C9voo2Zkx8O9QAN+3U7juuo8sdFh/hnSXSg6gc0D3crcwJxFvGyRTPlJrflLpcbIyk2RnpyEEs5RT/58tZwvXn0JPUHfpPnRyL3cLM3iI8rk1TWV+kZV12DKs5azIrrGV0+A2cZ/bagYRH51pIXMKy2UuQ/l8GvCDbKRoY8MEA5KQ2AV8Xb6+iR7gLt2XIXO6kaKhVhL8r4/5UDOlggc9g3u5+y9MHsahoQkQEwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDateButton_Normal" imageBackgroundStyle="Stretched" backColorAlpha="Transparent" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAWgEAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAw0lEQVRYR93YSQ6CQBSE4eZEnsIbeEs9Aa5cuhKMxgkFHFFwQhR1U3aT+PoMRSd/8pZfatkOANXp+lBkr9VsOKrteugNYsTnL03Ga9wVngn+t1p8plcnS/BR9gFbFp9qPFmCD9M32LL4U4mQLMGvjiXYEvwyeYGteuCD5Am2ZPnFoQBb9cDP9wXYkuVnuwfYEvx0m4MtwU82OdgS/Hh9B1uCH8U3sCX4YXQFW4L3NZ4twbv9AF54ocl4K7z5+jAHW8b9A3lef+cfsKzYAAAAAElFTkSuQmCCCwAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDateButton_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAWQIAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAABwklEQVRYR9XYXSsEcRTH8aO8GSFEIhLycOklkERJRGrtg7XLsk9JSRKRt+CG+5PWFi0SSklZ70DW2sVxfrNzM22utq35b337N1OzfeY0F/+ZGhGhi2O/fD+ekCm/2roR6hxbr6HUsU/YUy/vPCofqQn5SE9KLj3luuCCD0544Sb2Nsj7+bjkr2elcLcohXuPFB+WXBdc8MEJL9zEvkbJXU5b2O+noPw8h7SwCwtZPjjhhZs40CSfmdkS/CUiv69R1wYfnPDCTRxslvzNvDXxcnhMz2nZ8iQbF0dvemyVqFrwwQkv3MShFsnfLliPiRNvwy183C6haynJJp296bEj3ARuJqZFtQ1tXYtoa9qqFtZCdiu6BrVlLaD57Xy6eq2BlPBhyws38WrrP3g8Pm6cvI1XN3GkzUy8uok32s3Eq9twfKzDzMmrmzjZaSZe3YbjN7vMnLy6ibe6zcSrm3i7x0y8uol3es3Eq5t4t8+Br+bGqtL/duxt1E28128mXt3E+wOOLXGl06nm9Y4tsbqJDwYldzUjxaeAES8jcMILN6VOE8KHQ5LLzMkX3hMffa4NPjjhhZvw6SN1lhQ+GjYmeOH+A80nA9qRlnbKAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDropDownButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAALgEAAAKJUE5HDQoaCgAAAA1JSERSAAAADgAAABUIBgAAAHZZ34kAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA6xAAAOsQGcjB/aAAAAl0lEQVQ4T53NsQ4BYRSEUV7RQ4lGpdFoNNsoKLValUSrwoYsNhtEROYOu8U/SuMmX3Kbk2nfVh22/rka8lVaNWMNfOZWCcZjQyfB+5phJHhdMowEqwXDSLCcM4wEz1OGkWCRMYwSxLZLJ8FdjzAS3PcJI8F8QBgJHoaEkeBxRBgJFmPCSPCUEUZfcPKBvyd4mRFGCdaP2xvVjto1t9sipQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDropDownButton_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAxAAAAAKJUE5HDQoaCgAAAA1JSERSAAAADgAAABUIBgAAAHZZ34kAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA6xAAAOsQGcjB/aAAAALUlEQVQ4T2M8VK79n4EcANL4/0kXSRhs2ahG7IE2Gjh4UtNo4AyzwAFFKKkYAIEhit46DgG7AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ExplorerBarGroupHeader_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA4gAAAAJHSUY4OWEcABwA8wAAhZ6/yd7zx93y4e34+v3+1ub2zuD05e/5////3On30eP1yt7yAAAAAAAAAAAAAAAALAAAAAAcABwAAASXEMhJq72A6M277xJhjKSxnEGqrkIrEGFJnstqB+4byyZ6qzlYhter/VLBnYx2RLqEIh6zmRxKfcdqdIn9aYlEmpgGVZjPaDR4BC2433B4Og1N2O3xvL5Qv/v/gH9QA4SFhoeGgiEHjI2Oj42IQgiQlZaNk5eaj5mbnp2emqChlqOkkKanjqmqjKytkwiys7S0HxwYuboSEQA7CwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ExplorerBarGroupHeader_Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMAEAAAJHSUY4OWEcABwA9AAAhZ6/v9XsvNDp1+bz4u33zt3v7fT6zeH0v9Pq1Oj3w9juw9bqxtvw6fP73ef00eX25+/37PX86/L5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALAAAAAAcABwAAAW1ICCOZGmeQKSubOu2YpPMdG3fdBM/fO//wJ4OADkYj8ik8ggREZbQKMHJqFqv2Kx1CiAovuCweAzmOgLotHrNTjtEDoF8Tq/b529A/M7n5/d9gXhwdAiGh4iJdX+JjY6NjIkLk5SVC455BJabnJtcXmShoZ9apaZcEAWqq6ytrqtNRK+ztLGptListrm8BbESQcHCEiISA8fIycrLyMQAxszR0c7Q0tbNIgba29zd3t0o4eIiIQA7CwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_GridColumnHeader_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAALgEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABQIBgAAALnw3BEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAAAl0lEQVQ4T43LoQrCYBiF4d+rsHoLVu/KZrKZbCabyWaymZbWBoLN5BDFbeKYTBQtr/wInno+eOAL5+0UyYBurx+cK/NtCDEgH1ri9hccRxYFpzEYFJwnYFBwmYJBQTEDg4JqDgYF1wUYFNyWYFBQr8CgoFmDQcF9AwYFbQIGBY8UDAqeGRgUvHZgUPDeg0HB5wCGfxAf1xdytxP3fslo6wAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_GridColumnHeader_Normal" imageBackgroundStyle="Stretched" borderAlpha="Transparent" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAtgAAAAKJUE5HDQoaCgAAAA1JSERSAAAACAAAAAkIBgAAAA9TbS4AAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAAAH0lEQVQoU2NcsOH4fwZ84Nbzr/9xYbDm4aEA5BV8GAA7AecHh1TKzgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ProgressBarFill_Normal" imageBackgroundStyle="Stretched" borderAlpha="Transparent" imageBackgroundStretchMargins="1, 2, 1, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAANwEAAAKJUE5HDQoaCgAAAA1JSERSAAAACgAAAAwIBgAAAFtrLKAAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA6xAAAOsQGcjB/aAAAAoElEQVQoU2MUa/b7z0AMAClcfmUdHC9DYsPEwYaBiNrDLWBcc7gZK4YrzNif9R+Cs/+nA2l0DFcYvz/lPwzH7kv6H7cv+X/cfgSGKwRJ4sNwhTYXPf7DsPV59/9gfAGKL7r/hysUv6X+H4Ylbqr/R8YgcbhChWfm/xHYDMhGwk/NEAplX+n8l3sNwbJYMNxE0Zdy/+H4BZCNhuEKQQxCGABeT/4L5eI8lwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScheduleDayHeader_Normal" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAVAAAAAJHSUY4OWEKABQA8gAA/9hr/+Z0/+Bw/+15/9xtAAAAAAAAAAAALAAAAAAKABQAAAMhOLrcHTDKSSsUOOvNOyZgKAJkaZ5oSoos4b1wZs105DAJADsLAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMgEAAAKJUE5HDQoaCgAAAA1JSERSAAAADwAAABEIBgAAAAIK9qEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAm0lEQVQ4T+2TPQ6CQBCFdw+B1/HGUkgBFTQWJJJwBGMgmKhYyP7/PFe5wIItk7xikvnykjcztD63VT/c9pRSEl8guySpSJoVeI4vWOtgXZC1MMZAawOlNKRSEFKCCwnGBRjj6PoBaZbjB3vv4ZyPht8TWw9PwX218wYvXNUW2D+B3R/joq+6XLv5tuumLQ/HPDRFhL5zs051U34AR7uPVI8SqvQAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAIwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADwAAABEIBgAAAAIK9qEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAjElEQVQ4T+2TTQrCQAyFZy4geiJvraCLmZ0oStdSilh/2kkmsTd4OnoBIy4b+CCbb/OS57f7KooOc++9+27gAOdm00l0y3VEe7lBVSFSEOQs4JzBnEHMIGIkIvSpkFA3JyxWAW9Z9WGS7136Xe76UTalPQZmfM//BXZur6ZTHevmU4xXn0NZrGx2h/AE+qt0uvHMU0YAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAABAEAAAKJUE5HDQoaCgAAAA1JSERSAAAADwAAABEIBgAAAAIK9qEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAbUlEQVQ4T2Pcf/jYgfcfPtkzMjIyEAv+///PICjAf4Bh/Zad/6/duPX/4aPHROOr12/+X7d5x3+wZlI0gtQ+ePiIfM0gA8i2eVQzCXEMi9bR0CYx0OABBsolpGSOS1euQ9L2/sPH94MYpGKQPgAdHlufbQYBXAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalBottom_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 0, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAUAEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAABcIBgAAAN24tcYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAuUlEQVQ4T+2TSQrCQBBFK0fwBF7AQyheWBE3QtCdJigYiVHjEFyme+HCAXRhWT8Qp0pukMAj8Or1puh2uv0Bt1tNCo8nKvsa9Rq5wxER4svtwZNVKpgC0myOroqxpWobn3tSsA1PLtE4ShXwanX+xrC31sCreBZbnsZGAa/iYGd5vtXAq3hxsBzsjQJexcvEciiDf+B/4vP1nokoMQr4d9xz/WdH3ld+IB9+//H+pGNiZsIBiDIQCs4Lk95b/3Ol7/AAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalBottom_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 0, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAUAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABgIBgAAAPOgfQwAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAuUlEQVQ4T+2UsQrCQBBE737F3tpfVxvr2AgKWhiMnZAqaiQmkF1nEq0OMStXiYFHCMw8lrB3fprsdTIeuW+eZHNwbp2ehJKiak2wg646VXX4kKISCIZDQTf1S3BG2QIFwEcU3DGBgWCCC8oW4guutaiFYAJLmdlAUNaqFuILbo2qhWACS5nZv+D3/sFsmUqJ/R66C8yiQ/obaZvl7WKVdafxk4QZZtEhvYDsjrnMYX1u2Ns3M8gSz94DK7tM4v0QkOAAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalTop_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAPgEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAABcIBgAAAN24tcYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAp0lEQVQ4T+2SMQ6CQBBFFytLL+MFrL2sQRsTbQXURIyCgkq9u42FMcTCL1NgVn/2Bmzymj9vtpj8AICaLtd413fle73+QI1Hw0CFiwST2RyP58uLzMVTrphVBv+0n4j3lY+VxeHGSC4LP/K+EdOrISQnOb1Y7EpGcpK3hcWmMITkJCdng/jESE5ynBusMk1ITnKUa0TNgNGd7Fylu4bbEe81pExM27oPX8diGIE4jZ8AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalTop_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAVQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABgIBgAAAPOgfQwAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAvklEQVQ4T2P8//8/AwjcePj6/+NXnxj+QflgQSyAiZGRQVaMj0FDXpQRLA0y4Nr9V//3n7////PPf/+//vqPF4PUgNSC9IAtBxE7T935/4UIzTDDQWpBeuAG7Dh5m6DN6C4D6Rk1YDQMMNLBl5//gamReEx9A0CZhBSM4QJSNIPUYhjw6ce//6Rg6hvwEegCUjCGCz5+BxpAAqa+AR+AtpOC4S64dOc5OEo+fP9LEgbpAellADHef/tLFgbpBQD+nmtoK8jivQAAAABJRU5ErkJgggsAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalLeft_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 0, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAIwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFwAAAAsIBgAAAJQ+FkAAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAjElEQVQ4T2PcvPcUEwMDw99/Pz4AKSoDoOH/N27d9f/Lt19UxwzIBl+9//r/pXuv/1+88/r/2Vuv/5+6+er/yeuv/p+4/pIsDDf82v1X/6+ADL776v+F26//n7kFNPjGq//Hr4EMBllAKn75f9RwLMFGn2D5CU6GVE8tiHQOsYCamAGUQ2EWgNI8NTEA2J43+4yWN24AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalLeft_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 0, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAOwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGAAAABAIBgAAAAwkv5UAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAApElEQVQ4T+2UzQrCMBCEs+//QtqLZ3sRLOjBSD2pFIp/0VohGXcT700hAQ8NfLdhJ8yyQwBUjlftz+7YGlJisKkbOy+1my13KcHlZaG2h8YtVjVM7/D8YBDTg7XAg/XC/R24CV3g6rHBoCi1F8YMF81oA44levhkEBXVtIPBmP4vouyHxlWBrFXBBsRlBy47yFWnxHfRr02JDYiHJ0Hqf61P/rNfIplEOsAkNOMAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalRight_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAKQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFwAAAAsIBgAAAJQ+FkAAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAkklEQVQ4T+2STwuCUBDEV08d+/6fIokuQmdTDCxMy79n9x3VoA5Nrg88BsoDLy3M9TfD7FjOwQUZPnuzFaJN3fMN05LA+2MAOqUNlshPGcGDcc4ULoXCtVK41WoKOjaiwTxbXtLAvzPCjBHlg0HJiAeDpObR4A9ftZb5i/m1lrZ/6Yea3riAd3rnHxIH0xIwAPoCHhg10boUje8AAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalRight_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAQwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGAAAABAIBgAAAAwkv5UAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAArElEQVQ4T+2UvwvCMBCFz7i6uUhXZ/9/dXG2i+Cgg4G6CV38VbVVkuc9reBkl2QQGvhIhpDvuHCvM55vkPR7fjQcdCXG2l8dKFFcKCapdassfwAQoeBwIx7HD6XHqeZceZCiwovLvZlC788WGdbb3EURsAgWNU0togkoYetbwc9Pb1vUOBP/3aKog/YVFe9Bq8Mu2K5hBw07ZpERPr60O4094dmEQAVGBUYF8gR44D9+lDMt3QAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TileCloseButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAzwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAIAAAABcIBgAAAGoFTeEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABTUlEQVRIS82WP0vDQBjGL5OL38LRL+H30E03N2eV6iK4OQqCKCq6u7sWxCHU6hRbrP9aY0uxaXJpzONzZzBJHdWXBn4c3JDfw3t3L68DQOn6EjDwlOTnTM+oqdk9R0XuAuKbZaTvV8ConfHC9f8wLuM0bhVW55AGNSC4BN5OMo65jnPEvV/QPQW6Z18MXes0bhsA8SPwug90dmXwD60zD6BbQHtHFjoLAZrA05YsulkIEN0BDxuy0JlXwAZYl2XCAnhAa02WyCsewUQEWGUFBClXgJdQ/Ah+vIIKX4Eg5VfQoHxTlqgx3gm32QkFKXXC+BnwD2ShM++EiQ/0z2WhsxCgx4noQpakVwjw0eeQwIFkWJUh5PRF53cFUn3PEYz3QF9ncELSf02d/8xIOjBOGyBy56FvV5CGbMdpIIJxGadxKzsV1xZtGkmM07g/AR0wn2SgBHf3AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_TileStateChangeButtonLarge_HotTracked" backColor="212, 232, 255" borderColor="Gainsboro" imageBackgroundStyle="Stretched" backGradientStyle="None" borderColor3DBase="Black" backHatchStyle="None" borderColor2="WhiteSmoke">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAzwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAIAAAABcIBgAAAGoFTeEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABTUlEQVRIS82WP0vDQBjGL5OL38LRL+H30E03N2eV6iK4OQqCKCq6u7sWxCHU6hRbrP9aY0uxaXJpzONzZzBJHdWXBn4c3JDfw3t3L68DQOn6EjDwlOTnTM+oqdk9R0XuAuKbZaTvV8ConfHC9f8wLuM0bhVW55AGNSC4BN5OMo65jnPEvV/QPQW6Z18MXes0bhsA8SPwug90dmXwD60zD6BbQHtHFjoLAZrA05YsulkIEN0BDxuy0JlXwAZYl2XCAnhAa02WyCsewUQEWGUFBClXgJdQ/Ah+vIIKX4Eg5VfQoHxTlqgx3gm32QkFKXXC+BnwD2ShM++EiQ/0z2WhsxCgx4noQpakVwjw0eeQwIFkWJUh5PRF53cFUn3PEYz3QF9ncELSf02d/8xIOjBOGyBy56FvV5CGbMdpIIJxGadxKzsV1xZtGkmM07g/AR0wn2SgBHf3AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_TileStateChangeButtonNormal_HotTracked" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAzwEAAAKJUE5HDQoaCgAAAA1JSERSAAAAIAAAABcIBgAAAGoFTeEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABTUlEQVRIS82WP0vDQBjGL5OL38LRL+H30E03N2eV6iK4OQqCKCq6u7sWxCHU6hRbrP9aY0uxaXJpzONzZzBJHdWXBn4c3JDfw3t3L68DQOn6EjDwlOTnTM+oqdk9R0XuAuKbZaTvV8ConfHC9f8wLuM0bhVW55AGNSC4BN5OMo65jnPEvV/QPQW6Z18MXes0bhsA8SPwug90dmXwD60zD6BbQHtHFjoLAZrA05YsulkIEN0BDxuy0JlXwAZYl2XCAnhAa02WyCsewUQEWGUFBClXgJdQ/Ah+vIIKX4Eg5VfQoHxTlqgx3gm32QkFKXXC+BnwD2ShM++EiQ/0z2WhsxCgx4noQpakVwjw0eeQwIFkWJUh5PRF53cFUn3PEYz3QF9ncELSf02d/8xIOjBOGyBy56FvV5CGbMdpIIJxGadxKzsV1xZtGkmM07g/AR0wn2SgBHf3AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAdAIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAAB3UlEQVQ4T42S3UobURDHt6/gC8RHiK222iqCXhV8mL6HwqZi7dfFWtpetILYil40XihpNcaP1VZERNPQbBKMZHezxj3JbsL4nzHBKCUk8GMn58xvztc8ICKt/WcnRnvx/wUYBdHm3D6+6yDWM7KeuSNwgRalxEjMSU6Qsj5S6B1Qo5oXOOYxnuOcdqdNHjYrp1NU99MUXh5Q9WKJ/NwcMBCvUFg5lDnOKSWGzVYRKWD/fKZfnU0i4YyqxW/kZ9+CN/d4h7klqqsMcS47cnz719NeN/W8KS9Cmu1I7WJZctlhV7M3hgxlGdj276Y4g28HrNfI/YM7MYhdFBg0Q28X28Pq/6a7gnfBDruas/kEN53Fqq8gx7pC5d6Lw67mJLlAjlQeN27h4rpA5T+Iw67mbD02+dlqpbgU6YbAWZOnZhcFBgxV+ISbPSF1/hV86UxxHk+ZJlX4jAIDhuam+iPlvXHZUlDeRNN870hQTkkuO+xKI7nbj3Q/q1MjKFJwuUdVOw5+3GOVggraGzmcy440Uqsl3Z2Hpm9NS0KjVqDw6gg7SlLgpRAfy9iN/JI4904r3xbp0739MRxhAef8S426J3DMYzzn7vTJyv8twIPl3WgE6MAE1IRjHou0yxxfA3XYJn3FpqPNAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_Normal" imageBackgroundStyle="Centered" borderAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA4AEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAABSUlEQVQ4T52TT07CQBjF9STlCN7As3gDd7Dwz1IWRFjqxkZFWaKJcWE3NiZGQuIQDQlICEpohNrYINhIJD7nNQwZlEVLk1/6db733nTamWUAS/qVSpsJ+bwuWZWsTHoVebcluXRq7XnGwABFcucgt72bx+X1PZodD5+jnxDWHGOPGt2jm8XJuQ3XD/ARjOfCHjUyRKiQMEAOZPNnNvzhdySopSdcviwSW/LVOu4Ab/1RJKilh14GmEWrjK7/FQt66GWAqNQdOF4QC3roZQDa7nAh6A0DWq+DhVAB4u7xBQ2nHwt61BLMwsUtam0/FvSoj2hsZo4h6j1UW++RoJYeGWBMN9J+wcJD04sEtdONpJ0DsXd6hVLVgXhy58IeNf+2shaS3cgc4bB4A6vUQLnWC2HNMfbUzDNn4c+JNCjiLPxNE1hzzNC1rH8BmfknYvOoj6kAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAiwIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAAB9ElEQVQ4T42Ty08TURjFr39J+RPcEY0NRo3GR+JOF258xGcpGFBEU4rysBI1gzHGqImsjSsXslGT3gID+MggKLUVMmM0OsSym+kCWo7fuVItkeBMcjJ37nd+595vcu8mAKr+GUk3Nsj3eVGTaPNqbUreWjQY75n01gAMoLyXJ1Suu3FwPBOH++wIvmdP4cfIGSOOOccaPfTWuL/wtS1O8ckhAc6iNJlE8P4ClmY7sZTvRDDVjp8TzaZGT068tRATkOvdahWfHsaCnUA404FKMbWuwumL8McSoJeMaV/3b2uwb+2AP3oO5ZlLqH5Ob6hgusN4yZBVOhMfcoePovS6FdW5q6jOb6zKXLfxkiGr9ECT448mUP54BStubyQFHy7LLhIgq/TN7dJXM6qEv/RH0vJ8j2HIKn1b+rclwOsDvmYiqeL2GYas0tZOx7eTKBfSwLeBSApmUxKQBFml7+wa8l6cxOK7dln9xn8DVsRTetMGMmSVvrs7Zj/cD3+8JdIuwnyXeJMgQ9YcpOy9PVbh+TEsTLT+DllnJ1w5zKfE0wJ6yZiDVLsH2ft7neLwcZNeetuG8FMXKt51LMsPY8+LMsdaQTz0rjnKf0Ie7LPGHh+E++q0+Un14hxrWfH8c5lqN8u08+hATGSJHBFWxTHnYvVejn8Bl+fdsBqBzLYAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAPQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAApklEQVQoU4XMxw7CMBAE0OVv6P0/aX9E72fgDlxJwRxAWXZWsYUTAZZGTmaeXLiNu0z/TjDpcPI4fQ12CqZtAUd+Xke5oMdO4azFiRF0EZQJeuwUzpspGgryo0h2ihaNFA0E+QHCTtGy/hvJTvGqJujAr3M/F/TYKV5XFWWh7bATM9N9U/mAPfeNHrsihduyG/EK/u3mEAqzKynEbYH3ki3NvugB9G9Bqmxngb9JnAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_Normal" imageBackgroundStyle="Centered" imageAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAgklEQVQoU4WQzQmAMAxGdWZHcApn8O4CzuDBg4iCguAPUqpUxE9SaahIsPCakrx8h4ZRnCD4OySZ8xKxIXRpc4qwtG4HJFialIEES8OyQ4KlbtSQYKkZFCRYSrMcVb9+oL6VgOcbqFG2C+MEmlvJF4t6tgu06GYs+aIvvJL8RPd29QYScGxj3P4wwQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAWgEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAw0lEQVQoU4WMTQsBURSGj/8jSSJJkh8iG2UpCyIlnxMppeTHWJ2SlY3YkfGxsFOkyJiXc2tuaZrceroz7/N0fVz2g/4drgZw5Ywn4olrQdzmOTyXRReyiyeuh/BYlWBt6i5kF0/cDAOnnifiidsR2EfDE/HERhT2oeuJeOJ+DK9tA+99x4Xs4okHcdzXFYVltjXOJp6AbzhM4LIoKKxdS3/LLl5FKhwlcZ7lNfLvOB2pcJyCOc1Cbif4eUm/OEn/BLJ/ADYxTbKh3inhAAAAAElFTkSuQmCCCwAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbVertical_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAMQEAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAmklEQVQoU5WQyQrCQAyGM2dfxH3vc7q9kfvai5fqvXq1i+Ohpb+ZgSlVUMfARyDwkeQXt2kfQoCI+dRLzlFQsZSUPc7MCZl8x+OZh/uuBi4ykJGS6wTJRTFmRprUH2pJi/tqLv6UiqI8VLRoJaX+IN8o3TKsJHOi9aavP/2dXjDrIZh3ES46CJdtRKsWonUT8aaBeFt/iVsF8QQfAml3EzTitQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbVertical_Normal" imageBackgroundStyle="Centered" imageAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA/gAAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAACV+AAAlfgHJ8rxdAAAAZ0lEQVQoU2Msbp75n4EA6KlJY0RRAtL0888/nHj1tsP/QWqAgAGGGUACn7//xovRNYI1vfn0gyBG1gjW9OztV6IwTCNY04MXnwliDJtuPv7wHx/G6qfL997+x4Vxhh7IifgwcnCD2ABJX1/DAqxAggAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA0wEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABoIBgAAAIP6vWEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAABPElEQVQ4T6WUy0rDQBSG4xPUR/INfAX37lx7Q3Hj0o248rIQXIi4c1eXBREUb6sqNSm1sTWoTZNJY37/E6hJTDiiBj4IzP9xhjNnZiJoTE1alrVDpon2HXNxxqJQj25nkbyfA6MueSoha5KRrAhI/CvAPwP6+0Bvt8jLATC8TDOSTQVEbeB5G3C3quntpZlMMDbQ3dBhJie0gM66jmnlhPABaK/qMJNVCO8BZ0mHmW/CIgWFfwpNwF7QCZv5Lf1JmGcFhXKFXwlsq80OaZS7tMy2KhQFnrSzolM8aRmNNZ2CEDkc600dZrLRGLmAd6jDTCbEfeDtRIeZnOABg1Od2MsJH6+8t7zTw0Y1AR8IZr4qJOaRL0UHMNdAeFHE3ACxC8mMhbq5m0MScAATvxJZk8z4manx50jsH5BM7ROdgAzzsJs9IQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAtAEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABoIBgAAAIP6vWEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAABHUlEQVQ4T43MSU4CYRCG4c8T4M28gkfzBkpcGF2IGkREQOiBZurGKBpN3Aj2PJT/R+JOsCp5V/VU7QWLp30AR6YD066pm+Uh/GDReFm+ShTHsm24o6HF3A8kSdOt+HdBQ4vZ3JeqqlTRYjqbqzCf0m4OyrJStTmYTGcGl6poMZ5MpShKVbTwxhMV5lNajLyx5HmhihbuyFNhPqWF447UB7SwHVeyLFdFC8t2VJhPaTG0bEnTXBUtBkPL4EwVLfqPA0mSTBUtur2+CvMpLTrdnsRxqooW7c6DCvMpLVrte4miRBUtmq07CcNYFS1umrey/o5U0eLy6lrePz5ltY52RkOLs/OLBi+fl2/ytQr/jDsaWpzUT2umY5P8E03tB+2Udy0dMscrAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA3QEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABoIBgAAAIP6vWEAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA5wAAAOcAGU2TTEAAABRklEQVQ4T43UXSuDcRzG8csrmPfhWZ6fn08dOVeKsgNyYBaR2ZqINBFx4BWQV7D6KWmK1HBm8haMzer2d/3uHck2v9V1sPv/+d5n/7tKFmuqAZxx41yl3yUPJyGh2mQ6MeDer6bc581MyemZGrWQcJ3LXk+73G3Q5e/mXP5+/vf4LJea9Y1ayHK9/9bC05IrPIdL7zHkG7WQlQb/j/cSqTg/oIWsNvrB91u84vyAFrLWZA9oIevN9oAWEmmxB7SQaKs9oIXE2uwBLSTebg9oIZsd9oAWstVpD2gh2132gBay020PaCG7PfaAFrLXaw9oIYk+e0AL2e+3B7SQg0F7QAs5HLIHtJCj4eKNe42VvUBeZqN4RWkhxyPuIxV0Xw8LzstE/0SK9cOgRi3kZDSZPp9wWT4o95nRMzVqIadjAe6Cc/9MTeAHeziyZyUh7uoAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
  </resources>
</styleLibrary>