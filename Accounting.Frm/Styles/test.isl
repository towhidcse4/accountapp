﻿<?xml version="1.0" encoding="utf-8"?>
<styleLibrary>
  <annotation>
    <lastModified>2015-01-25T18:00:47</lastModified>
  </annotation>
  <styleSets defaultStyleSet="Default">
    <styleSet name="Default" viewStyle="Office2010" useFlatMode="True">
      <componentStyles>
        <componentStyle name="Inbox Panel">
          <properties>
            <property name="BorderStyle" colorCategory="{Default}">FixedSingle</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraActivityIndicator" useFlatMode="True" />
        <componentStyle name="UltraCalculator" useFlatMode="True" />
        <componentStyle name="UltraCalendarCombo" useFlatMode="True" />
        <componentStyle name="UltraCombo" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraDayView" viewStyle="Office2007" />
        <componentStyle name="UltraExplorerBar">
          <properties>
            <property name="ExpansionButtonCollapsedImage" colorCategory="{Default}">AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6QAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAUIBgAAAIma9tgAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABfSURBVBhXY3D18vsPwkDAAMORccn/41Oz/sMlvfyDwQrCY5P+x6Vk/k/NKoCo9PQL+u8fGvk/LDrhf2xS+v/kzHywQrhRoVHx/6MT0/4nZeSCJVAkQTgxLQdIwfj/GQB39VbY+xFBwgAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</property>
            <property name="ExpansionButtonExpandedImage" colorCategory="{Default}">AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6wAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAUIBgAAAIma9tgAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABhSURBVBhXTYrLDkAwFET9nAWLSkgTKh4tSps2FcTvD5ooi5O5M+dGAALSbHd8PRysn8ClwWz3u/4kKUpQ1qHmEmKxUO7wD1GcEiQkR0YrPzRixqAc9HrCy4dHvLSjwqRXXOSeV98XySvxAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraFormManager">
          <properties>
            <property name="Style" colorCategory="{Default}">Office2010</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraGanttView" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraGrid">
          <radioButtonGlyphInfo>Office2007RadioButtonGlyphInfo</radioButtonGlyphInfo>
        </componentStyle>
        <componentStyle name="UltraGridCellProxy" useFlatMode="True" />
        <componentStyle name="UltraMessageBoxManager" useFlatMode="True" />
        <componentStyle name="UltraMonthViewMulti" viewStyle="Office2007" useFlatMode="True" />
        <componentStyle name="UltraMonthViewSingle" viewStyle="Office2007" useFlatMode="True" />
        <componentStyle name="UltraNavigationBar" buttonStyle="Office2010Button" useFlatMode="True" />
        <componentStyle name="UltraOptionSet" useFlatMode="True">
          <radioButtonGlyphInfo>Office2007RadioButtonGlyphInfo</radioButtonGlyphInfo>
        </componentStyle>
        <componentStyle name="UltraProgressBar" useFlatMode="True" />
        <componentStyle name="UltraTabControl" buttonStyle="Office2010Button" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTabStripControl" buttonStyle="Office2010Button" useFlatMode="True">
          <properties>
            <property name="UseHotTracking" colorCategory="{Default}">True</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraTilePanel" useFlatMode="True" />
        <componentStyle name="UltraTimelineView" viewStyle="Office2007" />
        <componentStyle name="UltraTrackBar" viewStyle="Office2010" useFlatMode="True" />
        <componentStyle name="UltraTree" useFlatMode="True">
          <properties>
            <property name="NodeConnectorStyle" colorCategory="{Default}">None</property>
          </properties>
        </componentStyle>
        <componentStyle name="UltraWeekView" viewStyle="Office2007" />
      </componentStyles>
      <styles>
        <style role="ActivityIndicator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ActivityIndicator_MarqueeFill">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 1, 0, 1">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAIAEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAAoIBgAAAGYFdL4AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADrEAAA6xAZyMH9oAAACWSURBVChTY1h+Zd1/XHgZlGaoOdz8HxnXHm4BY2QxhvT9Wf9hOGN/NhCDaAgbJs4Qtz/5PwSn/I9Hw7H7kv7H7Uv+zwBiEMIM1hfc/1tfhGAbMPaAY+vzQHEgZpC4qf5f/BZ2DJIDYQaFZ2b/FZ5C8TNzNAwSM/vPIPta5z8KfqXzXw5IgzBMjEH0hdx/OH6JhsHicv8BIFjbnAIAP6UAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="Base">
          <states>
            <state name="Normal" themedElementAlpha="Transparent" />
          </states>
        </style>
        <style role="Button" borderStyle="None" />
        <style role="CalendarComboControlArea">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="CalendarComboDateButton">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_CalendarComboDateButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDateButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDateButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="CalendarComboDateButtonArea">
          <states>
            <state name="Normal" backColor="204, 219, 237" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="CalendarComboDropDown" borderStyle="TwoColor">
          <states>
            <state name="Normal" backColor="204, 219, 237" borderColor="133, 158, 191" backColor2="187, 206, 230" backGradientStyle="None" borderColor3DBase="133, 158, 191" backHatchStyle="None" borderColor2="White" />
          </states>
        </style>
        <style role="CalendarComboDropDownButton" borderStyle="Solid" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" foreColor="110, 112, 114" backColorAlpha="Transparent" borderAlpha="Transparent">
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ComboDropDownButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DayViewTimeSlotDescriptor">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DesktopAlertDropDownButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DesktopAlertPinButton">
          <states>
            <state name="Normal" backColor="Transparent" borderAlpha="Transparent" backColor2="Transparent" />
            <state name="HotTracked" backColor="255, 242, 195" backColor2="255, 223, 130" />
            <state name="Pressed" backColor="252, 154, 64" backColor2="255, 179, 89" />
          </states>
        </style>
        <style role="DockAreaPaneContentArea">
          <states>
            <state name="Normal" backColor="207, 221, 238" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DockAreaSplitterBarHorizontal">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Vertical" imageBackgroundStretchMargins="0, 1, 0, 1" />
          </states>
        </style>
        <style role="DockAreaSplitterBarVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="DockControlPane">
          <states>
            <state name="Normal" backColor="239, 246, 253" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="DockManagerSplitterBar" borderStyle="Solid" />
        <style role="DockPaneCaption" borderStyle="Solid">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
            <state name="Active" foreColor="Black" />
          </states>
        </style>
        <style role="DockPaneCloseButton">
          <states>
            <state name="HotTracked" backColorAlpha="Opaque" borderAlpha="Opaque" />
            <state name="Pressed" backColorAlpha="Opaque" borderAlpha="Opaque" />
          </states>
        </style>
        <style role="DockPanePinButton">
          <states>
            <state name="HotTracked" borderColor="244, 209, 82" backColorAlpha="Opaque" borderAlpha="Opaque" borderColor2="White" />
            <state name="Pressed" borderColor="194, 119, 43" backColorAlpha="Opaque" borderAlpha="Opaque" borderColor2="Transparent" />
          </states>
        </style>
        <style role="DockSlidingGroupHeader" buttonStyle="Office2010Button" />
        <style role="DropDownButton">
          <states>
            <state name="Normal" imageAlpha="Transparent" />
            <state name="HotTracked" imageAlpha="Transparent">
              <resources>
                <name>Default_ComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DropDownEditorButton">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_CalendarComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="DropDownResizeHandle">
          <states>
            <state name="Normal" backColor="236, 245, 253" foreColor="188, 198, 209" borderColor="167, 171, 176" backColor2="212, 229, 246" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="EditorButton">
          <states>
            <state name="Normal" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="EditorControl">
          <states>
            <state name="Normal" borderColor="177, 192, 214" />
          </states>
        </style>
        <style role="ExplorerBarControlArea">
          <states>
            <state name="Normal" backColor="White" borderColor="114, 142, 173" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarGroupHeader" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="196, 213, 232" foreColor="30, 57, 91" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>Default_ExplorerBarGroupHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarGroupItemAreaInner">
          <states>
            <state name="Normal" backColor="White" backColor2="241, 245, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarGroupItemAreaOuter" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="253, 254, 254" borderColor="132, 157, 189" backColor2="241, 245, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarItem">
          <states>
            <state name="Normal" foreColor="30, 57, 91" />
          </states>
        </style>
        <style role="ExplorerBarItemScrollButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ExplorerBarGroupHeader_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationOverflowButtonArea">
          <states>
            <state name="Normal" backColor="196, 213, 232" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ExplorerBarNavigationOverflowQuickCustomizeButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationPaneCollapsedGroupArea">
          <states>
            <state name="Normal" backColor="196, 213, 232" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_ExplorerBarGroupHeader_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ExplorerBarNavigationPaneExpansionButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="207, 221, 238" backColor2="224, 235, 249" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ExplorerBarNavigationSplitter">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="133, 158, 191" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="FilterProviderActionButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GanttViewControlArea">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GanttViewVerticalSplitterBar">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridAddNewBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="133, 158, 191" borderAlpha="Opaque" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="133, 158, 191" />
          </states>
        </style>
        <style role="GridAddNewBoxButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridBandHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCaption">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCardArea" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="132, 157, 189" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridCardCaption">
          <states>
            <state name="Normal" backColor="231, 233, 236" backColor2="215, 217, 219" backGradientStyle="Vertical" />
            <state name="Selected" backColor="255, 231, 113" backColor2="255, 149, 9" />
          </states>
        </style>
        <style role="GridCell">
          <states>
            <state name="Normal" borderColor="218, 220, 221" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected">
              <resources>
                <name>Default_GridCell_Selected</name>
              </resources>
            </state>
            <state name="HotTracked" backColor="Transparent" />
            <state name="Active" backColor="252, 252, 252" borderColor="218, 220, 221">
              <resources>
                <name>Default_GridCellProxy_EditMode</name>
              </resources>
            </state>
            <state name="EditMode">
              <resources>
                <name>Default_GridCellProxy_EditMode</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="GridCellProxy">
          <states>
            <state name="Normal" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="242, 247, 252" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridColScrollRegionSplitBox">
          <states>
            <state name="Normal" backColor="238, 238, 239" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridColumnHeader">
          <states>
            <state name="Normal" backColor="50, 104, 189" foreColor="White" borderColor="160, 176, 199" backColor2="87, 151, 226" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="255, 242, 157" foreColor="Navy" borderColor="232, 191, 58" backColor2="255, 252, 230" backGradientStyle="None" />
          </states>
        </style>
        <style role="GridControlArea">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" borderColor3DBase="160, 176, 199" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridFilterClearButton">
          <states>
            <state name="Normal" borderColor="160, 176, 199" />
          </states>
        </style>
        <style role="GridGroupByBox" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByBoxPrompt">
          <states>
            <state name="Normal" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByExpansionIndicator" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByRow">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="133, 158, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupByRowConnector">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridGroupHeader">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" borderAlpha="Transparent" backGradientStyle="None" borderColor3DBase="160, 176, 199" backHatchStyle="None" borderColor2="160, 176, 199" />
          </states>
        </style>
        <style role="GridHeader" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="133, 158, 191" />
          </states>
        </style>
        <style role="GridRow">
          <states>
            <state name="Normal" backColor="White" borderColor="218, 220, 221" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" borderColor="218, 220, 221">
              <resources>
                <name>Default_GridBandHeader_Normal</name>
              </resources>
            </state>
            <state name="HotTracked" backColor="59, 183, 235" foreColor="White" borderColor="218, 220, 221" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="253, 244, 191" borderColor="218, 220, 221" />
          </states>
        </style>
        <style role="GridRowEditTemplatePanel">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridRowSelector" borderStyle="None">
          <states>
            <state name="Normal" backColor="218, 231, 245" foreColor="Black" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="1, 1, 2, 1" borderColor2="White" />
          </states>
        </style>
        <style role="GridRowSelectorHeader" borderStyle="None">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridScrollRegionSplitBox" borderStyle="None">
          <states>
            <state name="Normal" backColor="238, 238, 239" borderColor="182, 186, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSpecialRowSeparator" borderStyle="None">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="133, 158, 191" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="GridSummaryFooter" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSummaryFooterCaption" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GridSummaryValue">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="GroupPaneSplitterBarVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="ListViewColumnHeader" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ListViewItem">
          <states>
            <state name="Selected" backColor="209, 232, 255" foreColor="0, 25, 51" borderColor="60, 162, 223" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="229, 243, 251" foreColor="0, 36, 73" borderColor="60, 162, 223" fontUnderline="False" backColor2="255, 255, 181" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MessageBoxButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
            <state name="Active">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="MessageBoxButtonArea">
          <states>
            <state name="Normal" backColor="White" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MessageBoxFooter">
          <states>
            <state name="Normal" backColor="220, 232, 246" borderColor="132, 157, 189" backColor2="179, 196, 216" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="MessageBoxHeader">
          <states>
            <state name="Normal" foreColor="59, 59, 59" />
          </states>
        </style>
        <style role="MonthViewMultiControlArea">
          <states>
            <state name="Normal" backColor="192, 219, 255" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="MonthViewSingleControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="141, 174, 217" />
          </states>
        </style>
        <style role="NavigationBarActionButton">
          <states>
            <state name="HotTracked" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarControlArea">
          <states>
            <state name="Normal" backColor="White" borderColor="139, 160, 188" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="NavigationBarLocationDropDownButton">
          <states>
            <state name="HotTracked" backColor="255, 220, 45" borderColor="255, 185, 45" backColor2="255, 255, 181" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarLocationTextButton">
          <states>
            <state name="HotTracked" backColor="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 192, 67" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="NavigationBarPreviousLocationsDropDownButton">
          <states>
            <state name="HotTracked" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 192, 67" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
            <state name="Pressed" backColor="255, 242, 181" borderColor="255, 185, 45" backColor2="255, 220, 45" borderColor3DBase="255, 185, 45" borderColor2="255, 185, 45" />
          </states>
        </style>
        <style role="PopupMenu">
          <states>
            <state name="Normal" imageAlpha="Transparent" />
          </states>
        </style>
        <style role="ProgressBarFill">
          <states>
            <state name="Normal" backColor="21, 233, 30" borderColor="Transparent" imageBackgroundStyle="Stretched" backColorAlpha="Opaque" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" borderColor2="Transparent">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAoQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIAwAAAJ7JVaQAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAACfUExURV6/Y6fUrlnAXxfbKCPXMhvfLh7pMB3rLGzAcHzDgxXoHkzHVXTBeWa/aTHTPhXpHj/PSh7jMx/mNE3HVWC/ZB7kNF6+Y2O/Zx/nNV2+Yl+/Y2K/ZnXBejLTP0DPS12+Y33DhF+/ZGe/aW3AcGK/Z3TBeme/ah3qLEDPSjLTPh7oMB/nNBzfLhjaKDHSPh/kMyTWMj/OSh7rLCDnNR/pMAyycPkAAACBSURBVChTbcEHEsIgEAXQb+9i771rSCDq/c/m7uDOMIT3UIpBK3Bg6HquZMPQDJwZep4bOTGsnQubC6ycI5sIxC2dPZsJlGPQ8YwF8jwfiAfZMWRZ1hdPsmWw1lYLkKZppQBa61oBjDFDMRVIkmQkFn8f1AMv8kUjcCdvtD3KUeoH9iwcNlRwP9sAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</image>
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAoQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIAwAAAJ7JVaQAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAACfUExURV6/Y6fUrlnAXxfbKCPXMhvfLh7pMB3rLGzAcHzDgxXoHkzHVXTBeWa/aTHTPhXpHj/PSh7jMx/mNE3HVWC/ZB7kNF6+Y2O/Zx/nNV2+Yl+/Y2K/ZnXBejLTP0DPS12+Y33DhF+/ZGe/aW3AcGK/Z3TBeme/ah3qLEDPSjLTPh7oMB/nNBzfLhjaKDHSPh/kMyTWMj/OSh7rLCDnNR/pMAyycPkAAACBSURBVChTbcEHEsIgEAXQb+9i771rSCDq/c/m7uDOMIT3UIpBK3Bg6HquZMPQDJwZep4bOTGsnQubC6ycI5sIxC2dPZsJlGPQ8YwF8jwfiAfZMWRZ1hdPsmWw1lYLkKZppQBa61oBjDFDMRVIkmQkFn8f1AMv8kUjcCdvtD3KUeoH9iwcNlRwP9sAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="Row">
          <states>
            <state name="AlternateItem" backColor="235, 235, 235" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleAppointment">
          <states>
            <state name="Normal" backColor="224, 233, 246" foreColor="Black" borderColor="141, 174, 217" backColor2="179, 201, 230" backGradientStyle="Vertical" />
            <state name="Selected" backColor="219, 230, 245" borderColor="Black" backColor2="181, 202, 230" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="ScheduleCurrentDay">
          <states>
            <state name="Normal" borderColor="201, 92, 5" />
            <state name="Selected" borderColor="201, 92, 5" />
          </states>
        </style>
        <style role="ScheduleDay">
          <states>
            <state name="Selected" backColor="169, 193, 222" foreColor="Black" borderColor="169, 193, 222" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="169, 193, 222" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleNonWorkingDay">
          <states>
            <state name="Normal" backColor="239, 242, 247" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScheduleTrailingDay">
          <states>
            <state name="Normal" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="ScrollBarButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ScrollBarButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ScrollBarButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScrollBarIntersection">
          <states>
            <state name="Normal" backColor="238, 238, 239" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ScrollBarThumbHorizontal">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarThumbHorizontal_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ScrollBarThumbHorizontal_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ScrollBarThumbHorizontal_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScrollBarThumbVertical">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarThumbVertical_Normal (2)</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ScrollBarThumbVertical_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ScrollBarThumbVertical_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScrollBarTrackHorizontal">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarTrackHorizontal_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ScrollBarTrackVertical">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ScrollBarTrackVertical_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownMaxValue">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAtgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAYIBgAAAA8OhHYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAsSURBVBhXYwjPbvqPCzP8//8fqwKQOFgSXQFMDC4JwsgSIIwiiY7xOKjpPwBNvGzN+33a8gAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownNextItem">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsQAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAQIBgAAAELGJX0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAnSURBVBhXYwjPbvqPCzP8//8fqwKQOFgSXQFMDC4JwsgS////ZwAAgs9IDdAcXyoAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="SpinButtonDownNextPage">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAtwAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAoIBgAAAHjMRA0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAtSURBVChTYwjPbvqPCzP8//8fqwKQOFgSXQFMDC4JwsgSIIwiiY4HmYP+MwAAeDm57730zSwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpMinValue">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsgAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAYIBgAAAA8OhHYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAAoSURBVBhXYwjPbvqPCzP8/48bo3BAqpH5KBIwjCKJLIGsAI+Dmv4DALP9bM1EDfMAAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpPrevItem">
          <states>
            <state name="Normal" backColor="White" imageAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="SpinButtonUpPrevPage">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAwwAAAAKJUE5HDQoaCgAAAA1JSERSAAAABwAAAAoIBgAAAHjMRA0AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAA5SURBVChTY/j//z8ch2c3ASkEH0UChlEkkSWQFWCVgGG4sdgwCgekGpmPIgHDKJLIEsgKyHXQfwYAwza576JDaiUAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="SpinEditorButton">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_ComboDropDownButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_ComboDropDownButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_ComboDropDownButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="StateEditorButton">
          <states>
            <state name="Normal" backColor="White" imageAlpha="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="White" imageAlpha="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="Pressed" imageAlpha="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Checked" imageAlpha="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Unchecked" imageAlpha="Transparent" backColorAlpha="Transparent" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="StatusBarPanel">
          <states>
            <state name="Normal" backColor="0, 149, 204" foreColor="White" backColor2="179, 196, 216" backGradientStyle="None" />
          </states>
        </style>
        <style role="TabClientArea">
          <states>
            <state name="Normal" backColor="239, 246, 253" backColor2="216, 228, 242" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TabClientAreaVertical">
          <states>
            <state name="Normal" borderColor="159, 178, 199" borderAlpha="Transparent" />
          </states>
        </style>
        <style role="TabControlClientAreaHorizontal">
          <states>
            <state name="Normal" backColor="230, 231, 232" borderColor="204, 206, 219" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabControlCloseButton">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabControlTabItemAreaHorizontalTop">
          <states>
            <state name="Normal" backColor="238, 238, 242" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabControlTabItemHorizontalTop">
          <states>
            <state name="Normal" backColor="238, 238, 242" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" backColor="204, 206, 219" foreColor="White" imageAlpha="Transparent" backGradientStyle="None" themedElementAlpha="Transparent" backHatchStyle="None" />
            <state name="HotTracked" backColor="28, 151, 234" foreColor="White" imageAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="0, 122, 204" foreColor="White" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTrackSelected" backColor="0, 122, 204" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaHorizontalBottom">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaHorizontalTop">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVerticalLeft">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemAreaVerticalRight">
          <states>
            <state name="Normal" backColor="187, 206, 230" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TabItemHorizontalBottom">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemHorizontalBottom_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemHorizontalTop">
          <states>
            <state name="Normal" imageAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected" imageAlpha="Transparent" />
          </states>
        </style>
        <style role="TabItemVerticalLeft">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemVerticalLeft_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TabItemVerticalRight">
          <states>
            <state name="Normal" backColorAlpha="Transparent" borderAlpha="Transparent" />
            <state name="Selected">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_Selected</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_HotTracked</name>
              </resources>
            </state>
            <state name="HotTrackSelected">
              <resources>
                <name>Default_TabControlTabItemVerticalRight_Selected</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileCloseButton">
          <states>
            <state name="Normal" backColor="199, 80, 80" foreColor="White" fontBold="False" fontItalic="False" fontStrikeout="False" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="224, 67, 67" borderAlpha="Transparent" backColor2="255, 246, 202" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TileHeader">
          <states>
            <state name="Normal" backColor="191, 210, 234" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TileStateChangeButtonLarge">
          <states>
            <state name="HotTracked" borderAlpha="Opaque">
              <resources>
                <name>Default_TileStateChangeButtonNormal_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TileStateChangeButtonNormal">
          <states>
            <state name="HotTracked">
              <resources>
                <name>Default_TileStateChangeButtonNormal_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderDays">
          <states>
            <state name="Normal" backColor="White" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderHours">
          <states>
            <state name="Normal" backColor="White" borderColor="141, 174, 217" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewColumnHeaderMonths">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderPrimaryInterval">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" borderColor="141, 174, 217" fontBold="True" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewColumnHeaderWeeks">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" backGradientStyle="None" borderColor3DBase="255, 223, 134" backHatchStyle="None">
              <resources>
                <name>Default_GridColumnHeader_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TimelineViewColumnHeaderYears">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="160, 176, 199" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TimelineViewControlArea" borderStyle="Solid">
          <states>
            <state name="Normal" borderColor="141, 174, 217" />
          </states>
        </style>
        <style role="TimelineViewDateTimeIntervalLabel">
          <states>
            <state name="Normal" backColor="218, 231, 245" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ToolTip">
          <states>
            <state name="Normal" backColor="White" borderColor="118, 118, 118" backColor2="201, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="TrackBarButton" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" foreColor="107, 111, 115">
              <resources>
                <name>Default_TrackBarButton_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarButton_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_TrackBarButton_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TrackBarMajorTickmarks">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarMinorTickmarks">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarThumbHorizontal" buttonStyle="Office2010Button">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_TrackBarThumbHorizontal_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarThumbHorizontal_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_TrackBarThumbHorizontal_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="TrackBarThumbVertical">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_TrackBarThumbVertical_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_TrackBarThumbVertical_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACRSURBVChTYzhUofOfEAYCBmTMcKhG//+nQ/F4MUgNqqYGw/9fz+X9/32jCisGyYEwSB1CU4vJ//9POonCYLVgTR3m//896iAag9QzHOq2+v/zdsP/vw/bcGKQPAiD1EJs6rf9/+Fc0f8/91uwYpAcCIPUgTRANE22///0QBpeDFID0wDRNN3pPyGMrOH///8MAB0yYQetKgLRAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
            </state>
          </states>
        </style>
        <style role="TrackBarTrackHorizontal">
          <states>
            <state name="Normal" backColor="138, 156, 184" imageBackgroundOrigin="Container" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TrackBarTrackVertical">
          <states>
            <state name="Normal" backColor="138, 156, 184" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeCell" borderStyle="None">
          <states>
            <state name="Normal" backColor="White" foreColor="30, 57, 91" borderColor="253, 244, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeColumnHeader">
          <states>
            <state name="Normal" backColor="50, 104, 189" foreColor="White" borderColor="253, 244, 191" backColor2="225, 236, 250" backGradientStyle="None" />
            <state name="HotTracked" backColor="255, 242, 157" foreColor="Navy" borderColor="253, 244, 191" backColor2="255, 252, 230" backGradientStyle="None" />
          </states>
        </style>
        <style role="TreeControlArea" borderStyle="None">
          <states>
            <state name="Normal" backColor="197, 214, 233" borderColor="253, 244, 191" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeNode" borderStyle="None">
          <states>
            <state name="Normal" backColor="Transparent" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
            <state name="Selected" backColor="253, 244, 191" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked" backColor="169, 193, 222" foreColor="30, 57, 91" fontUnderline="False" backGradientStyle="None" backHatchStyle="None" />
            <state name="Active" backColor="253, 244, 191" foreColor="30, 57, 91" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="TreeNodeExpansionIndicator" borderStyle="None">
          <states>
            <state name="Normal">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAugAAAAKJUE5HDQoaCgAAAA1JSERSAAAABQAAAAcIBgAAAMCnh+4AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAACwEAAAsBATZwT1sAAAAwSURBVBhXY1i2bNl/EAYCBhgGC4IAsgRcEARgEiiCIADi41cJEwBhsCCywP///xkAhM1w542/jIAAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
            <state name="Expanded">
              <image>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsAAAAAKJUE5HDQoaCgAAAA1JSERSAAAABQAAAAUIBgAAAI1vJuUAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAACwEAAAsBATZwT1sAAAAmSURBVBhXY/j//z8cq6qq/gdhFIHIyEiEIEwALogsgCKIilX/AwAMNDDBOvLhBQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</image>
            </state>
          </states>
        </style>
        <style role="UltraButtonBase">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraButtonDefault">
          <states>
            <state name="Normal" imageAlpha="Transparent" />
          </states>
        </style>
        <style role="UltraCalculator">
          <states>
            <state name="Normal" backColor="218, 231, 245" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraCalculatorButtonAction" buttonStyle="Flat">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonBase" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" borderAlpha="Transparent">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraButtonBase_HotTracked</name>
              </resources>
            </state>
            <state name="Pressed">
              <resources>
                <name>Default_UltraButtonBase_Pressed</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonImmediateAction">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonNumeric">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonOperator">
          <states>
            <state name="Normal" foreColor="30, 57, 91">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorButtonPendingCalculations">
          <states>
            <state name="Normal" foreColor="30, 57, 91" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDown">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDownHostCancelButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraCalculatorDropDownHostOkButton">
          <states>
            <state name="Normal" imageBackgroundStyle="Stretched">
              <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAApAEAAAKJUE5HDQoaCgAAAA1JSERSAAAAQAAAABoIBgAAAAd8Wk8AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAEvSURBVFhH5dm7UoNQFIVh38nKytbXTVLrxJgrAcyNA4TcCBhnIvfEzpmdfdRsfAZX8TXQ/bMOxeGm0VZ3zGEEJmAPOoBp+Efy3z+hWEGiIxx1gMKNT+Qf+AWYVsej7wAqrsg7nOFIACeqyH07w6kD7DlAzA/BSIDFviTF3wE0EmAeluREJzgSYLYraMHHAI0EmG4LmocVHAkw2RQ021VwJMDrOqfptoQjAexVTpNNCUcCmKuMbD4GaCTAOMjIWhdwJICxzMjkY4Cm1XF/Aoz8jMZBDqcO4GVkLHM4zWuAoZeSXgGa5vNvgIGb0pBXgEYC9N2EBrwCNHUAxQF4BWgkQM9JqK9SOBKg63xQj1eA5hrAfLQi6vIKkDzZsb4WD3WAW/bCvtjfHwf/3ajRVvcXw+84v7XMYeEAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
            </state>
          </states>
        </style>
        <style role="UltraColorPicker">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked" imageAlpha="Transparent">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCombo">
          <states>
            <state name="Normal" backColor="237, 245, 253" borderColor="217, 217, 217" />
            <state name="HotTracked" borderColor="229, 195, 101" borderAlpha="Opaque" backGradientStyle="None" />
            <state name="Pressed" borderColor="229, 195, 101" borderAlpha="Opaque" backGradientStyle="None" />
          </states>
        </style>
        <style role="UltraComboEditor">
          <states>
            <state name="Normal" borderColor="217, 217, 217">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked" borderColor="229, 195, 101" borderAlpha="Opaque" backGradientStyle="None" />
            <state name="Pressed" borderColor="229, 195, 101" borderAlpha="Opaque" backGradientStyle="None" />
          </states>
        </style>
        <style role="UltraComboEditPortion">
          <states>
            <state name="Normal" backColor="White" imageAlpha="Transparent" backGradientStyle="None" backHatchStyle="None">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="EditMode">
              <resources>
                <name>Default_UltraComboEditPortion_EditMode</name>
              </resources>
            </state>
            <state name="Focused" imageAlpha="Transparent">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraCurrencyEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraDateTimeEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraFontNameEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked" borderAlpha="Opaque">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraFormattedTextEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraGroupBoxContentArea">
          <states>
            <state name="Normal" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraGroupBoxExpansionIndicator">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraButtonBase_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraMaskedEdit">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraNumericEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraProgressBar" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="177, 192, 214" backGradientStyle="None" backHatchStyle="None" borderColor2="Transparent" />
          </states>
        </style>
        <style role="UltraSplitterButton">
          <states>
            <state name="Normal" backColor="Transparent" borderColor="Gray" backGradientStyle="None" backHatchStyle="None" />
            <state name="HotTracked">
              <resources>
                <name>Default_CalendarComboDropDownButton_HotTracked</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraSplitterHorizontal" buttonStyle="Office2010Button">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Vertical" />
          </states>
        </style>
        <style role="UltraSplitterVertical">
          <states>
            <state name="Normal" backColor="219, 235, 255" borderColor="132, 157, 189" backColor2="200, 217, 239" backGradientStyle="Horizontal" />
          </states>
        </style>
        <style role="UltraTabControl">
          <states>
            <state name="Normal" backColor="238, 238, 242" backGradientStyle="None" borderColor3DBase="Transparent" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTabStripControl">
          <states>
            <state name="Normal" borderColor3DBase="159, 178, 199" />
          </states>
        </style>
        <style role="UltraTextEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraTile" borderStyle="Solid">
          <states>
            <state name="Normal" backColor="White" borderColor="144, 154, 166" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="UltraTimeSpanEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="UltraTimeZoneEditor">
          <states>
            <state name="Normal">
              <resources>
                <name>Default_UltraComboEditPortion_Normal</name>
              </resources>
            </state>
            <state name="HotTracked">
              <resources>
                <name>Default_UltraComboEditPortion_Focused</name>
              </resources>
            </state>
          </states>
        </style>
        <style role="ValueList">
          <states>
            <state name="Normal" backColor="239, 239, 239" borderColor="167, 171, 176" imageAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
        <style role="ValueListItem">
          <states>
            <state name="Normal" backColor="239, 239, 239" borderColor="229, 195, 101" imageAlpha="Transparent" backGradientStyle="None" themedElementAlpha="Transparent" backHatchStyle="None" />
            <state name="Selected" backColor="252, 252, 252" backGradientStyle="None" backHatchStyle="None" />
          </states>
        </style>
      </styles>
    </styleSet>
  </styleSets>
  <resources>
    <resource name="Default_CalendarComboDateButton_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAUwMAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAALJSURBVFhH1dhZTxNRGMbx8/WMC+7fx30D3DC4a3BBNCGCW7yQCxNEFChLoWDUQpGty0yltLO05PV5zrQ4UwsNmkjn4ncxnWnPf4bOOVOUiKh4pE2SH46GBnvZvRFespdDo3ICKjlwRErWgmYlextepZXdXnwhIdZyF3SGQJfu1fGpgcPYiIu12AH3Q6BD97JbpT4eklL+u1g/bom1cKPxoZO97FapQcZ/xYvXxJq/WqWtCo+ph8dV3n8ZLsFFaIUWaIYLcB7OwVk4A6fhFJwsOwHH4Rj4mtDJXnar9KeDUlr7gh0YKIEBGh062ctulf58ABszYs3hisziSjQ6dLKX3SozhPjctLcjjj9do0Mne9kd8vjh/diI4UXcNHHcMI0OnexlN+KbyvG462sdXFeNq7MlXiTgxdoO/V6Mh04vvink8cZIJZ7feZ7ANvGkN5RnhADM535zhHl+S5j5qvGz9Hj8zseE3cqI7POmygQWkwQWk7p4HBeezXBh8rtSg2/Rqal6cSSuQ9747GW3Mkb3llfYdrheA5fkem5i6fbjowbd9izSnQBbuxu05HcvQH+e7mnXvcYo48cYX34wW3rg83BL9tKjoOXHPp1lTzwr1FXl6Z+Sfs+CVvBEyS4+mKGX3coYR3xhXqyVbngelKSezaV6xNZ6f0vTi7KXYmcqXmlOwGtxjM288XnrfZ7u6ta97Fbm+B7968RKvxPb6BPbfC9Otl/c3KC4a0Pi5kcg8pdG/wHGXRsWBx3Oz37dZWf60Nmne9mtzAnEu4ZY2WGxVyNi58bEyU+IW5iCGMxI0f7/XAv0+JPoieou3YdO9rJbmdHdUiquanb+mzgFj2vFtaIzu2Nc22uoNLGv0mpGEZ+YbhFzEtNlMS+lUg6wcz0r6+smGJCB9A7guBzfRA+wS/flxZxqkkSsVfS/PuZjzZKN7goN9oqI+gWobEsFgRiAlAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDateButton_Normal" imageBackgroundStyle="Stretched" backColorAlpha="Transparent" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAOgEAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACwSURBVFhH3dg7FoIwEIXh7H8r7oDO0kr06PEBCPGJgoqIojZjkuKmcQFeir+YOVN89SgRUb0gFLas28H7Qy36/KHJeq3b4X8d/HseX5oFWcBn5Zsujy/Mgizg0+JFl8efWrqAXx1buoBP8idd3cDH+YMu4KNDQ1c38Mt9Qxfwi92dLuDn25ou4Gebmi7gp+sbXcBPdEUX8OPsShfwIzOwBXwwiCVMLzRZr8Pzvj5EfQE37w8WaXsrmwAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDateButton_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAOAIAAAKJUE5HDQoaCgAAAA1JSERSAAAALwAAABgIBgAAAGpYpLkAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAGuSURBVFhH1dhtK0NxGMfx6/2MEIlIyM3LUBIlEQmbmW3G7pKSJHLzKnh+paVoSKiVlO0drM3MzeX6nZ2VY/NQnf+D77PzP+dznVOn/zkkIpQ69gkveIwJXrgpdaLwRY/keUQKqTEpXIxL8WLCdcEFH5zwwk3sbZD8+aiUrqelfDcv5ftFeX9Ycl1wwQcnvHAT+xqleDlpHfCRCcjnU1ALubCg5YMTXriJ/U3ymp6uwJ8j8vUSdW3wwQkv3MSBZindzFqT1S6IVcrWJtm4s1y1xL9lDaBOeOEmDrZI6XbOejR14RY+bqcnsJNs0lnud7gghtFBc1FtQ1vXItqaFtZCWtBuVQtoK5pfW7bTN0vOq1XxIcsLN3G49Q/8jwFcdedtvLqJI21m4tVNvNFuJl7dhuNjHWbi1U2c7DQTr27D8ZtdZuLVTbzVbSZe3cTbPWbi1U2802smXt3Eu30OfL1FbsmBVzfxXr+ZeHUT7w84tsT1FrmlCt7eEqub+GBQildT8p7xG/ExAie8cFPqNCF8OCTF9Iy84Tvx0efa4IMTXrgrvz7OksJHw8YEr4jQN1VGbAhOWnI8AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_CalendarComboDropDownButton_HotTracked" backColor="253, 244, 191" borderColor="229, 195, 101" backGradientStyle="None" />
    <resource name="Default_CalendarComboDropDownButton_Pressed" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAsgAAAAKJUE5HDQoaCgAAAA1JSERSAAAADgAAABUIBgAAAHZZ34kAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADrEAAA6xAZyMH9oAAAAoSURBVDhPYzhUrv2fHAzW+P9JF0l4VCMePKoRDx7ViAcPRY2kY+3/AONlqeSltQNXAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ComboDropDownButton_HotTracked" backColor="253, 244, 191" borderColor="229, 195, 101" imageAlpha="Transparent" borderAlpha="Opaque" backGradientStyle="None" />
    <resource name="Default_ComboDropDownButton_Normal" backColor="Transparent" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ComboDropDownButton_Pressed" backColor="229, 195, 101" borderColor="229, 195, 101" imageAlpha="Transparent" borderAlpha="Opaque" backGradientStyle="None" />
    <resource name="Default_DropDownButton_Normal" themedElementAlpha="Transparent" />
    <resource name="Default_ExplorerBarGroupHeader_HotTracked" backColor="253, 244, 191" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3" />
    <resource name="Default_ExplorerBarGroupHeader_Normal" backColor="255, 242, 157" foreColor="30, 57, 91" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 3" />
    <resource name="Default_GridBandHeader_Normal" backColor="253, 244, 191" backGradientStyle="None" />
    <resource name="Default_GridCell_Selected" backColor="148, 215, 243" borderColor="218, 220, 221" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_GridCellProxy_EditMode" foreColor="Black" borderColor="218, 220, 221" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="2, 2, 2, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA5AEAAAKJUE5HDQoaCgAAAA1JSERSAAAAGQAAABcIBgAAAP7j5BMAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAA68AAAOvAGVvHJJAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M0BrLToAAAAV5JREFUSEvtlbFKw1AUhvMMLW3yCNVBH8OhixTMa+js7qQ24iJSEBS0ILi6tWragiCaDpVCMSRECU1DqGmFGC/h956Ig4NDTbNIDvxT7j3f/c8N9xfKm1W5tH/FcttN5Havka+qXC3klXZilQ46rLJVkwUClI87uNF6GAwG0HUdhmHANM1EUh91rNQ1LNbumFDYaaLx0IubW5YF27bhOA5Go1Es13Vn1vfee8NG8bALochHRA4IMBwO4XkefN/HZDLBdDr9s2j/+NWHeNSHUFDU2AU5IAAtCIIAYRgmFvURT54I0orvgEZEDugDYwxRFCUW9RFPTQ7Za8cXRTMki3QCWjCPoj7imfUTQqNKFUJ/RQb5rTLITJVBZqp/DEn97brt66m8wi9vH1+QBR72q+ddaKY9tzx5ZxH64xCVhoOli2cmrCn1dQp7kWcxRaXEk0ziQSPxEyTVMgdsXPbkT1qmP49wxe74AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</imageBackground>
    </resource>
    <resource name="Default_GridColumnHeader_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAACAEAAAKJUE5HDQoaCgAAAA1JSERSAAAADAAAABQIBgAAALnw3BEAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADnAAAA5wAZTZNMQAAAB+SURBVDhPjcuhDoJgAABhn4tXMtlMNhPNZCPZTDYTiWaymWBMB5PJZDidlN8xN65e+NrdrE6jEPK5Mrb/oVgoDOVSYbiuFIbbWmGoYoWh3igM963C0CQKw2OnMLR7heF5UBi6o8LQpwrDK1MY3ieF4XNWGL4XhWHIlWnwovADalU0ptp9WcwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_GridColumnHeader_Normal" backColor="246, 250, 251" imageBackgroundStyle="Stretched" borderAlpha="Transparent" backColor2="211, 219, 233" imageBackgroundStretchMargins="1, 1, 1, 1">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAqQAAAAKJUE5HDQoaCgAAAA1JSERSAAAACAAAAAkIBgAAAA9TbS4AAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADnAAAA5wAZTZNMQAAAAfSURBVChTY1iw4fh/fJjh1vOv/3HhYaQARODGx/8DALcE/KDEN25PAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ProgressBarFill_Normal" imageBackgroundStyle="Stretched" borderAlpha="Transparent" imageBackgroundStretchMargins="1, 2, 1, 2">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAGQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACgAAAAwIBgAAAFtrLKAAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADrEAAA6xAZyMH9oAAACPSURBVChTYxBr9vtPDAYrXH5lHRwvQ2LDMFxh7eEWMK453IwVwxVm7M+C4uz/6UAaHcMVxu9PgePYfUn/4/Yl/4/bj8BwhSBJfBiu0OaiBxxbn3eH4AtQfNEdoVD8ljocS9xExSAxuEKFZ+ZI2AwVPzVDKJR9pfNf7jUEy2LBcIWiL+UQ+AUmhiskjP3+AwCosg8vnAxhNAAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScheduleDayHeader_Normal" imageBackgroundStyle="Stretched">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAhgAAAAJHSUY4OWEKABQAggAA/9hr/+Z0/+Bw/+15/9xtAAAAAAAAAAAAIf8LTkVUU0NBUEUyLjADAQEAACH5BAAAAAAALAAAAAAKABQAAAg4AAcIHEiwYMEACBMqXMiwIUIBECNKnEixIkQCGDNqBMCxo8ePIENy1EiSgMWTKCM6XMkyoUGCAQEAOwsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_ScrollBarButton_HotTracked" backColor="218, 218, 218" foreColor="Black" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarButton_Normal" backColor="232, 232, 236" foreColor="96, 96, 96" imageBackgroundStyle="Stretched" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" imageBackgroundStretchMargins="2, 2, 2, 2" />
    <resource name="Default_ScrollBarButton_Pressed" backColor="96, 96, 96" foreColor="White" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbHorizontal_HotTracked" backColor="166, 166, 166" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbHorizontal_Normal" backColor="208, 209, 215" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbHorizontal_Pressed" backColor="96, 96, 96" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbVertical_HotTracked" backColor="166, 166, 166" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbVertical_Normal" imageBackgroundOrigin="Client" imageBackgroundAlpha="Transparent" borderAlpha="Transparent" themedElementAlpha="Transparent" />
    <resource name="Default_ScrollBarThumbVertical_Normal (1)" backColor="208, 209, 215" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbVertical_Normal (2)" backColor="208, 209, 215" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarThumbVertical_Pressed" backColor="96, 96, 96" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarTrackHorizontal_Normal" backColor="232, 232, 236" imageAlpha="Transparent" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_ScrollBarTrackVertical_Normal" backColor="232, 232, 236" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_TabControlTabItemHorizontalBottom_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 0, 3, 3" />
    <resource name="Default_TabControlTabItemHorizontalBottom_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 0, 3, 3" />
    <resource name="Default_TabControlTabItemHorizontalTop_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAEQEAAAKJUE5HDQoaCgAAAA1JSERSAAAACwAAABcIBgAAAN24tcYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACHSURBVDhP7cu7CoNAEIXhff+3EUkjWHvBgIZEEy+x3tk2WKQ5cZDIwlHwASz+5sw3BoAJ4xzBJdpN7+pW+Jm+u/0fjA/rUSj/YcWP0eH+5nQnfJsP1SCU7oSr3qHsON0JX1uHohVKd8L5S5A9Od0JZ40gqS2lO+G0sXOykT2x14kP4e0WHOEH5H56X/2y7jwAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemHorizontalTop_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 3, 0">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAJgEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABgIBgAAAPOgfQwAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACcSURBVDhP7ZO9CoMwGEX7/k9kXZztIjjooGC3gov/Wgt6mxswixL8JKPDCSS59yRD8gCgSd8l/FcOL8ysMMPs1tNDUpQIogLdvGL4wQozzLJjBE9l7U+UN5hlxwh4taOgDXZuwS3YCfqZL+w87gX8JBLcC9rvKsK9oFGLEvaCSW0IcC+o1aIEI4jzj57U0yKCHXb16dW4XMILM/wBiQmTNG1C29MAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TabControlTabItemVerticalLeft_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 0, 3" />
    <resource name="Default_TabControlTabItemVerticalLeft_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="3, 3, 0, 3" />
    <resource name="Default_TabControlTabItemVerticalRight_HotTracked" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 3, 3, 3" />
    <resource name="Default_TabControlTabItemVerticalRight_Selected" imageBackgroundStyle="Stretched" imageBackgroundStretchMargins="0, 3, 3, 3" />
    <resource name="Default_TileStateChangeButtonNormal_HotTracked" backColor="54, 101, 179" foreColor="White" backColor2="255, 251, 226" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_TrackBarButton_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAATwIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADnAAAA5wAZTZNMQAAAHFSURBVDhPjZJPTxNRFMXnm7QfQUBQRFjIisRvpskUIiCwGAm6UBMCEl0ICyIopQoDQggx/DF02saSdtqhzGtn2lzOea2k00XTxS+5vfecM6/3PUNEIhS+jcVBAthAWrBmL96pj/wobI0m3ORzUc6ShN6hNKpZDWv2OKOm3dNmfmpXzl5K3b+Q8OZQqtdr4mfeAAv1Fwkrx3pGDbWRgOL2iHl7/gKCc6nmV8VPz4HXHcxjtiZ19VeopUcHFL8/iZdSEy3zCoQzXaldf9Zaeug1ij+GLeVYOPbvluhVd5xZaI+wE0voRcBjO/T2ml+/muoJnoIeeg135xE2nUb6NIaJnlCZBe2h13CTDMiIymLjDpbVAyq7qD30Gu7ukM1rqxXWdUgvBO6mvmp6ETBoqdxbbPaPqH8fwPvu5D/iKi9E5d4hYNAySqmHsfL+uD5SUN7Bo/nUlaCc0lp66NUPqfRzwPTTpjSCvAQ3+1ItroOvHWxIUMHzhoZaeu5fog751W/7zpQWNGo5CW9P8LWkBF4K9anuNc2TQu1/331AM6TP9A6e4ajL+J+X0qh7GtbscUZNuycSQMp7D2LABDaQFqzZi0X1YtwBTqtIy4MYCoQAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_Normal" imageBackgroundStyle="Centered" borderAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAuQEAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADnAAAA5wAZTZNMQAAAEvSURBVDhPnZJPSwJRHEX9Jvot2/Rn2UayZW2KynJpQbTITRJEIjRRCJqIJA7lJA2mDUnR7Z3HjDjhYsbFgTe/d899M8PLSIqxsrmXM2wbHINCWDPL/c/HHghtbBV1cXWnTn+oz+mvhTUz9sjMO/Oyc3xWlecH+gh+FsIeGbKxAjMoFE+r8iffiSCLYwvMIrduXq3vjfU2miaCLA4uBfvlSl2v/lcqcHDtt9+3XLnDIBU4uBSo502WAtcWdF/GSxEVOLePz2q7o1Tg4NqfWDq/UbPnpwIHl4LsWv5ITmugRvc9EWRxcGcXabdU0UNnmAiyOLObGJY4OyeXqjVcOU/eQtgjQzbyZgVhSWE1f6iD8rUqtbbqzYGFNTP2opMjYgVgAllCnGJAsCeGs2w8r8wffiZHKJKORMMAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarButton_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAbQIAAAKJUE5HDQoaCgAAAA1JSERSAAAAEAAAABAIBgAAAB/z/2EAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAADnAAAA5wAZTZNMQAAAHjSURBVDhPjZJNTxNRGIXvP2l/BUYjwajRCCbucOHGj6hoKSSggKYFFbASIYMxxqiJrIkrF7JRk06B4UPNKCi1FTI1Gh1i2c10AS3H91xpw5iGzOJpbt57ntN7J1cBCJBJNkSFccEWsAPXnEX/z9cWhTcXVGZg3/hcqhHOyzP4lb6E39NtGq454x4zzAYKtHx7v52fbBXhCooLcXifrmFzpQ+b2T54H7vxZ75d7zHDbLVE/2QGDxj5F6exbsXgL/egnE/UxV+6Dnc2Bmbp6AJz+GDUGj0Md+YqSsu9qHzr3xNvqUdn6dBVZqpxwpk6i+JiJyqrt1BZ25vy6oDO0qGrzJEm252JofTlJradwVB4n2/IKWKgq8z7h+Re7ahw8/twKLbW7miHrjLH5P6WFBSGgB+pUJSdIe3QVaZxxHatOEq5fuDnSCi8lYQUxEFXmQ+OThReX8TGh25pv1dX2M22ZIrvukCHrjIfHotYT1vgznWEOoWfTUo2Djp09UNKPzpu5F6dw/p857+SOifhP/vZhGQ6wCyd2kvks0w/PmHnp87r9uL7LvhfkygX7mJLPhjvvCEz7uUkw2zgKddKnjQbs89PwXl7WX+k3XDGPWaqcqCgSvrZyYhgCLaAHbjmLBLMQ/0FucH8PuBvg3UAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAAKwEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAChSURBVChThYpHEsIwEAT3OeT8T9KPyNFn4A5ccUAcoLzsbNkqYRVw6JJ2uuk26fE/KJx2OX2cvgJP4awjx5Gf17EHdniK5m1OjUQXEQWww1O0aGXRyEMj8RQvm1k09MAOT/Gq8TsST8m6LseBX+eBB3Z4SjY1PYphvsETM9N9W3XCvv1jh9dIw13FSg3kzp2NgNmXNcDr7h8RMEFJHndjegPrKIOOCmG1kQAAAABJRU5ErkJgggsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_Normal" imageBackgroundStyle="Centered" imageAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA/wAAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAB1SURBVChThcqxCYAwEIXh7OwITuEM9i7gDBYWIgoKATWEECUinlwgp1EOi7957xNJmsFfHrnjZCNk3cFGSK87G6HZODZCUm1shIbJshHqpGEjlBclNKP+hLtHADese0UFgL9HT1i1SwQihAX4BFiEsDcAAHEBzzuA0sZWwMgAAAAASUVORK5CYIILAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbHorizontal_Pressed">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAASAEAAAKJUE5HDQoaCgAAAA1JSERSAAAACQAAAA0IBgAAAHsARAYAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAAC+SURBVChThYrNCkFhFEXPA0mSSJLkQWSiDGVApOQ/UkrJwxidkpGJmJHfgZkiRS6bfcuNbjeD1fm+tbZozoV/iBbcOGncEXbRogfncRK3acYGPbtoyYvrLAtjUbJBzy5a8QH7liPsojU/nrumI+yizQCe24Yj7KLtIO7LMh6bug16dtFOCJd53sRY1yw+jl2A97AbxnGSNjFWVetNz26OzGEvgsMoZcH/p1kjov0o1sMEeL/9z4joIPY+3w7yAqoWYNmk2bA5AAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbVertical_HotTracked">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAADwEAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAACFSURBVChTY/hwyOT/x8PG/z8eMfr/6ajh/0/HDP5/Pq7///MJvf9fTur+/3JK5z8QMCBjsKZ/328B8c3//76h4xtg/PW0NopGuKbfzyf///0MhCcB8UQw/vN0AkLjGS24RoKakDV+O6sJ1kiUpj9P+xEaz2n8p41NMA1Y/URy6JEWT/8ZAKe2g47t9hUhAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_TrackBarThumbVertical_Normal" imageBackgroundStyle="Centered" imageAlpha="Transparent">
      <imageBackground>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5CaXRtYXABAAAABERhdGEHAgIAAAAJAwAAAA8DAAAA6AAAAAKJUE5HDQoaCgAAAA1JSERSAAAADQAAAAkIBgAAAOl6pmoAAAAEZ0FNQQAAsY8L/GEFAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAJcEhZcwAAJX4AACV+AcnyvF0AAABeSURBVChTYyhunvmfEAYCBmQM1vTzzz+cePW2wxgawZo+f/+NF6NrBGt68+kHQYysEazp2duvRGGYRrCmBy8+E8QYNt18/AEvxuqny/fe4sToGuCaCGFkDf///2cAAHezfrdmwSeBAAAAAElFTkSuQmCCCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</imageBackground>
    </resource>
    <resource name="Default_UltraButtonBase_HotTracked" backColor="255, 215, 80" foreColor="Black" borderAlpha="Transparent" backGradientStyle="None" />
    <resource name="Default_UltraButtonBase_Normal" backColor="245, 246, 247" foreColor="Black" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_UltraButtonBase_Pressed" backColor="251, 140, 60" foreColor="Black" borderAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
    <resource name="Default_UltraComboEditPortion_EditMode" borderColor="229, 195, 101" imageAlpha="Transparent" />
    <resource name="Default_UltraComboEditPortion_Focused" borderColor="229, 195, 101" imageAlpha="Transparent" />
    <resource name="Default_UltraComboEditPortion_Normal" backColor="White" borderColor="217, 217, 217" imageAlpha="Transparent" backGradientStyle="None" backHatchStyle="None" />
  </resources>
</styleLibrary>