﻿namespace Accounting
{
    partial class UpdateNotification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateNotification));
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.ForeColor = System.Drawing.Color.Coral;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.ultraLabel1.Location = new System.Drawing.Point(125, 7);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(554, 57);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "HDSAccoungting THÔNG BÁO CẬP NHẬT PHIÊN BẢN VỚI CÁC TÍNH NĂNG MỚI";
            // 
            // btnOk
            // 
            appearance2.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnOk.Appearance = appearance2;
            this.btnOk.Location = new System.Drawing.Point(743, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 28);
            this.btnOk.TabIndex = 42;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // ultraPanel1
            // 
            appearance3.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance3.ImageBackground = global::Accounting.Properties.Resources.Notify_Update_1;
            this.ultraPanel1.Appearance = appearance3;
            this.ultraPanel1.Location = new System.Drawing.Point(242, 70);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(335, 222);
            this.ultraPanel1.TabIndex = 43;
            // 
            // ultraGroupBox1
            // 
            appearance4.BackColor = System.Drawing.Color.White;
            appearance4.BackColor2 = System.Drawing.Color.White;
            appearance4.BackColorAlpha = Infragistics.Win.Alpha.UseAlphaLevel;
            appearance4.BackColorDisabled = System.Drawing.Color.White;
            appearance4.BackColorDisabled2 = System.Drawing.Color.White;
            appearance4.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance4.BorderColor = System.Drawing.Color.DarkGray;
            appearance4.BorderColor2 = System.Drawing.Color.White;
            appearance4.BorderColor3DBase = System.Drawing.Color.White;
            this.ultraGroupBox1.Appearance = appearance4;
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            appearance5.BackColor = System.Drawing.Color.White;
            appearance5.ImageAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBox1.ContentAreaAppearance = appearance5;
            this.ultraGroupBox1.Location = new System.Drawing.Point(92, 314);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(635, 207);
            this.ultraGroupBox1.TabIndex = 44;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.btnOk);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 527);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(830, 34);
            this.ultraPanel2.TabIndex = 45;
            this.ultraPanel2.Visible = false;
            // 
            // UpdateNotification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(830, 561);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraLabel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(846, 600);
            this.MinimizeBox = false;
            this.Name = "UpdateNotification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông báo cập nhật phiên bản";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UpdateNotification_FormClosing);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
    }
}