﻿namespace Accounting
{
    partial class FFilters<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinScrollBar.ScrollBarLook scrollBarLook1 = new Infragistics.Win.UltraWinScrollBar.ScrollBarLook();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FFilters));
            this.palFill = new Infragistics.Win.Misc.UltraPanel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBottom = new Infragistics.Win.Misc.UltraPanel();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.palFill.ClientArea.SuspendLayout();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.palBottom.ClientArea.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // palFill
            // 
            // 
            // palFill.ClientArea
            // 
            this.palFill.ClientArea.Controls.Add(this.uGrid);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(9, 9);
            this.palFill.Name = "palFill";
            scrollBarLook1.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.Office2010;
            this.palFill.ScrollBarLook = scrollBarLook1;
            this.palFill.Size = new System.Drawing.Size(631, 361);
            this.palFill.TabIndex = 0;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(631, 361);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // palBottom
            // 
            // 
            // palBottom.ClientArea
            // 
            this.palBottom.ClientArea.Controls.Add(this.btnCancel);
            this.palBottom.ClientArea.Controls.Add(this.btnOk);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(9, 370);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(631, 41);
            this.palBottom.TabIndex = 1;
            // 
            // btnCancel
            // 
            appearance1.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnCancel.Appearance = appearance1;
            this.btnCancel.Location = new System.Drawing.Point(550, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 29);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            appearance2.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnOk.Appearance = appearance2;
            this.btnOk.Location = new System.Drawing.Point(462, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(82, 29);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 420);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            //this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FFilters";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm nhanh";
            this.palFill.ClientArea.ResumeLayout(false);
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.palBottom.ClientArea.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel palFill;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraPanel palBottom;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnOk;
    }
}