﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HDBooks")]
[assembly: AssemblyDescription("Phần mềm kế toán doanh nghiệp")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Công ty phần mềm HDSoft")]
[assembly: AssemblyProduct("HDBooks")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0bc0615a-f284-40b6-ad61-b81b1254c53f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("20.5.23.34")]
[assembly: AssemblyFileVersion("20.5.23.34")]
[assembly: NeutralResourcesLanguageAttribute("vi-VN")]
