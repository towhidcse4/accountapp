﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.Frm.TextMessage;
using System.Text.RegularExpressions;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using System.Threading;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm
{
    public partial class FadjuststoreRepository : CustormForm
    {
        #region  khai báo
        private readonly IRepositoryService _IRepositoryService;
        private readonly IAccountService _IAccountService;
        public static bool isClose = true;
        bool Them = true;
        #endregion
        public FadjuststoreRepository()
        {
            #region khởi tạo
            InitializeComponent();
            txtDate.DateTime = DateTime.Now;
            _IRepositoryService = IoC.Resolve<IRepositoryService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            LoadObjRipository();
            LoadObjAccountUp();
            LoadObjAcountReduce();

            #endregion
        }

        #region Data Combox

        public void LoadObjRipository()
        {
            // Kho
            //cbbRespository.DataSource = _IRepositoryService.GetAll().OrderBy(p => p.RepositoryName).Where(p => p.IsActive == true).ToList();
            cbbRespository.DataSource = _IRepositoryService.GetAll_ByOrderbyIsActive(true);
            cbbRespository.DisplayMember = "RepositoryName";
            Utils.ConfigGrid(cbbRespository, ConstDatabase.Repository_TableName);
        }

        public void LoadObjAccountUp()
        {
            // tài khoản có
            //cbbAcountUp.DataSource = _IAccountService.GetAll().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList();
            cbbAcountUp.DataSource = _IAccountService.GetListActiveOrderBy(true);
            cbbAcountUp.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAcountUp, ConstDatabase.Account_TableName);
        }
        public void LoadObjAcountReduce()
        {
            //tài khoản nợ
            //cbbAcountReduce.DataSource = _IAccountService.GetAll().OrderBy(p => p.AccountNumber).Where(p => p.IsActive == true).ToList();
            cbbAcountReduce.DataSource = _IAccountService.GetListActiveOrderBy(true);
            cbbAcountReduce.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAcountReduce, ConstDatabase.Account_TableName);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.MaterialGoods_TableName);
        }
        #endregion

        private void rd_Allrepositorry_CheckedChanged(object sender, EventArgs e)
        {
            if (rd_Allrepositorry.Checked == true)
            {

                cbbRespository.Enabled = false;
            }
            else
            {

                cbbRespository.Enabled = true;
            }
        }
    }
}
