﻿namespace Accounting.Frm
{
    partial class FadjuststoreRepository
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            this.Gmain = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.btnHelp = new Infragistics.Win.Misc.UltraButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbAcountReduce = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbAcountUp = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbRespository = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.checkValues = new System.Windows.Forms.CheckBox();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.btnGetdata = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.chekMaterialgoods = new System.Windows.Forms.CheckBox();
            this.rd_Allrepositorry = new System.Windows.Forms.RadioButton();
            this.rd_Repository = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.Gmain)).BeginInit();
            this.Gmain.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAcountReduce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAcountUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRespository)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // Gmain
            // 
            this.Gmain.Controls.Add(this.ultraButton3);
            this.Gmain.Controls.Add(this.ultraButton2);
            this.Gmain.Controls.Add(this.btnHelp);
            this.Gmain.Controls.Add(this.groupBox2);
            this.Gmain.Controls.Add(this.groupBox1);
            this.Gmain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gmain.Location = new System.Drawing.Point(0, 0);
            this.Gmain.Name = "Gmain";
            this.Gmain.Size = new System.Drawing.Size(930, 403);
            this.Gmain.TabIndex = 0;
            this.Gmain.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton3
            // 
            this.ultraButton3.Location = new System.Drawing.Point(843, 367);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(75, 30);
            this.ultraButton3.TabIndex = 4;
            this.ultraButton3.Text = "Hủy bỏ";
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(762, 367);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(75, 30);
            this.ultraButton2.TabIndex = 3;
            this.ultraButton2.Text = "Điều chỉnh";
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(12, 367);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(75, 30);
            this.btnHelp.TabIndex = 2;
            this.btnHelp.Text = "Giúp";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.uGrid);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 81);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(924, 280);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 16);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(918, 261);
            this.uGrid.TabIndex = 10;
            this.uGrid.Text = "uGrid";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.cbbAcountReduce);
            this.groupBox1.Controls.Add(this.cbbAcountUp);
            this.groupBox1.Controls.Add(this.cbbRespository);
            this.groupBox1.Controls.Add(this.checkValues);
            this.groupBox1.Controls.Add(this.ultraLabel3);
            this.groupBox1.Controls.Add(this.btnGetdata);
            this.groupBox1.Controls.Add(this.ultraLabel2);
            this.groupBox1.Controls.Add(this.txtDate);
            this.groupBox1.Controls.Add(this.ultraLabel1);
            this.groupBox1.Controls.Add(this.chekMaterialgoods);
            this.groupBox1.Controls.Add(this.rd_Allrepositorry);
            this.groupBox1.Controls.Add(this.rd_Repository);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(924, 81);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // cbbAcountReduce
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAcountReduce.DisplayLayout.Appearance = appearance1;
            this.cbbAcountReduce.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAcountReduce.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAcountReduce.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAcountReduce.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cbbAcountReduce.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAcountReduce.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cbbAcountReduce.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAcountReduce.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAcountReduce.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAcountReduce.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cbbAcountReduce.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAcountReduce.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAcountReduce.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAcountReduce.DisplayLayout.Override.CellAppearance = appearance8;
            this.cbbAcountReduce.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAcountReduce.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAcountReduce.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cbbAcountReduce.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cbbAcountReduce.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAcountReduce.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cbbAcountReduce.DisplayLayout.Override.RowAppearance = appearance11;
            this.cbbAcountReduce.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAcountReduce.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cbbAcountReduce.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAcountReduce.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAcountReduce.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAcountReduce.Location = new System.Drawing.Point(772, 18);
            this.cbbAcountReduce.Name = "cbbAcountReduce";
            this.cbbAcountReduce.Size = new System.Drawing.Size(82, 22);
            this.cbbAcountReduce.TabIndex = 1037;
            // 
            // cbbAcountUp
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAcountUp.DisplayLayout.Appearance = appearance13;
            this.cbbAcountUp.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAcountUp.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAcountUp.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAcountUp.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cbbAcountUp.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAcountUp.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cbbAcountUp.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAcountUp.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAcountUp.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAcountUp.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cbbAcountUp.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAcountUp.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAcountUp.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAcountUp.DisplayLayout.Override.CellAppearance = appearance20;
            this.cbbAcountUp.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAcountUp.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAcountUp.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cbbAcountUp.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cbbAcountUp.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAcountUp.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cbbAcountUp.DisplayLayout.Override.RowAppearance = appearance23;
            this.cbbAcountUp.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAcountUp.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cbbAcountUp.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAcountUp.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAcountUp.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAcountUp.Location = new System.Drawing.Point(578, 18);
            this.cbbAcountUp.Name = "cbbAcountUp";
            this.cbbAcountUp.Size = new System.Drawing.Size(82, 22);
            this.cbbAcountUp.TabIndex = 1036;
            // 
            // cbbRespository
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbRespository.DisplayLayout.Appearance = appearance25;
            this.cbbRespository.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbRespository.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbRespository.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbRespository.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.cbbRespository.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbRespository.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.cbbRespository.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbRespository.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbRespository.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbRespository.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.cbbRespository.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbRespository.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.cbbRespository.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbRespository.DisplayLayout.Override.CellAppearance = appearance32;
            this.cbbRespository.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbRespository.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbRespository.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.cbbRespository.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.cbbRespository.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbRespository.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.cbbRespository.DisplayLayout.Override.RowAppearance = appearance35;
            this.cbbRespository.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbRespository.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.cbbRespository.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbRespository.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbRespository.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbRespository.Location = new System.Drawing.Point(54, 16);
            this.cbbRespository.Name = "cbbRespository";
            this.cbbRespository.Size = new System.Drawing.Size(91, 22);
            this.cbbRespository.TabIndex = 1035;
            // 
            // checkValues
            // 
            this.checkValues.AutoSize = true;
            this.checkValues.Location = new System.Drawing.Point(862, 22);
            this.checkValues.Name = "checkValues";
            this.checkValues.Size = new System.Drawing.Size(53, 17);
            this.checkValues.TabIndex = 1034;
            this.checkValues.Text = "Giá trị";
            this.checkValues.UseVisualStyleBackColor = true;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(666, 21);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel3.TabIndex = 1031;
            this.ultraLabel3.Text = "Tài khoản đ/c giảm";
            // 
            // btnGetdata
            // 
            this.btnGetdata.Location = new System.Drawing.Point(385, 17);
            this.btnGetdata.Name = "btnGetdata";
            this.btnGetdata.Size = new System.Drawing.Size(75, 23);
            this.btnGetdata.TabIndex = 1030;
            this.btnGetdata.Text = "Lấy số liệu";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(472, 21);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel2.TabIndex = 1029;
            this.ultraLabel2.Text = "Tài khoản đ/c tăng";
            // 
            // txtDate
            // 
            this.txtDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.txtDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.txtDate.Location = new System.Drawing.Point(249, 17);
            this.txtDate.MaskInput = "";
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(130, 21);
            this.txtDate.TabIndex = 1028;
            this.txtDate.Value = null;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(160, 21);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(92, 23);
            this.ultraLabel1.TabIndex = 4;
            this.ultraLabel1.Text = "Ngày kiểm kê";
            // 
            // chekMaterialgoods
            // 
            this.chekMaterialgoods.AutoSize = true;
            this.chekMaterialgoods.Location = new System.Drawing.Point(113, 47);
            this.chekMaterialgoods.Name = "chekMaterialgoods";
            this.chekMaterialgoods.Size = new System.Drawing.Size(148, 17);
            this.chekMaterialgoods.TabIndex = 2;
            this.chekMaterialgoods.Text = "Chỉ lấy vật tư có phát sinh";
            this.chekMaterialgoods.UseVisualStyleBackColor = true;
            // 
            // rd_Allrepositorry
            // 
            this.rd_Allrepositorry.AutoSize = true;
            this.rd_Allrepositorry.Location = new System.Drawing.Point(9, 46);
            this.rd_Allrepositorry.Name = "rd_Allrepositorry";
            this.rd_Allrepositorry.Size = new System.Drawing.Size(98, 17);
            this.rd_Allrepositorry.TabIndex = 1;
            this.rd_Allrepositorry.TabStop = true;
            this.rd_Allrepositorry.Text = "Tất cả các kho";
            this.rd_Allrepositorry.UseVisualStyleBackColor = true;
            this.rd_Allrepositorry.CheckedChanged += new System.EventHandler(this.rd_Allrepositorry_CheckedChanged);
            // 
            // rd_Repository
            // 
            this.rd_Repository.AutoSize = true;
            this.rd_Repository.Location = new System.Drawing.Point(9, 18);
            this.rd_Repository.Name = "rd_Repository";
            this.rd_Repository.Size = new System.Drawing.Size(44, 17);
            this.rd_Repository.TabIndex = 0;
            this.rd_Repository.TabStop = true;
            this.rd_Repository.Text = "Kho";
            this.rd_Repository.UseVisualStyleBackColor = true;
            // 
            // FadjuststoreRepository
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 403);
            this.Controls.Add(this.Gmain);
            this.Name = "FadjuststoreRepository";
            this.Text = "FadjuststoreRepository";
            ((System.ComponentModel.ISupportInitialize)(this.Gmain)).EndInit();
            this.Gmain.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAcountReduce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAcountUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRespository)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox Gmain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rd_Allrepositorry;
        private System.Windows.Forms.RadioButton rd_Repository;
        private System.Windows.Forms.CheckBox chekMaterialgoods;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraButton btnGetdata;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDate;
        private System.Windows.Forms.CheckBox checkValues;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton btnHelp;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbRespository;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAcountReduce;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAcountUp;
    }
}