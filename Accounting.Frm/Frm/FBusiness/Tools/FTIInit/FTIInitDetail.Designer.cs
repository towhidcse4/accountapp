﻿
namespace Accounting
{
    partial class FTIInitDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.txtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtUnit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbToolsCategory = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAllocatedAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAllocationTimes = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.chkIsIrrationalCost = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.cbbAllocationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAllocationAwaitAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblParentID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtQuantity = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtUnitPrice = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtToolsCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMaterialGoodsName = new Infragistics.Win.Misc.UltraLabel();
            this.txtToolsName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cldIncrementDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAllocationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRemainAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRemainAllocationTimes = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.chkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.materialGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accountBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.costSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.departmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToolsCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsIrrationalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationAwaitAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldIncrementDate)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGoodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel3);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(798, 240);
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAmount);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtUnit);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel12);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel11);
            this.ultraPanel3.ClientArea.Controls.Add(this.cbbToolsCategory);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel7);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAllocatedAmount);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel9);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAllocationTimes);
            this.ultraPanel3.ClientArea.Controls.Add(this.chkIsIrrationalCost);
            this.ultraPanel3.ClientArea.Controls.Add(this.cbbAllocationType);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel8);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel24);
            this.ultraPanel3.ClientArea.Controls.Add(this.cbbAllocationAwaitAccount);
            this.ultraPanel3.ClientArea.Controls.Add(this.lblParentID);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtQuantity);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtUnitPrice);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtToolsCode);
            this.ultraPanel3.ClientArea.Controls.Add(this.lblMaterialGoodsName);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtToolsName);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel3.ClientArea.Controls.Add(this.cldIncrementDate);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel10);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtAllocationAmount);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel17);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtRemainAmount);
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraLabel6);
            this.ultraPanel3.ClientArea.Controls.Add(this.txtRemainAllocationTimes);
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(798, 244);
            this.ultraPanel3.TabIndex = 0;
            // 
            // txtAmount
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtAmount.Appearance = appearance1;
            this.txtAmount.AutoSize = false;
            this.txtAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAmount.Location = new System.Drawing.Point(505, 103);
            this.txtAmount.MinValue = "0";
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PromptChar = ' ';
            this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAmount.Size = new System.Drawing.Size(250, 22);
            this.txtAmount.TabIndex = 280;
            this.txtAmount.Validated += new System.EventHandler(this.txtAmount_Validated);
            // 
            // txtUnit
            // 
            this.txtUnit.AutoSize = false;
            this.txtUnit.Location = new System.Drawing.Point(505, 46);
            this.txtUnit.MaxLength = 25;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(250, 22);
            this.txtUnit.TabIndex = 4;
            // 
            // ultraLabel12
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance2;
            this.ultraLabel12.Location = new System.Drawing.Point(390, 46);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(109, 22);
            this.ultraLabel12.TabIndex = 279;
            this.ultraLabel12.Text = "Đơn vị tính";
            // 
            // ultraLabel11
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance3;
            this.ultraLabel11.Location = new System.Drawing.Point(9, 45);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel11.TabIndex = 277;
            this.ultraLabel11.Text = "Loại";
            // 
            // cbbToolsCategory
            // 
            this.cbbToolsCategory.AutoSize = false;
            this.cbbToolsCategory.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbToolsCategory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbToolsCategory.Location = new System.Drawing.Point(116, 45);
            this.cbbToolsCategory.Name = "cbbToolsCategory";
            this.cbbToolsCategory.Size = new System.Drawing.Size(250, 22);
            this.cbbToolsCategory.TabIndex = 3;
            this.cbbToolsCategory.Validated += new System.EventHandler(this.cbbToolsCategory_Validated);
            // 
            // ultraLabel7
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance4;
            this.ultraLabel7.Location = new System.Drawing.Point(9, 187);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel7.TabIndex = 275;
            this.ultraLabel7.Text = "Giá trị PB / Kỳ";
            // 
            // txtAllocatedAmount
            // 
            appearance5.TextHAlignAsString = "Right";
            this.txtAllocatedAmount.Appearance = appearance5;
            this.txtAllocatedAmount.AutoSize = false;
            this.txtAllocatedAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAllocatedAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAllocatedAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.txtAllocatedAmount.Location = new System.Drawing.Point(116, 187);
            this.txtAllocatedAmount.MinValue = "0";
            this.txtAllocatedAmount.Name = "txtAllocatedAmount";
            this.txtAllocatedAmount.NullText = "";
            this.txtAllocatedAmount.PromptChar = ' ';
            this.txtAllocatedAmount.Size = new System.Drawing.Size(250, 22);
            this.txtAllocatedAmount.TabIndex = 13;
            this.txtAllocatedAmount.ValueChanged += new System.EventHandler(this.txtAllocatedAmount_ValueChanged);
            // 
            // ultraLabel9
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance6;
            this.ultraLabel9.Location = new System.Drawing.Point(390, 133);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(109, 22);
            this.ultraLabel9.TabIndex = 273;
            this.ultraLabel9.Text = "Số kỳ phân bổ (*)";
            // 
            // txtAllocationTimes
            // 
            appearance7.TextHAlignAsString = "Right";
            this.txtAllocationTimes.Appearance = appearance7;
            this.txtAllocationTimes.AutoSize = false;
            this.txtAllocationTimes.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAllocationTimes.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.txtAllocationTimes.Location = new System.Drawing.Point(505, 132);
            this.txtAllocationTimes.MinValue = "0";
            this.txtAllocationTimes.Name = "txtAllocationTimes";
            this.txtAllocationTimes.NullText = "";
            this.txtAllocationTimes.PromptChar = ' ';
            this.txtAllocationTimes.Size = new System.Drawing.Size(250, 22);
            this.txtAllocationTimes.TabIndex = 10;
            this.txtAllocationTimes.Validated += new System.EventHandler(this.txtAllocationTimes_Validated);
            // 
            // chkIsIrrationalCost
            // 
            this.chkIsIrrationalCost.BackColor = System.Drawing.Color.Transparent;
            this.chkIsIrrationalCost.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsIrrationalCost.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsIrrationalCost.Location = new System.Drawing.Point(505, 218);
            this.chkIsIrrationalCost.Name = "chkIsIrrationalCost";
            this.chkIsIrrationalCost.Size = new System.Drawing.Size(198, 19);
            this.chkIsIrrationalCost.TabIndex = 16;
            this.chkIsIrrationalCost.Text = "Chi phí không hợp lý";
            this.chkIsIrrationalCost.Visible = false;
            // 
            // cbbAllocationType
            // 
            this.cbbAllocationType.AutoSize = false;
            this.cbbAllocationType.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAllocationType.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "";
            valueListItem2.DataValue = "0";
            valueListItem2.DisplayText = "Phân bổ 1 lần";
            valueListItem7.DataValue = "1";
            valueListItem7.DisplayText = "Phân bổ 2 lần";
            valueListItem8.DataValue = "2";
            valueListItem8.DisplayText = "Phân bổ nhiều lần";
            this.cbbAllocationType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem7,
            valueListItem8});
            this.cbbAllocationType.Location = new System.Drawing.Point(116, 131);
            this.cbbAllocationType.Name = "cbbAllocationType";
            this.cbbAllocationType.Size = new System.Drawing.Size(250, 22);
            this.cbbAllocationType.TabIndex = 9;
            this.cbbAllocationType.ValueChanged += new System.EventHandler(this.cbbAllocationType_ValueChanged);
            // 
            // ultraLabel8
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance8;
            this.ultraLabel8.Location = new System.Drawing.Point(9, 133);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel8.TabIndex = 271;
            this.ultraLabel8.Text = "Kiểu phân bổ";
            // 
            // ultraLabel24
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance9;
            this.ultraLabel24.Location = new System.Drawing.Point(9, 215);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel24.TabIndex = 269;
            this.ultraLabel24.Text = "TK chờ phân bổ";
            // 
            // cbbAllocationAwaitAccount
            // 
            this.cbbAllocationAwaitAccount.AutoSize = false;
            this.cbbAllocationAwaitAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbAllocationAwaitAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAllocationAwaitAccount.Location = new System.Drawing.Point(116, 215);
            this.cbbAllocationAwaitAccount.Name = "cbbAllocationAwaitAccount";
            this.cbbAllocationAwaitAccount.Size = new System.Drawing.Size(250, 22);
            this.cbbAllocationAwaitAccount.TabIndex = 15;
            this.cbbAllocationAwaitAccount.Validated += new System.EventHandler(this.cbbAllocationAwaitAccount_Validated);
            // 
            // lblParentID
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblParentID.Appearance = appearance10;
            this.lblParentID.Location = new System.Drawing.Point(390, 101);
            this.lblParentID.Name = "lblParentID";
            this.lblParentID.Size = new System.Drawing.Size(90, 22);
            this.lblParentID.TabIndex = 265;
            this.lblParentID.Text = "Tổng giá trị";
            // 
            // ultraLabel5
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance11;
            this.ultraLabel5.Location = new System.Drawing.Point(390, 74);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel5.TabIndex = 263;
            this.ultraLabel5.Text = "Số lượng";
            // 
            // txtQuantity
            // 
            appearance12.TextHAlignAsString = "Right";
            this.txtQuantity.Appearance = appearance12;
            this.txtQuantity.AutoSize = false;
            this.txtQuantity.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtQuantity.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtQuantity.Location = new System.Drawing.Point(505, 73);
            this.txtQuantity.MinValue = "0";
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.PromptChar = ' ';
            this.txtQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtQuantity.Size = new System.Drawing.Size(250, 22);
            this.txtQuantity.TabIndex = 6;
            this.txtQuantity.Validated += new System.EventHandler(this.txtQuantity_Validated);
            // 
            // ultraLabel4
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance13;
            this.ultraLabel4.Location = new System.Drawing.Point(9, 104);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel4.TabIndex = 261;
            this.ultraLabel4.Text = "Đơn giá";
            // 
            // txtUnitPrice
            // 
            appearance14.TextHAlignAsString = "Right";
            this.txtUnitPrice.Appearance = appearance14;
            this.txtUnitPrice.AutoSize = false;
            this.txtUnitPrice.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtUnitPrice.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtUnitPrice.InputMask = "{LOC}nnnnnnn.nn";
            this.txtUnitPrice.Location = new System.Drawing.Point(116, 103);
            this.txtUnitPrice.MaxValue = "99999999999999999";
            this.txtUnitPrice.MinValue = "0";
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.NullText = "";
            this.txtUnitPrice.PromptChar = ' ';
            this.txtUnitPrice.Size = new System.Drawing.Size(250, 22);
            this.txtUnitPrice.TabIndex = 7;
            this.txtUnitPrice.Validated += new System.EventHandler(this.txtUnitPrice_Validated);
            // 
            // ultraLabel1
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance15;
            this.ultraLabel1.Location = new System.Drawing.Point(9, 18);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel1.TabIndex = 258;
            this.ultraLabel1.Text = "Mã CCDC (*)";
            // 
            // txtToolsCode
            // 
            this.txtToolsCode.AutoSize = false;
            this.txtToolsCode.Location = new System.Drawing.Point(116, 17);
            this.txtToolsCode.MaxLength = 25;
            this.txtToolsCode.Name = "txtToolsCode";
            this.txtToolsCode.Size = new System.Drawing.Size(250, 22);
            this.txtToolsCode.TabIndex = 1;
            // 
            // lblMaterialGoodsName
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.lblMaterialGoodsName.Appearance = appearance16;
            this.lblMaterialGoodsName.Location = new System.Drawing.Point(390, 18);
            this.lblMaterialGoodsName.Name = "lblMaterialGoodsName";
            this.lblMaterialGoodsName.Size = new System.Drawing.Size(98, 22);
            this.lblMaterialGoodsName.TabIndex = 259;
            this.lblMaterialGoodsName.Text = "Tên CCDC (*)";
            // 
            // txtToolsName
            // 
            this.txtToolsName.AutoSize = false;
            this.txtToolsName.Location = new System.Drawing.Point(505, 18);
            this.txtToolsName.MaxLength = 25;
            this.txtToolsName.Name = "txtToolsName";
            this.txtToolsName.Size = new System.Drawing.Size(250, 22);
            this.txtToolsName.TabIndex = 2;
            // 
            // ultraLabel2
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance17;
            this.ultraLabel2.Location = new System.Drawing.Point(9, 76);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel2.TabIndex = 250;
            this.ultraLabel2.Text = "Ngày ghi tăng (*)";
            // 
            // cldIncrementDate
            // 
            this.cldIncrementDate.AutoSize = false;
            this.cldIncrementDate.Location = new System.Drawing.Point(116, 73);
            this.cldIncrementDate.Name = "cldIncrementDate";
            this.cldIncrementDate.Size = new System.Drawing.Size(250, 22);
            this.cldIncrementDate.TabIndex = 5;
            // 
            // ultraLabel10
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance18;
            this.ultraLabel10.Location = new System.Drawing.Point(390, 159);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel10.TabIndex = 232;
            this.ultraLabel10.Text = "Giá trị đã phân bổ";
            // 
            // txtAllocationAmount
            // 
            appearance19.TextHAlignAsString = "Right";
            this.txtAllocationAmount.Appearance = appearance19;
            this.txtAllocationAmount.AutoSize = false;
            this.txtAllocationAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAllocationAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAllocationAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.txtAllocationAmount.Location = new System.Drawing.Point(505, 160);
            this.txtAllocationAmount.Name = "txtAllocationAmount";
            this.txtAllocationAmount.PromptChar = ' ';
            this.txtAllocationAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAllocationAmount.Size = new System.Drawing.Size(250, 22);
            this.txtAllocationAmount.TabIndex = 12;
            this.txtAllocationAmount.Validated += new System.EventHandler(this.txtAllocationAmount_Validated);
            // 
            // ultraLabel17
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance20;
            this.ultraLabel17.Location = new System.Drawing.Point(390, 187);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel17.TabIndex = 241;
            this.ultraLabel17.Text = "Giá trị còn lại";
            // 
            // txtRemainAmount
            // 
            appearance21.TextHAlignAsString = "Right";
            this.txtRemainAmount.Appearance = appearance21;
            this.txtRemainAmount.AutoSize = false;
            this.txtRemainAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtRemainAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtRemainAmount.InputMask = "{LOC}nnnnnnn.nn";
            this.txtRemainAmount.Location = new System.Drawing.Point(505, 187);
            this.txtRemainAmount.Name = "txtRemainAmount";
            this.txtRemainAmount.NullText = "";
            this.txtRemainAmount.PromptChar = ' ';
            this.txtRemainAmount.ReadOnly = true;
            this.txtRemainAmount.Size = new System.Drawing.Size(250, 22);
            this.txtRemainAmount.TabIndex = 14;
            // 
            // ultraLabel6
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance22;
            this.ultraLabel6.Location = new System.Drawing.Point(9, 159);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel6.TabIndex = 230;
            this.ultraLabel6.Text = "Số kỳ PB còn lại";
            // 
            // txtRemainAllocationTimes
            // 
            appearance23.TextHAlignAsString = "Right";
            this.txtRemainAllocationTimes.Appearance = appearance23;
            this.txtRemainAllocationTimes.AutoSize = false;
            this.txtRemainAllocationTimes.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtRemainAllocationTimes.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.txtRemainAllocationTimes.Location = new System.Drawing.Point(116, 159);
            this.txtRemainAllocationTimes.Name = "txtRemainAllocationTimes";
            this.txtRemainAllocationTimes.NullText = "0";
            this.txtRemainAllocationTimes.PromptChar = ' ';
            this.txtRemainAllocationTimes.Size = new System.Drawing.Size(250, 22);
            this.txtRemainAllocationTimes.TabIndex = 11;
            this.txtRemainAllocationTimes.Text = "0";
            this.txtRemainAllocationTimes.Validated += new System.EventHandler(this.txtRemainAllocationTimes_Validated);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraPanel4);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(798, 240);
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.uGrid);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(798, 240);
            this.ultraPanel4.TabIndex = 0;
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.contextMenuStrip1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance24;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(798, 240);
            this.uGrid.TabIndex = 1;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            this.uGrid.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid_Error);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance36;
            this.btnClose.Location = new System.Drawing.Point(715, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance37;
            this.btnSave.Location = new System.Drawing.Point(634, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chkActive
            // 
            appearance38.TextVAlignAsString = "Middle";
            this.chkActive.Appearance = appearance38;
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            this.chkActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkActive.Location = new System.Drawing.Point(14, 19);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(103, 22);
            this.chkActive.TabIndex = 219;
            this.chkActive.Text = "Ngừng phân bổ";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSaveContinue);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel1.ClientArea.Controls.Add(this.chkActive);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnClose);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 329);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(802, 47);
            this.ultraPanel1.TabIndex = 222;
            // 
            // btnSaveContinue
            // 
            appearance39.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance39;
            this.btnSaveContinue.Location = new System.Drawing.Point(498, 11);
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Size = new System.Drawing.Size(130, 30);
            this.btnSaveContinue.TabIndex = 18;
            this.btnSaveContinue.Text = "Lưu và Thêm mới";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraPanel6);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraPanel5);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(802, 329);
            this.ultraPanel2.TabIndex = 223;
            // 
            // ultraPanel6
            // 
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.ultraTabControl1);
            this.ultraPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel6.Location = new System.Drawing.Point(0, 63);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(802, 266);
            this.ultraPanel6.TabIndex = 2;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(802, 266);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chi tiết";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Thiết lập phân bổ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(798, 240);
            // 
            // ultraPanel5
            // 
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel5.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(802, 63);
            this.ultraPanel5.TabIndex = 1;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(0, 0);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Padding = new System.Drawing.Size(20, 20);
            this.ultraLabel3.Size = new System.Drawing.Size(802, 63);
            this.ultraLabel3.TabIndex = 0;
            this.ultraLabel3.Text = "Khai báo công cụ dụng cụ đầu kỳ";
            // 
            // materialGoodsBindingSource
            // 
            this.materialGoodsBindingSource.DataSource = typeof(Accounting.Core.Domain.MaterialGoods);
            // 
            // accountBindingSource
            // 
            this.accountBindingSource.DataSource = typeof(Accounting.Core.Domain.Account);
            // 
            // costSetBindingSource
            // 
            this.costSetBindingSource.DataSource = typeof(Accounting.Core.Domain.CostSet);
            // 
            // departmentBindingSource
            // 
            this.departmentBindingSource.DataSource = typeof(Accounting.Core.Domain.Department);
            // 
            // FTIInitDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 376);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FTIInitDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Khai báo CCDC đầu kỳ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FTIInitDetail_FormClosed);
            this.Shown += new System.EventHandler(this.FTIInitDetail_Shown);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToolsCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsIrrationalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAllocationAwaitAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToolsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldIncrementDate)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGoodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private System.Windows.Forms.BindingSource materialGoodsBindingSource;
        private System.Windows.Forms.BindingSource departmentBindingSource;
        private System.Windows.Forms.BindingSource accountBindingSource;
        private System.Windows.Forms.BindingSource costSetBindingSource;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkActive;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel ultraPanel6;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtUnit;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbToolsCategory;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocatedAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocationTimes;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsIrrationalCost;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbAllocationType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAllocationAwaitAccount;
        private Infragistics.Win.Misc.UltraLabel lblParentID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtQuantity;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtUnitPrice;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtToolsCode;
        private Infragistics.Win.Misc.UltraLabel lblMaterialGoodsName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtToolsName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor cldIncrementDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocationAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtRemainAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtRemainAllocationTimes;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmount;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
    }
}