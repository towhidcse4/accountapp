﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

//using System.Linq;

namespace Accounting
{
    public partial class FTIInitDetail : CustormForm
    {
        #region Decalre
        private readonly IMaterialGoodsService _IMaterialGoodsService = Utils.IMaterialGoodsService;
        private readonly ITIInitService _ITIInitService = Utils.ITIInitService;
        private readonly ISystemOptionService _ISystemOptionService = Utils.ISystemOptionService;
        private readonly ITemplateService _ITemplateService = Utils.ITemplateService;
        private readonly IToolLedgerService _IToolLedgerService = Utils.IToolLedgerService;
        private readonly ISystemOptionService _SystemOptionService = Utils.ISystemOptionService;
        TIInit _Select = new TIInit();
        private bool isEdit;
        public decimal mTaxrate = 0;
        public Guid Id { get; private set; }
        bool Them = true;
        public static bool isClose = true;
        BindingList<TIInitDetail> TIInitDetails = new BindingList<TIInitDetail>();
        private DateTime TCKHAC_NgayBDNamTC = DateTime.Now;
        bool OldIsActive = false;
        #endregion
        public FTIInitDetail()
        {
            InitializeComponent();
            _Select.TypeID = 436;
            _Select.DeclareType = 1;
            //_Select.IsActive = true;
            btnSaveContinue.Visible = true;
            //chkActive.Visible = false;
            InitializeGUI();
            //add by namnh
            cldIncrementDate.Location = new Point(cbbToolsCategory.Location.X, txtUnit.Location.Y);
            ultraLabel2.Location = new Point(ultraLabel11.Location.X, ultraLabel12.Location.Y);
            txtUnitPrice.Location = new Point(cldIncrementDate.Location.X, txtQuantity.Location.Y);
            ultraLabel4.Location = new Point(ultraLabel2.Location.X, ultraLabel5.Location.Y);
            cbbAllocationType.Location = new Point(txtUnitPrice.Location.X, txtAmount.Location.Y);
            ultraLabel8.Location = new Point(ultraLabel4.Location.X, lblParentID.Location.Y);
            txtRemainAllocationTimes.Location = new Point(cbbAllocationType.Location.X, txtAllocationTimes.Location.Y);
            ultraLabel6.Location = new Point(ultraLabel8.Location.X, ultraLabel9.Location.Y);
            txtAllocatedAmount.Location = new Point(txtRemainAllocationTimes.Location.X, txtAllocationAmount.Location.Y);
            ultraLabel7.Location = new Point(ultraLabel6.Location.X, ultraLabel10.Location.Y);
            cbbAllocationAwaitAccount.Location = new Point(txtAllocatedAmount.Location.X, txtRemainAmount.Location.Y);
            ultraLabel24.Location = new Point(ultraLabel7.Location.X, ultraLabel17.Location.Y);
            cbbToolsCategory.Visible = false;
            ultraLabel11.Visible = false;
            WaitingFrm.StopWaiting();
        }

        public FTIInitDetail(TIInit temp)
        {
            InitializeComponent();
            btnSaveContinue.Visible = false;
            _Select = temp;
            Them = false;
            OldIsActive = temp.CloneObject().IsActive;
            //chkActive.Visible = false;
            TIInitDetails = new BindingList<TIInitDetail>(temp.TIInitDetails);
            InitializeGUI();
            BindingData(_Select);
            WaitingFrm.StopWaiting();
        }

        #region Utils
        bool CheckError()
        {
            bool kq = true;
            // Kiểm tra null code và tên, và xem code đã trùng trong db chưa
            if (txtToolsCode.Text.IsNullOrEmpty() || txtToolsName.Text.IsNullOrEmpty() || cldIncrementDate.Value.IsNullOrEmpty())
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            else if (Them && _ITIInitService.ExistsToolsCode(txtToolsCode.Text.Trim()))
            {
                MSG.Error(resSystem.MSG_Error_32);
                txtToolsCode.Focus();
                return false;

            }
            if (txtAllocationTimes.Text.IsNullOrEmpty())
            {
                MSG.Error("Số kỳ phân bổ bắt buộc nhập");
                txtAllocationTimes.Focus();
                return false;
            }
            if (cldIncrementDate.Value == null)
            {
                MSG.Warning(resSystem.MSG_Catalog_FFixedAssetDetail22);
                cldIncrementDate.Focus();
                return false;
            }
            else
            {
                DateTime ghiTang = DateTime.Parse(cldIncrementDate.Value.ToString());
                // Ngày ghi tăng phải nhỏ hơn ngày sử dụng phần mềm
                if (DateTime.Compare(TCKHAC_NgayBDNamTC, ghiTang) <= 0)
                {
                    MSG.Error(resSystem.MSG_Catalog_FFixedAssetDetail24);
                    cldIncrementDate.Focus();
                    return false;
                }
            }
            if (_Select.RemainAllocationTimes > _Select.AllocationTimes)
            {
                MSG.Warning(string.Format("Số kỳ phân bổ còn lại không thể lớn hơn số kỳ phân bổ!"));
                txtRemainAllocationTimes.Text = string.Empty;
                txtRemainAllocationTimes.Focus();
                return false;
            }

            if (_Select.AllocationAmount > _Select.Amount)
            {
                MSG.Warning(string.Format("Giá trị đã phân bổ không thể lớn hơn Tổng giá trị!"));
                txtRemainAllocationTimes.Text = string.Empty;
                txtRemainAllocationTimes.Focus();
                return false;
            }
            if (TIInitDetails.Count > 0)
            {
                var sum = TIInitDetails.Sum(a => a.Quantity);
                var rate = TIInitDetails.Sum(a => a.Rate);
                if (sum != _Select.Quantity)
                {
                    MSG.Warning("Tổng số lượng nhập ở tab thiết lập phân bổ phải bằng số lượng ở thông tin chi tiết!");
                    return false;
                }
                if (100 - rate > (decimal)0.00001)
                {
                    MSG.Warning("Tổng tỷ lệ phân bổ ở tab thiết lập phân bổ phải bằng 100%!");
                    return false;
                }
                foreach (TIInitDetail detail in TIInitDetails)
                {
                    if (detail.ObjectID == null)
                    {
                        MSG.Warning("Bạn chưa chọn đối tượng phân bổ");
                        return false;
                    }
                    if (detail.ObjectType == 1 && (detail.Quantity == 0 || detail.Quantity == null))
                    {
                        MSG.Warning("Phân bổ đối tượng phòng ban số lượng phải > 0");
                        return false;
                    }
                }
            }
            else
            {
                MSG.Warning(string.Format("Bạn chưa thiết lập phân bổ!"));
                return false;
            }

            return kq;
        }

        ToolLedger CreateToolLedger(Guid id, Guid refID, Guid? Department, decimal quantity)
        {
            ToolLedger toolLedger = new ToolLedger();
            toolLedger.ID = toolLedger.ID == Guid.Empty ? Guid.NewGuid() : toolLedger.ID;
            toolLedger.ToolsID = id;
            toolLedger.No = "OPN";
            toolLedger.TypeID = 703;
            toolLedger.Date = _Select.PostedDate ?? DateTime.Now;
            toolLedger.PostedDate = _Select.PostedDate ?? DateTime.Now;
            toolLedger.IncrementAllocationTime = _Select.AllocationTimes;
            toolLedger.DecrementAllocationTime = _Select.AllocationTimes - _Select.RemainAllocationTimes;
            toolLedger.UnitPrice = _Select.UnitPrice;
            toolLedger.IncrementQuantity = quantity;
            toolLedger.IncrementAmount = _Select.Quantity != 0 ? _Select.Amount * quantity / _Select.Quantity : 0;
            toolLedger.AllocationAmount = _Select.Quantity != 0 ? _Select.AllocationAmount * quantity / _Select.Quantity : 0;
            toolLedger.AllocatedAmount = _Select.Quantity != 0 ? _Select.AllocatedAmount * quantity / _Select.Quantity : 0;
            toolLedger.ReferenceID = refID;
            toolLedger.RemainingAllocaitonTimes = _Select.RemainAllocationTimes ?? 0;
            toolLedger.RemainingAmount = _Select.RemainAmount;
            toolLedger.DepartmentID = Department;
            toolLedger.RemainingQuantity = _Select.Quantity ?? 0;
            toolLedger.IncrementDate = _Select.PostedDate ?? DateTime.Now;
            toolLedger.No = "OPN";
            return toolLedger;
        }

        private void InitializeGUI()
        {
            // Đối tượng THCP
            //cbbCostSet.DataSource = Utils.ICostSetService.GetListCostSetIsActive(true);
            //cbbCostSet.DisplayMember = "CostSetName";
            //Utils.ConfigGrid(cbbCostSet, ConstDatabase.CostSet_TableName);

            // tài khoản
            cbbAllocationAwaitAccount.DataSource = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(430, "cbbAllocationAwaitAccount");
            cbbAllocationAwaitAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAllocationAwaitAccount, ConstDatabase.Account_TableName);

            // loại CCDC
            cbbToolsCategory.DataSource = Utils.IMaterialGoodsCategoryService.GetAll();
            cbbToolsCategory.DisplayMember = "MaterialGoodsCategoryName";
            Utils.ConfigGrid(cbbToolsCategory, ConstDatabase.MaterialGoodsCategory_TableName);
            //Utils.ConfigGrid<CostSet>(this, cbbToolsCategory, ConstDatabase.MaterialGoodsCategory_TableName, "ParentID", "IsParentNode");

            uGrid.DataSource = TIInitDetails;
            Utils.ConfigGrid(uGrid, ConstDatabase.TIInitDetail_TableName, new List<TemplateColumn>(), isBusiness: true);
            //uGrid.CellChange += new CellEventHandler(Utils.uGrid_CellChange<TIInit>);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            TCKHAC_NgayBDNamTC = DateTime.ParseExact(_SystemOptionService.Getbykey(82).Data, "dd/MM/yyyy", null);
            cbbAllocationType.FormatNumberic(ConstDatabase.Format_TienVND);
            txtQuantity.FormatNumberic(ConstDatabase.Format_Quantity);//edit by cuongpv Format_TienVND -> Format_Quantity
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtUnitPrice.FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);//edit by cuongpv Format_TienVND -> Format_DonGiaQuyDoi
            txtAllocatedAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            //txtAllocationTimes.FormatNumberic(ConstDatabase.Format_TienVND);//comment by cuongpv
            txtAllocationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtRemainAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            //txtRemainAllocationTimes.FormatNumberic(ConstDatabase.Format_TienVND);//comment by cuongpv
        }
        private bool Save()
        {

            if (!Them && _ITIInitService.CheckDeleteTIInit(_Select.ID))
            {
                if (OldIsActive != chkActive.Checked)
                {

                    var model = _ITIInitService.Getbykey(_Select.ID);
                    model.IsActive = chkActive.Checked;
                    _ITIInitService.BeginTran();
                    try
                    {
                        _ITIInitService.Update(model);
                        _ITIInitService.CommitTran();
                    }
                    catch (Exception ex)
                    {
                        _ITIInitService.RolbackTran();
                    }
                    return true;
                }
                else
                {
                    MSG.Warning("Không thể sửa CCDC này vì CCDC này đã có phát sinh chứng từ liên quan!. Vui lòng thực hiện việc sửa đổi tại nghiệp vụ Điều chỉnh CCDC");
                    return false;
                }
            }
            BindingData(_Select, true);
            if (!CheckError()) return false;
            try
            {
                // TIInit

                //MaterialGoods materialGoods = Them ? new MaterialGoods() : _IMaterialGoodsService.Getbykey(_Select.MaterialGoodsID ?? Guid.Empty);
                //materialGoods = CreateMaterialGoods(materialGoods);

                //_Select.MaterialGoodsID = materialGoods.ID;
                _ITIInitService.BeginTran();
                if (Them)
                {

                    foreach (TIInitDetail item in TIInitDetails)
                    {
                        item.TIInitID = _Select.ID;
                    }
                    _Select.TIInitDetails = TIInitDetails;
                    _ITIInitService.CreateNew(_Select);
                }
                else
                {
                    foreach (TIInitDetail item in TIInitDetails)
                    {
                        item.TIInitID = _Select.ID;
                    }
                    _ITIInitService.Update(_Select);
                }
                // Material Goods
                //if (Them)
                //{
                //    _IMaterialGoodsService.CreateNew(materialGoods);
                //}
                //else
                //{
                //    _IMaterialGoodsService.Update(materialGoods);
                //}

                // Tool ledger
                List<ToolLedger> toolLedgers = Them ? new List<ToolLedger>() : _IToolLedgerService.FindByMaterialGoodsID(_Select.ID);
                //toolLedger = CreateToolLedger(toolLedger, materialGoods.ID, _Select.ID);
                List<ToolLedger> lst = new List<ToolLedger>();
                foreach (TIInitDetail detail in TIInitDetails)
                {
                    if (detail.ObjectType == 1)
                    {
                        ToolLedger toolLedger = CreateToolLedger(_Select.ID, _Select.ID, detail.ObjectID, detail.Quantity ?? 0);
                        lst.Add(toolLedger);
                    }

                }
                if (Them)
                {
                    foreach (ToolLedger detail in lst)
                        _IToolLedgerService.CreateNew(detail);
                }
                else
                {
                    foreach (ToolLedger detail in toolLedgers)
                        _IToolLedgerService.Delete(detail);
                    foreach (ToolLedger detail in lst)
                        _IToolLedgerService.CreateNew(detail);
                }

                _ITIInitService.CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                _ITIInitService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu công cụ dụng cụ." + ex.Message);
                return false;
            }
        }

        private MaterialGoods CreateMaterialGoods(MaterialGoods materialGoods)
        {
            materialGoods.ID = materialGoods.ID == Guid.Empty ? Guid.NewGuid() : materialGoods.ID;

            materialGoods.MaterialGoodsCode = _Select.ToolsCode;
            materialGoods.MaterialGoodsName = _Select.ToolsName;
            materialGoods.MaterialGoodsCategoryID = _Select.MaterialGoodsCategoryID;
            materialGoods.Quantity = _Select.Quantity ?? 0;
            materialGoods.AllocationType = _Select.AllocationType ?? 0;
            materialGoods.UnitPrice = _Select.UnitPrice;
            materialGoods.AllocationTimes = _Select.AllocationTimes ?? 0;
            materialGoods.Amount = _Select.Amount;
            materialGoods.AllocatedAmount = _Select.AllocatedAmount ?? 0;
            materialGoods.CostSetID = _Select.CostSetID;
            materialGoods.AllocationAwaitAccount = _Select.AllocationAwaitAccount;
            materialGoods.MaterialGoodsType = 1;
            materialGoods.IsActive = false;

            return materialGoods;
        }
        #endregion

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            TIInitDetails = new BindingList<TIInitDetail>();
            isClose = false;
            this.Close();
        }

        private void Control_ValueChanged(object sender, EventArgs e)
        {
            isEdit = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                isClose = false;
                Id = _Select.ID;
                this.Close();
            }
        }
        #endregion

        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                _Select = new TIInit();
                _Select.TypeID = 436;
                txtToolsCode.Text = null;
                txtToolsName.Text = null;
                cbbToolsCategory.SelectedRow = null;


                txtUnit.Text = null;
                txtQuantity.Text = null;
                cbbAllocationType.SelectedIndex = 0;
                txtUnitPrice.Text = null;
                txtAllocationTimes.Text = null;

                txtAmount.Text = null;
                txtAllocatedAmount.Text = null;
                //cbbCostSet.SelectedRow = null;
                cbbAllocationAwaitAccount.SelectedRow = null;
                // giá trị đã phân bổ
                txtAllocationAmount.Text = null;

                // số lần phân bổ còn lại
                txtRemainAllocationTimes.Text = null;

                // giá trị phân bổ còn lại
                txtRemainAmount.Text = null;

                chkIsIrrationalCost.Checked = false;
                TIInitDetails = new BindingList<TIInitDetail>();
                uGrid.DataSource = TIInitDetails;
                MSG.Information("Thêm mới CCDC thành công");
                isClose = false;
            }
        }

        private void FTIInitDetail_Shown(object sender, EventArgs e)
        {
            isEdit = false;

        }

        private void BindingData(TIInit TIInit, bool isCreate = false)
        {
            if (isCreate)
            {
                if (TIInit.ID == null || TIInit.ID == Guid.Empty) TIInit.ID = Guid.NewGuid();
                TIInit.ToolsCode = txtToolsCode.Text;
                TIInit.ToolsName = txtToolsName.Text;
                if (!string.IsNullOrEmpty(cbbToolsCategory.Text))
                {
                    TIInit.MaterialGoodsCategoryID = ((MaterialGoodsCategory)Utils.getSelectCbbItem(cbbToolsCategory)).ID;
                }
                TIInit.Unit = txtUnit.Text;
                if (!txtQuantity.Text.IsNullOrEmpty())
                    TIInit.Quantity = Decimal.Parse(txtQuantity.Text);
                TIInit.AllocationType = cbbAllocationType.SelectedIndex;
                if (!txtUnitPrice.Text.IsNullOrEmpty())
                    TIInit.UnitPrice = Decimal.Parse(txtUnitPrice.Text);
                if (!txtAllocationTimes.Text.IsNullOrEmpty())
                    TIInit.AllocationTimes = txtAllocationTimes.Text.ToInt();
                if (!txtAmount.Text.IsNullOrEmpty())
                    TIInit.Amount = Decimal.Parse(txtAmount.Text);
                if (!txtAllocatedAmount.Text.IsNullOrEmpty())
                    TIInit.AllocatedAmount = Decimal.Parse(txtAllocatedAmount.Text);
                //if (!string.IsNullOrEmpty(cbbCostSet.Text))
                //{
                //    TIInit.CostSetID = ((CostSet)Utils.getSelectCbbItem(cbbCostSet)).ID;
                //}
                if (!string.IsNullOrEmpty(cbbAllocationAwaitAccount.Text))
                {
                    TIInit.AllocationAwaitAccount = ((Account)Utils.getSelectCbbItem(cbbAllocationAwaitAccount)).AccountNumber;
                }

                // Ngày ghi tăng
                if (CheckDateTime(cldIncrementDate, false))
                {
                    TIInit.PostedDate = DateTime.Parse(cldIncrementDate.Value.ToString());
                }

                // giá trị đã phân bổ
                if (!string.IsNullOrEmpty(txtAllocationAmount.Text))
                {
                    TIInit.AllocationAmount = Decimal.Parse(txtAllocationAmount.Text);
                }

                // số lần phân bổ còn lại
                if (!string.IsNullOrEmpty(txtRemainAllocationTimes.Text))
                {
                    TIInit.RemainAllocationTimes = txtRemainAllocationTimes.Text.ToInt();
                }

                // giá trị phân bổ còn lại
                if (!string.IsNullOrEmpty(txtRemainAmount.Text))
                {
                    TIInit.RemainAmount = Decimal.Parse(txtRemainAmount.Text);
                }
                TIInit.IsIrrationalCost = chkIsIrrationalCost.Checked;

                TIInit.IsActive = chkActive.Checked;
                TIInit.DeclareType = 1;
            }
            else
            {
                txtToolsCode.Text = TIInit.ToolsCode;
                txtToolsName.Text = TIInit.ToolsName;
                if (TIInit.MaterialGoodsCategoryID != null)
                {
                    foreach (var item in cbbToolsCategory.Rows)
                    {

                        if (((item.ListObject as MaterialGoodsCategory).ID == TIInit.MaterialGoodsCategoryID))
                        {
                            cbbToolsCategory.SelectedRow = item;
                        }
                    }
                }

                txtUnit.Text = TIInit.Unit;
                txtQuantity.Text = TIInit.Quantity.ToString();
                if (TIInit.AllocationType != null)
                    cbbAllocationType.SelectedIndex = TIInit.AllocationType ?? 0;
                txtUnitPrice.Text = TIInit.UnitPrice.ToString();
                txtAllocationTimes.Text = TIInit.AllocationTimes.ToString(); ;

                txtAmount.Text = TIInit.Amount.ToString();
                txtAllocatedAmount.Text = TIInit.AllocatedAmount.ToString();

                //if (TIInit.CostSetID != null)
                //{
                //    foreach (var item in cbbCostSet.Rows)
                //    {

                //        if (((item.ListObject as CostSet).ID == TIInit.CostSetID))
                //        {
                //            cbbCostSet.SelectedRow = item;
                //        }
                //    }
                //}
                if (TIInit.AllocationAwaitAccount != null)
                {
                    foreach (var item in cbbAllocationAwaitAccount.Rows)
                    {

                        if (((item.ListObject as Account).AccountNumber == TIInit.AllocationAwaitAccount))
                        {
                            cbbAllocationAwaitAccount.SelectedRow = item;
                        }
                    }
                }

                // Ngày ghi tăng
                if (CheckDateTime(cldIncrementDate, false))
                {
                    cldIncrementDate.Value = TIInit.PostedDate;
                }

                // giá trị đã phân bổ
                if (TIInit.AllocationAmount != null)
                    txtAllocationAmount.Text = TIInit.AllocationAmount.ToString();

                // số lần phân bổ còn lại
                if (TIInit.RemainAllocationTimes != null)
                    txtRemainAllocationTimes.Text = TIInit.RemainAllocationTimes.ToString();

                // giá trị phân bổ còn lại
                txtRemainAmount.Text = TIInit.RemainAmount.ToString();

                chkIsIrrationalCost.Checked = TIInit.IsIrrationalCost ?? false;
                chkActive.Checked = TIInit.IsActive;
            }
        }

        bool CheckDateTime(UltraDateTimeEditor cld, bool dk)
        {
            DateTime dt;
            if (cld.Value != null)
            {
                try
                {
                    dt = DateTime.Parse(cld.Value.ToString());
                    return true;
                }
                catch (Exception)
                {
                    if (dk)
                        MSG.Error(resSystem.MSG_Catalog_FCostSetDetail6);
                    return false;
                }
            }
            return dk;
        }

        private void txtQuantity_Validated(object sender, EventArgs e)
        {
            CalculateAmountAndAllocateAmount();
            if (!txtQuantity.Value.IsNullOrEmpty())
            {
                decimal totalquantity = Convert.ToDecimal(txtQuantity.Value.ToString());
                foreach (UltraGridRow row in uGrid.Rows)
                {
                    if (row.Cells["Quantity"].Value != null && row.Cells["Quantity"].Value.ToString() != "")
                    {
                        var quantity = Convert.ToDecimal(row.Cells["Quantity"].Value.ToString());
                        row.Cells["Rate"].Value = quantity / totalquantity * 100;
                    }

                }
            }
            if (txtUnitPrice.Text.IsNullOrEmpty())
            {
                CalculateUnitPrice();
            }
        }

        private void CalculateAmountAndAllocateAmount()
        {
            decimal Declare1 = 0, Declare2 = 0;
            if (!txtQuantity.Value.IsNullOrEmpty() && !txtUnitPrice.Value.IsNullOrEmpty()
                                            && Decimal.TryParse(txtQuantity.Value.ToString(), out Declare1) && Decimal.TryParse(txtUnitPrice.Value.ToString(), out Declare2))
            {
                txtAmount.Value = Declare1 * Declare2;
                if (!txtAllocationTimes.Text.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty() && txtAllocationTimes.Value.ToInt() != 0)
                {
                    txtAllocatedAmount.Value = Declare1 * Declare2 / txtAllocationTimes.Value.ToInt();
                }
                CalculateRemainingAmount();
            }
        }
        private void CalculateUnitPrice()
        {
            decimal Declare1 = 0, Declare2 = 0;
            if (!txtQuantity.Value.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty()
                                            && Decimal.TryParse(txtQuantity.Value.ToString(), out Declare1) && Decimal.TryParse(txtAmount.Value.ToString(), out Declare2))
            {
                if (Declare1 != 0)
                    txtUnitPrice.Value = Declare2 / Declare1;
            }
        }
        private void txtUnitPrice_Validated(object sender, EventArgs e)
        {
            CalculateAmountAndAllocateAmount();
        }

        private void txtAllocationTimes_Validated(object sender, EventArgs e)
        {
            CalculateAllocatedAmount();
        }

        private void cbbToolsCategory_Validated(object sender, EventArgs e)
        {
            bool isContain = false;
            if (cbbToolsCategory.Value == null) return;
            foreach (var item in cbbToolsCategory.Rows)
            {
                if ((item.ListObject as MaterialGoodsCategory).MaterialGoodsCategoryCode.ToLower() == cbbToolsCategory.Value.ToString().ToLower()
                    || (item.ListObject as MaterialGoodsCategory).MaterialGoodsCategoryName.ToLower() == cbbToolsCategory.Value.ToString().ToLower())
                {
                    cbbToolsCategory.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbToolsCategory.Value = "";
                cbbToolsCategory.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        private void cbbAllocationAwaitAccount_Validated(object sender, EventArgs e)
        {
            bool isContain = false;
            if (cbbAllocationAwaitAccount.Value == null) return;
            foreach (var item in cbbAllocationAwaitAccount.Rows)
            {
                if ((item.ListObject as Account).AccountNumber == cbbAllocationAwaitAccount.Value.ToString())
                {
                    cbbAllocationAwaitAccount.SelectedRow = item;
                    isContain = true;
                    break;
                }
            }
            if (!isContain)
            {
                cbbAllocationAwaitAccount.Value = "";
                cbbAllocationAwaitAccount.Focus();
                MSG.Warning("Giá trị không tồn tại");
            }
        }

        //private void cbbCostSet_Validated(object sender, EventArgs e)
        //{
        //    bool isContain = false;
        //    if (cbbCostSet.Value == null) return;
        //    foreach (var item in cbbCostSet.Rows)
        //    {
        //        if ((item.ListObject as CostSet).CostSetCode.ToLower() == cbbCostSet.Value.ToString().ToLower()
        //            || (item.ListObject as CostSet).CostSetName.ToLower() == cbbCostSet.Value.ToString().ToLower())
        //        {
        //            cbbCostSet.SelectedRow = item;
        //            isContain = true;
        //            break;
        //        }
        //    }
        //    if (!isContain)
        //    {
        //        cbbCostSet.Value = "";
        //        cbbCostSet.Focus();
        //        MSG.Warning("Giá trị không tồn tại");
        //    }
        //}

        private void txtAllocationAmount_Validated(object sender, EventArgs e)
        {
            CalculateRemainingAmount();
        }

        private void txtRemainAllocationTimes_Validated(object sender, EventArgs e)
        {
            CalculateAllocationAmount();
            CalculateRemainingAmount();
        }

        private void txtAllocatedAmount_ValueChanged(object sender, EventArgs e)
        {
            CalculateAllocationAmount();
            CalculateRemainingAmount();
        }

        public void CalculateAllocationAmount()
        {
            if (!txtAllocatedAmount.Text.IsNullOrEmpty() && !txtRemainAllocationTimes.Text.IsNullOrEmpty() && !txtAllocationTimes.Text.IsNullOrEmpty())
            {
                decimal ala = Decimal.Parse(txtAllocatedAmount.Text);
                decimal remaintimes = Decimal.Parse(txtRemainAllocationTimes.Text);
                decimal times = Decimal.Parse(txtAllocationTimes.Text);
                if (times >= remaintimes)
                    txtAllocationAmount.Value = ala * (times - remaintimes);
            }
        }

        public void CalculateRemainingAmount()
        {
            if (!txtAllocationAmount.Text.IsNullOrEmpty() && !txtAmount.Text.IsNullOrEmpty())
            {
                decimal ala = Decimal.Parse(txtAllocationAmount.Text);
                decimal amount = Decimal.Parse(txtAmount.Text);
                txtRemainAmount.Value = ala > amount ? 0 : amount - ala;
            }
        }

        private void cbbAllocationType_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAllocationType.SelectedIndex == 1)
            {
                txtAllocationTimes.Value = 1;
                txtAllocationTimes.Enabled = false;
            }
            else if (cbbAllocationType.SelectedIndex == 2)
            {
                txtAllocationTimes.Value = 2;
                txtAllocationTimes.Enabled = false;
            }
            else
            {
                txtAllocationTimes.Value = null;
                txtAllocationTimes.Enabled = true;
            }
            CalculateAllocatedAmount();
        }

        private void CalculateAllocatedAmount()
        {
            if (!txtAllocationTimes.Text.IsNullOrEmpty() && !txtAmount.Value.IsNullOrEmpty() && txtAllocationTimes.Value.ToInt() != 0)
            {
                decimal amount = Decimal.Parse(txtAmount.Value.ToString());
                txtAllocatedAmount.Value = amount / txtAllocationTimes.Value.ToInt();
            }
            CalculateAllocationAmount();
            CalculateRemainingAmount();
        }

        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "ExpenseItemID" || activeCell.Column.Key == "ObjectID" || activeCell.Column.Key == "CostAccount")
            {
                UltraCombo combo = activeCell.ValueListResolved as UltraCombo;
                if (false == combo.IsItemInList() && combo != null)
                {
                    MSG.Warning("Dữ liệu không có trong danh mục");
                    activeCell.Value = null;
                }
            }
            if (e != null)
            {
                e.Cancel = true;
            }
        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {

            UltraGridCell cell = ((UltraGrid)sender).ActiveCell;
            if (cell == null) return;
            if (cell.Column.Key.Contains("CostAccount") && cell.Value != null && cell.Value.ToString() != "")
            {
                List<Account> listAccounts = new List<Account>();
                listAccounts = ((List<Account>)((UltraCombo)cell.Column.ValueList).DataSource).Where(
                             p => p.AccountNumber == cell.Text.Trim() && p.GetProperty<Account, bool>("IsActive")).ToList();
                if (listAccounts.Count == 0)
                {
                    MSG.Warning(resSystem.MSG_System_30);
                    cell.Value = null;
                }
            }
            else if (cell.Column.Key == "ExpenseItemID" && e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.SelectedRow != null)
                {
                    var ObjectIDs = (ExpenseItem)combo.SelectedRow.ListObject;

                    if (ObjectIDs.IsParentNode)
                    {
                        cell.Value = null;
                        MSG.Warning(resSystem.MSG_System_29);
                    }
                }
            }
            else if (cell.Column.Key.Contains("Quantity") && cell.Value != null && cell.Value.ToString() != "" && txtQuantity.Value != null && txtQuantity.Value.ToString() != "")
            {
                var quantity = Convert.ToDecimal(cell.Value.ToString());
                var totalQuantity = Convert.ToDecimal(txtQuantity.Value.ToString());
                UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                if (band.Columns.Exists("Rate"))
                    cell.Row.Cells["Rate"].Value = totalQuantity != 0 ? quantity / totalQuantity * 100 : 0;
            }
            if (cell.Column.Key.Contains("ObjectID") && e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.SelectedRow != null)
                {
                    var ObjectIDs = (Objects)combo.SelectedRow.ListObject;
                    if (ObjectIDs.IsParentNode)
                    {
                        MSG.Warning(resSystem.MSG_System_29);
                        cell.Value = null;
                        return;
                    }
                    int i = TIInitDetails.Count(x => x.ObjectID == (Guid)cell.Row.Cells["ObjectID"].Value);
                    if (i > 1)
                    {
                        MSG.Warning("Đối tượng này đã được thiết lập phân bổ");
                        cell.Value = null;
                        return;
                    }
                }
            }
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            var cell = ((UltraGrid)sender).ActiveCell;
            if (cell == null) return;
            if (cell.Column.Key.Contains("ObjectID") && e.Cell.ValueListResolved != null)
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                if (combo.SelectedRow != null)
                {
                    var ObjectIDs = (Objects)combo.SelectedRow.ListObject;
                    UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                    if (band.Columns.Exists("ObjectType"))
                        cell.Row.Cells["ObjectType"].Value = ObjectIDs.ObjectType;
                    if (ObjectIDs.ObjectType == 1)
                    {
                        cell.Row.Cells["CostAccount"].Value = ObjectIDs.CostAccount;
                        cell.Row.Cells["Rate"].Activation = Activation.AllowEdit;
                        cell.Row.Cells["Quantity"].Activation = Activation.AllowEdit;
                    }
                    else
                    {
                        cell.Row.Cells["Rate"].Activation = Activation.AllowEdit;
                        cell.Row.Cells["Quantity"].Activation = Activation.NoEdit;
                        cell.Row.Cells["Quantity"].Value = null;
                    }
                }
            }
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (uGrid.ActiveCell != null)
            {
                this.RemoveRow4Grid<FEMContractSaleDetail>(uGrid);
                uGrid.Update();
            }

        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGrid.AddNewRow4Grid();
        }

        private void txtAmount_Validated(object sender, EventArgs e)
        {
            CalculateUnitPrice();
        }

        private void FTIInitDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}

