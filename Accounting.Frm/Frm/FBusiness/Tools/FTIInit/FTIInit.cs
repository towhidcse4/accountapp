﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using Accounting.Core.IService;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FTIInit : CatalogBase
    {
        private readonly IMaterialGoodsService _IMaterialGoodsService = Utils.IMaterialGoodsService;
        private readonly ITIInitService _ITIInitService = Utils.ITIInitService;
        private readonly IToolLedgerService _IToolLedgerService = Utils.IToolLedgerService;
        public FTIInit()
        {
            InitializeComponent();
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Sửa (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            LoadDuLieu();
            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
        }

        protected override void AddFunction()
        {
            //new FTIInitDetail().ShowDialog(this);
            //if (!FTIInitDetail.isClose)
            //{
            //    Utils.ClearCacheByType<TIInit>();
            //    LoadDuLieu();
            //}
            //HUYPD Edit Show Form
            FTIInitDetail frm = new FTIInitDetail();
            frm.FormClosed += new FormClosedEventHandler(FTIInitDetail_FormClosed);
            frm.ShowDialogHD(this);
        }
        protected override void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                TIInit temp = uGrid.Selected.Rows[0].ListObject as TIInit;
                //new FTIInitDetail(temp).ShowDialog(this);
                //if (!FTIInitDetail.isClose)
                //{
                //    Utils.ClearCacheByType<TIInit>();
                //    LoadDuLieu();
                //}
                //HUYPD Edit Show Form
                FTIInitDetail frm = new FTIInitDetail(temp);
                frm.FormClosed += new FormClosedEventHandler(FTIInitDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
            else Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }
        private void FTIInitDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ClearCacheByType<TIInit>();
            LoadDuLieu();
        }
        protected override void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                TIInit temp = uGrid.Selected.Rows[0].ListObject as TIInit;
                if (_ITIInitService.CheckDeleteTIInit(temp.ID))
                {
                    MSG.Warning("Không thể xóa CCDC này vì CCDC này đã có phát sinh chứng từ liên quan!");
                    return;
                }
                if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.ToolsCode)) == System.Windows.Forms.DialogResult.Yes)
                {

                    try
                    {
                        _ITIInitService.BeginTran();
                        // xóa ccdc
                        _ITIInitService.Delete(temp);
                        // xóa tool ledger
                        List<ToolLedger> tools = _IToolLedgerService.FindByMaterialGoodsID(temp.ID);
                        foreach (ToolLedger tool in tools)
                            _IToolLedgerService.Delete(tool);
                        _ITIInitService.CommitTran();
                        Utils.ClearCacheByType<TIInit>();
                        LoadDuLieu();
                    }
                    catch (Exception ex)
                    {
                        _IMaterialGoodsService.RolbackTran();
                        throw ex;
                    }
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid)
        {
            WaitingFrm.StartWaiting();
            List<TIInit> lstMaterialGoods = _ITIInitService.FindByDeclareType(1);
            _ITIInitService.UnbindSession(lstMaterialGoods);
            lstMaterialGoods = _ITIInitService.FindByDeclareType(1);
            uGrid.DataSource = lstMaterialGoods.ToArray();
            if (configGrid) ConfigGrid(uGrid);
            WaitingFrm.StopWaiting();
        }

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {
            utralGrid.ConfigGrid(ConstDatabase.TIInit_TableName);
            this.ConfigEachColumn4Grid(0, uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCategoryID"], utralGrid);
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["Quantity"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AllocationTimes"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["UnitPrice"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["Amount"].Width = 150;
            uGrid.DisplayLayout.Bands[0].Columns["ToolsName"].Width = 300;
        }
        #endregion


        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void uGrid_DoubleClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();

        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void FMaterialTools_Load(object sender, EventArgs e)
        {

        }

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var temp = e.Row.ListObject as TIInit;
            if (temp == null) return;
            txtMaterialcode.Text = ":   " + temp.ToolsCode;
            txtMaterialName.Text = ":   " + temp.ToolsName;
            txtUnit.Text = ":   " + temp.UnitPrice.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            txtQuantity.Text = ":   " + temp.Quantity.ToStringNumbericFormat(ConstDatabase.Format_Quantity);
            txtAmount.Text = ":   " + temp.Amount.ToStringNumbericFormat(ConstDatabase.Format_TienVND);
            var voucher = _IMaterialGoodsService.GetListVouchers(temp.ID);
            foreach (var item in voucher)
            {
                var firstOrDefault = Utils.ListType.FirstOrDefault(x => x.ID == item.TypeID);
                if (firstOrDefault != null)
                    item.TypeName = firstOrDefault.TypeName;
            }
            uGridDetail.DataSource = voucher;
            uGridDetail.ConfigGrid(ConstDatabase.VoucherFixedAsset_KeyName);
            foreach (var column in uGridDetail.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridDetail);
            }
        }

        private void uGrid_DoubleClick_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGrid, "Khai báo công cụ dụng cụ đầu kì");
        }

        private void FTIInit_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();

            uGridDetail.ResetText();
            uGridDetail.ResetUpdateMode();
            uGridDetail.ResetExitEditModeOnLeave();
            uGridDetail.ResetRowUpdateCancelAction();
            uGridDetail.DataSource = null;
            uGridDetail.Layouts.Clear();
            uGridDetail.ResetLayouts();
            uGridDetail.ResetDisplayLayout();
            uGridDetail.Refresh();
            uGridDetail.ClearUndoHistory();
            uGridDetail.ClearXsdConstraints();
        }

        private void FTIInit_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
