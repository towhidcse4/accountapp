﻿namespace Accounting
{
    partial class FTIInit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtQuantity = new Infragistics.Win.Misc.UltraLabel();
            this.txtUnit = new Infragistics.Win.Misc.UltraLabel();
            this.txtMaterialName = new Infragistics.Win.Misc.UltraLabel();
            this.txtMaterialcode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.lbl1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtAmount);
            this.ultraTabPageControl1.Controls.Add(this.txtQuantity);
            this.ultraTabPageControl1.Controls.Add(this.txtUnit);
            this.ultraTabPageControl1.Controls.Add(this.txtMaterialName);
            this.ultraTabPageControl1.Controls.Add(this.txtMaterialcode);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl1.Controls.Add(this.lbl1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1002, 192);
            // 
            // txtAmount
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.txtAmount.Appearance = appearance18;
            this.txtAmount.Location = new System.Drawing.Point(171, 128);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(162, 23);
            this.txtAmount.TabIndex = 5;
            // 
            // txtQuantity
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.txtQuantity.Appearance = appearance19;
            this.txtQuantity.Location = new System.Drawing.Point(171, 99);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(162, 23);
            this.txtQuantity.TabIndex = 5;
            // 
            // txtUnit
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.txtUnit.Appearance = appearance20;
            this.txtUnit.Location = new System.Drawing.Point(171, 70);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(162, 23);
            this.txtUnit.TabIndex = 5;
            // 
            // txtMaterialName
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.txtMaterialName.Appearance = appearance21;
            this.txtMaterialName.Location = new System.Drawing.Point(171, 41);
            this.txtMaterialName.Name = "txtMaterialName";
            this.txtMaterialName.Size = new System.Drawing.Size(211, 23);
            this.txtMaterialName.TabIndex = 5;
            // 
            // txtMaterialcode
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.txtMaterialcode.Appearance = appearance22;
            this.txtMaterialcode.Location = new System.Drawing.Point(171, 12);
            this.txtMaterialcode.Name = "txtMaterialcode";
            this.txtMaterialcode.Size = new System.Drawing.Size(162, 23);
            this.txtMaterialcode.TabIndex = 5;
            // 
            // ultraLabel5
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance23;
            this.ultraLabel5.Location = new System.Drawing.Point(10, 128);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Tổng giá trị";
            // 
            // ultraLabel4
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance24;
            this.ultraLabel4.Location = new System.Drawing.Point(10, 99);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Số lượng";
            // 
            // ultraLabel3
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance25;
            this.ultraLabel3.Location = new System.Drawing.Point(10, 70);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Đơn giá";
            // 
            // ultraLabel2
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance26;
            this.ultraLabel2.Location = new System.Drawing.Point(10, 41);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(120, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tên công cụ, dụng cụ";
            // 
            // lbl1
            // 
            appearance27.TextVAlignAsString = "Middle";
            this.lbl1.Appearance = appearance27;
            this.lbl1.Location = new System.Drawing.Point(10, 12);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(111, 22);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Mã công cụ, dụng cụ";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridDetail);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1002, 192);
            // 
            // uGridDetail
            // 
            this.uGridDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDetail.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridDetail.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridDetail.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridDetail.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDetail.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDetail.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridDetail.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridDetail.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridDetail.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDetail.Location = new System.Drawing.Point(0, 0);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(1002, 192);
            this.uGridDetail.TabIndex = 8;
            this.uGridDetail.Text = "ultraGrid1";
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 363);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 158;
            this.ultraSplitter1.Size = new System.Drawing.Size(1004, 10);
            this.ultraSplitter1.TabIndex = 40;
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(152, 26);
            this.tsmDelete.Text = "Xóa";
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(152, 26);
            this.tsmEdit.Text = "Sửa";
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(152, 26);
            this.tsmAdd.Text = "Thêm";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(153, 114);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(152, 26);
            this.tmsReLoad.Text = "Tải Lại";
            // 
            // btnAdd
            // 
            appearance28.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance28;
            this.btnAdd.Location = new System.Drawing.Point(10, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            appearance29.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance29;
            this.btnDelete.Location = new System.Drawing.Point(172, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance30.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance30;
            this.btnEdit.Location = new System.Drawing.Point(91, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 373);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1004, 215);
            this.ultraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControl1.TabIndex = 39;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "&2. Chứng từ";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1002, 192);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 52);
            this.panel1.TabIndex = 38;
            // 
            // btnExportExcel
            // 
            appearance31.BackColor = System.Drawing.Color.DarkRed;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance31.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance31.Image = global::Accounting.Properties.Resources.Excel;
            this.btnExportExcel.Appearance = appearance31;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance32.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance32;
            this.btnExportExcel.Location = new System.Drawing.Point(253, 12);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance33;
            this.btnExportExcel.Size = new System.Drawing.Size(83, 30);
            this.btnExportExcel.TabIndex = 70;
            this.btnExportExcel.Text = "Kết xuất";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // uGrid
            // 
            this.uGrid.ContextMenuStrip = this.cms4Grid;
            appearance17.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Appearance = appearance17;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 52);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1004, 311);
            this.uGrid.TabIndex = 41;
            this.uGrid.Text = "uGrid";
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.DoubleClick += new System.EventHandler(this.uGrid_DoubleClick_1);
            // 
            // FTIInit
            // 
            this.ClientSize = new System.Drawing.Size(1004, 588);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FTIInit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FTIInit_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FTIInit_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraLabel txtAmount;
        private Infragistics.Win.Misc.UltraLabel txtQuantity;
        private Infragistics.Win.Misc.UltraLabel txtUnit;
        private Infragistics.Win.Misc.UltraLabel txtMaterialName;
        private Infragistics.Win.Misc.UltraLabel txtMaterialcode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.Misc.UltraLabel lbl1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
    }
}