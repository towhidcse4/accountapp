﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Core.Resource;
using FX.Core;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using AutoCompleteMode = Infragistics.Win.AutoCompleteMode;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FTIIncrementBuyDetail : FSTIIncrementBuyDetailBase
    {
        #region Khai báo
        UltraGrid ugrid0 = null;
        UltraGrid ugrid1 = null;
        UltraGrid ugrid3 = null;
        UltraGrid ugrid2 = null;
        bool tuOptPaymentChuyenSang = false;
        bool tuCbbPaymentChuyenSang = false;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region Khởi tạo
        public FTIIncrementBuyDetail(TIIncrement TIIncrement, IEnumerable<TIIncrement> TIIncrements, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();

            #endregion


            #region Thiết lập ban đầu cho Form
            Utils.ClearCacheByType<PaymentClause>();
            _statusForm = statusForm;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new TIIncrement { TypeID = 430 }) : TIIncrement;
            _typeID = _select.TypeID;
            _listSelects.AddRange(TIIncrements);

            if (_typeID == 902)
            {
                _selectJoin = new MCPayment();
                _selectJoin = IMCPaymentService.Getbykey(_select.ID);
            }

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 10;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));
            }
            #endregion
        }
        #endregion

        #region Overide
        public override void InitializeGUI(TIIncrement input)
        {
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            input.TemplateID = mauGiaoDien.ID;
            List<int> lstSingleCellInfo = new List<int>() { 902 };
            Utils.isPPInvoice = true;
            #region Top
            List<int> lstId = new List<int> { 902, 903, 904, 905, 906 };
            List<string> lstType = new List<string> { "Tiền mặt", "Ủy nhiệm chi", "Séc chuyển khoản", "Séc tiền mặt", "Thẻ tín dụng" };
            cbbPayment.Items.Clear();
            for (int i = 0; i < lstId.Count; i++)
            {
                cbbPayment.Items.Add(lstId[i], lstType[i]);
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                optPayment.Enabled = true;
                optPayment.Value = false;
            }
            else
            {
                optPayment.Enabled = false;
                cbbPayment.Enabled = false;
                if (_select.TypeID == 430)
                {
                    optPayment.Value = false;
                }
                else
                {
                    int typeId = _select.TypeID;
                    optPayment.Value = true;
                    _select.TypeID = typeId;
                    cbbPayment.SelectedItem = cbbPayment.Items.ValueList.FindByDataValue(_select.TypeID);
                }
            }
            #endregion

            #region Body
            this.ConfigTopVouchersNo<TIIncrement>(palTopVouchers, "No", "PostedDate", "Date");
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            BindingList<TIIncrementDetail> bdlTIIncrementDetail = new BindingList<TIIncrementDetail>();
            BindingList<PPInvoiceDetailCost> cost = new BindingList<PPInvoiceDetailCost>();
            bdlTIIncrementDetail = new BindingList<TIIncrementDetail>(_statusForm == ConstFrm.optStatusForm.Add
                                                                       ? new List<TIIncrementDetail>()
                                                                       : input.TIIncrementDetails);
            cost = new BindingList<PPInvoiceDetailCost>(_statusForm == ConstFrm.optStatusForm.Add
                                                                       ? new List<PPInvoiceDetailCost>()
                                                                       : (input.PPInvoiceDetailCosts ?? new List<PPInvoiceDetailCost>()));
            _listObjectInput = new BindingList<System.Collections.IList> { bdlTIIncrementDetail };
            _listObjectInputPost = new BindingList<System.Collections.IList> { cost };
            _listObjectInputGroup = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<TIIncrement>(palGrid, mauGiaoDien);
            //this.ConfigGridByTemplete<TIIncrement>(_select.TypeID, mauGiaoDien, true, _listObjectInput, true, _select.ID.ToString());           

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost, _listObjectInputGroup };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false, false };
            this.ConfigGridByManyTemplete<TIIncrement>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            #endregion
            #region Footer
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            _select.IsImportPurchase = _statusForm == ConstFrm.optStatusForm.Add ? false : input.IsImportPurchase;
            List<TIIncrement> inputCurrency = new List<TIIncrement> { _select };
            this.ConfigGridCurrencyByTemplate<TIIncrement>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            //Quân edit
            var uTabControl = Controls.Find("ultraTabControl", true)[0] as UltraTabControl;
            uTabControl.SelectedTabChanged += (s, e) => ultraTabControl_SelectedTabChanged(s, e, bdlTIIncrementDetail);
            #endregion


            #region Thông tin chung phần thanh toán Séc - UNC
            #region Account - Trả tiền
            DataBinding(new List<Control> { txtBankName, txtReason }, "BankName,Reason");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccount, "BankAccount", "ID", input, "BankAccountDetailID");
            #endregion
            #region Accounting - Nhận tiền
            this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjBank, "AccountingObjectBankAccount", accountingObjectType: 1);
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtAccountingObjtBankName, txtReceiver, txtIdentificationNo, txtIssueBy }, "AccountingObjectName,AccountingObjectAddress,AccountingObjectBankName,MContactName,IdentificationNo,IssueBy");
            DataBinding(new List<Control> { dteIssueDate }, "IssueDate", "2");
            txtIssueBy.Text = input.IssueBy;
            lblAccountingObjectAddress.Visible = txtAccountingObjectAddress.Visible = lblAccountingObjectBankAccount.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = true;
            if (input.TypeID == 905)
            {
                lblAccountingObjectAddress.Visible = txtAccountingObjectAddress.Visible = lblAccountingObjectBankAccount.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = false;
                lblReceiver.Visible = txtReceiver.Visible = lblIdentificationNo.Visible = txtIdentificationNo.Visible = lblIssuDate.Visible = dteIssueDate.Visible = lblIssueBy.Visible = txtIssueBy.Visible = true;
            }
            #endregion

            #region Thông tin chung phần thanh toán Tiền mặt
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDPM, "AccountingObjectCode", "ID",
                             _select, "AccountingObjectID", accountingObjectType: 1);
            DataBinding(new List<Control> { txtAccountingObjectAddressPM, txtAccountingObjectNamePM, txtReceiverPM, txtReasonPM, txtNumberAttach }, "AccountingObjectAddress,AccountingObjectName,MContactName,Reason,NumberAttach");
            #endregion

            #region Thông tin chung phần chưa thanh toán
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDBuy, "AccountingObjectCode", "ID",
                _select, "AccountingObjectID", accountingObjectType: 1);
            DataBinding(new List<Control> { txtAccountingObjectAddressBuy, txtAccountingObjectNameBuy, txtCompanyTaxCode, txtReasonBuy }, "AccountingObjectAddress,AccountingObjectName,TaxCode,Reason");
            #endregion

            #region Thông tin chung phần thanh toán bằng Thẻ tín dụng
            // Thông tin chung: Số thẻ, tên chủ thẻ, nội dung thanh toán
            this.ConfigCombo(input, "CreditCardNumber", Utils.ListCreditCard, cbbCreditCardNumber, picCreditCardType,
                             txtCreditCardType, txtOwnerCard);
            DataBinding(new List<Control> { txtReasonCreaditCard }, "Reason");
            //Phần còn lại sd chung với Séc - UNC
            #endregion

            ConfigGroup(false);

            #endregion
            #region Phần cuối
            lblTotalAmountOriginal.DataBindings.Clear();
            lblTotalAmountOriginal.BindControl("Text", input, "TotalAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalAmount.DataBindings.Clear();
            lblTotalAmount.BindControl("Text", input, "TotalAmount", ConstDatabase.Format_TienVND);
            lblTotalDiscountAmountOriginal.DataBindings.Clear();
            lblTotalDiscountAmountOriginal.BindControl("Text", input, "TotalDiscountAmountOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalDiscountAmount.DataBindings.Clear();
            lblTotalDiscountAmount.BindControl("Text", input, "TotalDiscountAmount", ConstDatabase.Format_TienVND);
            lblTotalAllOriginal.DataBindings.Clear();
            lblTotalAllOriginal.BindControl("Text", input, "TotalAllOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
            lblTotalAll.DataBindings.Clear();
            lblTotalAll.BindControl("Text", input, "TotalAll", ConstDatabase.Format_TienVND);
            lblTotalImportTaxAmount.DataBindings.Clear();
            lblTotalImportTaxAmount.BindControl("Text", input, "TotalImportTaxAmount", ConstDatabase.Format_TienVND);
            lblTotalSpecialConsumeTaxAmount.DataBindings.Clear();
            lblTotalSpecialConsumeTaxAmount.BindControl("Text", input, "TotalSpecialConsumeTaxAmount", ConstDatabase.Format_TienVND);
            lblTotalVATAmount.DataBindings.Clear();
            lblTotalVATAmount.BindControl("Text", input, "TotalVATAmount", ConstDatabase.Format_TienVND);
            lblTotalInwardAmount.DataBindings.Clear();
            lblTotalInwardAmount.BindControl("Text", input, "TotalInwardAmount", ConstDatabase.Format_TienVND);
            #endregion
            ugrid0 = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid1 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid3 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            if (ugrid3 != null)
                ugrid3.AfterRowsDeleted += new EventHandler(uGrid3_AfterRowsDeleted);
            if (uGridControl != null)
            {
                uGridControl.CellChange += new CellEventHandler(uGridControl_CellChange);
            }
            btnApportionTaxes.Visible = _select.IsImportPurchase ?? false;
            ugrid0.DisplayLayout.Bands[0].Columns["Quantity"].CellActivation = Activation.NoEdit;
            ugrid2 = Controls.Find("uGrid4", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 430)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Mua và ghi tăng CCDC";
                    txtReason.Text = "Mua và ghi tăng CCDC";
                    _select.Reason = "Mua và ghi tăng CCDC";
                };
            }
        }
        #endregion
        private void ultraTabControl_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e, BindingList<TIIncrementDetail> bdlTIIncrementDetail)
        {
            grHoaDon.Visible = false;
            if (paneTTC.Visible == true)
            {
                grBuyNotPay.Dock = DockStyle.Fill;
                var ppInvoid = new TIIncrementDetail();
                var ppInvoiddb = bdlTIIncrementDetail.FirstOrDefault();
                if (ppInvoiddb != null)
                {
                    ppInvoid = ppInvoiddb;
                }
                if (e.Tab.Key == "uTab1")
                {
                    grHoaDon.Visible = true;
                    grHoaDon.Anchor = AnchorStyles.Right;
                    grHoaDon.Anchor = AnchorStyles.Top;

                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                    ugrid1.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;


                    txtMauSoHD.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD.Text;
                    txtKyHieuHD.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD.Text;
                    txtSoHD.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD.Text;
                    dteNgayHD.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD.Value;

                    grBuyNotPay.Anchor = AnchorStyles.Left;
                    grBuyNotPay.Anchor = AnchorStyles.Right;
                    grBuyNotPay.Anchor = AnchorStyles.Top;
                    grBuyNotPay.Width = paneTTC.Width - grHoaDon.Width;
                }
            }

        }
        //Quân edit
        protected override bool AdditionalFunc()
        {
            if (txtKyHieuHD.Text != "" || txtMauSoHD.Text != "" || txtSoHD.Text != "" || dteNgayHD.Value != null)
            {
                if (txtKyHieuHD.Text != "" && txtMauSoHD.Text != "" && txtSoHD.Text != "" && dteNgayHD.Value != null)
                {
                    if (txtSoHD.Text.Length == 7)
                    {
                        var list = (BindingList<TIIncrementDetail>)_listObjectInput.FirstOrDefault();

                        foreach (var item in list)
                        {
                            item.InvoiceTemplate = txtMauSoHD.Text;
                            item.InvoiceSeries = txtKyHieuHD.Text;
                            item.InvoiceNo = txtSoHD.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD.Value.ToString());
                        }
                    }
                    else
                    {
                        MSG.Warning("Số hoá đơn phải là kiểu số và đúng 7 chữ số.");
                        return false;
                    }
                }
                else
                {
                    MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                    return false;
                }
            }
            else
            {
                var list = (BindingList<TIIncrementDetail>)_listObjectInput.FirstOrDefault();
                foreach (var item in list)
                {
                    item.InvoiceTemplate = "";
                    item.InvoiceSeries = "";
                    item.InvoiceNo = "";
                    item.InvoiceDate = null;
                }
            }
            return true;
        }
        private void ConfigGroup(bool status = true)
        {
            ultraPanel4.Visible = false;
            grAccount.Visible =
                grAccountCreaditCard.Visible = grAccounting.Visible = paneTTC.Visible = grPayment.Visible = false;
            string account = "";
            switch (_select.TypeID)
            {
                case 430:
                    paneTTC.Visible = true;
                    palInfo.Size = new Size(palInfo.Size.Width, paneTTC.Size.Height);
                    account = "331";
                    break;
                case 902:
                    grPayment.Visible = true;
                    palInfo.Size = new Size(palInfo.Size.Width, grPayment.Size.Height);
                    account = "1111";
                    break;
                case 905:
                    grAccount.Visible = grAccounting.Visible = true;
                    palInfo.Size = new Size(palInfo.Size.Width, grAccount.Size.Height + grAccounting.Size.Height);
                    //lblAccountingObject.Text = "Trả cho";
                    lblAccountingObjectAddress.Text = "Người lĩnh tiền";
                    txtAccountingObjectAddress.Visible = false;
                    lblAccountingObjectBankAccount.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = false;
                    txtReceiver.Visible = true;
                    lblIdentificationNo.Visible = txtIdentificationNo.Visible = true;
                    lblIssuDate.Visible = dteIssueDate.Visible = true;
                    lblIssueBy.Visible = txtIssueBy.Visible = true;
                    account = "1121";
                    break;
                case 906:
                    grAccountCreaditCard.Visible = grAccounting.Visible = true;
                    palInfo.Size = new Size(palInfo.Size.Width, grAccountCreaditCard.Size.Height + grAccounting.Size.Height);
                    //lblAccountingObject.Text = "Đối tượng";
                    lblAccountingObjectAddress.Text = "Địa chỉ";
                    lblIdentificationNo.Visible = lblIssuDate.Visible = lblIssueBy.Visible = false;
                    txtReceiver.Visible = txtIdentificationNo.Visible = dteIssueDate.Visible = txtIssueBy.Visible = false;
                    lblAccountingObjectBankAccount.Visible = true;
                    txtAccountingObjectAddress.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = true;
                    account = "1121";
                    break;
                default:
                    grAccount.Visible = grAccounting.Visible = true;
                    palInfo.Size = new Size(palInfo.Size.Width, grAccount.Size.Height + grAccounting.Size.Height);
                    //lblAccountingObject.Text = "Đối tượng";
                    lblAccountingObjectAddress.Text = "Địa chỉ";
                    lblIdentificationNo.Visible = lblIssuDate.Visible = lblIssueBy.Visible = false;
                    txtReceiver.Visible = txtIdentificationNo.Visible = dteIssueDate.Visible = txtIssueBy.Visible = false;
                    lblAccountingObjectBankAccount.Visible = true;
                    txtAccountingObjectAddress.Visible = cbbAccountingObjBank.Visible = txtAccountingObjtBankName.Visible = true;
                    account = "1121";
                    break;
            }
            if (TypeGroup == null) return;
            this.ReconfigAccountColumn<TIIncrement>(_select.TypeID);
            if (_select.TemplateID == null) return;
            //this.ReloadTemplate<TIIncrement>((Guid)_select.TemplateID, false);
            this.ConfigTemplateEdit<TIIncrement>(utmDetailBaseToolBar, TypeID, _statusForm, _select.TemplateID);


            if (cbbAccountingObjectIDPM.IsDroppedDown) cbbAccountingObjectIDPM.ToggleDropdown();
            if (cbbAccountingObjBank.IsDroppedDown) cbbAccountingObjBank.ToggleDropdown();
            if (cbbAccountingObjectBankAccount.IsDroppedDown) cbbAccountingObjectBankAccount.ToggleDropdown();
            if (cbbAccountingObjectID.IsDroppedDown) cbbAccountingObjectID.ToggleDropdown();
            if (cbbAccountingObjectIDBuy.IsDroppedDown) cbbAccountingObjectIDBuy.ToggleDropdown();
            if (ugrid0 != null)
            {
                var datasource = (BindingList<TIIncrementDetail>)ugrid0.DataSource;
                foreach (TIIncrementDetail detail in datasource)
                {
                    detail.CreditAccount = account;
                }
                ugrid0.DataSource = datasource;
            }
            ultraPanel4.Visible = true;
        }

        private void optPayment_ValueChanged(object sender, EventArgs e)
        {
            var opt = (UltraOptionSet_Ex)sender;
            if (opt.Value == null) return;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (!(bool)opt.Value)
                {

                    _select.TypeID = 430;
                    cbbPayment.Enabled = false;
                    ConfigGroup();
                }
                else
                {
                    cbbPayment.Enabled = true;
                    if (cbbPayment.SelectedItem == null)
                    {
                        cbbPayment.SelectedItem = cbbPayment.Items.ValueList.FindByDataValue(902);
                        return;
                    }
                    _select.TypeID = (int)cbbPayment.Value;
                    ConfigGroup();
                }
            }
            else if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                if (!(bool)opt.Value)
                {
                    if ((Utils.IRefVoucherService.Query.Any(n => n.RefID2 == _select.ID && n.TypeID == (cbbPayment.Value == null ? 0 : (int)cbbPayment.Value)) || (Utils.IRefVoucherRSInwardOutwardService.Query.Any(n => n.RefID2 == _select.ID && n.TypeID == (cbbPayment.Value == null ? 0 : (int)cbbPayment.Value)))))
                    {
                        if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            _select.TypeID = 430;
                            cbbPayment.Enabled = false;
                            ConfigGroup();
                        }
                        else
                        {
                            optPayment.CheckedIndex = 1;

                            //return;
                        }
                    }
                    else
                    {
                        _select.TypeID = 430;
                        cbbPayment.Enabled = false;
                        ConfigGroup();
                    }
                }
                else
                {
                    if ((Utils.ListMCPaymentDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0) || (Utils.ListMBTellerPaperDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0) || (Utils.ListMBCreditCardDetailVendor.Where(n => n.PPInvoiceID == _select.ID).ToList().Count != 0))
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Chứng từ đã phát sinh nghiệp vụ trả tiền nhà cung cấp");
                        opt.Value = false;
                    }
                    if ((Utils.IRefVoucherService.Query.Any(n => n.RefID2 == _select.ID && n.TypeID == 430)) || (Utils.IRefVoucherRSInwardOutwardService.Query.Any(n => n.RefID2 == _select.ID && n.TypeID == 430)))
                    {
                        if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            cbbPayment.Enabled = true;
                            tuOptPaymentChuyenSang = true;
                            if (cbbPayment.SelectedItem == null)
                            {
                                cbbPayment.SelectedItem = cbbPayment.Items.ValueList.FindByDataValue(902);
                                return;
                            }
                            _select.TypeID = (int)cbbPayment.Value;
                            ConfigGroup();
                        }
                        else
                        {
                            optPayment.CheckedIndex = 0;
                            //return;
                        }
                    }
                    else
                    {
                        cbbPayment.Enabled = true;
                        if (cbbPayment.SelectedItem == null)
                        {
                            cbbPayment.SelectedItem = cbbPayment.Items.ValueList.FindByDataValue(902);
                            return;
                        }
                        _select.TypeID = (int)cbbPayment.Value;
                        ConfigGroup();
                    }
                }
            }
            var hienthi = !(new List<int> { 902, 903, 904, 905, 906 }.Any(x => x == _select.TypeID) && _select.CurrencyID != "VND");
            if (ugrid0 != null)
            {
                var band = ugrid0.DisplayLayout.Bands[0];
                band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                band.Columns["CashOutAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAccount"].Hidden = hienthi;
            }
            if (ugrid1 != null)
            {
                var band = ugrid1.DisplayLayout.Bands[0];
                band.Columns["CashOutVATAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
            }
        }

        private void cbbPayment_SelectionChanged(object sender, EventArgs e)
        {
            var cbb = (UltraComboEditor)sender;
            if (cbb.SelectedItem == null) return;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = (int)cbb.Value;
            }
            else if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                if (tuOptPaymentChuyenSang == false)
                {
                    if (tuCbbPaymentChuyenSang == false)
                    {
                        if ((Utils.IRefVoucherService.Query.Any(n => n.RefID2 == _select.ID && n.TypeID == _select.TypeID)) || (Utils.IRefVoucherRSInwardOutwardService.Query.Any(n => n.RefID2 == _select.ID && n.TypeID == _select.TypeID)))
                        {
                            if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                            {
                                _select.TypeID = (int)cbb.Value;
                            }
                            else
                            {
                                tuCbbPaymentChuyenSang = true;
                                cbbPayment.SelectedItem = cbbPayment.Items.ValueList.FindByDataValue(_select.TypeID);

                                return;
                            }
                        }
                        else
                        {
                            _select.TypeID = (int)cbb.Value;
                        }
                    }
                    else
                    {
                        _select.TypeID = (int)cbb.Value;
                        tuCbbPaymentChuyenSang = false;
                    }
                }
                else
                {
                    _select.TypeID = (int)cbb.Value;
                    tuOptPaymentChuyenSang = false;
                }
            }
            ConfigGroup();
            var hienthi = (_select.CurrencyID == "VND");
            if (ugrid0 != null)
            {
                var band = ugrid0.DisplayLayout.Bands[0];
                band.Columns["CashOutExchangeRate"].Hidden = hienthi;
                band.Columns["CashOutAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferAccount"].Hidden = hienthi;
            }
            if (ugrid1 != null)
            {
                var band = ugrid1.DisplayLayout.Bands[0];
                band.Columns["CashOutVATAmount"].Hidden = hienthi;
                band.Columns["CashOutDifferVATAmount"].Hidden = hienthi;
            }
        }

        private void cbbBankAccount_Validated(object sender, EventArgs e)
        {
            if (cbbBankAccount.Value == null)
            {
                txtBankName.Value = "";
            }
        }

        private void btnLiabilities_Click(object sender, EventArgs e)
        {
            if (_select.AccountingObjectID == null)
            {
                MSG.Warning("Bạn phải chọn đối tượng để xem công nợ!!");
                return;
            }
            decimal debt = Utils.IGeneralLedgerService.GetCredCustomer(_select.AccountingObjectID ?? Guid.Empty, _select.PostedDate);
            MSG.MessageBoxStand(debt.ToStringNumbericFormat(ConstDatabase.Format_TienVND), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnApportionTaxes_Click(object sender, EventArgs e)
        {
            if (ugrid0 != null && ugrid0.Rows.Count == 0)
            {
                MSG.Warning("Bạn phải nhập chi tiết trước khi phân bổ");
                return;
            }

            var datasource = (BindingList<TIIncrementDetail>)ugrid0.DataSource;
            _select.TIIncrementDetails = datasource;
            var f = new FTIAllocationImportTax(_select, _statusForm);
            f.FormClosed += new FormClosedEventHandler(fAllocationImportTax_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void btnApportion_Click(object sender, EventArgs e)
        {
            if (ugrid0 != null && ugrid0.Rows.Count == 0)
            {
                MSG.Warning("Bạn phải nhập chi tiết trước khi phân bổ");
                return;
            }
            UltraGrid ugrid = Controls.Find("ugrid0", true).FirstOrDefault() as UltraGrid;
            var datasource = (BindingList<TIIncrementDetail>)ugrid.DataSource;
            _select.TIIncrementDetails = datasource;
            var f = new FTICostAlLocation(_select, _statusForm);
            f.FormClosed += new FormClosedEventHandler(fCostAllocation_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void fAllocationImportTax_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FTIAllocationImportTax)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var TIIncrement = f.TIIncrement;
                    //var uGrid0 = Controls.Find("uGrid0", true)[0] as UltraGrid;
                    if (ugrid0 == null) return;
                    foreach (var row in ugrid0.Rows)
                    {
                        var detail = TIIncrement.TIIncrementDetails.FirstOrDefault(x => x.ToolsID == (Guid)row.Cells["ToolsID"].Value);
                        if (detail == null) continue;
                        row.Cells["ImportTaxExpenseAmountOriginal"].Value = detail.ImportTaxExpenseAmountOriginal;
                        row.Cells["ImportTaxExpenseAmount"].Value = detail.ImportTaxExpenseAmount;
                        row.UpdateData();

                        row.CalculationImportTaxAmount(this);
                        row.CalculationVatAmountSpecialCase(this);

                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["ImportTaxAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["ImportTaxAmountOriginal"]);

                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["VATAmount"]);
                        this.UpdateSelectFromSummaryValue<PPInvoice>(row.Cells["VATAmountOriginal"]);

                    }
                    if (ugrid3 == null) return;
                    ugrid3.DataSource = TIIncrement.PPInvoiceDetailCosts;
                }
            }
        }

        private void fCostAllocation_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FTICostAlLocation)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var TIIncrement = f.TIIncrement;
                    //var uGrid0 = Controls.Find("uGrid0", true)[0] as UltraGrid;
                    if (ugrid0 == null) return;
                    foreach (var row in ugrid0.Rows)
                    {
                        var detail = TIIncrement.TIIncrementDetails.FirstOrDefault(x => x.ToolsID == (Guid)row.Cells["ToolsID"].Value);
                        if (detail == null) continue;
                        row.Cells["FreightAmountOriginal"].Value = detail.FreightAmount;
                        row.Cells["FreightAmount"].Value = detail.FreightAmount * _select.ExchangeRate;
                    }
                    if (ugrid3 == null) return;
                    ugrid3.DataSource = TIIncrement.PPInvoiceDetailCosts;
                }
            }
        }

        void uGridControl_CellChange(object sender, CellEventArgs e)
        {

            if (e.Cell.Column.Key == "IsImportPurchase")
            {
                //btnApportionTaxes.EnabledControl();
                btnApportionTaxes.Visible = e.Cell.Text == "Nhập khẩu";
                if (e.Cell.Text == "Trong nước")
                {
                    foreach (var row in ugrid1.Rows)
                    {
                        row.Cells["ImportTaxRate"].Value = (decimal)0;
                        row.Cells["ImportTaxAmountOriginal"].Value = (decimal)0;
                        row.Cells["ImportTaxAccount"].Value = "";

                    }
                    lblTotalImportTaxAmount.Text = "0";
                    lblTotalAllOriginal.DataBindings.Clear();
                    lblTotalAllOriginal.BindControl("Text", _select, "TotalAllOriginal", ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
                    lblTotalAll.DataBindings.Clear();
                    lblTotalAll.BindControl("Text", _select, "TotalAll", ConstDatabase.Format_TienVND);
                }
            }
        }

        private void uGrid3_AfterRowsDeleted(object sender, EventArgs e)
        {
            var dugrid3 = ugrid3.DataSource as List<PPInvoiceDetailCost>;
            var p = ((decimal?)ugrid0.Rows.SummaryValues["SumAmountOriginal"].Value ?? 0);
            if (p > 0)
                foreach (var row in ugrid0.Rows)
                {
                    var a = dugrid3.Where(x => x.CostType == false).Sum(x => x.AmountPBOriginal);
                    var b = ((decimal)(row.Cells["AmountOriginal"].Value ?? 0));
                    var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                    var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                    row.Cells["FreightAmountOriginal"].Value = d;
                    row.Cells["FreightAmount"].Value = d * _select.ExchangeRate;
                    var f = dugrid3.Where(x => x.CostType == true).Sum(x => x.AmountPBOriginal);
                    var g = ((decimal)(row.Cells["AmountOriginal"].Value ?? 0));
                    var h = (f == 0 || g == 0) ? 0 : (g / p * 100);
                    var i = (f == 0 || g == 0) ? 0 : (f * h / 100);
                    row.Cells["ImportTaxExpenseAmountOriginal"].Value = i;
                    row.Cells["ImportTaxExpenseAmount"].Value = i * _select.ExchangeRate;
                    row.CalculationImportTaxAmount(this);
                }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID,
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FTIIncrementBuyDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccountingObjectNamePM_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
    }
    public class FSTIIncrementBuyDetailBase : DetailBase<TIIncrement>
    {

    }
}
