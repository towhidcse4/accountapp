﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FTIOriginalVoucher : CustormForm
    {
        private IRSInwardOutwardService _IRSInwardOutwardService { get { return IoC.Resolve<IRSInwardOutwardService>(); } }
        private IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        private IMBTellerPaperService _IMBTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        private IGOtherVoucherService _IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        private IPPInvoiceService _IPPInvoiceService { get { return IoC.Resolve<IPPInvoiceService>(); } }
        private IPPServiceService _IPPServiceService { get { return IoC.Resolve<IPPServiceService>(); } }
        private DateTime _startDate;
        public List<TIOriginalVoucher> TIOriginalVoucher;
        public List<TIOriginalVoucher> selected;
        public string type;
        String clone;

        public FTIOriginalVoucher(String CurrencyID, List<TIOriginalVoucher> datasource)
        {
            InitializeComponent();

            btnGetData.Click += (s, e) => btnGetData_Click(s, e, CurrencyID);
            selected = datasource;
            ConfigControl(CurrencyID);
            clone = CurrencyID;
            type = "2";
            
        }

        private void ConfigControl(String CurrencyID)
        {

            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            cbbAboutTime.ConfigComboSelectTime(Utils.ObjConstValue.SelectTimes, "Name");
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];

            List<Item> types = new List<Item>();
            types.Add(new Item() { ID = "0", Value = "0", Name = "Phiếu chi", Visible = "1" });
            types.Add(new Item() { ID = "1", Value = "1", Name = "Séc/ủy nhiệm chi", Visible = "1" });
            types.Add(new Item() { ID = "2", Value = "2", Name = "Xuất kho", Visible = "1" });
            types.Add(new Item() { ID = "3", Value = "3", Name = "Chứng từ nghiệp vụ khác", Visible = "1" });
            types.Add(new Item() { ID = "4", Value = "4", Name = "Mua hàng", Visible = "1" });
            types.Add(new Item() { ID = "5", Value = "5", Name = "Mua dịch vụ", Visible = "1" });
            cbbType.ConfigComboSelectTime(types, "Name");
            cbbType.SelectedRow = cbbType.Rows[2];

            // lấy dữ liệu theo khoảng thời gian
            TIOriginalVoucher = _IRSInwardOutwardService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, CurrencyID);
            TIOriginalVoucher.Where(x => selected.Any(y => y.No == x.No)).Select(z => { z.Check = true; return z; }).ToList();
            // cấu hình cho lưới
            ConfigGrid(TIOriginalVoucher);
        }

        private void ConfigGrid(List<TIOriginalVoucher> input)
        {
            uGrid.DataSource = input;
            Utils.ConfigGrid(uGrid, ConstDatabase.TIOriginalVoucher_TableName, false);
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.UseFixedHeaders = false;
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid, true);
            }
            uGrid.DisplayLayout.Bands[0].Columns["Check"].Header.VisiblePosition = 0;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGetData_Click(object sender, EventArgs e, String CurrencyID)
        {
            switch (type)
            {
                case "0":
                    TIOriginalVoucher = _IMCPaymentService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "1":
                    TIOriginalVoucher = _IMBTellerPaperService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "2":
                    TIOriginalVoucher = _IRSInwardOutwardService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "3":
                    TIOriginalVoucher = _IGOtherVoucherService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "4":
                    TIOriginalVoucher = _IPPInvoiceService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "5":
                    TIOriginalVoucher = _IPPServiceService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                default:
                    TIOriginalVoucher = null;
                    break;
            }
            TIOriginalVoucher.Where(x => selected.Any(y => y.No == x.No)).Select(z => { z.Check = true; return z; }).ToList();
            ConfigGrid(TIOriginalVoucher);
        }

        private void cbbAboutTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.DateTime = dtBegin;
                dteDateTo.DateTime = dtEnd;
            }
        }

        private void dteDateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (ActiveControl != null && ActiveControl.Name == ((UltraDateTimeEditor)sender).Name)
                cbbAboutTime.SelectedRow = cbbAboutTime.Rows[34];
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            TIOriginalVoucher = (List<TIOriginalVoucher>)uGrid.DataSource;
            TIOriginalVoucher.AddRange(selected.Where(x => !TIOriginalVoucher.Any(y => y.No == x.No)).ToList());
            TIOriginalVoucher = TIOriginalVoucher.Where(a => a.Check).ToList();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void ultraLabel2_Click(object sender, EventArgs e)
        {

        }

        private void cbbType_ValueChanged(object sender, EventArgs e)
        {
            var model = cbbType.SelectedRow.ListObject as Item;
            if (clone == null || clone == null) return;
            switch (model.Value)
            {
                case "0":
                    TIOriginalVoucher = _IMCPaymentService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "1":
                    TIOriginalVoucher = _IMBTellerPaperService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "2":
                    TIOriginalVoucher = _IRSInwardOutwardService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "3":
                    TIOriginalVoucher = _IGOtherVoucherService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "4":
                    TIOriginalVoucher = _IPPInvoiceService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                case "5":
                    TIOriginalVoucher = _IPPServiceService.getOriginalVoucher(dteDateFrom.DateTime, dteDateTo.DateTime, clone);
                    break;
                default:
                    TIOriginalVoucher = null;
                    break;
            }
            type = model.Value;
            TIOriginalVoucher.Where(x => selected.Any(y => y.No == x.No)).Select(z => { z.Check = true; return z; }).ToList();
            uGrid.DataSource = TIOriginalVoucher;

        }
    }
}