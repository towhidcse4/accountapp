﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FTICostAlLocation : CustormForm
    {
        private IPPInvoiceDetailCostService _IPPInvoiceDetailCostService { get { return IoC.Resolve<IPPInvoiceDetailCostService>(); } }
        public TIIncrement TIIncrement { get; private set; }
        private List<PPServices> _ppServices = new List<PPServices>();
        private List<PPInvoiceDetailCost> _ppInvoiceDetailCosts = new List<PPInvoiceDetailCost>();
        int statusForm;
        public FTICostAlLocation(TIIncrement input, int _statusForm)
        {
            InitializeComponent();
            statusForm = _statusForm;
            TIIncrement = input;
            var lstFAIncrementDetail = input.TIIncrementDetails.Where(x => x.ToolsID != null).ToList();
            decimal sumFreightAmount = lstFAIncrementDetail.Sum(x => x.FreightAmount);
            _ppInvoiceDetailCosts = input.PPInvoiceDetailCosts.ToList();
            if (_ppInvoiceDetailCosts.Count > 0)
            {
                txtTotalFreightAmount.Value = _ppInvoiceDetailCosts.Where(x => x.CostType == true).Sum(x => x.TotalFreightAmountOriginal);
                btnAllocation.PerformClick();
            }
            List<TICostAllocation> listInput = lstFAIncrementDetail.Select(
                detail => new TICostAllocation
                {
                    ToolsID = 
                    (Guid)detail.ToolsID,
                    Description = detail.Description,
                    Quantity = detail.Quantity,
                    Amount = detail.AmountOriginal,
                    FreightAmount = detail.FreightAmount,
                    FreightRate = (decimal)sumFreightAmount == 0 ? (decimal)0 : ((detail.FreightAmount / (decimal)sumFreightAmount) * 100),
                    TIIncrementDetail = detail
                }).ToList();
            uGrid.DataSource = listInput;
            uGrid.ConfigGrid(ConstDatabase.CostAlLocation_KeyName, null, true, false);
            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.ConfigSummaryOfGrid();
            uGrid.DisplayLayout.Override.HeaderAppearance.TextHAlign = HAlign.Center;
            txtTotalFreightAmount.FormatNumberic(input.CurrencyID.Equals("VND") ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            btnAllocation.Enabled = uGrid.Rows.Count > 0;
            btnApply.Enabled = uGrid.Rows.Count > 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAllocation_Click(object sender, EventArgs e)
        {
            CalculateFreight();
        }

        private void CalculateFreight()
        {
            var p = optSort.CheckedIndex == 0 ? ((decimal?)uGrid.Rows.SummaryValues["SumQuantity"].Value ?? 0) : ((decimal?)uGrid.Rows.SummaryValues["SumAmount"].Value ?? 0);
            foreach (var row in uGrid.Rows)
            {
                //var a = txtTotalFreightAmount.Text == "" ? (decimal)0 : Convert.ToDecimal(txtTotalFreightAmount.Value ?? 0);
                var a = _ppServices.Sum(x => x.FreightAmount);
                var b = optSort.CheckedIndex == 0 ? ((decimal)(row.Cells["Quantity"].Value ?? 0)) : ((decimal)(row.Cells["Amount"].Value ?? 0));
                var c = (a == 0 || b == 0) ? 0 : (b / p * 100);
                var d = (a == 0 || b == 0) ? 0 : (a * c / 100);
                row.Cells["FreightRate"].Value = c;
                row.Cells["FreightAmount"].Value = d;
            }
        }

        private void btnSelectPPService_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FTISelectPPServices(TIIncrement, statusForm, false);
                f.FormClosed += new FormClosedEventHandler(fSelectPPServices_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void fSelectPPServices_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var f = (FTISelectPPServices)sender;
            if (f.DialogResult == DialogResult.OK)
            {
                _ppServices = f.SelectPPServices;
                txtTotalFreightAmount.Value = _ppServices.Sum(x => x.FreightAmount);
                txtTotalFreightAmount.ReadOnly = _ppServices.Count > 1;
                for(int i = 0; i < _ppInvoiceDetailCosts.Count; i++)
                {
                    if (_ppServices.Any(x => x.ID == _ppInvoiceDetailCosts[i].PPServiceID && _ppInvoiceDetailCosts[i].CostType == false)) _ppInvoiceDetailCosts.RemoveAt(i);
                }
                foreach (var pps in _ppServices)
                {
                    if (_ppInvoiceDetailCosts.Any(x => x.PPServiceID == pps.ID && x.CostType == false))
                    {
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmount = pps.TotalAmount;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].TotalFreightAmountOriginal = pps.TotalAmountOriginal;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB = pps.FreightAmount * TIIncrement.ExchangeRate;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal = pps.FreightAmount;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmount = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPB + (pps.AccumulatedAllocateAmount)) * TIIncrement.ExchangeRate;
                        _ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AccumulatedAllocateAmountOriginal = (_ppInvoiceDetailCosts[_ppInvoiceDetailCosts.FindIndex(x => x.PPServiceID == pps.ID)].AmountPBOriginal + (pps.AccumulatedAllocateAmount));
                    }
                    else
                    {
                        PPInvoiceDetailCost ppi = new PPInvoiceDetailCost
                        {
                            AccountingObjectID = pps.AccountingObjectID,
                            AccountingObjectName = pps.AccountingObjectName,
                            Date = pps.Date,
                            PostedDate = pps.PostedDate,
                            No = pps.No,
                            CostType = false,
                            TypeID = TIIncrement.TypeID,
                            PPServiceID = pps.ID,
                            TotalFreightAmount = pps.TotalAmount,
                            TotalFreightAmountOriginal = pps.TotalAmountOriginal,
                            AmountPB = pps.FreightAmount * TIIncrement.ExchangeRate,
                            AmountPBOriginal = pps.FreightAmount
                        };
                        ppi.AccumulatedAllocateAmount = ppi.AmountPB + pps.AccumulatedAllocateAmount;
                        ppi.AccumulatedAllocateAmountOriginal = ppi.AmountPBOriginal + pps.AccumulatedAllocateAmountOriginal;
                        ppi.ID = Guid.NewGuid();
                        _ppInvoiceDetailCosts.Add(ppi);
                    }
                }               
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                TIIncrement.PPInvoiceDetailCosts = _ppInvoiceDetailCosts;
                SaveAndClose();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        void SaveAndClose()
        {
            var listInput = (List<TICostAllocation>)uGrid.DataSource;
            foreach (var TIIncrementDetail in TIIncrement.TIIncrementDetails)
            {
                var costAllocation = listInput.FirstOrDefault(x => x.TIIncrementDetail.ToolsID == TIIncrementDetail.ToolsID);
                if (costAllocation != null)
                {
                    TIIncrementDetail.FreightAmount = costAllocation.FreightAmount;
                    //FAIncrementDetail.FreightAmount = costAllocation.FreightAmount * (FAIncrement.ExchangeRate ?? 1);
                }
                    
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
        }
    }
}
