﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class FTIIncrementDetail : FSTIIncrementBase
    {
        #region khai bao
        public static bool isClose = true;
        private readonly IRSInwardOutwardService _IRSInwardOutwardService = Utils.IRSInwardOutwardService;
        private IMCPaymentService _IMCPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        private IMBTellerPaperService _IMBTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        private IGOtherVoucherService _IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        private IPPInvoiceService _IPPInvoiceService { get { return IoC.Resolve<IPPInvoiceService>(); } }
        private IPPServiceService _IPPServiceService { get { return IoC.Resolve<IPPServiceService>(); } }
        UltraGrid ugrid2 = null;
        #endregion
        #region khởi tạo
        public FTIIncrementDetail(TIIncrement temp, List<TIIncrement> lstIncrements, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 907;
            }
            else
            {
                _select = temp;
            }
            _listSelects.AddRange(lstIncrements);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion

        }
        #endregion
        #region
        #endregion
        #region sử lý các viewgrid trên form
        #endregion
        #region event

        #endregion
        #region Hàm override
        public override void InitializeGUI(TIIncrement input)
        {
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            BindingList<TIIncrementDetail> dsIncrementDetails = new BindingList<TIIncrementDetail>();
            List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                dsIncrementDetails = new BindingList<TIIncrementDetail>(input.TIIncrementDetails);
                if (_select.Type != null && _select.OriginalVoucher != null)
                {
                    dsTIOriginalVouchers = _select.OriginalVoucher != null ? _IRSInwardOutwardService.FindByListID(_select.OriginalVoucher) : new List<TIOriginalVoucher>(); ;
                    switch (_select.Type)
                    {
                        case "0":
                            dsTIOriginalVouchers = _IMCPaymentService.FindByListID(_select.OriginalVoucher);
                            break;
                        case "1":
                            dsTIOriginalVouchers = _IMBTellerPaperService.FindByListID(_select.OriginalVoucher);
                            break;
                        case "2":
                            dsTIOriginalVouchers = _IRSInwardOutwardService.FindByListID(_select.OriginalVoucher);
                            break;
                        case "3":
                            dsTIOriginalVouchers = _IGOtherVoucherService.FindByListID(_select.OriginalVoucher);
                            break;
                        case "4":
                            dsTIOriginalVouchers = _IPPInvoiceService.FindByListID(_select.OriginalVoucher);
                            break;
                        case "5":
                            dsTIOriginalVouchers = _IPPServiceService.FindByListID(_select.OriginalVoucher);
                            break;
                        default:
                            dsTIOriginalVouchers = new List<TIOriginalVoucher>();
                            break;
                    }
                } 
                
            }
                
            #region cau hinh ban dau cho form

            _listObjectInput = new BindingList<System.Collections.IList> { dsIncrementDetails };
            _listObjectInputPost = new BindingList<System.Collections.IList> { dsTIOriginalVouchers };
            this.ConfigGridByTemplete_General<TIIncrement>(pnlUgrid, mauGiaoDien);

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
            this.ConfigGridByManyTemplete<TIIncrement>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());

            this.ConfigTopVouchersNo<TIIncrement>(pnlDateNo, "No", "PostedDate", "Date");
            // add by Hautv
            Control dteP = this.Controls.Find("dtePostedDate", true).First();
            Control lbeP = this.Controls.Find("lblPostedDate", true).First();
            if (dteP != null && lbeP != null)
            {
                dteP.Visible = false;
                lbeP.Visible = false;
            }
            /*---------------------------------------*/
            txtReason.DataBindings.Clear();
            txtReason.DataBindings.Add("Text", input, "Reason", true, DataSourceUpdateMode.OnPropertyChanged);

            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            _select.IsImportPurchase = _statusForm == ConstFrm.optStatusForm.Add ? false : input.IsImportPurchase;

            var inputCurrency = new List<TIIncrement> { _select };
            this.ConfigGridCurrencyByTemplate<TIIncrement>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
            ugrid2.AfterRowsDeleted += new EventHandler(uGrid2_AfterRowsDeleted);
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 907)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Ghi tăng CCDC khác";
                    txtReason.Text = "Ghi tăng CCDC khác";
                    _select.Reason = "Ghi tăng CCDC khác";
                };
            }
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            List<TIOriginalVoucher> datasource = (List<TIOriginalVoucher>)ugrid2.DataSource;
            if (datasource == null)
                datasource = new List<TIOriginalVoucher>();
            var f = new FTIOriginalVoucher(_select.CurrencyID, datasource);
            f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FTIOriginalVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    ugrid2.DataSource = f.TIOriginalVoucher;
                    String s = "";
                    decimal TotalAmountFromVoucher = 0;
                    foreach (TIOriginalVoucher ov in f.TIOriginalVoucher)
                    {
                        s += string.Format("{0}{1}",ov.ID.ToString(),";");
                        TotalAmountFromVoucher += ov.Amount;
                    }
                    _select.OriginalVoucher = s;
                    _select.Type = f.type;
                    _select.TotalAmountFromVoucher = TotalAmountFromVoucher;
                }
            }
        }

        private void uGrid2_AfterRowsDeleted(object sender, EventArgs e)
        {
            var dugrid3 = ugrid2.DataSource as List<TIOriginalVoucher>;
            String s = "";
            decimal TotalAmountFromVoucher = 0;
            foreach (TIOriginalVoucher ov in dugrid3)
            {
                s += string.Format("{0}{1}", ov.ID.ToString(), ";");
                TotalAmountFromVoucher += ov.Amount;
            }
            _select.OriginalVoucher = s;
            _select.TotalAmountFromVoucher = TotalAmountFromVoucher;
        }

        private void FTIIncrementDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FSTIIncrementBase : DetailBase<TIIncrement> { }
}
