﻿namespace Accounting
{
    partial class FTIIncrementBuyDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("btnNewAccountingObject");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("btnNewBankAccount");
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            this.ultraPanel_KhungTren = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbPayment = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.optPayment = new Accounting.UltraOptionSet_Ex();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.palInfo = new Infragistics.Win.Misc.UltraPanel();
            this.grAccounting = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnLiabilities = new Infragistics.Win.Misc.UltraButton();
            this.lblReceiver = new Infragistics.Win.Misc.UltraLabel();
            this.txtReceiver = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtIssueBy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblIssueBy = new Infragistics.Win.Misc.UltraLabel();
            this.txtIdentificationNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dteIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblIssuDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblIdentificationNo = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectBankAccount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjBank = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.grAccountCreaditCard = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCreditCardType = new Infragistics.Win.Misc.UltraLabel();
            this.picCreditCardType = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.cbbCreditCardNumber = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtOwnerCard = new System.Windows.Forms.Label();
            this.lblOwnerCard = new Infragistics.Win.Misc.UltraLabel();
            this.lblCreditCardNumber = new Infragistics.Win.Misc.UltraLabel();
            this.txtReasonCreaditCard = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.grPayment = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnLiabilitiesPM = new Infragistics.Win.Misc.UltraButton();
            this.txtAccountingObjectNamePM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectIDPM = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNumberAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.txtReasonPM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReceiverPM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.grAccount = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblBankAccount = new Infragistics.Win.Misc.UltraLabel();
            this.paneTTC = new Infragistics.Win.Misc.UltraPanel();
            this.grHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.grBuyNotPay = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnLiabilitiesBuy = new Infragistics.Win.Misc.UltraButton();
            this.txtAccountingObjectNameBuy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDBuy = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonBuy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressBuy = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.pltab_chitiet = new Infragistics.Win.Misc.UltraPanel();
            this.palGrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.btnApportion = new Infragistics.Win.Misc.UltraButton();
            this.btnApportionTaxes = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalInwardAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalSpecialConsumeTaxAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalImportTaxAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAllOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalDiscountAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAll = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.palBot = new System.Windows.Forms.Panel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraPanel_KhungTren.ClientArea.SuspendLayout();
            this.ultraPanel_KhungTren.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPayment)).BeginInit();
            this.palTopVouchers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).BeginInit();
            this.palInfo.ClientArea.SuspendLayout();
            this.palInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grAccounting)).BeginInit();
            this.grAccounting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grAccountCreaditCard)).BeginInit();
            this.grAccountCreaditCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonCreaditCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grPayment)).BeginInit();
            this.grPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiverPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grAccount)).BeginInit();
            this.grAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            this.paneTTC.ClientArea.SuspendLayout();
            this.paneTTC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).BeginInit();
            this.grHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grBuyNotPay)).BeginInit();
            this.grBuyNotPay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameBuy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDBuy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonBuy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressBuy)).BeginInit();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.pltab_chitiet.ClientArea.SuspendLayout();
            this.pltab_chitiet.SuspendLayout();
            this.palGrid.ClientArea.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.palBot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanel_KhungTren
            // 
            // 
            // ultraPanel_KhungTren.ClientArea
            // 
            this.ultraPanel_KhungTren.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel_KhungTren.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel_KhungTren.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel_KhungTren.Name = "ultraPanel_KhungTren";
            this.ultraPanel_KhungTren.Size = new System.Drawing.Size(856, 79);
            this.ultraPanel_KhungTren.TabIndex = 1;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.cbbPayment);
            this.ultraGroupBox3.Controls.Add(this.optPayment);
            this.ultraGroupBox3.Controls.Add(this.palTopVouchers);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance3.FontData.BoldAsString = "True";
            appearance3.FontData.SizeInPoints = 13F;
            this.ultraGroupBox3.HeaderAppearance = appearance3;
            this.ultraGroupBox3.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(856, 79);
            this.ultraGroupBox3.TabIndex = 2;
            this.ultraGroupBox3.Text = "Ghi tăng công cụ dụng cụ";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbPayment
            // 
            this.cbbPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbPayment.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbPayment.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbPayment.Enabled = false;
            this.cbbPayment.Location = new System.Drawing.Point(679, 36);
            this.cbbPayment.Name = "cbbPayment";
            this.cbbPayment.Size = new System.Drawing.Size(165, 21);
            this.cbbPayment.TabIndex = 44;
            // 
            // optPayment
            // 
            this.optPayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance1.TextVAlignAsString = "Middle";
            this.optPayment.Appearance = appearance1;
            this.optPayment.BackColor = System.Drawing.Color.Transparent;
            this.optPayment.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPayment.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = false;
            valueListItem1.DisplayText = "Chưa thanh toán";
            valueListItem2.DataValue = true;
            valueListItem2.DisplayText = "Thanh toán ngay";
            this.optPayment.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.optPayment.Location = new System.Drawing.Point(463, 39);
            this.optPayment.Name = "optPayment";
            this.optPayment.ReadOnly = false;
            this.optPayment.Size = new System.Drawing.Size(210, 19);
            this.optPayment.TabIndex = 43;
            // 
            // palTopVouchers
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance2;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.Location = new System.Drawing.Point(10, 26);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(403, 48);
            this.palTopVouchers.TabIndex = 37;
            // 
            // cbbAccountingObjectBankAccount
            // 
            appearance4.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccount.Appearance = appearance4;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Appearance = appearance5;
            this.cbbAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance12;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance15;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(131, 66);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.NullText = "<Chọn đối tượng cha>";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 80;
            // 
            // cbbBankAccountDetailID
            // 
            appearance17.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailID.Appearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccountDetailID.DisplayLayout.Appearance = appearance18;
            this.cbbBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.cbbBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.cbbBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance25;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance28;
            this.cbbBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.cbbBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccountDetailID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailID.Location = new System.Drawing.Point(131, 21);
            this.cbbBankAccountDetailID.Name = "cbbBankAccountDetailID";
            this.cbbBankAccountDetailID.NullText = "<Chọn đối tượng cha>";
            this.cbbBankAccountDetailID.Size = new System.Drawing.Size(160, 22);
            this.cbbBankAccountDetailID.TabIndex = 38;
            // 
            // palInfo
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            this.palInfo.Appearance = appearance30;
            // 
            // palInfo.ClientArea
            // 
            this.palInfo.ClientArea.Controls.Add(this.grAccounting);
            this.palInfo.ClientArea.Controls.Add(this.grAccountCreaditCard);
            this.palInfo.ClientArea.Controls.Add(this.grPayment);
            this.palInfo.ClientArea.Controls.Add(this.grAccount);
            this.palInfo.ClientArea.Controls.Add(this.paneTTC);
            this.palInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.palInfo.Location = new System.Drawing.Point(0, 0);
            this.palInfo.Name = "palInfo";
            this.palInfo.Size = new System.Drawing.Size(856, 222);
            this.palInfo.TabIndex = 39;
            // 
            // grAccounting
            // 
            this.grAccounting.Controls.Add(this.btnLiabilities);
            this.grAccounting.Controls.Add(this.lblReceiver);
            this.grAccounting.Controls.Add(this.txtReceiver);
            this.grAccounting.Controls.Add(this.txtIssueBy);
            this.grAccounting.Controls.Add(this.lblIssueBy);
            this.grAccounting.Controls.Add(this.txtIdentificationNo);
            this.grAccounting.Controls.Add(this.dteIssueDate);
            this.grAccounting.Controls.Add(this.lblIssuDate);
            this.grAccounting.Controls.Add(this.lblIdentificationNo);
            this.grAccounting.Controls.Add(this.txtAccountingObjtBankName);
            this.grAccounting.Controls.Add(this.lblAccountingObjectBankAccount);
            this.grAccounting.Controls.Add(this.cbbAccountingObjectID);
            this.grAccounting.Controls.Add(this.txtAccountingObjectAddress);
            this.grAccounting.Controls.Add(this.txtAccountingObjectName);
            this.grAccounting.Controls.Add(this.cbbAccountingObjBank);
            this.grAccounting.Controls.Add(this.lblAccountingObjectAddress);
            this.grAccounting.Controls.Add(this.lblAccountingObject);
            this.grAccounting.Dock = System.Windows.Forms.DockStyle.Top;
            appearance40.FontData.BoldAsString = "True";
            appearance40.FontData.SizeInPoints = 10F;
            this.grAccounting.HeaderAppearance = appearance40;
            this.grAccounting.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grAccounting.Location = new System.Drawing.Point(0, 450);
            this.grAccounting.Name = "grAccounting";
            this.grAccounting.Size = new System.Drawing.Size(856, 107);
            this.grAccounting.TabIndex = 44;
            this.grAccounting.Text = "Đơn vị nhận tiền";
            this.grAccounting.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnLiabilities
            // 
            this.btnLiabilities.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLiabilities.Location = new System.Drawing.Point(752, 25);
            this.btnLiabilities.Name = "btnLiabilities";
            this.btnLiabilities.Size = new System.Drawing.Size(92, 21);
            this.btnLiabilities.TabIndex = 43;
            this.btnLiabilities.Text = "Công nợ";
            // 
            // lblReceiver
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextHAlignAsString = "Left";
            appearance31.TextVAlignAsString = "Middle";
            this.lblReceiver.Appearance = appearance31;
            this.lblReceiver.Location = new System.Drawing.Point(10, 49);
            this.lblReceiver.Name = "lblReceiver";
            this.lblReceiver.Size = new System.Drawing.Size(83, 22);
            this.lblReceiver.TabIndex = 42;
            this.lblReceiver.Text = "Người lĩnh tiền";
            this.lblReceiver.Visible = false;
            // 
            // txtReceiver
            // 
            this.txtReceiver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiver.AutoSize = false;
            this.txtReceiver.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReceiver.Location = new System.Drawing.Point(100, 49);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(744, 22);
            this.txtReceiver.TabIndex = 41;
            this.txtReceiver.Visible = false;
            // 
            // txtIssueBy
            // 
            this.txtIssueBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIssueBy.AutoSize = false;
            this.txtIssueBy.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtIssueBy.Location = new System.Drawing.Point(493, 73);
            this.txtIssueBy.Name = "txtIssueBy";
            this.txtIssueBy.Size = new System.Drawing.Size(351, 22);
            this.txtIssueBy.TabIndex = 39;
            this.txtIssueBy.Visible = false;
            // 
            // lblIssueBy
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Middle";
            this.lblIssueBy.Appearance = appearance32;
            this.lblIssueBy.Location = new System.Drawing.Point(446, 73);
            this.lblIssueBy.Name = "lblIssueBy";
            this.lblIssueBy.Size = new System.Drawing.Size(54, 22);
            this.lblIssueBy.TabIndex = 38;
            this.lblIssueBy.Text = "Nơi cấp";
            this.lblIssueBy.Visible = false;
            // 
            // txtIdentificationNo
            // 
            this.txtIdentificationNo.AutoSize = false;
            this.txtIdentificationNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtIdentificationNo.Location = new System.Drawing.Point(100, 73);
            this.txtIdentificationNo.Name = "txtIdentificationNo";
            this.txtIdentificationNo.Size = new System.Drawing.Size(174, 22);
            this.txtIdentificationNo.TabIndex = 35;
            this.txtIdentificationNo.Visible = false;
            // 
            // dteIssueDate
            // 
            appearance33.TextHAlignAsString = "Center";
            appearance33.TextVAlignAsString = "Middle";
            this.dteIssueDate.Appearance = appearance33;
            this.dteIssueDate.AutoSize = false;
            this.dteIssueDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteIssueDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteIssueDate.Location = new System.Drawing.Point(345, 73);
            this.dteIssueDate.MaskInput = "dd/mm/yyyy";
            this.dteIssueDate.Name = "dteIssueDate";
            this.dteIssueDate.Size = new System.Drawing.Size(98, 22);
            this.dteIssueDate.TabIndex = 34;
            this.dteIssueDate.Value = null;
            this.dteIssueDate.Visible = false;
            // 
            // lblIssuDate
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextHAlignAsString = "Left";
            appearance34.TextVAlignAsString = "Middle";
            this.lblIssuDate.Appearance = appearance34;
            this.lblIssuDate.Location = new System.Drawing.Point(280, 73);
            this.lblIssuDate.Name = "lblIssuDate";
            this.lblIssuDate.Size = new System.Drawing.Size(63, 22);
            this.lblIssuDate.TabIndex = 37;
            this.lblIssuDate.Text = "Ngày cấp";
            this.lblIssuDate.Visible = false;
            // 
            // lblIdentificationNo
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextHAlignAsString = "Left";
            appearance35.TextVAlignAsString = "Middle";
            this.lblIdentificationNo.Appearance = appearance35;
            this.lblIdentificationNo.Location = new System.Drawing.Point(10, 73);
            this.lblIdentificationNo.Name = "lblIdentificationNo";
            this.lblIdentificationNo.Size = new System.Drawing.Size(83, 22);
            this.lblIdentificationNo.TabIndex = 36;
            this.lblIdentificationNo.Text = "Số CMND";
            this.lblIdentificationNo.Visible = false;
            // 
            // txtAccountingObjtBankName
            // 
            this.txtAccountingObjtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjtBankName.AutoSize = false;
            this.txtAccountingObjtBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjtBankName.Location = new System.Drawing.Point(280, 73);
            this.txtAccountingObjtBankName.Name = "txtAccountingObjtBankName";
            this.txtAccountingObjtBankName.Size = new System.Drawing.Size(564, 22);
            this.txtAccountingObjtBankName.TabIndex = 33;
            this.txtAccountingObjtBankName.Visible = false;
            // 
            // lblAccountingObjectBankAccount
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextHAlignAsString = "Left";
            appearance36.TextVAlignAsString = "Middle";
            this.lblAccountingObjectBankAccount.Appearance = appearance36;
            this.lblAccountingObjectBankAccount.Location = new System.Drawing.Point(10, 73);
            this.lblAccountingObjectBankAccount.Name = "lblAccountingObjectBankAccount";
            this.lblAccountingObjectBankAccount.Size = new System.Drawing.Size(83, 22);
            this.lblAccountingObjectBankAccount.TabIndex = 33;
            this.lblAccountingObjectBankAccount.Text = "Tài khoản";
            this.lblAccountingObjectBankAccount.Visible = false;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            editorButton1.AccessibleName = "btnNewAccountingObject";
            appearance37.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance37;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            editorButton1.Key = "btnNewAccountingObject";
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(100, 25);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(174, 22);
            this.cbbAccountingObjectID.TabIndex = 31;
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(100, 49);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(744, 22);
            this.txtAccountingObjectAddress.TabIndex = 25;
            this.txtAccountingObjectAddress.Visible = false;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(280, 25);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(464, 22);
            this.txtAccountingObjectName.TabIndex = 23;
            // 
            // cbbAccountingObjBank
            // 
            this.cbbAccountingObjBank.AutoSize = false;
            this.cbbAccountingObjBank.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjBank.Location = new System.Drawing.Point(100, 73);
            this.cbbAccountingObjBank.Name = "cbbAccountingObjBank";
            this.cbbAccountingObjBank.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjBank.Size = new System.Drawing.Size(174, 22);
            this.cbbAccountingObjBank.TabIndex = 40;
            this.cbbAccountingObjBank.Visible = false;
            // 
            // lblAccountingObjectAddress
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Left";
            appearance38.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance38;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(10, 49);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(83, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            this.lblAccountingObjectAddress.Visible = false;
            // 
            // lblAccountingObject
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance39;
            this.lblAccountingObject.Location = new System.Drawing.Point(10, 25);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(83, 22);
            this.lblAccountingObject.TabIndex = 0;
            this.lblAccountingObject.Text = "Nhà cung cấp";
            // 
            // grAccountCreaditCard
            // 
            this.grAccountCreaditCard.Controls.Add(this.txtCreditCardType);
            this.grAccountCreaditCard.Controls.Add(this.picCreditCardType);
            this.grAccountCreaditCard.Controls.Add(this.cbbCreditCardNumber);
            this.grAccountCreaditCard.Controls.Add(this.txtOwnerCard);
            this.grAccountCreaditCard.Controls.Add(this.lblOwnerCard);
            this.grAccountCreaditCard.Controls.Add(this.lblCreditCardNumber);
            this.grAccountCreaditCard.Controls.Add(this.txtReasonCreaditCard);
            this.grAccountCreaditCard.Controls.Add(this.ultraLabel11);
            this.grAccountCreaditCard.Dock = System.Windows.Forms.DockStyle.Top;
            appearance47.FontData.BoldAsString = "True";
            appearance47.FontData.SizeInPoints = 10F;
            this.grAccountCreaditCard.HeaderAppearance = appearance47;
            this.grAccountCreaditCard.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grAccountCreaditCard.Location = new System.Drawing.Point(0, 369);
            this.grAccountCreaditCard.Name = "grAccountCreaditCard";
            this.grAccountCreaditCard.Size = new System.Drawing.Size(856, 81);
            this.grAccountCreaditCard.TabIndex = 43;
            this.grAccountCreaditCard.Text = "Đơn vị trả tiền";
            this.grAccountCreaditCard.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCreditCardType
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Center";
            appearance41.TextVAlignAsString = "Middle";
            this.txtCreditCardType.Appearance = appearance41;
            this.txtCreditCardType.Location = new System.Drawing.Point(336, 25);
            this.txtCreditCardType.Name = "txtCreditCardType";
            this.txtCreditCardType.Size = new System.Drawing.Size(135, 22);
            this.txtCreditCardType.TabIndex = 37;
            // 
            // picCreditCardType
            // 
            appearance42.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance42.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance42.TextHAlignAsString = "Center";
            appearance42.TextVAlignAsString = "Middle";
            this.picCreditCardType.Appearance = appearance42;
            this.picCreditCardType.BackColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderShadowColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.picCreditCardType.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.picCreditCardType.Location = new System.Drawing.Point(285, 25);
            this.picCreditCardType.Name = "picCreditCardType";
            this.picCreditCardType.Size = new System.Drawing.Size(43, 22);
            this.picCreditCardType.TabIndex = 36;
            // 
            // cbbCreditCardNumber
            // 
            this.cbbCreditCardNumber.AutoSize = false;
            appearance43.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance43;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCreditCardNumber.ButtonsRight.Add(editorButton2);
            this.cbbCreditCardNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCreditCardNumber.Location = new System.Drawing.Point(100, 25);
            this.cbbCreditCardNumber.Name = "cbbCreditCardNumber";
            this.cbbCreditCardNumber.NullText = "<chọn dữ liệu>";
            this.cbbCreditCardNumber.Size = new System.Drawing.Size(175, 22);
            this.cbbCreditCardNumber.TabIndex = 35;
            // 
            // txtOwnerCard
            // 
            this.txtOwnerCard.BackColor = System.Drawing.Color.Transparent;
            this.txtOwnerCard.Location = new System.Drawing.Point(530, 25);
            this.txtOwnerCard.Name = "txtOwnerCard";
            this.txtOwnerCard.Size = new System.Drawing.Size(227, 22);
            this.txtOwnerCard.TabIndex = 34;
            this.txtOwnerCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOwnerCard
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.lblOwnerCard.Appearance = appearance44;
            this.lblOwnerCard.Location = new System.Drawing.Point(477, 25);
            this.lblOwnerCard.Name = "lblOwnerCard";
            this.lblOwnerCard.Size = new System.Drawing.Size(47, 22);
            this.lblOwnerCard.TabIndex = 33;
            this.lblOwnerCard.Text = "Chủ thẻ:";
            // 
            // lblCreditCardNumber
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.lblCreditCardNumber.Appearance = appearance45;
            this.lblCreditCardNumber.Location = new System.Drawing.Point(10, 25);
            this.lblCreditCardNumber.Name = "lblCreditCardNumber";
            this.lblCreditCardNumber.Size = new System.Drawing.Size(68, 22);
            this.lblCreditCardNumber.TabIndex = 0;
            this.lblCreditCardNumber.Text = "Số thẻ";
            // 
            // txtReasonCreaditCard
            // 
            this.txtReasonCreaditCard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonCreaditCard.AutoSize = false;
            this.txtReasonCreaditCard.Location = new System.Drawing.Point(100, 49);
            this.txtReasonCreaditCard.Name = "txtReasonCreaditCard";
            this.txtReasonCreaditCard.Size = new System.Drawing.Size(743, 22);
            this.txtReasonCreaditCard.TabIndex = 25;
            // 
            // ultraLabel11
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance46;
            this.ultraLabel11.Location = new System.Drawing.Point(10, 49);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(67, 22);
            this.ultraLabel11.TabIndex = 24;
            this.ultraLabel11.Text = "Nội dung TT";
            // 
            // grPayment
            // 
            this.grPayment.Controls.Add(this.btnLiabilitiesPM);
            this.grPayment.Controls.Add(this.txtAccountingObjectNamePM);
            this.grPayment.Controls.Add(this.ultraLabel6);
            this.grPayment.Controls.Add(this.cbbAccountingObjectIDPM);
            this.grPayment.Controls.Add(this.txtNumberAttach);
            this.grPayment.Controls.Add(this.lblNumberAttach);
            this.grPayment.Controls.Add(this.txtReasonPM);
            this.grPayment.Controls.Add(this.ultraLabel7);
            this.grPayment.Controls.Add(this.txtReceiverPM);
            this.grPayment.Controls.Add(this.ultraLabel8);
            this.grPayment.Controls.Add(this.txtAccountingObjectAddressPM);
            this.grPayment.Controls.Add(this.ultraLabel10);
            this.grPayment.Controls.Add(this.lblAccountingObjectID);
            this.grPayment.Dock = System.Windows.Forms.DockStyle.Top;
            appearance55.FontData.BoldAsString = "True";
            appearance55.FontData.SizeInPoints = 10F;
            this.grPayment.HeaderAppearance = appearance55;
            this.grPayment.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grPayment.Location = new System.Drawing.Point(0, 217);
            this.grPayment.Name = "grPayment";
            this.grPayment.Size = new System.Drawing.Size(856, 152);
            this.grPayment.TabIndex = 42;
            this.grPayment.Text = "Thông tin chung";
            this.grPayment.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnLiabilitiesPM
            // 
            this.btnLiabilitiesPM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLiabilitiesPM.Location = new System.Drawing.Point(750, 26);
            this.btnLiabilitiesPM.Name = "btnLiabilitiesPM";
            this.btnLiabilitiesPM.Size = new System.Drawing.Size(92, 21);
            this.btnLiabilitiesPM.TabIndex = 34;
            this.btnLiabilitiesPM.Text = "Công nợ";
            // 
            // txtAccountingObjectNamePM
            // 
            this.txtAccountingObjectNamePM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePM.AutoSize = false;
            this.txtAccountingObjectNamePM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNamePM.Location = new System.Drawing.Point(280, 26);
            this.txtAccountingObjectNamePM.Name = "txtAccountingObjectNamePM";
            this.txtAccountingObjectNamePM.Size = new System.Drawing.Size(464, 22);
            this.txtAccountingObjectNamePM.TabIndex = 3;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextHAlignAsString = "Center";
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance48;
            this.ultraLabel6.Location = new System.Drawing.Point(752, 118);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(92, 22);
            this.ultraLabel6.TabIndex = 33;
            this.ultraLabel6.Text = "Chứng từ gốc";
            // 
            // cbbAccountingObjectIDPM
            // 
            this.cbbAccountingObjectIDPM.AutoSize = false;
            appearance49.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton3.Appearance = appearance49;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDPM.ButtonsRight.Add(editorButton3);
            this.cbbAccountingObjectIDPM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectIDPM.Location = new System.Drawing.Point(100, 26);
            this.cbbAccountingObjectIDPM.Name = "cbbAccountingObjectIDPM";
            this.cbbAccountingObjectIDPM.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDPM.Size = new System.Drawing.Size(175, 22);
            this.cbbAccountingObjectIDPM.TabIndex = 2;
            // 
            // txtNumberAttach
            // 
            this.txtNumberAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumberAttach.AutoSize = false;
            this.txtNumberAttach.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtNumberAttach.Location = new System.Drawing.Point(100, 118);
            this.txtNumberAttach.Name = "txtNumberAttach";
            this.txtNumberAttach.Size = new System.Drawing.Size(644, 22);
            this.txtNumberAttach.TabIndex = 7;
            // 
            // lblNumberAttach
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.lblNumberAttach.Appearance = appearance50;
            this.lblNumberAttach.Location = new System.Drawing.Point(10, 118);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(68, 22);
            this.lblNumberAttach.TabIndex = 28;
            this.lblNumberAttach.Text = "Kèm theo";
            // 
            // txtReasonPM
            // 
            this.txtReasonPM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonPM.AutoSize = false;
            this.txtReasonPM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonPM.Location = new System.Drawing.Point(100, 95);
            this.txtReasonPM.Name = "txtReasonPM";
            this.txtReasonPM.Size = new System.Drawing.Size(743, 22);
            this.txtReasonPM.TabIndex = 6;
            // 
            // ultraLabel7
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance51;
            this.ultraLabel7.Location = new System.Drawing.Point(10, 95);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel7.TabIndex = 26;
            this.ultraLabel7.Text = "Lý do chi";
            // 
            // txtReceiverPM
            // 
            this.txtReceiverPM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiverPM.AutoSize = false;
            this.txtReceiverPM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReceiverPM.Location = new System.Drawing.Point(100, 72);
            this.txtReceiverPM.Name = "txtReceiverPM";
            this.txtReceiverPM.Size = new System.Drawing.Size(743, 22);
            this.txtReceiverPM.TabIndex = 5;
            // 
            // ultraLabel8
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance52;
            this.ultraLabel8.Location = new System.Drawing.Point(10, 72);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel8.TabIndex = 24;
            this.ultraLabel8.Text = "Người nhận";
            // 
            // txtAccountingObjectAddressPM
            // 
            this.txtAccountingObjectAddressPM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPM.AutoSize = false;
            this.txtAccountingObjectAddressPM.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressPM.Location = new System.Drawing.Point(100, 49);
            this.txtAccountingObjectAddressPM.Name = "txtAccountingObjectAddressPM";
            this.txtAccountingObjectAddressPM.Size = new System.Drawing.Size(743, 22);
            this.txtAccountingObjectAddressPM.TabIndex = 4;
            // 
            // ultraLabel10
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance53;
            this.ultraLabel10.Location = new System.Drawing.Point(10, 49);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel10.TabIndex = 22;
            this.ultraLabel10.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance54;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(10, 26);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(68, 22);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Đối tượng (*)";
            // 
            // grAccount
            // 
            this.grAccount.Controls.Add(this.cbbBankAccount);
            this.grAccount.Controls.Add(this.txtReason);
            this.grAccount.Controls.Add(this.lblReason);
            this.grAccount.Controls.Add(this.txtBankName);
            this.grAccount.Controls.Add(this.lblBankAccount);
            this.grAccount.Dock = System.Windows.Forms.DockStyle.Top;
            appearance59.FontData.BoldAsString = "True";
            appearance59.FontData.SizeInPoints = 10F;
            this.grAccount.HeaderAppearance = appearance59;
            this.grAccount.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grAccount.Location = new System.Drawing.Point(0, 133);
            this.grAccount.Name = "grAccount";
            this.grAccount.Size = new System.Drawing.Size(856, 84);
            this.grAccount.TabIndex = 41;
            this.grAccount.Text = "Đơn vị trả tiền";
            this.grAccount.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbBankAccount
            // 
            this.cbbBankAccount.AutoSize = false;
            editorButton4.AccessibleName = "btnNewBankAccount";
            appearance56.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton4.Appearance = appearance56;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            editorButton4.Key = "btnNewBankAccount";
            this.cbbBankAccount.ButtonsRight.Add(editorButton4);
            this.cbbBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbBankAccount.Location = new System.Drawing.Point(100, 25);
            this.cbbBankAccount.Name = "cbbBankAccount";
            this.cbbBankAccount.NullText = "<chọn dữ liệu>";
            this.cbbBankAccount.Size = new System.Drawing.Size(174, 22);
            this.cbbBankAccount.TabIndex = 31;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(100, 49);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(744, 22);
            this.txtReason.TabIndex = 25;
            // 
            // lblReason
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance57;
            this.lblReason.Location = new System.Drawing.Point(10, 50);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(83, 21);
            this.lblReason.TabIndex = 24;
            this.lblReason.Text = "Nội dung TT";
            // 
            // txtBankName
            // 
            this.txtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankName.AutoSize = false;
            this.txtBankName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtBankName.Location = new System.Drawing.Point(280, 25);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(564, 22);
            this.txtBankName.TabIndex = 23;
            // 
            // lblBankAccount
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            appearance58.TextHAlignAsString = "Left";
            appearance58.TextVAlignAsString = "Middle";
            this.lblBankAccount.Appearance = appearance58;
            this.lblBankAccount.Location = new System.Drawing.Point(10, 25);
            this.lblBankAccount.Name = "lblBankAccount";
            this.lblBankAccount.Size = new System.Drawing.Size(83, 22);
            this.lblBankAccount.TabIndex = 0;
            this.lblBankAccount.Text = "Tài khoản (*)";
            // 
            // paneTTC
            // 
            // 
            // paneTTC.ClientArea
            // 
            this.paneTTC.ClientArea.Controls.Add(this.grHoaDon);
            this.paneTTC.ClientArea.Controls.Add(this.grBuyNotPay);
            this.paneTTC.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneTTC.Location = new System.Drawing.Point(0, 0);
            this.paneTTC.Name = "paneTTC";
            this.paneTTC.Size = new System.Drawing.Size(856, 133);
            this.paneTTC.TabIndex = 40;
            // 
            // grHoaDon
            // 
            this.grHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon.Controls.Add(this.txtMauSoHD);
            this.grHoaDon.Controls.Add(this.txtSoHD);
            this.grHoaDon.Controls.Add(this.txtKyHieuHD);
            this.grHoaDon.Controls.Add(this.ultraLabel37);
            this.grHoaDon.Controls.Add(this.ultraLabel38);
            this.grHoaDon.Controls.Add(this.ultraLabel39);
            this.grHoaDon.Controls.Add(this.ultraLabel40);
            this.grHoaDon.Controls.Add(this.dteNgayHD);
            this.grHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon.Location = new System.Drawing.Point(539, -1);
            this.grHoaDon.Name = "grHoaDon";
            this.grHoaDon.Size = new System.Drawing.Size(316, 132);
            this.grHoaDon.TabIndex = 39;
            this.grHoaDon.Text = "Hóa đơn";
            this.grHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD
            // 
            this.txtMauSoHD.AutoSize = false;
            this.txtMauSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD.Location = new System.Drawing.Point(81, 21);
            this.txtMauSoHD.Name = "txtMauSoHD";
            this.txtMauSoHD.Size = new System.Drawing.Size(228, 22);
            this.txtMauSoHD.TabIndex = 76;
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(81, 72);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(228, 22);
            this.txtSoHD.TabIndex = 18;
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(81, 46);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(228, 22);
            this.txtKyHieuHD.TabIndex = 17;
            // 
            // ultraLabel37
            // 
            this.ultraLabel37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance60;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(13, 99);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel37.TabIndex = 69;
            this.ultraLabel37.Text = "Ngày HĐ";
            // 
            // ultraLabel38
            // 
            this.ultraLabel38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance61;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(11, 73);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel38.TabIndex = 68;
            this.ultraLabel38.Text = "Số HĐ";
            // 
            // ultraLabel39
            // 
            this.ultraLabel39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance62.BackColor = System.Drawing.Color.Transparent;
            appearance62.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance62;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(11, 48);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel39.TabIndex = 67;
            this.ultraLabel39.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance63.BackColor = System.Drawing.Color.Transparent;
            appearance63.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance63;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(13, 24);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 66;
            this.ultraLabel40.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD
            // 
            appearance64.TextHAlignAsString = "Center";
            appearance64.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance64;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(81, 99);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            // 
            // grBuyNotPay
            // 
            this.grBuyNotPay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grBuyNotPay.Controls.Add(this.btnLiabilitiesBuy);
            this.grBuyNotPay.Controls.Add(this.txtAccountingObjectNameBuy);
            this.grBuyNotPay.Controls.Add(this.cbbAccountingObjectIDBuy);
            this.grBuyNotPay.Controls.Add(this.txtReasonBuy);
            this.grBuyNotPay.Controls.Add(this.ultraLabel25);
            this.grBuyNotPay.Controls.Add(this.txtCompanyTaxCode);
            this.grBuyNotPay.Controls.Add(this.ultraLabel26);
            this.grBuyNotPay.Controls.Add(this.txtAccountingObjectAddressBuy);
            this.grBuyNotPay.Controls.Add(this.ultraLabel27);
            this.grBuyNotPay.Controls.Add(this.ultraLabel28);
            appearance70.FontData.BoldAsString = "True";
            appearance70.FontData.SizeInPoints = 10F;
            this.grBuyNotPay.HeaderAppearance = appearance70;
            this.grBuyNotPay.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.grBuyNotPay.Location = new System.Drawing.Point(3, 0);
            this.grBuyNotPay.Name = "grBuyNotPay";
            this.grBuyNotPay.Size = new System.Drawing.Size(537, 132);
            this.grBuyNotPay.TabIndex = 37;
            this.grBuyNotPay.Text = "Thông tin chung";
            this.grBuyNotPay.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnLiabilitiesBuy
            // 
            this.btnLiabilitiesBuy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLiabilitiesBuy.Location = new System.Drawing.Point(437, 27);
            this.btnLiabilitiesBuy.Name = "btnLiabilitiesBuy";
            this.btnLiabilitiesBuy.Size = new System.Drawing.Size(92, 21);
            this.btnLiabilitiesBuy.TabIndex = 35;
            this.btnLiabilitiesBuy.Text = "Công nợ";
            // 
            // txtAccountingObjectNameBuy
            // 
            this.txtAccountingObjectNameBuy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameBuy.AutoSize = false;
            this.txtAccountingObjectNameBuy.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNameBuy.Location = new System.Drawing.Point(280, 26);
            this.txtAccountingObjectNameBuy.Name = "txtAccountingObjectNameBuy";
            this.txtAccountingObjectNameBuy.Size = new System.Drawing.Size(151, 22);
            this.txtAccountingObjectNameBuy.TabIndex = 3;
            // 
            // cbbAccountingObjectIDBuy
            // 
            this.cbbAccountingObjectIDBuy.AutoSize = false;
            appearance65.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton5.Appearance = appearance65;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDBuy.ButtonsRight.Add(editorButton5);
            this.cbbAccountingObjectIDBuy.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectIDBuy.Location = new System.Drawing.Point(100, 26);
            this.cbbAccountingObjectIDBuy.Name = "cbbAccountingObjectIDBuy";
            this.cbbAccountingObjectIDBuy.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDBuy.Size = new System.Drawing.Size(174, 22);
            this.cbbAccountingObjectIDBuy.TabIndex = 2;
            // 
            // txtReasonBuy
            // 
            this.txtReasonBuy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonBuy.AutoSize = false;
            this.txtReasonBuy.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonBuy.Location = new System.Drawing.Point(100, 95);
            this.txtReasonBuy.Name = "txtReasonBuy";
            this.txtReasonBuy.Size = new System.Drawing.Size(430, 22);
            this.txtReasonBuy.TabIndex = 6;
            // 
            // ultraLabel25
            // 
            appearance66.BackColor = System.Drawing.Color.Transparent;
            appearance66.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance66;
            this.ultraLabel25.Location = new System.Drawing.Point(10, 95);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel25.TabIndex = 26;
            this.ultraLabel25.Text = "Diễn giải";
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyTaxCode.AutoSize = false;
            this.txtCompanyTaxCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(100, 72);
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(430, 22);
            this.txtCompanyTaxCode.TabIndex = 5;
            // 
            // ultraLabel26
            // 
            appearance67.BackColor = System.Drawing.Color.Transparent;
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance67;
            this.ultraLabel26.Location = new System.Drawing.Point(10, 72);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel26.TabIndex = 24;
            this.ultraLabel26.Text = "&Mã số thuế";
            // 
            // txtAccountingObjectAddressBuy
            // 
            this.txtAccountingObjectAddressBuy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressBuy.AutoSize = false;
            this.txtAccountingObjectAddressBuy.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressBuy.Location = new System.Drawing.Point(100, 49);
            this.txtAccountingObjectAddressBuy.Name = "txtAccountingObjectAddressBuy";
            this.txtAccountingObjectAddressBuy.Size = new System.Drawing.Size(430, 22);
            this.txtAccountingObjectAddressBuy.TabIndex = 4;
            // 
            // ultraLabel27
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance68;
            this.ultraLabel27.Location = new System.Drawing.Point(10, 49);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel27.TabIndex = 22;
            this.ultraLabel27.Text = "Địa chỉ";
            // 
            // ultraLabel28
            // 
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance69;
            this.ultraLabel28.Location = new System.Drawing.Point(10, 26);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel28.TabIndex = 0;
            this.ultraLabel28.Text = "Đối tượng (*)";
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.pltab_chitiet);
            this.ultraPanel4.ClientArea.Controls.Add(this.ultraSplitter1);
            this.ultraPanel4.ClientArea.Controls.Add(this.palInfo);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel4.Location = new System.Drawing.Point(0, 79);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(856, 523);
            this.ultraPanel4.TabIndex = 3;
            // 
            // pltab_chitiet
            // 
            this.pltab_chitiet.AutoSize = true;
            // 
            // pltab_chitiet.ClientArea
            // 
            this.pltab_chitiet.ClientArea.Controls.Add(this.palGrid);
            this.pltab_chitiet.ClientArea.Controls.Add(this.ultraPanel1);
            this.pltab_chitiet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pltab_chitiet.Location = new System.Drawing.Point(0, 234);
            this.pltab_chitiet.Name = "pltab_chitiet";
            this.pltab_chitiet.Size = new System.Drawing.Size(856, 289);
            this.pltab_chitiet.TabIndex = 5;
            // 
            // palGrid
            // 
            this.palGrid.AutoSize = true;
            // 
            // palGrid.ClientArea
            // 
            this.palGrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palGrid.ClientArea.Controls.Add(this.btnApportion);
            this.palGrid.ClientArea.Controls.Add(this.btnApportionTaxes);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 0);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(856, 125);
            this.palGrid.TabIndex = 7;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance71.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance71;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(758, 3);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 10;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // btnApportion
            // 
            this.btnApportion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApportion.Location = new System.Drawing.Point(637, 3);
            this.btnApportion.Name = "btnApportion";
            this.btnApportion.Size = new System.Drawing.Size(115, 22);
            this.btnApportion.TabIndex = 9;
            this.btnApportion.Text = "&Phân bổ chi phí";
            this.btnApportion.Click += new System.EventHandler(this.btnApportion_Click);
            // 
            // btnApportionTaxes
            // 
            this.btnApportionTaxes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance72.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnApportionTaxes.HotTrackAppearance = appearance72;
            this.btnApportionTaxes.Location = new System.Drawing.Point(424, 3);
            this.btnApportionTaxes.Name = "btnApportionTaxes";
            this.btnApportionTaxes.Size = new System.Drawing.Size(207, 22);
            this.btnApportionTaxes.TabIndex = 8;
            this.btnApportionTaxes.Text = "Phân &bổ chi phí trước hải quan";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel2);
            this.ultraPanel1.ClientArea.Controls.Add(this.palBot);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 125);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(856, 164);
            this.ultraPanel1.TabIndex = 6;
            // 
            // ultraPanel2
            // 
            appearance73.TextHAlignAsString = "Left";
            appearance73.TextVAlignAsString = "Middle";
            this.ultraPanel2.Appearance = appearance73;
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalInwardAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel3);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalSpecialConsumeTaxAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel4);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalImportTaxAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmountOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAllOriginal);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalDiscountAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalVATAmount);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel13);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblTotalAll);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel15);
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraLabel16);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(856, 107);
            this.ultraPanel2.TabIndex = 25;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance74.TextHAlignAsString = "Left";
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance74;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(147, 39);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Tiền chiết khấu";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance75.TextHAlignAsString = "Left";
            appearance75.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance75;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(147, 21);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Giá mua";
            // 
            // lblTotalInwardAmount
            // 
            this.lblTotalInwardAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance76.TextHAlignAsString = "Right";
            appearance76.TextVAlignAsString = "Middle";
            this.lblTotalInwardAmount.Appearance = appearance76;
            this.lblTotalInwardAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalInwardAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalInwardAmount.Location = new System.Drawing.Point(689, 74);
            this.lblTotalInwardAmount.Name = "lblTotalInwardAmount";
            this.lblTotalInwardAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalInwardAmount.TabIndex = 23;
            this.lblTotalInwardAmount.Text = "0";
            this.lblTotalInwardAmount.WrapText = false;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance77.TextHAlignAsString = "Left";
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance77;
            this.ultraLabel3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(541, 21);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Tiền thuế GTGT";
            // 
            // lblTotalSpecialConsumeTaxAmount
            // 
            this.lblTotalSpecialConsumeTaxAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance78.TextHAlignAsString = "Right";
            appearance78.TextVAlignAsString = "Middle";
            this.lblTotalSpecialConsumeTaxAmount.Appearance = appearance78;
            this.lblTotalSpecialConsumeTaxAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalSpecialConsumeTaxAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSpecialConsumeTaxAmount.Location = new System.Drawing.Point(743, 56);
            this.lblTotalSpecialConsumeTaxAmount.Name = "lblTotalSpecialConsumeTaxAmount";
            this.lblTotalSpecialConsumeTaxAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalSpecialConsumeTaxAmount.TabIndex = 21;
            this.lblTotalSpecialConsumeTaxAmount.Text = "0";
            this.lblTotalSpecialConsumeTaxAmount.WrapText = false;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance79.TextHAlignAsString = "Left";
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance79;
            this.ultraLabel4.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel4.Location = new System.Drawing.Point(147, 57);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(131, 17);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalImportTaxAmount
            // 
            this.lblTotalImportTaxAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance80.TextHAlignAsString = "Right";
            appearance80.TextVAlignAsString = "Middle";
            this.lblTotalImportTaxAmount.Appearance = appearance80;
            this.lblTotalImportTaxAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalImportTaxAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalImportTaxAmount.Location = new System.Drawing.Point(743, 38);
            this.lblTotalImportTaxAmount.Name = "lblTotalImportTaxAmount";
            this.lblTotalImportTaxAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalImportTaxAmount.TabIndex = 20;
            this.lblTotalImportTaxAmount.Text = "0";
            this.lblTotalImportTaxAmount.WrapText = false;
            // 
            // lblTotalDiscountAmountOriginal
            // 
            this.lblTotalDiscountAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance81.TextHAlignAsString = "Right";
            appearance81.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmountOriginal.Appearance = appearance81;
            this.lblTotalDiscountAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalDiscountAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDiscountAmountOriginal.Location = new System.Drawing.Point(284, 39);
            this.lblTotalDiscountAmountOriginal.Name = "lblTotalDiscountAmountOriginal";
            this.lblTotalDiscountAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalDiscountAmountOriginal.TabIndex = 5;
            this.lblTotalDiscountAmountOriginal.Text = "0";
            this.lblTotalDiscountAmountOriginal.WrapText = false;
            // 
            // lblTotalAmountOriginal
            // 
            this.lblTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance82.TextHAlignAsString = "Right";
            appearance82.TextVAlignAsString = "Middle";
            this.lblTotalAmountOriginal.Appearance = appearance82;
            this.lblTotalAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmountOriginal.Location = new System.Drawing.Point(284, 21);
            this.lblTotalAmountOriginal.Name = "lblTotalAmountOriginal";
            this.lblTotalAmountOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAmountOriginal.TabIndex = 4;
            this.lblTotalAmountOriginal.Text = "0";
            this.lblTotalAmountOriginal.WrapText = false;
            // 
            // lblTotalAllOriginal
            // 
            this.lblTotalAllOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance83.TextHAlignAsString = "Right";
            appearance83.TextVAlignAsString = "Middle";
            this.lblTotalAllOriginal.Appearance = appearance83;
            this.lblTotalAllOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAllOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAllOriginal.Location = new System.Drawing.Point(284, 57);
            this.lblTotalAllOriginal.Name = "lblTotalAllOriginal";
            this.lblTotalAllOriginal.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAllOriginal.TabIndex = 7;
            this.lblTotalAllOriginal.Text = "0";
            this.lblTotalAllOriginal.WrapText = false;
            // 
            // lblTotalDiscountAmount
            // 
            this.lblTotalDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance84.TextHAlignAsString = "Right";
            appearance84.TextVAlignAsString = "Middle";
            this.lblTotalDiscountAmount.Appearance = appearance84;
            this.lblTotalDiscountAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalDiscountAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDiscountAmount.Location = new System.Drawing.Point(410, 39);
            this.lblTotalDiscountAmount.Name = "lblTotalDiscountAmount";
            this.lblTotalDiscountAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalDiscountAmount.TabIndex = 9;
            this.lblTotalDiscountAmount.Text = "0";
            this.lblTotalDiscountAmount.WrapText = false;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance85.TextHAlignAsString = "Right";
            appearance85.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance85;
            this.lblTotalAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(410, 21);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAmount.TabIndex = 8;
            this.lblTotalAmount.Text = "0";
            this.lblTotalAmount.WrapText = false;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance86.TextHAlignAsString = "Right";
            appearance86.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance86;
            this.lblTotalVATAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(689, 21);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(120, 17);
            this.lblTotalVATAmount.TabIndex = 10;
            this.lblTotalVATAmount.Text = "0";
            this.lblTotalVATAmount.WrapText = false;
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance87.TextHAlignAsString = "Left";
            appearance87.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance87;
            this.ultraLabel13.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel13.Location = new System.Drawing.Point(540, 74);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel13.TabIndex = 15;
            this.ultraLabel13.Text = "Giá nhập kho";
            // 
            // lblTotalAll
            // 
            this.lblTotalAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance88.TextHAlignAsString = "Right";
            appearance88.TextVAlignAsString = "Middle";
            this.lblTotalAll.Appearance = appearance88;
            this.lblTotalAll.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblTotalAll.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAll.Location = new System.Drawing.Point(410, 57);
            this.lblTotalAll.Name = "lblTotalAll";
            this.lblTotalAll.Size = new System.Drawing.Size(120, 17);
            this.lblTotalAll.TabIndex = 11;
            this.lblTotalAll.Text = "0";
            this.lblTotalAll.WrapText = false;
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance89.TextHAlignAsString = "Left";
            appearance89.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance89;
            this.ultraLabel15.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(540, 56);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel15.TabIndex = 13;
            this.ultraLabel15.Text = "Tiền thuế TTĐB";
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance90.TextHAlignAsString = "Left";
            appearance90.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance90;
            this.ultraLabel16.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(540, 38);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(129, 17);
            this.ultraLabel16.TabIndex = 12;
            this.ultraLabel16.Text = "Tiền thuế nhập khẩu";
            // 
            // palBot
            // 
            this.palBot.AutoSize = true;
            this.palBot.Controls.Add(this.uGridControl);
            this.palBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBot.Location = new System.Drawing.Point(0, 107);
            this.palBot.Name = "palBot";
            this.palBot.Size = new System.Drawing.Size(856, 57);
            this.palBot.TabIndex = 28;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(500, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(356, 54);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "uGridControl";
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 222);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 178;
            this.ultraSplitter1.Size = new System.Drawing.Size(856, 12);
            this.ultraSplitter1.TabIndex = 3;
            // 
            // FTIIncrementBuyDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 602);
            this.Controls.Add(this.ultraPanel4);
            this.Controls.Add(this.ultraPanel_KhungTren);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FTIIncrementBuyDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ghi tăng công cụ dụng cụ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FTIIncrementBuyDetail_FormClosing);
            this.ultraPanel_KhungTren.ClientArea.ResumeLayout(false);
            this.ultraPanel_KhungTren.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPayment)).EndInit();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).EndInit();
            this.palInfo.ClientArea.ResumeLayout(false);
            this.palInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grAccounting)).EndInit();
            this.grAccounting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIssueBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentificationNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grAccountCreaditCard)).EndInit();
            this.grAccountCreaditCard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCreditCardNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonCreaditCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grPayment)).EndInit();
            this.grPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiverPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grAccount)).EndInit();
            this.grAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            this.paneTTC.ClientArea.ResumeLayout(false);
            this.paneTTC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).EndInit();
            this.grHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grBuyNotPay)).EndInit();
            this.grBuyNotPay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameBuy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDBuy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonBuy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressBuy)).EndInit();
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ClientArea.PerformLayout();
            this.ultraPanel4.ResumeLayout(false);
            this.pltab_chitiet.ClientArea.ResumeLayout(false);
            this.pltab_chitiet.ClientArea.PerformLayout();
            this.pltab_chitiet.ResumeLayout(false);
            this.pltab_chitiet.PerformLayout();
            this.palGrid.ClientArea.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.palGrid.PerformLayout();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.palBot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel_KhungTren;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        public Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailID;
        private Infragistics.Win.Misc.UltraPanel palInfo;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel pltab_chitiet;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblTotalInwardAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel lblTotalSpecialConsumeTaxAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lblTotalImportTaxAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalAllOriginal;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel lblTotalAll;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private System.Windows.Forms.Panel palBot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel paneTTC;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
        private Infragistics.Win.Misc.UltraGroupBox grBuyNotPay;
        private Infragistics.Win.Misc.UltraButton btnLiabilitiesBuy;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameBuy;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDBuy;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonBuy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressBuy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraGroupBox grAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.Misc.UltraLabel lblBankAccount;
        private Infragistics.Win.Misc.UltraGroupBox grPayment;
        private Infragistics.Win.Misc.UltraButton btnLiabilitiesPM;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePM;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDPM;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttach;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonPM;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiverPM;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPM;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.Misc.UltraGroupBox grAccountCreaditCard;
        private Infragistics.Win.Misc.UltraLabel txtCreditCardType;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox picCreditCardType;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCreditCardNumber;
        private System.Windows.Forms.Label txtOwnerCard;
        private Infragistics.Win.Misc.UltraLabel lblOwnerCard;
        private Infragistics.Win.Misc.UltraLabel lblCreditCardNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonCreaditCard;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraGroupBox grAccounting;
        private Infragistics.Win.Misc.UltraButton btnLiabilities;
        private Infragistics.Win.Misc.UltraLabel lblReceiver;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiver;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIssueBy;
        private Infragistics.Win.Misc.UltraLabel lblIssueBy;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtIdentificationNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteIssueDate;
        private Infragistics.Win.Misc.UltraLabel lblIssuDate;
        private Infragistics.Win.Misc.UltraLabel lblIdentificationNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjtBankName;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectBankAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjBank;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbPayment;
        private UltraOptionSet_Ex optPayment;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private Infragistics.Win.Misc.UltraPanel palGrid;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.Misc.UltraButton btnApportion;
        private Infragistics.Win.Misc.UltraButton btnApportionTaxes;
    }
}