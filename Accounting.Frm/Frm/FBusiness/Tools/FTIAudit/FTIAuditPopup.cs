﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinSchedule;

namespace Accounting
{


    public partial class FTIAuditPopup : CustormForm
    {
        readonly List<TIAudit> _dsTIAudits;
        public TIAudit TIAudit;
        public int _status;
        public FTIAuditPopup(TIAudit temp, List<TIAudit> dsFAAudits, int statusForm)
        {
            TIAudit = temp;
            _dsTIAudits = dsFAAudits;
            _status = statusForm;
            InitializeComponent();

        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            TIAudit.InventoryDate = dtpInventory.DateTime;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }




    }
}
