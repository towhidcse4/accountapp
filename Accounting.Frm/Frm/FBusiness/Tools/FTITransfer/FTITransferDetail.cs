﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class FTITransferDetail : FTITransferDetailBase
    {
        #region khai bao
        public static bool isClose = true;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion
        #region khởi tạo
        public FTITransferDetail(TITransfer temp, List<TITransfer> lstIncrements, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 433;
            }
            else
            {
                _select = temp;
            }
            _listSelects.AddRange(lstIncrements);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion

        }
        #endregion
        #region
        #endregion
        #region sử lý các viewgrid trên form
        #endregion
        #region event

        #endregion
        #region Hàm override
        public override void InitializeGUI(TITransfer input)
        {
            Template mauGiaoDien = Utils.CloneObject(Utils.GetMauGiaoDien(input.TypeID, input.TemplateID));
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add && mauGiaoDien != null ? mauGiaoDien.ID : input.TemplateID;
            input.TemplateID = mauGiaoDien.ID;
            this.ConfigTopVouchersNo<TITransfer>(pnlDateNo, "No", "PostedDate", "Date");
            // add by trungnq
            Control dteP = this.Controls.Find("dtePostedDate", true).First();
            Control lbeP = this.Controls.Find("lblPostedDate", true).First();
            if (dteP != null && lbeP != null)
            {
                dteP.Visible = false;
                lbeP.Visible = false;
            }
            /*---------------------------------------*/
            BindingList<TITransferDetail> dsIncrementDetails = new BindingList<TITransferDetail>();
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
            {
                dsIncrementDetails = new BindingList<TITransferDetail>(input.TITransferDetails);
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            }

            #region cau hinh ban dau cho form

            _listObjectInput = new BindingList<System.Collections.IList> { dsIncrementDetails };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<TITransfer>(pnlUgrid, mauGiaoDien);

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, false };
            this.ConfigGridByManyTemplete<TITransfer>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());


            txtDescription.DataBindings.Clear();
            txtDescription.DataBindings.Add("Text", input, "Reason", true, DataSourceUpdateMode.OnPropertyChanged);
            txtTransferor.DataBindings.Clear();
            txtTransferor.DataBindings.Add("Text", input, "Transferor", true, DataSourceUpdateMode.OnPropertyChanged);
            txtReceiver.DataBindings.Clear();
            txtReceiver.DataBindings.Add("Text", input, "Receiver", true, DataSourceUpdateMode.OnPropertyChanged);
            //_select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            //_select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            //_select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            //_select.IsImportPurchase = _statusForm == ConstFrm.optStatusForm.Add ? false : input.IsImportPurchase;

            //var inputCurrency = new List<TITransfer> { _select };
            //this.ConfigGridCurrencyByTemplate<TITransfer>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency }, uGridControl, mauGiaoDien);
            ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 433)//trungnq thêm vào để  làm task 6234
                {
                    txtDescription.Value = "Điều chuyển CCDC";
                    txtDescription.Text = "Điều chuyển CCDC";
                    _select.Reason = "Điều chuyển CCDC";
                };
            }
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal("VND", datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void ultraGroupBox1_SizeChanged(object sender, EventArgs e)
        {
            // add by Hautv
            int w = ultraGroupBox1.Width - ultraLabel2.Width - ultraLabel3.Width - ultraLabel1.Width - 40;
            txtTransferor.Width = (int)w / 3 - 40;
            txtReceiver.Width = (int)w / 3 - 40;
            txtDescription.Width = (int)w / 3 + 80;

            ultraLabel3.Location = new System.Drawing.Point(txtTransferor.Location.X + txtTransferor.Width + 5, ultraLabel3.Location.Y);
            txtReceiver.Location = new System.Drawing.Point(ultraLabel3.Location.X + ultraLabel3.Width, txtReceiver.Location.Y);
            ultraLabel1.Location = new System.Drawing.Point(txtReceiver.Location.X + txtReceiver.Width + 5, ultraLabel1.Location.Y);
            txtDescription.Location = new System.Drawing.Point(ultraLabel1.Location.X + ultraLabel1.Width, txtDescription.Location.Y);
        }

        private void FTITransferDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class FTITransferDetailBase : DetailBase<TITransfer> { }
}
