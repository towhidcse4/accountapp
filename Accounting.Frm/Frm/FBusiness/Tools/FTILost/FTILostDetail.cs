﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Castle.Windsor.Installer;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class FTILostDetail : FTILostDetailBase
    {
        #region khai bao
        public static bool isClose = true;
        #endregion
        #region khởi tạo
        public FTILostDetail(TILost temp, List<TILost> lstIncrements, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 435;
            }
            else
            {
                _select = temp;
            }
            _listSelects.AddRange(lstIncrements);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, _listSelects, statusForm);
            #endregion

        }
        #endregion
        #region
        #endregion
        #region sử lý các viewgrid trên form
        #endregion
        #region event

        #endregion
        #region Hàm override
        public override void InitializeGUI(TILost input)
        {
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add &&  mauGiaoDien != null ? mauGiaoDien.ID : input.TemplateID;
            input.TemplateID = mauGiaoDien.ID;
            
            BindingList<TILostDetail> dsIncrementDetails = new BindingList<TILostDetail>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
                dsIncrementDetails = new BindingList<TILostDetail>(input.TILostDetails);
            #region cau hinh ban dau cho form

            _listObjectInput = new BindingList<System.Collections.IList> { dsIncrementDetails };

            this.ConfigGridByTemplete_General<TILost>(pnlUgrid, mauGiaoDien);
            this.ConfigGridByTemplete<TILost>(input.TypeID, mauGiaoDien, true, _listObjectInput);
            this.ConfigTopVouchersNo<TILost>(pnlDateNo, "No", String.Empty, "Date");

            txtReason.DataBindings.Clear();
            txtReason.DataBindings.Add("Text", input, "Reason", true, DataSourceUpdateMode.OnPropertyChanged);
            //_select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            //_select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            //_select.IsImportPurchase = _statusForm == ConstFrm.optStatusForm.Add ? false : input.IsImportPurchase;

            var inputCurrency = new List<TILost> { _select };
            this.ConfigGridCurrencyByTemplate<TILost>(_select.TypeID, new BindingList<System.Collections.IList> { inputCurrency }, uGridControl, mauGiaoDien);

            #endregion
        }
        #endregion
    }

    public class FTILostDetailBase : DetailBase<TILost> { }
}
