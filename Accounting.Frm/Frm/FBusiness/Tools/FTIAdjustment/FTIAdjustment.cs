﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{

    public partial class FTIAdjustment : CustormForm
    {
        #region khai báo
        List<TIAdjustment> lstTIAdjustments = new List<TIAdjustment>();
        private List<TIAdjustmentDetail> lstfTIAdjustmentDetails = new List<TIAdjustmentDetail>();
        public ITIAdjustmentService _ITIAdjustmentService;
        //public IFixedAssetLedgerService _IFixedAssetLedgerService;
        //public IFixedAssetService _IFixedAssetService;
        public ITIAdjustmentDetailService _ITIAdjustmentDetailService;
        // ds template
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion
        #region khoi tao
        public FTIAdjustment()
        {
            #region khoi tao gia tri mac dinh cho forms
            InitializeComponent();
            _ITIAdjustmentService = IoC.Resolve<ITIAdjustmentService>();
            //_IFixedAssetLedgerService = IoC.Resolve<IFixedAssetLedgerService>();
            //_IFixedAssetService = IoC.Resolve<IFixedAssetService>();
            _ITIAdjustmentDetailService = IoC.Resolve<ITIAdjustmentDetailService>();

            #endregion
            #region thiet lap ban dau
            ugridDS.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            UgridCT.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region tao du lieu ban dau cho forms
            LoadDuLieu(true);
            ViewGridDs();
            if (ugridDS.Rows.Count > 0) ugridDS.Rows[0].Selected = true;

            ViewGridCt();

            #endregion
        }
        #endregion
        void ViewGridDs()
        {

            Utils.ConfigGrid(ugridDS, ConstDatabase.TIAdjustment_TableName);
            UltraGridBand band = ugridDS.DisplayLayout.Bands[0];
            ugridDS.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            ugridDS.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            //ugridDS.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            ugridDS.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            ugridDS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ugridDS.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            ugridDS.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            //Hiện những dòng trống?
            ugridDS.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            ugridDS.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;


        }

        void ViewGridCt()
        {
            Utils.ConfigGrid(UgridCT, ConstDatabase.TIAdjustmentDetail_TableName);
            UltraGridBand band = UgridCT.DisplayLayout.Bands[0];
            UgridCT.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            UgridCT.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            UgridCT.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            UgridCT.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            UgridCT.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            UgridCT.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            UgridCT.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;
            //Hiện những dòng trống?
            UgridCT.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            UgridCT.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            band.Columns["Amount"].CellAppearance.TextHAlign = HAlign.Right;
            band.Columns["Description"].Width = 100;
            UgridCT.DisplayLayout.Bands[0].Columns["Amount"].Header.VisiblePosition = 11;

        }

        #region button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunciton();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunciton();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunciton();
        }
        private void btnSaveLeg_Click(object sender, EventArgs e)
        {
            Saveleged();
        }

        private void btnRemoveleg_Click(object sender, EventArgs e)
        {
            Removeleged();
        }


        #endregion
        #region tsm
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunciton();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunciton();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunciton();
        }
        #endregion
        #region utils

        void AddFunciton()
        {
            TIAdjustment temp = ugridDS.Selected.Rows.Count <= 0 ? new TIAdjustment() : ugridDS.Selected.Rows[0].ListObject as TIAdjustment;
            new FTIAdjustmentDetail(temp, lstTIAdjustments, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FTIAdjustmentDetail.IsClose) LoadDuLieu();

        }

        void DeleteFunciton()
        {
            if (ugridDS.Selected.Rows.Count > 0)
            {
                TIAdjustment temp = ugridDS.Selected.Rows[0].ListObject as TIAdjustment;
                if (temp != null && MSG.Question(String.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _ITIAdjustmentService.BeginTran();
                    _ITIAdjustmentService.Delete(temp);
                    _ITIAdjustmentService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }

        void EditFunciton()
        {
            if (ugridDS.Selected.Rows.Count > 0)
            {
                TIAdjustment temp = ugridDS.Selected.Rows[0].ListObject as TIAdjustment;
                new FTIAdjustmentDetail(temp, lstTIAdjustments, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!DetailBase<TIAdjustment>.IsClose) LoadDuLieu();
                else
                    MSG.Error(resSystem.MSG_System_04);
            }

        }

        void Removeleged()
        {
            TIAdjustment _select = ugridDS.Selected.Rows[0].ListObject as TIAdjustment;
            try
            {
                _ITIAdjustmentService.BeginTran();
                _select.Recorded = false;
                //List<FixedAssetLedger> lstFixedAssetLedgers = _IFixedAssetLedgerService.GetByReferenceID(_select.ID);
                //foreach (var generalLedger in lstFixedAssetLedgers)
                //{
                //    _IFixedAssetLedgerService.Delete(generalLedger);
                //}
                _ITIAdjustmentService.Update(_select);
                _ITIAdjustmentService.CommitTran();
            }
            catch (Exception)
            {

                _ITIAdjustmentService.RolbackTran();
            }
        }
        void Saveleged()
        {

            try
            {
                TIAdjustment temp = ugridDS.Selected.Rows[0].ListObject as TIAdjustment;

                if (temp == null)
                {
                    return;
                }
                List<TIAdjustmentDetail> lstTIAdjustmentDetails = _ITIAdjustmentDetailService.GetByTIAdjustmentID(temp.ID);
                var lstDetailsFai = new List<FAIncrementDetail>();
                //List<FixedAssetLedger> listGenTemp = new List<FixedAssetLedger>();
                //for (int i = 0; i < lstTIAdjustmentDetails.Count; i++)
                //{
                //    var firstOrDefault = _IFixedAssetService.GetTimeUsed(lstTIAdjustmentDetails[i].FixedAssetID ?? Guid.NewGuid()).FirstOrDefault();
                //    if (firstOrDefault != null)
                //    {
                //        FixedAssetLedger genTemplLedger = new FixedAssetLedger
                //        {
                //            ID = Guid.NewGuid(),
                //            BranchID = temp.BranchID,
                //            ReferenceID = temp.ID,
                //            No = temp.No,
                //            TypeID = temp.TypeID,
                //            Date = temp.Date,
                //            PostedDate = temp.Date,
                //            FixedAssetID = lstTIAdjustmentDetails[i].FixedAssetID,
                //            DepartmentID = lstTIAdjustmentDetails[i].ToDepartment ?? new Guid(),
                //            DepreciationRate = firstOrDefault.MonthDepreciationRate,
                //            UsedTime = firstOrDefault.UsedTime ?? 0,
                //            OriginalPriceDebitAmount = firstOrDefault.OriginalPrice,
                //            DepreciationAmount = firstOrDefault.MonthPeriodDepreciationAmount ?? 0,
                //            DepreciationAccount = firstOrDefault.DepreciationAccount,
                //            OriginalPriceCreditAmount = 0,
                //            DepreciationDebitAmount = 0,
                //            Description = lstTIAdjustmentDetails[i].Description,
                //            Reason = temp.Reason,
                //            DepreciationCreditAmount = 0,
                //            OrderPriority = lstTIAdjustmentDetails[i].OrderPriority,
                //            OriginalPriceAccount = lstDetailsFai[i].DebitAccount,
                //        };

                //        listGenTemp.Add(genTemplLedger);
                //    }
                //    FixedAssetLedger genTempCorresponding = new FixedAssetLedger
                //    {
                //        ID = Guid.NewGuid(),
                //        BranchID = temp.BranchID,
                //        ReferenceID = temp.ID,
                //        No = temp.No,
                //        TypeID = temp.TypeID,
                //        Date = temp.Date,
                //        PostedDate = temp.Date,
                //        //FixedAssetID = lstTIAdjustmentDetails[i].FixedAssetID,
                //        //DepartmentID = lstTIAdjustmentDetails[i].ToDepartment ?? new Guid(),
                //        DepreciationRate = firstOrDefault.MonthDepreciationRate,
                //        //DepreciationAccount=firstOrDefault.DepreciationAccount,
                //        UsedTime = firstOrDefault.UsedTime ?? 0,
                //        OriginalPriceDebitAmount = firstOrDefault.OriginalPrice,
                //        DepreciationAmount = firstOrDefault.MonthPeriodDepreciationAmount ?? 0,
                //        DepreciationAccount = firstOrDefault.DepreciationAccount,
                //        OriginalPriceCreditAmount = 0,
                //        DepreciationDebitAmount = 0,
                //        Description = lstTIAdjustmentDetails[i].Description,
                //        Reason = temp.Reason,
                //        DepreciationCreditAmount = 0,
                //        OrderPriority = lstTIAdjustmentDetails[i].OrderPriority,
                //        OriginalPriceAccount = lstDetailsFai[i].DebitAccount
                //    };

                //    listGenTemp.Add(genTempCorresponding);
                //}


                //foreach (var generalLedger in listGenTemp)
                //{
                //    _IFixedAssetLedgerService.CreateNew(generalLedger);
                //}
                temp.Recorded = true;
                _ITIAdjustmentService.Update(temp);
                _ITIAdjustmentService.CommitTran();
            }
            catch (Exception)
            {

                _ITIAdjustmentService.RolbackTran();
            }
        }
        #endregion
        #region utils


        void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị

            lstTIAdjustments = _ITIAdjustmentService.GetAll();
            ugridDS.DataSource = lstTIAdjustments;
            UgridCT.DataSource = lstfTIAdjustmentDetails;
            if (configGrid) ConfigGrid(ugridDS);
            #endregion
        }
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung

            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }
        void DisColums(UltraGridColumn ugrid)
        {
            ugrid.CellActivation = Activation.NoEdit;
            ugrid.CellClickAction = CellClickAction.RowSelect;

        }
        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)
        {
            string keyofdsTemplate = String.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                // các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            //keyofdsTemplate = string.Empty;
            return dsTemplate[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {
            return typeId;
        }
        #endregion


    }
}
