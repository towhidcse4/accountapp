﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FTIAllocationDetail : FTIAllocationBase
    {
        #region khai bao
        private readonly ITIInitService _ITIInitService = Utils.ITIInitService;
        private readonly ITIDecrementDetailService _ITIDecrementDetailService = Utils.ITIDecrementDetailService;
        private readonly IDepartmentService _IDepartmentService = Utils.IDepartmentService;
        private UltraGrid uGridDS;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        //Tập các dữ liệu cần dùng
        #endregion
        public FTIAllocationDetail(TIAllocation temp, List<TIAllocation> dsDepreciations, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new CKTKHA(temp, dsDepreciations, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    base.Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    temp = dsDepreciations.FirstOrDefault(k => k.ID == frm.TIAllocation.ID);
                else temp.Date = frm.TIAllocation.Date;
            }
            #region Khởi tạo giá trị mặc định của Form
          //  base.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            //this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            #region Thiết lập ban đầu cho Form
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                temp.TypeID = 434;
                _select = temp;
                _select.PostedDate = temp.PostedDate;
                _select.Date = temp.PostedDate;
            }
            else
            {
                _select = temp;
            }

            _listSelects.AddRange(dsDepreciations);

            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, dsDepreciations, statusForm);
            base.StartPosition = FormStartPosition.CenterScreen;
            this.StartPosition = FormStartPosition.CenterScreen;

            //this.StartPosition = FormStartPosition.CenterParent;
            #endregion

            WaitingFrm.StopWaiting();
        }

        public override void ShowPopup(TIAllocation input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            var frm = new CKTKHA(input, _listSelects, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                _statusForm = ConstFrm.optStatusForm.View;
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.TIAllocation.ID);
            else
            {
                input = frm.TIAllocation;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }

        #region override
        public override void InitializeGUI(TIAllocation input)
        {
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            BindingList<TIAllocationDetail> dsTIAllocationDetails = new BindingList<TIAllocationDetail>();
            BindingList<TIAllocationAllocated> dsTIAllocationAllocateds = new BindingList<TIAllocationAllocated>();
            BindingList<TIAllocationPost> dsTIAllocationPosts = new BindingList<TIAllocationPost>();
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            Template mauGiaoDien = Utils.GetTemplateUIfromDatabase(input.TypeID,
                _statusForm == ConstFrm.optStatusForm.Add, input.TemplateID);
            _select.TemplateID = _statusForm == 2 ? mauGiaoDien.ID : input.TemplateID;
            if (_statusForm == ConstFrm.optStatusForm.Add) _select.Reason = string.Format("Phân bổ CCDC tháng {0} năm {1}", _select.Month, _select.Year);
            ConstFrm.TTH = _select.PostedDate.ToString("dd/MM/yyyy");
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
               
                var lst = new List<TIAllocationDetail>();
                var lstallo = new List<TIAllocationAllocated>();
                var lstPost = new List<TIAllocationPost>();
                //var lstpb = ITIAllocationDetailService.GetAll();
                List<TIInit> source = _ITIInitService.FindAllotionTools(_select.PostedDate).OrderBy(c=>c.ToolsCode).ToList();
                foreach (TIInit init in source)
                {
                    TIAllocationDetail detail = new TIAllocationDetail();
                    {
                        detail.ToolsID = init.ID;                        
                        detail.ToolsName = init.ToolsName;
                        detail.Quantity = init.Quantity;
                        var date = init.PostedDate??DateTime.Now;/*_ITIInitService.GetDateIncrement(init);*/
                        var c = (DateTime.DaysInMonth(input.PostedDate.Year, input.PostedDate.Month) - date.Day + 1);
                        var d = (DateTime.DaysInMonth(input.PostedDate.Year, input.PostedDate.Month));
                        detail.AllocationAmount = (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 125).Data == "0" && input.PostedDate.Year == date.Year && input.PostedDate.Month == date.Month) ? ((init.AllocatedAmount ?? 0) * c / d) : init.AllocatedAmount ?? 0;
                        detail.RemainingAmount = _ITIDecrementDetailService.GetRemainingAmount(init.ID, _select.PostedDate);

                    };
                    if (init.DeclareType == 0 && init.AllocationTimes == 1) detail.AllocationAmount = (init.Amount - (init.AllocationAmount ==null ? 0: init.AllocationAmount)) ?? 0;
                    if (init.RemainAmount == detail.RemainingAmount)
                    {
                        detail.AllocationAmount = 0;
                    }
                    detail.Amount = detail.AllocationAmount + detail.RemainingAmount;

                    List<TIInitDetail> allos = _ITIInitService.FindAllocationAllocated(init.ID, _select.PostedDate);
                    List<Guid?> dep = allos.Select(x => x.ObjectID).ToList();
                    //List<Department> deps = Utils.ListDepartment.Where(x => dep.Contains(x.ID)).ToList();
                    //List<CostSet> costs = Utils.ListCostSet.Where(x => dep.Contains(x.ID)).ToList();
                    foreach (TIInitDetail initdetail in allos)
                    {
                        TIAllocationAllocated allo = new TIAllocationAllocated
                        {
                            ToolsID = init.ID,
                            ToolsName = init.ToolsName,
                            TotalAllocationAmount = detail.Amount,
                            TotalAllocationAmountOriginal = detail.Amount,
                            Rate = initdetail.Rate ?? 0,
                            AllocationAmount = detail.Amount * (initdetail.Rate ?? 0) / 100,
                            AllocationAmountOriginal = detail.Amount * (initdetail.Rate ?? 0) / 100,
                            ObjectID = initdetail.ObjectID,
                            AccountNumber = initdetail.CostAccount,
                            ExpenseItemID = initdetail.ExpenseItemID
                        };
                        //Department Department = deps.Where(x => x.ID == initdetail.ObjectID).FirstOrDefault();
                        //CostSet costSet = costs.Where(x => x.ID == initdetail.ObjectID).FirstOrDefault();
                        //if (Department != null)
                        //{
                        //    allo.AccountNumber = Department.CostAccount;
                        //}
                        if(allo.AllocationAmountOriginal > 0)
                        lstallo.Add(allo);

                        TIAllocationPost post = new TIAllocationPost
                        {
                            ToolsID = init.ID,
                            Amount = detail.Amount * (initdetail.Rate ?? 0) / 100,
                            AmountOriginal = detail.Amount * (initdetail.Rate ?? 0) / 100,
                            Description = _select.Reason,
                            CreditAccount = init.AllocationAwaitAccount,
                            DebitAccount = allo.AccountNumber,
                            DepartmentID = initdetail.ObjectType == 1 ? initdetail.ObjectID : null,
                            CostSetID = initdetail.ObjectType != 1 ? initdetail.ObjectID : null,
                            ExpenseItemID = initdetail.ExpenseItemID
                        };
                        if(post.Amount > 0)
                            lstPost.Add(post);
                    }
                    if(detail.Amount > 0)
                        lst.Add(detail);
                }                

                dsTIAllocationDetails = new BindingList<TIAllocationDetail>(lst);
                dsTIAllocationAllocateds = new BindingList<TIAllocationAllocated>(lstallo);
                dsTIAllocationPosts = new BindingList<TIAllocationPost>(lstPost);
            }
            else
            {
                
                dsTIAllocationDetails = new BindingList<TIAllocationDetail>(input.TIAllocationDetails.OrderBy(x => x.OrderPriority).ToList());
                dsTIAllocationAllocateds = new BindingList<TIAllocationAllocated>(_select.TIAllocationAllocateds.OrderBy(x => x.OrderPriority).ToList());
                dsTIAllocationPosts = new BindingList<TIAllocationPost>(_select.TIAllocationPosts.OrderBy(x => x.OrderPriority).ToList());
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);

            }

            _listObjectInput = new BindingList<System.Collections.IList> { dsTIAllocationDetails, dsTIAllocationAllocateds };
            _listObjectInputGroup = new BindingList<System.Collections.IList> { dsTIAllocationPosts };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<TIAllocation>(pnlUgrid, mauGiaoDien);

            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputGroup, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { false, false, false, false };
            this.ConfigGridByManyTemplete<TIAllocation>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard);
            uGridDS = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();

            this.ConfigTopVouchersNo<TIAllocation>(pnlDateNo, "No", "PostedDate", "Date", null, true);
            DataBinding(new List<System.Windows.Forms.Control> { txtDG }, "Reason");
            ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            if (_statusForm == ConstFrm.optStatusForm.Add)
                _select.ID = Guid.NewGuid();
        }
        #endregion

        #region Event
        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal("VND", datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
        #endregion

        private void FTIAllocationDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
    public class FTIAllocationBase : DetailBase<TIAllocation>
    {
        public IGenCodeService IgenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
    }

}
