﻿namespace Accounting
{
    sealed partial class FTIAllocationDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.palTop = new System.Windows.Forms.Panel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnlDateNo = new Infragistics.Win.Misc.UltraPanel();
            this.palThongTinChung = new System.Windows.Forms.Panel();
            this.ugbThongTinChung = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDG = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDG = new Infragistics.Win.Misc.UltraLabel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.pnlDateNo.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).BeginInit();
            this.ugbThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDG)).BeginInit();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.ultraGroupBox2);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(965, 71);
            this.palTop.TabIndex = 28;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.pnlDateNo);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance2;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(965, 71);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "Bảng phân bổ CCDC";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnlDateNo
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.pnlDateNo.Appearance = appearance1;
            this.pnlDateNo.Location = new System.Drawing.Point(12, 27);
            this.pnlDateNo.Name = "pnlDateNo";
            this.pnlDateNo.Size = new System.Drawing.Size(323, 38);
            this.pnlDateNo.TabIndex = 0;
            // 
            // palThongTinChung
            // 
            this.palThongTinChung.Controls.Add(this.ugbThongTinChung);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 71);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(965, 71);
            this.palThongTinChung.TabIndex = 29;
            // 
            // ugbThongTinChung
            // 
            this.ugbThongTinChung.Controls.Add(this.txtDG);
            this.ugbThongTinChung.Controls.Add(this.lblDG);
            this.ugbThongTinChung.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance4.FontData.BoldAsString = "True";
            appearance4.FontData.SizeInPoints = 10F;
            this.ugbThongTinChung.HeaderAppearance = appearance4;
            this.ugbThongTinChung.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ugbThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.ugbThongTinChung.Name = "ugbThongTinChung";
            this.ugbThongTinChung.Size = new System.Drawing.Size(965, 71);
            this.ugbThongTinChung.TabIndex = 26;
            this.ugbThongTinChung.Text = "Thông tin chung";
            this.ugbThongTinChung.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtDG
            // 
            this.txtDG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDG.Location = new System.Drawing.Point(80, 28);
            this.txtDG.Multiline = true;
            this.txtDG.Name = "txtDG";
            this.txtDG.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDG.Size = new System.Drawing.Size(872, 36);
            this.txtDG.TabIndex = 34;
            this.txtDG.Text = "Phân bổ CCDC";
            // 
            // lblDG
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblDG.Appearance = appearance3;
            this.lblDG.Location = new System.Drawing.Point(6, 28);
            this.lblDG.Name = "lblDG";
            this.lblDG.Size = new System.Drawing.Size(68, 19);
            this.lblDG.TabIndex = 22;
            this.lblDG.Text = "Diễn giải :";
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 152);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(965, 220);
            this.pnlUgrid.TabIndex = 30;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance5;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(866, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 7;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 142);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 71;
            this.ultraSplitter1.Size = new System.Drawing.Size(965, 10);
            this.ultraSplitter1.TabIndex = 3;
            // 
            // FTIAllocationDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 372);
            this.Controls.Add(this.pnlUgrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.palThongTinChung);
            this.Controls.Add(this.palTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FTIAllocationDetail";
            this.Text = "Bảng chi tiết phân bổ Công cụ dụng cụ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FTIAllocationDetail_FormClosing);
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.pnlDateNo.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugbThongTinChung)).EndInit();
            this.ugbThongTinChung.ResumeLayout(false);
            this.ugbThongTinChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDG)).EndInit();
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel palThongTinChung;
        private Infragistics.Win.Misc.UltraGroupBox ugbThongTinChung;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDG;
        private Infragistics.Win.Misc.UltraLabel lblDG;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel pnlDateNo;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}