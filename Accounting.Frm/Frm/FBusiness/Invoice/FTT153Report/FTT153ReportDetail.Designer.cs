﻿namespace Accounting
{
    partial class FTT153ReportDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnView = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cbbInvoiceType = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbInvoiceForm = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtInvoiceSeries = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTempSortOrder = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbInvoiceTemplate = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceTypeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCopyNumber = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReportName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTempSortOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCopyNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            // 
            // btnView
            // 
            appearance1.Image = global::Accounting.Properties.Resources.btnsearch;
            this.btnView.Appearance = appearance1;
            this.btnView.Location = new System.Drawing.Point(21, 13);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(85, 30);
            this.btnView.TabIndex = 40;
            this.btnView.Text = "Xem mẫu";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnClose
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(756, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 42;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance3.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance3;
            this.btnSave.Location = new System.Drawing.Point(675, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 41;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uGrid
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance4;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.UseFixedHeaders = true;
            this.uGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uGrid.Location = new System.Drawing.Point(1, 166);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(845, 266);
            this.uGrid.TabIndex = 28;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // cbbInvoiceType
            // 
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbInvoiceType.DisplayLayout.Appearance = appearance15;
            this.cbbInvoiceType.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbInvoiceType.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceType.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceType.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.cbbInvoiceType.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceType.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.cbbInvoiceType.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbInvoiceType.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbInvoiceType.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbInvoiceType.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.cbbInvoiceType.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbInvoiceType.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceType.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbInvoiceType.DisplayLayout.Override.CellAppearance = appearance22;
            this.cbbInvoiceType.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbInvoiceType.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceType.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.cbbInvoiceType.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.cbbInvoiceType.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbInvoiceType.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.cbbInvoiceType.DisplayLayout.Override.RowAppearance = appearance25;
            this.cbbInvoiceType.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbInvoiceType.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.cbbInvoiceType.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbInvoiceType.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbInvoiceType.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbInvoiceType.Location = new System.Drawing.Point(606, 40);
            this.cbbInvoiceType.Name = "cbbInvoiceType";
            this.cbbInvoiceType.Size = new System.Drawing.Size(195, 22);
            this.cbbInvoiceType.TabIndex = 22;
            this.cbbInvoiceType.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.cbbInvoiceType_InitializeLayout);
            this.cbbInvoiceType.AfterCloseUp += new System.EventHandler(this.cbbInvoiceType_Close);
            this.cbbInvoiceType.ValueChanged += new System.EventHandler(this.Value_Changed);
            // 
            // cbbInvoiceForm
            // 
            valueListItem1.DataValue = 2;
            valueListItem1.DisplayText = "Hóa đơn điện tử";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Hóa đơn đặt in";
            valueListItem3.DataValue = 0;
            valueListItem3.DisplayText = "Hóa đơn tự in";
            this.cbbInvoiceForm.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.cbbInvoiceForm.Location = new System.Drawing.Point(177, 42);
            this.cbbInvoiceForm.Name = "cbbInvoiceForm";
            this.cbbInvoiceForm.Size = new System.Drawing.Size(195, 21);
            this.cbbInvoiceForm.TabIndex = 21;
            this.cbbInvoiceForm.ValueChanged += new System.EventHandler(this.Value_Changed);
            this.cbbInvoiceForm.Validated += new System.EventHandler(this.cbbInvoiceForm_Validated);
            // 
            // txtInvoiceSeries
            // 
            appearance27.TextVAlignAsString = "Middle";
            this.txtInvoiceSeries.Appearance = appearance27;
            this.txtInvoiceSeries.AutoSize = false;
            this.txtInvoiceSeries.Location = new System.Drawing.Point(606, 96);
            this.txtInvoiceSeries.MaxLength = 8;
            this.txtInvoiceSeries.Name = "txtInvoiceSeries";
            this.txtInvoiceSeries.Size = new System.Drawing.Size(195, 22);
            this.txtInvoiceSeries.TabIndex = 26;
            this.txtInvoiceSeries.ValueChanged += new System.EventHandler(this.Value_Changed);
            // 
            // ultraLabel8
            // 
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance28;
            this.ultraLabel8.Location = new System.Drawing.Point(463, 96);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel8.TabIndex = 12;
            this.ultraLabel8.Text = "Ký hiệu hóa đơn (*)";
            // 
            // ultraLabel7
            // 
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance29;
            this.ultraLabel7.Location = new System.Drawing.Point(463, 69);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel7.TabIndex = 8;
            this.ultraLabel7.Text = "Số thứ tự mẫu";
            // 
            // txtTempSortOrder
            // 
            this.txtTempSortOrder.Location = new System.Drawing.Point(606, 68);
            this.txtTempSortOrder.MaxValue = 999;
            this.txtTempSortOrder.Name = "txtTempSortOrder";
            this.txtTempSortOrder.PromptChar = ' ';
            this.txtTempSortOrder.Size = new System.Drawing.Size(195, 21);
            this.txtTempSortOrder.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtTempSortOrder.TabIndex = 24;
            this.txtTempSortOrder.Value = 1;
            this.txtTempSortOrder.ValueChanged += new System.EventHandler(this.EditorValueChange2);
            // 
            // ultraLabel6
            // 
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance30;
            this.ultraLabel6.Location = new System.Drawing.Point(463, 40);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel6.TabIndex = 4;
            this.ultraLabel6.Text = "Loại hóa đơn (*)";
            // 
            // cbbInvoiceTemplate
            // 
            appearance31.TextVAlignAsString = "Middle";
            this.cbbInvoiceTemplate.Appearance = appearance31;
            this.cbbInvoiceTemplate.AutoSize = false;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbInvoiceTemplate.DisplayLayout.Appearance = appearance32;
            this.cbbInvoiceTemplate.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbInvoiceTemplate.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate.DisplayLayout.GroupByBox.Appearance = appearance33;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceTemplate.DisplayLayout.GroupByBox.BandLabelAppearance = appearance34;
            this.cbbInvoiceTemplate.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance35.BackColor2 = System.Drawing.SystemColors.Control;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbInvoiceTemplate.DisplayLayout.GroupByBox.PromptAppearance = appearance35;
            this.cbbInvoiceTemplate.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbInvoiceTemplate.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbInvoiceTemplate.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance37.BackColor = System.Drawing.SystemColors.Highlight;
            appearance37.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbInvoiceTemplate.DisplayLayout.Override.ActiveRowAppearance = appearance37;
            this.cbbInvoiceTemplate.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbInvoiceTemplate.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate.DisplayLayout.Override.CardAreaAppearance = appearance38;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            appearance39.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbInvoiceTemplate.DisplayLayout.Override.CellAppearance = appearance39;
            this.cbbInvoiceTemplate.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbInvoiceTemplate.DisplayLayout.Override.CellPadding = 0;
            appearance40.BackColor = System.Drawing.SystemColors.Control;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbInvoiceTemplate.DisplayLayout.Override.GroupByRowAppearance = appearance40;
            appearance41.TextHAlignAsString = "Left";
            this.cbbInvoiceTemplate.DisplayLayout.Override.HeaderAppearance = appearance41;
            this.cbbInvoiceTemplate.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbInvoiceTemplate.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            this.cbbInvoiceTemplate.DisplayLayout.Override.RowAppearance = appearance42;
            this.cbbInvoiceTemplate.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbInvoiceTemplate.DisplayLayout.Override.TemplateAddRowAppearance = appearance43;
            this.cbbInvoiceTemplate.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbInvoiceTemplate.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbInvoiceTemplate.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbInvoiceTemplate.Location = new System.Drawing.Point(177, 123);
            this.cbbInvoiceTemplate.Name = "cbbInvoiceTemplate";
            this.cbbInvoiceTemplate.Size = new System.Drawing.Size(624, 22);
            this.cbbInvoiceTemplate.TabIndex = 27;
            this.cbbInvoiceTemplate.ValueChanged += new System.EventHandler(this.Value_Changed);
            // 
            // ultraLabel5
            // 
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance44;
            this.ultraLabel5.Location = new System.Drawing.Point(36, 124);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel5.TabIndex = 14;
            this.ultraLabel5.Text = "Dựa trên mẫu";
            // 
            // txtInvoiceTypeCode
            // 
            appearance45.TextVAlignAsString = "Middle";
            this.txtInvoiceTypeCode.Appearance = appearance45;
            this.txtInvoiceTypeCode.AutoSize = false;
            this.txtInvoiceTypeCode.Enabled = false;
            this.txtInvoiceTypeCode.Location = new System.Drawing.Point(177, 95);
            this.txtInvoiceTypeCode.Name = "txtInvoiceTypeCode";
            this.txtInvoiceTypeCode.Size = new System.Drawing.Size(195, 22);
            this.txtInvoiceTypeCode.TabIndex = 25;
            this.txtInvoiceTypeCode.ValueChanged += new System.EventHandler(this.Value_Changed);
            // 
            // ultraLabel4
            // 
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance46;
            this.ultraLabel4.Location = new System.Drawing.Point(36, 96);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel4.TabIndex = 10;
            this.ultraLabel4.Text = "Mẫu số hóa đơn (*)";
            // 
            // txtCopyNumber
            // 
            this.txtCopyNumber.Location = new System.Drawing.Point(177, 69);
            this.txtCopyNumber.MaxValue = 9;
            this.txtCopyNumber.Name = "txtCopyNumber";
            this.txtCopyNumber.PromptChar = ' ';
            this.txtCopyNumber.Size = new System.Drawing.Size(195, 21);
            this.txtCopyNumber.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
            this.txtCopyNumber.TabIndex = 23;
            this.txtCopyNumber.Value = 3;
            this.txtCopyNumber.ValueChanged += new System.EventHandler(this.EditorValueChange);
            // 
            // ultraLabel3
            // 
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance47;
            this.ultraLabel3.Location = new System.Drawing.Point(36, 68);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel3.TabIndex = 6;
            this.ultraLabel3.Text = "Số liên (*)";
            // 
            // ultraLabel2
            // 
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance48;
            this.ultraLabel2.Location = new System.Drawing.Point(36, 40);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel2.TabIndex = 2;
            this.ultraLabel2.Text = "Hình thức hóa đơn (*)";
            // 
            // txtReportName
            // 
            appearance49.TextVAlignAsString = "Middle";
            this.txtReportName.Appearance = appearance49;
            this.txtReportName.AutoSize = false;
            this.txtReportName.Location = new System.Drawing.Point(177, 12);
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Size = new System.Drawing.Size(624, 22);
            this.txtReportName.TabIndex = 20;
            this.txtReportName.ValueChanged += new System.EventHandler(this.Value_Changed);
            // 
            // ultraLabel1
            // 
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance50;
            this.ultraLabel1.Location = new System.Drawing.Point(36, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel1.TabIndex = 30;
            this.ultraLabel1.Text = "Tên mẫu hóa đơn (*)";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnView);
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Location = new System.Drawing.Point(1, 438);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(845, 49);
            this.ultraGroupBox3.TabIndex = 44;
            // 
            // FTT153ReportDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 497);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.cbbInvoiceType);
            this.Controls.Add(this.cbbInvoiceForm);
            this.Controls.Add(this.txtInvoiceSeries);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraLabel8);
            this.Controls.Add(this.ultraLabel7);
            this.Controls.Add(this.txtReportName);
            this.Controls.Add(this.txtTempSortOrder);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.ultraLabel6);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.cbbInvoiceTemplate);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel5);
            this.Controls.Add(this.txtCopyNumber);
            this.Controls.Add(this.txtInvoiceTypeCode);
            this.Controls.Add(this.ultraLabel4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(867, 536);
            this.MinimizeBox = false;
            this.Name = "FTT153ReportDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Khởi tạo mẫu hóa đơn";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTempSortOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbInvoiceTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCopyNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReportName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceSeries;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtTempSortOrder;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbInvoiceTemplate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceTypeCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtCopyNumber;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraButton btnView;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbInvoiceForm;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbInvoiceType;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
    }
}