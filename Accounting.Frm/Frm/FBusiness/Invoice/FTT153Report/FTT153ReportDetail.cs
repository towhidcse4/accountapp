﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Accounting.Core.Domain.obj;

namespace Accounting
{
    public partial class FTT153ReportDetail : CustormForm
    {
        #region khai báo
        public readonly IInvoiceTypeService _IInvoiceTypeService;
        public readonly ITT153InvoiceTemplateService _ITT153InvoiceTemplate;
        public readonly ITT153ReportService _ITT153ReportService;
        private List<TT153ReportDetail> lstTT153ReportDetail = new List<TT153ReportDetail>();
        TT153Report _select = new TT153Report();
        int _statusForm;
        public static bool isClose = true;
        List<TT153InvoiceTemplate> lstTemp = new List<TT153InvoiceTemplate>();
        // Biến kiểm tra số liên người dùng nhập
        int check;
        bool Them = true;
        Color color1 = Color.FromName("112, 48, 160");
        Color color2 = Color.FromName("255, 0, 0");
        Color color3 = Color.FromName("0, 176, 80");
        Color color4 = Color.Yellow;
        List<Color> LstColor = new List<Color>() { Color.Purple, Color.Red, Color.Blue };
        #endregion
        public FTT153ReportDetail(TT153Report temp, List<TT153Report> lstTT153Report, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            _ITT153InvoiceTemplate = IoC.Resolve<ITT153InvoiceTemplateService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _statusForm = statusForm;
            _select = temp;
            //txtInvoiceSeries.Editor = Infragistics.Wi
            //Utils.SortFormControls(ultraGroupBox1, ultraGroupBox1.TabIndex);
            #region Config các combobox
            InitializeGUI();
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Edit || _statusForm == ConstFrm.optStatusForm.View)
            {
                ObjandGUI(_select, false);
                lstTT153ReportDetail = Create(int.Parse(txtCopyNumber.Value.ToString()), false);
                btnSave.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
                lstTT153ReportDetail = Create(int.Parse(txtCopyNumber.Value.ToString()), true);
            }
            #region Load config uGrid
            LoaduGrid();
            #endregion
            WaitingFrm.StopWaiting();

        }
        private TT153Report ObjandGUI(TT153Report input, bool isGet)
        {
            if (isGet == true)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.ReportName = txtReportName.Text;
                InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                input.InvoiceType = ivt._InvoiceType;
                input.InvoiceForm = int.Parse(cbbInvoiceForm.Value.ToString());
                input.CopyNumber = decimal.Parse(txtCopyNumber.Value.ToString());
                input.TempSortOrder = int.Parse(txtTempSortOrder.Value.ToString());
                input.InvoiceSeries = txtInvoiceSeries.Text;
                input.InvoiceTemplate = txtInvoiceTypeCode.Text;
                input.InvoiceTypeID = ivt.ID;
                if (cbbInvoiceTemplate.Value != null && (Guid)cbbInvoiceTemplate.Value != Guid.Empty)
                    input.RootReportID = cbbInvoiceTemplate.Value.ToString();
                //TT153InvoiceTemplate ttIvt = _ITT153InvoiceTemplate.Getbykey((Guid)cbbInvoiceTemplate.Value);
                if (int.Parse(cbbInvoiceForm.Value.ToString()) == 1)
                {
                    input.IsInPlace = true;
                }
                else
                {
                    input.IsInPlace = false;
                }

                //input.RootReportID = ttIvt.ID.ToString();
                List<TT153ReportDetail> TT153ReportDetail = ((BindingList<TT153ReportDetail>)uGrid.DataSource).ToList();
                for (int i = 0; i < TT153ReportDetail.Count(); i++)
                {
                    if (i == 0)
                    {
                        input.Purpose1 = TT153ReportDetail[i].Purpose;
                        input.CodeColor1 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 1)
                    {
                        input.Purpose2 = TT153ReportDetail[i].Purpose;
                        input.CodeColor2 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 2)
                    {
                        input.Purpose3 = TT153ReportDetail[i].Purpose;
                        input.CodeColor3 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 3)
                    {
                        input.Purpose4 = TT153ReportDetail[i].Purpose;
                        input.CodeColor4 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 4)
                    {
                        input.Purpose5 = TT153ReportDetail[i].Purpose;
                        input.CodeColor5 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 5)
                    {
                        input.Purpose6 = TT153ReportDetail[i].Purpose;
                        input.CodeColor6 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 6)
                    {
                        input.Purpose7 = TT153ReportDetail[i].Purpose;
                        input.CodeColor7 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 7)
                    {
                        input.Purpose8 = TT153ReportDetail[i].Purpose;
                        input.CodeColor8 = TT153ReportDetail[i].CodeColor;
                    }
                    else if (i == 8)
                    {
                        input.Purpose9 = TT153ReportDetail[i].Purpose;
                        input.CodeColor9 = TT153ReportDetail[i].CodeColor;
                    }
                }
                for (int i = int.Parse(txtCopyNumber.Value.ToString()); i < 9; i++)
                {
                    if (i == 0)
                    {
                        input.Purpose1 = null;
                        input.CodeColor1 = null;
                    }
                    else if (i == 1)
                    {
                        input.Purpose2 = null;
                        input.CodeColor2 = null;
                    }
                    else if (i == 2)
                    {
                        input.Purpose3 = null;
                        input.CodeColor3 = null;
                    }
                    else if (i == 3)
                    {
                        input.Purpose4 = null;
                        input.CodeColor4 = null;
                    }
                    else if (i == 4)
                    {
                        input.Purpose5 = null;
                        input.CodeColor5 = null;
                    }
                    else if (i == 5)
                    {
                        input.Purpose6 = null;
                        input.CodeColor6 = null;
                    }
                    else if (i == 6)
                    {
                        input.Purpose7 = null;
                        input.CodeColor7 = null;
                    }
                    else if (i == 7)
                    {
                        input.Purpose8 = null;
                        input.CodeColor8 = null;
                    }
                    else if (i == 8)
                    {
                        input.Purpose9 = null;
                        input.CodeColor9 = null;
                    }
                }
            }
            else
            {
                //lstTT153ReportDetail = Create(Decimal.ToInt32(input.CopyNumber));
                txtCopyNumber.Value = input.CopyNumber;
                txtReportName.Value = input.ReportName;
                foreach (var item in cbbInvoiceType.Rows)
                {
                    if ((item.ListObject as InvoiceType).ID == input.InvoiceTypeID) cbbInvoiceType.SelectedRow = item;
                }
                InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                //if(ivt.InvoiceTypeCode.Contains("01/ hoặc 02/"))
                //    txtInvoiceTypeCode.Value = "01" + txtCopyNumber.Value.ToString() + "/" + "00" + txtTempSortOrder.Value;
                //else txtInvoiceTypeCode.Value = ivt.InvoiceTypeCode + txtCopyNumber.Value.ToString() + "/" + "00" + txtTempSortOrder.Value;
                txtInvoiceTypeCode.Value = input.InvoiceTemplate;
                txtInvoiceSeries.Value = input.InvoiceSeries;
                cbbInvoiceForm.Value = input.InvoiceForm;
                txtTempSortOrder.Value = input.TempSortOrder;
                foreach (var item in cbbInvoiceTemplate.Rows)
                {
                    if ((item.ListObject as TT153InvoiceTemplate).ID.ToString() == input.RootReportID) cbbInvoiceTemplate.SelectedRow = item;
                }
            }
            return input;
        }

        private void InitializeGUI()
        {
            lstTemp = _ITT153InvoiceTemplate.GetAll().OrderBy(n => n.InvoiceTempName).ToList();
            this.ConfigCombo(Utils.ListInvoiceType, cbbInvoiceType, "InvoiceTypeName", "ID");
            //lstTemp.Add(new TT153InvoiceTemplate());
            //this.ConfigCombo(lstTemp, cbbInvoiceTemplate, "InvoiceTempName", "ID");
            cbbInvoiceTemplate.DataSource = lstTemp;
            cbbInvoiceTemplate.DisplayMember = "InvoiceTempName";
            cbbInvoiceTemplate.ValueMember = "ID";
            Utils.ConfigGrid(cbbInvoiceTemplate, ConstDatabase.TT153InvoiceTemplate_TableName);

            check = int.Parse(txtCopyNumber.Value.ToString());
            cbbInvoiceForm.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            cbbInvoiceTemplate.DropDownStyle = UltraComboStyle.DropDownList;
            cbbInvoiceType.DropDownStyle = UltraComboStyle.DropDownList;

        }
        public void LoaduGrid()
        {
            uGrid.DataSource = new BindingList<TT153ReportDetail>(lstTT153ReportDetail);
            Utils.ConfigGrid(uGrid, ConstDatabase.TT153ReportDetail_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            var color = new UltraColorPicker();

            foreach (var colum in uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, colum, uGrid);
                if (colum.Key.Equals("CodeColor"))
                {
                    colum.Editor = color.Editor;
                }

            }
            uGrid.DisplayLayout.Bands[0].Columns["CopyNumberName"].CellActivation = Activation.NoEdit;
            int i = 0;
            foreach (UltraGridRow row in uGrid.Rows)
            {
                i++;
                foreach (UltraGridCell cell in row.Cells)
                {
                    if (cell.Column.Key.Equals("Purpose"))
                    {
                        cell.Activation = Activation.NoEdit;
                    }
                }
                if (i == 2) break;
            }

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            isClose = true;
        }

        private void txtCopyNumber_ValueChanged(object sender, EventArgs e)
        {
            //if(txtCopyNumber.Value)
            //{
            //    Utils.AddNewRow4Grid(uGrid);
            //}
        }

        private void txtReportName_ValueChanged(object sender, EventArgs e)
        {

        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {

        }

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {

        }
        private void cbbInvoiceType_Close(object sender, EventArgs e)
        {
            if (cbbInvoiceType.SelectedRow != null)
            {
                InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                if (ivt.InvoiceTypeCode.Contains("01/ hoặc 02/"))
                {
                    txtInvoiceTypeCode.Value = "01/";
                    txtInvoiceTypeCode.Enabled = true;
                }
                else
                {
                    txtInvoiceTypeCode.Enabled = false;
                    txtInvoiceTypeCode.Value = ivt.InvoiceTypeCode + txtCopyNumber.Value.ToString() + "/" + "00" + txtTempSortOrder.Value;
                }
            }

        }
        public bool CheckIsNumber(String Text)
        {
            bool kq = true;
            if (Text == null || Text.Length == 0) return false;
            foreach (char c in Text)
            {
                if (!char.IsDigit(c) && c != '.')
                {
                    return false;
                }
            }
            return kq;
        }
        private Boolean CheckError()
        {
            bool kq = true;
            //Check Error Chung
            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtCopyNumber.Text) || string.IsNullOrEmpty(txtInvoiceSeries.Text)
                || string.IsNullOrEmpty(cbbInvoiceForm.Text) || string.IsNullOrEmpty(txtReportName.Text) || string.IsNullOrEmpty(cbbInvoiceType.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }

            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153ReportService.GetAll().Any(n => n.InvoiceForm == int.Parse(cbbInvoiceForm.Value.ToString()) && n.InvoiceTypeID == (Guid)cbbInvoiceType.Value && n.InvoiceTemplate == txtInvoiceTypeCode.Text && n.InvoiceSeries == txtInvoiceSeries.Text))
                {
                    MSG.Error("Ký hiệu hóa đơn đã tồn tại");
                    return false;
                }
                if (cbbInvoiceType.SelectedRow != null)
                {
                    InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                    if (ivt.InvoiceTypeCode.Contains("01/ hoặc 02/"))
                    {
                        if (!(txtInvoiceTypeCode.Text.Split('/')[0].Contains("01") || txtInvoiceTypeCode.Text.Split('/')[0].Contains("02")))
                        {
                            MSG.Error("Sai định dạng mẫu số hóa đơn");
                            return false;
                        }
                    }
                }
            }
            else
            {
                if (_ITT153ReportService.GetAll().Any(n => n.ID != _select.ID && n.InvoiceForm == int.Parse(cbbInvoiceForm.Value.ToString()) && n.InvoiceTypeID == (Guid)cbbInvoiceType.Value && n.InvoiceTemplate == txtInvoiceTypeCode.Text && n.InvoiceSeries == txtInvoiceSeries.Text))
                {
                    MSG.Error("Ký hiệu hóa đơn đã tồn tại");
                    return false;
                }
                if (cbbInvoiceType.SelectedRow != null)
                {
                    InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                    if (ivt.InvoiceTypeCode.Contains("01/ hoặc 02/"))
                    {
                        if (!(txtInvoiceTypeCode.Text.Split('/')[0].Contains("01") || txtInvoiceTypeCode.Text.Split('/')[0].Contains("02")))
                        {
                            MSG.Error("Sai định dạng mẫu số hóa đơn");
                            return false;
                        }
                    }
                }

            }
            if (int.Parse(cbbInvoiceForm.Value.ToString()) == 2)
            {
                if (txtInvoiceSeries.Text[txtInvoiceSeries.Text.Length - 1] != 'E')
                {
                    MSG.Warning("Ký hiệu hóa đơn không đúng với hình thức hóa đơn");
                    return false;
                }
            }
            else if (int.Parse(cbbInvoiceForm.Value.ToString()) == 0)
            {
                if (txtInvoiceSeries.Text[txtInvoiceSeries.Text.Length - 1] != 'T')
                {
                    MSG.Warning("Ký hiệu hóa đơn không đúng với hình thức hóa đơn");
                    return false;
                }
            }
            else if (int.Parse(cbbInvoiceForm.Value.ToString()) == 1)
            {
                if (txtInvoiceSeries.Text[txtInvoiceSeries.Text.Length - 1] != 'P')
                {
                    MSG.Warning("Ký hiệu hóa đơn không đúng với hình thức hóa đơn");
                    return false;
                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153ReportService.GetAll().Count(n => n.ReportName == txtReportName.Text) > 0)
                {
                    MSG.Error("Tên mẫu hóa đơn đã tồn tại");
                    return false;
                }
            }
            else
            {
                if (txtReportName.Text != _select.ReportName)
                    if (_ITT153ReportService.GetAll().Count(n => n.InvoiceTemplate == txtInvoiceTypeCode.Text) > 0)
                    {
                        MSG.Error("Tên mẫu hóa đơn đã tồn tại");
                        return false;
                    }

            }

            if (txtInvoiceSeries.Text.Length == 6)
            {
                if (txtInvoiceSeries.Text[0] < 'A' || txtInvoiceSeries.Text[0] > 'Z' || txtInvoiceSeries.Text[1] < 'A' || txtInvoiceSeries.Text[1] > 'Z')
                {
                    MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                    return false;
                }
                if (txtInvoiceSeries.Text[2] != '/')
                {
                    MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                    return false;
                }
                if (!CheckIsNumber(txtInvoiceSeries.Text.Substring(3, 2)))
                {
                    MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                    return false;
                }
            }
            else if (txtInvoiceSeries.Text.Length == 8)
            {
                if (!CheckIsNumber(txtInvoiceSeries.Text.Substring(0, 2)) || !CheckIsNumber(txtInvoiceSeries.Text.Substring(5, 2)))
                {
                    MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                    return false;
                }
                if (txtInvoiceSeries.Text[2] < 'A' || txtInvoiceSeries.Text[2] > 'Z' || txtInvoiceSeries.Text[3] < 'A' || txtInvoiceSeries.Text[3] > 'Z')
                {
                    MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                    return false;
                }
                if (txtInvoiceSeries.Text[4] != '/')
                {
                    MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                    return false;
                }
            }
            else
            {
                MSG.Warning("Nhập sai định dạng ký hiệu hóa đơn vui lòng nhập lại");
                return false;
            }
            return kq;
        }

        private void Delete()
        {
            UltraGridRow row = uGrid.Rows[uGrid.Rows.Count - 1];
            row.Delete(false);
            uGrid.Update();
        }
        private void EditorValueChange(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            if (cbbInvoiceType.SelectedRow != null)
            {
                InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                if (ivt.InvoiceTypeCode.Contains("01/ hoặc 02/"))
                {
                    //txtInvoiceTypeCode.Value = "01" + txtCopyNumber.Value.ToString() + "/" + "00" + txtTempSortOrder.Value;
                }
                else txtInvoiceTypeCode.Value = ivt.InvoiceTypeCode + txtCopyNumber.Value.ToString() + "/" + txtTempSortOrder.Value.ToString().PadLeft(3, '0');
            }
            int count = lstTT153ReportDetail.Count();
            if (count + 1 == int.Parse(txtCopyNumber.Value.ToString()))
            {
                count++;
                if (count == 1)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên " + count,
                        Purpose = "Lưu",
                        CodeColor = ColorTranslator.ToHtml(color1)
                    });
                }
                else if (count == 2)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên " + count,
                        Purpose = "Giao cho người mua",
                        CodeColor = ColorTranslator.ToHtml(color2)
                    });
                }
                else if (count == 3)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên " + count,
                        Purpose = "Sử dụng nội bộ",
                        CodeColor = ColorTranslator.ToHtml(color3)
                    });
                }
                else
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên " + count,
                        Purpose = "",
                        CodeColor = ColorTranslator.ToHtml(color4)
                    });
            }
            else if (count - 1 == int.Parse(txtCopyNumber.Value.ToString()))
            {
                Delete();
            }
            else
                lstTT153ReportDetail = Create(int.Parse(txtCopyNumber.Value.ToString()), true);

            LoaduGrid();

        }
        public List<TT153ReportDetail> Create(int solien, bool isNull)
        {
            if (isNull)
            {
                lstTT153ReportDetail = new List<TT153ReportDetail>();
                if (solien == 1)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 1",
                        Purpose = "Lưu",
                        CodeColor = ColorTranslator.ToHtml(color1)
                    });
                }
                if (solien == 2)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 1",
                        Purpose = "Lưu",
                        CodeColor = ColorTranslator.ToHtml(color1)
                    });
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 2",
                        Purpose = "Giao cho người mua",
                        CodeColor = ColorTranslator.ToHtml(color2)
                    });
                }
                if (solien == 3)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 1",
                        Purpose = "Lưu",
                        CodeColor = ColorTranslator.ToHtml(color1)
                    });
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 2",
                        Purpose = "Giao cho người mua",
                        CodeColor = ColorTranslator.ToHtml(color2)
                    });
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 3",
                        Purpose = "Sử dụng nội bộ",
                        CodeColor = ColorTranslator.ToHtml(color3)
                    });
                }
                if (solien > 3)
                {
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 1",
                        Purpose = "Lưu",
                        CodeColor = ColorTranslator.ToHtml(color1)
                    });
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 2",
                        Purpose = "Giao cho người mua",
                        CodeColor = ColorTranslator.ToHtml(color2)
                    });
                    lstTT153ReportDetail.Add(new TT153ReportDetail
                    {
                        CopyNumberName = "Liên 3",
                        Purpose = "Sử dụng nội bộ",
                        CodeColor = ColorTranslator.ToHtml(color3)
                    });
                    for (int i = 4; i < solien + 1; i++)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = "",
                            CodeColor = ColorTranslator.ToHtml(color4)
                        });
                    }
                }
                return lstTT153ReportDetail;
            }
            else
            {
                lstTT153ReportDetail = new List<TT153ReportDetail>();
                for (int i = 1; i <= _select.CopyNumber; i++)
                {
                    if (i == 1)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose1,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor1))
                        });
                    }
                    else if (i == 2)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose2,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor2))
                        });
                    }
                    else if (i == 3)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose3,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor3))
                        });
                    }
                    else if (i == 4)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose4,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor4))
                        });
                    }
                    else if (i == 5)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose5,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor5))
                        });
                    }
                    else if (i == 6)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose6,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor6))
                        });
                    }
                    else if (i == 7)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose7,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor7))
                        });
                    }
                    else if (i == 8)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose8,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor8))
                        });
                    }
                    else if (i == 9)
                    {
                        lstTT153ReportDetail.Add(new TT153ReportDetail
                        {
                            CopyNumberName = "Liên " + i,
                            Purpose = _select.Purpose9,
                            CodeColor = ColorTranslator.ToHtml(Color.FromName(_select.CodeColor9))
                        });
                    }
                }
            }
            return lstTT153ReportDetail;
        }
        private void EditorValueChange2(object sender, EventArgs e)
        {
            if (cbbInvoiceType.SelectedRow != null)
            {

                InvoiceType ivt = _IInvoiceTypeService.Getbykey((Guid)cbbInvoiceType.Value);
                if (ivt.InvoiceTypeCode.Contains("01/ hoặc 02/"))
                {
                    //txtInvoiceTypeCode.Value = "01/";
                    //txtInvoiceTypeCode.Enabled = true;
                }
                else
                {
                    //txtInvoiceTypeCode.Enabled = false;
                    txtInvoiceTypeCode.Value = ivt.InvoiceTypeCode + txtCopyNumber.Value.ToString() + "/" + txtTempSortOrder.Value.ToString().PadLeft(3,'0');
                }
            }

            btnSave.Enabled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            TT153Report temp;
            #region Fill dữ liệu từ control vào obj
            if (_statusForm == ConstFrm.optStatusForm.Add)
                temp = new TT153Report();
            else
                temp = _ITT153ReportService.Getbykey(_select.ID);
            temp = ObjandGUI(temp, true);
            #endregion
            #region CSDL
            _ITT153ReportService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153ReportService.CreateNew(temp);
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit || _statusForm == ConstFrm.optStatusForm.View)
            {
                _ITT153ReportService.Update(temp);
            }
            try
            {
                _ITT153ReportService.CommitTran();
            }
            catch
            {
                _ITT153ReportService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu.");
                return;
            }

            #endregion
            #region xử lý form, kết thúc form
            Utils.ClearCacheByType<TT153Report>();
            List<TT153Report> lst = _ITT153ReportService.GetAll();
            _ITT153ReportService.UnbindSession(lst);
            Utils.ClearCacheByType<TT153ReportDetail>();
            IsClose = true;
            MSG.Information("Lưu thành công");
            //_select = temp;
            //btnSave.Enabled = false;
            //_statusForm = ConstFrm.optStatusForm.View;
            this.Close();
            #endregion
        }

        public void Print(Guid ID)
        {
            ITT153InvoiceTemplateService InvoiceTempSrv = IoC.Resolve<ITT153InvoiceTemplateService>();
            TT153InvoiceTemplate tt153InvoiceTemp = InvoiceTempSrv.Getbykey(ID);
            if (tt153InvoiceTemp == null) return;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\Frm\\FPrint\\Template\\{1}", path, tt153InvoiceTemp.InvoiceTempPath);

            PerpetuumSoft.Reporting.Components.ReportManager rm = new PerpetuumSoft.Reporting.Components.ReportManager();
            PerpetuumSoft.Reporting.Components.FileReportSlot frs = new PerpetuumSoft.Reporting.Components.FileReportSlot();
            rm.Reports.Add(frs);
            frs.FilePath = filePath;
            ReportForm<int> f = new ReportForm<int>(frs, "");
            f.AddDatasource("Data", _select);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show(this);

        }
        private void btnView_Click(object sender, EventArgs e)
        {
            if (cbbInvoiceTemplate.Value != null && (Guid)cbbInvoiceTemplate.Value != Guid.Empty)
            {
                Print((Guid)cbbInvoiceTemplate.Value);
            }
            else
            {
                MSG.Warning("Bạn chưa chọn dựa trên mẫu hóa đơn nào." + " \n Vui lòng chọn và thử  lại.");
            }
        }

        private void cbbInvoiceForm_Validated(object sender, EventArgs e)
        {
            if (cbbInvoiceForm.Value != null)
                if ((int)cbbInvoiceForm.Value == 2)
                {
                    txtCopyNumber.Value = 0;
                }
                else
                {
                    txtCopyNumber.Value = 3;
                }
        }

        private void Value_Changed(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            btnSave.Enabled = true;
        }

        private void cbbInvoiceType_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }
    }
}