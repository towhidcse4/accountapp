﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinEditors;


namespace Accounting
{
    public partial class FSelectInvoice : CustormForm
    {
        #region Khởi tạo
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        private PPInvoice PPInvoice;
        private int statusForm;
        private bool p;
        private DateTime _startDate;
        public readonly ISAInvoiceService _ISAInvoiceService;
        public readonly IInvoiceTypeService _IInvoiceTypeService;
        public readonly ITT153DeletedInvoiceService _ITT153DeletedInvoiceService;
        public readonly ISABillService _ISABillService;
        public readonly ISAReturnService _ISAReturnService;
        public List<TT153DeleteSAInvoice> lstDeleteSAInvoice;
        public TT153DeletedInvoice deleteinvoice;
        public bool check = false;
        bool _StatusForm;
        #endregion
        public FSelectInvoice(TT153DeletedInvoice tT153DeletedInvoice, int StatusForm, bool p)
        {
            _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            lstDeleteSAInvoice = new List<TT153DeleteSAInvoice>();
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            _ISABillService = IoC.Resolve<ISABillService>();
            _ISAReturnService = IoC.Resolve<ISAReturnService>();
            _ITT153DeletedInvoiceService = IoC.Resolve<ITT153DeletedInvoiceService>();
            deleteinvoice = tT153DeletedInvoice;

            check = false;
            _startDate = DateTime.Now;

            // TODO: Complete member initialization
            this.statusForm = statusForm;
            this.p = p;
            InitializeComponent();
            //if (ConstFrm.optStatusForm.Add == StatusForm)
            //{
            //    Load();
            //}
            ObjandGUI(tT153DeletedInvoice, false);
            InitializeGUI();
            WaitingFrm.StopWaiting();
            cbbAboutTime.ConfigComboSelectTime(_lstItems, "Name");
            cbbAboutTime.SelectedRow = cbbAboutTime.Rows[4];
        }
        private void InitializeGUI()
        {
            LoadUGrid();
        }

        public void Load()
        {
            WaitingFrm.StartWaiting();
            lstDeleteSAInvoice = new List<TT153DeleteSAInvoice>();
            foreach (SAInvoice item in Utils.ListSAInvoice.Where(n => n.InvoiceNo != null && n.InvoiceNo != "" && n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime))
            {
                InvoiceType invoiceType = new InvoiceType();
                foreach (InvoiceType _item in Utils.ListInvoiceType)
                {
                    if (_item.ID == item.InvoiceTypeID)
                    {
                        invoiceType = _item;
                    }
                }
                bool T = false;
                foreach (TT153DeletedInvoice itemDelete in _ITT153DeletedInvoiceService.GetAll())
                {
                    if (itemDelete.InvoiceNo == item.InvoiceNo) T = true;
                }
                if (T && deleteinvoice.InvoiceNo == item.InvoiceNo)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = true,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
                else if (!T)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = false,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
            }
            foreach (SABill item in Utils.ListSABill.Where(n => n.InvoiceNo != null && n.InvoiceNo != "" && n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime))
            {
                InvoiceType invoiceType = new InvoiceType();
                foreach (InvoiceType _item in Utils.ListInvoiceType)
                {
                    if (_item.ID == item.InvoiceTypeID)
                    {
                        invoiceType = _item;
                    }
                }
                bool T = false;
                foreach (TT153DeletedInvoice itemDelete in _ITT153DeletedInvoiceService.GetAll())
                {
                    if (itemDelete.InvoiceNo == item.InvoiceNo) T = true;
                }
                if (T && deleteinvoice.InvoiceNo == item.InvoiceNo)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = true,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
                else if (!T)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = false,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
            }
            foreach (SAReturn item in Utils.ListSAReturn.Where(n => n.InvoiceNo != null && n.InvoiceNo != "" && n.TypeID == 340 && n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime))
            {
                InvoiceType invoiceType = new InvoiceType();
                foreach (InvoiceType _item in Utils.ListInvoiceType)
                {
                    if (_item.ID == item.InvoiceTypeID)
                    {
                        invoiceType = _item;
                    }
                }
                bool T = false;
                foreach (TT153DeletedInvoice itemDelete in _ITT153DeletedInvoiceService.GetAll())
                {
                    if (itemDelete.InvoiceNo == item.InvoiceNo) T = true;
                }
                if (T && deleteinvoice.InvoiceNo == item.InvoiceNo)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = true,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
                else if (!T)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = false,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
            }
            foreach (PPDiscountReturn item in Utils.ListPPDiscountReturn.Where(n => n.InvoiceNo != null && n.InvoiceNo != "" && n.TypeID == 220 && n.InvoiceDate >= dteDateFrom.DateTime && n.InvoiceDate <= dteDateTo.DateTime))
            {
                InvoiceType invoiceType = new InvoiceType();
                foreach (InvoiceType _item in Utils.ListInvoiceType)
                {
                    if (_item.ID == item.InvoiceTypeID)
                    {
                        invoiceType = _item;
                    }
                }
                bool T = false;
                foreach (TT153DeletedInvoice itemDelete in _ITT153DeletedInvoiceService.GetAll())
                {
                    if (itemDelete.InvoiceNo == item.InvoiceNo) T = true;
                }
                if (T && deleteinvoice.InvoiceNo == item.InvoiceNo)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = true,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType??0,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
                else if (!T)
                {
                    lstDeleteSAInvoice.Add(new TT153DeleteSAInvoice
                    {
                        Status = false,
                        SAInvoiceID = item.ID,
                        InvoiceNo = item.InvoiceNo,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceType = item.InvoiceType??0,
                        InvoiceTypeString = invoiceType.InvoiceTypeName,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceDate = item.InvoiceDate,
                        InvoiceTemplate = item.InvoiceTemplate,
                        AccountingObjectName = item.AccountingObjectName,
                    });
                }
            }
            lstDeleteSAInvoice = lstDeleteSAInvoice.OrderBy(n => n.InvoiceTemplate).OrderBy(n => n.InvoiceSeries).OrderByDescending(n => n.InvoiceNo).ToList();
            WaitingFrm.StopWaiting();
        }

        public void LoadUGrid()
        {
            uGrid.SetDataBinding(lstDeleteSAInvoice, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TT153DeleteSAInvoice_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            uGrid.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellAppearance.TextHAlign = HAlign.Center;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellAppearance.TextVAlign = VAlign.Middle;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            Load();
            LoadUGrid();
        }

        private void cbbAboutTime_ValueChanged_1(object sender, EventArgs e)
        {
            if (cbbAboutTime.SelectedRow != null)
            {
                var model = cbbAboutTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
                dteDateFrom.Value = dtBegin;
                dteDateTo.Value = dtEnd;
            }
        }
        public TT153DeletedInvoice ObjandGUI(TT153DeletedInvoice input, bool isGet)
        {
            if (isGet)
            {

                foreach (TT153DeleteSAInvoice item in lstDeleteSAInvoice)
                {
                    if (item.Status == true)
                    {
                        input.InvoiceNo = item.InvoiceNo;
                        input.InvoiceDate = item.InvoiceDate;
                        input.InvoiceSeries = item.InvoiceSeries;
                        foreach (InvoiceType _item in Utils.ListInvoiceType)
                        {
                            if (_item.ID == item.InvoiceTypeID)
                            {
                                input.InvoiceTypeID = _item.ID;
                            }
                        }
                        input.SAInvoiceID = item.SAInvoiceID;
                        input.AccountingObjectName = item.AccountingObjectName;
                    }
                }
            }
            else
            {
                foreach (TT153DeleteSAInvoice item in lstDeleteSAInvoice)
                {
                    if (item.InvoiceNo == input.InvoiceNo)
                    {
                        item.Status = true;
                    }
                }
            }
            return input;
        }
        private Boolean CheckError()
        {
            bool kq = true;
            if (lstDeleteSAInvoice.Count(n => n.Status == true) < 1)
            {
                MSG.Warning("Chưa chọn hóa đơn cần xóa.");
                return false;
            }
            if (lstDeleteSAInvoice.Count(n => n.Status == true) > 1)
            {
                MSG.Warning("Chỉ được chọn 1 một hóa đơn");
                return false;
            }
            //Check Error Chung
            //Check Error Riêng từng Form
            //if (string.IsNullOrEmpty(txtCopyNumber.Text) || string.IsNullOrEmpty(txtInvoiceSeries.Text)
            //    || string.IsNullOrEmpty(cbbInvoiceForm.Text) || string.IsNullOrEmpty(txtReportName.Text))
            //{
            //    MSG.Error(resSystem.MSG_System_03);
            //    return false;
            //}
            return kq;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check data
            if (!CheckError()) return;
            #endregion
            #region Fill dữ liệu
            ObjandGUI(deleteinvoice, true);
            #endregion
            check = true;
            Close();
        }
    }
}
