﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;


namespace Accounting
{
    public partial class FTT153DeletedInvoiceDetail : CustormForm
    {
        #region Khai báo
        public readonly IInvoiceTypeService _IInvoiceTypeService;
        public readonly ISAInvoiceService _ISAInvoiceService;
        public readonly ITT153DeletedInvoiceService _ITT153DeletedInvoiceService;
        public readonly ITT153DestructionInvoiceDetailService _ITT153DestructionInvoiceDetailService;
        public readonly ITT153LostInvoiceDetailService _ITT153LostInvoiceDetailService;
        public readonly ITT153ReportService _ITT153ReportService;
        TT153DeletedInvoice _select;
        int _statusForm;
        string linkFile;
        DateTime refTime;
        public PPInvoice PPInvoice { get; set; }
        #endregion
        #region Khởi tạo
        public FTT153DeletedInvoiceDetail(TT153DeletedInvoice temp, List<TT153DeletedInvoice> lstDeletedInvoice, int statusForm)
        {
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            _ITT153DeletedInvoiceService = IoC.Resolve<ITT153DeletedInvoiceService>();
            _ITT153LostInvoiceDetailService = IoC.Resolve<ITT153LostInvoiceDetailService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153DestructionInvoiceDetailService = IoC.Resolve<ITT153DestructionInvoiceDetailService>();
            InitializeComponent();
            _statusForm = statusForm;
            linkFile = null;
            this._select = temp;
            #region Thêm button vào dataeditor
            //var btnRefDateTime = new EditorButton("btnRefDateTime");
            //var appearance = new Infragistics.Win.Appearance
            //{
            //    Image = Properties.Resources.clock,
            //    ImageHAlign = Infragistics.Win.HAlign.Center,
            //    ImageVAlign = Infragistics.Win.VAlign.Middle
            //};
            //btnRefDateTime.Appearance = appearance;
            //btnRefDateTime.Key = "btnRefDateTime";
            //btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            //txtDate.ButtonsRight.Add(btnRefDateTime);
            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                refTime = DateTime.Now;
                btnDown.Enabled = false;
                temp = new TT153DeletedInvoice();
                btnSave.Enabled = true;
            }
            else
            {
                refTime = _select.Date;
                temp = _ITT153DeletedInvoiceService.Getbykey(temp.ID);
                ObjandGUI(temp, false);
                btnSave.Enabled = false;
            }
            InitializeGUI();
            WaitingFrm.StopWaiting();

        }
        #endregion
        #region Nghiệp vụ
        private void InitializeGUI()
        {

        }

        public TT153DeletedInvoice ObjandGUI(TT153DeletedInvoice input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.Date = Utils.GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
                input.No = txtNo.Text;
                input.Reason = txtReason.Text;
                input.AttachFileName = txtAttachFileName.Text;
                if (input.AttachFileName != null && input.AttachFileName != "")
                    if (linkFile != null && linkFile != "")
                        input.AttachFileContent = Utils.FileToByte(linkFile);
            }
            else
            {
                SAInvoice sAInvoice = _ISAInvoiceService.Getbykey(input.SAInvoiceID);
                if(sAInvoice == null)
                {
                    SABill sABill = Utils.ISABillService.Getbykey(input.SAInvoiceID);
                    if (sABill == null)
                    {
                        SAReturn sAReturn = Utils.ISAReturnService.Getbykey(input.SAInvoiceID);
                        if (sAReturn == null)
                        {
                            PPDiscountReturn pPDiscountReturn = Utils.IPPDiscountReturnService.Getbykey(input.SAInvoiceID);
                            if (pPDiscountReturn != null)
                            {
                                txtInvoiceType.Value = pPDiscountReturn.InvoiceTemplate;
                                txtAccountingObjectName.Value = pPDiscountReturn.AccountingObjectName;
                            }
                        }
                        else
                        {
                            txtInvoiceType.Value = sAReturn.InvoiceTemplate;
                            txtAccountingObjectName.Value = sAReturn.AccountingObjectName;
                        }
                    }
                    else
                    {
                        txtInvoiceType.Value = sABill.InvoiceTemplate;
                        txtAccountingObjectName.Value = sABill.AccountingObjectName;
                    }
                }
                else
                {
                    txtInvoiceType.Value = sAInvoice.InvoiceTemplate;
                    txtAccountingObjectName.Value = sAInvoice.AccountingObjectName;
                }

                txtDate.Value = input.Date;
                if (input.InvoiceDate != null)
                {
                    txtInvoiceDate.Value = input.InvoiceDate.Value.ToString("dd/MM/yyyy");
                }
                txtInvoiceNo.Value = input.InvoiceNo;
                txtInvoiceSeries.Value = input.InvoiceSeries;
                txtNo.Value = input.No;
                
                txtReason.Value = input.Reason;
                txtAttachFileName.Value = input.AttachFileName;

            }
            return input;
        }

        public Boolean CheckError()
        {
            bool kq = true;
            //Check Error Chung
            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtDate.Text) || string.IsNullOrEmpty(txtNo.Text))
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            if (string.IsNullOrEmpty(txtInvoiceNo.Text))
            {
                MSG.Warning("Chưa chọn hóa đơn xóa");
                return false;
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153DeletedInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                {
                    MSG.Error("Số quyết định đã tồn tại");
                    return false;
                }
            }
            else
            {
                if (txtNo.Text != _select.No)
                    if (_ITT153DeletedInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                    {
                        MSG.Error("Số quyết định đã tồn tại");
                        return false;
                    }

            }
            SAInvoice sAInvoice = _ISAInvoiceService.Getbykey(_select.SAInvoiceID);
            if (sAInvoice == null)
            {
                SABill sABill = Utils.ISABillService.Getbykey(_select.SAInvoiceID);
                if (sABill == null)
                {
                    SAReturn sAReturn = Utils.ISAReturnService.Getbykey(_select.SAInvoiceID);
                    if (sAReturn == null)
                    {
                        PPDiscountReturn pPDiscountReturn = Utils.IPPDiscountReturnService.Getbykey(_select.SAInvoiceID);
                        foreach (TT153DestructionInvoiceDetail item in _ITT153DestructionInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                        {
                            if (int.Parse(pPDiscountReturn.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(pPDiscountReturn.InvoiceNo) <= int.Parse(item.ToNo))
                            {
                                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                                return false;
                            }
                        }
                        foreach (TT153LostInvoiceDetail item in _ITT153LostInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                        {
                            if (int.Parse(pPDiscountReturn.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(pPDiscountReturn.InvoiceNo) <= int.Parse(item.ToNo))
                            {
                                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                                return false;
                            }
                        }
                    }
                    else
                    {
                        foreach (TT153DestructionInvoiceDetail item in _ITT153DestructionInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                        {
                            if (int.Parse(sAReturn.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(sAReturn.InvoiceNo) <= int.Parse(item.ToNo))
                            {
                                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                                return false;
                            }
                        }
                        foreach (TT153LostInvoiceDetail item in _ITT153LostInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                        {
                            if (int.Parse(sAReturn.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(sAReturn.InvoiceNo) <= int.Parse(item.ToNo))
                            {
                                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (TT153DestructionInvoiceDetail item in _ITT153DestructionInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                    {
                        if (int.Parse(sABill.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(sABill.InvoiceNo) <= int.Parse(item.ToNo))
                        {
                            MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                            return false;
                        }
                    }
                    foreach (TT153LostInvoiceDetail item in _ITT153LostInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                    {
                        if (int.Parse(sABill.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(sABill.InvoiceNo) <= int.Parse(item.ToNo))
                        {
                            MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                            return false;
                        }
                    }
                }
            }
            else
            {
                foreach (TT153DestructionInvoiceDetail item in _ITT153DestructionInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                {
                    if (int.Parse(sAInvoice.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(sAInvoice.InvoiceNo) <= int.Parse(item.ToNo))
                    {
                        MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                        return false;
                    }
                }
                foreach (TT153LostInvoiceDetail item in _ITT153LostInvoiceDetailService.GetAllBySAInvoiceID(_select.SAInvoiceID))
                {
                    if (int.Parse(sAInvoice.InvoiceNo) >= int.Parse(item.FromNo) && int.Parse(sAInvoice.InvoiceNo) <= int.Parse(item.ToNo))
                    {
                        MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy hoặc báo mất, cháy, hỏng");
                        return false;
                    }
                }
            }

            return kq;
        }

        private void btnSelectInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                FSelectInvoice f = new FSelectInvoice(_select, _statusForm, true);
                //f.FormClosed += new FormClosedEventHandler(fSelectPPServices_FormClosed);
                f.ShowDialog(this);
                if (f.check)
                {
                    txtInvoiceNo.Value = this._select.InvoiceNo;
                    if (this._select.InvoiceDate != null)
                    {
                        txtInvoiceDate.Value = this._select.InvoiceDate.Value.ToString("dd/MM/yyyy");
                    }
                    txtAccountingObjectName.Value = this._select.AccountingObjectName;
                    txtInvoiceSeries.Value = this._select.InvoiceSeries;
                    SAInvoice sAInvoice = _ISAInvoiceService.Getbykey(this._select.SAInvoiceID);
                    if (sAInvoice == null)
                    {
                        SABill sABill = Utils.ISABillService.Getbykey(this._select.SAInvoiceID);
                        if (sABill == null)
                        {
                            SAReturn sAReturn = Utils.ISAReturnService.Getbykey(this._select.SAInvoiceID);
                            if (sAReturn == null)
                            {
                                PPDiscountReturn pPDiscountReturn = Utils.IPPDiscountReturnService.Getbykey(this._select.SAInvoiceID);
                                txtInvoiceType.Value = pPDiscountReturn.InvoiceTemplate;
                            }
                            else
                            {
                                txtInvoiceType.Value = sAReturn.InvoiceTemplate;
                            }
                        }
                        else
                        {
                            txtInvoiceType.Value = sABill.InvoiceTemplate;
                        }
                    }
                    else
                    {
                        txtInvoiceType.Value = sAInvoice.InvoiceTemplate;
                    }

                    btnSave.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Check lỗi
            if (!CheckError()) return;
            #endregion
            #region Fill giữ liệu
            _select = ObjandGUI(_select, true);
            #endregion
            #region CSDL
            _ITT153DeletedInvoiceService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153DeletedInvoiceService.CreateNew(_select);
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit || _statusForm == ConstFrm.optStatusForm.View)
            {
                _ITT153DeletedInvoiceService.Update(_select);
            }
            try
            {
                _ITT153DeletedInvoiceService.CommitTran();
            }
            catch (Exception ex)
            {
                _ITT153DeletedInvoiceService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu.");
                return;

            }
            #endregion
            #region xử lý form, kết thúc form
            Utils.ClearCacheByType<TT153DeletedInvoice>();
            List<TT153DeletedInvoice> lst = _ITT153DeletedInvoiceService.GetAll();
            _ITT153DeletedInvoiceService.UnbindSession(lst);
            Utils.ClearCacheByType<ITT153DeletedInvoiceService>();
            IsClose = true;

            MSG.Information("Lưu thành công");
            this.Close();
            //isClose = false;
            #endregion
        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog();

            var result = openFile.ShowDialog(this);

            // giới hạn dung lượng file cho phép là 5 MB
            // giới hạn dung lượng file cho phép là 10 MB
            linkFile = openFile.FileName;
            if (linkFile != null && linkFile != "")
            {
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 10240)
                {
                    MSG.Error("Dung lượng file vượt quá mức cho phép (10MB)");
                    return;
                }
                String[] Name = openFile.FileName.Split('\\');
                txtAttachFileName.Value = Name.Last().ToString();
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (_select.AttachFileContent == null || _select.AttachFileName == null || _select.AttachFileName == "")
            {
                MSG.Warning("không có file để tải về");
                return;
            }
            var openfolder = new FolderBrowserDialog();
            openfolder.Description = "Chọn thư mục lưu file";
            openfolder.ShowNewFolderButton = false;
            openfolder.ShowDialog(this);
            string linkFileSave = openfolder.SelectedPath;
            if (linkFileSave != null && linkFileSave != "")
            {
                Utils.databaseToFile(_select.AttachFileContent, linkFileSave + "\\" + _select.AttachFileName);
                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDown_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Tải xuống file đính kèm", btnDown);
        }

        private void ultraGroupBox1_Click(object sender, EventArgs e)
        {

        }
        #region TimeForm
        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                var f = new MsgRefDateTime(refTime);
                f.Text = "Thiết lập thời gian";
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            btnSave.Enabled = true;
            refTime = frm.RefDateTime;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
        }
        #endregion

        #endregion

        private void btnBrowser_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Chọn file đính kèm", btnBrowser);
        }

        private void Value_Change(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
        }
    }
}