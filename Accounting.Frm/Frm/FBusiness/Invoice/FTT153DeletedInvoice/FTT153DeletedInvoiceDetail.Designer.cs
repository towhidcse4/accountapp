﻿namespace Accounting
{
    partial class FTT153DeletedInvoiceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FTT153DeletedInvoiceDetail));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDown = new Infragistics.Win.Misc.UltraButton();
            this.btnSelectInvoice = new Infragistics.Win.Misc.UltraButton();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceSeries = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtInvoiceDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.btnBrowser = new Infragistics.Win.Misc.UltraButton();
            this.txtAttachFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtInvoiceNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Location = new System.Drawing.Point(1, 222);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(845, 49);
            this.ultraGroupBox3.TabIndex = 3;
            // 
            // btnClose
            // 
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            this.btnClose.Appearance = appearance1;
            this.btnClose.Location = new System.Drawing.Point(756, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 59;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSave
            // 
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnSave.Appearance = appearance2;
            this.btnSave.Location = new System.Drawing.Point(675, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 58;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraGroupBox1
            // 
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            this.ultraGroupBox1.Appearance = appearance3;
            this.ultraGroupBox1.Controls.Add(this.btnDown);
            this.ultraGroupBox1.Controls.Add(this.btnSelectInvoice);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.txtInvoiceSeries);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.txtInvoiceType);
            this.ultraGroupBox1.Controls.Add(this.txtInvoiceDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.btnBrowser);
            this.ultraGroupBox1.Controls.Add(this.txtAttachFileName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.txtInvoiceNo);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.txtDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(4, -9);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(842, 225);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.Click += new System.EventHandler(this.ultraGroupBox1_Click);
            // 
            // btnDown
            // 
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            this.btnDown.Appearance = appearance4;
            this.btnDown.Location = new System.Drawing.Point(765, 92);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(26, 22);
            this.btnDown.TabIndex = 70;
            this.btnDown.Tag = "";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            this.btnDown.MouseHover += new System.EventHandler(this.btnDown_MouseHover);
            // 
            // btnSelectInvoice
            // 
            this.btnSelectInvoice.Location = new System.Drawing.Point(387, 119);
            this.btnSelectInvoice.Name = "btnSelectInvoice";
            this.btnSelectInvoice.Size = new System.Drawing.Size(34, 23);
            this.btnSelectInvoice.TabIndex = 69;
            this.btnSelectInvoice.Text = "...";
            this.btnSelectInvoice.Click += new System.EventHandler(this.btnSelectInvoice_Click);
            // 
            // txtAccountingObjectName
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.txtAccountingObjectName.Appearance = appearance5;
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Enabled = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(212, 179);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(579, 22);
            this.txtAccountingObjectName.TabIndex = 68;
            this.txtAccountingObjectName.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel8
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance6;
            this.ultraLabel8.Location = new System.Drawing.Point(20, 180);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel8.TabIndex = 67;
            this.ultraLabel8.Text = "Đối tượng";
            // 
            // txtInvoiceSeries
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.txtInvoiceSeries.Appearance = appearance7;
            this.txtInvoiceSeries.AutoSize = false;
            this.txtInvoiceSeries.Enabled = false;
            this.txtInvoiceSeries.Location = new System.Drawing.Point(545, 150);
            this.txtInvoiceSeries.Name = "txtInvoiceSeries";
            this.txtInvoiceSeries.Size = new System.Drawing.Size(246, 22);
            this.txtInvoiceSeries.TabIndex = 66;
            this.txtInvoiceSeries.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel4
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance8;
            this.ultraLabel4.Location = new System.Drawing.Point(440, 151);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel4.TabIndex = 65;
            this.ultraLabel4.Text = "Ký hiệu hóa đơn";
            // 
            // txtInvoiceType
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.txtInvoiceType.Appearance = appearance9;
            this.txtInvoiceType.AutoSize = false;
            this.txtInvoiceType.Enabled = false;
            this.txtInvoiceType.Location = new System.Drawing.Point(212, 150);
            this.txtInvoiceType.Name = "txtInvoiceType";
            this.txtInvoiceType.Size = new System.Drawing.Size(209, 22);
            this.txtInvoiceType.TabIndex = 63;
            this.txtInvoiceType.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // txtInvoiceDate
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.txtInvoiceDate.Appearance = appearance10;
            this.txtInvoiceDate.AutoSize = false;
            this.txtInvoiceDate.Enabled = false;
            this.txtInvoiceDate.Location = new System.Drawing.Point(545, 122);
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.Size = new System.Drawing.Size(246, 22);
            this.txtInvoiceDate.TabIndex = 62;
            this.txtInvoiceDate.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel3
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance11;
            this.ultraLabel3.Location = new System.Drawing.Point(442, 122);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel3.TabIndex = 61;
            this.ultraLabel3.Text = "Ngày hóa đơn";
            // 
            // btnBrowser
            // 
            appearance12.Image = global::Accounting.Properties.Resources.iAdd;
            this.btnBrowser.Appearance = appearance12;
            this.btnBrowser.Location = new System.Drawing.Point(726, 92);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(35, 22);
            this.btnBrowser.TabIndex = 60;
            this.btnBrowser.Click += new System.EventHandler(this.btnBrowser_Click);
            this.btnBrowser.MouseHover += new System.EventHandler(this.btnBrowser_MouseHover);
            // 
            // txtAttachFileName
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.txtAttachFileName.Appearance = appearance13;
            this.txtAttachFileName.AutoSize = false;
            this.txtAttachFileName.Enabled = false;
            this.txtAttachFileName.Location = new System.Drawing.Point(212, 92);
            this.txtAttachFileName.Name = "txtAttachFileName";
            this.txtAttachFileName.Size = new System.Drawing.Size(508, 22);
            this.txtAttachFileName.TabIndex = 50;
            this.txtAttachFileName.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel7
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance14;
            this.ultraLabel7.Location = new System.Drawing.Point(20, 88);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel7.TabIndex = 49;
            this.ultraLabel7.Text = "Tệp đính kèm";
            // 
            // ultraLabel6
            // 
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance15;
            this.ultraLabel6.Location = new System.Drawing.Point(20, 121);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel6.TabIndex = 48;
            this.ultraLabel6.Text = "Số hóa đơn";
            // 
            // txtInvoiceNo
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.txtInvoiceNo.Appearance = appearance16;
            this.txtInvoiceNo.AutoSize = false;
            this.txtInvoiceNo.Enabled = false;
            this.txtInvoiceNo.Location = new System.Drawing.Point(212, 120);
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(166, 22);
            this.txtInvoiceNo.TabIndex = 47;
            this.txtInvoiceNo.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // txtReason
            // 
            appearance17.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance17;
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(212, 50);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(579, 36);
            this.txtReason.TabIndex = 46;
            this.txtReason.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel2
            // 
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance18;
            this.ultraLabel2.Location = new System.Drawing.Point(20, 58);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel2.TabIndex = 45;
            this.ultraLabel2.Text = "Lý do xóa";
            // 
            // txtNo
            // 
            appearance19.TextVAlignAsString = "Middle";
            this.txtNo.Appearance = appearance19;
            this.txtNo.AutoSize = false;
            this.txtNo.Location = new System.Drawing.Point(547, 22);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(244, 22);
            this.txtNo.TabIndex = 40;
            this.txtNo.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel9
            // 
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance20;
            this.ultraLabel9.Location = new System.Drawing.Point(442, 23);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(85, 22);
            this.ultraLabel9.TabIndex = 39;
            this.ultraLabel9.Text = "Số quyết định(*)";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(212, 23);
            this.txtDate.Name = "txtDate";
            this.txtDate.Nullable = false;
            this.txtDate.Size = new System.Drawing.Size(168, 21);
            this.txtDate.TabIndex = 38;
            this.txtDate.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel5
            // 
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance21;
            this.ultraLabel5.Location = new System.Drawing.Point(20, 151);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel5.TabIndex = 30;
            this.ultraLabel5.Text = "Mẫu số hóa đơn";
            // 
            // ultraLabel1
            // 
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance22;
            this.ultraLabel1.Location = new System.Drawing.Point(20, 24);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Ngày lập(*)";
            // 
            // FTT153DeletedInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 276);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(863, 315);
            this.MinimizeBox = false;
            this.Name = "FTT153DeletedInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xóa hóa đơn";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDate;
        private Infragistics.Win.Misc.UltraButton btnBrowser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAttachFileName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceSeries;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtInvoiceDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraButton btnSelectInvoice;
        private Infragistics.Win.Misc.UltraButton btnDown;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}