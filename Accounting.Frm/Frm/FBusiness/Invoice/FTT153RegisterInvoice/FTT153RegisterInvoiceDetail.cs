﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;


namespace Accounting
{
    public partial class FTT153RegisterInvoiceDetail : CustormForm
    {
        #region Khai báo
        public readonly IInvoiceTypeService _IInvoiceTypeService;
        public readonly ITT153InvoiceTemplateService _ITT153InvoiceTemplate;
        public readonly ITT153ReportService _ITT153ReportService;
        private List<TT153RegisterInvoiceDetail> lstTT153RegisterInvoiceDetail = new List<TT153RegisterInvoiceDetail>();
        public readonly ITT153RegisterInvoiceDetailService _ITT153RegisterInvoiceDetailService;
        public readonly ITT153RegisterInvoiceService _ITT153RegisterInvoiceService;
        public readonly ITT153PublishInvoiceDetailService _ITT153PublishInvoiceDetailService;
        public readonly ISystemOptionService _ISystemOptionService;
        TT153RegisterInvoice _select;
        List<TT153RegisterReportDetail> lstRRD = new List<TT153RegisterReportDetail>();
        List<TT153Report> lstTT153Report;
        int _statusForm;
        string linkFile;
        DateTime refTime;
        #endregion
        #region Khởi tạo 
        public FTT153RegisterInvoiceDetail(TT153RegisterInvoice temp, List<TT153RegisterInvoice> lstTT153RegisterInvoice, int statusForm)
        {
            _ITT153RegisterInvoiceDetailService= IoC.Resolve<ITT153RegisterInvoiceDetailService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153RegisterInvoiceService = IoC.Resolve<ITT153RegisterInvoiceService>();
            _ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            lstTT153Report = _ITT153ReportService.GetAll();
            linkFile = null;

            InitializeComponent();
            _select = temp;
            _statusForm = statusForm;
            cbbStatus.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;

            #region Thêm button vào dataeditor
            //var btnRefDateTime = new EditorButton("btnRefDateTime");
            //var appearance = new Infragistics.Win.Appearance
            //{
            //    Image = Properties.Resources.clock,
            //    ImageHAlign = Infragistics.Win.HAlign.Center,
            //    ImageVAlign = Infragistics.Win.VAlign.Middle
            //};
            //btnRefDateTime.Appearance = appearance;
            //btnRefDateTime.Key = "btnRefDateTime";
            //btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            //txtDate.ButtonsRight.Add(btnRefDateTime);
            #endregion

            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                txtSigner.Value = _ISystemOptionService.Getbykey(21).Data;
                refTime = DateTime.Now;
                btnDown.Enabled = false;
                foreach (TT153Report item in lstTT153Report)
                {
                    if (_ITT153RegisterInvoiceDetailService.GetAll().Count(n => n.TT153ReportID == item.ID) == 0)
                        lstRRD.Add(new TT153RegisterReportDetail
                        {
                            Status = false,
                            ID = item.ID,
                            InvoiceTemplate = item.InvoiceTemplate,
                            InvoiceForm = item.InvoiceFormString,
                            InvoiceType = item.InvoiceTypeString,
                            InvoiceSeries = item.InvoiceSeries,
                            Purpose = ""
                        });
                }
                btnSave.Enabled = true;
            }
            else
            {
                refTime = _select.Date;
                foreach (TT153Report item in lstTT153Report)
                {
                    if (_ITT153RegisterInvoiceDetailService.GetAll().Count(n => n.TT153ReportID == item.ID) == 0 || _select.TT153RegisterInvoiceDetail.Count(n => n.TT153ReportID == item.ID) > 0)
                    lstRRD.Add(new TT153RegisterReportDetail
                    {
                        Status = false,
                        ID = item.ID,
                        InvoiceTemplate = item.InvoiceTemplate,
                        InvoiceForm = item.InvoiceFormString,
                        InvoiceType = item.InvoiceTypeString,
                        InvoiceSeries = item.InvoiceSeries,
                        Purpose = ""
                    });
                }
                foreach (TT153RegisterReportDetail item_Detail in lstRRD)
                {
                    foreach(TT153RegisterInvoiceDetail it in temp.TT153RegisterInvoiceDetail)
                    {
                        if(it.TT153ReportID== item_Detail.ID)
                        {
                            item_Detail.Status = true;
                            item_Detail.Purpose = it.Purpose;
                        }
                    }
                }
                ObjandGUI(temp, false);
                btnSave.Enabled = false;
            }
            InitializeGUI();
            WaitingFrm.StopWaiting();
        }
        #endregion
        private void InitializeGUI()
        {
            loaduGrid();
        }

        public void loaduGrid()
        {
            uGrid.SetDataBinding(lstRRD, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TT153RegisterReportDetail_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            bool T = false;
            foreach (TT153RegisterInvoiceDetail item in _select.TT153RegisterInvoiceDetail)
            {
                if (_ITT153PublishInvoiceDetailService.GetAll().Count(n => n.TT153ReportID == item.TT153ReportID) > 0)
                {
                    T = true;
                }

            }
            if (_statusForm == ConstFrm.optStatusForm.Add ||( _statusForm == ConstFrm.optStatusForm.View && !T ))
            {
                UltraGridColumn ugc = band.Columns["Status"];
                ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                ugc.CellActivation = Activation.AllowEdit;
                ugc.Header.Fixed = true;
                uGrid.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            }
            uGrid.DisplayLayout.Bands[0].Columns["Purpose"].CellClickAction = CellClickAction.EditAndSelectText;
        }

        private TT153RegisterInvoice ObjandGUI(TT153RegisterInvoice input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.Date = Utils.GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
                input.No = txtNo.Text;
                input.Description = txtDescription.Text;
                input.Signer = txtSigner.Text;
                input.Status = int.Parse(cbbStatus.Value.ToString());
                input.AttachFileName = txtAttachFileName.Text;
                if (input.AttachFileName != null && input.AttachFileName != "")
                    if (linkFile != null && linkFile != "")
                        input.AttachFileContent = Utils.FileToByte(linkFile);
                List<int> index = new List<int>();
                if (input.TT153RegisterInvoiceDetail.Count > 0)
                {
                    foreach (TT153RegisterReportDetail item in lstRRD)
                    {
                        int i;
                        bool check = false;
                        for (i = 0; i < input.TT153RegisterInvoiceDetail.Count; i++)
                        {
                            if (input.TT153RegisterInvoiceDetail[i].TT153ReportID == item.ID)
                            {
                                check = true;
                                break;
                            }
                        }
                        if(check== true)
                        {
                            if (item.Status == true)
                            {
                                input.TT153RegisterInvoiceDetail[i].Purpose = item.Purpose;
                            }
                            else
                            {
                                input.TT153RegisterInvoiceDetail.RemoveAt(i);
                            }
                        }
                        else
                        {
                            TT153RegisterInvoiceDetail temp = new TT153RegisterInvoiceDetail();
                            if (item.Status == true)
                            {
                                temp.TT153RegisterInvoiceID=  input.ID;
                                temp.Purpose = item.Purpose;
                                temp.TT153ReportID = item.ID;
                                input.TT153RegisterInvoiceDetail.Add(temp);
                            }
                        }
                    }
                }
                else
                {
                    foreach(TT153RegisterReportDetail item in lstRRD)
                    {
                        TT153RegisterInvoiceDetail temp = new TT153RegisterInvoiceDetail();
                        if (item.Status == true)
                        {
                            temp.TT153RegisterInvoiceID = input.ID;
                            temp.Purpose = item.Purpose;
                            temp.TT153ReportID = item.ID;
                            input.TT153RegisterInvoiceDetail.Add(temp);
                        }
                    }
                }
                
            }
            else
            {
                txtDate.Value = input.Date;
                txtNo.Value = input.No;
                txtDescription.Value = input.Description;
                txtSigner.Value = input.Signer;
                cbbStatus.Value = input.Status;
                txtAttachFileName.Value = input.AttachFileName;
            }
            return input;
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Boolean CheckError()
        {
            bool kq = true;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153RegisterInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                {
                    MSG.Error("Số đăng ký đã tồn tại");
                    return false;
                }
            }
            else
            {
                if (txtNo.Text != _select.No)
                    if (_ITT153RegisterInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                    {
                        MSG.Error("Số đăng ký đã tồn tại");
                        return false;
                    }

            }
            if (lstRRD.Count(n => n.Status) == 0)
            {
                MSG.Error("Chưa có mẫu hóa đơn nào được chọn để đăng kí sử dụng");
                return false;
            }
            return kq;
        }

        private void ChoseFile(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog();
            //{
            //    //InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),

            //};
            var result = openFile.ShowDialog(this);
            // giới hạn dung lượng file cho phép là 10 MB
            linkFile = openFile.FileName;
            if (linkFile != null && linkFile != "")
            {
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 10240)
                {
                    MSG.Error("Dung lượng file vượt quá mức cho phép (10MB)");
                    return;
                }
                String[] Name = openFile.FileName.Split('\\');
                txtAttachFileName.Value = Name.Last().ToString();
            }
        }

        private void btn_SaveClick(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            TT153RegisterInvoice temp;
            //List<TT153RegisterInvoiceDetail> tempDetail= new List<TT153RegisterInvoiceDetail>();
            #region Fill dữ liệu từ control vào obj
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                temp = new TT153RegisterInvoice();
            }
            else
            {
                temp = _ITT153RegisterInvoiceService.Getbykey(_select.ID);
            }
            temp = ObjandGUI(temp, true);
            //_select = temp;
            //tempDetail = ObjandGUI(tempDetail, true);
            #endregion
            #region CSDL
            _ITT153RegisterInvoiceService.BeginTran();
            _ITT153RegisterInvoiceDetailService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153RegisterInvoiceService.CreateNew(temp);
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit || _statusForm == ConstFrm.optStatusForm.View)
            {
                _ITT153RegisterInvoiceService.Update(temp);
            }
            try
            {
                _ITT153RegisterInvoiceService.CommitTran();
            }
            catch (Exception ex)
            {
                _ITT153RegisterInvoiceService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu." + ex.Message);
                return;
                
            }

            #endregion
            #region xử lý form, kết thúc form
            Utils.ClearCacheByType<TT153RegisterInvoice>();
            List<TT153RegisterInvoice> lst = _ITT153RegisterInvoiceService.GetAll();
            _ITT153RegisterInvoiceService.UnbindSession(lst);
            Utils.ClearCacheByType<TT153RegisterInvoiceDetail>();
            IsClose = true;
            MSG.Information("Lưu thành công");
            this.Close();
            //isClose = false;
            #endregion
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (_select.AttachFileContent == null || _select.AttachFileName == null || _select.AttachFileName == "")
            {
                MSG.Warning("không có file để tải về");
                return;
            }
            var openfolder = new FolderBrowserDialog();
            openfolder.Description = "Chọn thư mục lưu file";
            openfolder.ShowNewFolderButton = false;
            openfolder.ShowDialog(this);
            string linkFileSave = openfolder.SelectedPath;
            if (linkFileSave != null && linkFileSave != "")
            {
                Utils.databaseToFile(_select.AttachFileContent, linkFileSave + "\\" + _select.AttachFileName);
                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDown_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Tải xuống file đính kèm", btnDown);
        }
        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                var f = new MsgRefDateTime(refTime);
                f.Text = "Thiết lập thời gian";
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            refTime = frm.RefDateTime;
            btnSave.Enabled = true;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
        }

        private void btnBrowser_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Chọn file đính kèm", btnBrowser);
        }

        private void Value_Changed(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            btnSave.Enabled = true;
        }
    }
}