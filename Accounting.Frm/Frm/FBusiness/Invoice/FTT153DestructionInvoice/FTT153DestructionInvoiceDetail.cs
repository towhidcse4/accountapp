﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;


namespace Accounting
{
    public partial class FTT153DestructionInvoiceDetail : CustormForm
    {
        #region Khai Bao
        public readonly ITT153DestructionInvoiceService _ITT153DestructionInvoiceService;
        public readonly ITT153ReportService _ITT153ReportService;
        public readonly ITT153PublishInvoiceDetailService _ITT153PublishInvoiceDetailService;
        public readonly ITT153RegisterInvoiceDetailService _ITT153RegisterInvoiceDetailService;
        public readonly ISystemOptionService _ISystemOptionService;
        public readonly ITT153LostInvoiceDetailService _ITT153LostInvoiceDetailService;
        public readonly ITT153DeletedInvoiceService _ITT153DeletedInvoiceService;
        public readonly ITT153DestructionInvoiceDetailService _ITT153DestructionInvoiceDetailService;
        TT153DestructionInvoice _select;
        List<TT153DestructionPublishDetail> lstTT153DestructionPublish;
        int _statusForm;
        string linkFile;
        DateTime refTime;
        #endregion
        #region Khởi tạo
        public FTT153DestructionInvoiceDetail(TT153DestructionInvoice temp, List<TT153DestructionInvoice> lstTT153DestructionInvoice, int statusForm)
        {
            _ITT153DestructionInvoiceService = IoC.Resolve<ITT153DestructionInvoiceService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _ITT153RegisterInvoiceDetailService = IoC.Resolve<ITT153RegisterInvoiceDetailService>();
            _ITT153LostInvoiceDetailService = IoC.Resolve<ITT153LostInvoiceDetailService>();
            lstTT153DestructionPublish = new List<TT153DestructionPublishDetail>();
            _ITT153DeletedInvoiceService = IoC.Resolve<ITT153DeletedInvoiceService>();
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ITT153DestructionInvoiceDetailService = IoC.Resolve<ITT153DestructionInvoiceDetailService>();
            linkFile = null;
            InitializeComponent();
            _select = temp;
            _statusForm = statusForm;
            cbbStatus.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            #region Thêm button vào dataeditor
            var btnRefDateTime = new EditorButton("btnRefDateTime");
            var appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.clock,
                ImageHAlign = Infragistics.Win.HAlign.Center,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            btnRefDateTime.Appearance = appearance;
            btnRefDateTime.Key = "btnRefDateTime";
            btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            txtDate.ButtonsRight.Add(btnRefDateTime);
            #endregion

            if (ConstFrm.optStatusForm.Add == statusForm)
            {
                btnSave.Enabled = true;
                btnPrint.Enabled = false;
                txtGetDetectionObject.Value = _ISystemOptionService.Getbykey(8).Data;
                txtRepresentationInLaw.Value = _ISystemOptionService.Getbykey(21).Data;
                refTime = DateTime.Now;
                btnDown.Enabled = false;
                foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll())
                {
                    TT153RegisterInvoiceDetail tT153RegisterInvoiceDetail = _ITT153RegisterInvoiceDetailService.Getbykey(item.TT153RegisterInvoiceDetailID);
                    TT153Report tt153Report = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    if (lstTT153DestructionPublish.Count(n => n.TT153ReportID == item.TT153ReportID) == 0)
                        lstTT153DestructionPublish.Add(new TT153DestructionPublishDetail
                        {
                            Status = false,
                            TT153ReportID = item.TT153ReportID,
                            TT153PublishInvoiceDetailID = item.ID,
                            InvoiceTemplate = tt153Report.InvoiceTemplate,
                            InvoiceType = item.InvoiceType,
                            InvoiceType_string = tt153Report.InvoiceTypeString,
                            InvoiceTypeID = item.InvoiceTypeID,
                            InvoiceSeries = item.InvoiceSeries,
                        });
                }
            }
            else
            {

                refTime = _select.Date;
                foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll())
                {
                    TT153RegisterInvoiceDetail tT153RegisterInvoiceDetail = _ITT153RegisterInvoiceDetailService.Getbykey(item.TT153RegisterInvoiceDetailID);
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(tT153RegisterInvoiceDetail.TT153ReportID);
                    if (lstTT153DestructionPublish.Count(n => n.TT153ReportID == item.TT153ReportID) == 0)
                        lstTT153DestructionPublish.Add(new TT153DestructionPublishDetail
                        {
                            Status = false,
                            TT153ReportID = item.TT153ReportID,
                            TT153PublishInvoiceDetailID = item.ID,
                            InvoiceTemplate = tT153Report.InvoiceTemplate,
                            InvoiceType = item.InvoiceType,
                            InvoiceType_string = tT153Report.InvoiceTypeString,
                            InvoiceTypeID = item.InvoiceTypeID,
                            InvoiceSeries = item.InvoiceSeries,
                        });
                }
                foreach (TT153DestructionPublishDetail item_Detail in lstTT153DestructionPublish)
                {
                    foreach (TT153DestructionInvoiceDetail item in temp.TT153DestructionInvoiceDetails)
                    {
                        if (item.TT153ReportID == item_Detail.TT153ReportID)
                        {
                            item_Detail.Status = true;
                            item_Detail.FromNo = item.FromNo;
                            item_Detail.ToNo = item.ToNo;
                            item_Detail.Quantity = Convert.ToInt32(item.Quantity);
                        }
                    }
                }

                ObjandGUI(temp, false);
                btnSave.Enabled = false;
                btnPrint.Enabled = true;
            }
            InitializeGUI();
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Nghiệp vụ
        private void InitializeGUI()
        {
            LoadUGrid();
        }
        public void LoadUGrid()
        {
            uGrid.SetDataBinding(lstTT153DestructionPublish, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TT153DestructionPublishDetail_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            uGrid.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["FromNo"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["ToNo"].CellClickAction = CellClickAction.EditAndSelectText;

            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["ToNo"], 1);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["FromNo"], 1);
        }

        public TT153DestructionInvoice ObjandGUI(TT153DestructionInvoice input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.Date = Utils.GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
                input.No = txtNo.Text;
                input.GetDetectionObject = txtGetDetectionObject.Text;
                input.RepresentationInLaw = txtRepresentationInLaw.Text;
                input.Reason = txtReason.Text;
                input.DestructionMethod = txtDestructionMethod.Text;
                input.Status = int.Parse(cbbStatus.Value.ToString());
                input.AttachFileName = txtAttachFileName.Text;
                if (input.AttachFileName != null && input.AttachFileName != "")
                    if (linkFile != null && linkFile != "")
                        input.AttachFileContent = Utils.FileToByte(linkFile);
                if (input.TT153DestructionInvoiceDetails.Count > 0)
                {
                    foreach (TT153DestructionPublishDetail item in lstTT153DestructionPublish)
                    {
                        int i;
                        bool check = false;
                        for (i = 0; i < input.TT153DestructionInvoiceDetails.Count; i++)
                        {
                            if (input.TT153DestructionInvoiceDetails[i].TT153ReportID == item.TT153ReportID)
                            {
                                check = true;
                                break;
                            }
                        }
                        if (check == true)
                        {
                            if (item.Status == true)
                            {
                                input.TT153DestructionInvoiceDetails[i].InvoiceType = item.InvoiceType;
                                input.TT153DestructionInvoiceDetails[i].InvoiceSeries = item.InvoiceSeries;
                                input.TT153DestructionInvoiceDetails[i].InvoiceTypeID = item.InvoiceTypeID;
                                input.TT153DestructionInvoiceDetails[i].FromNo = item.FromNo;
                                input.TT153DestructionInvoiceDetails[i].ToNo = item.ToNo;
                                input.TT153DestructionInvoiceDetails[i].Quantity = decimal.Parse(item.Quantity.ToString());
                            }
                            else
                            {
                                input.TT153DestructionInvoiceDetails.RemoveAt(i);
                            }
                        }
                        else
                        {
                            TT153DestructionInvoiceDetail temp = new TT153DestructionInvoiceDetail();
                            if (item.Status == true)
                            {
                                temp.TT153DestructionInvoiceID = input.ID;
                                temp.TT153PublishInvoiceDetailID = item.TT153PublishInvoiceDetailID;
                                temp.TT153ReportID = item.TT153ReportID;
                                temp.InvoiceType = item.InvoiceType;
                                temp.InvoiceSeries = item.InvoiceSeries;
                                temp.InvoiceTypeID = item.InvoiceTypeID;
                                temp.FromNo = item.FromNo;
                                temp.ToNo = item.ToNo;
                                temp.Quantity = decimal.Parse(item.Quantity.ToString());
                                input.TT153DestructionInvoiceDetails.Add(temp);
                            }
                        }
                    }
                }
                else
                {
                    foreach (TT153DestructionPublishDetail item in lstTT153DestructionPublish)
                    {
                        TT153DestructionInvoiceDetail temp = new TT153DestructionInvoiceDetail();
                        if (item.Status == true)
                        {
                            temp.TT153PublishInvoiceDetailID = item.TT153PublishInvoiceDetailID;
                            temp.TT153DestructionInvoiceID = input.ID;
                            temp.TT153ReportID = item.TT153ReportID;
                            temp.InvoiceType = item.InvoiceType;
                            temp.InvoiceSeries = item.InvoiceSeries;
                            temp.InvoiceTypeID = item.InvoiceTypeID;
                            temp.FromNo = item.FromNo;
                            temp.ToNo = item.ToNo;
                            temp.Quantity = decimal.Parse(item.Quantity.ToString());
                            input.TT153DestructionInvoiceDetails.Add(temp);
                        }
                    }
                }

            }
            else
            {
                txtDate.Value = input.Date;
                txtNo.Value = input.No;
                txtReason.Value = input.Reason;
                txtRepresentationInLaw.Value = input.RepresentationInLaw;
                txtGetDetectionObject.Value = input.GetDetectionObject;
                txtDestructionMethod.Value = input.DestructionMethod;
                cbbStatus.Value = input.Status;
                txtAttachFileName.Value = input.AttachFileName;

            }
            return input;
        }
        public virtual void Reset()
        {

        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChoseFile_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog();

            var result = openFile.ShowDialog(this);

            // giới hạn dung lượng file cho phép là 5 MB
            // giới hạn dung lượng file cho phép là 10 MB
            linkFile = openFile.FileName;
            if (linkFile != null && linkFile != "")
            {
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 10240)
                {
                    MSG.Error("Dung lượng file vượt quá mức cho phép (10MB)");
                    return;
                }
                String[] Name = openFile.FileName.Split('\\');
                txtAttachFileName.Value = Name.Last().ToString();
            }
        }
        public bool CheckError()
        {
            bool kq = true;
            foreach (TT153DestructionPublishDetail item in lstTT153DestructionPublish)
            {
                if (item.Status)
                {
                    if (item.Quantity <= 0)
                    {
                        MSG.Warning("Số lượng phải lớn hơn 0");
                        return false;
                    }
                    if (string.IsNullOrEmpty(item.FromNo))
                    {
                        MSG.Warning("Từ số không được để trống");
                        return false;
                    }
                    if (string.IsNullOrEmpty(item.ToNo))
                    {
                        MSG.Warning("Đến số không được để trống");
                        return false;
                    }

                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153DestructionInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                {
                    MSG.Error("Số quyết định đã tồn tại");
                    return false;
                }
            }
            else
            {
                if (txtNo.Text != _select.No)
                    if (_ITT153DestructionInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                    {
                        MSG.Error("Số quyết định đã tồn tại");
                        return false;
                    }

            }
            if (lstTT153DestructionPublish.Count(n => n.Status) == 0)
            {
                MSG.Error("Chưa chọn mẫu hóa đơn nào để thông báo hủy");
                return false;
            }
            #region
            foreach (TT153DestructionPublishDetail item in lstTT153DestructionPublish)
            {
                if (item.Status)
                {
                    if (int.Parse(item.ToNo) > _ITT153PublishInvoiceDetailService.GetWithMaxToNo(item.TT153ReportID))
                    {
                        MSG.Warning("Số hóa đơn nhập vào chưa được thông báo phát hành");
                        return false;
                    }
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    List<SAInvoice> lst = Utils.ListSAInvoice.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();
                    List<SABill> lst1 = Utils.ListSABill.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();
                    List<SAReturn> lst2 = Utils.ListSAReturn.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();
                    List<PPDiscountReturn> lst3 = Utils.ListPPDiscountReturn.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();

                    if (lst.Count > 0 || lst1.Count > 0 || lst2.Count > 0 || lst3.Count > 0)
                    {
                        int ToNoMax = lst.Select(n => int.Parse(n.InvoiceNo)).Max();
                        if (int.Parse(item.FromNo) <= ToNoMax)
                        {
                            MSG.Warning("Số hóa đơn nhập vào đã được sử dụng");
                            return false;
                        }
                    }
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                        {
                            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                            {
                                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                                return false;
                            }
                        }
                    }
                    else
                    {
                        TT153DestructionInvoiceDetail t153DestructionInvoiceDetail = _select.TT153DestructionInvoiceDetails.FirstOrDefault(n => n.TT153ReportID == item.TT153ReportID);
                        if (int.Parse(item.ToNo) < int.Parse(t153DestructionInvoiceDetail.FromNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                                    return false;
                                }
                            }
                        }
                        else if (int.Parse(item.FromNo) < int.Parse(t153DestructionInvoiceDetail.FromNo) && int.Parse(item.ToNo) >= int.Parse(t153DestructionInvoiceDetail.FromNo) && int.Parse(item.ToNo) <= int.Parse(t153DestructionInvoiceDetail.ToNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(t153DestructionInvoiceDetail.FromNo) - 1 && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                                    return false;
                                }
                            }
                        }
                        else if (int.Parse(item.FromNo) >= int.Parse(t153DestructionInvoiceDetail.FromNo) && int.Parse(item.FromNo) <= int.Parse(t153DestructionInvoiceDetail.ToNo) && int.Parse(item.ToNo) > int.Parse(t153DestructionInvoiceDetail.ToNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(t153DestructionInvoiceDetail.ToNo) + 1 <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                                    return false;
                                }
                            }
                        }
                        else if (int.Parse(item.FromNo) > int.Parse(t153DestructionInvoiceDetail.ToNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                                    return false;
                                }
                            }
                        }
                    }
                    #region Code Cũ
                    //if (int.Parse(item.ToNo) > _ITT153PublishInvoiceDetailService.GetWithMaxToNo(item.TT153ReportID))
                    //{
                    //    MSG.Warning("Số hóa đơn nhập vào chưa được thông báo phát hành");
                    //    return false;
                    //}


                    //if(int.Parse(item.FromNo)< Utils.ISAInvoiceService.GetWithMaxToNo(tT153Report.InvoiceTypeID, tT153Report.InvoiceForm, tT153Report.InvoiceTemplate))
                    //{
                    //    MSG.Warning("Số hóa đơn nhập vào đã được sử dụng");
                    //    return false;
                    //}

                    //foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //{
                    //    if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //    {
                    //        MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //        return false;
                    //    }
                    //}
                    //if (_statusForm == ConstFrm.optStatusForm.Add)
                    //{
                    //    foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //    {
                    //        if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //        {
                    //            MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //            return false;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    TT153DestructionInvoiceDetail t153DestructionInvoiceDetail = _select.TT153DestructionInvoiceDetails.FirstOrDefault(n => n.TT153ReportID == item.TT153ReportID);
                    //    if (int.Parse(item.ToNo) < int.Parse(t153DestructionInvoiceDetail.FromNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }
                    //    else if (int.Parse(item.FromNo) < int.Parse(t153DestructionInvoiceDetail.FromNo) && int.Parse(item.ToNo) >= int.Parse(t153DestructionInvoiceDetail.FromNo) && int.Parse(item.ToNo) <= int.Parse(t153DestructionInvoiceDetail.ToNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(t153DestructionInvoiceDetail.FromNo)-1 && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(t153DestructionInvoiceDetail.FromNo)-1 && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }else if (int.Parse(item.FromNo) >= int.Parse(t153DestructionInvoiceDetail.FromNo) && int.Parse(item.FromNo) <= int.Parse(t153DestructionInvoiceDetail.ToNo) && int.Parse(item.ToNo) > int.Parse(t153DestructionInvoiceDetail.ToNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(t153DestructionInvoiceDetail.ToNo)+1 <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(t153DestructionInvoiceDetail.ToNo)+1 <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }else if (int.Parse(item.FromNo) > int.Parse(t153DestructionInvoiceDetail.ToNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }
                    //}
                    //foreach (SAInvoice itemDelete in _ITT153DeletedInvoiceService.GetAllSAInvoice(item.TT153ReportID))
                    //{
                    //    if(int.Parse(itemDelete.InvoiceNo)<= int.Parse(item.ToNo) && int.Parse(itemDelete.InvoiceNo) >= int.Parse(item.FromNo))
                    //    {
                    //        MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //        return false;
                    //    }
                    //}
                    #endregion

                }
            }
            #endregion
            return kq;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            TT153DestructionInvoice temp;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                temp = new TT153DestructionInvoice();
            }
            else
            {
                temp = _ITT153DestructionInvoiceService.Getbykey(_select.ID);
            }
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            #region Fill giữ liệu
            temp = ObjandGUI(temp, true);
            #endregion
            #region CSDL
            _ITT153DestructionInvoiceService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153DestructionInvoiceService.CreateNew(temp);
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                _ITT153DestructionInvoiceService.Update(temp);
            }
            try
            {
                _ITT153DestructionInvoiceService.CommitTran();
            }
            catch (Exception ex)
            {
                _ITT153DestructionInvoiceService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu.");
                return;

            }
            #endregion
            #region xử lý form, kết thúc form
            Utils.ClearCacheByType<TT153DestructionInvoice>();
            List<TT153DestructionInvoice> lst = _ITT153DestructionInvoiceService.GetAll();
            _ITT153DestructionInvoiceService.UnbindSession(lst);
            Utils.ClearCacheByType<TT153DestructionInvoiceDetail>();
            IsClose = true;
            //this.Close();
            //isClose = false;
            MSG.Information("Lưu thành công");
            _select = temp;
            _statusForm = ConstFrm.optStatusForm.View;
            btnSave.Enabled = false;
            btnPrint.Enabled = true;
            #endregion
        }



        private void btnDown_Click(object sender, EventArgs e)
        {
            if (_select.AttachFileContent == null || _select.AttachFileName == null || _select.AttachFileName == "")
            {
                MSG.Warning("không có file để tải về");
                return;
            }
            var openfolder = new FolderBrowserDialog();
            openfolder.Description = "Chọn thư mục lưu file";
            openfolder.ShowNewFolderButton = false;
            openfolder.ShowDialog(this);
            string linkFileSave = openfolder.SelectedPath;
            if (linkFileSave != null && linkFileSave != "")
            {
                Utils.databaseToFile(_select.AttachFileContent, linkFileSave + "\\" + _select.AttachFileName);
                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDown_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Tải xuống file đính kèm", btnDown);
        }
        private void ChoseFile_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Chọn file đính kèm", ChoseFile);
        }
        #region TimeForm
        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                var f = new MsgRefDateTime(refTime);
                f.Text = "Thiết lập thời gian";
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            refTime = frm.RefDateTime;
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
        }
        #endregion

        #endregion

        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell.Column.Key.Equals("FromNo") || cell.Column.Key.Equals("ToNo"))
            {
                if (cell.Value != null && cell.Value != "")
                {
                    string t = cell.Value.ToString();
                    cell.Value = t.PadLeft(7, '0');
                }
                else
                {
                    cell.Row.Cells["Quantity"].Value = 0;
                }
            }
        }
        private void uGrid_AfterCellUpdate1(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("ToNo") || e.Cell.Column.Key.Equals("FromNo"))
            {
                if (e.Cell.Row.Cells["FromNo"].Value != null && e.Cell.Row.Cells["ToNo"].Value != null && e.Cell.Row.Cells["FromNo"].Value != "" && e.Cell.Row.Cells["ToNo"].Value != "")
                {
                    if (int.Parse(e.Cell.Row.Cells["ToNo"].Value.ToString().Trim('_')) != 0 && int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString().Trim('_')) != 0)
                    {
                        int result = int.Parse(e.Cell.Row.Cells["ToNo"].Value.ToString().Trim('_')) - int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString().Trim('_')) + 1;
                        e.Cell.Row.Cells["Quantity"].Value = result;

                    }
                    else
                    {
                        e.Cell.Row.Cells["Quantity"].Value = 0;
                    }
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Utils.Print("TT153DestructionInvoice", _ITT153DestructionInvoiceService.Getbykey(_select.ID), this);
        }

        private void Value_Change(object sender, EventArgs e)
        {
            btnPrint.Enabled = false;
            btnSave.Enabled = true;
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            btnPrint.Enabled = false;
            btnSave.Enabled = true;
        }
    }
}