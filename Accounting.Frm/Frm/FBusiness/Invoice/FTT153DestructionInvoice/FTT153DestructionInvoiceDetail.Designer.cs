﻿namespace Accounting
{
    partial class FTT153DestructionInvoiceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FTT153DestructionInvoiceDetail));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDown = new Infragistics.Win.Misc.UltraButton();
            this.cbbStatus = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ChoseFile = new Infragistics.Win.Misc.UltraButton();
            this.txtAttachFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDestructionMethod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRepresentationInLaw = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtGetDetectionObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnPrint = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestructionMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepresentationInLaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGetDetectionObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            // 
            // ultraGroupBox1
            // 
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.btnDown);
            this.ultraGroupBox1.Controls.Add(this.cbbStatus);
            this.ultraGroupBox1.Controls.Add(this.ChoseFile);
            this.ultraGroupBox1.Controls.Add(this.txtAttachFileName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.txtDestructionMethod);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtRepresentationInLaw);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.txtGetDetectionObject);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.txtDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, -9);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(845, 228);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.TextChanged += new System.EventHandler(this.Value_Change);
            // 
            // btnDown
            // 
            appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
            this.btnDown.Appearance = appearance2;
            this.btnDown.Location = new System.Drawing.Point(788, 167);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(26, 23);
            this.btnDown.TabIndex = 65;
            this.btnDown.Tag = "";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            this.btnDown.MouseHover += new System.EventHandler(this.btnDown_MouseHover);
            // 
            // cbbStatus
            // 
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Chưa nộp cho cơ quan thuế";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Đã nộp cho cơ quan thuế";
            this.cbbStatus.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.cbbStatus.Location = new System.Drawing.Point(228, 167);
            this.cbbStatus.Name = "cbbStatus";
            this.cbbStatus.Size = new System.Drawing.Size(190, 21);
            this.cbbStatus.TabIndex = 61;
            this.cbbStatus.Text = "Đã nộp cho cơ quan thuế";
            this.cbbStatus.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ChoseFile
            // 
            appearance3.Image = global::Accounting.Properties.Resources.iAdd;
            this.ChoseFile.Appearance = appearance3;
            this.ChoseFile.Location = new System.Drawing.Point(747, 168);
            this.ChoseFile.Name = "ChoseFile";
            this.ChoseFile.Size = new System.Drawing.Size(35, 22);
            this.ChoseFile.TabIndex = 60;
            this.ChoseFile.Click += new System.EventHandler(this.ChoseFile_Click);
            this.ChoseFile.MouseHover += new System.EventHandler(this.ChoseFile_MouseHover);
            // 
            // txtAttachFileName
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.txtAttachFileName.Appearance = appearance4;
            this.txtAttachFileName.AutoSize = false;
            this.txtAttachFileName.Enabled = false;
            this.txtAttachFileName.Location = new System.Drawing.Point(547, 167);
            this.txtAttachFileName.Name = "txtAttachFileName";
            this.txtAttachFileName.Size = new System.Drawing.Size(194, 22);
            this.txtAttachFileName.TabIndex = 50;
            this.txtAttachFileName.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel7
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance5;
            this.ultraLabel7.Location = new System.Drawing.Point(455, 167);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel7.TabIndex = 49;
            this.ultraLabel7.Text = "Tệp đính kèm";
            // 
            // ultraLabel6
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance6;
            this.ultraLabel6.Location = new System.Drawing.Point(36, 139);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(99, 22);
            this.ultraLabel6.TabIndex = 48;
            this.ultraLabel6.Text = "Phương thức hủy";
            // 
            // txtDestructionMethod
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.txtDestructionMethod.Appearance = appearance7;
            this.txtDestructionMethod.AutoSize = false;
            this.txtDestructionMethod.Location = new System.Drawing.Point(228, 139);
            this.txtDestructionMethod.Name = "txtDestructionMethod";
            this.txtDestructionMethod.Size = new System.Drawing.Size(586, 22);
            this.txtDestructionMethod.TabIndex = 47;
            this.txtDestructionMethod.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // txtReason
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance8;
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(228, 111);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(586, 22);
            this.txtReason.TabIndex = 46;
            this.txtReason.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel2
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(36, 111);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel2.TabIndex = 45;
            this.ultraLabel2.Text = "Lý do hủy";
            // 
            // txtRepresentationInLaw
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.txtRepresentationInLaw.Appearance = appearance10;
            this.txtRepresentationInLaw.AutoSize = false;
            this.txtRepresentationInLaw.Location = new System.Drawing.Point(228, 83);
            this.txtRepresentationInLaw.Name = "txtRepresentationInLaw";
            this.txtRepresentationInLaw.Size = new System.Drawing.Size(586, 22);
            this.txtRepresentationInLaw.TabIndex = 44;
            this.txtRepresentationInLaw.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel4
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance11;
            this.ultraLabel4.Location = new System.Drawing.Point(36, 83);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(157, 22);
            this.ultraLabel4.TabIndex = 43;
            this.ultraLabel4.Text = "Người đại diện theo pháp luật";
            // 
            // txtGetDetectionObject
            // 
            appearance12.TextVAlignAsString = "Middle";
            this.txtGetDetectionObject.Appearance = appearance12;
            this.txtGetDetectionObject.AutoSize = false;
            this.txtGetDetectionObject.Location = new System.Drawing.Point(228, 55);
            this.txtGetDetectionObject.Name = "txtGetDetectionObject";
            this.txtGetDetectionObject.Size = new System.Drawing.Size(586, 22);
            this.txtGetDetectionObject.TabIndex = 42;
            this.txtGetDetectionObject.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel3
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance13;
            this.ultraLabel3.Location = new System.Drawing.Point(36, 55);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(177, 22);
            this.ultraLabel3.TabIndex = 41;
            this.ultraLabel3.Text = "Cơ quan thuế nhận quyết định";
            // 
            // txtNo
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.txtNo.Appearance = appearance14;
            this.txtNo.AutoSize = false;
            this.txtNo.Location = new System.Drawing.Point(547, 25);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(267, 22);
            this.txtNo.TabIndex = 40;
            this.txtNo.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel9
            // 
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance15;
            this.ultraLabel9.Location = new System.Drawing.Point(455, 26);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(74, 22);
            this.ultraLabel9.TabIndex = 39;
            this.ultraLabel9.Text = "Số quyết định";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(228, 27);
            this.txtDate.Name = "txtDate";
            this.txtDate.Nullable = false;
            this.txtDate.Size = new System.Drawing.Size(190, 21);
            this.txtDate.TabIndex = 38;
            this.txtDate.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel5
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance16;
            this.ultraLabel5.Location = new System.Drawing.Point(36, 167);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel5.TabIndex = 30;
            this.ultraLabel5.Text = "Trạng thái";
            // 
            // ultraLabel1
            // 
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance17;
            this.ultraLabel1.Location = new System.Drawing.Point(36, 27);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Ngày lập";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGrid);
            this.ultraGroupBox2.Location = new System.Drawing.Point(1, 225);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(845, 207);
            this.ultraGroupBox2.TabIndex = 2;
            // 
            // uGrid
            // 
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance18;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance25;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.UseFixedHeaders = true;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(839, 204);
            this.uGrid.TabIndex = 2;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate1);
            this.uGrid.AfterExitEditMode += new System.EventHandler(this.uGrid_AfterExitEditMode);
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnPrint);
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Location = new System.Drawing.Point(1, 438);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(845, 49);
            this.ultraGroupBox3.TabIndex = 3;
            // 
            // btnPrint
            // 
            appearance29.Image = ((object)(resources.GetObject("appearance29.Image")));
            this.btnPrint.Appearance = appearance29;
            this.btnPrint.Location = new System.Drawing.Point(594, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 30);
            this.btnPrint.TabIndex = 61;
            this.btnPrint.Text = "In";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            appearance30.Image = ((object)(resources.GetObject("appearance30.Image")));
            this.btnClose.Appearance = appearance30;
            this.btnClose.Location = new System.Drawing.Point(756, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 59;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSave
            // 
            appearance31.Image = ((object)(resources.GetObject("appearance31.Image")));
            this.btnSave.Appearance = appearance31;
            this.btnSave.Location = new System.Drawing.Point(675, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 58;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FTT153DestructionInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 497);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(867, 536);
            this.MinimizeBox = false;
            this.Name = "FTT153DestructionInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hủy hóa đơn";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestructionMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepresentationInLaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGetDetectionObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRepresentationInLaw;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtGetDetectionObject;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraButton ChoseFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAttachFileName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDestructionMethod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbStatus;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDate;
        private Infragistics.Win.Misc.UltraButton btnPrint;
        private Infragistics.Win.Misc.UltraButton btnDown;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}