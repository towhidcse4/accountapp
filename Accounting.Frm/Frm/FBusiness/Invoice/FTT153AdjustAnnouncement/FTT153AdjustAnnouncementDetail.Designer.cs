﻿namespace Accounting
{
    partial class FTT153AdjustAnnouncementDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance("Bảng kê hóa đơn chưa sử dụng", 146035063);
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance("Thông báo điều chỉnh thông tin trên thông báo phát hành hóa đơn", 146076797);
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.cms4Button = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.popUp = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.chkAnnoun = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnPrint = new Infragistics.Win.Misc.UltraDropDownButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDown = new Infragistics.Win.Misc.UltraButton();
            this.dtePublishInvoiceDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnBrowser = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbStatus = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtAttachFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPublishInvoiceNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtGetDetectionObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRespresentationInLaw = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.contextMenuStrip1.SuspendLayout();
            this.cms4Button.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAnnoun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePublishInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPublishInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGetDetectionObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRespresentationInLaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            // 
            // cms4Button
            // 
            this.cms4Button.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem,
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem});
            this.cms4Button.Name = "cms4Button";
            this.cms4Button.Size = new System.Drawing.Size(244, 48);
            this.cms4Button.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cms4Button_ItemClicked);
            // 
            // bảngKêhóađơnchưasửdụngToolStripMenuItem
            // 
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem.Name = "bảngKêhóađơnchưasửdụngToolStripMenuItem";
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem.Text = "Bảng kê hóa đơn chưa sử dụng";
            this.bảngKêhóađơnchưasửdụngToolStripMenuItem.Click += new System.EventHandler(this.BKHDCSD_Click);
            // 
            // thôngBáođiềuchỉnhthôngtinToolStripMenuItem
            // 
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem.Name = "thôngBáođiềuchỉnhthôngtinToolStripMenuItem";
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem.Text = "Thông báo điều chỉnh thông tin";
            this.thôngBáođiềuchỉnhthôngtinToolStripMenuItem.Click += new System.EventHandler(this.TBDCTT_Click);
            // 
            // ultraLabel10
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance1;
            this.ultraLabel10.Location = new System.Drawing.Point(12, 208);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(108, 22);
            this.ultraLabel10.TabIndex = 73;
            this.ultraLabel10.Text = "Thông tin thay đổi";
            // 
            // chkAnnoun
            // 
            this.chkAnnoun.Location = new System.Drawing.Point(7, 318);
            this.chkAnnoun.Name = "chkAnnoun";
            this.chkAnnoun.Size = new System.Drawing.Size(421, 20);
            this.chkAnnoun.TabIndex = 5;
            this.chkAnnoun.Text = "Kèm bảng kê hóa đơn chưa sử dụng chuyển địa điểm kinh doanh khác địa bàn";
            this.chkAnnoun.CheckedChanged += new System.EventHandler(this.chkAnnoun_CheckedChanged);
            this.chkAnnoun.Validated += new System.EventHandler(this.chkAnnoun_Validated);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uGrid2);
            this.ultraGroupBox4.Location = new System.Drawing.Point(1, 344);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(848, 148);
            this.ultraGroupBox4.TabIndex = 4;
            // 
            // uGrid2
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance2;
            this.uGrid2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid2.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.UseFixedHeaders = true;
            this.uGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid2.Location = new System.Drawing.Point(3, 0);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(842, 145);
            this.uGrid2.TabIndex = 3;
            this.uGrid2.Text = "ultraGrid1";
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.AfterExitEditMode += new System.EventHandler(this.uGrid2_AfterExitEditMode);
            this.uGrid2.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_CellChange);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnPrint);
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Location = new System.Drawing.Point(4, 503);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(848, 49);
            this.ultraGroupBox3.TabIndex = 3;
            // 
            // btnPrint
            // 
            this.btnPrint.AllowDrop = true;
            appearance13.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.btnPrint.Appearance = appearance13;
            this.btnPrint.Appearances.Add(appearance14);
            this.btnPrint.Appearances.Add(appearance15);
            this.btnPrint.Location = new System.Drawing.Point(594, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 30);
            this.btnPrint.TabIndex = 61;
            this.btnPrint.Text = "In";
            this.btnPrint.DroppingDown += new System.ComponentModel.CancelEventHandler(this.btnPrint_DroppingDown);
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            appearance16.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance16;
            this.btnClose.Location = new System.Drawing.Point(756, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 59;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSave
            // 
            appearance17.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance17;
            this.btnSave.Location = new System.Drawing.Point(675, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 58;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGrid1);
            this.ultraGroupBox2.Location = new System.Drawing.Point(4, 234);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(845, 78);
            this.ultraGroupBox2.TabIndex = 2;
            // 
            // uGrid1
            // 
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance18;
            this.uGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance25;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.UseFixedHeaders = true;
            this.uGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid1.Location = new System.Drawing.Point(3, 0);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(839, 75);
            this.uGrid1.TabIndex = 2;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_CellChange);
            // 
            // ultraGroupBox1
            // 
            appearance29.Image = global::Accounting.Properties.Resources.folder;
            this.ultraGroupBox1.Appearance = appearance29;
            this.ultraGroupBox1.Controls.Add(this.btnDown);
            this.ultraGroupBox1.Controls.Add(this.dtePublishInvoiceDate);
            this.ultraGroupBox1.Controls.Add(this.btnBrowser);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.cbbStatus);
            this.ultraGroupBox1.Controls.Add(this.txtAttachFileName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.txtPublishInvoiceNo);
            this.ultraGroupBox1.Controls.Add(this.txtGetDetectionObject);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.txtRespresentationInLaw);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.dteDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(4, -9);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(845, 209);
            this.ultraGroupBox1.TabIndex = 1;
            // 
            // btnDown
            // 
            appearance30.Image = global::Accounting.Properties.Resources.btnSave;
            this.btnDown.Appearance = appearance30;
            this.btnDown.Location = new System.Drawing.Point(781, 163);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(26, 23);
            this.btnDown.TabIndex = 73;
            this.btnDown.Tag = "";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            this.btnDown.MouseHover += new System.EventHandler(this.btnDown_MouseHover);
            // 
            // dtePublishInvoiceDate
            // 
            this.dtePublishInvoiceDate.Location = new System.Drawing.Point(547, 137);
            this.dtePublishInvoiceDate.Name = "dtePublishInvoiceDate";
            this.dtePublishInvoiceDate.Size = new System.Drawing.Size(260, 21);
            this.dtePublishInvoiceDate.TabIndex = 72;
            this.dtePublishInvoiceDate.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // btnBrowser
            // 
            appearance31.Image = global::Accounting.Properties.Resources.iAdd;
            this.btnBrowser.Appearance = appearance31;
            this.btnBrowser.Location = new System.Drawing.Point(741, 163);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(34, 23);
            this.btnBrowser.TabIndex = 71;
            this.btnBrowser.Click += new System.EventHandler(this.ultraButton1_Click);
            this.btnBrowser.MouseHover += new System.EventHandler(this.btnBrowser_MouseHover);
            // 
            // ultraLabel3
            // 
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance32;
            this.ultraLabel3.Location = new System.Drawing.Point(442, 137);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel3.TabIndex = 70;
            this.ultraLabel3.Text = "Ngày phát hành";
            // 
            // cbbStatus
            // 
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Chưa có hiệu lực";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Đã có hiệu lực";
            this.cbbStatus.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.cbbStatus.Location = new System.Drawing.Point(548, 108);
            this.cbbStatus.Name = "cbbStatus";
            this.cbbStatus.Size = new System.Drawing.Size(259, 21);
            this.cbbStatus.TabIndex = 69;
            this.cbbStatus.Text = "Đã có hiệu lực";
            this.cbbStatus.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // txtAttachFileName
            // 
            appearance33.TextVAlignAsString = "Middle";
            this.txtAttachFileName.Appearance = appearance33;
            this.txtAttachFileName.AutoSize = false;
            this.txtAttachFileName.Enabled = false;
            this.txtAttachFileName.Location = new System.Drawing.Point(548, 164);
            this.txtAttachFileName.Name = "txtAttachFileName";
            this.txtAttachFileName.Size = new System.Drawing.Size(187, 22);
            this.txtAttachFileName.TabIndex = 68;
            this.txtAttachFileName.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // ultraLabel8
            // 
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance34;
            this.ultraLabel8.Location = new System.Drawing.Point(442, 166);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel8.TabIndex = 67;
            this.ultraLabel8.Text = "Tệp đính kèm";
            // 
            // ultraLabel4
            // 
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance35;
            this.ultraLabel4.Location = new System.Drawing.Point(442, 109);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(61, 22);
            this.ultraLabel4.TabIndex = 65;
            this.ultraLabel4.Text = "Trạng thái";
            // 
            // txtPublishInvoiceNo
            // 
            appearance36.TextVAlignAsString = "Middle";
            this.txtPublishInvoiceNo.Appearance = appearance36;
            this.txtPublishInvoiceNo.AutoSize = false;
            this.txtPublishInvoiceNo.Location = new System.Drawing.Point(277, 136);
            this.txtPublishInvoiceNo.Name = "txtPublishInvoiceNo";
            this.txtPublishInvoiceNo.Size = new System.Drawing.Size(144, 22);
            this.txtPublishInvoiceNo.TabIndex = 63;
            this.txtPublishInvoiceNo.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // txtGetDetectionObject
            // 
            appearance37.TextVAlignAsString = "Middle";
            this.txtGetDetectionObject.Appearance = appearance37;
            this.txtGetDetectionObject.AutoSize = false;
            this.txtGetDetectionObject.Location = new System.Drawing.Point(212, 80);
            this.txtGetDetectionObject.Name = "txtGetDetectionObject";
            this.txtGetDetectionObject.Size = new System.Drawing.Size(595, 22);
            this.txtGetDetectionObject.TabIndex = 50;
            this.txtGetDetectionObject.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // ultraLabel7
            // 
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance38;
            this.ultraLabel7.Location = new System.Drawing.Point(20, 80);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(157, 22);
            this.ultraLabel7.TabIndex = 49;
            this.ultraLabel7.Text = "Cơ quan thuế nhận thông báo";
            // 
            // ultraLabel6
            // 
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance39;
            this.ultraLabel6.Location = new System.Drawing.Point(20, 108);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(157, 22);
            this.ultraLabel6.TabIndex = 48;
            this.ultraLabel6.Text = "Người đại diện theo pháp luật";
            // 
            // txtRespresentationInLaw
            // 
            appearance40.TextVAlignAsString = "Middle";
            this.txtRespresentationInLaw.Appearance = appearance40;
            this.txtRespresentationInLaw.AutoSize = false;
            this.txtRespresentationInLaw.Location = new System.Drawing.Point(212, 108);
            this.txtRespresentationInLaw.Name = "txtRespresentationInLaw";
            this.txtRespresentationInLaw.Size = new System.Drawing.Size(209, 22);
            this.txtRespresentationInLaw.TabIndex = 47;
            this.txtRespresentationInLaw.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // txtDescription
            // 
            appearance41.TextVAlignAsString = "Middle";
            this.txtDescription.Appearance = appearance41;
            this.txtDescription.AutoSize = false;
            this.txtDescription.Location = new System.Drawing.Point(212, 51);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(595, 23);
            this.txtDescription.TabIndex = 46;
            this.txtDescription.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // ultraLabel2
            // 
            appearance42.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance42;
            this.ultraLabel2.Location = new System.Drawing.Point(20, 52);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel2.TabIndex = 45;
            this.ultraLabel2.Text = "Nội dung";
            // 
            // txtNo
            // 
            appearance43.TextVAlignAsString = "Middle";
            this.txtNo.Appearance = appearance43;
            this.txtNo.AutoSize = false;
            this.txtNo.Location = new System.Drawing.Point(548, 22);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(259, 22);
            this.txtNo.TabIndex = 40;
            this.txtNo.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // ultraLabel9
            // 
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance44;
            this.ultraLabel9.Location = new System.Drawing.Point(442, 23);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(74, 22);
            this.ultraLabel9.TabIndex = 39;
            this.ultraLabel9.Text = "Số thông báo";
            // 
            // dteDate
            // 
            this.dteDate.Location = new System.Drawing.Point(212, 24);
            this.dteDate.Name = "dteDate";
            this.dteDate.Nullable = false;
            this.dteDate.Size = new System.Drawing.Size(209, 21);
            this.dteDate.TabIndex = 38;
            this.dteDate.ValueChanged += new System.EventHandler(this.txtNo_ValueChanged);
            // 
            // ultraLabel5
            // 
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance45;
            this.ultraLabel5.Location = new System.Drawing.Point(20, 136);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(251, 24);
            this.ultraLabel5.TabIndex = 30;
            this.ultraLabel5.Text = "Điều chỉnh thông tin trên thông báo phát hành số";
            // 
            // ultraLabel1
            // 
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance46;
            this.ultraLabel1.Location = new System.Drawing.Point(20, 24);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Ngày lập";
            // 
            // FTT153AdjustAnnouncementDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 556);
            this.Controls.Add(this.ultraLabel10);
            this.Controls.Add(this.chkAnnoun);
            this.Controls.Add(this.ultraGroupBox4);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(867, 595);
            this.MinimizeBox = false;
            this.Name = "FTT153AdjustAnnouncementDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Điều chỉnh thông báo phát hành hóa đơn";
            this.contextMenuStrip1.ResumeLayout(false);
            this.cms4Button.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkAnnoun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePublishInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPublishInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGetDetectionObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRespresentationInLaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtGetDetectionObject;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRespresentationInLaw;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAttachFileName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPublishInvoiceNo;
        private Infragistics.Win.Misc.UltraButton btnBrowser;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbStatus;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePublishInvoiceDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkAnnoun;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraDropDownButton btnPrint;
        private System.Windows.Forms.ContextMenuStrip cms4Button;
        private System.Windows.Forms.ToolStripMenuItem bảngKêhóađơnchưasửdụngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngBáođiềuchỉnhthôngtinToolStripMenuItem;
        private Infragistics.Win.Misc.UltraPopupControlContainer popUp;
        private Infragistics.Win.Misc.UltraButton btnDown;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}