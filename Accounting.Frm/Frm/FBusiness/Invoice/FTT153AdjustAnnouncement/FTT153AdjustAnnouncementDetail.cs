﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;


namespace Accounting
{
    public partial class FTT153AdjustAnnouncementDetail : CustormForm
    {
        #region Khai báo
        public readonly ISystemOptionService _ISystemOptionService;
        public readonly ITT153PublishInvoiceDetailService _ITT153PublishInvoiceDetailService;
        public readonly ITT153RegisterInvoiceDetailService _ITT153RegisterInvoiceDetailService;
        public readonly ITT153ReportService _ITT153ReportService;
        public readonly ITT153AdjustAnnouncementService _ITT153AdjustAnnouncementService;
        public readonly ISAInvoiceService _ISAInvoiceService;
        TT153AdjustAnnouncement _select;
        List<ChangeInformation> lstChangeInfo;
        List<TT153AnnouncementPublishDetail> lstAnnounPublish;
        int _statusForm;
        string linkFile;
        DateTime refTime;
        #endregion
        #region Khởi tạo
        public FTT153AdjustAnnouncementDetail(TT153AdjustAnnouncement temp, List<TT153AdjustAnnouncement> lstTT153AdjustAnnouncement, int statusForm)
        {
            InitializeComponent();
            btnPrint.PopupItem = popUp;
            _ITT153AdjustAnnouncementService = IoC.Resolve<ITT153AdjustAnnouncementService>();
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _ITT153RegisterInvoiceDetailService = IoC.Resolve<ITT153RegisterInvoiceDetailService>();
            temp = _ITT153AdjustAnnouncementService.Getbykey(temp.ID);
            _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            _select = temp;
            linkFile = null;
            _statusForm = statusForm;
            lstChangeInfo = new List<ChangeInformation>();
            lstAnnounPublish = new List<TT153AnnouncementPublishDetail>();
            cbbStatus.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            #region Thêm button vào data editor
            var btnRefDateTime = new EditorButton("btnRefDateTime");
            var appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.clock,
                ImageHAlign = Infragistics.Win.HAlign.Center,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            btnRefDateTime.Appearance = appearance;
            btnRefDateTime.Key = "btnRefDateTime";
            btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            dteDate.ButtonsRight.Add(btnRefDateTime);
            #endregion
            LoadLstUgrid();
            InitializeGUI();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                txtRespresentationInLaw.Value = _ISystemOptionService.Getbykey(21).Data;
                txtGetDetectionObject.Value = _ISystemOptionService.Getbykey(8).Data;
                refTime = DateTime.Now;
                btnDown.Enabled = false;
                //ObjandGUI(_select, false);
                lstChangeInfo = Create();
                btnSave.Enabled = true;
                btnPrint.Enabled = false;
            }
            else
            {
                refTime = _select.Date;
                ObjandGUI(temp, false);
                lstChangeInfo = Create();
                btnSave.Enabled = false;
                btnPrint.Enabled = true;
            }
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Nghiệp vụ
        public void LoadLstUgrid2()
        {
            int ToNo;
            int FromNo;
            int Quantity;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll())
                {
                    FromNo = 1;
                    Quantity = 0;
                    TT153RegisterInvoiceDetail tT153RegisterInvoiceDetail = _ITT153RegisterInvoiceDetailService.Getbykey(item.TT153RegisterInvoiceDetailID);
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    List<SAInvoice> lstSAInvoices = _ISAInvoiceService.GetAll().Where(n => n.InvoiceTypeID == item.InvoiceTypeID && n.InvoiceForm == item.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate).ToList();
                    ToNo = 1;
                    if (_ITT153PublishInvoiceDetailService.GetAllWithTT153ReportID(tT153Report.ID).Count > 0)
                    {
                        ToNo = _ITT153PublishInvoiceDetailService.GetWithMaxToNo(tT153Report.ID);
                    }
                    foreach (TT153PublishInvoiceDetail item_ in _ITT153PublishInvoiceDetailService.GetAll().Where(n => n.TT153ReportID == item.TT153ReportID))
                    {
                        Quantity += Convert.ToInt32(item_.Quantity);
                    }
                    if (lstSAInvoices.Count > 0)
                    {

                        foreach (SAInvoice itemSA in lstSAInvoices)
                        {
                            if (itemSA.InvoiceNo != null && itemSA.InvoiceNo != "")
                                if (FromNo <= int.Parse(itemSA.InvoiceNo))
                                {
                                    FromNo = int.Parse(itemSA.InvoiceNo) + 1;
                                }
                        }
                        if (lstAnnounPublish.Count(n => n.TT153ReportID == item.TT153ReportID) < 1)
                            lstAnnounPublish.Add(new TT153AnnouncementPublishDetail
                            {
                                Status = false,
                                TT153ReportID = item.TT153ReportID,
                                TT153PublishInvoiceDetailID = item.ID,
                                InvoiceTemplate = tT153Report.InvoiceTemplate,
                                InvoiceType = item.InvoiceType,
                                InvoiceType_string = tT153Report.InvoiceTypeString,
                                InvoiceTypeID = item.InvoiceTypeID,
                                InvoiceSeries = item.InvoiceSeries,
                                FromNo = FromNo.ToString().PadLeft(7, '0'),
                                ToNo = ToNo.ToString().PadLeft(7, '0'),
                                Quantity = Quantity - FromNo + 1
                            });
                    }
                    else
                    {
                        if (lstAnnounPublish.Count(n => n.TT153ReportID == item.TT153ReportID) < 1)
                            lstAnnounPublish.Add(new TT153AnnouncementPublishDetail
                            {
                                Status = false,
                                TT153ReportID = item.TT153ReportID,
                                TT153PublishInvoiceDetailID = item.ID,
                                InvoiceTemplate = tT153Report.InvoiceTemplate,
                                InvoiceType = item.InvoiceType,
                                InvoiceType_string = tT153Report.InvoiceTypeString,
                                InvoiceTypeID = item.InvoiceTypeID,
                                InvoiceSeries = item.InvoiceSeries,
                                FromNo = _ITT153PublishInvoiceDetailService.GetWithMinFromNo(item.TT153ReportID).ToString().PadLeft(7, '0'),
                                ToNo = ToNo.ToString().PadLeft(7, '0'),
                                Quantity = Quantity
                            });
                    }
                }
            }
            else
            {
                foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll())
                {
                    ToNo = 0;
                    FromNo = 1;
                    Quantity = 0;
                    TT153RegisterInvoiceDetail tT153RegisterInvoiceDetail = _ITT153RegisterInvoiceDetailService.Getbykey(item.TT153RegisterInvoiceDetailID);
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    List<SAInvoice> lstSAInvoices = _ISAInvoiceService.GetAll().Where(n => n.InvoiceTypeID == item.InvoiceTypeID && n.InvoiceForm == item.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate).ToList();
                    foreach (TT153PublishInvoiceDetail item_ in _ITT153PublishInvoiceDetailService.GetAll().Where(n => n.TT153ReportID == item.TT153ReportID))
                    {
                        if (int.Parse(item_.ToNo) > ToNo)
                        {
                            ToNo = int.Parse(item_.ToNo);
                        }
                        Quantity += Convert.ToInt32(item_.Quantity);
                    }
                    if (lstSAInvoices.Count > 0)
                    {
                        foreach (SAInvoice itemSA in lstSAInvoices)
                        {
                            if (itemSA.InvoiceNo != null && itemSA.InvoiceNo != "")
                                if (FromNo <= int.Parse(itemSA.InvoiceNo))
                                {
                                    FromNo = int.Parse(itemSA.InvoiceNo) + 1;
                                }
                        }
                        if (lstAnnounPublish.Count(n => n.TT153ReportID == item.TT153ReportID) < 1)
                            lstAnnounPublish.Add(new TT153AnnouncementPublishDetail
                            {
                                Status = false,
                                TT153ReportID = item.TT153ReportID,
                                TT153PublishInvoiceDetailID = item.ID,
                                InvoiceTemplate = tT153Report.InvoiceTemplate,
                                InvoiceType = item.InvoiceType,
                                InvoiceType_string = tT153Report.InvoiceTypeString,
                                InvoiceTypeID = item.InvoiceTypeID,
                                InvoiceSeries = item.InvoiceSeries,
                                FromNo = FromNo.ToString().PadLeft(7, '0'),
                                ToNo = ToNo.ToString().PadLeft(7, '0'),
                                Quantity = Quantity - FromNo + 1
                            });
                    }
                    else
                    {
                        if (lstAnnounPublish.Count(n => n.TT153ReportID == item.TT153ReportID) < 1)
                            lstAnnounPublish.Add(new TT153AnnouncementPublishDetail
                            {
                                Status = false,
                                TT153ReportID = item.TT153ReportID,
                                TT153PublishInvoiceDetailID = item.ID,
                                InvoiceTemplate = tT153Report.InvoiceTemplate,
                                InvoiceType = item.InvoiceType,
                                InvoiceType_string = tT153Report.InvoiceTypeString,
                                InvoiceTypeID = item.InvoiceTypeID,
                                InvoiceSeries = item.InvoiceSeries,
                                FromNo = _ITT153PublishInvoiceDetailService.GetWithMinFromNo(item.TT153ReportID).ToString().PadLeft(7, '0'),
                                ToNo = ToNo.ToString().PadLeft(7, '0'),
                                Quantity = Quantity
                            });
                    }
                }
                foreach (TT153AnnouncementPublishDetail item_Detail in lstAnnounPublish)
                {
                    foreach (TT153AdjustAnnouncementDetailListInvoice item in _select.TT153AdjustAnnouncementDetailListInvoices)
                    {
                        if (item.TT153ReportID == item_Detail.TT153ReportID)
                        {
                            item_Detail.Status = true;
                            item_Detail.FromNo = item.FromNo;
                            item_Detail.ToNo = item.ToNo;
                            item_Detail.Quantity = Convert.ToInt32(item.Quantity);
                        }
                    }
                }
            }
        }
        public void LoadLstUgrid()
        {

            foreach (TT153PublishInvoiceDetail item in Utils.ListTT153PublishInvoiceDetail.GroupBy(n => n.TT153ReportID).Select(n => n.First()).ToList())
            {
                TT153Report tT153Report = _ITT153ReportService.Getbykey(item.TT153ReportID);
                int ToNo = Utils.ITT153PublishInvoiceDetailService.GetWithMaxToNo(item.TT153ReportID);
                int FrNo = Utils.ITT153PublishInvoiceDetailService.GetWithMinFromNo(item.TT153ReportID);
                int FromNo1 = Utils.ISAInvoiceService.GetMaxToNo(tT153Report.InvoiceTypeID, tT153Report.InvoiceForm, tT153Report.InvoiceTemplate, tT153Report.InvoiceSeries);
                int FromNo2 = Utils.ISABillService.GetMaxToNo(tT153Report.InvoiceTypeID, tT153Report.InvoiceForm, tT153Report.InvoiceTemplate, tT153Report.InvoiceSeries);
                int FromNo3 = Utils.ISAReturnService.GetMaxToNo(tT153Report.InvoiceTypeID, tT153Report.InvoiceForm, tT153Report.InvoiceTemplate, tT153Report.InvoiceSeries);
                int FromNo4 = Utils.IPPDiscountReturnService.GetMaxToNo(tT153Report.InvoiceTypeID, tT153Report.InvoiceForm, tT153Report.InvoiceTemplate, tT153Report.InvoiceSeries);
                List<int> a = new List<int> { FromNo1, FromNo2, FromNo3, FromNo4 };
                int FromNo = a.Max() > FrNo ? a.Max() +1 : FrNo;
                lstAnnounPublish.Add(new TT153AnnouncementPublishDetail
                {
                    Status = false,
                    TT153ReportID = item.TT153ReportID,
                    TT153PublishInvoiceDetailID = item.ID,
                    InvoiceTemplate = tT153Report.InvoiceTemplate,
                    InvoiceType = item.InvoiceType,
                    InvoiceType_string = tT153Report.InvoiceTypeString,
                    InvoiceTypeID = item.InvoiceTypeID,
                    InvoiceSeries = item.InvoiceSeries,
                    FromNo = FromNo.ToString().PadLeft(7, '0'),
                    ToNo = ToNo.ToString().PadLeft(7, '0'),
                    Quantity = (ToNo - FromNo) +1
                });
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
            }
            else
            {
                foreach (TT153AnnouncementPublishDetail item_Detail in lstAnnounPublish)
                {
                    foreach (TT153AdjustAnnouncementDetailListInvoice item in _select.TT153AdjustAnnouncementDetailListInvoices)
                    {
                        if (item.TT153ReportID == item_Detail.TT153ReportID)
                        {
                            item_Detail.Status = true;
                            item_Detail.FromNo = item.FromNo;
                            item_Detail.ToNo = item.ToNo;
                            item_Detail.Quantity = Convert.ToInt32(item.Quantity);
                        }
                    }
                }
            }

        }
        private void InitializeGUI()
        {
            LoaduGrid1();
            LoaduGrid2();
        }

        #region Load Grid
        private void LoaduGrid1()
        {
            uGrid1.SetDataBinding(lstChangeInfo, "");
            Utils.ConfigGrid(uGrid1, ConstDatabase.ChangeInformation_TableName);
            uGrid1.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid1.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid1.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid1.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid1.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            this.uGrid1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = false;
            uGrid1.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid1.DisplayLayout.Bands[0].Columns["OldInformation"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid1.DisplayLayout.Bands[0].Columns["NewInformation"].CellClickAction = CellClickAction.EditAndSelectText;
        }

        private void LoaduGrid2()
        {
            uGrid2.SetDataBinding(lstAnnounPublish, "");
            Utils.ConfigGrid(uGrid2, ConstDatabase.TT153AnnouncementPublishDetail_TableName);
            uGrid2.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid2.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid2.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid2.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid2.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            //string[] a = { "Status", "FromNo", "ToNo" };
            //foreach(string item in a)
            //{
            //    uGrid2.DisplayLayout.Bands[0].Columns[item].CellClickAction = CellClickAction.EditAndSelectText;
            //}
            uGrid2.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            Utils.FormatNumberic(uGrid2.DisplayLayout.Bands[0].Columns["ToNo"], 1);
            Utils.FormatNumberic(uGrid2.DisplayLayout.Bands[0].Columns["FromNo"], 1);
        }
        #endregion

        public TT153AdjustAnnouncement ObjandGUI(TT153AdjustAnnouncement input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.Date = Utils.GetRefDateTime(refTime, Utils.StringToDateTime(dteDate.Text) ?? DateTime.Now);
                input.No = txtNo.Text;
                input.GetDetectionObject = txtGetDetectionObject.Text;
                input.Description = txtDescription.Text;
                input.RespresentationInLaw = txtRespresentationInLaw.Text;
                input.PublishInvoiceNo = txtPublishInvoiceNo.Text;
                input.PublishInvoiceDate = dtePublishInvoiceDate.DateTime;
                input.Status = int.Parse(cbbStatus.Value.ToString());
                input.AttachFileName = txtAttachFileName.Text;
                if (input.AttachFileName != null && input.AttachFileName != "")
                    if (linkFile != null && linkFile != "")
                        input.AttachFileContent = Utils.FileToByte(linkFile);

                if (input.TT153AdjustAnnouncementDetails.Count > 0)
                {
                    foreach (ChangeInformation item in lstChangeInfo)
                    {
                        int i;
                        bool check = false;
                        for (i = 0; i < input.TT153AdjustAnnouncementDetails.Count; i++)
                        {
                            if (input.TT153AdjustAnnouncementDetails[i].Name == item.NameInformation)
                            {
                                check = true;
                                break;
                            }
                        }
                        if (check)
                        {
                            if (item.Status == true)
                            {
                                input.TT153AdjustAnnouncementDetails[i].OldInformation = item.OldInformation;
                                input.TT153AdjustAnnouncementDetails[i].NewInformation = item.NewInformation;
                            }
                            else
                            {
                                input.TT153AdjustAnnouncementDetails.RemoveAt(i);
                            }
                        }
                        else
                        {
                            TT153AdjustAnnouncementDetail temp = new TT153AdjustAnnouncementDetail();
                            if (item.Status == true)
                            {
                                temp.TT153AdjustAnnouncementID = input.ID;
                                temp.Name = item.NameInformation;
                                temp.NewInformation = item.NewInformation;
                                temp.OldInformation = item.OldInformation;
                                input.TT153AdjustAnnouncementDetails.Add(temp);
                            }
                        }
                    }
                }
                else
                {
                    foreach (ChangeInformation item in lstChangeInfo)
                    {
                        TT153AdjustAnnouncementDetail temp = new TT153AdjustAnnouncementDetail();
                        if (item.Status == true)
                        {
                            temp.TT153AdjustAnnouncementID = input.ID;
                            temp.Name = item.NameInformation;
                            temp.NewInformation = item.NewInformation;
                            temp.OldInformation = item.OldInformation;
                            input.TT153AdjustAnnouncementDetails.Add(temp);
                        }
                    }
                }
                if (chkAnnoun.Checked)
                    if (input.TT153AdjustAnnouncementDetailListInvoices.Count > 0)
                    {
                        foreach (TT153AnnouncementPublishDetail item in lstAnnounPublish)
                        {
                            int i;
                            bool check = false;
                            for (i = 0; i < input.TT153AdjustAnnouncementDetailListInvoices.Count; i++)
                            {
                                if (input.TT153AdjustAnnouncementDetailListInvoices[i].TT153ReportID == item.TT153ReportID)
                                {
                                    check = true;
                                    break;
                                }
                            }
                            if (check)
                            {
                                if (item.Status == true)
                                {
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].TT153ReportID = item.TT153ReportID;
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].InvoiceTypeID = item.InvoiceTypeID;
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].InvoiceSeries = item.InvoiceSeries;
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].InvoiceType = item.InvoiceType;
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].FromNo = item.FromNo;
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].ToNo = item.ToNo;
                                    input.TT153AdjustAnnouncementDetailListInvoices[i].Quantity = (decimal)item.Quantity;
                                }
                                else
                                {
                                    input.TT153AdjustAnnouncementDetailListInvoices.RemoveAt(i);
                                }
                            }
                            else
                            {
                                TT153AdjustAnnouncementDetailListInvoice temp = new TT153AdjustAnnouncementDetailListInvoice();
                                if (item.Status == true)
                                {
                                    temp.TT153AdjustAnnouncementID = input.ID;
                                    temp.TT153ReportID = item.TT153ReportID;
                                    temp.InvoiceTypeID = item.InvoiceTypeID;
                                    temp.InvoiceSeries = item.InvoiceSeries;
                                    temp.InvoiceType = item.InvoiceType;
                                    temp.FromNo = item.FromNo;
                                    temp.ToNo = item.ToNo;
                                    temp.Quantity = (decimal)item.Quantity;
                                    input.TT153AdjustAnnouncementDetailListInvoices.Add(temp);
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (TT153AnnouncementPublishDetail item in lstAnnounPublish)
                        {
                            TT153AdjustAnnouncementDetailListInvoice temp = new TT153AdjustAnnouncementDetailListInvoice();
                            if (item.Status == true)
                            {
                                temp.TT153AdjustAnnouncementID = input.ID;
                                temp.TT153ReportID = item.TT153ReportID;
                                temp.InvoiceTypeID = item.InvoiceTypeID;
                                temp.InvoiceSeries = item.InvoiceSeries;
                                temp.InvoiceType = item.InvoiceType;
                                temp.FromNo = item.FromNo;
                                temp.ToNo = item.ToNo;
                                temp.Quantity = (decimal)item.Quantity;
                                input.TT153AdjustAnnouncementDetailListInvoices.Add(temp);
                            }
                        }
                    }
                else
                {
                    for (int i = input.TT153AdjustAnnouncementDetailListInvoices.Count - 1; i >= 0; i--)
                    {
                        input.TT153AdjustAnnouncementDetailListInvoices.RemoveAt(i);
                    }
                }
            }
            else
            {
                dteDate.DateTime = input.Date;
                txtNo.Value = input.No;
                txtGetDetectionObject.Value = input.GetDetectionObject;
                txtDescription.Value = input.Description;
                txtRespresentationInLaw.Value = input.RespresentationInLaw;
                txtPublishInvoiceNo.Value = input.PublishInvoiceNo;
                dtePublishInvoiceDate.DateTime = input.PublishInvoiceDate;
                cbbStatus.Value = input.Status;
                txtAttachFileName.Value = input.AttachFileName;
                if (input.TT153AdjustAnnouncementDetailListInvoices.Count > 0)
                {
                    chkAnnoun.Checked = true;
                }
            }
            return input;
        }
        public List<ChangeInformation> Create()
        {
            //lstChangeInfo = new List<ChangeInformation>();
            lstChangeInfo.Add(new ChangeInformation
            {
                NameInformation = _ISystemOptionService.GetByCode("TTDN_Tencty").Name,
                OldInformation = _ISystemOptionService.GetByCode("TTDN_Tencty").Data,
            });
            lstChangeInfo.Add(new ChangeInformation
            {
                NameInformation = _ISystemOptionService.GetByCode("TTDN_Diachi").Name,
                OldInformation = _ISystemOptionService.GetByCode("TTDN_Diachi").Data,
            });
            lstChangeInfo.Add(new ChangeInformation
            {
                NameInformation = _ISystemOptionService.GetByCode("TTDN_PhoneNumber").Name,
                OldInformation = _ISystemOptionService.GetByCode("TTDN_PhoneNumber").Data,
            });
            if (ConstFrm.optStatusForm.Add != _statusForm)
            {
                foreach (TT153AdjustAnnouncementDetail item in _select.TT153AdjustAnnouncementDetails)
                {
                    foreach (ChangeInformation item_Change in lstChangeInfo)
                    {
                        if (item_Change.NameInformation == item.Name)
                        {
                            item_Change.Status = true;
                            item_Change.OldInformation = item.OldInformation;
                            item_Change.NewInformation = item.NewInformation;
                            break;
                        }
                    }
                }
            }
            return lstChangeInfo;
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog();

            var result = openFile.ShowDialog(this);

            // giới hạn dung lượng file cho phép là 5 MB
            // giới hạn dung lượng file cho phép là 10 MB
            linkFile = openFile.FileName;
            if (linkFile != null && linkFile != "")
            {
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 10240)
                {
                    MSG.Error("Dung lượng file vượt quá mức cho phép (10MB)");
                    return;
                }
                String[] Name = openFile.FileName.Split('\\');
                txtAttachFileName.Value = Name.Last().ToString();
            }
        }
        public bool CheckIsNumber(String Text)
        {
            bool kq = true;
            //if (Text == null || Text.Length == 0) return false;
            foreach (char c in Text)
            {
                if (!char.IsDigit(c) && c != '.')
                {
                    return false;
                }
            }
            return kq;
        }

        public bool CheckError()
        {
            bool kq = true;
            //if(chkAnnoun.Checked)
            //foreach (TT153AnnouncementPublishDetail item in lstAnnounPublish)
            //{
            //    if (item.Status)
            //    {
            //        if (item.Quantity <= 0)
            //        {
            //            MSG.Warning("Số lượng phải lớn hơn 0");
            //            return false;
            //        }
            //        if (!CheckIsNumber(item.ToNo) || !CheckIsNumber(item.FromNo) || item.FromNo.Length == 0 || item.ToNo.Length == 0)
            //        {
            //            MSG.Warning("Nhập sai định dạng số");
            //            return false;
            //        }
            //    }
            //}
            foreach (ChangeInformation item in lstChangeInfo)
            {
                if (lstChangeInfo.Count(n => n.Status == true) == 0)
                {
                    MSG.Warning("Chưa nhập thông tin thay đổi. Vui lòng kiểm tra lại");
                    return false;
                }

            }
            if (chkAnnoun.Checked == true)
            {
                foreach (TT153AnnouncementPublishDetail item in lstAnnounPublish)
                {
                    if (lstAnnounPublish.Count(n => n.Status == true) == 0)
                    {
                        MSG.Warning("Bạn chưa chọn thông tin hóa đơn chưa sử dụng");
                        return false;
                    }
                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153AdjustAnnouncementService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                {
                    MSG.Error("Số thông báo đã tồn tạ");
                    return false;
                }
            }
            else
            {
                if (txtNo.Text != _select.No)
                    if (_ITT153AdjustAnnouncementService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                    {
                        MSG.Error("Số thông báo đã tồn tại");
                        return false;
                    }

            }
            return kq;
        }
        private void uGrid2_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("ToNo") || e.Cell.Column.Key.Equals("FromNo"))
            {
                if (e.Cell.Row.Cells["FromNo"].Value != null && e.Cell.Row.Cells["ToNo"].Value != null && e.Cell.Row.Cells["FromNo"].Value != "" && e.Cell.Row.Cells["ToNo"].Value != "")
                {
                    if (int.Parse(e.Cell.Row.Cells["ToNo"].Value.ToString().Trim('_')) != 0 && int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString().Trim('_')) != 0)
                    {
                        int result = int.Parse(e.Cell.Row.Cells["ToNo"].Value.ToString().Trim('_')) - int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString().Trim('_')) + 1;
                        e.Cell.Row.Cells["Quantity"].Value = result;

                    }
                    else
                    {
                        e.Cell.Row.Cells["Quantity"].Value = 0;
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            TT153AdjustAnnouncement temp;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                temp = new TT153AdjustAnnouncement();
            }
            else
            {
                temp = _ITT153AdjustAnnouncementService.Getbykey(_select.ID);
            }
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            #region Fill giữ liệu
            temp = ObjandGUI(temp, true);
            #endregion
            #region CSDL
            _ITT153AdjustAnnouncementService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153AdjustAnnouncementService.CreateNew(temp);
            }
            else
            {
                _ITT153AdjustAnnouncementService.Update(temp);
            }
            try
            {
                _ITT153AdjustAnnouncementService.CommitTran();
            }
            catch (Exception ex)
            {
                _ITT153AdjustAnnouncementService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu." + ex.Message);
                return;

            }
            #endregion
            #region xử lý form, kết thúc form
            List<TT153AdjustAnnouncement> lst = _ITT153AdjustAnnouncementService.GetAll();
            _ITT153AdjustAnnouncementService.UnbindSession(lst);
            Utils.ClearCacheByType<TT153AdjustAnnouncement>();
            Utils.ClearCacheByType<TT153AdjustAnnouncementDetailListInvoice>();
            Utils.ClearCacheByType<TT153AdjustAnnouncementDetail>();
            IsClose = true;
            MSG.Information("Lưu lại thành công.");
            _select = temp;
            btnSave.Enabled = false;
            btnPrint.Enabled = true;
            _statusForm = ConstFrm.optStatusForm.View;
            #endregion
        }

        private void chkAnnoun_CheckedChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
            //UltraGridBand band = uGrid2.DisplayLayout.Bands[0];
            //UltraGridColumn ugc = band.Columns["Status"];
            //if (chkAnnoun.Checked)
            //{
            //    uGrid2.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            //    ugc.CellActivation = Activation.AllowEdit;
            //}
            //else
            //{
            //    ugc.CellActivation = Activation.NoEdit;
            //    uGrid2.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.RowSelect;
            //}
        }

        private void btnPrint_DroppingDown(object sender, CancelEventArgs e)
        {
            cms4Button.Show(btnPrint, new Point(0, btnPrint.Height));
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (_select.AttachFileContent == null || _select.AttachFileName == null || _select.AttachFileName == "")
            {
                MSG.Warning("không có file để tải về");
                return;
            }
            var openfolder = new FolderBrowserDialog();
            openfolder.Description = "Chọn thư mục lưu file";
            openfolder.ShowNewFolderButton = false;
            openfolder.ShowDialog(this);
            string linkFileSave = openfolder.SelectedPath;
            if (linkFileSave != null && linkFileSave != "")
            {
                Utils.databaseToFile(_select.AttachFileContent, linkFileSave + "\\" + _select.AttachFileName);
                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDown_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Tải xuống file đính kèm", btnDown);
        }
        #region TimeForm
        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                var f = new MsgRefDateTime(refTime);
                f.Text = "Thiết lập thời gian";
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
            refTime = frm.RefDateTime;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
        }
        #endregion

        #endregion

        private void uGrid2_AfterExitEditMode(object sender, EventArgs e)
        {
            UltraGridCell cell = uGrid2.ActiveCell;
            if (cell.Column.Key.Equals("FromNo") || cell.Column.Key.Equals("ToNo"))
            {
                if (cell.Value != null && cell.Value != "")
                {
                    string t = cell.Value.ToString();
                    cell.Value = t.PadLeft(7, '0');
                }
                else
                {
                    cell.Row.Cells["Quantity"].Value = 0;
                }
            }
        }

        private void chkAnnoun_Validated(object sender, EventArgs e)
        {
            //if (chkAnnoun.Checked)
            //{
            //    uGrid2.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            //}
            //else
            //{
            //    uGrid2.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.RowSelect;
            //}
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            cms4Button.Show(btnPrint, new Point(0, btnPrint.Height));
        }

        private void cms4Button_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            //if (cms4Button.Items.ContainsKey("Bảng kê hóa đơn chưa sử dụng"))
            //{
            //    Utils.Print("TT153-BK01-AC", _select, this);
            //}
        }

        private void BKHDCSD_Click(object sender, EventArgs e)
        {
            if (chkAnnoun.Checked == true)
            {
                Utils.Print("TT153-BK01-AC", _ITT153AdjustAnnouncementService.Getbykey(_select.ID), this);
            }
            else
            {
                MSG.Warning("Không có dữ liệu bảng kê được đính kèm");
            }
        }

        private void TBDCTT_Click(object sender, EventArgs e)
        {
            Utils.Print("TT153-TB04-AC", _ITT153AdjustAnnouncementService.Getbykey(_select.ID), this);
        }

        private void txtNo_ValueChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
        }

        private void uGrid1_CellChange(object sender, CellEventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
        }

        private void btnBrowser_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Chọn file đính kèm", btnBrowser);
        }
    }
}