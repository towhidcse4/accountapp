﻿namespace Accounting
{
    partial class FTT153PublishInvoiceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance("Thông báo phát hành hóa đơn", 146035063);
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance("Thông báo phát hành hóa đơn điện tử", 146076797);
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.PrintOrderPrintingInvoice = new System.Windows.Forms.ToolStripMenuItem();
            this.ElectronicInvoice = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnPrint = new Infragistics.Win.Misc.UltraDropDownButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.cms4Button = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.thôngBáoPhátHànhHóaĐơnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.popUp = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.palBody2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cbbStatus = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.txtRepresentationInLaw = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReceiptedTaxOffical = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.cms4Button.SuspendLayout();
            this.palBody2.ClientArea.SuspendLayout();
            this.palBody2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepresentationInLaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiptedTaxOffical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PrintOrderPrintingInvoice,
            this.ElectronicInvoice});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(299, 48);
            // 
            // PrintOrderPrintingInvoice
            // 
            this.PrintOrderPrintingInvoice.Name = "PrintOrderPrintingInvoice";
            this.PrintOrderPrintingInvoice.Size = new System.Drawing.Size(298, 22);
            this.PrintOrderPrintingInvoice.Text = "Thông báo phát hành hóa đơn tự in/đặt in";
            this.PrintOrderPrintingInvoice.Click += new System.EventHandler(this.PrintOrderPrintingInvoice_Click);
            // 
            // ElectronicInvoice
            // 
            this.ElectronicInvoice.Name = "ElectronicInvoice";
            this.ElectronicInvoice.Size = new System.Drawing.Size(298, 22);
            this.ElectronicInvoice.Text = "Thông báo phát hành hóa đơn điện tử";
            this.ElectronicInvoice.Click += new System.EventHandler(this.ElectronicInvoice_Click);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnPrint);
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Location = new System.Drawing.Point(2, 337);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(876, 49);
            this.ultraGroupBox3.TabIndex = 11;
            // 
            // btnPrint
            // 
            this.btnPrint.AllowDrop = true;
            appearance1.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.btnPrint.Appearance = appearance1;
            this.btnPrint.Appearances.Add(appearance2);
            this.btnPrint.Appearances.Add(appearance3);
            this.btnPrint.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnPrint.Location = new System.Drawing.Point(619, 14);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 30);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "In";
            this.btnPrint.DroppingDown += new System.ComponentModel.CancelEventHandler(this.btnPrint_DroppingDown);
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            appearance4.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance4;
            this.btnClose.Location = new System.Drawing.Point(781, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance5;
            this.btnSave.Location = new System.Drawing.Point(700, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cms4Button
            // 
            this.cms4Button.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thôngBáoPhátHànhHóaĐơnToolStripMenuItem,
            this.thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem});
            this.cms4Button.Name = "cms4Button";
            this.cms4Button.Size = new System.Drawing.Size(68, 48);
            // 
            // thôngBáoPhátHànhHóaĐơnToolStripMenuItem
            // 
            this.thôngBáoPhátHànhHóaĐơnToolStripMenuItem.Name = "thôngBáoPhátHànhHóaĐơnToolStripMenuItem";
            this.thôngBáoPhátHànhHóaĐơnToolStripMenuItem.Size = new System.Drawing.Size(67, 22);
            // 
            // thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem
            // 
            this.thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem.Name = "thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem";
            this.thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem.Size = new System.Drawing.Size(67, 22);
            // 
            // palBody2
            // 
            // 
            // palBody2.ClientArea
            // 
            this.palBody2.ClientArea.Controls.Add(this.uGrid);
            this.palBody2.Location = new System.Drawing.Point(2, 121);
            this.palBody2.Name = "palBody2";
            this.palBody2.Size = new System.Drawing.Size(876, 210);
            this.palBody2.TabIndex = 9;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(876, 210);
            this.uGrid.TabIndex = 10;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.AfterCellUpdate);
            this.uGrid.AfterExitEditMode += new System.EventHandler(this.uGrid_AfterExitEditMode);
            this.uGrid.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid_BeforeRowActivate);
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            this.uGrid.CellDataError += new Infragistics.Win.UltraWinGrid.CellDataErrorEventHandler(this.uGrid_CellDataError);
            // 
            // cbbStatus
            // 
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Chưa có hiệu lực";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Đã có hiệu lực";
            this.cbbStatus.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.cbbStatus.Location = new System.Drawing.Point(592, 50);
            this.cbbStatus.Name = "cbbStatus";
            this.cbbStatus.Size = new System.Drawing.Size(215, 21);
            this.cbbStatus.TabIndex = 6;
            this.cbbStatus.Text = "Đã có hiệu lực";
            // 
            // txtRepresentationInLaw
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.txtRepresentationInLaw.Appearance = appearance6;
            this.txtRepresentationInLaw.AutoSize = false;
            this.txtRepresentationInLaw.Location = new System.Drawing.Point(247, 49);
            this.txtRepresentationInLaw.Name = "txtRepresentationInLaw";
            this.txtRepresentationInLaw.Size = new System.Drawing.Size(208, 22);
            this.txtRepresentationInLaw.TabIndex = 5;
            // 
            // ultraLabel4
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.Location = new System.Drawing.Point(64, 50);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(157, 22);
            this.ultraLabel4.TabIndex = 1;
            this.ultraLabel4.Text = "Người đại diện theo pháp luật";
            // 
            // txtReceiptedTaxOffical
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.txtReceiptedTaxOffical.Appearance = appearance8;
            this.txtReceiptedTaxOffical.AutoSize = false;
            this.txtReceiptedTaxOffical.Location = new System.Drawing.Point(247, 78);
            this.txtReceiptedTaxOffical.Name = "txtReceiptedTaxOffical";
            this.txtReceiptedTaxOffical.Size = new System.Drawing.Size(560, 22);
            this.txtReceiptedTaxOffical.TabIndex = 7;
            // 
            // ultraLabel3
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance9;
            this.ultraLabel3.Location = new System.Drawing.Point(64, 78);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(177, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Cơ quan thuế tiếp nhận thông báo";
            // 
            // txtNo
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.txtNo.Appearance = appearance10;
            this.txtNo.AutoSize = false;
            this.txtNo.Location = new System.Drawing.Point(592, 21);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(215, 22);
            this.txtNo.TabIndex = 4;
            // 
            // ultraLabel9
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance11;
            this.ultraLabel9.Location = new System.Drawing.Point(502, 23);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(74, 22);
            this.ultraLabel9.TabIndex = 8;
            this.ultraLabel9.Text = "Số thông báo";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(247, 22);
            this.txtDate.Name = "txtDate";
            this.txtDate.Nullable = false;
            this.txtDate.Size = new System.Drawing.Size(208, 21);
            this.txtDate.TabIndex = 3;
            // 
            // ultraLabel5
            // 
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance12;
            this.ultraLabel5.Location = new System.Drawing.Point(502, 50);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(76, 22);
            this.ultraLabel5.TabIndex = 14;
            this.ultraLabel5.Text = "Trạng thái";
            // 
            // ultraLabel1
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance13;
            this.ultraLabel1.Location = new System.Drawing.Point(64, 23);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Ngày lập";
            // 
            // FTT153PublishInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 389);
            this.Controls.Add(this.cbbStatus);
            this.Controls.Add(this.ultraLabel4);
            this.Controls.Add(this.txtRepresentationInLaw);
            this.Controls.Add(this.txtReceiptedTaxOffical);
            this.Controls.Add(this.palBody2);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.txtNo);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.ultraLabel9);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.ultraLabel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(894, 428);
            this.MinimizeBox = false;
            this.Name = "FTT153PublishInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông báo phát hành hóa đơn";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.cms4Button.ResumeLayout(false);
            this.palBody2.ClientArea.ResumeLayout(false);
            this.palBody2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepresentationInLaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiptedTaxOffical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem PrintOrderPrintingInvoice;
        private System.Windows.Forms.ToolStripMenuItem ElectronicInvoice;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraDropDownButton btnPrint;
        private System.Windows.Forms.ContextMenuStrip cms4Button;
        private System.Windows.Forms.ToolStripMenuItem thôngBáoPhátHànhHóaĐơnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngBáoPhátHànhHóaĐơnĐiệnTửToolStripMenuItem;
        private Infragistics.Win.Misc.UltraPopupControlContainer popUp;
        private Infragistics.Win.Misc.UltraPanel palBody2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbStatus;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRepresentationInLaw;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReceiptedTaxOffical;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    }
}