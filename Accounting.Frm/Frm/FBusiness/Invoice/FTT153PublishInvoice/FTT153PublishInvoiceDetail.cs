﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;


namespace Accounting
{
    public partial class FTT153PublishInvoiceDetail : CustormForm
    {
        #region Khai báo
        public readonly IInvoiceTypeService _IInvoiceTypeService;
        public readonly ITT153InvoiceTemplateService _ITT153InvoiceTemplateService;
        public readonly ITT153RegisterInvoiceService _ITT153RegisterInvoiceService;
        public readonly ITT153ReportService _ITT153ReportService;
        public readonly ITT153PublishInvoiceDetailService _ITT153PublishInvoiceDetailService;
        public readonly ITT153PublishInvoiceService _ITT153PublishInvoiceService;
        public readonly ITT153RegisterInvoiceDetailService _ITT153RegisterInvoiceDetailService;
        public readonly ISystemOptionService _ISystemOptionService;
        List<TT153PublishRegister> lstTT153PublicRegister;
        TT153PublishInvoice _select;
        string[] ColumEdit = { "FromNo", "ToNo", "Quantity_Int", "StartUsing", "CompanyName", "CompanyTaxCode", "ContractNo", "DSigned" };
        int _statusForm = -1;
        int ToNo;
        DateTime refTime;
        int Quantity;
        #endregion
        #region Khởi tạo
        public FTT153PublishInvoiceDetail(TT153PublishInvoice temp, List<TT153PublishInvoice> lstTT153PublishInvoice, int statusForm)
        {
            _IInvoiceTypeService = IoC.Resolve<IInvoiceTypeService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153PublishInvoiceService = IoC.Resolve<ITT153PublishInvoiceService>();
            _ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _ITT153InvoiceTemplateService = IoC.Resolve<ITT153InvoiceTemplateService>();
            _ITT153RegisterInvoiceService = IoC.Resolve<ITT153RegisterInvoiceService>();
            _ITT153RegisterInvoiceDetailService = IoC.Resolve<ITT153RegisterInvoiceDetailService>();
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            lstTT153PublicRegister = new List<TT153PublishRegister>();
            InitializeComponent();
            //Utils.SortFormControls(this, ultraPanel1.TabIndex);
            _select = temp;
            _statusForm = statusForm;
            cbbStatus.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            #region Thêm button vào dataeditor
            //var btnRefDateTime = new EditorButton("btnRefDateTime");
            //var appearance = new Infragistics.Win.Appearance
            //{
            //    Image = Properties.Resources.clock,
            //    ImageHAlign = Infragistics.Win.HAlign.Center,
            //    ImageVAlign = Infragistics.Win.VAlign.Middle
            //};
            //btnRefDateTime.Appearance = appearance;
            //btnRefDateTime.Key = "btnRefDateTime";
            //btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            //txtDate.ButtonsRight.Add(btnRefDateTime);
            #endregion
            #region Thiết lập giá trị khởi tạo cho Form 
            if (ConstFrm.optStatusForm.Add == statusForm)
            {
                txtReceiptedTaxOffical.Value = _ISystemOptionService.Getbykey(8).Data;
                txtRepresentationInLaw.Value = _ISystemOptionService.Getbykey(21).Data;
                refTime = DateTime.Now;
                foreach (TT153RegisterInvoiceDetail item in _ITT153RegisterInvoiceDetailService.GetAll())
                {
                    TT153Report ttReport = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    ToNo = 1;
                    if (_ITT153PublishInvoiceDetailService.GetAllWithTT153ReportID(ttReport.ID).Count > 0)
                    {
                        ToNo = _ITT153PublishInvoiceDetailService.GetWithMaxToNo(ttReport.ID) + 1;
                    }
                    lstTT153PublicRegister.Add(new TT153PublishRegister
                    {
                        Status = false,
                        TT153RegisterInvoiceDetailID = item.ID,
                        ReportID = item.TT153ReportID,
                        InvoiceForm = ttReport.InvoiceForm,
                        InvoiceForm_String = ttReport.InvoiceFormString,
                        InvoiceType = ttReport.InvoiceType,
                        InvoiceType_String = ttReport.InvoiceTypeString,
                        InvoiceTypeID = ttReport.InvoiceTypeID,
                        InvoiceSeries = ttReport.InvoiceSeries,
                        InvoiceTemplate = ttReport.InvoiceTemplate,
                        FromNo = ToNo.ToString().PadLeft(7, '0'),
                        ToNo = "",
                        StartUsing = DateTime.Now.AddDays(2),
                    });
                }
                btnSave.Enabled = true;
                btnPrint.Enabled = false;
            }
            else
            {
                refTime = _select.Date;
                #region Code cũ
                //foreach (TT153RegisterInvoiceDetail item in _ITT153RegisterInvoiceDetailService.GetAll())
                //{
                //    TT153Report ttReport = _ITT153ReportService.Getbykey(item.TT153ReportID);
                //    //ToNo = 1;
                //    //if (_ITT153PublishInvoiceDetailService.GetAll().Count(n => n.TT153ReportID == ttReport.ID) > 0)
                //    //{
                //    //    foreach (TT153PublishInvoiceDetail item_ in _ITT153PublishInvoiceDetailService.GetAll().Where(n => n.TT153ReportID == ttReport.ID))
                //    //    {
                //    //        if(CheckIsNumber(item_.ToNo))
                //    //        if (int.Parse(item_.ToNo) >= ToNo)
                //    //        {
                //    //            ToNo = int.Parse(item_.ToNo);
                //    //            ToNo++;
                //    //        }
                //    //    }
                //    //}
                //    ToNo = 1;
                //    if (_ITT153PublishInvoiceDetailService.GetAllWithTT153ReportID(ttReport.ID).Count > 0)
                //    {
                //        ToNo = _ITT153PublishInvoiceDetailService.GetWithMaxToNo(ttReport.ID) + 1;
                //    }
                //    lstTT153PublicRegister.Add(new TT153PublishRegister
                //    {
                //        Status = false,
                //        TT153RegisterInvoiceDetailID = item.ID,
                //        ReportID = item.TT153ReportID,
                //        InvoiceForm = ttReport.InvoiceForm,
                //        InvoiceForm_String = ttReport.InvoiceFormString.PadLeft(7, '0'),
                //        InvoiceType = ttReport.InvoiceType,
                //        InvoiceType_String = ttReport.InvoiceTypeString.PadLeft(7, '0'),
                //        InvoiceTypeID = ttReport.InvoiceTypeID,
                //        InvoiceSeries = ttReport.InvoiceSeries,
                //        InvoiceTemplate = ttReport.InvoiceTemplate,
                //        FromNo = ToNo.ToString().PadLeft(7, '0'),
                //        ToNo = "",
                //        StartUsing = DateTime.Now.AddDays(2),
                //    });
                //}
                //foreach (TT153PublishRegister item_Detail in lstTT153PublicRegister)
                //{
                //    foreach (TT153PublishInvoiceDetail item in temp.TT153PublishInvoiceDetails)
                //    {
                //        if (item.TT153ReportID == item_Detail.ReportID)
                //        {
                //            item_Detail.Status = true;
                //            item_Detail.FromNo = item.FromNo.PadLeft(7, '0');
                //            item_Detail.ToNo = item.ToNo.PadLeft(7, '0');
                //            item_Detail.StartUsing = item.StartUsing;
                //            item_Detail.Quantity_Int = Convert.ToInt32(item.Quantity).ToString();
                //            item_Detail.CompanyName = item.CompanyName;
                //            item_Detail.CompanyTaxCode = item.CompanyTaxCode;
                //            item_Detail.ContractNo = item.ContractNo;
                //            item_Detail.DSigned = item.ContractDate;
                //        }
                //    }
                //}
                #endregion
                #region Code mới
                foreach (TT153PublishInvoiceDetail item in temp.TT153PublishInvoiceDetails)
                {
                    TT153Report ttReport = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    TT153PublishRegister tT153PublishRegister = new TT153PublishRegister();

                    tT153PublishRegister.ReportID = item.TT153ReportID;
                    tT153PublishRegister.InvoiceForm = ttReport.InvoiceForm;
                    tT153PublishRegister.InvoiceForm_String = ttReport.InvoiceFormString;
                    tT153PublishRegister.InvoiceType = ttReport.InvoiceType;
                    tT153PublishRegister.InvoiceType_String = ttReport.InvoiceTypeString;
                    tT153PublishRegister.InvoiceTypeID = ttReport.InvoiceTypeID;
                    tT153PublishRegister.InvoiceSeries = ttReport.InvoiceSeries;
                    tT153PublishRegister.InvoiceTemplate = ttReport.InvoiceTemplate;

                    tT153PublishRegister.Status = true;
                    tT153PublishRegister.FromNo = item.FromNo.PadLeft(7, '0');
                    tT153PublishRegister.ToNo = item.ToNo.PadLeft(7, '0');
                    tT153PublishRegister.StartUsing = item.StartUsing;
                    tT153PublishRegister.Quantity_Int = Convert.ToInt32(item.Quantity).ToString();
                    tT153PublishRegister.CompanyName = item.CompanyName;
                    tT153PublishRegister.CompanyTaxCode = item.CompanyTaxCode;
                    tT153PublishRegister.ContractNo = item.ContractNo;
                    tT153PublishRegister.DSigned = item.ContractDate;
                    lstTT153PublicRegister.Add(tT153PublishRegister);
                }
                #endregion
                ObjandGUI(temp, false);
                btnSave.Enabled = false;
                btnPrint.Enabled = true;
            }
            InitializeGUI();
            WaitingFrm.StopWaiting();
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region LoaduGrid
        private void InitializeGUI()
        {
            LoadUGrid();
        }

        public void LoadUGrid()
        {
            uGrid.SetDataBinding(lstTT153PublicRegister, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TT153PublishRegister_TableName, false);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;


            uGrid.DisplayLayout.Bands[0].Columns["InvoiceForm_String"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceType_String"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["ToNo"].CellActivation = Activation.NoEdit;

            if (ConstFrm.optStatusForm.Add == _statusForm)
            {
                UltraGridBand band = uGrid.DisplayLayout.Bands[0];
                UltraGridColumn ugc = band.Columns["Status"];
                ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                ugc.CellActivation = Activation.AllowEdit;
                ugc.Header.Fixed = true;
                uGrid.DisplayLayout.Bands[0].Columns["Quantity_Int"].CellClickAction = CellClickAction.EditAndSelectText;
                uGrid.DisplayLayout.Bands[0].Columns["FromNo"].CellClickAction = CellClickAction.EditAndSelectText;
            }
            else
            {
                uGrid.DisplayLayout.Bands[0].Columns["Status"].CellActivation = Activation.NoEdit;
                uGrid.DisplayLayout.Bands[0].Columns["StartUsing"].CellActivation = Activation.NoEdit;
                uGrid.DisplayLayout.Bands[0].Columns["Quantity_Int"].CellActivation = Activation.NoEdit;
                uGrid.DisplayLayout.Bands[0].Columns["FromNo"].CellActivation = Activation.NoEdit;

            }

            uGrid.DisplayLayout.Bands[0].Columns["DSigned"].Editor = new UltraDateTimeEditor().Editor;
            uGrid.DisplayLayout.Bands[0].Columns["DSigned"].MaskInput = "{LOC}dd/mm/yyyy";
            uGrid.DisplayLayout.Bands[0].Columns["DSigned"].Format = Constants.DdMMyyyy;



            uGrid.DisplayLayout.Bands[0].Columns["StartUsing"].MaskInput = "dd/mm/yyyy";
            uGrid.DisplayLayout.Bands[0].Columns["Quantity_Int"].MaskInput = "{LOC}nnn,nnn,nnn,nnn,nnn";
            uGrid.DisplayLayout.Bands[0].Columns["FromNo"].MaskInput = "{LOC}nnn,nnn,nnn,nnn,nnn";
            //Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["Quantity_Int"], 1);
            //Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["FromNo"], 1);
        }
        #endregion
        public TT153PublishInvoice ObjandGUI(TT153PublishInvoice input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.Date = Utils.GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
                input.No = txtNo.Text;
                input.ReceiptedTaxOffical = txtReceiptedTaxOffical.Text;
                input.RepresentationInLaw = txtRepresentationInLaw.Text;
                input.Status = int.Parse(cbbStatus.Value.ToString());
                input.IsActive = true;

                if (input.TT153PublishInvoiceDetails.Count > 0)
                {
                    foreach (TT153PublishRegister item in lstTT153PublicRegister)
                    {
                        int i;
                        bool check = false;
                        for (i = 0; i < input.TT153PublishInvoiceDetails.Count; i++)
                        {
                            if (input.TT153PublishInvoiceDetails[i].TT153ReportID == item.ReportID)
                            {
                                check = true;
                                break;
                            }
                        }
                        if (check == true)
                        {
                            if (item.Status == true)
                            {
                                input.TT153PublishInvoiceDetails[i].InvoiceForm = item.InvoiceForm;
                                input.TT153PublishInvoiceDetails[i].InvoiceType = item.InvoiceType;
                                input.TT153PublishInvoiceDetails[i].InvoiceSeries = item.InvoiceSeries;
                                input.TT153PublishInvoiceDetails[i].InvoiceTypeID = item.InvoiceTypeID;
                                input.TT153PublishInvoiceDetails[i].FromNo = item.FromNo;
                                input.TT153PublishInvoiceDetails[i].ToNo = item.ToNo;
                                input.TT153PublishInvoiceDetails[i].Quantity = decimal.Parse(item.Quantity_Int.ToString());
                                input.TT153PublishInvoiceDetails[i].StartUsing = item.StartUsing;
                                input.TT153PublishInvoiceDetails[i].CompanyName = item.CompanyName;
                                input.TT153PublishInvoiceDetails[i].CompanyTaxCode = item.CompanyTaxCode;
                                input.TT153PublishInvoiceDetails[i].ContractNo = item.ContractNo;
                                input.TT153PublishInvoiceDetails[i].ContractDate = item.DSigned;
                            }
                            else
                            {
                                input.TT153PublishInvoiceDetails.RemoveAt(i);
                            }
                        }
                        else
                        {
                            TT153PublishInvoiceDetail temp = new TT153PublishInvoiceDetail();
                            if (item.Status == true)
                            {
                                temp.TT153PublishInvoiceID = input.ID;
                                temp.TT153RegisterInvoiceDetailID = item.TT153RegisterInvoiceDetailID;
                                temp.InvoiceForm = item.InvoiceForm;
                                temp.InvoiceType = item.InvoiceType;
                                temp.InvoiceSeries = item.InvoiceSeries;
                                temp.InvoiceTypeID = item.InvoiceTypeID;
                                temp.FromNo = item.FromNo;
                                temp.ToNo = item.ToNo;
                                temp.Quantity = decimal.Parse(item.Quantity_Int.ToString());
                                temp.StartUsing = item.StartUsing;
                                temp.CompanyName = item.CompanyName;
                                temp.CompanyTaxCode = item.CompanyTaxCode;
                                temp.ContractNo = item.ContractNo;
                                temp.ContractDate = item.DSigned;
                                temp.TT153ReportID = item.ReportID;
                                input.TT153PublishInvoiceDetails.Add(temp);
                            }
                        }
                    }
                }
                else
                {
                    foreach (TT153PublishRegister item in lstTT153PublicRegister)
                    {
                        TT153PublishInvoiceDetail temp = new TT153PublishInvoiceDetail();
                        if (item.Status == true)
                        {
                            temp.TT153RegisterInvoiceDetailID = item.TT153RegisterInvoiceDetailID;
                            temp.TT153PublishInvoiceID = input.ID;
                            temp.InvoiceForm = item.InvoiceForm;
                            temp.InvoiceType = item.InvoiceType;
                            temp.InvoiceSeries = item.InvoiceSeries;
                            temp.InvoiceTypeID = item.InvoiceTypeID;
                            temp.FromNo = item.FromNo;
                            temp.ToNo = item.ToNo;
                            temp.Quantity = decimal.Parse(item.Quantity_Int.ToString());
                            temp.StartUsing = item.StartUsing;
                            temp.CompanyName = item.CompanyName;
                            temp.CompanyTaxCode = item.CompanyTaxCode;
                            temp.ContractNo = item.ContractNo;
                            temp.ContractDate = item.DSigned;
                            temp.TT153ReportID = item.ReportID;
                            input.TT153PublishInvoiceDetails.Add(temp);
                        }
                    }
                }

            }
            else
            {
                txtDate.Value = input.Date;
                txtNo.Value = input.No;
                txtReceiptedTaxOffical.Value = input.ReceiptedTaxOffical;
                txtRepresentationInLaw.Value = input.RepresentationInLaw;
                cbbStatus.Value = input.Status;
            }
            return input;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public virtual void Reset()
        {

        }
        public bool CheckIsNumber(String Text)
        {
            bool kq = true;
            if (Text == null || Text.Length == 0) return false;
            foreach (char c in Text)
            {
                if (!char.IsDigit(c) && c != '.')
                {
                    return false;
                }
            }
            return kq;
        }

        private void AfterCellUpdate(object sender, CellEventArgs e)
        {

            if (e.Cell.Column.Key.Equals("Quantity_Int") || e.Cell.Column.Key.Equals("FromNo"))
            {
                if (e.Cell.Row.Cells["FromNo"].Value != null && e.Cell.Row.Cells["Quantity_Int"].Value != null && e.Cell.Row.Cells["FromNo"].Value != "" && e.Cell.Row.Cells["Quantity_Int"].Value != "")
                {

                    string valueQuantity = e.Cell.Row.Cells["Quantity_Int"].Value.ToString();
                    if (valueQuantity.Contains('.'))
                    {
                        int index = valueQuantity.IndexOf(".");
                        valueQuantity = (index > 0 ? valueQuantity.Substring(0, index) : "");
                    }
                    if (int.Parse(valueQuantity) > 0)
                    {
                        int result = int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString()) + int.Parse(valueQuantity) - 1;
                        e.Cell.Row.Cells["ToNo"].Value = result.ToString().PadLeft(7, '0');
                    }
                }
            }
            //if (e.Cell.Column.Key.Equals("Status"))
            //{
            //    if ((bool)e.Cell.Row.Cells["Status"].Value)
            //    {
            //        foreach (var item in e.Cell.Row.Cells)
            //        {

            //            foreach (string item_ in ColumEdit)
            //            {
            //                if (item.Column.Key.Equals(item_))
            //                    item.Activation = Activation.AllowEdit;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        foreach (var item in e.Cell.Row.Cells)
            //        {
            //            foreach (string item_ in ColumEdit)
            //            {
            //                if (item.Column.Key.Equals(item_))
            //                    item.Activation = Activation.NoEdit;
            //            }
            //        }
            //    }
            //}
        }
        public bool CheckEror()
        {
            bool kq = true;
            foreach (TT153PublishRegister item in lstTT153PublicRegister)
            {
                if (lstTT153PublicRegister.Count(n => n.Status) == 0)
                {
                    MSG.Error("Chưa có mẫu hóa đơn nào được chọn để thông báo phát hành");
                    return false;
                }
                if (item.Status == true)
                {
                    if (item.Quantity == null || string.IsNullOrEmpty(item.Quantity_Int))
                    {
                        MSG.Warning("Số lượng hóa đơn phải lớn hơn 0");
                        Utils.ClearCacheByType<TT153PublishInvoice>();
                        Utils.ClearCacheByType<TT153PublishInvoiceDetail>();
                        return false;
                    }
                    if (item.FromNo == null || string.IsNullOrEmpty(item.FromNo))
                    {
                        MSG.Warning("Số lượng hóa đơn phải lớn hơn 0");
                        Utils.ClearCacheByType<TT153PublishInvoice>();
                        Utils.ClearCacheByType<TT153PublishInvoiceDetail>();
                        return false;
                    }
                    if (int.Parse(item.Quantity_Int) <= 0)
                    {
                        MSG.Warning("Số lượng hóa đơn phải lớn hơn 0");
                        Utils.ClearCacheByType<TT153PublishInvoice>();
                        Utils.ClearCacheByType<TT153PublishInvoiceDetail>();
                        return false;
                    }
                    if (!string.IsNullOrEmpty(item.CompanyTaxCode))
                    {
                        if (!Core.Utils.CheckMST(item.CompanyTaxCode))
                        {
                            MSG.Error(resSystem.MSG_System_44);
                            return false;
                        }
                    }
                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153PublishInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                {
                    MSG.Error("Số thông báo đã tồn tại");
                    return false;
                }
            }
            else
            {
                if (txtNo.Text != _select.No)
                    if (_ITT153PublishInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                    {
                        MSG.Error("Số thông báo đã tồn tại");
                        return false;
                    }

            }


            return kq;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            TT153PublishInvoice temp;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                temp = new TT153PublishInvoice();
            }
            else
            {
                temp = _ITT153PublishInvoiceService.Getbykey(_select.ID);
            }
            #region Check Eror
            if (!CheckEror()) return;
            #endregion
            #region Fill giữ liệu
            temp = ObjandGUI(temp, true);
            #endregion

            #region CSDL
            _ITT153PublishInvoiceService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153PublishInvoiceService.CreateNew(temp);
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                _ITT153PublishInvoiceService.Update(temp);
            }
            try
            {
                _ITT153PublishInvoiceService.CommitTran();
            }
            catch (Exception ex)
            {
                _ITT153PublishInvoiceService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu.");
                return;

            }
            #endregion
            #region xử lý form, kết thúc form
            List<TT153PublishInvoice> lst = _ITT153PublishInvoiceService.GetAll();
            _ITT153PublishInvoiceService.UnbindSession(lst);
            Utils.ClearCacheByType<TT153PublishInvoice>();
            Utils.ClearCacheByType<TT153PublishInvoiceDetail>();
            IsClose = true;
            MSG.Information("Lưu lại thành công.");
            _select = temp;
            _statusForm = ConstFrm.optStatusForm.View;
            btnPrint.Enabled = true;
            btnSave.Enabled = false;
            #endregion

        }
        #endregion

        private void uGrid_BeforeRowActivate(object sender, RowEventArgs e)
        {
            foreach (var item in e.Row.Cells)
            {
                if (item.Column.Key.Equals("ReportID"))
                {
                    ToNo = 1;
                    if (_ITT153PublishInvoiceDetailService.GetAllWithTT153ReportID((Guid)item.Value).Count > 0)
                    {

                        ToNo = _ITT153PublishInvoiceDetailService.GetWithMaxToNo((Guid)item.Value) + 1;
                    }
                }
            }
        }

        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell.Column.Key.Equals("FromNo"))
            {
                if (cell.Value != null && cell.Value != "")
                {
                    string t = cell.Value.ToString();
                    if (int.Parse(t) == 0 || int.Parse(t) < ToNo)
                    {
                        MSG.Warning("Từ số phải lớn hơn hoặc bằng" + ToNo);
                        cell.Value = ToNo.ToString().PadLeft(7, '0');
                    }
                    else
                        cell.Value = t.PadLeft(7, '0');
                }
                else
                {
                    cell.Value = ToNo.ToString().PadLeft(7, '0');
                }
            }
        }

        private void uGrid_CellDataError(object sender, CellDataErrorEventArgs e)
        {
            // CellDataError gets fired when the user attempts to exit the edit mode
            // after entering an invalid value in the cell. There are several properties
            // on the passed in event args that you can set to control the UltraGrid's
            // behaviour.

            // Typically ForceExit is false. The UltraGrid forces exits on cells under
            // circumstances like when it's being disposed of. If ForceExit is true, then 
            // the UltraGrid will ignore StayInEditMode property and exit the cell 
            // restoring the original value ignoring the value you set to StayInEditMode
            // property.
            if (!e.ForceExit)
            {
                // Default for StayInEditMode is true. However you can set it to false to
                // cause the grid to exit the edit mode and restore the original value. We
                // will just leave it true for this example.
                e.StayInEditMode = true;

                // Set the RaiseErrorEvent to false to prevent the grid from raising 
                // the error event and displaying any message.
                e.RaiseErrorEvent = false;

                // Instead display our own message.
                if (this.uGrid.ActiveCell.Column.DataType == typeof(DateTime) || this.uGrid.ActiveCell.Column.Key.Equals("DSigned"))
                {
                    MessageBox.Show(this, "Vui lòng nhập ngày hợp lệ.", "Đầu vào không hợp lệ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //else if (this.uGrid.ActiveCell.Column.DataType == typeof(decimal))
                //{
                //    MessageBox.Show(this, "Please enter a valid numer.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            else
            {
                // Set the RaiseErrorEvent to false to prevent the grid from raising 
                // the error event.
                e.RaiseErrorEvent = false;
            }
        }
        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                var f = new MsgRefDateTime(refTime);
                f.Text = "Thiết lập thời gian";
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            refTime = frm.RefDateTime;
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
        }
        #region Button print
        private void btnPrint_DroppingDown(object sender, CancelEventArgs e)
        {
            contextMenuStrip1.Show(btnPrint, new Point(0, btnPrint.Height));
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(btnPrint, new Point(0, btnPrint.Height));
        }
        #endregion
        private void ElectronicInvoice_Click(object sender, EventArgs e)
        {
            Utils.Print("TT153PublishInvoiceHDDT", _ITT153PublishInvoiceService.Getbykey(_select.ID), this);
        }
        private void PrintOrderPrintingInvoice_Click(object sender, EventArgs e)
        {
            Utils.Print("TT153PublishInvoiceTIDI", _ITT153PublishInvoiceService.Getbykey(_select.ID), this);
        }

        private void txtNo_ValueChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
        }
    }
}