﻿namespace Accounting
{
    partial class FTT153LostInvoiceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FTT153LostInvoiceDetail));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDown = new Infragistics.Win.Misc.UltraButton();
            this.cbbStatus = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDateLost = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.txtAttachFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtRespresentationInLaw = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtGetDetectionObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnPrint = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateLost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRespresentationInLaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGetDetectionObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            // 
            // ultraGroupBox1
            // 
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.btnDown);
            this.ultraGroupBox1.Controls.Add(this.cbbStatus);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.txtDateLost);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.ultraButton1);
            this.ultraGroupBox1.Controls.Add(this.txtAttachFileName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.txtRespresentationInLaw);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.txtGetDetectionObject);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.txtDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, -10);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(845, 222);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // btnDown
            // 
            appearance2.Image = global::Accounting.Properties.Resources.btnSave;
            this.btnDown.Appearance = appearance2;
            this.btnDown.Location = new System.Drawing.Point(425, 172);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(26, 23);
            this.btnDown.TabIndex = 64;
            this.btnDown.Tag = "";
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            this.btnDown.MouseHover += new System.EventHandler(this.btnDown_MouseHover);
            // 
            // cbbStatus
            // 
            valueListItem1.DataValue = 0;
            valueListItem1.DisplayText = "Chưa nộp cho cơ quan thuế";
            valueListItem2.DataValue = 1;
            valueListItem2.DisplayText = "Đã nộp cho cơ quan thuế";
            this.cbbStatus.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.cbbStatus.Location = new System.Drawing.Point(552, 119);
            this.cbbStatus.Name = "cbbStatus";
            this.cbbStatus.Size = new System.Drawing.Size(239, 21);
            this.cbbStatus.TabIndex = 8;
            this.cbbStatus.Text = "Chưa nộp cho cơ quan thuế";
            this.cbbStatus.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel5
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance3;
            this.ultraLabel5.Location = new System.Drawing.Point(460, 119);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(74, 22);
            this.ultraLabel5.TabIndex = 30;
            this.ultraLabel5.Text = "Trạng thái";
            // 
            // txtDateLost
            // 
            this.txtDateLost.Location = new System.Drawing.Point(212, 118);
            this.txtDateLost.Name = "txtDateLost";
            this.txtDateLost.Size = new System.Drawing.Size(175, 21);
            this.txtDateLost.TabIndex = 4;
            this.txtDateLost.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel8
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance4;
            this.ultraLabel8.Location = new System.Drawing.Point(20, 119);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(157, 22);
            this.ultraLabel8.TabIndex = 61;
            this.ultraLabel8.Text = "Ngày mất/cháy/hỏng hóa đơn";
            // 
            // ultraButton1
            // 
            appearance5.Image = global::Accounting.Properties.Resources.iAdd;
            this.ultraButton1.Appearance = appearance5;
            this.ultraButton1.Location = new System.Drawing.Point(389, 173);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(30, 22);
            this.ultraButton1.TabIndex = 7;
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            this.ultraButton1.MouseHover += new System.EventHandler(this.ultraButton1_MouseHover);
            // 
            // txtAttachFileName
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.txtAttachFileName.Appearance = appearance6;
            this.txtAttachFileName.AutoSize = false;
            this.txtAttachFileName.Enabled = false;
            this.txtAttachFileName.Location = new System.Drawing.Point(212, 173);
            this.txtAttachFileName.Name = "txtAttachFileName";
            this.txtAttachFileName.Size = new System.Drawing.Size(171, 22);
            this.txtAttachFileName.TabIndex = 6;
            this.txtAttachFileName.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel7
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance7;
            this.ultraLabel7.Location = new System.Drawing.Point(21, 174);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel7.TabIndex = 49;
            this.ultraLabel7.Text = "Tệp đính kèm";
            // 
            // txtReason
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance8;
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(212, 146);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(579, 22);
            this.txtReason.TabIndex = 5;
            this.txtReason.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel2
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(20, 145);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(177, 26);
            this.ultraLabel2.TabIndex = 45;
            this.ultraLabel2.Text = "Lý do mất/cháy/hỏng hóa đơn";
            // 
            // txtRespresentationInLaw
            // 
            appearance10.TextVAlignAsString = "Middle";
            this.txtRespresentationInLaw.Appearance = appearance10;
            this.txtRespresentationInLaw.AutoSize = false;
            this.txtRespresentationInLaw.Location = new System.Drawing.Point(212, 88);
            this.txtRespresentationInLaw.Name = "txtRespresentationInLaw";
            this.txtRespresentationInLaw.Size = new System.Drawing.Size(579, 22);
            this.txtRespresentationInLaw.TabIndex = 3;
            this.txtRespresentationInLaw.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel4
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance11;
            this.ultraLabel4.Location = new System.Drawing.Point(21, 89);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(157, 22);
            this.ultraLabel4.TabIndex = 43;
            this.ultraLabel4.Text = "Người đại diện theo pháp luật";
            // 
            // txtGetDetectionObject
            // 
            appearance12.TextVAlignAsString = "Middle";
            this.txtGetDetectionObject.Appearance = appearance12;
            this.txtGetDetectionObject.AutoSize = false;
            this.txtGetDetectionObject.Location = new System.Drawing.Point(212, 60);
            this.txtGetDetectionObject.Name = "txtGetDetectionObject";
            this.txtGetDetectionObject.Size = new System.Drawing.Size(579, 22);
            this.txtGetDetectionObject.TabIndex = 2;
            this.txtGetDetectionObject.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel3
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance13;
            this.ultraLabel3.Location = new System.Drawing.Point(20, 61);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(177, 22);
            this.ultraLabel3.TabIndex = 41;
            this.ultraLabel3.Text = "Cơ quan thuế nhận thông báo";
            // 
            // txtNo
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.txtNo.Appearance = appearance14;
            this.txtNo.AutoSize = false;
            this.txtNo.Location = new System.Drawing.Point(552, 30);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(239, 22);
            this.txtNo.TabIndex = 1;
            this.txtNo.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel9
            // 
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance15;
            this.ultraLabel9.Location = new System.Drawing.Point(460, 32);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(74, 22);
            this.ultraLabel9.TabIndex = 39;
            this.ultraLabel9.Text = "Số thông báo";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(212, 32);
            this.txtDate.Name = "txtDate";
            this.txtDate.Nullable = false;
            this.txtDate.Size = new System.Drawing.Size(175, 21);
            this.txtDate.TabIndex = 0;
            this.txtDate.ValueChanged += new System.EventHandler(this.Value_Change);
            // 
            // ultraLabel1
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance16;
            this.ultraLabel1.Location = new System.Drawing.Point(20, 32);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(87, 22);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Ngày lập";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGrid);
            this.ultraGroupBox2.Location = new System.Drawing.Point(1, 218);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(845, 214);
            this.ultraGroupBox2.TabIndex = 2;
            // 
            // uGrid
            // 
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance17;
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance27;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.UseFixedHeaders = true;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(3, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(839, 211);
            this.uGrid.TabIndex = 2;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_AfterCellUpdate);
            this.uGrid.AfterExitEditMode += new System.EventHandler(this.uGrid_AfterExitEditMode);
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnPrint);
            this.ultraGroupBox3.Controls.Add(this.btnClose);
            this.ultraGroupBox3.Controls.Add(this.btnSave);
            this.ultraGroupBox3.Location = new System.Drawing.Point(1, 438);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(845, 49);
            this.ultraGroupBox3.TabIndex = 3;
            // 
            // btnPrint
            // 
            appearance28.Image = ((object)(resources.GetObject("appearance28.Image")));
            this.btnPrint.Appearance = appearance28;
            this.btnPrint.Location = new System.Drawing.Point(594, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 30);
            this.btnPrint.TabIndex = 9;
            this.btnPrint.Text = "In";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            appearance29.Image = ((object)(resources.GetObject("appearance29.Image")));
            this.btnClose.Appearance = appearance29;
            this.btnClose.Location = new System.Drawing.Point(756, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance30.Image = ((object)(resources.GetObject("appearance30.Image")));
            this.btnSave.Appearance = appearance30;
            this.btnSave.Location = new System.Drawing.Point(675, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FTT153LostInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 497);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(867, 536);
            this.MinimizeBox = false;
            this.Name = "FTT153LostInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mất, cháy, hỏng hóa đơn";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateLost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRespresentationInLaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGetDetectionObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtRespresentationInLaw;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtGetDetectionObject;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDate;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAttachFileName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDateLost;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbStatus;
        private Infragistics.Win.Misc.UltraButton btnPrint;
        private Infragistics.Win.Misc.UltraButton btnDown;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}