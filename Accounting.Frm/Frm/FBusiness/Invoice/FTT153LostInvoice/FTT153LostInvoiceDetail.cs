﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;


namespace Accounting
{
    public partial class FTT153LostInvoiceDetail : CustormForm
    {
        #region Khai báo
        public readonly IInvoiceTypeService _IInvoiceTypeService;
        public readonly ITT153InvoiceTemplateService _ITT153InvoiceTemplateService;
        public readonly ITT153RegisterInvoiceService _ITT153RegisterInvoiceService;
        public readonly ITT153ReportService _ITT153ReportService;
        public readonly ITT153PublishInvoiceDetailService _ITT153PublishInvoiceDetailService;
        public readonly ITT153PublishInvoiceService _ITT153PublishInvoiceService;
        public readonly ITT153RegisterInvoiceDetailService _ITT153RegisterInvoiceDetailService;
        public readonly ITT153LostInvoiceService _ITT153LostInvoiceService;
        public readonly ITT153DestructionInvoiceDetailService _ITT153DestructionInvoiceDetailService;
        public readonly ITT153DeletedInvoiceService _ITT153DeletedInvoiceService;
        List<TT153LostPublishInvoiceDetail> lstTT153LostPublishInvoiceDetail;
        public readonly ISystemOptionService _ISystemOptionService;
        public readonly ITT153LostInvoiceDetailService _ITT153LostInvoiceDetailService;
        TT153LostInvoice _select;
        int _statusForm;
        string linkFile;
        DateTime refTime;
        #endregion
        #region Khởi tạo
        public FTT153LostInvoiceDetail(TT153LostInvoice temp, List<TT153LostInvoice> lstTT153LostInvoice, int statusForm)
        {
            _ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _ITT153RegisterInvoiceDetailService = IoC.Resolve<ITT153RegisterInvoiceDetailService>();
            _ITT153ReportService = IoC.Resolve<ITT153ReportService>();
            _ITT153LostInvoiceService = IoC.Resolve<ITT153LostInvoiceService>();
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ITT153DestructionInvoiceDetailService = IoC.Resolve<ITT153DestructionInvoiceDetailService>();
            _ITT153DeletedInvoiceService=IoC.Resolve<ITT153DeletedInvoiceService>();
            lstTT153LostPublishInvoiceDetail = new List<TT153LostPublishInvoiceDetail>();
            _ITT153LostInvoiceDetailService = IoC.Resolve<ITT153LostInvoiceDetailService>();
            InitializeComponent();
            //Utils.SortFormControls(this, ultraGroupBox1.TabIndex);
            _statusForm = statusForm;
            cbbStatus.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            linkFile = null;
            temp = _ITT153LostInvoiceService.Getbykey(temp.ID);
            _select = temp;

            #region Thêm button vào dataeditor
            var btnRefDateTime = new EditorButton("btnRefDateTime");
            var appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.clock,
                ImageHAlign = Infragistics.Win.HAlign.Center,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            btnRefDateTime.Appearance = appearance;
            btnRefDateTime.Key = "btnRefDateTime";
            btnRefDateTime.Click += (s, e) => btnRefDateTime_Click(s, e);
            txtDate.ButtonsRight.Add(btnRefDateTime);
            #endregion
            #region Create
            if (ConstFrm.optStatusForm.Add == statusForm)
            {
                txtGetDetectionObject.Value = _ISystemOptionService.Getbykey(8).Data;
                txtRespresentationInLaw.Value = _ISystemOptionService.Getbykey(21).Data;
                refTime = DateTime.Now;
                btnDown.Enabled = false;
                foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll())
                {
                    TT153RegisterInvoiceDetail tT153RegisterInvoiceDetail = _ITT153RegisterInvoiceDetailService.Getbykey(item.TT153RegisterInvoiceDetailID);
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(tT153RegisterInvoiceDetail.TT153ReportID);
                    if(lstTT153LostPublishInvoiceDetail.Count(n=>n.TT153ReportID==item.TT153ReportID)==0)
                    lstTT153LostPublishInvoiceDetail.Add(new TT153LostPublishInvoiceDetail
                    {
                        Status = false,
                        TT153ReportID = item.TT153ReportID,
                        TT153PublishInvoiceDetailID = item.ID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceTypeString = tT153Report.InvoiceTypeString,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceForm = item.InvoiceForm,
                        InvoiceFormString = tT153Report.InvoiceFormString,
                        InvoiceTemplate = tT153Report.InvoiceTemplate
                    });
                    btnSave.Enabled = true;
                    btnPrint.Enabled = false;
                }
            }
            else
            {
                refTime = _select.Date;
                foreach (TT153PublishInvoiceDetail item in _ITT153PublishInvoiceDetailService.GetAll())
                {
                    TT153RegisterInvoiceDetail tT153RegisterInvoiceDetail = _ITT153RegisterInvoiceDetailService.Getbykey(item.TT153RegisterInvoiceDetailID);
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(tT153RegisterInvoiceDetail.TT153ReportID);
                    if(lstTT153LostPublishInvoiceDetail.Count(n=>n.TT153ReportID == item.TT153ReportID)==0)
                    lstTT153LostPublishInvoiceDetail.Add(new TT153LostPublishInvoiceDetail
                    {
                        Status = false,
                        TT153ReportID = item.TT153ReportID,
                        TT153PublishInvoiceDetailID = item.ID,
                        InvoiceType = item.InvoiceType,
                        InvoiceTypeID = item.InvoiceTypeID,
                        InvoiceTypeString = tT153Report.InvoiceTypeString,
                        InvoiceSeries = item.InvoiceSeries,
                        InvoiceForm = item.InvoiceForm,
                        InvoiceFormString = tT153Report.InvoiceFormString,
                        InvoiceTemplate = tT153Report.InvoiceTemplate
                    });
                }
                foreach (TT153LostPublishInvoiceDetail item_Detail in lstTT153LostPublishInvoiceDetail)
                {
                    foreach (TT153LostInvoiceDetail item in temp.TT153LostInvoiceDetails)
                    {
                        if (item.TT153ReportID == item_Detail.TT153ReportID)
                        {
                            item_Detail.Status = true;
                            item_Detail.FromNo = item.FromNo;
                            item_Detail.ToNo = item.ToNo;
                            item_Detail.Quantity = Convert.ToInt32(item.Quantity);
                            item_Detail.CopyPart = item.CopyPart;
                            item_Detail.Description = item.Description;
                        }
                    }
                }

                ObjandGUI(temp, false);
                btnSave.Enabled = false;
                btnPrint.Enabled = true;
            }
            #endregion
            InitializeGUI();
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Nghiệp vụ
        private void InitializeGUI()
        {
            LoadUGrid();
        }

        public void LoadUGrid()
        {
            uGrid.SetDataBinding(lstTT153LostPublishInvoiceDetail, "");
            Utils.ConfigGrid(uGrid, ConstDatabase.TT153LostPublishInvoiceDetail_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            //uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            uGrid.DisplayLayout.Bands[0].Columns["Status"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["FromNo"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["CopyPart"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["ToNo"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["Description"].CellClickAction = CellClickAction.EditAndSelectText;

            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["ToNo"], 1);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["FromNo"], 1);
        }

        public TT153LostInvoice ObjandGUI(TT153LostInvoice input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == null || input.ID == Guid.Empty)
                {
                    input.ID = Guid.NewGuid();
                }
                input.Date = Utils.GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
                input.DateLost = txtDateLost.DateTime;
                input.No = txtNo.Text;
                input.GetDetectionObject = txtGetDetectionObject.Text;
                input.Reason = txtReason.Text;
                input.RespresentationInLaw = txtRespresentationInLaw.Text;
                input.Status = int.Parse(cbbStatus.Value.ToString());
                input.AttachFileName = txtAttachFileName.Text;
                if (input.AttachFileName != null && input.AttachFileName != "")
                    if (linkFile != null && linkFile != "")
                        input.AttachFileContent = Utils.FileToByte(linkFile);
                if (input.TT153LostInvoiceDetails.Count > 0)
                {
                    foreach (TT153LostPublishInvoiceDetail item in lstTT153LostPublishInvoiceDetail)
                    {
                        int i;
                        bool check = false;
                        for (i = 0; i < input.TT153LostInvoiceDetails.Count; i++)
                        {
                            if (input.TT153LostInvoiceDetails[i].TT153ReportID == item.TT153ReportID)
                            {
                                check = true;
                                break;
                            }
                        }
                        if (check == true)
                        {
                            if (item.Status == true)
                            {
                                input.TT153LostInvoiceDetails[i].InvoiceType = item.InvoiceType;
                                input.TT153LostInvoiceDetails[i].InvoiceSeries = item.InvoiceSeries;
                                input.TT153LostInvoiceDetails[i].InvoiceTypeID = item.InvoiceTypeID;
                                input.TT153LostInvoiceDetails[i].FromNo = item.FromNo;
                                input.TT153LostInvoiceDetails[i].ToNo = item.ToNo;
                                input.TT153LostInvoiceDetails[i].Quantity = decimal.Parse(item.Quantity.ToString());
                                input.TT153LostInvoiceDetails[i].CopyPart = item.CopyPart;
                                input.TT153LostInvoiceDetails[i].Description = item.Description;
                            }
                            else
                            {
                                input.TT153LostInvoiceDetails.RemoveAt(i);
                            }
                        }
                        else
                        {
                            TT153LostInvoiceDetail temp = new TT153LostInvoiceDetail();
                            if (item.Status == true)
                            {
                                temp.TT153LostInvoiceID = input.ID;
                                temp.TT153PublishInvoiceDetailID = item.TT153PublishInvoiceDetailID;
                                temp.TT153ReportID = item.TT153ReportID;
                                temp.InvoiceType = item.InvoiceType;
                                temp.InvoiceSeries = item.InvoiceSeries;
                                temp.InvoiceTypeID = item.InvoiceTypeID;
                                temp.CopyPart = item.CopyPart;
                                temp.FromNo = item.FromNo.PadLeft(7,'0');
                                temp.ToNo = item.ToNo.PadLeft(7, '0');
                                temp.Quantity = decimal.Parse(item.Quantity.ToString());
                                temp.Description = item.Description;
                                input.TT153LostInvoiceDetails.Add(temp);
                            }
                        }
                    }
                }
                else
                {
                    foreach (TT153LostPublishInvoiceDetail item in lstTT153LostPublishInvoiceDetail)
                    {
                        TT153LostInvoiceDetail temp = new TT153LostInvoiceDetail();
                        if (item.Status == true)
                        {
                            temp.TT153PublishInvoiceDetailID = item.TT153PublishInvoiceDetailID;
                            temp.TT153LostInvoiceID = input.ID;
                            temp.TT153ReportID = item.TT153ReportID;
                            temp.InvoiceType = item.InvoiceType;
                            temp.InvoiceSeries = item.InvoiceSeries;
                            temp.InvoiceTypeID = item.InvoiceTypeID;
                            temp.FromNo = item.FromNo;
                            temp.ToNo = item.ToNo;
                            temp.CopyPart = item.CopyPart;
                            temp.Quantity = decimal.Parse(item.Quantity.ToString());
                            temp.Description = item.Description;
                            input.TT153LostInvoiceDetails.Add(temp);
                        }
                    }
                }

            }
            else
            {
                txtDate.Value = input.Date;
                txtDateLost.Value = input.DateLost;
                txtNo.Value = input.No;
                txtReason.Value = input.Reason;
                txtRespresentationInLaw.Value = input.RespresentationInLaw;
                txtGetDetectionObject.Value = input.GetDetectionObject;
                cbbStatus.Value = input.Status;
                txtAttachFileName.Value = input.AttachFileName;

            }
            return input;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public virtual void Reset()
        {

        }
        public bool CheckError()
        {
            bool kq = true;
            foreach(TT153LostPublishInvoiceDetail item in lstTT153LostPublishInvoiceDetail)
            {
                if (item.Status)
                {
                    if (item.Quantity <= 0)
                    {
                        MSG.Warning("Số lượng phải lớn hơn 0");
                        return false;
                    }
                    if (string.IsNullOrEmpty(item.FromNo))
                    {
                        MSG.Warning("Từ số không được để trống");
                        return false;
                    }
                    if (string.IsNullOrEmpty(item.ToNo))
                    {
                        MSG.Warning("Đến số không được để trống");
                        return false;
                    }
                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_ITT153LostInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                {
                    MSG.Error("Số thông báo đã tồn tại");
                    return false;
                }
            }
            else
            {
                if (txtNo.Text != _select.No)
                    if (_ITT153LostInvoiceService.GetAll().Count(n => n.No == txtNo.Text) > 0)
                    {
                        MSG.Error("Số thông báo đã tồn tại");
                        return false;
                    }

            }
            if (lstTT153LostPublishInvoiceDetail.Count(n => n.Status) == 0)
            {
                MSG.Error("Chưa chọn mẫu hóa đơn nào để thông báo mất cháy hỏng");
                return false;
            }
            #region
            foreach (TT153LostPublishInvoiceDetail item in lstTT153LostPublishInvoiceDetail)
            {
                if (item.Status)
                {
                    if (int.Parse(item.ToNo) > _ITT153PublishInvoiceDetailService.GetWithMaxToNo(item.TT153ReportID))
                    {
                        MSG.Warning("Số hóa đơn nhập vào chưa được thông báo phát hành");
                        return false;
                    }
                    TT153Report tT153Report = _ITT153ReportService.Getbykey(item.TT153ReportID);
                    List<SAInvoice> lst = Utils.ListSAInvoice.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();
                    List<SABill> lst1 = Utils.ListSABill.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();
                    List<SAReturn> lst2 = Utils.ListSAReturn.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();
                    List<PPDiscountReturn> lst3 = Utils.ListPPDiscountReturn.Where(n => n.InvoiceTypeID == tT153Report.InvoiceTypeID && n.InvoiceForm == tT153Report.InvoiceForm && n.InvoiceTemplate == tT153Report.InvoiceTemplate && n.InvoiceSeries == tT153Report.InvoiceSeries && n.InvoiceNo != "" & n.InvoiceNo != null).ToList();

                    if (lst.Count > 0 || lst1.Count > 0 || lst2.Count > 0 || lst3.Count > 0)
                    {
                        int ToNoMax = lst.Select(n => int.Parse(n.InvoiceNo)).Max();
                        if (int.Parse(item.ToNo) > ToNoMax)
                        {
                            MSG.Warning("Số hóa đơn nhập chưa được sử dụng");
                            return false;
                        }
                    }
                    var lstcheck = _ITT153DeletedInvoiceService.GetAllSAInvoice(item.TT153ReportID);
                    foreach (SAInvoice itemDelete in lstcheck)
                    {
                        if (int.Parse(itemDelete.InvoiceNo) <= int.Parse(item.ToNo) && int.Parse(itemDelete.InvoiceNo) >= int.Parse(item.FromNo))
                        {
                            MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa");
                            return false;
                        }
                    }
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                        {
                            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                            {
                                MSG.Warning("Số hóa đơn nhập vào đã được thông báo mất, cháy, hỏng");
                                return false;
                            }
                        }
                    }
                    else
                    {
                        TT153LostInvoiceDetail tT153LostInvoiceDetail = _select.TT153LostInvoiceDetails.FirstOrDefault(n => n.TT153ReportID == item.TT153ReportID);
                        if (int.Parse(item.ToNo) < int.Parse(tT153LostInvoiceDetail.FromNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo mất, cháy, hỏng");
                                    return false;
                                }
                            }
                        }
                        else if (int.Parse(item.FromNo) < int.Parse(tT153LostInvoiceDetail.FromNo) && int.Parse(item.ToNo) >= int.Parse(tT153LostInvoiceDetail.FromNo) && int.Parse(item.ToNo) <= int.Parse(tT153LostInvoiceDetail.ToNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(tT153LostInvoiceDetail.FromNo) - 1 && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo mất, cháy, hỏng");
                                    return false;
                                }
                            }
                        }
                        else if (int.Parse(item.FromNo) >= int.Parse(tT153LostInvoiceDetail.FromNo) && int.Parse(item.FromNo) <= int.Parse(tT153LostInvoiceDetail.ToNo) && int.Parse(item.ToNo) > int.Parse(tT153LostInvoiceDetail.ToNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(tT153LostInvoiceDetail.ToNo) + 1 <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo mất, cháy, hỏng");
                                    return false;
                                }
                            }
                        }
                        else if (int.Parse(item.FromNo) > int.Parse(tT153LostInvoiceDetail.ToNo))
                        {
                            foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                            {
                                if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                                {
                                    MSG.Warning("Số hóa đơn nhập vào đã được thông báo mất, cháy, hỏng");
                                    return false;
                                }
                            }
                        }
                    }
                    #region Code Cũ
                    //foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //{
                    //    if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //    {
                    //        MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc hủy");
                    //        return false;
                    //    }
                    //}
                    //if (_statusForm == ConstFrm.optStatusForm.Add)
                    //{
                    //    foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //    {
                    //        if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //        {
                    //            MSG.Warning("Số hóa đơn nhập vào đã được thông báo mất, cháy, hỏng");
                    //            return false;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    TT153LostInvoiceDetail tT153LostInvoiceDetail = _select.TT153LostInvoiceDetails.FirstOrDefault(n => n.TT153ReportID == item.TT153ReportID);
                    //    if (int.Parse(item.ToNo) < int.Parse(tT153LostInvoiceDetail.FromNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }
                    //    else if (int.Parse(item.FromNo) < int.Parse(tT153LostInvoiceDetail.FromNo) && int.Parse(item.ToNo) >= int.Parse(tT153LostInvoiceDetail.FromNo) && int.Parse(item.ToNo) <= int.Parse(tT153LostInvoiceDetail.ToNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(tT153LostInvoiceDetail.FromNo) - 1 && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(tT153LostInvoiceDetail.FromNo) - 1 && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }
                    //    else if (int.Parse(item.FromNo) >= int.Parse(tT153LostInvoiceDetail.FromNo) && int.Parse(item.FromNo) <= int.Parse(tT153LostInvoiceDetail.ToNo) && int.Parse(item.ToNo) > int.Parse(tT153LostInvoiceDetail.ToNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(tT153LostInvoiceDetail.ToNo) + 1 <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(tT153LostInvoiceDetail.ToNo) + 1 <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }
                    //    else if (int.Parse(item.FromNo) > int.Parse(tT153LostInvoiceDetail.ToNo))
                    //    {
                    //        foreach (TT153LostInvoiceDetail itemLost in _ITT153LostInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc báo mất, cháy, hỏng");
                    //                return false;
                    //            }
                    //        }
                    //        foreach (TT153DestructionInvoiceDetail itemLost in _ITT153DestructionInvoiceDetailService.GetAllByTT153ReportID(item.TT153ReportID))
                    //        {
                    //            if (int.Parse(itemLost.FromNo) <= int.Parse(item.ToNo) && int.Parse(item.FromNo) <= int.Parse(itemLost.ToNo))
                    //            {
                    //                MSG.Warning("Số hóa đơn nhập vào đã được thông báo hủy");
                    //                return false;
                    //            }
                    //        }
                    //    }

                    //}
                    //foreach (SAInvoice itemDelete in _ITT153DeletedInvoiceService.GetAllSAInvoice(item.TT153ReportID))
                    //{
                    //    if (int.Parse(itemDelete.InvoiceNo) <= int.Parse(item.ToNo) && int.Parse(itemDelete.InvoiceNo) >= int.Parse(item.FromNo))
                    //    {
                    //        MSG.Warning("Số hóa đơn nhập vào đã được thông báo xóa hoặc hủy");
                    //        return false;
                    //    }
                    //}
                    #endregion
                }
            }
            #endregion
            return kq;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            TT153LostInvoice temp;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                temp = new TT153LostInvoice();
            }
            else
            {
                temp = _ITT153LostInvoiceService.Getbykey(_select.ID);
            }
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion
            #region Fill giữ liệu
            temp = ObjandGUI(temp, true);
            #endregion
            #region CSDL
            _ITT153LostInvoiceService.BeginTran();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _ITT153LostInvoiceService.CreateNew(temp);
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                _ITT153LostInvoiceService.Update(temp);
            }
            try
            {
                _ITT153LostInvoiceService.CommitTran();
            }
            catch (Exception ex)
            {
                _ITT153PublishInvoiceService.RolbackTran();
                MSG.Warning("Có lỗi xảy ra khi lưu.");
                return;

            }
            #endregion
            #region xử lý form, kết thúc form
            Utils.ClearCacheByType<TT153LostInvoice>();
            List<TT153LostInvoice> lst = _ITT153LostInvoiceService.GetAll();
            _ITT153LostInvoiceService.UnbindSession(lst);
            Utils.ClearCacheByType<TT153LostInvoiceDetail>();
            IsClose = true;
            _select = temp;
            _statusForm = ConstFrm.optStatusForm.View;
            btnSave.Enabled = false;
            btnPrint.Enabled = true;
            //isClose = false;
            #endregion
        }

        public bool CheckIsNumber(String Text)
        {
            bool kq = true;
            //if (Text == null || Text.Length == 0) return false;
            foreach (char c in Text)
            {
                if (!char.IsDigit(c) && c != '.')
                {
                    return false;
                }
            }
            return kq;
        }
        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("ToNo") || e.Cell.Column.Key.Equals("FromNo"))
            {
                if (e.Cell.Row.Cells["FromNo"].Value != null && e.Cell.Row.Cells["ToNo"].Value != null && e.Cell.Row.Cells["FromNo"].Value != "" && e.Cell.Row.Cells["ToNo"].Value != "")
                {
                    if (int.Parse(e.Cell.Row.Cells["ToNo"].Value.ToString().Trim('_')) != 0 && int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString().Trim('_')) != 0)
                    {
                        int result = int.Parse(e.Cell.Row.Cells["ToNo"].Value.ToString().Trim('_')) - int.Parse(e.Cell.Row.Cells["FromNo"].Value.ToString().Trim('_')) + 1;
                        e.Cell.Row.Cells["Quantity"].Value = result;

                    }
                    else
                    {
                        e.Cell.Row.Cells["Quantity"].Value = 0;
                    }
                }
            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog();
            
            var result = openFile.ShowDialog(this);

            // giới hạn dung lượng file cho phép là 5 MB
            // giới hạn dung lượng file cho phép là 10 MB
            linkFile = openFile.FileName;
            if (linkFile != null && linkFile != "")
            {
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                if (fileSize > 10240)
                {
                    MSG.Error("Dung lượng file vượt quá mức cho phép (10MB)");
                    return;
                }
                String[] Name = openFile.FileName.Split('\\');
                txtAttachFileName.Value = Name.Last().ToString();
            }
        }
        
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (_select.AttachFileContent == null || _select.AttachFileName == null || _select.AttachFileName == "")
            {
                MSG.Warning("không có file để tải về");
                return;
            }
            var openfolder = new FolderBrowserDialog();
            openfolder.Description = "Chọn thư mục lưu file";
            openfolder.ShowNewFolderButton = false;
            openfolder.ShowDialog(this);
            string linkFileSave = openfolder.SelectedPath;
            if (linkFileSave != null && linkFileSave != "")
            {
                Utils.databaseToFile(_select.AttachFileContent, linkFileSave + "\\" + _select.AttachFileName);
                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnDown_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Tải xuống file đính kèm", btnDown);
            
        }

        private void ultraButton1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Chọn file đính kèm", ultraButton1);
        }
        private void btnRefDateTime_Click(object sender, EditorButtonEventArgs e)
        {
            try
            {
                var f = new MsgRefDateTime(refTime);
                f.Text = "Thiết lập thời gian";
                f.FormClosed += new FormClosedEventHandler(MsgRefDateTime_FormClosed);
                f.ShowDialog(this);               
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void MsgRefDateTime_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing) return;
            var frm = (MsgRefDateTime)sender;
            if (frm.DialogResult != DialogResult.OK) return;
            refTime = frm.RefDateTime;
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
            //var refDateTime = GetRefDateTime(refTime, Utils.StringToDateTime(txtDate.Text) ?? DateTime.Now);
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            Utils.Print("TT153LostInvoice", _ITT153LostInvoiceService.Getbykey(_select.ID), this);
        }

        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            UltraGridCell cell = uGrid.ActiveCell;
            if (cell.Column.Key.Equals("FromNo") || cell.Column.Key.Equals("ToNo"))
            {
                if (cell.Value != null && cell.Value != "")
                {
                    string t = cell.Value.ToString();
                    cell.Value = t.PadLeft(7, '0');
                }
                else
                {
                    cell.Row.Cells["Quantity"].Value = 0;
                }
            }
        }

        #endregion

        private void Value_Change(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            btnSave.Enabled = true;
            btnPrint.Enabled = false;
        }
    }
}