﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGCurrencyAssessment : CustormForm
    {
        #region Khởi tạo
        private readonly IGeneralLedgerService _IGeneralLedgerService;
        public IGenCodeService IGenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
        public IGOtherVoucherService IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        private IGOtherVoucherDetailForeignCurrencyService _IGOtherVoucherDetailForeignCurrencyService { get { return IoC.Resolve<IGOtherVoucherDetailForeignCurrencyService>(); } }
        List<GOtherVoucherDetailForeignCurrency> lsttk = new List<GOtherVoucherDetailForeignCurrency>();
        List<GOtherVoucherDetailDebtPayment> lstct = new List<GOtherVoucherDetailDebtPayment>();
        #endregion

        #region Fmain
        public FGCurrencyAssessment()
        {
            WaitingFrm.StartWaiting();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            InitializeComponent();
            LoadDuLieu();
            this.cbbAccountLo.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbAccountLo.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            this.cbbAccountLai.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbAccountLai.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            this.cbbCurentcy.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbCurentcy.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            txtExchangeRate.FormatNumberic(ConstDatabase.Format_Rate);
            //Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                
                this.StartPosition = FormStartPosition.CenterScreen;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Hàm Chung
        void LoadDuLieu()
        {
            // Load dữ liệu combobox nhà cung cấp
            cbbAccountLo.DataSource = Utils.ListAccount;
            cbbAccountLo.DisplayMember = "AccountNumber";
            cbbAccountLo.Text = Utils.ListSystemOption.FirstOrDefault(x => x.ID == 88).Data;
            Utils.ConfigGrid(cbbAccountLo, ConstDatabase.Account_TableName);

            cbbAccountLai.DataSource = Utils.ListAccount;
            cbbAccountLai.DisplayMember = "AccountNumber";
            cbbAccountLai.Text = Utils.ListSystemOption.FirstOrDefault(x => x.ID == 87).Data;

            Utils.ConfigGrid(cbbAccountLai, ConstDatabase.Account_TableName);
            cbbCurentcy.DataSource = Utils.ListCurrency.Where(x => x.IsActive && x.ID != "VND").ToList();
            cbbCurentcy.Text = "USD";
            txtExchangeRate.Text = Utils.ListCurrency.FirstOrDefault(x => x.ID == "USD").ExchangeRate.ToString();

            Utils.ConfigGrid(cbbCurentcy, ConstDatabase.Currency_TableName);
            // Grid tài khoản
            uGridTK.DataSource = new BindingList<GOtherVoucherDetailForeignCurrency>();
            Utils.ConfigGrid(uGridTK, ConstDatabase.GOtherVoucherDetailForeignCurrency_TableName);
            UltraGridBand band = uGridTK.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                this.ConfigEachColumn4Grid(0, item, uGridTK);
                item.Width = 115;
                if (item.Key == "Check")
                {
                    item.CellClickAction = CellClickAction.Edit;
                    item.Width = 90;
                }
            }
            band.Columns["BankAccountDetailID"].LockColumn(true);
            band.Columns["AccountingObjectID"].LockColumn(true);
            band.Columns["AccountNumber"].LockColumn(true);

            UltraGridColumn ugc = band.Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(uGridTK.Rows, false);
            Utils.AddSumColumn(uGridTK, "DebitAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridTK, "DebitAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
            Utils.AddSumColumn(uGridTK, "CreditAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridTK, "CreditAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
            Utils.AddSumColumn(uGridTK, "DebitReevaluate", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridTK, "DebitAmountDiffer", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridTK, "CreditReevaluate", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridTK, "CreditAmountDiffer", false, "", ConstDatabase.Format_TienVND, false);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["DebitAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["DebitAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["CreditAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["CreditAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["DebitReevaluate"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["DebitAmountDiffer"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["CreditReevaluate"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridTK.DisplayLayout.Bands[0].Columns["CreditAmountDiffer"], ConstDatabase.Format_TienVND);
            // Grid chứng từ
            uGridCT.DataSource = new BindingList<GOtherVoucherDetailDebtPayment>();
            UltraGridBand bandct = uGridCT.DisplayLayout.Bands[0];
            Utils.ConfigGrid(uGridCT, ConstDatabase.GOtherVoucherDetailDebtPayment_TableName);
            foreach (var item in bandct.Columns)
            {
                this.ConfigEachColumn4Grid(0, item, uGridCT);
                item.Width = 120;
            }
            bandct.Columns["AccountingObjectID"].LockColumn(true);
            bandct.Columns["AccountNumber"].LockColumn(true);
            Utils.AddSumColumn(uGridCT, "RemainingAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency, false);
            Utils.AddSumColumn(uGridCT, "RemainingAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridCT, "RevaluedAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.AddSumColumn(uGridCT, "DifferAmount", false, "", ConstDatabase.Format_TienVND, false);
            Utils.FormatNumberic(uGridCT.DisplayLayout.Bands[0].Columns["RemainingAmountOriginal"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridCT.DisplayLayout.Bands[0].Columns["RemainingAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridCT.DisplayLayout.Bands[0].Columns["RevaluedAmount"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridCT.DisplayLayout.Bands[0].Columns["DifferAmount"], ConstDatabase.Format_TienVND);
        }
        #endregion 

        #region Event
        private void btnGetData_Click(object sender, EventArgs e)
        {
            var cu = (Currency)cbbCurentcy.SelectedRow.ListObject;
            var dt = (DateTime)dteDateTo.Value;
            var ex = Convert.ToDecimal(txtExchangeRate.Text);
            lsttk = _IGeneralLedgerService.GetGOtherVoucherDetailForeignCurrency(cu, dt, ex);
            uGridTK.SetDataBinding(lsttk, "");
            lstct = _IGeneralLedgerService.GetGOtherVoucherDetailDebtPayment(cu, dt, ex);
            uGridCT.SetDataBinding(lstct, "");
            uGridTK.DisplayLayout.Bands[0].Columns["Check"].SetHeaderCheckedState(uGridTK.Rows, true);

        }
        private void btnDoiTru_Click(object sender, EventArgs e)
        {
            if (cbbAccountLai.Text == null || string.IsNullOrEmpty(cbbAccountLai.Text) || cbbAccountLo.Text == null || string.IsNullOrEmpty(cbbAccountLo.Text))
            {
                MSG.Warning("Chưa chọn tài khoản lãi/lỗ lại. Vui lòng kiểm tra lại!");
                cbbAccountLai.Focus();
                return;
            }
            var list1 = ((List<GOtherVoucherDetailForeignCurrency>)uGridTK.DataSource).ToList().Where(c => c.Check).ToList();
            if (list1.Count == 0)
            {
                MSG.Warning("Chưa chọn tài khoản đánh giá lại. Vui lòng kiểm tra lại!");
                cbbAccountLai.Focus();
                return;
            }
            var lstold = _IGOtherVoucherDetailForeignCurrencyService.Query.ToList().Where(x => list1.Any(d => d.AccountNumber == x.AccountNumber && d.BankAccountDetailID == x.BankAccountDetailID
             && d.AccountingObjectID == x.AccountingObjectID)).ToList();
            foreach (var x in lstold)
            {
                if (x.DateReevaluate > ((DateTime)dteDateTo.Value).AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second))
                {
                    MSG.Warning("Ngày đánh giá phải lớn hơn hoặc bằng ngày đánh giá lại gân nhất!");
                    dteDateTo.Focus();
                    return;
                }
            }
            var list2 = ((List<GOtherVoucherDetailDebtPayment>)uGridCT.DataSource).ToList();
            string acclai = cbbAccountLai.Text;
            string acclo = cbbAccountLo.Text;
            var cu = (Currency)cbbCurentcy.SelectedRow.ListObject;
            decimal ex = 0;
            ex =Convert.ToDecimal(txtExchangeRate.Text);
            GOtherVoucher gOtherVoucher = new GOtherVoucher()
            {
                ID = Guid.NewGuid(),
                TypeID = 601,
                PostedDate = (DateTime)dteDateTo.Value,
                Date = (DateTime)dteDateTo.Value,
                No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(60)),
                Reason = string.Format("Đánh giá lại tài khoản ngoại tệ"),
                TotalAmount = 0,
                TotalAmountOriginal = 0,
                CurrencyID = "VND",
                ExchangeRate = 1,
                Recorded = false,
                Exported = true,
                TemplateID = new Guid("0EA573DA-AFED-4F65-AA45-7C3DBBC6C2EB")
            };
            foreach (var x in list1)
            {
                x.GOtherVoucherID = gOtherVoucher.ID;
                x.DateReevaluate = ((DateTime)dteDateTo.Value).AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
                foreach (var c in x.VoucherDetailForeignCurrencys) c.NewExchangeRate = ex;
                GOtherVoucherDetail gOtherVoucherDetail = new GOtherVoucherDetail()
                {
                    GOtherVoucherID = gOtherVoucher.ID,
                    Description = (x.CreditAmountDiffer < 0 || x.DebitAmountDiffer > 0) ? string.Format("Xử lý chênh lệch lãi từ đánh giá lại TK ngoại tệ")
                            : string.Format("Xử lý chênh lệch lỗ từ đánh giá lại TK ngoại tệ"),
                    DebitAccount = (x.CreditAmountDiffer < 0 || x.DebitAmountDiffer > 0) ? x.AccountNumber : acclo,
                    CreditAccount = (x.CreditAmountDiffer < 0 || x.DebitAmountDiffer > 0) ? acclai : x.AccountNumber,
                    Amount = x.CreditAmountDiffer == 0 ? Math.Abs(x.DebitAmountDiffer) : Math.Abs(x.CreditAmountDiffer),
                    AmountOriginal = x.CreditAmountDiffer == 0 ? Math.Abs(x.DebitAmountDiffer) : Math.Abs(x.CreditAmountDiffer),
                    BankAccountDetailID = x.BankAccountDetailID,
                    CreditAccountingObjectID = ((x.CreditAmountDiffer < 0 || x.DebitAmountDiffer > 0) && x.AccountNumber != "141") ? null : x.AccountingObjectID,
                    DebitAccountingObjectID = ((x.CreditAmountDiffer < 0 || x.DebitAmountDiffer > 0) && x.AccountNumber != "141") ? x.AccountingObjectID : null,
                    EmployeeID = (x.AccountNumber == "141") ? x.AccountingObjectID : null,
                    ExchangeRate = ex,
                    IsMatch = false
                };
                if (gOtherVoucherDetail.Amount != 0)
                    gOtherVoucher.GOtherVoucherDetails.Add(gOtherVoucherDetail);
            }
            gOtherVoucher.GOtherVoucherDetailForeignCurrencys = list1;
            foreach (var x in list2)
            {
                x.ID = Guid.Empty;
                x.GOtherVoucherID = gOtherVoucher.ID;
                x.NewExchangeRate = ex;
            }
            gOtherVoucher.GOtherVoucherDetailDebtPayments = list2;
            using (var frm = new FGOtherVoucherDetail(gOtherVoucher, IGOtherVoucherService.GetAll(), ConstFrm.optStatusForm.Add))
            {
                frm.ShowDialog(this);
            }
        }
        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void uGridCT_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGridCT.ActiveRow == null) return;
            var voucher = uGridCT.ActiveRow.ListObject as GOtherVoucherDetailDebtPayment;
            if (voucher == null) return;
            Utils.ViewVoucherSelected(voucher.RefID ?? Guid.Empty, voucher.TypeID);
        }
        private void uGridTK_CellChange(object sender, CellEventArgs e)
        {
            if (!e.Cell.Row.Cells[e.Cell.Column.Key].IsFilterRowCell)
            {
                switch (e.Cell.Column.Key)
                {
                    case "Check":
                        uGridTK.UpdateData();
                        var tk = ((List<GOtherVoucherDetailForeignCurrency>)uGridTK.DataSource).Where(x => x.Check).ToList();
                        uGridCT.SetDataBinding(lstct.Where(x => tk.Any(d => d.AccountNumber == x.AccountNumber && d.AccountingObjectID == x.AccountingObjectID)).ToList(), "");
                        break;
                    default:
                        break;
                }
            }
        }
        private void uGridTK_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (e.Column.Key == "Check")
            {
                UltraGridColumn col = this.uGridTK.DisplayLayout.Bands[0].Columns["Check"];
                switch (col.GetHeaderCheckedState(e.Rows))
                {

                    case CheckState.Checked:
                        uGridTK.UpdateData();
                        uGridCT.SetDataBinding(lstct.ToList(), "");
                        break;
                    case CheckState.Unchecked:
                        uGridTK.UpdateData();
                        uGridCT.SetDataBinding(new List<GOtherVoucherDetailDebtPayment>(), "");
                        break;
                    default:
                        break;

                }
            }
        }
        private void txtExchangeRate_ValueChanged(object sender, EventArgs e)
        {
            if (txtExchangeRate.Text != null && txtExchangeRate.Text != "")
            {
                decimal newEx = Convert.ToDecimal(txtExchangeRate.Text.ToString());
                foreach (var row in uGridTK.Rows)
                {
                    row.Cells["DebitReevaluate"].Value = (decimal)row.Cells["DebitAmountOriginal"].Value * newEx;
                    row.Cells["DebitAmountDiffer"].Value = (decimal)row.Cells["DebitReevaluate"].Value != 0 ?
                          (decimal)row.Cells["DebitReevaluate"].Value - (decimal)row.Cells["DebitAmount"].Value : 0;
                    row.Cells["CreditReevaluate"].Value = (decimal)row.Cells["CreditAmountOriginal"].Value * newEx;
                    row.Cells["CreditAmountDiffer"].Value = (decimal)row.Cells["CreditReevaluate"].Value != 0 ?
                          (decimal)row.Cells["CreditReevaluate"].Value - (decimal)row.Cells["CreditAmount"].Value : 0;
                }
                foreach (var row in uGridCT.Rows)
                {
                    row.Cells["RevaluedAmount"].Value = (decimal)row.Cells["RemainingAmountOriginal"].Value * newEx;
                    row.Cells["DifferAmount"].Value = (decimal)row.Cells["RevaluedAmount"].Value - (decimal)row.Cells["RemainingAmount"].Value;
                }
            }

        }
        private void cbbCurentcy_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            Currency a = new Currency();
            if (cbbCurentcy.Text != a.ID && cbbCurentcy.Text != "")
            {
                MSG.Warning(string.Format("Loại tiền nhập vào không có trong danh mục!"));
                cbbCurentcy.Focus();
                return;
            }
        }
        #endregion


    }
}

