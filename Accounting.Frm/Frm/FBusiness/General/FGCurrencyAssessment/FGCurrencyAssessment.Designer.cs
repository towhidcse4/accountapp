﻿namespace Accounting
{
    partial class FGCurrencyAssessment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.btnDoiTru = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridTK = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtExchangeRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCurentcy = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountLo = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountLai = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridCT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTK)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurentcy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountLai)).BeginInit();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).BeginInit();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.btnKetThuc);
            this.ultraGroupBox4.Controls.Add(this.btnDoiTru);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance3.FontData.BoldAsString = "True";
            appearance3.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance3;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(1059, 43);
            this.ultraGroupBox4.TabIndex = 44;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnKetThuc
            // 
            this.btnKetThuc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance1.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnKetThuc.Appearance = appearance1;
            this.btnKetThuc.Location = new System.Drawing.Point(952, 8);
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Size = new System.Drawing.Size(101, 27);
            this.btnKetThuc.TabIndex = 44;
            this.btnKetThuc.Text = "Hủy bỏ";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // btnDoiTru
            // 
            this.btnDoiTru.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance2.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnDoiTru.Appearance = appearance2;
            this.btnDoiTru.Location = new System.Drawing.Point(845, 8);
            this.btnDoiTru.Name = "btnDoiTru";
            this.btnDoiTru.Size = new System.Drawing.Size(101, 27);
            this.btnDoiTru.TabIndex = 43;
            this.btnDoiTru.Text = "Thực hiện";
            this.btnDoiTru.Click += new System.EventHandler(this.btnDoiTru_Click);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance4.FontData.BoldAsString = "True";
            appearance4.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance4;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1059, 23);
            this.ultraGroupBox3.TabIndex = 44;
            this.ultraGroupBox3.Text = "Chứng từ công nợ và thanh toán bằng ngoại tệ";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance5;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 107);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1059, 26);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.Text = "Số dư tài khoản gốc ngoại tệ đánh giá lại";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ultraPanel3);
            this.splitContainer1.Panel1.Controls.Add(this.ultraPanel2);
            this.splitContainer1.Panel1.Controls.Add(this.ultraPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel6);
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel5);
            this.splitContainer1.Size = new System.Drawing.Size(1059, 578);
            this.splitContainer1.SplitterDistance = 335;
            this.splitContainer1.TabIndex = 45;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.uGridTK);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 133);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(1059, 179);
            this.ultraPanel3.TabIndex = 48;
            // 
            // uGridTK
            // 
            this.uGridTK.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridTK.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridTK.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridTK.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridTK.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridTK.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridTK.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridTK.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridTK.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridTK.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridTK.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridTK.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridTK.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridTK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridTK.Location = new System.Drawing.Point(0, 0);
            this.uGridTK.Name = "uGridTK";
            this.uGridTK.Size = new System.Drawing.Size(1059, 179);
            this.uGridTK.TabIndex = 7;
            this.uGridTK.Text = "uGrid";
            this.uGridTK.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridTK_CellChange);
            this.uGridTK.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.uGridTK_AfterHeaderCheckStateChanged);
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 312);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(1059, 23);
            this.ultraPanel2.TabIndex = 47;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1059, 133);
            this.ultraPanel1.TabIndex = 46;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.txtExchangeRate);
            this.ultraGroupBox2.Controls.Add(this.btnGetData);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.dteDateTo);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.cbbCurentcy);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox2.Controls.Add(this.lblAccount);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountLo);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObject);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountLai);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance49.FontData.BoldAsString = "True";
            appearance49.FontData.SizeInPoints = 9F;
            this.ultraGroupBox2.HeaderAppearance = appearance49;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1059, 101);
            this.ultraGroupBox2.TabIndex = 42;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtExchangeRate
            // 
            appearance6.TextHAlignAsString = "Right";
            this.txtExchangeRate.Appearance = appearance6;
            this.txtExchangeRate.AutoSize = false;
            this.txtExchangeRate.Location = new System.Drawing.Point(183, 63);
            this.txtExchangeRate.MaxValue = 1E+100D;
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.PromptChar = ' ';
            this.txtExchangeRate.Size = new System.Drawing.Size(189, 22);
            this.txtExchangeRate.TabIndex = 105;
            this.txtExchangeRate.ValueChanged += new System.EventHandler(this.txtExchangeRate_ValueChanged);
            // 
            // btnGetData
            // 
            appearance7.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnGetData.Appearance = appearance7;
            this.btnGetData.Location = new System.Drawing.Point(863, 29);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(137, 23);
            this.btnGetData.TabIndex = 75;
            this.btnGetData.Text = "Lấy dữ liệu";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // ultraLabel3
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance8;
            this.ultraLabel3.Location = new System.Drawing.Point(396, 31);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(104, 18);
            this.ultraLabel3.TabIndex = 74;
            this.ultraLabel3.Text = "Đánh giá đến ngày";
            // 
            // dteDateTo
            // 
            this.dteDateTo.AutoSize = false;
            this.dteDateTo.CausesValidation = false;
            this.dteDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateTo.Location = new System.Drawing.Point(525, 28);
            this.dteDateTo.MaskInput = "dd/mm/yyyy";
            this.dteDateTo.Name = "dteDateTo";
            this.dteDateTo.Size = new System.Drawing.Size(198, 22);
            this.dteDateTo.TabIndex = 73;
            // 
            // ultraLabel2
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance9;
            this.ultraLabel2.Location = new System.Drawing.Point(65, 61);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(112, 18);
            this.ultraLabel2.TabIndex = 42;
            this.ultraLabel2.Text = "Tỷ giá đánh giá lại";
            // 
            // cbbCurentcy
            // 
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurentcy.DisplayLayout.Appearance = appearance10;
            this.cbbCurentcy.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbCurentcy.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurentcy.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurentcy.DisplayLayout.GroupByBox.Appearance = appearance11;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurentcy.DisplayLayout.GroupByBox.BandLabelAppearance = appearance12;
            this.cbbCurentcy.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance13.BackColor2 = System.Drawing.SystemColors.Control;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurentcy.DisplayLayout.GroupByBox.PromptAppearance = appearance13;
            this.cbbCurentcy.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurentcy.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurentcy.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance15.BackColor = System.Drawing.SystemColors.Highlight;
            appearance15.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurentcy.DisplayLayout.Override.ActiveRowAppearance = appearance15;
            this.cbbCurentcy.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurentcy.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurentcy.DisplayLayout.Override.CardAreaAppearance = appearance16;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurentcy.DisplayLayout.Override.CellAppearance = appearance17;
            this.cbbCurentcy.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurentcy.DisplayLayout.Override.CellPadding = 0;
            appearance18.BackColor = System.Drawing.SystemColors.Control;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurentcy.DisplayLayout.Override.GroupByRowAppearance = appearance18;
            appearance19.TextHAlignAsString = "Left";
            this.cbbCurentcy.DisplayLayout.Override.HeaderAppearance = appearance19;
            this.cbbCurentcy.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurentcy.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurentcy.DisplayLayout.Override.RowAppearance = appearance20;
            this.cbbCurentcy.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurentcy.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.cbbCurentcy.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurentcy.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurentcy.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurentcy.Location = new System.Drawing.Point(183, 27);
            this.cbbCurentcy.Name = "cbbCurentcy";
            this.cbbCurentcy.Size = new System.Drawing.Size(189, 22);
            this.cbbCurentcy.TabIndex = 41;
            this.cbbCurentcy.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbCurentcy_ItemNotInList);
            // 
            // ultraLabel1
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance22;
            this.ultraLabel1.Location = new System.Drawing.Point(75, 29);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(53, 18);
            this.ultraLabel1.TabIndex = 40;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // lblAccount
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Left";
            appearance23.TextVAlignAsString = "Middle";
            this.lblAccount.Appearance = appearance23;
            this.lblAccount.Location = new System.Drawing.Point(401, 66);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(118, 18);
            this.lblAccount.TabIndex = 3;
            this.lblAccount.Text = "TK xử lý chênh lệch lãi";
            // 
            // cbbAccountLo
            // 
            this.cbbAccountLo.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbAccountLo.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.StartsWith;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountLo.DisplayLayout.Appearance = appearance24;
            this.cbbAccountLo.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbAccountLo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountLo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountLo.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountLo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.cbbAccountLo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountLo.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.cbbAccountLo.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountLo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountLo.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountLo.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.cbbAccountLo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountLo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountLo.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountLo.DisplayLayout.Override.CellAppearance = appearance31;
            this.cbbAccountLo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountLo.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountLo.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.cbbAccountLo.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.cbbAccountLo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountLo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountLo.DisplayLayout.Override.RowAppearance = appearance34;
            this.cbbAccountLo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountLo.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.cbbAccountLo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountLo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountLo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountLo.Location = new System.Drawing.Point(863, 65);
            this.cbbAccountLo.Name = "cbbAccountLo";
            this.cbbAccountLo.Size = new System.Drawing.Size(198, 22);
            this.cbbAccountLo.TabIndex = 0;
            // 
            // lblAccountingObject
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextHAlignAsString = "Left";
            appearance36.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance36;
            this.lblAccountingObject.Location = new System.Drawing.Point(742, 64);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(115, 22);
            this.lblAccountingObject.TabIndex = 1;
            this.lblAccountingObject.Text = "TK xử lý chênh lệch lỗ";
            // 
            // cbbAccountLai
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountLai.DisplayLayout.Appearance = appearance37;
            this.cbbAccountLai.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbAccountLai.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountLai.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountLai.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountLai.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.cbbAccountLai.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountLai.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.cbbAccountLai.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountLai.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountLai.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountLai.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.cbbAccountLai.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountLai.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountLai.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountLai.DisplayLayout.Override.CellAppearance = appearance44;
            this.cbbAccountLai.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountLai.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountLai.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.cbbAccountLai.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.cbbAccountLai.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountLai.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountLai.DisplayLayout.Override.RowAppearance = appearance47;
            this.cbbAccountLai.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountLai.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.cbbAccountLai.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountLai.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountLai.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountLai.Location = new System.Drawing.Point(525, 64);
            this.cbbAccountLai.Name = "cbbAccountLai";
            this.cbbAccountLai.Size = new System.Drawing.Size(198, 22);
            this.cbbAccountLai.TabIndex = 2;
            // 
            // ultraPanel6
            // 
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.uGridCT);
            this.ultraPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel6.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(1059, 196);
            this.ultraPanel6.TabIndex = 47;
            // 
            // uGridCT
            // 
            this.uGridCT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridCT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridCT.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridCT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCT.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridCT.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridCT.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridCT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCT.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridCT.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridCT.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridCT.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridCT.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCT.Location = new System.Drawing.Point(0, 0);
            this.uGridCT.Name = "uGridCT";
            this.uGridCT.Size = new System.Drawing.Size(1059, 196);
            this.uGridCT.TabIndex = 8;
            this.uGridCT.Text = "uGrid";
            this.uGridCT.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridCT_DoubleClickRow);
            // 
            // ultraPanel5
            // 
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel5.Location = new System.Drawing.Point(0, 196);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(1059, 43);
            this.ultraPanel5.TabIndex = 46;
            // 
            // FGCurrencyAssessment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 578);
            this.Controls.Add(this.splitContainer1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FGCurrencyAssessment";
            this.Text = "Đánh giá lại tài khoản ngoại tệ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTK)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurentcy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountLai)).EndInit();
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).EndInit();
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
        private Infragistics.Win.Misc.UltraButton btnDoiTru;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel6;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurentcy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountLo;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountLai;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtExchangeRate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTK;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCT;
    }
}