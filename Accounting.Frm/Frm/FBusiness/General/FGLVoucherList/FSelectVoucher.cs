﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Type = Accounting.Core.Domain.Type;

namespace Accounting
{
    public partial class FSelectVoucher : CustormForm
    {
        private readonly ITypeService _typeSrv;


        private readonly IPPInvoiceService _ppInvoiceSrv;
        private readonly IAccountingObjectService _accountingObjectSrv;
        private readonly IInvoiceTypeService _invoiceTypeSrv;
        private readonly IGoodsServicePurchaseService _goodsServicePurchaseSrv;
        private readonly List<string> _lstImportPurchase = new List<string> { "Trong nước", "Nhập khẩu" };
        private readonly List<AccountingObject> _lstAccountingObject;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        UltraCombo cbbAccountingObjectTaxID = new UltraCombo();
        // ngày hoạch toán
        DateTime ngayHoachToan = DateTime.ParseExact(ConstFrm.DbStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        public List<GVoucherListDetail> VoucherListDetails { get { return _dsVoucherListDetails; } }
        public DateTime DateFrom { get { return _dteFrom; } }
        public DateTime DateTo { get { return _dteTo; } }
        private List<GVoucherListDetail> _dsVoucherListDetails = new List<GVoucherListDetail>();
        private DateTime _dteFrom;
        List<Core.Domain.Type> listTypes = new List<Type>();
        List<GenCode> lstGenCode = new List<GenCode>();
        private DateTime _dteTo;
        public FSelectVoucher()
        {
            _typeSrv = IoC.Resolve<ITypeService>();
            Utils.ClearCacheByType<GeneralLedger>();
            _ppInvoiceSrv = IoC.Resolve<IPPInvoiceService>();
            _accountingObjectSrv = IoC.Resolve<IAccountingObjectService>();
            _invoiceTypeSrv = IoC.Resolve<IInvoiceTypeService>();
            _goodsServicePurchaseSrv = IoC.Resolve<IGoodsServicePurchaseService>();
            _lstAccountingObject = _accountingObjectSrv.GetAccountingObjects(1, true); // danh sách khách hàng là nhà cung cấp 
            InitializeComponent();
            // load dữ liệu cho Combobox Hình thức mua hàng
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = _lstItems;
            cbbDateTime.DisplayMember = "Name";
            if (_lstItems.Count > 0)
            {
                cbbDateTime.SelectedRow = cbbDateTime.Rows[0];
            }
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            // load ngày bắt đầu và ngày kết thúc (note: mặc định ban đầu load theo ngày hoạch toán)
            dteTo.DateTime = dteFrom.DateTime = ngayHoachToan;
            // load dữ liệu cho Grid Mẫu thuế
            // load dữ liệu cho Grid Danh sách
            ViewDanhSach(new List<GVoucherListDetail>());// load dữ liệu Combobox Đối tượng. Note: đối tượng là nhà cung cấp
            listTypes = _typeSrv.GetAll();
            List<int> lstRemove = new List<int> { 85, 86, 31, 30, 18, 435, 56, 20 };
            // get list generate 
            lstGenCode = Utils.IGenCodeService.Query.Where(o => o.IsReset == true && !lstRemove.Contains(o.TypeGroupID)).ToList();
            // config combobox 
            Utils.ConfigCombo<GenCode>(this, lstGenCode, cbbTypeiD, "TypeGroupName", "TypeGroupID");

            //cbbTypeiD.DataSource = listTypes;
            //cbbTypeiD.DisplayMember = "TypeName";
            //cbbTypeiD.SelectedRow = cbbTypeiD.Rows[0];
            //Utils.ConfigGrid(cbbTypeiD, ConstDatabase.Type_TableName);
        }
        private void btnGetData_Click(object sender, EventArgs e)
        {
            // tạo dữ liệu test
            if (cbbTypeiD.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn loại chứng từ!");
            }
            else
            {
                try
                {
                    WaitingFrm.StartWaiting();
                    List<GVoucherListDetail> lstDetails =
                    IoC.Resolve<IGeneralLedgerService>()
                        .GetRecordingVouchers(((Core.Domain.GenCode)cbbTypeiD.SelectedRow.ListObject).TypeGroupID,
                            (DateTime)dteFrom.Value, (DateTime)dteTo.Value);
                    WaitingFrm.StopWaiting();
                    // end dữ liệu test
                    lstDetails = lstDetails.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ThenBy(n => n.OrderPriority).ToList();
                    if (lstDetails.Count == 0)
                    {
                        MSG.Warning("Không tìm thấy chứng từ trong khoảng thời gian đã chọn");
                        uGridDanhSach.DataSource = lstDetails;
                    }
                    else
                    {
                        uGridDanhSach.DataSource = lstDetails;
                    }
                }
                catch (Exception)
                {
                    WaitingFrm.StopWaiting();
                }

            }
        }

        #region Xử lý sự kiện trên form

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbDateTime.SelectedRow != null)
            {
                var model = cbbDateTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFrom.DateTime = dtBegin;
                dteTo.DateTime = dtEnd;
            }

        }

        private void cbbAccountingObjectID_TextChanged(object sender, EventArgs e)
        {
            //UltraCombo cbbTemp = (UltraCombo)sender;
            //Utils.AutoComplateUltraCombo(cbbTypeiD, lstGenCode.Where(x => x.TypeGroupName.ToLower().Contains(cbbTemp.Text.ToLower())).ToList());
        }

        #endregion

        #region Hàm xử lý riêng của Form
        // Danh sách hàng tiền của mua hàng chưa nhận hóa đơn
        //private List<InvoiceReceive> DsMuaHangChuaLayHoaDon(List<PPInvoice> model)
        //{
        //    List<InvoiceReceive> lst = new List<InvoiceReceive>();
        //    for (int i = 0; i < model.Count; i++)
        //    {
        //        foreach (var item in model[i].PPInvoiceDetails)
        //        {
        //            InvoiceReceive objInvoiceReceive = new InvoiceReceive();
        //            objInvoiceReceive.TrangThai = false;
        //            objInvoiceReceive.NgayHachToan = model[i].PostedDate;
        //            objInvoiceReceive.NgayChungTu = model[i].Date;
        //            objInvoiceReceive.SoChungTu = model[i].No;
        //            objInvoiceReceive.DienGiai = item.Description;
        //            objInvoiceReceive.SoTien = item.AmountOriginal;
        //            lst.Add(objInvoiceReceive);
        //        }
        //    }
        //    return lst;
        //}
        // Hiển thị vùng thông tin danh sách
        private void ViewDanhSach(List<GVoucherListDetail> model)
        {
            uGridDanhSach.DataSource = model;
            Utils.ConfigGrid(uGridDanhSach, ConstDatabase.FGVoucherListDetail_TableName);
            uGridDanhSach.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridDanhSach.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridDanhSach.Text = "";
            // xét style cho các cột có dạng checkbox
            uGridDanhSach.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGridDanhSach.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Status"].CellActivation = Activation.AllowEdit;
            band.Columns["Status"].Header.Fixed = true;
            #region Colums Style
            foreach (var item in uGridDanhSach.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Status"))
                    item.Width = 30;
                if (item.Key.Equals("VoucherTypeID"))
                    this.ConfigCbbToGrid(uGridDanhSach, item.Key, _typeSrv.GetAll(), "ID", "TypeName", ConstDatabase.Type_TableName);
            }
            #endregion
            // xóa dòng xét tổng ở cuối Grid
            uGridDanhSach.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            // thêm dòng tính tổng cột "Số tiền" vào cuối Grid
            SummarySettings summary = band.Summaries.Add("SumVoucherAmount", SummaryType.Sum, band.Columns["VoucherAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            summary.Appearance.TextHAlign = HAlign.Right;

            summary = band.Summaries.Add(SummaryType.Count, band.Columns["Status"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridDanhSach.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridDanhSach.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridDanhSach.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridDanhSach.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            uGridDanhSach.DisplayLayout.Bands[0].Columns["VoucherAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDanhSach.DisplayLayout.Bands[0].Columns["Status"].Width = 65;
        }
        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            uGridDanhSach.UpdateData();
            if (uGridDanhSach.DataSource != null)
            {
                var lst = (uGridDanhSach.DataSource as List<GVoucherListDetail>).Where(t => t.Status).ToList();
                if (lst.Count == 0)
                {
                    MSG.Warning("Chưa chọn chứng từ ghi sổ");
                    return;
                }
                _dsVoucherListDetails = lst;
                _dteFrom = dteFrom.DateTime;
                _dteTo = dteTo.DateTime;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbbTypeiD_ItemNotInList(object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            //MSG.Warning("Loại chứng từ không tồn tại "+ cbbTypeiD.Text + " xin vui lòng kiểm tra lại");
            //cbbTypeiD.Focus();
        }
    }


}
