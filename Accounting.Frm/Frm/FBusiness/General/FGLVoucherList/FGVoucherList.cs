﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Type = Accounting.Core.Domain.Type;

#endregion

namespace Accounting
{
    public partial class FGVoucherList : CustormForm
    {
        private const int TypeId = 680;
        private readonly IAccountDefaultService _accountDefaultSrv;
        private readonly List<Type> _dsType = new List<Type>(); //Bảng BudgetItem
        private readonly IGVoucherListService _gVoucherListSrv;
        private readonly List<Account> _lstAccount = new List<Account>();
        private List<GVoucherList> _lstGvoucherList = new List<GVoucherList>();

        public FGVoucherList()
        {
            _gVoucherListSrv = IoC.Resolve<IGVoucherListService>();
            _accountDefaultSrv = IoC.Resolve<IAccountDefaultService>();
            ITypeService typeSrv = IoC.Resolve<ITypeService>();
            _dsType = typeSrv.GetAll();

            _lstGvoucherList = _gVoucherListSrv.GetAll();
            InitializeComponent();
            // hiện thị danh sách thu tiền khách hàng
            ViewTabIndex(_lstGvoucherList);
            // mặc định hiển thị chi tiết thông tin khách hàng đầu tiên

            GVoucherList model = _lstGvoucherList.Count > 0 ? _lstGvoucherList.OrderByDescending(n => n.Date).ThenByDescending(n => n.No).ToList()[0] : new GVoucherList();
            // tab chi tiết
            uGridCT.DataSource = model.GVoucherListDetails.Count > 0
                ? model.GVoucherListDetails.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ToList()
                : new List<GVoucherListDetail>();
            ViewTabDetails(uGridCT);
            if (uGridIndex.Rows.Count == 0)
            {
                btnDelete.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = true;
            }
        }

        // thực hiện khi chọn một khách hàng trong danh sách khách hàng có phát sinh
        private void uGridIndex_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            // lấy đối tượng được chọn
            if (uGridIndex.ActiveRow != null && uGridIndex.Selected.Rows.Count > 0 && uGridIndex.ActiveRow.Index >= 0)
            {
                GVoucherList model = uGridIndex.ActiveRow.ListObject as GVoucherList;
                uGridCT.DataSource = model.GVoucherListDetails.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ToList();
            }
        }

        public void ViewTabIndex(List<GVoucherList> lstGvoucherList)
        {
            lstGvoucherList = lstGvoucherList.OrderByDescending(n => n.Date).ThenByDescending(n => n.No).ToList();
            _lstGvoucherList = lstGvoucherList;
            uGridIndex.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.RowSelect;
            uGridIndex.DataSource = lstGvoucherList.ToList();
            Utils.ConfigGrid(uGridIndex, ConstDatabase.FGVoucherList_TableName);
            //Tổng số hàng
            uGridIndex.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
            //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridIndex.DisplayLayout.Bands[0];
            if (band.Summaries.Count != 0)
                band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Date"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridIndex.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridIndex.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridIndex.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridIndex.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            // Thêm tổng số ở cột "Số còn phải thu"
            summary = band.Summaries.Add("TotalAmount", SummaryType.Sum, band.Columns["TotalAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            summary.Appearance.TextHAlign = HAlign.Right;
            if (uGridIndex.Rows.Count > 0)
            {
                uGridIndex.Refresh();
                uGridIndex.Rows[0].Selected = true;
                uGridIndex.Rows[0].Activated = true;
            }
            uGridIndex.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridIndex.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;
        }

        private void ViewTabDetails(UltraGrid uGridInput)
        {
            Utils.ConfigGrid(uGridInput, ConstDatabase.FGVoucherListDetail_TableName);
            uGridInput.Text = "";

            //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridInput.DisplayLayout.Bands[0];
            if (band.Summaries.Count != 0)
                band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["VoucherTypeID"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridInput.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridInput.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridInput.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridInput.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            // Thêm tổng số ở cột "Số còn phải thu"
            summary = band.Summaries.Add("SumVoucherAmount", SummaryType.Sum, band.Columns["VoucherAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            summary.Appearance.TextHAlign = HAlign.Right;

            #region Colums Style

            foreach (var item in uGridInput.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Status")) //tài khoản nợ, tài khoản có
                    item.Hidden = true;
                if (item.Key.Equals("DebitAccount") || item.Key.Equals("CreditAccount")) //tài khoản nợ, tài khoản có
                {
                    List<Account> lstTemp = _accountDefaultSrv.GetAccountDefaultByTypeId(TypeId, item.Key).Count > 0
                        ? _accountDefaultSrv.GetAccountDefaultByTypeId(TypeId, item.Key)
                        : _lstAccount.OrderBy(k => k.AccountName).ToList();
                    this.ConfigCbbToGrid(uGridInput, item.Key, lstTemp, "ID", "AccountNumber",
                        ConstDatabase.Account_TableName);
                }
                //TK Ngân hàng
                if (item.Key.Equals("VoucherTypeID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsType, "ID", "TypeName", ConstDatabase.Type_TableName);
            }
            uGridInput.DisplayLayout.Bands[0].Columns["VoucherAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            #endregion
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FGVoucherListDetail fGVoucherListDetail = new FGVoucherListDetail(new GVoucherList(), _lstGvoucherList, ConstFrm.optStatusForm.Add);
            fGVoucherListDetail.Show(this);
            fGVoucherListDetail.FormClosing += (s, e_) => DetailBase_FormClosing(s, e_);
            //tsmReLoad.PerformClick();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteFuntion();
            if(uGridIndex.Rows.Count == 0)
            {
                btnDelete.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = true;
            }
        }
        private void deleteFuntion()
        {
            if (uGridIndex.ActiveRow.Index < 0)
            {
                MSG.Warning("Chưa chọn chứng từ để xóa");
                return;
            }
            if (MSG.MessageBoxStand("Bạn có muốn xóa chứng từ này không?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            // lấy đối tượng được chọn
            if (uGridIndex.ActiveRow != null)
            {
                foreach (var item in uGridIndex.Selected.Rows)
                {
                    GVoucherList model = item.ListObject as GVoucherList;
                    //if (model.Recorded)
                    //{
                    //    MessageBox.Show("Bạn không thể xóa chứng từ đã được ghi sổ");
                    //    return;
                    //}
                    _gVoucherListSrv.Delete(model);

                }
                _gVoucherListSrv.CommitChanges();
                uGridIndex.DataSource = _lstGvoucherList = _gVoucherListSrv.GetAll();
                if (_lstGvoucherList.Count > 0)
                {
                    ViewTabIndex(_lstGvoucherList);
                    uGridCT.DataSource = _lstGvoucherList[0].GVoucherListDetails.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ToList();
                    ViewTabDetails(uGridCT);
                }
                else
                {
                    ViewTabIndex(new List<GVoucherList>());
                    uGridCT.DataSource = new List<GVoucherListDetail>();
                    ViewTabDetails(uGridCT);
                }
            }
            //MessageBox.Show("Danh sách trống !");
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            ShowEdit();

        }
        private void ShowEdit()
        {
            if (uGridIndex.ActiveRow != null)
            {
                if (uGridIndex.ActiveRow.Index >= 0)
                {
                    GVoucherList model = uGridIndex.ActiveRow.ListObject as GVoucherList;
                    FGVoucherListDetail fGVoucherListDetail = new FGVoucherListDetail(model, _lstGvoucherList, ConstFrm.optStatusForm.View);
                    fGVoucherListDetail.FormClosing += (s, e) => DetailBase_FormClosing(s, e);
                    fGVoucherListDetail.Show(this);
                    //fGVoucherListDetail.FormClosed += new FormClosingEventHandler(fGVoucherListDetail.Clo);

                }
            }
            else
            {
                MSG.Warning("Bạn chưa chọn chứng từ cần sửa");
            }
        }
        private void DetailBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            var frm = (Form)sender;
            if (frm is FGVoucherListDetail)
            {
                WaitingFrm.StartWaiting();

                _lstGvoucherList = _gVoucherListSrv.GetAll();
                ViewTabIndex(_lstGvoucherList);
                //if (_lstGvoucherList.Count > 0)
                //{
                //    uGridIndex.Rows[0].Selected = true;
                //    uGridIndex.Rows[0].Activated = true;
                //}
                if (uGridIndex.ActiveRow != null && uGridIndex.Selected.Rows.Count > 0)
                {
                    GVoucherList model = uGridIndex.ActiveRow.ListObject as GVoucherList;
                    uGridCT.DataSource = model.GVoucherListDetails.OrderByDescending(n => n.VoucherDate).ThenBy(n => n.VoucherNo).ToList();
                }
                WaitingFrm.StopWaiting();
            }
            if (uGridIndex.Rows.Count == 0)
            {
                btnDelete.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = true;
            }
        }
        private void tsmReLoad_Click(object sender, EventArgs e)
        {
            //_lstGvoucherList = _gVoucherListSrv.GetAll();
            //uGridIndex.DataSource = _lstGvoucherList;
            //if (_lstGvoucherList.Count > 0)
            //{
            //    uGridIndex.Rows[0].Selected = true;
            //    uGridIndex.Rows[0].Activated = true;
            //}
            //uGridCT.DataSource = _lstGvoucherList[0].GVoucherListDetails;
        }

        private void uGridIndex_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            ShowEdit();
        }

        private void FGVoucherList_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            _gVoucherListSrv.UnbindSession(_gVoucherListSrv.GetAll());
            List<GVoucherList> gVoucherLists = _gVoucherListSrv.GetAll().OrderByDescending(n => n.Date).ThenByDescending(n => n.No).ToList();
            uGridIndex.DataSource = gVoucherLists;
            if (gVoucherLists.Count > 0)
            {
                uGridIndex.Refresh();
                uGridIndex.Rows[0].Selected = true;
                uGridIndex.Rows[0].Activate();
            }
            WaitingFrm.StopWaiting();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGridIndex, "Chứng từ ghi sổ");
        }

        private void FGVoucherList_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridIndex.ResetText();
            uGridIndex.ResetUpdateMode();
            uGridIndex.ResetExitEditModeOnLeave();
            uGridIndex.ResetRowUpdateCancelAction();
            uGridIndex.DataSource = null;
            uGridIndex.Layouts.Clear();
            uGridIndex.ResetLayouts();
            uGridIndex.ResetDisplayLayout();
            uGridIndex.Refresh();
            uGridIndex.ClearUndoHistory();
            uGridIndex.ClearXsdConstraints();
            
            uGridCT.ResetText();
            uGridCT.ResetUpdateMode();
            uGridCT.ResetExitEditModeOnLeave();
            uGridCT.ResetRowUpdateCancelAction();
            uGridCT.DataSource = null;
            uGridCT.Layouts.Clear();
            uGridCT.ResetLayouts();
            uGridCT.ResetDisplayLayout();
            uGridCT.Refresh();
            uGridCT.ClearUndoHistory();
            uGridCT.ClearXsdConstraints();
        }
    }
}