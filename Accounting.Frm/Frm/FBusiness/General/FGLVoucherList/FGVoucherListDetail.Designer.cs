﻿namespace Accounting
{
    partial class FGVoucherListDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGVoucherListDetail));
            this.grHome = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.Body = new System.Windows.Forms.Panel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridDetails = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnDeleteLine = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.btnView = new Infragistics.Win.Misc.UltraButton();
            this.bntRemove = new Infragistics.Win.Misc.UltraButton();
            this.Header = new System.Windows.Forms.Panel();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grHome)).BeginInit();
            this.grHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            this.Body.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetails)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // grHome
            // 
            this.grHome.Controls.Add(this.ultraLabel1);
            this.grHome.Controls.Add(this.lblPostedDate);
            this.grHome.Controls.Add(this.txtReason);
            this.grHome.Controls.Add(this.lblReason);
            this.grHome.Controls.Add(this.dteDate);
            this.grHome.Controls.Add(this.txtNo);
            this.grHome.Dock = System.Windows.Forms.DockStyle.Top;
            appearance6.FontData.BoldAsString = "True";
            appearance6.FontData.SizeInPoints = 13F;
            this.grHome.HeaderAppearance = appearance6;
            this.grHome.Location = new System.Drawing.Point(0, 0);
            this.grHome.Name = "grHome";
            this.grHome.Size = new System.Drawing.Size(935, 109);
            this.grHome.TabIndex = 31;
            this.grHome.Text = "Chứng từ ghi sổ";
            this.grHome.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.ForeColor = System.Drawing.Color.White;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.ultraLabel1.Location = new System.Drawing.Point(1, 22);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(99, 24);
            this.ultraLabel1.TabIndex = 30;
            this.ultraLabel1.Text = "Số chứng từ";
            // 
            // lblPostedDate
            // 
            appearance2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance2.ForeColor = System.Drawing.Color.White;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance2;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.lblPostedDate.Location = new System.Drawing.Point(98, 22);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(98, 24);
            this.lblPostedDate.TabIndex = 29;
            this.lblPostedDate.Text = "Ngày chứng từ";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.Location = new System.Drawing.Point(66, 76);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(857, 21);
            this.txtReason.TabIndex = 25;
            // 
            // lblReason
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Appearance = appearance3;
            this.lblReason.AutoSize = true;
            this.lblReason.Location = new System.Drawing.Point(6, 80);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(54, 14);
            this.lblReason.TabIndex = 24;
            this.lblReason.Text = "Diễn giải :";
            // 
            // dteDate
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance4;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(98, 46);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(98, 21);
            this.dteDate.TabIndex = 27;
            this.dteDate.Value = null;
            // 
            // txtNo
            // 
            appearance5.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance5;
            this.txtNo.Location = new System.Drawing.Point(1, 46);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(99, 21);
            this.txtNo.TabIndex = 5;
            // 
            // Body
            // 
            this.Body.Controls.Add(this.ultraGroupBox1);
            this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Body.Location = new System.Drawing.Point(0, 110);
            this.Body.Name = "Body";
            this.Body.Size = new System.Drawing.Size(935, 631);
            this.Body.TabIndex = 28;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraPanel2);
            this.ultraGroupBox1.Controls.Add(this.ultraPanel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance9;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(935, 631);
            this.ultraGroupBox1.TabIndex = 32;
            this.ultraGroupBox1.Text = "Danh sách chứng từ gốc";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel2
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraPanel2.Appearance = appearance7;
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.uGridDetails);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(3, 47);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(929, 581);
            this.ultraPanel2.TabIndex = 49;
            // 
            // uGridDetails
            // 
            this.uGridDetails.ContextMenuStrip = this.contextMenuStrip1;
            this.uGridDetails.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDetails.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDetails.Location = new System.Drawing.Point(0, 0);
            this.uGridDetails.Name = "uGridDetails";
            this.uGridDetails.Size = new System.Drawing.Size(929, 581);
            this.uGridDetails.TabIndex = 1;
            this.uGridDetails.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridDetails.BeforeCellCancelUpdate += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridDetails_BeforeCellCancelUpdate);
            this.uGridDetails.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.uGridDetails_BeforeRowsDeleted);
            this.uGridDetails.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridDetails_Error);
            this.uGridDetails.CellDataError += new Infragistics.Win.UltraWinGrid.CellDataErrorEventHandler(this.uGridDetails_CellDataError);
            this.uGridDetails.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridDetails_DoubleClickRow);
            this.uGridDetails.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uGridDetails_KeyPress);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDeleteLine});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(177, 26);
            // 
            // btnDeleteLine
            // 
            this.btnDeleteLine.Name = "btnDeleteLine";
            this.btnDeleteLine.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.btnDeleteLine.Size = new System.Drawing.Size(176, 22);
            this.btnDeleteLine.Text = "Xóa dòng";
            this.btnDeleteLine.Click += new System.EventHandler(this.btnDeleteLine_Click);
            // 
            // ultraPanel1
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraPanel1.Appearance = appearance8;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnAdd);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnView);
            this.ultraPanel1.ClientArea.Controls.Add(this.bntRemove);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(929, 28);
            this.ultraPanel1.TabIndex = 48;
            // 
            // btnAdd
            // 
            this.btnAdd.AutoSize = true;
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAdd.Location = new System.Drawing.Point(841, 0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 28);
            this.btnAdd.TabIndex = 46;
            this.btnAdd.Text = "Chọn chứng từ";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnView
            // 
            this.btnView.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnView.Location = new System.Drawing.Point(164, 2);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 47;
            this.btnView.Text = "Xem ...";
            this.btnView.Visible = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // bntRemove
            // 
            this.bntRemove.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.bntRemove.Location = new System.Drawing.Point(83, 2);
            this.bntRemove.Name = "bntRemove";
            this.bntRemove.Size = new System.Drawing.Size(75, 23);
            this.bntRemove.TabIndex = 45;
            this.bntRemove.Text = "Loại bỏ";
            this.bntRemove.Visible = false;
            this.bntRemove.Click += new System.EventHandler(this.bntRemove_Click);
            // 
            // Header
            // 
            this.Header.Controls.Add(this.grHome);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(935, 110);
            this.Header.TabIndex = 27;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Left
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Left";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Right
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Right";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Top
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Top";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(935, 741);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Bottom
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Floating;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Bottom";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(935, 741);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // FGVoucherListDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 741);
            this.Controls.Add(this.Body);
            this.Controls.Add(this.Header);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Top);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FGVoucherListDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FGVoucherListDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.grHome)).EndInit();
            this.grHome.ResumeLayout(false);
            this.grHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            this.Body.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetails)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Body;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Top;
        private Infragistics.Win.Misc.UltraGroupBox grHome;
        private System.Windows.Forms.Panel Header;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnView;
        private Infragistics.Win.Misc.UltraButton bntRemove;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetails;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnDeleteLine;
    }
}
