﻿namespace Accounting
{
    partial class FSelectVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.lblTypeVoucher = new Infragistics.Win.Misc.UltraLabel();
            this.lblDateTime = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbTypeiD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.pafHeader = new Infragistics.Win.Misc.UltraPanel();
            this.grbInfomation = new Infragistics.Win.Misc.UltraGroupBox();
            this.palFooter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.palBody2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridDanhSach = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeiD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).BeginInit();
            this.pafHeader.ClientArea.SuspendLayout();
            this.pafHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).BeginInit();
            this.grbInfomation.SuspendLayout();
            this.palFooter.ClientArea.SuspendLayout();
            this.palFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.palBody2.ClientArea.SuspendLayout();
            this.palBody2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTypeVoucher
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.lblTypeVoucher.Appearance = appearance1;
            this.lblTypeVoucher.AutoSize = true;
            this.lblTypeVoucher.Location = new System.Drawing.Point(485, 24);
            this.lblTypeVoucher.Name = "lblTypeVoucher";
            this.lblTypeVoucher.Size = new System.Drawing.Size(72, 14);
            this.lblTypeVoucher.TabIndex = 0;
            this.lblTypeVoucher.Text = "Loại chứng từ";
            // 
            // lblDateTime
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.lblDateTime.Appearance = appearance2;
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Location = new System.Drawing.Point(7, 24);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(89, 14);
            this.lblDateTime.TabIndex = 1;
            this.lblDateTime.Text = "Khoảng thời gian";
            // 
            // lblBeginDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.lblBeginDate.Appearance = appearance3;
            this.lblBeginDate.Location = new System.Drawing.Point(211, 24);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(27, 17);
            this.lblBeginDate.TabIndex = 3;
            this.lblBeginDate.Text = "Từ";
            // 
            // lblEndDate
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.lblEndDate.Appearance = appearance4;
            this.lblEndDate.Location = new System.Drawing.Point(349, 24);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(25, 17);
            this.lblEndDate.TabIndex = 4;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbTypeiD
            // 
            this.cbbTypeiD.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbTypeiD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTypeiD.LimitToList = true;
            this.cbbTypeiD.Location = new System.Drawing.Point(563, 19);
            this.cbbTypeiD.Name = "cbbTypeiD";
            this.cbbTypeiD.NullText = "<chọn dữ liệu>";
            this.cbbTypeiD.Size = new System.Drawing.Size(139, 22);
            this.cbbTypeiD.TabIndex = 32;
            this.cbbTypeiD.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbTypeiD_ItemNotInList);
            this.cbbTypeiD.TextChanged += new System.EventHandler(this.cbbAccountingObjectID_TextChanged);
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(98, 19);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(107, 22);
            this.cbbDateTime.TabIndex = 33;
            this.cbbDateTime.ValueChanged += new System.EventHandler(this.cbbDateTime_ValueChanged);
            // 
            // dteTo
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dteTo.Appearance = appearance5;
            this.dteTo.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteTo.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteTo.Location = new System.Drawing.Point(380, 19);
            this.dteTo.MaskInput = "";
            this.dteTo.Name = "dteTo";
            this.dteTo.Size = new System.Drawing.Size(99, 21);
            this.dteTo.TabIndex = 35;
            this.dteTo.Value = null;
            // 
            // dteFrom
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.dteFrom.Appearance = appearance6;
            this.dteFrom.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFrom.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteFrom.Location = new System.Drawing.Point(244, 19);
            this.dteFrom.MaskInput = "";
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Size = new System.Drawing.Size(99, 21);
            this.dteFrom.TabIndex = 36;
            this.dteFrom.Value = null;
            // 
            // btnGetData
            // 
            appearance7.Image = global::Accounting.Properties.Resources.refresh;
            this.btnGetData.Appearance = appearance7;
            this.btnGetData.Location = new System.Drawing.Point(708, 18);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(99, 24);
            this.btnGetData.TabIndex = 37;
            this.btnGetData.Text = "Lấy dữ liệu";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.Image = global::Accounting.Properties.Resources.apply_32;
            this.btnOk.Appearance = appearance8;
            this.btnOk.Location = new System.Drawing.Point(1058, 12);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(82, 30);
            this.btnOk.TabIndex = 41;
            this.btnOk.Text = "Đồng ý";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnCancel.Appearance = appearance9;
            this.btnCancel.Location = new System.Drawing.Point(1146, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 30);
            this.btnCancel.TabIndex = 42;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pafHeader
            // 
            // 
            // pafHeader.ClientArea
            // 
            this.pafHeader.ClientArea.Controls.Add(this.grbInfomation);
            this.pafHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pafHeader.Location = new System.Drawing.Point(0, 0);
            this.pafHeader.Name = "pafHeader";
            this.pafHeader.Size = new System.Drawing.Size(1239, 55);
            this.pafHeader.TabIndex = 43;
            // 
            // grbInfomation
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.BackColor2 = System.Drawing.Color.Transparent;
            this.grbInfomation.Appearance = appearance10;
            this.grbInfomation.Controls.Add(this.lblTypeVoucher);
            this.grbInfomation.Controls.Add(this.lblDateTime);
            this.grbInfomation.Controls.Add(this.lblBeginDate);
            this.grbInfomation.Controls.Add(this.lblEndDate);
            this.grbInfomation.Controls.Add(this.cbbTypeiD);
            this.grbInfomation.Controls.Add(this.btnGetData);
            this.grbInfomation.Controls.Add(this.cbbDateTime);
            this.grbInfomation.Controls.Add(this.dteFrom);
            this.grbInfomation.Controls.Add(this.dteTo);
            this.grbInfomation.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance11.FontData.BoldAsString = "True";
            appearance11.FontData.SizeInPoints = 8F;
            this.grbInfomation.HeaderAppearance = appearance11;
            this.grbInfomation.Location = new System.Drawing.Point(0, 0);
            this.grbInfomation.Name = "grbInfomation";
            this.grbInfomation.Size = new System.Drawing.Size(1239, 55);
            this.grbInfomation.TabIndex = 38;
            this.grbInfomation.Text = "Điều kiện chọn";
            this.grbInfomation.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palFooter
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.BackColor2 = System.Drawing.Color.Transparent;
            appearance12.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance12.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance12.BackColorDisabled2 = System.Drawing.Color.Transparent;
            appearance12.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance12.BorderColor = System.Drawing.Color.Transparent;
            appearance12.BorderColor2 = System.Drawing.Color.Transparent;
            appearance12.BorderColor3DBase = System.Drawing.Color.Transparent;
            this.palFooter.Appearance = appearance12;
            // 
            // palFooter.ClientArea
            // 
            this.palFooter.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palFooter.Location = new System.Drawing.Point(0, 421);
            this.palFooter.Name = "palFooter";
            this.palFooter.Size = new System.Drawing.Size(1239, 45);
            this.palFooter.TabIndex = 46;
            // 
            // ultraGroupBox1
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance13;
            this.ultraGroupBox1.Controls.Add(this.btnOk);
            this.ultraGroupBox1.Controls.Add(this.btnCancel);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance14.FontData.BoldAsString = "True";
            appearance14.FontData.SizeInPoints = 8F;
            this.ultraGroupBox1.HeaderAppearance = appearance14;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1239, 45);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.palBody2);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 55);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1239, 366);
            this.ultraPanel1.TabIndex = 47;
            // 
            // palBody2
            // 
            // 
            // palBody2.ClientArea
            // 
            this.palBody2.ClientArea.Controls.Add(this.uGridDanhSach);
            this.palBody2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palBody2.Location = new System.Drawing.Point(0, 0);
            this.palBody2.Name = "palBody2";
            this.palBody2.Size = new System.Drawing.Size(1239, 366);
            this.palBody2.TabIndex = 45;
            // 
            // uGridDanhSach
            // 
            this.uGridDanhSach.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDanhSach.Location = new System.Drawing.Point(0, 0);
            this.uGridDanhSach.Name = "uGridDanhSach";
            this.uGridDanhSach.Size = new System.Drawing.Size(1239, 366);
            this.uGridDanhSach.TabIndex = 39;
            this.uGridDanhSach.Text = "ultraGrid1";
            this.uGridDanhSach.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // FSelectVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 466);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.palFooter);
            this.Controls.Add(this.pafHeader);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSelectVoucher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn chứng từ";
            ((System.ComponentModel.ISupportInitialize)(this.cbbTypeiD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).EndInit();
            this.pafHeader.ClientArea.ResumeLayout(false);
            this.pafHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).EndInit();
            this.grbInfomation.ResumeLayout(false);
            this.grbInfomation.PerformLayout();
            this.palFooter.ClientArea.ResumeLayout(false);
            this.palFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.palBody2.ClientArea.ResumeLayout(false);
            this.palBody2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblTypeVoucher;
        private Infragistics.Win.Misc.UltraLabel lblDateTime;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbTypeiD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFrom;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraPanel pafHeader;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel palFooter;
        private Infragistics.Win.Misc.UltraGroupBox grbInfomation;
        private Infragistics.Win.Misc.UltraPanel palBody2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDanhSach;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
    }
}