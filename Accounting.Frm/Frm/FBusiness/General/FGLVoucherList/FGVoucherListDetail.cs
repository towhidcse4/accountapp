﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Accounting;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.Windows.Forms;
using Type = Accounting.Core.Domain.Type;

namespace Accounting
{
    public partial class FGVoucherListDetail : FGVoucherListDetailBase
    {
        #region khởi tạo

        private readonly IGVoucherListService _iGVoucherListService;
        private readonly IVoucherTypeService _voucherTypeSrv;
        private readonly IGVoucherListDetailService _gVoucherListDetailSrv;
        private const int TypeId = 680;
        private const int TypeGruopId = 61;
        readonly List<Account> _lstAccount = new List<Account>();
        readonly List<Type> _dsType = new List<Type>();
        private List<GVoucherListDetail> _listGVoucherListDetails = new List<GVoucherListDetail>();
        private readonly IGVoucherListService _gVoucherListSrv;

        public FGVoucherListDetail(GVoucherList temp, List<GVoucherList> lstItems, int statusForm)
        {
            //Add -> thêm, Sửa -> sửa, Xem -> Sửa ở trạng thái Enable = False
            _iGVoucherListService = IoC.Resolve<IGVoucherListService>();
            _voucherTypeSrv = IoC.Resolve<IVoucherTypeService>();
            _gVoucherListDetailSrv = IoC.Resolve<IGVoucherListDetailService>();
            _gVoucherListSrv = IoC.Resolve<IGVoucherListService>();
            _lstAccount = IAccountService.GetListAccountIsActive(true);
            _dsType = IoC.Resolve<ITypeService>().GetAll();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion


            #region Thiết lập ban đầu cho Form
            _listSelects.AddRange(lstItems);
            _statusForm = statusForm;
            _select = temp.CloneObject();
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = TypeId;
                _select.No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(TypeGruopId));
                _select.Date = Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now;
            }

            InitializeGUI(_select);
            //Change Menu Top
            ReloadToolbar(_select, lstItems, _statusForm);
            ChangeStatusControl(!statusForm.Equals(ConstFrm.optStatusForm.View));
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnTemplate0", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 0);
            //this.WindowState = FormWindowState.Maximized;
            #endregion

            Visible();
        }
        #endregion
        #region override
        public override void InitializeGUI(GVoucherList input)
        {
            // xét giá trị cho Form
            txtNo.Text = input.No;
            dteDate.DateTime = input.Date;
            txtReason.Text = input.Description;
            #region Body
            uGridDetails.SetDataBinding(input.GVoucherListDetails, "");
            ConfigGridTabHangTien(uGridDetails);
            #endregion
        }
        protected override bool CheckError()
        {
            return true;
        }

        //protected override object SetOrGetGuiObject(GVoucherList input, bool isGet)
        //{
        //    return input;
        //}
        protected override void RSForm()
        {
            _select = new GVoucherList();
            _select.TypeID = TypeId;
            _select.No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(TypeGruopId));
            _select.Date = Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now;
            // xét giá trị cho Form
            txtNo.Text = _select.No;
            dteDate.DateTime = _select.Date;
            txtReason.Text = _select.Description;
            uGridDetails.SetDataBinding(_select.GVoucherListDetails, "");
            ReloadToolbar(_select, _gVoucherListSrv.GetAll(), ConstFrm.optStatusForm.Add);
        }

        protected override bool Add()
        {
            if (_select.GVoucherListDetails.Count == 0)
            {
                MSG.Error("Bạn phải chọn ít nhất một chứng từ");
                return false;
            }
            //Gui to object
            #region Fill dữ liệu control vào obj
            uGridDetails.UpdateData();
            GVoucherList temp = GetValueForm(_select);//(_select, _listGVoucherListDetails);
            #endregion

            #region Thao tác CSDL
            try
            {
                if (_iGVoucherListService.Query.Any(n => n.No == txtNo.Text))
                {
                    MSG.Error("Số chứng từ đã tồn tại");
                    return false;
                }
                _iGVoucherListService.BeginTran();
                _iGVoucherListService.CreateNew(temp);

                #region Up bảng gencode sinh chứng từ tiếp theo
                GenCode gencode = IGenCodeService.getGenCode(TypeGruopId);
                Dictionary<string, string> dicNo = Utils.CheckNo(txtNo.Text);
                if (dicNo != null)
                {
                    gencode.Prefix = dicNo["Prefix"];
                    decimal _decimal = 0;
                    decimal.TryParse(dicNo["Value"], out _decimal);
                    gencode.CurrentValue = _decimal + 1;
                    gencode.Suffix = dicNo["Suffix"];
                    IGenCodeService.Update(gencode);
                }
                #endregion
                _iGVoucherListService.CommitTran();
                _iGVoucherListService.UnbindSession(_iGVoucherListService.GetAll());
                _listSelects.Insert(0, temp);
            }
            catch (Exception ex)
            {
                _iGVoucherListService.RolbackTran();
            }
            #endregion
            Visible();
            return true;
        }

        protected override bool Saveleged()
        {
            bool status = true;
            try
            {
                #region
                //MBTellerPaper temp = IoC.Resolve<IMBTellerPaperService>().GetByNo(txtNo.Text);
                //if (temp == null)
                //{
                //    return status;
                //}
                //List<MCReceiptDetail> listTempDetail =
                //    IMCReceiptDetailService.Query.Where(p => p.MCReceiptID == temp.ID).ToList();
                //List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
                //for (int i = 0; i < listTempDetail.Count; i++)
                //{
                //    GeneralLedger genTemp = new GeneralLedger
                //    {
                //        ID = Guid.NewGuid(),
                //        BranchID = null,
                //        ReferenceID = temp.ID,
                //        TypeID = temp.TypeID,
                //        Date = temp.Date,
                //        PostedDate = temp.PostedDate,
                //        No = temp.No,
                //        //InvoiceDate = temp.InvoiceDate,
                //        //InvoiceNo = temp.InvoiceNo,
                //        Account = listTempDetail[i].DebitAccount,
                //        AccountCorresponding = listTempDetail[i].CreditAccount,
                //        BankAccountDetailID = listTempDetail[i].BankAccountDetailID,
                //        CurrencyID = temp.CurrencyID,
                //        ExchangeRate = temp.ExchangRate,
                //        DebitAmount = listTempDetail[i].Amount,
                //        DebitAmountOriginal = listTempDetail[i].AmountOriginal,
                //        CreditAmount = 0,
                //        CreditAmountOriginal = 0,
                //        Reason = temp.Reason,
                //        Description = listTempDetail[i].Description,
                //        AccountingObjectID = temp.AccountingObjectID,
                //        EmployeeID = temp.EmployeeID,
                //        BudgetItemID = listTempDetail[i].BudgetItemID,
                //        CostSetID = listTempDetail[i].CostSetID,
                //        ContractID = listTempDetail[i].ContractID,
                //        StatisticsCodeID = listTempDetail[i].StatisticsCodeID,
                //        InvoiceSeries = temp.InvoiceSeries,
                //        ContactName = temp.Payers,
                //        DetailID = listTempDetail[i].ID,
                //        RefNo = temp.No,
                //        RefDate = temp.Date,
                //        DepartmentID = listTempDetail[i].DepartmentID,
                //        ExpenseItemID = listTempDetail[i].ExpenseItemID,
                //        IsIrrationalCost = listTempDetail[i].IsIrrationalCost
                //    };
                //    if (((listTempDetail[i].CreditAccount.StartsWith("133")) || listTempDetail[i].CreditAccount.StartsWith("33311")))
                //    {
                //        genTemp.VATDescription = listTempDetail[i].Description;
                //    }
                //    listGenTemp.Add(genTemp);
                //    GeneralLedger genTempCorresponding = new GeneralLedger
                //    {
                //        ID = Guid.NewGuid(),
                //        BranchID = null,
                //        ReferenceID = temp.ID,
                //        TypeID = temp.TypeID,
                //        Date = temp.Date,
                //        PostedDate = temp.PostedDate,
                //        No = temp.No,
                //        InvoiceDate = temp.InvoiceDate,
                //        InvoiceNo = temp.InvoiceNo,
                //        Account = listTempDetail[i].CreditAccount,
                //        AccountCorresponding = listTempDetail[i].DebitAccount,
                //        BankAccountDetailID = listTempDetail[i].BankAccountDetailID,
                //        CurrencyID = temp.CurrencyID,
                //        ExchangeRate = temp.ExchangRate,
                //        DebitAmount = 0,
                //        DebitAmountOriginal = 0,
                //        CreditAmount = listTempDetail[i].Amount,
                //        CreditAmountOriginal = listTempDetail[i].AmountOriginal,
                //        Reason = temp.Reason,
                //        Description = listTempDetail[i].Description,
                //        AccountingObjectID = temp.AccountingObjectID,
                //        EmployeeID = temp.EmployeeID,
                //        BudgetItemID = listTempDetail[i].BudgetItemID,
                //        CostSetID = listTempDetail[i].CostSetID,
                //        ContractID = listTempDetail[i].ContractID,
                //        StatisticsCodeID = listTempDetail[i].StatisticsCodeID,
                //        InvoiceSeries = temp.InvoiceSeries,
                //        ContactName = temp.Payers,
                //        DetailID = listTempDetail[i].ID,
                //        RefNo = temp.No,
                //        RefDate = temp.Date,
                //        DepartmentID = listTempDetail[i].DepartmentID,
                //        ExpenseItemID = listTempDetail[i].ExpenseItemID,
                //        IsIrrationalCost = listTempDetail[i].IsIrrationalCost
                //    };
                //    if (((listTempDetail[i].CreditAccount.StartsWith("133")) || listTempDetail[i].CreditAccount.StartsWith("33311")))
                //    {
                //        genTempCorresponding.VATDescription = listTempDetail[i].Description;
                //    }
                //    listGenTemp.Add(genTempCorresponding);
                //}
                //IMCReceiptService.BeginTran();
                //foreach (var generalLedger in listGenTemp)
                //{
                //    IGeneralLedgerService.CreateNew(generalLedger);
                //}
                //temp.Recorded = true;
                //IMCReceiptService.Update(temp);
                //IMCReceiptService.CommitTran();
                #endregion
            }
            catch
            {
                //IMCReceiptService.RolbackTran();
                status = false;
            }
            return true;
        }

        //protected override bool Removeleged()
        //{
        //    // Xóa dữ liệu phiếu thu ? các bảng sổ cái và cập nhật trạng thái của phiếu thu
        //    // 1. Xóa dữ liệu trong các bảng sổ cái GenegedLeged
        //    // 2. Cập nhật trạng thái của phiếu thu
        //    bool status = true;
        //    try
        //    {
        //        _iGVoucherListService.BeginTran();
        //        // 1. Xóa dữ liệu vào các bảng sổ cái GenegedLeged
        //        //_select.Recorded = false;
        //        //List<GeneralLedger> listGenTemp = IGeneralLedgerService.Query.Where(p => p.ReferenceID == _select.ID).ToList();
        //        List<GeneralLedger> listGenTemp = IGeneralLedgerService.GetListByReferenceID(_select.ID);
        //        foreach (var generalLedger in listGenTemp)
        //        {
        //            IGeneralLedgerService.Delete(generalLedger);
        //        }
        //        _iGVoucherListService.Update(_select);
        //        _iGVoucherListService.CommitTran();
        //    }
        //    catch (Exceptions)
        //    {
        //        _iGVoucherListService.RolbackTran();
        //        status = false;
        //    }
        //    return status;
        //} 

        protected override bool Edit()
        {

            //Gui to object
            #region Fill dữ liệu control vào obj
            uGridDetails.UpdateData();
            GVoucherList objNew = GetValueForm(_select);//(_select, _listGVoucherListDetails);
            if (objNew.GVoucherListDetails.Count == 0)
            {
                MSG.Error("Bạn phải chọn ít nhất một chứng từ");
                return false;
            }
            #endregion

            #region Thao tác CSDL

            if (objNew.GVoucherListDetails.Count > 0)
            {
                try
                {
                    _iGVoucherListService.BeginTran();
                    //List<GVoucherListDetail> lstDetailsFull = _gVoucherListDetailSrv.GetByGvoucherListId(_select.ID);
                    //// lấy tất cả danh sách chi tiết chứng từ ghi sổ (bao gồm cả chứng từ cũ và chứng từ mới)
                    // // cập nhật dữ liệu mới nhất trên uGridDetails
                    //List<GVoucherListDetail> lstDetailInGrid = uGridDetails.DataSource as List<GVoucherListDetail>;
                    //// danh sách chi tiết CTGS trên Grid
                    //if (lstDetailInGrid != null)
                    //    foreach (var item in lstDetailsFull.Except(lstDetailInGrid))
                    //        _gVoucherListDetailSrv.Delete(item);
                    //// xóa những chi tiết CTGS không được chọn khỏi CSDL
                    GVoucherList glVoucherList = _iGVoucherListService.Getbykey(objNew.ID);
                    glVoucherList.Date = dteDate.DateTime;
                    glVoucherList.Description = txtReason.Text;
                    glVoucherList.No = txtNo.Text;
                    glVoucherList.TypeID = TypeID;
                    glVoucherList.Description = txtReason.Text;
                    glVoucherList.TotalAmount = objNew.GVoucherListDetails.Sum(c => (decimal)c.VoucherAmount);
                    glVoucherList.Exported = true;
                    glVoucherList.GVoucherListDetails.Clear();
                    foreach (GVoucherListDetail gVoucherListDetail in objNew.GVoucherListDetails)
                    {
                        gVoucherListDetail.ID = Guid.NewGuid();
                        glVoucherList.GVoucherListDetails.Add(gVoucherListDetail);
                    }
                    _iGVoucherListService.Update(glVoucherList);

                    //foreach (GVoucherListDetail gVoucherListDetail in objNew.GVoucherListDetails.Where(gVoucherListDetail => glVoucherList.GVoucherListDetails.All(c => c.ID != gVoucherListDetail.ID)))
                    //{
                    //    _gVoucherListDetailSrv.CreateNew(gVoucherListDetail);
                    //}
                    //foreach (GVoucherListDetail gVoucherListDetail in glVoucherList.GVoucherListDetails.Where(gVoucherListDetail => objNew.GVoucherListDetails.Any(c => c.ID == gVoucherListDetail.ID)))
                    //{
                    //    _gVoucherListDetailSrv.Delete(gVoucherListDetail);
                    //}
                    // xóa những chi tiết CTGS không được chọn khỏi CSDL


                    //foreach (var items in glVoucherList.GVoucherListDetails)
                    //    items.GVoucherListID = glVoucherList.ID;

                    _iGVoucherListService.CommitTran();
                    Utils.ClearCacheByType<GVoucherList>();
                    _select = _iGVoucherListService.Getbykey(_select.ID);
                }
                catch (Exception)
                {
                    _iGVoucherListService.RolbackTran();
                    throw;
                }
            }
            #endregion
            Visible();
            return true;
        }

        protected override bool Delete()
        {
            try
            {
                _iGVoucherListService.BeginTran();
                _select = _iGVoucherListService.Getbykey(_select.ID);
                _iGVoucherListService.Delete(_select);
                _iGVoucherListService.CommitTran();
                Utils.ClearCacheByType<GVoucherList>();
            }
            catch (Exception ex)
            {
                MSG.Error(ex.Message);
                _iGVoucherListService.RolbackTran();
            }


            return true;
        }

        //public override void ChangeStatusControl(bool isEnable)
        //{
        //}
        #endregion

        #region Utils
        GVoucherList GetValueForm(GVoucherList model)
        {
            uGridDetails.UpdateData();
            model.ID = model.ID == Guid.Empty ? Guid.NewGuid() : model.ID;
            model.Date = dteDate.DateTime;
            model.Description = txtReason.Text;
            model.No = txtNo.Text;
            model.TypeID = TypeID;
            model.Description = txtReason.Text;
            model.TotalAmount = model.GVoucherListDetails.Sum(c => (decimal)c.VoucherAmount);
            model.Exported = true;
            foreach (var items in model.GVoucherListDetails)
                items.GVoucherListID = model.ID;
            return model;
        }

        private void Visible()
        {
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnTemplate0", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 0);
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 0);
        }
        #endregion


        void ConfigGridTabHangTien(UltraGrid uGridInput)
        {
            Utils.ConfigGrid(uGridInput, ConstDatabase.FGVoucherListDetail_TableName);
            uGridInput.Text = "";
            //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridInput.DisplayLayout.Bands[0];
            if (band.Summaries.Count != 0)
                band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["VoucherTypeID"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridInput.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridInput.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridInput.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridInput.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            // Thêm tổng số ở cột "Số còn phải thu"
            summary = band.Summaries.Add("SumVoucherAmount", SummaryType.Sum, band.Columns["VoucherAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            summary.Appearance.TextHAlign = HAlign.Right;
            uGridInput.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;
            uGridInput.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            #region Colums Style
            foreach (var item in uGridInput.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Status"))   //tài khoản nợ, tài khoản có
                    item.Hidden = true;
                if (item.Key.Equals("DebitAccount") || item.Key.Equals("CreditAccount"))   //tài khoản nợ, tài khoản có
                {
                    List<Account> lstTemp =
                        IAccountDefaultService.GetAccountDefaultByTypeId(_select.TypeID, item.Key).Count > 0
                            ? IAccountDefaultService.GetAccountDefaultByTypeId(_select.TypeID,
                                                                               item.Key)
                            : _lstAccount.OrderBy(k => k.AccountName).ToList();
                    this.ConfigCbbToGrid(uGridInput, item.Key, lstTemp, "ID", "AccountNumber", ConstDatabase.Account_TableName);
                }
                //TK Ngân hàng
                if (item.Key.Equals("VoucherTypeID"))
                    this.ConfigCbbToGrid(uGridInput, item.Key, _dsType, "ID", "TypeName", ConstDatabase.Type_TableName);
            }
            uGridInput.DisplayLayout.Bands[0].Columns["VoucherAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            #endregion
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (var frm = new FSelectVoucher())
            {
                //frm.FormClosed += new FormClosedEventHandler(frm_FormClosed);
                if (frm.ShowDialog(this) == DialogResult.OK)
                {
                    List<GVoucherListDetail> temp = frm.VoucherListDetails;
                    if (temp != null)
                    {
                        foreach (var item in Utils.CloneObject(temp))
                        {
                            _select.GVoucherListDetails.Add(item);
                        }
                        uGridDetails.SetDataBinding(_select.GVoucherListDetails, "");
                        _select.FromDate = frm.DateFrom;
                        _select.ToDate = frm.DateTo;
                    }
                }
            }
        }
        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //var frm = (FSelectVoucher)sender;
            //if (e.CloseReason == CloseReason.UserClosing)
            //    if (frm.DialogResult == DialogResult.OK)
            //    {
            //        List<GVoucherListDetail> temp = frm.VoucherListDetails;
            //        if (temp != null)
            //        {
            //            uGridDetails.DataSource = Utils.CloneObject(temp);
            //            _select.FromDate = frm.DateFrom;
            //            _select.ToDate = frm.DateTo;
            //        }
            //    }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            GVoucherListDetail data = new GVoucherListDetail();
            WaitingFrm.StartWaiting();
            if (uGridDetails.ActiveRow != null)
            {
                data = uGridDetails.ActiveRow.ListObject as GVoucherListDetail;
                Utils.ViewVoucherSelected(data.VoucherID, data.VoucherTypeID);
                WaitingFrm.StopWaiting();
            }
            else
            {
                WaitingFrm.StopWaiting();
                MSG.Error("Bạn cần chọn chứng từ trước");
            }
        }

        private void bntRemove_Click(object sender, EventArgs e)
        {
            uGridDetails.UpdateData();
            if (uGridDetails.Selected.Rows.Count == 0)
            {
                MessageBox.Show("Bạn chưa chọn chứng từ cần xóa", "Thông báo");
            }
            else
            {
                //GVoucherListDetail gVoucherListDetail = (GVoucherListDetail)uGridDetails.ActiveRow.ListObject;
                //_select.GVoucherListDetails.Remove(gVoucherListDetail);
                //uGridDetails.Refresh();
                uGridDetails.DeleteSelectedRows();
                if (uGridDetails.Rows.Count > 0)
                {
                    uGridDetails.Rows[0].Selected = true;
                    uGridDetails.Rows[0].Activated = true;
                }
            }
        }

        private void uGridDetails_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
        }

        private void btnDeleteLine_Click(object sender, EventArgs e)
        {
            if (_statusForm == ConstFrm.optStatusForm.View)
            {
                return;
            }
            uGridDetails.UpdateData();
            if (uGridDetails.Selected.Rows.Count == 0)
            {
                MessageBox.Show("Bạn chưa chọn chứng từ cần xóa", "Thông báo");
            }
            else
            {
                //GVoucherListDetail gVoucherListDetail = (GVoucherListDetail)uGridDetails.ActiveRow.ListObject;
                //_select.GVoucherListDetails.Remove(gVoucherListDetail);
                //uGridDetails.Refresh();
                uGridDetails.DeleteSelectedRows();
                if (uGridDetails.Rows.Count > 0)
                {
                    uGridDetails.Rows[0].Selected = true;
                    uGridDetails.Rows[0].Activated = true;
                }
            }
        }

        private void uGridDetails_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            GVoucherListDetail data = new GVoucherListDetail();
            WaitingFrm.StartWaiting();
            if (uGridDetails.ActiveRow != null)
            {
                data = uGridDetails.ActiveRow.ListObject as GVoucherListDetail;
                Utils.ViewVoucherSelected(data.VoucherID, data.VoucherTypeID);
                WaitingFrm.StopWaiting();
            }
            else
            {
                WaitingFrm.StopWaiting();
                MSG.Error("Bạn cần chọn chứng từ trước");
            }
        }

        private void FGVoucherListDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Utils.IGVoucherListService.UnbindSession(_select);
            //_select = Utils.IGVoucherListService.Getbykey(_select.ID);
            IsClose = true;
        }

        private void uGridDetails_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void uGridDetails_CellDataError(object sender, CellDataErrorEventArgs e)
        {

        }

        private void uGridDetails_Error(object sender, ErrorEventArgs e)
        {

        }

        private void uGridDetails_BeforeCellCancelUpdate(object sender, CancelableCellEventArgs e)
        {

        }
    }

    public class FGVoucherListDetailBase : DetailBase<GVoucherList>
    {
    }
}