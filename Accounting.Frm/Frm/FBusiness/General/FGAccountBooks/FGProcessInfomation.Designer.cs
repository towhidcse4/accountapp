﻿namespace Accounting
{
    partial class FGProcessInfomation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGProcessInfomation));
            this.pafHeader = new Infragistics.Win.Misc.UltraPanel();
            this.grbInfomation = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblGhiSo = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhoaHtoan = new Infragistics.Win.Misc.UltraLabel();
            this.lblXoa = new Infragistics.Win.Misc.UltraLabel();
            this.pafHeader.ClientArea.SuspendLayout();
            this.pafHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).BeginInit();
            this.grbInfomation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            this.SuspendLayout();
            // 
            // pafHeader
            // 
            // 
            // pafHeader.ClientArea
            // 
            this.pafHeader.ClientArea.Controls.Add(this.grbInfomation);
            resources.ApplyResources(this.pafHeader, "pafHeader");
            this.pafHeader.Name = "pafHeader";
            // 
            // grbInfomation
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            this.grbInfomation.Appearance = appearance1;
            this.grbInfomation.Controls.Add(this.lblXoa);
            this.grbInfomation.Controls.Add(this.lblKhoaHtoan);
            this.grbInfomation.Controls.Add(this.lblGhiSo);
            this.grbInfomation.Controls.Add(this.ultraLabel1);
            this.grbInfomation.Controls.Add(this.btnApply);
            this.grbInfomation.Controls.Add(this.btnCancel);
            resources.ApplyResources(this.grbInfomation, "grbInfomation");
            appearance8.FontData.BoldAsString = resources.GetString("resource.BoldAsString");
            appearance8.FontData.SizeInPoints = ((float)(resources.GetObject("resource.SizeInPoints")));
            this.grbInfomation.HeaderAppearance = appearance8;
            this.grbInfomation.Name = "grbInfomation";
            this.grbInfomation.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnApply
            // 
            resources.ApplyResources(this.btnApply, "btnApply");
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            this.btnApply.Appearance = appearance6;
            this.btnApply.Name = "btnApply";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            this.btnCancel.Appearance = appearance7;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cbbDateTime
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance9;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbDateTime.ButtonsRight.Add(editorButton1);
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            resources.ApplyResources(this.cbbDateTime, "cbbDateTime");
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            // 
            // cbbCurrency
            // 
            appearance10.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance10;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCurrency.ButtonsRight.Add(editorButton2);
            this.cbbCurrency.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            resources.ApplyResources(this.cbbCurrency, "cbbCurrency");
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.NullText = "<chọn dữ liệu>";
            // 
            // ultraLabel1
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(appearance5, "appearance5");
            this.ultraLabel1.Appearance = appearance5;
            resources.ApplyResources(this.ultraLabel1, "ultraLabel1");
            this.ultraLabel1.Name = "ultraLabel1";
            // 
            // lblGhiSo
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(appearance4, "appearance4");
            this.lblGhiSo.Appearance = appearance4;
            resources.ApplyResources(this.lblGhiSo, "lblGhiSo");
            this.lblGhiSo.Name = "lblGhiSo";
            // 
            // lblKhoaHtoan
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(appearance3, "appearance3");
            this.lblKhoaHtoan.Appearance = appearance3;
            resources.ApplyResources(this.lblKhoaHtoan, "lblKhoaHtoan");
            this.lblKhoaHtoan.Name = "lblKhoaHtoan";
            // 
            // lblXoa
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(appearance2, "appearance2");
            this.lblXoa.Appearance = appearance2;
            resources.ApplyResources(this.lblXoa, "lblXoa");
            this.lblXoa.Name = "lblXoa";
            // 
            // FGProcessInfomation
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pafHeader);
            this.Name = "FGProcessInfomation";
            this.pafHeader.ClientArea.ResumeLayout(false);
            this.pafHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).EndInit();
            this.grbInfomation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel pafHeader;
        private Infragistics.Win.Misc.UltraGroupBox grbInfomation;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraLabel lblXoa;
        private Infragistics.Win.Misc.UltraLabel lblKhoaHtoan;
        private Infragistics.Win.Misc.UltraLabel lblGhiSo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;

    }
}