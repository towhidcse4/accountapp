﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using Castle.Components.DictionaryAdapter;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGProcessInfomation : CustormForm
    {
        public static bool IsClose;

        public FGProcessInfomation(List<VoucherNotSave> lstVoucherNotSave)
        {
            InitializeComponent();
            lblGhiSo.Text += lstVoucherNotSave.Count(t => t.ProcessType == 0);
            lblKhoaHtoan.Text += lstVoucherNotSave.Count(t => t.ProcessType == 1);
            lblXoa.Text += lstVoucherNotSave.Count(t => t.ProcessType == 2);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
            IsClose = true;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            Close();
            IsClose = false;
        }
    }


}
