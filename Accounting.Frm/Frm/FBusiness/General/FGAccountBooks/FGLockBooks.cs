﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;

namespace Accounting
{
    public partial class FGLockBooks : CustormForm
    {
        public FGLockBooks()
        {
            InitializeComponent();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (GetVoucherNoteSave() != null)
            {
                using (var frm = new FGProcessVoucher(GetVoucherNoteSave()))
                {
                    frm.ShowDialog(this);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Dữ liệu test
        private List<VoucherNotSave> GetVoucherNoteSave()
        {
            return new List<VoucherNotSave>
                       {
                           new VoucherNotSave
                               {
                                   VoucherId = Guid.NewGuid(),
                                   VoucherDate = DateTime.Now,
                                   PostedDate = DateTime.Now,
                                   VoucherNo = "PC00002",
                                   ProcessType = 0,
                               },
                               new VoucherNotSave
                               {
                                   VoucherId = Guid.NewGuid(),
                                   VoucherDate = DateTime.Now,
                                   PostedDate = DateTime.Now,
                                   VoucherNo = "PC00003",
                                   ProcessType = 0,
                               },
                               new VoucherNotSave
                               {
                                   VoucherId = Guid.NewGuid(),
                                   VoucherDate = DateTime.Now,
                                   PostedDate = DateTime.Now,
                                   VoucherNo = "PC00004",
                                   ProcessType = 0,
                               },
                       };
        }
        #endregion
    }
}
