﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using Castle.Components.DictionaryAdapter;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGProcessVoucher : CustormForm
    {
        private readonly IGeneralLedgerService _generalLedgerSrv;
        // ngày hoạch toán
        DateTime ngayHoachToan = DateTime.ParseExact(ConstFrm.DbStartDate, "dd/MM/yyyy",
                                                     CultureInfo.InvariantCulture);
        public FGProcessVoucher(List<VoucherNotSave> dsVoucherNotSave)
        {
            _generalLedgerSrv = IoC.Resolve<IGeneralLedgerService>();
            InitializeComponent();
            uGridDanhSach.DataSource = dsVoucherNotSave;
            ViewDanhSach(uGridDanhSach);
        }






        #region Cấu hình và sự kiện trong bảng hiện thị danh sách chứng từ
        // Hiển thị vùng thông tin danh sách
        private void ViewDanhSach(UltraGrid ultraGrid)
        {
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

            ultraGrid.Text = "";
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];

            #region Colums Style
            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("VoucherId"))
                {
                    item.Hidden = true;
                }
                if (item.Key.Equals("VoucherName"))
                {
                    item.Header.Caption = "Loại chứng từ";
                    item.Width = 200;
                    item.CellActivation = Activation.NoEdit;
                }
                if (item.Key.Contains("VoucherDate"))
                {
                    item.Header.Caption = "Ngày chứng từ";
                    item.Width = 80;
                    item.CellActivation = Activation.NoEdit;
                }
                if (item.Key.Contains("PostedDate"))
                {
                    item.Header.Caption = "Ngày hạch toán";
                    item.Width = 80;
                }
                if (item.Key.Contains("VoucherNo"))
                {
                    item.Header.Caption = "Số chứng từ";
                    item.CellActivation = Activation.NoEdit;
                }
                if (item.Key.Contains("ProcessType"))
                {
                    item.Header.Caption = "Kiểu xử lý";
                    Dictionary<int, string> _dic = new Dictionary<int, string>();
                    _dic.Add(0, "Ghi sổ");
                    _dic.Add(1, "Chuyển Ngày HT");
                    _dic.Add(2, "Xóa");
                    UltraCombo cbbProcessType = new UltraCombo();
                    cbbProcessType.DataSource = _dic.ToList();
                    cbbProcessType.DisplayMember = "Value";
                    cbbProcessType.ValueMember = "key";
                    cbbProcessType.DisplayLayout.Bands[0].Columns["key"].Hidden = true;
                    cbbProcessType.DisplayLayout.Bands[0].Columns["Value"].Header.Caption = "";
                    cbbProcessType.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                    cbbProcessType.DropDownWidth = 0;
                    item.EditorComponent = cbbProcessType;
                    item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;

                    cbbProcessType.RowSelected += new RowSelectedEventHandler(cbbProcessType_RowSelected);
                }
            }
            foreach (var row in uGridDanhSach.Rows)
                row.Cells["PostedDate"].Activation = Activation.NoEdit;
            #endregion
            // xóa dòng xét tổng ở cuối Grid
            ultraGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            // thêm dòng tính tổng cột "Số tiền" vào cuối Grid
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["VoucherName"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        }
        // sự kiện chọn kiểu xử lý
        private void cbbProcessType_RowSelected(object sender, RowSelectedEventArgs e)
        {
            int c = (int)e.Row.GetCellValue("key");
            uGridDanhSach.ActiveRow.Cells["PostedDate"].Activation = c == 1 ? Activation.AllowEdit : Activation.NoEdit;
        }
        #endregion

        

        private void btnApply_Click(object sender, EventArgs e)
        {
            uGridDanhSach.UpdateData();
            var dsVoucher = uGridDanhSach.DataSource as List<VoucherNotSave>;
            new FGProcessInfomation(dsVoucher).ShowDialog(this);
            if (!FGProcessInfomation.IsClose)
            {
                //reload
                uGridDanhSach.DataSource = new List<VoucherNotSave>{new VoucherNotSave
                                                                    {
                                                                     VoucherId   = Guid.NewGuid(),
                                                                     VoucherName = "Thu tiền khách hàng",
                                                                     PostedDate = new DateTime(2011,4,5),
                                                                     VoucherDate = new DateTime(2012,6,7),
                                                                     ProcessType = 2,
                                                                     VoucherNo = "PT0005"
                                                                    }};
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }


}
