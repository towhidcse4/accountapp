﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGForSetOff : CustormForm
    {
        private readonly IGeneralLedgerService _generalLedgerSrv;
        private readonly ICurrencyService _currencySrv;
        private readonly IAccountingObjectService _accountingObjectSrv;
        private readonly List<Item> _lstItems = Utils.ObjConstValue.SelectTimes;
        // ngày hoạch toán
        DateTime ngayHoachToan = DateTime.ParseExact(ConstFrm.DbStartDate, "dd/MM/yyyy",
                                                     CultureInfo.InvariantCulture);
        public FGForSetOff()
        {
            _generalLedgerSrv = IoC.Resolve<IGeneralLedgerService>();
            _currencySrv = IoC.Resolve<ICurrencyService>();
            _accountingObjectSrv = IoC.Resolve<IAccountingObjectService>();
            InitializeComponent();
            cbbDateTime.DataSource = _lstItems;
            cbbDateTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            // load ngày bắt đầu và ngày kết thúc (note: mặc định ban đầu load theo ngày hoạch toán)
            dteTo.DateTime = dteFrom.DateTime = ngayHoachToan;
            cbbCurrency.DataSource = _currencySrv.GetAll();
            cbbCurrency.DisplayMember = "ID";
            Utils.ConfigGrid(cbbCurrency, ConstDatabase.Currency_TableName);
            uGridDanhSach.DataSource = new List<ForSetOff>();
            ViewDanhSach(uGridDanhSach);
        }




        private void btnGetData_Click(object sender, EventArgs e)
        {
            // tạo dữ liệu test
            List<ForSetOff> lstDetails = (from c in _generalLedgerSrv.Query
                                          where (c.Account == "331 " || c.AccountCorresponding == "331") && (c.DebitAmount != 0 || c.CreditAmount != 0)
                                          select new ForSetOff { AccountingObjectName = c.ContactName, AccountNumber = c.Account, CreditAmount = c.CreditAmount, DebitAmount = c.DebitAmount }).ToList();
            // end dữ liệu test
            uGridDanhSach.DataSource = lstDetails;
        }


        #region Xử lý sự kiện trên form

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            if (cbbDateTime.SelectedRow != null)
            {
                var model = cbbDateTime.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(ngayHoachToan.Year,ngayHoachToan, model, out dtBegin, out dtEnd);
                dteFrom.DateTime = dtBegin;
                dteTo.DateTime = dtEnd;
            }

        }


        #endregion

        #region Hàm xử lý riêng của Form
        // Hiển thị vùng thông tin danh sách
        private void ViewDanhSach(UltraGrid ultraGrid)
        {
            Utils.ConfigGrid(ultraGrid, ConstDatabase.FGForSetOff_FormName);
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ultraGrid.Text = "";
            // xét style cho các cột có dạng checkbox
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Status"].CellActivation = Activation.AllowEdit;
            band.Columns["Status"].Header.Fixed = true;
            #region Colums Style
            foreach (var item in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Status"))
                    item.Width = 30;
                //if (item.Key.Equals("VoucherTypeID"))
                //    this.ConfigCbbToGrid(uGridDanhSach, item.Key, _typeSrv.GetAll(), "ID", "TypeName", ConstDatabase.Type_TableName);
            }
            #endregion
            // xóa dòng xét tổng ở cuối Grid
            ultraGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            // thêm dòng tính tổng cột "Số tiền" vào cuối Grid
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Status"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        }
        #endregion

        private void cbbCurrency_TextChanged(object sender, EventArgs e)
        {
            if (cbbCurrency.SelectedRow != null)
            {
                txtRateCurrency.Visible = true;
                Currency item = cbbCurrency.SelectedRow.ListObject as Currency;
                if (item.ID.ToUpper() == "VND")
                    txtRateCurrency.Visible = false;
                txtRateCurrency.Value = item.ExchangeRate;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // tạo chứng từ nghiệp vụ khác
            try
            {

            }
            catch (Exception)
            {

                throw;
            }
        }

    }


}
