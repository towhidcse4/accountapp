﻿namespace Accounting
{
    partial class FGForSetOff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            this.lblTypeVoucher = new Infragistics.Win.Misc.UltraLabel();
            this.lblDateTime = new Infragistics.Win.Misc.UltraLabel();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.btnOk = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.pafHeader = new Infragistics.Win.Misc.UltraPanel();
            this.grbInfomation = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtRateCurrency = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.palFooter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.palBody2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridDanhSach = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).BeginInit();
            this.pafHeader.ClientArea.SuspendLayout();
            this.pafHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).BeginInit();
            this.grbInfomation.SuspendLayout();
            this.palFooter.ClientArea.SuspendLayout();
            this.palFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.palBody2.ClientArea.SuspendLayout();
            this.palBody2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTypeVoucher
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.lblTypeVoucher.Appearance = appearance1;
            this.lblTypeVoucher.Location = new System.Drawing.Point(6, 45);
            this.lblTypeVoucher.Name = "lblTypeVoucher";
            this.lblTypeVoucher.Size = new System.Drawing.Size(78, 21);
            this.lblTypeVoucher.TabIndex = 0;
            this.lblTypeVoucher.Text = "Loại chứng từ";
            // 
            // lblDateTime
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.lblDateTime.Appearance = appearance2;
            this.lblDateTime.Location = new System.Drawing.Point(5, 18);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(100, 23);
            this.lblDateTime.TabIndex = 1;
            this.lblDateTime.Text = "Khoảng thời gian";
            // 
            // lblBeginDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.lblBeginDate.Appearance = appearance3;
            this.lblBeginDate.Location = new System.Drawing.Point(298, 22);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(27, 17);
            this.lblBeginDate.TabIndex = 3;
            this.lblBeginDate.Text = "Từ";
            // 
            // lblEndDate
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.lblEndDate.Appearance = appearance4;
            this.lblEndDate.Location = new System.Drawing.Point(436, 21);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(25, 17);
            this.lblEndDate.TabIndex = 4;
            this.lblEndDate.Text = "Đến";
            // 
            // cbbCurrency
            // 
            appearance5.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance5;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbCurrency.ButtonsRight.Add(editorButton1);
            this.cbbCurrency.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCurrency.Location = new System.Drawing.Point(111, 46);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.NullText = "<chọn dữ liệu>";
            this.cbbCurrency.Size = new System.Drawing.Size(181, 22);
            this.cbbCurrency.TabIndex = 32;
            this.cbbCurrency.TextChanged += new System.EventHandler(this.cbbCurrency_TextChanged);
            // 
            // cbbDateTime
            // 
            appearance6.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance6;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbDateTime.ButtonsRight.Add(editorButton2);
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(111, 20);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(181, 22);
            this.cbbDateTime.TabIndex = 33;
            this.cbbDateTime.ValueChanged += new System.EventHandler(this.cbbDateTime_ValueChanged);
            // 
            // dteTo
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.dteTo.Appearance = appearance7;
            this.dteTo.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteTo.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteTo.Location = new System.Drawing.Point(467, 20);
            this.dteTo.MaskInput = "";
            this.dteTo.Name = "dteTo";
            this.dteTo.Size = new System.Drawing.Size(99, 21);
            this.dteTo.TabIndex = 35;
            this.dteTo.Value = null;
            // 
            // dteFrom
            // 
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.dteFrom.Appearance = appearance8;
            this.dteFrom.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFrom.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteFrom.Location = new System.Drawing.Point(331, 19);
            this.dteFrom.MaskInput = "";
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Size = new System.Drawing.Size(99, 21);
            this.dteFrom.TabIndex = 36;
            this.dteFrom.Value = null;
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(467, 45);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(75, 23);
            this.btnGetData.TabIndex = 37;
            this.btnGetData.Text = "Lấy số liệu";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(1065, 12);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Thực hiện";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(1146, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 42;
            this.btnCancel.Text = "Hủy bỏ";
            // 
            // pafHeader
            // 
            // 
            // pafHeader.ClientArea
            // 
            this.pafHeader.ClientArea.Controls.Add(this.grbInfomation);
            this.pafHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pafHeader.Location = new System.Drawing.Point(0, 0);
            this.pafHeader.Name = "pafHeader";
            this.pafHeader.Size = new System.Drawing.Size(1239, 73);
            this.pafHeader.TabIndex = 43;
            // 
            // grbInfomation
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.BackColor2 = System.Drawing.Color.Transparent;
            this.grbInfomation.Appearance = appearance9;
            this.grbInfomation.Controls.Add(this.txtRateCurrency);
            this.grbInfomation.Controls.Add(this.lblTypeVoucher);
            this.grbInfomation.Controls.Add(this.lblDateTime);
            this.grbInfomation.Controls.Add(this.lblBeginDate);
            this.grbInfomation.Controls.Add(this.lblEndDate);
            this.grbInfomation.Controls.Add(this.cbbCurrency);
            this.grbInfomation.Controls.Add(this.btnGetData);
            this.grbInfomation.Controls.Add(this.cbbDateTime);
            this.grbInfomation.Controls.Add(this.dteFrom);
            this.grbInfomation.Controls.Add(this.dteTo);
            this.grbInfomation.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance10.FontData.BoldAsString = "True";
            appearance10.FontData.SizeInPoints = 8F;
            this.grbInfomation.HeaderAppearance = appearance10;
            this.grbInfomation.Location = new System.Drawing.Point(0, 0);
            this.grbInfomation.Name = "grbInfomation";
            this.grbInfomation.Size = new System.Drawing.Size(1239, 73);
            this.grbInfomation.TabIndex = 38;
            this.grbInfomation.Text = "Điều kiện chọn";
            this.grbInfomation.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtRateCurrency
            // 
            this.txtRateCurrency.Location = new System.Drawing.Point(330, 47);
            this.txtRateCurrency.Name = "txtRateCurrency";
            this.txtRateCurrency.Size = new System.Drawing.Size(100, 20);
            this.txtRateCurrency.TabIndex = 38;
            this.txtRateCurrency.Visible = false;
            // 
            // palFooter
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.BackColor2 = System.Drawing.Color.Transparent;
            appearance11.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance11.BackColorDisabled2 = System.Drawing.Color.Transparent;
            appearance11.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.BorderColor = System.Drawing.Color.Transparent;
            appearance11.BorderColor2 = System.Drawing.Color.Transparent;
            appearance11.BorderColor3DBase = System.Drawing.Color.Transparent;
            this.palFooter.Appearance = appearance11;
            // 
            // palFooter.ClientArea
            // 
            this.palFooter.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palFooter.Location = new System.Drawing.Point(0, 421);
            this.palFooter.Name = "palFooter";
            this.palFooter.Size = new System.Drawing.Size(1239, 45);
            this.palFooter.TabIndex = 46;
            // 
            // ultraGroupBox1
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.BackColor2 = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance12;
            this.ultraGroupBox1.Controls.Add(this.btnOk);
            this.ultraGroupBox1.Controls.Add(this.btnCancel);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance13.FontData.BoldAsString = "True";
            appearance13.FontData.SizeInPoints = 8F;
            this.ultraGroupBox1.HeaderAppearance = appearance13;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1239, 45);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.palBody2);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 73);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1239, 348);
            this.ultraPanel1.TabIndex = 47;
            // 
            // palBody2
            // 
            // 
            // palBody2.ClientArea
            // 
            this.palBody2.ClientArea.Controls.Add(this.uGridDanhSach);
            this.palBody2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palBody2.Location = new System.Drawing.Point(0, 0);
            this.palBody2.Name = "palBody2";
            this.palBody2.Size = new System.Drawing.Size(1239, 348);
            this.palBody2.TabIndex = 45;
            // 
            // uGridDanhSach
            // 
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDanhSach.DisplayLayout.Appearance = appearance14;
            this.uGridDanhSach.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDanhSach.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDanhSach.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDanhSach.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDanhSach.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridDanhSach.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDanhSach.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridDanhSach.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDanhSach.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDanhSach.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDanhSach.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridDanhSach.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDanhSach.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDanhSach.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDanhSach.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridDanhSach.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDanhSach.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDanhSach.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridDanhSach.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridDanhSach.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDanhSach.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridDanhSach.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridDanhSach.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDanhSach.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridDanhSach.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDanhSach.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDanhSach.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDanhSach.Location = new System.Drawing.Point(0, 0);
            this.uGridDanhSach.Name = "uGridDanhSach";
            this.uGridDanhSach.Size = new System.Drawing.Size(1239, 348);
            this.uGridDanhSach.TabIndex = 39;
            // 
            // FGForSetOff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 466);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.palFooter);
            this.Controls.Add(this.pafHeader);
            this.Name = "FGForSetOff";
            this.Text = "Chọn chứng từ";
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).EndInit();
            this.pafHeader.ClientArea.ResumeLayout(false);
            this.pafHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbInfomation)).EndInit();
            this.grbInfomation.ResumeLayout(false);
            this.grbInfomation.PerformLayout();
            this.palFooter.ClientArea.ResumeLayout(false);
            this.palFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.palBody2.ClientArea.ResumeLayout(false);
            this.palBody2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblTypeVoucher;
        private Infragistics.Win.Misc.UltraLabel lblDateTime;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFrom;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.Misc.UltraButton btnOk;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraPanel pafHeader;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel palFooter;
        private Infragistics.Win.Misc.UltraGroupBox grbInfomation;
        private Infragistics.Win.Misc.UltraPanel palBody2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDanhSach;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtRateCurrency;
    }
}