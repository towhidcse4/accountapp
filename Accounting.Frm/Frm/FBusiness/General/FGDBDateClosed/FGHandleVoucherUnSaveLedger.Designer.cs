﻿namespace Accounting
{
    partial class FGHandleVoucherUnSaveLedger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGHandleVoucherUnSaveLedger));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteNewPostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnTranferPostedDate = new Infragistics.Win.Misc.UltraButton();
            this.btnPost = new Infragistics.Win.Misc.UltraButton();
            this.grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.muPost = new System.Windows.Forms.ToolStripMenuItem();
            this.muTranferPostedDate = new System.Windows.Forms.ToolStripMenuItem();
            this.muDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.muSelectALl = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblDelete = new Infragistics.Win.Misc.UltraLabel();
            this.lblTransferPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblPost = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.gridErrors = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.muEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.lblErrorTitle = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.lblDBCloseDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblResult3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblResult2 = new Infragistics.Win.Misc.UltraLabel();
            this.lblResult1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.tabsMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnBack = new Infragistics.Win.Misc.UltraButton();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.BtnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteNewPostedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridErrors)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabsMain)).BeginInit();
            this.tabsMain.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Controls.Add(this.grid);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(835, 401);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox2.Controls.Add(this.dteNewPostedDate);
            this.ultraGroupBox2.Location = new System.Drawing.Point(674, 184);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(148, 53);
            this.ultraGroupBox2.TabIndex = 3;
            this.ultraGroupBox2.Text = "Ngày hạch toán mới";
            // 
            // dteNewPostedDate
            // 
            this.dteNewPostedDate.AutoSize = false;
            this.dteNewPostedDate.Location = new System.Drawing.Point(14, 21);
            this.dteNewPostedDate.Name = "dteNewPostedDate";
            this.dteNewPostedDate.Size = new System.Drawing.Size(121, 22);
            this.dteNewPostedDate.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.btnDelete);
            this.ultraGroupBox1.Controls.Add(this.btnTranferPostedDate);
            this.ultraGroupBox1.Controls.Add(this.btnPost);
            this.ultraGroupBox1.Location = new System.Drawing.Point(674, 43);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(148, 134);
            this.ultraGroupBox1.TabIndex = 2;
            this.ultraGroupBox1.Text = "Chọn cách xử lý";
            // 
            // btnDelete
            // 
            appearance1.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            appearance1.TextHAlignAsString = "Center";
            this.btnDelete.Appearance = appearance1;
            this.btnDelete.Location = new System.Drawing.Point(13, 93);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(122, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnTranferPostedDate
            // 
            appearance2.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            appearance2.TextHAlignAsString = "Center";
            this.btnTranferPostedDate.Appearance = appearance2;
            this.btnTranferPostedDate.Location = new System.Drawing.Point(13, 57);
            this.btnTranferPostedDate.Name = "btnTranferPostedDate";
            this.btnTranferPostedDate.Size = new System.Drawing.Size(122, 30);
            this.btnTranferPostedDate.TabIndex = 1;
            this.btnTranferPostedDate.Text = "Chuyển ngày HT";
            this.btnTranferPostedDate.Click += new System.EventHandler(this.btnTranferPostedDate_Click);
            // 
            // btnPost
            // 
            appearance3.Image = global::Accounting.Properties.Resources.ubtnPost;
            appearance3.TextHAlignAsString = "Center";
            this.btnPost.Appearance = appearance3;
            this.btnPost.Location = new System.Drawing.Point(14, 21);
            this.btnPost.Name = "btnPost";
            this.btnPost.Size = new System.Drawing.Size(121, 30);
            this.btnPost.TabIndex = 0;
            this.btnPost.Text = "Ghi sổ";
            this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // grid
            // 
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.ContextMenuStrip = this.contextMenuStrip1;
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.grid.DisplayLayout.Appearance = appearance4;
            this.grid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.grid.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grid.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.grid.DisplayLayout.MaxColScrollRegions = 1;
            this.grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grid.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.grid.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.grid.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grid.DisplayLayout.Override.CellAppearance = appearance11;
            this.grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.grid.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.grid.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.grid.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.grid.DisplayLayout.Override.RowAppearance = appearance14;
            this.grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.grid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.grid.Location = new System.Drawing.Point(11, 43);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(648, 351);
            this.grid.TabIndex = 1;
            this.grid.Text = "ultraGrid1";
            this.grid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.grid_AfterSelectChange);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.muPost,
            this.muTranferPostedDate,
            this.muDelete,
            this.muSelectALl});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(178, 92);
            // 
            // muPost
            // 
            this.muPost.Image = global::Accounting.Properties.Resources.ubtnPost;
            this.muPost.Name = "muPost";
            this.muPost.Size = new System.Drawing.Size(177, 22);
            this.muPost.Text = "Ghi sổ";
            this.muPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // muTranferPostedDate
            // 
            this.muTranferPostedDate.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.muTranferPostedDate.Name = "muTranferPostedDate";
            this.muTranferPostedDate.Size = new System.Drawing.Size(177, 22);
            this.muTranferPostedDate.Text = "Chuyển ngày HT";
            this.muTranferPostedDate.Click += new System.EventHandler(this.btnTranferPostedDate_Click);
            // 
            // muDelete
            // 
            this.muDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.muDelete.Name = "muDelete";
            this.muDelete.Size = new System.Drawing.Size(177, 22);
            this.muDelete.Text = "Xóa";
            this.muDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // muSelectALl
            // 
            this.muSelectALl.Name = "muSelectALl";
            this.muSelectALl.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.muSelectALl.Size = new System.Drawing.Size(177, 22);
            this.muSelectALl.Text = "Chọn tất cả";
            this.muSelectALl.Click += new System.EventHandler(this.muSelectALl_Click);
            // 
            // ultraLabel1
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance16;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel1.Location = new System.Drawing.Point(11, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(378, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Bước 1: Chọn cách xử lý các chứng từ chưa ghi sổ";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel4);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel3);
            this.ultraTabPageControl2.Controls.Add(this.lblDelete);
            this.ultraTabPageControl2.Controls.Add(this.lblTransferPostedDate);
            this.ultraTabPageControl2.Controls.Add(this.lblPost);
            this.ultraTabPageControl2.Controls.Add(this.ultraLabel2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(835, 401);
            // 
            // ultraLabel6
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance17;
            this.ultraLabel6.Location = new System.Drawing.Point(11, 132);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(156, 22);
            this.ultraLabel6.TabIndex = 8;
            this.ultraLabel6.Text = "Chứng từ sẽ được xóa";
            // 
            // ultraLabel5
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance18;
            this.ultraLabel5.Location = new System.Drawing.Point(11, 98);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(213, 22);
            this.ultraLabel5.TabIndex = 7;
            this.ultraLabel5.Text = "Chứng từ sẽ được chuyển ngày hạch toán";
            // 
            // ultraLabel4
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance19;
            this.ultraLabel4.Location = new System.Drawing.Point(11, 64);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(156, 22);
            this.ultraLabel4.TabIndex = 6;
            this.ultraLabel4.Text = "Chứng từ sẽ được ghi sổ";
            // 
            // ultraLabel3
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance20;
            this.ultraLabel3.Location = new System.Drawing.Point(11, 212);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(279, 23);
            this.ultraLabel3.TabIndex = 5;
            this.ultraLabel3.Text = "Nhấn nút \" Thực hiện \" để thực hiện các việc trên";
            // 
            // lblDelete
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            this.lblDelete.Appearance = appearance21;
            this.lblDelete.AutoSize = true;
            this.lblDelete.Location = new System.Drawing.Point(230, 136);
            this.lblDelete.Name = "lblDelete";
            this.lblDelete.Size = new System.Drawing.Size(60, 14);
            this.lblDelete.TabIndex = 4;
            this.lblDelete.Text = "ultraLabel3";
            // 
            // lblTransferPostedDate
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            this.lblTransferPostedDate.Appearance = appearance22;
            this.lblTransferPostedDate.AutoSize = true;
            this.lblTransferPostedDate.Location = new System.Drawing.Point(230, 102);
            this.lblTransferPostedDate.Name = "lblTransferPostedDate";
            this.lblTransferPostedDate.Size = new System.Drawing.Size(60, 14);
            this.lblTransferPostedDate.TabIndex = 3;
            this.lblTransferPostedDate.Text = "ultraLabel3";
            // 
            // lblPost
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.lblPost.Appearance = appearance23;
            this.lblPost.AutoSize = true;
            this.lblPost.Location = new System.Drawing.Point(230, 68);
            this.lblPost.Name = "lblPost";
            this.lblPost.Size = new System.Drawing.Size(60, 14);
            this.lblPost.TabIndex = 2;
            this.lblPost.Text = "ultraLabel3";
            // 
            // ultraLabel2
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance24;
            this.ultraLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel2.Location = new System.Drawing.Point(11, 13);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(481, 23);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Bước 2: Xem xét việc lựa chọn cách xử lý với các chứng từ chưa ghi sổ";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.btnEdit);
            this.ultraTabPageControl3.Controls.Add(this.gridErrors);
            this.ultraTabPageControl3.Controls.Add(this.lblErrorTitle);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(835, 401);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            appearance25.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance25;
            this.btnEdit.Location = new System.Drawing.Point(11, 365);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(105, 26);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "Sửa chứng từ";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // gridErrors
            // 
            this.gridErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridErrors.ContextMenuStrip = this.contextMenuStrip2;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridErrors.DisplayLayout.Appearance = appearance26;
            this.gridErrors.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.gridErrors.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridErrors.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.gridErrors.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridErrors.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.gridErrors.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridErrors.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.gridErrors.DisplayLayout.MaxColScrollRegions = 1;
            this.gridErrors.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridErrors.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridErrors.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.gridErrors.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridErrors.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.gridErrors.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            appearance33.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridErrors.DisplayLayout.Override.CellAppearance = appearance33;
            this.gridErrors.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridErrors.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.gridErrors.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance35.TextHAlignAsString = "Left";
            this.gridErrors.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.gridErrors.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.gridErrors.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.gridErrors.DisplayLayout.Override.RowAppearance = appearance36;
            this.gridErrors.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridErrors.DisplayLayout.Override.TemplateAddRowAppearance = appearance37;
            this.gridErrors.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.gridErrors.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.gridErrors.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.gridErrors.Location = new System.Drawing.Point(11, 43);
            this.gridErrors.Name = "gridErrors";
            this.gridErrors.Size = new System.Drawing.Size(815, 316);
            this.gridErrors.TabIndex = 3;
            this.gridErrors.Text = "ultraGrid1";
            this.gridErrors.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.gridErrors_DoubleClickRow);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.muEdit});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(145, 26);
            // 
            // muEdit
            // 
            this.muEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.muEdit.Name = "muEdit";
            this.muEdit.Size = new System.Drawing.Size(144, 22);
            this.muEdit.Text = "Sửa chứng từ";
            this.muEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblErrorTitle
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.lblErrorTitle.Appearance = appearance38;
            this.lblErrorTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorTitle.Location = new System.Drawing.Point(11, 13);
            this.lblErrorTitle.Name = "lblErrorTitle";
            this.lblErrorTitle.Size = new System.Drawing.Size(481, 23);
            this.lblErrorTitle.TabIndex = 2;
            this.lblErrorTitle.Text = "Xử lý chứng từ ghi sổ không thành công";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.lblDBCloseDate);
            this.ultraTabPageControl4.Controls.Add(this.lblResult3);
            this.ultraTabPageControl4.Controls.Add(this.lblResult2);
            this.ultraTabPageControl4.Controls.Add(this.lblResult1);
            this.ultraTabPageControl4.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(835, 401);
            // 
            // lblDBCloseDate
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            this.lblDBCloseDate.Appearance = appearance39;
            this.lblDBCloseDate.AutoSize = true;
            this.lblDBCloseDate.Location = new System.Drawing.Point(11, 158);
            this.lblDBCloseDate.Name = "lblDBCloseDate";
            this.lblDBCloseDate.Size = new System.Drawing.Size(60, 14);
            this.lblDBCloseDate.TabIndex = 8;
            this.lblDBCloseDate.Text = "ultraLabel3";
            // 
            // lblResult3
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            this.lblResult3.Appearance = appearance40;
            this.lblResult3.AutoSize = true;
            this.lblResult3.Location = new System.Drawing.Point(11, 129);
            this.lblResult3.Name = "lblResult3";
            this.lblResult3.Size = new System.Drawing.Size(60, 14);
            this.lblResult3.TabIndex = 7;
            this.lblResult3.Text = "ultraLabel3";
            // 
            // lblResult2
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            this.lblResult2.Appearance = appearance41;
            this.lblResult2.AutoSize = true;
            this.lblResult2.Location = new System.Drawing.Point(11, 100);
            this.lblResult2.Name = "lblResult2";
            this.lblResult2.Size = new System.Drawing.Size(60, 14);
            this.lblResult2.TabIndex = 6;
            this.lblResult2.Text = "ultraLabel3";
            // 
            // lblResult1
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            this.lblResult1.Appearance = appearance42;
            this.lblResult1.AutoSize = true;
            this.lblResult1.Location = new System.Drawing.Point(11, 71);
            this.lblResult1.Name = "lblResult1";
            this.lblResult1.Size = new System.Drawing.Size(60, 14);
            this.lblResult1.TabIndex = 5;
            this.lblResult1.Text = "ultraLabel3";
            // 
            // ultraLabel7
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance43;
            this.ultraLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel7.Location = new System.Drawing.Point(11, 13);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(481, 23);
            this.ultraLabel7.TabIndex = 3;
            this.ultraLabel7.Text = "Xử lý chứng từ chưa ghi sổ và khóa sổ thành công";
            // 
            // tabsMain
            // 
            this.tabsMain.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabsMain.Controls.Add(this.ultraTabPageControl1);
            this.tabsMain.Controls.Add(this.ultraTabPageControl2);
            this.tabsMain.Controls.Add(this.ultraTabPageControl3);
            this.tabsMain.Controls.Add(this.ultraTabPageControl4);
            this.tabsMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabsMain.Location = new System.Drawing.Point(0, 0);
            this.tabsMain.Name = "tabsMain";
            this.tabsMain.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabsMain.Size = new System.Drawing.Size(839, 427);
            this.tabsMain.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "";
            this.tabsMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.tabsMain.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.tabsMain_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(835, 401);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnBack);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnNext);
            this.ultraPanel1.ClientArea.Controls.Add(this.BtnCancel);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 427);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(839, 45);
            this.ultraPanel1.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance44.Image = global::Accounting.Properties.Resources.ubtnBack;
            this.btnBack.Appearance = appearance44;
            this.btnBack.Location = new System.Drawing.Point(557, 11);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(86, 26);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Quay lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance45.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance45.ImageHAlign = Infragistics.Win.HAlign.Right;
            appearance45.TextHAlignAsString = "Center";
            this.btnNext.Appearance = appearance45;
            this.btnNext.Location = new System.Drawing.Point(649, 11);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(86, 26);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Tiếp theo";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance46.Image = global::Accounting.Properties.Resources.cancel_16;
            this.BtnCancel.Appearance = appearance46;
            this.BtnCancel.Location = new System.Drawing.Point(741, 11);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(86, 26);
            this.BtnCancel.TabIndex = 4;
            this.BtnCancel.Text = "Hủy bỏ";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // FGHandleVoucherUnSaveLedger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 472);
            this.Controls.Add(this.tabsMain);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGHandleVoucherUnSaveLedger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xử lý các chứng từ chưa ghi sổ";
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteNewPostedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridErrors)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabsMain)).EndInit();
            this.tabsMain.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabsMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid grid;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnTranferPostedDate;
        private Infragistics.Win.Misc.UltraButton btnPost;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNewPostedDate;
        private Infragistics.Win.Misc.UltraButton btnBack;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton BtnCancel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem muPost;
        private System.Windows.Forms.ToolStripMenuItem muTranferPostedDate;
        private System.Windows.Forms.ToolStripMenuItem muDelete;
        private System.Windows.Forms.ToolStripMenuItem muSelectALl;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel lblDelete;
        private Infragistics.Win.Misc.UltraLabel lblTransferPostedDate;
        private Infragistics.Win.Misc.UltraLabel lblPost;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lblErrorTitle;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraLabel lblDBCloseDate;
        private Infragistics.Win.Misc.UltraLabel lblResult3;
        private Infragistics.Win.Misc.UltraLabel lblResult2;
        private Infragistics.Win.Misc.UltraLabel lblResult1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridErrors;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem muEdit;
    }
}