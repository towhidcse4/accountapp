﻿namespace Accounting
{
    partial class FGDisposeDbDateClosed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGDisposeDbDateClosed));
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblOldDBDateClosed = new Infragistics.Win.Misc.UltraLabel();
            this.dteNewDBDateClosed = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnAccept = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.dteNewDBDateClosed)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel2
            // 
            appearance1.Image = global::Accounting.Properties.Resources.Unlock;
            this.ultraLabel2.Appearance = appearance1;
            this.ultraLabel2.ImageSize = new System.Drawing.Size(32, 32);
            this.ultraLabel2.Location = new System.Drawing.Point(12, 16);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(32, 32);
            this.ultraLabel2.TabIndex = 1;
            // 
            // ultraLabel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(64, 7);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(125, 22);
            this.ultraLabel1.TabIndex = 2;
            this.ultraLabel1.Text = "Ngày khóa sổ hiện thời :";
            // 
            // ultraLabel3
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(64, 35);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(125, 22);
            this.ultraLabel3.TabIndex = 3;
            this.ultraLabel3.Text = "Đặt ngày khóa sổ về";
            // 
            // lblOldDBDateClosed
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblOldDBDateClosed.Appearance = appearance4;
            this.lblOldDBDateClosed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOldDBDateClosed.Location = new System.Drawing.Point(195, 7);
            this.lblOldDBDateClosed.Name = "lblOldDBDateClosed";
            this.lblOldDBDateClosed.Size = new System.Drawing.Size(99, 22);
            this.lblOldDBDateClosed.TabIndex = 4;
            // 
            // dteNewDBDateClosed
            // 
            this.dteNewDBDateClosed.AutoSize = false;
            this.dteNewDBDateClosed.Location = new System.Drawing.Point(195, 36);
            this.dteNewDBDateClosed.Name = "dteNewDBDateClosed";
            this.dteNewDBDateClosed.Size = new System.Drawing.Size(112, 22);
            this.dteNewDBDateClosed.TabIndex = 5;
            // 
            // btnAccept
            // 
            appearance5.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnAccept.Appearance = appearance5;
            this.btnAccept.Location = new System.Drawing.Point(137, 71);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(82, 31);
            this.btnAccept.TabIndex = 7;
            this.btnAccept.Text = "Đồng ý";
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnClose
            // 
            appearance6.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance6;
            this.btnClose.Location = new System.Drawing.Point(225, 71);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 31);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FGDisposeDbDateClosed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 108);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dteNewDBDateClosed);
            this.Controls.Add(this.lblOldDBDateClosed);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.ultraLabel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGDisposeDbDateClosed";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bỏ khóa sổ";
            ((System.ComponentModel.ISupportInitialize)(this.dteNewDBDateClosed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel lblOldDBDateClosed;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNewDBDateClosed;
        private Infragistics.Win.Misc.UltraButton btnAccept;
        private Infragistics.Win.Misc.UltraButton btnClose;
    }
}