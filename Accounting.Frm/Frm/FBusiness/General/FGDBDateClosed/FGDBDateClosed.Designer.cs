﻿namespace Accounting
{
    partial class FGDBDateClosed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGDBDateClosed));
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteDBDateClosed = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblDBDateClosedOld = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnAccept = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDBDateClosed)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.dteDBDateClosed);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 49);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(387, 153);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thông tin khóa sổ mới";
            // 
            // dteDBDateClosed
            // 
            this.dteDBDateClosed.AutoSize = false;
            this.dteDBDateClosed.DateTime = new System.DateTime(2015, 6, 22, 0, 0, 0, 0);
            this.dteDBDateClosed.Location = new System.Drawing.Point(204, 35);
            this.dteDBDateClosed.MaskInput = "dd/mm/yyyy";
            this.dteDBDateClosed.Name = "dteDBDateClosed";
            this.dteDBDateClosed.Size = new System.Drawing.Size(97, 22);
            this.dteDBDateClosed.TabIndex = 5;
            this.dteDBDateClosed.Value = new System.DateTime(2015, 6, 22, 0, 0, 0, 0);
            // 
            // ultraLabel5
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance1;
            this.ultraLabel5.Location = new System.Drawing.Point(72, 68);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(309, 74);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Sau khi khóa sổ, bạn sẽ không thể sửa đổi các chứng từ được hạch toán kể từ ngày " +
    "khóa sổ mới trở về trước. Muốn thực hiện sửa đổi bạn phải chạy chức năng \"Bỏ khó" +
    "a sổ kỳ kế toán\".";
            // 
            // ultraLabel4
            // 
            appearance2.Image = global::Accounting.Properties.Resources.warning;
            this.ultraLabel4.Appearance = appearance2;
            this.ultraLabel4.ImageSize = new System.Drawing.Size(32, 32);
            this.ultraLabel4.Location = new System.Drawing.Point(20, 89);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(32, 32);
            this.ultraLabel4.TabIndex = 3;
            // 
            // ultraLabel3
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(72, 35);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(125, 23);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Chọn ngày khóa sổ mới";
            // 
            // ultraLabel2
            // 
            appearance4.Image = global::Accounting.Properties.Resources.Lock;
            this.ultraLabel2.Appearance = appearance4;
            this.ultraLabel2.ImageSize = new System.Drawing.Size(32, 32);
            this.ultraLabel2.Location = new System.Drawing.Point(20, 30);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(32, 32);
            this.ultraLabel2.TabIndex = 0;
            // 
            // ultraLabel1
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance5;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "Ngày khóa sổ cũ:";
            // 
            // lblDBDateClosedOld
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblDBDateClosedOld.Appearance = appearance6;
            this.lblDBDateClosedOld.Location = new System.Drawing.Point(118, 12);
            this.lblDBDateClosedOld.Name = "lblDBDateClosedOld";
            this.lblDBDateClosedOld.Size = new System.Drawing.Size(100, 23);
            this.lblDBDateClosedOld.TabIndex = 2;
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(317, 208);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 31);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAccept
            // 
            appearance8.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnAccept.Appearance = appearance8;
            this.btnAccept.Location = new System.Drawing.Point(229, 208);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(82, 31);
            this.btnAccept.TabIndex = 4;
            this.btnAccept.Text = "Đồng ý";
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // FGDBDateClosed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 246);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblDBDateClosedOld);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGDBDateClosed";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Khóa sổ kỳ kế toán";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDBDateClosed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblDBDateClosedOld;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDBDateClosed;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnAccept;
    }
}