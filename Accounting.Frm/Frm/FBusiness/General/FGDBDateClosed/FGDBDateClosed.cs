﻿using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FGDBDateClosed : DialogForm
    {
        public FGDBDateClosed()
        {
            InitializeComponent();
            ConfigControl();
        }

        private void ConfigControl()
        {
            lblDBDateClosedOld.Text = Utils.GetDBDateClosed();
            var oldDBDateClosed = Utils.GetDBDateClosed().CloneObject().StringToDateTime();
            dteDBDateClosed.Value = oldDBDateClosed.HasValue ? oldDBDateClosed.Value.AddDays(1) : Utils.GetDbStartDate().StringToDateTime();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            var lstUnrecorded = Utils.IViewVouchersCloseBookService.GetAllVoucherUnRecorded(dteDBDateClosed.DateTime);
            Utils.IViewVouchersCloseBookService.UnbindSession(lstUnrecorded);
            lstUnrecorded = Utils.IViewVouchersCloseBookService.GetAllVoucherUnRecorded(dteDBDateClosed.DateTime).Where(n=>n.TypeID != 310 && n.TypeID != 200).ToList();
            var source = (from u in lstUnrecorded
                          group u by u.ID into g
                          select new Accounting.Core.Domain.VoucherUnSaveLedger
                          {
                              ID = g.Key,
                              TypeID = g.FirstOrDefault().TypeID,
                              TypeName = g.FirstOrDefault().TypeName,
                              No = g.FirstOrDefault().No,
                              Date = g.FirstOrDefault().Date,
                              PostedDate = g.FirstOrDefault().PostedDate,
                              TotalAmount = g.FirstOrDefault().TotalAmount,
                              TotalAmountOriginal = g.FirstOrDefault().TotalAmountOriginal,
                              RefTable = g.FirstOrDefault().RefTable
                          }).ToList();
            WaitingFrm.StopWaiting();
            if (source.Count > 0)
            {
                if (MSG.MessageBoxStand(string.Format(resSystem.MSG_FGDBDateClosed, source.Count), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (new FGHandleVoucherUnSaveLedger(source, dteDBDateClosed.DateTime).ShowDialog(this) != DialogResult.OK)
                        return;
                    goto Exec;
                }
                return;
            }
        Exec:
            WaitingFrm.StartWaiting();
            Utils.SetDBDateClosedOld(Utils.GetDBDateClosed().CloneObject());
            Utils.SetDBDateClosed(dteDBDateClosed.DateTime.ToString("dd/MM/yyyy"));
            Logs.Save(0, resSystem.Logs_Title_02, Logs.Action.LedgerClosed, string.Format(resSystem.Logs_Reference_00, Utils.GetDBDateClosed()), (Guid?)null, string.Format(resSystem.Logs_Description_00, Utils.GetDBDateClosedOld(), Utils.GetDBDateClosed()));
            WaitingFrm.StopWaiting();
            MSG.Information("Khóa sổ thành công");
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
