﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using FX.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core;

namespace Accounting
{
    public partial class FGHandleVoucherUnSaveLedger : DialogForm
    {
        public FGHandleVoucherUnSaveLedger(List<VoucherUnSaveLedger> input, DateTime newDBDateClosed)
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            ConfigControl(input, newDBDateClosed);
            WaitingFrm.StopWaiting();
        }
        #region Utils
        private void ConfigControl(List<VoucherUnSaveLedger> input, DateTime newDBDateClosed)
        {
            tabsMain.ConfigTabControlWizard();
            dteNewPostedDate.Value = newDBDateClosed.AddDays(1);
            grid.DataSource = input;
            if (grid.DisplayLayout.Bands[0].Columns.Exists("Method"))
            {
                var combo = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
                combo.Items.Add(0, "Ghi sổ");
                combo.Items.Add(1, "Chuyển ngày hạch toán");
                combo.Items.Add(2, "Xóa");
                grid.DisplayLayout.Bands[0].Columns["Method"].EditorComponent = combo;
                grid.DisplayLayout.Bands[0].Columns["Method"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
            }
            grid.ConfigGrid(ConstDatabase.VoucherUnSaveLedger_KeyName, new List<TemplateColumn>(), false, true, setReadOnlyColor: false);
            grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            grid.ActiveRow = grid.Rows[0];
            grid.Rows[0].Selected = true;
            gridErrors.DataSource = new List<VoucherUnSaveLedger>();
            gridErrors.ConfigGrid(ConstDatabase.VoucherSaveLedgerErrors_KeyName, true, setReadOnlyColor: false);
            foreach (var column in gridErrors.DisplayLayout.Bands[0].Columns)
            {
                column.ConfigColumnByNumberic();
            }
            lblDBCloseDate.Text = string.Format("Đã khóa sổ vào ngày {0}", newDBDateClosed.ToString("dd/MM/yyyy"));
        }

        private List<VoucherUnSaveLedger> ExecuteMethod(List<VoucherUnSaveLedger> list)
        {
            WaitingFrm.StartWaiting();
            var errors = new List<VoucherUnSaveLedger>();
            foreach (var item in list)
            {
                //MCPayment
                if (item.RefTable.Equals("MCPayment"))
                {
                    Run<MCPayment>(item, ref errors);
                }
                //MBDeposit
                else if (item.RefTable.Equals("MBDeposit"))
                {
                    Run<MBDeposit>(item, ref errors);
                }
                //MBInternalTransfer
                else if (item.RefTable.Equals("MBInternalTransfer"))
                {
                    Run<MBInternalTransfer>(item, ref errors);
                }
                //MBTellerPaper
                else if (item.RefTable.Equals("MBTellerPaper"))
                {
                    Run<MBTellerPaper>(item, ref errors);
                }
                //MBCreditCard
                else if (item.RefTable.Equals("MBCreditCard"))
                {
                    Run<MBCreditCard>(item, ref errors);
                }
                //MCReceipt
                else if (item.RefTable.Equals("MCReceipt"))
                {
                    Run<MCReceipt>(item, ref errors);
                }
                //FAAdjustment
                else if (item.RefTable.Equals("FAAdjustment"))
                {
                    Run<FAAdjustment>(item, ref errors);
                }
                //FADepreciation
                else if (item.RefTable.Equals("FADepreciation"))
                {
                    Run<FADepreciation>(item, ref errors);
                }
                //FAIncrement
                else if (item.RefTable.Equals("FAIncrement"))
                {
                    Run<FAIncrement>(item, ref errors);
                }
                //FADecrement
                else if (item.RefTable.Equals("FADecrement"))
                {
                    Run<FADecrement>(item, ref errors);
                }
                //GOtherVoucher
                else if (item.RefTable.Equals("GOtherVoucher"))
                {
                    Run<GOtherVoucher>(item, ref errors);
                }
                //RSInwardOutward
                else if (item.RefTable.Equals("RSInwardOutward"))
                {
                    Run<RSInwardOutward>(item, ref errors);
                }
                //RSTransfer
                else if (item.RefTable.Equals("RSTransfer"))
                {
                    Run<RSTransfer>(item, ref errors);
                }
                //PPInvoice
                else if (item.RefTable.Equals("PPInvoice"))
                {
                    Run<PPInvoice>(item, ref errors);
                }
                //PPDiscountReturn
                else if (item.RefTable.Equals("PPDiscountReturn"))
                {
                    Run<PPDiscountReturn>(item, ref errors);
                }
                //PPService
                else if (item.RefTable.Equals("PPService"))
                {
                    Run<PPService>(item, ref errors);
                }
                //SAInvoice
                else if (item.RefTable.Equals("SAInvoice"))
                {
                    Run<SAInvoice>(item, ref errors);
                }
                //SAReturn
                else if (item.RefTable.Equals("SAReturn"))
                {
                    Run<SAReturn>(item, ref errors);
                }
            }
            WaitingFrm.StopWaiting();
            return errors;
        }
        private void Run<T>(VoucherUnSaveLedger input, ref List<VoucherUnSaveLedger> errors)
        {
            BaseService<T, Guid> services = this.GetIService<T>();
            switch (input.Method)
            {
                case 0:
                    {
                        var temp = services.Getbykey(input.ID);
                        object tempJoin = Utils.GetSelectJoin<T>(input.TypeID, input.ID);
                        string msg = "";
                        var isSuccess = temp.Saveleged(tempJoin, input.TypeID, ref msg, isMess: false);
                        if (!isSuccess)
                        {
                            input.Cause = resSystem.MSG_System_69;
                            errors.Add(input);
                        }
                    }
                    break;
                case 1:
                    {
                        var temp = services.Getbykey(input.ID);
                        if (temp.HasProperty("PostedDate"))
                        {
                            temp.SetProperty("PostedDate", dteNewPostedDate.DateTime);
                            try
                            {
                                services.BeginTran();
                                services.Update(temp);
                                services.CommitTran();
                            }
                            catch (Exception)
                            {
                                services.RolbackTran();
                            }
                        }
                    }
                    break;
                case 2:
                    {
                        try
                        {
                            services.BeginTran();
                            services.Delete(input.ID);
                            services.CommitTran();
                        }
                        catch (Exception)
                        {
                            services.RolbackTran();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void EditFunction(VoucherUnSaveLedger input)
        {
            int status = ConstFrm.optStatusForm.View;
            //MCPayment
            if (input.RefTable.Equals("MCPayment"))
            {
                ShowFrm<MCPayment>(input, status, -1);
            }
            //MBDeposit
            else if (input.RefTable.Equals("MBDeposit"))
            {
                ShowFrm<MBDeposit>(input, status, -1);
            }
            //MBInternalTransfer
            else if (input.RefTable.Equals("MBInternalTransfer"))
            {
                ShowFrm<MBInternalTransfer>(input, status, -1);
            }
            //MBTellerPaper
            else if (input.RefTable.Equals("MBTellerPaper"))
            {
                ShowFrm<MBTellerPaper>(input, status, -1);
            }
            //MBCreditCard
            else if (input.RefTable.Equals("MBCreditCard"))
            {
                ShowFrm<MBCreditCard>(input, status, -1);
            }
            //MCReceipt
            else if (input.RefTable.Equals("MCReceipt"))
            {
                ShowFrm<MCReceipt>(input, status, -1);
            }
            //FAAdjustment
            else if (input.RefTable.Equals("FAAdjustment"))
            {
                ShowFrm<FAAdjustment>(input, status, -1);
            }
            //FADepreciation
            else if (input.RefTable.Equals("FADepreciation"))
            {
                ShowFrm<FADepreciation>(input, status, -1);
            }
            //FAIncrement
            else if (input.RefTable.Equals("FAIncrement"))
            {
                ShowFrm<FAIncrement>(input, status, -1);
            }
            //FADecrement
            else if (input.RefTable.Equals("FADecrement"))
            {
                ShowFrm<FADecrement>(input, status, -1);
            }
            //GOtherVoucher
            else if (input.RefTable.Equals("GOtherVoucher"))
            {
                ShowFrm<GOtherVoucher>(input, status, -1);
            }
            //RSInwardOutward
            else if (input.RefTable.Equals("RSInwardOutward"))
            {
                ShowFrm<RSInwardOutward>(input, status, -1);
            }
            //RSTransfer
            else if (input.RefTable.Equals("RSTransfer"))
            {
                ShowFrm<RSTransfer>(input, status, -1);
            }
            //PPInvoice
            else if (input.RefTable.Equals("PPInvoice"))
            {
                ShowFrm<PPInvoice>(input, status, -1);
            }
            //PPDiscountReturn
            else if (input.RefTable.Equals("PPDiscountReturn"))
            {
                ShowFrm<PPDiscountReturn>(input, status, -1);
            }
            //PPService
            else if (input.RefTable.Equals("PPService"))
            {
                ShowFrm<PPService>(input, status, -1);
            }
            //SAInvoice
            else if (input.RefTable.Equals("SAInvoice"))
            {
                ShowFrm<SAInvoice>(input, status, -1);
            }
            //SAReturn
            else if (input.RefTable.Equals("SAReturn"))
            {
                ShowFrm<SAReturn>(input, status, -1);
            }
        }
        void ShowFrm<T>(VoucherUnSaveLedger input, int status, int _loaiForm)
        {
            WaitingFrm.StartWaiting();
            BaseService<T, Guid> services = this.GetIService<T>();
            var temp = services.Getbykey(input.ID);
            if (temp is FAIncrement)
            {
                if (new List<int> { 500, 119, 129, 132, 142, 172 }.Contains(input.TypeID))
                    _loaiForm = -500;
                else if (input.TypeID == 510)
                    _loaiForm = -510;
            }
            else if (temp is PPInvoice)
            {
                _loaiForm = (temp as PPInvoice).StoredInRepository == true ? 0 : 1;
            }
            else if (temp is SAInvoice)
            {
                if (new List<int> { 321, 322 }.Contains(input.TypeID))
                    _loaiForm = 1;
                else if (input.TypeID == 320)
                    _loaiForm = 0;
                else if (new List<int> { 323, 324, 325 }.Contains(input.TypeID))
                    _loaiForm = 2;
            }
            else if (temp is RSInwardOutward)
            {
                if (new List<int> { 400, 401, 402, 403, 404 }.Contains(input.TypeID))
                    _loaiForm = -9998;
                else if (new List<int> { 410, 411, 412, 413, 414, 415 }.Contains(input.TypeID))
                    _loaiForm = -9999;
            }
            else if (temp is GOtherVoucher)
            {
                if (new List<int> { 600 }.Contains(input.TypeID))
                    _loaiForm = -600;
                else if (new List<int> { 620 }.Contains(input.TypeID))
                    _loaiForm = -620;
            }
            bool isFrmDetailOk = false;
            string strName = string.Empty;
            try
            {
                string strTemp;
                switch (_loaiForm)
                {
                    case -9999:
                        strTemp = "Output";
                        break;
                    case -500:  //H.A thêm -500 cho loại Form Mua và gia tăng TSCĐ
                        strTemp = "Buy";
                        break;
                    case -620:  //H.A thêm -620 cho loại Form Kết chuyển lãi, lỗ
                        strTemp = "ILTransfer";
                        break;
                    default:
                        strTemp = string.Empty;
                        break;
                }
                strName = string.Format("Accounting.F{0}{1}Detail", typeof(T).Name, strTemp);
                System.Type type = System.Type.GetType(strName);
                if (type == null) return;
                List<int> lstTypeFrmNotStand = new List<int> { -1, -500, -510, -600, -620, -9998, -9999 };  //danh sách những loại frm dùng hàm mặc định CreateInstance(type, temp, DsObject, status)
                var frm = lstTypeFrmNotStand.Contains(_loaiForm) ? (CustormForm)Activator.CreateInstance(type, temp, new List<T>(), status) : (CustormForm)Activator.CreateInstance(type, temp, new List<T>(), status, _loaiForm);
                frm.TopLevel = true;
                if (frm.HasProperty("IsSaveGeneralLedger"))
                    frm.SetProperty("IsSaveGeneralLedger", true);
                if (!frm.IsDisposed)
                    frm.ShowDialog(this);
                services.UnbindSession(temp);
                temp = services.Getbykey(input.ID);
                if (temp.GetProperty<T, bool>("Recorded") == true)
                {
                    foreach (var row in gridErrors.Rows)
                    {
                        var v = row.ListObject as VoucherUnSaveLedger;
                        if (v.ID == input.ID)
                            row.Delete(false);
                    }
                }
                isFrmDetailOk = true;
            }
            catch (Exception exception)
            {
                WaitingFrm.StopWaiting();
                MSG.Warning(isFrmDetailOk ? exception.StackTrace : string.Format("Vui lòng kiểm tra lại frm {0} \r\n Chi tiết lỗi: \r\n\r\n {2} \r\n\r\n {1}", strName.Replace("Accounting.", ""), exception.StackTrace, exception.Message));
            }
        }

        #endregion
        #region Event
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (tabsMain.ActiveTab.Index == 1)
            {
                var errors = ExecuteMethod(grid.DataSource as List<VoucherUnSaveLedger>);
                if (errors.Count == 0) tabsMain.Tabs[2].Visible = false;
                else
                {
                    gridErrors.DataSource = errors;
                    gridErrors.Rows[0].Selected = true;
                }
            }
            else if (tabsMain.ActiveTab.Index == 2)
            {
                //var errors = ExecuteMethod(grid.DataSource as List<VoucherUnSaveLedger>);
                //if (errors.Count > 0)
                //{
                //    gridErrors.DataSource = errors;
                //    gridErrors.Rows[0].Selected = true;
                //    tabsMain.Tabs[1].Selected = true;
                //    return;
                //}
                if (gridErrors.Rows.Count > 0)
                {
                    MSG.Warning("Bạn phải sửa chứng từ để có thể ghi sổ.");
                    return;
                }
            }
            else if (tabsMain.ActiveTab.Index == tabsMain.Tabs.Count - 1)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
            tabsMain.PerformAction(Infragistics.Win.UltraWinTabControl.UltraTabControlAction.SelectNextTab);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            tabsMain.PerformAction(Infragistics.Win.UltraWinTabControl.UltraTabControlAction.SelectPreviousTab);
        }

        private void tabsMain_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            if (tabsMain.ActiveTab.Visible == false)
            {
                tabsMain.PerformAction(Infragistics.Win.UltraWinTabControl.UltraTabControlAction.SelectNextTab);
                return;
            }
            //btnNext.Enabled = tabsMain.ActiveTab.Index != (tabsMain.Tabs.Count - 1);
            btnBack.Enabled = tabsMain.ActiveTab.Index == 1;
            btnNext.Text = tabsMain.ActiveTab.Index == 1 ? "Thực hiện" : (tabsMain.ActiveTab.Index == (tabsMain.Tabs.Count - 1) ? "Kết thúc" : "Tiếp theo");
            //btnOk.Enabled = tabControl.ActiveTab.Index == (tabsMain.Tabs.Count - 1);
            if (tabsMain.ActiveTab.Index == 1)
            {
                grid.UpdateDataGrid();
                var result = grid.DataSource as List<VoucherUnSaveLedger>;
                lblPost.Text = string.Format(": {0}", result.Where(r => r.Method == 0).Count());
                lblTransferPostedDate.Text = string.Format(": {0}", result.Where(r => r.Method == 1).Count());
                lblDelete.Text = string.Format(": {0}", result.Where(r => r.Method == 2).Count());
            }
            else if (tabsMain.ActiveTab.Index == 3)
            {
                var result = grid.DataSource as List<VoucherUnSaveLedger>;
                lblResult1.Text = string.Format("Đã ghi sổ {0} chứng từ", result.Where(r => r.Method == 0).Count());
                lblResult2.Text = string.Format("Đã chuyển ngày hạch toán {0} chứng từ", result.Where(r => r.Method == 1).Count());
                lblResult3.Text = string.Format("Đã xóa {0} chứng từ", result.Where(r => r.Method == 2).Count());
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void grid_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            btnPost.Enabled = btnTranferPostedDate.Enabled = btnDelete.Enabled = muPost.Enabled = muTranferPostedDate.Enabled = muDelete.Enabled = grid.Selected.Rows.Count > 0;
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            foreach (var row in grid.Selected.Rows)
            {
                row.Cells["Method"].Value = 0;
            }
        }

        private void btnTranferPostedDate_Click(object sender, EventArgs e)
        {
            foreach (var row in grid.Selected.Rows)
            {
                row.Cells["Method"].Value = 1;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (var row in grid.Selected.Rows)
            {
                row.Cells["Method"].Value = 2;
            }
        }

        private void muSelectALl_Click(object sender, EventArgs e)
        {
            foreach (var row in grid.Rows)
            {
                row.Selected = true;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridErrors.Selected.Rows.Count > 0)
            {
                EditFunction(gridErrors.Selected.Rows[0].ListObject as VoucherUnSaveLedger);
            }
        }

        private void gridErrors_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            btnEdit_Click(btnEdit, null);
        }
        #endregion

    }
}
