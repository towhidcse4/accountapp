﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FGDisposeDbDateClosed : DialogForm
    {
        public FGDisposeDbDateClosed()
        {
            InitializeComponent();
            var oldDBDateClosed = Utils.GetDBDateClosedOld().CloneObject().StringToDateTime();
            lblOldDBDateClosed.Text = Utils.GetDBDateClosed();
            dteNewDBDateClosed.Value = oldDBDateClosed.HasValue ? oldDBDateClosed.Value : Utils.GetDBDateClosed().StringToDateTime().Value.AddDays(-1);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (Utils.SetDBDateClosed(dteNewDBDateClosed.DateTime.ToString("dd/MM/yyyy")))
            {
                Accounting.TextMessage.MSG.Information("Bỏ khóa sổ thành công.");
                Logs.Save(0, Accounting.TextMessage.resSystem.Logs_Title_03, Logs.Action.LedgerOpened, string.Format(Accounting.TextMessage.resSystem.Logs_Reference_00, Utils.GetDBDateClosed()), (Guid?)null, string.Format(Accounting.TextMessage.resSystem.Logs_Description_00, lblOldDBDateClosed.Text, Utils.GetDBDateClosed()));
                DialogResult = DialogResult.OK;
                Close();
            }
            else
                Accounting.TextMessage.MSG.Warning("Bỏ khóa sổ không thành công. \r\nVui lòng kiểm tra lại.");
        }
    }
}
