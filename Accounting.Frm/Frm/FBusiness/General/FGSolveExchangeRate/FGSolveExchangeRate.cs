﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGSolveExchangeRate : CustormForm
    {
        private readonly  IAccountService _accountSrv = IoC.Resolve<IAccountService>();
        private readonly IGenCodeService _genCodeSrv = IoC.Resolve<IGenCodeService>();
        private readonly ICurrencyService _currencySrv = IoC.Resolve<ICurrencyService>();
        private readonly IAccountingObjectService _accountingObjectSrv = IoC.Resolve<IAccountingObjectService>();
        private readonly IContractStateService _contractStateSrv = IoC.Resolve<IContractStateService>();
        private readonly IStatisticsCodeService _statisticsCodeSrv = IoC.Resolve<IStatisticsCodeService>();
        public FGSolveExchangeRate()
        {
            InitializeComponent();
            // tk xử lý chênh lệch lãi
            cbbAccountCredit.DataSource = _accountSrv.GetChildrenAccount("515;413");
            cbbAccountCredit.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAccountCredit, ConstDatabase.Account_TableName);
            // tk xử lý chênh lệch lỗ
            cbbAccountDebit.DataSource = _accountSrv.GetChildrenAccount("635;413");
            cbbAccountDebit.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAccountDebit, ConstDatabase.Account_TableName);
            // ngày chứng từ, ngày hoạch toán
            dteDate.DateTime = dtePostedDate.DateTime = Utils.StringToDateTime(ConstFrm.DbStartDate)??DateTime.Now;
            // số chứng từ
            txtNo.Text = Utils.TaoMaChungTu(_genCodeSrv.getGenCode(ConstFrm.TypeGroup_GOtherVoucher));
            // danh sách chứng từ
            SolveExchangeRate solveExchange = new SolveExchangeRate();
            Utils.IAccountingObjectService.HandlingExchangeRate((DateTime)dteDate.Value, solveExchange);
            uGridIndex.DataSource = solveExchange;
            //ViewIndex(uGridIndex);
        }
        #region Cấu hình các Grid
        private void ViewIndex(UltraGrid ultraGrid)
        {
            Utils.ConfigGrid(ultraGrid, ConstDatabase.FGSolveExchangeRate_FormName);
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ultraGrid.Text = "";
            // xét style cho các cột có dạng checkbox
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Status"].CellActivation = Activation.AllowEdit;
            band.Columns["Status"].Header.Fixed = true;
            #region Colums Style
            foreach (var item in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Equals("Status"))
                    item.Width = 100;
                if (item.Key.Equals("CurrencyId"))
                    this.ConfigCbbToGrid(ultraGrid, item.Key, _currencySrv.GetIsActive(true), "ID", "ID", ConstDatabase.Currency_TableName);
                if (item.Key.Equals("AccountingObjectId"))
                    this.ConfigCbbToGrid(ultraGrid, item.Key, _accountingObjectSrv.GetAll(), "ID", "AccountingObjectCode", ConstDatabase.AccountingObject_TableName);
                if (item.Key.Equals("ContractStateId"))
                    this.ConfigCbbToGrid(ultraGrid, item.Key, _contractStateSrv.GetAll(), "ID", "ContractStateName", ConstDatabase.ContractState_TableName);
                if (item.Key.Equals("StatisticsCodeId"))
                    this.ConfigCbbToGrid(ultraGrid, item.Key, _statisticsCodeSrv.GetAll(), "ID", "StatisticsCodeName", ConstDatabase.StatisticsCode_TableName);
            }
            #endregion
            // xóa dòng xét tổng ở cuối Grid
            ultraGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            // thêm dòng tính tổng cột "Số tiền" vào cuối Grid
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Status"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        }
        #endregion
        #region Tạo dữ liệu test
        public List<SolveExchangeRateDetail> SolveChangeRateDatabaseTest()
        {
            return new List<SolveExchangeRateDetail>
                       {
                           new SolveExchangeRateDetail
                               {
                                   AccountNumber = "515",
                                   Date = DateTime.Now,
                                   VoucherNo = "NVK0001",
                                   InvoiceNo = "000007",
                                   AmountBanlace = 5000000,
                                   AmountDeficit = 0,
                                   AccountingObjectID = new Guid("C468982E-0051-46B4-AA81-0927996CFA13"),
                                   ContractStateId = 28,
                                   StatisticsCodeId = new Guid("BDF11AEF-6193-447E-A303-1CBEE72C9146")
                               },
                               new SolveExchangeRateDetail
                               {
                                   AccountNumber = "4131",
                                   Date = DateTime.Now,
                                   VoucherNo = "NVK0003",
                                   InvoiceNo = "000008",
                                   AmountBanlace = 0,
                                   AmountDeficit = 10000000,
                                   AccountingObjectID = new Guid("1F3D989A-897C-4239-96A7-207DDBBED1FA"),
                                   ContractStateId = 25,
                                   StatisticsCodeId = new Guid("5CFC063E-951D-44AA-9AFC-DFBD3CE6F94F")
                               }
                       };
        }
        #endregion
        public void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
