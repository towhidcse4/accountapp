﻿namespace Accounting
{
    partial class FGSolveExchangeRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGSolveExchangeRate));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.panelHeader = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountDebit = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDescription = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.cbbAccountCredit = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.lblD = new Infragistics.Win.Misc.UltraLabel();
            this.lblNo = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.panelFooter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.panelBody = new Infragistics.Win.Misc.UltraPanel();
            this.uGridIndex = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelHeader.ClientArea.SuspendLayout();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountDebit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            this.panelFooter.ClientArea.SuspendLayout();
            this.panelFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.panelBody.ClientArea.SuspendLayout();
            this.panelBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            // 
            // panelHeader.ClientArea
            // 
            this.panelHeader.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(946, 164);
            this.panelHeader.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox1.Controls.Add(this.lblNo);
            this.ultraGroupBox1.Controls.Add(this.dteDate);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.dtePostedDate);
            this.ultraGroupBox1.Controls.Add(this.lblPostedDate);
            this.ultraGroupBox1.Controls.Add(this.lblDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(946, 164);
            this.ultraGroupBox1.TabIndex = 34;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox2
            // 
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBox2.Appearance = appearance1;
            this.ultraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox2.Controls.Add(this.cbbAccountDebit);
            this.ultraGroupBox2.Controls.Add(this.txtDescription);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountCredit);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObject);
            this.ultraGroupBox2.Controls.Add(this.lblD);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 51);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(940, 110);
            this.ultraGroupBox2.TabIndex = 34;
            this.ultraGroupBox2.Text = "Thông tin chung";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountDebit
            // 
            this.cbbAccountDebit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountDebit.Location = new System.Drawing.Point(393, 19);
            this.cbbAccountDebit.Name = "cbbAccountDebit";
            this.cbbAccountDebit.NullText = "<chọn dữ liệu>";
            this.cbbAccountDebit.Size = new System.Drawing.Size(121, 22);
            this.cbbAccountDebit.TabIndex = 39;
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.FontData.BoldAsString = "False";
            appearance2.FontData.ItalicAsString = "False";
            appearance2.FontData.Name = "Microsoft Sans Serif";
            appearance2.FontData.SizeInPoints = 8.25F;
            appearance2.FontData.StrikeoutAsString = "False";
            appearance2.FontData.UnderlineAsString = "False";
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Top";
            this.txtDescription.Appearance = appearance2;
            this.txtDescription.Location = new System.Drawing.Point(57, 59);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(880, 42);
            this.txtDescription.TabIndex = 38;
            this.txtDescription.Value = "<span style=\"font-family:Times New Roman; font-size:10pt;\">Xử lý chênh lệch tỷ gi" +
    "á</span>";
            // 
            // cbbAccountCredit
            // 
            this.cbbAccountCredit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountCredit.Location = new System.Drawing.Point(126, 19);
            this.cbbAccountCredit.Name = "cbbAccountCredit";
            this.cbbAccountCredit.NullText = "<chọn dữ liệu>";
            this.cbbAccountCredit.Size = new System.Drawing.Size(121, 22);
            this.cbbAccountCredit.TabIndex = 36;
            // 
            // lblAccountingObjectAddress
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance3;
            this.lblAccountingObjectAddress.AutoSize = true;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(3, 59);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(48, 14);
            this.lblAccountingObjectAddress.TabIndex = 34;
            this.lblAccountingObjectAddress.Text = "Diễn giải";
            // 
            // lblAccountingObject
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance4;
            this.lblAccountingObject.AutoSize = true;
            this.lblAccountingObject.Location = new System.Drawing.Point(3, 27);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(117, 14);
            this.lblAccountingObject.TabIndex = 33;
            this.lblAccountingObject.Text = "TK xử lý chênh lệch lãi";
            // 
            // lblD
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.lblD.Appearance = appearance5;
            this.lblD.AutoSize = true;
            this.lblD.Location = new System.Drawing.Point(272, 27);
            this.lblD.Name = "lblD";
            this.lblD.Size = new System.Drawing.Size(115, 14);
            this.lblD.TabIndex = 32;
            this.lblD.Text = "TK xử lý chênh lệch lỗ";
            // 
            // lblNo
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.lblNo.Appearance = appearance6;
            this.lblNo.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblNo.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblNo.Location = new System.Drawing.Point(3, 3);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(100, 24);
            this.lblNo.TabIndex = 28;
            this.lblNo.Text = "Số chứng từ";
            // 
            // dteDate
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance7;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(201, 24);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(100, 21);
            this.dteDate.TabIndex = 33;
            this.dteDate.Value = null;
            // 
            // txtNo
            // 
            appearance8.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance8;
            this.txtNo.Location = new System.Drawing.Point(3, 24);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(100, 21);
            this.txtNo.TabIndex = 29;
            // 
            // dtePostedDate
            // 
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance9;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(102, 24);
            this.dtePostedDate.MaskInput = "";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(100, 21);
            this.dtePostedDate.TabIndex = 32;
            this.dtePostedDate.Value = null;
            // 
            // lblPostedDate
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance10;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblPostedDate.Location = new System.Drawing.Point(102, 3);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 30;
            this.lblPostedDate.Text = "Ngày hoạch toán";
            // 
            // lblDate
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance11;
            this.lblDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblDate.Location = new System.Drawing.Point(201, 3);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 24);
            this.lblDate.TabIndex = 31;
            this.lblDate.Text = "Ngày chứng từ";
            // 
            // panelFooter
            // 
            // 
            // panelFooter.ClientArea
            // 
            this.panelFooter.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 378);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(946, 49);
            this.panelFooter.TabIndex = 1;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.btnApply);
            this.ultraGroupBox3.Controls.Add(this.btnCancel);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(946, 49);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
            this.btnApply.Appearance = appearance12;
            this.btnApply.Location = new System.Drawing.Point(766, 10);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(85, 27);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Đồng ý";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.Image = ((object)(resources.GetObject("appearance13.Image")));
            this.btnCancel.Appearance = appearance13;
            this.btnCancel.Location = new System.Drawing.Point(857, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panelBody
            // 
            // 
            // panelBody.ClientArea
            // 
            this.panelBody.ClientArea.Controls.Add(this.uGridIndex);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 164);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(946, 214);
            this.panelBody.TabIndex = 2;
            // 
            // uGridIndex
            // 
            this.uGridIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridIndex.Location = new System.Drawing.Point(0, 0);
            this.uGridIndex.Name = "uGridIndex";
            this.uGridIndex.Size = new System.Drawing.Size(946, 214);
            this.uGridIndex.TabIndex = 1;
            this.uGridIndex.Text = "ultraGrid1";
            this.uGridIndex.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // FGSolveExchangeRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 427);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.Name = "FGSolveExchangeRate";
            this.Text = "Xử lý chênh lệch tỷ giá";
            this.panelHeader.ClientArea.ResumeLayout(false);
            this.panelHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountDebit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            this.panelFooter.ClientArea.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.panelBody.ClientArea.ResumeLayout(false);
            this.panelBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel panelHeader;
        private Infragistics.Win.Misc.UltraPanel panelFooter;
        private Infragistics.Win.Misc.UltraPanel panelBody;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel lblNo;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.Misc.UltraLabel lblD;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtDescription;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountDebit;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountCredit;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridIndex;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraButton btnCancel;
    }
}