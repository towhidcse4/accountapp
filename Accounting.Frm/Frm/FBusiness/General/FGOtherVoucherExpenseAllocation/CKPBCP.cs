﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinSchedule;

namespace Accounting
{


    public partial class CKPBCP : CustormForm
    {
        readonly List<GOtherVoucher> _dsGOtherVouchers;
        public GOtherVoucher GOtherVoucher;
        public int _status;
        public CKPBCP(GOtherVoucher temp, List<GOtherVoucher> dsGOtherVouchers, int statusForm)
        {
            GOtherVoucher = temp;
            _dsGOtherVouchers = dsGOtherVouchers;
            _status = statusForm;
            InitializeComponent();
            var dte = Utils.GetDbStartDate().StringToDateTime();
            cbbThang.Value = dte.HasValue ? dte.Value.Month : DateTime.Now.Month;
            txtYear.Value = dte.HasValue ? dte.Value.Year : DateTime.Now.Year;

        }
        private void btnApply_Click(object sender, EventArgs e)
        {

            int thang = 0;
            try
            {
                thang = int.Parse(cbbThang.Text);
                if (thang > 12 || thang < 1)
                {
                    MSG.Warning("Giá trị tháng không hợp lệ");
                    return;
                }
            }
            catch (Exception)
            {
                MSG.Warning("Giá trị tháng không hợp lệ");
                return;
            }
            int nam = Convert.ToInt32(txtYear.Value);
            var temp = _dsGOtherVouchers.FirstOrDefault(p => p.PostedDate.Month == thang && p.PostedDate.Year == nam);
            if (temp != null)
            {
                if (MSG.MessageBoxStand(string.Format("Tháng {0} năm {1} đã được phân bổ. Bạn có muốn xem chứng từ phân bổ đó không?", thang, nam), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    DialogResult = DialogResult.OK;
                    _status = ConstFrm.optStatusForm.View;
                    GOtherVoucher = temp;
                    //new FGOtherVoucherDetail(temp, _dsGOtherVouchers, ConstFrm.optStatusForm.View, 0, 0).ShowDialog(this);
                    Close();
                }
                return;
            }
            var dbDateClosed = Utils.GetDBDateClosed();
            var dte = dbDateClosed.StringToDateTime();
            if (dte.HasValue && (dte.Value.Year > nam || (dte.Value.Year == nam && dte.Value.Month > thang)))
            {
                MSG.Warning(string.Format("Ngày hạch toán phải lớn hơn ngày khóa sổ: {0}. Xin vui lòng kiểm tra lại.", dbDateClosed));
                return;
            }
            GOtherVoucher.PostedDate = new DateTime(nam, thang, DateTime.DaysInMonth(nam, thang));
            DialogResult = DialogResult.OK;
            //new FGOtherVoucherDetail(_temp, _dsGOtherVouchers, _status, thang, nam).ShowDialog(this);
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtYear_Validated(object sender, EventArgs e)
        {
            int thang = 0;
            try
            {
                thang = int.Parse(txtYear.Text);
                if (thang > 2100 || thang <= 1980)
                {
                    MSG.Warning("Giá trị năm không hợp lệ");
                    txtYear.Value = DateTime.Now.Year;
                    return;
                }

            }
            catch (Exception)
            {
                MSG.Warning("Giá trị năm không hợp lệ");
                txtYear.Value = DateTime.Now.Year;
                return;
            }
        }
    }
}
