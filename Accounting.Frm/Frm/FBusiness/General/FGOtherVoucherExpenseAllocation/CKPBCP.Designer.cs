﻿namespace Accounting
{
    partial class CKPBCP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            this.lblMonth = new Infragistics.Win.Misc.UltraLabel();
            this.lblYears = new Infragistics.Win.Misc.UltraLabel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.txtYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.cbbThang = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMonth
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.lblMonth.Appearance = appearance1;
            this.lblMonth.Location = new System.Drawing.Point(6, 19);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(62, 23);
            this.lblMonth.TabIndex = 0;
            this.lblMonth.Text = "Tháng :";
            // 
            // lblYears
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.lblYears.Appearance = appearance2;
            this.lblYears.Location = new System.Drawing.Point(143, 19);
            this.lblYears.Name = "lblYears";
            this.lblYears.Size = new System.Drawing.Size(41, 23);
            this.lblYears.TabIndex = 1;
            this.lblYears.Text = "Năm :";
            // 
            // btnClose
            // 
            appearance3.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance3;
            this.btnClose.Location = new System.Drawing.Point(202, 80);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 340;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            appearance4.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnApply.Appearance = appearance4;
            this.btnApply.Location = new System.Drawing.Point(121, 80);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 30);
            this.btnApply.TabIndex = 341;
            this.btnApply.Text = "Đồng ý";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // txtYear
            // 
            this.txtYear.AutoSize = false;
            this.txtYear.Location = new System.Drawing.Point(180, 19);
            this.txtYear.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtYear.MaskInput = "nnnn";
            this.txtYear.MinValue = 1980;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(72, 22);
            this.txtYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtYear.TabIndex = 343;
            this.txtYear.Tag = "";
            this.txtYear.Value = 2012;
            this.txtYear.Validated += new System.EventHandler(this.txtYear_Validated);
            // 
            // cbbThang
            // 
            appearance5.TextHAlignAsString = "Right";
            this.cbbThang.Appearance = appearance5;
            this.cbbThang.AutoSize = false;
            valueListItem2.DataValue = ((short)(1));
            valueListItem3.DataValue = ((short)(2));
            valueListItem1.DataValue = ((short)(3));
            valueListItem4.DataValue = ((short)(4));
            valueListItem5.DataValue = ((short)(5));
            valueListItem6.DataValue = ((short)(6));
            valueListItem7.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem7.DataValue = ((short)(7));
            valueListItem8.DataValue = ((short)(8));
            valueListItem9.DataValue = ((short)(9));
            valueListItem10.DataValue = ((short)(10));
            valueListItem11.DataValue = ((short)(11));
            valueListItem12.DataValue = ((short)(12));
            this.cbbThang.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem3,
            valueListItem1,
            valueListItem4,
            valueListItem5,
            valueListItem6,
            valueListItem7,
            valueListItem8,
            valueListItem9,
            valueListItem10,
            valueListItem11,
            valueListItem12});
            this.cbbThang.Location = new System.Drawing.Point(50, 19);
            this.cbbThang.Name = "cbbThang";
            this.cbbThang.Size = new System.Drawing.Size(77, 22);
            this.cbbThang.TabIndex = 344;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.cbbThang);
            this.ultraGroupBox1.Controls.Add(this.txtYear);
            this.ultraGroupBox1.Controls.Add(this.lblYears);
            this.ultraGroupBox1.Controls.Add(this.lblMonth);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(265, 59);
            this.ultraGroupBox1.TabIndex = 345;
            // 
            // CKPBCP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 117);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnApply);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CKPBCP";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn kỳ tính phân bổ chi phí trả trước";
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbThang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel lblMonth;
        private Infragistics.Win.Misc.UltraLabel lblYears;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbThang;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
    }
}