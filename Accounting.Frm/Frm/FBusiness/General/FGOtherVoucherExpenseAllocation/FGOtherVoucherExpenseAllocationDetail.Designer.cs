﻿using Accounting.Core.Domain;

namespace Accounting
{
    partial class FGOtherVoucherExpenseAllocationDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbReason = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.palFill = new Infragistics.Win.Misc.UltraPanel();
            this.palTab = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTop = new Infragistics.Win.Misc.UltraPanel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).BeginInit();
            this.palFill.ClientArea.SuspendLayout();
            this.palFill.SuspendLayout();
            this.palTab.ClientArea.SuspendLayout();
            this.palTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            this.palTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.cbbReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(941, 74);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance1;
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(292, 26);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(606, 22);
            this.txtReason.TabIndex = 5;
            // 
            // cbbReason
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.cbbReason.Appearance = appearance2;
            this.cbbReason.AutoSize = false;
            this.cbbReason.Location = new System.Drawing.Point(74, 26);
            this.cbbReason.Name = "cbbReason";
            this.cbbReason.NullText = "<chọn dữ liệu>";
            this.cbbReason.Size = new System.Drawing.Size(212, 22);
            this.cbbReason.TabIndex = 4;
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Location = new System.Drawing.Point(7, 25);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(52, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Tag = "";
            this.ultraLabel1.Text = "Diễn giải";
            this.ultraLabel1.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            // 
            // palFill
            // 
            // 
            // palFill.ClientArea
            // 
            this.palFill.ClientArea.Controls.Add(this.palTab);
            this.palFill.ClientArea.Controls.Add(this.ultraSplitter1);
            this.palFill.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 85);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(941, 410);
            this.palFill.TabIndex = 44;
            // 
            // palTab
            // 
            // 
            // palTab.ClientArea
            // 
            this.palTab.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palTab.Location = new System.Drawing.Point(0, 84);
            this.palTab.Name = "palTab";
            this.palTab.Size = new System.Drawing.Size(941, 326);
            this.palTab.TabIndex = 44;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance4;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(843, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 8;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 74);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 0;
            this.ultraSplitter1.Size = new System.Drawing.Size(941, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.palTop);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Top;
            appearance6.FontData.BoldAsString = "True";
            appearance6.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance6;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(941, 85);
            this.grpTop.TabIndex = 42;
            this.grpTop.Text = "Chứng từ nghiệp vụ khác";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTop
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.palTop.Appearance = appearance5;
            this.palTop.Location = new System.Drawing.Point(7, 27);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(343, 49);
            this.palTop.TabIndex = 42;
            // 
            // FGOtherVoucherExpenseAllocationDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(941, 495);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.grpTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FGOtherVoucherExpenseAllocationDetail";
            this.Text = "Chứng từ nghiệp vụ khác";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FGOtherVoucherExpenseAllocationDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FGOtherVoucherExpenseAllocationDetail_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).EndInit();
            this.palFill.ClientArea.ResumeLayout(false);
            this.palFill.ResumeLayout(false);
            this.palTab.ClientArea.ResumeLayout(false);
            this.palTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.palTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel palFill;
        private Infragistics.Win.Misc.UltraPanel palTab;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private Infragistics.Win.Misc.UltraPanel palTop;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbReason;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}