﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Components.DictionaryAdapter;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using ColumnStyle = System.Windows.Forms.ColumnStyle;
using Type = System.Type;

namespace Accounting
{
    public partial class FGOtherVoucherExpenseAllocationDetail : fgOtherVoucherExpenseAllocationStand
    {
        #region khai báo
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2 = null;
        UltraGrid ugrid1 = null;
        //add by cuongpv
        UltraGrid ugridHachToan = null;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        //end add by cuongpv
        #endregion

        #region khởi tạo
        public FGOtherVoucherExpenseAllocationDetail(GOtherVoucher gotherVoucher, List<GOtherVoucher> dsGOtherVoucher, int statusForm)
        {
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                WaitingFrm.StopWaiting();
                var frm = new CKPBCP(gotherVoucher, dsGOtherVoucher, statusForm);
                if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                {
                    base.Close();
                    return;
                }
                WaitingFrm.StartWaiting();
                statusForm = frm._status;
                if (statusForm == ConstFrm.optStatusForm.View)
                    gotherVoucher = dsGOtherVoucher.FirstOrDefault(k => k.ID == frm.GOtherVoucher.ID);
                else gotherVoucher.PostedDate = frm.GOtherVoucher.PostedDate;
            }
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion

            _statusForm = statusForm;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new GOtherVoucher { TypeID = 690 }) : gotherVoucher;
            _select.PostedDate = gotherVoucher.PostedDate;
            _select.Date = gotherVoucher.PostedDate;
            _select.CurrencyID = "VND";
            _select.ExchangeRate = 1;
            _listSelects.AddRange(dsGOtherVoucher);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);

            //Change Menu Top
            ReloadToolbar(_select, _listSelects, _statusForm);
           
        }

        public override void ShowPopup(GOtherVoucher input, int statusForm)
        {
            WaitingFrm.StopWaiting();
            var frm = new CKPBCP(input, _listSelects, statusForm);
            if (frm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            WaitingFrm.StartWaiting();
            statusForm = frm._status;
            if (statusForm == ConstFrm.optStatusForm.View)
                input = _listSelects.FirstOrDefault(k => k.ID == frm.GOtherVoucher.ID);
            else
            {
                input = frm.GOtherVoucher;
            }

            _statusForm = statusForm;
            _select = input;

            InitializeGUI(_select);
        }
        #endregion
        #region override
        public override void InitializeGUI(GOtherVoucher inputVoucher)
        {
            #region Get giao diện
            Template mauGiaoDien = Utils.GetMauGiaoDien(inputVoucher.TypeID, inputVoucher.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #region Phần đầu
            grpTop.Text = _select.Type == null ? Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID)).TypeName.ToUpper() : _select.Type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<GOtherVoucher>(palTop, "No", "PostedDate", "Date");
            if (_statusForm == ConstFrm.optStatusForm.Add) _select.Reason = string.Format("Phân bổ chi phí trả trước tháng {0} năm {1}", _select.PostedDate.Month, _select.PostedDate.Year);
            #endregion

            BindingList<GOtherVoucherDetail> dsGOtherVoucherDetails = new BindingList<GOtherVoucherDetail>();
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            BindingList<GOtherVoucherDetailExpense> dsGOtherVoucherDetailExpenses = new BindingList<GOtherVoucherDetailExpense>();
            BindingList<GOtherVoucherDetailExpenseAllocation> dsGOtherVoucherDetailExpenseAllocations = new BindingList<GOtherVoucherDetailExpenseAllocation>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
                var lst = new List<GOtherVoucherDetail>();
                var lstexpense = new List<GOtherVoucherDetailExpense>();
                var lstallo = new List<GOtherVoucherDetailExpenseAllocation>();
                var lstcpdpb = IGOtherVoucherDetailExpenseService.GetAll();
                foreach (var pe in IPrepaidExpenseService.GetAll_ByDate(_select.PostedDate))
                {
                    var expense = new GOtherVoucherDetailExpense();
                    expense.PrepaidExpenseID = pe.ID;
                    expense.PrepaidExpenseName = pe.PrepaidExpenseName;
                    expense.Amount = pe.Amount;
                    expense.RemainingAmount = pe.TypeExpense == 0 ? (pe.Amount - pe.AllocationAmount - lstcpdpb.Where(c => c.PrepaidExpenseID == pe.ID).Sum(c => c.AllocationAmount)) : (pe.Amount - lstcpdpb.Where(c => c.PrepaidExpenseID == pe.ID).Sum(c => c.AllocationAmount));
                    expense.AllocationAmount = (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 125).Data == "0" && inputVoucher.PostedDate.Year == pe.Date.Year && inputVoucher.PostedDate.Month == pe.Date.Month) ? ((pe.AllocatedAmount / DateTime.DaysInMonth(inputVoucher.PostedDate.Year, inputVoucher.PostedDate.Month) * (DateTime.DaysInMonth(inputVoucher.PostedDate.Year, inputVoucher.PostedDate.Month) - pe.Date.Day + 1))) : pe.AllocatedAmount;
                    if ((pe.AllocationTime - pe.AllocatedPeriod - lstcpdpb.Where(c => c.PrepaidExpenseID == pe.ID).Count()) == 1) expense.AllocationAmount = expense.RemainingAmount;
                    foreach (var x in pe.PrepaidExpenseAllocations)
                    {
                        var allo = new GOtherVoucherDetailExpenseAllocation();
                        allo.PrepaidExpenseID = pe.ID;
                        allo.PrepaidExpenseName = pe.PrepaidExpenseName;
                        allo.AllocationObjectID = x.AllocationObjectID;
                        allo.AllocationObjectType = x.AllocationObjectType;
                        allo.AllocationObjectName = x.AllocationObjectName;
                        allo.AllocationRate = x.AllocationRate;
                        allo.AllocationAmount = ((expense.AllocationAmount * x.AllocationRate) / 100);
                        allo.Amount = pe.Amount;
                        allo.CostAccount = x.CostAccount;
                        allo.ExpenseItemID = x.ExpenseItemID;
                        lstallo.Add(allo);

                        var detail = new GOtherVoucherDetail();
                        detail.Description = inputVoucher.Reason;
                        detail.DebitAccount = x.CostAccount;
                        detail.CreditAccount = pe.AllocationAccount;
                        detail.ExpenseItemID = x.ExpenseItemID;
                        detail.Amount = allo.AllocationAmount;
                        detail.AmountOriginal = allo.AllocationAmount;
                        lst.Add(detail);
                    }
                    lstexpense.Add(expense);
                }
                dsGOtherVoucherDetails = new BindingList<GOtherVoucherDetail>(lst);//tab 3
                dsGOtherVoucherDetailExpenses = new BindingList<GOtherVoucherDetailExpense>(lstexpense);//tab 1
                dsGOtherVoucherDetailExpenseAllocations = new BindingList<GOtherVoucherDetailExpenseAllocation>(lstallo);//tab 2
            }
            else
            {
                bdlRefVoucher = new BindingList<RefVoucher>(inputVoucher.RefVouchers);//tab 4
                dsGOtherVoucherDetails = new BindingList<GOtherVoucherDetail>(inputVoucher.GOtherVoucherDetails.OrderBy(x=>x.OrderPriority).ToList());//edit by cuongpv: .OrderBy(x=>x.OrderPriority).ToList()
                dsGOtherVoucherDetailExpenses = new BindingList<GOtherVoucherDetailExpense>(inputVoucher.GOtherVoucherDetailExpenses.OrderBy(x => x.OrderPriority).ToList());//edit by cuongpv: OrderBy(x => x.OrderPriority).ToList()
                dsGOtherVoucherDetailExpenseAllocations = new BindingList<GOtherVoucherDetailExpenseAllocation>(inputVoucher.GOtherVoucherDetailExpenseAllocations.OrderBy(x => x.OrderPriority).ToList());//edit by cuongpv: OrderBy(x => x.OrderPriority).ToList()
            }
            _listObjectInput = new BindingList<System.Collections.IList> { dsGOtherVoucherDetails };
            _listObjectInputGroup = new BindingList<System.Collections.IList> { dsGOtherVoucherDetailExpenses };
            _listObjectInputPost = new BindingList<System.Collections.IList> { dsGOtherVoucherDetailExpenseAllocations, bdlRefVoucher };

            this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInputGroup, _listObjectInputPost, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { false, false, false, false };
            this.ConfigGridByManyTemplete<GOtherVoucher>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard);
            ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;         
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);

            ugrid1 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid1.DisplayLayout.Bands[0].Columns["PrepaidExpenseID"].CellActivation = Activation.NoEdit;
            ugrid1.DisplayLayout.Bands[0].Columns["PrepaidExpenseName"].CellActivation = Activation.NoEdit;

            //Tab Hóa đơn
            txtReason.DataBindings.Clear();
            txtReason.DataBindings.Add("Text", inputVoucher, "Reason", true, DataSourceUpdateMode.OnPropertyChanged);
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                SetOrGetGuiObject(inputVoucher, false);
            }

            //add by cuongpv
            if(inputVoucher.TypeID == 690)
            {
                ugridHachToan = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                if (ugridHachToan != null && ugridHachToan.Rows.Count > 0)
                {
                    foreach (var itemRow in ugridHachToan.Rows)
                    {
                        if (itemRow.Cells["DebitAccount"].Column.ToString() == "DebitAccount")
                            itemRow.Cells["DebitAccount"].SetErrorForCell(ugridHachToan.CheckAccount<GOtherVoucherDetail>(itemRow.Cells["DebitAccount"]));

                        if (itemRow.Cells["CreditAccount"].Column.ToString() == "CreditAccount")
                            itemRow.Cells["CreditAccount"].SetErrorForCell(ugridHachToan.CheckAccount<GOtherVoucherDetail>(itemRow.Cells["CreditAccount"]));
                    }
                }
            }
            //end add by cuongpv

            #endregion
        }
        #endregion

        #region Event

        private void txtReason_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                e.Handled = true;
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FGOtherVoucherExpenseAllocationDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void FGOtherVoucherExpenseAllocationDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class fgOtherVoucherExpenseAllocationStand : DetailBase<GOtherVoucher>
    {
    }
}
