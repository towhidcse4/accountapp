﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Facilities.TypedFactory.Internal;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPrepaidExpense : CatalogBase
    {
        private IPrepaidExpenseService _IPrepaidExpenseService
        {
            get { return IoC.Resolve<IPrepaidExpenseService>(); }
        }
        private IGOtherVoucherDetailExpenseService IGOtherVoucherDetailExpenseService
        {
            get { return IoC.Resolve<IGOtherVoucherDetailExpenseService>(); }
        }
        List<PrepaidExpense> lst = new List<PrepaidExpense>();
        public FPrepaidExpense()
        {
            InitializeComponent();
            LoadCogfig();
        }
        void LoadCogfig()
        {
            lst = _IPrepaidExpenseService.GetAll();
            _IPrepaidExpenseService.UnbindSession(lst);
            lst = _IPrepaidExpenseService.GetAll();
            List<PrepaidExpense> newlist = lst.CloneObject();
            var lstpbcp = IGOtherVoucherDetailExpenseService.GetAll();
            foreach (var x in newlist)
            {
                x.AllocationAmount = ((x.TypeExpense == 0 ? x.AllocationAmount : 0) + (lstpbcp.Where(c => c.PrepaidExpenseID == x.ID).ToList().Count > 0 ? lstpbcp.Where(c => c.PrepaidExpenseID == x.ID).Sum(t => t.AllocationAmount) : 0));
                x.AllocatedPeriod = ((x.TypeExpense == 0 ? x.AllocatedPeriod : 0) + (lstpbcp.Where(c => c.PrepaidExpenseID == x.ID).ToList().Count));
            }
            uGridDS.DataSource = new BindingList<PrepaidExpense>(newlist);
            Utils.ConfigGrid(uGridDS, ConstDatabase.PrepaidExpense_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "Amount", false);
            uGridDS.DisplayLayout.Bands[0].Columns["AllocationAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "AllocationAmount", false);
            uGridDS.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "AllocatedAmount", false);
            if (uGridDS.Rows.Count > 0)
            {
                uGridDS.Rows[0].Selected = true;
                UltraGridRow rowSelect = uGridDS.Rows[0];
                PrepaidExpense model = (PrepaidExpense)rowSelect.ListObject;
                PrepaidExpense newmodel = lst.FirstOrDefault(x => x.ID == model.ID);
                uGridPrepaidExpenseAllocation.DataSource = new BindingList<PrepaidExpenseAllocation>(newmodel.PrepaidExpenseAllocations);
                foreach (var item in uGridPrepaidExpenseAllocation.DisplayLayout.Bands[0].Columns)
                {
                    this.ConfigEachColumn4Grid(0, item, uGridPrepaidExpenseAllocation, true);
                }
                Utils.ConfigGrid(uGridPrepaidExpenseAllocation, ConstDatabase.PrepaidExpenseAllocation_TableName);
                var grdband = uGridPrepaidExpenseAllocation.DisplayLayout.Bands[0];
                grdband.Columns["AllocationObjectID"].LockColumn(true);
                grdband.Columns["CostAccount"].LockColumn(true);
                grdband.Columns["ExpenseItemID"].LockColumn(true);
                ///
                uGridPrepaidExpenseVoucher.DataSource = new BindingList<PrepaidExpenseVoucher>(newmodel.PrepaidExpenseVouchers);
                Utils.ConfigGrid(uGridPrepaidExpenseVoucher, ConstDatabase.PrepaidExpenseVoucher_TableName);
                uGridPrepaidExpenseVoucher.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridPrepaidExpenseVoucher.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
            else
            {
                uGridPrepaidExpenseAllocation.DataSource = new BindingList<PrepaidExpenseAllocation>(new List<PrepaidExpenseAllocation>());
                Utils.ConfigGrid(uGridPrepaidExpenseAllocation, ConstDatabase.PrepaidExpenseAllocation_TableName);
                var grdband = uGridPrepaidExpenseAllocation.DisplayLayout.Bands[0];
                grdband.Columns["AllocationObjectID"].LockColumn(true);
                grdband.Columns["CostAccount"].LockColumn(true);
                grdband.Columns["ExpenseItemID"].LockColumn(true);
                ///
                uGridPrepaidExpenseVoucher.DataSource = new BindingList<PrepaidExpenseVoucher>(new List<PrepaidExpenseVoucher>());
                Utils.ConfigGrid(uGridPrepaidExpenseVoucher, ConstDatabase.PrepaidExpenseVoucher_TableName);
                uGridPrepaidExpenseVoucher.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                uGridPrepaidExpenseVoucher.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
        }
        #region Event
        protected override void DeleteFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                PrepaidExpense temp = uGridDS.Selected.Rows[0].ListObject as PrepaidExpense;
                if (_IPrepaidExpenseService.CheckDelete(temp.ID))
                {
                    if (MSG.Question(string.Format(resSystem.MSG_System_05, temp.PrepaidExpenseCode)) == System.Windows.Forms.DialogResult.Yes)
                    {
                        _IPrepaidExpenseService.BeginTran();
                        try
                        {
                            var item = _IPrepaidExpenseService.Getbykey(temp.ID);
                            _IPrepaidExpenseService.Delete(item);
                            _IPrepaidExpenseService.CommitTran();
                            Utils.ClearCacheByType<PrepaidExpense>();
                            LoadCogfig();
                        }
                        catch (Exception ex)
                        {
                            _IPrepaidExpenseService.RolbackTran();
                            MSG.Warning("Có lỗi xảy ra khi xóa chi phí trả trước");
                        }
                    }
                }
                else
                {
                    MSG.Warning("Chi phí trả trước khi đã phát sinh chứng từ Phân bổ Chi phí Trả trước. Không được phép xóa!");
                }
            }
            else
                MSG.Error("Bạn cần chon 1 chi phí để xóa!");
        }
        protected override void ResetFunction()
        {
            WaitingFrm.StartWaiting();
            LoadCogfig();
            WaitingFrm.StopWaiting();
        }
        protected override void EditFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {

                PrepaidExpense temp = uGridDS.Selected.Rows[0].ListObject as PrepaidExpense;
                var model = _IPrepaidExpenseService.Getbykey(temp.ID);
                _IPrepaidExpenseService.UnbindSession(model);
                model = _IPrepaidExpenseService.Getbykey(temp.ID);
                model.AllocationAmount = temp.AllocationAmount;
                model.AllocatedPeriod = temp.AllocatedPeriod;
                //new FPrepaidExpenseDetail(model, model.TypeExpense).ShowDialog(this);
                //if (!FPrepaidExpenseDetail.isClose) { Utils.ClearCacheByType<PrepaidExpense>(); LoadCogfig(); }
                //HUYPD Edit Show Form
                FPrepaidExpenseDetail frm = new FPrepaidExpenseDetail(model, model.TypeExpense);
                frm.FormClosed += new FormClosedEventHandler(FPrepaidExpenseDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
            else
                MSG.Error("Bạn cần chon 1 chi phí để sửa!");
        }
        private void FPrepaidExpenseDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ClearCacheByType<PrepaidExpense>();
            LoadCogfig();
        }
        #region button
        private void drpbtnAdd_DroppingDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Control control = (Control)sender;
            var startPoint = control.PointToScreen(new Point(control.Location.X + control.Margin.Left + control.Padding.Left - drpbtnAdd.ImageSize.Width, control.Location.Y + control.Size.Height + control.Margin.Top + control.Padding.Top - drpbtnAdd.ImageSize.Height));
            cms4ButtonPrepaidExpense.Show(startPoint);
        }

        private void drpbtnAdd_Click(object sender, EventArgs e)
        {
            drpbtnAdd_DroppingDown(sender, new System.ComponentModel.CancelEventArgs(false));
        }

        private void drpbtnAdd_MouseHover(object sender, EventArgs e)
        {
            drpbtnAdd_DroppingDown(sender, new System.ComponentModel.CancelEventArgs(false));
        }
        private void btnPrepaidExpense_Click(object sender, EventArgs e)
        {
            //new FPrepaidExpenseDetail(1).ShowDialog(this);
            //if (!FPrepaidExpenseDetail.isClose) { Utils.ClearCacheByType<PrepaidExpense>(); LoadCogfig(); }
            //HUYPD Edit Show Form
            FPrepaidExpenseDetail frm = new FPrepaidExpenseDetail(1);
            frm.FormClosed += new FormClosedEventHandler(FPrepaidExpenseDetail_FormClosed);
            frm.ShowDialogHD(this);
        }
        private void btnOPNPrepaidExpense_Click(object sender, EventArgs e)
        {
            //new FPrepaidExpenseDetail(0).ShowDialog(this);
            //if (!FPrepaidExpenseDetail.isClose) { Utils.ClearCacheByType<PrepaidExpense>(); LoadCogfig(); }
            //HUYPD Edit Show Form
            FPrepaidExpenseDetail frm = new FPrepaidExpenseDetail(0);
            frm.FormClosed += new FormClosedEventHandler(FPrepaidExpenseDetail_FormClosed);
            frm.ShowDialogHD(this);
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetFunction();
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadCogfig();
        }

        private void uGridDS_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        private void uGridDS_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }
        private void uGridDS_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null || e.Row.IsFilterRow) return;
            var temp = e.Row.ListObject as PrepaidExpense;
            PrepaidExpense newmodel = lst.FirstOrDefault(x => x.ID == temp.ID);
            uGridPrepaidExpenseAllocation.SetDataBinding(newmodel.PrepaidExpenseAllocations, "");
            uGridPrepaidExpenseVoucher.SetDataBinding(newmodel.PrepaidExpenseVouchers, "");
            Utils.ConfigGrid(uGridPrepaidExpenseVoucher, ConstDatabase.PrepaidExpenseVoucher_TableName);
            if (newmodel.PrepaidExpenseVouchers.Count > 0) uGridPrepaidExpenseVoucher.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridPrepaidExpenseVoucher.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        #endregion

        #endregion

        private void FPrepaidExpense_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDS.ResetText();
            uGridDS.ResetUpdateMode();
            uGridDS.ResetExitEditModeOnLeave();
            uGridDS.ResetRowUpdateCancelAction();
            uGridDS.DataSource = null;
            uGridDS.Layouts.Clear();
            uGridDS.ResetLayouts();
            uGridDS.ResetDisplayLayout();
            uGridDS.Refresh();
            uGridDS.ClearUndoHistory();
            uGridDS.ClearXsdConstraints();

            uGridPrepaidExpenseAllocation.ResetText();
            uGridPrepaidExpenseAllocation.ResetUpdateMode();
            uGridPrepaidExpenseAllocation.ResetExitEditModeOnLeave();
            uGridPrepaidExpenseAllocation.ResetRowUpdateCancelAction();
            uGridPrepaidExpenseAllocation.DataSource = null;
            uGridPrepaidExpenseAllocation.Layouts.Clear();
            uGridPrepaidExpenseAllocation.ResetLayouts();
            uGridPrepaidExpenseAllocation.ResetDisplayLayout();
            uGridPrepaidExpenseAllocation.Refresh();
            uGridPrepaidExpenseAllocation.ClearUndoHistory();
            uGridPrepaidExpenseAllocation.ClearXsdConstraints();

            uGridPrepaidExpenseVoucher.ResetText();
            uGridPrepaidExpenseVoucher.ResetUpdateMode();
            uGridPrepaidExpenseVoucher.ResetExitEditModeOnLeave();
            uGridPrepaidExpenseVoucher.ResetRowUpdateCancelAction();
            uGridPrepaidExpenseVoucher.DataSource = null;
            uGridPrepaidExpenseVoucher.Layouts.Clear();
            uGridPrepaidExpenseVoucher.ResetLayouts();
            uGridPrepaidExpenseVoucher.ResetDisplayLayout();
            uGridPrepaidExpenseVoucher.Refresh();
            uGridPrepaidExpenseVoucher.ClearUndoHistory();
            uGridPrepaidExpenseVoucher.ClearXsdConstraints();
        }

        private void FPrepaidExpense_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
