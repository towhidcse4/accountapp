﻿namespace Accounting
{
    partial class FPrepaidExpenseDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPrepaidExpenseAllocation = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPrepaidExpenseVoucher = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.chkIsActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.btnSaveContinue = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtAllocatedPeriod = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAllocationTime = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAllocatedAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAllocationAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.btnApportion = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPrepaidExpenseName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtPrepaidExpenseCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMaterialGoodsName = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPrepaidExpenseAllocation)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPrepaidExpenseVoucher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepaidExpenseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepaidExpenseCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridPrepaidExpenseAllocation);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(794, 277);
            // 
            // uGridPrepaidExpenseAllocation
            // 
            this.uGridPrepaidExpenseAllocation.ContextMenuStrip = this.contextMenuStrip1;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Appearance = appearance1;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.CellPadding = 0;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPrepaidExpenseAllocation.DisplayLayout.UseFixedHeaders = true;
            this.uGridPrepaidExpenseAllocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPrepaidExpenseAllocation.Location = new System.Drawing.Point(0, 0);
            this.uGridPrepaidExpenseAllocation.Name = "uGridPrepaidExpenseAllocation";
            this.uGridPrepaidExpenseAllocation.Size = new System.Drawing.Size(794, 277);
            this.uGridPrepaidExpenseAllocation.TabIndex = 110;
            this.uGridPrepaidExpenseAllocation.Text = "uGridPB";
            this.uGridPrepaidExpenseAllocation.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPrepaidExpenseAllocation_AfterCellUpdate);
            this.uGridPrepaidExpenseAllocation.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridPrepaidExpenseAllocation_Error);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(205, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(204, 22);
            this.tsmAdd.Text = "Thêm một dòng";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(204, 22);
            this.tsmDelete.Text = "Xóa một dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridPrepaidExpenseVoucher);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(794, 277);
            // 
            // uGridPrepaidExpenseVoucher
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Appearance = appearance13;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.HideRowSelector;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.CellPadding = 0;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPrepaidExpenseVoucher.DisplayLayout.UseFixedHeaders = true;
            this.uGridPrepaidExpenseVoucher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPrepaidExpenseVoucher.Location = new System.Drawing.Point(0, 0);
            this.uGridPrepaidExpenseVoucher.Name = "uGridPrepaidExpenseVoucher";
            this.uGridPrepaidExpenseVoucher.Size = new System.Drawing.Size(794, 277);
            this.uGridPrepaidExpenseVoucher.TabIndex = 2;
            this.uGridPrepaidExpenseVoucher.Text = "ultraGrid3";
            // 
            // chkIsActive
            // 
            appearance25.TextVAlignAsString = "Middle";
            this.chkIsActive.Appearance = appearance25;
            this.chkIsActive.BackColor = System.Drawing.Color.Transparent;
            this.chkIsActive.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkIsActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsActive.Location = new System.Drawing.Point(9, 445);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(116, 22);
            this.chkIsActive.TabIndex = 11;
            this.chkIsActive.Text = "Ngừng PB";
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(794, 277);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 134);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(796, 300);
            this.ultraTabControl1.TabIndex = 100;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Phân bổ";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Nguồn gốc hình thành";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // btnSaveContinue
            // 
            this.btnSaveContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance26.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSaveContinue.Appearance = appearance26;
            this.btnSaveContinue.Location = new System.Drawing.Point(486, 439);
            this.btnSaveContinue.Name = "btnSaveContinue";
            this.btnSaveContinue.Size = new System.Drawing.Size(136, 30);
            this.btnSaveContinue.TabIndex = 12;
            this.btnSaveContinue.Text = "Lưu và Thêm mới";
            this.btnSaveContinue.Click += new System.EventHandler(this.btnSaveContinue_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance27.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance27;
            this.btnClose.Location = new System.Drawing.Point(709, 439);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance28;
            this.btnSave.Location = new System.Drawing.Point(628, 439);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // dteDate
            // 
            this.dteDate.AutoSize = false;
            this.dteDate.FormatString = "dd/MM/yyyy";
            this.dteDate.Location = new System.Drawing.Point(126, 31);
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(239, 22);
            this.dteDate.TabIndex = 103;
            // 
            // txtAllocatedPeriod
            // 
            appearance29.TextHAlignAsString = "Right";
            this.txtAllocatedPeriod.Appearance = appearance29;
            this.txtAllocatedPeriod.AutoSize = false;
            this.txtAllocatedPeriod.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludePromptChars;
            this.txtAllocatedPeriod.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.txtAllocatedPeriod.Location = new System.Drawing.Point(126, 90);
            this.txtAllocatedPeriod.Name = "txtAllocatedPeriod";
            this.txtAllocatedPeriod.PromptChar = ' ';
            this.txtAllocatedPeriod.Size = new System.Drawing.Size(128, 22);
            this.txtAllocatedPeriod.TabIndex = 108;
            this.txtAllocatedPeriod.ValueChanged += new System.EventHandler(this.txtAllocatedPeriod_ValueChanged);
            // 
            // txtAllocationTime
            // 
            appearance30.TextHAlignAsString = "Right";
            this.txtAllocationTime.Appearance = appearance30;
            this.txtAllocationTime.AutoSize = false;
            this.txtAllocationTime.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludePromptChars;
            this.txtAllocationTime.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this.txtAllocationTime.Location = new System.Drawing.Point(126, 61);
            this.txtAllocationTime.Name = "txtAllocationTime";
            this.txtAllocationTime.PromptChar = ' ';
            this.txtAllocationTime.Size = new System.Drawing.Size(128, 22);
            this.txtAllocationTime.TabIndex = 105;
            this.txtAllocationTime.ValueChanged += new System.EventHandler(this.txtAllocationTime_ValueChanged);
            // 
            // txtAllocatedAmount
            // 
            appearance31.TextHAlignAsString = "Right";
            this.txtAllocatedAmount.Appearance = appearance31;
            this.txtAllocatedAmount.AutoSize = false;
            this.txtAllocatedAmount.Location = new System.Drawing.Point(366, 61);
            this.txtAllocatedAmount.Name = "txtAllocatedAmount";
            this.txtAllocatedAmount.PromptChar = ' ';
            this.txtAllocatedAmount.Size = new System.Drawing.Size(170, 22);
            this.txtAllocatedAmount.TabIndex = 106;
            // 
            // txtAllocationAmount
            // 
            appearance32.TextHAlignAsString = "Right";
            this.txtAllocationAmount.Appearance = appearance32;
            this.txtAllocationAmount.AutoSize = false;
            this.txtAllocationAmount.Location = new System.Drawing.Point(366, 90);
            this.txtAllocationAmount.Name = "txtAllocationAmount";
            this.txtAllocationAmount.PromptChar = ' ';
            this.txtAllocationAmount.Size = new System.Drawing.Size(395, 22);
            this.txtAllocationAmount.TabIndex = 109;
            // 
            // txtAmount
            // 
            appearance33.TextHAlignAsString = "Right";
            this.txtAmount.Appearance = appearance33;
            this.txtAmount.AutoSize = false;
            this.txtAmount.Location = new System.Drawing.Point(522, 31);
            this.txtAmount.MaxValue = 1E+100D;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PromptChar = ' ';
            this.txtAmount.Size = new System.Drawing.Size(239, 22);
            this.txtAmount.TabIndex = 104;
            this.txtAmount.ValueChanged += new System.EventHandler(this.txtAmount_ValueChanged);
            // 
            // btnApportion
            // 
            this.btnApportion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApportion.Location = new System.Drawing.Point(679, 114);
            this.btnApportion.Name = "btnApportion";
            this.btnApportion.Size = new System.Drawing.Size(115, 22);
            this.btnApportion.TabIndex = 110;
            this.btnApportion.Text = "Chọn chứng từ";
            this.btnApportion.Click += new System.EventHandler(this.btnApportion_Click);
            // 
            // ultraLabel10
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance34;
            this.ultraLabel10.Location = new System.Drawing.Point(261, 61);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel10.TabIndex = 119;
            this.ultraLabel10.Text = "Số tiền PB hàng kỳ";
            // 
            // ultraLabel9
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance35;
            this.ultraLabel9.Location = new System.Drawing.Point(549, 60);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel9.TabIndex = 118;
            this.ultraLabel9.Text = "TK chờ PB";
            // 
            // ultraLabel8
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance36;
            this.ultraLabel8.Location = new System.Drawing.Point(262, 89);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel8.TabIndex = 117;
            this.ultraLabel8.Text = "Số tiền đã PB";
            // 
            // ultraLabel7
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance37;
            this.ultraLabel7.Location = new System.Drawing.Point(26, 90);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel7.TabIndex = 116;
            this.ultraLabel7.Text = "Số kỳ đã PB";
            // 
            // ultraLabel6
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance38;
            this.ultraLabel6.Location = new System.Drawing.Point(26, 61);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel6.TabIndex = 115;
            this.ultraLabel6.Text = "Số kỳ phân bổ";
            // 
            // ultraLabel3
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance39;
            this.ultraLabel3.Location = new System.Drawing.Point(415, 32);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel3.TabIndex = 114;
            this.ultraLabel3.Text = "Tổng số tiền";
            // 
            // ultraLabel2
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance40;
            this.ultraLabel2.Location = new System.Drawing.Point(26, 32);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel2.TabIndex = 113;
            this.ultraLabel2.Text = "Ngày ghi nhận";
            // 
            // txtPrepaidExpenseName
            // 
            this.txtPrepaidExpenseName.AutoSize = false;
            this.txtPrepaidExpenseName.Location = new System.Drawing.Point(522, 3);
            this.txtPrepaidExpenseName.Name = "txtPrepaidExpenseName";
            this.txtPrepaidExpenseName.Size = new System.Drawing.Size(239, 22);
            this.txtPrepaidExpenseName.TabIndex = 102;
            // 
            // ultraLabel1
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance41;
            this.ultraLabel1.Location = new System.Drawing.Point(415, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(118, 22);
            this.ultraLabel1.TabIndex = 112;
            this.ultraLabel1.Text = "Tên CP trả trước";
            // 
            // txtPrepaidExpenseCode
            // 
            this.txtPrepaidExpenseCode.AutoSize = false;
            this.txtPrepaidExpenseCode.Location = new System.Drawing.Point(126, 4);
            this.txtPrepaidExpenseCode.Name = "txtPrepaidExpenseCode";
            this.txtPrepaidExpenseCode.Size = new System.Drawing.Size(239, 22);
            this.txtPrepaidExpenseCode.TabIndex = 101;
            // 
            // lblMaterialGoodsName
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextVAlignAsString = "Middle";
            this.lblMaterialGoodsName.Appearance = appearance42;
            this.lblMaterialGoodsName.Location = new System.Drawing.Point(26, 4);
            this.lblMaterialGoodsName.Name = "lblMaterialGoodsName";
            this.lblMaterialGoodsName.Size = new System.Drawing.Size(118, 22);
            this.lblMaterialGoodsName.TabIndex = 111;
            this.lblMaterialGoodsName.Text = "Mã CP trả trước(*)";
            // 
            // cbbAccount
            // 
            this.cbbAccount.AllowNull = Infragistics.Win.DefaultableBoolean.True;
            this.cbbAccount.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            this.cbbAccount.AutoSize = false;
            this.cbbAccount.DisplayLayout.DefaultSelectedForeColor = System.Drawing.Color.Black;
            this.cbbAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccount.LimitToList = true;
            this.cbbAccount.Location = new System.Drawing.Point(628, 59);
            this.cbbAccount.Name = "cbbAccount";
            this.cbbAccount.Size = new System.Drawing.Size(133, 22);
            this.cbbAccount.TabIndex = 107;
            this.cbbAccount.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbReponsitoryAccount_ItemNotInList);
            // 
            // FPrepaidExpenseDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 474);
            this.Controls.Add(this.cbbAccount);
            this.Controls.Add(this.dteDate);
            this.Controls.Add(this.txtAllocatedPeriod);
            this.Controls.Add(this.txtAllocationTime);
            this.Controls.Add(this.txtAllocatedAmount);
            this.Controls.Add(this.txtAllocationAmount);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.btnApportion);
            this.Controls.Add(this.ultraLabel10);
            this.Controls.Add(this.ultraLabel9);
            this.Controls.Add(this.ultraLabel8);
            this.Controls.Add(this.ultraLabel7);
            this.Controls.Add(this.ultraLabel6);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.txtPrepaidExpenseName);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.txtPrepaidExpenseCode);
            this.Controls.Add(this.lblMaterialGoodsName);
            this.Controls.Add(this.btnSaveContinue);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.chkIsActive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPrepaidExpenseDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPrepaidExpenseAllocation)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPrepaidExpenseVoucher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepaidExpenseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrepaidExpenseCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsActive;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPrepaidExpenseVoucher;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPrepaidExpenseAllocation;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.Misc.UltraButton btnSaveContinue;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocatedPeriod;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocationTime;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocatedAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAllocationAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmount;
        private Infragistics.Win.Misc.UltraButton btnApportion;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPrepaidExpenseName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtPrepaidExpenseCode;
        private Infragistics.Win.Misc.UltraLabel lblMaterialGoodsName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccount;
    }
}
