﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FPrepaidExpenseDetail : DialogForm //UserControl
    {
        #region khai báo
        private readonly IPrepaidExpenseService _IPrepaidExpenseService;
        PrepaidExpense _Select = new PrepaidExpense();
        bool Them = true;
        int Type = 0;
        public static bool isClose = true;
        public bool isError = false;
        bool OldIsActive = false;
        bool IsFirst = false;
        #endregion

        #region khởi tạo
        public FPrepaidExpenseDetail(int type)
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            Type = type;
            if (Type == 0) Text = "Thêm mới Chi phí trả trước đầu kỳ";
            else
            {
                Text = "Thêm mới Chi phí trả trước";
                ultraLabel8.Visible = false;
                ultraLabel7.Visible = false;
                txtAllocationAmount.Visible = false;
                txtAllocatedPeriod.Visible = false;
            }
            _IPrepaidExpenseService = IoC.Resolve<IPrepaidExpenseService>();
            LoadDuLieu();
            #endregion

            #region Fill dữ liệu Obj vào control  
            txtAllocatedAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAllocationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            ObjandGUI(_Select, false);
            #endregion

        }

        public FPrepaidExpenseDetail(PrepaidExpense temp, int type)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            Type = type;
            IsFirst = true;
            if (Type == 0) Text = "Sửa Chi phí trả trước đầu kỳ";
            else Text = "Sửa Chi phí trả trước";
            txtPrepaidExpenseCode.Enabled = false;
            _Select = temp;
            Them = false;
            OldIsActive = temp.CloneObject().IsActive;
            //Khai báo các webservices
            _IPrepaidExpenseService = IoC.Resolve<IPrepaidExpenseService>();
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            LoadDuLieu();
            #endregion

            #region Fill dữ liệu Obj vào control   
            txtAllocatedAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAllocationAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            ObjandGUI(temp, false);
            txtAllocatedAmount.Text = temp.AllocatedAmount.ToString();
            txtAllocationAmount.Text = temp.AllocationAmount.ToString();
            #endregion

        }


        #endregion

        #region nghiệp vụ
        #region Button Event
        private void btnSaveContinue_Click(object sender, EventArgs e)
        {
            #region CheckErr
            if (!Them)
            {
                if (!_IPrepaidExpenseService.CheckDelete(_Select.ID))
                {
                    if (OldIsActive != chkIsActive.Checked)
                    {

                        var model = _IPrepaidExpenseService.Getbykey(_Select.ID);
                        model.IsActive = chkIsActive.Checked;
                        _IPrepaidExpenseService.BeginTran();
                        try
                        {
                            _IPrepaidExpenseService.Update(model);
                            _IPrepaidExpenseService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            _IPrepaidExpenseService.RolbackTran();
                        }
                        txtPrepaidExpenseCode.Focus();
                        _Select = new PrepaidExpense();
                        ObjandGUI(_Select, false);
                        SetDefaultValueControls();
                        return;
                    }
                    else
                    {
                        MSG.Warning("Chi phí trả trước khi đã phát sinh chứng từ Phân bổ Chi phí Trả trước. Không được phép sửa!");
                        return;
                    }
                }
            }
            foreach (var row in uGridPrepaidExpenseAllocation.Rows)
            {
                if (row.Cells["CostAccount"].Value == null || string.IsNullOrEmpty(row.Cells["CostAccount"].Value.ToString()))
                {
                    MSG.Warning("TK chi phí không được để trống!");
                    uGridPrepaidExpenseAllocation.Focus();
                    return;
                }
            }
            if (txtPrepaidExpenseCode.Text == "" || string.IsNullOrEmpty(txtPrepaidExpenseCode.Text))
            {
                MSG.Warning("Mã CP trả trước không thể để trống!");
                return;
            }
            if (txtPrepaidExpenseName.Text == "" || string.IsNullOrEmpty(txtPrepaidExpenseName.Text))
            {
                MSG.Warning("Tên CP trả trước không thể để trống!");
                return;
            }
            if ((int)txtAllocationTime.Value == 0)
            {
                MSG.Warning("Vui lòng nhập số kỳ phân bổ!");
                return;
            }
            if ((double)txtAmount.Value == 0)
            {
                MSG.Warning("Vui lòng nhập tổng số tiền!");
                return;
            }
            if (Them && _IPrepaidExpenseService.GetAll().Any(x => x.PrepaidExpenseCode == txtPrepaidExpenseCode.Text))
            {
                MSG.Warning("Mã CP trả trước đã tồn tại!");
                return;
            }
            if (Type == 0 && (DateTime)dteDate.Value > Utils.ListSystemOption.FirstOrDefault(x => x.ID == 104).Data.StringToDateTime())
            {
                MSG.Warning("Ngày ghi nhận phải nhỏ hơn ngày bắt đầu hạch toán trên phần mềm!");
                return;
            }
            if (((BindingList<PrepaidExpenseAllocation>)uGridPrepaidExpenseAllocation.DataSource).Sum(x => x.AllocationRate) != 100)
            {
                MSG.Warning("Tỷ lệ phân bổ không được vượt quá 100%!");
                return;
            }
            if (Convert.ToDecimal(txtAmount.Text) != ((BindingList<PrepaidExpenseVoucher>)uGridPrepaidExpenseVoucher.DataSource).Sum(x => x.Amount))
            {
                if (MSG.Question("Số tiền trên nguồn gốc hình thành không bằng Tổng số tiền của chi phí trả trước. Bạn có muốn tiếp tục lưu không?") == System.Windows.Forms.DialogResult.No) return;
            }

            #endregion

            #region Fill dữ liệu control vào obj
            PrepaidExpense temp = Them ? new PrepaidExpense() : _IPrepaidExpenseService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IPrepaidExpenseService.BeginTran();
            try
            {
                if (Them)
                {
                    _IPrepaidExpenseService.CreateNew(temp);
                }
                else
                {
                    _IPrepaidExpenseService.Update(temp);
                }
                _IPrepaidExpenseService.CommitTran();
            }
            catch (Exception ex)
            {
                _IPrepaidExpenseService.RolbackTran();
            }
            #endregion

            #region reset form
            txtPrepaidExpenseCode.Focus();
            _Select = new PrepaidExpense();
            ObjandGUI(_Select, false);
            SetDefaultValueControls();
            #endregion
        }
        private void SetDefaultValueControls()
        {
            chkIsActive.Checked = false;
            txtPrepaidExpenseCode.Enabled = true;
            txtPrepaidExpenseCode.Text = txtPrepaidExpenseCode.NullText;
            txtPrepaidExpenseName.Text = txtPrepaidExpenseName.NullText;
            txtAmount.Text = txtAmount.NullText;
            txtAllocatedPeriod.Text = txtAllocatedPeriod.NullText;
            txtAllocatedAmount.Text = txtAllocatedAmount.NullText;
            txtAllocationAmount.Text = txtAllocationAmount.NullText;
            txtAllocationTime.Text = txtAllocationTime.NullText;
            dteDate.Value = DateTime.Now;
            cbbAccount.Text = cbbAccount.NullText;
        }
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region CheckErr
            if (!Them)
            {
                if (!_IPrepaidExpenseService.CheckDelete(_Select.ID))
                {
                    if (OldIsActive != chkIsActive.Checked)
                    {

                        var model = _IPrepaidExpenseService.Getbykey(_Select.ID);
                        model.IsActive = chkIsActive.Checked;
                        _IPrepaidExpenseService.BeginTran();
                        try
                        {
                            _IPrepaidExpenseService.Update(model);
                            _IPrepaidExpenseService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            _IPrepaidExpenseService.RolbackTran();
                        }
                        isClose = false;
                        Close();
                        return;
                    }
                    else
                    {
                        MSG.Warning("Chi phí trả trước khi đã phát sinh chứng từ Phân bổ Chi phí Trả trước. Không được phép sửa!");
                        return;
                    }
                }
            }
            foreach (var row in uGridPrepaidExpenseAllocation.Rows)
            {
                if (row.Cells["CostAccount"].Value == null || string.IsNullOrEmpty(row.Cells["CostAccount"].Value.ToString()))
                {
                    MSG.Warning("TK chi phí không được để trống!");
                    uGridPrepaidExpenseAllocation.Focus();
                    return;
                }
            }
            if (txtPrepaidExpenseCode.Text == "" || string.IsNullOrEmpty(txtPrepaidExpenseCode.Text))
            {
                MSG.Warning("Mã CP trả trước không thể để trống!");
                return;
            }
            if (txtPrepaidExpenseName.Text == "" || string.IsNullOrEmpty(txtPrepaidExpenseName.Text))
            {
                MSG.Warning("Tên CP trả trước không thể để trống!");
                return;
            }
            if ((int)txtAllocationTime.Value == 0)
            {
                MSG.Warning("Vui lòng nhập số kỳ phân bổ!");
                return;
            }
            if ((double)txtAmount.Value == 0)
            {
                MSG.Warning("Vui lòng nhập tổng số tiền!");
                return;
            }
            if (dteDate.Text == "" || string.IsNullOrEmpty(dteDate.Text))
            {
                MSG.Warning("Ngày ghi nhận không được để trống!");
                return;
            }
            if (Them && _IPrepaidExpenseService.GetAll().Any(x => x.PrepaidExpenseCode == txtPrepaidExpenseCode.Text))
            {
                MSG.Warning("Mã CP trả trước đã tồn tại!");
                return;
            }
            if (Type == 0 && (DateTime)dteDate.Value > Utils.ListSystemOption.FirstOrDefault(x => x.ID == 104).Data.StringToDateTime())
            {
                MSG.Warning("Ngày ghi nhận phải nhỏ hơn ngày bắt đầu hạch toán trên phần mềm!");
                return;
            }
            if (((BindingList<PrepaidExpenseAllocation>)uGridPrepaidExpenseAllocation.DataSource).Sum(x => x.AllocationRate) != 100)
            {
                MSG.Warning("Tỷ lệ phân bổ không được vượt quá 100%!");
                return;
            }
            if (Convert.ToDecimal(txtAmount.Text) != ((BindingList<PrepaidExpenseVoucher>)uGridPrepaidExpenseVoucher.DataSource).Sum(x => x.Amount))
            {
                if (MSG.Question("Số tiền trên nguồn gốc hình thành không bằng Tổng số tiền của chi phí trả trước. Bạn có muốn tiếp tục lưu không?") == System.Windows.Forms.DialogResult.No) return;
            }
            #endregion 

            #region Fill dữ liệu control vào obj
            PrepaidExpense temp = Them ? new PrepaidExpense() : _IPrepaidExpenseService.Getbykey(_Select.ID);
            temp = ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            _IPrepaidExpenseService.BeginTran();
            try
            {
                if (Them)
                {
                    _IPrepaidExpenseService.CreateNew(temp);
                }
                else
                {
                    _IPrepaidExpenseService.Update(temp);
                }
                _IPrepaidExpenseService.CommitTran();
            }
            catch (Exception ex)
            {
                _IPrepaidExpenseService.RolbackTran();
            }
            #endregion

            #region xử lý form, kết thúc form
            isClose = false;
            Close();
            #endregion
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            isClose = true;
            Close();
        }
        private void cbbReponsitoryAccount_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbAccount.Text != b.AccountNumber && cbbAccount.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbAccount.Focus();
                return;
            }
        }
        private void tsmDelete_Click(object sender, EventArgs e)
        {
            UltraGridCell cell = uGridPrepaidExpenseAllocation.ActiveCell;
            if (uGridPrepaidExpenseAllocation.ActiveCell != null)
            {
                UltraGridRow row = null;
                if (uGridPrepaidExpenseAllocation.ActiveCell != null || uGridPrepaidExpenseAllocation.ActiveRow != null)
                {
                    row = uGridPrepaidExpenseAllocation.ActiveCell != null ? uGridPrepaidExpenseAllocation.ActiveCell.Row : uGridPrepaidExpenseAllocation.ActiveRow;
                }
                else
                {
                    if (uGridPrepaidExpenseAllocation.Rows.Count > 0) row = uGridPrepaidExpenseAllocation.Rows[uGridPrepaidExpenseAllocation.Rows.Count - 1];
                }
                if (row != null) row.Delete(false);
                uGridPrepaidExpenseAllocation.Update();
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGridPrepaidExpenseAllocation.AddNewRow4Grid();
        }

        private void txtAmount_ValueChanged(object sender, EventArgs e)
        {
            decimal amount = 0;
            int soky = 0;
            int sokydapb = 0;
            if (txtAmount.Text != null && !string.IsNullOrEmpty(txtAmount.Text)) amount = Convert.ToDecimal(txtAmount.Text);
            if (txtAllocationTime.Text != null && !string.IsNullOrEmpty(txtAllocationTime.Text)) soky = (int)txtAllocationTime.Value;
            if (txtAllocatedPeriod.Text != null && !string.IsNullOrEmpty(txtAllocatedPeriod.Text)) sokydapb = (int)txtAllocatedPeriod.Value;
            txtAllocatedAmount.Value = soky == 0 ? 0 : (amount / soky);
            txtAllocationAmount.Value = soky == 0 ? 0 : (amount / soky) * sokydapb;
        }

        private void txtAllocationTime_ValueChanged(object sender, EventArgs e)
        {
            decimal amount = 0;
            int soky = 0;
            int sokydapb = 0;
            if (txtAmount.Text != null && !string.IsNullOrEmpty(txtAmount.Text)) amount = Convert.ToDecimal(txtAmount.Text);
            if (txtAllocationTime.Text != null && !string.IsNullOrEmpty(txtAllocationTime.Text)) soky = (int)txtAllocationTime.Value;
            if (txtAllocatedPeriod.Text != null && !string.IsNullOrEmpty(txtAllocatedPeriod.Text)) sokydapb = (int)txtAllocatedPeriod.Value;
            txtAllocatedAmount.Value = soky == 0 ? 0 : (amount / soky);
            txtAllocationAmount.Value = soky == 0 ? 0 : (amount / soky) * sokydapb;
        }

        private void txtAllocatedPeriod_ValueChanged(object sender, EventArgs e)
        {
            decimal amount = 0;
            int soky = 0;
            int sokydapb = 0;
            if (txtAmount.Text != null && !string.IsNullOrEmpty(txtAmount.Text)) amount = Convert.ToDecimal(txtAmount.Text);
            if (txtAllocationTime.Text != null && !string.IsNullOrEmpty(txtAllocationTime.Text)) soky = (int)txtAllocationTime.Value;
            if (txtAllocatedPeriod.Text != null && !string.IsNullOrEmpty(txtAllocatedPeriod.Text)) sokydapb = (int)txtAllocatedPeriod.Value;
            txtAllocationAmount.Value = soky == 0 ? 0 : (amount / soky) * sokydapb;
        }

        private void uGridPrepaidExpenseAllocation_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "AllocationObjectID")
            {
                var combo = (UltraCombo)e.Cell.ValueListResolved;
                var model = (Objects)combo.SelectedRow.ListObject;
                e.Cell.Row.Cells["AllocationObjectName"].Value = model.ObjectName;
                e.Cell.Row.Cells["AllocationObjectType"].Value = model.ObjectType;
            }
            if (e.Cell.Column.Key == "AllocationRate")
            {
                //decimal rate = 0;
                //foreach (var row in uGridPrepaidExpenseAllocation.Rows)
                //{
                //    rate = rate + (decimal)row.Cells["AllocationRate"].Value;
                //}
                //if (rate > 100)
                //{
                //    MSG.Warning("Tỷ lệ phân bổ không được vượt quá 100%!");
                //    e.Cell.Value = 0;
                //}
            }
            if (e.Cell.Column.Key.Equals("CostAccount"))
            {
                if (e.Cell.Text != "" && !string.IsNullOrEmpty(e.Cell.Text))
                {
                    int count = Utils.ListAccount.Count(x => x.AccountNumber == e.Cell.Text);
                    if (count == 0)
                    {
                        MSG.Error("Dữ liệu không có trong danh mục");
                        e.Cell.Value = null;
                    }
                    else
                    {
                        bool isError = Utils.ListAccount.Where(x => x.AccountNumber == (string)e.Cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                        if (isError)
                        {
                            e.Cell.Value = null;
                            MSG.Error(resSystem.MSG_System_29);
                        }
                    }
                    Utils.RemoveNotificationCell(uGridPrepaidExpenseAllocation, e.Cell);
                }
                else
                {
                    Utils.NotificationCell(uGridPrepaidExpenseAllocation, e.Cell, "TK chi phí không được để trống!");
                }
            }
            else if (e.Cell.Column.Key.Equals("ExpenseItemID"))
            {
                bool isError = Utils.ListExpenseItem.Where(x => x.ID == (Guid?)e.Cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                if (isError)
                {
                    e.Cell.Value = null;
                    MSG.Error(resSystem.MSG_System_29);
                }
            }
        }
        private void uGridPrepaidExpenseAllocation_Error(object sender, ErrorEventArgs e)
        {
            UltraGridCell cell = uGridPrepaidExpenseAllocation.ActiveCell;
            if (uGridPrepaidExpenseAllocation.ActiveCell == null) return;
            if (cell.Column.Key.Equals("AllocationObjectID"))
            {
                int count = Utils.ListObjects.Count(x => x.ObjectCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
            }
            else if (cell.Column.Key.Equals("ExpenseItemID"))
            {
                int count = Utils.ListExpenseItem.Count(x => x.ExpenseItemCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
                else
                {
                    bool isError = Utils.ListExpenseItem.Where(x => x.ID == (Guid?)cell.Value && x.IsActive).Select(x => x.IsParentNode).FirstOrDefault();
                    if (isError)
                        MSG.Error(resSystem.MSG_System_29);
                }
            }

            e.Cancel = true;
        }
        //private void uGridPrepaidExpenseAllocation_Leave(object sender, EventArgs e)
        //{
        //    foreach (var row in uGridPrepaidExpenseAllocation.Rows)
        //    {
        //        if (row.Cells["CostAccount"].Value == null || string.IsNullOrEmpty(row.Cells["CostAccount"].Value.ToString()))
        //        {
        //            Utils.NotificationCell(uGridPrepaidExpenseAllocation, row.Cells["CostAccount"], "TK chi phí không được để trống!");
        //            uGridPrepaidExpenseAllocation.Focus();
        //        }
        //        else
        //        {
        //            Utils.RemoveNotificationCell(uGridPrepaidExpenseAllocation, row.Cells["CostAccount"]);
        //        }
        //    }
        //}
        private void btnApportion_Click(object sender, EventArgs e)
        {
            var lst = ((BindingList<PrepaidExpenseVoucher>)uGridPrepaidExpenseVoucher.DataSource).ToList();
            List<TIOriginalVoucher> datasource = new List<TIOriginalVoucher>();
            foreach (var x in lst)
            {
                TIOriginalVoucher model = new TIOriginalVoucher();
                model.Date = x.Date;
                model.No = x.No;
                model.PostedDate = x.Date;
                model.Reason = x.Reason;
                model.Amount = x.Amount;
                model.ID = x.ID;
                datasource.Add(model);
            }
            var f = new FPEOriginalVoucher(datasource);
            f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            List<PrepaidExpenseVoucher> lst = new List<PrepaidExpenseVoucher>();
            var f = (FPEOriginalVoucher)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    foreach (var x in f.TIOriginalVoucher)
                    {
                        PrepaidExpenseVoucher model = new PrepaidExpenseVoucher();
                        model.Amount = x.Amount;
                        model.No = x.No;
                        model.Date = x.Date;
                        model.Reason = x.Reason;
                        lst.Add(model);
                    }
                }
            }
            uGridPrepaidExpenseVoucher.DataSource = new BindingList<PrepaidExpenseVoucher>(lst);
        }
        #endregion
        #endregion

        #region Utils   
        void LoadDuLieu()
        {
            LoadDuLieu(true);
            WaitingFrm.StopWaiting();
        }

        private void LoadDuLieu(bool configGrid)
        {
            cbbAccount.DataSource = Utils.ListAccount.Where(p => p.IsActive == true).ToList();
            cbbAccount.ValueMember = "AccountNumber";
            cbbAccount.DisplayMember = "AccountNumber";
            Utils.ConfigGrid(cbbAccount, ConstDatabase.Account_TableName);

            uGridPrepaidExpenseAllocation.DataSource = new BindingList<PrepaidExpenseAllocation>(new List<PrepaidExpenseAllocation>());
            uGridPrepaidExpenseAllocation.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGridPrepaidExpenseAllocation.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGridPrepaidExpenseAllocation.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //Hiện những dòng trống?
            uGridPrepaidExpenseAllocation.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPrepaidExpenseAllocation.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGridPrepaidExpenseAllocation.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPrepaidExpenseAllocation.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tắt tiêu đề
            uGridPrepaidExpenseAllocation.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            CreaterColumsStyle(uGridPrepaidExpenseAllocation);
            Utils.ConfigGrid(uGridPrepaidExpenseAllocation, ConstDatabase.PrepaidExpenseAllocation_TableName, new List<TemplateColumn>(), true);

            uGridPrepaidExpenseVoucher.DataSource = new BindingList<PrepaidExpenseVoucher>(new List<PrepaidExpenseVoucher>());
            Utils.ConfigGrid(uGridPrepaidExpenseVoucher, ConstDatabase.PrepaidExpenseVoucher_TableName);
            uGridPrepaidExpenseVoucher.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
        }
        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            #region Grid động
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                this.ConfigEachColumn4Grid(0, item, uGrid);
            }
            #endregion
        }
        PrepaidExpense ObjandGUI(PrepaidExpense input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();//them moi id
                input.TypeExpense = Type;
                input.PrepaidExpenseCode = txtPrepaidExpenseCode.Text;
                input.PrepaidExpenseName = txtPrepaidExpenseName.Text;
                input.Date = ((DateTime)dteDate.Value).Date;
                input.Amount = Convert.ToDecimal(txtAmount.Text);
                input.AllocatedPeriod = Type == 0 ? Convert.ToInt32(txtAllocatedPeriod.Text) : 0;
                input.AllocatedAmount = Convert.ToDecimal(txtAllocatedAmount.Text);
                input.AllocationAmount = Type == 0 ? Convert.ToDecimal(txtAllocationAmount.Text) : 0;
                input.AllocationTime = Convert.ToInt32(txtAllocationTime.Text);
                input.AllocationAccount = cbbAccount.Text;
                input.IsActive = chkIsActive.Checked;
                #region PrepaidExpenseDetail                
                List<PrepaidExpenseAllocation> prepaidExpenseAllocations = ((BindingList<PrepaidExpenseAllocation>)uGridPrepaidExpenseAllocation.DataSource).ToList();
                input.PrepaidExpenseAllocations.Clear();
                foreach (PrepaidExpenseAllocation item in prepaidExpenseAllocations)
                {
                    item.PrepaidExpenseID = input.ID;
                    input.PrepaidExpenseAllocations.Add(item);
                }
                List<PrepaidExpenseVoucher> prepaidExpenseVouchers = ((BindingList<PrepaidExpenseVoucher>)uGridPrepaidExpenseVoucher.DataSource).ToList();
                input.PrepaidExpenseVouchers.Clear();
                foreach (PrepaidExpenseVoucher item in prepaidExpenseVouchers)
                {
                    item.PrepaidExpenseID = input.ID;
                    input.PrepaidExpenseVouchers.Add(item);
                }
                #endregion
            }
            else
            {
                txtPrepaidExpenseCode.Text = input.PrepaidExpenseCode;
                txtPrepaidExpenseName.Text = input.PrepaidExpenseName;
                dteDate.Value = input.Date;
                txtAmount.Text = input.Amount.ToString();
                txtAllocatedPeriod.Text = input.AllocatedPeriod.ToString();
                txtAllocationAmount.Text = input.AllocationAmount.ToString();
                txtAllocationTime.Text = input.AllocationTime.ToString();
                txtAllocatedAmount.Text = input.AllocatedAmount.ToString();

                foreach (var item in cbbAccount.Rows)
                {
                    if ((item.ListObject as Account).AccountNumber == input.AllocationAccount) cbbAccount.SelectedRow = item;
                }
                chkIsActive.Checked = input.IsActive;
                BindingList<PrepaidExpenseAllocation> PrepaidExpenseAllocation = new BindingList<PrepaidExpenseAllocation>(input.PrepaidExpenseAllocations);
                uGridPrepaidExpenseAllocation.DataSource = PrepaidExpenseAllocation;


                BindingList<PrepaidExpenseVoucher> PrepaidExpenseVoucher = new BindingList<PrepaidExpenseVoucher>(input.PrepaidExpenseVouchers);
                uGridPrepaidExpenseVoucher.DataSource = PrepaidExpenseVoucher;
                uGridPrepaidExpenseVoucher.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            }
            return input;
        }


        #endregion
    }
}
