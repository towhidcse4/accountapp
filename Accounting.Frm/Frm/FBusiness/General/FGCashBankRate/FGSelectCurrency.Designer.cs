﻿namespace Accounting
{
    partial class FGSelectCurrency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ultraPanelGrid = new Infragistics.Win.Misc.UltraPanel();
            this.uGridDanhSach = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uBtnDongY = new Infragistics.Win.Misc.UltraButton();
            this.uBtnHuyBo = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelGrid.ClientArea.SuspendLayout();
            this.ultraPanelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanelGrid
            // 
            // 
            // ultraPanelGrid.ClientArea
            // 
            this.ultraPanelGrid.ClientArea.Controls.Add(this.uGridDanhSach);
            this.ultraPanelGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelGrid.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelGrid.Name = "ultraPanelGrid";
            this.ultraPanelGrid.Size = new System.Drawing.Size(545, 342);
            this.ultraPanelGrid.TabIndex = 0;
            // 
            // uGridDanhSach
            // 
            this.uGridDanhSach.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDanhSach.Location = new System.Drawing.Point(0, 0);
            this.uGridDanhSach.Name = "uGridDanhSach";
            this.uGridDanhSach.Size = new System.Drawing.Size(545, 342);
            this.uGridDanhSach.TabIndex = 40;
            this.uGridDanhSach.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uBtnDongY
            // 
            this.uBtnDongY.Location = new System.Drawing.Point(353, 361);
            this.uBtnDongY.Name = "uBtnDongY";
            this.uBtnDongY.Size = new System.Drawing.Size(75, 23);
            this.uBtnDongY.TabIndex = 1;
            this.uBtnDongY.Text = "Đồng ý";
            this.uBtnDongY.Click += new System.EventHandler(this.uBtnDongY_Click);
            // 
            // uBtnHuyBo
            // 
            this.uBtnHuyBo.Location = new System.Drawing.Point(458, 361);
            this.uBtnHuyBo.Name = "uBtnHuyBo";
            this.uBtnHuyBo.Size = new System.Drawing.Size(75, 23);
            this.uBtnHuyBo.TabIndex = 2;
            this.uBtnHuyBo.Text = "Hủy bỏ";
            this.uBtnHuyBo.Click += new System.EventHandler(this.uBtnHuyBo_Click);
            // 
            // FGSelectCurrency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 396);
            this.Controls.Add(this.uBtnHuyBo);
            this.Controls.Add(this.uBtnDongY);
            this.Controls.Add(this.ultraPanelGrid);
            this.Name = "FGSelectCurrency";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Chọn loại tiền tính tỷ giá xuất quỹ";
            this.TopMost = true;
            this.ultraPanelGrid.ClientArea.ResumeLayout(false);
            this.ultraPanelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanelGrid;
        private Infragistics.Win.Misc.UltraButton uBtnDongY;
        private Infragistics.Win.Misc.UltraButton uBtnHuyBo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDanhSach;

    }
}