﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGSelectCurrency : CustormForm
    {
        public List<Currency> CurrencySelected { get; set; }
        List<Currency> listCurrencies = new List<Currency>();
        public FGSelectCurrency(List<Currency> listCurrenciesInput)
        {
            ICurrencyService currencySrv = IoC.Resolve<ICurrencyService>();
            InitializeComponent();
            listCurrencies = Utils.CloneObject(currencySrv.GetIsActive(true).Where(x=>x.ID != "VND").ToList());
            if (listCurrenciesInput != null && listCurrenciesInput.Count > 0)
            {
                foreach (Currency currency in listCurrencies)
                {
                    if (listCurrenciesInput.Count(c => c.ID == currency.ID) == 1)
                    {
                        currency.Status = true;
                    }
                }

            }
            uGridDanhSach.SetDataBinding(listCurrencies, "");
            Utils.ConfigGrid(uGridDanhSach, ConstDatabase.FGSelectCurrency_FormName);
            uGridDanhSach.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridDanhSach.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridDanhSach.DisplayLayout.Bands[0].Summaries.Clear();
            uGridDanhSach.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGridDanhSach.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
        }

        private void uBtnHuyBo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void uBtnDongY_Click(object sender, EventArgs e)
        {
            CurrencySelected = listCurrencies.Where(c => c.Status).ToList();
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
