﻿namespace Accounting
{
    partial class FGCashBankRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            this.ultraPanelTop = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.btnThucHien = new Infragistics.Win.Misc.UltraButton();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanelBottom = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBoxBottom = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanelCenter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBoxCenter = new Infragistics.Win.Misc.UltraGroupBox();
            this.udYear = new System.Windows.Forms.NumericUpDown();
            this.cbbMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.btnSelect = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.optAccountingObjectType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraPanelTop.ClientArea.SuspendLayout();
            this.ultraPanelTop.SuspendLayout();
            this.ultraPanelBottom.ClientArea.SuspendLayout();
            this.ultraPanelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).BeginInit();
            this.ultraGroupBoxBottom.SuspendLayout();
            this.ultraPanelCenter.ClientArea.SuspendLayout();
            this.ultraPanelCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxCenter)).BeginInit();
            this.ultraGroupBoxCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraPanelTop
            // 
            // 
            // ultraPanelTop.ClientArea
            // 
            this.ultraPanelTop.ClientArea.Controls.Add(this.ultraLabel5);
            this.ultraPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelTop.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelTop.Name = "ultraPanelTop";
            this.ultraPanelTop.Size = new System.Drawing.Size(400, 47);
            this.ultraPanelTop.TabIndex = 0;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(13, 14);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(375, 27);
            this.ultraLabel5.TabIndex = 0;
            // 
            // btnThucHien
            // 
            this.btnThucHien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnThucHien.Appearance = appearance1;
            this.btnThucHien.Location = new System.Drawing.Point(199, 4);
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Size = new System.Drawing.Size(94, 27);
            this.btnThucHien.TabIndex = 2;
            this.btnThucHien.Text = "Thực hiện";
            this.btnThucHien.Click += new System.EventHandler(this.btnThucHien_Click);
            // 
            // btnKetThuc
            // 
            this.btnKetThuc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnKetThuc.Appearance = appearance2;
            this.btnKetThuc.Location = new System.Drawing.Point(299, 4);
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Size = new System.Drawing.Size(88, 27);
            this.btnKetThuc.TabIndex = 3;
            this.btnKetThuc.Text = "Kết thúc";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // ultraPanelBottom
            // 
            // 
            // ultraPanelBottom.ClientArea
            // 
            this.ultraPanelBottom.ClientArea.Controls.Add(this.ultraGroupBoxBottom);
            this.ultraPanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanelBottom.Location = new System.Drawing.Point(0, 135);
            this.ultraPanelBottom.Name = "ultraPanelBottom";
            this.ultraPanelBottom.Size = new System.Drawing.Size(400, 38);
            this.ultraPanelBottom.TabIndex = 2;
            // 
            // ultraGroupBoxBottom
            // 
            appearance3.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBoxBottom.Appearance = appearance3;
            this.ultraGroupBoxBottom.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded;
            this.ultraGroupBoxBottom.Controls.Add(this.btnKetThuc);
            this.ultraGroupBoxBottom.Controls.Add(this.btnThucHien);
            this.ultraGroupBoxBottom.Location = new System.Drawing.Point(4, 3);
            this.ultraGroupBoxBottom.Name = "ultraGroupBoxBottom";
            this.ultraGroupBoxBottom.Size = new System.Drawing.Size(393, 36);
            this.ultraGroupBoxBottom.TabIndex = 1;
            // 
            // ultraPanelCenter
            // 
            // 
            // ultraPanelCenter.ClientArea
            // 
            this.ultraPanelCenter.ClientArea.Controls.Add(this.ultraGroupBoxCenter);
            this.ultraPanelCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanelCenter.Location = new System.Drawing.Point(0, 47);
            this.ultraPanelCenter.Name = "ultraPanelCenter";
            this.ultraPanelCenter.Size = new System.Drawing.Size(400, 88);
            this.ultraPanelCenter.TabIndex = 1;
            // 
            // ultraGroupBoxCenter
            // 
            this.ultraGroupBoxCenter.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Standard;
            appearance4.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance4.BorderColor = System.Drawing.Color.Navy;
            this.ultraGroupBoxCenter.Appearance = appearance4;
            this.ultraGroupBoxCenter.Controls.Add(this.udYear);
            this.ultraGroupBoxCenter.Controls.Add(this.cbbMonth);
            this.ultraGroupBoxCenter.Controls.Add(this.btnSelect);
            this.ultraGroupBoxCenter.Controls.Add(this.ultraLabel3);
            this.ultraGroupBoxCenter.Controls.Add(this.ultraLabel2);
            this.ultraGroupBoxCenter.Controls.Add(this.optAccountingObjectType);
            this.ultraGroupBoxCenter.Location = new System.Drawing.Point(12, 6);
            this.ultraGroupBoxCenter.Name = "ultraGroupBoxCenter";
            this.ultraGroupBoxCenter.Size = new System.Drawing.Size(376, 74);
            this.ultraGroupBoxCenter.TabIndex = 0;
            // 
            // udYear
            // 
            this.udYear.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udYear.Location = new System.Drawing.Point(154, 42);
            this.udYear.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.udYear.Minimum = new decimal(new int[] {
            1990,
            0,
            0,
            0});
            this.udYear.Name = "udYear";
            this.udYear.Size = new System.Drawing.Size(75, 21);
            this.udYear.TabIndex = 77;
            this.udYear.Value = new decimal(new int[] {
            1990,
            0,
            0,
            0});
            // 
            // cbbMonth
            // 
            this.cbbMonth.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append;
            this.cbbMonth.Location = new System.Drawing.Point(57, 42);
            this.cbbMonth.Name = "cbbMonth";
            this.cbbMonth.Size = new System.Drawing.Size(54, 21);
            this.cbbMonth.TabIndex = 76;
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.Location = new System.Drawing.Point(272, 9);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(40, 21);
            this.btnSelect.TabIndex = 75;
            this.btnSelect.Text = "...";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(117, 42);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(31, 21);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Năm";
            // 
            // ultraLabel2
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance6;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 42);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(39, 21);
            this.ultraLabel2.TabIndex = 64;
            this.ultraLabel2.Text = "Tháng";
            // 
            // optAccountingObjectType
            // 
            this.optAccountingObjectType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.optAccountingObjectType.Appearance = appearance7;
            this.optAccountingObjectType.BackColor = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optAccountingObjectType.CheckedIndex = 0;
            this.optAccountingObjectType.ItemOrigin = new System.Drawing.Point(5, 0);
            valueListItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem4.DataValue = ((short)(0));
            valueListItem4.DisplayText = "Tất cả các ngoại tệ";
            valueListItem1.DataValue = ((short)(1));
            valueListItem1.DisplayText = "Chọn ngoại tệ";
            this.optAccountingObjectType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem4,
            valueListItem1});
            this.optAccountingObjectType.ItemSpacingHorizontal = 20;
            this.optAccountingObjectType.Location = new System.Drawing.Point(6, 12);
            this.optAccountingObjectType.Name = "optAccountingObjectType";
            this.optAccountingObjectType.Size = new System.Drawing.Size(245, 24);
            this.optAccountingObjectType.TabIndex = 29;
            this.optAccountingObjectType.Text = "Tất cả các ngoại tệ";
            this.optAccountingObjectType.ValueChanged += new System.EventHandler(this.optAccountingObjectType_ValueChanged);
            // 
            // FGCashBankRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 173);
            this.Controls.Add(this.ultraPanelBottom);
            this.Controls.Add(this.ultraPanelCenter);
            this.Controls.Add(this.ultraPanelTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FGCashBankRate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tính tỷ giá xuất quỹ";
            this.ultraPanelTop.ClientArea.ResumeLayout(false);
            this.ultraPanelTop.ResumeLayout(false);
            this.ultraPanelBottom.ClientArea.ResumeLayout(false);
            this.ultraPanelBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxBottom)).EndInit();
            this.ultraGroupBoxBottom.ResumeLayout(false);
            this.ultraPanelCenter.ClientArea.ResumeLayout(false);
            this.ultraPanelCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxCenter)).EndInit();
            this.ultraGroupBoxCenter.ResumeLayout(false);
            this.ultraGroupBoxCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optAccountingObjectType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanelTop;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraButton btnThucHien;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
        private Infragistics.Win.Misc.UltraPanel ultraPanelBottom;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxBottom;
        private Infragistics.Win.Misc.UltraPanel ultraPanelCenter;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxCenter;
        private Infragistics.Win.Misc.UltraButton btnSelect;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optAccountingObjectType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbMonth;
        private System.Windows.Forms.NumericUpDown udYear;
    }
}