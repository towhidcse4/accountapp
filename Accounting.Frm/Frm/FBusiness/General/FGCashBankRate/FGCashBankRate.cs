﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Itenso.TimePeriod;

namespace Accounting
{
    public partial class FGCashBankRate : CustormForm
    {
        private readonly IGeneralLedgerService _generalLedgerService;
        private readonly ICurrencyService _currencySrv;
        private readonly IGOtherVoucherService _IGOtherVoucherService;

        readonly DateTime _ngayHoachToan = Utils.StringToDateTime(Utils.GetDbStartDate()) ?? DateTime.Now;

        private List<Currency> _lstCurrencys;
        public FGCashBankRate()
        {
            _generalLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _currencySrv = IoC.Resolve<ICurrencyService>();
            _IGOtherVoucherService = IoC.Resolve<IGOtherVoucherService>();
            InitializeComponent();
            btnSelect.Enabled = false;
            ultraLabel5.Text = string.Format("Phương pháp tính: {0}", Utils.ListSystemOption.FirstOrDefault(x => x.ID == 89).Data);
            cbbMonth.DataSource = new List<string> { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            foreach (ValueListItem item in cbbMonth.Items)
            {
                if ((string)item.DataValue == (_ngayHoachToan.Month < 10 ? "0" + _ngayHoachToan.Month : _ngayHoachToan.Month.ToString(CultureInfo.InvariantCulture)))
                {
                    cbbMonth.SelectedItem = item;
                    break;
                }
            }
            udYear.Value = _ngayHoachToan.Year;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            FGSelectCurrency frm = new FGSelectCurrency(_lstCurrencys);
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                _lstCurrencys = frm.CurrencySelected;
            }
        }
        private void optAccountingObjectType_ValueChanged(object sender, EventArgs e)
        {

            if (optAccountingObjectType.CheckedIndex == 0)
                btnSelect.Enabled = false;
            else if (optAccountingObjectType.CheckedIndex == 1)
            {
                btnSelect.Enabled = true;
                btnSelect.PerformClick();
            }
        }

        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 89).Data == "Bình quân cuối kỳ")
            {
                if (optAccountingObjectType.CheckedIndex == 0)
                    _lstCurrencys = _currencySrv.GetIsActive(true);
                if ((_lstCurrencys != null) && (_lstCurrencys.Count > 0))
                {
                    DateTime dt = new DateTime((int)udYear.Value, int.Parse((string)cbbMonth.SelectedItem.ListObject), DateTime.DaysInMonth((int)udYear.Value, int.Parse((string)cbbMonth.SelectedItem.ListObject)));
                    if (!_IGOtherVoucherService.GetGOtherVoucherGetByDate(dt))
                    {
                        MSG.Warning("Kỳ tính tỷ giá xuất quỹ này đã phát sinh chứng từ nghiệp vụ xử lý chênh lệch tỷ giá. " +
                            "\nVui lòng xóa chứng từ xử lý này trước khi thực hiện tính tỷ giá xuất quỹ của kỳ này!");
                    }
                    else
                    {
                        GOtherVoucher gOtherVoucher = _generalLedgerService.FormulaExchangeRate_AverageForEndOfPeriods(_lstCurrencys, dt);
                        if (gOtherVoucher.GOtherVoucherDetails.Count <= 0)
                        {
                            MessageBox.Show("Không có phát sinh chênh lệch tỷ giá!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        var frm = new FGOtherVoucherDetail(gOtherVoucher, _IGOtherVoucherService.GetAll(), ConstFrm.optStatusForm.Add);
                        frm.ShowDialogHD(this);
                    }
                }
                else
                {
                    MessageBox.Show("Chưa chọn loại tiền nào để tính", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
    }
}
