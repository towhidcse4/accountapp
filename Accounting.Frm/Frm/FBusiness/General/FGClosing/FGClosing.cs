﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGClosing : CustormForm
    {
        private readonly IAccountService _accountSrv = IoC.Resolve<IAccountService>();
        private readonly IGenCodeService _genCodeSrv = IoC.Resolve<IGenCodeService>();
        private readonly ICurrencyService _currencySrv = IoC.Resolve<ICurrencyService>();
        private readonly IAccountingObjectService _accountingObjectSrv = IoC.Resolve<IAccountingObjectService>();
        private readonly IContractStateService _contractStateSrv = IoC.Resolve<IContractStateService>();
        private readonly IStatisticsCodeService _statisticsCodeSrv = IoC.Resolve<IStatisticsCodeService>();
        public FGClosing()
        {
            InitializeComponent();
            uGridSoDuNt.DataSource = new List<ClosingReal>();
            ViewReal(uGridSoDuNt);
        }


        #region Tạo dữ liệu test
        //public List<SolveExchangeRate> SolveChangeRateDatabaseTest()
        //{
        //    return new List<SolveExchangeRate>
        //               {
        //                   new SolveExchangeRate
        //                       {
        //                           AccountNumber = "515",
        //                           Date = DateTime.Now,
        //                           VoucherNo = "NVK0001",
        //                           InvoiceNo = "000007",
        //                           AmountBanlace = 5000000,
        //                           AmountDeficit = 0,
        //                           AccountingObjectId = new Guid("C468982E-0051-46B4-AA81-0927996CFA13"),
        //                           ContractStateId = 28,
        //                           StatisticsCodeId = new Guid("BDF11AEF-6193-447E-A303-1CBEE72C9146")
        //                       },
        //                       new SolveExchangeRate
        //                       {
        //                           AccountNumber = "4131",
        //                           Date = DateTime.Now,
        //                           VoucherNo = "NVK0003",
        //                           InvoiceNo = "000008",
        //                           AmountBanlace = 0,
        //                           AmountDeficit = 10000000,
        //                           AccountingObjectId = new Guid("1F3D989A-897C-4239-96A7-207DDBBED1FA"),
        //                           ContractStateId = 25,
        //                           StatisticsCodeId = new Guid("5CFC063E-951D-44AA-9AFC-DFBD3CE6F94F")
        //                       }
        //               };
        //}
        #endregion
        #region Cấu hình các Grid
        private void ViewReal(UltraGrid ultraGrid)
        {
            //Utils.ConfigGrid(ultraGrid, ConstDatabase.FGSolveExchangeRate_FormName);
            ultraGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            ultraGrid.Text = "";
            // xét style cho các cột có dạng checkbox
            ultraGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = ultraGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            band.Columns["Status"].CellActivation = Activation.AllowEdit;
            band.Columns["Status"].Header.Fixed = true;

            UltraGridGroup group0 = band.Groups.Add("group0");
            group0.Header.Caption = "";

            UltraGridGroup group1 = band.Groups.Add("group1");
            group1.Header.Caption = "Dư nợ";

            UltraGridGroup group2 = band.Groups.Add("group2");
            group2.Header.Caption = "Dư có";


            foreach (var item in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Contains("Status"))
                {
                    item.Group = group0;
                    item.Header.Caption = "";
                }
                if (item.Key.Contains("AccountNumber"))
                {
                    item.Group = group0;
                    item.Header.Caption = "Tài khoản";
                }
                if (item.Key.Contains("AccountBank"))
                {
                    item.Group = group0;
                    item.Header.Caption = "TK Ngân hàng";
                }
                if (item.Key.Contains("AccountingObjectId"))
                {
                    this.ConfigCbbToGrid(ultraGrid, item.Key, _accountingObjectSrv.GetAll(), "AccountingObjectId", "AccountingObjectCode", ConstDatabase.AccountingObject_TableName);
                    item.Group = group0;
                    item.Header.Caption = "Đối tượng";
                }
                if (item.Key.Contains("DebitAmountOriginal"))
                {
                    item.Group = group1;
                    item.Header.Caption = "Nguyên tệ";
                }
                if (item.Key.Contains("DebitAmount"))
                {
                    item.Group = group1;
                    item.Header.Caption = "Quy đổi";
                }
                if (item.Key.Contains("DebitRealAmount"))
                {
                    item.Group = group1;
                    item.Header.Caption = "Đánh giá lại";
                }
                if (item.Key.Contains("DebitNotMatchAmount"))
                {
                    item.Group = group1;
                    item.Header.Caption = "Chênh lệch";
                }
                if (item.Key.Contains("CreditAmountOriginal"))
                {
                    item.Group = group2;
                    item.Header.Caption = "Nguyên tệ";
                }
                if (item.Key.Contains("CreditAmount"))
                {
                    item.Group = group2;
                    item.Header.Caption = "Quy đổi";
                }
                if (item.Key.Contains("CreditRealAmount"))
                {
                    item.Group = group2;
                    item.Header.Caption = "Đánh giá lại";
                }
                if (item.Key.Contains("CreditNotMatchAmount"))
                {
                    item.Group = group2;
                    item.Header.Caption = "Chênh lệch";
                }
            }

            // xóa dòng xét tổng ở cuối Grid
            ultraGrid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
            if (band.Summaries.Count != 0) band.Summaries.Clear();
            // thêm dòng tính tổng cột "Số tiền" vào cuối Grid
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Status"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

        }
        private void ViewNotMatch(UltraGrid ultraGrid)
        {
            foreach (var item in ultraGrid.DisplayLayout.Bands[0].Columns)
            {
                if (item.Key.Contains("CreditNotMatchAmount"))
                {
                }
            }
        }
        #endregion

    }

    public class Test
    {

    }
}
