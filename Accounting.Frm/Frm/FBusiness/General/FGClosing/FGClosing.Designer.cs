﻿namespace Accounting
{
    partial class FGClosing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGClosing));
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridSoDuNt = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridHtoan = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panelHeader = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblNo = new Infragistics.Win.Misc.UltraLabel();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnApply = new Infragistics.Win.Misc.UltraButton();
            this.ultraCombo2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCombo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankOverBalance = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            this.cbbAccountCredit = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.panelFooter = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.panelBody = new Infragistics.Win.Misc.UltraPanel();
            this.UltraTabControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSoDuNt)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHtoan)).BeginInit();
            this.panelHeader.ClientArea.SuspendLayout();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountCredit)).BeginInit();
            this.panelFooter.ClientArea.SuspendLayout();
            this.panelFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.panelBody.ClientArea.SuspendLayout();
            this.panelBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UltraTabControl2)).BeginInit();
            this.UltraTabControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridSoDuNt);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(664, 206);
            // 
            // uGridSoDuNt
            // 
            this.uGridSoDuNt.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridSoDuNt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSoDuNt.Location = new System.Drawing.Point(0, 0);
            this.uGridSoDuNt.Name = "uGridSoDuNt";
            this.uGridSoDuNt.Size = new System.Drawing.Size(664, 206);
            this.uGridSoDuNt.TabIndex = 0;
            this.uGridSoDuNt.Text = "ultraGrid1";
            this.uGridSoDuNt.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridHtoan);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(664, 206);
            // 
            // uGridHtoan
            // 
            this.uGridHtoan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridHtoan.Location = new System.Drawing.Point(0, 0);
            this.uGridHtoan.Name = "uGridHtoan";
            this.uGridHtoan.Size = new System.Drawing.Size(664, 206);
            this.uGridHtoan.TabIndex = 0;
            this.uGridHtoan.Text = "ultraGrid1";
            // 
            // panelHeader
            // 
            // 
            // panelHeader.ClientArea
            // 
            this.panelHeader.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(666, 235);
            this.panelHeader.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox4);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox3);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(666, 235);
            this.ultraGroupBox1.TabIndex = 35;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox4.Controls.Add(this.ultraDateTimeEditor1);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor1);
            this.ultraGroupBox4.Controls.Add(this.ultraDateTimeEditor2);
            this.ultraGroupBox4.Location = new System.Drawing.Point(333, 3);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(321, 70);
            this.ultraGroupBox4.TabIndex = 43;
            this.ultraGroupBox4.Text = "Chứng từ thứ 2";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.ultraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.ultraLabel1.Location = new System.Drawing.Point(13, 19);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 24);
            this.ultraLabel1.TabIndex = 28;
            this.ultraLabel1.Text = "Số chứng từ";
            // 
            // ultraLabel2
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.ultraLabel2.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.ultraLabel2.Location = new System.Drawing.Point(211, 19);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 24);
            this.ultraLabel2.TabIndex = 31;
            this.ultraLabel2.Text = "Ngày chứng từ";
            // 
            // ultraLabel3
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.ultraLabel3.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.ultraLabel3.Location = new System.Drawing.Point(112, 19);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 24);
            this.ultraLabel3.TabIndex = 30;
            this.ultraLabel3.Text = "Ngày hoạch toán";
            // 
            // ultraDateTimeEditor1
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraDateTimeEditor1.Appearance = appearance4;
            this.ultraDateTimeEditor1.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ultraDateTimeEditor1.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(112, 40);
            this.ultraDateTimeEditor1.MaskInput = "";
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 32;
            this.ultraDateTimeEditor1.Value = null;
            // 
            // ultraTextEditor1
            // 
            appearance5.TextHAlignAsString = "Center";
            this.ultraTextEditor1.Appearance = appearance5;
            this.ultraTextEditor1.Location = new System.Drawing.Point(13, 40);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 29;
            // 
            // ultraDateTimeEditor2
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraDateTimeEditor2.Appearance = appearance6;
            this.ultraDateTimeEditor2.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ultraDateTimeEditor2.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(211, 40);
            this.ultraDateTimeEditor2.MaskInput = "";
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor2.TabIndex = 33;
            this.ultraDateTimeEditor2.Value = null;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.lblNo);
            this.ultraGroupBox3.Controls.Add(this.lblDate);
            this.ultraGroupBox3.Controls.Add(this.lblPostedDate);
            this.ultraGroupBox3.Controls.Add(this.dtePostedDate);
            this.ultraGroupBox3.Controls.Add(this.txtNo);
            this.ultraGroupBox3.Controls.Add(this.dteDate);
            this.ultraGroupBox3.Location = new System.Drawing.Point(6, 3);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(321, 70);
            this.ultraGroupBox3.TabIndex = 42;
            this.ultraGroupBox3.Text = "Chứng từ thứ nhất";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblNo
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.lblNo.Appearance = appearance7;
            this.lblNo.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblNo.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblNo.Location = new System.Drawing.Point(13, 19);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(100, 24);
            this.lblNo.TabIndex = 28;
            this.lblNo.Text = "Số chứng từ";
            // 
            // lblDate
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance8;
            this.lblDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblDate.Location = new System.Drawing.Point(211, 19);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 24);
            this.lblDate.TabIndex = 31;
            this.lblDate.Text = "Ngày chứng từ";
            // 
            // lblPostedDate
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Center";
            appearance9.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance9;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded4Thick;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Etched;
            this.lblPostedDate.Location = new System.Drawing.Point(112, 19);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 30;
            this.lblPostedDate.Text = "Ngày hoạch toán";
            // 
            // dtePostedDate
            // 
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance10;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(112, 40);
            this.dtePostedDate.MaskInput = "";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(100, 21);
            this.dtePostedDate.TabIndex = 32;
            this.dtePostedDate.Value = null;
            // 
            // txtNo
            // 
            appearance11.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance11;
            this.txtNo.Location = new System.Drawing.Point(13, 40);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(100, 21);
            this.txtNo.TabIndex = 29;
            // 
            // dteDate
            // 
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance12;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(211, 40);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(100, 21);
            this.dteDate.TabIndex = 33;
            this.dteDate.Value = null;
            // 
            // ultraGroupBox2
            // 
            appearance13.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraGroupBox2.Appearance = appearance13;
            this.ultraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox2.Controls.Add(this.btnApply);
            this.ultraGroupBox2.Controls.Add(this.ultraCombo2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraCombo1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.txtBankOverBalance);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox2.Controls.Add(this.txtDescription);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountCredit);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObject);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 79);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(660, 153);
            this.ultraGroupBox2.TabIndex = 34;
            this.ultraGroupBox2.Text = "Thông tin chung";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            appearance14.Image = ((object)(resources.GetObject("appearance14.Image")));
            this.btnApply.Appearance = appearance14;
            this.btnApply.Location = new System.Drawing.Point(566, 122);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(85, 27);
            this.btnApply.TabIndex = 53;
            this.btnApply.Text = "Đánh giá";
            // 
            // ultraCombo2
            // 
            appearance15.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraCombo2.ButtonsRight.Add(editorButton1);
            this.ultraCombo2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo2.Location = new System.Drawing.Point(517, 46);
            this.ultraCombo2.Name = "ultraCombo2";
            this.ultraCombo2.NullText = "<chọn dữ liệu>";
            this.ultraCombo2.Size = new System.Drawing.Size(134, 22);
            this.ultraCombo2.TabIndex = 52;
            // 
            // ultraLabel6
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance16;
            this.ultraLabel6.Location = new System.Drawing.Point(343, 46);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(151, 22);
            this.ultraLabel6.TabIndex = 51;
            this.ultraLabel6.Text = "TK xử lý lỗ chênh lệch tỷ giá";
            // 
            // ultraCombo1
            // 
            appearance17.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance17;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraCombo1.ButtonsRight.Add(editorButton2);
            this.ultraCombo1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo1.Location = new System.Drawing.Point(177, 48);
            this.ultraCombo1.Name = "ultraCombo1";
            this.ultraCombo1.NullText = "<chọn dữ liệu>";
            this.ultraCombo1.Size = new System.Drawing.Size(121, 22);
            this.ultraCombo1.TabIndex = 50;
            // 
            // ultraLabel5
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance18;
            this.ultraLabel5.Location = new System.Drawing.Point(3, 48);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(151, 22);
            this.ultraLabel5.TabIndex = 49;
            this.ultraLabel5.Text = "TK xử lý lãi chênh lệch tỷ giá";
            // 
            // txtBankOverBalance
            // 
            appearance19.TextHAlignAsString = "Right";
            this.txtBankOverBalance.Appearance = appearance19;
            this.txtBankOverBalance.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtBankOverBalance.InputMask = "nnnnnnnnnnnnnnn";
            this.txtBankOverBalance.Location = new System.Drawing.Point(517, 23);
            this.txtBankOverBalance.Name = "txtBankOverBalance";
            this.txtBankOverBalance.PromptChar = ' ';
            this.txtBankOverBalance.Size = new System.Drawing.Size(134, 20);
            this.txtBankOverBalance.TabIndex = 48;
            // 
            // ultraLabel4
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance20;
            this.ultraLabel4.Location = new System.Drawing.Point(343, 21);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(148, 22);
            this.ultraLabel4.TabIndex = 40;
            this.ultraLabel4.Text = "Đánh giá lại theo tỷ giá mới";
            // 
            // txtDescription
            // 
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Top";
            this.txtDescription.Appearance = appearance21;
            this.txtDescription.Location = new System.Drawing.Point(57, 76);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(594, 42);
            this.txtDescription.TabIndex = 38;
            this.txtDescription.Value = "<span style=\"font-family:Times New Roman; font-size:10pt;\">Đánh giá lại số dư ngo" +
    "ại tệ cuối kỳ</span>";
            // 
            // cbbAccountCredit
            // 
            appearance22.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton3.Appearance = appearance22;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountCredit.ButtonsRight.Add(editorButton3);
            this.cbbAccountCredit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountCredit.Location = new System.Drawing.Point(177, 20);
            this.cbbAccountCredit.Name = "cbbAccountCredit";
            this.cbbAccountCredit.NullText = "<chọn dữ liệu>";
            this.cbbAccountCredit.Size = new System.Drawing.Size(121, 22);
            this.cbbAccountCredit.TabIndex = 36;
            // 
            // lblAccountingObjectAddress
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Left";
            appearance23.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance23;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(4, 76);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(63, 19);
            this.lblAccountingObjectAddress.TabIndex = 34;
            this.lblAccountingObjectAddress.Text = "Diễn giải";
            // 
            // lblAccountingObject
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance24;
            this.lblAccountingObject.Location = new System.Drawing.Point(3, 20);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(132, 22);
            this.lblAccountingObject.TabIndex = 33;
            this.lblAccountingObject.Text = "Đánh giá lại theo loại tiền";
            // 
            // panelFooter
            // 
            // 
            // panelFooter.ClientArea
            // 
            this.panelFooter.ClientArea.Controls.Add(this.ultraGroupBox5);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 464);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(666, 52);
            this.panelFooter.TabIndex = 1;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(666, 52);
            this.ultraGroupBox5.TabIndex = 43;
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // panelBody
            // 
            // 
            // panelBody.ClientArea
            // 
            this.panelBody.ClientArea.Controls.Add(this.UltraTabControl2);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 235);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(666, 229);
            this.panelBody.TabIndex = 2;
            // 
            // UltraTabControl2
            // 
            this.UltraTabControl2.Controls.Add(this.ultraTabSharedControlsPage1);
            this.UltraTabControl2.Controls.Add(this.ultraTabPageControl1);
            this.UltraTabControl2.Controls.Add(this.ultraTabPageControl2);
            this.UltraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UltraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.UltraTabControl2.Name = "UltraTabControl2";
            this.UltraTabControl2.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.UltraTabControl2.Size = new System.Drawing.Size(666, 229);
            this.UltraTabControl2.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.UltraTabControl2.TabIndex = 5;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1. Số dư ngoại tệ";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Hạch toán chênh lệch tỷ giá";
            this.UltraTabControl2.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.UltraTabControl2.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(664, 206);
            // 
            // FGClosing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 516);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.Name = "FGClosing";
            this.Text = "Đánh giá lại tài khoản ngoại tệ";
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSoDuNt)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridHtoan)).EndInit();
            this.panelHeader.ClientArea.ResumeLayout(false);
            this.panelHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountCredit)).EndInit();
            this.panelFooter.ClientArea.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.panelBody.ClientArea.ResumeLayout(false);
            this.panelBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UltraTabControl2)).EndInit();
            this.UltraTabControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel panelHeader;
        private Infragistics.Win.Misc.UltraPanel panelFooter;
        private Infragistics.Win.Misc.UltraPanel panelBody;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel lblNo;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor txtDescription;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountCredit;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtBankOverBalance;
        private Infragistics.Win.Misc.UltraButton btnApply;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl UltraTabControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSoDuNt;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHtoan;
    }
}