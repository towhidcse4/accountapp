﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FGOtherVoucher : CustormForm
    {
        #region khai báo

        private readonly IGOtherVoucherService _IGOtherVoucherService = IoC.Resolve<IGOtherVoucherService>();
        List<GOtherVoucher> _dsGOtherVoucher = new List<GOtherVoucher>();

        #endregion

        #region khởi tạo
        public FGOtherVoucher()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
            WaitingFrm.StopWaiting();
        }
        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            int year = (dbStartDate == null ? DateTime.Now.Year : dbStartDate.Value.Year);
            _dsGOtherVoucher = Utils.ListGOtherVoucher.ToList();/*_IGOtherVoucherService.Query.Where(p => p.TypeID == 600 || p.TypeID == 640).ToList()*/;

            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsGOtherVoucher;

            if (configGrid) ConfigGrid();
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmViewClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion


        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary> 
        void AddFunction()
        {
            GOtherVoucher temp = new GOtherVoucher { TypeID = 600 };
            if (temp != null)
            {
                new FGOtherVoucherDetail(temp, _dsGOtherVoucher, ConstFrm.optStatusForm.Add).ShowDialog(this);
            }
            if (!FGOtherVoucherDetail.IsClose)
            {
                Utils.ClearCacheByType<GOtherVoucher>();
                LoadDuLieu();
            }
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                GOtherVoucher temp = uGrid.Selected.Rows[0].ListObject as GOtherVoucher;
                GOtherVoucher model = _IGOtherVoucherService.Getbykey(temp.ID);
                new FGOtherVoucherDetail(model, _dsGOtherVoucher, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FGOtherVoucherDetail.IsClose)
                {
                    Utils.ClearCacheByType<GOtherVoucher>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                GOtherVoucher temp = uGrid.Selected.Rows[0].ListObject as GOtherVoucher;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _IGOtherVoucherService.BeginTran();
                    _IGOtherVoucherService.Delete(temp);
                    _IGOtherVoucherService.CommitTran();
                    Utils.ClearCacheByType<GOtherVoucher>();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid()
        {
            //Grid Danh sách
            Utils.ConfigGrid(uGrid, ConstDatabase.GOtherVoucher_TableName);
            Utils.ConfigSummaryOfGrid(uGrid);
            //Grid Chi tiết
            //Utils.ConfigGrid(uGridDetail, ConstDatabase.GOtherVoucherDetail_TableName);
        }
        #endregion

        private void uGrid_AfterRowActivate(object sender, EventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            GOtherVoucher GOtherVoucher = _dsGOtherVoucher[_dsGOtherVoucher.IndexOf((GOtherVoucher)listObject)];
            if (GOtherVoucher == null) return;


            //Tab hoạch toán
            uGridDetail.DataSource = GOtherVoucher.GOtherVoucherDetails;    //Utils.CloneObject(SAReturn.SAReturnDetails);

            #region Config Grid
            //hiển thị 1 band
            uGridDetail.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridDetail.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridDetail.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridDetail.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridDetail.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridDetail.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridDetail.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            Template mauGiaoDien = Utils.GetMauGiaoDien(GOtherVoucher.TypeID, GOtherVoucher.TemplateID, Utils.ListTemplate);

            Utils.ConfigGrid(uGridDetail, null, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.FirstOrDefault(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            SummarySettings summary = band.Summaries.Add("SumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            foreach (var col in uGridDetail.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
            }

            #endregion
        }
    }
}
