﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Components.DictionaryAdapter;
using FX.Core;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using ColumnStyle = System.Windows.Forms.ColumnStyle;
using Type = System.Type;

namespace Accounting
{
    public partial class FGOtherVoucherDetail : fgOtherVoucherDetailStand
    {
        #region khai báo
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2;
        UltraGrid ugrid3;
        UltraGrid ugridHD;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region khởi tạo
        public FGOtherVoucherDetail(GOtherVoucher gotherVoucher, IEnumerable<GOtherVoucher> dsGOtherVoucher, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion
            ultraTextEditor1.Visible = false;
            _statusForm = statusForm;
            if (gotherVoucher.TypeID != 660 && gotherVoucher.TypeID != 601 && gotherVoucher.TypeID != 670)
                _select = statusForm == ConstFrm.optStatusForm.Add ? (new GOtherVoucher { TypeID = 600 }) : gotherVoucher;
            else _select = gotherVoucher;
            _listSelects.AddRange(dsGOtherVoucher);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);

            //Change Menu Top
            ReloadToolbar(_select, _listSelects, _statusForm);
        }

        #endregion
        #region override
        public override void InitializeGUI(GOtherVoucher inputVoucher)
        {
            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = Utils.GetMauGiaoDien(inputVoucher.TypeID, inputVoucher.TemplateID, Utils.ListTemplate);
            //if (_select.TemplateID == null || _select.TemplateID == Guid.Empty)
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;
            #endregion
            #endregion

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #region Phần đầu
            grpTop.Text = _select.Type == null ? Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID)).TypeName.ToUpper() : _select.Type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<GOtherVoucher>(palTop, "No", "PostedDate", "Date");
            #endregion            
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            if (inputVoucher.TypeID == 660)
            {
                BindingList<GOtherVoucherDetail> _bdlGOtherVoucherDetail = new BindingList<GOtherVoucherDetail>(inputVoucher.GOtherVoucherDetails);
                _listObjectInput = new BindingList<System.Collections.IList> { _bdlGOtherVoucherDetail };
                this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
                this.ConfigGridByTemplete<GOtherVoucher>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput);
                btnOriginalVoucher.Visible = false;
            }
            else if (inputVoucher.TypeID == 601)
            {
                BindingList<GOtherVoucherDetail> _bdlGOtherVoucherDetail = new BindingList<GOtherVoucherDetail>(inputVoucher.GOtherVoucherDetails);
                BindingList<GOtherVoucherDetailForeignCurrency> _bdlGOtherVoucherDetailForeignCurrency = new BindingList<GOtherVoucherDetailForeignCurrency>(inputVoucher.GOtherVoucherDetailForeignCurrencys);
                BindingList<GOtherVoucherDetailDebtPayment> _bdlGOtherVoucherDetailDebtPayment = new BindingList<GOtherVoucherDetailDebtPayment>(inputVoucher.GOtherVoucherDetailDebtPayments);
                _listObjectInput = new BindingList<System.Collections.IList> { _bdlGOtherVoucherDetail, _bdlGOtherVoucherDetailForeignCurrency, _bdlGOtherVoucherDetailDebtPayment };
                this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
                this.ConfigGridByTemplete<GOtherVoucher>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput, false);
                btnOriginalVoucher.Visible = false;
                ugrid3 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                if (ugrid3 != null)
                {
                    ugrid3.DisplayLayout.Bands[0].Columns["RemainingAmountOriginal"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                }

            }
            else if (inputVoucher.TypeID == 670)
            {
                BindingList<GOtherVoucherDetail> _bdlGOtherVoucherDetail = new BindingList<GOtherVoucherDetail>(inputVoucher.GOtherVoucherDetails);
                BindingList<GOtherVoucherDetailExcept> _bdlGOtherVoucherDetailExcept = new BindingList<GOtherVoucherDetailExcept>(inputVoucher.GOtherVoucherDetailExcepts);
                _listObjectInput = new BindingList<System.Collections.IList> { _bdlGOtherVoucherDetail, _bdlGOtherVoucherDetailExcept };
                this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
                this.ConfigGridByTemplete<GOtherVoucher>(inputVoucher.TypeID, mauGiaoDien, true, _listObjectInput, false);
                btnOriginalVoucher.Visible = false;
            }
            else
            {
                BindingList<GOtherVoucherDetail> _bdlGOtherVoucherDetail = new BindingList<GOtherVoucherDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new List<GOtherVoucherDetail>() : (inputVoucher.GOtherVoucherDetails == null ? new List<GOtherVoucherDetail>() : inputVoucher.GOtherVoucherDetails));
                BindingList<GOtherVoucherDetailTax> _bdlGOtherVoucherDetailTax = new BindingList<GOtherVoucherDetailTax>(_statusForm == ConstFrm.optStatusForm.Add
                                                                          ? new List<GOtherVoucherDetailTax>()
                                                                          : (inputVoucher.GOtherVoucherDetailTaxs == null ? new List<GOtherVoucherDetailTax>() : inputVoucher.GOtherVoucherDetailTaxs));
                BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
                if (_statusForm != ConstFrm.optStatusForm.Add)
                    bdlRefVoucher = new BindingList<RefVoucher>(inputVoucher.RefVouchers);
                _listObjectInput = new BindingList<System.Collections.IList> { _bdlGOtherVoucherDetail, _bdlGOtherVoucherDetailTax };
                _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
                this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                this.ConfigGridByManyTemplete<GOtherVoucher>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugridHD = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                if (ugrid2 != null)
                {
                    ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                    ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                }
                btnOriginalVoucher.Visible = true;//add by cuongpv
                var uTabControl = Controls.Find("ultraTabControl", true)[0] as UltraTabControl;
                uTabControl.SelectedTabChanged += (s, e) => ultraTabControl_SelectedTabChanged(s, e, _bdlGOtherVoucherDetailTax);
            }
            if (ugrid2 != null)
            {
                ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            }
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : inputVoucher.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add && _typeID != 670 ? "VND" : inputVoucher.CurrencyID;
            List<GOtherVoucher> select = new List<GOtherVoucher> { _select };
            this.ConfigGridCurrencyByTemplate<GOtherVoucher>(_select.TypeID, new BindingList<System.Collections.IList> { select },
                                              uGridControl, mauGiaoDien);
            //Tab Hóa đơn                      
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                SetOrGetGuiObject(inputVoucher, false);
            }
            this.ConfigReasonCombo<GOtherVoucher>(_select.TypeID, _statusForm, cbbReason, ultraTextEditor1, "AutoPrincipleName", "ID");
            txtReason.DataBindings.Clear();
            txtReason.DataBindings.Add("Text", inputVoucher, "Reason", true, DataSourceUpdateMode.OnPropertyChanged);

            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (_select.TypeID == 600)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Hạch toán nghiệp vụ khác";
                    txtReason.Text = "Hạch toán nghiệp vụ khác";
                    _select.Reason = "Hạch toán nghiệp vụ khác";
                };
            }
        }
        #endregion
        private void ultraTabControl_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e, BindingList<GOtherVoucherDetailTax> bdlGOtherVoucherDetailTax)
        {
            grHoaDon.Visible = false;
            var ppInvoid = new GOtherVoucherDetailTax();
            var ppInvoiddb = bdlGOtherVoucherDetailTax.FirstOrDefault();
            if (ppInvoiddb != null)
            {
                ppInvoid = ppInvoiddb;
            }
            if (e.Tab.Key == "uTab2")
            {
                grHoaDon.Visible = true;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                ugridHD.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                txtMauSoHD.Text = ppInvoid.InvoiceTemplate != null ? ppInvoid.InvoiceTemplate : txtMauSoHD.Text;
                txtKyHieuHD.Text = ppInvoid.InvoiceSeries != null ? ppInvoid.InvoiceSeries : txtKyHieuHD.Text;
                txtSoHD.Text = ppInvoid.InvoiceNo != null ? ppInvoid.InvoiceNo : txtSoHD.Text;
                dteNgayHD.Value = ppInvoid.InvoiceDate != null ? ppInvoid.InvoiceDate : dteNgayHD.Value;
            }

        }
        protected override bool AdditionalFunc()
        {
            if (txtKyHieuHD.Text != "" || txtMauSoHD.Text != "" || txtSoHD.Text != "" || dteNgayHD.Value != null)
            {
                if (txtKyHieuHD.Text != "" && txtMauSoHD.Text != "" && txtSoHD.Text != "" && dteNgayHD.Value != null)
                {
                    if (txtSoHD.Text.Length == 7)
                    {
                        //var list = (BindingList<GOtherVoucherDetailTax>)_listObjectInput[1];

                        foreach (var item in _select.GOtherVoucherDetailTaxs)
                        {
                            item.InvoiceTemplate = txtMauSoHD.Text;
                            item.InvoiceSeries = txtKyHieuHD.Text;
                            item.InvoiceNo = txtSoHD.Text;
                            item.InvoiceDate = Convert.ToDateTime(dteNgayHD.Value.ToString());
                        }
                    }
                    else
                    {
                        MSG.Warning("Số hoá đơn phải là kiểu số và đúng 7 chữ số.");
                        return false;
                    }
                }
                else
                {
                    MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                    return false;
                }
            }
            else
            {
                //var list = (BindingList<GOtherVoucherDetailTax>)_listObjectInput[1];
                foreach (var item in _select.GOtherVoucherDetailTaxs)
                {
                    item.InvoiceTemplate = "";
                    item.InvoiceSeries = "";
                    item.InvoiceNo = "";
                    item.InvoiceDate = null;
                }
            }
            return true;
        }
        #region Event
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FGOtherVoucherDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }

    public class fgOtherVoucherDetailStand : DetailBase<GOtherVoucher>
    {
    }
}
