﻿using Accounting.Core.Domain;

namespace Accounting
{
    partial class FGOtherVoucherDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.palTop = new Infragistics.Win.Misc.UltraPanel();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palFill = new Infragistics.Win.Misc.UltraPanel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBottom = new Infragistics.Win.Misc.UltraPanel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.palTab = new Infragistics.Win.Misc.UltraPanel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbReason = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.grHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            this.palFill.ClientArea.SuspendLayout();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.palBottom.ClientArea.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.palTab.ClientArea.SuspendLayout();
            this.palTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).BeginInit();
            this.grHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            this.SuspendLayout();
            // 
            // palTop
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.palTop.Appearance = appearance1;
            this.palTop.Location = new System.Drawing.Point(7, 27);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(343, 49);
            this.palTop.TabIndex = 40;
            this.palTop.TabStop = false;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.palTop);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Top;
            appearance2.FontData.BoldAsString = "True";
            appearance2.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance2;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(941, 85);
            this.grpTop.TabIndex = 0;
            this.grpTop.Text = "Chứng từ nghiệp vụ khác";
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palFill
            // 
            // 
            // palFill.ClientArea
            // 
            this.palFill.ClientArea.Controls.Add(this.palTab);
            this.palFill.ClientArea.Controls.Add(this.ultraSplitter1);
            this.palFill.ClientArea.Controls.Add(this.grHoaDon);
            this.palFill.ClientArea.Controls.Add(this.palBottom);
            this.palFill.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 85);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(941, 410);
            this.palFill.TabIndex = 1;
            this.palFill.TabStop = false;
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(475, 3);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(466, 60);
            this.uGridControl.TabIndex = 0;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // palBottom
            // 
            // 
            // palBottom.ClientArea
            // 
            this.palBottom.ClientArea.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 344);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(941, 66);
            this.palBottom.TabIndex = 3;
            this.palBottom.TabStop = false;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 108);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 74;
            this.ultraSplitter1.Size = new System.Drawing.Size(941, 15);
            this.ultraSplitter1.TabIndex = 2;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance3;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(852, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 0;
            this.btnOriginalVoucher.TabStop = false;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // palTab
            // 
            this.palTab.AutoSize = true;
            // 
            // palTab.ClientArea
            // 
            this.palTab.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palTab.Location = new System.Drawing.Point(0, 123);
            this.palTab.Name = "palTab";
            this.palTab.Size = new System.Drawing.Size(941, 221);
            this.palTab.TabIndex = 0;
            this.palTab.TabStop = false;
            // 
            // ultraLabel1
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance12;
            this.ultraLabel1.Location = new System.Drawing.Point(7, 22);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(52, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Tag = "";
            this.ultraLabel1.Text = "Diễn giải";
            this.ultraLabel1.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextVAlignAsString = "Middle";
            this.txtReason.Appearance = appearance10;
            this.txtReason.AutoSize = false;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(292, 23);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(606, 22);
            this.txtReason.TabIndex = 2;
            // 
            // ultraTextEditor1
            // 
            this.ultraTextEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.TextVAlignAsString = "Middle";
            this.ultraTextEditor1.Appearance = appearance9;
            this.ultraTextEditor1.AutoSize = false;
            this.ultraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.ultraTextEditor1.Location = new System.Drawing.Point(74, 47);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(10, 10);
            this.ultraTextEditor1.TabIndex = 3;
            this.ultraTextEditor1.TabStop = false;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraTextEditor1);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.cbbReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(941, 60);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbReason
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.cbbReason.Appearance = appearance11;
            this.cbbReason.AutoSize = false;
            this.cbbReason.Location = new System.Drawing.Point(74, 23);
            this.cbbReason.Name = "cbbReason";
            this.cbbReason.NullText = "<chọn dữ liệu>";
            this.cbbReason.Size = new System.Drawing.Size(212, 22);
            this.cbbReason.TabIndex = 1;
            // 
            // grHoaDon
            // 
            this.grHoaDon.Controls.Add(this.txtMauSoHD);
            this.grHoaDon.Controls.Add(this.txtSoHD);
            this.grHoaDon.Controls.Add(this.txtKyHieuHD);
            this.grHoaDon.Controls.Add(this.ultraLabel37);
            this.grHoaDon.Controls.Add(this.ultraLabel38);
            this.grHoaDon.Controls.Add(this.ultraLabel39);
            this.grHoaDon.Controls.Add(this.ultraLabel40);
            this.grHoaDon.Controls.Add(this.dteNgayHD);
            this.grHoaDon.Dock = System.Windows.Forms.DockStyle.Top;
            this.grHoaDon.Location = new System.Drawing.Point(0, 60);
            this.grHoaDon.Name = "grHoaDon";
            this.grHoaDon.Size = new System.Drawing.Size(941, 48);
            this.grHoaDon.TabIndex = 0;
            this.grHoaDon.Text = "Hóa đơn";
            this.grHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD
            // 
            this.txtMauSoHD.AutoSize = false;
            this.txtMauSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD.Location = new System.Drawing.Point(74, 18);
            this.txtMauSoHD.Name = "txtMauSoHD";
            this.txtMauSoHD.Size = new System.Drawing.Size(142, 22);
            this.txtMauSoHD.TabIndex = 84;
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(501, 19);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(142, 22);
            this.txtSoHD.TabIndex = 78;
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(292, 19);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(142, 22);
            this.txtKyHieuHD.TabIndex = 77;
            // 
            // ultraLabel37
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance4;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(649, 18);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel37.TabIndex = 83;
            this.ultraLabel37.Text = "Ngày HĐ";
            // 
            // ultraLabel38
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance5;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(455, 18);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel38.TabIndex = 82;
            this.ultraLabel38.Text = "Số HĐ";
            // 
            // ultraLabel39
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance6;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(228, 19);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel39.TabIndex = 81;
            this.ultraLabel39.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel40
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance7;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(12, 18);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 80;
            this.ultraLabel40.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD
            // 
            appearance8.TextHAlignAsString = "Center";
            appearance8.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance8;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(709, 19);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(94, 22);
            this.dteNgayHD.TabIndex = 79;
            this.dteNgayHD.Value = null;
            // 
            // FGOtherVoucherDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(941, 495);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.grpTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FGOtherVoucherDetail";
            this.Text = "Chứng từ nghiệp vụ khác";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FGOtherVoucherDetail_FormClosing);
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.palFill.ClientArea.ResumeLayout(false);
            this.palFill.ClientArea.PerformLayout();
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.palBottom.ClientArea.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.palTab.ClientArea.ResumeLayout(false);
            this.palTab.ResumeLayout(false);
            this.palTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).EndInit();
            this.grHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel palTop;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private Infragistics.Win.Misc.UltraPanel palFill;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel palTab;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon;
        private Infragistics.Win.Misc.UltraPanel palBottom;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
    }
}