﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public partial class FGOtherVoucherILTransferDetail : fgOInterestAndLossTransferDetailStand
    {
        #region khai báo
        BindingList<Account> _dsAccount = Utils.ListAccount;    //Bảng Account
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        UltraGrid ugrid2 = null;
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region khởi tạo
        public FGOtherVoucherILTransferDetail(GOtherVoucher gotherVoucher, IEnumerable<GOtherVoucher> dsGOtherVoucher, int statusForm)
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            base.InitializeComponent1();
            #endregion
            
            _statusForm = statusForm;
            _select = statusForm == ConstFrm.optStatusForm.Add ? (new GOtherVoucher { TypeID = 620, CurrencyID = "VND", ExchangeRate = 1 }) : gotherVoucher;
            _listSelects.AddRange(dsGOtherVoucher);
            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI(_select);

            //Change Menu Top
            ReloadToolbar(_select, _listSelects, _statusForm);
        }
        #endregion
        #region override
        public override void InitializeGUI(GOtherVoucher inputVoucher)
        {
            #region Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)
            Template mauGiaoDien = Utils.GetTemplateUIfromDatabase(inputVoucher.TypeID, _statusForm == ConstFrm.optStatusForm.Add, inputVoucher.TemplateID);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : inputVoucher.TemplateID;
            #endregion
            #endregion
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                
                bdlRefVoucher = new BindingList<RefVoucher>(inputVoucher.RefVouchers);
            }
            
                
            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control (nếu đang sửa)

            #region Phần đầu
            grpTop.Text = _select.Type == null ? Utils.ListType.FirstOrDefault(p => p.ID.Equals(_select.TypeID)).TypeName.ToUpper() : _select.Type.TypeName.ToUpper();
            this.ConfigTopVouchersNo<GOtherVoucher>(palTop, "No", "PostedDate", "Date");
            #endregion
            #region Phần chung config theo Templete
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
                LoadData();
            }
            BindingList<GOtherVoucherDetail> _bdlGOtherVoucherDetail = new BindingList<GOtherVoucherDetail>(_select.GOtherVoucherDetails == null ? new List<GOtherVoucherDetail>() : _select.GOtherVoucherDetails);
            _listObjectInput = new BindingList<System.Collections.IList> { _bdlGOtherVoucherDetail };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<GOtherVoucher>(palTab, mauGiaoDien);
            System.Type[] blackList = { typeof(GOtherVoucherDetail) };
            //this.ConfigGridByTemplete<GOtherVoucher>(_select.TypeID, mauGiaoDien, true, _listObjectInput, false, null, false, false, blackList);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { false, false };
            this.ConfigGridByManyTemplete<GOtherVoucher>(inputVoucher.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString(), false, false, blackList);
            ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            #endregion

            #region Phần riêng
            //Tab Hóa đơn
            DataBinding(new List<Control> {txtReason}, "Reason");
            #endregion
            #endregion

            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                SetOrGetGuiObject(inputVoucher, false);

            }
        }

        #endregion

        #region Utils
        void LoadData()
        {
            DateTime toDate = _select.PostedDate;
            //toDate.AddDays(1);
            _select.GOtherVoucherDetails = Utils.IGeneralLedgerService.getILTranferDetail(toDate);
        }

        #endregion

        #region Event
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }
    }

    public class fgOInterestAndLossTransferDetailStand : DetailBase<GOtherVoucher>
    {
    }
}
