﻿using Accounting.Core.Domain;

namespace Accounting
{
    partial class FGOtherVoucherILTransferDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.palTab = new Infragistics.Win.Misc.UltraPanel();
            this.palFill = new Infragistics.Win.Misc.UltraPanel();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.grpTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTop = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            this.palTab.ClientArea.SuspendLayout();
            this.palTab.SuspendLayout();
            this.palFill.ClientArea.SuspendLayout();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).BeginInit();
            this.grpTop.SuspendLayout();
            this.palTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(889, 74);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.txtReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReason.Location = new System.Drawing.Point(71, 22);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(806, 32);
            this.txtReason.TabIndex = 1;
            // 
            // ultraLabel1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(7, 25);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(52, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Tag = "";
            this.ultraLabel1.Text = "Diễn giải";
            this.ultraLabel1.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            // 
            // palTab
            // 
            // 
            // palTab.ClientArea
            // 
            this.palTab.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.palTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palTab.Location = new System.Drawing.Point(0, 84);
            this.palTab.Name = "palTab";
            this.palTab.Size = new System.Drawing.Size(889, 274);
            this.palTab.TabIndex = 44;
            // 
            // palFill
            // 
            // 
            // palFill.ClientArea
            // 
            this.palFill.ClientArea.Controls.Add(this.palTab);
            this.palFill.ClientArea.Controls.Add(this.ultraSplitter1);
            this.palFill.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 85);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(889, 358);
            this.palFill.TabIndex = 46;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 74);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 0;
            this.ultraSplitter1.Size = new System.Drawing.Size(889, 10);
            this.ultraSplitter1.TabIndex = 1;
            // 
            // grpTop
            // 
            this.grpTop.Controls.Add(this.palTop);
            this.grpTop.Dock = System.Windows.Forms.DockStyle.Top;
            appearance4.FontData.BoldAsString = "True";
            appearance4.FontData.SizeInPoints = 13F;
            this.grpTop.HeaderAppearance = appearance4;
            this.grpTop.Location = new System.Drawing.Point(0, 0);
            this.grpTop.Name = "grpTop";
            this.grpTop.Size = new System.Drawing.Size(889, 85);
            this.grpTop.TabIndex = 45;
            this.grpTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTop
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.palTop.Appearance = appearance3;
            this.palTop.Location = new System.Drawing.Point(7, 28);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(343, 49);
            this.palTop.TabIndex = 43;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance2;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(791, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 9;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // FGOtherVoucherILTransferDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 443);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.grpTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FGOtherVoucherILTransferDetail";
            this.Text = "Kết chuyển lãi, lỗ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            this.palTab.ClientArea.ResumeLayout(false);
            this.palTab.ResumeLayout(false);
            this.palFill.ClientArea.ResumeLayout(false);
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTop)).EndInit();
            this.grpTop.ResumeLayout(false);
            this.palTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel palTab;
        private Infragistics.Win.Misc.UltraPanel palFill;
        private Infragistics.Win.Misc.UltraGroupBox grpTop;
        private Infragistics.Win.Misc.UltraPanel palTop;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}