﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using FX.Data;
//using HideTabHeaders_CS;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
//using UltraTabControlNoBorder;
using Type = Accounting.Core.Domain.Type;
using System.ComponentModel;
using Accounting.Core.IService;
using FX.Core;
using Newtonsoft.Json;

namespace Accounting
{
    public partial class BaseBusiness<T> : BaseForm
    {
        #region khai báo
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public IRepositoryLedgerService _IRepositoryLedgerService { get { return IoC.Resolve<IRepositoryLedgerService>(); } }
        string _title = "";
        int _loaiForm = -1;    //Biến này được tạo ra và dùng trong trường hợp 1 form dùng cho nhiều loại nghiệp vụ giống nhau. Vd: Mua và ghi tăng tài sản cố định, ghi tăng khác....
        public bool IsGridShow = true;  //TRUE: dùng grid để chứa dữ liệu, FALSE: dùng TreeGrid để chứa dữ liệu
        public T _select = (T)Activator.CreateInstance(typeof(T));//= new T();  
        public List<T> DsObject;    //danh sách List<Object> đang thao tác
        public int defaultTypeId;
        public List<int> lstTypeId;
        private bool _notEvent = false;
        //public Thread TLoadControl;
        //public Thread TLoadData;
        //public Thread TLoadChilGrid;
        private Template TemplateChild;
        private string _subSystemCode;
        public bool XoaNT = false;
        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        BindingList<Control> Buttons = new BindingList<Control>();

        #endregion

        #region khởi tạo
        //public BaseBusiness(int loaiForm = -1)
        //{
        //    InitializeComponent();
        //    initBaseBusiness(null, loaiForm);
        //}

        public BaseBusiness(string title = "", string subSystemCode = "", int loaiForm = -1, List<int> lstTypeId = null, int defaultTypeId = 0)
        {
            this.defaultTypeId = defaultTypeId;
            this.lstTypeId = lstTypeId;
            this._title = title;
            this._subSystemCode = subSystemCode;
            initBaseBusiness(lstTypeId, loaiForm);
        }

        void LoadControl()
        {
            InitializeComponent();

            Buttons = new BindingList<Control> { btnAdd, drpbtnAdd, btnEdit, btnDelete, btnRecorded, btnUnRecorded, btnReset, btnPrint, btnHelp, btnUpload, btnExportExcel };
            //foreach (var button in Buttons)
            //{
            //    button.Visible = true;
            //}
            if (typeof(T) == typeof(PPDiscountReturn) || typeof(T) == typeof(SAReturn))
            {
                btnAdd.Visible = false;
                drpbtnAdd.Visible = true;
            }
            else
            {
                btnAdd.Visible = true;
                drpbtnAdd.Visible = false;
            }
            btnRecorded.Visible = btnUnRecorded.Visible = false;
            palTtc.Dock = DockStyle.Fill;
            if (typeof(T) == typeof(SABill))
            {
                btnUpload.Visible = true;
            }
            else
            {
                btnUpload.Visible = false;
            }
            #region Config Base Frm
            //kích thước frm
            Size = new Size(800, 450);
            //căn giữa màn hình
            StartPosition = FormStartPosition.CenterScreen;
            //Frm cần show uGrid hay uTree
            uGrid.Size = new Size(1, 1);
            uTree.Size = new Size(1, 1);
            if (IsGridShow) { uGrid.Dock = DockStyle.Fill; }
            else { uTree.Dock = DockStyle.Fill; }
            #endregion

            #region Set tooltip cho button
            ToolTip ToolTip1 = new ToolTip();
            if (typeof(T) == typeof(PPDiscountReturn) || typeof(T) == typeof(SAReturn))
                ToolTip1.SetToolTip(drpbtnAdd, "Thêm (Ctrl + N)");
            else
                ToolTip1.SetToolTip(btnAdd, "Thêm (Ctrl + N)");
            ToolTip ToolTip2 = new ToolTip();
            ToolTip2.SetToolTip(btnEdit, "Xem (Ctrl + E)");
            ToolTip ToolTip3 = new ToolTip();
            ToolTip3.SetToolTip(btnDelete, "Xóa (Ctrl + D)");
            ToolTip ToolTip4 = new ToolTip();
            ToolTip4.SetToolTip(btnReset, "Làm mới (F5)");
            ToolTip ToolTip5 = new ToolTip();
            ToolTip5.SetToolTip(btnRecorded, "Ghi sổ (Ctrl + G)");
            ToolTip ToolTip6 = new ToolTip();
            ToolTip6.SetToolTip(btnUnRecorded, "Bỏ ghi (Ctrl + B)");
            ToolTip ToolTip7 = new ToolTip();
            ToolTip7.SetToolTip(btnPrint, "In (Ctrl + P)");
            ToolTip ToolTip8 = new ToolTip();
            ToolTip8.SetToolTip(btnHelp, "Giúp (F1)");
            #endregion

            //var frm = Application.OpenForms.Cast<Form>().FirstOrDefault(x => x.Name == "FMain") as FMain;


            #region Event Init

            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.N);
            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.D);
            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.E);
            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.G);
            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.B);
            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.P);
            //KeyboardHook.HookedKeys.Add(Keys.Control | Keys.A);
            //KeyboardHook.HookedKeys.Add(Keys.F5);
            //KeyboardHook.HookedKeys.Add(Keys.F1);
            //if (frm != null)
            //{
            //    frm.KeyboardHook.KeyDown += BaseBusiness_KeyDown;
            //    frm.KeyboardHook.Start();
            //}
            //Button Click
            btnAdd.Click += BtnAddClick;
            btnEdit.Click += BtnEditClick;
            btnDelete.Click += BtnDeleteClick;
            btnRecorded.Click += BtnRecordedClick;
            btnUnRecorded.Click += BtnUnRecordedClick;
            btnReset.Click += BtnResetClick;
            //btnPrint.Click += BtnPrintClick;
            btnHelp.Click += BtnHelpClick;
            btnUpload.Click += BtnUpLoadClick;
            btnExportExcel.Click += BtnExportExcel;

            //ToolStripMenuItem (ContentMenu) Click
            tsmAdd.Click += TsmAddClick;
            tsmEdit.Click += TsmEditClick;
            tsmDelete.Click += TsmDeleteClick;
            tsmReLoad.Click += TsmReLoadClick;
            tsmPost.Click += TsmPostClick;
            tsmUnPost.Click += TsmUnPostClick;
            if (IsGridShow)
            {//init uGrid
                uGrid.ContextMenuStrip = cms4Grid;
                UGridInitializeLayout(uGrid);
                //uGrid.MouseDown += UGridMouseDown;
                uGrid.DoubleClickRow += UGridDoubleClickRow;
                uGrid.AfterSelectChange += UGridAfterSelectChangeBase;
                uGrid.BeforeRowsDeleted += uGrid_BeforeRowsDeleted;
                uGrid.Leave += UGridLeave;
                //lần đầu config Grid
                uGrid.DataSource = Activator.CreateInstance(typeof(List<T>));
                ConfigGrid(uGrid);
            }
            else
            {//init uTree
                uTree.ContextMenuStrip = cms4Grid;
                uTree.InitializeDataNode += UTreeInitializeDataNode;
                uTree.AfterDataNodesCollectionPopulated += Utils.uTree_AfterDataNodesCollectionPopulated;
                uTree.ColumnSetGenerated += UTreeColumnSetGenerated;
                //uTree.MouseDown += UTreeMouseDown;
                uTree.DoubleClick += UTreeDoubleClick;
            }
            #endregion

            //uTabControl.DrawFilter = new DrawFilter();

            #region Chỉ hiển thị Tab chi tiết
            if (typeof(T) == typeof(PSTimeSheetSummary) || typeof(T) == typeof(GOtherVoucher)
                || typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet))
            {
                uTabControl.Tabs[0].Visible = false;
                uTabControl.CreationFilter = new HideTabHeaders();
            }
            if (typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
                || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice))
            {
                uTabControl.Visible = false;
                ultraSplitter1.Collapsed = true;
            }
            #endregion
            #region add by cuongpv loc Data theo Ky
            //config Date theo ky
            //ConfigComboDate(cbbDateTime);

            List<Item> lstItems = Utils.ObjConstValue.SelectTimes;
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = lstItems;
            cbbDateTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            cbbDateTime.ValueChanged += cbbDateTime_ValueChanged;

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(_subSystemCode, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            if (typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet) || typeof(T) == typeof(PSTimeSheetSummary) || _loaiForm == -840 || typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
                || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice))
            {
                lblKyKeToan.Visible = false;
                cbbDateTime.Visible = false;
                lblBeginDate.Visible = false;
                dtBeginDate.Visible = false;
                lblEndDate.Visible = false;
                dtEndDate.Visible = false;
                btnLoc.Visible = false;
            }
            else
            {
                lblKyKeToan.Visible = true;
                cbbDateTime.Visible = true;
                lblBeginDate.Visible = true;
                dtBeginDate.Visible = true;
                lblEndDate.Visible = true;
                dtEndDate.Visible = true;
                btnLoc.Visible = true;
            }
            #endregion
            //ReportUtils.ProcessControls(this);//add by cuongpv phan trang
        }
        //add by cuongpv

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.SelectedRow != null)
            {
                var model = cbb.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(DateTime.Now.Year, DateTime.Now, model, out dtBegin, out dtEnd);
                dtBeginDate.Value = dtBegin;
                dtEndDate.Value = dtEnd;
            }
        }

        //end add by cuongpv
        void ConfigButtons()
        {
            var size = new Size(90, 30);
            var pBegin = new Point(10, 12);
            var space = 0;
            int i = 0;
            foreach (var button in Buttons)
            {
                if (!button.Visible) continue;
                button.Location = new Point(pBegin.X + ((size.Width + space) * i), pBegin.Y);
                button.Size = size;
                i++;
            }
            grTitle.Visible = true;
        }

        void LoadDataBase(List<int> lstTypeId = null, int loaiForm = -1)
        {
            _loaiForm = loaiForm;
            DateTime dtFrom = dtBeginDate.DateTime.Date;
            DateTime dtTo = dtEndDate.DateTime.Date;
            dtFrom = dtFrom.AddHours(0).AddMinutes(0).AddSeconds(0);
            dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
            //DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(_subSystemCode, Authenticate.User.userid);
            //if (dateTimeCacheHistory != null)
            //{
            //    //dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
            //    //dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
            //    //_IDateTimeCacheHistoryService.BeginTran();
            //    //_IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
            //    //_IDateTimeCacheHistoryService.CommitTran();
            //}
            //else
            //{
            //    dateTimeCacheHistory = new DateTimeCacheHistory();
            //    dateTimeCacheHistory.ID = Guid.NewGuid();
            //    dateTimeCacheHistory.IDUser = Authenticate.User.userid;
            //    dateTimeCacheHistory.SubSystemCode = _subSystemCode;
            //    dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
            //    dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
            //    dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
            //    //_IDateTimeCacheHistoryService.BeginTran();
            //    //_IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
            //    //_IDateTimeCacheHistoryService.CommitTran();
            //}


            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtFrom > dtTo)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            //if (typeof(T) == typeof(PPInvoice))
            //{
            //    List<PPInvoice> lstPpInvoices = new List<PPInvoice>();
            //    //0: Mua hàng qua kho, 1: mua hàng không qua kho
            //    lstPpInvoices = loaiForm == 0 ? IPPInvoiceService.Query.Where(k => (bool)k.StoredInRepository && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList() : IPPInvoiceService.Query.Where(k => !(bool)k.StoredInRepository && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.IPPInvoiceService.UnbindSession(lstPpInvoices);
            //    //Utils.IPPInvoiceService.UnbindSession(DsObject);
            //    //lstPpInvoices = loaiForm == 0 ? IPPInvoiceService.Query.Where(k => (bool)k.StoredInRepository && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList() : IPPInvoiceService.Query.Where(k => !(bool)k.StoredInRepository && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstPpInvoices.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(PPDiscountReturn))
            //{
            //    List<PPDiscountReturn> lstPpDiscountReturns = IPPDiscountReturnService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.IPPDiscountReturnService.UnbindSession(lstPpDiscountReturns);
            //    //Utils.IPPDiscountReturnService.UnbindSession(DsObject);
            //    //lstPpDiscountReturns = IPPDiscountReturnService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstPpDiscountReturns.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(SAInvoice))
            //{
            //    List<SAInvoice> lstSaInvoices = new List<SAInvoice>();
            //    if (loaiForm == 0) lstSaInvoices = ISAInvoiceService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    else if (loaiForm == 1) lstSaInvoices = ISAInvoiceService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    else lstSaInvoices = ISAInvoiceService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.ISAInvoiceService.UnbindSession(lstSaInvoices);
            //    //Utils.ISAInvoiceService.UnbindSession(DsObject);
            //    //if (loaiForm == 0) lstSaInvoices = ISAInvoiceService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //else if (loaiForm == 1) lstSaInvoices = ISAInvoiceService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //else lstSaInvoices = ISAInvoiceService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstSaInvoices.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(SAReturn))
            //{
            //    List<SAReturn> lstSaReturns = new List<SAReturn>();
            //    lstSaReturns = ISAReturnService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.ISAReturnService.UnbindSession(lstSaReturns);
            //    //Utils.ISAReturnService.UnbindSession(DsObject);
            //    //lstSaReturns = ISAReturnService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstSaReturns.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(RSInwardOutward))
            //{
            //    List<RSInwardOutward> lstInwardOutwards = IRSInwardOutwardService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where                
            //    lstInwardOutwards = IRSInwardOutwardService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.IRSInwardOutwardService.UnbindSession(DsObject);
            //    //Utils.IRSInwardOutwardService.UnbindSession(lstInwardOutwards);
            //    //lstInwardOutwards = IRSInwardOutwardService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstInwardOutwards.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(FAIncrement))
            //{
            //    List<FAIncrement> lstFaIncrements = IFAIncrementService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.IFAIncrementService.UnbindSession(lstFaIncrements);
            //    //Utils.IFAIncrementService.UnbindSession(DsObject);
            //    //lstFaIncrements = IFAIncrementService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstFaIncrements.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(TIIncrement))
            //{
            //    List<TIIncrement> lstTiIncrements = ITIIncrementService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //Utils.ITIIncrementService.UnbindSession(lstTiIncrements);
            //    //Utils.ITIIncrementService.UnbindSession(DsObject);
            //    //lstTiIncrements = ITIIncrementService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstTiIncrements.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(GOtherVoucher))
            //{
            //    //add by cuongpv
            //    List<GOtherVoucher> lstGOtherVouchers = new List<GOtherVoucher>();
            //    if (_loaiForm == -840)
            //        lstGOtherVouchers = IGOtherVoucherService.Query.Where(k => lstTypeId.Contains(k.TypeID)).ToList();
            //    else
            //        lstGOtherVouchers = IGOtherVoucherService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    //end add by cuongpv
            //    //Utils.IGOtherVoucherService.UnbindSession(lstGOtherVouchers);
            //    //Utils.IGOtherVoucherService.UnbindSession(DsObject);
            //    //if (_loaiForm == -840)
            //    //    lstGOtherVouchers = IGOtherVoucherService.Query.Where(k => lstTypeId.Contains(k.TypeID)).ToList();
            //    //else
            //    //    lstGOtherVouchers = IGOtherVoucherService.Query.Where(k => lstTypeId.Contains(k.TypeID) && (k.PostedDate >= dtFrom && k.PostedDate <= dtTo)).ToList();//edit by cuongpv: them khoang thoi gian vao where
            //    DsObject = lstGOtherVouchers.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else if (typeof(T) == typeof(TIInit))
            //{
            //    List<TIInit> lstGOtherVouchers = ITIInitService.Query.Where(k => k.PostedDate >= dtFrom && k.PostedDate <= dtTo).ToList();//edit by cuongpv: them khoang thoi gian vao where, bo GetAll()
            //    //Utils.ITIInitService.UnbindSession(lstGOtherVouchers);
            //    //Utils.ITIInitService.UnbindSession(DsObject);
            //    //lstGOtherVouchers = ITIInitService.Query.Where(k => k.PostedDate >= dtFrom && k.PostedDate <= dtTo).ToList();//edit by cuongpv: them khoang thoi gian vao where, bo GetAll()
            //    DsObject = lstGOtherVouchers.Cast<T>().ToList();
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            //else
            //{
            //    BaseService<T, Guid> services = this.GetIService((T)Activator.CreateInstance(typeof(T)));
            //    try
            //    {
            //        //add by cuongpv
            //        if (typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet) || typeof(T) == typeof(PSTimeSheetSummary) || typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
            //            || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice))
            //        {

            //            DsObject = services.Query.ToList();//phan luong ko can loc theo ky ke toan
            //            //services.UnbindSession(DsObject);
            //            //DsObject = services.Query.ToList();

            //        }
            //        else if (typeof(T) == typeof(SABill))
            //        {
            //            DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("InvoiceDate") >= dtFrom && k.GetProperty<T, DateTime>("InvoiceDate") <= dtTo).ToList();
            //            //services.UnbindSession(DsObject);
            //            //DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("InvoiceDate") >= dtFrom && k.GetProperty<T, DateTime>("InvoiceDate") <= dtTo).ToList();
            //        }
            //        else
            //        {
            //            string key = Activator.CreateInstance(typeof(T)).HasProperty("PostedDate") ? "PostedDate" : "Date";
            //            if (key == "PostedDate")
            //            {
            //                DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("PostedDate") >= dtFrom && k.GetProperty<T, DateTime>("PostedDate") <= dtTo).ToList();
            //                //services.UnbindSession(DsObject);
            //                //DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("PostedDate") >= dtFrom && k.GetProperty<T, DateTime>("PostedDate") <= dtTo).ToList();
            //            }
            //            else
            //            {
            //                DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("Date") >= dtFrom && k.GetProperty<T, DateTime>("Date") <= dtTo).ToList();
            //                //services.UnbindSession(DsObject);
            //                //DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("Date") >= dtFrom && k.GetProperty<T, DateTime>("Date") <= dtTo).ToList();
            //            }

            //            if (!Activator.CreateInstance(typeof(T)).HasProperty(key))
            //            {
            //                key = "IWDate";
            //                DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("IWDate") >= dtFrom && k.GetProperty<T, DateTime>("IWDate") <= dtTo).ToList();
            //                //services.UnbindSession(DsObject);
            //                //DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("IWDate") >= dtFrom && k.GetProperty<T, DateTime>("IWDate") <= dtTo).ToList();
            //            }
            //        }
            //        //end add by cuongpv
            //        //DsObject = this.GetIService((T)Activator.CreateInstance(typeof(T))).Query.ToList();//comment by cuongpv
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //    Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            //}
            LoadData();
            SetDataForuGrid();
            Shown += (s, e) => BaseBusiness_Load(s, e, DsObject);
            this.DeleteSelectedBorderOfUltraTabControl();
        }

        void initBaseBusiness(List<int> lstTypeId = null, int loaiForm = -1)
        {
            _loaiForm = loaiForm;//add by cuongpv
            //if (dsObject != null) DsObject = dsObject;
            //else DsObject = (List<T>)Activator.CreateInstance(typeof(List<T>));

            WaitingFrm.StartWaiting();
            //LoadControl();//add by cuongpv
            //LoadDuLieu();
            LoadControl();
            //TLoadControl = new Thread(LoadControl);
            //TLoadData = new Thread(new ThreadStart(() => LoadDataBase(lstTypeId, loaiForm)));
            //TLoadControl.IsBackground = true;
            //TLoadData.IsBackground = true;
            //TLoadControl.Start();
            //TLoadData.Start();
            //LoadControl();
            //LoadDataBase(lstTypeId, loaiForm);//comment by cuongpv
            //TLoadControl.Join();
            LoadDataBase(lstTypeId, loaiForm);//add by cuongpv
        }

        void BaseBusiness_Load<TD>(object sender, EventArgs e, List<TD> input)
        {
            ConfigButtons();
            //string key = Activator.CreateInstance(typeof(TD)).HasProperty("PostedDate") ? "PostedDate" : "Date";
            //if (!Activator.CreateInstance(typeof(TD)).HasProperty(key)) key = "IWDate";
            //if (IsGridShow)
            //{
            //    if (typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet) || typeof(T) == typeof(PSTimeSheetSummary))
            //    {
            //        DsObject = DsObject.OrderByStand("Year", "Month");
            //    }
            //    if (typeof(T) == typeof(SABill))
            //    {
            //        // edit by tungnt: sắp xếp số hóa đơn lớn -> bé
            //        // để ngày hóa đơn từ dd/MM/yyyy hh:mm:ss -> dd/MM/yyyy
            //        DsObject = DsObject.OrderByDescending(n => n.GetProperty<T, DateTime>("RefDateTime")).ThenByDescending(p => p.GetProperty("InvoiceTemplate")).ThenByDescending(p => p.GetProperty("InvoiceNo")).ToList();
            //    }
            //    else
            //        DsObject = DsObject.OrderByStand(key, "No");

            //    uGrid.DataSource = DsObject;
            //    FillColorRecordedGrid();
            //}

            //if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            if (defaultTypeId != 0)
            {
                TD selectItem = (TD)Activator.CreateInstance(typeof(TD));
                selectItem.SetProperty("TypeID", defaultTypeId);
                ConfigTtt(selectItem);
            }
            if (DsObject.Count == 0)
            {
                TD selectItem = (TD)Activator.CreateInstance(typeof(TD));
                ConfigTtt(selectItem);
            }
            uGrid0.Text = string.Empty;

            uGrid0.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            //ConfigGrid(selectItem);
            ConfigButtonsBySelected();

            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            //if (uGrid.Rows.Count == 0)
            //{ if (typeof(T) == typeof(MCAudit))
            //    { if (_select.HasProperty("TypeID"))
            //        { _select.SetProperty("TypeID", 180); }
            //        if (_select.HasProperty("TemplateID"))
            //        { _select.SetProperty("TemplateID", 0); }
            //    }
            //    if (typeof(T) == typeof(MBCreditCard))
            //    {
            //        if (_select.HasProperty("TypeID"))
            //        { _select.SetProperty("TypeID", 170); }
            //        if (_select.HasProperty("TemplateID"))
            //        { _select.SetProperty("TemplateID", 0); }
            //    }
            //    TLoadChilGrid = new Thread(new ThreadStart(() => GetTemplateChild(_select)));
            //    TLoadChilGrid.IsBackground = true;
            //    TLoadChilGrid.Start();
            //   // ConfigTtt(_select);
            //    ConfigButtonsBySelected();
            //    TLoadChilGrid.Join();
            //    ConfigGrid(_select);
            //}

            else WaitingFrm.StopWaiting();
            //LoadDuLieu();
            //ađ by trungnq
            if (uGrid.Rows.Count == 0)
            {
                if (typeof(T) == typeof(MCAudit))
                {
                    if (_select.HasProperty("TypeID"))
                    { _select.SetProperty("TypeID", 180); }
                    if (_select.HasProperty("TemplateID"))
                    { _select.SetProperty("TemplateID", 0); }
                }
                if (typeof(T) == typeof(MBCreditCard))
                {
                    if (_select.HasProperty("TypeID"))
                    { _select.SetProperty("TypeID", 170); }
                    if (_select.HasProperty("TemplateID"))
                    { _select.SetProperty("TemplateID", 0); }
                }
                if (typeof(T) == typeof(MBInternalTransfer))
                {
                    if (_select.HasProperty("TypeID"))
                    { _select.SetProperty("TypeID", 150); }
                    if (_select.HasProperty("TemplateID"))
                    { _select.SetProperty("TemplateID", 0); }
                }
                if (typeof(T) == typeof(TIAllocation))
                {
                    if (_select.HasProperty("TypeID"))
                    { _select.SetProperty("TypeID", 434); }
                    if (_select.HasProperty("TemplateID"))
                    { _select.SetProperty("TemplateID", 0); }
                }
                GetTemplateChild(_select);
                //TLoadChilGrid = new Thread(new ThreadStart(() => ));
                //TLoadChilGrid.IsBackground = true;
                //TLoadChilGrid.Start();
                // ConfigTtt(_select);
                ConfigButtonsBySelected();
                //TLoadChilGrid.Join();
                ConfigGrid(_select);// thêm bởi Trungnq 
            }
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunctionBase();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunctionBase();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunctionBase(true);
        }

        private void TsmReLoadClick(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            LoadDuLieu(false, true);
            WaitingFrm.StopWaiting();
        }

        private void TsmPostClick(object sender, EventArgs e)
        {
            RecordedFunctionBase(isSingleRow: true);
        }

        private void TsmUnPostClick(object sender, EventArgs e)
        {
            UnRecordedFunctionBase(isSingleRow: true);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunctionBase();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunctionBase();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunctionBase();
        }

        private void BtnRecordedClick(object sender, EventArgs e)
        {
            RecordedFunctionBase(isSingleRow: false);
        }

        private void BtnUnRecordedClick(object sender, EventArgs e)
        {
            UnRecordedFunctionBase(isSingleRow: false);
        }

        private void BtnResetClick(object sender, EventArgs e)
        {
            ResetFunctionBase();
        }

        private void BtnPrintClick(object sender, EventArgs e)
        {
            PrintFunctionBase();
        }

        private void BtnHelpClick(object sender, EventArgs e)
        {
            HelpFunctionBase();
        }
        private void BtnUpLoadClick(object sender, EventArgs e)
        {
            Frm.FBusiness.SalePrice.FSABill.UploadFileExcel uploadFileExcel = new Frm.FBusiness.SalePrice.FSABill.UploadFileExcel();
            uploadFileExcel.ShowDialog(this);
            if (uploadFileExcel.reset)
                ResetFunctionBase();
        }
        private void BtnExportExcel(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGrid, this._title);
        }
        private void ExportExcel(UltraGrid ultraGrid)
        {

        }
        #endregion

        /// <summary>
        /// Nghiệp vụ Add
        /// [override]
        /// </summary>
        void AddFunctionBase(int? typeId = null)
        {
            if (!Authenticate.Permissions("TH", _subSystemCode)) return;
            T temp = (T)Activator.CreateInstance(typeof(T));
            if (typeId.HasValue && temp.HasProperty("TypeID")) temp.SetProperty("TypeID", typeId.Value);
            ShowFrm(typeof(T) == typeof(PPService) ? default(T) : temp, ConstFrm.optStatusForm.Add);
        }
        public void ShowFrm1(T temp, int status, int _loaiForm)
        {
            //WaitingFrm.StartWaiting();
            bool isFrmDetailOk = false;
            string strName = string.Empty;
            try
            {
                string strTemp;
                switch (_loaiForm)
                {
                    case -9999:
                        strTemp = "Output";
                        break;
                    case -430:
                    case -500:  //H.A thêm -500 cho loại Form Mua và gia tăng TSCĐ
                        strTemp = "Buy";
                        break;
                    case -620:  //H.A thêm -620 cho loại Form Kết chuyển lãi, lỗ
                        strTemp = "ILTransfer";
                        break;
                    default:
                        strTemp = string.Empty;
                        break;
                }
                List<T> DsObject = null;
                DsObject.Add(temp);
                strName = _loaiForm != -840 ? (_loaiForm != -690 ? string.Format("Accounting.F{0}{1}Detail", typeof(T).Name, strTemp) : "Accounting.FGOtherVoucherExpenseAllocationDetail") : "Accounting.FPSOtherVoucherDetail";
                System.Type type = System.Type.GetType(strName);
                if (type == null) return;
                List<int> lstTypeFrmNotStand = new List<int> { -1, -500, -510, -600, -620, -9998, -9999, -430, -907, -840, -690 };  //danh sách những loại frm dùng hàm mặc định CreateInstance(type, temp, DsObject, status)
                var frm = lstTypeFrmNotStand.Contains(_loaiForm) ? (CustormForm)Activator.CreateInstance(type, temp, DsObject, status) : (CustormForm)Activator.CreateInstance(type, temp, DsObject, status, _loaiForm);
                frm.TopLevel = true;
                //frm.FormClosing += (s, e) => DetailBase_FormClosing(s, e, status);
                //frm.FormClosed += (s, e) => DetailBase_FormClosed(s, e, status);
                if (frm is DetailBase<T>)
                {
                    ((DetailBase<T>)frm).SubSystemCode = _subSystemCode;
                    ((DetailBase<T>)frm).defaultTypeId = _loaiForm;
                }
                if (!frm.IsDisposed)
                {
                    frm.Show(this);
                }
                isFrmDetailOk = true;
            }
            catch (Exception exception)
            {
                //View_Exception(exception);
                WaitingFrm.StopWaiting();
                MSG.Warning(isFrmDetailOk ? exception.StackTrace : string.Format("Vui lòng kiểm tra lại frm {0} \r\n Chi tiết lỗi: \r\n\r\n {2} \r\n\r\n {1}", strName.Replace("Accounting.", ""), exception.StackTrace, exception.Message));
            }
        }
        public void ShowFrm(T temp, int status)
        {
            //WaitingFrm.StartWaiting();
            bool isFrmDetailOk = false;
            string strName = string.Empty;
            try
            {
                string strTemp;
                switch (_loaiForm)
                {
                    case -9999:
                        strTemp = "Output";
                        break;
                    case -430:
                    case -500:  //H.A thêm -500 cho loại Form Mua và gia tăng TSCĐ
                        strTemp = "Buy";
                        break;
                    case -620:  //H.A thêm -620 cho loại Form Kết chuyển lãi, lỗ
                        strTemp = "ILTransfer";
                        break;
                    default:
                        strTemp = string.Empty;
                        break;
                }
                //WaitingFrm.StartWaiting();
                Utils.lstCombo.Clear();
                Utils.lstComboUGrid.Clear();
                strName = _loaiForm != -840 ? (_loaiForm != -690 ? string.Format("Accounting.F{0}{1}Detail", typeof(T).Name, strTemp) : "Accounting.FGOtherVoucherExpenseAllocationDetail") : "Accounting.FPSOtherVoucherDetail";
                System.Type type = System.Type.GetType(strName);
                //Danh sách form không dùng WaitingFrm
                if (strName != "Accounting.FTIAllocationDetail"
                 && strName != "Accounting.FFADepreciationDetail"
                 && strName != "Accounting.FPPDiscountReturnDetail"
                 && strName != "Accounting.FTT153ReportDetail"
                 && strName != "Accounting.FPSTimeSheetDetail"
                 && strName != "Accounting.FPSTimeSheetSummaryDetail"
                 && strName != "Accounting.FPSSalarySheetDetail"
                 && strName != "Accounting.FPSOtherVoucherDetail") WaitingFrm.StartWaiting();
                if (type == null) return;
                List<int> lstTypeFrmNotStand = new List<int> { -1, -500, -510, -600, -620, -9998, -9999, -430, -907, -840, -690 };  //danh sách những loại frm dùng hàm mặc định CreateInstance(type, temp, DsObject, status)
                                                                                                                                    //using (var frm = lstTypeFrmNotStand.Contains(_loaiForm) ? (CustormForm)Activator.CreateInstance(type, temp, DsObject, status) : (CustormForm)Activator.CreateInstance(type, temp, DsObject, status, _loaiForm))
                                                                                                                                    //{
                                                                                                                                    //    log.Error("HUYPDLOG" + strName + ": " + frm.Text);
                                                                                                                                    //    frm.TopLevel = true;
                                                                                                                                    //    frm.FormClosing += (s, e) => DetailBase_FormClosing(s, e, status);
                                                                                                                                    //    frm.FormClosed += (s, e) => DetailBase_FormClosed(s, e, status);
                                                                                                                                    //    frm.Shown += (s, e) => DetailBase_Show(s, e);
                                                                                                                                    //    if (frm is DetailBase<T>)
                                                                                                                                    //    {
                                                                                                                                    //        ((DetailBase<T>)frm).SubSystemCode = _subSystemCode;
                                                                                                                                    //        ((DetailBase<T>)frm).defaultTypeId = this.defaultTypeId != 0 ? this.defaultTypeId : (this.lstTypeId != null ? this.lstTypeId.OrderBy(x => x).FirstOrDefault() : 0);
                                                                                                                                    //    }
                                                                                                                                    //    if (!frm.IsDisposed)
                                                                                                                                    //    {
                                                                                                                                    //        frm.Show(this);
                                                                                                                                    //    }
                                                                                                                                    //    isFrmDetailOk = true;
                                                                                                                                    //    frm.Dispose();
                                                                                                                                    //}

                //using (var frm = lstTypeFrmNotStand.Contains(_loaiForm) ? (CustormForm)Activator.CreateInstance(type, temp, DsObject, status) : (CustormForm)Activator.CreateInstance(type, temp, DsObject, status, _loaiForm))
                //{
                //var frm = new CustormForm();
                //switch (strName)
                //{
                //    case "Accounting.FTIAllocationDetail":
                //        frm = (CustormForm)new FTIAllocationDetail(temp as TIAllocation, DsObject as List<TIAllocation>, status);
                //        break;
                //    default:
                //        frm = lstTypeFrmNotStand.Contains(_loaiForm) ? (CustormForm)Activator.CreateInstance(type, temp, DsObject, status) : (CustormForm)Activator.CreateInstance(type, temp, DsObject, status, _loaiForm);
                //        break;
                //}
                var frm = new CustormForm();
                frm = lstTypeFrmNotStand.Contains(_loaiForm) ? (CustormForm)Activator.CreateInstance(type, temp, DsObject, status) : (CustormForm)Activator.CreateInstance(type, temp, DsObject, status, _loaiForm);
                log.Error("HUYPDLOG" + strName + ": " + frm.Text);
                frm.TopLevel = true;
                frm.FormClosing += (s, e) => DetailBase_FormClosing(s, e, status);
                frm.FormClosed += (s, e) => DetailBase_FormClosed(s, e, status);
                frm.Shown += (s, e) => DetailBase_Show(s, e);
                if (frm is DetailBase<T>)
                {
                    ((DetailBase<T>)frm).SubSystemCode = _subSystemCode;
                    ((DetailBase<T>)frm).defaultTypeId = this.defaultTypeId != 0 ? this.defaultTypeId : (this.lstTypeId != null ? this.lstTypeId.OrderBy(x => x).FirstOrDefault() : 0);
                }
                if (!frm.IsDisposed)
                {
                    frm.Show(this);
                }
                isFrmDetailOk = true;
                //frm.Dispose();
                //}
            }
            catch (Exception exception)
            {
                View_Exception(exception);
                WaitingFrm.StopWaiting();
                MSG.Warning(isFrmDetailOk ? exception.StackTrace : string.Format("Vui lòng kiểm tra lại frm {0} \r\n Chi tiết lỗi: \r\n\r\n {2} \r\n\r\n {1}", strName.Replace("Accounting.", ""), exception.StackTrace, exception.Message));
            }
        }
        private readonly log4net.ILog log = log4net.LogManager.GetLogger("Accounting.BaseForm");//add log by cuongpv
        private void View_Exception(Exception ex)
        {
            var traceback = ex.StackTrace;
            var data = ex.Data;
            var message = ex.Message;
        }

        private void DetailBase_FormClosing(object sender, FormClosingEventArgs e, int status)
        {
            var frm = (Form)sender;
            if (frm.GetProperty<Form, bool>("IsCloseNonStatic"))
            {
                //WaitingFrm.StartWaiting();
                //if (uGrid.DataSource != null)
                //    LoadDuLieu(isClose: true, isFAdd: status == ConstFrm.optStatusForm.Add);
                //if (frm is DetailBase<T>)
                //{
                //    var @base = frm as DetailBase<T>;
                //    if (status == ConstFrm.optStatusForm.Add)
                //    {
                //        var gId = @base._select.GetProperty<T, Guid?>("ID");
                //        if (gId.HasValue)
                //        {
                //            if (uGrid.Rows != null)
                //            {
                //                var rowsSelect = uGrid.Rows.Where(row => gId.Value == (Guid?)row.Cells["ID"].Value);
                //                uGrid.ActiveRow = rowsSelect.Any() ? rowsSelect.FirstOrDefault() : uGrid.Rows.FirstOrDefault();
                //                if (uGrid.ActiveRow != null)
                //                {
                //                    uGrid.Selected.Rows.Clear();
                //                    uGrid.ActiveRow.Selected = true;
                //                    uGrid.ActiveRow.ScrollRowToMiddle();
                //                    _notEvent = false;
                //                    //UGridAfterSelectChangeBase(uGrid, null);
                //                }
                //            }
                //        }
                //    }

                //}
                //WaitingFrm.StopWaiting();

                WaitingFrm.StartWaiting();
                LoadData();
                SetDataForuGrid();
                if (frm is DetailBase<T>)
                {
                    var @base = frm as DetailBase<T>;
                    if (status == ConstFrm.optStatusForm.Add)
                    {
                        var gId = @base._select.GetProperty<T, Guid?>("ID");
                        if (gId.HasValue)
                        {
                            if (uGrid.Rows != null)
                            {
                                var rowsSelect = uGrid.Rows.Where(row => gId.Value == (Guid?)row.Cells["ID"].Value);
                                uGrid.ActiveRow = rowsSelect.Any() ? rowsSelect.FirstOrDefault() : uGrid.Rows.FirstOrDefault();
                                if (uGrid.ActiveRow != null)
                                {
                                    uGrid.Selected.Rows.Clear();
                                    uGrid.ActiveRow.Selected = true;
                                    uGrid.ActiveRow.ScrollRowToMiddle();
                                    _notEvent = false;
                                    //UGridAfterSelectChangeBase(uGrid, null);
                                }
                            }
                        }
                    }

                }
                WaitingFrm.StopWaiting();
            }
        }

        // Add by Hautv tối ưu tốc độ- fix tăng ram
        // Load Dữ liệu
        private void LoadData()
        {
            DateTime dtFrom = dtBeginDate.DateTime.Date;
            DateTime dtTo = dtEndDate.DateTime.Date;
            dtFrom = dtFrom.AddHours(0).AddMinutes(0).AddSeconds(0);
            dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);


            if (dtBeginDate.Value == null || dtEndDate.Value == null)
            {
                MSG.Warning("Ngày không được để trống");
                return;
            }

            if (dtFrom > dtTo)
            {
                MSG.Warning("Từ ngày không được lớn hơn đến ngày");
                return;
            }

            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            BaseService<T, Guid> services = this.GetIService((T)Activator.CreateInstance(typeof(T)));
            services.UnbindSession(_select);
            if (typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet) || typeof(T) == typeof(PSTimeSheetSummary) || typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
                || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice))
            {
                DsObject = services.Query.ToList();//phan luong ko can loc theo ky ke toan
            }
            else if (typeof(T) == typeof(SABill))
            {
                DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("InvoiceDate") >= dtFrom && k.GetProperty<T, DateTime>("InvoiceDate") <= dtTo).ToList();
            }
            else
            {
                string key1 = Activator.CreateInstance(typeof(T)).HasProperty("PostedDate") ? "PostedDate" : "Date";
                if (key1 == "PostedDate")
                {
                    DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("PostedDate") >= dtFrom && k.GetProperty<T, DateTime>("PostedDate") <= dtTo).ToList();
                }
                else
                {
                    DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("Date") >= dtFrom && k.GetProperty<T, DateTime>("Date") <= dtTo).ToList();
                }

                if (!Activator.CreateInstance(typeof(T)).HasProperty(key1))
                {
                    key1 = "IWDate";
                    DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("IWDate") >= dtFrom && k.GetProperty<T, DateTime>("IWDate") <= dtTo).ToList();
                }
            }
            switch (_loaiForm)
            {
                case -9999: //xuất kho
                    {
                        List<int> lstTypeId = new List<int> { 410, 411, 412, 413, 414, 415 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -9998: //nhập kho
                    {
                        List<int> lstTypeId = new List<int> { 400, 401, 402, 403, 404 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -500://Mua TSCĐ
                    {
                        List<int> lstTypeId = new List<int> { 500, 119, 129, 132, 142, 172 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -430://Mua TSCĐ
                    {
                        List<int> lstTypeId = new List<int> { 430, 902, 903, 904, 905, 906 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -907: //Ghi tăng TSCĐ
                    {
                        List<int> lstTypeId = new List<int> { 907 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();

                    }
                    break;
                case -510: //Ghi tăng TSCĐ
                    {
                        List<int> lstTypeId = new List<int> { 510 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();

                    }
                    break;
                case -600: //Chứng từ nghiệp vụ khác
                    {
                        List<int> lstTypeId = new List<int> { 600, 601, 602, 660, 670 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -690: //Chứng từ nghiệp vụ khác
                    {
                        List<int> lstTypeId = new List<int> { 690 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -620: //Kết chuyển lãi, lỗ
                    {
                        List<int> lstTypeId = new List<int> { 620 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                case -840: //Chứng từ nghiệp vụ khác
                    {
                        List<int> lstTypeId = new List<int> { 840 };
                        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                    }
                    break;
                //case -220: //Hàng mua trả lại
                //    {
                //        List<int> lstTypeId = new List<int> { 220, 230 };
                //        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                //    }
                //    break;
                //case -230: //Hàng mua giảm giá
                //    {
                //        List<int> lstTypeId = new List<int> { 230 };
                //        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                //    }
                //    break;
                case 0:
                    {
                        if (typeof(T) == typeof(PPInvoice))
                            //Mua hàng có qua kho
                            //List<int> lstTypeId = new List<int> { 210, 260, 261, 262, 263, 264 };
                            DsObject = DsObject.Where(k => k.GetProperty<T, bool>("StoredInRepository")).ToList();
                        else if (typeof(T) == typeof(SAInvoice))
                        {//Bán hàng chưa thu tiền
                            List<int> lstTypeId = new List<int> { 320 };
                            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                        }
                    }
                    break;
                case 1: //Mua hàng ko qua kho
                    {
                        if (typeof(T) == typeof(PPInvoice))
                            //List<int> lstTypeId = new List<int> { 210, 260, 261, 262, 263, 264 };
                            DsObject = DsObject.Where(k => !k.GetProperty<T, bool>("StoredInRepository")).ToList();
                        else if (typeof(T) == typeof(SAInvoice))
                        {//Bán hàng thu tiền ngay
                            List<int> lstTypeId = new List<int> { 321, 322 };
                            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                        }
                    }
                    break;
                case 2:
                    {
                        if (typeof(T) == typeof(SAInvoice))
                        {//Bán hàng đại lý bán đúng giá nhận ủy thác xuất nhập khẩu
                            List<int> lstTypeId = new List<int> { 323, 324, 325 };
                            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
                        }
                    }
                    break;
                case -1:
                    {
                        if (typeof(T) == typeof(TIInit))
                        {
                            lstTypeId = new List<int> { 436 };
                            TIInit tiinit = (TIInit)Activator.CreateInstance(typeof(TIInit));
                            List<string> lstTypeIdStr = new List<string> { "ID", "Quantity", "UnitPrice", "Amount", "PostedDate" };
                            List<string> lstTypeIdStrVni = new List<string> { "Thành phẩm", "Số lượng", "Đơn giá", "Giá trị tồn CCDC", "Ngày ghi sổ" };
                            ConfigTtt2(tiinit, 436, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                        }
                    }
                    break;
            }

        }

        // Add by Hautv fix bug tang ram
        // set data and color
        private void SetDataForuGrid()
        {
            string key = Activator.CreateInstance(typeof(T)).HasProperty("PostedDate") ? "PostedDate" : "Date";
            if (!Activator.CreateInstance(typeof(T)).HasProperty(key)) key = "IWDate";
            if (typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet) || typeof(T) == typeof(PSTimeSheetSummary))
            {
                DsObject = DsObject.OrderByStand("Year", "Month");
            }
            if (typeof(T) == typeof(SABill))
            {
                // edit by tungnt: sắp xếp số hóa đơn lớn -> bé
                // để ngày hóa đơn từ dd/MM/yyyy hh:mm:ss -> dd/MM/yyyy
                DsObject = DsObject.OrderByDescending(n => n.GetProperty<T, DateTime>("RefDateTime")).ThenByDescending(p => p.GetProperty("InvoiceTemplate")).ThenByDescending(p => p.GetProperty("InvoiceNo")).ToList();
            }
            else
                DsObject = DsObject.OrderByStand(key, "No");
            if (uGrid != null)
            {
                uGrid.DataSource = DsObject;
                FillColorRecordedGrid();
            }
        }

        private void DetailBase_Show(object sender, EventArgs e)
        {
            WaitingFrm.StopWaiting();
        }

        private void DetailBase_FormClosed(object sender, FormClosedEventArgs e, int status)
        {
            Utils.lstCombo.Clear();
            Utils.lstComboUGrid.Clear();
            var frm = (Form)sender;
            if (frm.GetProperty<Form, bool>("RestartForm"))
            {
                if (frm is DetailBase<T>)
                {
                    var @base = frm as DetailBase<T>;
                    var initSelect = @base.initSelect.CloneObject();
                    @base.Dispose();
                    ShowFrm(@base.initSelect, ConstFrm.optStatusForm.Add);
                }
            }
            frm.Dispose();
            GC.Collect();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// [override]
        /// </summary>
        void EditFunctionBase()
        {
            if (uGrid.ActiveRow != null)
            {
                //if (!Authenticate.Permissions("SU", _subSystemCode)) return;
                bool a = uGrid.Selected.Rows.Count > 0;
                bool b = uGrid.ActiveRow != null;
                if (uGrid.ActiveRow.IsFilterRow) return;
                if (a || b)
                {
                    T temp = (T)(b ? uGrid.ActiveRow.ListObject : uGrid.Selected.Rows[0].ListObject);
                    if (this.IsVocherExit(temp)) { MSG.Error(resSystem.MSG_Error_13); return; }
                    _select = temp;
                    ShowFrm(temp, ConstFrm.optStatusForm.View);
                }
                else
                    MSG.Error(resSystem.MSG_System_04);
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }
        public void EditFunctionBase1(object voucher)
        {
            //if (!Authenticate.Permissions("SU", _subSystemCode)) return;

            T temp = (T)(voucher);
            if (this.IsVocherExit(temp)) { MSG.Error(resSystem.MSG_Error_13); return; }
            _select = temp;
            ShowFrm(temp, ConstFrm.optStatusForm.View);

        }
        /// <summary>
        /// Nghiệp vụ Delete
        /// [override]
        /// </summary>
        void DeleteFunctionBase(bool isSingleRow = false)
        {
            if (!Authenticate.Permissions("XO", _subSystemCode)) return;
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (uGrid.ActiveRow.IsFilterRow) return;
            if (a || b)
            {
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                if (!a) { _notEvent = true; lst.Add(uGrid.ActiveRow); _notEvent = false; }
                if (isSingleRow && a) { lst.Clear(); lst.Add(uGrid.ActiveRow); }
                //Lấy index item sát trên item cuối cùng được select
                //var lastItem = (T) lst[lst.Count - 1].ListObject;
                List<int> lstIndex = (from UltraGridRow row in lst orderby row.Index select row.Index).ToList();
                List<int> lstIndexUnSelect =
                    (from UltraGridRow row in uGrid.Rows where !lstIndex.Contains(row.Index) orderby row.Index select row.Index
                    ).ToList();
                int? index = lstIndex.Count > 0 ? lstIndexUnSelect.Cast<int?>().LastOrDefault(p => p < lstIndex[lstIndex.Count - 1]) : (int?)null;
                //Lấy Id của item sát trên item cuối cùng được select
                Guid? id = null;
                if (index != null && uGrid.DisplayLayout.Bands[0].Columns.Exists("ID")) id = uGrid.Rows[(int)index].Cells["ID"].Value as Guid?;
                string report = null;
                if (typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
                         || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice))
                {
                    report = "Bạn muốn xóa không?";
                }
                if (typeof(T) == typeof(SABill))
                {
                    report = "Bạn có chắc chắn muốn xóa hóa đơn này?";
                }
                else
                {
                    report = "Bạn có muốn xóa những chứng từ này không?";
                }
                if (MSG.Question(report) == DialogResult.Yes && lst.Count > 0)
                {
                    var strResult = new List<Result>();
                    foreach (UltraGridRow ultraGridRow in lst)
                    {
                        T temp = (T)ultraGridRow.ListObject;
                        //đang ghi sổ thì ko được phép xóa
                        string key = "Recorded";
                        var result = new Result();
                        if (temp != null && temp.HasProperty("Date")) result.Date = temp.GetProperty<T, DateTime?>("Date");
                        if (temp != null && temp.HasProperty("No")) result.No = temp.GetProperty<T, string>("No");
                        if (temp != null && temp.HasProperty("TypeID"))
                        {
                            var typeId = temp.GetProperty<T, int?>("TypeID");
                            if (typeId != null)
                            {
                                var type = Utils.ListType.FirstOrDefault(x => typeId != null && x.ID == typeId.Value);
                                if (type != null)
                                    result.TypeName = type.TypeName;
                                if (new[] { 404, 411 }.Contains(typeId.Value))
                                {
                                    result.Reason = resSystem.MSG_Error_22;
                                    result.IsSucceed = false;
                                    strResult.Add(result);
                                    continue;
                                }
                                if (CheckTSCD(typeId, temp)) continue;
                            }


                        }
                        if (temp != null && temp.HasProperty(key) && temp.GetProperty<T, bool>(key))
                        {
                            if (temp.HasProperty("TypeID"))
                            {
                                if (temp.GetProperty("TypeID").ToString() == "900" || temp.GetProperty("TypeID").ToString() == "901")
                                {
                                }
                                else
                                {
                                    if (lst.Count == 1) MSG.Warning(resSystem.MSG_Error_14);
                                    result.Reason = resSystem.MSG_Error_14;
                                    result.IsSucceed = false;
                                    strResult.Add(result);
                                    continue;
                                }
                            }
                            else
                            {
                                if (lst.Count == 1) MSG.Warning(resSystem.MSG_Error_14);
                                result.Reason = resSystem.MSG_Error_14;
                                result.IsSucceed = false;
                                strResult.Add(result);
                                continue;
                            }
                        }

                        //chứng từ không tồn tại hoặc xóa bởi người dùng khác
                        if (this.IsVocherExit(temp))
                        {
                            if (lst.Count == 1) MSG.Warning(resSystem.MSG_Error_13);
                            result.Reason = resSystem.MSG_Error_13;
                            result.IsSucceed = false;
                            strResult.Add(result);
                            continue;
                        }
                        //Bắt ràng buộc khi xóa Bảng Lương
                        if (typeof(T) == typeof(PSSalarySheet) && temp.GetProperty<T, Guid?>("GOtherVoucherID") != null)
                        {
                            MSG.Error("Không được xóa Bảng Lương đã được hạch toán");
                            return;
                        }
                        //xóa chứng từ
                        if (temp == null) goto error1;
                        var properties = new string[] { "No", "IWNo", "OWNo" };
                        key = properties.FirstOrDefault(s => temp.HasProperty(s) && !string.IsNullOrEmpty(temp.GetProperty<T, string>(s)));
                        //Thêm điều kiện dành riêng cho Khai báo CCDC đầu kỳ
                        int? typeid = temp.GetProperty<T, int?>("TypeID");
                        if (!string.IsNullOrEmpty(key) || (temp.HasProperty("TypeID") && (new int[] { 810, 811, 820, 821, 830, 831, 832, 834 }.Any(x => x == typeid)))) goto next;
                        if (typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
                         || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice) || typeof(T) == typeof(SABill)) goto next;
                        error1:
                        result.Reason = "Không có số chứng từ";
                        result.IsSucceed = false;
                        strResult.Add(result);
                        continue;
                    next:
                        // Bỏ do bên trên đã hỏi khách hàng chắc chắn xoá, và tránh để messagebox trong vòng lặp,
                        //if (lst.Count == 1 && MSG.Question(string.Format(resSystem.MSG_System_05, temp.GetProperty<T, string>(key))) != DialogResult.Yes)
                        //    return;
                        #region Xóa chứng từ bán hàng kèm hóa đơn đã phát hành
                        if (new int[] { 326 }.Any(x => x == _select.GetProperty<T, int>("TypeID")) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data == "1" && _select.GetProperty<T, int>("InvoiceForm") == 2 && _select.GetProperty<T, string>("InvoiceNo") != "" && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo")))
                        {
                            MSG.Warning("Xóa hóa đơn thất bại! Hóa đơn này đã được cấp số.");
                            return;
                        }
                        if (new int[] { 320, 321, 322, 323, 324, 325, 340, 220 }.Any(x => x == _select.GetProperty<T, int>("TypeID")) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data == "1" && _select.GetProperty<T, int>("InvoiceForm") == 2 && _select.GetProperty<T, string>("InvoiceNo") != "" && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo")))
                        {
                            MSG.Warning("Xóa chứng từ thất bại! Chứng từ này đã phát sinh hóa đơn được cấp số.");
                            return;
                        }
                        else if (new int[] { 320, 321, 322, 323, 324, 325, 326 }.Any(x => x == _select.GetProperty<T, int>("TypeID")) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data != "1")
                        {
                            if (_select.GetProperty<T, int>("TypeID") == 326)
                            {
                                var sabill = _select as SABill;
                                if (Utils.ListSAInvoice.Where(x => x.BillRefID == sabill.ID).ToList().Count == 0 &&
                                     sabill.InvoiceNo != null && !string.IsNullOrEmpty(sabill.InvoiceNo))
                                {
                                    if (MSG.MessageBoxStand("Hóa đơn này đã có số. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                                }
                                else if (Utils.ListSAInvoice.Where(x => x.BillRefID == sabill.ID).ToList().Count > 0)
                                {
                                    if (MSG.MessageBoxStand("Hóa đơn này phát sinh liên quan đến chứng từ khác. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                                }
                            }
                            else
                            {
                                var saIn = _select as SAInvoice;
                                if (saIn.BillRefID == null && saIn.InvoiceNo != null && !string.IsNullOrEmpty(saIn.InvoiceNo))
                                {
                                    if (MSG.MessageBoxStand("Chứng từ này đã phát sinh hóa đơn. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                                }
                                else if (saIn.BillRefID != null && saIn.BillRefID != Guid.Empty)
                                {
                                    if (MSG.MessageBoxStand("Chứng từ này đã phát sinh hóa đơn. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                                }
                            }
                        }
                        #region Xóa hóa đơn (điện tử mới tạo lập) add by Hautv
                        if (typeof(T) == typeof(SABill) || typeof(T) == typeof(SAInvoice) || typeof(T) == typeof(SAReturn) || typeof(T) == typeof(PPDiscountReturn))
                        {
                            try
                            {
                                if (Utils.tichHopHDDT() && temp.GetProperty("InvoiceForm") != null && temp.GetProperty("InvoiceForm").ToString() == "2")
                                {
                                    string mess = "";
                                    SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                                    if (!string.IsNullOrEmpty(systemOption.Data))
                                    {
                                        SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                                        if (supplierService.SupplierServiceCode == "SDS")
                                        {
                                            RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                                            var requestDL = new Request();
                                            requestDL.Ikey = temp.GetProperty("ID").ToString();
                                            requestDL.Pattern = temp.GetProperty("InvoiceTemplate").ToString();
                                            requestDL.Serial = temp.GetProperty("InvoiceSeries").ToString();
                                            Response responseDL = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "XoaHDChuaPhatHanh-Svr").ApiPath, requestDL);
                                            if (responseDL != null && responseDL.Status != 2) // Xóa hóa đơn không thành công hoặc không tồn tại trong hệ thống
                                            {
                                                mess += responseDL.Message;
                                                if (!mess.IsNullOrEmpty())
                                                {
                                                    if (DialogResult.Yes != MessageBox.Show(mess +
                                                    "\n Nếu tiếp tục thực hiện dữ liệu sẽ không được đồng bộ giữa phần mềm kế toán và hóa đơn điện tử " +
                                                    "\n Bạn có chắc chắn muốn tiếp tục thao tác này ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                                    {
                                                        return;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                // Xóa thành công 
                                            }
                                        }
                                    }
                                    else
                                    {
                                        mess += "Chưa kết nối hóa đơn điện tử";
                                        if (DialogResult.Yes != MessageBox.Show("Đồng bộ dữ liệu thất bại do mất kết nối hóa đơn điện tử " +
                                                    "\n Nếu tiếp tục thực hiện dữ liệu sẽ không được đồng bộ giữa phần mềm kế toán và hóa đơn điện tử " +
                                                    "\n Bạn có chắc chắn muốn tiếp tục thao tác này ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                        {
                                            return;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (DialogResult.Yes != MessageBox.Show("Đồng bộ dữ liệu thất bại do mất kết nối hóa đơn điện tử " +
                                                    "\n Nếu tiếp tục thực hiện dữ liệu sẽ không được đồng bộ giữa phần mềm kế toán và hóa đơn điện tử " +
                                                    "\n Bạn có chắc chắn muốn tiếp tục thao tác này ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                {
                                    return;
                                }
                            }
                        }
                        #endregion
                        #endregion
                        #region Chứng từ tham chiếu nào đã bị xóa thì xóa luôn trong db lưu tham chiếu
                        Guid iD = temp.GetProperty<T, Guid>("ID");
                        List<RefVoucherRSInwardOutward> lstRefVoucherRS = Utils.IRefVoucherRSInwardOutwardService.GetByRefID2(iD).ToList();
                        if (lstRefVoucherRS.Count != 0)
                        {
                            Utils.IRefVoucherRSInwardOutwardService.BeginTran();
                            foreach (RefVoucherRSInwardOutward item in lstRefVoucherRS)
                            {
                                Utils.IRefVoucherRSInwardOutwardService.Delete(item);
                            }
                            Utils.IRefVoucherRSInwardOutwardService.CommitTran();
                        }


                        //Guid iD = temp.GetProperty<T, Guid>("ID");
                        List<RefVoucher> lstRefVoucher = Utils.IRefVoucherService.GetByRefID2(iD).ToList();
                        if (lstRefVoucher.Count != 0)
                        {
                            Utils.IRefVoucherService.BeginTran();
                            foreach (RefVoucher item in lstRefVoucher)
                            {
                                Utils.IRefVoucherService.Delete(item);
                            }
                            Utils.IRefVoucherService.CommitTran();
                        }
                        #endregion
                        DeleteFunction(temp);
                        result.IsSucceed = true;
                        strResult.Add(result);

                    }
                    if (lst.Count > 1 && strResult.Count > 0) new MsgResult(strResult, true).ShowDialog(this);



                    //WaitingFrm.StartWaiting();
                    LoadDuLieu();
                    if (id != null)
                        uGrid.ActiveRow = (from UltraGridRow row in uGrid.Rows where (row.Cells["ID"].Value as Guid?) == id select row).
                                FirstOrDefault();
                    else
                        uGrid.ActiveRow = uGrid.Rows.FirstOrDefault();
                    if (uGrid.ActiveRow != null)
                    {
                        uGrid.Selected.Rows.Clear();
                        uGrid.ActiveRow.Selected = true;
                        uGrid.ActiveRow.ScrollRowToMiddle();
                        _notEvent = false;
                        //UGridAfterSelectChangeBase(uGrid, null);
                    }
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }

        private bool CheckTSCD(int? typeId, T temp)
        {
            string fixedassetid = temp.GetProperty<T, string>("FixedAssetID");
            const string message = "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!";
            // Nếu là danh mục TSCĐ, kiểm tra xem đã ghi tăng chưa
            // Nếu đã ghi tăng, thông báo
            // "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!"
            if (typeof(T) == typeof(FixedAsset) && IFixedAssetService.CheckDeleteFixedAsset(new Guid(fixedassetid)))
            {
                MSG.Warning(message);
                return true;
            }
            // Nếu là ghi tăng tài sản cố định, kiểm tra xem đã giảm, tính khấu hao, điều chuyển, điều chỉnh chưa
            // Nếu đã thực hiện 1 trong những tác vụ trên thông báo :
            // "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!"
            if (new[] { 500, 119, 129, 132, 142, 172, 510 }.Contains(typeId.Value) && (typeof(T) == typeof(FAIncrement)))
            {
                var FAIncrement = (FAIncrement)(object)temp;
                List<Guid?> fixedassetids = new List<Guid?>();
                foreach (FAIncrementDetail detail in FAIncrement.FAIncrementDetails)
                {
                    fixedassetids.Add(detail.FixedAssetID);
                }
                if (IFixedAssetService.CheckDeleteIncrement(fixedassetids))
                {
                    MSG.Warning(message);
                    return true;
                }
            }
            else if (new[] { 430, 902, 903, 904, 905, 906, 907 }.Contains(typeId.Value) && (typeof(T) == typeof(TIIncrement)))
            {
                var TIIncrement = (TIIncrement)(object)temp;
                List<Guid?> TIIncrementIDs = new List<Guid?>();
                foreach (TIIncrementDetail detail in TIIncrement.TIIncrementDetails)
                {
                    if (detail.DepartmentID == null) TIIncrementIDs.Add(detail.ToolsID);
                }
                if (ITIInitService.CheckDeleteIncrement(TIIncrementIDs))
                {
                    MSG.Warning("Không thể xóa CCDC này vì tài sản này đã có phát sinh chứng từ liên quan!");
                    return true;
                }
            }
            return false;
        }

        void DeleteFunction(T temp)
        {
            try
            {
                if (ConstFrm.optBizFollowConst.status == ConstFrm.optBizFollowConst.NotSaveleged) UnRecordedFunctionBase(temp);
                BaseService<T, Guid> services = this.GetIService(temp);
                //try
                //{
                //    services.BeginTran();
                //    services.Delete(temp);
                //    services.CommitTran();
                //}
                //catch (Exception)
                //{//lỗi khi thực thi services
                //    services.RolbackTran();
                //    //MSG.Error("abcdefgh....");
                //}
                try
                {
                    services.BeginTran();
                    //if (runRsInwardOutward) IRSInwardOutwardService.Delete(rsInwardOutward);
                    var guid = temp.GetProperty<T, Guid>("ID");
                    #region Xóa nghiệm thu

                    #endregion
                    #region Hóa đơn
                    if (typeof(T) == typeof(TT153Report))
                    {
                        if (Utils.ITT153RegisterInvoiceDetailService.GetAll().Count(n => n.TT153ReportID == (Guid)guid) > 0)
                        {
                            MSG.Warning("Mẫu hóa đơn đã được đăng ký sử dụng");
                            services.RolbackTran();
                            return;
                        }
                    }
                    if (typeof(T) == typeof(TT153RegisterInvoice))
                    {
                        foreach (TT153RegisterInvoiceDetail item in Utils.ITT153RegisterInvoiceService.Getbykey((Guid)guid).TT153RegisterInvoiceDetail)
                        {
                            if (Utils.ITT153PublishInvoiceDetailService.GetAll().Count(n => n.TT153ReportID == item.TT153ReportID) > 0)
                            {
                                MSG.Warning("Mẫu hóa đơn đã được thông báo phát hành");
                                services.RolbackTran();
                                return;
                            }
                        }
                    }
                    if (typeof(T) == typeof(TT153PublishInvoice))
                    {
                        foreach (TT153PublishInvoiceDetail item in Utils.ITT153PublishInvoiceService.Getbykey((Guid)guid).TT153PublishInvoiceDetails)
                        {
                            TT153Report tT153Report = Utils.ITT153ReportService.Getbykey(item.TT153ReportID);
                            if (tT153Report != null)
                            {
                                if (ISAInvoiceService.GetListSAInVoiceTT153(tT153Report.InvoiceTypeID, tT153Report.InvoiceForm, tT153Report.InvoiceTemplate, tT153Report.InvoiceSeries).Where(n => int.Parse(n.InvoiceNo) > int.Parse(item.FromNo) && int.Parse(n.InvoiceNo) < int.Parse(item.ToNo)).ToList().Count > 0)
                                {
                                    MSG.Warning("Không thể xóa vì đã có phát sinh chứng từ liên quan");
                                    services.RolbackTran();
                                    return;
                                }
                            }
                        }
                    }
                    #endregion
                    services.Delete(guid);
                    if (temp.HasProperty("TypeID") && temp.GetProperty<T, int>("TypeID") == 600 && temp.GetProperty<T, Guid?>("CPPeriodID") != null && temp.GetProperty<T, Guid?>("CPAcceptanceID") != null)
                    {
                        var md = Utils.ListCPPeriod.FirstOrDefault(x => x.ID == temp.GetProperty<T, Guid>("CPPeriodID"));
                        var act = md.CPAcceptances.FirstOrDefault(c => c.ID == temp.GetProperty<T, Guid>("CPAcceptanceID"));
                        md.CPAcceptances.Remove(act);
                        ICPAcceptanceService.Delete(act);
                        XoaNT = true;
                    }
                    //if (TypeID.Equals(900) || TypeID.Equals(901))
                    //{
                    //    if (_select.HasProperty("ID"))
                    //    {
                    //        RSInwardOutward rsInward =
                    //            IRSInwardOutwardService.Query.FirstOrDefault(
                    //                p => p.RSInwardID == _select.GetProperty<T, Guid>("ID"));
                    //        RSInwardOutward rsOutward =
                    //            IRSInwardOutwardService.Query.FirstOrDefault(
                    //                p => p.RSOutwardID == _select.GetProperty<T, Guid>("ID"));
                    //        if (rsInward != null)
                    //            IRSInwardOutwardService.Delete(rsInward);
                    //        if (rsOutward != null)
                    //            IRSInwardOutwardService.Delete(rsOutward);
                    //    }
                    //}
                    #region Xóa chứng từ móc
                    if (temp.HasProperty("TypeID") && temp.HasProperty("ID"))
                    {
                        int typeID = temp.GetProperty<T, int>("TypeID");
                        object tempJoin = Utils.GetSelectJoin<T>(typeID, guid);
                        if (!DeleteSelectJoin(temp, tempJoin, guid, typeID))
                        {
                            MSG.Warning(resSystem.MSG_System_53);
                            services.RolbackTran();
                            return;
                        }
                        #region Nếu là kiểm kê TSCĐ, recommendation == ghi giảm => tạo chứng từ ghi giảm
                        if (typeID == 560)
                        {
                            FAAudit fAAudit = (FAAudit)(object)_select;
                            FADecrement oldFaDecrement = IFADecrementService.findByRefID(fAAudit.ID);
                            if (oldFaDecrement != null)
                            {
                                // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                                if (oldFaDecrement.Recorded)
                                {
                                    MSG.Warning("Bảng kiểm kê tài sản đã có phát sinh chứng từ xử lý ghi giảm. Vui lòng kiểm tra lại trước khi thực hiện việc xóa bảng kiểm kê này!");
                                    services.RolbackTran();
                                    IFADecrementService.RolbackTran();
                                    return;
                                }
                                IFADecrementService.Delete(oldFaDecrement);
                            }

                        }
                        #endregion
                        #region Nếu là kiểm kê CCDC, kiểm tra recommendation == ghi giảm || ghi tăng
                        if (typeID == 435)
                        {
                            TIAudit fAAudit = (TIAudit)(object)_select;
                            TIDecrement oldTIDecrement = ITIDecrementService.findByRefID(fAAudit.ID);
                            TIIncrement oldTIIncrement = ITIIncrementService.findByRefID(fAAudit.ID);
                            if ((oldTIDecrement != null && oldTIDecrement.Recorded) || (oldTIIncrement != null && oldTIIncrement.Recorded))
                            {
                                // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                                MSG.Warning("Bảng kiểm kê CCDC đã có phát sinh chứng từ xử lý ghi giảm hoặc ghi tăng. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                return;
                            }
                            else
                            {
                                if (oldTIDecrement != null)
                                    ITIDecrementService.Delete(oldTIDecrement);
                                if (oldTIIncrement != null)
                                    ITIIncrementService.Delete(oldTIIncrement);
                            }

                        }
                        #endregion
                        #region Nếu là kiểm kê qũy
                        if (typeID == 180)
                        {
                            MCAudit mcAudit = (MCAudit)(object)_select;
                            MCPayment mcPay = IMCPaymentService.findByAuditID(mcAudit.ID);
                            MCReceipt mcRec = IMCReceiptService.findByAuditID(mcAudit.ID);
                            if ((mcPay != null && mcPay.Recorded) || (mcRec != null && mcRec.Recorded))
                            {
                                MSG.Warning("Bảng kiểm kê quỹ đã có phát sinh chứng từ xử lý phiếu thu hoặc phiếu chi. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                                services.RolbackTran();
                                IMCReceiptService.RolbackTran();
                                IMCPaymentService.RolbackTran();
                                return;
                            }
                            else
                            {
                                if (mcPay != null)
                                    IMCPaymentService.Delete(mcPay);
                                if (mcRec != null)
                                    IMCReceiptService.Delete(mcRec);
                            }

                        }
                        #endregion
                    }
                    #endregion

                    if (!GenVoucherOther(temp, temp, ConstFrm.optStatusForm.Delete, remove: true))
                    {
                        MSG.Warning(resSystem.MSG_System_53);
                        services.RolbackTran();
                        return;
                    }
                    services.CommitTran();
                    if (temp.HasProperty("TypeID"))
                        Logs.SaveBusiness(temp, temp.GetProperty<T, int>("TypeID"), Logs.Action.Delete);
                    else Logs.SaveBusiness(temp, 0, Logs.Action.Delete);
                    if (XoaNT) Utils.ClearCacheByType<GOtherVoucher>();
                }
                catch (Exception ex)
                {//lỗi khi thực thi services
                    MSG.Warning(resSystem.MSG_System_53);
                    services.RolbackTran();
                    //MSG.Error("abcdefgh....");
                    return;
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(resSystem.MSG_System_53 + ex.Message);
                //lỗi khi tạo services chung
                //MSG.Error("abcdefgh....");
            }
        }
        public void TDTGia_NgayLapCTu<T>(T input)
        {
            string VTHH_PPTinhGiaXKho = _ISystemOptionService.GetByCode("VTHH_PPTinhGiaXKho").Data;
            string VTHH_TDTGia_NgayLapCTu = _ISystemOptionService.GetByCode("VTHH_TDTGia_NgayLapCTu").Data;
            if (VTHH_TDTGia_NgayLapCTu == "1")
            {
                switch (input.GetType().ToString())
                {
                    #region SAInvoice
                    case "Accounting.Core.Domain.SAInvoice":
                        SAInvoice saInvoice = (SAInvoice)(object)input;
                        List<MaterialGoods> lstmtg = new List<MaterialGoods>();
                        foreach (var x in saInvoice.SAInvoiceDetails)
                        {
                            var md = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == x.MaterialGoodsID);
                            lstmtg.Add(md);
                        }
                        if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                            _IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(lstmtg, saInvoice.PostedDate, saInvoice.PostedDate);
                        else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                            _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg, saInvoice.PostedDate, saInvoice.PostedDate);
                        break;
                    #endregion

                    #region RSInwardOutward
                    case "Accounting.Core.Domain.RSInwardOutward":
                        RSInwardOutward rsInwardOutward = (RSInwardOutward)(object)input;
                        if (rsInwardOutward.TypeID.ToString().StartsWith("41"))
                        {
                            List<MaterialGoods> lstmtg1 = new List<MaterialGoods>();
                            foreach (var x in rsInwardOutward.RSInwardOutwardDetails)
                            {
                                lstmtg1.Add(Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == x.MaterialGoodsID));
                            }
                            if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                                _IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(lstmtg1, rsInwardOutward.PostedDate, rsInwardOutward.PostedDate);
                            else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                                _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg1, rsInwardOutward.PostedDate, rsInwardOutward.PostedDate);
                        }
                        break;
                    #endregion

                    #region RSTransfer
                    case "Accounting.Core.Domain.RSTransfer":
                        RSTransfer rsTransfer = (RSTransfer)(object)input;
                        List<MaterialGoods> lstmtg2 = new List<MaterialGoods>();
                        foreach (var x in rsTransfer.RSTransferDetails)
                        {
                            var md = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == x.MaterialGoodsID);
                            lstmtg2.Add(md);
                        }
                        if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                            _IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(lstmtg2, rsTransfer.PostedDate, rsTransfer.PostedDate);
                        else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                            _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg2, rsTransfer.PostedDate, rsTransfer.PostedDate);
                        break;
                        #endregion
                }
            }
        }
        void RecordedFunctionBase(T temp = default(T), bool isSingleRow = false)
        {

            if (!Authenticate.Permissions("GH", _subSystemCode)) return;
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (uGrid.ActiveRow.IsFilterRow) return;
            if (a || b)
            {
                //WaitingFrm.StartWaiting();
                //SelectedRowsCollection lst = uGrid.Selected.Rows;comment by cuongpv

                //add by cuongpv
                List<UltraGridRow> lst = uGrid.Selected.Rows.Cast<UltraGridRow>().ToList();
                if (lst.First().ListObject.HasProperty("PostedDate"))
                {
                    lst = lst.OrderBy(r => r.ListObject.GetProperty("PostedDate")).ToList<UltraGridRow>();
                }

                //end add by cuongpv

                //lấy index của item cuối cùng được select
                List<int> lstIndex = (from UltraGridRow row in lst orderby row.Index select row.Index).ToList();
                var index = lstIndex.LastOrDefault();
                if (!a) { _notEvent = true; lst.Add(uGrid.ActiveRow); index = uGrid.ActiveRow.Index; _notEvent = false; }
                if (isSingleRow && a)
                {
                    lst.Clear(); lst.Add(uGrid.ActiveRow);
                    if (uGrid.ActiveRow != null) index = uGrid.ActiveRow.Index;
                }
                var strResult = new List<Result>();

                foreach (UltraGridRow row in lst)
                {
                    var result = new Result();
                    temp = (T)row.ListObject;
                    const string key = "Recorded";

                    if (new int[] { 173, 133, 143, 126, 116 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(PPService))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var m = IPPServiceService.Getbykey(iD);

                            if (m != null)
                            {
                                var module = ITypeService.Getbykey(m.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + m.No + " của chức năng " + module.TypeName;

                            }
                        }
                    }
                    else if (new int[] { 172, 129, 132, 142, 119 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(FAIncrement))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var n = IFAIncrementService.Getbykey(iD);

                            if (n != null)
                            {
                                var module = ITypeService.Getbykey(n.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + n.No + " của chức năng " + module.TypeName;

                            }
                        }
                    }
                    else if (new int[] { 906, 903, 904, 905, 902 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(TIIncrement))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var c = ITIIncrementService.Getbykey(iD);

                            if (c != null)
                            {
                                var module = ITypeService.Getbykey(c.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + c.No + " của chức năng " + module.TypeName;

                            }
                        }
                    }
                    else if (new int[] { 171, 127, 131, 141, 117, 402, }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(PPInvoice))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var d = IPPInvoiceService.Getbykey(iD);
                            if (d != null)
                            {
                                var module = ITypeService.Getbykey(d.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + d.No + " của chức năng " + module.TypeName;

                            }
                        }
                    }
                    else if (new int[] { 162, 163, 102, 103, 412, 415, }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(SAInvoice))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var e = ISAInvoiceService.Getbykey(iD);

                            if (e != null)
                            {
                                var module = ITypeService.Getbykey(e.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + e.No + " tại chức năng " + module.TypeName;

                            }
                        }
                    }
                    else if (new int[] { 403 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(SAReturn))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var f = ISAReturnService.Getbykey(iD);

                            if (f != null)
                            {
                                var module = ITypeService.Getbykey(f.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + f.No + " của chức năng " + module.TypeName;
                                return;
                            }
                        }
                    }
                    else if (new int[] { 413 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(PPDiscountReturn))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var g = IPPDiscountReturnService.Getbykey(iD);

                            if (g != null)
                            {
                                var module = ITypeService.Getbykey(g.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + g.No + " của chức năng " + module.TypeName;
                                return;
                            }
                        }

                    }

                    if (temp != null && temp.HasProperty(key) && temp.GetProperty<T, bool>(key)) continue;
                    if (temp != null && temp.HasProperty("Date")) result.Date = temp.GetProperty<T, DateTime?>("Date");
                    if (temp != null && temp.HasProperty("No")) result.No = temp.GetProperty<T, string>("No");
                    if (temp != null && temp.HasProperty("TypeID"))
                    {
                        var typeId = temp.GetProperty<T, int?>("TypeID");
                        var type = Utils.ListType.FirstOrDefault(x => typeId != null && x.ID == typeId.Value);
                        if (type != null)
                            result.TypeName = type.TypeName;
                    }
                    if (temp.HasProperty("TypeID") && temp.HasProperty("ID"))
                    {
                        if (result.Reason.IsNullOrEmpty())
                        {
                            int typeID = temp.GetProperty<T, int>("TypeID");
                            var gId = temp.GetProperty<T, Guid>("ID");
                            object tempJoin = Utils.GetSelectJoin<T>(typeID, gId);
                            string msg = "";
                            result.IsSucceed = temp.Saveleged(tempJoin, typeID, ref msg, isMess: false);
                            result.Reason = msg;
                        }
                        else
                        {
                            result.IsSucceed = false;
                        }
                    }
                    if (string.IsNullOrEmpty(result.Reason))
                        result.Reason = !result.IsSucceed ? resSystem.MSG_System_69 : "";
                    strResult.Add(result);
                }
                if (lst.Count > 1 && strResult.Count(x => !x.IsSucceed) > 0) new MsgResult(strResult, true, true).ShowDialog(this);
                else if (lst.Count == 1 && strResult.Count(x => !x.IsSucceed) > 0) MSG.Warning(strResult[0].Reason);
                TDTGia_NgayLapCTu(temp);
                //WaitingFrm.StartWaiting();
                LoadDuLieu();
                if (uGrid.Rows.Count > index)
                {
                    foreach (var item in lstIndex)
                    {
                        checkUnrecord_nhieudong = true;
                        uGrid.Rows[item].Selected = true;
                    }
                    uGrid.Rows[index].ScrollRowToMiddle();
                    _notEvent = false;
                }
                //UGridAfterSelectChangeBase(uGrid, null);
                //ConfigButtonRecorded(lst.Count > 1 ? default(T) : temp);
            }
            else
                MSG.Error(resSystem.MSG_Error_10);
        }

        void UnRecordedFunctionBase(T temp = default(T), bool isSingleRow = false)
        {
            if (!Authenticate.Permissions("BO", _subSystemCode)) return;
            bool a = uGrid.Selected.Rows.Count > 0;
            bool b = uGrid.ActiveRow != null;
            if (uGrid.ActiveRow.IsFilterRow) return;
            if (a || b)
            {
                //WaitingFrm.StartWaiting();
                SelectedRowsCollection lst = uGrid.Selected.Rows;
                //lấy index của item cuối cùng được select
                List<int> lstIndex = (from UltraGridRow row in lst orderby row.Index select row.Index).ToList();
                var index = lstIndex.LastOrDefault();
                if (!a) { _notEvent = true; lst.Add(uGrid.ActiveRow); index = uGrid.ActiveRow.Index; _notEvent = false; }
                if (isSingleRow && a)
                {
                    lst.Clear(); lst.Add(uGrid.ActiveRow);
                    if (uGrid.ActiveRow != null) index = uGrid.ActiveRow.Index;
                }
                var strResult = new List<Result>();
                foreach (UltraGridRow ultraGridRow in lst)
                {
                    var result = new Result();
                    temp = (T)ultraGridRow.ListObject;
                    const string key = "Recorded";
                    Guid iD_ = _select.GetProperty<T, Guid>("ID");
                    if (iD_ != null)
                    {
                        if (Utils.IGVoucherListDetailService.Query.Any(n => n.VoucherID == iD_))
                        {
                            result.Reason = "Bỏ ghi sổ thất bại! Chứng từ đã được lập chứng từ ghi sổ, vui lòng kiểm tra lại";
                        }
                        else if (Utils.ListExceptVoucher.Any(n => n.GLVoucherID == iD_ || n.GLVoucherExceptID == iD_))
                        {
                            result.Reason = "Bỏ ghi sổ thất bại! Chứng từ đã được đối trừ, vui lòng bỏ đối trừ rồi thử lại!";
                        }
                    }
                    if (new int[] { 173, 133, 143, 126, 116 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(PPService))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var m = IPPServiceService.Getbykey(iD);
                            if (m != null)
                            {
                                var module = ITypeService.Getbykey(m.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + m.No + " của chức năng " + module.TypeName;
                            }
                        }
                    }
                    else if (new int[] { 172, 129, 132, 142, 119 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(FAIncrement))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var n = IFAIncrementService.Getbykey(iD);
                            if (n != null)
                            {
                                var module = ITypeService.Getbykey(n.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + n.No + " của chức năng " + module.TypeName;
                            }
                        }
                    }
                    else if (new int[] { 906, 903, 904, 905, 902 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(TIIncrement))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var c = ITIIncrementService.Getbykey(iD);
                            if (c != null)
                            {
                                var module = ITypeService.Getbykey(c.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + c.No + " của chức năng " + module.TypeName;
                            }
                        }
                    }
                    else if (new int[] { 171, 127, 131, 141, 117, 402, }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(PPInvoice))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var d = IPPInvoiceService.Getbykey(iD);
                            if (d != null)
                            {
                                var module = ITypeService.Getbykey(d.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + d.No + " của chức năng " + module.TypeName;
                            }
                        }
                    }
                    else if (new int[] { 162, 163, 102, 103, 412, 415, }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(SAInvoice))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var e = ISAInvoiceService.Getbykey(iD);
                            if (e != null)
                            {
                                var module = ITypeService.Getbykey(e.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + e.No + " tại chức năng " + module.TypeName;
                            }
                        }
                    }
                    else if (new int[] { 403 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(SAReturn))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var f = ISAReturnService.Getbykey(iD);
                            if (f != null)
                            {
                                var module = ITypeService.Getbykey(f.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + f.No + " của chức năng " + module.TypeName;
                            }
                        }
                    }
                    else if (new int[] { 413 }.Contains(temp.GetProperty<T, int>("TypeID")))
                    {
                        if (typeof(T) != typeof(PPDiscountReturn))
                        {
                            Guid iD = _select.GetProperty<T, Guid>("ID");
                            var g = IPPDiscountReturnService.Getbykey(iD);
                            if (g != null)
                            {
                                var module = ITypeService.Getbykey(g.TypeID);
                                result.Reason = "Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + g.No + " của chức năng " + module.TypeName;
                            }
                        }

                    }

                    if (temp != null && temp.HasProperty(key) && !temp.GetProperty<T, bool>(key)) continue;
                    if (temp != null && temp.HasProperty("PostedDate"))
                    {
                        DateTime? PostedDateCheck = temp.GetProperty<T, DateTime?>("PostedDate");
                        if (PostedDateCheck != null)
                        {
                            DateTime PostedDateCheck1 = PostedDateCheck ?? DateTime.Now;//trungnq thêm để sửa bug 6414
                            if (PostedDateCheck1.Date <= Utils.GetDBDateClosed().StringToDateTime()) continue;
                        }
                    }
                    if (temp != null && temp.HasProperty("Date")) result.Date = temp.GetProperty<T, DateTime?>("Date");
                    if (temp != null && temp.HasProperty("No")) result.No = temp.GetProperty<T, string>("No");
                    if (temp != null && temp.HasProperty("TypeID"))
                    {
                        var typeId = temp.GetProperty<T, int?>("TypeID");
                        var type = Utils.ListType.FirstOrDefault(x => typeId != null && x.ID == typeId.Value);
                        if (type != null)
                            result.TypeName = type.TypeName;
                    }
                    if (temp.HasProperty("TypeID") && temp.HasProperty("ID"))
                    {
                        if (result.Reason.IsNullOrEmpty())
                        {
                            int typeID = temp.GetProperty<T, int>("TypeID");
                            var gId = temp.GetProperty<T, Guid>("ID");
                            object tempJoin = Utils.GetSelectJoin<T>(typeID, gId);
                            result.IsSucceed = temp.Removeleged(tempJoin, typeID);
                        }
                        else
                        {
                            result.IsSucceed = false;
                        }
                    }
                    strResult.Add(result);
                }
                if (lst.Count > 1 && strResult.Count(x => !x.IsSucceed) > 0) new MsgResult(strResult, true, false).ShowDialog(this);
                else if (lst.Count == 1 && strResult.Count(x => !x.IsSucceed) > 0) MSG.Warning(strResult[0].Reason);
                //WaitingFrm.StartWaiting();
                LoadDuLieu();
                if (uGrid.Rows.Count > index)
                {
                    foreach (var item in lstIndex)
                    {
                        checkUnrecord_nhieudong = true;
                        uGrid.Rows[item].Selected = true;
                    }
                    uGrid.Rows[index].ScrollRowToMiddle();
                    _notEvent = false;

                }
                //UGridAfterSelectChangeBase(uGrid, null);
                //ConfigButtonRecorded(lst.Count > 1 ? default(T) : temp);
            }
            else
                MSG.Error(resSystem.MSG_Error_10);
        }

        public void ResetFunctionBase()
        {
            WaitingFrm.StartWaiting();
            //LoadDuLieu(false, true);
            LoadData();
            SetDataForuGrid();
            WaitingFrm.StopWaiting();
        }

        void PrintFunctionBase()
        {
            uGrid.PrintPreview();
        }

        void HelpFunctionBase()
        {
            ////
        }
        #endregion

        #region Event
        /// <summary>
        /// Đăng ký Shortcut Keys cho Form
        /// </summary>
        /// <param name="message"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message message, Keys keys)
        {
            switch (keys)
            {
                case Keys.Control | Keys.N:
                    {//thêm mới
                        if (typeof(T) == typeof(PPDiscountReturn) || typeof(T) == typeof(SAReturn))
                            AddFunctionBase(defaultTypeId);
                        else AddFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.D:
                    {//xóa
                        DeleteFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.E:
                    {//xem
                        EditFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.G:
                    {//ghi sổ
                        RecordedFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.B:
                    {//bỏ ghi sổ
                        UnRecordedFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.P:
                    {//In
                        PrintFunctionBase();
                        return true;
                    }
                case Keys.F1:
                    {//Giúp
                        HelpFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.A:
                    {//Chọn hết các trường
                        tsmSelectAll_Click(tsmSelectAll, null);
                        return true;
                    }
                case Keys.F5:
                    {//Làm mới
                        ResetFunctionBase();
                        return true;
                    }
                case Keys.Control | Keys.T:
                    {//
                        if (typeof(T) == typeof(PPDiscountReturn))
                            btnPPReturn_Click(btnPPReturn, null);
                        else if (typeof(T) == typeof(SAReturn))
                            btnSAReturn_Click(btnSAReturn, null);
                        return true;
                    }
                case Keys.Control | Keys.H:
                    {//
                        if (typeof(T) == typeof(PPDiscountReturn))
                            btnPPDiscount_Click(btnPPDiscount, null);
                        else if (typeof(T) == typeof(SAReturn))
                            btnSADiscount_Click(btnSADiscount, null);
                        return true;
                    }
            }
            return base.ProcessCmdKey(ref message, keys);
        }

        #region Ugrid
        void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //UltraGrid grid = (UltraGrid)sender;
            //if (grid.ActiveCell != null && grid.ActiveCell.Column.Key.Equals("Check")) return;
            EditFunctionBase();
        }
        bool checkUnrecord_nhieudong = false; // Add by Hautv sửa bỏ ghi sổ nhiều dòng
        void UGridAfterSelectChangeBase(object sender, AfterSelectChangeEventArgs e)
        {
            if (checkUnrecord_nhieudong)
            {
                checkUnrecord_nhieudong = false;
                return;
            }
            if (_notEvent) goto End;
            UltraGrid grid = (UltraGrid)sender;
            if (grid.Selected.Rows.Count < 1) goto End;
            object listObject = grid.Selected.Rows[grid.Selected.Rows.Count - 1].ListObject;
            _select = (T)listObject;
            if (listObject == null) goto End;
            _select = (T)listObject;
            if (_select.HasProperty("Recorded"))
            {
                var recorded = _select.GetProperty<T, bool?>("Recorded");
                if (recorded.HasValue)
                    grid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = recorded.Value ? Utils.DefaultRowSelectedColor : Utils.GetColorNotRecorded();
            }

            //TLoadChilGrid.StopThread();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            //Load config hiển thị dữ liệu từ Database
            GetTemplateChild(_select);
            //TLoadChilGrid = new Thread(new ThreadStart(() => GetTemplateChild(_select)));
            //TLoadChilGrid.IsBackground = true;
            //TLoadChilGrid.Start();
            ConfigTtt(_select);
            ConfigButtonsBySelected();
            //TLoadChilGrid.Join();
            ConfigGrid(_select);

            UGridAfterSelectChange(_select);

        End:
            WaitingFrm.StopWaiting();
        }
        /// <summary>
        /// xử lý mỗi khi đối tượng đang được chọn ở grid chính thay đổi
        /// [override]
        /// </summary>
        /// <param name="selectItem"></param>
        public virtual void UGridAfterSelectChange(T selectItem)
        {
            //
        }

        void uGrid_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            e.Cancel = true;
        }
        #endregion

        #region UTree
        private void UTreeMouseDown(object sender, MouseEventArgs e)
        {//khi click chuột phải hiện content menu
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void UTreeDoubleClick(object sender, EventArgs e)
        {//click đúp chuột để sửa
            EditFunctionBase();
        }

        private void UTreeColumnSetGenerated(object sender, ColumnSetGeneratedEventArgs e)
        {
            Utils.uTree_ColumnSetGenerated(sender, e, typeof(T).Name);
        }

        private void UTreeInitializeDataNode(object sender, InitializeDataNodeEventArgs e)
        {
            Utils.uTree_InitializeDataNode(sender, e, typeof(T).Name);
        }
        #endregion  //Utree

        #endregion

        #region Utils
        /// <summary>
        /// có thể kế thừa viết lại hoặc không
        /// [override]
        /// </summary>
        /// <param name="sender"></param>
        void UGridInitializeLayout(object sender)
        {//Thiết lập cho Grid
            UltraGrid ultraGrid = (UltraGrid)sender;
            //hiển thị 1 band
            ultraGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = ultraGrid.Name.Equals("uGrid") ? Infragistics.Win.DefaultableBoolean.True : Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.FilterUIType = ultraGrid.Name.Equals("uGrid") ? FilterUIType.FilterRow : FilterUIType.Default;
            //tự thay đổi kích thước cột
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            ////select cả hàng hay ko?
            //ultraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            //ultraGrid.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            ////Hiện những dòng trống?
            //ultraGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            //ultraGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            ////Fix Header
            //ultraGrid.DisplayLayout.UseFixedHeaders = true;

            ultraGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.AlignWithDataRows;
            //chọn một dòng
            ultraGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
        }

        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, _loaiForm == -840 ? "PSOtherVoucher" : (_loaiForm == -510 ? "FAIncrementDiff" : typeof(T).Name));
            utralGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            utralGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
            utralGrid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            //foreach (var column in utralGrid.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>().Where(column => column.Key.Contains("Amount") || column.Key.Contains("OrgPrice")))
            //{
            //    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
            //    column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            //    column.ConfigColumnByNumberic();
            //}
            foreach (var column in utralGrid.DisplayLayout.Bands[0].Columns)
            {
                if (column.IsVisibleInLayout)
                {
                    this.ConfigEachColumn4Grid(0, column, utralGrid);
                    column.CellActivation = Activation.Disabled;
                    if (@column.Key.Equals("PSTimeSheetName") || column.Key.Equals("PSSalarySheetName") || column.Key.Equals("PSTimeSheetSummaryName"))
                    {
                        column.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Left;

                    }
                }
            }
            //utralGrid.DisplayLayout.Bands[0].Columns.Add("Check", "");
            //foreach (var column in utralGrid.DisplayLayout.Bands[0].Columns)
            //{
            //    if (column.Key.Equals("Check"))
            //    {
            //        column.Header.VisiblePosition = 0;
            //        column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //        column.DataType = typeof(bool);
            //        column.DefaultCellValue = false;
            //        column.Hidden = false;
            //        column.Width = 10;
            //        column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            //        column.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            //        column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            //        column.CellActivation = Activation.AllowEdit;
            //    }
            //    else
            //        column.Header.VisiblePosition += 1;
            //}
            //utralGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            utralGrid.ConfigSummaryOfGrid();
        }

        /// <summary>
        /// Load dữ liệu (lấy dữ liệu từ csdl, xử lý hiển thị, hiển thị và xử lý hiển thị)
        /// [override]
        /// </summary>
        /// <param name="config"></param>
        void LoadDuLieu(bool config = true, bool isClose = false, bool isFAdd = false)
        {
            //Thread loadDataThread = new Thread(new ThreadStart(() => LoadDbToGrid(isClose)));
            //loadDataThread.IsBackground = true;
            //loadDataThread.Start();
            LoadDbToGrid(isClose, isFAdd);
        }

        private void LoadDbToGrid(bool isClose = false, bool isFAdd = false)
        {
            #region Lấy dữ liệu từ CSDL
            ////add by cuongpv
            //DateTime dtFrom = dtBeginDate.DateTime.Date;
            //DateTime dtTo = dtEndDate.DateTime.Date;
            //dtFrom = dtFrom.AddHours(0).AddMinutes(0).AddSeconds(0);
            //dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);


            //if (dtBeginDate.Value == null || dtEndDate.Value == null)
            //{
            //    MSG.Warning("Ngày không được để trống");
            //    return;
            //}

            //if (dtFrom > dtTo)
            //{
            //    MSG.Warning("Từ ngày không được lớn hơn đến ngày");
            //    return;
            //}

            ////end add by cuongpv
            ////Dữ liệu lấy theo năm hoạch toán
            //DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            //BaseService<T, Guid> services = this.GetIService((T)Activator.CreateInstance(typeof(T)));

            ////add by cuongpv 
            //if (typeof(T) == typeof(PSTimeSheet) || typeof(T) == typeof(PSSalarySheet) || typeof(T) == typeof(PSTimeSheetSummary) || typeof(T) == typeof(TT153Report) || typeof(T) == typeof(TT153RegisterInvoice) || typeof(T) == typeof(TT153PublishInvoice)
            //    || typeof(T) == typeof(TT153DestructionInvoice) || typeof(T) == typeof(TT153LostInvoice) || typeof(T) == typeof(TT153AdjustAnnouncement) || typeof(T) == typeof(TT153DeletedInvoice))
            //{
            //    DsObject = services.Query.ToList();//phan luong ko can loc theo ky ke toan
            //    if (isClose)
            //    {
            //        services.UnbindSession(DsObject);
            //        DsObject = services.Query.ToList();
            //    }
            //}
            //else if (typeof(T) == typeof(SABill))
            //{
            //    DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("InvoiceDate") >= dtFrom && k.GetProperty<T, DateTime>("InvoiceDate") <= dtTo).ToList();
            //    if (isClose)
            //    {
            //        services.UnbindSession(DsObject);
            //        DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("InvoiceDate") >= dtFrom && k.GetProperty<T, DateTime>("InvoiceDate") <= dtTo).ToList();
            //    }
            //}
            //else
            //{
            //    string key1 = Activator.CreateInstance(typeof(T)).HasProperty("PostedDate") ? "PostedDate" : "Date";
            //    if (key1 == "PostedDate")
            //    {
            //        DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("PostedDate") >= dtFrom && k.GetProperty<T, DateTime>("PostedDate") <= dtTo).ToList();
            //        if (isClose)
            //        {
            //            services.UnbindSession(DsObject);
            //            DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("PostedDate") >= dtFrom && k.GetProperty<T, DateTime>("PostedDate") <= dtTo).ToList();
            //        }
            //    }
            //    else
            //    {
            //        DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("Date") >= dtFrom && k.GetProperty<T, DateTime>("Date") <= dtTo).ToList();
            //        if (isClose)
            //        {
            //            services.UnbindSession(DsObject);
            //            DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("Date") >= dtFrom && k.GetProperty<T, DateTime>("Date") <= dtTo).ToList();
            //        }
            //    }

            //    if (!Activator.CreateInstance(typeof(T)).HasProperty(key1))
            //    {
            //        key1 = "IWDate";
            //        DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("IWDate") >= dtFrom && k.GetProperty<T, DateTime>("IWDate") <= dtTo).ToList();
            //        if (isClose)
            //        {
            //            services.UnbindSession(DsObject);
            //            DsObject = services.Query.ToList().Where(k => k.GetProperty<T, DateTime>("IWDate") >= dtFrom && k.GetProperty<T, DateTime>("IWDate") <= dtTo).ToList();
            //        }
            //    }
            //}
            //end add by cuongpv


            LoadData();
            #region apply loading thì xóa đoạn này đi, bỏ comment dưới
            //comment by cuongpv
            //DsObject = services.Query.ToList();
            //if (isClose)
            //{
            //    services.UnbindSession(DsObject);
            //    DsObject = services.Query.ToList();
            //}
            //end comment by cuongpv
            //switch (_loaiForm)
            //{
            //    case -9999: //xuất kho
            //        {
            //            List<int> lstTypeId = new List<int> { 410, 411, 412, 413, 414, 415 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -9998: //nhập kho
            //        {
            //            List<int> lstTypeId = new List<int> { 400, 401, 402, 403, 404 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -500://Mua TSCĐ
            //        {
            //            List<int> lstTypeId = new List<int> { 500, 119, 129, 132, 142, 172 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -430://Mua TSCĐ
            //        {
            //            List<int> lstTypeId = new List<int> { 430, 902, 903, 904, 905, 906 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -907: //Ghi tăng TSCĐ
            //        {
            //            List<int> lstTypeId = new List<int> { 907 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();

            //        }
            //        break;
            //    case -510: //Ghi tăng TSCĐ
            //        {
            //            List<int> lstTypeId = new List<int> { 510 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();

            //        }
            //        break;
            //    case -600: //Chứng từ nghiệp vụ khác
            //        {
            //            List<int> lstTypeId = new List<int> { 600, 601, 602, 660, 670 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -690: //Chứng từ nghiệp vụ khác
            //        {
            //            List<int> lstTypeId = new List<int> { 690 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -620: //Kết chuyển lãi, lỗ
            //        {
            //            List<int> lstTypeId = new List<int> { 620 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    case -840: //Chứng từ nghiệp vụ khác
            //        {
            //            List<int> lstTypeId = new List<int> { 840 };
            //            DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //        }
            //        break;
            //    //case -220: //Hàng mua trả lại
            //    //    {
            //    //        List<int> lstTypeId = new List<int> { 220, 230 };
            //    //        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //    //    }
            //    //    break;
            //    //case -230: //Hàng mua giảm giá
            //    //    {
            //    //        List<int> lstTypeId = new List<int> { 230 };
            //    //        DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //    //    }
            //    //    break;
            //    case 0:
            //        {
            //            if (typeof(T) == typeof(PPInvoice))
            //                //Mua hàng có qua kho
            //                //List<int> lstTypeId = new List<int> { 210, 260, 261, 262, 263, 264 };
            //                DsObject = DsObject.Where(k => k.GetProperty<T, bool>("StoredInRepository")).ToList();
            //            else if (typeof(T) == typeof(SAInvoice))
            //            {//Bán hàng chưa thu tiền
            //                List<int> lstTypeId = new List<int> { 320 };
            //                DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //            }
            //        }
            //        break;
            //    case 1: //Mua hàng ko qua kho
            //        {
            //            if (typeof(T) == typeof(PPInvoice))
            //                //List<int> lstTypeId = new List<int> { 210, 260, 261, 262, 263, 264 };
            //                DsObject = DsObject.Where(k => !k.GetProperty<T, bool>("StoredInRepository")).ToList();
            //            else if (typeof(T) == typeof(SAInvoice))
            //            {//Bán hàng thu tiền ngay
            //                List<int> lstTypeId = new List<int> { 321, 322 };
            //                DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //            }
            //        }
            //        break;
            //    case 2:
            //        {
            //            if (typeof(T) == typeof(SAInvoice))
            //            {//Bán hàng đại lý bán đúng giá nhận ủy thác xuất nhập khẩu
            //                List<int> lstTypeId = new List<int> { 323, 324, 325 };
            //                DsObject = DsObject.Where(k => lstTypeId.Contains(k.GetProperty<T, int>("TypeID"))).ToList();
            //            }
            //        }
            //        break;
            //    case -1:
            //        {
            //            if (typeof(T) == typeof(TIInit))
            //            {
            //                lstTypeId = new List<int> { 436 };
            //                TIInit tiinit = (TIInit)Activator.CreateInstance(typeof(TIInit));
            //                List<string> lstTypeIdStr = new List<string> { "ID", "Quantity", "UnitPrice", "Amount", "PostedDate" };
            //                List<string> lstTypeIdStrVni = new List<string> { "Thành phẩm", "Số lượng", "Đơn giá", "Giá trị tồn CCDC", "Ngày ghi sổ" };
            //                ConfigTtt2(tiinit, 436, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            //                goto End;
            //            }
            //        }
            //        break;
            //}

            #region hiển thị và xử lý hiển thị

            if (IsGridShow)
            {
                SetDataForuGrid();
                //if (config) ConfigGrid(uGrid);
            }

            #endregion

            //chọn tự select dòng đã select trước
            if (!isFAdd && _select.HasProperty("ID"))
            {
                var gId = _select.GetProperty<T, Guid?>("ID");
                if (gId.HasValue)
                {
                    var rowsSelect = uGrid.Rows.Where(row => gId.Value == (Guid?)row.Cells["ID"].Value);
                    uGrid.ActiveRow = rowsSelect.Any() ? rowsSelect.FirstOrDefault() : uGrid.Rows.FirstOrDefault();
                    if (uGrid.ActiveRow != null)
                    {
                        uGrid.Selected.Rows.Clear();
                        uGrid.ActiveRow.Selected = true;
                        uGrid.ActiveRow.ScrollRowToMiddle();
                        _notEvent = false;
                        //UGridAfterSelectChangeBase(uGrid, null);
                    }
                }
            }
            //_select = (T)uGrid.Rows[0].ListObject;
            //Xóa hết phần chi tiết nếu DS = 0
            if (DsObject.Count == 0)
            {
                _select = (T)Activator.CreateInstance(typeof(T));
                ConfigTtt(_select);
                ConfigButtonsBySelected();
                ConfigGrid(_select);
            }
        #endregion

        #region loading
        //WaitingFrm waitingFrm = new WaitingFrm();
        //System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
        //bw.DoWork += (s, args) =>
        //                 {
        //                     Invoke(new MethodInvoker(waitingFrm.Show));
        //                     DsObject = services.Query.ToList();
        //                     if (isClose)
        //                     {
        //                         services.UnbindSession(DsObject);
        //                         DsObject = services.Query.ToList();
        //                     }
        //                 };
        //bw.RunWorkerCompleted += (s, args) =>
        //                             {
        //                                 T temp = (T)Activator.CreateInstance(typeof(T));
        //                                 if (temp.HasProperty("PostedDate"))
        //                                     DsObject =
        //                                         DsObject.OrderByDescending(
        //                                             k => k.GetType().GetProperty("PostedDate").GetValue(k, null)).
        //                                             ToList();

        //                                 #region Xử lý dữ liệu

        //                                 #endregion

        //                                 #region hiển thị và xử lý hiển thị

        //                                 if (IsGridShow)
        //                                 {
        //                                     uGrid.DataSource = DsObject;
        //                                     FillColorRecordedGrid();
        //                                     //if (config) ConfigGrid(uGrid);
        //                                 }

        //                                 #endregion

        //                                 //chọn tự select dòng đầu tiên
        //                                 if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
        //                                 //_select = (T)uGrid.Rows[0].ListObject;

        //                                 waitingFrm.Close();
        //                             };
        //bw.RunWorkerAsync();
        #endregion
        #endregion
        End:
            if (true)
            {

            }
        }

        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            return dsTemplate[keyofdsTemplate];
        }
        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }

        #region Config vùng thông tin chung
        void ConfigTtt<T2>(T2 input)
        {
            int typeId = input.GetProperty<T2, int>("TypeID");
            List<int> lstTypeId = new List<int>();
            List<string> lstTypeIdStr = new List<string>();
            List<string> lstTypeIdStrVni = new List<string>();

            #region Tiền và các khoản tương đương tiền
            //phiếu thu
            if (input is MCReceipt)
            {
                //lstTypeId = new List<int> { 100 };

                //if(lstTypeId.Contains(typeId))
                //{
                //    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Payers", "Reason", "NumberAttach", "TotalAmount", "TypeID" };
                //    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Người nộp", "Lý do nộp", "Kèm theo", "Số tiền", "Loại phiếu thu" };
                //    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                //}

                lstTypeId = new List<int> { 100, 101, 102, 103 };

                //if (lstTypeId.Contains(typeId))   //command bởi TrungNQ để khi để trốg ugrid trên thì bên dưới vẫn hiển thị thuộc tính
                //{
                //lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Payers", "Reason", "NumberAttach", "TotalAmount", "TypeID", "InvoiceType", "InvoiceDate", "InvoiceSeries", "InvoiceNo" };
                //lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Người nộp", "Lý do nộp", "Kèm theo", "Số tiền", "Loại phiếu thu", "Loại hóa đơn", "Ngày hóa đơn", "Ký hiệu hóa đơn", "Số hóa đơn" };

                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Payers", "Reason", "NumberAttach", "TotalAll", "TypeID" };

                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Người nộp", "Lý do nộp", "Kèm theo", "Số tiền", "Loại phiếu thu" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                //}

            }
            //phiếu chi
            if (input is MCPayment)
            {
                lstTypeId = new List<int> { 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 902 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Receiver", "Reason", "NumberAttach", "TotalAll", "TypeID" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Người nhận", "Lý do chi", "Kèm theo", "Tổng tiền thanh toán", "Loại phiếu chi" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //nộp tiền tài khoản
            if (input is MBDeposit)
            {
                lstTypeId = new List<int> { 160, 161 };

                if (lstTypeId.Contains(typeId))
                {

                    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalAll", "TypeID" };
                    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng nộp", "Địa chỉ", "Diễn giải", "Số tiền", "Loại chứng từ" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }

                lstTypeId = new List<int> { 162, 163 };
                if (lstTypeId.Contains(typeId))
                {
                    //lstTypeIdStr.Add("InvoiceType"); lstTypeIdStr.Add("InvoiceDate"); lstTypeIdStrVni.Add("InvoiceSeries"); lstTypeIdStrVni.Add("InvoiceNo");
                    //lstTypeIdStrVni.Add("Loại hóa đơn"); lstTypeIdStrVni.Add("Ngày hóa đơn"); lstTypeIdStrVni.Add("Ký hiệu hóa đơn"); lstTypeIdStrVni.Add("Số hóa đơn");
                    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalAmount", "TypeID", "InvoiceType", "InvoiceDate", "InvoiceSeries", "InvoiceNo" };
                    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng nộp", "Địa chỉ", "Diễn giải", "Số tiền", "Loại chứng từ", "Loại hóa đơn", "Ngày hóa đơn", "Ký hiệu hóa đơn", "Số hóa đơn" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }
            }
            //séc, ủy nhiệm chi (MBTellerPaper)
            if (input is MBTellerPaper)
            {
                lstTypeId = new List<int> { 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 140, 141, 142, 143, 144, 903, 904, 905 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "BankAccountDetailID", "Reason", "TotalAll", "TypeID", "AccountingObjectName", "AccountingObjectAddress", "AccountingObjectBankAccount" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Tài khoản đơn vị trả tiền", "Nội dung thanh toán", "Số tiền", "Loại chứng từ", "Đối tượng nhận", "Địa chỉ đối tượng nhận", "Tài khoản đối tượng nhận tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni, 58);
            }
            //thẻ tín dụng
            if (input is MBCreditCard)
            {
                lstTypeId = new List<int> { 170, 171, 172, 173, 174, 906 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalAll", "TypeID" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng nhận", "Địa chỉ", "Diễn giải", "Số tiền", "Loại chứng từ" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //chuyển tiền nội bộ
            if (input is MBInternalTransfer)
            {
                lstTypeId = new List<int> { 150 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "Reason", "TotalAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Diễn giải", "Số tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //kiểm kê quỹ
            if (input is MCAudit)
            {
                lstTypeId = new List<int> { 180 };
                lstTypeIdStr = new List<string> { "AuditDate", "Date", "No", "Description", "Summary", "DifferAmount", "CurrencyID" };
                lstTypeIdStrVni = new List<string> { "Kiểm kê đến ngày", "Ngày chứng từ", "Số chứng từ", "Diễn giải", "Kết quả kiểm kê", "Số tiền chênh lệch", "Loại tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            #endregion

            #region Mua Hàng
            //đơn mua hàng
            if (input is PPOrder)
            {
                lstTypeId = new List<int> { 200 };
                lstTypeIdStr = new List<string> { "No", "Date", "DeliverDate", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmount" };
                lstTypeIdStrVni = new List<string> { "Số đơn hàng", "Ngày đơn hàng", "Ngày giao hàng", "Đối tượng", "Địa chỉ", "Diễn giải", "Tổng tiền thanh toán" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //Mua hàng
            if (input is PPInvoice)
            {
                lstTypeId = new List<int> { 210, 260, 261, 262, 263, 264 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalAll", "TypeID" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải", "Tổng tiền hàng", "Loại chứng từ" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //nhận hóa đơn dịch vụ
            if (input is PPService)
            {
                lstTypeId = new List<int> { 240, 116, 126, 133, 143, 173 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalAll" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải", "Tổng tiền thanh toán" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //trả tiền nhà cung cấp
            //hàng trả lại giảm giá
            if (input is PPDiscountReturn)
            {
                lstTypeId = new List<int> { 220, 230 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmountOriginal" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải", "Tổng tiền thanh toán" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            #endregion

            #region Bán Hàng
            //báo giá
            if (input is SAQuote)
            {
                lstTypeId = new List<int> { 300 };
                lstTypeIdStr = new List<string> { "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmountStand" };
                lstTypeIdStrVni = new List<string> { "Số báo giá", "Ngày báo giá", "Đối tượng", "Địa chỉ", "Diễn giải", "Số tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //đơn đặt hàng
            if (input is SAOrder)
            {
                lstTypeId = new List<int> { 310 };
                lstTypeIdStr = new List<string> { "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmount" };
                lstTypeIdStrVni = new List<string> { "Số đơn hàng", "Ngày đơn hàng", "Đối tượng", "Địa chỉ", "Diễn giải", "Số tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            //bán hàng chưa thu tiền & bán hàng thu tiền ngay & bán hàng từ đại lý bán đúng giá, ủy thác XNK
            if (input is SAInvoice)
            {
                lstTypeId = new List<int> { 320, 321, 322, 323, 324, 325 };
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmount", "TypeID" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải", "Số tiền", "Loại chứng từ" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            if (input is SABill)
            {
                lstTypeId = new List<int> { 326 };
                lstTypeIdStr = new List<string> { "InvoiceDate", "InvoiceNo", "InvoiceSeries", "InvoiceTemplate", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày hóa đơn", "Số hóa đơn", "Ký hiệu hóa đơn", "Mẫu số hóa đơn", "Đối tượng", "Địa chỉ", "Diễn giải", "Số tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            if (input is SAReturn)
            {
                //Hàng bán trả lại
                lstTypeId = new List<int> { 330 };

                if (lstTypeId.Contains(typeId))
                {
                    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalPaymentAmount", "TypeID" };
                    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải", "Số tiền", "Loại chứng từ" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }

                //Hàng bán giảm giá
                lstTypeId = new List<int> { 340 };
                if (lstTypeId.Contains(typeId))
                {
                    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason", "TotalAmount", "TypeID" };
                    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải", "Số tiền", "Loại chứng từ" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }
            }
            #endregion

            #region TSCĐ
            if (input is FAIncrement)
            {
                lstTypeId = new List<int> { 500, 510, 119, 129, 132, 142, 172 };
                lstTypeIdStr = new List<string> { "PostedDate", "Date", "No", "Reason", "TotalOrgPrice" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Ngày chứng từ", "Số chứng từ", "Diễn giải", "Nguyên giá" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            if (input is FADecrement)
            {
                lstTypeId = new List<int> { 520 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "Reason", "TotalAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Lý do ghi giảm", "Số tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is FATransfer)
            {
                lstTypeId = new List<int> { 550 };
                lstTypeIdStr = new List<string> { "Date", "No", "Transferor", "Receiver" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Số chứng từ", "Người điều chuyển", "Người tiếp nhận" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            if (input is FAAdjustment)
            {
                lstTypeId = new List<int> { 530 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "Reason", "FixedAssetName" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Lý do ghi giảm", "Tài sản" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is FADepreciation)
            {
                lstTypeId = new List<int> { 540 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "Reason", "TypeVoucher", "TotalAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Diễn giải", "Loại tài sản", "Số tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is FAAudit)
            {
                lstTypeId = new List<int> { 560 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "InventoryDate", "Summary", "Description" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Ngày kiểm kê", "Kết quả kiểm kê", "Diễn giải" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            #endregion

            #region CCDC
            if (input is TIIncrement)
            {
                lstTypeId = new List<int> { 430, 902, 903, 904, 905, 906, 907 };
                lstTypeIdStr = new List<string> { "Date", "No", "Reason", "TotalAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Số chứng từ", "Diễn giải", "Tổng tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is TIDecrement)
            {
                lstTypeId = new List<int> { 431 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "Reason", "TotalRemainingAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Diễn giải", "Tổng tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is TITransfer)
            {
                lstTypeId = new List<int> { 433 };
                lstTypeIdStr = new List<string> { "Date", "No", "Reason", "Transferor", "Receiver", "TotalTransferQuantity" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Số chứng từ", "Diễn giải", "Người điều chuyển", "Người tiếp nhận", "Tổng số lượng" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            if (input is TIAdjustment)
            {
                lstTypeId = new List<int> { 432 };
                lstTypeIdStr = new List<string> { "Date", "No", "Reason", "TotalAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Số chứng từ", "Diễn giải", "Tổng tiền" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is TIAllocation)
            {
                lstTypeId = new List<int> { 434 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "Reason", "TotalAmount" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Diễn giải", "Tổng tiền phân bổ" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            if (input is TIAudit)
            {
                lstTypeId = new List<int> { 435 };
                lstTypeIdStr = new List<string> { "Date", "PostedDate", "No", "InventoryDate", "Summary", "Description" };
                lstTypeIdStrVni = new List<string> { "Ngày chứng từ", "Ngày hạch toán", "Số chứng từ", "Ngày kiểm kê", "Kết quả kiểm kê", "Diễn giải" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            if (input is TIInit)
            {
                lstTypeId = new List<int> { 436 };
                lstTypeIdStr = new List<string> { "ID", "Quantity", "UnitPrice", "Amount", "PostedDate" };
                lstTypeIdStrVni = new List<string> { "Thành phẩm", "Số lượng", "Đơn giá", "Giá trị tồn CCDC", "Ngày ghi sổ" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            #endregion

            #region Kho
            if (input is RSInwardOutward)
            {
                //Nhập kho
                lstTypeId = new List<int> { 400, 401, 402, 403, 404 };
                if (lstTypeId.Contains(typeId))
                {
                    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason" };
                    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Đối tượng", "Địa chỉ", "Diễn giải" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }

                //Xuất kho
                lstTypeId = new List<int> { 410, 411, 412, 413, 414, 415 };
                if (lstTypeId.Contains(typeId))
                {
                    lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "Reason" };
                    lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số phiếu xuất", "Ngày phiếu xuất", "Đối tượng", "Địa chỉ", "Lý do xuất" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }
            }
            if (input is RSAssemblyDismantlement)
            {
                //Lắp ráp
                lstTypeId = new List<int> { 900 };
                if (lstTypeId.Contains(typeId))
                {
                    lstTypeIdStr = new List<string> { "MaterialGoodsName", "Quantity", "UnitPrice", "Unit", "Reason" };
                    lstTypeIdStrVni = new List<string> { "Thành phẩm", "Số lượng", "Đơn giá", "Đơn vị tính", "Diễn giải" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }

                //Tháo rỡ
                lstTypeId = new List<int> { 901 };
                if (lstTypeId.Contains(typeId))
                {
                    lstTypeIdStr = new List<string> { "MaterialGoodsName", "Quantity", "UnitPrice", "Unit", "Reason" };
                    lstTypeIdStrVni = new List<string> { "Vật tư", "Số lượng", "Đơn giá", "Đơn vị tính", "Diễn giải" };
                    ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
                }
            }

            if (input is RSTransfer)
            {
                //Nhập kho
                lstTypeId = new List<int> { 420, 421, 422 };//edit by cuongpv them: 421, 422
                lstTypeIdStr = new List<string> { "PostedDate", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "IWResponsitoryKeeper" };
                lstTypeIdStrVni = new List<string> { "Ngày hạch toán", "Số chứng từ", "Ngày chứng từ", "Người thực hiện", "Địa chỉ", "Thủ kho nhập" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }
            #endregion

            #region hợp đồng 
            if (input is EMContractDetailMG)
            {
                lstTypeId = new List<int> { 861 };
                lstTypeIdStr = new List<string> { "MaterialGoodsCode", "MaterialGoodsName", "Unit", "Quantity", "QuantityReceipt", "UnitPrice", "TotalAmount", "DiscountRate", "DiscountAmount", "VATRate", "VATAmount", "SAOrderID" };
                lstTypeIdStrVni = new List<string> { "Mã hàng", "Tên hàng", "Đơn vị tính", "Số lượng yêu cầu", "Số lượng đã giao", "Đơn giá", "Thành tiền", "Tỷ lệ CK", "Tiền triết khấu", "% thuế GTGT", "Tiền thuế GTGT", "Số đơn đặt hàng" };
                ConfigTtt2(input, typeId, lstTypeId, lstTypeIdStr, lstTypeIdStrVni);
            }

            #endregion

        }

        void ConfigTtt2<T2>(T2 input, int typeId, List<int> lstTypeId, List<string> lstTypeIdStr, List<string> lstTypeIdStrVni, int buocnhayXAdd = 0, int? column = null)
        {
            bool isNull = Utils.Compare(input, (T2)Activator.CreateInstance(typeof(T2)));
            if (!isNull && !lstTypeId.Contains(typeId)) return;
            int count = lstTypeIdStr.Count;
            Dictionary<int, List<Point>> tDictionary = GenLstPoint(ref count, 120 + buocnhayXAdd);
            int j = 0;
            palTtc.Controls.Clear();
            for (int i = 0; i < count; i++)
            {
                int index = i + 1;
                if (index == 11 || !tDictionary.Any(k => k.Key == index)) continue;
                string proName = lstTypeIdStr[j];
                string proNameVni = lstTypeIdStrVni[j];
                //tạo control 1 (label)
                Control[] control = palTtc.Controls.Find(string.Format("{0}_1", proName), false);
                if (control.Length == 0)
                {
                    UltraLabel lbl1 = new UltraLabel
                    {
                        Appearance = new Infragistics.Win.Appearance { BackColor = Color.Transparent },
                        //BorderStyleOuter = UIElementBorderStyle.Dotted,
                        Size = new Size(120 + buocnhayXAdd, 17),  //95, 17
                        Name = string.Format("{0}_1", proName),
                        Text = proNameVni,
                        Location = tDictionary[index][0],
                        //AutoSize = true
                    };
                    //palTtc.Controls.Add(lbl1);
                    AddLabel(lbl1);
                }
                else
                    control[0].Text = proNameVni;
                //tạo control 2 (value)
                string value = string.Empty;
                if (isNull) goto next;
                if ("PostedDate,Date,InvoiceDate,DeliverDate,AuditDate,InventoryDate".Split(',').ToList().Contains(proName) && input.HasProperty(proName))
                {
                    DateTime dateTime = input.GetProperty<T2, DateTime>(proName);
                    value = dateTime == Utils.GetDateTimeEmpty() ? "" : dateTime.ToString("dd/MM/yyyy");
                }
                else if (proName.Equals("TypeID"))
                {
                    value = string.Empty;
                    foreach (Type type in Utils.ListType.Where(type => type.ID == input.GetProperty<T2, int>(proName)))
                    {
                        value = type.TypeName;
                        break;
                    }
                }
                else if (proName.Equals("BankAccountDetailID"))
                {
                    value = string.Empty;
                    foreach (BankAccountDetail type in Utils.ListBankAccountDetail.Where(type => type.ID == input.GetProperty<T2, Guid>(proName)))
                    {
                        value = type.BankAccount;
                        break;
                    }
                }
                else if (proName.Equals("MaterialGoodsID"))
                {
                    value = string.Empty;
                    foreach (MaterialGoods type in Utils.ListMaterialGoods.Where(type => type.ID == input.GetProperty<T2, Guid>(proName)))
                    {
                        value = type.MaterialGoodsName;
                        break;
                    }
                }
                else if (proName.Equals("DepartmentID"))
                {
                    value = string.Empty;
                    foreach (Department type in Utils.ListDepartment.Where(type => type.ID == input.GetProperty<T2, Guid>(proName)))
                    {
                        value = type.DepartmentName;
                        break;
                    }
                }
                else if (proName.Equals("AccountingObjectBankAccount"))
                {
                    value = string.Empty;
                    foreach (AccountingObjectBankAccount type in Utils.ListAccountingObjectBank.Where(type => type.ID == input.GetProperty<T2, Guid>(proName)))
                    {
                        value = type.BankAccount;
                        break;
                    }
                }
                else if (proName.Equals("InvoiceType"))
                {
                    int invoicetype = input.GetProperty<T2, int>(proName);
                    if (invoicetype == 0)
                        value = "Không có hóa đơn";
                    else if (invoicetype == 1) value = "Hóa đơn thông thường";
                    else if (invoicetype == 2) value = "Hóa đơn GTGT";
                }
                else
                {
                    try
                    {
                        var s = input.GetProperty<T2, object>(proName);
                        value = s != null ? s.ToString() : "";
                        //value = input.GetProperty<T2, object>(proName).ToString();
                    }
                    catch { value = input.GetProperty<T2, string>(proName); }
                }
            next:
                control = palTtc.Controls.Find(string.Format("{0}_2", proName), false);
                if (!control.Any())
                {
                    UltraLabel lbl2 = new UltraLabel
                    {
                        Appearance = new Infragistics.Win.Appearance { BackColor = Color.Transparent },
                        //BorderStyleOuter = UIElementBorderStyle.Dotted,
                        Size = new Size(95 + buocnhayXAdd, 17),
                        Name = string.Format("{0}_2", proName),
                        Text = string.Format(": {0}", value),
                        Location = tDictionary[index][1],
                        AutoSize = true
                    };
                    if (proName.Contains("Amount") || proName.Contains("OrgPrice") || proName.Contains("TotalAllOriginal") || proName.Contains("TotalAll"))
                    {
                        if (!isNull)
                            lbl2.FormatNumberic(value, ConstDatabase.Format_TienVND);
                        else
                            lbl2.Text = value;
                        lbl2.Text = string.Format(": {0}", lbl2.Text.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault());
                    }
                    else if (proName.Contains("Quantity"))
                    {
                        if (!isNull)
                            lbl2.FormatNumberic(value, ConstDatabase.Format_Quantity);
                        else
                            lbl2.Text = value;
                        lbl2.Text = string.Format(": {0}", lbl2.Text.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault());
                    }
                    else if (proName.Contains("UnitPrice"))
                    {
                        if (!isNull)
                            lbl2.FormatNumberic(value, ConstDatabase.Format_DonGiaQuyDoi);
                        else
                            lbl2.Text = value;
                        lbl2.Text = string.Format(": {0}", lbl2.Text.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault());
                    }
                    //palTtc.Controls.Add(lbl2);
                    AddLabel(lbl2);
                }
                else
                {
                    if (proName.Contains("Amount") || proName.Contains("OrgPrice"))
                    {
                        if (!isNull)
                            control[0].FormatNumberic(value, ConstDatabase.Format_TienVND);
                        else
                            control[0].Text = value;
                        control[0].SetPropertyValue(a => a.Text, string.Format(": {0}", control[0].Text.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()));
                    }
                    else if (proName.Contains("Quantity"))
                    {
                        if (!isNull)
                            control[0].FormatNumberic(value, ConstDatabase.Format_Quantity);
                        else
                            control[0].Text = value;
                        control[0].SetPropertyValue(a => a.Text, string.Format(": {0}", control[0].Text.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()));
                    }
                    else if (proName.Contains("UnitPrice"))
                    {
                        if (!isNull)
                            control[0].FormatNumberic(value, ConstDatabase.Format_DonGiaQuyDoi);
                        else
                            control[0].Text = value;
                        control[0].SetPropertyValue(a => a.Text, string.Format(": {0}", control[0].Text.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()));
                    }
                    else
                        control[0].SetPropertyValue(a => a.Text, string.Format(": {0}", value.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()));
                }
                j++;
            }
        }
        delegate void AddLabelDelegate(UltraLabel label);
        void AddLabel(UltraLabel label)
        {
            if (InvokeRequired)
            {
                Invoke(new AddLabelDelegate(AddLabel), label);
                return;
            }

            palTtc.Controls.Add(label);
        }

        Dictionary<int, List<Point>> GenLstPoint(ref int sophantu, int buocnhayXInput = 120)
        {
            Dictionary<int, List<Point>> tDictionary = new Dictionary<int, List<Point>>();

            List<Point> ketqua = new List<Point>();
            Point startPoint = new Point(5, 5);
            int buocnhayX = buocnhayXInput;  //100
            const int khoangcachXx = 200;
            const int buocnhayY = 24;
            const int limitY = 10;
            Point current = new Point(startPoint.X, startPoint.Y);

            if (sophantu == 0) return tDictionary;
            if (sophantu == 1)
            {
                tDictionary.Add(1, new List<Point> { startPoint });
                return tDictionary;
            }
            for (int i = 1; i <= sophantu; i++)
            {
                ketqua = new List<Point>();
                if (i == 11)
                {
                    sophantu++;
                    i++;
                }
                if (i > limitY && i % limitY > 3)
                {
                    int j = limitY - (i % limitY) + 2;
                    sophantu += j;
                    i += j;
                }
                if (sophantu == 6)
                {
                    current.Y = startPoint.Y + ((i % limitY == 0 ? limitY : i % limitY) - 1) * buocnhayY;
                    current.X = startPoint.X + (i % limitY == 0 ? (i / limitY) - 1 : (i / limitY)) * (buocnhayX + khoangcachXx);
                }
                else if (sophantu >= 13)
                {
                    if (i > 7)
                    {
                        current.Y = startPoint.Y + (((i % limitY == 0 ? limitY : i % limitY) - 1) - 7) * buocnhayY;
                        current.X = startPoint.X + ((i % limitY == 0 ? (i / limitY) - 1 : (i / limitY))) * (buocnhayX + khoangcachXx) + 450;
                    }
                    else
                    {
                        current.Y = startPoint.Y + ((i % limitY == 0 ? limitY : i % limitY) - 1) * buocnhayY;
                        current.X = startPoint.X + (i % limitY == 0 ? (i / limitY) - 1 : (i / limitY)) * (buocnhayX + khoangcachXx);
                    }
                }
                else
                {
                    if (i > 6)
                    {
                        current.Y = startPoint.Y + (((i % limitY == 0 ? limitY : i % limitY) - 1) - 6) * buocnhayY;
                        current.X = startPoint.X + ((i % limitY == 0 ? (i / limitY) - 1 : (i / limitY))) * (buocnhayX + khoangcachXx) + 450;
                    }
                    else
                    {
                        current.Y = startPoint.Y + ((i % limitY == 0 ? limitY : i % limitY) - 1) * buocnhayY;
                        current.X = startPoint.X + (i % limitY == 0 ? (i / limitY) - 1 : (i / limitY)) * (buocnhayX + khoangcachXx);
                    }
                }

                ketqua.Add(current);
                current.X += buocnhayX;
                ketqua.Add(current);

                tDictionary.Add(i, ketqua);
            }

            return tDictionary;
        }
        #endregion

        #region Config Grid
        void ConfigGrid(T input)
        {
            uGrid0.ResetText();
            uGrid0.ResetUpdateMode();
            uGrid0.ResetExitEditModeOnLeave();
            uGrid0.ResetRowUpdateCancelAction();
            uGrid0.DataSource = null;
            uGrid0.Layouts.Clear();
            uGrid0.ResetLayouts();
            uGrid0.ResetDisplayLayout();
            uGrid0.Refresh();
            uGrid0.ClearUndoHistory();
            uGrid0.ClearXsdConstraints();

            #region Config Grid
            if (!input.HasProperty("TypeID") || TemplateChild == null) return;
            int typeIdInput = input.GetProperty<T, int>("TypeID");
            //edit by cuongpv
            typeIdInput = new int[] { 421, 422 }.Contains(typeIdInput) ? 420 : typeIdInput;
            //end edit by cuongpv
            //check currency
            string currency = input.HasProperty("CurrencyID") ? input.GetProperty<T, string>("CurrencyID") : string.Empty;
            bool isForeignCurrency = !string.IsNullOrEmpty(input.GetProperty<T, string>("CurrencyID")) && !currency.Equals("VND");

            foreach (TemplateDetail detail in TemplateChild.TemplateDetails)
            {
                foreach (TemplateColumn column in detail.TemplateColumns)
                {
                    column.IsReadOnly = false;
                }
            }
            this.ConfigGridByTemplete<T>(typeIdInput, TemplateChild, false, new BindingList<IList> { GetIlist(input) }, isCatalog: true, isForeignCurrency: isForeignCurrency);
            if (uGrid0 != null && uGrid0.DisplayLayout.Bands[0].Columns.Exists("OrderPriority"))
            {
                uGrid0.DisplayLayout.Bands[0].Columns["OrderPriority"].SortIndicator = SortIndicator.Ascending;
                if (typeof(T) == typeof(PSSalarySheet))
                {
                    uGrid0.DisplayLayout.Bands[0].Columns["SalaryCoefficient"].FormatNumberic(ConstDatabase.Format_Rate);
                    uGrid0.DisplayLayout.Bands[0].Columns["BasicWage"].FormatNumberic(ConstDatabase.Format_TienVND);
                    uGrid0.DisplayLayout.Bands[0].Columns["WorkingDayUnitPrice"].FormatNumberic(ConstDatabase.Format_DonGiaQuyDoi);
                    uGrid0.DisplayLayout.Bands[0].Columns["PaidWorkingDayAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                    uGrid0.DisplayLayout.Bands[0].Columns["NonWorkingDayAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }

            uGrid0.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            uGrid0.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            #endregion

            if (typeof(T) == typeof(PSTimeSheet))
            {
                try
                {
                    List<int> weekend = new List<int>();
                    int Year = input.GetProperty<T, int>("Year");
                    int Month = input.GetProperty<T, int>("Month");
                    int daysInMonth = DateTime.DaysInMonth(Year, Month);
                    for (int i = 1; i <= daysInMonth; i++)
                    {
                        DateTime dt = new DateTime(Year, Month, i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            weekend.Add(i);
                        }
                        else if (dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            weekend.Add(i);
                        }

                    }
                    for (int i = 1; i <= daysInMonth; i++)
                    {
                        DateTime dt = new DateTime(Year, Month, i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            uGrid0.DisplayLayout.Bands[0].Columns["Day" + i].Header.Appearance.BackColor = System.Drawing.ColorTranslator.FromHtml("#f26f21");
                        }
                        else
                        {
                            uGrid0.DisplayLayout.Bands[0].Columns["Day" + i].Header.Appearance.BackColor = Color.FromArgb(255, 50, 104, 189);
                        }
                    }
                }
                catch { }
            }

            //add by cuongpv
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            //end add by cuongpv
        }



        void GetTemplateChild(T input)
        {
            if (!input.HasProperty("TypeID") || !input.HasProperty("TemplateID")) return;
            int typeIdInput = input.GetProperty<T, int>("TypeID");
            Guid? templateIdInput = input.GetProperty<T, Guid?>("TemplateID");
            TemplateChild = Utils.GetMauGiaoDien(typeIdInput, templateIdInput, Utils.ListTemplate);
        }

        /// <summary>
        /// Get Ilist (Config chứng từ móc tùy từng frm)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        IList GetIlist(T input)
        {
            int typeIdInput = input.GetProperty<T, int>("TypeID");
            //SAInvoiceDetail
            List<int> lstTypeId = Utils.TypesSaInvoice;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new SAInvoiceDetail());

            lstTypeId = new List<int> { 101 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, this, string.Format("{0}DetailCustomers", input.GetType().Name));

            lstTypeId = Utils.TypesPpInvoice;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new PPInvoiceDetail());

            lstTypeId = Utils.TypesFaIncrement;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new FAIncrementDetail());

            lstTypeId = Utils.TypesPpService;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new PPServiceDetail());

            lstTypeId = Utils.TypesPpDiscountReturn;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new PPDiscountReturnDetail());

            lstTypeId = Utils.TypesSaReturn;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new SAReturnDetail());

            lstTypeId = new List<int> { 118, 128, 134, 144 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, this, string.Format("{0}DetailVendors", input.GetType().Name));

            lstTypeId = Utils.TypesTiIncrement;
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new TIIncrementDetail());

            lstTypeId = new List<int> { 121 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new MBTellerPaperDetailSalary(), "MBTellerPaperDetailSalarys");

            lstTypeId = new List<int> { 111 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new MCPaymentDetailSalary(), "MCPaymentDetailSalarys");

            lstTypeId = new List<int> { 115 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new MCPaymentDetailInsurance(), "MCPaymentDetailInsurances");

            lstTypeId = new List<int> { 125 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new MBTellerPaperDetailInsurance(), "MBTellerPaperDetailInsurances");
            lstTypeId = new List<int> { 690 };
            if (lstTypeId.Contains(typeIdInput)) return getIlistByKey(input, new GOtherVoucherDetailExpense(), "GOtherVoucherDetailExpenses");

            return getIlistByKey(input, this, string.Format("{0}Details", input.GetType().Name));
        }

        IList getIlistByKey<TK>(T input, TK service, string chose = "")
        {
            if (!string.IsNullOrEmpty(chose))
            {

                return input.GetProperty<T, IList>(chose);
            }
            Guid idParrent = input.GetProperty<T, Guid>("ID");
            IList list = this.GetIService(service).Query.ToList();
            if (input.GetProperty<T, int>("TypeID") == 121 || input.GetProperty<T, int>("TypeID") == 111)
                return list;
            IList lstReturn = new List<TK>();
            foreach (TK item in from TK item in list let str = string.Format("{0}ID", service.GetType().Name.Replace("Detail", "")) let guid = item.GetProperty<TK, Guid>(str) where guid == idParrent select item)
                lstReturn.Add(item);
            return lstReturn;
        }
        #endregion

        #region Config Recorded & UnRecorded
        void ConfigButtonsBySelected(T input = default(T))
        {
            //nếu ko có chứng từ nào trên danh sách thì disable các button: Sửa, Xóa, Ghi sổ, bỏ ghi sổ
            btnEdit.Enabled = btnDelete.Enabled = btnRecorded.Enabled = btnUnRecorded.Enabled = !(((List<T>)uGrid.DataSource).Count == 0);
            //config button ghi so/ bo ghi so
            btnRecorded.Visible = btnUnRecorded.Visible = false;
            if (input != null)
            {
                const string key = "Recorded";
                bool isRecorded = input.GetProperty<T, bool>(key);
                btnRecorded.Visible = !isRecorded;
                btnUnRecorded.Visible = isRecorded;
                return;
            }
            //if (uGrid.Selected.Rows.Count == 0) return;
            if (uGrid.Selected.Rows.Count == 1)
            {
                T temp = (T)uGrid.Selected.Rows[0].ListObject;
                const string key = "Recorded";
                if (temp != null && temp.HasProperty(key))
                {
                    bool isRecorded = temp.GetProperty<T, bool>(key);
                    btnRecorded.Visible = !isRecorded;
                    btnUnRecorded.Visible = isRecorded;
                    if (isRecorded && temp.HasProperty("PostedDate"))
                    {
                        var date = temp.GetProperty<T, DateTime>("PostedDate");
                        btnUnRecorded.Enabled = date.Date > Utils.GetDBDateClosed().StringToDateTime();
                        btnDelete.Enabled = date.Date > Utils.GetDBDateClosed().StringToDateTime();
                    }
                }
            }
            else if (uGrid.Selected.Rows.Count > 1)
            {
                // Hautv edit xóa xuất hóa đơn
                const string key = "Recorded";
                if (uGrid.Selected.Rows[0] != null && uGrid.Selected.Rows[0].HasProperty(key))
                {
                    btnDelete.Enabled = btnUnRecorded.Enabled = false;
                }
                foreach (UltraGridRow ultraGridRow in uGrid.Selected.Rows)
                {
                    T temp = (T)ultraGridRow.ListObject;
                    if (btnRecorded.Visible && btnUnRecorded.Visible) goto G;
                    if (temp != null && temp.HasProperty(key))
                    {
                        bool isRecorded = temp.GetProperty<T, bool>(key);
                        if (!isRecorded && !btnRecorded.Visible)
                            btnRecorded.Visible = true;
                        if (isRecorded && !btnUnRecorded.Visible)
                            btnUnRecorded.Visible = true;
                    }
                G:
                    if (btnUnRecorded.Enabled) continue;
                    var date = temp.GetProperty<T, DateTime>("PostedDate");
                    if (date > Utils.GetDBDateClosed().StringToDateTime())
                    {
                        if (btnUnRecorded.Visible)
                        {
                            btnUnRecorded.Enabled = true;
                        }

                        btnDelete.Enabled = true;
                    }
                }
                btnEdit.Enabled = false;
            }
            if (btnAdd.Enabled && !Authenticate.Permissions("TH", _subSystemCode))
                btnAdd.Enabled = false;
            //if (btnEdit.Enabled && !Authenticate.Permissions("SU", _subSystemCode))
            //    btnEdit.Enabled = false;
            if (btnDelete.Enabled && !Authenticate.Permissions("XO", _subSystemCode))
                btnDelete.Enabled = false;
            if (btnRecorded.Enabled && !Authenticate.Permissions("GH", _subSystemCode))
                btnRecorded.Enabled = false;
            if (btnUnRecorded.Enabled && !Authenticate.Permissions("BO", _subSystemCode))
                btnUnRecorded.Enabled = false;
            if (btnPrint.Enabled && !Authenticate.Permissions("IN", _subSystemCode))
                btnPrint.Enabled = false;

            if (tsmAdd.Enabled && !Authenticate.Permissions("TH", _subSystemCode))
                tsmAdd.Enabled = false;
            //if (tsmEdit.Enabled && !Authenticate.Permissions("SU", _subSystemCode))
            //    tsmEdit.Enabled = false;
            if (tsmDelete.Enabled && !Authenticate.Permissions("XO", _subSystemCode))
                tsmDelete.Enabled = false;
            if (tsmPost.Enabled && !Authenticate.Permissions("GH", _subSystemCode))
                tsmPost.Enabled = false;
            if (tsmUnPost.Enabled && !Authenticate.Permissions("BO", _subSystemCode))
                tsmUnPost.Enabled = false;

            //set màu active là màu ghi sổ nếu chứng từ được chọn đang ghi sổ (đang xây dựng)
            //if (uGrid == null || uGrid.ActiveRow == null) return;
            //uGrid.ActiveRow.RowSelectorAppearance.ForeColor = Utils.GetColorNotRecorded();  //btnUnRecorded.Enabled ? Utils.GetColorNotRecorded() : Color.Black;
            if (typeof(T) == typeof(SAOrder) || typeof(T) == typeof(PPOrder) || typeof(T) == typeof(MCAudit) || typeof(T) == typeof(MCAudit) || typeof(T) == typeof(SAQuote) || typeof(T) == typeof(RSAssemblyDismantlement) || typeof(T) == typeof(FAAudit) || typeof(T) == typeof(TIAudit))
            {
                if (uGrid.Selected.Rows.Count == 1)
                {
                    T temp = (T)uGrid.Selected.Rows[0].ListObject;
                    const string key = "Recorded";
                    if (temp != null)
                    {
                        if (temp.HasProperty("Date"))
                        {
                            var date = temp.GetProperty<T, DateTime>("Date");
                            btnDelete.Enabled = date > Utils.GetDBDateClosed().StringToDateTime();
                        }
                    }
                }
            }

            if (typeof(T) == typeof(FAAudit) || typeof(T) == typeof(RSAssemblyDismantlement))
            {
                btnUnRecorded.Visible = false;
                btnRecorded.Visible = false;
            }

            ConfigButtons();
        }
        #endregion

        void FillColorRecordedGrid()
        {
            if (((List<T>)uGrid.DataSource).Count == 0) return;
            //tô lại màu ghi sổ
            try
            {
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("Recorded"))
                {
                    foreach (var item in uGrid.Rows)
                    {
                        if ((bool)item.Cells["Recorded"].Value)
                        {
                            item.Appearance.ResetForeColorDisabled();
                            //item.Appearance.ForeColorDisabled = Utils.DefaultRowSelectedColor;
                            item.Appearance.ForeColor = Utils.DefaultRowSelectedColor;
                            //item.Appearance.ForeColor= System.Drawing.Color.Red;
                        }
                        else if (!(bool)item.Cells["Recorded"].Value)
                            item.Appearance.ForeColorDisabled = Utils.GetColorNotRecorded();
                    }
                }
            }
            catch { }
        }

        public bool DeleteSelectJoin<T1>(T1 @select, object selectJoin, Guid? id, int typeID)
        {
            bool status = true;
            try
            {
                #region Xóa chứng từ móc
                if (id == null) return false;
                var guid = (Guid)id;
                //SaInvoice
                if (Utils.TypesSaInvoice.Any(p => p == typeID))
                {
                    var temp = ISAInvoiceService.Getbykey(guid);
                    if (temp != null)
                        ISAInvoiceService.Delete(temp);
                }
                //PPInvoice
                else if (Utils.TypesPpInvoice.Any(p => p == typeID))
                {
                    var temp = IPPInvoiceService.Getbykey(guid);
                    if (temp != null)
                        IPPInvoiceService.Delete(temp);
                }
                //FAIncrement
                else if (Utils.TypesFaIncrement.Any(p => p == typeID))
                {
                    var temp = IFAIncrementService.Getbykey(guid);
                    if (temp != null)
                        IFAIncrementService.Delete(temp);
                }
                //FAIncrement
                else if (Utils.TypesTiIncrement.Any(p => p == typeID))
                {
                    var temp = ITIIncrementService.Getbykey(guid);
                    if (temp != null)
                        ITIIncrementService.Delete(temp);
                }
                //PPService
                else if (Utils.TypesPpService.Any(p => p == typeID))
                {
                    var temp = IPPServiceService.Getbykey(guid);
                    if (temp != null)
                        IPPServiceService.Delete(temp);
                }
                //PPDiscountReturn
                else if (Utils.TypesPpDiscountReturn.Any(p => p == typeID))
                {
                    var temp = IPPDiscountReturnService.Getbykey(guid);
                    if (temp != null)
                        IPPDiscountReturnService.Delete(temp);
                }
                //SAReturn
                else if (Utils.TypesSaReturn.Any(p => p == typeID))
                {
                    var temp = ISAReturnService.Getbykey(guid);
                    if (temp != null)
                        ISAReturnService.Delete(temp);
                }
                //======================================================//
                #region Lưu thêm TH chứng từ móc 3
                #region Nếu TH hiện là Thu/Chi
                if (new int[] { 102, 117, 127, 131, 141, 171 }.Any(p => p == typeID))
                {
                    SaveLeftJoinSelect<T1, RSInwardOutward>(@select, id, true);
                }
                #endregion
                #region Nếu TH hiện là Nhập kho
                else if (new int[] { 402 }.Any(p => p == typeID))
                {
                    SaveLeftJoinSelect<T1, MCPayment>(@select, id, true);
                }
                #endregion
                #region Nếu TH hiện là Xuất kho
                else if (new int[] { 412 }.Any(p => p == typeID))
                {
                    SaveLeftJoinSelect<T1, MCReceipt>(@select, id, true);
                }
                #endregion
                #endregion
                #endregion
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }
        #endregion

        private void BaseBusiness_Load(object sender, EventArgs e)
        {
            Activate();
            Focus();
            uGrid.Focus();
            //TLoadChilGrid = new Thread(new ThreadStart(() => GetTemplateChild(_select)));
            //TLoadChilGrid.IsBackground = true;
            //TLoadChilGrid.Start();
            //ConfigTtt(_select);
            //ConfigButtonsBySelected();
            //TLoadChilGrid.Join();
            //ConfigGrid(_select);

            //UGridAfterSelectChange(_select);
            //LoadDuLieu();
        }

        private void cms4Grid_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            T input = default(T);
            //nếu ko có chứng từ nào trên danh sách thì disable các button: Sửa, Xóa, Ghi sổ, bỏ ghi sổ
            tsmEdit.Visible = tsmDelete.Visible = tsmPost.Visible = tsmUnPost.Visible = !(((List<T>)uGrid.DataSource).Count == 0);
            //config button ghi so/ bo ghi so
            tsmPost.Visible = tsmUnPost.Visible = false;
            tsmSelectAll.Enabled = uGrid.Selected.Rows.Count > 0;
            if (input != null)
            {
                const string key = "Recorded";
                bool isRecorded = input.GetProperty<T, bool>(key);
                tsmPost.Visible = !isRecorded;
                tsmUnPost.Visible = isRecorded;
                return;
            }
            if (uGrid.Selected.Rows.Count == 0) return;
            if (uGrid.Selected.Rows.Count == 1)
            {
                T temp = (T)uGrid.Selected.Rows[0].ListObject;
                const string key = "Recorded";
                if (temp != null && temp.HasProperty("TypeID"))
                {
                    int typeId = temp.GetProperty<T, int>("TypeID");
                    tsmDelete.Enabled = !new[] { 404, 411 }.Contains(typeId);
                }
                if (temp != null && temp.HasProperty(key))
                {
                    bool isRecorded = temp.GetProperty<T, bool>(key);
                    tsmPost.Visible = !isRecorded;
                    tsmUnPost.Visible = isRecorded;
                    if (temp.HasProperty("PostedDate"))
                    {
                        var date = temp.GetProperty<T, DateTime>("PostedDate");
                        tsmUnPost.Enabled = date > Utils.GetDBDateClosed().StringToDateTime();
                        tsmDelete.Enabled = date > Utils.GetDBDateClosed().StringToDateTime();
                    }
                }
            }
            else if (uGrid.Selected.Rows.Count > 1)
            {
                tsmDelete.Enabled = true;
                foreach (UltraGridRow ultraGridRow in uGrid.Selected.Rows)
                {
                    if (btnRecorded.Enabled && btnUnRecorded.Enabled) break;

                    T temp = (T)ultraGridRow.ListObject;
                    const string key = "Recorded";
                    if (temp != null && temp.HasProperty(key))
                    {
                        bool isRecorded = temp.GetProperty<T, bool>(key);
                        tsmPost.Visible = !isRecorded;
                        tsmUnPost.Visible = isRecorded;
                    }
                }
                tsmEdit.Visible = false;
            }
        }

        private void btnPPReturn_Click(object sender, EventArgs e)
        {
            AddFunctionBase(220);
        }

        private void btnPPDiscount_Click(object sender, EventArgs e)
        {
            AddFunctionBase(230);
        }

        private void btnSAReturn_Click(object sender, EventArgs e)
        {
            AddFunctionBase(330);
        }

        private void btnSADiscount_Click(object sender, EventArgs e)
        {
            AddFunctionBase(340);
        }

        private void drpbtnAdd_DroppingDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Control control = (Control)sender;
            var startPoint = control.PointToScreen(new Point(control.Location.X + control.Margin.Left + control.Padding.Left - drpbtnAdd.ImageSize.Width, control.Location.Y + control.Size.Height + control.Margin.Top + control.Padding.Top - drpbtnAdd.ImageSize.Height));
            if (typeof(T) == typeof(PPDiscountReturn))
                cms4ButtonPPDiscountReturn.Show(startPoint);
            else if (typeof(T) == typeof(SAReturn))
                cms4ButtonSAReturns.Show(startPoint);
        }

        private void drpbtnAdd_Click(object sender, EventArgs e)
        {
            //if (typeof(T) == typeof(PPDiscountReturn))
            //AddFunctionBase(defaultTypeId);
            //else if (typeof(T) == typeof(SAReturn))
            //    AddFunctionBase(330);
            drpbtnAdd_DroppingDown(sender, new System.ComponentModel.CancelEventArgs(false));
        }

        private void drpbtnAdd_MouseHover(object sender, EventArgs e)
        {
            drpbtnAdd_DroppingDown(sender, new System.ComponentModel.CancelEventArgs(false));
        }

        private void UGridLeave(object sender, EventArgs e)
        {
            _select = (T)Activator.CreateInstance(typeof(T));
        }

        private void tsmSelectAll_Click(object sender, EventArgs e)
        {
            _notEvent = true;
            foreach (UltraGridRow ultraGridRow in uGrid.Rows)
                ultraGridRow.Selected = true;
            _notEvent = false;
            //UGridAfterSelectChangeBase(uGrid, null);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (Utils.isDemo)
            {
                MSG.Warning("Phiên bản dùng thử không có quyền sử dụng chức năng này");
                return;
            }
            uGrid.PrintPreview();
        }

        private void uGrid_InitializePrintPreview(object sender, CancelablePrintPreviewEventArgs e)
        {
            // Set the zomm level to 100 % in the print preview.
            e.PrintPreviewSettings.Zoom = 1.0;

            // Set the location and size of the print preview dialog.
            e.PrintPreviewSettings.DialogLeft = SystemInformation.WorkingArea.X;
            e.PrintPreviewSettings.DialogTop = SystemInformation.WorkingArea.Y;
            e.PrintPreviewSettings.DialogWidth = SystemInformation.WorkingArea.Width;
            e.PrintPreviewSettings.DialogHeight = SystemInformation.WorkingArea.Height;

            // Horizontally fit everything in a signle page.
            e.DefaultLogicalPageLayoutInfo.FitWidthToPages = 1;

            // Set up the header and the footer.
            e.DefaultLogicalPageLayoutInfo.PageHeader = string.Format("Danh sách {0}", _title).ToUpper();
            e.DefaultLogicalPageLayoutInfo.PageHeaderHeight = 40;
            e.DefaultLogicalPageLayoutInfo.PageHeaderAppearance.FontData.SizeInPoints = 14;
            e.DefaultLogicalPageLayoutInfo.PageHeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
            e.DefaultLogicalPageLayoutInfo.PageHeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;

            // Use <#> token in the string to designate page numbers.
            e.DefaultLogicalPageLayoutInfo.PageFooter = "Trang <#>.";
            e.DefaultLogicalPageLayoutInfo.PageFooterHeight = 40;
            e.DefaultLogicalPageLayoutInfo.PageFooterAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
            e.DefaultLogicalPageLayoutInfo.PageFooterAppearance.FontData.Italic = Infragistics.Win.DefaultableBoolean.True;
            e.DefaultLogicalPageLayoutInfo.PageFooterBorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;

            // Set the ClippingOverride to Yes.
            e.DefaultLogicalPageLayoutInfo.ClippingOverride = ClippingOverride.Yes;

            // Set the document name through the PrintDocument which returns a PrintDocument object.
            e.PrintDocument.DocumentName = _title;
        }

        private void btnLoc_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode(_subSystemCode, Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);

                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = _subSystemCode;
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
        }

        private void BaseBusiness_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }

        private void BaseBusiness_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();


            uGrid0.ResetText();
            uGrid0.ResetUpdateMode();
            uGrid0.ResetExitEditModeOnLeave();
            uGrid0.ResetRowUpdateCancelAction();
            uGrid0.DataSource = null;
            uGrid0.Layouts.Clear();
            uGrid0.ResetLayouts();
            uGrid0.ResetDisplayLayout();
            uGrid0.Refresh();
            uGrid0.ClearUndoHistory();
            uGrid0.ClearXsdConstraints();
        }

        //private void uGrid_ClickCell(object sender, ClickCellEventArgs e)
        //{
        //    if (!e.Cell.Column.Key.Equals("Check"))
        //        e.Cell.Row.Selected = true;
        //}
    }
}
