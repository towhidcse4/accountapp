﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.OverdueInterest;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSAOverdueInterest : CustormForm
    {
        private List<AccountingObject> lstAccountingObject = new List<AccountingObject>();
        private List<Account> lstAccounts = new List<Account>();
        public FSAOverdueInterest()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            //// Load dữ liệu combobox Khách hàng
            //cbbAccountingObject.DataSource = _accountingObjectSrv.GetAccountingObjects(0, true);
            lstAccountingObject = Utils.IAccountingObjectService.GetAccountingObjects(0);
            lstAccounts = Utils.IAccountService.GetByDetailType(1);
            Utils.GetValueList<AccountingObject>(this, cbbAccountingObject, lstAccountingObject, "ID", "AccountingObjectCode", ConstDatabase.AccountingObject_TableName, null, null, 0);
            Utils.GetValueList<Account>(this, cbbAccount, lstAccounts, "ID", "AccountNumber", ConstDatabase.Account_TableName);
            // load ngày tính lãi. Note: mặc định lấy ngày hạch toán.
            dtNgayTinhLai.Value = OPNUntil.StartDateOPN;
            ViewDanhSach(new List<OverdueInterest>());
            if (cbbAccount.Rows.Count > 0) cbbAccount.SelectedRow = cbbAccount.Rows[0];
            if (cbbAccountingObject.Rows.Count > 0) cbbAccountingObject.SelectedRow = cbbAccountingObject.Rows[0];
            WaitingFrm.StopWaiting();
        }

        private void bntExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ViewDanhSach(List<OverdueInterest> model)
        {
            uGridDanhSach.DataSource = model;
            Utils.ConfigGrid(uGridDanhSach, "", MauDanhSach());
            uGridDanhSach.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridDanhSach.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

            uGridDanhSach.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            Utils.FormatNumberic(uGridDanhSach.DisplayLayout.Bands[0].Columns["NoQuaHan"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridDanhSach.DisplayLayout.Bands[0].Columns["NoQuaHanQd"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGridDanhSach.DisplayLayout.Bands[0].Columns["TienPhat"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridDanhSach.DisplayLayout.Bands[0].Columns["TienPhatQd"], ConstDatabase.Format_ForeignCurrency);
            //uGridDanhSach.DisplayLayout.Bands[0].Summaries.Clear();
            OPNUntil.AddSumChoCot(uGridDanhSach, "NoQuaHan");
            OPNUntil.AddSumChoCot(uGridDanhSach, "NoQuaHanQd");
            OPNUntil.AddSumChoCot(uGridDanhSach, "TienPhat");
            OPNUntil.AddSumChoCot(uGridDanhSach, "TienPhatQd");
        }
        // Mẫu giao diện Grid danh sách tính lãi nợ của khách hàng
        private List<TemplateColumn> MauDanhSach()
        {
            return new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "ID",
                                   ColumnCaption = "",
                                   ColumnWidth = 100,
                                   IsVisible = false,
                                   IsVisibleCbb = false,
                                   VisiblePosition = 1,
                                   IsReadOnly = false
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "MaKhachHang",
                                   ColumnCaption = "Mã khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   IsReadOnly = true,
                                   IsColumnHeader = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "TenKhachHang",
                                   ColumnCaption = "Tên khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3,
                                   IsReadOnly = true,
                                   IsColumnHeader = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "SoChungTu",
                                   ColumnCaption = "Số chứng từ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "LoaiTien",
                                   ColumnCaption = "Loại tiền",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 5,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NoQuaHan",
                                   ColumnCaption = "Nợ quá hạn",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 6,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NoQuaHanQd",
                                   ColumnCaption = "Nợ quá hạn quy đổi",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 7,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "TienPhat",
                                   ColumnCaption = "Tiền phạt",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 8,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "TienPhatQd",
                                   ColumnCaption = "Tiền phạt quy đổi",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 9,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "TkPhaiThu",
                                   ColumnCaption = "Tài khoản phải thu",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition =10,
                                   IsReadOnly = true
                               }
                       };
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            if (lstAccountingObject.All(c => c.AccountingObjectCode != cbbAccountingObject.Text
                || lstAccounts.All(d => d.AccountNumber != cbbAccount.Text)))
            {
                MSG.Error("Tài khoản không có trên hệ thống!");
                return;
            }
            if (string.IsNullOrEmpty(dtNgayTinhLai.Text.Replace("/","").Replace("_","").Trim()))
            {
                MSG.Error("Ngày tháng không đúng");
                return;
            };
            ISAInvoiceService iSaInvoiceService = IoC.Resolve<ISAInvoiceService>();
            AccountingObject accountingObject = (AccountingObject)cbbAccountingObject.SelectedRow.ListObject;
            Account account = (Account)cbbAccount.SelectedRow.ListObject;
            List<OverdueInterest> lstOverdueInterests = new List<OverdueInterest>();
            lstOverdueInterests = iSaInvoiceService.GetOverdueInterests(
                new List<AccountingObject>() { accountingObject }, new List<Account> { account },
                (DateTime)dtNgayTinhLai.Value);
            ViewDanhSach(lstOverdueInterests);
        }

        private void FSAOverdueInterest_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDanhSach.ResetText();
            uGridDanhSach.ResetUpdateMode();
            uGridDanhSach.ResetExitEditModeOnLeave();
            uGridDanhSach.ResetRowUpdateCancelAction();
            uGridDanhSach.DataSource = null;
            uGridDanhSach.Layouts.Clear();
            uGridDanhSach.ResetLayouts();
            uGridDanhSach.ResetDisplayLayout();
            uGridDanhSach.Refresh();
            uGridDanhSach.ClearUndoHistory();
            uGridDanhSach.ClearXsdConstraints();
        }

        private void FSAOverdueInterest_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
