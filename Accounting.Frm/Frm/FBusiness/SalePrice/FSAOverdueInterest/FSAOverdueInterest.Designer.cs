﻿namespace Accounting
{
    partial class FSAOverdueInterest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccount = new Infragistics.Win.Misc.UltraLabel();
            this.lblNgayTinhLai = new Infragistics.Win.Misc.UltraLabel();
            this.dtNgayTinhLai = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.Header = new System.Windows.Forms.Panel();
            this.Body = new System.Windows.Forms.Panel();
            this.uGridDanhSach = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayTinhLai)).BeginInit();
            this.Header.SuspendLayout();
            this.Body.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).BeginInit();
            this.SuspendLayout();
            // 
            // cbbAccountingObject
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject.DisplayLayout.Appearance = appearance1;
            this.cbbAccountingObject.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cbbAccountingObject.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject.DisplayLayout.Override.CellAppearance = appearance8;
            this.cbbAccountingObject.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cbbAccountingObject.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject.DisplayLayout.Override.RowAppearance = appearance11;
            this.cbbAccountingObject.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cbbAccountingObject.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObject.Location = new System.Drawing.Point(96, 12);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.Size = new System.Drawing.Size(100, 22);
            this.cbbAccountingObject.TabIndex = 0;
            // 
            // lblAccountingObject
            // 
            appearance29.TextHAlignAsString = "Center";
            appearance29.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance29;
            this.lblAccountingObject.Location = new System.Drawing.Point(24, 12);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(66, 22);
            this.lblAccountingObject.TabIndex = 1;
            this.lblAccountingObject.Text = "Khách hàng";
            // 
            // cbbAccount
            // 
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccount.DisplayLayout.Appearance = appearance14;
            this.cbbAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.cbbAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.cbbAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccount.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccount.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.cbbAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccount.DisplayLayout.Override.CellAppearance = appearance21;
            this.cbbAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccount.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.cbbAccount.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.cbbAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccount.DisplayLayout.Override.RowAppearance = appearance24;
            this.cbbAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.cbbAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccount.Location = new System.Drawing.Point(308, 12);
            this.cbbAccount.Name = "cbbAccount";
            this.cbbAccount.Size = new System.Drawing.Size(100, 22);
            this.cbbAccount.TabIndex = 2;
            // 
            // lblAccount
            // 
            appearance30.TextHAlignAsString = "Center";
            appearance30.TextVAlignAsString = "Middle";
            this.lblAccount.Appearance = appearance30;
            this.lblAccount.Location = new System.Drawing.Point(202, 12);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(100, 22);
            this.lblAccount.TabIndex = 3;
            this.lblAccount.Text = "Tài khoản phải thu";
            // 
            // lblNgayTinhLai
            // 
            appearance31.TextVAlignAsString = "Middle";
            this.lblNgayTinhLai.Appearance = appearance31;
            this.lblNgayTinhLai.Location = new System.Drawing.Point(418, 12);
            this.lblNgayTinhLai.Name = "lblNgayTinhLai";
            this.lblNgayTinhLai.Size = new System.Drawing.Size(69, 22);
            this.lblNgayTinhLai.TabIndex = 4;
            this.lblNgayTinhLai.Text = "Ngày tính lãi";
            // 
            // dtNgayTinhLai
            // 
            appearance32.TextHAlignAsString = "Center";
            appearance32.TextVAlignAsString = "Middle";
            this.dtNgayTinhLai.Appearance = appearance32;
            this.dtNgayTinhLai.AutoSize = false;
            this.dtNgayTinhLai.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtNgayTinhLai.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtNgayTinhLai.Location = new System.Drawing.Point(493, 12);
            this.dtNgayTinhLai.MaskInput = "";
            this.dtNgayTinhLai.Name = "dtNgayTinhLai";
            this.dtNgayTinhLai.Size = new System.Drawing.Size(99, 22);
            this.dtNgayTinhLai.TabIndex = 37;
            this.dtNgayTinhLai.Value = null;
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(608, 12);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(113, 22);
            this.btnGetData.TabIndex = 38;
            this.btnGetData.Text = "Tính lãi nợ";
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // Header
            // 
            this.Header.Controls.Add(this.lblAccount);
            this.Header.Controls.Add(this.btnGetData);
            this.Header.Controls.Add(this.cbbAccountingObject);
            this.Header.Controls.Add(this.dtNgayTinhLai);
            this.Header.Controls.Add(this.lblAccountingObject);
            this.Header.Controls.Add(this.lblNgayTinhLai);
            this.Header.Controls.Add(this.cbbAccount);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(853, 48);
            this.Header.TabIndex = 39;
            // 
            // Body
            // 
            this.Body.Controls.Add(this.uGridDanhSach);
            this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Body.Location = new System.Drawing.Point(0, 48);
            this.Body.Name = "Body";
            this.Body.Size = new System.Drawing.Size(853, 303);
            this.Body.TabIndex = 40;
            // 
            // uGridDanhSach
            // 
            this.uGridDanhSach.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDanhSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDanhSach.Location = new System.Drawing.Point(0, 0);
            this.uGridDanhSach.Name = "uGridDanhSach";
            this.uGridDanhSach.Size = new System.Drawing.Size(853, 303);
            this.uGridDanhSach.TabIndex = 40;
            this.uGridDanhSach.Text = "ultraGrid1";
            this.uGridDanhSach.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // FSAOverdueInterest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 351);
            this.Controls.Add(this.Body);
            this.Controls.Add(this.Header);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSAOverdueInterest";
            this.Text = "Tính lãi nợ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSAOverdueInterest_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSAOverdueInterest_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayTinhLai)).EndInit();
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            this.Body.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDanhSach)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccount;
        private Infragistics.Win.Misc.UltraLabel lblAccount;
        private Infragistics.Win.Misc.UltraLabel lblNgayTinhLai;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtNgayTinhLai;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private System.Windows.Forms.Panel Header;
        private System.Windows.Forms.Panel Body;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDanhSach;
    }
}