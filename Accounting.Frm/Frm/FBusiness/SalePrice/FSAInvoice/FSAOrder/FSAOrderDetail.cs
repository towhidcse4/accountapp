﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using System;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public sealed partial class FSAOrderDetail : FsaOrderDetailStand
    {
        #region khởi tạo
        private ISAOrderDetailService _ISAOrderDetailService;
        public static string No;
        private UltraGrid uGrid;
        UltraGrid ugrid2 = null;
        UltraGrid ugrid0 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        public FSAOrderDetail()
        {
            InitializeComponent();
            this.CenterToParent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            #region Thiết lập ban đầu
            _ISAOrderDetailService = IoC.Resolve<ISAOrderDetailService>();
            //InitializeGUI();
            #endregion
        }
        public FSAOrderDetail(SAOrder temp, List<SAOrder> listObject, int statusForm)
        {
            InitializeComponent();
            base.InitializeComponent1();

            #region Thiết lập ban đầu
            _typeID = 310; _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = _typeID; else _select = temp;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            Utils.ClearCacheByType<PaymentClause>();
            #endregion
        }
        #endregion

        public override void InitializeGUI(SAOrder input)
        {
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            //config số đơn hàng, ngày đơn hàng
            this.ConfigTopVouchersNo<SAOrder>(palTopVouchers, "No", "", "Date", null, false);
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            //Config Grid
            _listObjectInput.Add(new BindingList<SAOrderDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<SAOrderDetail>() : input.SAOrderDetails));
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<SAOrder>(pnlUgrid, mauGiaoDien);
            if (mauGiaoDien.TemplateName == "Mẫu thuế")
            {
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<SAOrder>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            }
            else
            {
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                this.ConfigGridByManyTemplete<SAOrder>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            }
            ugrid0 = Controls.Find("uGrid0", true).FirstOrDefault() as UltraGrid;
            ugrid0.DisplayLayout.Bands[0].Columns["QuantityReceipt"].CellActivation = Activation.NoEdit; // edit by tungnt: khóa cột số lương nhận
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);

            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            List<SAOrder> inputCurrency = new List<SAOrder> { input };
            this.ConfigGridCurrencyByTemplate<SAOrder>(input.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);

            if (input.HasProperty("TypeID") && input.HasProperty("Exported"))
            {
                int type = int.Parse(_select.GetProperty("TypeID").ToString());
                var isExported = (bool)_select.GetProperty("Exported");
                var ultraGrid = (UltraGrid)pnlUgrid.Controls.Find("uGrid1", true).FirstOrDefault();
                ultraGrid.ConfigColumnByExported(isExported, _select.GetProperty("CurrencyID").ToString(), type);
                //ultraGrid.ConfigColumnByImportPurchase(isExported, _select.GetProperty("CurrencyID").ToString(), _statusForm);
            }
            //ConfigGridBase(input, uGridControl, pnlUgrid);           
            //config combo
            Utils.isSAInvoice = true;
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            //Databinding
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtCompanyTaxCode, txtContactName, txtReason, txtDeliveryPlace }, "AccountingObjectName, AccountingObjectAddress, CompanyTaxCode, ContactName, Reason, DeliveryPlace");
            DataBinding(new List<Control> { txtTotalAmountOriginal, txtTotalVATAmountOriginal, txtTotalPaymentAmountOriginalStand }, "TotalAmountOriginal, TotalVATAmountOriginal, TotalPaymentAmountOriginal", "3,3,3", true); //, TotalDiscountAmountOriginal, TotalDiscountAmount
            DataBinding(new List<Control> { txtTotalAmount, txtTotalVATAmount, txtTotalPaymentAmountStand }, "TotalAmount, TotalVATAmount,TotalPaymentAmount", "3,3,3");
            //Total
            //_controls.Add(txtTotalPaymentAmountOriginalStand);
            //_controls.Add(txtTotalPaymentAmountStand);
            //AutoCaculatorTotal(_controls);
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 310)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Đơn đặt hàng";
                    txtReason.Text = "Đơn đặt hàng";
                    _select.Reason = "Đơn đặt hàng";
                };
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                
                    BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                    if (datasource == null)
                        datasource = new BindingList<RefVoucher>();
                    var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                    f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                    try
                    {
                        f.ShowDialog(this);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                    }
                
            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FSAOrderDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccountingObjectName_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
    }

    public class FsaOrderDetailStand : DetailBase<SAOrder> { }
}