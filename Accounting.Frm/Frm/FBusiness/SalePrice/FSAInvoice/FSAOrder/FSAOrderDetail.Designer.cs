﻿namespace Accounting
{
    sealed partial class FSAOrderDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtDeliveryPlace = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.palTopVouchers = new Infragistics.Win.Misc.UltraPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.txtTotalPaymentAmountOriginalStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryPlace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.palTopVouchers.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.panel5.SuspendLayout();
            this.palBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.txtDeliveryPlace);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox1.Controls.Add(this.txtCompanyTaxCode);
            this.ultraGroupBox1.Controls.Add(this.lblCompanyTaxCode);
            this.ultraGroupBox1.Controls.Add(this.txtReason);
            this.ultraGroupBox1.Controls.Add(this.lblReason);
            this.ultraGroupBox1.Controls.Add(this.txtContactName);
            this.ultraGroupBox1.Controls.Add(this.lblContactName);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectID);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance8.FontData.BoldAsString = "True";
            appearance8.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance8;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(929, 146);
            this.ultraGroupBox1.TabIndex = 26;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtDeliveryPlace
            // 
            this.txtDeliveryPlace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDeliveryPlace.AutoSize = false;
            this.txtDeliveryPlace.Location = new System.Drawing.Point(555, 69);
            this.txtDeliveryPlace.Name = "txtDeliveryPlace";
            this.txtDeliveryPlace.Size = new System.Drawing.Size(361, 22);
            this.txtDeliveryPlace.TabIndex = 34;
            // 
            // ultraLabel1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(426, 70);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel1.TabIndex = 33;
            this.ultraLabel1.Text = "Địa điểm giao hàng";
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(304, 21);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(612, 22);
            this.txtAccountingObjectName.TabIndex = 32;
            this.txtAccountingObjectName.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            appearance2.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(128, 21);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(170, 22);
            this.cbbAccountingObjectID.TabIndex = 31;
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.AutoSize = false;
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(128, 69);
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(278, 22);
            this.txtCompanyTaxCode.TabIndex = 29;
            // 
            // lblCompanyTaxCode
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCode.Appearance = appearance3;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(6, 71);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(115, 19);
            this.lblCompanyTaxCode.TabIndex = 28;
            this.lblCompanyTaxCode.Text = "Mã số thuế";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(128, 117);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(788, 22);
            this.txtReason.TabIndex = 27;
            // 
            // lblReason
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance4;
            this.lblReason.Location = new System.Drawing.Point(6, 118);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(115, 19);
            this.lblReason.TabIndex = 26;
            this.lblReason.Text = "Diễn giải";
            // 
            // txtContactName
            // 
            this.txtContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactName.AutoSize = false;
            this.txtContactName.Location = new System.Drawing.Point(128, 93);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(788, 22);
            this.txtContactName.TabIndex = 25;
            // 
            // lblContactName
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.lblContactName.Appearance = appearance5;
            this.lblContactName.Location = new System.Drawing.Point(6, 94);
            this.lblContactName.Name = "lblContactName";
            this.lblContactName.Size = new System.Drawing.Size(115, 19);
            this.lblContactName.TabIndex = 24;
            this.lblContactName.Text = "Họ tên người liên hệ";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(128, 45);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(788, 22);
            this.txtAccountingObjectAddress.TabIndex = 23;
            this.txtAccountingObjectAddress.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // lblAccountingObjectAddress
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance6;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(6, 47);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(115, 19);
            this.lblAccountingObjectAddress.TabIndex = 22;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance7;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(6, 23);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(115, 19);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Khách hàng";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ultraGroupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(929, 88);
            this.panel1.TabIndex = 27;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.palTopVouchers);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance10.FontData.BoldAsString = "True";
            appearance10.FontData.SizeInPoints = 13F;
            this.ultraGroupBox2.HeaderAppearance = appearance10;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(929, 88);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "ĐƠN ĐẶT HÀNG";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palTopVouchers
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopVouchers.Appearance = appearance9;
            this.palTopVouchers.AutoSize = true;
            this.palTopVouchers.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.palTopVouchers.Location = new System.Drawing.Point(12, 31);
            this.palTopVouchers.Name = "palTopVouchers";
            this.palTopVouchers.Size = new System.Drawing.Size(308, 43);
            this.palTopVouchers.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.palBottom);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 88);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(929, 653);
            this.panel2.TabIndex = 28;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.ultraSplitter1);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(929, 542);
            this.panel4.TabIndex = 28;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pnlUgrid);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 156);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(929, 386);
            this.panel6.TabIndex = 28;
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 0);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(929, 386);
            this.pnlUgrid.TabIndex = 0;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance11;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(830, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 146);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 146;
            this.ultraSplitter1.Size = new System.Drawing.Size(929, 10);
            this.ultraSplitter1.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ultraGroupBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(929, 146);
            this.panel5.TabIndex = 27;
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountOriginalStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmount);
            this.palBottom.Controls.Add(this.txtTotalAmount);
            this.palBottom.Controls.Add(this.lblTotalVATAmount);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmount);
            this.palBottom.Controls.Add(this.lblTotalAmount);
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 542);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(929, 111);
            this.palBottom.TabIndex = 27;
            // 
            // txtTotalPaymentAmountOriginalStand
            // 
            this.txtTotalPaymentAmountOriginalStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Right";
            appearance12.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountOriginalStand.Appearance = appearance12;
            this.txtTotalPaymentAmountOriginalStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountOriginalStand.Location = new System.Drawing.Point(585, 47);
            this.txtTotalPaymentAmountOriginalStand.Name = "txtTotalPaymentAmountOriginalStand";
            this.txtTotalPaymentAmountOriginalStand.Size = new System.Drawing.Size(169, 19);
            this.txtTotalPaymentAmountOriginalStand.TabIndex = 44;
            // 
            // txtTotalVATAmountOriginal
            // 
            this.txtTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Right";
            appearance13.TextVAlignAsString = "Middle";
            this.txtTotalVATAmountOriginal.Appearance = appearance13;
            this.txtTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmountOriginal.Location = new System.Drawing.Point(585, 26);
            this.txtTotalVATAmountOriginal.Name = "txtTotalVATAmountOriginal";
            this.txtTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalVATAmountOriginal.TabIndex = 43;
            // 
            // txtTotalAmountOriginal
            // 
            this.txtTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.txtTotalAmountOriginal.Appearance = appearance14;
            this.txtTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmountOriginal.Location = new System.Drawing.Point(585, 5);
            this.txtTotalAmountOriginal.Name = "txtTotalAmountOriginal";
            this.txtTotalAmountOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalAmountOriginal.TabIndex = 42;
            // 
            // txtTotalPaymentAmountStand
            // 
            this.txtTotalPaymentAmountStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Right";
            appearance15.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountStand.Appearance = appearance15;
            this.txtTotalPaymentAmountStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountStand.Location = new System.Drawing.Point(760, 47);
            this.txtTotalPaymentAmountStand.Name = "txtTotalPaymentAmountStand";
            this.txtTotalPaymentAmountStand.Size = new System.Drawing.Size(169, 19);
            this.txtTotalPaymentAmountStand.TabIndex = 41;
            // 
            // txtTotalVATAmount
            // 
            this.txtTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextHAlignAsString = "Right";
            appearance16.TextVAlignAsString = "Middle";
            this.txtTotalVATAmount.Appearance = appearance16;
            this.txtTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmount.Location = new System.Drawing.Point(760, 26);
            this.txtTotalVATAmount.Name = "txtTotalVATAmount";
            this.txtTotalVATAmount.Size = new System.Drawing.Size(169, 19);
            this.txtTotalVATAmount.TabIndex = 40;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Right";
            appearance17.TextVAlignAsString = "Middle";
            this.txtTotalAmount.Appearance = appearance17;
            this.txtTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(760, 5);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(169, 19);
            this.txtTotalAmount.TabIndex = 39;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance18;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(441, 27);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalVATAmount.TabIndex = 34;
            this.lblTotalVATAmount.Text = "Tổng thuế GTGT";
            // 
            // lblTotalPaymentAmount
            // 
            this.lblTotalPaymentAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmount.Appearance = appearance19;
            this.lblTotalPaymentAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(441, 48);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalPaymentAmount.TabIndex = 32;
            this.lblTotalPaymentAmount.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance20;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(441, 5);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalAmount.TabIndex = 30;
            this.lblTotalAmount.Text = "Tổng tiền hàng";
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.Location = new System.Drawing.Point(441, 71);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(488, 40);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // FSAOrderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 741);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSAOrderDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSAOrderDetail_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryPlace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.palTopVouchers.ResumeLayout(false);
            this.palTopVouchers.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.Misc.UltraLabel lblContactName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel palTopVouchers;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmount;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountOriginalStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmount;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDeliveryPlace;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
    }
}
