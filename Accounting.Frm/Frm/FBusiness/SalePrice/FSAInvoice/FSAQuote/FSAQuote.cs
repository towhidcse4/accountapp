﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSAQuote : CustormForm //UserControl
    {
        #region khai báo
        private readonly ISAQuoteService _iSAQuoteService;
        List<SAQuote> _dsSAQuote = new List<SAQuote>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FSAQuote()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _iSAQuoteService = IoC.Resolve<ISAQuoteService>();
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            _dsSAQuote = _iSAQuoteService.Query.Where(k => k.Date.Year == (dbStartDate == null ? DateTime.Now.Year : dbStartDate.Value.Year)).ToList();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsSAQuote;

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            SAQuote temp = uGrid.Selected.Rows.Count <= 0 ? new SAQuote() : uGrid.Selected.Rows[0].ListObject as SAQuote;
            new FSAQuoteDetail(temp, _dsSAQuote, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FSAQuoteDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SAQuote temp = uGrid.Selected.Rows[0].ListObject as SAQuote;
                new FSAQuoteDetail(temp, _dsSAQuote, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FSAQuoteDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SAQuote temp = uGrid.Selected.Rows[0].ListObject as SAQuote;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _iSAQuoteService.BeginTran();
                    _iSAQuoteService.Delete(temp);
                    _iSAQuoteService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.SAQuote_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }

        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            return dsTemplate[keyofdsTemplate];
        }

        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }
        #endregion

        #region Event
        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            SAQuote SAQuote = listObject as SAQuote;
            if (SAQuote == null) return;

            //Tab thông tin chung
            lblNo.Text = SAQuote.No;
            lblDate.Text = SAQuote.Date.ToString(Constants.DdMMyyyy);
            lblAccountingObjectName.Text = SAQuote.AccountingObjectName;
            lblAccountingObjectAddress.Text = SAQuote.AccountingObjectAddress;
            lblReason.Text = SAQuote.Reason;
            lblTotalAmount.Text = string.Format(SAQuote.TotalAmount == 0 ? "0" : "{0:0,0}", SAQuote.TotalAmount);  //SAQuote.TotalAmount.ToString("{0:0,0}");

            //Tab hoạch toán
            uGridPosted.DataSource = SAQuote.SAQuoteDetails;    //Utils.CloneObject(SAQuote.SAQuoteDetails);

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            Template mauGiaoDien = GetMauGiaoDien(SAQuote.TypeID, SAQuote.TemplateID, DsTemplate);

            Utils.ConfigGrid(uGridPosted, ConstDatabase.SAQuoteDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("SumAmount", SummaryType.Sum, band.Columns["Amount"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }
        private void FSAQuoteFormClosing(object sender, FormClosingEventArgs e)
        {
            //giải phóng biến static khi đóng form giúp giải phóng tài nguyên, tuy nhiên nếu để lại thì tốc độ truy cập lần sau sẽ tăng nếu mở form lại
            //DsTemplate = new Dictionary<string, Template>();
        }
        #endregion
    }
}
