﻿namespace Accounting.Frm.Frm.FBusiness.FSAInvoice.FSAInvoice
{
    partial class UBottomTong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.txtTotalDiscountAmountOriginal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTotalDiscountAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTotalDiscountAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmountOriginal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTotalPaymentAmountOriginal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTotalAmountOriginal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTotalVATAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDiscountAmountOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDiscountAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVATAmountOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPaymentAmountOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmountOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPaymentAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTotalDiscountAmountOriginal
            // 
            this.txtTotalDiscountAmountOriginal.Location = new System.Drawing.Point(320, 23);
            this.txtTotalDiscountAmountOriginal.Multiline = true;
            this.txtTotalDiscountAmountOriginal.Name = "txtTotalDiscountAmountOriginal";
            this.txtTotalDiscountAmountOriginal.ReadOnly = true;
            this.txtTotalDiscountAmountOriginal.Size = new System.Drawing.Size(169, 22);
            this.txtTotalDiscountAmountOriginal.TabIndex = 53;
            // 
            // txtTotalDiscountAmount
            // 
            this.txtTotalDiscountAmount.Location = new System.Drawing.Point(135, 23);
            this.txtTotalDiscountAmount.Multiline = true;
            this.txtTotalDiscountAmount.Name = "txtTotalDiscountAmount";
            this.txtTotalDiscountAmount.ReadOnly = true;
            this.txtTotalDiscountAmount.Size = new System.Drawing.Size(169, 22);
            this.txtTotalDiscountAmount.TabIndex = 52;
            // 
            // lblTotalDiscountAmount
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalDiscountAmount.Appearance = appearance1;
            this.lblTotalDiscountAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblTotalDiscountAmount.Location = new System.Drawing.Point(2, 25);
            this.lblTotalDiscountAmount.Name = "lblTotalDiscountAmount";
            this.lblTotalDiscountAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalDiscountAmount.TabIndex = 51;
            this.lblTotalDiscountAmount.Text = "Tổng tiền chiết khấu";
            // 
            // txtTotalVATAmountOriginal
            // 
            this.txtTotalVATAmountOriginal.Location = new System.Drawing.Point(320, 46);
            this.txtTotalVATAmountOriginal.Multiline = true;
            this.txtTotalVATAmountOriginal.Name = "txtTotalVATAmountOriginal";
            this.txtTotalVATAmountOriginal.ReadOnly = true;
            this.txtTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 22);
            this.txtTotalVATAmountOriginal.TabIndex = 50;
            // 
            // txtTotalPaymentAmountOriginal
            // 
            this.txtTotalPaymentAmountOriginal.Location = new System.Drawing.Point(320, 69);
            this.txtTotalPaymentAmountOriginal.Name = "txtTotalPaymentAmountOriginal";
            this.txtTotalPaymentAmountOriginal.ReadOnly = true;
            this.txtTotalPaymentAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.txtTotalPaymentAmountOriginal.TabIndex = 49;
            // 
            // txtTotalAmountOriginal
            // 
            this.txtTotalAmountOriginal.Location = new System.Drawing.Point(320, 1);
            this.txtTotalAmountOriginal.Name = "txtTotalAmountOriginal";
            this.txtTotalAmountOriginal.ReadOnly = true;
            this.txtTotalAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.txtTotalAmountOriginal.TabIndex = 48;
            // 
            // txtTotalVATAmount
            // 
            this.txtTotalVATAmount.Location = new System.Drawing.Point(135, 46);
            this.txtTotalVATAmount.Multiline = true;
            this.txtTotalVATAmount.Name = "txtTotalVATAmount";
            this.txtTotalVATAmount.ReadOnly = true;
            this.txtTotalVATAmount.Size = new System.Drawing.Size(169, 22);
            this.txtTotalVATAmount.TabIndex = 47;
            // 
            // lblTotalVATAmount
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalVATAmount.Appearance = appearance2;
            this.lblTotalVATAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblTotalVATAmount.Location = new System.Drawing.Point(2, 48);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalVATAmount.TabIndex = 46;
            this.lblTotalVATAmount.Text = "Tổng thuế GTGT";
            // 
            // txtTotalPaymentAmount
            // 
            this.txtTotalPaymentAmount.Location = new System.Drawing.Point(135, 69);
            this.txtTotalPaymentAmount.Name = "txtTotalPaymentAmount";
            this.txtTotalPaymentAmount.ReadOnly = true;
            this.txtTotalPaymentAmount.Size = new System.Drawing.Size(169, 21);
            this.txtTotalPaymentAmount.TabIndex = 45;
            // 
            // lblTotalPaymentAmount
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalPaymentAmount.Appearance = appearance3;
            this.lblTotalPaymentAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(2, 70);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalPaymentAmount.TabIndex = 44;
            this.lblTotalPaymentAmount.Text = "Tổng tiền thanh toán";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Location = new System.Drawing.Point(135, 1);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(169, 21);
            this.txtTotalAmount.TabIndex = 43;
            // 
            // lblTotalAmount
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalAmount.Appearance = appearance4;
            this.lblTotalAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblTotalAmount.Location = new System.Drawing.Point(2, 3);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalAmount.TabIndex = 42;
            this.lblTotalAmount.Text = "Tổng tiền hàng";
            // 
            // UBottomTong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtTotalDiscountAmountOriginal);
            this.Controls.Add(this.txtTotalDiscountAmount);
            this.Controls.Add(this.lblTotalDiscountAmount);
            this.Controls.Add(this.txtTotalVATAmountOriginal);
            this.Controls.Add(this.txtTotalPaymentAmountOriginal);
            this.Controls.Add(this.txtTotalAmountOriginal);
            this.Controls.Add(this.txtTotalVATAmount);
            this.Controls.Add(this.lblTotalVATAmount);
            this.Controls.Add(this.txtTotalPaymentAmount);
            this.Controls.Add(this.lblTotalPaymentAmount);
            this.Controls.Add(this.txtTotalAmount);
            this.Controls.Add(this.lblTotalAmount);
            this.Name = "UBottomTong";
            this.Size = new System.Drawing.Size(490, 91);
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDiscountAmountOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalDiscountAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVATAmountOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPaymentAmountOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmountOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPaymentAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalDiscountAmountOriginal;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalDiscountAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalDiscountAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalVATAmountOriginal;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalPaymentAmountOriginal;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalAmountOriginal;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
    }
}
