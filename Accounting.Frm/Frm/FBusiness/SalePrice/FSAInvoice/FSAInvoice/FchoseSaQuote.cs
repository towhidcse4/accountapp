﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;

namespace Accounting
{
    public partial class FchoseSaQuote<T> : CustormForm
    {
        #region Khai báo
        private readonly BindingList<IList> _input;
        readonly T _select;
        readonly SAQuote _saQuote;
        private UltraGrid _ultraGrid;
        private readonly List<ObjSaQuoteDetail> _lstSaQuoteDetail;
        #endregion

        public FchoseSaQuote(T @select, BindingList<IList> input, SAQuote saQuote, UltraGrid ultraGrid)
        {
            InitializeComponent();
            //config GUI

            //Lấy dữ liệu hàng hóa báo giá
            //Utils.ClearCacheByType<SAQuote>();
            //SAQuote saQuote1 = Utils.ISAQuoteService.Getbykey(saQuote.ID);
            //Utils.ISAQuoteService.UnbindSession(saQuote1);
            //saQuote1 = Utils.ISAQuoteService.Getbykey(saQuote.ID);
            txtNo.Text = saQuote.No;
            dteDate.DateTime = saQuote.Date;
            dteDate.ReadOnly = dteDate.Enabled = true;

            _lstSaQuoteDetail = new List<ObjSaQuoteDetail>();
            foreach (var item in saQuote.SAQuoteDetails)
            {
                //var detail = (ObjSaQuoteDetail)item;
                var detail = new ObjSaQuoteDetail();
                ReflectionUtils.Copy(typeof(SAQuoteDetail), item, typeof(ObjSaQuoteDetail), detail);
                _lstSaQuoteDetail.Add(detail);
            }
            uGrid.DataSource = _lstSaQuoteDetail;
            ConfigGrid(uGrid);
            _select = @select;
            _input = input;
            _saQuote = saQuote;
            _ultraGrid = ultraGrid;
            uGrid.Focus();
        }

        #region Event
        private void BtnCloseClick(object sender, EventArgs e)
        {
            Close();
            Close();
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            if (_input == null) return;
            uGrid.UpdateData();

            //lấy những cái được chọn và convert sang SainVoidDetails rồi import vào Grid bên dưới
            List<ObjSaQuoteDetail> lstSelect = _lstSaQuoteDetail.Where(k => k.Check).ToList();
            if (lstSelect.Count == 0)
            {
                MSG.Warning(resSystem.MSG_Error_21);
                return;
            }
            //Convert sang sang SainVoidDetails
            if (typeof(T) == typeof(SAInvoice))
            {
                List<SAInvoiceDetail> result = new List<SAInvoiceDetail>();
                foreach (ObjSaQuoteDetail objRsInwardOutward in lstSelect)
                {
                    SAInvoiceDetail temp = new SAInvoiceDetail();
                    ReflectionUtils.Copy(typeof(ObjSaQuoteDetail), objRsInwardOutward, typeof(SAInvoiceDetail), temp, new List<string> { "ID" });                   
                    result.Add(temp);
                }
                //Import vào cái Grid bên dưới (nếu có hàng hóa sẵn thì có thể chèn thêm vào hoặc có thể xóa trắng rồi chèn mỗi các cái đang chọn)
                if (result.Count > 0) _input[0].Clear();
                foreach (SAInvoiceDetail saInvoiceDetail in result)
                    _input[0].Add(saInvoiceDetail);
            }
            else if (typeof(T) == typeof(SAOrder))
            {
                //Convert sang sang SainVoidDetails
                List<SAOrderDetail> result = new List<SAOrderDetail>();
                foreach (ObjSaQuoteDetail objRsInwardOutward in lstSelect)
                {
                    SAOrderDetail temp = new SAOrderDetail();
                    ReflectionUtils.Copy(typeof(ObjSaQuoteDetail), objRsInwardOutward, typeof(SAOrderDetail), temp, new List<string> { "ID" });
                    result.Add(temp);
                }
                //Import vào cái Grid bên dưới (nếu có hàng hóa sẵn thì có thể chèn thêm vào hoặc có thể xóa trắng rồi chèn mỗi các cái đang chọn)
                if (result.Count > 0) _input[0].Clear();
                foreach (SAOrderDetail saOrderDetail in result)
                    _input[0].Add(saOrderDetail);
            }
            if (_select.HasProperty("AccountingObjectID"))
                _select.SetProperty("AccountingObjectID", _saQuote.AccountingObjectID);
            if (_select.HasProperty("AccountingObjectName"))
                _select.SetProperty("AccountingObjectName", _saQuote.AccountingObjectName);
            if (_select.HasProperty("AccountingObjectAddress"))
                _select.SetProperty("AccountingObjectAddress", _saQuote.AccountingObjectAddress);
            if (_select.HasProperty("CompanyTaxCode"))
                _select.SetProperty("CompanyTaxCode", _saQuote.CompanyTaxCode);
            if (_select.HasProperty("ContactName"))
                _select.SetProperty("ContactName", _saQuote.ContactName);
            if (_select.HasProperty("TotalAmount"))
                _select.SetProperty("TotalAmount", _saQuote.TotalAmount);
            if (_select.HasProperty("TotalAmountOriginal"))
                _select.SetProperty("TotalAmountOriginal", _saQuote.TotalAmountOriginal);
            if (_select.HasProperty("TotalDiscountAmount"))
                _select.SetProperty("TotalDiscountAmount", _saQuote.TotalDiscountAmount);
            if (_select.HasProperty("TotalDiscountAmountOriginal"))
                _select.SetProperty("TotalDiscountAmountOriginal", _saQuote.TotalDiscountAmountOriginal);
            if (_select.HasProperty("TotalVATAmount"))
                _select.SetProperty("TotalVATAmount", _saQuote.TotalVATAmount);
            if (_select.HasProperty("TotalVATAmountOriginal"))
                _select.SetProperty("TotalVATAmountOriginal", _saQuote.TotalVATAmountOriginal);
            if (_select.HasProperty("CurrencyID"))
                _select.SetProperty("CurrencyID", _saQuote.CurrencyID);
            if (_select.HasProperty("ExchangeRate"))
                _select.SetProperty("ExchangeRate", _saQuote.ExchangeRate);
            Close();
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid _uGrid)
        {
            Utils.ConfigGrid(_uGrid, string.Empty, genTemplateColum(), isConfigGuiManually: true);
            _uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            _uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;

            foreach (var column in _uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(300, column, _uGrid);
                //if (column.Key.Contains("Amount"))
                //{
                //    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
                //    column.CellAppearance.TextHAlign = HAlign.Right;
                //    column.ConfigColumnByNumberic();
                //}
                //else if (column.Key.Contains("MaterialGoods"))
                //{
                //    this.ConfigCbbToGrid<SAInvoiceDetail, MaterialGoodsCustom>(_uGrid, column.Key, Utils.ListMaterialGoodsCustom, "ID", "MaterialGoodsCode", ConstDatabase.MaterialGoodsCustom_TableName);
                //}
                //else if (column.Key.Contains("Quantity"))
                //{
                //    column.MaxLength = 36;
                //    column.CellAppearance.TextHAlign = HAlign.Right;
                //    column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                //}
            }
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.VisiblePosition = 0;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        List<TemplateColumn> genTemplateColum()
        {
            Dictionary<int, int> dicVisiblePosition;
            List<string> strColumnName, strColumnCaption, strColumnToolTip;
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb;
            List<int> intColumnWidth,
                      intColumnMaxWidth,
                      intColumnMinWidth,
                      intVisiblePosition,
                      VTbolIsVisible,
                      VTbolIsVisibleCbb,
                      VTintVisiblePosition,
                      VTbolIsReadOnly;

            #region ObjSaQuoteDetail
            strColumnName = new List<string> { "Check", "ID", "SAQuoteID", "MaterialGoodsID", "UnitPrice", "UnitPriceOriginal", "UnitPriceConvert", "UnitPriceConvertOriginal", "Amount", "AmountOriginal", "VATAmount", "VATAmountOriginal", "DiscountAmount", "DiscountAmountOriginal", "UnitPriceAfterTax", "UnitPriceAfterTaxOriginal", "AmountAfterTax", "AmountAfterTaxOriginal", "DiscountAmountAfterTax", "DiscountAmountAfterTaxOriginal", "CustomProperty1", "CustomProperty2", "CustomProperty3", "OrderPriority", "DepartmentID", "ExpenseItemID", "BudgetItemID", "DiscountAccount", "AccountingObjectID", "CostSetID", "ContractID", "StatisticsCodeID", "ExpiryDate", "LotNo", "ConfrontID", "UnitConvert", "ConvertRate", "VATRate", "VATAccount", "CostAccount", "ReponsitoryAccount", "DiscountRate", "Description", "RepositoryID", "Unit", "Quantity", "QuantityConvert", };
            //"0-Check", "1-ID", "2-SAQuoteID", "3-MaterialGoodsID", "4-UnitPrice", "5-UnitPriceOriginal", "6-UnitPriceConvert", "7-UnitPriceConvertOriginal", "8-Amount", "9-AmountOriginal", "10-VATAmount", "11-VATAmountOriginal", "12-DiscountAmount", "13-DiscountAmountOriginal", "14-UnitPriceAfterTax", "15-UnitPriceAfterTaxOriginal", "16-AmountAfterTax", "17-AmountAfterTaxOriginal", "18-DiscountAmountAfterTax", "19-DiscountAmountAfterTaxOriginal", "20-CustomProperty1", "21-CustomProperty2", "22-CustomProperty3", "23-OrderPriority", "24-DepartmentID", "25-ExpenseItemID", "26-BudgetItemID", "27-DiscountAccount", "28-AccountingObjectID", "29-CostSetID", "30-ContractID", "31-StatisticsCodeID", "32-ExpiryDate", "33-LotNo", "34-ConfrontID", "35-UnitConvert", "36-ConvertRate", "37-VATRate", "38-VATAccount", "39-CostAccount", "40-ReponsitoryAccount", "41-DiscountRate", "42-Description", "43-RepositoryID", "44-Unit", "45-Quantity", "46-QuantityConvert",  
            strColumnCaption = strColumnToolTip = new List<string> { "", "ID", "SAQuoteID", "Mã hàng", "Đơn giá", "UnitPriceOriginal", "UnitPriceConvert", "UnitPriceConvertOriginal", "Thành tiền", "AmountOriginal", "VATAmount", "VATAmountOriginal", "DiscountAmount", "DiscountAmountOriginal", "UnitPriceAfterTax", "UnitPriceAfterTaxOriginal", "AmountAfterTax", "AmountAfterTaxOriginal", "DiscountAmountAfterTax", "DiscountAmountAfterTaxOriginal", "CustomProperty1", "CustomProperty2", "CustomProperty3", "OrderPriority", "DepartmentID", "ExpenseItemID", "BudgetItemID", "DiscountAccount", "AccountingObjectID", "CostSetID", "ContractID", "StatisticsCodeID", "ExpiryDate", "LotNo", "ConfrontID", "UnitConvert", "ConvertRate", "VATRate", "VATAccount", "CostAccount", "ReponsitoryAccount", "DiscountRate", "Diễn giải", "RepositoryID", "Unit", "Số lượng", "QuantityConvert" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 0, 3, 4, 8, 42, 45 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 1, 2 };
            VTintVisiblePosition = new List<int>() { 0, 1, 4, 5, 2, 3 };
            VTbolIsReadOnly = new List<int>() { 3, 4, 8, 42, 45 };

            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition 
            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); }

            for (int i = 0; i < strColumnName.Count; i++)
            {
                bool temp = VTbolIsVisible.Contains(i);
                bolIsVisible.Add(temp);
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);
            }
            #endregion

            return ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
        }
        #endregion

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            //chọn tất cả các hàng hóa cùng phiếu xuất

        }

        private void FchoseSaQuote_Load(object sender, EventArgs e)
        {
            this.BringToFront();
            SendKeys.Send("{TAB}");
        }
    }
}
