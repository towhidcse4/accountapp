﻿namespace Accounting
{
    partial class FchoseSaQuote<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.palFill = new System.Windows.Forms.Panel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palTop = new System.Windows.Forms.Panel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblListDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.palBottom = new System.Windows.Forms.Panel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.uGrid);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 43);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(688, 277);
            this.palFill.TabIndex = 1;
            // 
            // uGrid
            // 
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(688, 277);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.txtNo);
            this.palTop.Controls.Add(this.dteDate);
            this.palTop.Controls.Add(this.lblListDate);
            this.palTop.Controls.Add(this.ultraLabel1);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(688, 43);
            this.palTop.TabIndex = 0;
            // 
            // txtNo
            // 
            this.txtNo.AutoSize = false;
            this.txtNo.Location = new System.Drawing.Point(82, 11);
            this.txtNo.Name = "txtNo";
            this.txtNo.ReadOnly = true;
            this.txtNo.Size = new System.Drawing.Size(148, 22);
            this.txtNo.TabIndex = 82;
            // 
            // dteDate
            // 
            appearance11.TextHAlignAsString = "Center";
            appearance11.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance11;
            this.dteDate.AutoSize = false;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.Location = new System.Drawing.Point(343, 11);
            this.dteDate.MaskInput = "dd/mm/yyyy";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(109, 22);
            this.dteDate.TabIndex = 77;
            this.dteDate.Value = null;
            // 
            // lblListDate
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.lblListDate.Appearance = appearance12;
            this.lblListDate.Location = new System.Drawing.Point(262, 14);
            this.lblListDate.Name = "lblListDate";
            this.lblListDate.Size = new System.Drawing.Size(75, 16);
            this.lblListDate.TabIndex = 78;
            this.lblListDate.Text = "Ngày báo giá";
            // 
            // ultraLabel1
            // 
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance13;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(61, 18);
            this.ultraLabel1.TabIndex = 76;
            this.ultraLabel1.Text = "Số báo giá";
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.btnClose);
            this.palBottom.Controls.Add(this.btnSave);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 320);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(688, 47);
            this.palBottom.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance14.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance14;
            this.btnClose.Location = new System.Drawing.Point(600, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 73;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.BtnCloseClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance15.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnSave.Appearance = appearance15;
            this.btnSave.Location = new System.Drawing.Point(519, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // FchoseSaQuote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 367);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FchoseSaQuote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn VTHH từ báo giá";
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.Misc.UltraLabel lblListDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
    }
}