﻿namespace Accounting.Frm.Frm.FBusiness.FSAInvoice.FSAInvoice
{
    partial class UTopPhieuXuatKho
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.lblChungTuGoc = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtSContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChi = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHang = new Infragistics.Win.Misc.UltraLabel();
            this.BtnSelectSaInvoice = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(124, 45);
            this.txtCompanyTaxCode.Multiline = true;
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(156, 22);
            this.txtCompanyTaxCode.TabIndex = 53;
            // 
            // lblCompanyTaxCode
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyTaxCode.Appearance = appearance1;
            this.lblCompanyTaxCode.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(2, 47);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(115, 19);
            this.lblCompanyTaxCode.TabIndex = 52;
            this.lblCompanyTaxCode.Text = "Mã số thuế";
            // 
            // lblChungTuGoc
            // 
            this.lblChungTuGoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblChungTuGoc.Appearance = appearance2;
            this.lblChungTuGoc.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblChungTuGoc.Location = new System.Drawing.Point(775, 95);
            this.lblChungTuGoc.Name = "lblChungTuGoc";
            this.lblChungTuGoc.Size = new System.Drawing.Size(137, 19);
            this.lblChungTuGoc.TabIndex = 51;
            this.lblChungTuGoc.Text = "Chứng từ gốc";
            // 
            // txtOriginalNo
            // 
            this.txtOriginalNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginalNo.Location = new System.Drawing.Point(124, 93);
            this.txtOriginalNo.Name = "txtOriginalNo";
            this.txtOriginalNo.Size = new System.Drawing.Size(645, 21);
            this.txtOriginalNo.TabIndex = 50;
            // 
            // lblNumberAttach
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberAttach.Appearance = appearance3;
            this.lblNumberAttach.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblNumberAttach.Location = new System.Drawing.Point(2, 94);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(115, 19);
            this.lblNumberAttach.TabIndex = 49;
            this.lblNumberAttach.Text = "Kèm theo";
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.Location = new System.Drawing.Point(286, 1);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(483, 21);
            this.txtAccountingObjectName.TabIndex = 48;
            // 
            // cbbAccountingObjectID
            // 
            appearance4.Image = global::Accounting.Frm.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(124, 0);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectID.TabIndex = 47;
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.CbbAccountingObjectIdpxkRowSelected);
            this.cbbAccountingObjectID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.CbbAccountingObjectIdpxkEditorButtonClick);
            this.cbbAccountingObjectID.Leave += new System.EventHandler(this.CbbAccountingObjectIdpxkLeave);
            // 
            // txtSReason
            // 
            this.txtSReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSReason.Location = new System.Drawing.Point(124, 69);
            this.txtSReason.Name = "txtSReason";
            this.txtSReason.Size = new System.Drawing.Size(790, 21);
            this.txtSReason.TabIndex = 46;
            // 
            // lblSReason
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.lblSReason.Appearance = appearance5;
            this.lblSReason.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblSReason.Location = new System.Drawing.Point(2, 70);
            this.lblSReason.Name = "lblSReason";
            this.lblSReason.Size = new System.Drawing.Size(115, 19);
            this.lblSReason.TabIndex = 45;
            this.lblSReason.Text = "Lý do xuất";
            // 
            // txtSContactName
            // 
            this.txtSContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSContactName.Location = new System.Drawing.Point(397, 46);
            this.txtSContactName.Name = "txtSContactName";
            this.txtSContactName.Size = new System.Drawing.Size(517, 21);
            this.txtSContactName.TabIndex = 44;
            // 
            // lblSContactName
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.lblSContactName.Appearance = appearance6;
            this.lblSContactName.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblSContactName.Location = new System.Drawing.Point(289, 47);
            this.lblSContactName.Name = "lblSContactName";
            this.lblSContactName.Size = new System.Drawing.Size(102, 19);
            this.lblSContactName.TabIndex = 43;
            this.lblSContactName.Text = "Họ tên người nhận";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(124, 23);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(790, 21);
            this.txtAccountingObjectAddress.TabIndex = 42;
            // 
            // lblDiaChi
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.lblDiaChi.Appearance = appearance7;
            this.lblDiaChi.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblDiaChi.Location = new System.Drawing.Point(2, 25);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(115, 19);
            this.lblDiaChi.TabIndex = 41;
            this.lblDiaChi.Text = "Địa chỉ";
            // 
            // lblKhachHang
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.lblKhachHang.Appearance = appearance8;
            this.lblKhachHang.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblKhachHang.Location = new System.Drawing.Point(2, 3);
            this.lblKhachHang.Name = "lblKhachHang";
            this.lblKhachHang.Size = new System.Drawing.Size(115, 19);
            this.lblKhachHang.TabIndex = 40;
            this.lblKhachHang.Text = "Khách hàng";
            // 
            // BtnSelectSaInvoice
            // 
            this.BtnSelectSaInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSelectSaInvoice.Location = new System.Drawing.Point(772, 1);
            this.BtnSelectSaInvoice.Name = "BtnSelectSaInvoice";
            this.BtnSelectSaInvoice.Size = new System.Drawing.Size(140, 22);
            this.BtnSelectSaInvoice.TabIndex = 54;
            this.BtnSelectSaInvoice.Text = "Chọn chứng từ bán hàng";
            this.BtnSelectSaInvoice.Click += new System.EventHandler(this.BtnSelectSaInvoice_Click);
            // 
            // UTopPhieuXuatKho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnSelectSaInvoice);
            this.Controls.Add(this.txtCompanyTaxCode);
            this.Controls.Add(this.lblCompanyTaxCode);
            this.Controls.Add(this.lblChungTuGoc);
            this.Controls.Add(this.txtOriginalNo);
            this.Controls.Add(this.lblNumberAttach);
            this.Controls.Add(this.txtAccountingObjectName);
            this.Controls.Add(this.cbbAccountingObjectID);
            this.Controls.Add(this.txtSReason);
            this.Controls.Add(this.lblSReason);
            this.Controls.Add(this.txtSContactName);
            this.Controls.Add(this.lblSContactName);
            this.Controls.Add(this.txtAccountingObjectAddress);
            this.Controls.Add(this.lblDiaChi);
            this.Controls.Add(this.lblKhachHang);
            this.Name = "UTopPhieuXuatKho";
            this.Size = new System.Drawing.Size(915, 115);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGoc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNo;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSReason;
        private Infragistics.Win.Misc.UltraLabel lblSReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactName;
        private Infragistics.Win.Misc.UltraLabel lblSContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblDiaChi;
        private Infragistics.Win.Misc.UltraLabel lblKhachHang;
        private Infragistics.Win.Misc.UltraButton BtnSelectSaInvoice;
    }
}
