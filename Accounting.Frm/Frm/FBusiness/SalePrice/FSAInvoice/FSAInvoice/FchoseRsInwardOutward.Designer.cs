﻿namespace Accounting
{
    partial class FchoseRsInwardOutward
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FchoseRsInwardOutward));
            this.palFill = new System.Windows.Forms.Panel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palTop = new System.Windows.Forms.Panel();
            this.cbbSelectTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.dteFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblListDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnGetData = new Infragistics.Win.Misc.UltraButton();
            this.palBottom = new System.Windows.Forms.Panel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSelectTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).BeginInit();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.uGrid);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 43);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(693, 277);
            this.palFill.TabIndex = 1;
            // 
            // uGrid
            // 
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(693, 277);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.cbbSelectTime);
            this.palTop.Controls.Add(this.dteTo);
            this.palTop.Controls.Add(this.ultraLabel2);
            this.palTop.Controls.Add(this.dteFrom);
            this.palTop.Controls.Add(this.lblListDate);
            this.palTop.Controls.Add(this.ultraLabel1);
            this.palTop.Controls.Add(this.btnGetData);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(693, 43);
            this.palTop.TabIndex = 0;
            // 
            // cbbSelectTime
            // 
            this.cbbSelectTime.AutoSize = false;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbSelectTime.DisplayLayout.Appearance = appearance1;
            this.cbbSelectTime.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbSelectTime.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSelectTime.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSelectTime.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cbbSelectTime.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbSelectTime.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cbbSelectTime.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbSelectTime.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbSelectTime.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbSelectTime.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cbbSelectTime.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbSelectTime.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cbbSelectTime.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbSelectTime.DisplayLayout.Override.CellAppearance = appearance8;
            this.cbbSelectTime.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbSelectTime.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbSelectTime.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cbbSelectTime.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cbbSelectTime.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbSelectTime.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cbbSelectTime.DisplayLayout.Override.RowAppearance = appearance11;
            this.cbbSelectTime.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbSelectTime.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cbbSelectTime.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbSelectTime.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbSelectTime.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbSelectTime.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbSelectTime.Location = new System.Drawing.Point(97, 10);
            this.cbbSelectTime.Name = "cbbSelectTime";
            this.cbbSelectTime.Size = new System.Drawing.Size(181, 22);
            this.cbbSelectTime.TabIndex = 81;
            // 
            // dteTo
            // 
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.dteTo.Appearance = appearance13;
            this.dteTo.AutoSize = false;
            this.dteTo.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteTo.FormatString = "";
            this.dteTo.Location = new System.Drawing.Point(467, 10);
            this.dteTo.MaskInput = "dd/mm/yyyy";
            this.dteTo.Name = "dteTo";
            this.dteTo.Size = new System.Drawing.Size(109, 22);
            this.dteTo.TabIndex = 79;
            this.dteTo.Value = null;
            // 
            // ultraLabel2
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance14;
            this.ultraLabel2.Location = new System.Drawing.Point(433, 13);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(28, 16);
            this.ultraLabel2.TabIndex = 80;
            this.ultraLabel2.Text = "Đến";
            // 
            // dteFrom
            // 
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            this.dteFrom.Appearance = appearance15;
            this.dteFrom.AutoSize = false;
            this.dteFrom.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteFrom.FormatString = "";
            this.dteFrom.Location = new System.Drawing.Point(313, 11);
            this.dteFrom.MaskInput = "dd/mm/yyyy";
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Size = new System.Drawing.Size(109, 22);
            this.dteFrom.TabIndex = 77;
            this.dteFrom.Value = null;
            // 
            // lblListDate
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.lblListDate.Appearance = appearance16;
            this.lblListDate.Location = new System.Drawing.Point(284, 14);
            this.lblListDate.Name = "lblListDate";
            this.lblListDate.Size = new System.Drawing.Size(28, 16);
            this.lblListDate.TabIndex = 78;
            this.lblListDate.Text = "Từ";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(12, 13);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(79, 18);
            this.ultraLabel1.TabIndex = 76;
            this.ultraLabel1.Text = "Chọn thời gian";
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(594, 10);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(88, 22);
            this.btnGetData.TabIndex = 74;
            this.btnGetData.Text = "Lấy số liệu";
            this.btnGetData.Click += new System.EventHandler(this.BtnLaySoLieuClick);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.btnClose);
            this.palBottom.Controls.Add(this.btnSave);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 320);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(693, 47);
            this.palBottom.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance17.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance17;
            this.btnClose.Location = new System.Drawing.Point(606, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 73;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.BtnCloseClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance18.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnSave.Appearance = appearance18;
            this.btnSave.Location = new System.Drawing.Point(525, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // FchoseRsInwardOutward
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 367);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FchoseRsInwardOutward";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách phiếu xuất kho";
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbSelectTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom)).EndInit();
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnGetData;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteFrom;
        private Infragistics.Win.Misc.UltraLabel lblListDate;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSelectTime;
    }
}