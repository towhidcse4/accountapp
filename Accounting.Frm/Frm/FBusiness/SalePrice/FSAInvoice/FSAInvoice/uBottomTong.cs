﻿using System.Windows.Forms;

namespace Accounting.Frm.Frm.FBusiness.FSAInvoice.FSAInvoice
{
    public partial class UBottomTong : UserControl
    {
        public UBottomTong()
        {
            InitializeComponent();
        }

        public string TotalAmount
        {
            get { return txtTotalAmount.Text; }
            set { txtTotalAmount.Text = value; }
        }
        public string TotalDiscountAmount
        {
            get { return txtTotalDiscountAmount.Text; }
            set { txtTotalDiscountAmount.Text = value; }
        }
        public string TotalVatAmount
        {
            get { return txtTotalVATAmount.Text; }
            set { txtTotalVATAmount.Text = value; }
        }
        public string TotalPaymentAmount
        {
            get { return txtTotalPaymentAmount.Text; }
            set { txtTotalPaymentAmount.Text = value; }
        }

        public string TotalAmountOriginal
        {
            get { return txtTotalAmountOriginal.Text; }
            set { txtTotalAmountOriginal.Text = value; }
        }
        public string TotalDiscountAmountOriginal
        {
            get { return txtTotalDiscountAmountOriginal.Text; }
            set { txtTotalDiscountAmountOriginal.Text = value; }
        }
        public string TotalVatAmountOriginal
        {
            get { return txtTotalVATAmountOriginal.Text; }
            set { txtTotalVATAmountOriginal.Text = value; }
        }
        public string TotalPaymentAmountOriginal
        {
            get { return txtTotalPaymentAmountOriginal.Text; }
            set { txtTotalPaymentAmountOriginal.Text = value; }
        }
    }
}
