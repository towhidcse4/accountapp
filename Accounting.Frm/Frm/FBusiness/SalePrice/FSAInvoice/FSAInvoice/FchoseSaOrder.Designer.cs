﻿namespace Accounting
{
    partial class FchoseSaOrder<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.palFill = new System.Windows.Forms.Panel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBottom = new System.Windows.Forms.Panel();
            this.btnViewSaOrder = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.uGrid);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 0);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(889, 320);
            this.palFill.TabIndex = 1;
            // 
            // uGrid
            // 
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(889, 320);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            this.uGrid.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid_ClickCell);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.btnViewSaOrder);
            this.palBottom.Controls.Add(this.btnClose);
            this.palBottom.Controls.Add(this.btnSave);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 320);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(889, 47);
            this.palBottom.TabIndex = 2;
            // 
            // btnViewSaOrder
            // 
            this.btnViewSaOrder.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance1.Image = global::Accounting.Properties.Resources.btnsearch;
            this.btnViewSaOrder.Appearance = appearance1;
            this.btnViewSaOrder.Location = new System.Drawing.Point(579, 8);
            this.btnViewSaOrder.Name = "btnViewSaOrder";
            this.btnViewSaOrder.Size = new System.Drawing.Size(135, 30);
            this.btnViewSaOrder.TabIndex = 74;
            this.btnViewSaOrder.Text = "Xem đơn đặt hàng";
            this.btnViewSaOrder.Visible = false;
            this.btnViewSaOrder.Click += new System.EventHandler(this.btnViewSaOrder_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance2.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance2;
            this.btnClose.Location = new System.Drawing.Point(801, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 73;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.BtnCloseClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance3.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnSave.Appearance = appearance3;
            this.btnSave.Location = new System.Drawing.Point(720, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // FchoseSaOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 367);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FchoseSaOrder";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đơn đặt hàng";
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraButton btnViewSaOrder;
    }
}