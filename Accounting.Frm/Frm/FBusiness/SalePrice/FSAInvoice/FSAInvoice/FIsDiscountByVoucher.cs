﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;

namespace Accounting
{
    public partial class FIsDiscountByVoucher : DialogForm
    {
        #region Khai báo
        private BindingList<IList> _input;
        private List<ObjSaInvoiceDetail> _lstSaInvoice;
        private bool _isCalculatorView = true;
        private decimal _exRate = 1;
        #endregion

        public FIsDiscountByVoucher(BindingList<IList> input, decimal exRate, string currency)
        {
            InitializeComponent();
            txtTongChietKhau.Appearance.TextHAlign = HAlign.Right;

            _input = input;
            _exRate = exRate;
            if (_input == null || _input.Count == 0) return;

            _lstSaInvoice = new List<ObjSaInvoiceDetail>();
            decimal totalDiscountAmount = (_input[0].Cast<SAInvoiceDetail>()).Sum(k => k.DiscountAmount);
            foreach (SAInvoiceDetail item in _input[0])
            {
                ObjSaInvoiceDetail temp = new ObjSaInvoiceDetail();
                ReflectionUtils.Copy(typeof(SAInvoiceDetail), item, typeof(ObjSaInvoiceDetail), temp);
                temp.Check = false;
                temp.DiscountRate = totalDiscountAmount == 0 ? 0 : (temp.DiscountAmount / totalDiscountAmount * 100);
                _lstSaInvoice.Add(temp);
            }
            uGrid.DataSource = _lstSaInvoice;

            Utils.ConfigGrid(uGrid, string.Empty, genTemplateColum(), isConfigGuiManually: true);
            uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;

            foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                //if (column.Key.Contains("Amount"))
                //{
                //    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DoubleNonNegative;
                //    column.CellAppearance.TextHAlign = HAlign.Right;
                //    column.ConfigColumnByNumberic();
                //}
                //else if (column.Key.Contains("MaterialGoods"))
                //{
                //    this.ConfigCbbToGrid<SAInvoiceDetail, MaterialGoodsCustom>(uGrid, column.Key, Utils.ListMaterialGoodsCustom, "ID", "MaterialGoodsCode", ConstDatabase.MaterialGoodsCustom_TableName);
                //}
                //else if (column.Key.Contains("DiscountRate"))
                //{
                //    column.MaxLength = 36;
                //    column.CellAppearance.TextHAlign = HAlign.Right;
                //}
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.ConfigSummaryOfGrid();
            SummarySettings summary = uGrid.DisplayLayout.Bands[0].Summaries.Add("SumDiscountRate", SummaryType.Sum, uGrid.DisplayLayout.Bands[0].Columns["DiscountRate"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:" + Utils.GetTypeFormatNumberic(ConstDatabase.Format_Coefficient) + "}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            summary.Appearance.TextHAlign = HAlign.Right;
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            txtTongChietKhau.FormatNumberic(currency.Equals("VND") ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
        }

        #region Event
        private void BtnCloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            _isCalculatorView = false;
            //btnPhanBo.PerformClick();
            uGrid.UpdateData();
            decimal tck;
            decimal.TryParse(txtTongChietKhau.Value.ToString(), out tck);
            if (_lstSaInvoice.Where(x => x.Check).Sum(x => x.DiscountAmount) != tck)
            {
                MSG.Warning(resSystem.MSG_System_67);
                return;
            }
            for (int i = 0; i < _lstSaInvoice.Count; i++)
            {
                if (!_lstSaInvoice[i].Check) continue;
                (_input[0][i] as SAInvoiceDetail).DiscountRate = _lstSaInvoice[i].Amount == 0 ? 0 : (_lstSaInvoice[i].DiscountAmount / _lstSaInvoice[i].Amount * 100);
                (_input[0][i] as SAInvoiceDetail).DiscountAmount = (_input[0][i] as SAInvoiceDetail).DiscountRate == 0 ? 0 : _lstSaInvoice[i].DiscountAmount;
                (_input[0][i] as SAInvoiceDetail).DiscountAmountOriginal = (_input[0][i] as SAInvoiceDetail).DiscountRate == 0 ? 0 : _lstSaInvoice[i].DiscountAmountOriginal;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void BtnPhanBoClick(object sender, EventArgs e)
        {
            if (_input == null || _input.Count == 0) return;
            uGrid.UpdateData();
            //lấy những cái được chọn và tính phân bổ
            decimal sumSl = _lstSaInvoice.Where(k => k.Check).Sum(k => k.Quantity ?? 0);
            decimal sumGt = _lstSaInvoice.Where(k => k.Check).Sum(k => k.Amount);
            bool isCheckSl = optPhanBo.CheckedItem.Tag.Equals("SL");
            decimal tck;
            decimal.TryParse(txtTongChietKhau.Value.ToString(), out tck);
            foreach (var row in uGrid.Rows)
            {
                if (!((bool)row.Cells["Check"].Value)) continue;
                var saInvoiceDetailView = row.ListObject as SAInvoiceDetail;
                decimal quantity = saInvoiceDetailView.Quantity ?? 0;
                decimal tilephanbo = isCheckSl ? (quantity / sumSl) : (saInvoiceDetailView.Amount / sumGt);
                row.Cells["DiscountRate"].Value = tilephanbo * 100;
                row.Cells["DiscountAmount"].Value = tck * tilephanbo;
                row.Cells["DiscountAmountOriginal"].Value = saInvoiceDetailView.DiscountAmount / _exRate;
                //if (_isCalculatorView) continue;
                //(_input[0][i] as SAInvoiceDetail).DiscountRate = tilephanbo;
                //(_input[0][i] as SAInvoiceDetail).DiscountAmount = tck * tilephanbo;
            }
        }

        private void uGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            if (uGrid.ActiveCell == null) return;
            var cell = uGrid.ActiveCell;
            if (uGrid.DisplayLayout.Bands[0].Columns.Exists("DiscountRate"))
            {
                if (cell.Column.Key.Equals("DiscountRate") || cell.Column.Key.Equals("DiscountAmountOriginal") ||
                    cell.Column.Key.Equals("AmountOriginal"))
                {
                    decimal rate = Utils.GetValueFromText(cell.Row.Cells["DiscountRate"]);
                    if (rate > 100)
                    {
                        uGrid.NotificationCell(cell.Row.Cells["DiscountRate"], resSystem.MSG_System_66);
                    }
                    else
                    {
                        uGrid.RemoveNotificationCell(cell.Row.Cells["DiscountRate"]);
                        decimal tck;
                        decimal.TryParse(txtTongChietKhau.Value.ToString(), out tck);
                        cell.Row.Cells["DiscountAmount"].Value = tck * rate / 100;
                        cell.Row.Cells["DiscountAmountOriginal"].Value = tck * rate / 100 / _exRate;
                    }
                }
            }
        }
        #endregion

        #region Utils
        List<TemplateColumn> genTemplateColum()
        {
            Dictionary<int, int> dicVisiblePosition;
            List<string> strColumnName, strColumnCaption, strColumnToolTip;
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb;
            List<int> intColumnWidth,
                      intColumnMaxWidth,
                      intColumnMinWidth,
                      intVisiblePosition,
                      VTbolIsVisible,
                      VTbolIsVisibleCbb,
                      VTintVisiblePosition,
                      VTbolIsReadOnly;

            #region ObjSaInvoiceDetail
            strColumnName = new List<string> { "Check", "ID", "SAInvoiceID", "MaterialGoodsID", "UnitPrice", "UnitPriceOriginal", "UnitPriceConvert", "UnitPriceConvertOriginal", "Amount", "AmountOriginal", "DiscountAmount", "DiscountAmountOriginal", "OWPrice", "OWPriceOriginal", "OWAmount", "OWAmountOriginal", "VATAmount", "VATAmountOriginal", "SpecialConsumeTaxAmount", "SpecialConsumeTaxAmountOriginal", "SpecialConsumeUnitPrice", "SpecialConsumeUnitPriceOriginal", "UnitPriceAfterTax", "UnitPriceAfterTaxOriginal", "AmountAfterTax", "AmountAfterTaxOriginal", "DiscountAmountAfterTax", "DiscountAmountAfterTaxOriginal", "ContractDetailID", "IsPromotion", "CustomProperty1", "CustomProperty2", "CustomProperty3", "OrderPriority", "DepartmentID", "ExpenseItemID", "BudgetItemID", "CreditAccountingObjectID", "ConfrontDetailID", "SAOrderID", "SAOrderNo", "ConvertRate", "VATAccount", "RepositoryAccount", "CostAccount", "ConfrontID", "ExpiryDate", "LotNo", "Waranty", "AccountingObjectID", "CostSetID", "ContractID", "StatisticsCodeID", "UnitConvert", "SpecialConsumeTaxRate", "DiscountAccount", "VATRate", "DiscountRate", "RepositoryID", "Description", "DebitAccount", "CreditAccount", "Unit", "Quantity", "QuantityConvert", "VATDescription", "OWPriceconvert", "OWPriceconvertOriginal", "OutwardAmount", "MaterialGood", };
            //"0-Check", "1-ID", "2-SAInvoiceID", "3-MaterialGoodsID", "4-UnitPrice", "5-UnitPriceOriginal", "6-UnitPriceConvert", "7-UnitPriceConvertOriginal", "8-Amount", "9-AmountOriginal", "10-DiscountAmount", "11-DiscountAmountOriginal", "12-OWPrice", "13-OWPriceOriginal", "14-OWAmount", "15-OWAmountOriginal", "16-VATAmount", "17-VATAmountOriginal", "18-SpecialConsumeTaxAmount", "19-SpecialConsumeTaxAmountOriginal", "20-SpecialConsumeUnitPrice", "21-SpecialConsumeUnitPriceOriginal", "22-UnitPriceAfterTax", "23-UnitPriceAfterTaxOriginal", "24-AmountAfterTax", "25-AmountAfterTaxOriginal", "26-DiscountAmountAfterTax", "27-DiscountAmountAfterTaxOriginal", "28-ContractDetailID", "29-IsPromotion", "30-CustomProperty1", "31-CustomProperty2", "32-CustomProperty3", "33-OrderPriority", "34-DepartmentID", "35-ExpenseItemID", "36-BudgetItemID", "37-CreditAccountingObjectID", "38-ConfrontDetailID", "39-SAOrderID", "40-SAOrderNo", "41-ConvertRate", "42-VATAccount", "43-RepositoryAccount", "44-CostAccount", "45-ConfrontID", "46-ExpiryDate", "47-LotNo", "48-Waranty", "49-AccountingObjectID", "50-CostSetID", "51-ContractID", "52-StatisticsCodeID", "53-UnitConvert", "54-SpecialConsumeTaxRate", "55-DiscountAccount", "56-VATRate", "57-DiscountRate", "58-RepositoryID", "59-Description", "60-DebitAccount", "61-CreditAccount", "62-Unit", "63-Quantity", "64-QuantityConvert", "65-VATDescription", "66-OWPriceconvert", "67-OWPriceconvertOriginal", "68-OutwardAmount", "69-MaterialGood",  
            strColumnCaption = strColumnToolTip = new List<string> { "", "ID", "SAInvoiceID", "Mã hàng", "UnitPrice", "UnitPriceOriginal", "UnitPriceConvert", "UnitPriceConvertOriginal", "Thành tiền", "AmountOriginal", "Tiền chiết khấu", "DiscountAmountOriginal", "OWPrice", "OWPriceOriginal", "OWAmount", "OWAmountOriginal", "VATAmount", "VATAmountOriginal", "Tiền thuế TTĐB QĐ", "SpecialConsumeTaxAmountOriginal", "SpecialConsumeUnitPrice", "SpecialConsumeUnitPriceOriginal", "UnitPriceAfterTax", "UnitPriceAfterTaxOriginal", "AmountAfterTax", "AmountAfterTaxOriginal", "DiscountAmountAfterTax", "DiscountAmountAfterTaxOriginal", "ContractDetailID", "IsPromotion", "CustomProperty1", "CustomProperty2", "CustomProperty3", "OrderPriority", "DepartmentID", "ExpenseItemID", "BudgetItemID", "CreditAccountingObjectID", "ConfrontDetailID", "SAOrderID", "SAOrderNo", "ConvertRate", "VATAccount", "RepositoryAccount", "CostAccount", "ConfrontID", "ExpiryDate", "LotNo", "Waranty", "AccountingObjectID", "CostSetID", "ContractID", "StatisticsCodeID", "UnitConvert", "SpecialConsumeTaxRate", "DiscountAccount", "VATRate", "Tỷ lệ phân bổ (%)", "RepositoryID", "Diễn giải", "DebitAccount", "CreditAccount", "Unit", "Số lượng", "QuantityConvert", "VATDescription", "OWPriceconvert", "OWPriceconvertOriginal", "Giá trị xuất kho", "MaterialGood" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 0, 3, 8, 59, 63, 57, 10 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 1, 2 };
            VTintVisiblePosition = new List<int>() { 0, 1, 4, 2, 3, 5, 6 };
            VTbolIsReadOnly = new List<int>() { 3, 8, 59, 63 };

            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition 
            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); }

            for (int i = 0; i < strColumnName.Count; i++)
            {
                bool temp = VTbolIsVisible.Contains(i);
                bolIsVisible.Add(temp);
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);
            }
            #endregion

            return ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
        }
        #endregion
    }
}
