﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;

namespace Accounting
{
    public partial class FchoseSaOrder<T> : DialogForm
    {
        #region Khai báonote
        private readonly BindingList<IList> _input;
        public List<ObjSaOrderDetail> _lstSaOrderDetail { get; private set; }
        private SAInvoice _select = new SAInvoice();
        private FSAInvoiceDetail frm;
        #endregion

        public FchoseSaOrder(BindingList<IList> input, T @select, Guid? accountingObjectId, DateTime date, FSAInvoiceDetail f, List<SAInvoiceDetail> detail1)
        {
            InitializeComponent();
            _select = @select as SAInvoice;
            frm = f;
            _lstSaOrderDetail = new List<ObjSaOrderDetail>();
            if (accountingObjectId.HasValue)
            {
                var result = Utils.ISAOrderService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.Date <= date).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
                Utils.ISAOrderService.UnbindSession(result);
                result = Utils.ISAOrderService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.Date <= date).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
                foreach (var saOrder in result)
                {
                    foreach (var saOrderDetail in saOrder.SAOrderDetails.Where(x => ((x.Quantity ?? 0) - (x.QuantityReceipt ?? 0)) > 0))
                    {
                        var selectOrder = new ObjSaOrderDetail
                        {
                            ID = saOrder.ID,
                            No = saOrder.No,
                            Date = saOrder.Date,
                            Reason = saOrder.Reason,
                            MaterialGoodsID = saOrderDetail.MaterialGoodsID,
                            MaterialGoodsName = saOrderDetail.Description, // lấy tên hàng theo tên hàng nhập từ SAOrder
                            QuantityStore = (saOrderDetail.Quantity ?? 0) - (saOrderDetail.QuantityReceipt ?? 0),
                            QuantityExport = (saOrderDetail.Quantity ?? 0) - (saOrderDetail.QuantityReceipt ?? 0),
                            UnitPrice = saOrderDetail.UnitPrice,
                            Amount = saOrderDetail.Amount,
                            AccountingObjectID = saOrder.AccountingObjectID,
                            CurrencyID = saOrder.CurrencyID,
                            ExchangeRate = saOrder.ExchangeRate,
                            SAOrderDetails = saOrderDetail,
                            
                        };
                        var materialGoodsCustom = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == saOrderDetail.MaterialGoodsID);
                        if (materialGoodsCustom != null)
                        {
                            //selectOrder.MaterialGoodsName = materialGoodsCustom.MaterialGoodsName;
                            selectOrder.MaterialGoodsCode = materialGoodsCustom.MaterialGoodsCode;
                        }                           
                        var saInvoice = _select as SAInvoice;
                        if (saInvoice != null && detail1 != null && detail1.Count > 0)
                        {
                            var selected = detail1.FirstOrDefault(x => x.SAOrderID == selectOrder.ID && x.MaterialGoodsID == selectOrder.MaterialGoodsID);
                            if (selected != null)
                            {
                                selectOrder.Check = true;
                                selectOrder.QuantityExport = selected.Quantity;
                            }
                        }
                        _lstSaOrderDetail.Add(selectOrder);
                    }
                }                
            }
            uGrid.DataSource = _lstSaOrderDetail.OrderBy(n => n.SAOrderDetails.OrderPriority).ToList(); // tungnt: sắp xếp thứ tự trên grid
            ConfigGrid(uGrid);

            _input = input;
        }

        #region Event
        private void BtnCloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            //uGrid.UpdateData();

            ////lấy những cái được chọn và convert sang SainVoidDetails rồi import vào Grid bên dưới
            //List<ObjSaOrderDetail> lstSelect = _lstSaOrderDetail.Where(k => k.Check).ToList();
            //if (lstSelect.Count == 0)
            //{
            //    MSG.Warning(resSystem.MSG_Error_20);
            //    return;
            //}
            ////Convert sang sang SainVoidDetails
            //List<SAInvoiceDetail> lstKq = new List<SAInvoiceDetail>();
            //foreach (ObjSaOrderDetail objSaOrderDetail in lstSelect)
            //{
            //    SAInvoiceDetail temp = new SAInvoiceDetail();
            //    ReflectionUtils.Copy(typeof(SAOrderDetail), objSaOrderDetail, typeof(SAInvoiceDetail), temp, new List<string> { "Quantity" });
            //    temp.ID = Guid.Empty;
            //    temp.Quantity = objSaOrderDetail.QuantityExport;
            //    temp.DetailID = objSaOrderDetail.ID;
            //    temp.SAOrderNo = objSaOrderDetail.No;
            //    temp.SAOrderID = objSaOrderDetail.SAOrderID;
            //    temp.MaterialGoodsID = objSaOrderDetail.MaterialGoodsID;
            //    if (objSaOrderDetail.QuantityExport != objSaOrderDetail.Quantity)
            //    {
            //        temp.Amount = (temp.Quantity ?? 0) * temp.UnitPrice;
            //        temp.AmountOriginal = (temp.Quantity ?? 0) * temp.UnitPriceOriginal;
            //    }
            //    lstKq.Add(temp);
            //}
            ////Import vào cái Grid bên dưới (nếu có hàng hóa sẵn thì có thể chèn thêm vào hoặc có thể xóa trắng rồi chèn mỗi các cái đang chọn)
            //if (lstKq.Count > 0) _input[0].Clear();
            //foreach (SAInvoiceDetail saInvoiceDetail in lstKq)
            //    _input[0].Add(saInvoiceDetail);
            //var grpCurrency = (from u in lstSelect group u by u.CurrencyID into g select new { CurrencyID = g.Key, ExchangeRate = g.Select(p => p.ExchangeRate) });
            //if (grpCurrency.Count() == 1)
            //{
            //    var f = grpCurrency.FirstOrDefault();
            //    if (f != null)
            //    {
            //        _select.CurrencyID = f.CurrencyID;
            //        var currency = Utils.ListCurrency.FirstOrDefault(c => c.ID == f.CurrencyID);
            //        var grpEx = f.ExchangeRate.GroupBy(x => x).Select(x => x.Key).ToList();
            //        decimal? exrate = grpEx.Count == 1 ? grpEx.FirstOrDefault() : (decimal?)null;
            //        frm.AutoExchangeByCurrencySelected(currency, exrate);
            //        if (frm.uGridControl != null && frm.uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
            //        {
            //            frm.ConfigGridCurrencyByCurrency(frm.uGridControl, currency.ID, exrate ?? 1);
            //        }
            //    }
            //}
            //var grpID = lstSelect.GroupBy(x => x.SAOrderID).ToList();
            //if (grpID.Count == 1)
            //{
            //    _select.EmployeeID = grpID[0].FirstOrDefault().EmployeeID;
            //    _select.PaymentClauseID = grpID[0].FirstOrDefault().PaymentClauseID;
            //}
            //Close();
            var lstSAOrder = from u in _lstSaOrderDetail where u.Check select u;
            if (lstSAOrder.Count() == 0)
            {
                MSG.Warning(resSystem.MSG_Error_19);
                return;
            }
            //SelectOrders = lstSAOrder.ToList();
            _lstSaOrderDetail = lstSAOrder.ToList();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            //chọn tất cả các hàng hóa cùng đơn đặt hàng
            if (e.Cell.Column.Key == "Check")
            {
                var gid = (Guid?)e.Cell.Row.Cells["ID"].Value;
                if (gid.HasValue)
                {
                    e.Cell.Row.Cells["Check"].Value = !(bool)e.Cell.Value;
                }
                uGrid.UpdateDataGrid();
            }
        }

        private void btnViewSaOrder_Click(object sender, EventArgs e)
        {
            ViewSaOrder();
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            // edit by tungnt
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                ObjSaOrderDetail temp = (ObjSaOrderDetail)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                var ppOder = Utils.ISAOrderService.Query.FirstOrDefault(x => x.ID == temp.ID);
                var f = new FSAOrderDetail(ppOder, new List<SAOrder>() { ppOder }, ConstFrm.optStatusForm.View);
                f.ShowDialog(this);
            }
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid _uGrid)
        {
            Utils.ConfigGrid(_uGrid, string.Empty, genTemplateColum(), isConfigGuiManually: true, isHaveSum: false, setReadOnlyColor: false);
            _uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            _uGrid.DisplayLayout.UseFixedHeaders = false;
            _uGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;

            foreach (var column in _uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.VisiblePosition = 0;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        List<TemplateColumn> genTemplateColum()
        {
            Dictionary<int, int> dicVisiblePosition;
            List<string> strColumnName, strColumnCaption, strColumnToolTip;
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb;
            List<int> intColumnWidth,
                      intColumnMaxWidth,
                      intColumnMinWidth,
                      intVisiblePosition,
                      VTbolIsVisible,
                      VTbolIsVisibleCbb,
                      VTintVisiblePosition,
                      VTbolIsReadOnly;

            #region ObjSaOrderDetail
            strColumnName = new List<string> { "Check", "Date", "No", "MaterialGoodsCode", "MaterialGoodsName", "QuantityStore", "QuantityExport", "ID", "MaterialGoodsID", "UnitPrice", "Amount", "AccountingObjectID", "Reason", "CurrencyID", "ExchangeRate" };
            //"0-Check", "1-Date", "2-No", "3-MaterialGoodsCode", "4-MaterialGoodsName", "5-QuantityStore", "6-QuantityExport", "7-ID", "8-SAOrderID", "9-MaterialGoodsID", "10-UnitPrice", "11-UnitPriceOriginal", "12-UnitPriceConvert", "13-UnitPriceConvertOriginal", "14-Amount", "15-AmountOriginal", "16-VATAmount", "17-VATAmountOriginal", "18-DiscountAmount", "19-DiscountAmountOriginal", "20-UnitPriceAfterTax", "21-UnitPriceAfterTaxOriginal", "22-DiscountAmountAfterTax", "23-DiscountAmountAfterTaxOriginal", "24-DepartmentID", "25-ExpenseItemID", "26-BudgetItemID", "27-CustomProperty1", "28-CustomProperty2", "29-CustomProperty3", "30-OrderPriority", "31-DiscountAccount", "32-AccountingObjectID", "33-CostSetID", "34-ContractID", "35-StatisticsCodeID", "36-ConfrontID", "37-ExpiryDate", "38-LotNo", "39-UnitConvert", "40-ConvertRate", "41-CostAccount", "42-RepositoryAccount", "43-DiscountRate", "44-VATRate", "45-Description", "46-RepositoryID", "47-DebitAccount", "48-CreditAccount", "49-Unit", "50-Quantity", "51-QuantityConvert", "52-QuantityReceipt", "53-QuantityReceiptConvert", "54-VATAccount", "55-AmountAfterTaxOriginal", "56-AmountAfterTax",  
            strColumnCaption = strColumnToolTip = new List<string> { "", "Ngày đơn hàng", "Số đơn hàng", "Mã hàng", "Tên hàng", "Số lượng còn lại", "Số lượng xuất", "ID", "MaterialGoodsID", "Đơn giá", "Thành tiền", "AccountingObjectID", "Diễn giải", "CurrencyID", "ExchangeRate" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 0, 1, 2, 3, 4, 5, 6, 9, 10 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 1, 2 };
            VTintVisiblePosition = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8};
            VTbolIsReadOnly = new List<int>() { 1, 2, 3, 4, 5, 9, 10 };

            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition 
            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); }

            for (int i = 0; i < strColumnName.Count; i++)
            {
                bool temp = VTbolIsVisible.Contains(i);
                bolIsVisible.Add(temp);
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(100);
                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);
            }
            #endregion

            return ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
        }

        private void ViewSaOrder()
        {
            //try
            //{
            //    if (uGrid.ActiveRow == null) return;
            //    var selectOrder = uGrid.ActiveRow.ListObject as ObjSaOrderDetail;
            //    if (selectOrder == null) return;
            //    var saOder = Utils.ISAOrderService.Query.FirstOrDefault(x => x.ID == selectOrder.ID);
            //    if (saOder == null)
            //    {
            //        MSG.Warning(resSystem.MSG_Error_13);
            //        return;
            //    }
            //    var f = new FSAOrderDetail(saOder, new List<SAOrder>() { saOder }, ConstFrm.optStatusForm.View);
            //    f.ShowDialog(this);
            //}
            //catch (Exception ex)
            //{
            //    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            //}
        }
        #endregion

        private void uGrid_ClickCell(object sender, ClickCellEventArgs e)
        {
            btnViewSaOrder.Enabled = (uGrid.ActiveRow != null || uGrid.Selected.Rows.Count > 0);
        }
    }
}
