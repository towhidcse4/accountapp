﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm.Frm.FBusiness.FSAInvoice.FSAInvoice
{
    public partial class UTopPhieuXuatKho : UserControl
    {
        #region khai báo
        public IAccountingObjectService IaccountingObjectService
        {
            get;
            set;
        }
        #endregion

        public UTopPhieuXuatKho()
        {
            InitializeComponent();
            //load danh sách
            DsAccountingObject = new List<AccountingObject>();
            //thiết lập
            cbbAccountingObjectID.DataSource = new List<AccountingObject>();
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
        }

        #region Properties
        private bool _showBtnSelectSaInvoice;
        public bool ShowBtnSelectSaInvoice
        {
            get { return _showBtnSelectSaInvoice; }
            set
            {
                _showBtnSelectSaInvoice = value;
                BtnSelectSaInvoice.Visible = _showBtnSelectSaInvoice;
                txtAccountingObjectName.Width = _showBtnSelectSaInvoice ? 483 : 628;
            }
        }
        public List<AccountingObject> DsAccountingObject
        {
            get { return (List<AccountingObject>)cbbAccountingObjectID.DataSource; }
            set
            {
                List<AccountingObject> accountingObjects = value;
                if (accountingObjects == null || accountingObjects.Count <= 0) return;
                cbbAccountingObjectID.DataSource = value;
                //thiết lập
                //cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
                //Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            }
        }

        public Guid AccountingObjectID
        {
            get
            {
                return AccountingObjectItem.ID;
            }
            set
            {
                Guid temp = value;
                AccountingObjectItem = DsAccountingObject.FirstOrDefault(k => k.ID == temp);
            }
        }

        public AccountingObject AccountingObjectItem
        {
            get
            {
                return (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectID);
            }
            set
            {
                AccountingObject temp = value;
                foreach (var item in from item in cbbAccountingObjectID.Rows let accountingObject = item.ListObject as AccountingObject where accountingObject != null && accountingObject.ID == temp.ID select item)
                    cbbAccountingObjectID.SelectedRow = item;
            }
        }

        public string AccountingObjectName
        {
            get { return txtAccountingObjectName.Text; }
            set { txtAccountingObjectName.Text = value; }
        }
        public string AccountingObjectAddress
        {
            get { return txtAccountingObjectAddress.Text; }
            set { txtAccountingObjectAddress.Text = value; }
        }
        public string TaxCode
        {
            get { return txtCompanyTaxCode.Text; }
            set { txtCompanyTaxCode.Text = value; }
        }
        public string ContactName
        {
            get { return txtSContactName.Text; }
            set { txtSContactName.Text = value; }
        }

        public string Reason
        {
            //get;
            //set;
            get { return txtSReason.Text; }
            set { txtSReason.Text = value; }
        }
        public string OriginalNo
        {
            get { return txtOriginalNo.Text; }
            set { txtOriginalNo.Text = value; }
        }
        #endregion

        #region Event
        public void ClickButton(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            CbbAccountingObjectIdpxkEditorButtonClick(sender, e);
        }

        //PRIVATE EVENT
        private void CbbAccountingObjectIdpxkEditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            //mở form thêm mới đối tượng kế toán
            new FAccountingObjectCustomersDetail().ShowDialog(this);
            //load lại danh sách kế toán (nếu thêm thành công)
            //DsAccountingObject = IaccountingObjectService.Query.Where(k => k.IsActive && (k.ObjectType == 1 || k.ObjectType == 2)).ToList();
            List<int> i = new List<int> { 1, 2 };
            DsAccountingObject = new List<AccountingObject>(); //IaccountingObjectService.GetAll_ByListObjectType(i);
        }

        private void CbbAccountingObjectIdpxkLeave(object sender, EventArgs e)
        {
            //rời khởi control thì kiểm tra
            UltraCombo cbbInput;
            try { cbbInput = (UltraCombo)sender; if (cbbInput == null) return; }
            catch { return; }

            if (string.IsNullOrEmpty(cbbInput.Text)) return;
            if (!((List<AccountingObject>)cbbInput.DataSource).Any(item => item.AccountingObjectCode.Equals(cbbInput.Text)))
                MSG.Error("Không có đối tượng kế toán này trong danh mục");  //không có đối tượng này
        }

        private bool _isRunCbbRowSelect;
        private void CbbAccountingObjectIdpxkRowSelected(object sender, RowSelectedEventArgs e)
        {
            UltraCombo cbbInput;
            try { cbbInput = (UltraCombo)sender; if (cbbInput == null) return; }
            catch { return; }

            //khi chọn một đối tượng kế toán thì tự động điền địa chỉ và tên đối tượng
            if (e.Row == null || e.Row.ListObject == null || _isRunCbbRowSelect) return;

            _isRunCbbRowSelect = true;

            string strCode = ((AccountingObject)e.Row.ListObject).AccountingObjectCode;
            string strName = ((AccountingObject)e.Row.ListObject).AccountingObjectName;
            string strAddress = ((AccountingObject)e.Row.ListObject).Address;
            string strCompanyTaxCode = ((AccountingObject)e.Row.ListObject).TaxCode;

            foreach (UltraGridRow items in cbbInput.Rows.Where(t => ((AccountingObject)t.ListObject).AccountingObjectCode.Equals(strCode)))
                items.Selected = true;

            txtAccountingObjectName.Text = strName;
            txtAccountingObjectAddress.Text = strAddress;
            txtCompanyTaxCode.Text = strCompanyTaxCode;

            _isRunCbbRowSelect = false;
        }
        #endregion

        private void BtnSelectSaInvoice_Click(object sender, EventArgs e)
        {

        }
    }
}
