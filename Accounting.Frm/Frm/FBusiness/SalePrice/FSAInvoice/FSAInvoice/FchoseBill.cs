﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;

namespace Accounting
{
    public partial class FchoseBill<T> : DialogForm
    {
        #region Khai báo
        public SABill _lstSA { get; private set; }
        private SAInvoice _select = new SAInvoice();
        private Guid? AccountingObjectId;
        #endregion

        public FchoseBill(BindingList<IList> input, T @select, Guid? accountingObjectId)
        {
            InitializeComponent();
            _select = @select as SAInvoice;
            AccountingObjectId = accountingObjectId;
            var lstRefDetailId = new List<SAInvoiceDetail>();
            if (_select != null) lstRefDetailId = _select.SAInvoiceDetails.ToList();
            Utils.ProcessControls(this);
            var result = new List<SABill>();
            if (accountingObjectId.HasValue)
            {
                var lstSA = Utils.ISAInvoiceService.Query.Where(x => x.BillRefID != null).ToList();
                result = Utils.ISABillService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.RefDateTime.Value.Date <= dtEndDate.DateTime.Date && x.RefDateTime.Value.Date >= dtBeginDate.DateTime.Date && x.CurrencyID == _select.CurrencyID && (x.StatusInvoice == 1 || x.StatusInvoice == 0)).OrderByDescending(s => s.InvoiceDate).ThenByDescending(s => s.InvoiceNo).ToList();
                Utils.ISABillService.UnbindSession(result);
                result = Utils.ISABillService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.RefDateTime.Value.Date <= dtEndDate.DateTime.Date && x.RefDateTime.Value.Date >= dtBeginDate.DateTime.Date && x.CurrencyID == _select.CurrencyID && (x.StatusInvoice == 1 || x.StatusInvoice == 0)).OrderByDescending(s => s.InvoiceDate).ThenByDescending(s => s.InvoiceNo).ToList();
                if (lstSA.Count > 0) result = result.Where(x => !lstSA.Any(c => c.BillRefID == x.ID)).ToList();
            }
            uGrid.DataSource = result;
            ConfigGrid(uGrid);
        }

        #region Event

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            //chọn tất cả các hàng hóa cùng đơn đặt hàng
            if (e.Cell.Column.Key == "Check")
            {
                var gid = (Guid?)e.Cell.Row.Cells["ID"].Value;
                if (gid.HasValue)
                {
                    e.Cell.Row.Cells["Check"].Value = !(bool)e.Cell.Value;
                }
                foreach(var row in uGrid.Rows)
                {
                    if (row.Index != e.Cell.Row.Index) row.Cells["Check"].Value = false;
                }
                uGrid.UpdateDataGrid();
            }
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid _uGrid)
        {
            Utils.ConfigGrid(_uGrid, ConstDatabase.SABill_TableName);
            _uGrid.DisplayLayout.Bands[0].Columns["TotalAmountOriginal"].Hidden = true;
            _uGrid.DisplayLayout.Bands[0].Columns["TotalVATAmountOriginal"].Hidden = true;
            _uGrid.DisplayLayout.Bands[0].Columns["TotalDiscountAmountOriginal"].Hidden = true;
            _uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            _uGrid.DisplayLayout.UseFixedHeaders = false;
            _uGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
            _uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            foreach (var column in _uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Hidden = false;
            ugc.Header.VisiblePosition = 0;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellClickAction = CellClickAction.Edit;
        }       
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            _lstSA = (uGrid.DataSource as List<SABill>).FirstOrDefault(x=>x.Check);
            if (_lstSA == null)
            {
                MSG.Warning("Bạn chưa chọn chứng từ nào! Xin vui lòng chọn lại!");
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            var result = new List<SABill>();
            if (AccountingObjectId.HasValue)
            {
                var lstSA = Utils.ISAInvoiceService.Query.Where(x => x.BillRefID != null).ToList();
                result = Utils.ISABillService.Query.Where(x => x.AccountingObjectID == (Guid)AccountingObjectId && x.RefDateTime.Value.Date <= dtEndDate.DateTime && x.RefDateTime.Value.Date >= dtBeginDate.DateTime && x.CurrencyID == _select.CurrencyID && (x.StatusInvoice == 1 || x.StatusInvoice == 0)).OrderByDescending(s => s.InvoiceDate).ThenByDescending(s => s.InvoiceNo).ToList();
                Utils.ISABillService.UnbindSession(result);
                result = Utils.ISABillService.Query.Where(x => x.AccountingObjectID == (Guid)AccountingObjectId && x.RefDateTime.Value.Date <= dtEndDate.DateTime && x.RefDateTime.Value.Date >= dtBeginDate.DateTime && x.CurrencyID == _select.CurrencyID && (x.StatusInvoice == 1 || x.StatusInvoice == 0)).OrderByDescending(s => s.InvoiceDate).ThenByDescending(s => s.InvoiceNo).ToList();
                if (lstSA.Count > 0) result = result.Where(x => !lstSA.Any(c => c.BillRefID == x.ID)).ToList();
            }
            uGrid.DataSource = result;

        }
    }
}
