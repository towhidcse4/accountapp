﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;

namespace Accounting
{
    public partial class FchoseRsInwardOutward : DialogForm
    {
        #region Khai báo
        private readonly BindingList<IList> _input;
        private readonly List<ObjRsInwardOutward> _lstRsInwardOutward;
        private DateTime _startDate;
        private Guid? _saInvoiceId = null;
        private Guid? _objAccid = null;
        private List<Guid> _guids;
        public List<ObjRsInwardOutward> Selecteds { get; private set; }
        #endregion

        public FchoseRsInwardOutward(BindingList<IList> input, Guid? saInvoiceId, List<Guid> ObjRsInwardOutwardIDs, Guid? accObjid)
        {
            InitializeComponent();
            _saInvoiceId = saInvoiceId.CloneObject();
            _objAccid = accObjid.CloneObject();
            //config GUI
            List<Item> lstItems = Utils.ObjConstValue.SelectTimes;
            // load dữ liệu cho Combobox Chọn thời gian
            var stringToDateTime = Utils.StringToDateTime(ConstFrm.DbStartDate);
            if (stringToDateTime != null)
                _startDate = (DateTime)stringToDateTime;
            else _startDate = DateTime.Now;
            cbbSelectTime.ConfigComboSelectTime(lstItems, "Name");
            cbbSelectTime.ValueChanged += cbbDateTime_ValueChanged;
            cbbSelectTime.SelectedRow = cbbSelectTime.Rows[0];

            _lstRsInwardOutward = new List<ObjRsInwardOutward>();
            uGrid.DataSource = _lstRsInwardOutward;
            ConfigGrid(uGrid);

            //var thisMonthStart = _startDate.AddDays(1 - _startDate.Day);
            //var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
            //dteFrom.DateTime = thisMonthStart;
            //dteTo.DateTime = thisMonthEnd;

            _input = input.CloneObject();
            _guids = ObjRsInwardOutwardIDs;
            btnGetData.PerformClick();
            this.InvokeOnClick(btnGetData, EventArgs.Empty);
        }

        #region Event
        private void BtnCloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            if (_input == null) return;
            uGrid.UpdateData();

            //lấy những cái được chọn và convert sang SainVoidDetails rồi import vào Grid bên dưới
            Selecteds = _lstRsInwardOutward.Where(k => k.Check).ToList();
            //Convert sang sang SainVoidDetails
            //List<SAInvoiceDetail> lstKq = new List<SAInvoiceDetail>();
            //foreach (ObjRsInwardOutward objRsInwardOutward in lstSelect)
            //{
            //    SAInvoiceDetail temp = new SAInvoiceDetail();
            //    ReflectionUtils.Copy(typeof(ObjRsInwardOutward), objRsInwardOutward, typeof(SAInvoiceDetail), temp);
            //    ReflectionUtils.Copy(typeof(ObjRsInwardOutward), objRsInwardOutward.RSInwardOutward, typeof(SAInvoiceDetail), temp);
            //    temp.ID = Guid.Empty;
            //    Selecteds.Add(temp.CloneObject());
            //    if (objRsInwardOutward.RSInwardOutwardID.HasValue)
            //        _guids.Add(objRsInwardOutward.RSInwardOutwardID.Value.CloneObject());
            //}
            //Import vào cái Grid bên dưới (nếu có hàng hóa sẵn thì có thể chèn thêm vào hoặc có thể xóa trắng rồi chèn mỗi các cái đang chọn)
            //foreach (SAInvoiceDetai           l saInvoiceDetail in lstKq)
            //{
            //    Selecteds.Add(saInvoiceDetail);
            //}
            DialogResult = DialogResult.OK;
            Close();
        }

        private void BtnLaySoLieuClick(object sender, EventArgs e)
        {
            //Lấy dữ liệu 
            //- trừ TypeID của lắp ráp tháo dỡ , .... (- 411, 412, 413, 414, 415) chỉ nhận 410
            //- lọc phiếu xuất kho theo thời gian (từ...đến)
            //- lọc bỏ các phiếu đã xuất từ bán hàng trước đó
            _lstRsInwardOutward.Clear();
            var result = Utils.IRSInwardOutwardService.Query.Where(x => x.Date >= dteFrom.DateTime && x.Date <= dteTo.DateTime && x.TypeID == 410 && (x.SAInvoiceID == _saInvoiceId || x.SAInvoiceID == null) && (x.AccountingObjectID != null) && (x.AccountingObjectID == _objAccid));
            Utils.IRSInwardOutwardService.UnbindSession(result);
            result = Utils.IRSInwardOutwardService.Query.Where(x => x.Date >= dteFrom.DateTime && x.Date <= dteTo.DateTime && x.TypeID == 410 && (x.SAInvoiceID == _saInvoiceId || x.SAInvoiceID == null) && (x.AccountingObjectID != null) && (x.AccountingObjectID == _objAccid));
            //lấy dữ liệu gán vào biến _lstRsInwardOutward
            foreach (var rsInwardOutward in result)
            {
                foreach (var detail in rsInwardOutward.RSInwardOutwardDetails)
                {
                    var objRsInwardOutward = new ObjRsInwardOutward();
                    foreach (var prop in detail.GetType().GetProperties().Where(prop => prop.CanRead && prop.CanWrite && objRsInwardOutward.HasProperty(prop.Name)))
                    {
                        objRsInwardOutward.SetProperty(prop.Name, detail.GetProperty(prop.Name).CloneObject());
                    }
                    objRsInwardOutward.No = rsInwardOutward.No.CloneObject();
                    objRsInwardOutward.Date = rsInwardOutward.Date.CloneObject();
                    objRsInwardOutward.SAInvoiceID = rsInwardOutward.SAInvoiceID.CloneObject();
                    //var accObj = Utils.ListAccountingObject.FirstOrDefault(x => x.ID == rsInwardOutward.AccountingObjectID);
                    //if (accObj != null)
                    //{
                    objRsInwardOutward.AccountingObjectID = rsInwardOutward.AccountingObjectID;
                    objRsInwardOutward.AccountingObjectName = rsInwardOutward.AccountingObjectName;
                    objRsInwardOutward.AccountingObjectAddress = rsInwardOutward.AccountingObjectAddress;
                    //}
                    if (objRsInwardOutward.SAInvoiceID.HasValue && objRsInwardOutward.SAInvoiceID == _saInvoiceId || _guids.Any(x => x == objRsInwardOutward.ID)) objRsInwardOutward.Check = true;
                    _lstRsInwardOutward.Add(objRsInwardOutward);
                }
            }
            //_lstRsInwardOutward.AddRange((from u in result select new ObjRsInwardOutward(u.)).ToList());
            //tiếp tục
            if (_guids.Count > 0)
                foreach (var item in _lstRsInwardOutward)
                {
                    item.Check = _guids.Contains(item.RSInwardOutwardID.Value);
                }
            uGrid.DataSource = _lstRsInwardOutward;
            uGrid.DataBind();
        }

        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.SelectedRow == null) return;
            var model = cbb.SelectedRow.ListObject as Item;
            DateTime dtBegin;
            DateTime dtEnd;
            Utils.GetDateBeginDateEnd(_startDate.Year,_startDate, model, out dtBegin, out dtEnd);
            dteFrom.DateTime = dtBegin;
            dteTo.DateTime = dtEnd;
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid _uGrid)
        {
            Utils.ConfigGrid(_uGrid, string.Empty, genTemplateColum(), false, true, isFilterForm: false, isConfigGuiManually: true, setReadOnlyColor: false);
            _uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            _uGrid.DisplayLayout.UseFixedHeaders = false;
            _uGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
            foreach (var column in _uGrid.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key.Contains("MaterialGoods"))
                {
                    this.ConfigCbbToGrid<SAInvoiceDetail, MaterialGoodsCustom>(_uGrid, column.Key, Utils.ListMaterialGoodsCustom, "ID", "MaterialGoodsCode", ConstDatabase.MaterialGoodsCustom_TableName);
                }
                else if (column.Key.Contains("Quantity"))
                {
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                    column.MaxLength = 36;
                    column.CellAppearance.TextHAlign = HAlign.Right;
                    column.Nullable = Infragistics.Win.UltraWinGrid.Nullable.Nothing;
                    column.FormatNumberic(ConstDatabase.Format_Quantity);
                }

                if (column.Key.Contains("Date"))
                    column.CellActivation = Activation.NoEdit;
                if (column.DataType.IsNumericType())
                    column.FilterOperatorDefaultValue = FilterOperatorDefaultValue.Equals;
                else
                    column.FilterOperatorDefaultValue = FilterOperatorDefaultValue.Contains;
            }

            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Header.VisiblePosition = 0;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
        }

        List<TemplateColumn> genTemplateColum()
        {
            Dictionary<int, int> dicVisiblePosition;
            List<string> strColumnName, strColumnCaption, strColumnToolTip;
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb;
            List<int> intColumnWidth,
                      intColumnMaxWidth,
                      intColumnMinWidth,
                      intVisiblePosition,
                      VTbolIsVisible,
                      VTbolIsVisibleCbb,
                      VTintVisiblePosition,
                      VTbolIsReadOnly;

            #region ObjRsInwardOutward
            strColumnName = new List<string> { "Check", "No", "Date", "AccountingObjectName", "AccountingObjectAddress", "OrderRefNo", "AccountingObjectID", "UnitPrice", "UnitPriceOriginal", "UnitPriceConvert", "UnitPriceConvertOriginal", "Amount", "AmountOriginal", "OWPurpose", "ID", "RSInwardOutwardID", "MaterialGoodsID", "Description", "RepositoryID", "DebitAccount", "CreditAccount", "Unit", "Quantity", "QuantityConvert", "CostSetID", "ContractID", "EmployeeID", "StatisticsCodeID", "ConfrontID", "ConfrontDetailID", "ExpiryDate", "LotNo", "UnitConvert", "ConvertRate", "DepartmentID", "ExpenseItemID", "BudgetItemID", "MaterialQuantumID", "MaterialQuantity", "CustomProperty1", "CustomProperty2", "CustomProperty3", "IsIrrationalCost", "OrderPriority", "RSInwardOutward", "PostedDate", "TypeID", };
            //"0-Check", "1-No", "2-Date", "3-AccountingObjectName", "4-AccountingObjectAddress", "5-OrderRefNo", "6-AccountingObjectID", "7-UnitPrice", "8-UnitPriceOriginal", "9-UnitPriceConvert", "10-UnitPriceConvertOriginal", "11-Amount", "12-AmountOriginal", "13-OWPurpose", "14-ID", "15-RSInwardOutwardID", "16-MaterialGoodsID", "17-Description", "18-RepositoryID", "19-DebitAccount", "20-CreditAccount", "21-Unit", "22-Quantity", "23-QuantityConvert", "24-CostSetID", "25-ContractID", "26-EmployeeID", "27-StatisticsCodeID", "28-ConfrontID", "29-ConfrontDetailID", "30-ExpiryDate", "31-LotNo", "32-UnitConvert", "33-ConvertRate", "34-DepartmentID", "35-ExpenseItemID", "36-BudgetItemID", "37-MaterialQuantumID", "38-MaterialQuantity", "39-CustomProperty1", "40-CustomProperty2", "41-CustomProperty3", "42-IsIrrationalCost", "43-OrderPriority", "44-RSInwardOutward", "45-PostedDate", "46-TypeID",  
            strColumnCaption = strColumnToolTip = new List<string> { "", "Số chứng từ", "Ngày chứng từ", "Khách hàng", "Địa chỉ", "OrderRefNo", "AccountingObjectID", "UnitPrice", "UnitPriceOriginal", "UnitPriceConvert", "UnitPriceConvertOriginal", "Amount", "AmountOriginal", "OWPurpose", "ID", "RSInwardOutwardID", "Mã hàng", "Diễn giải", "RepositoryID", "DebitAccount", "CreditAccount", "Unit", "Số lượng", "QuantityConvert", "CostSetID", "ContractID", "EmployeeID", "StatisticsCodeID", "ConfrontID", "ConfrontDetailID", "ExpiryDate", "LotNo", "UnitConvert", "ConvertRate", "DepartmentID", "ExpenseItemID", "BudgetItemID", "MaterialQuantumID", "MaterialQuantity", "CustomProperty1", "CustomProperty2", "CustomProperty3", "IsIrrationalCost", "OrderPriority", "RSInwardOutward", "PostedDate", "TypeID" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>() { 90, 80, 80, 121, 121, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 101, 180, -1, -1, -1, -1, 101, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int> { 0, 1, 2, 3, 4, 16, 17, 22 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int> { 1, 2 };
            VTintVisiblePosition = new List<int>() { 1, 2, 3, 4, 5, 17, 16, 22 };
            VTbolIsReadOnly = new List<int>() { 1, 2, 3, 4, 16, 17, 22 };

            dicVisiblePosition = new Dictionary<int, int>();   //bolIsVisible/ intVisiblePosition 
            if (VTintVisiblePosition.Count > 0) { for (int j = 0; j < VTbolIsVisible.Count; j++) dicVisiblePosition.Add(VTbolIsVisible[j], VTintVisiblePosition[j]); }

            for (int i = 0; i < strColumnName.Count; i++)
            {
                bool temp = VTbolIsVisible.Contains(i);
                bolIsVisible.Add(temp);
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(VTbolIsReadOnly.Contains(i));
                intColumnWidth.Add(intColumnWidth[i]);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(dicVisiblePosition.Count > 0 ? (temp ? dicVisiblePosition[i] : strColumnName.Count - 1) : -1);
            }
            #endregion

            return ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();
        }
        #endregion

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            //chọn tất cả các hàng hóa cùng phiếu xuất
            if (e.Cell.Column.Key == "Check")
            {
                var gid = (Guid?)e.Cell.Row.Cells["RSInwardOutwardID"].Value;
                if (gid.HasValue)
                {
                    foreach (var rw in uGrid.Rows.Where(row => row != e.Cell.Row && (Guid?)row.Cells["RSInwardOutwardID"].Value == gid))
                    {
                        rw.Cells["Check"].Value = !(bool)e.Cell.Value;
                    }
                }
                uGrid.UpdateDataGrid();
            }
        }
    }
}
