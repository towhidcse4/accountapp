﻿using System.ComponentModel;

namespace Accounting
{
    sealed partial class FSAInvoiceDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSAInvoiceDetail));
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            this.ultraTabPageHoaDon = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.dteListDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtListCommonNameInventory = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblListCommonNameInventory = new Infragistics.Win.Misc.UltraLabel();
            this.lblListDate = new Infragistics.Win.Misc.UltraLabel();
            this.txtListNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblListNo = new Infragistics.Win.Misc.UltraLabel();
            this.chkisAttachList = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.lblisAttachList = new Infragistics.Win.Misc.UltraLabel();
            this.btnCongNo = new Infragistics.Win.Misc.UltraButton();
            this.btnSAOrder = new Infragistics.Win.Misc.UltraButton();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePhieuXuatKho = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxPhieuXuatKho = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.txtCompanyTaxCodePXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCodePXK = new Infragistics.Win.Misc.UltraLabel();
            this.lblChungTuGoc = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNoPXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttachPXK = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNamePXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDPXK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtSContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChiPXK = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHangPXK = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePhieuThu = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxPhieuThu = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.lblChungTuGocPT = new Infragistics.Win.Misc.UltraLabel();
            this.txtNumberAttachPT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttachPT = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNamePT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDPT = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMReasonPayPT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMReasonPayPT = new Infragistics.Win.Misc.UltraLabel();
            this.txtMContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChiPT = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHangPT = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageGiayBaoCo = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBoxGiayBaoCo = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblNopTienTaiKhoanGBC = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameGBC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDGBC = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMReasonPayGBC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMReasonPayGBC = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressGBC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChiGBC = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHangGBC = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbLoaiHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbHinhThucHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbMauHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboThanhToan = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.dteListDateHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtListCommonNameInventoryHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtListNoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.chkisAttachListHD = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCompanyTaxCodeHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReasonHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.palTop = new System.Windows.Forms.Panel();
            this.ultraGroupBoxTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palVouchersNoHd = new Infragistics.Win.Misc.UltraPanel();
            this.palVouchersNoPxk = new Infragistics.Win.Misc.UltraPanel();
            this.palVouchersNoTt = new Infragistics.Win.Misc.UltraPanel();
            this.palVouchersNoGbc = new Infragistics.Win.Misc.UltraPanel();
            this.palChung = new System.Windows.Forms.Panel();
            this.optHoaDon = new Accounting.UltraOptionSet_Ex();
            this.optThanhToan = new Accounting.UltraOptionSet_Ex();
            this.lblTT = new Infragistics.Win.Misc.UltraLabel();
            this.optPhieuXuatKho = new Accounting.UltraOptionSet_Ex();
            this.btnOutwardNo = new Infragistics.Win.Misc.UltraButton();
            this.cbbChonThanhToan = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.palFill = new System.Windows.Forms.Panel();
            this.palGrid = new System.Windows.Forms.Panel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new System.Windows.Forms.Panel();
            this.group = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtOldInvSeries = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvTemplate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTttt = new Infragistics.Win.Misc.UltraLabel();
            this.btnSelectBill = new Infragistics.Win.Misc.UltraButton();
            this.cbBill = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.lblBillDeleted = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalDiscountAmountSubOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalDiscountAmountSub = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountOriginalStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.btnIsDiscountByVoucher = new Infragistics.Win.Misc.UltraButton();
            this.chkIsDiscountByVoucher = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).BeginInit();
            this.ultraGroupBoxHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.ultraTabPagePhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).BeginInit();
            this.ultraGroupBoxPhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXK)).BeginInit();
            this.ultraTabPagePhieuThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuThu)).BeginInit();
            this.ultraGroupBoxPhieuThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPT)).BeginInit();
            this.ultraTabPageGiayBaoCo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxGiayBaoCo)).BeginInit();
            this.ultraGroupBoxGiayBaoCo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameGBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDGBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayGBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressGBC)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHinhThucHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMauHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDateHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventoryHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachListHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).BeginInit();
            this.ultraGroupBoxTop.SuspendLayout();
            this.palVouchersNoHd.SuspendLayout();
            this.palVouchersNoPxk.SuspendLayout();
            this.palVouchersNoTt.SuspendLayout();
            this.palVouchersNoGbc.SuspendLayout();
            this.palChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPhieuXuatKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).BeginInit();
            this.palFill.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group)).BeginInit();
            this.group.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsDiscountByVoucher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageHoaDon
            // 
            this.ultraTabPageHoaDon.Controls.Add(this.ultraGroupBoxHoaDon);
            this.ultraTabPageHoaDon.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageHoaDon.Name = "ultraTabPageHoaDon";
            this.ultraTabPageHoaDon.Size = new System.Drawing.Size(927, 148);
            // 
            // ultraGroupBoxHoaDon
            // 
            this.ultraGroupBoxHoaDon.Controls.Add(this.dteListDate);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtListCommonNameInventory);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblListCommonNameInventory);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblListDate);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtListNo);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblListNo);
            this.ultraGroupBoxHoaDon.Controls.Add(this.chkisAttachList);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblisAttachList);
            this.ultraGroupBoxHoaDon.Controls.Add(this.btnCongNo);
            this.ultraGroupBoxHoaDon.Controls.Add(this.btnSAOrder);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtCompanyTaxCode);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblCompanyTaxCode);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtReason);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblReason);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtContactName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblContactName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraGroupBoxHoaDon.Controls.Add(this.lblAccountingObjectID);
            this.ultraGroupBoxHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance13.FontData.BoldAsString = "True";
            appearance13.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDon.HeaderAppearance = appearance13;
            this.ultraGroupBoxHoaDon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDon.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxHoaDon.Name = "ultraGroupBoxHoaDon";
            this.ultraGroupBoxHoaDon.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxHoaDon.TabIndex = 26;
            this.ultraGroupBoxHoaDon.Text = "Thông tin chung";
            this.ultraGroupBoxHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dteListDate
            // 
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.dteListDate.Appearance = appearance1;
            this.dteListDate.AutoSize = false;
            this.dteListDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteListDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteListDate.Location = new System.Drawing.Point(331, 116);
            this.dteListDate.MaskInput = "";
            this.dteListDate.Name = "dteListDate";
            this.dteListDate.Size = new System.Drawing.Size(109, 22);
            this.dteListDate.TabIndex = 9;
            this.dteListDate.Value = null;
            // 
            // txtListCommonNameInventory
            // 
            this.txtListCommonNameInventory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtListCommonNameInventory.AutoSize = false;
            this.txtListCommonNameInventory.Location = new System.Drawing.Point(545, 116);
            this.txtListCommonNameInventory.Name = "txtListCommonNameInventory";
            this.txtListCommonNameInventory.Size = new System.Drawing.Size(373, 22);
            this.txtListCommonNameInventory.TabIndex = 10;
            // 
            // lblListCommonNameInventory
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.lblListCommonNameInventory.Appearance = appearance2;
            this.lblListCommonNameInventory.Location = new System.Drawing.Point(450, 116);
            this.lblListCommonNameInventory.Name = "lblListCommonNameInventory";
            this.lblListCommonNameInventory.Size = new System.Drawing.Size(107, 22);
            this.lblListCommonNameInventory.TabIndex = 0;
            this.lblListCommonNameInventory.Text = "Mặt hàng chung";
            // 
            // lblListDate
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.lblListDate.Appearance = appearance3;
            this.lblListDate.Location = new System.Drawing.Point(292, 116);
            this.lblListDate.Name = "lblListDate";
            this.lblListDate.Size = new System.Drawing.Size(47, 22);
            this.lblListDate.TabIndex = 0;
            this.lblListDate.Text = "Ngày";
            // 
            // txtListNo
            // 
            this.txtListNo.AutoSize = false;
            this.txtListNo.Location = new System.Drawing.Point(156, 116);
            this.txtListNo.Name = "txtListNo";
            this.txtListNo.Size = new System.Drawing.Size(128, 22);
            this.txtListNo.TabIndex = 8;
            // 
            // lblListNo
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblListNo.Appearance = appearance4;
            this.lblListNo.Location = new System.Drawing.Point(127, 116);
            this.lblListNo.Name = "lblListNo";
            this.lblListNo.Size = new System.Drawing.Size(23, 22);
            this.lblListNo.TabIndex = 0;
            this.lblListNo.Text = "Số";
            // 
            // chkisAttachList
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.chkisAttachList.Appearance = appearance5;
            this.chkisAttachList.BackColor = System.Drawing.Color.Transparent;
            this.chkisAttachList.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkisAttachList.Location = new System.Drawing.Point(107, 116);
            this.chkisAttachList.Name = "chkisAttachList";
            this.chkisAttachList.Size = new System.Drawing.Size(14, 22);
            this.chkisAttachList.TabIndex = 7;
            this.chkisAttachList.CheckedChanged += new System.EventHandler(this.ChkisAttachListCheckedChanged);
            // 
            // lblisAttachList
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.lblisAttachList.Appearance = appearance6;
            this.lblisAttachList.Location = new System.Drawing.Point(6, 116);
            this.lblisAttachList.Name = "lblisAttachList";
            this.lblisAttachList.Size = new System.Drawing.Size(95, 22);
            this.lblisAttachList.TabIndex = 0;
            this.lblisAttachList.Text = "In kèm bảng kê";
            // 
            // btnCongNo
            // 
            this.btnCongNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCongNo.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnCongNo.Location = new System.Drawing.Point(833, 20);
            this.btnCongNo.Name = "btnCongNo";
            this.btnCongNo.Size = new System.Drawing.Size(85, 23);
            this.btnCongNo.TabIndex = 20;
            this.btnCongNo.TabStop = false;
            this.btnCongNo.Text = "Công nợ";
            this.btnCongNo.Click += new System.EventHandler(this.BtnCongNoClick);
            // 
            // btnSAOrder
            // 
            this.btnSAOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSAOrder.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnSAOrder.Location = new System.Drawing.Point(719, 20);
            this.btnSAOrder.Name = "btnSAOrder";
            this.btnSAOrder.Size = new System.Drawing.Size(108, 23);
            this.btnSAOrder.TabIndex = 20;
            this.btnSAOrder.TabStop = false;
            this.btnSAOrder.Text = "Đơn đặt hàng";
            this.btnSAOrder.Click += new System.EventHandler(this.btnSAOrder_Click);
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectName.TabIndex = 2;
            this.txtAccountingObjectName.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
            editorButton1.Appearance = appearance7;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(107, 20);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(177, 22);
            this.cbbAccountingObjectID.TabIndex = 1;
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectID.ValueChanged += new System.EventHandler(this.cbbAccountingObjectID_ValueChanged);
            this.cbbAccountingObjectID.Leave += new System.EventHandler(this.ChkisAttachListCheckedChanged);
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.AutoSize = false;
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(107, 68);
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(177, 22);
            this.txtCompanyTaxCode.TabIndex = 4;
            // 
            // lblCompanyTaxCode
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCode.Appearance = appearance8;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(6, 68);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(95, 22);
            this.lblCompanyTaxCode.TabIndex = 0;
            this.lblCompanyTaxCode.Text = "Mã số thuế";
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(107, 92);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(811, 22);
            this.txtReason.TabIndex = 7;
            // 
            // lblReason
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.lblReason.Appearance = appearance9;
            this.lblReason.Location = new System.Drawing.Point(6, 92);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(95, 22);
            this.lblReason.TabIndex = 0;
            this.lblReason.Text = "Diễn giải";
            // 
            // txtContactName
            // 
            this.txtContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactName.AutoSize = false;
            this.txtContactName.Location = new System.Drawing.Point(403, 68);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(515, 22);
            this.txtContactName.TabIndex = 6;
            // 
            // lblContactName
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextVAlignAsString = "Middle";
            this.lblContactName.Appearance = appearance10;
            this.lblContactName.Location = new System.Drawing.Point(292, 68);
            this.lblContactName.Name = "lblContactName";
            this.lblContactName.Size = new System.Drawing.Size(107, 22);
            this.lblContactName.TabIndex = 5;
            this.lblContactName.Text = "Họ tên người liên hệ";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(107, 44);
            this.txtAccountingObjectAddress.Multiline = true;
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(811, 22);
            this.txtAccountingObjectAddress.TabIndex = 3;
            this.txtAccountingObjectAddress.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // lblAccountingObjectAddress
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.lblAccountingObjectAddress.Appearance = appearance11;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(6, 44);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(95, 22);
            this.lblAccountingObjectAddress.TabIndex = 0;
            this.lblAccountingObjectAddress.Text = "Địa chỉ";
            // 
            // lblAccountingObjectID
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.lblAccountingObjectID.Appearance = appearance12;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(6, 19);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(95, 23);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Khách hàng ";
            // 
            // ultraTabPagePhieuXuatKho
            // 
            this.ultraTabPagePhieuXuatKho.Controls.Add(this.ultraGroupBoxPhieuXuatKho);
            this.ultraTabPagePhieuXuatKho.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePhieuXuatKho.Name = "ultraTabPagePhieuXuatKho";
            this.ultraTabPagePhieuXuatKho.Size = new System.Drawing.Size(927, 148);
            // 
            // ultraGroupBoxPhieuXuatKho
            // 
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraButton1);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.ultraButton2);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtCompanyTaxCodePXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblCompanyTaxCodePXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblChungTuGoc);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtOriginalNoPXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblNumberAttachPXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectNamePXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.cbbAccountingObjectIDPXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtSReason);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblSReason);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtSContactName);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblSContactName);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectAddressPXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblDiaChiPXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblKhachHangPXK);
            this.ultraGroupBoxPhieuXuatKho.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance22.FontData.BoldAsString = "True";
            appearance22.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxPhieuXuatKho.HeaderAppearance = appearance22;
            this.ultraGroupBoxPhieuXuatKho.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuXuatKho.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuXuatKho.Name = "ultraGroupBoxPhieuXuatKho";
            this.ultraGroupBoxPhieuXuatKho.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxPhieuXuatKho.TabIndex = 27;
            this.ultraGroupBoxPhieuXuatKho.Text = "Thông tin chung";
            this.ultraGroupBoxPhieuXuatKho.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton1.Location = new System.Drawing.Point(833, 20);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(85, 23);
            this.ultraButton1.TabIndex = 41;
            this.ultraButton1.TabStop = false;
            this.ultraButton1.Text = "Công nợ";
            this.ultraButton1.Click += new System.EventHandler(this.BtnCongNoClick);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton2.Location = new System.Drawing.Point(719, 20);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(108, 23);
            this.ultraButton2.TabIndex = 40;
            this.ultraButton2.TabStop = false;
            this.ultraButton2.Text = "Đơn đặt hàng";
            this.ultraButton2.Click += new System.EventHandler(this.btnSAOrder_Click);
            // 
            // txtCompanyTaxCodePXK
            // 
            this.txtCompanyTaxCodePXK.AutoSize = false;
            this.txtCompanyTaxCodePXK.Location = new System.Drawing.Point(128, 68);
            this.txtCompanyTaxCodePXK.Name = "txtCompanyTaxCodePXK";
            this.txtCompanyTaxCodePXK.Size = new System.Drawing.Size(156, 22);
            this.txtCompanyTaxCodePXK.TabIndex = 39;
            // 
            // lblCompanyTaxCodePXK
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCodePXK.Appearance = appearance14;
            this.lblCompanyTaxCodePXK.Location = new System.Drawing.Point(6, 67);
            this.lblCompanyTaxCodePXK.Name = "lblCompanyTaxCodePXK";
            this.lblCompanyTaxCodePXK.Size = new System.Drawing.Size(115, 23);
            this.lblCompanyTaxCodePXK.TabIndex = 38;
            this.lblCompanyTaxCodePXK.Text = "Mã số thuế";
            // 
            // lblChungTuGoc
            // 
            this.lblChungTuGoc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Center";
            appearance15.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.Appearance = appearance15;
            this.lblChungTuGoc.Location = new System.Drawing.Point(840, 116);
            this.lblChungTuGoc.Name = "lblChungTuGoc";
            this.lblChungTuGoc.Size = new System.Drawing.Size(78, 22);
            this.lblChungTuGoc.TabIndex = 37;
            this.lblChungTuGoc.Text = "Chứng từ gốc";
            // 
            // txtOriginalNoPXK
            // 
            this.txtOriginalNoPXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginalNoPXK.AutoSize = false;
            this.txtOriginalNoPXK.Location = new System.Drawing.Point(128, 116);
            this.txtOriginalNoPXK.Name = "txtOriginalNoPXK";
            this.txtOriginalNoPXK.Size = new System.Drawing.Size(706, 22);
            this.txtOriginalNoPXK.TabIndex = 36;
            // 
            // lblNumberAttachPXK
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.lblNumberAttachPXK.Appearance = appearance16;
            this.lblNumberAttachPXK.Location = new System.Drawing.Point(6, 115);
            this.lblNumberAttachPXK.Name = "lblNumberAttachPXK";
            this.lblNumberAttachPXK.Size = new System.Drawing.Size(115, 23);
            this.lblNumberAttachPXK.TabIndex = 35;
            this.lblNumberAttachPXK.Text = "Kèm theo";
            // 
            // txtAccountingObjectNamePXK
            // 
            this.txtAccountingObjectNamePXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePXK.AutoSize = false;
            this.txtAccountingObjectNamePXK.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNamePXK.Name = "txtAccountingObjectNamePXK";
            this.txtAccountingObjectNamePXK.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectNamePXK.TabIndex = 32;
            this.txtAccountingObjectNamePXK.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // cbbAccountingObjectIDPXK
            // 
            this.cbbAccountingObjectIDPXK.AutoSize = false;
            appearance17.Image = ((object)(resources.GetObject("appearance17.Image")));
            editorButton2.Appearance = appearance17;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDPXK.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObjectIDPXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDPXK.Location = new System.Drawing.Point(128, 20);
            this.cbbAccountingObjectIDPXK.Name = "cbbAccountingObjectIDPXK";
            this.cbbAccountingObjectIDPXK.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDPXK.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectIDPXK.TabIndex = 31;
            // 
            // txtSReason
            // 
            this.txtSReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSReason.AutoSize = false;
            this.txtSReason.Location = new System.Drawing.Point(128, 92);
            this.txtSReason.Name = "txtSReason";
            this.txtSReason.Size = new System.Drawing.Size(790, 22);
            this.txtSReason.TabIndex = 27;
            // 
            // lblSReason
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.lblSReason.Appearance = appearance18;
            this.lblSReason.Location = new System.Drawing.Point(6, 92);
            this.lblSReason.Name = "lblSReason";
            this.lblSReason.Size = new System.Drawing.Size(115, 22);
            this.lblSReason.TabIndex = 26;
            this.lblSReason.Text = "Lý do xuất";
            // 
            // txtSContactName
            // 
            this.txtSContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSContactName.AutoSize = false;
            this.txtSContactName.Location = new System.Drawing.Point(401, 68);
            this.txtSContactName.Name = "txtSContactName";
            this.txtSContactName.Size = new System.Drawing.Size(517, 22);
            this.txtSContactName.TabIndex = 25;
            // 
            // lblSContactName
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.lblSContactName.Appearance = appearance19;
            this.lblSContactName.Location = new System.Drawing.Point(293, 68);
            this.lblSContactName.Name = "lblSContactName";
            this.lblSContactName.Size = new System.Drawing.Size(102, 22);
            this.lblSContactName.TabIndex = 24;
            this.lblSContactName.Text = "Họ tên người nhận";
            // 
            // txtAccountingObjectAddressPXK
            // 
            this.txtAccountingObjectAddressPXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPXK.AutoSize = false;
            this.txtAccountingObjectAddressPXK.Location = new System.Drawing.Point(128, 44);
            this.txtAccountingObjectAddressPXK.Name = "txtAccountingObjectAddressPXK";
            this.txtAccountingObjectAddressPXK.Size = new System.Drawing.Size(790, 22);
            this.txtAccountingObjectAddressPXK.TabIndex = 23;
            this.txtAccountingObjectAddressPXK.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // lblDiaChiPXK
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.lblDiaChiPXK.Appearance = appearance20;
            this.lblDiaChiPXK.Location = new System.Drawing.Point(6, 44);
            this.lblDiaChiPXK.Name = "lblDiaChiPXK";
            this.lblDiaChiPXK.Size = new System.Drawing.Size(115, 22);
            this.lblDiaChiPXK.TabIndex = 22;
            this.lblDiaChiPXK.Text = "Địa chỉ";
            // 
            // lblKhachHangPXK
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.lblKhachHangPXK.Appearance = appearance21;
            this.lblKhachHangPXK.Location = new System.Drawing.Point(6, 20);
            this.lblKhachHangPXK.Name = "lblKhachHangPXK";
            this.lblKhachHangPXK.Size = new System.Drawing.Size(115, 22);
            this.lblKhachHangPXK.TabIndex = 0;
            this.lblKhachHangPXK.Text = "Khách hàng";
            // 
            // ultraTabPagePhieuThu
            // 
            this.ultraTabPagePhieuThu.Controls.Add(this.ultraGroupBoxPhieuThu);
            this.ultraTabPagePhieuThu.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePhieuThu.Name = "ultraTabPagePhieuThu";
            this.ultraTabPagePhieuThu.Size = new System.Drawing.Size(927, 148);
            // 
            // ultraGroupBoxPhieuThu
            // 
            this.ultraGroupBoxPhieuThu.Controls.Add(this.ultraButton3);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.ultraButton4);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.lblChungTuGocPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.txtNumberAttachPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.lblNumberAttachPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.txtAccountingObjectNamePT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.cbbAccountingObjectIDPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.txtMReasonPayPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.lblMReasonPayPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.txtMContactName);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.lblMContactName);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.txtAccountingObjectAddressPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.lblDiaChiPT);
            this.ultraGroupBoxPhieuThu.Controls.Add(this.lblKhachHangPT);
            this.ultraGroupBoxPhieuThu.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance30.FontData.BoldAsString = "True";
            appearance30.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxPhieuThu.HeaderAppearance = appearance30;
            this.ultraGroupBoxPhieuThu.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuThu.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuThu.Name = "ultraGroupBoxPhieuThu";
            this.ultraGroupBoxPhieuThu.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxPhieuThu.TabIndex = 28;
            this.ultraGroupBoxPhieuThu.Text = "Thông tin chung";
            this.ultraGroupBoxPhieuThu.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton3
            // 
            this.ultraButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton3.Location = new System.Drawing.Point(833, 20);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(85, 23);
            this.ultraButton3.TabIndex = 43;
            this.ultraButton3.Text = "Công nợ";
            this.ultraButton3.Click += new System.EventHandler(this.BtnCongNoClick);
            // 
            // ultraButton4
            // 
            this.ultraButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton4.Location = new System.Drawing.Point(719, 20);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(108, 23);
            this.ultraButton4.TabIndex = 42;
            this.ultraButton4.Text = "Đơn đặt hàng";
            this.ultraButton4.Click += new System.EventHandler(this.btnSAOrder_Click);
            // 
            // lblChungTuGocPT
            // 
            this.lblChungTuGocPT.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Center";
            appearance23.TextVAlignAsString = "Middle";
            this.lblChungTuGocPT.Appearance = appearance23;
            this.lblChungTuGocPT.Location = new System.Drawing.Point(839, 116);
            this.lblChungTuGocPT.Name = "lblChungTuGocPT";
            this.lblChungTuGocPT.Size = new System.Drawing.Size(79, 22);
            this.lblChungTuGocPT.TabIndex = 37;
            this.lblChungTuGocPT.Text = "Chứng từ gốc";
            // 
            // txtNumberAttachPT
            // 
            this.txtNumberAttachPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumberAttachPT.AutoSize = false;
            this.txtNumberAttachPT.Location = new System.Drawing.Point(128, 116);
            this.txtNumberAttachPT.Name = "txtNumberAttachPT";
            this.txtNumberAttachPT.Size = new System.Drawing.Size(706, 22);
            this.txtNumberAttachPT.TabIndex = 36;
            // 
            // lblNumberAttachPT
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextVAlignAsString = "Middle";
            this.lblNumberAttachPT.Appearance = appearance24;
            this.lblNumberAttachPT.Location = new System.Drawing.Point(6, 116);
            this.lblNumberAttachPT.Name = "lblNumberAttachPT";
            this.lblNumberAttachPT.Size = new System.Drawing.Size(115, 22);
            this.lblNumberAttachPT.TabIndex = 35;
            this.lblNumberAttachPT.Text = "Kèm theo";
            // 
            // txtAccountingObjectNamePT
            // 
            this.txtAccountingObjectNamePT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePT.AutoSize = false;
            this.txtAccountingObjectNamePT.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNamePT.Name = "txtAccountingObjectNamePT";
            this.txtAccountingObjectNamePT.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectNamePT.TabIndex = 32;
            this.txtAccountingObjectNamePT.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // cbbAccountingObjectIDPT
            // 
            this.cbbAccountingObjectIDPT.AutoSize = false;
            appearance25.Image = ((object)(resources.GetObject("appearance25.Image")));
            editorButton3.Appearance = appearance25;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDPT.ButtonsRight.Add(editorButton3);
            this.cbbAccountingObjectIDPT.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDPT.Location = new System.Drawing.Point(128, 20);
            this.cbbAccountingObjectIDPT.Name = "cbbAccountingObjectIDPT";
            this.cbbAccountingObjectIDPT.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDPT.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectIDPT.TabIndex = 31;
            // 
            // txtMReasonPayPT
            // 
            this.txtMReasonPayPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMReasonPayPT.AutoSize = false;
            this.txtMReasonPayPT.Location = new System.Drawing.Point(128, 92);
            this.txtMReasonPayPT.Name = "txtMReasonPayPT";
            this.txtMReasonPayPT.Size = new System.Drawing.Size(790, 22);
            this.txtMReasonPayPT.TabIndex = 27;
            // 
            // lblMReasonPayPT
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.lblMReasonPayPT.Appearance = appearance26;
            this.lblMReasonPayPT.Location = new System.Drawing.Point(6, 92);
            this.lblMReasonPayPT.Name = "lblMReasonPayPT";
            this.lblMReasonPayPT.Size = new System.Drawing.Size(115, 22);
            this.lblMReasonPayPT.TabIndex = 26;
            this.lblMReasonPayPT.Text = "Lý do nộp";
            // 
            // txtMContactName
            // 
            this.txtMContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMContactName.AutoSize = false;
            this.txtMContactName.Location = new System.Drawing.Point(128, 68);
            this.txtMContactName.Name = "txtMContactName";
            this.txtMContactName.Size = new System.Drawing.Size(790, 22);
            this.txtMContactName.TabIndex = 25;
            // 
            // lblMContactName
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextVAlignAsString = "Middle";
            this.lblMContactName.Appearance = appearance27;
            this.lblMContactName.Location = new System.Drawing.Point(6, 68);
            this.lblMContactName.Name = "lblMContactName";
            this.lblMContactName.Size = new System.Drawing.Size(115, 22);
            this.lblMContactName.TabIndex = 24;
            this.lblMContactName.Text = "Họ tên người nộp tiền";
            // 
            // txtAccountingObjectAddressPT
            // 
            this.txtAccountingObjectAddressPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPT.AutoSize = false;
            this.txtAccountingObjectAddressPT.Location = new System.Drawing.Point(128, 44);
            this.txtAccountingObjectAddressPT.Name = "txtAccountingObjectAddressPT";
            this.txtAccountingObjectAddressPT.Size = new System.Drawing.Size(790, 22);
            this.txtAccountingObjectAddressPT.TabIndex = 23;
            this.txtAccountingObjectAddressPT.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // lblDiaChiPT
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextVAlignAsString = "Middle";
            this.lblDiaChiPT.Appearance = appearance28;
            this.lblDiaChiPT.Location = new System.Drawing.Point(6, 44);
            this.lblDiaChiPT.Name = "lblDiaChiPT";
            this.lblDiaChiPT.Size = new System.Drawing.Size(115, 22);
            this.lblDiaChiPT.TabIndex = 22;
            this.lblDiaChiPT.Text = "Địa chỉ";
            // 
            // lblKhachHangPT
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextVAlignAsString = "Middle";
            this.lblKhachHangPT.Appearance = appearance29;
            this.lblKhachHangPT.Location = new System.Drawing.Point(6, 20);
            this.lblKhachHangPT.Name = "lblKhachHangPT";
            this.lblKhachHangPT.Size = new System.Drawing.Size(115, 22);
            this.lblKhachHangPT.TabIndex = 0;
            this.lblKhachHangPT.Text = "Khách hàng";
            // 
            // ultraTabPageGiayBaoCo
            // 
            this.ultraTabPageGiayBaoCo.Controls.Add(this.ultraGroupBoxGiayBaoCo);
            this.ultraTabPageGiayBaoCo.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageGiayBaoCo.Name = "ultraTabPageGiayBaoCo";
            this.ultraTabPageGiayBaoCo.Size = new System.Drawing.Size(927, 148);
            // 
            // ultraGroupBoxGiayBaoCo
            // 
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.ultraButton5);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.ultraButton6);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.txtBankName);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.cbbBankAccountDetailID);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.lblNopTienTaiKhoanGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.txtAccountingObjectNameGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.cbbAccountingObjectIDGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.txtMReasonPayGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.lblMReasonPayGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.txtAccountingObjectAddressGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.lblDiaChiGBC);
            this.ultraGroupBoxGiayBaoCo.Controls.Add(this.lblKhachHangGBC);
            this.ultraGroupBoxGiayBaoCo.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance36.FontData.BoldAsString = "True";
            appearance36.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxGiayBaoCo.HeaderAppearance = appearance36;
            this.ultraGroupBoxGiayBaoCo.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxGiayBaoCo.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxGiayBaoCo.Name = "ultraGroupBoxGiayBaoCo";
            this.ultraGroupBoxGiayBaoCo.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxGiayBaoCo.TabIndex = 29;
            this.ultraGroupBoxGiayBaoCo.Text = "Thông tin chung";
            this.ultraGroupBoxGiayBaoCo.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton5
            // 
            this.ultraButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton5.Location = new System.Drawing.Point(833, 20);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(85, 23);
            this.ultraButton5.TabIndex = 43;
            this.ultraButton5.Text = "Công nợ";
            this.ultraButton5.Click += new System.EventHandler(this.BtnCongNoClick);
            // 
            // ultraButton6
            // 
            this.ultraButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton6.Location = new System.Drawing.Point(719, 20);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(108, 23);
            this.ultraButton6.TabIndex = 42;
            this.ultraButton6.Text = "Đơn đặt hàng";
            this.ultraButton6.Click += new System.EventHandler(this.btnSAOrder_Click);
            // 
            // txtBankName
            // 
            this.txtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(290, 68);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(628, 22);
            this.txtBankName.TabIndex = 40;
            // 
            // cbbBankAccountDetailID
            // 
            this.cbbBankAccountDetailID.AutoSize = false;
            this.cbbBankAccountDetailID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbBankAccountDetailID.Location = new System.Drawing.Point(128, 68);
            this.cbbBankAccountDetailID.Name = "cbbBankAccountDetailID";
            this.cbbBankAccountDetailID.NullText = "<chọn dữ liệu>";
            this.cbbBankAccountDetailID.Size = new System.Drawing.Size(156, 22);
            this.cbbBankAccountDetailID.TabIndex = 39;
            // 
            // lblNopTienTaiKhoanGBC
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextVAlignAsString = "Middle";
            this.lblNopTienTaiKhoanGBC.Appearance = appearance31;
            this.lblNopTienTaiKhoanGBC.Location = new System.Drawing.Point(6, 68);
            this.lblNopTienTaiKhoanGBC.Name = "lblNopTienTaiKhoanGBC";
            this.lblNopTienTaiKhoanGBC.Size = new System.Drawing.Size(115, 22);
            this.lblNopTienTaiKhoanGBC.TabIndex = 38;
            this.lblNopTienTaiKhoanGBC.Text = "Nộp vào tài khoản";
            // 
            // txtAccountingObjectNameGBC
            // 
            this.txtAccountingObjectNameGBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameGBC.AutoSize = false;
            this.txtAccountingObjectNameGBC.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNameGBC.Name = "txtAccountingObjectNameGBC";
            this.txtAccountingObjectNameGBC.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectNameGBC.TabIndex = 32;
            this.txtAccountingObjectNameGBC.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // cbbAccountingObjectIDGBC
            // 
            this.cbbAccountingObjectIDGBC.AutoSize = false;
            appearance32.Image = ((object)(resources.GetObject("appearance32.Image")));
            editorButton4.Appearance = appearance32;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDGBC.ButtonsRight.Add(editorButton4);
            this.cbbAccountingObjectIDGBC.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDGBC.Location = new System.Drawing.Point(128, 20);
            this.cbbAccountingObjectIDGBC.Name = "cbbAccountingObjectIDGBC";
            this.cbbAccountingObjectIDGBC.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDGBC.Size = new System.Drawing.Size(156, 22);
            this.cbbAccountingObjectIDGBC.TabIndex = 31;
            // 
            // txtMReasonPayGBC
            // 
            this.txtMReasonPayGBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMReasonPayGBC.AutoSize = false;
            this.txtMReasonPayGBC.Location = new System.Drawing.Point(128, 92);
            this.txtMReasonPayGBC.Multiline = true;
            this.txtMReasonPayGBC.Name = "txtMReasonPayGBC";
            this.txtMReasonPayGBC.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMReasonPayGBC.Size = new System.Drawing.Size(790, 46);
            this.txtMReasonPayGBC.TabIndex = 27;
            // 
            // lblMReasonPayGBC
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextVAlignAsString = "Middle";
            this.lblMReasonPayGBC.Appearance = appearance33;
            this.lblMReasonPayGBC.Location = new System.Drawing.Point(6, 92);
            this.lblMReasonPayGBC.Name = "lblMReasonPayGBC";
            this.lblMReasonPayGBC.Size = new System.Drawing.Size(115, 21);
            this.lblMReasonPayGBC.TabIndex = 26;
            this.lblMReasonPayGBC.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddressGBC
            // 
            this.txtAccountingObjectAddressGBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressGBC.AutoSize = false;
            this.txtAccountingObjectAddressGBC.Location = new System.Drawing.Point(128, 44);
            this.txtAccountingObjectAddressGBC.Name = "txtAccountingObjectAddressGBC";
            this.txtAccountingObjectAddressGBC.Size = new System.Drawing.Size(790, 22);
            this.txtAccountingObjectAddressGBC.TabIndex = 23;
            this.txtAccountingObjectAddressGBC.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // lblDiaChiGBC
            // 
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextVAlignAsString = "Middle";
            this.lblDiaChiGBC.Appearance = appearance34;
            this.lblDiaChiGBC.Location = new System.Drawing.Point(6, 44);
            this.lblDiaChiGBC.Name = "lblDiaChiGBC";
            this.lblDiaChiGBC.Size = new System.Drawing.Size(115, 22);
            this.lblDiaChiGBC.TabIndex = 22;
            this.lblDiaChiGBC.Text = "Địa chỉ";
            // 
            // lblKhachHangGBC
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextVAlignAsString = "Middle";
            this.lblKhachHangGBC.Appearance = appearance35;
            this.lblKhachHangGBC.Location = new System.Drawing.Point(6, 20);
            this.lblKhachHangGBC.Name = "lblKhachHangGBC";
            this.lblKhachHangGBC.Size = new System.Drawing.Size(115, 22);
            this.lblKhachHangGBC.TabIndex = 0;
            this.lblKhachHangGBC.Text = "Khách hàng";
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(927, 148);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox2.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox2.Controls.Add(this.cbbLoaiHD);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox2.Controls.Add(this.cbbHinhThucHD);
            this.ultraGroupBox2.Controls.Add(this.txtSoHD);
            this.ultraGroupBox2.Controls.Add(this.txtKyHieuHD);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox2.Controls.Add(this.cbbMauHD);
            this.ultraGroupBox2.Controls.Add(this.dteNgayHD);
            this.ultraGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox2.Location = new System.Drawing.Point(614, 1);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(314, 148);
            this.ultraGroupBox2.TabIndex = 29;
            this.ultraGroupBox2.Text = "Hóa đơn";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance37;
            this.ultraLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel20.Location = new System.Drawing.Point(12, 44);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel20.TabIndex = 75;
            this.ultraLabel20.Text = "Loại HĐ";
            // 
            // cbbLoaiHD
            // 
            this.cbbLoaiHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoaiHD.AutoSize = false;
            this.cbbLoaiHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbLoaiHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbLoaiHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiHD.Location = new System.Drawing.Point(80, 44);
            this.cbbLoaiHD.Name = "cbbLoaiHD";
            this.cbbLoaiHD.Size = new System.Drawing.Size(228, 22);
            this.cbbLoaiHD.TabIndex = 15;
            // 
            // ultraLabel19
            // 
            this.ultraLabel19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance38;
            this.ultraLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel19.Location = new System.Drawing.Point(12, 20);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel19.TabIndex = 73;
            this.ultraLabel19.Text = "Hình thứcHĐ";
            // 
            // cbbHinhThucHD
            // 
            this.cbbHinhThucHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbHinhThucHD.AutoSize = false;
            this.cbbHinhThucHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbHinhThucHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbHinhThucHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbHinhThucHD.Location = new System.Drawing.Point(80, 20);
            this.cbbHinhThucHD.Name = "cbbHinhThucHD";
            this.cbbHinhThucHD.Size = new System.Drawing.Size(228, 22);
            this.cbbHinhThucHD.TabIndex = 14;
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(80, 118);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(82, 22);
            this.txtSoHD.TabIndex = 18;
            this.txtSoHD.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(80, 94);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(228, 22);
            this.txtKyHieuHD.TabIndex = 17;
            this.txtKyHieuHD.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChanged);
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance39;
            this.ultraLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel17.Location = new System.Drawing.Point(165, 119);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel17.TabIndex = 69;
            this.ultraLabel17.Text = "Ngày HĐ";
            // 
            // ultraLabel18
            // 
            this.ultraLabel18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance40;
            this.ultraLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel18.Location = new System.Drawing.Point(11, 118);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel18.TabIndex = 68;
            this.ultraLabel18.Text = "Số HĐ";
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance41;
            this.ultraLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel16.Location = new System.Drawing.Point(11, 93);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel16.TabIndex = 67;
            this.ultraLabel16.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance42;
            this.ultraLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel15.Location = new System.Drawing.Point(13, 72);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel15.TabIndex = 66;
            this.ultraLabel15.Text = "Mẫu số HĐ";
            // 
            // cbbMauHD
            // 
            this.cbbMauHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMauHD.AutoSize = false;
            this.cbbMauHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbMauHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbMauHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMauHD.Location = new System.Drawing.Point(80, 69);
            this.cbbMauHD.Name = "cbbMauHD";
            this.cbbMauHD.Size = new System.Drawing.Size(228, 22);
            this.cbbMauHD.TabIndex = 16;
            // 
            // dteNgayHD
            // 
            appearance43.TextHAlignAsString = "Center";
            appearance43.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance43;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(218, 119);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(90, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            this.dteNgayHD.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            this.dteNgayHD.Leave += new System.EventHandler(this.dteNgayHD_Leave);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.uComboThanhToan);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingBankName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectBankAccount);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox1.Controls.Add(this.dteListDateHD);
            this.ultraGroupBox1.Controls.Add(this.txtListCommonNameInventoryHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.txtListNoHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.chkisAttachListHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectNameHD);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectIDHD);
            this.ultraGroupBox1.Controls.Add(this.txtCompanyTaxCodeHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.txtReasonHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.txtContactNameHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddressHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel13);
            appearance59.FontData.BoldAsString = "True";
            appearance59.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance59;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(615, 148);
            this.ultraGroupBox1.TabIndex = 27;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uComboThanhToan
            // 
            this.uComboThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uComboThanhToan.AutoSize = false;
            this.uComboThanhToan.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboThanhToan.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.uComboThanhToan.Location = new System.Drawing.Point(486, 92);
            this.uComboThanhToan.Name = "uComboThanhToan";
            this.uComboThanhToan.Size = new System.Drawing.Size(122, 22);
            this.uComboThanhToan.TabIndex = 9;
            this.uComboThanhToan.TextChanged += new System.EventHandler(this.txtThanhToan_TextChanged);
            // 
            // txtAccountingBankName
            // 
            this.txtAccountingBankName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingBankName.AutoSize = false;
            this.txtAccountingBankName.Location = new System.Drawing.Point(486, 68);
            this.txtAccountingBankName.Name = "txtAccountingBankName";
            this.txtAccountingBankName.Size = new System.Drawing.Size(122, 22);
            this.txtAccountingBankName.TabIndex = 7;
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance44;
            this.ultraLabel21.Location = new System.Drawing.Point(373, 68);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(108, 22);
            this.ultraLabel21.TabIndex = 85;
            this.ultraLabel21.Text = "Tên ngân hàng";
            // 
            // cbbAccountingObjectBankAccount
            // 
            this.cbbAccountingObjectBankAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbAccountingObjectBankAccount.AutoSize = false;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(245, 68);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(123, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 6;
            // 
            // ultraLabel33
            // 
            this.ultraLabel33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance45;
            this.ultraLabel33.Location = new System.Drawing.Point(163, 68);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(76, 21);
            this.ultraLabel33.TabIndex = 63;
            this.ultraLabel33.Text = "TK ngân hàng";
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance46;
            this.ultraLabel14.Location = new System.Drawing.Point(374, 92);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(108, 22);
            this.ultraLabel14.TabIndex = 43;
            this.ultraLabel14.Text = "Hình thức thanh toán";
            // 
            // dteListDateHD
            // 
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            this.dteListDateHD.Appearance = appearance47;
            this.dteListDateHD.AutoSize = false;
            this.dteListDateHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteListDateHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteListDateHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteListDateHD.Location = new System.Drawing.Point(277, 118);
            this.dteListDateHD.MaskInput = "";
            this.dteListDateHD.Name = "dteListDateHD";
            this.dteListDateHD.Size = new System.Drawing.Size(91, 22);
            this.dteListDateHD.TabIndex = 12;
            this.dteListDateHD.Value = null;
            // 
            // txtListCommonNameInventoryHD
            // 
            this.txtListCommonNameInventoryHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtListCommonNameInventoryHD.AutoSize = false;
            this.txtListCommonNameInventoryHD.Location = new System.Drawing.Point(486, 118);
            this.txtListCommonNameInventoryHD.Name = "txtListCommonNameInventoryHD";
            this.txtListCommonNameInventoryHD.Size = new System.Drawing.Size(122, 22);
            this.txtListCommonNameInventoryHD.TabIndex = 13;
            // 
            // ultraLabel5
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance48;
            this.ultraLabel5.Location = new System.Drawing.Point(397, 116);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(101, 22);
            this.ultraLabel5.TabIndex = 41;
            this.ultraLabel5.Text = "Mặt hàng chung";
            // 
            // ultraLabel6
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance49;
            this.ultraLabel6.Location = new System.Drawing.Point(245, 117);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(47, 22);
            this.ultraLabel6.TabIndex = 39;
            this.ultraLabel6.Text = "Ngày";
            // 
            // txtListNoHD
            // 
            this.txtListNoHD.AutoSize = false;
            this.txtListNoHD.Location = new System.Drawing.Point(156, 118);
            this.txtListNoHD.Name = "txtListNoHD";
            this.txtListNoHD.Size = new System.Drawing.Size(83, 22);
            this.txtListNoHD.TabIndex = 11;
            // 
            // ultraLabel7
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance50;
            this.ultraLabel7.Location = new System.Drawing.Point(127, 116);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(23, 22);
            this.ultraLabel7.TabIndex = 37;
            this.ultraLabel7.Text = "Số";
            // 
            // chkisAttachListHD
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance51.TextHAlignAsString = "Center";
            appearance51.TextVAlignAsString = "Middle";
            this.chkisAttachListHD.Appearance = appearance51;
            this.chkisAttachListHD.BackColor = System.Drawing.Color.Transparent;
            this.chkisAttachListHD.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkisAttachListHD.Location = new System.Drawing.Point(107, 118);
            this.chkisAttachListHD.Name = "chkisAttachListHD";
            this.chkisAttachListHD.Size = new System.Drawing.Size(14, 22);
            this.chkisAttachListHD.TabIndex = 10;
            // 
            // ultraLabel8
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance52;
            this.ultraLabel8.Location = new System.Drawing.Point(6, 117);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel8.TabIndex = 35;
            this.ultraLabel8.Text = "In kèm bảng kê";
            // 
            // txtAccountingObjectNameHD
            // 
            this.txtAccountingObjectNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameHD.AutoSize = false;
            this.txtAccountingObjectNameHD.Location = new System.Drawing.Point(245, 20);
            this.txtAccountingObjectNameHD.Name = "txtAccountingObjectNameHD";
            this.txtAccountingObjectNameHD.Size = new System.Drawing.Size(363, 22);
            this.txtAccountingObjectNameHD.TabIndex = 2;
            this.txtAccountingObjectNameHD.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // cbbAccountingObjectIDHD
            // 
            this.cbbAccountingObjectIDHD.AutoSize = false;
            appearance53.Image = ((object)(resources.GetObject("appearance53.Image")));
            editorButton5.Appearance = appearance53;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDHD.ButtonsRight.Add(editorButton5);
            this.cbbAccountingObjectIDHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDHD.Location = new System.Drawing.Point(107, 20);
            this.cbbAccountingObjectIDHD.Name = "cbbAccountingObjectIDHD";
            this.cbbAccountingObjectIDHD.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDHD.Size = new System.Drawing.Size(132, 22);
            this.cbbAccountingObjectIDHD.TabIndex = 1;
            // 
            // txtCompanyTaxCodeHD
            // 
            this.txtCompanyTaxCodeHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyTaxCodeHD.AutoSize = false;
            this.txtCompanyTaxCodeHD.Location = new System.Drawing.Point(486, 44);
            this.txtCompanyTaxCodeHD.Name = "txtCompanyTaxCodeHD";
            this.txtCompanyTaxCodeHD.Size = new System.Drawing.Size(122, 22);
            this.txtCompanyTaxCodeHD.TabIndex = 4;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance54;
            this.ultraLabel9.Location = new System.Drawing.Point(375, 44);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(108, 22);
            this.ultraLabel9.TabIndex = 28;
            this.ultraLabel9.Text = "Mã số thuế";
            // 
            // txtReasonHD
            // 
            this.txtReasonHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonHD.AutoSize = false;
            this.txtReasonHD.Location = new System.Drawing.Point(107, 92);
            this.txtReasonHD.Name = "txtReasonHD";
            this.txtReasonHD.Size = new System.Drawing.Size(261, 22);
            this.txtReasonHD.TabIndex = 8;
            // 
            // ultraLabel10
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance55;
            this.ultraLabel10.Location = new System.Drawing.Point(6, 92);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel10.TabIndex = 26;
            this.ultraLabel10.Text = "Diễn giải";
            // 
            // txtContactNameHD
            // 
            this.txtContactNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactNameHD.AutoSize = false;
            this.txtContactNameHD.Location = new System.Drawing.Point(107, 68);
            this.txtContactNameHD.Name = "txtContactNameHD";
            this.txtContactNameHD.Size = new System.Drawing.Size(47, 22);
            this.txtContactNameHD.TabIndex = 5;
            // 
            // ultraLabel11
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance56;
            this.ultraLabel11.Location = new System.Drawing.Point(5, 68);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(107, 22);
            this.ultraLabel11.TabIndex = 24;
            this.ultraLabel11.Text = "Họ tên người liên hệ";
            // 
            // txtAccountingObjectAddressHD
            // 
            this.txtAccountingObjectAddressHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressHD.AutoSize = false;
            this.txtAccountingObjectAddressHD.Location = new System.Drawing.Point(107, 44);
            this.txtAccountingObjectAddressHD.Name = "txtAccountingObjectAddressHD";
            this.txtAccountingObjectAddressHD.Size = new System.Drawing.Size(261, 22);
            this.txtAccountingObjectAddressHD.TabIndex = 3;
            this.txtAccountingObjectAddressHD.TextChanged += new System.EventHandler(this.txtAccountingObjectAddress_TextChanged);
            // 
            // ultraLabel12
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance57;
            this.ultraLabel12.Location = new System.Drawing.Point(6, 44);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel12.TabIndex = 22;
            this.ultraLabel12.Text = "Địa chỉ";
            // 
            // ultraLabel13
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            appearance58.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance58;
            this.ultraLabel13.Location = new System.Drawing.Point(6, 19);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(95, 23);
            this.ultraLabel13.TabIndex = 0;
            this.ultraLabel13.Text = "Khách hàng ";
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.ultraGroupBoxTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(929, 82);
            this.palTop.TabIndex = 27;
            // 
            // ultraGroupBoxTop
            // 
            this.ultraGroupBoxTop.Controls.Add(this.palVouchersNoHd);
            this.ultraGroupBoxTop.Controls.Add(this.palVouchersNoPxk);
            this.ultraGroupBoxTop.Controls.Add(this.palVouchersNoTt);
            this.ultraGroupBoxTop.Controls.Add(this.palVouchersNoGbc);
            this.ultraGroupBoxTop.Controls.Add(this.palChung);
            this.ultraGroupBoxTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance68.FontData.BoldAsString = "True";
            appearance68.FontData.SizeInPoints = 13F;
            this.ultraGroupBoxTop.HeaderAppearance = appearance68;
            this.ultraGroupBoxTop.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxTop.Name = "ultraGroupBoxTop";
            this.ultraGroupBoxTop.Size = new System.Drawing.Size(929, 82);
            this.ultraGroupBoxTop.TabIndex = 31;
            this.ultraGroupBoxTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palVouchersNoHd
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            this.palVouchersNoHd.Appearance = appearance60;
            this.palVouchersNoHd.Location = new System.Drawing.Point(12, 13);
            this.palVouchersNoHd.Name = "palVouchersNoHd";
            this.palVouchersNoHd.Size = new System.Drawing.Size(316, 50);
            this.palVouchersNoHd.TabIndex = 40;
            this.palVouchersNoHd.TabStop = false;
            // 
            // palVouchersNoPxk
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            this.palVouchersNoPxk.Appearance = appearance61;
            this.palVouchersNoPxk.Location = new System.Drawing.Point(12, 13);
            this.palVouchersNoPxk.Name = "palVouchersNoPxk";
            this.palVouchersNoPxk.Size = new System.Drawing.Size(316, 50);
            this.palVouchersNoPxk.TabIndex = 43;
            // 
            // palVouchersNoTt
            // 
            appearance62.BackColor = System.Drawing.Color.Transparent;
            this.palVouchersNoTt.Appearance = appearance62;
            this.palVouchersNoTt.Location = new System.Drawing.Point(12, 13);
            this.palVouchersNoTt.Name = "palVouchersNoTt";
            this.palVouchersNoTt.Size = new System.Drawing.Size(316, 50);
            this.palVouchersNoTt.TabIndex = 42;
            // 
            // palVouchersNoGbc
            // 
            appearance63.BackColor = System.Drawing.Color.Transparent;
            this.palVouchersNoGbc.Appearance = appearance63;
            this.palVouchersNoGbc.Location = new System.Drawing.Point(12, 13);
            this.palVouchersNoGbc.Name = "palVouchersNoGbc";
            this.palVouchersNoGbc.Size = new System.Drawing.Size(316, 50);
            this.palVouchersNoGbc.TabIndex = 41;
            // 
            // palChung
            // 
            this.palChung.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.palChung.BackColor = System.Drawing.Color.Transparent;
            this.palChung.Controls.Add(this.optHoaDon);
            this.palChung.Controls.Add(this.optThanhToan);
            this.palChung.Controls.Add(this.lblTT);
            this.palChung.Controls.Add(this.optPhieuXuatKho);
            this.palChung.Controls.Add(this.btnOutwardNo);
            this.palChung.Controls.Add(this.cbbChonThanhToan);
            this.palChung.Location = new System.Drawing.Point(499, 2);
            this.palChung.Name = "palChung";
            this.palChung.Size = new System.Drawing.Size(420, 77);
            this.palChung.TabIndex = 39;
            // 
            // optHoaDon
            // 
            this.optHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance64.TextHAlignAsString = "Left";
            appearance64.TextVAlignAsString = "Middle";
            this.optHoaDon.Appearance = appearance64;
            this.optHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.optHoaDon.BackColorInternal = System.Drawing.Color.Transparent;
            this.optHoaDon.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem7.DataValue = true;
            valueListItem7.DisplayText = "Lập kèm hóa đơn";
            valueListItem7.Tag = "KHD";
            valueListItem8.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem8.DataValue = false;
            valueListItem8.DisplayText = "Không kèm hóa đơn";
            valueListItem8.Tag = "KKHD";
            this.optHoaDon.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.optHoaDon.ItemSpacingHorizontal = 17;
            this.optHoaDon.Location = new System.Drawing.Point(3, 57);
            this.optHoaDon.Name = "optHoaDon";
            this.optHoaDon.ReadOnly = false;
            this.optHoaDon.Size = new System.Drawing.Size(288, 17);
            this.optHoaDon.TabIndex = 46;
            this.optHoaDon.TabStop = false;
            this.optHoaDon.ValueChanged += new System.EventHandler(this.optHoaDon_ValueChanged);
            // 
            // optThanhToan
            // 
            appearance65.TextHAlignAsString = "Left";
            appearance65.TextVAlignAsString = "Middle";
            this.optThanhToan.Appearance = appearance65;
            this.optThanhToan.BackColor = System.Drawing.Color.Transparent;
            this.optThanhToan.BackColorInternal = System.Drawing.Color.Transparent;
            this.optThanhToan.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem9.DataValue = false;
            valueListItem9.DisplayText = "Chưa thanh toán";
            valueListItem9.Tag = "CTT";
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = true;
            valueListItem1.DisplayText = "Thanh toán ngay";
            valueListItem1.Tag = "TTN";
            this.optThanhToan.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem1});
            this.optThanhToan.ItemSpacingHorizontal = 20;
            this.optThanhToan.Location = new System.Drawing.Point(3, 7);
            this.optThanhToan.Name = "optThanhToan";
            this.optThanhToan.ReadOnly = false;
            this.optThanhToan.Size = new System.Drawing.Size(288, 20);
            this.optThanhToan.TabIndex = 44;
            this.optThanhToan.TabStop = false;
            this.optThanhToan.ValueChanged += new System.EventHandler(this.OptThanhToanValueChangedStand);
            // 
            // lblTT
            // 
            appearance66.TextVAlignAsString = "Middle";
            this.lblTT.Appearance = appearance66;
            this.lblTT.Location = new System.Drawing.Point(159, 3);
            this.lblTT.Name = "lblTT";
            this.lblTT.Size = new System.Drawing.Size(132, 22);
            this.lblTT.TabIndex = 45;
            this.lblTT.Text = "Phương thức thanh toán";
            // 
            // optPhieuXuatKho
            // 
            this.optPhieuXuatKho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance67.TextHAlignAsString = "Left";
            appearance67.TextVAlignAsString = "Middle";
            this.optPhieuXuatKho.Appearance = appearance67;
            this.optPhieuXuatKho.BackColor = System.Drawing.Color.Transparent;
            this.optPhieuXuatKho.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPhieuXuatKho.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem3.DataValue = true;
            valueListItem3.DisplayText = "Kiêm phiếu xuất kho";
            valueListItem3.Tag = "KPXK";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Không kiêm phiếu xuất kho";
            valueListItem4.Tag = "KKPXK";
            this.optPhieuXuatKho.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.optPhieuXuatKho.ItemSpacingHorizontal = 1;
            this.optPhieuXuatKho.Location = new System.Drawing.Point(3, 33);
            this.optPhieuXuatKho.Name = "optPhieuXuatKho";
            this.optPhieuXuatKho.ReadOnly = false;
            this.optPhieuXuatKho.Size = new System.Drawing.Size(288, 17);
            this.optPhieuXuatKho.TabIndex = 44;
            this.optPhieuXuatKho.TabStop = false;
            this.optPhieuXuatKho.ValueChanged += new System.EventHandler(this.OptPhieuXuatKhoValueChangedStand);
            // 
            // btnOutwardNo
            // 
            this.btnOutwardNo.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnOutwardNo.Location = new System.Drawing.Point(297, 27);
            this.btnOutwardNo.Name = "btnOutwardNo";
            this.btnOutwardNo.Size = new System.Drawing.Size(119, 23);
            this.btnOutwardNo.TabIndex = 35;
            this.btnOutwardNo.Text = "Phiếu xuất";
            this.btnOutwardNo.Click += new System.EventHandler(this.BtnOutwardNoClick);
            // 
            // cbbChonThanhToan
            // 
            this.cbbChonThanhToan.AutoSize = false;
            this.cbbChonThanhToan.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem6.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem6.DataValue = 321;
            valueListItem6.DisplayText = "Tiền mặt";
            valueListItem6.Tag = "tabPhieuThu";
            valueListItem12.DataValue = 322;
            valueListItem12.DisplayText = "Chuyển khoản";
            valueListItem12.Tag = "tabGiayBaoCo";
            this.cbbChonThanhToan.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem12});
            this.cbbChonThanhToan.Location = new System.Drawing.Point(298, 3);
            this.cbbChonThanhToan.Name = "cbbChonThanhToan";
            this.cbbChonThanhToan.Size = new System.Drawing.Size(117, 22);
            this.cbbChonThanhToan.TabIndex = 36;
            this.cbbChonThanhToan.TabStop = false;
            this.cbbChonThanhToan.Visible = false;
            this.cbbChonThanhToan.ValueChanged += new System.EventHandler(this.CbbChonThanhToanValueChangedStand);
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.ultraSplitter1);
            this.palFill.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 82);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(929, 518);
            this.palFill.TabIndex = 28;
            // 
            // palGrid
            // 
            this.palGrid.Controls.Add(this.pnlUgrid);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 181);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(929, 337);
            this.palGrid.TabIndex = 28;
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 0);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(929, 337);
            this.pnlUgrid.TabIndex = 20;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance69.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance69;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(833, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.TabStop = false;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 171);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 171;
            this.ultraSplitter1.Size = new System.Drawing.Size(929, 10);
            this.ultraSplitter1.TabIndex = 2;
            // 
            // palThongTinChung
            // 
            this.palThongTinChung.Controls.Add(this.group);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(929, 171);
            this.palThongTinChung.TabIndex = 27;
            // 
            // group
            // 
            this.group.Controls.Add(this.ultraTabSharedControlsPage2);
            this.group.Controls.Add(this.ultraTabPageHoaDon);
            this.group.Controls.Add(this.ultraTabPagePhieuXuatKho);
            this.group.Controls.Add(this.ultraTabPagePhieuThu);
            this.group.Controls.Add(this.ultraTabPageGiayBaoCo);
            this.group.Controls.Add(this.ultraTabPageControl1);
            this.group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group.Location = new System.Drawing.Point(0, 0);
            this.group.Name = "group";
            this.group.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.group.Size = new System.Drawing.Size(929, 171);
            this.group.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.group.TabIndex = 27;
            ultraTab4.Key = "tabHoaDon";
            ultraTab4.TabPage = this.ultraTabPageHoaDon;
            ultraTab4.Text = "Chứng từ bán hàng";
            ultraTab5.Key = "tabPhieuXuatKho";
            ultraTab5.TabPage = this.ultraTabPagePhieuXuatKho;
            ultraTab5.Text = "Phiếu xuất kho";
            ultraTab5.Visible = false;
            ultraTab6.Key = "tabPhieuThu";
            ultraTab6.TabPage = this.ultraTabPagePhieuThu;
            ultraTab6.Text = "Phiếu thu";
            ultraTab6.Visible = false;
            ultraTab7.Key = "tabGiayBaoCo";
            ultraTab7.TabPage = this.ultraTabPageGiayBaoCo;
            ultraTab7.Text = "Giấy báo có";
            ultraTab7.Visible = false;
            ultraTab1.Key = "tabHoaDon1";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Hóa đơn";
            ultraTab1.Visible = false;
            this.group.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab7,
            ultraTab1});
            this.group.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.group.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControlThongTinChung_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(927, 148);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.panel1);
            this.palBottom.Controls.Add(this.btnSelectBill);
            this.palBottom.Controls.Add(this.cbBill);
            this.palBottom.Controls.Add(this.lblBillDeleted);
            this.palBottom.Controls.Add(this.txtTotalDiscountAmountSubOriginal);
            this.palBottom.Controls.Add(this.txtTotalDiscountAmountSub);
            this.palBottom.Controls.Add(this.ultraLabel3);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountOriginalStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmount);
            this.palBottom.Controls.Add(this.txtTotalAmount);
            this.palBottom.Controls.Add(this.lblTotalVATAmount);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmount);
            this.palBottom.Controls.Add(this.lblTotalAmount);
            this.palBottom.Controls.Add(this.btnIsDiscountByVoucher);
            this.palBottom.Controls.Add(this.chkIsDiscountByVoucher);
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 600);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(929, 137);
            this.palBottom.TabIndex = 27;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtOldInvSeries);
            this.panel1.Controls.Add(this.ultraLabel4);
            this.panel1.Controls.Add(this.txtOldInvTemplate);
            this.panel1.Controls.Add(this.ultraLabel2);
            this.panel1.Controls.Add(this.txtOldInvDate);
            this.panel1.Controls.Add(this.ultraLabel1);
            this.panel1.Controls.Add(this.txtOldInvNo);
            this.panel1.Controls.Add(this.lblTttt);
            this.panel1.Location = new System.Drawing.Point(5, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 34);
            this.panel1.TabIndex = 61;
            // 
            // txtOldInvSeries
            // 
            this.txtOldInvSeries.AutoSize = false;
            this.txtOldInvSeries.Location = new System.Drawing.Point(482, 1);
            this.txtOldInvSeries.Name = "txtOldInvSeries";
            this.txtOldInvSeries.ReadOnly = true;
            this.txtOldInvSeries.Size = new System.Drawing.Size(100, 22);
            this.txtOldInvSeries.TabIndex = 49;
            // 
            // ultraLabel4
            // 
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance70;
            this.ultraLabel4.Location = new System.Drawing.Point(433, 3);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(43, 19);
            this.ultraLabel4.TabIndex = 48;
            this.ultraLabel4.Text = "Ký hiệu";
            // 
            // txtOldInvTemplate
            // 
            this.txtOldInvTemplate.AutoSize = false;
            this.txtOldInvTemplate.Location = new System.Drawing.Point(327, 1);
            this.txtOldInvTemplate.Name = "txtOldInvTemplate";
            this.txtOldInvTemplate.ReadOnly = true;
            this.txtOldInvTemplate.Size = new System.Drawing.Size(100, 22);
            this.txtOldInvTemplate.TabIndex = 47;
            // 
            // ultraLabel2
            // 
            appearance71.BackColor = System.Drawing.Color.Transparent;
            appearance71.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance71;
            this.ultraLabel2.Location = new System.Drawing.Point(283, 3);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(44, 19);
            this.ultraLabel2.TabIndex = 46;
            this.ultraLabel2.Text = "Mẫu số";
            // 
            // txtOldInvDate
            // 
            this.txtOldInvDate.AutoSize = false;
            this.txtOldInvDate.Location = new System.Drawing.Point(176, 1);
            this.txtOldInvDate.Name = "txtOldInvDate";
            this.txtOldInvDate.ReadOnly = true;
            this.txtOldInvDate.Size = new System.Drawing.Size(100, 22);
            this.txtOldInvDate.TabIndex = 45;
            // 
            // ultraLabel1
            // 
            appearance72.BackColor = System.Drawing.Color.Transparent;
            appearance72.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance72;
            this.ultraLabel1.Location = new System.Drawing.Point(135, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(37, 19);
            this.ultraLabel1.TabIndex = 44;
            this.ultraLabel1.Text = "Ngày";
            // 
            // txtOldInvNo
            // 
            this.txtOldInvNo.AutoSize = false;
            this.txtOldInvNo.Location = new System.Drawing.Point(27, 1);
            this.txtOldInvNo.Name = "txtOldInvNo";
            this.txtOldInvNo.ReadOnly = true;
            this.txtOldInvNo.Size = new System.Drawing.Size(100, 22);
            this.txtOldInvNo.TabIndex = 43;
            // 
            // lblTttt
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextVAlignAsString = "Middle";
            this.lblTttt.Appearance = appearance73;
            this.lblTttt.Location = new System.Drawing.Point(3, 3);
            this.lblTttt.Name = "lblTttt";
            this.lblTttt.Size = new System.Drawing.Size(30, 19);
            this.lblTttt.TabIndex = 26;
            this.lblTttt.Text = "Số";
            // 
            // btnSelectBill
            // 
            this.btnSelectBill.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnSelectBill.Enabled = false;
            this.btnSelectBill.Location = new System.Drawing.Point(171, 36);
            this.btnSelectBill.Name = "btnSelectBill";
            this.btnSelectBill.Size = new System.Drawing.Size(85, 23);
            this.btnSelectBill.TabIndex = 59;
            this.btnSelectBill.Text = "Chọn HĐ";
            this.btnSelectBill.Click += new System.EventHandler(this.btnSelectBill_Click);
            // 
            // cbBill
            // 
            this.cbBill.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbBill.Location = new System.Drawing.Point(9, 36);
            this.cbBill.Name = "cbBill";
            this.cbBill.Size = new System.Drawing.Size(156, 22);
            this.cbBill.TabIndex = 23;
            this.cbBill.Text = "Đã lập hóa đơn";
            this.cbBill.CheckStateChanged += new System.EventHandler(this.cbBill_CheckStateChanged);
            // 
            // lblBillDeleted
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            appearance74.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance74.BorderColor = System.Drawing.Color.DarkRed;
            appearance74.BorderColor2 = System.Drawing.Color.DarkRed;
            appearance74.BorderColor3DBase = System.Drawing.Color.DarkRed;
            appearance74.FontData.BoldAsString = "True";
            appearance74.ForeColor = System.Drawing.Color.DarkRed;
            appearance74.ForeColorDisabled = System.Drawing.Color.IndianRed;
            appearance74.TextHAlignAsString = "Center";
            appearance74.TextVAlignAsString = "Middle";
            this.lblBillDeleted.Appearance = appearance74;
            this.lblBillDeleted.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.TwoColor;
            this.lblBillDeleted.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.TwoColor;
            this.lblBillDeleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillDeleted.Location = new System.Drawing.Point(9, 95);
            this.lblBillDeleted.Name = "lblBillDeleted";
            this.lblBillDeleted.Size = new System.Drawing.Size(163, 38);
            this.lblBillDeleted.TabIndex = 57;
            this.lblBillDeleted.Text = "ĐÃ XÓA HÓA ĐƠN";
            this.lblBillDeleted.Visible = false;
            // 
            // txtTotalDiscountAmountSubOriginal
            // 
            this.txtTotalDiscountAmountSubOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance75.BackColor = System.Drawing.Color.Transparent;
            appearance75.TextHAlignAsString = "Right";
            appearance75.TextVAlignAsString = "Middle";
            this.txtTotalDiscountAmountSubOriginal.Appearance = appearance75;
            this.txtTotalDiscountAmountSubOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiscountAmountSubOriginal.Location = new System.Drawing.Point(584, 27);
            this.txtTotalDiscountAmountSubOriginal.Name = "txtTotalDiscountAmountSubOriginal";
            this.txtTotalDiscountAmountSubOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalDiscountAmountSubOriginal.TabIndex = 56;
            // 
            // txtTotalDiscountAmountSub
            // 
            this.txtTotalDiscountAmountSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance76.BackColor = System.Drawing.Color.Transparent;
            appearance76.TextHAlignAsString = "Right";
            appearance76.TextVAlignAsString = "Middle";
            this.txtTotalDiscountAmountSub.Appearance = appearance76;
            this.txtTotalDiscountAmountSub.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiscountAmountSub.Location = new System.Drawing.Point(759, 27);
            this.txtTotalDiscountAmountSub.Name = "txtTotalDiscountAmountSub";
            this.txtTotalDiscountAmountSub.Size = new System.Drawing.Size(169, 19);
            this.txtTotalDiscountAmountSub.TabIndex = 55;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance77;
            this.ultraLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(440, 28);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel3.TabIndex = 54;
            this.ultraLabel3.Text = "Tiền chiết khấu";
            // 
            // txtTotalPaymentAmountOriginalStand
            // 
            this.txtTotalPaymentAmountOriginalStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextHAlignAsString = "Right";
            appearance78.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountOriginalStand.Appearance = appearance78;
            this.txtTotalPaymentAmountOriginalStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountOriginalStand.Location = new System.Drawing.Point(584, 69);
            this.txtTotalPaymentAmountOriginalStand.Name = "txtTotalPaymentAmountOriginalStand";
            this.txtTotalPaymentAmountOriginalStand.Size = new System.Drawing.Size(169, 19);
            this.txtTotalPaymentAmountOriginalStand.TabIndex = 53;
            // 
            // txtTotalVATAmountOriginal
            // 
            this.txtTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.TextHAlignAsString = "Right";
            appearance79.TextVAlignAsString = "Middle";
            this.txtTotalVATAmountOriginal.Appearance = appearance79;
            this.txtTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmountOriginal.Location = new System.Drawing.Point(584, 48);
            this.txtTotalVATAmountOriginal.Name = "txtTotalVATAmountOriginal";
            this.txtTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalVATAmountOriginal.TabIndex = 52;
            // 
            // txtTotalAmountOriginal
            // 
            this.txtTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance80.BackColor = System.Drawing.Color.Transparent;
            appearance80.TextHAlignAsString = "Right";
            appearance80.TextVAlignAsString = "Middle";
            this.txtTotalAmountOriginal.Appearance = appearance80;
            this.txtTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmountOriginal.Location = new System.Drawing.Point(584, 6);
            this.txtTotalAmountOriginal.Name = "txtTotalAmountOriginal";
            this.txtTotalAmountOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalAmountOriginal.TabIndex = 51;
            // 
            // txtTotalPaymentAmountStand
            // 
            this.txtTotalPaymentAmountStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance81.BackColor = System.Drawing.Color.Transparent;
            appearance81.TextHAlignAsString = "Right";
            appearance81.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountStand.Appearance = appearance81;
            this.txtTotalPaymentAmountStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountStand.Location = new System.Drawing.Point(759, 69);
            this.txtTotalPaymentAmountStand.Name = "txtTotalPaymentAmountStand";
            this.txtTotalPaymentAmountStand.Size = new System.Drawing.Size(169, 19);
            this.txtTotalPaymentAmountStand.TabIndex = 50;
            // 
            // txtTotalVATAmount
            // 
            this.txtTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance82.BackColor = System.Drawing.Color.Transparent;
            appearance82.TextHAlignAsString = "Right";
            appearance82.TextVAlignAsString = "Middle";
            this.txtTotalVATAmount.Appearance = appearance82;
            this.txtTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmount.Location = new System.Drawing.Point(759, 48);
            this.txtTotalVATAmount.Name = "txtTotalVATAmount";
            this.txtTotalVATAmount.Size = new System.Drawing.Size(169, 19);
            this.txtTotalVATAmount.TabIndex = 49;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance83.BackColor = System.Drawing.Color.Transparent;
            appearance83.TextHAlignAsString = "Right";
            appearance83.TextVAlignAsString = "Middle";
            this.txtTotalAmount.Appearance = appearance83;
            this.txtTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(759, 6);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(169, 19);
            this.txtTotalAmount.TabIndex = 48;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance84.BackColor = System.Drawing.Color.Transparent;
            appearance84.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance84;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(440, 49);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalVATAmount.TabIndex = 47;
            this.lblTotalVATAmount.Text = "Tổng tiền thuế GTGT";
            // 
            // lblTotalPaymentAmount
            // 
            this.lblTotalPaymentAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance85.BackColor = System.Drawing.Color.Transparent;
            appearance85.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmount.Appearance = appearance85;
            this.lblTotalPaymentAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(440, 70);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalPaymentAmount.TabIndex = 46;
            this.lblTotalPaymentAmount.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance86.BackColor = System.Drawing.Color.Transparent;
            appearance86.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance86;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(440, 6);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalAmount.TabIndex = 45;
            this.lblTotalAmount.Text = "Tổng tiền hàng";
            // 
            // btnIsDiscountByVoucher
            // 
            this.btnIsDiscountByVoucher.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnIsDiscountByVoucher.Location = new System.Drawing.Point(171, 8);
            this.btnIsDiscountByVoucher.Name = "btnIsDiscountByVoucher";
            this.btnIsDiscountByVoucher.Size = new System.Drawing.Size(85, 23);
            this.btnIsDiscountByVoucher.TabIndex = 43;
            this.btnIsDiscountByVoucher.Text = "Phân bổ";
            this.btnIsDiscountByVoucher.Click += new System.EventHandler(this.BtnIsDiscountByVoucherClick);
            // 
            // chkIsDiscountByVoucher
            // 
            this.chkIsDiscountByVoucher.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.chkIsDiscountByVoucher.Location = new System.Drawing.Point(9, 8);
            this.chkIsDiscountByVoucher.Name = "chkIsDiscountByVoucher";
            this.chkIsDiscountByVoucher.Size = new System.Drawing.Size(156, 22);
            this.chkIsDiscountByVoucher.TabIndex = 22;
            this.chkIsDiscountByVoucher.Text = "Chiết khấu theo hóa đơn";
            this.chkIsDiscountByVoucher.CheckStateChanged += new System.EventHandler(this.chkIsDiscountByVoucher_CheckStateChanged);
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridControl.DisplayLayout.MaxRowScrollRegions = 1;
            this.uGridControl.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.None;
            this.uGridControl.Location = new System.Drawing.Point(440, 93);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(488, 43);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            this.uGridControl.AfterExitEditMode += new System.EventHandler(this.uGridControl_AfterExitEditMode);
            this.uGridControl.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridControl_CellChange);
            // 
            // FSAInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 737);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(945, 726);
            this.Name = "FSAInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSAInvoiceDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSAInvoiceDetail_FormClosed);
            this.ultraTabPageHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).EndInit();
            this.ultraGroupBoxHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dteListDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.ultraTabPagePhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).EndInit();
            this.ultraGroupBoxPhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXK)).EndInit();
            this.ultraTabPagePhieuThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuThu)).EndInit();
            this.ultraGroupBoxPhieuThu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPT)).EndInit();
            this.ultraTabPageGiayBaoCo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxGiayBaoCo)).EndInit();
            this.ultraGroupBoxGiayBaoCo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameGBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDGBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayGBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressGBC)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHinhThucHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMauHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uComboThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDateHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventoryHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachListHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).EndInit();
            this.ultraGroupBoxTop.ResumeLayout(false);
            this.palVouchersNoHd.ResumeLayout(false);
            this.palVouchersNoPxk.ResumeLayout(false);
            this.palVouchersNoTt.ResumeLayout(false);
            this.palVouchersNoGbc.ResumeLayout(false);
            this.palChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optPhieuXuatKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbChonThanhToan)).EndInit();
            this.palFill.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.group)).EndInit();
            this.group.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsDiscountByVoucher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel palGrid;
        private System.Windows.Forms.Panel palThongTinChung;
        [Browsable(false)]
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkIsDiscountByVoucher;
        private Infragistics.Win.Misc.UltraButton btnIsDiscountByVoucher;
        [Browsable(false)]
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbChonThanhToan;
        private Infragistics.Win.Misc.UltraButton btnOutwardNo;
        private System.Windows.Forms.Panel palChung;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraPanel palVouchersNoHd;
        private Infragistics.Win.Misc.UltraPanel palVouchersNoPxk;
        private Infragistics.Win.Misc.UltraPanel palVouchersNoTt;
        private Infragistics.Win.Misc.UltraPanel palVouchersNoGbc;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountOriginalStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel txtTotalDiscountAmountSubOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalDiscountAmountSub;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private UltraOptionSet_Ex optThanhToan;
        private UltraOptionSet_Ex optPhieuXuatKho;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraLabel lblTT;
        private Infragistics.Win.Misc.UltraLabel lblBillDeleted;
        private Infragistics.Win.Misc.UltraButton btnSelectBill;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbBill;
        private UltraOptionSet_Ex optHoaDon;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvSeries;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvTemplate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvNo;
        private Infragistics.Win.Misc.UltraLabel lblTttt;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl group;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageHoaDon;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDon;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteListDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListCommonNameInventory;
        private Infragistics.Win.Misc.UltraLabel lblListCommonNameInventory;
        private Infragistics.Win.Misc.UltraLabel lblListDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListNo;
        private Infragistics.Win.Misc.UltraLabel lblListNo;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkisAttachList;
        private Infragistics.Win.Misc.UltraLabel lblisAttachList;
        private Infragistics.Win.Misc.UltraButton btnCongNo;
        private Infragistics.Win.Misc.UltraButton btnSAOrder;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.Misc.UltraLabel lblContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePhieuXuatKho;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuXuatKho;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodePXK;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCodePXK;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGoc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNoPXK;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttachPXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePXK;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDPXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSReason;
        private Infragistics.Win.Misc.UltraLabel lblSReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactName;
        private Infragistics.Win.Misc.UltraLabel lblSContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPXK;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiPXK;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangPXK;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePhieuThu;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuThu;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGocPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttachPT;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttachPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePT;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayPT;
        private Infragistics.Win.Misc.UltraLabel lblMReasonPayPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMContactName;
        private Infragistics.Win.Misc.UltraLabel lblMContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPT;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiPT;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangPT;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageGiayBaoCo;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxGiayBaoCo;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailID;
        private Infragistics.Win.Misc.UltraLabel lblNopTienTaiKhoanGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameGBC;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayGBC;
        private Infragistics.Win.Misc.UltraLabel lblMReasonPayGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressGBC;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiGBC;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangGBC;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbLoaiHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbHinhThucHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMauHD;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboThanhToan;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingBankName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteListDateHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListCommonNameInventoryHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListNoHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkisAttachListHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameHD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactNameHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
    }
}
