﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Core.IService;
using FX.Core;
using log4net;
using Accounting.Core.Domain.ControlsMapping;

namespace Accounting
{
    public sealed partial class FSAInvoiceDetail : FSaInvoiceDetailStand
    {
        #region Khai báo
        private readonly ISAPolicyPriceSettingService _ISAPolicyPriceSettingService;
        private readonly ITT153DeletedInvoiceService _ITT153DeletedInvoiceService;
        private readonly IAccountingObjectService _IAccountingObjectService;//add by cuongpv
        ITT153PublishInvoiceDetailService ITT153PublishInvoiceDetailService;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger("Accounting.FSAInvoiceDetail");//add log by cuongpv
        UltraGrid uGrid0;
        UltraGrid uGrid1;
        private int _typeFrm;
        private bool _isFirst = true;
        private bool __isFirstKho = true;
        private bool checkChungTuThamChieuPhieuXuatKhoCuChua = false;
        UltraGrid ugrid2 = null;
        SAInvoice sainput;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion

        #region khởi tạo
        public FSAInvoiceDetail(SAInvoice temp, List<SAInvoice> listObject, int statusForm, int loaiFrm = 0)   //default = -1
        {
            log.Error("Begin Load form FSAInvoiceDetail");
            InitializeComponent();
            base.InitializeComponent1();
            _ISAPolicyPriceSettingService = IoC.Resolve<ISAPolicyPriceSettingService>();
            _ITT153DeletedInvoiceService = IoC.Resolve<ITT153DeletedInvoiceService>();
            ITT153PublishInvoiceDetailService = IoC.Resolve<ITT153PublishInvoiceDetailService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();//add by cuongpv
            #region Thiết lập ban đầu
            switch (loaiFrm)
            {
                case -1:
                case 0:
                    _typeID = 320;  //loại chưa thanh toán (320)
                    break;
                case 1:
                    _typeID = 321;  //loại thanh toán ngay (321 + phiếu thu 102), (322 + giấy báo có nttk 162)
                    break;
                case 2:
                    _typeID = 323;  //loại chưa thanh toán (323), loại thanh toán ngay (324 + phiếu thu 103), (325 + giấy báo có nttk 163)
                    break;
                    //ngoài ra (320,321,322 + phiếu xuất kho 412), (323,324,325 + phiếu xuất kho 415)
            }
            _typeFrm = loaiFrm;
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = _typeID; else _select = temp;
            _listSelects.AddRange(listObject);
            log.Error("Khởi tạo FSAInvoiceDetail Bước 1");
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            log.Error("Khởi tạo FSAInvoiceDetail Bước 2");
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            if (statusForm == ConstFrm.optStatusForm.Add && Utils.ListSystemOption.FirstOrDefault(x => x.ID == 122).Data == "1") cbBill.Enabled = false;
            //if (_select.IsBill) 
            group.SelectedTab = group.Tabs["tabHoaDon"];
            Utils.ClearCacheByType<PaymentClause>();
            log.Error("End Load form FSAInvoiceDetail");
            if (_select.BillRefID != null) optHoaDon.Enabled = false; 
            #endregion
        }
        #endregion

        #region override
        public override void InitializeGUI(SAInvoice input)
        {
            //List<AccountingObject> lstAccountingOject = _IAccountingObjectService.GetListAccountingOjectFilter(null,null);add by cuongpv
            //Config GUI
            //optPhieuXuatKho.Value = string.IsNullOrEmpty(_select.OutwardNo) ? false : true;
            log.Error("Khởi tạo InitializeGUI Bước 1.1");
            Dictionary<string, string> lstdatasourceUcomboThanhToan = new Dictionary<string, string>();//trungnq thêm để làm task 6235, thêm combobox hình thức thanh toán
            lstdatasourceUcomboThanhToan.Add("0", "Tiền mặt");
            lstdatasourceUcomboThanhToan.Add("1", "Chuyển Khoản");
            lstdatasourceUcomboThanhToan.Add("2", "TM/CK");
            uComboThanhToan.DataSource = lstdatasourceUcomboThanhToan;
            Utils.ConfigGrid(uComboThanhToan, ConstDatabase.TMIndustryTypeGH_TableName);
            uComboThanhToan.DisplayMember = "Value";
            uComboThanhToan.ValueMember = "Key";//trungnq
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                optPhieuXuatKho.CheckedIndex = (_select == null || !(_select.IsDeliveryVoucher ?? false)) ? 1 : 0;

            }
            else
            {
                if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 50).Data == "1")
                    optPhieuXuatKho.CheckedIndex = 1;
                else optPhieuXuatKho.CheckedIndex = 0;
            }
            btnOutwardNo.Enabled = optPhieuXuatKho.CheckedItem.Tag.Equals("KKPXK");
            btnOutwardNo.Tag = new Dictionary<string, object> { { "Enabled", optPhieuXuatKho.CheckedItem.Tag.Equals("KKPXK") } };
            #region Top
            this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoHd, "No", "PostedDate", "Date", resetNo: _statusForm == ConstFrm.optStatusForm.Add);
            if (_select.OutwardNo != null && !string.IsNullOrEmpty(_select.OutwardNo)) this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoPxk, "OutwardNo", "SPostedDate", "SDate", ConstFrm.TypeGroup_RSInwardOutwardOutput);
            _backSelect.TypeID = _select.TypeID.CloneObject();

            #endregion
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            //config số đơn hàng, ngày đơn hàng
            //Config Grid
            _listObjectInput = new BindingList<System.Collections.IList> { new BindingList<SAInvoiceDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<SAInvoiceDetail>() : input.SAInvoiceDetails) };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<SAInvoice>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
            log.Error("Khởi tạo InitializeGUI Bước 1.2");
            this.ConfigGridByManyTemplete<SAInvoice>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            log.Error("Khởi tạo InitializeGUI Bước 1.3");
            if (_statusForm != ConstFrm.optStatusForm.Add) _select.ObjRsInwardOutwardIDs = IRSInwardOutwardService.Query.Where(p => p.SAInvoiceID == _select.ID).Select(p => p.ID).ToList();
            //ConfigGridBase(input, uGridControl, pnlUgrid);
            List<SAInvoice> inputCurrency = new List<SAInvoice> { input };
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            if (input.HasProperty("TypeID") && input.HasProperty("Exported"))
            {
                int type = int.Parse(_select.GetProperty("TypeID").ToString());
                var isExported = (bool)_select.GetProperty("Exported");
                var ultraGrid = (UltraGrid)pnlUgrid.Controls.Find("uGrid1", true).FirstOrDefault();
                ultraGrid.ConfigColumnByExported(isExported, _select.GetProperty("CurrencyID").ToString(), type);
                //ultraGrid.ConfigColumnByImportPurchase(isExported, _select.GetProperty("CurrencyID").ToString(), _statusForm);
            }
            log.Error("Khởi tạo InitializeGUI Bước 1.4");
            this.ConfigGridCurrencyByTemplate<SAInvoice>(input.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            AddCbb(input);
            log.Error("Khởi tạo InitializeGUI Bước 1.5");
            uGrid0 = (UltraGrid)palGrid.Controls.Find("uGrid0", true).FirstOrDefault();
            uGrid1 = (UltraGrid)palGrid.Controls.Find("uGrid1", true).FirstOrDefault();
            if(ugrid2!=null)
            {
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            }    
            
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                optThanhToan.Value = !new int[] { 320, 323 }.Contains(_select.TypeID);
            }
            else
            {
                if (_select.TypeID == 320 || _select.TypeID == 323) optThanhToan.Value = false;
                else
                {
                    int typeId = _select.TypeID;
                    optThanhToan.Value = true;
                    _select.TypeID = typeId;
                }
                if (input.PaymentMethod != null && !string.IsNullOrEmpty(input.PaymentMethod)) uComboThanhToan.Text = input.PaymentMethod;
            }
            //config combo
            Utils.isSAInvoice = true;
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDPXK, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectID Bước 1.6");
            this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
            log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDPXK Bước 1.7");
            this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDPXK, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
            log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDPT Bước 1.8");
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDPT, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDGBC Bước 1.9");
            this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDGBC, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            log.Error("Khởi tạo InitializeGUI.cbbBankAccountDetailID Bước 1.10");
            this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccountDetailID, "BankAccount", "ID", input, "BankAccountDetailID");
            //this.ConfigCombo(Utils.ListAccountingObjectBank, cbbAccountingObjectBankAccount, "BankAccount", "BankAccount", input, "AccountingObjectBankAccount");
            log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDHD Bước 1.11");
            this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
            //Databinding
            log.Error("Khởi tạo InitializeGUI Bước 1.12");
            DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtCompanyTaxCode, txtContactName, txtReason }, "AccountingObjectName,AccountingObjectAddress,CompanyTaxCode,ContactName,Reason");
            DataBinding(new List<Control> { txtListNo, dteListDate, txtListCommonNameInventory }, "ListNo,ListDate,ListCommonNameInventory", "0,2,0");
            DataBinding(new List<Control> { txtAccountingObjectNamePXK, txtAccountingObjectAddressPXK, txtCompanyTaxCodePXK, txtSContactName, txtSReason, txtOriginalNoPXK }, "AccountingObjectName,AccountingObjectAddress,CompanyTaxCode,SContactName,SReason,OriginalNo");
            DataBinding(new List<Control> { txtAccountingObjectNamePT, txtAccountingObjectAddressPT, txtMContactName, txtMReasonPayPT, txtNumberAttachPT }, "AccountingObjectName,AccountingObjectAddress,MContactName,MReasonPay,NumberAttach");
            DataBinding(new List<Control> { txtAccountingObjectNameGBC, txtAccountingObjectAddressGBC, txtBankName, txtMReasonPayGBC }, "AccountingObjectName,AccountingObjectAddress,BankName,MReasonPay");
            DataBinding(new List<Control> { txtAccountingObjectNameHD, txtAccountingObjectAddressHD, txtCompanyTaxCodeHD, txtContactNameHD, txtReasonHD }, "AccountingObjectName,AccountingObjectAddress,CompanyTaxCode,ContactName,Reason");
            DataBinding(new List<Control> { txtListNoHD, dteListDateHD, txtListCommonNameInventoryHD }, "ListNo,ListDate,ListCommonNameInventory", "0,2,0");
            DataBinding(new List<Control> { chkIsDiscountByVoucher }, "IsDiscountByVoucher", "1");
            DataBinding(new List<Control> { chkisAttachListHD }, "IsAttachList", "1");
            DataBinding(new List<Control> { txtAccountingBankName }, "AccountingObjectBankName");
            //lst control binding
            DataBinding(new List<Control>
                           {
                               txtTotalAmount,
                               txtTotalVATAmount,
                               txtTotalDiscountAmountSub,
                               txtTotalPaymentAmountStand
                           }, "TotalAmount, TotalVATAmount, TotalDiscountAmount, TotalPaymentAmount", "3,3,3,3");

            DataBinding(new List<Control>
                           {
                               txtTotalAmountOriginal,
                               txtTotalVATAmountOriginal,
                               txtTotalDiscountAmountSubOriginal,
                               txtTotalPaymentAmountOriginalStand
                           }, "TotalAmountOriginal, TotalVATAmountOriginal, TotalDiscountAmountOriginal, TotalPaymentAmountOriginal", "3,3,3,3", true);

            if (_statusForm == ConstFrm.optStatusForm.Add)
                input.IsDiscountByVoucher = input.IsAttachList = input.IsAttachList = false;
            btnIsDiscountByVoucher.Enabled = chkIsDiscountByVoucher.CheckState == CheckState.Checked;
            btnIsDiscountByVoucher.Tag = new Dictionary<string, object> { { "Enabled", chkIsDiscountByVoucher.CheckState == CheckState.Checked } };
            DataBinding(new List<Control> { chkisAttachList }, "IsAttachList", "1");
            btnSelectBill.Enabled = cbBill.CheckState == CheckState.Checked;
            btnSelectBill.Tag = new Dictionary<string, object> { { "Enabled", cbBill.CheckState == CheckState.Checked } };
            DataBinding(new List<Control> { cbBill }, "IsAttachListBill", "1");
            if (_statusForm == ConstFrm.optStatusForm.Add) ChkisAttachListCheckedChanged(chkisAttachList, new EventArgs());
            txtListNo.Tag = dteListDate.Tag = txtListCommonNameInventory.Tag = new Dictionary<string, object> { { "Enabled", chkisAttachList.CheckState == CheckState.Checked } };
            if (optPhieuXuatKho.CheckedIndex == 1)
            {
                _select.OutwardNo = null;
                _select.IsDeliveryVoucher = false;
            }
            else
            {
                _select.IsDeliveryVoucher = true;
            }
            if (input != null && input.SAInvoiceDetails.Any(d => d.LotNo != null))
            {
                uGrid0.DisplayLayout.Bands[0].Columns["ExpiryDate"].Hidden = false;
                uGrid0.DisplayLayout.Bands[0].Columns["ExpiryDate"].Hidden = false;
            }
            if (input != null && _ITT153DeletedInvoiceService.Query.Any(x => x.SAInvoiceID == input.ID)) lblBillDeleted.Visible = true;
            if (input != null && input.BillRefID != null)
            {
                var HD = Utils.ListSABill.FirstOrDefault(x => x.ID == input.BillRefID);
                txtOldInvNo.Text = HD.InvoiceNo;
                txtOldInvDate.Text = HD.InvoiceDate == null ? "" : (HD.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                txtOldInvTemplate.Text = HD.InvoiceTemplate;
                txtOldInvSeries.Text = HD.InvoiceSeries;
            }
            else
            {
                panel1.Visible = false;
            }
            uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].LockColumn(true);
            uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].LockColumn(true);
            uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].Hidden = false;
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                optHoaDon.CheckedIndex = input.IsBill ? 0 : 1;
            }
            else
            {
                if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 122).Data == "1")
                    optHoaDon.CheckedIndex = 0;
                else optHoaDon.CheckedIndex = 1;
                uComboThanhToan.Text = "TM/CK";
            }
            switch (input.TypeID)
            {
                case 320: //bán hàng chưa thanh toán
                    #region bán hàng chưa thu tiền
                    {
                        optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        lblTT.Visible = false;
                    }

                    #endregion
                    break;
                case 321: //bán hàng thu tiền ngay tiền mặt
                    #region bán hàng thu tiền ngay tiền mặt
                    {
                        cbbChonThanhToan.SelectedIndex = 0; //mặc định là tiền mặt
                        optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"  
                        if (_statusForm == ConstFrm.optStatusForm.View) this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoTt, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroup_MCReceipt, objectDataSource: _select);
                    }
                    #endregion
                    break;
                case 322: //bán hàng thu tiền ngay chuyển khoản
                    #region bán hàng thu tiền ngay chuyển khoản
                    {
                        cbbChonThanhToan.SelectedIndex = 1; //mặc định là chuyển khoản
                        optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"   
                        if (_statusForm == ConstFrm.optStatusForm.View) this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoGbc, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroupMBDeposit, objectDataSource: _select);
                    }
                    #endregion
                    break;
                case 323: //bán hàng từ đại lý bán đúng giá ủy thác XNK chưa thanh toán
                    #region bán hàng từ đại lý bán đúng giá ủy thác XNK chưa thanh toán
                    {
                        cbbChonThanhToan.Visible = true;
                    }
                    #endregion
                    break;
                case 324: //bán hàng từ đại lý bán đúng giá ủy thác XNK tiền mặt
                    #region bán hàng từ đại lý bán đúng giá ủy thác XNK tiền mặt
                    {
                        cbbChonThanhToan.SelectedIndex = 0; //mặc định là tiền mặt
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        if (_statusForm == ConstFrm.optStatusForm.View) this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoTt, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroup_MCReceipt, objectDataSource: _select);
                    }
                    #endregion
                    break;
                case 325: //bán hàng từ đại lý bán đúng giá ủy thác XNK chuyển khoản
                    #region bán hàng từ đại lý bán đúng giá ủy thác XNK chuyển khoản
                    {
                        cbbChonThanhToan.SelectedIndex = 1; //mặc định là chuyển khoản
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        if (_statusForm == ConstFrm.optStatusForm.View) this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoGbc, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroupMBDeposit, objectDataSource: _select);
                    }
                    #endregion
                    break;
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 320)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Bán hàng chưa thu tiền";
                    txtReason.Text = "Bán hàng chưa thu tiền";
                    _select.Reason = "Bán hàng chưa thu tiền";
                    txtSReason.Value = "Xuất kho bán hàng";
                    txtSReason.Text = "Xuất kho bán hàng";
                    _select.SReason = "Xuất kho bán hàng";

                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn sainvoice
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.AllowEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.NoEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.NoEdit;
                    }

                }
                else if (input.TypeID == 321)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Bán hàng thu tiền ngay";
                    txtReason.Text = "Bán hàng thu tiền ngay";
                    _select.Reason = "Bán hàng thu tiền ngay";
                    txtSReason.Value = "Xuất kho bán hàng";
                    txtSReason.Text = "Xuất kho bán hàng";
                    _select.SReason = "Xuất kho bán hàng";
                    txtMReasonPayPT.Value = "Thu tiền từ bán hàng";
                    txtMReasonPayPT.Text = "Thu tiền từ bán hàng";
                    _select.MReasonPay = "Thu tiền từ bán hàng";
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn sainvoice
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.AllowEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.NoEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.NoEdit;
                    }
                }
                else if (input.TypeID == 323)//trungnq thêm vào để  làm task 6234
                {
                    txtReason.Value = "Hàng gửi bán đại lý";
                    txtReason.Text = "Hàng gửi bán đại lý";
                    _select.Reason = "Hàng gửi bán đại lý";
                    txtSReason.Value = "Xuất bán cho đại lý, ủy thác";
                    txtSReason.Text = "Xuất bán cho đại lý, ủy thác";
                    _select.SReason = "Xuất bán cho đại lý, ủy thác";
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn sainvoice
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.AllowEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.NoEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.NoEdit;
                    }
                }
            }
            log.Error("Khởi tạo InitializeGUI Bước 1.13");
        }

        public override void SetLaiUgridControl(SAInvoice input)
        {
            ConfigGroup(false);
            // uGridControl.ConfigSizeGridCurrency(this, true);
        }
        #endregion

        #region Event
        private void BtnIsDiscountByVoucherClick(object sender, EventArgs e)
        {//Chiết khấu theo hóa đơn -> phân bổ          
            try
            {
                chkIsDiscountByVoucher.Checked = true;
                var frm = new FIsDiscountByVoucher(_listObjectInput, _select.ExchangeRate.HasValue ? _select.ExchangeRate.Value : 1, _select.CurrencyID);
                frm.ShowDialog(this);
                if (frm.DialogResult != DialogResult.OK) return;
                UltraGrid uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid != null)
                {
                    uGrid.DataBind();
                    foreach (var row in uGrid.Rows)
                    {
                        this.CheckErrorInCell<SAInvoice>(uGrid, row.Cells["DiscountRate"]);
                    }
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
            //_listObjectInput đã update dữ liệu nhưng Grid binding đến nó lại không update?????
        }

        private void BtnCongNoClick(object sender, EventArgs e)
        {
            decimal debt = Utils.IGeneralLedgerService.GetDebtCustomer(_select.AccountingObjectID ?? Guid.Empty, _select.PostedDate);
            MSG.MessageBoxStand(string.Format("Dư Nợ : {0}", debt.ToStringNumbericFormat(ConstDatabase.Format_TienVND)), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void BtnOutwardNoClick(object sender, EventArgs e)
        {
            try
            {
                var frm = new FchoseRsInwardOutward(_listObjectInput, _statusForm == ConstFrm.optStatusForm.Add ? (Guid?)null : _select.ID, _select.ObjRsInwardOutwardIDs.CloneObject(), _select.AccountingObjectID);
                frm.ShowDialog(this);
                if (frm.DialogResult != DialogResult.OK) return;
                var lstSelect = frm.Selecteds;
                _select.ObjRsInwardOutwardIDs.Clear();
                if (_listObjectInput[0] is BindingList<SAInvoiceDetail>)
                {
                    if (lstSelect.Count > 0)
                    {
                        _listObjectInput[0].Clear();
                        _select.ObjRsInwardOutwardIDs.Clear();
                    }
                    foreach (ObjRsInwardOutward objRsInwardOutward in lstSelect)
                    {
                        SAInvoiceDetail temp = new SAInvoiceDetail();
                        ReflectionUtils.Copy(typeof(ObjRsInwardOutward), objRsInwardOutward, typeof(SAInvoiceDetail), temp, new List<string> { "DebitAccount", "CreditAccount", "DiscountAccount", "CostAccount", "RepositoryAccount", "VATAccount" });
                        temp.ID = Guid.Empty;
                        temp.OWPrice = temp.UnitPrice;
                        temp.OWPriceOriginal = temp.UnitPriceOriginal;
                        temp.OWAmount = temp.Amount;
                        temp.OWAmountOriginal = temp.AmountOriginal;
                        temp.UnitPriceOriginal = 0;
                        temp.UnitPrice = 0;
                        temp.AmountOriginal = 0;
                        temp.Amount = 0;
                        temp.VATDescription = "Thuế GTGT";
                        temp.VATRate = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == temp.MaterialGoodsID).TaxRate;
                        _listObjectInput[0].Add(temp.CloneObject());
                        if (objRsInwardOutward.RSInwardOutwardID.HasValue)
                            _select.ObjRsInwardOutwardIDs.Add(objRsInwardOutward.RSInwardOutwardID.Value.CloneObject());
                    }
                }
                UltraGrid uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid != null) uGrid.DataBind();
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void btnSAOrder_Click(object sender, EventArgs e)
        {
            var f = new FchoseSaOrder<SAInvoice>(_listObjectInput, _select, _select.AccountingObjectID, _select.PostedDate, this, (uGrid0.DataSource as BindingList<SAInvoiceDetail>).ToList());
            f.FormClosed += new FormClosedEventHandler(fSelectOrders_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void fSelectOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FchoseSaOrder<SAInvoice>)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectOrders = frm._lstSaOrderDetail.OrderBy(n => n.SAOrderDetails.OrderPriority).ToList(); // tungnt: sắp xếp thứ tự trên grid;
                if (uGrid0 != null)
                {
                    var source = (BindingList<SAInvoiceDetail>)uGrid0.DataSource;
                    if (selectOrders.Count > 0) source.Clear();
                    foreach (var selectOrder in selectOrders)
                    {
                        uGrid0.AddNewRow4Grid();
                        var row = uGrid0.Rows[uGrid0.Rows.Count - 1];
                        row.Cells["MaterialGoodsID"].Value = selectOrder.MaterialGoodsID;
                        row.UpdateData();
                        uGrid0.UpdateData();
                        row.Cells["Description"].Value = selectOrder.MaterialGoodsName;
                        row.Cells["RepositoryID"].Value = selectOrder.SAOrderDetails.RepositoryID;
                        row.Cells["DebitAccount"].Value = selectOrder.SAOrderDetails.DebitAccount;
                        row.Cells["CreditAccount"].Value = selectOrder.SAOrderDetails.CreditAccount;
                        row.Cells["Unit"].Value = selectOrder.SAOrderDetails.Unit;
                        row.Cells["Quantity"].Value = selectOrder.QuantityExport;
                        row.Cells["UnitConvert"].Value = selectOrder.SAOrderDetails.UnitConvert;
                        row.Cells["UnitPrice"].Value = selectOrder.SAOrderDetails.UnitPrice;
                        row.Cells["UnitPriceOriginal"].Value = selectOrder.SAOrderDetails.UnitPriceOriginal;
                        row.Cells["UnitPriceConvert"].Value = selectOrder.SAOrderDetails.UnitPriceConvert;
                        row.Cells["UnitPriceConvertOriginal"].Value = selectOrder.SAOrderDetails.UnitPriceConvertOriginal;
                        row.Cells["Amount"].Value = selectOrder.SAOrderDetails.UnitPrice *
                                                    (selectOrder.SAOrderDetails.Quantity ?? 0);
                        row.Cells["AmountOriginal"].Value = selectOrder.SAOrderDetails.UnitPriceOriginal *
                                                            (selectOrder.SAOrderDetails.Quantity ?? 0);
                        row.Cells["AccountingObjectID"].Value = selectOrder.AccountingObjectID;
                        row.Cells["SAOrderID"].Value = selectOrder.ID;
                        row.Cells["SAOrderNo"].Value = selectOrder.No;
                        row.Cells["DetailID"].Value = selectOrder.SAOrderDetails.ID;
                        row.Cells["QuantityConvert"].Value = selectOrder.SAOrderDetails.QuantityConvert;
                        row.Cells["VATDescription"].Value = selectOrder.SAOrderDetails.VATDescription;
                        row.Cells["VATRate"].Value = selectOrder.SAOrderDetails.VATRate;
                        row.Cells["VATAmount"].Value = selectOrder.SAOrderDetails.VATAmount;
                        row.Cells["VATAmountOriginal"].Value = selectOrder.SAOrderDetails.VATAmountOriginal;
                        row.Cells["VATAccount"].Value = selectOrder.SAOrderDetails.VATAccount;
                        row.Cells["CostSetID"].Value = selectOrder.SAOrderDetails.CostSetID;
                        row.Cells["ContractID"].Value = selectOrder.SAOrderDetails.ContractID;
                        row.Cells["StatisticsCodeID"].Value = selectOrder.SAOrderDetails.StatisticsCodeID;
                        row.Cells["DepartmentID"].Value = selectOrder.SAOrderDetails.DepartmentID;
                        row.Cells["ExpenseItemID"].Value = selectOrder.SAOrderDetails.ExpenseItemID;
                        row.Cells["BudgetItemID"].Value = selectOrder.SAOrderDetails.BudgetItemID;
                        row.Cells["ExpiryDate"].Value = selectOrder.SAOrderDetails.ExpiryDate;
                        row.Cells["LotNo"].Value = selectOrder.SAOrderDetails.LotNo;
                        row.Cells["UnitConvert"].Value = selectOrder.SAOrderDetails.UnitConvert;
                        row.Cells["ConvertRate"].Value = selectOrder.SAOrderDetails.ConvertRate;
                        row.Cells["UnitPriceAfterTax"].Value = selectOrder.SAOrderDetails.UnitPriceAfterTax;
                        row.Cells["UnitPriceAfterTaxOriginal"].Value = selectOrder.SAOrderDetails.UnitPriceAfterTaxOriginal;
                        row.Cells["AmountAfterTax"].Value = selectOrder.SAOrderDetails.AmountAfterTax;
                        row.Cells["AmountAfterTaxOriginal"].Value = selectOrder.SAOrderDetails.AmountAfterTaxOriginal;
                        row.Cells["DiscountRate"].Value = selectOrder.SAOrderDetails.DiscountRate;
                        row.Cells["DiscountAmount"].Value = selectOrder.SAOrderDetails.DiscountAmount;
                        row.Cells["DiscountAmountOriginal"].Value = selectOrder.SAOrderDetails.DiscountAmountOriginal;
                        row.Cells["DiscountAmountAfterTax"].Value = selectOrder.SAOrderDetails.DiscountAmountAfterTax;
                        row.Cells["DiscountAmountAfterTaxOriginal"].Value = selectOrder.SAOrderDetails.DiscountAmountAfterTaxOriginal;
                        row.UpdateData();
                        uGrid0.ActiveCell = row.Cells["Quantity"];
                        this.DoEverythingInGrid<SAInvoice>(uGrid0);
                    }
                }
                var grpCurrency = (from u in selectOrders group u by u.CurrencyID into g select new { CurrencyID = g.Key, ExchangeRate = g.Select(p => p.ExchangeRate) });
                if (grpCurrency.Count() == 1)
                {
                    var f = grpCurrency.FirstOrDefault();
                    if (f != null)
                    {
                        _select.CurrencyID = f.CurrencyID;
                        var currency = Utils.ListCurrency.FirstOrDefault(c => c.ID == _select.CurrencyID);
                        var grpEx = f.ExchangeRate.GroupBy(x => x).Select(x => x.Key).ToList();
                        decimal? exrate = grpEx.Count == 1 ? grpEx.FirstOrDefault() : (decimal?)null;
                        this.AutoExchangeByCurrencySelected(currency, exrate);
                        if (uGridControl != null && uGridControl.DisplayLayout.Bands[0].Columns.Exists("CurrencyID"))
                        {
                            this.ConfigGridCurrencyByCurrency(uGridControl, currency.ID, exrate ?? 1);
                        }
                    }
                }
                var orderDates = selectOrders.GroupBy(k => k.Date).Select(k => k.Key).ToList();
                if (orderDates.Count == 1)
                {
                    _select.DueDate = orderDates.FirstOrDefault();
                    uGridControl.Rows[0].Refresh(RefreshRow.RefreshDisplay);
                    uGridControl.Rows[0].Refresh(RefreshRow.ReloadData);
                }
                ConfigGroup(false);
                if (uGrid0.Rows.Count > 0)
                    foreach (var row in uGrid0.Rows)
                        foreach (var cell in row.Cells)
                            Utils.CheckErrorInCell<SAInvoice>(this, uGrid0, cell);
            }
        }

        private void ChkisAttachListCheckedChanged(object sender, EventArgs e)
        {//In kèm bảng kê hay không?
            _select.IsAttachList = txtListNo.Enabled = dteListDate.Enabled = txtListCommonNameInventory.Enabled = chkisAttachList.CheckState == CheckState.Checked;
            txtListNo.Tag = dteListDate.Tag = txtListCommonNameInventory.Tag = new Dictionary<string, object> { { "Enabled", chkisAttachList.CheckState == CheckState.Checked } };
            if (dteListDate.Enabled)
            {
                if (dteListDate.Value != null)
                {
                    dteListDate.DateTime = _select.PostedDate;
                    chkisAttachList.CheckState = CheckState.Checked;
                }
            }
            if (chkisAttachList.CheckState == CheckState.Checked) return;
            txtListNo.Text = string.Empty;
            txtListCommonNameInventory.Text = string.Empty;
            //dteListDate.Value = "";
            //dteListDate.ResetDateTime();
            dteListDate.Value = null;
        }

        private void OptThanhToanValueChangedStand(object sender, EventArgs e)
        {//opt chọn thanh toán value change
            UltraOptionSet optionSet;
            try { optionSet = (UltraOptionSet)sender; if (optionSet == null) return; }
            catch { return; }
            if (_statusForm != ConstFrm.optStatusForm.Edit)
            {
                if (!_isFirst && optionSet.CheckedItem.Tag.ToString().Equals("CTT")) _select.TypeID = _typeFrm == 2 ? 323 : 320;
                if (_isFirst)
                {
                    if (new int[] { 321, 324 }.Contains(_select.TypeID))
                    {
                        cbbChonThanhToan.SelectedIndex = 0;
                        CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                    }
                    else if (new int[] { 322, 325 }.Contains(_select.TypeID))
                    {
                        cbbChonThanhToan.SelectedIndex = 1;
                        CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                    }
                }
                bool isResetNo = true;
                if (_statusForm != ConstFrm.optStatusForm.Add)
                    isResetNo = !(_select.TypeID == _backSelect.TypeID);
                //if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_KoKiemPXuatKho").Data == "1")
                ConfigGroup(isResetNo);
                cbbChonThanhToan.Enabled = optionSet.CheckedItem.Tag.ToString().Equals("TTN");

                if (cbbChonThanhToan.Enabled && cbbChonThanhToan.SelectedItem == null) cbbChonThanhToan.SelectedIndex = 0;
                foreach (var ultraTab in group.Tabs)
                    ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));
            }
            else
            {
                if (optionSet.CheckedIndex == 0)
                {
                    List<RefVoucher> listrefVouchers = new List<RefVoucher>();
                    List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = new List<RefVoucherRSInwardOutward>();
                    if (cbbChonThanhToan.SelectedIndex == 0)// o: tiền mặt là phiếu thu  1: chuyển khoản là báo có, nộp tiền vào tài khoản
                    {
                        listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("10") && n.No == _select.MNo).ToList();
                        listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("10") && n.No == _select.MNo).ToList();
                    }
                    else
                    {
                        listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("16") && n.No == _select.MNo).ToList();
                        listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("16") && n.No == _select.MNo).ToList();
                    }
                    if (listrefVouchers.Count != 0 || listrefVouchersRSInwardOutward.Count != 0)
                    {
                        if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            if (!_isFirst && optionSet.CheckedItem.Tag.ToString().Equals("CTT")) _select.TypeID = _typeFrm == 2 ? 323 : 320;
                            if (_isFirst)
                            {
                                if (new int[] { 321, 324 }.Contains(_select.TypeID))
                                {
                                    cbbChonThanhToan.SelectedIndex = 0;
                                    CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                                }
                                else if (new int[] { 322, 325 }.Contains(_select.TypeID))
                                {
                                    cbbChonThanhToan.SelectedIndex = 1;
                                    CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                                }
                            }
                            bool isResetNo = true;
                            if (_statusForm != ConstFrm.optStatusForm.Add)
                                isResetNo = !(_select.TypeID == _backSelect.TypeID);
                            //if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_KoKiemPXuatKho").Data == "1")
                            ConfigGroup(isResetNo);
                            cbbChonThanhToan.Enabled = optionSet.CheckedItem.Tag.ToString().Equals("TTN");

                            if (cbbChonThanhToan.Enabled && cbbChonThanhToan.SelectedItem == null) cbbChonThanhToan.SelectedIndex = 0;
                            foreach (var ultraTab in group.Tabs)
                                ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));

                        }
                        else
                        {
                            optionSet.CheckedIndex = 1;
                        }
                    }
                    else
                    {
                        if (!_isFirst && optionSet.CheckedItem.Tag.ToString().Equals("CTT")) _select.TypeID = _typeFrm == 2 ? 323 : 320;
                        if (_isFirst)
                        {
                            if (new int[] { 321, 324 }.Contains(_select.TypeID))
                            {
                                cbbChonThanhToan.SelectedIndex = 0;
                                CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                            }
                            else if (new int[] { 322, 325 }.Contains(_select.TypeID))
                            {
                                cbbChonThanhToan.SelectedIndex = 1;
                                CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                            }
                        }
                        bool isResetNo = true;
                        if (_statusForm != ConstFrm.optStatusForm.Add)
                            isResetNo = !(_select.TypeID == _backSelect.TypeID);
                        //if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_KoKiemPXuatKho").Data == "1")
                        ConfigGroup(isResetNo);
                        cbbChonThanhToan.Enabled = optionSet.CheckedItem.Tag.ToString().Equals("TTN");

                        if (cbbChonThanhToan.Enabled && cbbChonThanhToan.SelectedItem == null) cbbChonThanhToan.SelectedIndex = 0;
                        foreach (var ultraTab in group.Tabs)
                            ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));

                    }
                }
                else
                {
                    if (!_isFirst && optionSet.CheckedItem.Tag.ToString().Equals("CTT")) _select.TypeID = _typeFrm == 2 ? 323 : 320;
                    if (_isFirst)
                    {
                        if (new int[] { 321, 324 }.Contains(_select.TypeID))
                        {
                            cbbChonThanhToan.SelectedIndex = 0;
                            CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                        }
                        else if (new int[] { 322, 325 }.Contains(_select.TypeID))
                        {
                            cbbChonThanhToan.SelectedIndex = 1;
                            CbbChonThanhToanValueChangedStand(cbbChonThanhToan, new EventArgs());
                        }
                    }
                    bool isResetNo = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        isResetNo = !(_select.TypeID == _backSelect.TypeID);
                    //if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_KoKiemPXuatKho").Data == "1")
                    ConfigGroup(isResetNo);
                    cbbChonThanhToan.Enabled = optionSet.CheckedItem.Tag.ToString().Equals("TTN");

                    if (cbbChonThanhToan.Enabled && cbbChonThanhToan.SelectedItem == null) cbbChonThanhToan.SelectedIndex = 0;
                    foreach (var ultraTab in group.Tabs)
                        ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));

                }
            }
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<SAInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.Exported);
                }
            }
        }

        private void CbbChonThanhToanValueChangedStand(object sender, EventArgs e)
        {//cbb Chọn thanh toán value change
            if (optThanhToan.CheckedItem.Tag.Equals("CTT") && optThanhToan.CheckedItem.CheckState == CheckState.Checked) return;
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (!_isFirst)
                {
                    if (_typeFrm != 2) _select.TypeID = cbbChonThanhToan.SelectedIndex == 0 ? 321 : 322;
                    else _select.TypeID = cbbChonThanhToan.SelectedIndex == 0 ? 324 : 325;
                    bool isResetNo = true;
                    if (_statusForm != ConstFrm.optStatusForm.Add)
                        isResetNo = !(_select.TypeID == _backSelect.TypeID);
                    ConfigGroup(isResetNo);
                }
                if (cbbChonThanhToan.Text == "Chuyển khoản")
                {
                    UltraCombo cbbDebitAccount = new UltraCombo();
                    cbbDebitAccount.DataSource = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(322, "DebitAccount");
                    cbbDebitAccount.DisplayMember = "AccountNumber";
                    Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.Account_TableName);
                    uGrid0.DisplayLayout.Bands[0].Columns["DebitAccount"].ValueList = cbbDebitAccount;
                    if (_statusForm == ConstFrm.optStatusForm.Add)
                    {
                        if (_select.TypeID == 322)//trungnq thêm vào để  làm task 6234
                        {
                            txtMReasonPayGBC.Value = "Thu tiền từ bán hàng bằng TGNH";
                            txtMReasonPayGBC.Text = "Thu tiền từ bán hàng bằng TGNH";
                            _select.MReasonPay = "Thu tiền từ bán hàng bằng TGNH";
                        };
                    }
                }
                foreach (var ultraTab in group.Tabs)
                    ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));
            }
            else if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                List<RefVoucher> listrefVouchers = new List<RefVoucher>();
                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = new List<RefVoucherRSInwardOutward>();
                if (cbbChonThanhToan.SelectedIndex == 0)// o: tiền mặt là phiếu thu  1: chuyển khoản là báo có, nộp tiền vào tài khoản
                {
                    listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("16") && n.No == _select.MNo).ToList();
                    listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("16") && n.No == _select.MNo).ToList();
                }
                else
                {
                    listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("10") && n.No == _select.MNo).ToList();
                    listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("10") && n.No == _select.MNo).ToList();
                }
                if (listrefVouchers.Count != 0 || listrefVouchersRSInwardOutward.Count != 0)
                {
                    if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (!_isFirst)
                        {
                            if (_typeFrm != 2) _select.TypeID = cbbChonThanhToan.SelectedIndex == 0 ? 321 : 322;
                            else _select.TypeID = cbbChonThanhToan.SelectedIndex == 0 ? 324 : 325;
                            bool isResetNo = true;
                            if (_statusForm != ConstFrm.optStatusForm.Add)
                                isResetNo = !(_select.TypeID == _backSelect.TypeID);
                            ConfigGroup(isResetNo);
                        }
                        if (cbbChonThanhToan.Text == "Chuyển khoản")
                        {
                            UltraCombo cbbDebitAccount = new UltraCombo();
                            cbbDebitAccount.DataSource = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(322, "DebitAccount");
                            cbbDebitAccount.DisplayMember = "AccountNumber";
                            Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.Account_TableName);
                            uGrid0.DisplayLayout.Bands[0].Columns["DebitAccount"].ValueList = cbbDebitAccount;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {
                                if (_select.TypeID == 322)//trungnq thêm vào để  làm task 6234
                                {
                                    txtMReasonPayGBC.Value = "Thu tiền từ bán hàng bằng TGNH";
                                    txtMReasonPayGBC.Text = "Thu tiền từ bán hàng bằng TGNH";
                                    _select.MReasonPay = "Thu tiền từ bán hàng bằng TGNH";
                                };
                            }
                        }
                        foreach (var ultraTab in group.Tabs)
                            ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));
                    }
                    else
                    {
                        cbbChonThanhToan.SelectedIndex = cbbChonThanhToan.SelectedIndex == 0 ? 1 : 0;
                    }
                }
                else
                {
                    if (!_isFirst)
                    {
                        if (_typeFrm != 2) _select.TypeID = cbbChonThanhToan.SelectedIndex == 0 ? 321 : 322;
                        else _select.TypeID = cbbChonThanhToan.SelectedIndex == 0 ? 324 : 325;
                        bool isResetNo = true;
                        if (_statusForm != ConstFrm.optStatusForm.Add)
                            isResetNo = !(_select.TypeID == _backSelect.TypeID);
                        ConfigGroup(isResetNo);
                    }
                    if (cbbChonThanhToan.Text == "Chuyển khoản")
                    {
                        UltraCombo cbbDebitAccount = new UltraCombo();
                        cbbDebitAccount.DataSource = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(322, "DebitAccount");
                        cbbDebitAccount.DisplayMember = "AccountNumber";
                        Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.Account_TableName);
                        uGrid0.DisplayLayout.Bands[0].Columns["DebitAccount"].ValueList = cbbDebitAccount;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            if (_select.TypeID == 322)//trungnq thêm vào để  làm task 6234
                            {
                                txtMReasonPayGBC.Value = "Thu tiền từ bán hàng bằng TGNH";
                                txtMReasonPayGBC.Text = "Thu tiền từ bán hàng bằng TGNH";
                                _select.MReasonPay = "Thu tiền từ bán hàng bằng TGNH";
                            };
                        }
                    }
                    foreach (var ultraTab in group.Tabs)
                        ultraTab.Visible = ultraTab.Key.Equals("tabHoaDon") || (cbbChonThanhToan.Enabled && ultraTab.Key.Equals(cbbChonThanhToan.SelectedItem.Tag.ToString())) || (optPhieuXuatKho.CheckedIndex == 0 && ultraTab.Key.Equals("tabPhieuXuatKho")) || (optHoaDon.CheckedIndex == 0 && ultraTab.Key.Equals("tabHoaDon1"));

                }
            }
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<SAInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.Exported);
                }
            }
        }
        private void optHoaDon_ValueChanged(object sender, EventArgs e)
        {
            if (optHoaDon.CheckedIndex == 1)
            {
                _select.InvoiceForm = null;
                _select.InvoiceTypeID = null;
                _select.InvoiceTemplate = null;
                _select.InvoiceSeries = null;
                _select.InvoiceNo = null;
                _select.InvoiceDate = null;
            }
            group.Tabs["tabHoaDon1"].Visible = optHoaDon.CheckedIndex == 0;
            cbBill.Enabled = optHoaDon.CheckedIndex == 1;
            if (uGridControl != null && optHoaDon.CheckedIndex == 1)
            {
                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1" && !_select.InvoiceNo.IsNullOrEmpty() && _select.StatusInvoice == 1)
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Hệ thống đang được tích hợp hóa đơn điện tử và hóa đơn này đã được phát hành");
                        optHoaDon.CheckedIndex = 0;
                    }
                    else
                    {
                        //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                        //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                        //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                        //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                        //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                        //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                        _select.InvoiceForm = null;
                        _select.InvoiceTypeID = null;
                        _select.InvoiceTemplate = null;
                        _select.InvoiceSeries = null;
                        _select.InvoiceNo = null;
                        _select.InvoiceDate = null;
                        uGridControl.ConfigSizeGridCurrency(this, false);
                    }
                }
            }
            else
            {
                if (_statusForm == ConstFrm.optStatusForm.Add)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
                    {
                        if (_select.InvoiceForm == null)
                        {
                            _select.InvoiceForm = 2;
                            UltraCombo ultraComboInvoiceTypeID = uGridControl.Rows[0].Cells["InvoiceTypeID"].ValueListResolved as UltraCombo;
                            if (ultraComboInvoiceTypeID != null && ultraComboInvoiceTypeID.DataSource != null)
                            {
                                List<InvoiceType> lst = ultraComboInvoiceTypeID.DataSource as List<InvoiceType>;
                                if (lst.Count == 1)
                                {
                                    _select.InvoiceTypeID = lst.First().ID;
                                    UltraCombo ultraComboInvoiceTemplate = uGridControl.Rows[0].Cells["InvoiceTemplate"].ValueList as UltraCombo;
                                    if (ultraComboInvoiceTemplate != null && ultraComboInvoiceTemplate.DataSource != null)
                                    {
                                        List<TT153Report> tT153Reports = ultraComboInvoiceTemplate.DataSource as List<TT153Report>;
                                        if (tT153Reports.Count == 1)
                                        {
                                            _select.InvoiceTemplate = tT153Reports.First().InvoiceTemplate;
                                            _select.InvoiceSeries = tT153Reports.First().InvoiceSeries;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _select.InvoiceTypeID = null;
                                _select.InvoiceTemplate = null;
                                _select.InvoiceSeries = null;
                                _select.InvoiceNo = null;
                                _select.InvoiceDate = null;
                            }
                        }
                    }
                }
            }
            _select.IsBill = optHoaDon.CheckedIndex == 0;
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<SAInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.Exported);
                }
            }
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
        }
        private void OptPhieuXuatKhoValueChangedStand(object sender, EventArgs e)
        {//opt Phiếu xuất kho value change
            if (/*_isFirst && _statusForm == ConstFrm.optStatusForm.Add &&*/ optPhieuXuatKho.CheckedIndex == 0)
            {
                bool isreset = (__isFirstKho && _statusForm == ConstFrm.optStatusForm.Add && optPhieuXuatKho.CheckedIndex == 0);
                this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoPxk, "OutwardNo", "SPostedDate", "SDate", ConstFrm.TypeGroup_RSInwardOutwardOutput, resetNo: isreset);
                __isFirstKho = false;
                _backSelect.OutwardNo = _select.OutwardNo.CloneObject();
            }
            if (_statusForm == ConstFrm.optStatusForm.Edit)
            {
                if (_select.IsDeliveryVoucher == false)
                {
                    checkChungTuThamChieuPhieuXuatKhoCuChua = true;
                }
            }
            UltraOptionSet optionSet;
            try { optionSet = (UltraOptionSet)sender; if (optionSet == null || _select == null) return; }
            catch { return; }
            uGrid0 = (UltraGrid)palGrid.Controls.Find("uGrid0", true).FirstOrDefault();
            if (optionSet.CheckedItem.Tag.Equals("KKPXK"))
            {
                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {// trường hợp này chỉ check các chứng từ nào tham chiếu đến phiếu xuất kho, khi về sau phiếu xuất kho lập kèm mở ra cho sửa thì sẽ phải check thêm trường hợp phiếu xuất kho tham chiếu đến những chứng từ nào
                    List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("41") && n.No == _select.OutwardNo).ToList();
                    List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("41") && n.No == _select.OutwardNo).ToList();
                    if ((listrefVouchers.Count != 0 && !checkChungTuThamChieuPhieuXuatKhoCuChua) || (listrefVouchersRSInwardOutward.Count != 0 && !checkChungTuThamChieuPhieuXuatKhoCuChua))
                    {
                        if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            _select.OutwardNo = null;
                            _select.IsDeliveryVoucher = false;
                            checkChungTuThamChieuPhieuXuatKhoCuChua = true;
                        }
                        else
                        {
                            optionSet.CheckedIndex = 0;
                        }
                    }
                    else
                    {
                        _select.OutwardNo = null;
                        _select.IsDeliveryVoucher = false;
                    }
                }
                else
                {
                    _select.OutwardNo = null;
                    _select.IsDeliveryVoucher = false;

                }

            }
            else
            {
                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {
                    if (_select.OutwardRefID == null || _select.OutwardRefID == Guid.Empty)
                    {
                        if (!_isFirst && _select.TypeID == _backSelect.TypeID)
                        {
                            _select.OutwardNo = _backSelect.OutwardNo.CloneObject();
                            _select.IsDeliveryVoucher = true;
                            var topvoucher = palVouchersNoPxk.GetTopVouchers<SAInvoice>();
                            if (topvoucher != null)
                            {
                                topvoucher.No = _select.OutwardNo;
                            }
                        }
                        else
                        {
                            if (_isFirst && _statusForm != ConstFrm.optStatusForm.Add)
                            { }
                            else
                            {
                                this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoPxk, "OutwardNo", "SPostedDate", "SDate", ConstFrm.TypeGroup_RSInwardOutwardOutput, resetNo: false);
                                _select.IsDeliveryVoucher = true;
                            }

                        }
                    }
                    else
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Chứng từ đã được đính kèm bởi phiếu xuất");
                        optionSet.CheckedIndex = 1;
                    }
                }
                else
                {
                    if (!_isFirst && _select.TypeID == _backSelect.TypeID)
                    {
                        _select.OutwardNo = _backSelect.OutwardNo.CloneObject();
                        _select.IsDeliveryVoucher = true;
                        var topvoucher = palVouchersNoPxk.GetTopVouchers<SAInvoice>();
                        if (topvoucher != null)
                        {
                            topvoucher.No = _select.OutwardNo;
                        }
                    }
                    else
                    {
                        if (_isFirst && _statusForm != ConstFrm.optStatusForm.Add)
                        { }
                        else
                        {
                            this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoPxk, "OutwardNo", "SPostedDate", "SDate", ConstFrm.TypeGroup_RSInwardOutwardOutput, resetNo: false);
                            _select.IsDeliveryVoucher = true;
                        }

                    }
                }
            }
            if (_statusForm != ConstFrm.optStatusForm.View)
                btnOutwardNo.Enabled = optionSet.CheckedItem.Tag.Equals("KKPXK");
            btnOutwardNo.Tag = new Dictionary<string, object> { { "Enabled", optionSet.CheckedItem.Tag.Equals("KKPXK") } };
            group.Tabs["tabPhieuXuatKho"].Visible = (_select.OutwardNo != null && !string.IsNullOrEmpty(_select.OutwardNo));
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<SAInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.Exported);
                }
            }
        }

        private void ultraTabControlThongTinChung_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            palVouchersNoHd.Visible = e.Tab.Key.Equals("tabHoaDon");
            palVouchersNoPxk.Visible = e.Tab.Key.Equals("tabPhieuXuatKho");
            palVouchersNoTt.Visible = e.Tab.Key.Equals("tabPhieuThu");
            palVouchersNoGbc.Visible = e.Tab.Key.Equals("tabGiayBaoCo");
            bool value = (e.Tab.Key.Equals("tabHoaDon1") && optHoaDon.CheckedIndex == 0);
            if (uGridControl != null)
            {
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                uGridControl.ConfigSizeGridCurrency(this, false);
            }
            //palVouchersNoGbc.Visible = e.Tab.Key.Equals("tabGiayBaoCo");
        }

        private void chkIsDiscountByVoucher_CheckStateChanged(object sender, EventArgs e)
        {
            if (_statusForm != ConstFrm.optStatusForm.View)
                btnIsDiscountByVoucher.Enabled = chkIsDiscountByVoucher.CheckState == CheckState.Checked;
            btnIsDiscountByVoucher.Tag = new Dictionary<string, object> { { "Enabled", chkIsDiscountByVoucher.CheckState == CheckState.Checked } };
        }
        private void cbbAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (cbbAccountingObjectID.Text != null && !string.IsNullOrEmpty(cbbAccountingObjectID.Text) && Utils.ListAccountingObject.Any(x => x.AccountingObjectCode == cbbAccountingObjectID.Text) && _statusForm != ConstFrm.optStatusForm.View)
            {
                foreach (var row in uGrid0.Rows)
                {
                    decimal a = 0/*_ISAPolicyPriceSettingService.UnitPrice((Guid)row.Cells["MaterialGoodsID"].Value, (Guid)cbbAccountingObjectID.Value)*/;
                    var lst1 = Utils.ListSAPolicySalePriceCustomer.ToList().Where(x => x.AccountingObjectID == (Guid)cbbAccountingObjectID.Value).ToList();
                    var lst2 = Utils.ListSAPolicySalePriceGroup.ToList().Where(x => lst1.Any(d => d.SAPolicySalePriceGroupID == x.ID)).ToList();
                    var lst3 = new List<SAPolicyPriceTable>();
                    if (row.Cells["MaterialGoodsID"].Value != null)
                        lst3 = Utils.ListSAPolicyPriceTable.ToList().Where(x => lst2.Any(d => d.SAPolicyPriceSettingID == x.SAPolicyPriceSettingID)).Where(x => x.MaterialGoodsID == (Guid)row.Cells["MaterialGoodsID"].Value).ToList(); ;
                    if (lst3.Count > 0) a = lst3[0].Price;
                    if (a > 0)
                        row.Cells["UnitPriceOriginal"].Value = a;
                }
            }
        }
        private void cbBill_CheckStateChanged(object sender, EventArgs e)
        {
            if (_statusForm != ConstFrm.optStatusForm.View)
            {
                btnSelectBill.Enabled = cbBill.CheckState == CheckState.Checked;
                optHoaDon.Enabled = cbBill.CheckState != CheckState.Checked;
                if (cbBill.CheckState != CheckState.Checked)
                {
                    _select.BillRefID = null;
                    panel1.Visible = false;
                }
            }
            btnSelectBill.Tag = new Dictionary<string, object> { { "Enabled", cbBill.CheckState == CheckState.Checked } };
        }

        private void btnSelectBill_Click(object sender, EventArgs e)
        {
            var f = new FchoseBill<SAInvoice>(_listObjectInput, _select, _select.AccountingObjectID);
            f.FormClosed += new FormClosedEventHandler(FchoseBill_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void FchoseBill_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FchoseBill<SAInvoice>)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectSAs = frm._lstSA;
                if (uGrid0 != null)
                {
                    var source = (BindingList<SAInvoiceDetail>)uGrid0.DataSource;
                    List<SAInvoiceDetail> oldvalue = source.ToList().CloneObject();
                    oldvalue = oldvalue.CloneObject();
                    if (selectSAs != null) source.Clear();
                    _select.BillRefID = selectSAs.ID;
                    _select.IsAttachListBill = true;
                    foreach (var selectOrder in selectSAs.SABillDetails)
                    {
                        uGrid0.AddNewRow4Grid();
                        var row = uGrid0.Rows[uGrid0.Rows.Count - 1];
                        row.Cells["MaterialGoodsID"].Value = selectOrder.MaterialGoodsID;
                        row.UpdateData();
                        uGrid0.UpdateData();
                        row.Cells["Description"].Value = selectOrder.Description;
                        row.Cells["RepositoryID"].Value = oldvalue.Any(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID) ? oldvalue.FirstOrDefault(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID).RepositoryID : null;
                        row.Cells["DebitAccount"].Value = oldvalue.Any(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID) ? oldvalue.FirstOrDefault(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID).DebitAccount : null;
                        row.Cells["CreditAccount"].Value = oldvalue.Any(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID) ? oldvalue.FirstOrDefault(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID).CreditAccount : null;
                        row.Cells["Quantity"].Value = selectOrder.Quantity;
                        row.Cells["UnitPrice"].Value = selectOrder.UnitPrice;
                        row.Cells["UnitPriceOriginal"].Value = selectOrder.UnitPriceOriginal;
                        row.Cells["Amount"].Value = selectOrder.Amount;
                        row.Cells["AmountOriginal"].Value = selectOrder.AmountOriginal;
                        row.Cells["AccountingObjectID"].Value = selectOrder.AccountingObjectID;
                        row.Cells["VATRate"].Value = selectOrder.VATRate;
                        row.Cells["VATAmount"].Value = selectOrder.VATAmount;
                        row.Cells["VATAmountOriginal"].Value = selectOrder.VATAmountOriginal;
                        row.Cells["VATAccount"].Value = oldvalue.Any(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID) ? oldvalue.FirstOrDefault(x => x.MaterialGoodsID == selectOrder.MaterialGoodsID).VATAccount : null;
                        row.Cells["DiscountRate"].Value = selectOrder.DiscountRate;
                        row.Cells["DiscountAmount"].Value = selectOrder.DiscountAmount;
                        row.Cells["DiscountAmountOriginal"].Value = selectOrder.DiscountAmountOriginal;
                        row.Cells["LotNo"].Value = selectOrder.LotNo;
                        row.Cells["ExpiryDate"].Value = selectOrder.ExpiryDate;
                        row.UpdateData();
                        //uGrid0.ActiveCell = row.Cells["Quantity"];
                        //this.DoEverythingInGrid<SAInvoice>(uGrid0);
                    }
                    _select.TotalAmount = ((BindingList<SAInvoiceDetail>)uGrid0.DataSource).ToList().Sum(x => x.Amount);
                    _select.TotalDiscountAmount = ((BindingList<SAInvoiceDetail>)uGrid0.DataSource).ToList().Sum(x => x.DiscountAmount);
                    _select.TotalVATAmount = ((BindingList<SAInvoiceDetail>)uGrid0.DataSource).ToList().Sum(x => x.VATAmount);
                }
                ConfigGroup(false);
                if (uGrid0.Rows.Count > 0)
                    foreach (var row in uGrid0.Rows)
                        foreach (var cell in row.Cells)
                            Utils.CheckErrorInCell<SAInvoice>(this, uGrid0, cell);
            }
        }
        private void txtThanhToan_TextChanged(object sender, EventArgs e)
        {
            _select.PaymentMethod = uComboThanhToan.Text;
        }
        private void cbbAccountingObjectID_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObjectID.Value != null)
            {
                DataBinding(new List<Control> { txtAccountingObjectName, }, "AccountingObjectName");
            }
        }
        #endregion

        #region Utils
        void ConfigByType(SAInvoice input, bool resetNo)
        {
            switch (input.TypeID)
            {
                case 320: //bán hàng chưa thanh toán
                    #region bán hàng chưa thu tiền
                    {
                        //optThanhToan.CheckedIndex = 0;  //tự động chọn "chưa thanh toán"
                        //optThanhToan.Value = false;
                        //OptThanhToanValueChangedStand(optThanhToan, new EventArgs());
                        optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        //cbbChonThanhToan.Visible = false;
                        lblTT.Visible = false;
                    }

                    #endregion
                    break;
                case 321: //bán hàng thu tiền ngay tiền mặt
                    #region bán hàng thu tiền ngay tiền mặt
                    {
                        cbbChonThanhToan.SelectedIndex = 0; //mặc định là tiền mặt
                        //optThanhToan.Value = true;  //tự động chọn "thanh toán ngay"
                        optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoTt, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroup_MCReceipt, objectDataSource: _select, resetNo: resetNo);
                    }
                    #endregion
                    break;
                case 322: //bán hàng thu tiền ngay chuyển khoản
                    #region bán hàng thu tiền ngay chuyển khoản
                    {
                        cbbChonThanhToan.SelectedIndex = 1; //mặc định là chuyển khoản
                        //optThanhToan.Value = true;  //tự động chọn "thanh toán ngay"
                        optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoGbc, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroupMBDeposit, objectDataSource: _select, resetNo: resetNo);
                    }
                    #endregion
                    break;
                case 323: //bán hàng từ đại lý bán đúng giá ủy thác XNK chưa thanh toán
                    #region bán hàng từ đại lý bán đúng giá ủy thác XNK chưa thanh toán
                    {
                        //optThanhToan.Value = false;  //tự động chọn "chưa thanh toán"
                        //optThanhToan.Visible = true;   //hiện phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;
                    }
                    #endregion
                    break;
                case 324: //bán hàng từ đại lý bán đúng giá ủy thác XNK tiền mặt
                    #region bán hàng từ đại lý bán đúng giá ủy thác XNK tiền mặt
                    {
                        cbbChonThanhToan.SelectedIndex = 0; //mặc định là tiền mặt
                        //optThanhToan.Value = true;  //tự động chọn "thanh toán ngay"
                        //optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoTt, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroup_MCReceipt, objectDataSource: _select, resetNo: resetNo);
                    }
                    #endregion
                    break;
                case 325: //bán hàng từ đại lý bán đúng giá ủy thác XNK chuyển khoản
                    #region bán hàng từ đại lý bán đúng giá ủy thác XNK chuyển khoản
                    {
                        cbbChonThanhToan.SelectedIndex = 1; //mặc định là chuyển khoản
                        //optThanhToan.Value = true;  //tự động chọn "thanh toán ngay"
                        //optThanhToan.Visible = false;   //ẩn phần chọn "chưa thanh toán" hay "thanh toán ngay"
                        cbbChonThanhToan.Visible = true;    //hiện cbb cho chọn thanh toán ngay bằng "tiền mặt" hay "chuyển khoản"
                        this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoGbc, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroupMBDeposit, objectDataSource: _select, resetNo: resetNo);
                    }
                    #endregion
                    break;
            }
            if (!resetNo && !_isFirst && _select.TypeID == _backSelect.TypeID)
            {
                _select.MNo = _backSelect.MNo.CloneObject();
                var topvoucher2 = palVouchersNoTt.GetTopVouchers<SAInvoice>();
                if (topvoucher2 != null)
                {
                    topvoucher2.No = _select.MNo;
                }
            }
        }
        private void ConfigGroup(bool status = true)
        {
            ConfigByType(_select, status);
            if (TypeGroup == null) return;
            this.ReconfigAccountColumn<SAInvoice>(_select.TypeID);
            if (_select.TemplateID == null) return;
            //this.ReloadTemplate<SAInvoice>((Guid)_select.TemplateID, false);
            this.ConfigTemplateEdit<SAInvoice>(utmDetailBaseToolBar, TypeID, _statusForm, _select.TemplateID);
            _isFirst = false;
            bool value = (group.SelectedTab == group.Tabs["tabHoaDon1"] && optHoaDon.CheckedIndex == 0);
            if (uGridControl != null)
            {
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                uGridControl.ConfigSizeGridCurrency(this, false);
            }
        }
        #endregion

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }
        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FSAInvoiceDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
            //log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDPXK Bước 1.7");
            //this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDPXK, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
            //log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDPT Bước 1.8");
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDPT, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            //log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDGBC Bước 1.9");
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDGBC, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            ////this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
            //log.Error("Khởi tạo InitializeGUI.cbbBankAccountDetailID Bước 1.10");
            //this.ConfigCombo(Utils.ListBankAccountDetail, cbbBankAccountDetailID, "BankAccount", "ID", input, "BankAccountDetailID");
            ////this.ConfigCombo(Utils.ListAccountingObjectBank, cbbAccountingObjectBankAccount, "BankAccount", "BankAccount", input, "AccountingObjectBankAccount");
            //log.Error("Khởi tạo InitializeGUI.cbbAccountingObjectIDHD Bước 1.11");
            //this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
            cbbAccountingObjectID.Dispose();
            cbbAccountingObjectID = null;

            cbbAccountingObjectIDPXK.Dispose();
            cbbAccountingObjectIDPXK = null;

            cbbAccountingObjectIDPT.Dispose();
            cbbAccountingObjectIDPT = null;

            cbbAccountingObjectIDGBC.Dispose();
            cbbAccountingObjectIDGBC = null;

            cbbBankAccountDetailID.Dispose();
            cbbBankAccountDetailID = null;

            cbbAccountingObjectIDHD.Dispose();
            cbbAccountingObjectIDHD = null;

            GC.Collect();
            GC.Collect();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void FSAInvoiceDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                cbbAccountingObjectID.DataSource = null;
                cbbAccountingObjectID.Dispose();

                cbbAccountingObjectIDPXK.DataSource = null;
                cbbAccountingObjectIDPXK.Dispose();

                cbbAccountingObjectIDPT.DataSource = null;
                cbbAccountingObjectIDPT.Dispose();

                cbbAccountingObjectIDGBC.DataSource = null;
                cbbAccountingObjectIDGBC.Dispose();

                cbbBankAccountDetailID.DataSource = null;
                cbbBankAccountDetailID.Dispose();

                cbbAccountingObjectIDHD.DataSource = null;
                cbbAccountingObjectIDHD.Dispose();
                this.Dispose();
            }
            catch
            {

            }
        }

        private void txtAccountingObjectAddress_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }

        private void uGridControl_AfterExitEditMode(object sender, EventArgs e)
        {

        }

        private void uGridControl_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "Exported")
            {

                if (e.Cell.Text == "Trong nước")
                {
                    UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
                    if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
                    {
                        if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                        {
                            Utils.ConfigAccountColumnInGrid<SAInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: false);
                        }
                    }
                }
                else if (e.Cell.Text == "Nhập khẩu")
                {
                    UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
                    if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
                    {
                        if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                        {
                            Utils.ConfigAccountColumnInGrid<SAInvoice>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: true);
                        }
                    }
                }

            }
        }
        private void AddCbb(SAInvoice input)
        {
            Dictionary<int, string> listIT = new Dictionary<int, string>();
            listIT.Add(2, "Hóa đơn điện tử");
            listIT.Add(1, "Hóa đơn đặt in");
            listIT.Add(0, "Hóa đơn tự in");
            cbbHinhThucHD.DataSource = listIT;
            cbbHinhThucHD.DisplayMember = "Value";
            cbbHinhThucHD.ValueMember = "Key";//trungnq
            Utils.ConfigGrid(cbbHinhThucHD, ConstDatabase.TMIndustryTypeGH_TableName);
            cbbHinhThucHD.Value = input.InvoiceForm;
            cbbHinhThucHD.RowSelected += new RowSelectedEventHandler(combobox_selected);

            List<Guid> lstPLInvoice = Utils.ListTT153PublishInvoiceDetail.Select(n => n.InvoiceTypeID).ToList();
            var data = Utils.ListInvoiceType.Where(n => lstPLInvoice.Any(m => m == n.ID)).ToList();
            cbbLoaiHD.DataSource = data;
            cbbLoaiHD.ValueMember = "ID";
            cbbLoaiHD.DisplayMember = "InvoiceTypeName";
            cbbLoaiHD.RowSelected += new RowSelectedEventHandler(combobox_selected);
            Utils.ConfigGrid(cbbLoaiHD, ConstDatabase.InvoiceType_TableName);
            cbbLoaiHD.Value = input.InvoiceTypeID;
            dteNgayHD.Value = input.InvoiceDate;
            txtSoHD.Text = input.InvoiceNo;
            cbbMauHD.Value = new Guid();
            if (cbbLoaiHD.Value != null && cbbHinhThucHD.Value != null)
            {
                var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == input.InvoiceTemplate);
                if (templateInvoid != null)
                    cbbMauHD.Value = templateInvoid.ID;
                else
                    cbbMauHD.Value = null;
            }
            cbbMauHD.RowSelected += new RowSelectedEventHandler(combobox_selected);
        }
        private void combobox_selected(object sender, RowSelectedEventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.Name == "cbbHinhThucHD")
            {
                List<TT153Report> lstinvoice = new List<TT153Report>();
                _select.InvoiceForm = int.Parse(cbb.Value.ToString());
                if (cbbLoaiHD.Value != null)
                {
                    var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbb.Value.ToString())).ToList();
                    lstinvoice = (from a in lstinvoice1
                                  join b in Utils.ITT153PublishInvoiceDetailService.Query
                                       on a.ID equals b.TT153ReportID
                                  select a).Distinct().ToList();
                    cbbMauHD.DataSource = lstinvoice;
                    cbbMauHD.ValueMember = "ID";
                    cbbMauHD.DisplayMember = "InvoiceTemplate";
                    Utils.ConfigGrid(cbbMauHD, ConstDatabase.TT153Report_TableName);
                    _select.InvoiceTypeID = Guid.Parse(cbbLoaiHD.Value.ToString());
                    if (lstinvoice.Count == 1)
                    {
                        var gt = lstinvoice.FirstOrDefault();
                        if (gt != null)
                        {
                            cbbMauHD.Value = gt.ID != null ? gt.ID : new Guid();
                            txtKyHieuHD.Text = _select.InvoiceSeries != null ? _select.InvoiceSeries : null;
                            _select.InvoiceSeries = _select.InvoiceSeries != null ? _select.InvoiceSeries : null;
                            _select.InvoiceTemplate = _select.InvoiceTemplate != null ? _select.InvoiceTemplate : null;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {

                                List<SAInvoice> lst = new List<SAInvoice>();
                                List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                                List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                   x => new SAInvoice
                                   {
                                       InvoiceTypeID = x.InvoiceTypeID,
                                       InvoiceForm = x.InvoiceForm,
                                       InvoiceTemplate = x.InvoiceTemplate,
                                       InvoiceNo = x.InvoiceNo
                                   }
                                   ).ToList();
                                if (lstsA.Count > 0) lst.AddRange(lstsA);
                                if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                                if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                                List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                                if (ls.Count > 0)
                                {
                                    int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                    txtSoHD.Text = maxno.ToString("0000000");
                                }
                                else
                                {
                                    txtSoHD.Text = "0000001";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            cbbMauHD.Value = null;
                            txtKyHieuHD.Text = null;
                            txtSoHD.Text = null;
                        }
                        else
                        {
                            var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == _select.InvoiceTemplate);
                            if (templateInvoid != null)
                            {
                                cbbMauHD.Value = templateInvoid.ID;
                                txtKyHieuHD.Text = templateInvoid.InvoiceSeries;
                            }
                            else
                            {
                                cbbMauHD.Value = null;
                                txtKyHieuHD.Text = null;
                            }
                        }

                    }
                }
                else
                {
                    cbbMauHD.Value = null;
                    txtKyHieuHD.Text = null;
                }

            }
            if (cbb.Name == "cbbLoaiHD")
            {
                List<TT153Report> lstinvoice = new List<TT153Report>();
                _select.InvoiceTypeID = Guid.Parse(cbbLoaiHD.Value.ToString());
                if (cbbHinhThucHD.Value != null)
                {
                    var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                    lstinvoice = (from a in lstinvoice1
                                  join b in Utils.ITT153PublishInvoiceDetailService.Query
                                       on a.ID equals b.TT153ReportID
                                  select a).Distinct().ToList();
                    cbbMauHD.DataSource = lstinvoice;
                    cbbMauHD.ValueMember = "ID";
                    cbbMauHD.DisplayMember = "InvoiceTemplate";
                    Utils.ConfigGrid(cbbMauHD, ConstDatabase.TT153Report_TableName);
                    _select.InvoiceForm = int.Parse(cbbHinhThucHD.Value.ToString());
                    if (lstinvoice.Count == 1)
                    {
                        var gt = lstinvoice.FirstOrDefault();
                        if (gt != null)
                        {
                            cbbMauHD.Value = gt.ID;
                            txtKyHieuHD.Text = gt.InvoiceSeries;
                            _select.InvoiceSeries = gt.InvoiceSeries;
                            _select.InvoiceTemplate = gt.InvoiceTemplate;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {
                                List<SAInvoice> lst = new List<SAInvoice>();
                                List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                                List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                   x => new SAInvoice
                                   {
                                       InvoiceTypeID = x.InvoiceTypeID,
                                       InvoiceForm = x.InvoiceForm,
                                       InvoiceTemplate = x.InvoiceTemplate,
                                       InvoiceNo = x.InvoiceNo
                                   }
                                   ).ToList();
                                if (lstsA.Count > 0) lst.AddRange(lstsA);
                                if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                                if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                                List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                                if (ls.Count > 0)
                                {
                                    int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                    txtSoHD.Text = maxno.ToString("0000000");
                                }
                                else
                                {
                                    txtSoHD.Text = "0000001";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            cbbMauHD.Value = null;
                            txtKyHieuHD.Text = null;
                            txtSoHD.Text = null;
                        }
                        else
                        {
                            var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == _select.InvoiceTemplate);
                            if (templateInvoid != null)
                            {
                                cbbMauHD.Value = templateInvoid.ID;
                                txtKyHieuHD.Text = templateInvoid.InvoiceSeries;
                            }
                            else
                            {
                                cbbMauHD.Value = null;
                                txtKyHieuHD.Text = null;
                            }
                        }
                    }

                }
                else
                {
                    cbbMauHD.Value = null;
                    txtKyHieuHD.Text = null;
                }
            }
            if (cbb.Name == "cbbMauHD")
            {
                Guid guid = cbb.Value != null ? new Guid(cbb.Value.ToString()) : new Guid();

                var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                if (lstinvoice1.Count == 1)
                {
                    var gt = lstinvoice1.FirstOrDefault();
                    if (gt != null)
                    {
                        _select.InvoiceTemplate = gt.InvoiceTemplate;
                        txtKyHieuHD.Text = gt.InvoiceSeries;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            List<SAInvoice> lst = new List<SAInvoice>();
                            List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                            List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                x => new SAInvoice
                                {
                                    InvoiceTypeID = x.InvoiceTypeID,
                                    InvoiceForm = x.InvoiceForm,
                                    InvoiceTemplate = x.InvoiceTemplate,
                                    InvoiceNo = x.InvoiceNo
                                }
                                    ).ToList();
                            List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                               x => new SAInvoice
                               {
                                   InvoiceTypeID = x.InvoiceTypeID,
                                   InvoiceForm = x.InvoiceForm,
                                   InvoiceTemplate = x.InvoiceTemplate,
                                   InvoiceNo = x.InvoiceNo
                               }
                               ).ToList();
                            if (lstsA.Count > 0) lst.AddRange(lstsA);
                            if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                            if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                            List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                            if (ls.Count > 0)
                            {
                                int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                txtSoHD.Text = maxno.ToString("0000000");
                            }
                            else
                            {
                                txtSoHD.Text = "0000001";
                            }
                        }
                    }
                }
                else
                {
                    var gttemp = lstinvoice1.FirstOrDefault(t => t.ID == guid);
                    if (gttemp != null)
                    {
                        txtKyHieuHD.Text = gttemp.InvoiceSeries;
                        _select.InvoiceTemplate = gttemp.InvoiceTemplate;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            List<SAInvoice> lst = new List<SAInvoice>();
                            List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                            List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                x => new SAInvoice
                                {
                                    InvoiceTypeID = x.InvoiceTypeID,
                                    InvoiceForm = x.InvoiceForm,
                                    InvoiceTemplate = x.InvoiceTemplate,
                                    InvoiceNo = x.InvoiceNo
                                }
                                    ).ToList();
                            List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                               x => new SAInvoice
                               {
                                   InvoiceTypeID = x.InvoiceTypeID,
                                   InvoiceForm = x.InvoiceForm,
                                   InvoiceTemplate = x.InvoiceTemplate,
                                   InvoiceNo = x.InvoiceNo
                               }
                               ).ToList();
                            if (lstsA.Count > 0) lst.AddRange(lstsA);
                            if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                            if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                            List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                            if (ls.Count > 0)
                            {
                                int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                txtSoHD.Text = maxno.ToString("0000000");
                            }
                            else
                            {
                                txtSoHD.Text = "0000001";
                            }
                        }
                    }
                }
            }
        }

        private void dteNgayHD_Leave(object sender, EventArgs e)
        {
            try
            {
                _select.InvoiceDate = dteNgayHD.Value == null ? (DateTime?)null : Convert.ToDateTime(dteNgayHD.Value.ToString());
            }
            catch (Exception)
            {


            }

        }

        private void dteNgayHD_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                _select.InvoiceDate = dteNgayHD.Value == null ? (DateTime?)null : Convert.ToDateTime(dteNgayHD.Value.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtSoHD_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceNo = txtSoHD.Text;
        }

        private void txtKyHieuHD_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceSeries = txtKyHieuHD.Text;
        }
    }

    public class FSaInvoiceDetailStand : DetailBase<SAInvoice> { }
}