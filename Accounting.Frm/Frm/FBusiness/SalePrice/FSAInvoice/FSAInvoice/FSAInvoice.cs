﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm
{
    public partial class FSAInvoice : CustormForm //UserControl
    {
        #region khai báo
        /// <summary>
        /// 0: bán hàng chưa thu tiền, 1: bán hàng đã thu tiền, 2: bán hàng đại lý bán đúng giá nhận ủy thác xuất nhập khẩu, -1: chưa thiết lập
        /// </summary>
        public static int LoaiFormBanHang = -1;

        private readonly ISAInvoiceService _iSaInvoiceService;
        List<SAInvoice> _dsSaInvoice = new List<SAInvoice>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FSAInvoice()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _iSaInvoiceService = IoC.Resolve<ISAInvoiceService>();
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            int year = (dbStartDate == null ? DateTime.Now.Year : dbStartDate.Value.Year);
            //lọc theo TypeID => bán hàng chưa thu tiền, bán hàng thu tiền ngay, bán hàng khác
            List<int> dsTypeID = new List<int>();
            switch (LoaiFormBanHang)
            {
                case 0: //bán hàng chưa thu tiền
                    dsTypeID = new List<int> { 320 };
                    break;
                case 1: //bán hàng thu tiền ngay
                    dsTypeID = new List<int> { 321, 322 };
                    break;
                case 2: //bán hàng khác
                    dsTypeID = new List<int>();
                    break;
            }
            _dsSaInvoice = _iSaInvoiceService.GetSaInvoices(year, dsTypeID);
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsSaInvoice;

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void BtnAddClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {
            SAInvoice temp = uGrid.Selected.Rows.Count <= 0 ? new SAInvoice() : uGrid.Selected.Rows[0].ListObject as SAInvoice;
            temp.TypeID = 320;
            new FSAInvoiceDetail(temp, _dsSaInvoice, ConstFrm.optStatusForm.Add).ShowDialog(this);
            if (!FSAInvoiceDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SAInvoice temp = uGrid.Selected.Rows[0].ListObject as SAInvoice;
                new FSAInvoiceDetail(temp, _dsSaInvoice, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FSAInvoiceDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SAInvoice temp = uGrid.Selected.Rows[0].ListObject as SAInvoice;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _iSaInvoiceService.BeginTran();
                    _iSaInvoiceService.Delete(temp);
                    _iSaInvoiceService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.SAInvoice_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }

        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            return dsTemplate[keyofdsTemplate];
        }

        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }
        #endregion

        #region Event
        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            SAInvoice saInvoice = listObject as SAInvoice;
            if (saInvoice == null) return;

            //Tab thông tin chung
            lblNo.Text = saInvoice.No;
            lblDate.Text = saInvoice.Date.ToString(Constants.DdMMyyyy);
            lblAccountingObjectName.Text = saInvoice.AccountingObjectName;
            lblAccountingObjectAddress.Text = saInvoice.AccountingObjectAddress;
            lblReason.Text = saInvoice.Reason;
            lblTotalAmount.Text = string.Format(saInvoice.TotalAmount == 0 ? "0" : "{0:0,0}", saInvoice.TotalAmount);  //SAInvoice.TotalAmount.ToString("{0:0,0}");

            //Tab hoạch toán
            uGridPosted.DataSource = saInvoice.SAInvoiceDetails.ToList();    //Utils.CloneObject(SAInvoice.SAInvoiceDetails);

            #region Config Grid
            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            Template mauGiaoDien = GetMauGiaoDien(saInvoice.TypeID, saInvoice.TemplateID, DsTemplate);

            Utils.ConfigGrid(uGridPosted, ConstDatabase.SAInvoiceDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            //SummarySettings summary = band.Summaries.Add("SumAmount", SummaryType.Sum, band.Columns["Amount"]);
            //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            //summary.DisplayFormat = "{0:N0}";
            //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }

        private void FsaInvoiceFormClosing(object sender, FormClosingEventArgs e)
        {
            //giải phóng biến static khi đóng form giúp giải phóng tài nguyên, tuy nhiên nếu để lại thì tốc độ truy cập lần sau sẽ tăng nếu mở form lại
            //DsTemplate = new Dictionary<string, Template>();
        }
        #endregion

        private void UGridInitializeLayout(object sender, InitializeLayoutEventArgs e)
        {//Thiết lập cho Grid
            UltraGrid ultraGrid = (UltraGrid)sender;
            //hiển thị 1 band
            ultraGrid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            ultraGrid.DisplayLayout.Override.AllowRowFiltering = ultraGrid.Name.Equals("uGrid") ? Infragistics.Win.DefaultableBoolean.True : Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.FilterUIType = ultraGrid.Name.Equals("uGrid") ? FilterUIType.FilterRow : FilterUIType.Default;
            //tự thay đổi kích thước cột
            ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            ultraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            ultraGrid.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            ultraGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            ultraGrid.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            ultraGrid.DisplayLayout.UseFixedHeaders = true;
        }
    }
}
