﻿namespace Accounting
{
    partial class FIsDiscountByVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FIsDiscountByVoucher));
            this.palTop = new System.Windows.Forms.Panel();
            this.txtTongChietKhau = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.btnPhanBo = new Infragistics.Win.Misc.UltraButton();
            this.optPhanBo = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.lblTongChietKhau = new Infragistics.Win.Misc.UltraLabel();
            this.palFill = new System.Windows.Forms.Panel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBottom = new System.Windows.Forms.Panel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optPhanBo)).BeginInit();
            this.palFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.txtTongChietKhau);
            this.palTop.Controls.Add(this.btnPhanBo);
            this.palTop.Controls.Add(this.optPhanBo);
            this.palTop.Controls.Add(this.lblTongChietKhau);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(688, 38);
            this.palTop.TabIndex = 0;
            // 
            // txtTongChietKhau
            // 
            appearance1.TextHAlignAsString = "Right";
            this.txtTongChietKhau.Appearance = appearance1;
            this.txtTongChietKhau.AutoSize = false;
            this.txtTongChietKhau.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtTongChietKhau.InputMask = "{LOC}nn,nnn,nnn,nnn,nnn";
            this.txtTongChietKhau.Location = new System.Drawing.Point(106, 9);
            this.txtTongChietKhau.MaxValue = 10000000000000D;
            this.txtTongChietKhau.MinValue = 0D;
            this.txtTongChietKhau.Name = "txtTongChietKhau";
            this.txtTongChietKhau.NullText = "0";
            this.txtTongChietKhau.PromptChar = ' ';
            this.txtTongChietKhau.Size = new System.Drawing.Size(198, 21);
            this.txtTongChietKhau.TabIndex = 1012;
            // 
            // btnPhanBo
            // 
            this.btnPhanBo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance2.Image = global::Accounting.Properties.Resources.allocation;
            this.btnPhanBo.Appearance = appearance2;
            this.btnPhanBo.Location = new System.Drawing.Point(600, 7);
            this.btnPhanBo.Name = "btnPhanBo";
            this.btnPhanBo.Size = new System.Drawing.Size(77, 25);
            this.btnPhanBo.TabIndex = 74;
            this.btnPhanBo.Text = "Phân bổ";
            this.btnPhanBo.Click += new System.EventHandler(this.BtnPhanBoClick);
            // 
            // optPhanBo
            // 
            this.optPhanBo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.optPhanBo.Appearance = appearance3;
            this.optPhanBo.BackColor = System.Drawing.Color.Transparent;
            this.optPhanBo.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPhanBo.CheckedIndex = 0;
            valueListItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem3.DataValue = "Default Item";
            valueListItem3.DisplayText = "Phân bổ theo số lượng";
            valueListItem3.Tag = "SL";
            valueListItem5.DataValue = "ValueListItem1";
            valueListItem5.DisplayText = "Phân bổ theo giá trị";
            valueListItem5.Tag = "GT";
            this.optPhanBo.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem5});
            this.optPhanBo.Location = new System.Drawing.Point(323, 11);
            this.optPhanBo.Name = "optPhanBo";
            this.optPhanBo.Size = new System.Drawing.Size(268, 20);
            this.optPhanBo.TabIndex = 31;
            this.optPhanBo.Text = "Phân bổ theo số lượng";
            // 
            // lblTongChietKhau
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.lblTongChietKhau.Appearance = appearance4;
            this.lblTongChietKhau.Location = new System.Drawing.Point(11, 10);
            this.lblTongChietKhau.Name = "lblTongChietKhau";
            this.lblTongChietKhau.Size = new System.Drawing.Size(89, 19);
            this.lblTongChietKhau.TabIndex = 26;
            this.lblTongChietKhau.Text = "Tổng chiết khấu";
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.uGrid);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 38);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(688, 282);
            this.palFill.TabIndex = 1;
            // 
            // uGrid
            // 
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(688, 282);
            this.uGrid.TabIndex = 0;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.AfterExitEditMode += new System.EventHandler(this.uGrid_AfterExitEditMode);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.btnClose);
            this.palBottom.Controls.Add(this.btnSave);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 320);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(688, 47);
            this.palBottom.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance5.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnClose.Appearance = appearance5;
            this.btnClose.Location = new System.Drawing.Point(600, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 73;
            this.btnClose.Text = "Hủy bỏ";
            this.btnClose.Click += new System.EventHandler(this.BtnCloseClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance6.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(519, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "Đồng ý";
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // FIsDiscountByVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 367);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FIsDiscountByVoucher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phân bổ chiết khấu";
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optPhanBo)).EndInit();
            this.palFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel lblTongChietKhau;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optPhanBo;
        private Infragistics.Win.Misc.UltraButton btnPhanBo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTongChietKhau;
    }
}