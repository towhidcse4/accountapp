﻿namespace Accounting
{
    partial class FSAReturn
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance("Hàng mua trả lại", 146035063);
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance("Hàng mua giảm giá", 146076797);
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNo = new System.Windows.Forms.Label();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAccountingObjectName = new System.Windows.Forms.Label();
            this.lblReason = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAccountingObjectAddress = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPosted = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAdd = new Infragistics.Win.Misc.UltraDropDownButton();
            this.cbbAdd = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAddHBTL = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAddHBGG = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmView = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cms4Button = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.hbtlcontex = new System.Windows.Forms.ToolStripMenuItem();
            this.HmtlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAdd)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.cms4Grid.SuspendLayout();
            this.panel3.SuspendLayout();
            this.cms4Button.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.label2);
            this.ultraTabPageControl1.Controls.Add(this.lblNo);
            this.ultraTabPageControl1.Controls.Add(this.lblTotalAmount);
            this.ultraTabPageControl1.Controls.Add(this.label7);
            this.ultraTabPageControl1.Controls.Add(this.label3);
            this.ultraTabPageControl1.Controls.Add(this.lblDate);
            this.ultraTabPageControl1.Controls.Add(this.label6);
            this.ultraTabPageControl1.Controls.Add(this.lblAccountingObjectName);
            this.ultraTabPageControl1.Controls.Add(this.lblReason);
            this.ultraTabPageControl1.Controls.Add(this.label9);
            this.ultraTabPageControl1.Controls.Add(this.label5);
            this.ultraTabPageControl1.Controls.Add(this.lblAccountingObjectAddress);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(757, 173);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số chứng từ";
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.BackColor = System.Drawing.Color.Transparent;
            this.lblNo.Location = new System.Drawing.Point(110, 8);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(10, 13);
            this.lblNo.TabIndex = 11;
            this.lblNo.Text = ":";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalAmount.Location = new System.Drawing.Point(111, 119);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(10, 13);
            this.lblTotalAmount.TabIndex = 18;
            this.lblTotalAmount.Text = ":";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(10, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "Số tiền";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(10, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày chứng từ";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Location = new System.Drawing.Point(111, 31);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(10, 13);
            this.lblDate.TabIndex = 12;
            this.lblDate.Text = ":";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(10, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Đối tượng";
            // 
            // lblAccountingObjectName
            // 
            this.lblAccountingObjectName.AutoSize = true;
            this.lblAccountingObjectName.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectName.Location = new System.Drawing.Point(111, 53);
            this.lblAccountingObjectName.Name = "lblAccountingObjectName";
            this.lblAccountingObjectName.Size = new System.Drawing.Size(10, 13);
            this.lblAccountingObjectName.TabIndex = 13;
            this.lblAccountingObjectName.Text = ":";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.BackColor = System.Drawing.Color.Transparent;
            this.lblReason.Location = new System.Drawing.Point(110, 98);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(10, 13);
            this.lblReason.TabIndex = 16;
            this.lblReason.Text = ":";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(9, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Diễn giải";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(10, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Địa chỉ";
            // 
            // lblAccountingObjectAddress
            // 
            this.lblAccountingObjectAddress.AutoSize = true;
            this.lblAccountingObjectAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(111, 74);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(10, 13);
            this.lblAccountingObjectAddress.TabIndex = 14;
            this.lblAccountingObjectAddress.Text = ":";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridPosted);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(757, 173);
            // 
            // uGridPosted
            // 
            this.uGridPosted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridPosted.Location = new System.Drawing.Point(0, 0);
            this.uGridPosted.Name = "uGridPosted";
            this.uGridPosted.Size = new System.Drawing.Size(757, 173);
            this.uGridPosted.TabIndex = 1;
            this.uGridPosted.Text = "ultraGrid1";
            this.uGridPosted.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uGrid
            // 
            this.uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid.Location = new System.Drawing.Point(0, 0);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(761, 281);
            this.uGrid.TabIndex = 1;
            this.uGrid.Text = "uGrid";
            this.uGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.UGridAfterSelectChange);
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.UGridDoubleClickRow);
            this.uGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UGridMouseDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.cbbAdd);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(761, 49);
            this.panel1.TabIndex = 2;
            // 
            // btnAdd
            // 
            this.btnAdd.AllowDrop = true;
            appearance11.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance11;
            this.btnAdd.Appearances.Add(appearance12);
            this.btnAdd.Appearances.Add(appearance13);
            this.btnAdd.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnAdd.Location = new System.Drawing.Point(13, 9);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.DroppingDown += new System.ComponentModel.CancelEventHandler(this.btnAdd_DroppingDown);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cbbAdd
            // 
            editorButton1.Text = "THÊM";
            this.cbbAdd.ButtonsRight.Add(editorButton1);
            this.cbbAdd.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "ValueListItem0";
            valueListItem1.DisplayText = "Thêm hàng bán trả lại";
            valueListItem1.Tag = "HBTL";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Thêm hàng bán giảm giá";
            valueListItem2.Tag = "HBGG";
            this.cbbAdd.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.cbbAdd.Location = new System.Drawing.Point(286, 12);
            this.cbbAdd.Name = "cbbAdd";
            this.cbbAdd.Size = new System.Drawing.Size(185, 21);
            this.cbbAdd.TabIndex = 9;
            // 
            // btnDelete
            // 
            appearance14.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance14;
            this.btnDelete.Location = new System.Drawing.Point(196, 9);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
            // 
            // btnEdit
            // 
            appearance15.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance15;
            this.btnEdit.Location = new System.Drawing.Point(105, 9);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.BtnEditClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ultraTabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 330);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(761, 199);
            this.panel2.TabIndex = 3;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(761, 199);
            this.ultraTabControl1.TabIndex = 20;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Chi tiết";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(757, 173);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmView,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(150, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAddHBTL,
            this.tsmAddHBGG});
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(149, 22);
            this.tsmAdd.Text = "Thêm";
            // 
            // tsmAddHBTL
            // 
            this.tsmAddHBTL.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAddHBTL.Name = "tsmAddHBTL";
            this.tsmAddHBTL.Size = new System.Drawing.Size(175, 22);
            this.tsmAddHBTL.Text = "Hàng bán trả lại";
            this.tsmAddHBTL.Click += new System.EventHandler(this.TsmAddHbtlClick);
            // 
            // tsmAddHBGG
            // 
            this.tsmAddHBGG.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAddHBGG.Name = "tsmAddHBGG";
            this.tsmAddHBGG.Size = new System.Drawing.Size(175, 22);
            this.tsmAddHBGG.Text = "Hàng bán giảm giá";
            this.tsmAddHBGG.Click += new System.EventHandler(this.TsmAddHbggClick);
            // 
            // tsmView
            // 
            this.tsmView.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmView.Name = "tsmView";
            this.tsmView.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmView.ShowShortcutKeys = false;
            this.tsmView.Size = new System.Drawing.Size(149, 22);
            this.tsmView.Text = "Xem";
            this.tsmView.Click += new System.EventHandler(this.TsmViewClick);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.Size = new System.Drawing.Size(149, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.TsmDeleteClick);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsReLoad.Size = new System.Drawing.Size(149, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.TmsReLoadClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.uGrid);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 49);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(761, 281);
            this.panel3.TabIndex = 2;
            // 
            // cms4Button
            // 
            this.cms4Button.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hbtlcontex,
            this.HmtlToolStripMenuItem});
            this.cms4Button.Name = "cms4Button";
            this.cms4Button.Size = new System.Drawing.Size(176, 70);
            // 
            // hbtlcontex
            // 
            this.hbtlcontex.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.hbtlcontex.Name = "hbtlcontex";
            this.hbtlcontex.Size = new System.Drawing.Size(175, 22);
            this.hbtlcontex.Text = "Hàng bán trả lại";
            this.hbtlcontex.Click += new System.EventHandler(this.hbtlcontex_Click);
            // 
            // HmtlToolStripMenuItem
            // 
            this.HmtlToolStripMenuItem.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.HmtlToolStripMenuItem.Name = "HmtlToolStripMenuItem";
            this.HmtlToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.HmtlToolStripMenuItem.Text = "Hàng bán giảm giá";
            this.HmtlToolStripMenuItem.Click += new System.EventHandler(this.HmtlToolStripMenuItem_Click);
            // 
            // FSAReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 529);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FSAReturn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSAReturnFormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAdd)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.cms4Grid.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.cms4Button.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmView;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.Label lblReason;
        private System.Windows.Forms.Label lblAccountingObjectAddress;
        private System.Windows.Forms.Label lblAccountingObjectName;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblNo;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmAddHBTL;
        private System.Windows.Forms.ToolStripMenuItem tsmAddHBGG;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbAdd;
        private Infragistics.Win.Misc.UltraDropDownButton btnAdd;
        private System.Windows.Forms.ContextMenuStrip cms4Button;
        private System.Windows.Forms.ToolStripMenuItem hbtlcontex;
        private System.Windows.Forms.ToolStripMenuItem HmtlToolStripMenuItem;
    }
}
