﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Frm;
using Accounting.Frm.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting.Frm
{
    public partial class FSelectSaInvoiceItem : Fselect
    {
        #region khai báo

        private readonly ISAInvoiceService _iSaInvoiceService = IoC.Resolve<ISAInvoiceService>();
        private List<SAInvoice> _dSaInvoices = new List<SAInvoice>();
        private BindingList<SAInvoice> lstSaInvoices = new BindingList<SAInvoice>();
        private List<UgridSelectDetail> _dsSelectDetails = new List<UgridSelectDetail>();
        private List<SAInvoiceDetail> _dsSAInvoiceDetail = new List<SAInvoiceDetail>();
        private readonly BindingList<AccountingObject> _dsAcc = Utils.ListAccountingObject;
        private SAInvoice _dsSAInvoice = new SAInvoice();

        #endregion

        public FSelectSaInvoiceItem(Guid? _accoutingObject = null)
        {
            #region Khởi tạo giá trị mặc định của Form

            InitializeComponent();

            #endregion

            if (_accoutingObject != null)
                _dsSAInvoice.AccountingObjectID = _accoutingObject;
            InitializeGUI(_dsSAInvoice);
        }

        public override void InitializeGUI(SAInvoice inputVoucher)
        {
            #region Lấy dữ liệu từ Database và fill vào Obj

            _dSaInvoices = ISAInvoiceService.GetAll();
            IPPInvoiceService.UnbindSession(_dSaInvoices);
            _dSaInvoices = ISAInvoiceService.GetAll();

            _dsSAInvoiceDetail = ISAInvoiceDetailService.GetAll();

            #endregion

            cbbAccountingObjectID.DataSource = _dsAcc;
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            cbbAccountingObjectID.ValueMember = "ID";
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            cbbAccountingObjectID.DataBindings.Add("Value", _dsSAInvoice, "AccountingObjectID", true,
                DataSourceUpdateMode.OnPropertyChanged);

            uGridSaInvoiceDetails.DataSource = new List<PPInvoice>();
            Utils.ConfigGrid(uGridSaInvoiceDetails, "", mauColumns());
            uGridSaInvoiceDetails.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridSaInvoiceDetails.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
            }

            UgridDetail.DataSource = new List<PPInvoiceDetail>();
            //Utils.ConfigGrid(UgridDetail, "",;
            UgridDetail.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in UgridDetail.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
            }

            uGridSaInvoiceReturn.DataSource = new List<PPDiscountReturnDetail>();
            // Utils.ConfigGrid(uGridSaInvoiceReturn, ConstDatabase.PPDiscountReturnDetail_TableName_FPPShoppingVouchers);
            uGridSaInvoiceReturn.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridSaInvoiceReturn.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
            }
        }

        #region Utils

        private void SetDataforuGridInvoiceDetail(UltraGrid ultraGrid)
        {
            // ReSharper disable SuggestUseVarKeywordEvident
            SAInvoice model = (SAInvoice) ultraGrid.ActiveRow.ListObject;

            List<SAInvoiceDetail> temp = model.SAInvoiceDetails.ToList();
            UgridDetail.DataSource = temp.Count > 0 ? temp : new List<SAInvoiceDetail>();
        }

        private void RemoveDataFromGrid(List<SAInvoiceDetail> lstInvoiceDetails)
        {
            //List<SAInvoiceDetail> temp = SplitListObject(lstInvoiceDetails);
            //for (int i = 0; i < lstInvoiceDetails.Count; i++)
            //{
            //    int index = _dSaInvoices.IndexOf(lstInvoiceDetails[i].MaterialGood);
            //    _dSaInvoices[index].SaInvoiceDetails.Add(temp[i]);
            //    _dsSAInvoiceDetail.Remove(lstInvoiceDetails[i]);
            //}
            //uGridSaInvoiceReturn.DataSource = _dsSAInvoiceDetail;
            //uGridSaInvoiceReturn.DataBind();
            //SetDataforuGridInvoiceDetail(uGridSaInvoiceDetails);
        }

        private void SplitListObject(List<SAInvoiceDetail> lstInvoiceDetails)
        {
            //return lstInvoiceDetails.Select(p => new SAInvoiceDetail()
            //{
                //ID = p.ID,
                //RepositoryID = p.RepositoryID,
                //Description = p.Description,
                //DebitAccount = p.DebitAccount,
                //CreditAccount = p.CreditAccount,
                //Unit = p.Unit,
                //Quantity = p.Quantity,
                //QuantityConvert = p.QuantityConvert,
                //UnitPrice = p.UnitPrice == null ? 0 : (decimal)p.UnitPrice,
                //UnitPriceOriginal = p.UnitPriceOriginal == null ? 0 : (decimal)p.UnitPriceOriginal,
                //UnitPriceConvert = p.UnitPriceConvert == null ? 0 : (decimal)p.UnitPriceConvert,
                //UnitPriceConvertOriginal = p.UnitPriceConvertOriginal == null ? 0 : (decimal)p.UnitPriceConvertOriginal,
                //Amount = p.Amount == null ? 0 : (decimal)p.Amount,
                //AmountOriginal = p.AmountOriginal == null ? 0 : (decimal)p.AmountOriginal,
                //VATRate = p.VATRate,
                //VATAmount = p.VATAmount == null ? 0 : (decimal)p.VATAmount,
                //VATAmountOriginal = p.VATAmountOriginal == null ? 0 : (decimal)p.VATAmountOriginal,
                //VATAccount = p.VATAccount,
                //VATDescription = p.VATDescription,
                //AccountingObjectID = p.AccountingObjectID,
                //BudgetItemID = p.BudgetItemID,
                //CostSetID = p.CostSetID,
                //ContractID = p.ContractID,
                //AmountAfterTax = p.AmountAfterTax,
                //DepartmentID = p.DepartmentID,
                //ExpiryDate = p.ExpiryDate,
                //LotNo = p.LotNo,
                //UnitConvert = p.UnitConvert,
                //OWAmount = p.OWAmount,
                //DiscountAccount = p.DiscountAccount,
                //ConvertRate = p.ConvertRate,
                //ExpenseItemID = p.ExpenseItemID,
                //OrderPriority = p.OrderPriority,
                //MaterialGoodsID = p.MaterialGoodsID == null ? Guid.Empty : (Guid)p.MaterialGoodsID,
                //Ma
            //}).ToList();
        }
        //void SetDataforuGridInvoiceDetail(UltraGrid ultraGrid)
        //{
        //     model = (SAInvoice)ultraGrid.ActiveRow.ListObject;
        //    List<SAInvoiceDetail> temp = model..ToList();
        //    if (temp.Count > 0)
        //        uGridInvoiceDetail.DataSource = temp;
        //    else
        //        uGridInvoiceDetail.DataSource = new List<PPInvoiceDetail>();
        //}
        private List<TemplateColumn> mauColumns2()
        {
            return new List<TemplateColumn>()
            {

                new TemplateColumn()
                {
                    ColumnName = "No",
                    ColumnCaption = "Số chứng từ",
                    ColumnWidth = 50,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 0
                },
                new TemplateColumn()
                {
                    ColumnName = "PostDate",
                    ColumnCaption = "Ngày chứng từ",
                    ColumnWidth = 80,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 1
                },
                new TemplateColumn()
                {
                    ColumnName = "SaInvoiceCode",
                    ColumnCaption = "Mã hàng hóa",
                    ColumnWidth = 50,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 2
                },
                new TemplateColumn()
                {
                    ColumnName = "SaInvoiceName",
                    ColumnCaption = "Tên hàng hóa",
                    ColumnWidth = 150,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 3

                },
            };
        }

        private List<TemplateColumn> mauColumns1()
        {
            return new List<TemplateColumn>
            {
                new TemplateColumn()
                {
                    ColumnName = "MaterialGoodsID",
                    ColumnCaption = "Mã hàng hóa",
                    ColumnWidth = 50,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 0
                },
                new TemplateColumn()
                {
                    ColumnName = "SaInvoiceName",
                    ColumnCaption = "Tên hàng hóa",
                    ColumnWidth = 150,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 1
                },
                new TemplateColumn()
                {
                    ColumnName = "quantumNo",
                    ColumnCaption = "Số lượng bán",
                    ColumnWidth = 150,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 2
                },
            };
        }

        private List<TemplateColumn> mauColumns()
        {
            return new List<TemplateColumn>
            {
                new TemplateColumn()
                {
                    ColumnName = "No",
                    ColumnCaption = "Số chứng từ",
                    ColumnWidth = 50,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 0
                },
                new TemplateColumn()
                {
                    ColumnName = "Date",
                    ColumnCaption = "Ngày chứng từ",
                    ColumnWidth = 80,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 1
                },
                new TemplateColumn()
                {
                    ColumnName = "InvoiceNo",
                    ColumnCaption = "Số hóa đơn",
                    ColumnWidth = 80,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 2
                },
                new TemplateColumn()
                {
                    ColumnName = "InvoiceDate",
                    ColumnCaption = "Ngày hóa đơn",
                    ColumnWidth = 80,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 3
                },
                new TemplateColumn()
                {
                    ColumnName = "Reason",
                    ColumnCaption = "Diễn giải",
                    ColumnWidth = 150,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 4
                },
                new TemplateColumn()
                {
                    ColumnName = "TotalAmountOriginal",
                    ColumnCaption = "Số tiền",
                    ColumnWidth = 70,
                    IsVisible = true,
                    IsVisibleCbb = false,
                    VisiblePosition = 5
                },
            };
        }

        /// <summary>
        /// Lấy danh sách hóa đơn bán hàng của một khách hàng
        /// </summary>
        /// <param name="accountingObject"></param>
        /// <returns></returns>
        private List<SAInvoice> GetSaInvoices(AccountingObject accountingObject)
        {
            List<SAInvoice> dSaInvoices = _dSaInvoices.Where(k => k.AccountingObjectID == accountingObject.ID).ToList();
            return dSaInvoices;
        }


        #endregion


        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void cbbAccountingObjectID_RowSelected(object sender,
            Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            //var acc = e.Row.ListObject as AccountingObject;
            //AccountingObject accountingObject= new AccountingObject();
            //if (e.Row.Selected != true || e.Row == null || e.Row.ListObject == null)
            //{
            //    return;
            //}
            //else
            //{
            //    uGridSaInvoiceDetails.DataSource = GetSaInvoices(acc);
            //    //Utils.ConfigGrid(uGridSaInvoiceDetails, "templateUgridSaInvoice", mauColumns());
            //    if (uGridSaInvoiceDetails.Rows[0].Selected)
            //    {
            //        UgridDetail.DataSource = GetSaInvoices(acc).SelectMany(c =>c.SaInvoiceDetails).ToList(); 
            //    }
            if (e.Row.Selected)
            {
                AccountingObject model = (AccountingObject) e.Row.ListObject;
                List<SAInvoice> temp =
                    _dSaInvoices.Where(p => p.AccountingObjectID == model.ID).ToList();
                if (temp.Count > 0)
                {
                    uGridSaInvoiceDetails.DataSource = temp;
                    uGridSaInvoiceDetails.ActiveRow = uGridSaInvoiceDetails.Rows[0];
                }
                else
                {
                    uGridSaInvoiceDetails.DataSource = new List<SAInvoice>();
                    UgridDetail.DataSource = new List<SAInvoiceDetail>();
                }
            }

        }

        //var saInvoice = (e.Row.ListObject as SAInvoice);

        //foreach (var item in saInvoice)
        //{
        //    uGridSaInvoiceDetails.DataSource = item;
        //}

    }


    public class Fselect : DetailBase<SAInvoice> { }
}
