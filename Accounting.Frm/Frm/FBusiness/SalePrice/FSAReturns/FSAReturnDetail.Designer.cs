﻿namespace Accounting
{
    partial class FSAReturnDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageHoaDon = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.grHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBoxHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOrginal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnVouchersDiscount = new Infragistics.Win.Misc.UltraButton();
            this.txtContractName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonReturn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPagePhieuXuatKho = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.grHoaDonPN = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHDPN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHDPN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHDPN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHDPN = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBoxPhieuXuatKho = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCompanyTaxCodeXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.lblChungTuGoc = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectXK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChi = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHang = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.GBHDTL = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOrginalHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtContractNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonReturnHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBoxStand = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnVouchersReturn = new Infragistics.Win.Misc.UltraButton();
            this.txtCompanyTaxCodePXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCodePXK = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNoPXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttachPXK = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNamePXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDPXK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDienGiai = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblDiaChiPXK = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHangPXK = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtMauSoHDI = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtSoHDI = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHDI = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.dteNgayHDI = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.GBHDGG = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboThanhToan = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNoPXKHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtCompanyTaxCodePXKHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNamePXKHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDPXKHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTextEditor1HD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPXKHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palTop = new System.Windows.Forms.Panel();
            this.ultraGroupBoxTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.optPhieuXuatKho = new Accounting.UltraOptionSet_Ex();
            this.optHoaDon = new Accounting.UltraOptionSet_Ex();
            this.palTopSctNhtNctHoaDon = new Infragistics.Win.Misc.UltraPanel();
            this.palTopSctNhtNctPXK = new Infragistics.Win.Misc.UltraPanel();
            this.lblCachNhapDG = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCachNhapDonGia = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.palBottom = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtOldInvSeries = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvTemplate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTttt = new Infragistics.Win.Misc.UltraLabel();
            this.btnSelectBill = new Infragistics.Win.Misc.UltraButton();
            this.cbBill = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.txtTotalDiscountAmountSubOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalDiscountAmountSub = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountOriginalStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraTabSharedControlsPage4 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.paneHoaDon = new Infragistics.Win.Misc.UltraPanel();
            this.uGroupBoxEx = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage3 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabControlGG = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabPageHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).BeginInit();
            this.grHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).BeginInit();
            this.ultraGroupBoxHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            this.ultraTabPagePhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDonPN)).BeginInit();
            this.grHoaDonPN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).BeginInit();
            this.ultraGroupBoxPhieuXuatKho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressXK)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GBHDTL)).BeginInit();
            this.GBHDTL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrginalHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturnHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStand)).BeginInit();
            this.uGroupBoxStand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXK)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GBHDGG)).BeginInit();
            this.GBHDGG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboThanhToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXKHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXKHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXKHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPXKHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1HD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXKHD)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).BeginInit();
            this.ultraGroupBoxTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optPhieuXuatKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHoaDon)).BeginInit();
            this.palTopSctNhtNctHoaDon.SuspendLayout();
            this.palTopSctNhtNctPXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCachNhapDonGia)).BeginInit();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.paneHoaDon.ClientArea.SuspendLayout();
            this.paneHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEx)).BeginInit();
            this.uGroupBoxEx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlGG)).BeginInit();
            this.ultraTabControlGG.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageHoaDon
            // 
            this.ultraTabPageHoaDon.Controls.Add(this.grHoaDon);
            this.ultraTabPageHoaDon.Controls.Add(this.ultraGroupBoxHoaDon);
            this.ultraTabPageHoaDon.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageHoaDon.Name = "ultraTabPageHoaDon";
            this.ultraTabPageHoaDon.Size = new System.Drawing.Size(944, 150);
            // 
            // grHoaDon
            // 
            this.grHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDon.Controls.Add(this.txtMauSoHD);
            this.grHoaDon.Controls.Add(this.txtSoHD);
            this.grHoaDon.Controls.Add(this.txtKyHieuHD);
            this.grHoaDon.Controls.Add(this.ultraLabel37);
            this.grHoaDon.Controls.Add(this.ultraLabel38);
            this.grHoaDon.Controls.Add(this.ultraLabel39);
            this.grHoaDon.Controls.Add(this.ultraLabel40);
            this.grHoaDon.Controls.Add(this.dteNgayHD);
            this.grHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDon.Location = new System.Drawing.Point(654, 0);
            this.grHoaDon.Name = "grHoaDon";
            this.grHoaDon.Size = new System.Drawing.Size(290, 150);
            this.grHoaDon.TabIndex = 32;
            this.grHoaDon.Text = "Hóa đơn";
            this.grHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHD
            // 
            this.txtMauSoHD.AutoSize = false;
            this.txtMauSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHD.Location = new System.Drawing.Point(81, 21);
            this.txtMauSoHD.Name = "txtMauSoHD";
            this.txtMauSoHD.Size = new System.Drawing.Size(203, 22);
            this.txtMauSoHD.TabIndex = 76;
            this.txtMauSoHD.TextChanged += new System.EventHandler(this.txtMauSoHD_TextChanged);
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(81, 72);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(203, 22);
            this.txtSoHD.TabIndex = 18;
            this.txtSoHD.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(81, 46);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(203, 22);
            this.txtKyHieuHD.TabIndex = 17;
            this.txtKyHieuHD.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChanged);
            // 
            // ultraLabel37
            // 
            this.ultraLabel37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel37.Appearance = appearance1;
            this.ultraLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel37.Location = new System.Drawing.Point(11, 99);
            this.ultraLabel37.Name = "ultraLabel37";
            this.ultraLabel37.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel37.TabIndex = 69;
            this.ultraLabel37.Text = "Ngày HĐ";
            // 
            // ultraLabel38
            // 
            this.ultraLabel38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel38.Appearance = appearance2;
            this.ultraLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel38.Location = new System.Drawing.Point(9, 73);
            this.ultraLabel38.Name = "ultraLabel38";
            this.ultraLabel38.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel38.TabIndex = 68;
            this.ultraLabel38.Text = "Số HĐ";
            // 
            // ultraLabel39
            // 
            this.ultraLabel39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel39.Appearance = appearance3;
            this.ultraLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel39.Location = new System.Drawing.Point(9, 48);
            this.ultraLabel39.Name = "ultraLabel39";
            this.ultraLabel39.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel39.TabIndex = 67;
            this.ultraLabel39.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance4;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(11, 24);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 66;
            this.ultraLabel40.Text = "Mẫu số HĐ";
            // 
            // dteNgayHD
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance5;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(81, 99);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            this.dteNgayHD.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            // 
            // ultraGroupBoxHoaDon
            // 
            this.ultraGroupBoxHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel2);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtOrginal);
            this.ultraGroupBoxHoaDon.Controls.Add(this.btnVouchersDiscount);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtContractName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel1);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel3);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBoxHoaDon.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtReasonReturn);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel4);
            this.ultraGroupBoxHoaDon.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel6);
            this.ultraGroupBoxHoaDon.Controls.Add(this.ultraLabel7);
            appearance14.FontData.BoldAsString = "True";
            appearance14.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxHoaDon.HeaderAppearance = appearance14;
            this.ultraGroupBoxHoaDon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDon.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxHoaDon.Name = "ultraGroupBoxHoaDon";
            this.ultraGroupBoxHoaDon.Size = new System.Drawing.Size(654, 150);
            this.ultraGroupBoxHoaDon.TabIndex = 26;
            this.ultraGroupBoxHoaDon.Text = "Thông tin chung";
            this.ultraGroupBoxHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance6;
            this.ultraLabel2.Location = new System.Drawing.Point(571, 119);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(74, 22);
            this.ultraLabel2.TabIndex = 76;
            this.ultraLabel2.Text = "Chứng từ gốc";
            // 
            // txtOrginal
            // 
            this.txtOrginal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOrginal.AutoSize = false;
            this.txtOrginal.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtOrginal.Location = new System.Drawing.Point(92, 119);
            this.txtOrginal.Name = "txtOrginal";
            this.txtOrginal.Size = new System.Drawing.Size(473, 22);
            this.txtOrginal.TabIndex = 75;
            // 
            // btnVouchersDiscount
            // 
            this.btnVouchersDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVouchersDiscount.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnVouchersDiscount.Location = new System.Drawing.Point(485, 22);
            this.btnVouchersDiscount.Name = "btnVouchersDiscount";
            this.btnVouchersDiscount.Size = new System.Drawing.Size(160, 22);
            this.btnVouchersDiscount.TabIndex = 74;
            this.btnVouchersDiscount.Text = "Chọn chứng từ bán hàng";
            this.btnVouchersDiscount.Click += new System.EventHandler(this.btnVouchersReturn_Click);
            // 
            // txtContractName
            // 
            this.txtContractName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextVAlignAsString = "Middle";
            this.txtContractName.Appearance = appearance7;
            this.txtContractName.AutoSize = false;
            this.txtContractName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtContractName.Location = new System.Drawing.Point(92, 71);
            this.txtContractName.Name = "txtContractName";
            this.txtContractName.Size = new System.Drawing.Size(553, 22);
            this.txtContractName.TabIndex = 68;
            // 
            // ultraLabel1
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance8;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 71);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel1.TabIndex = 67;
            this.ultraLabel1.Text = "Người giao";
            // 
            // ultraLabel3
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance9;
            this.ultraLabel3.Location = new System.Drawing.Point(6, 119);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel3.TabIndex = 64;
            this.ultraLabel3.Text = "Kèm theo";
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(256, 23);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(221, 22);
            this.txtAccountingObjectName.TabIndex = 63;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            this.cbbAccountingObjectID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(92, 23);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectID.TabIndex = 62;
            // 
            // txtReasonReturn
            // 
            this.txtReasonReturn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextVAlignAsString = "Middle";
            this.txtReasonReturn.Appearance = appearance10;
            this.txtReasonReturn.AutoSize = false;
            this.txtReasonReturn.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonReturn.Location = new System.Drawing.Point(92, 95);
            this.txtReasonReturn.Name = "txtReasonReturn";
            this.txtReasonReturn.Size = new System.Drawing.Size(553, 22);
            this.txtReasonReturn.TabIndex = 61;
            // 
            // ultraLabel4
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance11;
            this.ultraLabel4.Location = new System.Drawing.Point(6, 95);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel4.TabIndex = 60;
            this.ultraLabel4.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(92, 47);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(553, 22);
            this.txtAccountingObjectAddress.TabIndex = 57;
            // 
            // ultraLabel6
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance12;
            this.ultraLabel6.Location = new System.Drawing.Point(6, 47);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel6.TabIndex = 56;
            this.ultraLabel6.Text = "Địa chỉ";
            // 
            // ultraLabel7
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance13;
            this.ultraLabel7.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(80, 21);
            this.ultraLabel7.TabIndex = 55;
            this.ultraLabel7.Text = "Đối tượng";
            // 
            // ultraTabPagePhieuXuatKho
            // 
            this.ultraTabPagePhieuXuatKho.Controls.Add(this.grHoaDonPN);
            this.ultraTabPagePhieuXuatKho.Controls.Add(this.ultraGroupBoxPhieuXuatKho);
            this.ultraTabPagePhieuXuatKho.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPagePhieuXuatKho.Name = "ultraTabPagePhieuXuatKho";
            this.ultraTabPagePhieuXuatKho.Size = new System.Drawing.Size(944, 150);
            // 
            // grHoaDonPN
            // 
            this.grHoaDonPN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grHoaDonPN.Controls.Add(this.txtMauSoHDPN);
            this.grHoaDonPN.Controls.Add(this.txtSoHDPN);
            this.grHoaDonPN.Controls.Add(this.txtKyHieuHDPN);
            this.grHoaDonPN.Controls.Add(this.ultraLabel22);
            this.grHoaDonPN.Controls.Add(this.ultraLabel23);
            this.grHoaDonPN.Controls.Add(this.ultraLabel24);
            this.grHoaDonPN.Controls.Add(this.ultraLabel25);
            this.grHoaDonPN.Controls.Add(this.dteNgayHDPN);
            this.grHoaDonPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grHoaDonPN.Location = new System.Drawing.Point(654, 0);
            this.grHoaDonPN.Name = "grHoaDonPN";
            this.grHoaDonPN.Size = new System.Drawing.Size(290, 149);
            this.grHoaDonPN.TabIndex = 32;
            this.grHoaDonPN.Text = "Hóa đơn";
            this.grHoaDonPN.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHDPN
            // 
            this.txtMauSoHDPN.AutoSize = false;
            this.txtMauSoHDPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHDPN.Location = new System.Drawing.Point(81, 21);
            this.txtMauSoHDPN.Name = "txtMauSoHDPN";
            this.txtMauSoHDPN.Size = new System.Drawing.Size(203, 22);
            this.txtMauSoHDPN.TabIndex = 76;
            this.txtMauSoHDPN.TextChanged += new System.EventHandler(this.txtMauSoHD_TextChanged);
            // 
            // txtSoHDPN
            // 
            this.txtSoHDPN.AutoSize = false;
            this.txtSoHDPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHDPN.Location = new System.Drawing.Point(81, 72);
            this.txtSoHDPN.Name = "txtSoHDPN";
            this.txtSoHDPN.Size = new System.Drawing.Size(203, 22);
            this.txtSoHDPN.TabIndex = 18;
            this.txtSoHDPN.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHDPN
            // 
            this.txtKyHieuHDPN.AutoSize = false;
            this.txtKyHieuHDPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHDPN.Location = new System.Drawing.Point(81, 46);
            this.txtKyHieuHDPN.Name = "txtKyHieuHDPN";
            this.txtKyHieuHDPN.Size = new System.Drawing.Size(203, 22);
            this.txtKyHieuHDPN.TabIndex = 17;
            this.txtKyHieuHDPN.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChanged);
            // 
            // ultraLabel22
            // 
            this.ultraLabel22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel22.Appearance = appearance15;
            this.ultraLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel22.Location = new System.Drawing.Point(11, 99);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel22.TabIndex = 69;
            this.ultraLabel22.Text = "Ngày HĐ";
            // 
            // ultraLabel23
            // 
            this.ultraLabel23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.Color.Transparent;
            appearance16.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance16;
            this.ultraLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel23.Location = new System.Drawing.Point(9, 73);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel23.TabIndex = 68;
            this.ultraLabel23.Text = "Số HĐ";
            // 
            // ultraLabel24
            // 
            this.ultraLabel24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel24.Appearance = appearance17;
            this.ultraLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel24.Location = new System.Drawing.Point(9, 48);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel24.TabIndex = 67;
            this.ultraLabel24.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel25
            // 
            this.ultraLabel25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel25.Appearance = appearance18;
            this.ultraLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel25.Location = new System.Drawing.Point(11, 24);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel25.TabIndex = 66;
            this.ultraLabel25.Text = "Mẫu số HĐ";
            // 
            // dteNgayHDPN
            // 
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            this.dteNgayHDPN.Appearance = appearance19;
            this.dteNgayHDPN.AutoSize = false;
            this.dteNgayHDPN.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHDPN.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHDPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHDPN.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHDPN.Location = new System.Drawing.Point(81, 99);
            this.dteNgayHDPN.MaskInput = "";
            this.dteNgayHDPN.Name = "dteNgayHDPN";
            this.dteNgayHDPN.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHDPN.TabIndex = 19;
            this.dteNgayHDPN.Value = null;
            this.dteNgayHDPN.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            // 
            // ultraGroupBoxPhieuXuatKho
            // 
            this.ultraGroupBoxPhieuXuatKho.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.TextVAlignAsString = "Middle";
            this.ultraGroupBoxPhieuXuatKho.Appearance = appearance20;
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtCompanyTaxCodeXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblCompanyTaxCode);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblChungTuGoc);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtOriginalNo);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblNumberAttach);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectNameXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.cbbAccountingObjectXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtSReason);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblSReason);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.txtAccountingObjectAddressXK);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblDiaChi);
            this.ultraGroupBoxPhieuXuatKho.Controls.Add(this.lblKhachHang);
            appearance29.FontData.BoldAsString = "True";
            appearance29.FontData.SizeInPoints = 10F;
            this.ultraGroupBoxPhieuXuatKho.HeaderAppearance = appearance29;
            this.ultraGroupBoxPhieuXuatKho.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuXuatKho.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuXuatKho.Name = "ultraGroupBoxPhieuXuatKho";
            this.ultraGroupBoxPhieuXuatKho.Size = new System.Drawing.Size(654, 150);
            this.ultraGroupBoxPhieuXuatKho.TabIndex = 27;
            this.ultraGroupBoxPhieuXuatKho.Text = "Thông tin chung";
            this.ultraGroupBoxPhieuXuatKho.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCompanyTaxCodeXK
            // 
            this.txtCompanyTaxCodeXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCodeXK.Appearance = appearance21;
            this.txtCompanyTaxCodeXK.AutoSize = false;
            this.txtCompanyTaxCodeXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCodeXK.Location = new System.Drawing.Point(92, 71);
            this.txtCompanyTaxCodeXK.Name = "txtCompanyTaxCodeXK";
            this.txtCompanyTaxCodeXK.Size = new System.Drawing.Size(553, 22);
            this.txtCompanyTaxCodeXK.TabIndex = 68;
            // 
            // lblCompanyTaxCode
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCode.Appearance = appearance22;
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(6, 71);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(80, 22);
            this.lblCompanyTaxCode.TabIndex = 67;
            this.lblCompanyTaxCode.Text = "Người giao";
            // 
            // lblChungTuGoc
            // 
            this.lblChungTuGoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextHAlignAsString = "Center";
            appearance23.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.Appearance = appearance23;
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            this.lblChungTuGoc.HotTrackAppearance = appearance24;
            this.lblChungTuGoc.Location = new System.Drawing.Point(558, 120);
            this.lblChungTuGoc.Name = "lblChungTuGoc";
            this.lblChungTuGoc.Size = new System.Drawing.Size(87, 22);
            this.lblChungTuGoc.TabIndex = 66;
            this.lblChungTuGoc.Text = "Chứng từ gốc";
            // 
            // txtOriginalNo
            // 
            this.txtOriginalNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginalNo.AutoSize = false;
            this.txtOriginalNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtOriginalNo.Location = new System.Drawing.Point(92, 119);
            this.txtOriginalNo.Name = "txtOriginalNo";
            this.txtOriginalNo.Size = new System.Drawing.Size(467, 22);
            this.txtOriginalNo.TabIndex = 65;
            // 
            // lblNumberAttach
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextVAlignAsString = "Middle";
            this.lblNumberAttach.Appearance = appearance25;
            this.lblNumberAttach.Location = new System.Drawing.Point(6, 119);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(80, 21);
            this.lblNumberAttach.TabIndex = 64;
            this.lblNumberAttach.Text = "Kèm theo";
            // 
            // txtAccountingObjectNameXK
            // 
            this.txtAccountingObjectNameXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameXK.AutoSize = false;
            this.txtAccountingObjectNameXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNameXK.Location = new System.Drawing.Point(256, 23);
            this.txtAccountingObjectNameXK.Name = "txtAccountingObjectNameXK";
            this.txtAccountingObjectNameXK.Size = new System.Drawing.Size(389, 22);
            this.txtAccountingObjectNameXK.TabIndex = 63;
            // 
            // cbbAccountingObjectXK
            // 
            this.cbbAccountingObjectXK.AutoSize = false;
            this.cbbAccountingObjectXK.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectXK.Location = new System.Drawing.Point(92, 23);
            this.cbbAccountingObjectXK.Name = "cbbAccountingObjectXK";
            this.cbbAccountingObjectXK.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectXK.TabIndex = 62;
            // 
            // txtSReason
            // 
            this.txtSReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSReason.AutoSize = false;
            this.txtSReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtSReason.Location = new System.Drawing.Point(92, 95);
            this.txtSReason.Name = "txtSReason";
            this.txtSReason.Size = new System.Drawing.Size(553, 22);
            this.txtSReason.TabIndex = 61;
            // 
            // lblSReason
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.lblSReason.Appearance = appearance26;
            this.lblSReason.Location = new System.Drawing.Point(6, 95);
            this.lblSReason.Name = "lblSReason";
            this.lblSReason.Size = new System.Drawing.Size(80, 22);
            this.lblSReason.TabIndex = 60;
            this.lblSReason.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddressXK
            // 
            this.txtAccountingObjectAddressXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressXK.AutoSize = false;
            this.txtAccountingObjectAddressXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressXK.Location = new System.Drawing.Point(92, 47);
            this.txtAccountingObjectAddressXK.Name = "txtAccountingObjectAddressXK";
            this.txtAccountingObjectAddressXK.Size = new System.Drawing.Size(553, 22);
            this.txtAccountingObjectAddressXK.TabIndex = 57;
            // 
            // lblDiaChi
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextVAlignAsString = "Middle";
            this.lblDiaChi.Appearance = appearance27;
            this.lblDiaChi.Location = new System.Drawing.Point(6, 47);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(80, 22);
            this.lblDiaChi.TabIndex = 56;
            this.lblDiaChi.Text = "Địa chỉ";
            // 
            // lblKhachHang
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextVAlignAsString = "Middle";
            this.lblKhachHang.Appearance = appearance28;
            this.lblKhachHang.Location = new System.Drawing.Point(6, 23);
            this.lblKhachHang.Name = "lblKhachHang";
            this.lblKhachHang.Size = new System.Drawing.Size(80, 22);
            this.lblKhachHang.TabIndex = 55;
            this.lblKhachHang.Text = "Đối tượng";
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.GBHDTL);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(944, 150);
            // 
            // GBHDTL
            // 
            this.GBHDTL.Controls.Add(this.ultraLabel8);
            this.GBHDTL.Controls.Add(this.txtOrginalHD);
            this.GBHDTL.Controls.Add(this.txtContractNameHD);
            this.GBHDTL.Controls.Add(this.ultraLabel10);
            this.GBHDTL.Controls.Add(this.ultraLabel12);
            this.GBHDTL.Controls.Add(this.txtAccountingObjectNameHD);
            this.GBHDTL.Controls.Add(this.cbbAccountingObjectIDHD);
            this.GBHDTL.Controls.Add(this.txtReasonReturnHD);
            this.GBHDTL.Controls.Add(this.ultraLabel13);
            this.GBHDTL.Controls.Add(this.txtAccountingObjectAddressHD);
            this.GBHDTL.Controls.Add(this.ultraLabel14);
            this.GBHDTL.Controls.Add(this.ultraLabel15);
            this.GBHDTL.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance38.FontData.BoldAsString = "True";
            appearance38.FontData.SizeInPoints = 10F;
            this.GBHDTL.HeaderAppearance = appearance38;
            this.GBHDTL.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.GBHDTL.Location = new System.Drawing.Point(0, 0);
            this.GBHDTL.Name = "GBHDTL";
            this.GBHDTL.Size = new System.Drawing.Size(944, 150);
            this.GBHDTL.TabIndex = 27;
            this.GBHDTL.Text = "Thông tin chung";
            this.GBHDTL.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextHAlignAsString = "Center";
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance30;
            this.ultraLabel8.Location = new System.Drawing.Point(820, 119);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel8.TabIndex = 76;
            this.ultraLabel8.Text = "Chứng từ gốc";
            // 
            // txtOrginalHD
            // 
            this.txtOrginalHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOrginalHD.AutoSize = false;
            this.txtOrginalHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtOrginalHD.Location = new System.Drawing.Point(92, 119);
            this.txtOrginalHD.Name = "txtOrginalHD";
            this.txtOrginalHD.Size = new System.Drawing.Size(722, 22);
            this.txtOrginalHD.TabIndex = 75;
            // 
            // txtContractNameHD
            // 
            this.txtContractNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance31.TextVAlignAsString = "Middle";
            this.txtContractNameHD.Appearance = appearance31;
            this.txtContractNameHD.AutoSize = false;
            this.txtContractNameHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtContractNameHD.Location = new System.Drawing.Point(92, 71);
            this.txtContractNameHD.Name = "txtContractNameHD";
            this.txtContractNameHD.Size = new System.Drawing.Size(843, 22);
            this.txtContractNameHD.TabIndex = 68;
            // 
            // ultraLabel10
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance32;
            this.ultraLabel10.Location = new System.Drawing.Point(6, 71);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel10.TabIndex = 67;
            this.ultraLabel10.Text = "Người giao";
            // 
            // ultraLabel12
            // 
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance33;
            this.ultraLabel12.Location = new System.Drawing.Point(6, 119);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel12.TabIndex = 64;
            this.ultraLabel12.Text = "Kèm theo";
            // 
            // txtAccountingObjectNameHD
            // 
            this.txtAccountingObjectNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameHD.AutoSize = false;
            this.txtAccountingObjectNameHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNameHD.Location = new System.Drawing.Point(256, 23);
            this.txtAccountingObjectNameHD.Name = "txtAccountingObjectNameHD";
            this.txtAccountingObjectNameHD.Size = new System.Drawing.Size(679, 22);
            this.txtAccountingObjectNameHD.TabIndex = 63;
            // 
            // cbbAccountingObjectIDHD
            // 
            this.cbbAccountingObjectIDHD.AutoSize = false;
            this.cbbAccountingObjectIDHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectIDHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectIDHD.Location = new System.Drawing.Point(92, 23);
            this.cbbAccountingObjectIDHD.Name = "cbbAccountingObjectIDHD";
            this.cbbAccountingObjectIDHD.Size = new System.Drawing.Size(158, 22);
            this.cbbAccountingObjectIDHD.TabIndex = 62;
            // 
            // txtReasonReturnHD
            // 
            this.txtReasonReturnHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance34.TextVAlignAsString = "Middle";
            this.txtReasonReturnHD.Appearance = appearance34;
            this.txtReasonReturnHD.AutoSize = false;
            this.txtReasonReturnHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtReasonReturnHD.Location = new System.Drawing.Point(92, 95);
            this.txtReasonReturnHD.Name = "txtReasonReturnHD";
            this.txtReasonReturnHD.Size = new System.Drawing.Size(843, 22);
            this.txtReasonReturnHD.TabIndex = 61;
            // 
            // ultraLabel13
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance35;
            this.ultraLabel13.Location = new System.Drawing.Point(6, 95);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel13.TabIndex = 60;
            this.ultraLabel13.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddressHD
            // 
            this.txtAccountingObjectAddressHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressHD.AutoSize = false;
            this.txtAccountingObjectAddressHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressHD.Location = new System.Drawing.Point(92, 47);
            this.txtAccountingObjectAddressHD.Name = "txtAccountingObjectAddressHD";
            this.txtAccountingObjectAddressHD.Size = new System.Drawing.Size(843, 22);
            this.txtAccountingObjectAddressHD.TabIndex = 57;
            // 
            // ultraLabel14
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance36;
            this.ultraLabel14.Location = new System.Drawing.Point(6, 47);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel14.TabIndex = 56;
            this.ultraLabel14.Text = "Địa chỉ";
            // 
            // ultraLabel15
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance37;
            this.ultraLabel15.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(80, 21);
            this.ultraLabel15.TabIndex = 55;
            this.ultraLabel15.Text = "Đối tượng";
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(944, 150);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGroupBoxStand);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(944, 157);
            // 
            // uGroupBoxStand
            // 
            this.uGroupBoxStand.Controls.Add(this.btnVouchersReturn);
            this.uGroupBoxStand.Controls.Add(this.txtCompanyTaxCodePXK);
            this.uGroupBoxStand.Controls.Add(this.lblCompanyTaxCodePXK);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel9);
            this.uGroupBoxStand.Controls.Add(this.txtOriginalNoPXK);
            this.uGroupBoxStand.Controls.Add(this.lblNumberAttachPXK);
            this.uGroupBoxStand.Controls.Add(this.txtAccountingObjectNamePXK);
            this.uGroupBoxStand.Controls.Add(this.cbbAccountingObjectIDPXK);
            this.uGroupBoxStand.Controls.Add(this.txtDienGiai);
            this.uGroupBoxStand.Controls.Add(this.ultraLabel11);
            this.uGroupBoxStand.Controls.Add(this.txtAccountingObjectAddressPXK);
            this.uGroupBoxStand.Controls.Add(this.lblDiaChiPXK);
            this.uGroupBoxStand.Controls.Add(this.lblKhachHangPXK);
            this.uGroupBoxStand.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance48.FontData.BoldAsString = "True";
            appearance48.FontData.SizeInPoints = 10F;
            this.uGroupBoxStand.HeaderAppearance = appearance48;
            this.uGroupBoxStand.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.uGroupBoxStand.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxStand.Name = "uGroupBoxStand";
            this.uGroupBoxStand.Size = new System.Drawing.Size(944, 157);
            this.uGroupBoxStand.TabIndex = 29;
            this.uGroupBoxStand.Text = "Thông tin chung";
            this.uGroupBoxStand.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnVouchersReturn
            // 
            this.btnVouchersReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVouchersReturn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.btnVouchersReturn.Location = new System.Drawing.Point(746, 27);
            this.btnVouchersReturn.Name = "btnVouchersReturn";
            this.btnVouchersReturn.Size = new System.Drawing.Size(187, 22);
            this.btnVouchersReturn.TabIndex = 75;
            this.btnVouchersReturn.Text = "Chọn chứng từ bán hàng";
            this.btnVouchersReturn.Click += new System.EventHandler(this.btnVouchersReturn_Click);
            // 
            // txtCompanyTaxCodePXK
            // 
            this.txtCompanyTaxCodePXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCodePXK.Appearance = appearance39;
            this.txtCompanyTaxCodePXK.AutoSize = false;
            this.txtCompanyTaxCodePXK.Location = new System.Drawing.Point(92, 75);
            this.txtCompanyTaxCodePXK.Name = "txtCompanyTaxCodePXK";
            this.txtCompanyTaxCodePXK.Size = new System.Drawing.Size(841, 22);
            this.txtCompanyTaxCodePXK.TabIndex = 39;
            // 
            // lblCompanyTaxCodePXK
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextVAlignAsString = "Middle";
            this.lblCompanyTaxCodePXK.Appearance = appearance40;
            this.lblCompanyTaxCodePXK.Location = new System.Drawing.Point(6, 73);
            this.lblCompanyTaxCodePXK.Name = "lblCompanyTaxCodePXK";
            this.lblCompanyTaxCodePXK.Size = new System.Drawing.Size(80, 22);
            this.lblCompanyTaxCodePXK.TabIndex = 38;
            this.lblCompanyTaxCodePXK.Text = "Người giao";
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Center";
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance41;
            this.ultraLabel9.Location = new System.Drawing.Point(819, 122);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(114, 22);
            this.ultraLabel9.TabIndex = 37;
            this.ultraLabel9.Text = "Chứng từ gốc";
            // 
            // txtOriginalNoPXK
            // 
            this.txtOriginalNoPXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.TextVAlignAsString = "Middle";
            this.txtOriginalNoPXK.Appearance = appearance42;
            this.txtOriginalNoPXK.AutoSize = false;
            this.txtOriginalNoPXK.Location = new System.Drawing.Point(92, 123);
            this.txtOriginalNoPXK.Name = "txtOriginalNoPXK";
            this.txtOriginalNoPXK.Size = new System.Drawing.Size(721, 22);
            this.txtOriginalNoPXK.TabIndex = 36;
            // 
            // lblNumberAttachPXK
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextVAlignAsString = "Middle";
            this.lblNumberAttachPXK.Appearance = appearance43;
            this.lblNumberAttachPXK.Location = new System.Drawing.Point(6, 121);
            this.lblNumberAttachPXK.Name = "lblNumberAttachPXK";
            this.lblNumberAttachPXK.Size = new System.Drawing.Size(80, 21);
            this.lblNumberAttachPXK.TabIndex = 35;
            this.lblNumberAttachPXK.Text = "Kèm theo";
            // 
            // txtAccountingObjectNamePXK
            // 
            this.txtAccountingObjectNamePXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePXK.AutoSize = false;
            this.txtAccountingObjectNamePXK.Location = new System.Drawing.Point(255, 27);
            this.txtAccountingObjectNamePXK.Name = "txtAccountingObjectNamePXK";
            this.txtAccountingObjectNamePXK.Size = new System.Drawing.Size(485, 22);
            this.txtAccountingObjectNamePXK.TabIndex = 32;
            this.txtAccountingObjectNamePXK.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbAccountingObjectIDPXK
            // 
            this.cbbAccountingObjectIDPXK.AutoSize = false;
            appearance44.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance44;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDPXK.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectIDPXK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDPXK.Location = new System.Drawing.Point(92, 27);
            this.cbbAccountingObjectIDPXK.Name = "cbbAccountingObjectIDPXK";
            this.cbbAccountingObjectIDPXK.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDPXK.Size = new System.Drawing.Size(157, 22);
            this.cbbAccountingObjectIDPXK.TabIndex = 31;
            // 
            // txtDienGiai
            // 
            this.txtDienGiai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDienGiai.AutoSize = false;
            this.txtDienGiai.Location = new System.Drawing.Point(92, 99);
            this.txtDienGiai.Name = "txtDienGiai";
            this.txtDienGiai.Size = new System.Drawing.Size(841, 22);
            this.txtDienGiai.TabIndex = 27;
            // 
            // ultraLabel11
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance45;
            this.ultraLabel11.Location = new System.Drawing.Point(6, 97);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel11.TabIndex = 26;
            this.ultraLabel11.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddressPXK
            // 
            this.txtAccountingObjectAddressPXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPXK.AutoSize = false;
            this.txtAccountingObjectAddressPXK.Location = new System.Drawing.Point(92, 51);
            this.txtAccountingObjectAddressPXK.Name = "txtAccountingObjectAddressPXK";
            this.txtAccountingObjectAddressPXK.Size = new System.Drawing.Size(841, 22);
            this.txtAccountingObjectAddressPXK.TabIndex = 23;
            this.txtAccountingObjectAddressPXK.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // lblDiaChiPXK
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            appearance46.TextVAlignAsString = "Middle";
            this.lblDiaChiPXK.Appearance = appearance46;
            this.lblDiaChiPXK.Location = new System.Drawing.Point(6, 49);
            this.lblDiaChiPXK.Name = "lblDiaChiPXK";
            this.lblDiaChiPXK.Size = new System.Drawing.Size(80, 22);
            this.lblDiaChiPXK.TabIndex = 22;
            this.lblDiaChiPXK.Text = "Địa chỉ";
            // 
            // lblKhachHangPXK
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.TextVAlignAsString = "Middle";
            this.lblKhachHangPXK.Appearance = appearance47;
            this.lblKhachHangPXK.Location = new System.Drawing.Point(6, 25);
            this.lblKhachHangPXK.Name = "lblKhachHangPXK";
            this.lblKhachHangPXK.Size = new System.Drawing.Size(80, 22);
            this.lblKhachHangPXK.TabIndex = 0;
            this.lblKhachHangPXK.Text = "Đối tượng";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl4.Controls.Add(this.GBHDGG);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(944, 157);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.txtMauSoHDI);
            this.ultraGroupBox1.Controls.Add(this.txtSoHDI);
            this.ultraGroupBox1.Controls.Add(this.txtKyHieuHDI);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel26);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel34);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel35);
            this.ultraGroupBox1.Controls.Add(this.dteNgayHDI);
            this.ultraGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox1.Location = new System.Drawing.Point(682, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(263, 157);
            this.ultraGroupBox1.TabIndex = 33;
            this.ultraGroupBox1.Text = "Hóa đơn";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtMauSoHDI
            // 
            this.txtMauSoHDI.AutoSize = false;
            this.txtMauSoHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMauSoHDI.Location = new System.Drawing.Point(77, 21);
            this.txtMauSoHDI.Name = "txtMauSoHDI";
            this.txtMauSoHDI.Size = new System.Drawing.Size(166, 22);
            this.txtMauSoHDI.TabIndex = 76;
            // 
            // txtSoHDI
            // 
            this.txtSoHDI.AutoSize = false;
            this.txtSoHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHDI.Location = new System.Drawing.Point(77, 72);
            this.txtSoHDI.Name = "txtSoHDI";
            this.txtSoHDI.Size = new System.Drawing.Size(166, 22);
            this.txtSoHDI.TabIndex = 18;
            // 
            // txtKyHieuHDI
            // 
            this.txtKyHieuHDI.AutoSize = false;
            this.txtKyHieuHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHDI.Location = new System.Drawing.Point(77, 46);
            this.txtKyHieuHDI.Name = "txtKyHieuHDI";
            this.txtKyHieuHDI.Size = new System.Drawing.Size(166, 22);
            this.txtKyHieuHDI.TabIndex = 17;
            // 
            // ultraLabel26
            // 
            this.ultraLabel26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance49.BackColor = System.Drawing.Color.Transparent;
            appearance49.TextVAlignAsString = "Middle";
            this.ultraLabel26.Appearance = appearance49;
            this.ultraLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel26.Location = new System.Drawing.Point(10, 99);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel26.TabIndex = 69;
            this.ultraLabel26.Text = "Ngày HĐ";
            // 
            // ultraLabel27
            // 
            this.ultraLabel27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.TextVAlignAsString = "Middle";
            this.ultraLabel27.Appearance = appearance50;
            this.ultraLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel27.Location = new System.Drawing.Point(9, 73);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel27.TabIndex = 68;
            this.ultraLabel27.Text = "Số HĐ";
            // 
            // ultraLabel34
            // 
            this.ultraLabel34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextVAlignAsString = "Middle";
            this.ultraLabel34.Appearance = appearance51;
            this.ultraLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel34.Location = new System.Drawing.Point(9, 48);
            this.ultraLabel34.Name = "ultraLabel34";
            this.ultraLabel34.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel34.TabIndex = 67;
            this.ultraLabel34.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel35
            // 
            this.ultraLabel35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextVAlignAsString = "Middle";
            this.ultraLabel35.Appearance = appearance52;
            this.ultraLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel35.Location = new System.Drawing.Point(10, 24);
            this.ultraLabel35.Name = "ultraLabel35";
            this.ultraLabel35.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel35.TabIndex = 66;
            this.ultraLabel35.Text = "Mẫu số HĐ";
            // 
            // dteNgayHDI
            // 
            appearance53.TextHAlignAsString = "Center";
            appearance53.TextVAlignAsString = "Middle";
            this.dteNgayHDI.Appearance = appearance53;
            this.dteNgayHDI.AutoSize = false;
            this.dteNgayHDI.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHDI.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHDI.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHDI.Location = new System.Drawing.Point(77, 98);
            this.dteNgayHDI.MaskInput = "";
            this.dteNgayHDI.Name = "dteNgayHDI";
            this.dteNgayHDI.Size = new System.Drawing.Size(100, 22);
            this.dteNgayHDI.TabIndex = 19;
            this.dteNgayHDI.Value = null;
            // 
            // GBHDGG
            // 
            this.GBHDGG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GBHDGG.Controls.Add(this.uComboThanhToan);
            this.GBHDGG.Controls.Add(this.txtAccountingBankName);
            this.GBHDGG.Controls.Add(this.ultraLabel21);
            this.GBHDGG.Controls.Add(this.cbbAccountingObjectBankAccount);
            this.GBHDGG.Controls.Add(this.ultraLabel20);
            this.GBHDGG.Controls.Add(this.ultraLabel17);
            this.GBHDGG.Controls.Add(this.ultraLabel28);
            this.GBHDGG.Controls.Add(this.txtOriginalNoPXKHD);
            this.GBHDGG.Controls.Add(this.txtCompanyTaxCodePXKHD);
            this.GBHDGG.Controls.Add(this.ultraLabel29);
            this.GBHDGG.Controls.Add(this.ultraLabel30);
            this.GBHDGG.Controls.Add(this.txtAccountingObjectNamePXKHD);
            this.GBHDGG.Controls.Add(this.cbbAccountingObjectIDPXKHD);
            this.GBHDGG.Controls.Add(this.ultraTextEditor1HD);
            this.GBHDGG.Controls.Add(this.ultraLabel31);
            this.GBHDGG.Controls.Add(this.txtAccountingObjectAddressPXKHD);
            this.GBHDGG.Controls.Add(this.ultraLabel32);
            this.GBHDGG.Controls.Add(this.ultraLabel33);
            appearance65.FontData.BoldAsString = "True";
            appearance65.FontData.SizeInPoints = 10F;
            this.GBHDGG.HeaderAppearance = appearance65;
            this.GBHDGG.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.GBHDGG.Location = new System.Drawing.Point(0, 0);
            this.GBHDGG.Name = "GBHDGG";
            this.GBHDGG.Size = new System.Drawing.Size(682, 157);
            this.GBHDGG.TabIndex = 27;
            this.GBHDGG.Text = "Thông tin chung";
            this.GBHDGG.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uComboThanhToan
            // 
            this.uComboThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uComboThanhToan.AutoSize = false;
            this.uComboThanhToan.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboThanhToan.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.uComboThanhToan.Location = new System.Drawing.Point(513, 97);
            this.uComboThanhToan.Name = "uComboThanhToan";
            this.uComboThanhToan.Size = new System.Drawing.Size(160, 22);
            this.uComboThanhToan.TabIndex = 84;
            this.uComboThanhToan.TextChanged += new System.EventHandler(this.txtThanhToanGG_TextChanged);
            // 
            // txtAccountingBankName
            // 
            this.txtAccountingBankName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingBankName.AutoSize = false;
            this.txtAccountingBankName.Location = new System.Drawing.Point(513, 72);
            this.txtAccountingBankName.Name = "txtAccountingBankName";
            this.txtAccountingBankName.Size = new System.Drawing.Size(160, 22);
            this.txtAccountingBankName.TabIndex = 82;
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance54.BackColor = System.Drawing.Color.Transparent;
            appearance54.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance54;
            this.ultraLabel21.Location = new System.Drawing.Point(393, 72);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel21.TabIndex = 83;
            this.ultraLabel21.Text = "Tên ngân hàng";
            // 
            // cbbAccountingObjectBankAccount
            // 
            this.cbbAccountingObjectBankAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbAccountingObjectBankAccount.AutoSize = false;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(250, 72);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(121, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 81;
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance55;
            this.ultraLabel20.Location = new System.Drawing.Point(160, 72);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(76, 21);
            this.ultraLabel20.TabIndex = 80;
            this.ultraLabel20.Text = "TK ngân hàng";
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance56.BackColor = System.Drawing.Color.Transparent;
            appearance56.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance56;
            this.ultraLabel17.Location = new System.Drawing.Point(394, 96);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(135, 22);
            this.ultraLabel17.TabIndex = 77;
            this.ultraLabel17.Text = "Hình thức thanh toán";
            // 
            // ultraLabel28
            // 
            this.ultraLabel28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextHAlignAsString = "Center";
            appearance57.TextVAlignAsString = "Middle";
            this.ultraLabel28.Appearance = appearance57;
            this.ultraLabel28.Location = new System.Drawing.Point(558, 120);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(115, 22);
            this.ultraLabel28.TabIndex = 76;
            this.ultraLabel28.Text = "Chứng từ gốc";
            // 
            // txtOriginalNoPXKHD
            // 
            this.txtOriginalNoPXKHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginalNoPXKHD.AutoSize = false;
            this.txtOriginalNoPXKHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtOriginalNoPXKHD.Location = new System.Drawing.Point(92, 120);
            this.txtOriginalNoPXKHD.Name = "txtOriginalNoPXKHD";
            this.txtOriginalNoPXKHD.Size = new System.Drawing.Size(460, 22);
            this.txtOriginalNoPXKHD.TabIndex = 75;
            // 
            // txtCompanyTaxCodePXKHD
            // 
            this.txtCompanyTaxCodePXKHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance58.TextVAlignAsString = "Middle";
            this.txtCompanyTaxCodePXKHD.Appearance = appearance58;
            this.txtCompanyTaxCodePXKHD.AutoSize = false;
            this.txtCompanyTaxCodePXKHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtCompanyTaxCodePXKHD.Location = new System.Drawing.Point(92, 72);
            this.txtCompanyTaxCodePXKHD.Name = "txtCompanyTaxCodePXKHD";
            this.txtCompanyTaxCodePXKHD.Size = new System.Drawing.Size(62, 22);
            this.txtCompanyTaxCodePXKHD.TabIndex = 68;
            // 
            // ultraLabel29
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            appearance59.TextVAlignAsString = "Middle";
            this.ultraLabel29.Appearance = appearance59;
            this.ultraLabel29.Location = new System.Drawing.Point(6, 71);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel29.TabIndex = 67;
            this.ultraLabel29.Text = "Người giao";
            // 
            // ultraLabel30
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextVAlignAsString = "Middle";
            this.ultraLabel30.Appearance = appearance60;
            this.ultraLabel30.Location = new System.Drawing.Point(6, 119);
            this.ultraLabel30.Name = "ultraLabel30";
            this.ultraLabel30.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel30.TabIndex = 64;
            this.ultraLabel30.Text = "Kèm theo";
            // 
            // txtAccountingObjectNamePXKHD
            // 
            this.txtAccountingObjectNamePXKHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePXKHD.AutoSize = false;
            this.txtAccountingObjectNamePXKHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectNamePXKHD.Location = new System.Drawing.Point(244, 23);
            this.txtAccountingObjectNamePXKHD.Name = "txtAccountingObjectNamePXKHD";
            this.txtAccountingObjectNamePXKHD.Size = new System.Drawing.Size(429, 22);
            this.txtAccountingObjectNamePXKHD.TabIndex = 63;
            this.txtAccountingObjectNamePXKHD.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // cbbAccountingObjectIDPXKHD
            // 
            this.cbbAccountingObjectIDPXKHD.AutoSize = false;
            this.cbbAccountingObjectIDPXKHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectIDPXKHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectIDPXKHD.Location = new System.Drawing.Point(92, 23);
            this.cbbAccountingObjectIDPXKHD.Name = "cbbAccountingObjectIDPXKHD";
            this.cbbAccountingObjectIDPXKHD.Size = new System.Drawing.Size(147, 22);
            this.cbbAccountingObjectIDPXKHD.TabIndex = 62;
            // 
            // ultraTextEditor1HD
            // 
            this.ultraTextEditor1HD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance61.TextVAlignAsString = "Middle";
            this.ultraTextEditor1HD.Appearance = appearance61;
            this.ultraTextEditor1HD.AutoSize = false;
            this.ultraTextEditor1HD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.ultraTextEditor1HD.Location = new System.Drawing.Point(92, 96);
            this.ultraTextEditor1HD.Name = "ultraTextEditor1HD";
            this.ultraTextEditor1HD.Size = new System.Drawing.Size(279, 22);
            this.ultraTextEditor1HD.TabIndex = 61;
            // 
            // ultraLabel31
            // 
            appearance62.BackColor = System.Drawing.Color.Transparent;
            appearance62.TextVAlignAsString = "Middle";
            this.ultraLabel31.Appearance = appearance62;
            this.ultraLabel31.Location = new System.Drawing.Point(6, 95);
            this.ultraLabel31.Name = "ultraLabel31";
            this.ultraLabel31.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel31.TabIndex = 60;
            this.ultraLabel31.Text = "Diễn giải";
            // 
            // txtAccountingObjectAddressPXKHD
            // 
            this.txtAccountingObjectAddressPXKHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPXKHD.AutoSize = false;
            this.txtAccountingObjectAddressPXKHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.txtAccountingObjectAddressPXKHD.Location = new System.Drawing.Point(92, 48);
            this.txtAccountingObjectAddressPXKHD.Name = "txtAccountingObjectAddressPXKHD";
            this.txtAccountingObjectAddressPXKHD.Size = new System.Drawing.Size(581, 22);
            this.txtAccountingObjectAddressPXKHD.TabIndex = 57;
            this.txtAccountingObjectAddressPXKHD.TextChanged += new System.EventHandler(this.txtAccountingObjectName_TextChanged);
            // 
            // ultraLabel32
            // 
            appearance63.BackColor = System.Drawing.Color.Transparent;
            appearance63.TextVAlignAsString = "Middle";
            this.ultraLabel32.Appearance = appearance63;
            this.ultraLabel32.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel32.Name = "ultraLabel32";
            this.ultraLabel32.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel32.TabIndex = 56;
            this.ultraLabel32.Text = "Địa chỉ";
            // 
            // ultraLabel33
            // 
            appearance64.BackColor = System.Drawing.Color.Transparent;
            appearance64.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance64;
            this.ultraLabel33.Location = new System.Drawing.Point(6, 23);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(80, 21);
            this.ultraLabel33.TabIndex = 55;
            this.ultraLabel33.Text = "Đối tượng";
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(944, 157);
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.ultraGroupBoxTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(946, 64);
            this.palTop.TabIndex = 30;
            // 
            // ultraGroupBoxTop
            // 
            this.ultraGroupBoxTop.Controls.Add(this.optPhieuXuatKho);
            this.ultraGroupBoxTop.Controls.Add(this.optHoaDon);
            this.ultraGroupBoxTop.Controls.Add(this.palTopSctNhtNctHoaDon);
            this.ultraGroupBoxTop.Controls.Add(this.palTopSctNhtNctPXK);
            this.ultraGroupBoxTop.Controls.Add(this.lblCachNhapDG);
            this.ultraGroupBoxTop.Controls.Add(this.cbbCachNhapDonGia);
            this.ultraGroupBoxTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance71.FontData.BoldAsString = "True";
            appearance71.FontData.SizeInPoints = 13F;
            this.ultraGroupBoxTop.HeaderAppearance = appearance71;
            this.ultraGroupBoxTop.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxTop.Name = "ultraGroupBoxTop";
            this.ultraGroupBoxTop.Size = new System.Drawing.Size(946, 64);
            this.ultraGroupBoxTop.TabIndex = 31;
            this.ultraGroupBoxTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optPhieuXuatKho
            // 
            this.optPhieuXuatKho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance66.TextHAlignAsString = "Left";
            appearance66.TextVAlignAsString = "Middle";
            this.optPhieuXuatKho.Appearance = appearance66;
            this.optPhieuXuatKho.BackColor = System.Drawing.Color.Transparent;
            this.optPhieuXuatKho.BackColorInternal = System.Drawing.Color.Transparent;
            this.optPhieuXuatKho.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem3.DataValue = true;
            valueListItem3.DisplayText = "Kiêm phiếu nhập kho";
            valueListItem3.Tag = "KPXK";
            valueListItem4.DataValue = "ValueListItem1";
            valueListItem4.DisplayText = "Không kiêm phiếu nhập kho";
            valueListItem4.Tag = "KKPXK";
            this.optPhieuXuatKho.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.optPhieuXuatKho.ItemSpacingHorizontal = 1;
            this.optPhieuXuatKho.Location = new System.Drawing.Point(542, 34);
            this.optPhieuXuatKho.Name = "optPhieuXuatKho";
            this.optPhieuXuatKho.ReadOnly = false;
            this.optPhieuXuatKho.Size = new System.Drawing.Size(288, 17);
            this.optPhieuXuatKho.TabIndex = 73;
            this.optPhieuXuatKho.Visible = false;
            this.optPhieuXuatKho.ValueChanged += new System.EventHandler(this.OptPhieuXuatKhoValueChangedStand);
            // 
            // optHoaDon
            // 
            this.optHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance67.TextHAlignAsString = "Left";
            appearance67.TextVAlignAsString = "Middle";
            this.optHoaDon.Appearance = appearance67;
            this.optHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.optHoaDon.BackColorInternal = System.Drawing.Color.Transparent;
            this.optHoaDon.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem7.DataValue = true;
            valueListItem7.DisplayText = "Lập kèm hóa đơn";
            valueListItem7.Tag = "KHD";
            valueListItem8.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem8.DataValue = false;
            valueListItem8.DisplayText = "Không kèm hóa đơn";
            valueListItem8.Tag = "KKHD";
            this.optHoaDon.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.optHoaDon.ItemSpacingHorizontal = 17;
            this.optHoaDon.Location = new System.Drawing.Point(542, 34);
            this.optHoaDon.Name = "optHoaDon";
            this.optHoaDon.ReadOnly = false;
            this.optHoaDon.Size = new System.Drawing.Size(288, 17);
            this.optHoaDon.TabIndex = 72;
            this.optHoaDon.Visible = false;
            this.optHoaDon.ValueChanged += new System.EventHandler(this.optHoaDon_ValueChanged);
            // 
            // palTopSctNhtNctHoaDon
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctHoaDon.Appearance = appearance68;
            this.palTopSctNhtNctHoaDon.AutoSize = true;
            this.palTopSctNhtNctHoaDon.Location = new System.Drawing.Point(13, 12);
            this.palTopSctNhtNctHoaDon.Name = "palTopSctNhtNctHoaDon";
            this.palTopSctNhtNctHoaDon.Size = new System.Drawing.Size(390, 36);
            this.palTopSctNhtNctHoaDon.TabIndex = 71;
            // 
            // palTopSctNhtNctPXK
            // 
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.BackColor2 = System.Drawing.Color.Transparent;
            this.palTopSctNhtNctPXK.Appearance = appearance69;
            this.palTopSctNhtNctPXK.AutoSize = true;
            this.palTopSctNhtNctPXK.Location = new System.Drawing.Point(13, 12);
            this.palTopSctNhtNctPXK.Name = "palTopSctNhtNctPXK";
            this.palTopSctNhtNctPXK.Size = new System.Drawing.Size(391, 36);
            this.palTopSctNhtNctPXK.TabIndex = 70;
            // 
            // lblCachNhapDG
            // 
            this.lblCachNhapDG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextVAlignAsString = "Middle";
            this.lblCachNhapDG.Appearance = appearance70;
            this.lblCachNhapDG.Location = new System.Drawing.Point(542, 6);
            this.lblCachNhapDG.Name = "lblCachNhapDG";
            this.lblCachNhapDG.Size = new System.Drawing.Size(105, 19);
            this.lblCachNhapDG.TabIndex = 67;
            this.lblCachNhapDG.Text = "Cách nhập đơn giá";
            // 
            // cbbCachNhapDonGia
            // 
            this.cbbCachNhapDonGia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbCachNhapDonGia.AutoSize = false;
            this.cbbCachNhapDonGia.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2010Button;
            this.cbbCachNhapDonGia.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem6.DataValue = 2;
            valueListItem6.DisplayText = "Lấy từ giá xuất bán";
            valueListItem6.Tag = "LayTuGiaXuatBan";
            valueListItem1.DataValue = 3;
            valueListItem1.DisplayText = "Nhập đơn giá bằng tay";
            valueListItem1.Tag = "NhapDonGiaBangTay";
            this.cbbCachNhapDonGia.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem1});
            this.cbbCachNhapDonGia.Location = new System.Drawing.Point(653, 6);
            this.cbbCachNhapDonGia.Name = "cbbCachNhapDonGia";
            this.cbbCachNhapDonGia.Size = new System.Drawing.Size(283, 22);
            this.cbbCachNhapDonGia.TabIndex = 36;
            this.cbbCachNhapDonGia.ValueChanged += new System.EventHandler(this.cbbCachNhapDonGia_ValueChanged);
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 254);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(946, 278);
            this.pnlUgrid.TabIndex = 31;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance72.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance72;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(848, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.panel1);
            this.palBottom.Controls.Add(this.btnSelectBill);
            this.palBottom.Controls.Add(this.cbBill);
            this.palBottom.Controls.Add(this.txtTotalDiscountAmountSubOriginal);
            this.palBottom.Controls.Add(this.txtTotalDiscountAmountSub);
            this.palBottom.Controls.Add(this.ultraLabel5);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountOriginalStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmount);
            this.palBottom.Controls.Add(this.txtTotalAmount);
            this.palBottom.Controls.Add(this.lblTotalVATAmount);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmount);
            this.palBottom.Controls.Add(this.lblTotalAmount);
            this.palBottom.Controls.Add(this.uGridControl);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 532);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(946, 140);
            this.palBottom.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtOldInvSeries);
            this.panel1.Controls.Add(this.ultraLabel16);
            this.panel1.Controls.Add(this.txtOldInvTemplate);
            this.panel1.Controls.Add(this.ultraLabel18);
            this.panel1.Controls.Add(this.txtOldInvDate);
            this.panel1.Controls.Add(this.ultraLabel19);
            this.panel1.Controls.Add(this.txtOldInvNo);
            this.panel1.Controls.Add(this.lblTttt);
            this.panel1.Location = new System.Drawing.Point(4, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(656, 34);
            this.panel1.TabIndex = 71;
            // 
            // txtOldInvSeries
            // 
            this.txtOldInvSeries.AutoSize = false;
            this.txtOldInvSeries.Location = new System.Drawing.Point(538, 1);
            this.txtOldInvSeries.Name = "txtOldInvSeries";
            this.txtOldInvSeries.ReadOnly = true;
            this.txtOldInvSeries.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvSeries.TabIndex = 49;
            // 
            // ultraLabel16
            // 
            appearance73.BackColor = System.Drawing.Color.Transparent;
            appearance73.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance73;
            this.ultraLabel16.Location = new System.Drawing.Point(488, 3);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(43, 19);
            this.ultraLabel16.TabIndex = 48;
            this.ultraLabel16.Text = "Ký hiệu";
            // 
            // txtOldInvTemplate
            // 
            this.txtOldInvTemplate.AutoSize = false;
            this.txtOldInvTemplate.Location = new System.Drawing.Point(362, 0);
            this.txtOldInvTemplate.Name = "txtOldInvTemplate";
            this.txtOldInvTemplate.ReadOnly = true;
            this.txtOldInvTemplate.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvTemplate.TabIndex = 47;
            // 
            // ultraLabel18
            // 
            appearance74.BackColor = System.Drawing.Color.Transparent;
            appearance74.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance74;
            this.ultraLabel18.Location = new System.Drawing.Point(315, 3);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(44, 19);
            this.ultraLabel18.TabIndex = 46;
            this.ultraLabel18.Text = "Mẫu số";
            // 
            // txtOldInvDate
            // 
            this.txtOldInvDate.AutoSize = false;
            this.txtOldInvDate.Location = new System.Drawing.Point(197, 1);
            this.txtOldInvDate.Name = "txtOldInvDate";
            this.txtOldInvDate.ReadOnly = true;
            this.txtOldInvDate.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvDate.TabIndex = 45;
            // 
            // ultraLabel19
            // 
            appearance75.BackColor = System.Drawing.Color.Transparent;
            appearance75.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance75;
            this.ultraLabel19.Location = new System.Drawing.Point(153, 4);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(37, 19);
            this.ultraLabel19.TabIndex = 44;
            this.ultraLabel19.Text = "Ngày";
            // 
            // txtOldInvNo
            // 
            this.txtOldInvNo.AutoSize = false;
            this.txtOldInvNo.Location = new System.Drawing.Point(31, 1);
            this.txtOldInvNo.Name = "txtOldInvNo";
            this.txtOldInvNo.ReadOnly = true;
            this.txtOldInvNo.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvNo.TabIndex = 43;
            // 
            // lblTttt
            // 
            appearance76.BackColor = System.Drawing.Color.Transparent;
            appearance76.TextVAlignAsString = "Middle";
            this.lblTttt.Appearance = appearance76;
            this.lblTttt.Location = new System.Drawing.Point(3, 3);
            this.lblTttt.Name = "lblTttt";
            this.lblTttt.Size = new System.Drawing.Size(30, 19);
            this.lblTttt.TabIndex = 26;
            this.lblTttt.Text = "Số";
            // 
            // btnSelectBill
            // 
            this.btnSelectBill.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnSelectBill.Enabled = false;
            this.btnSelectBill.Location = new System.Drawing.Point(172, 6);
            this.btnSelectBill.Name = "btnSelectBill";
            this.btnSelectBill.Size = new System.Drawing.Size(85, 23);
            this.btnSelectBill.TabIndex = 70;
            this.btnSelectBill.Text = "Chọn HĐ";
            this.btnSelectBill.Visible = false;
            this.btnSelectBill.Click += new System.EventHandler(this.btnSelectBill_Click);
            // 
            // cbBill
            // 
            this.cbBill.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbBill.Location = new System.Drawing.Point(10, 6);
            this.cbBill.Name = "cbBill";
            this.cbBill.Size = new System.Drawing.Size(156, 22);
            this.cbBill.TabIndex = 69;
            this.cbBill.Text = "Đã lập hóa đơn";
            this.cbBill.Visible = false;
            this.cbBill.CheckStateChanged += new System.EventHandler(this.cbBill_CheckStateChanged);
            // 
            // txtTotalDiscountAmountSubOriginal
            // 
            this.txtTotalDiscountAmountSubOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance77.BackColor = System.Drawing.Color.Transparent;
            appearance77.TextHAlignAsString = "Right";
            appearance77.TextVAlignAsString = "Middle";
            this.txtTotalDiscountAmountSubOriginal.Appearance = appearance77;
            this.txtTotalDiscountAmountSubOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalDiscountAmountSubOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiscountAmountSubOriginal.Location = new System.Drawing.Point(594, 27);
            this.txtTotalDiscountAmountSubOriginal.Name = "txtTotalDiscountAmountSubOriginal";
            this.txtTotalDiscountAmountSubOriginal.Size = new System.Drawing.Size(169, 21);
            this.txtTotalDiscountAmountSubOriginal.TabIndex = 68;
            // 
            // txtTotalDiscountAmountSub
            // 
            this.txtTotalDiscountAmountSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance78.BackColor = System.Drawing.Color.Transparent;
            appearance78.TextHAlignAsString = "Right";
            appearance78.TextVAlignAsString = "Middle";
            this.txtTotalDiscountAmountSub.Appearance = appearance78;
            this.txtTotalDiscountAmountSub.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalDiscountAmountSub.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiscountAmountSub.Location = new System.Drawing.Point(769, 27);
            this.txtTotalDiscountAmountSub.Name = "txtTotalDiscountAmountSub";
            this.txtTotalDiscountAmountSub.Size = new System.Drawing.Size(169, 21);
            this.txtTotalDiscountAmountSub.TabIndex = 67;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance79.BackColor = System.Drawing.Color.Transparent;
            appearance79.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance79;
            this.ultraLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel5.Location = new System.Drawing.Point(465, 27);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(115, 21);
            this.ultraLabel5.TabIndex = 66;
            this.ultraLabel5.Text = "Tiền chiết khấu";
            // 
            // txtTotalPaymentAmountOriginalStand
            // 
            this.txtTotalPaymentAmountOriginalStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance80.BackColor = System.Drawing.Color.Transparent;
            appearance80.TextHAlignAsString = "Right";
            appearance80.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountOriginalStand.Appearance = appearance80;
            this.txtTotalPaymentAmountOriginalStand.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalPaymentAmountOriginalStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountOriginalStand.Location = new System.Drawing.Point(594, 73);
            this.txtTotalPaymentAmountOriginalStand.Name = "txtTotalPaymentAmountOriginalStand";
            this.txtTotalPaymentAmountOriginalStand.Size = new System.Drawing.Size(169, 21);
            this.txtTotalPaymentAmountOriginalStand.TabIndex = 65;
            // 
            // txtTotalVATAmountOriginal
            // 
            this.txtTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance81.BackColor = System.Drawing.Color.Transparent;
            appearance81.TextHAlignAsString = "Right";
            appearance81.TextVAlignAsString = "Middle";
            this.txtTotalVATAmountOriginal.Appearance = appearance81;
            this.txtTotalVATAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmountOriginal.Location = new System.Drawing.Point(594, 50);
            this.txtTotalVATAmountOriginal.Name = "txtTotalVATAmountOriginal";
            this.txtTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.txtTotalVATAmountOriginal.TabIndex = 64;
            // 
            // txtTotalAmountOriginal
            // 
            this.txtTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance82.BackColor = System.Drawing.Color.Transparent;
            appearance82.TextHAlignAsString = "Right";
            appearance82.TextVAlignAsString = "Middle";
            this.txtTotalAmountOriginal.Appearance = appearance82;
            this.txtTotalAmountOriginal.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmountOriginal.Location = new System.Drawing.Point(594, 4);
            this.txtTotalAmountOriginal.Name = "txtTotalAmountOriginal";
            this.txtTotalAmountOriginal.Size = new System.Drawing.Size(169, 21);
            this.txtTotalAmountOriginal.TabIndex = 63;
            // 
            // txtTotalPaymentAmountStand
            // 
            this.txtTotalPaymentAmountStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance83.BackColor = System.Drawing.Color.Transparent;
            appearance83.TextHAlignAsString = "Right";
            appearance83.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountStand.Appearance = appearance83;
            this.txtTotalPaymentAmountStand.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalPaymentAmountStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountStand.Location = new System.Drawing.Point(769, 73);
            this.txtTotalPaymentAmountStand.Name = "txtTotalPaymentAmountStand";
            this.txtTotalPaymentAmountStand.Size = new System.Drawing.Size(169, 21);
            this.txtTotalPaymentAmountStand.TabIndex = 62;
            // 
            // txtTotalVATAmount
            // 
            this.txtTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance84.BackColor = System.Drawing.Color.Transparent;
            appearance84.TextHAlignAsString = "Right";
            appearance84.TextVAlignAsString = "Middle";
            this.txtTotalVATAmount.Appearance = appearance84;
            this.txtTotalVATAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmount.Location = new System.Drawing.Point(769, 50);
            this.txtTotalVATAmount.Name = "txtTotalVATAmount";
            this.txtTotalVATAmount.Size = new System.Drawing.Size(169, 21);
            this.txtTotalVATAmount.TabIndex = 61;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance85.BackColor = System.Drawing.Color.Transparent;
            appearance85.TextHAlignAsString = "Right";
            appearance85.TextVAlignAsString = "Middle";
            this.txtTotalAmount.Appearance = appearance85;
            this.txtTotalAmount.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.txtTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(769, 4);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(169, 21);
            this.txtTotalAmount.TabIndex = 60;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance86.BackColor = System.Drawing.Color.Transparent;
            appearance86.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance86;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(465, 50);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(115, 21);
            this.lblTotalVATAmount.TabIndex = 59;
            this.lblTotalVATAmount.Text = "Tổng thuế GTGT";
            // 
            // lblTotalPaymentAmount
            // 
            this.lblTotalPaymentAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance87.BackColor = System.Drawing.Color.Transparent;
            appearance87.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmount.Appearance = appearance87;
            this.lblTotalPaymentAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(465, 73);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(115, 21);
            this.lblTotalPaymentAmount.TabIndex = 58;
            this.lblTotalPaymentAmount.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance88.BackColor = System.Drawing.Color.Transparent;
            appearance88.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance88;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(465, 4);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(115, 21);
            this.lblTotalAmount.TabIndex = 57;
            this.lblTotalAmount.Text = "Tổng tiền hàng";
            // 
            // uGridControl
            // 
            this.uGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridControl.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridControl.Location = new System.Drawing.Point(594, 97);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(352, 43);
            this.uGridControl.TabIndex = 1;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 244);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 176;
            this.ultraSplitter1.Size = new System.Drawing.Size(946, 10);
            this.ultraSplitter1.TabIndex = 34;
            // 
            // ultraTabSharedControlsPage4
            // 
            this.ultraTabSharedControlsPage4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage4.Name = "ultraTabSharedControlsPage4";
            this.ultraTabSharedControlsPage4.Size = new System.Drawing.Size(944, 157);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.paneHoaDon);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraTabControlGG);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 64);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(946, 180);
            this.ultraPanel1.TabIndex = 33;
            // 
            // paneHoaDon
            // 
            // 
            // paneHoaDon.ClientArea
            // 
            this.paneHoaDon.ClientArea.Controls.Add(this.uGroupBoxEx);
            this.paneHoaDon.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneHoaDon.Location = new System.Drawing.Point(0, 180);
            this.paneHoaDon.Name = "paneHoaDon";
            this.paneHoaDon.Size = new System.Drawing.Size(946, 180);
            this.paneHoaDon.TabIndex = 31;
            // 
            // uGroupBoxEx
            // 
            this.uGroupBoxEx.Controls.Add(this.ultraTabSharedControlsPage2);
            this.uGroupBoxEx.Controls.Add(this.ultraTabPageHoaDon);
            this.uGroupBoxEx.Controls.Add(this.ultraTabPagePhieuXuatKho);
            this.uGroupBoxEx.Controls.Add(this.ultraTabPageControl1);
            this.uGroupBoxEx.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxEx.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxEx.Name = "uGroupBoxEx";
            this.uGroupBoxEx.SharedControlsPage = this.ultraTabSharedControlsPage3;
            this.uGroupBoxEx.Size = new System.Drawing.Size(946, 173);
            this.uGroupBoxEx.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.uGroupBoxEx.TabIndex = 30;
            ultraTab2.Key = "tabHoaDon";
            ultraTab2.TabPage = this.ultraTabPageHoaDon;
            ultraTab2.Text = "Chứng từ";
            ultraTab3.Key = "tabPhieuXuatKho";
            ultraTab3.TabPage = this.ultraTabPagePhieuXuatKho;
            ultraTab3.Text = "Phiếu nhập";
            ultraTab6.Key = "tabHoaDon1";
            ultraTab6.TabPage = this.ultraTabPageControl1;
            ultraTab6.Text = "Hóa đơn";
            ultraTab5.TabPage = this.ultraTabSharedControlsPage2;
            this.uGroupBoxEx.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab3,
            ultraTab6,
            ultraTab5});
            this.uGroupBoxEx.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.uGroupBoxEx.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.uGroupBoxEx_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage3
            // 
            this.ultraTabSharedControlsPage3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage3.Name = "ultraTabSharedControlsPage3";
            this.ultraTabSharedControlsPage3.Size = new System.Drawing.Size(944, 150);
            // 
            // ultraTabControlGG
            // 
            this.ultraTabControlGG.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControlGG.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControlGG.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControlGG.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraTabControlGG.Location = new System.Drawing.Point(0, 0);
            this.ultraTabControlGG.Name = "ultraTabControlGG";
            this.ultraTabControlGG.SharedControlsPage = this.ultraTabSharedControlsPage4;
            this.ultraTabControlGG.Size = new System.Drawing.Size(946, 180);
            this.ultraTabControlGG.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.ultraTabControlGG.TabIndex = 30;
            ultraTab4.Key = "tabHoaDon";
            ultraTab4.TabPage = this.ultraTabPageControl2;
            ultraTab4.Text = "Chứng từ";
            ultraTab1.Key = "tabHoaDon1";
            ultraTab1.TabPage = this.ultraTabPageControl4;
            ultraTab1.Text = "Hóa đơn";
            ultraTab7.TabPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControlGG.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab4,
            ultraTab1,
            ultraTab7});
            this.ultraTabControlGG.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.ultraTabControlGG.ActiveTabChanged += new Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventHandler(this.ultraTabControlGG_ActiveTabChanged);
            // 
            // FSAReturnDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 672);
            this.Controls.Add(this.pnlUgrid);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSAReturnDetail";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSAReturnDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSAReturnDetail_FormClosed);
            this.ultraTabPageHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDon)).EndInit();
            this.grHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).EndInit();
            this.ultraGroupBoxHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOrginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            this.ultraTabPagePhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHoaDonPN)).EndInit();
            this.grHoaDonPN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).EndInit();
            this.ultraGroupBoxPhieuXuatKho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressXK)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GBHDTL)).EndInit();
            this.GBHDTL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOrginalHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonReturnHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStand)).EndInit();
            this.uGroupBoxStand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXK)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMauSoHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GBHDGG)).EndInit();
            this.GBHDGG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uComboThanhToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXKHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXKHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXKHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDPXKHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1HD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXKHD)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).EndInit();
            this.ultraGroupBoxTop.ResumeLayout(false);
            this.ultraGroupBoxTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optPhieuXuatKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optHoaDon)).EndInit();
            this.palTopSctNhtNctHoaDon.ResumeLayout(false);
            this.palTopSctNhtNctHoaDon.PerformLayout();
            this.palTopSctNhtNctPXK.ResumeLayout(false);
            this.palTopSctNhtNctPXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCachNhapDonGia)).EndInit();
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.paneHoaDon.ClientArea.ResumeLayout(false);
            this.paneHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEx)).EndInit();
            this.uGroupBoxEx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlGG)).EndInit();
            this.ultraTabControlGG.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTop;
        private Infragistics.Win.Misc.UltraLabel lblCachNhapDG;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbCachNhapDonGia;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctPXK;
        private Infragistics.Win.Misc.UltraPanel palTopSctNhtNctHoaDon;
        private Infragistics.Win.Misc.UltraLabel txtTotalDiscountAmountSubOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalDiscountAmountSub;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountOriginalStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private UltraOptionSet_Ex optHoaDon;
        private Infragistics.Win.Misc.UltraButton btnSelectBill;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbBill;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvSeries;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvTemplate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvNo;
        private Infragistics.Win.Misc.UltraLabel lblTttt;
        private UltraOptionSet_Ex optPhieuXuatKho;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage4;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel paneHoaDon;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uGroupBoxEx;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageHoaDon;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDon;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOrginal;
        private Infragistics.Win.Misc.UltraButton btnVouchersDiscount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContractName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonReturn;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPagePhieuXuatKho;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuXuatKho;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeXK;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGoc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNo;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameXK;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSReason;
        private Infragistics.Win.Misc.UltraLabel lblSReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressXK;
        private Infragistics.Win.Misc.UltraLabel lblDiaChi;
        private Infragistics.Win.Misc.UltraLabel lblKhachHang;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox GBHDTL;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOrginalHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContractNameHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameHD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonReturnHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage3;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel37;
        private Infragistics.Win.Misc.UltraLabel ultraLabel38;
        private Infragistics.Win.Misc.UltraLabel ultraLabel39;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
        private Infragistics.Win.Misc.UltraGroupBox grHoaDonPN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHDPN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHDPN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHDPN;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHDPN;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlGG;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStand;
        private Infragistics.Win.Misc.UltraButton btnVouchersReturn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodePXK;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCodePXK;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNoPXK;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttachPXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePXK;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDPXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDienGiai;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPXK;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiPXK;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangPXK;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraGroupBox GBHDGG;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboThanhToan;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingBankName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNoPXKHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodePXKHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.Misc.UltraLabel ultraLabel30;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePXKHD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDPXKHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1HD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel31;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPXKHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel32;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMauSoHDI;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHDI;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHDI;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel34;
        private Infragistics.Win.Misc.UltraLabel ultraLabel35;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHDI;
    }
}