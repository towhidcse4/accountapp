﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSelectSAVouchers : FSelectSAVouchersStand
    {
        #region Khai báo

        private SAInvoice _saInvoice = new SAInvoice();
        private BindingList<AccountingObject> _dsAccountingObject = Utils.ListAccountingObject;
        private List<SAInvoice> _dsSAInvoice = new List<SAInvoice>();
        private List<SAInvoiceDetail> _dsSaInvoiceDetails = new List<SAInvoiceDetail>();
        public List<SAReturnDetail> SaReturnDetailSelected { get { return _dsSAReturnDetail; } }
        List<SAReturnDetail> _dsSAReturnDetail = new List<SAReturnDetail>();
        List<SAReturnDetail> _lstSelectAll
        {
            get
            {
                return Utils.ListSAReturn.ToList().SelectMany(x=>x.SAReturnDetails).ToList();
            }
        }
        List<SAReturn> lstSaReturnRecorded = Utils.ListSAReturn.ToList();
        int typeID = 0;
        #endregion

        #region Khởi tạo
        public FSelectSAVouchers(Guid? _accountingObjectID = null, int? TypeID = null)
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion
            typeID = TypeID??0;
            if (_accountingObjectID != null)
                _saInvoice.AccountingObjectID = _accountingObjectID;
            InitializeGUI(_saInvoice);
            if (_saInvoice.AccountingObjectID != null)
            {
                foreach (var item in cbbAccountingObj.Rows)
                {
                    if ((item.ListObject as AccountingObject).ID == _saInvoice.AccountingObjectID) cbbAccountingObj.SelectedRow = item;
                }
            }
       
            WaitingFrm.StopWaiting();
        }

        #endregion

        #region override
        public override void InitializeGUI(SAInvoice inputVoucher)
        {
            #region Lấy dữ liệu từ Database và fill vào Obj
            _dsSAInvoice = Utils.ListSAInvoice.ToList().CloneObject();
            _dsSaInvoiceDetails = Utils.ListSAInvoice.SelectMany(x=>x.SAInvoiceDetails).ToList().CloneObject();
            foreach (var saInvoice in _dsSAInvoice)
            {
                //List<PPInvoiceDetail> temp = model.PPInvoiceDetails.Where(x => (x.Quantity - (_lstSelectAll.Where(y => y.ConfrontDetailID == x.ID && lstPpDiscountReturnRecorded.Any(z => z.ID == y.PPDiscountReturnID)).Sum(y => y.Quantity)) > 0)).ToList();
                saInvoice.SAInvoiceDetails = _dsSaInvoiceDetails.Where(x => x.SAInvoiceID == saInvoice.ID && (x.Quantity - (_lstSelectAll.Where(y => y.SAInvoiceDetailID == x.ID && lstSaReturnRecorded.Any(z => z.ID == y.SAReturnID && z.TypeID == typeID)).Sum(y => y.Quantity)) > 0)).ToList();
            }
            #endregion
            this.ConfigCombo(_dsAccountingObject, cbbAccountingObj, "AccountingObjectCode", "ID", accountingObjectType: 0);
            
            uGridInvoice.DataSource = new List<SAInvoice>();
            Utils.ConfigGrid(uGridInvoice, ConstDatabase.SAInvoice_TableName_FSelectSAVouchers);
            uGridInvoice.DisplayLayout.Bands[0].Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridInvoice.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridInvoice.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
                col.Header.Appearance.TextHAlign = HAlign.Center;
            }

            uGridInvoiceDetail.DataSource = new List<SAInvoiceDetail>();
            Utils.ConfigGrid(uGridInvoiceDetail, ConstDatabase.SAInvoiceDetail_TableName_FSelectSAVouchers);
            uGridInvoiceDetail.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridInvoiceDetail.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridInvoiceDetail.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
                col.Header.Appearance.TextHAlign = HAlign.Center;
            }

            uGridListed.DataSource = new List<SAReturnDetail>();
            Utils.ConfigGrid(uGridListed, ConstDatabase.SAReturnDetail_TableName_FSelectSAVouchers);
            uGridListed.DisplayLayout.Bands[0].Summaries.Clear();
            foreach (var col in uGridListed.DisplayLayout.Bands[0].Columns)
            {
                col.CellActivation = Activation.NoEdit;
                col.Header.Appearance.TextHAlign = HAlign.Center;
            }            
        }
        #endregion

        #region Utils

        void SetDataforuGridInvoiceDetail(UltraGrid ultraGrid)
        {
            SAInvoice model = (SAInvoice)ultraGrid.ActiveRow.ListObject;
            List<SAInvoiceDetail> temp = model.SAInvoiceDetails.ToList();
            if (temp.Count > 0)
            {
                foreach (var item in temp)
                {
                    item.Quantity = item.Quantity - _lstSelectAll.Where(y => y.SAInvoiceDetailID == item.ID && lstSaReturnRecorded.Any(z => z.ID == y.SAReturnID && z.TypeID == typeID)).Sum(y => y.Quantity);
                    item.MaterialGoods = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == item.MaterialGoodsID);

                    if(item.MaterialGoods != null)
                    {
                        item.MaterialGoodsCode = item.MaterialGoods.MaterialGoodsCode;
                        item.MaterialGoodsName = item.MaterialGoods.MaterialGoodsName;
                    }
                }
                uGridInvoiceDetail.DataSource = temp;
                btnSelected.Enabled = true;
                btnSelectedAll.Enabled = true;
                CheckBtnUnSelected();
            }
            else
            {
                uGridInvoiceDetail.DataSource = new List<SAInvoiceDetail>();
                btnSelected.Enabled = false;
                btnSelectedAll.Enabled = false;
                CheckBtnUnSelected();
            }
        }

        void CheckBtnUnSelected()
        {
            if (uGridListed.Rows.Count > 0)
            {
                btnUnSelected.Enabled = true;
                btnUnSelectedAll.Enabled = true;
            }
            else
            {
                btnUnSelected.Enabled = false;
                btnUnSelectedAll.Enabled = false;
            }
        }

        void AddDataToGrid(SAInvoice saInvoice, List<SAInvoiceDetail> lstSaInvoiceDetail)
        {
            var temp = lstSaInvoiceDetail.Select(p => new SAReturnDetail()
            {
                RepositoryID = p.RepositoryID,
                Description = p.Description,
                //DebitAccount = p.DebitAccount,
                //CreditAccount = p.CreditAccount,
                Unit = p.Unit,
                Quantity = p.Quantity,
                QuantityConvert = p.QuantityConvert,
                UnitPrice = p.UnitPrice,
                UnitPriceOriginal = p.UnitPriceOriginal,
                UnitPriceConvert = p.UnitPriceConvert,
                UnitPriceConvertOriginal = p.UnitPriceConvertOriginal,
                Amount = p.Amount,
                AmountOriginal = p.AmountOriginal,
                DiscountRate = p.DiscountRate,
                DiscountAmount = p.DiscountAmount,
                DiscountAmountOriginal = p.DiscountAmountOriginal,
                //DiscountAccount = p.DiscountAccount,
                VATRate = p.VATRate,
                VATAmount = p.VATAmount,
                VATAmountOriginal = p.VATAmountOriginal,
                VATAccount = p.VATAccount,
                //RepositoryAccount = p.RepositoryAccount,
                //CostAccount = p.CostAccount,
                OWPrice = p.OWPrice,
                OWPriceOriginal = p.OWPriceOriginal,
                OWAmount = p.OWAmount,
                OWAmountOriginal = p.OWAmountOriginal,
                ExpiryDate = p.ExpiryDate,
                LotNo = p.LotNo,
                AccountingObjectID = saInvoice.AccountingObjectID,
                CostSetID = p.CostSetID,
                ContractID = p.ContractID,
                SAInvoiceID = p.SAInvoiceID == Guid.Empty ? (Guid?)null : p.SAInvoiceID,
                SAInvoiceDetailID = p.ID == Guid.Empty ? (Guid?)null : p.ID,
                No = saInvoice.No,
                Date = saInvoice.Date,
                ConfrontInvDate = saInvoice.InvoiceDate,
                ConfrontInvNo = saInvoice.InvoiceNo,
                StatisticsCodeID = p.StatisticsCodeID,
                UnitConvert = p.UnitConvert,
                SpecialConsumeTaxRate = p.SpecialConsumeTaxRate,
                SpecialConsumeTaxAmount = p.SpecialConsumeTaxAmount,
                SpecialConsumeTaxAmountOriginal = p.SpecialConsumeTaxAmountOriginal,
                SpecialConsumeUnitPrice = p.SpecialConsumeUnitPrice,
                SpecialConsumeUnitPriceOriginal = p.SpecialConsumeUnitPriceOriginal,
                ConvertRate = p.ConvertRate,
                DepartmentID = p.DepartmentID,
                ExpenseItemID = p.ExpenseItemID,
                BudgetItemID = p.BudgetItemID,
                OrderPriority = p.OrderPriority,
                MaterialGoodsID = p.MaterialGoodsID,
                MaterialGoods = p.MaterialGoods,
                SAInvoice = saInvoice              
            });
            _dsSAReturnDetail.AddRange(temp.ToList());           
            foreach (var item in lstSaInvoiceDetail)
            {
                int index = _dsSAInvoice.IndexOf(saInvoice);
                _dsSAInvoice[index].SAInvoiceDetails.Remove(item);
            }
            uGridListed.DataSource = _dsSAReturnDetail;
            uGridListed.DataBind();
            SetDataforuGridInvoiceDetail(uGridInvoice);
        }

        void RemoveDataFromGrid(List<SAReturnDetail> lstSAReturnDetail)
        {
            int? indexActive = null;
            if (lstSAReturnDetail.Count == 1)
                indexActive = ((List<SAReturnDetail>)uGridListed.DataSource).LastIndexOf(lstSAReturnDetail[0]).CloneObject();
            List<SAInvoiceDetail> temp = SplitListObject(lstSAReturnDetail); 
            if (lstSAReturnDetail.Count > 1)
            {
                for (int i = 0; i < lstSAReturnDetail.Count; i++)
                {
                    int index = _dsSAInvoice.IndexOf(lstSAReturnDetail[i].SAInvoice);
                    _dsSAInvoice[index].SAInvoiceDetails.Add(temp[i]);
                }
                lstSAReturnDetail.Clear();
            }
            else
            {
                for (int i = 0; i < lstSAReturnDetail.Count; i++)
                {
                    int index = _dsSAInvoice.IndexOf(lstSAReturnDetail[i].SAInvoice);
                    _dsSAInvoice[index].SAInvoiceDetails.Add(temp[i]);
                    _dsSAReturnDetail.Remove(lstSAReturnDetail[i]);
                }
            }
            uGridListed.DataSource = _dsSAReturnDetail;
            uGridListed.DataBind();
            SetDataforuGridInvoiceDetail(uGridInvoice);
            if (indexActive.HasValue && uGridListed.Rows.Count > 0 && indexActive < uGridListed.Rows.Count) uGridListed.ActiveRow = uGridListed.Rows[indexActive.Value];
        }

        private List<SAInvoiceDetail> SplitListObject(List<SAReturnDetail> lstSAReturnDetail)
        {
            return lstSAReturnDetail.Select(p => new SAInvoiceDetail()
            {
                ID = p.SAInvoiceDetailID ?? Guid.Empty,
                SAInvoiceID = p.SAInvoiceID ?? Guid.Empty,
                RepositoryID = p.RepositoryID,
                Description = p.Description,
                //DebitAccount = p.DebitAccount,
                //CreditAccount = p.CreditAccount,
                Unit = p.Unit,
                Quantity = p.Quantity,
                QuantityConvert = p.QuantityConvert,
                UnitPrice = p.UnitPrice,
                UnitPriceOriginal = p.UnitPriceOriginal,
                UnitPriceConvert = p.UnitPriceConvert,
                UnitPriceConvertOriginal = p.UnitPriceConvertOriginal,
                Amount = p.Amount,
                AmountOriginal = p.AmountOriginal,
                DiscountRate = p.DiscountRate,
                DiscountAmount = p.DiscountAmount,
                DiscountAmountOriginal = p.DiscountAmountOriginal,
                //DiscountAccount = p.DiscountAccount,
                VATRate = p.VATRate,
                VATAmount = p.VATAmount,
                VATAmountOriginal = p.VATAmountOriginal,
                VATAccount = p.VATAccount,
                //RepositoryAccount = p.RepositoryAccount,
                //CostAccount = p.CostAccount,
                OWPrice = p.OWPrice,
                OWPriceOriginal = p.OWPriceOriginal,
                OWAmount = p.OWAmount,
                OWAmountOriginal = p.OWAmountOriginal,
                ExpiryDate = p.ExpiryDate,
                LotNo = p.LotNo,
                AccountingObjectID = p.AccountingObjectID,
                CostSetID = p.CostSetID,
                ContractID = p.ContractID,
                StatisticsCodeID = p.StatisticsCodeID,
                UnitConvert = p.UnitConvert,
                SpecialConsumeTaxRate = p.SpecialConsumeTaxRate,
                SpecialConsumeTaxAmount = p.SpecialConsumeTaxAmount,
                SpecialConsumeTaxAmountOriginal = p.SpecialConsumeTaxAmountOriginal,
                SpecialConsumeUnitPrice = p.SpecialConsumeUnitPrice,
                SpecialConsumeUnitPriceOriginal = p.SpecialConsumeUnitPriceOriginal,
                ConvertRate = p.ConvertRate,
                DepartmentID = p.DepartmentID,
                ExpenseItemID = p.ExpenseItemID,
                BudgetItemID = p.BudgetItemID,
                OrderPriority = p.OrderPriority,
                MaterialGoodsID = p.MaterialGoodsID,
                MaterialGoods = p.MaterialGoods,
            }).ToList();
        }
        #endregion

        #region Event
        private void cbbAccountingObj_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.Selected)
            {
                AccountingObject model = (AccountingObject)e.Row.ListObject;
                List<SAInvoice> temp =
                    _dsSAInvoice.Where(p => p.AccountingObjectID == model.ID && p.SAInvoiceDetails.Count > 0).OrderByDescending(x=>x.Date).ToList();
                if (temp.Count > 0)
                {
                    uGridInvoice.DataSource = temp;
                    uGridInvoice.ActiveRow = uGridInvoice.Rows[0];
                }
                else
                {
                    uGridInvoice.DataSource = new List<SAInvoice>();
                    uGridInvoiceDetail.DataSource = new List<SAInvoiceDetail>();                   
                }
            }

        }

        private void uGridInvoice_AfterRowActivate(object sender, EventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;
            SetDataforuGridInvoiceDetail(ultraGrid);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if(_dsSAReturnDetail.Count == 0)
            {
                MSG.Warning("Bạn cần chọn chứng từ bán hàng trước");
                return;
            }
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void uGridInvoiceDetail_DoubleClick(object sender, EventArgs e)
        {
            UltraGrid ultraGrid = (UltraGrid)sender;
            SAInvoice saInvoice = (SAInvoice)uGridInvoice.ActiveRow.ListObject;
            List<SAInvoiceDetail> lstSaInvoiceDetail = new List<SAInvoiceDetail>
                {
                    (SAInvoiceDetail) ultraGrid.ActiveRow.ListObject
                };
            AddDataToGrid(saInvoice, lstSaInvoiceDetail);
        }

        private void btnSelected_Click(object sender, EventArgs e)
        {
            if (uGridInvoiceDetail.ActiveRow != null && uGridInvoiceDetail.ActiveRow.ListObject != null)
            {
                SAInvoice saInvoice = (SAInvoice)uGridInvoice.ActiveRow.ListObject;
                List<SAInvoiceDetail> lstSaInvoiceDetail = new List<SAInvoiceDetail>
                {
                    (SAInvoiceDetail) uGridInvoiceDetail.ActiveRow.ListObject
                };
                AddDataToGrid(saInvoice, lstSaInvoiceDetail);
                CheckBtnUnSelected();
            }
        }

        private void btnSelectedAll_Click(object sender, EventArgs e)
        {
            SAInvoice saInvoice = (SAInvoice)uGridInvoice.ActiveRow.ListObject;
            List<SAInvoiceDetail> lstSaInvoiceDetail = (List<SAInvoiceDetail>)uGridInvoiceDetail.DataSource;
            AddDataToGrid(saInvoice, lstSaInvoiceDetail);
            CheckBtnUnSelected();
        }

        private void btnUnSelected_Click(object sender, EventArgs e)
        {
            if (uGridListed.ActiveRow != null && uGridListed.ActiveRow.ListObject != null)
            {
                SAReturnDetail saReturnDetail = (SAReturnDetail)uGridListed.ActiveRow.ListObject;
                RemoveDataFromGrid(new List<SAReturnDetail> { saReturnDetail });
                CheckBtnUnSelected();
            }
        }

        private void btnUnSelectedAll_Click(object sender, EventArgs e)
        {
            RemoveDataFromGrid(_dsSAReturnDetail);
            CheckBtnUnSelected();
        }
        #endregion

    }

    public class FSelectSAVouchersStand : DetailBase<SAInvoice> { }
}
