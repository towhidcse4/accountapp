﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class FSAReturnDetail : FaAReturnDetailStand
    {
        UltraGrid uGrid0;
        UltraGrid uGrid1;
        UltraGrid uGrid2;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private readonly ICurrencyService _IcurrentService;
        BindingList<SAReturnDetail> _bdlSAReturnDetail = new BindingList<SAReturnDetail>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        private bool __isFirstKho = true;
        private bool _isFirst = true;
        public FSAReturnDetail(SAReturn temp, List<SAReturn> listObject, int statusForm)
        {
            InitializeComponent();
            base.InitializeComponent1();
            _IcurrentService = IoC.Resolve<ICurrencyService>();
            #region Thiết lập ban đầu
            temp.TypeID = temp.TypeID == 0 ? 330 : temp.TypeID;
            _typeID = temp.TypeID; _statusForm = statusForm;
            oldTypeID = temp.TypeID;
            if (statusForm == ConstFrm.optStatusForm.Add) _select.TypeID = _typeID; else _select = temp;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            if (statusForm == ConstFrm.optStatusForm.Add && Utils.ListSystemOption.FirstOrDefault(x => x.ID == 122).Data == "1") cbBill.Enabled = false;
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.Height += 50;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
            #endregion
        }
        #region override
        public override void SetLaiUgridControl(SAReturn input)
        {
            SetUgridControlVeBanDau();
        }
        public override void InitializeGUI(SAReturn input)
        {
            Dictionary<string, string> lstdatasourceUcomboThanhToan = new Dictionary<string, string>();//trungnq thêm để làm task 6235, thêm combobox hình thức thanh toán
            lstdatasourceUcomboThanhToan.Add("0", "Tiền mặt");
            lstdatasourceUcomboThanhToan.Add("1", "Chuyển Khoản");
            lstdatasourceUcomboThanhToan.Add("2", "TM/CK");
            uComboThanhToan.DataSource = lstdatasourceUcomboThanhToan;
            Utils.ConfigGrid(uComboThanhToan, ConstDatabase.TMIndustryTypeGH_TableName);
            uComboThanhToan.DisplayMember = "Value";
            uComboThanhToan.ValueMember = "Key";//trungnq
            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            //ultraTabControlGG.Dock = uGroupBoxEx.Dock = DockStyle.Fill;
            ultraTabControlGG.Visible = _select.TypeID == 340;
            optHoaDon.Visible = _select.TypeID == 340;
            cbBill.Visible = _select.TypeID == 340;
            btnSelectBill.Visible = _select.TypeID == 340;
            if (_select.TypeID == 330) optPhieuXuatKho.Visible = true;
            uGroupBoxEx.Visible = palTopSctNhtNctPXK.Visible = cbbCachNhapDonGia.Visible = lblCachNhapDG.Visible = _select.TypeID == 330;
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();

            if (_select.TypeID == 330)
            {
                if (_statusForm != ConstFrm.optStatusForm.Add)
                {
                    optPhieuXuatKho.CheckedIndex = (_select == null || !(_select.IsDeliveryVoucher ?? false)) ? 1 : 0;
                }
                else
                {
                    if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 50).Data == "1")
                        optPhieuXuatKho.CheckedIndex = 1;
                    else optPhieuXuatKho.CheckedIndex = 0;
                }
            }
            //config số đơn hàng, ngày đơn hàng
            this.ConfigTopVouchersNo<SAReturn>(palTopSctNhtNctHoaDon, "No", "PostedDate", "Date");
            if (_select.TypeID.Equals(330) && _statusForm == ConstFrm.optStatusForm.Edit && _select.ID == Guid.Empty) this.ConfigTopVouchersNo<SAReturn>(palTopSctNhtNctPXK, "IWNo", "IWPostedDate", "IWDate", ConstFrm.TypeGroup_RSInwardOutward);
            if (_select.TypeID.Equals(330) && _statusForm == ConstFrm.optStatusForm.View) this.ConfigTopVouchersNo<SAReturn>(palTopSctNhtNctPXK, "IWNo", "IWPostedDate", "IWDate", ConstFrm.TypeGroup_RSInwardOutward);
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            //Config Grid
            _bdlSAReturnDetail = new BindingList<SAReturnDetail>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<SAReturnDetail>() : input.SAReturnDetails);
            _listObjectInput.Add(_bdlSAReturnDetail);
            _listObjectInput.Add(new BindingList<SAReturnDetailCustomer>(_statusForm == ConstFrm.optStatusForm.Add ? new BindingList<SAReturnDetailCustomer>() : input.SAReturnDetailCustomers));
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<SAReturn>(pnlUgrid, mauGiaoDien);
            if (mauGiaoDien.TemplateName == "Mẫu thuế")
            {
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, false };
                this.ConfigGridByManyTemplete<SAReturn>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            }
            else
            {
                List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInput, _listObjectInput, _listObjectInputPost };
                List<Boolean> manyStandard = new List<Boolean>() { true, true, true, false };
                this.ConfigGridByManyTemplete<SAReturn>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
                ugrid2 = Controls.Find("uGrid3", true).FirstOrDefault() as UltraGrid;
                ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
                ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            }
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            List<SAReturn> inputCurrency = new List<SAReturn> { input };
            this.ConfigGridCurrencyByTemplate<SAReturn>(input.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            //ConfigGridBase(input, uGridControl, pnlUgrid);
            uGrid0 = (UltraGrid)pnlUgrid.Controls.Find("uGrid0", true).FirstOrDefault();
            uGrid1 = (UltraGrid)pnlUgrid.Controls.Find("uGrid1", true).FirstOrDefault();
            uGrid2 = (UltraGrid)pnlUgrid.Controls.Find("uGrid2", true).FirstOrDefault();


            //Config Combo + DataBinding
            if (_select.TypeID.Equals(340))
            {
                Utils.isSAInvoice = true;
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDPXK, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
                this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDPXKHD, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount", accountingObjectType: 0);
                //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDPXK, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
                //this.ConfigCombo(Utils.ListAccountingObjectBank, cbbAccountingObjectBankAccount, "BankAccount", "BankAccount", input, "AccountingObjectBankAccount");
                DataBinding(new List<Control> { txtAccountingObjectNamePXK, txtAccountingObjectAddressPXK, txtCompanyTaxCodePXK, txtDienGiai, txtOriginalNoPXK }, "AccountingObjectName,AccountingObjectAddress,ContactName,Reason,OriginalNo");
                //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDPXKHD, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
                DataBinding(new List<Control> { txtAccountingObjectNamePXKHD, txtAccountingObjectAddressPXKHD, txtCompanyTaxCodePXKHD, ultraTextEditor1HD, txtOriginalNoPXKHD }, "AccountingObjectName,AccountingObjectAddress,ContactName,Reason,OriginalNo");
                DataBinding(new List<Control> { txtAccountingBankName }, "AccountingObjectBankName");
            }
            else
            {
                Utils.isSAInvoice = true;
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectID, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectXK, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
                DataBinding(new List<Control> { txtAccountingObjectName, txtAccountingObjectAddress, txtContractName, txtReasonReturn, txtOrginal }, "AccountingObjectName,AccountingObjectAddress,ContactName,Reason,OriginalNo");
                DataBinding(new List<Control> { txtAccountingObjectNameXK, txtAccountingObjectAddressXK, txtCompanyTaxCodeXK, txtSReason, txtOriginalNo }, "AccountingObjectName,AccountingObjectAddress,IWContactName,IWReason,IWNumberAttach");
                this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectCode", "ID", input, "AccountingObjectID", accountingObjectType: 0);
                DataBinding(new List<Control> { txtAccountingObjectNameHD, txtAccountingObjectAddressHD, txtContractNameHD, txtReasonReturnHD, txtOrginalHD }, "AccountingObjectName,AccountingObjectAddress,ContactName,Reason,OriginalNo");
            }

            DataBinding(new List<Control>
                           {
                               txtTotalAmount,
                               txtTotalVATAmount,
                               txtTotalDiscountAmountSub,
                               txtTotalPaymentAmountStand
                           }, "TotalAmount, TotalVATAmount, TotalDiscountAmount, TotalPaymentAmount", "3,3,3,3");

            DataBinding(new List<Control>
                           {
                               txtTotalAmountOriginal,
                               txtTotalVATAmountOriginal,
                               txtTotalDiscountAmountSubOriginal,
                               txtTotalPaymentAmountOriginalStand
                           }, "TotalAmountOriginal, TotalVATAmountOriginal, TotalDiscountAmountOriginal, TotalPaymentAmountOriginal", "3,3,3,3", true);


            if (_statusForm == ConstFrm.optStatusForm.Add && _select.TypeID == 330)
            {
                cbbCachNhapDonGia.SelectedIndex = 0;
            }
            else if (_select.TypeID == 330)
            {
                cbbCachNhapDonGia.SelectedIndex = _select.AutoOWAmountCal ?? 0;
            }
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                if (input.PaymentMethod != null && !string.IsNullOrEmpty(input.PaymentMethod) && _select.TypeID == 340) uComboThanhToan.Text = input.PaymentMethod;
            }
            else
            {
                if (_select.TypeID == 340)
                    uComboThanhToan.Text = "TM/CK";//add by cuongpv
            }
            btnSelectBill.Enabled = cbBill.CheckState == CheckState.Checked;
            btnSelectBill.Tag = new Dictionary<string, object> { { "Enabled", cbBill.CheckState == CheckState.Checked } };
            DataBinding(new List<Control> { cbBill }, "IsAttachListBill", "1");
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                optHoaDon.CheckedIndex = input.IsBill ? 0 : 1;
                //optHoaDon.Enabled = false;
                optHoaDon.ReadOnly = true;//trungnq sửa enabled thành readonly để sửa bug 6411 
            }
            else
            {
                if (Utils.ListSystemOption.FirstOrDefault(x => x.ID == 122).Data == "1")
                    optHoaDon.CheckedIndex = 0;
                else optHoaDon.CheckedIndex = 1;
            }
            if (input != null && input.BillRefID != null)
            {
                var HD = Utils.ListSABill.FirstOrDefault(x => x.ID == input.BillRefID);
                txtOldInvNo.Text = HD.InvoiceNo;
                txtOldInvDate.Text = HD.InvoiceDate == null ? "" : (HD.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                txtOldInvTemplate.Text = HD.InvoiceTemplate;
                txtOldInvSeries.Text = HD.InvoiceSeries;
            }
            else
            {
                panel1.Visible = false;
            }
            if (_select.TypeID == 330)
            {
                uGroupBoxEx.Tabs["tabHoaDon1"].Visible = false;
            }
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                if (input.TypeID == 330)//trungnq thêm vào để  làm task 6234
                {
                    txtReasonReturn.Value = "Hàng bán trả lại";
                    txtReasonReturn.Text = "Hàng bán trả lại";
                    _select.Reason = "Hàng bán trả lại";
                    txtSReason.Value = "Nhập kho từ hàng bán trả lại";
                    txtSReason.Text = "Nhập kho từ hàng bán trả lại";
                    _select.IWReason = "Nhập kho từ hàng bán trả lại";


                }
                else if (input.TypeID == 340)//trungnq thêm vào để  làm task 6234
                {
                    txtDienGiai.Value = "Giảm giá hàng bán";
                    txtDienGiai.Text = "Giảm giá hàng bán";
                    _select.Reason = "Giảm giá hàng bán";
                }
            }
            if (_statusForm == ConstFrm.optStatusForm.Add || _statusForm == ConstFrm.optStatusForm.Edit || _statusForm == ConstFrm.optStatusForm.View)
            {
                if (input.TypeID == 330)//trungnq thêm vào để  làm task 6240
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn hàng bán trả lại
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.AllowEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].CellActivation = Activation.NoEdit;
                        uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].CellActivation = Activation.NoEdit;
                    }
                }
            }
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
        }
        #endregion

        #region Event
        private void cbbCachNhapDonGia_ValueChanged(object sender, EventArgs e)
        {
            if (_select.TypeID == 340) return;
            switch (cbbCachNhapDonGia.SelectedIndex)
            {
                case 0:
                    SetLockEditColumn(true);
                    _select.AutoOWAmountCal = 0;
                    break;
                case 1:
                    SetLockEditColumn(false);
                    _select.AutoOWAmountCal = 1;
                    break;
            }
        }

        private void SetLockEditColumn(bool isLock)
        {
            //var uGridPosted = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
            //if (uGridPosted == null) return;
            //if (!uGridPosted.DisplayLayout.Bands[0].Columns.Exists("UnitPrice") ||
            //    !uGridPosted.DisplayLayout.Bands[0].Columns.Exists("UnitPriceOriginal") ) return;
            //uGridPosted.DisplayLayout.Bands[0].Columns["UnitPrice"].LockColumn(isLock);
            //uGridPosted.DisplayLayout.Bands[0].Columns["UnitPriceOriginal"].LockColumn(isLock);
            var uGrid1 = (UltraGrid)Controls.Find("uGrid1", true).FirstOrDefault();
            uGrid1.DisplayLayout.Bands[0].Columns["OWAmount"].LockColumn(isLock);
            uGrid1.DisplayLayout.Bands[0].Columns["OWPrice"].LockColumn(isLock);
        }

        private void uTabControlReturn_ActiveTabChanged(object sender, Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventArgs e)
        {
            palTopSctNhtNctHoaDon.Visible = false;
            palTopSctNhtNctPXK.Visible = false;
            switch (e.Tab.Key)
            {
                case "tabHoaDon":
                    palTopSctNhtNctHoaDon.Visible = true;
                    break;
                case "tabPhieuXuatKho":
                    palTopSctNhtNctPXK.Visible = true;
                    break;
            }
            bool value = (e.Tab.Key.Equals("tabHoaDon1"));
            if (uGridControl != null && _typeID == 340)
            {
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = !value;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = !value;
                uGridControl.ConfigSizeGridCurrency(this, false);
            }
        }

        private void btnVouchersDiscount_Click(object sender, EventArgs e)
        {
            try
            {
                using (var frm = new FSelectSAVouchers(_select.AccountingObjectID, _select.TypeID))
                {
                    frm.FormClosed += FSelectSAVouchers_FormClosed;
                    frm.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        void FSelectSAVouchers_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FSelectSAVouchers)sender;
            if (e.CloseReason == CloseReason.UserClosing)
                if (frm.DialogResult == DialogResult.OK)
                {
                    List<SAReturnDetail> temp = frm.SaReturnDetailSelected;
                    _select.SAReturnDetails = temp;
                    if (temp != null && temp.Count > 0)
                    {
                        _bdlSAReturnDetail.Clear();
                        foreach (SAReturnDetail item in temp)
                        {
                            _bdlSAReturnDetail.Add(item.CloneObject());
                            _select.CurrencyID = item.SAInvoice.CurrencyID;
                            _select.ExchangeRate = item.SAInvoice.ExchangeRate;
                        }
                        var tabControl = Controls.Find("ultraTabControl", true).FirstOrDefault() as UltraTabControl;
                        if (tabControl != null)
                        {
                            foreach (var tab in tabControl.Tabs)
                            {
                                var uGrid = Controls.Find(string.Format("uGrid{0}", tab.Index), true).FirstOrDefault() as UltraGrid;
                                if (uGrid != null)
                                {
                                    uGrid.DataBind();
                                    if (tab.Index == 0)
                                    {
                                        foreach (var row in uGrid.Rows)
                                        {
                                            uGrid.ActiveCell = row.Cells["UnitPriceOriginal"];
                                            this.DoEverythingInGrid<SAReturn>(uGrid);
                                            var model = Utils.ListMaterialGoodTools.FirstOrDefault(x => x.ID == (Guid)row.Cells["MaterialGoodsID"].Value);
                                            Utils.uGrid1_AfterCellUpdate<SAReturn>(this, uGrid, model, row.Index);
                                        }
                                    }
                                }
                            }
                        }
                        var uGridControl = Controls.Find("uGridControl", true).FirstOrDefault() as UltraGrid;
                        uGridControl.Rows[0].Cells["CurrencyID"].Value = _select.CurrencyID;
                        uGridControl.Rows[0].Cells["ExchangeRate"].Hidden = false;
                        uGridControl.Rows[0].Cells["ExchangeRate"].Value = _select.ExchangeRate;
                        AccountingObject _accTemp = Utils.ListAccountingObject.FirstOrDefault(p => p.ID == temp[0].AccountingObjectID);
                        int index = Utils.ListAccountingObject.IndexOf(_accTemp);
                        if (_select.TypeID == 330)
                        {
                            //cbbAccountingObjectID.SelectedRow = cbbAccountingObjectID.Rows[index];
                            //AccountingObject item = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectID);
                            //txtAccountingObjectName.Text = item.AccountingObjectName;
                            //txtAccountingObjectAddress.Text = item.Address;
                            //txtContractName.Text = item.ContactName;

                            //Cấu hình tab chứng từ
                            this.ActiveControl = cbbAccountingObjectID;
                            cbbAccountingObjectID.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectID.SelectedRow = cbbAccountingObjectID.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectID, true);
                            //Cấu hình tab xuất kho
                            this.ActiveControl = cbbAccountingObjectXK;
                            cbbAccountingObjectXK.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectXK.SelectedRow = cbbAccountingObjectXK.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectXK, true);
                            //Cấu hình tab hoá đơn
                            this.ActiveControl = cbbAccountingObjectIDHD;
                            cbbAccountingObjectIDHD.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectIDHD.SelectedRow = cbbAccountingObjectIDHD.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectIDHD, true);
                        }
                        else
                        {
                            //cbbAccountingObjectIDPXK.SelectedRow = cbbAccountingObjectIDPXK.Rows[index];
                            //AccountingObject item = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectIDPXK);
                            //txtAccountingObjectNamePXK.Text = item.AccountingObjectName;
                            //txtAccountingObjectAddressPXK.Text = item.Address;
                            //txtCompanyTaxCodePXK.Text = item.ContactName;
                            //Cấu hình tab chứng từ
                            this.ActiveControl = cbbAccountingObjectIDPXK;
                            cbbAccountingObjectIDPXK.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectIDPXK.SelectedRow = cbbAccountingObjectIDPXK.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectIDPXK, true);
                            //Cấu hình tab hoá đơn
                            this.ActiveControl = cbbAccountingObjectIDPXKHD;
                            cbbAccountingObjectIDPXKHD.DataSource = new BindingList<AccountingObject>(new List<AccountingObject> { _accTemp });
                            cbbAccountingObjectIDPXKHD.SelectedRow = cbbAccountingObjectIDPXKHD.Rows[0];
                            _accTemp.ExcuteRowSelectedEventOfcbbAutoFilter(this, cbbAccountingObjectIDPXKHD, true);
                        }
                        //cbbAccountingObjectXK.SelectedRow = cbbAccountingObjectXK.Rows[index];
                    }
                }

        }

        private void txtBottom_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            if (!string.IsNullOrEmpty(txt.Text))
            {
                Utils.FormatNumberic(txt, txt.Text, ConstDatabase.Format_TienVND);
            }
        }

        private void txtOriginalBottom_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            if (!string.IsNullOrEmpty(txt.Text))
                Utils.FormatNumberic(txt, txt.Text, ConstDatabase.Format_TienVND);
        }
        #endregion

        private void OptPhieuXuatKhoValueChangedStand(object sender, EventArgs e)
        {//opt Phiếu xuất kho value change
            if (optPhieuXuatKho.CheckedIndex == 0)
            {
                bool isreset = (__isFirstKho && _statusForm == ConstFrm.optStatusForm.Add && optPhieuXuatKho.CheckedIndex == 0);
                if (_statusForm == ConstFrm.optStatusForm.Edit && _select.IsDeliveryVoucher == false && optPhieuXuatKho.CheckedIndex == 0)
                { isreset = true; }
                this.ConfigTopVouchersNo<SAReturn>(palTopSctNhtNctPXK, "IWNo", "IWPostedDate", "IWDate", ConstFrm.TypeGroup_RSInwardOutward, resetNo: isreset);
                __isFirstKho = false;
                _backSelect.IWNo = _select.IWNo.CloneObject();
            }
            UltraOptionSet optionSet;
            try { optionSet = (UltraOptionSet)sender; if (optionSet == null || _select == null) return; }
            catch { return; }
            if (optionSet.CheckedItem.Tag.Equals("KKPXK"))
            {
                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {
                    List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("40") && n.No == _select.IWNo).ToList();
                    List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.ID && n.TypeID.ToString().StartsWith("40") && n.No == _select.IWNo).ToList();
                    if ((listrefVouchersRSInwardOutward.Count != 0) || (listrefVouchers.Count != 0))
                    {
                        if (MSG.Question("Chứng từ này đã phát sinh liên kết với chứng từ khác. Thực hiện sửa đổi chứng từ sẽ phải xóa bỏ toàn bộ liên kết. Bạn có muốn tiếp tục ?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            _select.IWNo = null;
                            _select.IsDeliveryVoucher = false;

                        }
                        else
                        {
                            optionSet.CheckedIndex = 0;
                        }
                    }
                    else
                    {
                        _select.IWNo = null;
                        _select.IsDeliveryVoucher = false;
                    }
                }
                else
                {
                    _select.IWNo = null;
                    _select.IsDeliveryVoucher = false;
                }
            }
            else
            {
                if (!_isFirst && _select.TypeID == _backSelect.TypeID)
                {
                    _select.IWNo = _backSelect.IWNo.CloneObject();
                    _select.IsDeliveryVoucher = true;
                    var topvoucher = palTopSctNhtNctPXK.GetTopVouchers<SAReturn>();
                    if (topvoucher != null)
                    {
                        topvoucher.No = _select.IWNo;
                    }
                }
                else
                {
                    if (_isFirst && _statusForm != ConstFrm.optStatusForm.Add)
                    { }
                    else
                    {
                        this.ConfigTopVouchersNo<SAReturn>(palTopSctNhtNctPXK, "IWNo", "IWPostedDate", "IWDate", ConstFrm.TypeGroup_RSInwardOutward, resetNo: false);
                        _select.IsDeliveryVoucher = true;
                    }
                    if (_statusForm == ConstFrm.optStatusForm.Edit)
                    {
                        this.ConfigTopVouchersNo<SAReturn>(palTopSctNhtNctPXK, "IWNo", "IWPostedDate", "IWDate", ConstFrm.TypeGroup_RSInwardOutward, resetNo: false);
                        _select.IsDeliveryVoucher = true;
                    }

                }
            }
            uGroupBoxEx.Tabs["tabPhieuXuatKho"].Visible = (_select.IWNo != null && !string.IsNullOrEmpty(_select.IWNo));
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<SAReturn>(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.Exported);
                }
            }

        }
        private void txtContractName_Validating(object sender, CancelEventArgs e)
        {
            txtCompanyTaxCodeXK.Value = txtContractName.Value;
        }

        private void txtReasonReturn_Validated(object sender, EventArgs e)
        {
            txtSReason.Value = txtReasonReturn.Value;
        }

        private void txtOrginal_Validated(object sender, EventArgs e)
        {
            txtOriginalNo.Value = txtOrginal.Value;
        }

        private void txtAccountingObjectAddressHD_Validated(object sender, EventArgs e)
        {
            txtAccountingObjectAddressXK.Value = txtAccountingObjectAddress.Value;
        }

        private void ultraTabControlGG_ActiveTabChanged(object sender, ActiveTabChangedEventArgs e)
        {
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
            uGridControl.ConfigSizeGridCurrency(this, false);
            palTopSctNhtNctHoaDon.Visible = false;
            switch (e.Tab.Key)
            {
                case "tabHoaDon":
                    palTopSctNhtNctHoaDon.Visible = true;
                    break;
            }
            bool value = (e.Tab.Key.Equals("tabHoaDon1"));
            if (uGridControl != null && _typeID == 340)
            {
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = !value;
                //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = !value;
                //uGridControl.ConfigSizeGridCurrency(this, false);
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                txtMauSoHDI.Text = _select.InvoiceTemplate != null ? _select.InvoiceTemplate : txtMauSoHDI.Text;
                txtKyHieuHDI.Text = _select.InvoiceSeries != null ? _select.InvoiceSeries : txtKyHieuHDI.Text;
                txtSoHDI.Text = _select.InvoiceNo != null ? _select.InvoiceNo : txtSoHDI.Text;
                dteNgayHDI.Value = _select.InvoiceDate != null ? _select.InvoiceDate : dteNgayHDI.Value;
            }
        }
        private void SetUgridControlVeBanDau()
        {
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
            uGridControl.ConfigSizeGridCurrency(this, true);
        }
        private void txtThanhToanGG_TextChanged(object sender, EventArgs e)
        {
            _select.PaymentMethod = uComboThanhToan.Text;
        }

        private void optHoaDon_ValueChanged(object sender, EventArgs e)
        {
            if (optHoaDon.CheckedIndex == 1)
            {
                _select.InvoiceForm = null;
                _select.InvoiceTypeID = null;
                _select.InvoiceTemplate = null;
                _select.InvoiceSeries = null;
                _select.InvoiceNo = null;
                _select.InvoiceDate = null;
            }
            ultraTabControlGG.Tabs["tabHoaDon1"].Visible = optHoaDon.CheckedIndex == 0;
            cbBill.Enabled = optHoaDon.CheckedIndex == 1;
            if (uGridControl != null && optHoaDon.CheckedIndex == 1 && _typeID == 340)
            {
                if (_statusForm == ConstFrm.optStatusForm.Edit)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1" && !_select.InvoiceNo.IsNullOrEmpty() && _select.StatusInvoice == 1)
                    {
                        MSG.Warning("Không thể thực hiện thao tác này! Hệ thống đang được tích hợp hóa đơn điện tử và hóa đơn này đã được phát hành");
                        optHoaDon.CheckedIndex = 0;
                    }
                    else
                    {
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                        uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
                        uGridControl.ConfigSizeGridCurrency(this, false);
                        _select.InvoiceForm = null;
                        _select.InvoiceTypeID = null;
                        _select.InvoiceTemplate = null;
                        _select.InvoiceSeries = null;
                        _select.InvoiceNo = null;
                        _select.InvoiceDate = null;
                    }
                }
            }
            else
            {
                if (_statusForm == ConstFrm.optStatusForm.Add)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
                    {
                        if (_select.InvoiceForm == null)
                        {
                            _select.InvoiceForm = 2;
                            UltraCombo ultraComboInvoiceTypeID = uGridControl.Rows[0].Cells["InvoiceTypeID"].ValueListResolved as UltraCombo;
                            if (ultraComboInvoiceTypeID != null && ultraComboInvoiceTypeID.DataSource != null)
                            {
                                List<InvoiceType> lst = ultraComboInvoiceTypeID.DataSource as List<InvoiceType>;
                                if (lst.Count == 1)
                                {
                                    _select.InvoiceTypeID = lst.First().ID;
                                    UltraCombo ultraComboInvoiceTemplate = uGridControl.Rows[0].Cells["InvoiceTemplate"].ValueList as UltraCombo;
                                    if (ultraComboInvoiceTemplate != null && ultraComboInvoiceTemplate.DataSource != null)
                                    {
                                        List<TT153Report> tT153Reports = ultraComboInvoiceTemplate.DataSource as List<TT153Report>;
                                        if (tT153Reports.Count == 1)
                                        {
                                            _select.InvoiceTemplate = tT153Reports.First().InvoiceTemplate;
                                            _select.InvoiceSeries = tT153Reports.First().InvoiceSeries;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _select.InvoiceTypeID = null;
                                _select.InvoiceTemplate = null;
                                _select.InvoiceSeries = null;
                                _select.InvoiceNo = null;
                                _select.InvoiceDate = null;
                            }
                        }
                    }
                }
            }
            if (_select.TypeID == 340)
            {
                _select.IsBill = optHoaDon.CheckedIndex == 0;
            }
            if (optHoaDon.Visible != false)
            {
                _select.IsBill = optHoaDon.CheckedIndex == 0;
            }
            UltraGrid ultraGrid = (UltraGrid)this.Controls.Find("uGrid1", true).FirstOrDefault();
            if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("DeductionDebitAccount"))//trungnq thêm để làm bug 6453
            {
                if (ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"].Hidden == false)
                {
                    Utils.ConfigAccountColumnInGrid<SAReturn
                        >(this, _select.TypeID, ultraGrid, ultraGrid.DisplayLayout.Bands[0].Columns["DeductionDebitAccount"], isImportPurchase: _select.Exported);
                }
            }
        }

        private void cbBill_CheckStateChanged(object sender, EventArgs e)
        {
            if (_statusForm != ConstFrm.optStatusForm.View)
            {
                btnSelectBill.Enabled = cbBill.CheckState == CheckState.Checked;
                optHoaDon.Enabled = cbBill.CheckState != CheckState.Checked;
            }
            btnSelectBill.Tag = new Dictionary<string, object> { { "Enabled", cbBill.CheckState == CheckState.Checked } };
        }

        private void btnSelectBill_Click(object sender, EventArgs e)
        {
            var f = new FchoseBillR<SAReturn>(_listObjectInput, _select, _select.AccountingObjectID);
            f.FormClosed += new FormClosedEventHandler(FchoseBill_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void FchoseBill_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FchoseBillR<SAReturn>)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectSAs = frm._lstSA;
                if (uGrid0 != null)
                {
                    var source = (BindingList<SAReturnDetail>)uGrid0.DataSource;
                    if (selectSAs != null) source.Clear();
                    _select.BillRefID = selectSAs.ID;
                    _select.IsAttachListBill = true;
                    foreach (var selectOrder in selectSAs.SABillDetails)
                    {
                        uGrid0.AddNewRow4Grid();
                        var row = uGrid0.Rows[uGrid0.Rows.Count - 1];
                        row.Cells["MaterialGoodsID"].Value = selectOrder.MaterialGoodsID;
                        row.UpdateData();
                        uGrid0.UpdateData();
                        uGrid1.UpdateData();
                        row.Cells["Description"].Value = selectOrder.Description;
                        row.Cells["Quantity"].Value = selectOrder.Quantity;
                        row.Cells["UnitPrice"].Value = selectOrder.UnitPrice;
                        row.Cells["UnitPriceOriginal"].Value = selectOrder.UnitPriceOriginal;
                        row.Cells["Amount"].Value = selectOrder.Amount;
                        row.Cells["AmountOriginal"].Value = selectOrder.AmountOriginal;
                        row.Cells["AccountingObjectID"].Value = selectOrder.AccountingObjectID;
                        row.Cells["VATRate"].Value = selectOrder.VATRate;
                        row.Cells["VATAmount"].Value = selectOrder.VATAmount;
                        row.Cells["VATAmountOriginal"].Value = selectOrder.VATAmountOriginal;
                        row.Cells["DiscountRate"].Value = selectOrder.DiscountRate;
                        row.Cells["DiscountAmount"].Value = selectOrder.DiscountAmount;
                        row.Cells["DiscountAmountOriginal"].Value = selectOrder.DiscountAmountOriginal;
                        row.Cells["LotNo"].Value = selectOrder.LotNo;
                        row.Cells["ExpiryDate"].Value = selectOrder.ExpiryDate;
                        row.UpdateData();
                        uGrid0.ActiveCell = row.Cells["Quantity"];
                        this.DoEverythingInGrid<SAReturn>(uGrid0);
                    }
                }
                if (uGrid0.Rows.Count > 0)
                {
                    foreach (var row in uGrid0.Rows)
                    {
                        foreach (var cell in row.Cells)
                            Utils.CheckErrorInCell<SAReturn>(this, uGrid0, cell);
                    }
                    foreach (var row in uGrid1.Rows)
                    {
                        Utils.CheckErrorInCell<SAReturn>(this, uGrid1, row.Cells["MaterialGoodsID"]);
                    }
                    foreach (var row in uGrid2.Rows)
                    {
                        Utils.CheckErrorInCell<SAReturn>(this, uGrid2, row.Cells["MaterialGoodsID"]);
                    }
                }
            }
        }

        private void btnVouchersReturn_Click(object sender, EventArgs e)
        {
            try
            {
                using (var frm = new FSelectSAVouchers(_select.AccountingObjectID))
                {
                    frm.FormClosed += FSelectSAVouchers_FormClosed;
                    frm.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void txtAccountingObjectName_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;

            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID,
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FSAReturnDetail_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void FSAReturnDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void uGroupBoxEx_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            SAReturn sar = new SAReturn();
            sar = _select;
            if ((e.Tab.Key == "tabHoaDon" || e.Tab.Key == "tabPhieuXuatKho") || _typeID == 330)
            {
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].CellActivation = Activation.NoEdit;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].CellActivation = Activation.NoEdit;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].CellActivation = Activation.NoEdit;

                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
                uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;

                txtMauSoHD.Text = sar.InvoiceTemplate != null ? sar.InvoiceTemplate : txtMauSoHD.Text;
                txtKyHieuHD.Text = sar.InvoiceSeries != null ? sar.InvoiceSeries : txtKyHieuHD.Text;
                txtSoHD.Text = sar.InvoiceNo != null ? sar.InvoiceNo : txtSoHD.Text;
                dteNgayHD.Value = sar.InvoiceDate != null ? sar.InvoiceDate : dteNgayHD.Value;

                txtMauSoHDPN.Text = sar.InvoiceTemplate != null ? sar.InvoiceTemplate : txtMauSoHDPN.Text;
                txtKyHieuHDPN.Text = sar.InvoiceSeries != null ? sar.InvoiceSeries : txtKyHieuHDPN.Text;
                txtSoHDPN.Text = sar.InvoiceNo != null ? sar.InvoiceNo : txtSoHDPN.Text;
                dteNgayHDPN.Value = sar.InvoiceDate != null ? sar.InvoiceDate : dteNgayHDPN.Value;
            }
        }
        protected override bool AdditionalFunc()
        {
            if (_typeID == 330)
            {
                if (txtKyHieuHD.Text != "" || txtMauSoHD.Text != "" || txtSoHD.Text != "" || dteNgayHD.Value != null)
                {
                    if (txtKyHieuHD.Text != "" && txtMauSoHD.Text != "" && txtSoHD.Text != "" && dteNgayHD.Value != null)
                    {
                        if (txtSoHD.Text.Length == 7)
                        {
                            _select.InvoiceTemplate = txtMauSoHD.Text;
                            _select.InvoiceSeries = txtKyHieuHD.Text;
                            _select.InvoiceNo = txtSoHD.Text;
                            _select.InvoiceDate = Convert.ToDateTime(dteNgayHD.Value.ToString());
                        }
                        else
                        {
                            MSG.Warning("Số hoá đơn phải là kiểu số và đúng 7 chữ số.");
                            return false;
                        }
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    _select.InvoiceTemplate = "";
                    _select.InvoiceSeries = "";
                    _select.InvoiceNo = "";
                    _select.InvoiceDate = null;
                }
            }
            if (_typeID == 340)
            {
                if (txtKyHieuHDI.Text != "" || txtMauSoHDI.Text != "" || txtSoHDI.Text != "" || dteNgayHDI.Value != null)
                {
                    if (txtKyHieuHDI.Text != "" && txtMauSoHDI.Text != "" && txtSoHDI.Text != "" && dteNgayHDI.Value != null)
                    {
                        _select.InvoiceTemplate = txtMauSoHDI.Text;
                        _select.InvoiceSeries = txtKyHieuHDI.Text;
                        _select.InvoiceNo = txtSoHDI.Text;
                        _select.InvoiceDate = Convert.ToDateTime(dteNgayHDI.Value.ToString());
                    }
                    else
                    {
                        MSG.Warning("Chưa nhập đủ thông tin hóa đơn ,vui lòng kiểm tra lại");
                        return false;
                    }
                }
                else
                {
                    _select.InvoiceTemplate = "";
                    _select.InvoiceSeries = "";
                    _select.InvoiceNo = "";
                    _select.InvoiceDate = null;
                }
            }
            return true;
        }

        private void txtMauSoHD_TextChanged(object sender, EventArgs e)
        {
            var txt = (UltraTextEditor)sender;
            txtMauSoHD.Text = txt.Text;
            txtMauSoHDPN.Text = txt.Text;
        }

        private void txtKyHieuHD_TextChanged(object sender, EventArgs e)
        {
            var txt = (UltraTextEditor)sender;
            txtKyHieuHD.Text = txt.Text;
            txtKyHieuHDPN.Text = txt.Text;
        }

        private void txtSoHD_TextChanged(object sender, EventArgs e)
        {
            var txt = (UltraTextEditor)sender;
            txtSoHD.Text = txt.Text;
            txtSoHDPN.Text = txt.Text;
        }

        private void dteNgayHD_ValueChanged(object sender, EventArgs e)
        {
            var txt = (UltraDateTimeEditor)sender;
            dteNgayHD.Value = txt.Value;
            dteNgayHDPN.Value = txt.Value;
        }
    }

    public class FaAReturnDetailStand : DetailBase<SAReturn> { }
}
