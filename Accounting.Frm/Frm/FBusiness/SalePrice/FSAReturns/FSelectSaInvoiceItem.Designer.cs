﻿namespace Accounting.Frm
{
    partial class FSelectSaInvoiceItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.palTop = new System.Windows.Forms.Panel();
            this.lblSaInvoice = new Infragistics.Win.Misc.UltraLabel();
            this.lblKhachHang = new Infragistics.Win.Misc.UltraLabel();
            this.cbbSaInvoice = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.palFill = new System.Windows.Forms.Panel();
            this.palCenter = new System.Windows.Forms.Panel();
            this.palLeft = new System.Windows.Forms.Panel();
            this.UgridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridSaInvoiceDetails = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palRight = new System.Windows.Forms.Panel();
            this.uGridSaInvoiceReturn = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.palBottom = new System.Windows.Forms.Panel();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnSelectedAll = new Infragistics.Win.Misc.UltraButton();
            this.btnUnSelected = new Infragistics.Win.Misc.UltraButton();
            this.btnUnSelectedAll = new Infragistics.Win.Misc.UltraButton();
            this.btnSelected = new Infragistics.Win.Misc.UltraButton();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSaInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            this.palFill.SuspendLayout();
            this.palCenter.SuspendLayout();
            this.palLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UgridDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSaInvoiceDetails)).BeginInit();
            this.palRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSaInvoiceReturn)).BeginInit();
            this.palBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.lblSaInvoice);
            this.palTop.Controls.Add(this.lblKhachHang);
            this.palTop.Controls.Add(this.cbbSaInvoice);
            this.palTop.Controls.Add(this.cbbAccountingObjectID);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(844, 50);
            this.palTop.TabIndex = 0;
            // 
            // lblSaInvoice
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.lblSaInvoice.Appearance = appearance1;
            this.lblSaInvoice.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblSaInvoice.Location = new System.Drawing.Point(482, 17);
            this.lblSaInvoice.Name = "lblSaInvoice";
            this.lblSaInvoice.Size = new System.Drawing.Size(148, 19);
            this.lblSaInvoice.TabIndex = 51;
            this.lblSaInvoice.Text = "Chọn hóa đơn bán hàng";
            // 
            // lblKhachHang
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.lblKhachHang.Appearance = appearance2;
            this.lblKhachHang.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.lblKhachHang.Location = new System.Drawing.Point(12, 15);
            this.lblKhachHang.Name = "lblKhachHang";
            this.lblKhachHang.Size = new System.Drawing.Size(115, 19);
            this.lblKhachHang.TabIndex = 49;
            this.lblKhachHang.Text = "Khách hàng";
            // 
            // cbbSaInvoice
            // 
            this.cbbSaInvoice.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbSaInvoice.Location = new System.Drawing.Point(634, 15);
            this.cbbSaInvoice.Name = "cbbSaInvoice";
            this.cbbSaInvoice.NullText = "<chọn dữ liệu>";
            this.cbbSaInvoice.Size = new System.Drawing.Size(198, 22);
            this.cbbSaInvoice.TabIndex = 50;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(131, 13);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(198, 22);
            this.cbbAccountingObjectID.TabIndex = 48;
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.palCenter);
            this.palFill.Controls.Add(this.palLeft);
            this.palFill.Controls.Add(this.palRight);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 50);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(844, 391);
            this.palFill.TabIndex = 1;
            // 
            // palCenter
            // 
            this.palCenter.Controls.Add(this.btnSelectedAll);
            this.palCenter.Controls.Add(this.btnUnSelected);
            this.palCenter.Controls.Add(this.btnUnSelectedAll);
            this.palCenter.Controls.Add(this.btnSelected);
            this.palCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palCenter.Location = new System.Drawing.Point(374, 0);
            this.palCenter.Name = "palCenter";
            this.palCenter.Size = new System.Drawing.Size(53, 391);
            this.palCenter.TabIndex = 1;
            // 
            // palLeft
            // 
            this.palLeft.Controls.Add(this.UgridDetail);
            this.palLeft.Controls.Add(this.uGridSaInvoiceDetails);
            this.palLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.palLeft.Location = new System.Drawing.Point(0, 0);
            this.palLeft.Name = "palLeft";
            this.palLeft.Size = new System.Drawing.Size(374, 391);
            this.palLeft.TabIndex = 2;
            // 
            // UgridDetail
            // 
            this.UgridDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UgridDetail.Location = new System.Drawing.Point(0, 203);
            this.UgridDetail.Name = "UgridDetail";
            this.UgridDetail.Size = new System.Drawing.Size(374, 188);
            this.UgridDetail.TabIndex = 1;
            this.UgridDetail.Text = "ultraGrid1";
            // 
            // uGridSaInvoiceDetails
            // 
            this.uGridSaInvoiceDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridSaInvoiceDetails.Location = new System.Drawing.Point(0, 0);
            this.uGridSaInvoiceDetails.Name = "uGridSaInvoiceDetails";
            this.uGridSaInvoiceDetails.Size = new System.Drawing.Size(374, 203);
            this.uGridSaInvoiceDetails.TabIndex = 0;
            this.uGridSaInvoiceDetails.Text = "ultraGrid1";
            // 
            // palRight
            // 
            this.palRight.Controls.Add(this.uGridSaInvoiceReturn);
            this.palRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.palRight.Location = new System.Drawing.Point(427, 0);
            this.palRight.Name = "palRight";
            this.palRight.Size = new System.Drawing.Size(417, 391);
            this.palRight.TabIndex = 0;
            // 
            // uGridSaInvoiceReturn
            // 
            this.uGridSaInvoiceReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSaInvoiceReturn.Location = new System.Drawing.Point(0, 0);
            this.uGridSaInvoiceReturn.Name = "uGridSaInvoiceReturn";
            this.uGridSaInvoiceReturn.Size = new System.Drawing.Size(417, 391);
            this.uGridSaInvoiceReturn.TabIndex = 1;
            this.uGridSaInvoiceReturn.Text = "ultraGrid2";
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.btnClose);
            this.palBottom.Controls.Add(this.btnSave);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 441);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(844, 49);
            this.palBottom.TabIndex = 2;
            // 
            // btnClose
            // 
            appearance7.Image = global::Accounting.Frm.Properties.Resources.ubtnDelete;
            this.btnClose.Appearance = appearance7;
            this.btnClose.Location = new System.Drawing.Point(748, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 73;
            this.btnClose.Text = "Đóng";
            // 
            // btnSave
            // 
            appearance8.Image = global::Accounting.Frm.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance8;
            this.btnSave.Location = new System.Drawing.Point(661, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSelectedAll
            // 
            appearance3.FontData.BoldAsString = "True";
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.btnSelectedAll.Appearance = appearance3;
            this.btnSelectedAll.Location = new System.Drawing.Point(6, 298);
            this.btnSelectedAll.Name = "btnSelectedAll";
            this.btnSelectedAll.Size = new System.Drawing.Size(41, 36);
            this.btnSelectedAll.TabIndex = 9;
            this.btnSelectedAll.Text = ">>";
            // 
            // btnUnSelected
            // 
            appearance4.FontData.BoldAsString = "True";
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.btnUnSelected.Appearance = appearance4;
            this.btnUnSelected.Location = new System.Drawing.Point(6, 256);
            this.btnUnSelected.Name = "btnUnSelected";
            this.btnUnSelected.Size = new System.Drawing.Size(42, 36);
            this.btnUnSelected.TabIndex = 8;
            this.btnUnSelected.Text = "<";
            // 
            // btnUnSelectedAll
            // 
            appearance5.FontData.BoldAsString = "True";
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.btnUnSelectedAll.Appearance = appearance5;
            this.btnUnSelectedAll.Location = new System.Drawing.Point(6, 340);
            this.btnUnSelectedAll.Name = "btnUnSelectedAll";
            this.btnUnSelectedAll.Size = new System.Drawing.Size(41, 36);
            this.btnUnSelectedAll.TabIndex = 10;
            this.btnUnSelectedAll.Text = "<<";
            // 
            // btnSelected
            // 
            appearance6.FontData.BoldAsString = "True";
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.btnSelected.Appearance = appearance6;
            this.btnSelected.Location = new System.Drawing.Point(5, 214);
            this.btnSelected.Name = "btnSelected";
            this.btnSelected.Size = new System.Drawing.Size(42, 36);
            this.btnSelected.TabIndex = 7;
            this.btnSelected.Text = ">";
            // 
            // FSelectSaInvoiceItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 490);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.Name = "FSelectSaInvoiceItem";
            this.Text = "Chọn chứng từ bán hàng";
            this.palTop.ResumeLayout(false);
            this.palTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSaInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            this.palFill.ResumeLayout(false);
            this.palCenter.ResumeLayout(false);
            this.palLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UgridDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSaInvoiceDetails)).EndInit();
            this.palRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSaInvoiceReturn)).EndInit();
            this.palBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraLabel lblKhachHang;
        private Infragistics.Win.Misc.UltraLabel lblSaInvoice;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbSaInvoice;
        private System.Windows.Forms.Panel palCenter;
        private System.Windows.Forms.Panel palLeft;
        private System.Windows.Forms.Panel palRight;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSaInvoiceDetails;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSaInvoiceReturn;
        private Infragistics.Win.UltraWinGrid.UltraGrid UgridDetail;
        private Infragistics.Win.Misc.UltraButton btnSelectedAll;
        private Infragistics.Win.Misc.UltraButton btnUnSelected;
        private Infragistics.Win.Misc.UltraButton btnUnSelectedAll;
        private Infragistics.Win.Misc.UltraButton btnSelected;
    }
}