﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSAReturn : CustormForm //UserControl
    {
        #region khai báo
        private readonly ISAReturnService _iSaReturnService;
        List<SAReturn> _dsSaReturn = new List<SAReturn>();

        // Chứa danh sách các template dựa theo TypeID (KEY type of string = TypeID@Guid, VALUES = Template của TypeID và TemplateID ứng với chứng từ đó)
        // - khi một chứng từ được load, nó kiểm tra Template nó cần có trong dsTemplate chưa, chưa có thì load trong csdl có rồi thì lấy luôn trong dsTemplate để tránh truy vấn nhiều
        // - dsTemplate sẽ được cập nhật lại khi có sự thay đổi giao diện
        //Note: nó có tác dụng tăng tốc độ của chương trình, tuy nhiên chú ý nó là hàm Static nên tồn tại ngay cả khi đóng form, chỉ mất đi khi đóng toàn bộ chương trình do đó cần chú ý set nó bằng null hoặc bằng new Dictionary<string, Template>() hoặc clear item trong dic cho giải phóng bộ nhớ
        public static Dictionary<string, Template> DsTemplate = new Dictionary<string, Template>();
        #endregion

        #region khởi tạo
        public FSAReturn()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _iSaReturnService = IoC.Resolve<ISAReturnService>();
            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
        }

        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            //Dữ liệu lấy theo năm hoạch toán
            DateTime? dbStartDate = Utils.StringToDateTime(ConstFrm.DbStartDate);
            int year = (dbStartDate == null ? DateTime.Now.Year : dbStartDate.Value.Year);
            _dsSaReturn = _iSaReturnService.Query.Where(k => k.Date.Year == year).ToList();
            #endregion

            #region Xử lý dữ liệu
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = _dsSaReturn;

            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void TsmAddHbtlClick(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void TsmAddHbggClick(object sender, EventArgs e)
        {
            AddFunction(false);
        }

        private void TsmViewClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void TsmDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void TmsReLoadClick(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_DroppingDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Control control = (Control)sender;
            var startPoint = control.PointToScreen(new Point(control.Location.X + control.Margin.Left + control.Padding.Left - btnAdd.ImageSize.Width, control.Location.Y + control.Size.Height + control.Margin.Top + control.Padding.Top - btnAdd.ImageSize.Height));
            cms4Button.Show(startPoint);
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void BtnEditClick(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void UGridMouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void UGridDoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        /// <summary>
        /// Nghiệp vụ Add
        /// </summary> 
        void AddFunction(bool isHBTL = true)
        {
            SAReturn temp = uGrid.Selected.Rows.Count <= 0 ? new SAReturn() : uGrid.Selected.Rows[0].ListObject as SAReturn;
            if (temp != null)
            {
                temp.TypeID = isHBTL ? 330 : 340;
                new FSAReturnDetail(temp, _dsSaReturn, ConstFrm.optStatusForm.Add).ShowDialog(this);
            }
            if (!FSAReturnDetail.IsClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        void EditFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SAReturn temp = uGrid.Selected.Rows[0].ListObject as SAReturn;
                new FSAReturnDetail(temp, _dsSaReturn, ConstFrm.optStatusForm.View).ShowDialog(this);
                if (!FSAReturnDetail.IsClose) LoadDuLieu();
            }
            else
                MSG.Error(resSystem.MSG_System_04);
        }

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                SAReturn temp = uGrid.Selected.Rows[0].ListObject as SAReturn;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.No)) == DialogResult.Yes)
                {
                    _iSaReturnService.BeginTran();
                    _iSaReturnService.Delete(temp);
                    _iSaReturnService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.SAReturn_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["Date"].Format = Constants.DdMMyyyy;
        }

        public static Template GetMauGiaoDien(int typeId, Guid? templateId, Dictionary<string, Template> dsTemplate)    //out string keyofdsTemplate
        {
            string keyofdsTemplate = string.Format("{0}@{1}", typeId, templateId);
            if (!dsTemplate.Keys.Contains(keyofdsTemplate))
            {
                //Note: các chứng từ được sinh tự động từ nghiệp vụ khác thì phải truyền TypeID của nghiệp vụ khác đó
                int typeIdTemp = GetTypeIdRef(typeId);
                Template template = Utils.GetTemplateInDatabase(templateId, typeIdTemp);
                dsTemplate.Add(keyofdsTemplate, template);
                return template;
            }
            return dsTemplate[keyofdsTemplate];
        }

        static int GetTypeIdRef(int typeId)
        {//phiếu chi
            //if (typeID == 111) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])
            //else if (typeID == 112) return 111568658;  //phiếu chi trả lương ([Tiền lương] -> [Trả lương])....
            return typeId;
        }
        #endregion

        #region Event
        private void UGridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            SAReturn SAReturn = listObject as SAReturn;
            if (SAReturn == null) return;

            //Tab thông tin chung
            lblNo.Text = SAReturn.No;
            lblDate.Text = SAReturn.Date.ToString(Constants.DdMMyyyy);
            lblAccountingObjectName.Text = SAReturn.AccountingObjectName;
            lblAccountingObjectAddress.Text = SAReturn.AccountingObjectAddress;
            lblReason.Text = SAReturn.Reason;
            lblTotalAmount.Text = string.Format(SAReturn.TotalAmount == 0 ? "0" : "{0:0,0}", SAReturn.TotalAmount);  //SAReturn.TotalAmount.ToString("{0:0,0}");

            //Tab hoạch toán
            uGridPosted.DataSource = SAReturn.SAReturnDetails;    //Utils.CloneObject(SAReturn.SAReturnDetails);

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();

            Template mauGiaoDien = GetMauGiaoDien(SAReturn.TypeID, SAReturn.TemplateID, DsTemplate);

            Utils.ConfigGrid(uGridPosted, ConstDatabase.SAReturnDetail_TableName, mauGiaoDien == null ? new List<TemplateColumn>() : mauGiaoDien.TemplateDetails.Single(k => k.TabIndex == 0).TemplateColumns, 0);
            //Thêm tổng số ở cột "Số tiền"
            SummarySettings summary = band.Summaries.Add("SumAmount", SummaryType.Sum, band.Columns["Amount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion
        }
        private void FSAReturnFormClosing(object sender, FormClosingEventArgs e)
        {
            //giải phóng biến static khi đóng form giúp giải phóng tài nguyên, tuy nhiên nếu để lại thì tốc độ truy cập lần sau sẽ tăng nếu mở form lại
            //DsTemplate = new Dictionary<string, Template>();
        }
        #endregion

        private void hbtlcontex_Click(object sender, EventArgs e)
        {
            AddFunction(true);
        }

        private void HmtlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }

     

     

       
    }
}
