﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSAExceptVoucher : CustormForm
    {
        #region Khởi tạo
        private readonly IAccountingObjectService _IAccountingObjectService;
        private readonly IGeneralLedgerService _IGeneralLedgerService;
        private readonly IAccountService _IAccountService;
        private readonly ICurrencyService _ICurrencyService;
        private readonly IExceptVoucherService _IExceptVoucherService;
        public IGenCodeService IGenCodeService { get { return IoC.Resolve<IGenCodeService>(); } }
        public IGOtherVoucherService IGOtherVoucherService { get { return IoC.Resolve<IGOtherVoucherService>(); } }
        List<ConfrontVouchers> lstConfrontVouchersesDsachNo = new List<ConfrontVouchers>();
        List<ConfrontVouchers> lstConfrontVouchersesDsachCtTraTien = new List<ConfrontVouchers>();
        List<ExceptVoucher> lsExceptVouchersSave = new List<ExceptVoucher>();

        #endregion
        #region Fmain
        public FSAExceptVoucher()
        {
            WaitingFrm.StartWaiting();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IAccountService = IoC.Resolve<IAccountService>();
            _ICurrencyService = IoC.Resolve<ICurrencyService>();
            _IExceptVoucherService = IoC.Resolve<IExceptVoucherService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            InitializeComponent();
            LoadDuLieu();
            //ultratxtSoDu.Value = (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToan));
            this.cbbAccountingObject.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbAccountingObject.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            this.cbbAccount.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbAccount.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            this.cbbCurentcy.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.cbbCurentcy.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                //this.StartPosition = FormStartPosition.CenterParent;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Hàm Chung
        void LoadGrid()
        {
            if (cbbAccountingObject.SelectedRow != null)
            {
                if (cbbAccountingObject.SelectedRow.ListObject != null)
                {
                    if (cbbAccount.SelectedRow.ListObject != null)
                    {
                        if (cbbCurentcy.SelectedRow.ListObject != null)
                        {
                            var AccountingObject = cbbAccountingObject.SelectedRow.ListObject as AccountingObject;

                            var Account = cbbAccount.SelectedRow.ListObject as Account;

                            var Currency = cbbCurentcy.SelectedRow.ListObject as Currency;
                            var Ex = _IGeneralLedgerService.GetVouchersSell(AccountingObject.ID, Account.AccountNumber, Currency.ID);
                            lstConfrontVouchersesDsachNo = Ex.Debits.ToList().OrderBy(c => c.NgayChungTu).OrderByDescending(x => x.ChuaThanhToan).ToList();
                            uGridDsachNo.SetDataBinding(lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), "");
                            lstConfrontVouchersesDsachCtTraTien = Ex.Credits.ToList().OrderBy(c => c.NgayChungTu).OrderByDescending(x => x.SoConLai).ToList();
                            uGridDsachCtTraTien.SetDataBinding(lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), "");
                            ultratxtSoDu.Refresh();
                        }
                    }
                }
            }

        }
        void LoadDuLieu()
        {
            // Load dữ liệu combobox nhà cung cấp
            cbbAccountingObject.DataSource = _IAccountingObjectService.GetAccountingObjects(0, true);
            cbbAccountingObject.DisplayMember = "AccountingObjectCode";
            Utils.ConfigGrid(cbbAccountingObject, ConstDatabase.AccountingObject_TableName);// Load dữ liệu combobox TK phải thu
            cbbAccount.DataSource = _IAccountService.GetByDetailType(); // load dữ liệu tk phải thu chi tiết theo nhà cung cấp 
            cbbAccount.DisplayMember = "AccountNumber";
            cbbAccount.Rows[0].Selected = true;
            Utils.ConfigGrid(cbbAccount, ConstDatabase.Account_TableName);
            cbbCurentcy.DataSource = _ICurrencyService.GetIsActive(true);
            cbbCurentcy.DisplayMember = "ID";
            //for (int i = 0; i < cbbCurentcy.Rows.Count; i++)
            //{
            //    var item = cbbCurentcy.Rows[i].ListObject as Currency;
            //    if (item.ID == "VND")
            //    {
            //        cbbCurentcy.SelectedRow = cbbCurentcy.Rows[i];
            //        break;
            //    }
            //}
            Utils.ConfigGrid(cbbCurentcy, ConstDatabase.Currency_TableName);
            cbbCurentcy.Text = "VND";
        }
        #endregion
        #region Header
        #region Combobox Nhà cung cấp vs Tài Khoản Phải Thu
        private void cbbAccountingObject_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (cbbAccountingObject.SelectedRow != null)
            {
                var model = cbbAccountingObject.SelectedRow.ListObject as AccountingObject;
                lblAccountingObjectName.Text = model.AccountingObjectName;
            }
            var AccountingObject = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObject);
            var Account = (Account)Utils.getSelectCbbItem(cbbAccount);
            var Currency = (Currency)Utils.getSelectCbbItem(cbbCurentcy);
            Except Ex = null;
            if (AccountingObject == null)
                MSG.Error("Dữ liệu không có trong danh mục");
            if (AccountingObject != null && Account != null && Currency != null)
                Ex = _IGeneralLedgerService.GetVouchersSell(AccountingObject.ID, Account.AccountNumber, Currency.ID);
            if (Ex != null)
            {
                lstConfrontVouchersesDsachNo = Ex.Debits.ToList().OrderBy(c => c.ChuaThanhToan).OrderByDescending(x => x.NgayChungTu).ToList();
                lstConfrontVouchersesDsachCtTraTien = Ex.Credits.ToList().OrderBy(c => c.SoConLai).OrderByDescending(x => x.NgayChungTu).ToList();
            }
            uGridDsachNo.SetDataBinding(lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), "");
            uGridDsachCtTraTien.SetDataBinding(lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), "");
            if (cbbCurentcy.Text == "VND")
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                //định dạng tiền việt nam đồng âm thì trong ngoặc
            }
            else
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
            }
        }
        #endregion
        #region Combobox Loại Tiền
        private void cbbAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (cbbAccountingObject.SelectedRow != null)
            {
                var model = cbbAccountingObject.SelectedRow.ListObject as AccountingObject;
                lblAccountingObjectName.Text = model.AccountingObjectName;
            }
            if (cbbAccountingObject.SelectedRow != null)
            {
                var AccountingObject = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObject);
                var Account = (Account)Utils.getSelectCbbItem(cbbAccount);
                var Currency = (Currency)Utils.getSelectCbbItem(cbbCurentcy);
                Except Ex = null;
                if (Account == null)
                    MSG.Error("Dữ liệu không có trong danh mục");
                if (AccountingObject != null && Account != null && Currency != null)
                    Ex = _IGeneralLedgerService.GetVouchersSell(AccountingObject.ID, Account.AccountNumber, Currency.ID);
                if (Ex != null)
                {
                    lstConfrontVouchersesDsachNo = Ex.Debits.ToList().OrderBy(c => c.ChuaThanhToan).OrderByDescending(x => x.NgayChungTu).ToList();
                    lstConfrontVouchersesDsachCtTraTien = Ex.Credits.ToList().OrderBy(c => c.SoConLai).OrderByDescending(x => x.NgayChungTu).ToList();
                }
                uGridDsachNo.SetDataBinding(lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), "");
                uGridDsachCtTraTien.SetDataBinding(lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), "");
            }

            if (cbbCurentcy.Text == "VND")
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                //định dạng tiền việt nam đồng âm thì trong ngoặc
            }
            else
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
            }
        }

        private void cbbCurentcy_RowSelected(object sender, RowSelectedEventArgs e)
        {
            //if (cbbCurentcy.SelectedRow.ListObject != null)
            {
                var model = (Currency)Utils.getSelectCbbItem(cbbCurentcy);
                if (model == null)
                    MSG.Error("Dữ liệu không có trong danh mục");
                if (model != null)
                {
                    if (model.ID == "VND")
                    {
                        LoadGrid();
                        ViewDsach(uGridDsachNo, lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), true, true);
                        ViewDsach(uGridDsachCtTraTien, lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), true, false);

                    }
                    else
                    {
                        LoadGrid();
                        ViewDsach(uGridDsachNo, lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), false, true);
                        ViewDsach(uGridDsachCtTraTien, lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList(), false, false);
                    }
                }
            }
            if (cbbCurentcy.Text == "VND")
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList().Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList().Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                //định dạng tiền việt nam đồng âm thì trong ngoặc
            }
            else
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList().Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList().Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
            }
        }
        private void cbbCurentcy_ValueChanged(object sender, EventArgs e)
        {

        }
        #endregion
        private void cbbAccount_ValueChanged(object sender, EventArgs e)
        {

        }
        #endregion
        #region Body
        #region Cấu hình mẫu giao diện hiện thị danh sách Chiết khấu Nợ và Triết khấu Trả tiền/Thue tiền
        private List<TemplateColumn> MauDanhSach(bool isVnd, bool isDebit)
        {
            return new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "Status",
                                   ColumnCaption = "",
                                   ColumnWidth = 30,
                                   ColumnMaxWidth = 60,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NgayChungTu",
                                   ColumnCaption = "Ngày chứng từ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "LoaiChungTu",
                                   ColumnCaption = "Loại chứng từ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "SoChungTu",
                                   ColumnCaption = "Số chứng từ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NhanVien",
                                   ColumnCaption = "Nhân viên",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 6,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                    ColumnName = "TriGiaQd",
                                   ColumnCaption = "Trị giá quy đổi",
                                   ColumnWidth = 100,
                                   IsVisible = !isVnd,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 9,
                                   IsReadOnly = true


                               },
                               new TemplateColumn
                               {
                                    ColumnName = "RefVoucherExchangeRate",
                                   ColumnCaption = "Tỷ giá CT",
                                   ColumnWidth = 100,
                                   IsVisible = !isVnd,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 7,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                    ColumnName = "TriGia",
                                   ColumnCaption = "Trị giá",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 8,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "ChuaThanhToanQd",
                                   ColumnCaption = "Số tiền còn nợ quy đổi",
                                   ColumnWidth = 100,
                                   IsVisible = !isVnd && isDebit,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 11,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {

                                   ColumnName = "ChuaThanhToan",
                                   ColumnCaption = "Số tiền còn nợ",
                                   ColumnWidth = 100,
                                   IsVisible = isDebit,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 10,
                                   IsReadOnly = true


                               },
                               new TemplateColumn
                               {
                                    ColumnName = "SoConLaiQd",
                                   ColumnCaption = "Số tiền còn lại quy đổi",
                                   ColumnWidth = 100,
                                   IsVisible = !isVnd && !isDebit,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 14,
                                   IsReadOnly = true


                               },
                               new TemplateColumn
                               {
                                    ColumnName = "LastExchangeRate",
                                   ColumnCaption = "Tỷ giá đánh giá lại",
                                   ColumnWidth = 100,
                                   IsVisible = !isVnd,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 12,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                    ColumnName = "SoConLai",
                                   ColumnCaption = "Số tiền còn lại",
                                   ColumnWidth = 100,
                                   IsVisible = !isDebit,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 13,
                                   IsReadOnly = true
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "SoDoiTru",
                                   ColumnCaption = "Số đối trừ",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 15
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "SoDoiTruQD",
                                   ColumnCaption = "Số đối trừ QD",
                                   ColumnWidth = 100,
                                   IsVisible = !isVnd,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 16
                               }

                       };
        }
        private void ViewDsach(UltraGrid uGrid, List<ConfrontVouchers> model, bool isVnd, bool isDebit)
        {
            uGrid.DataSource = model;
            Utils.ConfigGrid(uGrid, "", MauDanhSach(isVnd, isDebit));
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            uGrid.DisplayLayout.Bands[0].Columns["NgayChungTu"].Format = "dd/MM/yyyy";
            // xét style cho các cột có dạng combobox
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(uGrid.Rows, false);

            Utils.AddSumColumn(uGrid, "TriGia", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "TriGiaQd", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "ChuaThanhToan", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "ChuaThanhToanQd", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "SoConLai", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "SoConLaiQd", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "SoDoiTru", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGrid, "SoDoiTruQD", false, "", ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["RefVoucherExchangeRate"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["LastExchangeRate"], ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["TriGia"], isVnd ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["TriGiaQd"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["ChuaThanhToan"], isVnd ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["ChuaThanhToanQd"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SoConLai"], isVnd ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SoConLaiQd"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SoDoiTru"], isVnd ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SoDoiTruQD"], ConstDatabase.Format_TienVND);


        }
        #endregion
        private void uGridDsachNo_CellChange(object sender, CellEventArgs e)
        {
            uGridDsachNo.UpdateData();
            uGridDsachCtTraTien.Update();
            if (e.Cell.Column.Key == "SoDoiTru")
            {
                e.Cell.Row.Cells["SoDoiTruQD"].Value = (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value > 0 ? (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value :
                        (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["RefVoucherExchangeRate"].Value;
                if (Convert.ToDecimal(e.Cell.Row.Cells["SoDoiTru"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["ChuaThanhToan"].Value))
                {
                    MSG.Warning(resSystem.PPExceptVoucher6);
                    e.Cell.Row.Cells["SoDoiTru"].Value = Convert.ToDecimal(e.Cell.Row.Cells["ChuaThanhToan"].Value);
                    e.Cell.Row.Cells["SoDoiTruQD"].Value = (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value > 0 ? (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value :
                        (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["RefVoucherExchangeRate"].Value;
                }
                else if (Convert.ToDecimal(e.Cell.Row.Cells["SoDoiTru"].Value) == 0)
                {
                    e.Cell.Row.Cells["Status"].Value = false;
                }
            }
            if (e.Cell.Column.Key == "Status")
            {
                if (!(bool)e.Cell.Value) e.Cell.Row.Cells["SoDoiTru"].Value = (decimal)0;
                ultraButton1.PerformClick();
                uGridDsachNo.Refresh();
                uGridDsachCtTraTien.Refresh();
            }

        }

        private void uGridDsachCtTraTien_CellChange(object sender, CellEventArgs e)
        {
            uGridDsachNo.UpdateData();
            uGridDsachCtTraTien.UpdateData();
            if (e.Cell.Column.Key == "SoDoiTru")
            {
                e.Cell.Row.Cells["SoDoiTruQD"].Value = (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value > 0 ? (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value :
                        (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["RefVoucherExchangeRate"].Value;
                if (Convert.ToDecimal(e.Cell.Row.Cells["SoDoiTru"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["SoConLai"].Value))
                {
                    MSG.Warning(resSystem.PPExceptVoucher7);
                    e.Cell.Row.Cells["SoDoiTru"].Value = Convert.ToDecimal(e.Cell.Row.Cells["SoConLai"].Value);
                    e.Cell.Row.Cells["SoDoiTruQD"].Value = (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value > 0 ? (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["LastExchangeRate"].Value :
                        (decimal)e.Cell.Row.Cells["SoDoiTru"].Value * (decimal)e.Cell.Row.Cells["RefVoucherExchangeRate"].Value;
                }
                else if (Convert.ToDecimal(e.Cell.Row.Cells["SoDoiTru"].Value) == 0)
                {
                    e.Cell.Row.Cells["Status"].Value = false;
                }
            }
            if (e.Cell.Column.Key == "Status")
            {
                if (!(bool)e.Cell.Value) e.Cell.Row.Cells["SoDoiTru"].Value = (decimal)0;
                ultraButton1.PerformClick();
                uGridDsachNo.Refresh();
                uGridDsachCtTraTien.Refresh();

            }

        }
        private void ultraButton1_Click(object sender, EventArgs e)
        {
            TinhToanDoiTru();
        }
        private void TinhToanDoiTru()
        {

            if (lstConfrontVouchersesDsachCtTraTien.Count <= 0 || lstConfrontVouchersesDsachNo.Count <= 0)
            {
                return;
            }
            else if (lstConfrontVouchersesDsachCtTraTien.Count(c => c.Status) <= 0 && lstConfrontVouchersesDsachNo.Count(c => c.Status) <= 0)
            {
                foreach (var x in lstConfrontVouchersesDsachNo)
                {
                    x.SoDoiTru = x.Status == true ? x.ChuaThanhToan : 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                }
                foreach (var x in lstConfrontVouchersesDsachCtTraTien)
                {
                    x.SoDoiTru = x.Status == true ? x.SoConLai : 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                }
                return;
            }
            else if (lstConfrontVouchersesDsachCtTraTien.Count(c => c.Status) <= 0 && lstConfrontVouchersesDsachNo.Count(c => c.Status) > 0)
            {
                foreach (var x in lstConfrontVouchersesDsachNo)
                {
                    x.SoDoiTru = x.Status == true ? x.ChuaThanhToan : 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                }
            }
            else if (lstConfrontVouchersesDsachCtTraTien.Count(c => c.Status) > 0 && lstConfrontVouchersesDsachNo.Count(c => c.Status) <= 0)
            {
                foreach (var x in lstConfrontVouchersesDsachCtTraTien)
                {
                    x.SoDoiTru = x.Status == true ? x.SoConLai : 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                }
            }
            else if (lstConfrontVouchersesDsachCtTraTien.Count(c => c.Status) > 0 &&
                     lstConfrontVouchersesDsachNo.Count(c => c.Status) > 0)
            {
                foreach (var x in lstConfrontVouchersesDsachNo)
                {
                    x.SoDoiTru = 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);

                }
                foreach (var x in lstConfrontVouchersesDsachCtTraTien)
                {
                    x.SoDoiTru = 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                }

            }
            decimal ConNo = (decimal)lstConfrontVouchersesDsachNo.Where(c => c.Status).Sum(c => c.ChuaThanhToan);

            decimal ThuTien = (decimal)lstConfrontVouchersesDsachCtTraTien.Where(c => c.Status).Sum(c => c.SoConLai);
            decimal Tong = 0;
            if (ConNo >= ThuTien)
            {
                Tong = ThuTien;
            }
            else if (ThuTien > ConNo)
            {
                Tong = ConNo;
            }
            decimal TongNo = Tong;
            decimal TongThu = Tong;
            int i = 0, j = 0;

            List<int> intTongNo = new List<int>();
            List<int> intTongCo = new List<int>();
            foreach (ConfrontVouchers x in lstConfrontVouchersesDsachNo)
            {
                i++;
                if (!x.Status)
                {
                    x.SoDoiTru = 0;
                    x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                    continue;
                }
                else
                {
                    if (TongNo == 0)
                    {
                        intTongCo.Add(i);
                        continue;
                    }
                    else if ((TongNo - x.ChuaThanhToan) < 0)
                    {
                        x.SoDoiTru = TongNo;
                        x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                        TongNo = 0;

                    }
                    else if ((TongNo - x.ChuaThanhToan) >= 0)
                    {
                        x.SoDoiTru = x.ChuaThanhToan;
                        x.SoDoiTruQD = x.LastExchangeRate == 0 ? ((x.SoDoiTru ?? 0) * x.RefVoucherExchangeRate ?? 1) : ((x.SoDoiTru ?? 0) * x.LastExchangeRate);
                        TongNo -= (decimal)(x.ChuaThanhToan ?? 0);

                    }
                }
                foreach (ConfrontVouchers y in lstConfrontVouchersesDsachCtTraTien)
                {
                    j++;
                    if (!y.Status)
                    {
                        y.SoDoiTru = null;
                        continue;
                    }
                    else
                    {
                        if (TongThu == 0)
                        {
                            intTongNo.Add(j);
                            continue;
                        }
                        else if ((TongThu - y.SoConLai) < 0)
                        {
                            y.SoDoiTru = TongThu;
                            y.SoDoiTruQD = y.LastExchangeRate == 0 ? ((y.SoDoiTru ?? 0) * y.RefVoucherExchangeRate ?? 1) : ((y.SoDoiTru ?? 0) * y.LastExchangeRate);
                            TongThu = 0;
                        }
                        else if ((TongThu - y.SoConLai) >= 0)
                        {
                            y.SoDoiTru = y.SoConLai;
                            y.SoDoiTruQD = y.LastExchangeRate == 0 ? ((y.SoDoiTru ?? 0) * y.RefVoucherExchangeRate ?? 1) : ((y.SoDoiTru ?? 0) * y.LastExchangeRate);
                            TongThu -= (decimal)(y.SoConLai ?? 0);
                        }
                    }

                }
            }

            uGridDsachNo.Refresh();
            uGridDsachCtTraTien.Refresh();
            uGridDsachNo.DisplayLayout.Bands[0].Summaries["SoDoiTruSum"].Refresh();
            uGridDsachNo.DisplayLayout.Bands[0].Summaries["SoDoiTruQDSum"].Refresh();
            uGridDsachCtTraTien.DisplayLayout.Bands[0].Summaries["SoDoiTruSum"].Refresh();
            uGridDsachCtTraTien.DisplayLayout.Bands[0].Summaries["SoDoiTruQDSum"].Refresh();
        }
        private void uGridDsachNo_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (lstConfrontVouchersesDsachNo.Count > 0)
            {

                uGridDsachNo.UpdateData();
                uGridDsachCtTraTien.Update();
                if (e.Column.Key == "Status")
                {
                    ultraButton1.PerformClick();
                    uGridDsachNo.Refresh();
                    uGridDsachCtTraTien.Refresh();
                }
            }
        }

        private void uGridDsachCtTraTien_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (lstConfrontVouchersesDsachCtTraTien.Count > 0)
            {
                uGridDsachNo.UpdateData();
                uGridDsachCtTraTien.UpdateData();
                if (e.Column.Key == "Status")
                {

                    ultraButton1.PerformClick();
                    uGridDsachNo.Refresh();
                    uGridDsachCtTraTien.Refresh();
                }
            }
        }
        #endregion
        #region Footer
        #region Button Đối Trừ
        private void btnDoiTru_Click(object sender, EventArgs e)
        {
            uGridDsachCtTraTien.UpdateData();
            uGridDsachNo.UpdateData();
            //var lstCreditVoucher = uGridDsachNo.DataSource as List<ConfrontVouchers>;//Lấy ra list chứng từ còn nợ trong gridview
            //var lstDebitVoucher = uGridDsachCtTraTien.DataSource as List<ConfrontVouchers>;//Lấy ra list chứng từ thu tiền trong gridview
            Currency currency = (Currency)Utils.getSelectCbbItem(cbbCurentcy);
            List<ConfrontVouchers> lstConfrontVouchersesDsachNo1 = new List<ConfrontVouchers>();
            List<ConfrontVouchers> lstConfrontVouchersesDsachCtTraTien1 = new List<ConfrontVouchers>();
            lstConfrontVouchersesDsachNo1 = lstConfrontVouchersesDsachNo.Where(c => c.Status == true).ToList();
            lstConfrontVouchersesDsachCtTraTien1 = lstConfrontVouchersesDsachCtTraTien.Where(c => c.Status == true).ToList();
            decimal TongSoDoiTruCredit = (decimal)(lstConfrontVouchersesDsachNo1.Sum(c => c.SoDoiTru) ?? 0);//Cộng tổng cột số đối trừ trong list chứng từ con nợ
            decimal TongSoDoiTruDebit = (decimal)(lstConfrontVouchersesDsachCtTraTien1.Where(c => c.Status == true).Sum(c => c.SoDoiTru) ?? 0);//Cộng tổng cột số đối trừ trong list chứng từ thu tiền
            if (TongSoDoiTruDebit != TongSoDoiTruCredit)//Nếu tổng còn nợ khác với tổng thu tiền thì không thực hiện được đối chiếu báo lỗi
            {
                MSG.Warning(resSystem.SAExceptVoucher);
                return;
            }
            else if (cbbAccountingObject.SelectedRow == null)//Không được phép để trống nhà cung cấp
            {
                MSG.Warning(/*resSystem.PPExceptVoucher2*/"Bạn chưa chọn khách hàng để đối trừ chứng từ. Vui lòng thực hiện lại!");
                return;
            }
            else if (cbbAccount.SelectedRow == null)//Tài khoản không được phép để trống
            {
                MSG.Warning(resSystem.PPExceptVoucher3);
                return;
            }
            else if (dteDateTo.Value == null)
            {
                MSG.Warning("Ngày đối trừ không được để trống");
                return;
            }
            else if (lstConfrontVouchersesDsachNo1.Count > 0 && lstConfrontVouchersesDsachCtTraTien1.Count > 0)
            {
                if (lstConfrontVouchersesDsachNo1.Any(c => c.Status == true && c.SoDoiTru != 0 && c.SoDoiTru != null) && lstConfrontVouchersesDsachCtTraTien1.Any(c => c.Status == true && c.SoDoiTru != 0 && c.SoDoiTru != null))
                //Nếu khôn chọn chứng từ thì không được thực hiện đối chiếu phải chọn 2 chứng từ ở 2 grid
                {
                    Guid accountingObjectId;
                    string debitAccount;
                    lsExceptVouchersSave = new List<ExceptVoucher>();
                    accountingObjectId = (cbbAccountingObject.SelectedRow.ListObject as AccountingObject).ID;
                    debitAccount = (cbbAccount.SelectedRow.ListObject as Account).AccountNumber;
                    Currency cu = (Currency)cbbCurentcy.SelectedRow.ListObject;
                    List<AccountingObject> lstAccountingObjects = _IAccountingObjectService.Query.ToList();
                    List<ExceptVoucher> lstExceptVoucher = new List<ExceptVoucher>(); //
                    List<ConfrontVouchers> lstNoSave = lstConfrontVouchersesDsachNo1.Where(c => c.Status && c.SoDoiTru != 0).ToList();
                    List<ConfrontVouchers> lstCoSave = lstConfrontVouchersesDsachCtTraTien1.Where(c => c.Status && c.SoDoiTru != 0).ToList();
                    try
                    {

                        foreach (var x in lstNoSave)
                        {
                            decimal TongSDT = (decimal)x.SoDoiTru;
                            foreach (var y in lstCoSave.Where(c => c.Status))
                            {
                                if (TongSDT == 0)
                                {
                                    break;
                                }
                                if (y.SoDoiTru <= TongSDT)
                                {
                                    ExceptVoucher addlist = new ExceptVoucher();
                                    addlist.GLVoucherID = x.GlVoucherId;
                                    addlist.Date = (DateTime)dteDateTo.Value; //lưu theo ngày hạch toán
                                    addlist.PostedDate = (DateTime)dteDateTo.Value;
                                    addlist.DebitAccount = debitAccount;
                                    addlist.No = x.SoChungTu;
                                    addlist.Amount = (x.LastExchangeRate == 0 ? (y.SoDoiTru ?? 0) * (x.RefVoucherExchangeRate ?? 1) : (decimal)y.SoDoiTru * x.LastExchangeRate);
                                    addlist.AmountOriginal = (decimal)y.SoDoiTru;
                                    addlist.InvoiceNo = x.SoHoaDon;
                                    addlist.EmployeeID = lstAccountingObjects.Count(c => c.AccountingObjectCode == x.NhanVien) == 1 ? lstAccountingObjects.SingleOrDefault(c => c.AccountingObjectCode == x.NhanVien).ID : (Guid?)null;
                                    addlist.AccountingObjectID = accountingObjectId;
                                    addlist.CurrencyID = x.CurrencyID;
                                    addlist.GLVoucherExceptID = y.GlVoucherExceptId;
                                    addlist.ExceptNo = y.SoChungTu;
                                    addlist.ExceptEmployeeID = lstAccountingObjects.Count(c => c.AccountingObjectCode == x.NhanVien) == 1 ? lstAccountingObjects.SingleOrDefault(c => c.AccountingObjectCode == x.NhanVien).ID : (Guid?)null;                                    
                                    y.Status = false;
                                    TongSDT -= (decimal)y.SoDoiTru;
                                    lsExceptVouchersSave.Add(addlist);
                                }
                                else if (y.SoDoiTru > TongSDT)
                                {
                                    ExceptVoucher addlist = new ExceptVoucher();
                                    addlist.GLVoucherID = x.GlVoucherId;
                                    addlist.Date = (DateTime)dteDateTo.Value; //lưu theo ngày hạch toán
                                    addlist.PostedDate = (DateTime)dteDateTo.Value;
                                    addlist.DebitAccount = debitAccount;
                                    addlist.No = x.SoChungTu;
                                    addlist.Amount = (x.LastExchangeRate == 0 ? TongSDT * (x.RefVoucherExchangeRate ?? 1) : TongSDT * x.LastExchangeRate);
                                    addlist.AmountOriginal = TongSDT;
                                    addlist.InvoiceNo = x.SoHoaDon;
                                    addlist.EmployeeID = lstAccountingObjects.Count(c => c.AccountingObjectCode == x.NhanVien) == 1 ? lstAccountingObjects.SingleOrDefault(c => c.AccountingObjectCode == x.NhanVien).ID : (Guid?)null;
                                    addlist.AccountingObjectID = accountingObjectId;
                                    addlist.CurrencyID = x.CurrencyID;
                                    addlist.GLVoucherExceptID = y.GlVoucherExceptId;
                                    addlist.ExceptNo = y.SoChungTu;
                                    addlist.ExceptEmployeeID = lstAccountingObjects.Count(c => c.AccountingObjectCode == x.NhanVien) == 1 ? lstAccountingObjects.SingleOrDefault(c => c.AccountingObjectCode == x.NhanVien).ID : (Guid?)null;
                                    y.SoDoiTru = y.SoDoiTru - TongSDT;
                                    lsExceptVouchersSave.Add(addlist);
                                    break;
                                }
                            }
                        }
                        _IExceptVoucherService.BeginTran();
                        foreach (var x in lsExceptVouchersSave)
                        {
                            x.ID = Guid.NewGuid();
                            _IExceptVoucherService.CreateNew(x);
                        }
                        decimal amount = lstNoSave.Sum(x => x.SoDoiTruQD) - lstCoSave.Sum(x => x.SoDoiTruQD);
                        if (amount != 0)
                        {
                            GOtherVoucher gOtherVoucher = new GOtherVoucher()
                            {
                                ID = Guid.NewGuid(),
                                TypeID = 670,
                                PostedDate = (DateTime)dteDateTo.Value,
                                Date = (DateTime)dteDateTo.Value,
                                No = Utils.TaoMaChungTu(IGenCodeService.getGenCode(60)),
                                Reason = string.Format("Xử lý chênh lệch tỷ giá khi đối trừ chứng từ"),
                                TotalAmount = Math.Abs(amount),
                                TotalAmountOriginal = 0,
                                CurrencyID = cu.ID,
                                ExchangeRate = cu.ExchangeRate,
                                Recorded = false,
                                Exported = true,
                            };
                            GOtherVoucherDetail GOtherVoucherDetails = new GOtherVoucherDetail()
                            {
                                GOtherVoucherID = gOtherVoucher.ID,
                                CurrencyID = cu.ID,
                                Description = amount < 0 ? "Xử lý chênh lệch tỷ giá lãi" : "Xử lý chênh lệch tỷ giá lỗ",
                                DebitAccount = amount < 0 ? debitAccount : Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLo").Data,
                                CreditAccount = amount < 0 ? Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLai").Data : debitAccount,
                                Amount = Math.Abs(amount),
                                AmountOriginal = 0,
                                DebitAccountingObjectID = amount < 0 ? accountingObjectId : Guid.Empty,
                                CreditAccountingObjectID = amount > 0 ? accountingObjectId : Guid.Empty
                            };
                            gOtherVoucher.GOtherVoucherDetails.Add(GOtherVoucherDetails);
                            foreach (var t in lsExceptVouchersSave)
                            {
                                GOtherVoucherDetailExcept GOtherVoucherDetailExcepts = new GOtherVoucherDetailExcept()
                                {
                                    GOtherVoucherID = gOtherVoucher.ID,
                                    PayRefID = t.GLVoucherExceptID,
                                    PayRefDate = lstCoSave.FirstOrDefault(x => x.SoChungTu == t.ExceptNo).NgayChungTu,
                                    PayRefNo = t.ExceptNo,
                                    PayAmountOriginal = lstCoSave.FirstOrDefault(x => x.SoChungTu == t.ExceptNo).SoDoiTru,
                                    PayAmount = lstCoSave.FirstOrDefault(x => x.SoChungTu == t.ExceptNo).SoDoiTruQD,
                                    DebtRefID = t.GLVoucherID,
                                    DebtRefDate = lstNoSave.FirstOrDefault(x => x.SoChungTu == t.No).NgayChungTu,
                                    DebtRefNo = t.No,
                                    DebtAmountOriginal = lstNoSave.FirstOrDefault(x => x.SoChungTu == t.No).SoDoiTru,
                                    DebtAmount = lstNoSave.FirstOrDefault(x => x.SoChungTu == t.No).SoDoiTruQD,
                                    DiffAmount = (lstCoSave.FirstOrDefault(x => x.SoChungTu == t.ExceptNo).SoDoiTruQD - lstNoSave.FirstOrDefault(x => x.SoChungTu == t.No).SoDoiTruQD)
                                };
                                gOtherVoucher.GOtherVoucherDetailExcepts.Add(GOtherVoucherDetailExcepts);
                            }

                            using (var frm = new FGOtherVoucherDetail(gOtherVoucher, IGOtherVoucherService.GetAll(), ConstFrm.optStatusForm.Add))
                            {
                                frm.ShowDialog(this);
                                if (frm._statusForm == ConstFrm.optStatusForm.View)
                                {
                                    _IExceptVoucherService.CommitTran();
                                    LoadGrid();
                                    if (cbbCurentcy.Text == "VND")
                                    {
                                        Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                                        //định dạng tiền việt nam đồng âm thì trong ngoặc
                                    }
                                    else
                                    {
                                        Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
                                    }
                                    MSG.Information(resSystem.PPExceptVoucher4);
                                }
                                else _IExceptVoucherService.RolbackTran();
                            }
                        }
                        else
                        {
                            _IExceptVoucherService.CommitTran();
                            LoadGrid();
                            if (cbbCurentcy.Text == "VND")
                            {
                                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                                //định dạng tiền việt nam đồng âm thì trong ngoặc
                            }
                            else
                            {
                                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
                            }
                            MSG.Information(resSystem.PPExceptVoucher4);
                        }                       
                    }
                    catch (Exception)
                    {
                        _IExceptVoucherService.RolbackTran();
                        throw;
                    }


                }
            }
            else
            {
                MSG.Warning(resSystem.PPExceptVoucher5);
          
            }
        }
        #endregion
        #region Button Bỏ Đối Trừ
        private void bntBoDoiTru_Click(object sender, EventArgs e)
        {
            Guid accountingObjectId;
            string debitAccount;
            if (cbbAccountingObject.SelectedRow == null)
            {
                MSG.Warning("Bạn chưa chọn khách hàng để đối trừ chứng từ. Vui lòng thức hiện lại!");
                return;
            }
            accountingObjectId = (cbbAccountingObject.SelectedRow.ListObject as AccountingObject).ID;
            if (cbbAccount.SelectedRow == null)
            {
                MSG.Warning(resSystem.PPExceptVoucher);
                return;
            }
            debitAccount = (cbbAccount.SelectedRow.ListObject as Account).AccountNumber;
            new FSAERemoveExceptVoucher(accountingObjectId, debitAccount).ShowDialog(this);
            if (cbbAccountingObject != null)
            {
                if (FSAERemoveExceptVoucher.IsClose && FSAERemoveExceptVoucher.IsClose1 == true) LoadGrid();
            }
            if (cbbCurentcy.Text == "VND")
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                //định dạng tiền việt nam đồng âm thì trong ngoặc
            }
            else
            {
                Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
            }
        }
        #endregion

        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void dteDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (cbbAccountingObject.SelectedRow != null)
            {

                var lstthu = lstConfrontVouchersesDsachNo.ToList().Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList();
                uGridDsachNo.SetDataBinding(lstthu.OrderByDescending(c => c.NgayChungTu).ToList(), "");
                var lsttra = lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).ToList();
                uGridDsachCtTraTien.SetDataBinding(lsttra.OrderByDescending(c => c.NgayChungTu).ToList(), "");
                ultratxtSoDu.Refresh();
                if (cbbCurentcy.Text == "VND")
                {
                    Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.SoConLai) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.ChuaThanhToan)), ConstDatabase.Format_TienVND);
                    //định dạng tiền việt nam đồng âm thì trong ngoặc
                }
                else
                {
                    Utils.FormatNumberic(ultratxtSoDu, (lstConfrontVouchersesDsachCtTraTien.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.SoConLaiQd) - lstConfrontVouchersesDsachNo.Where(x => x.NgayChungTu <= (DateTime)dteDateTo.Value).Sum(c => c.ChuaThanhToanQd)), ConstDatabase.Format_TienVND);
                }
            }
        }
    }
}


