﻿namespace Accounting
{
    partial class FSAExceptVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.dteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultratxtSoDu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbCurentcy = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectName = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccount = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObject = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblAccountingObject = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnKetThuc = new Infragistics.Win.Misc.UltraButton();
            this.btnDoiTru = new Infragistics.Win.Misc.UltraButton();
            this.bntBoDoiTru = new Infragistics.Win.Misc.UltraButton();
            this.uGridDsachNo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDsachCtTraTien = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultratxtSoDu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurentcy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDsachNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDsachCtTraTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.dteDateTo);
            this.ultraGroupBox2.Controls.Add(this.ultratxtSoDu);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.ultraGroupBox5);
            this.ultraGroupBox2.Controls.Add(this.cbbCurentcy);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObjectName);
            this.ultraGroupBox2.Controls.Add(this.lblAccount);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObject);
            this.ultraGroupBox2.Controls.Add(this.lblAccountingObject);
            this.ultraGroupBox2.Controls.Add(this.cbbAccount);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance45.FontData.BoldAsString = "True";
            appearance45.FontData.SizeInPoints = 9F;
            this.ultraGroupBox2.HeaderAppearance = appearance45;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(941, 110);
            this.ultraGroupBox2.TabIndex = 42;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance1;
            this.ultraLabel3.Location = new System.Drawing.Point(659, 40);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(88, 18);
            this.ultraLabel3.TabIndex = 74;
            this.ultraLabel3.Text = "Ngày đối trừ:";
            // 
            // dteDateTo
            // 
            this.dteDateTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dteDateTo.AutoSize = false;
            this.dteDateTo.CausesValidation = false;
            this.dteDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.dteDateTo.Location = new System.Drawing.Point(753, 37);
            this.dteDateTo.MaskInput = "dd/mm/yyyy";
            this.dteDateTo.Name = "dteDateTo";
            this.dteDateTo.Size = new System.Drawing.Size(131, 22);
            this.dteDateTo.TabIndex = 73;
            this.dteDateTo.ValueChanged += new System.EventHandler(this.dteDateTo_ValueChanged);
            // 
            // ultratxtSoDu
            // 
            this.ultratxtSoDu.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance2.TextHAlignAsString = "Right";
            this.ultratxtSoDu.Appearance = appearance2;
            this.ultratxtSoDu.Enabled = false;
            this.ultratxtSoDu.Location = new System.Drawing.Point(718, 73);
            this.ultratxtSoDu.Name = "ultratxtSoDu";
            this.ultratxtSoDu.Size = new System.Drawing.Size(211, 21);
            this.ultratxtSoDu.TabIndex = 46;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance3;
            this.ultraLabel2.Location = new System.Drawing.Point(656, 73);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(63, 18);
            this.ultraLabel2.TabIndex = 42;
            this.ultraLabel2.Text = "Số dư :";
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            appearance4.FontData.BoldAsString = "True";
            appearance4.FontData.SizeInPoints = 10F;
            this.ultraGroupBox5.HeaderAppearance = appearance4;
            this.ultraGroupBox5.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(935, 23);
            this.ultraGroupBox5.TabIndex = 44;
            this.ultraGroupBox5.Text = "Thông tin chung";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbCurentcy
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurentcy.DisplayLayout.Appearance = appearance5;
            this.cbbCurentcy.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbCurentcy.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurentcy.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurentcy.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurentcy.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.cbbCurentcy.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurentcy.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.cbbCurentcy.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurentcy.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurentcy.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurentcy.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.cbbCurentcy.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurentcy.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurentcy.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurentcy.DisplayLayout.Override.CellAppearance = appearance12;
            this.cbbCurentcy.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurentcy.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurentcy.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.cbbCurentcy.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.cbbCurentcy.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurentcy.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurentcy.DisplayLayout.Override.RowAppearance = appearance15;
            this.cbbCurentcy.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurentcy.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.cbbCurentcy.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurentcy.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurentcy.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurentcy.Location = new System.Drawing.Point(408, 70);
            this.cbbCurentcy.Name = "cbbCurentcy";
            this.cbbCurentcy.Size = new System.Drawing.Size(146, 22);
            this.cbbCurentcy.TabIndex = 41;
            this.cbbCurentcy.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCurentcy_RowSelected);
            this.cbbCurentcy.ValueChanged += new System.EventHandler(this.cbbCurentcy_ValueChanged);
            // 
            // ultraLabel1
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance17;
            this.ultraLabel1.Location = new System.Drawing.Point(349, 71);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(53, 18);
            this.ultraLabel1.TabIndex = 40;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // lblAccountingObjectName
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.lblAccountingObjectName.Appearance = appearance18;
            this.lblAccountingObjectName.Location = new System.Drawing.Point(347, 41);
            this.lblAccountingObjectName.Name = "lblAccountingObjectName";
            this.lblAccountingObjectName.Size = new System.Drawing.Size(517, 18);
            this.lblAccountingObjectName.TabIndex = 39;
            // 
            // lblAccount
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.lblAccount.Appearance = appearance19;
            this.lblAccount.Location = new System.Drawing.Point(13, 68);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(84, 18);
            this.lblAccount.TabIndex = 3;
            this.lblAccount.Text = "TK phải thu";
            // 
            // cbbAccountingObject
            // 
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObject.DisplayLayout.Appearance = appearance20;
            this.cbbAccountingObject.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbAccountingObject.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObject.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObject.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.cbbAccountingObject.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObject.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObject.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObject.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObject.DisplayLayout.Override.CellAppearance = appearance27;
            this.cbbAccountingObject.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObject.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObject.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.cbbAccountingObject.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObject.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObject.DisplayLayout.Override.RowAppearance = appearance30;
            this.cbbAccountingObject.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObject.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.cbbAccountingObject.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObject.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObject.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObject.Location = new System.Drawing.Point(103, 37);
            this.cbbAccountingObject.Name = "cbbAccountingObject";
            this.cbbAccountingObject.Size = new System.Drawing.Size(238, 22);
            this.cbbAccountingObject.TabIndex = 0;
            this.cbbAccountingObject.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObject_RowSelected);
            // 
            // lblAccountingObject
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Middle";
            this.lblAccountingObject.Appearance = appearance32;
            this.lblAccountingObject.Location = new System.Drawing.Point(13, 37);
            this.lblAccountingObject.Name = "lblAccountingObject";
            this.lblAccountingObject.Size = new System.Drawing.Size(84, 22);
            this.lblAccountingObject.TabIndex = 1;
            this.lblAccountingObject.Text = "Khách hàng";
            // 
            // cbbAccount
            // 
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccount.DisplayLayout.Appearance = appearance33;
            this.cbbAccount.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.GroupByBox.Appearance = appearance34;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance35;
            this.cbbAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance36.BackColor2 = System.Drawing.SystemColors.Control;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance36;
            this.cbbAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccount.DisplayLayout.Override.ActiveCellAppearance = appearance37;
            appearance38.BackColor = System.Drawing.SystemColors.Highlight;
            appearance38.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccount.DisplayLayout.Override.ActiveRowAppearance = appearance38;
            this.cbbAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.Override.CardAreaAppearance = appearance39;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            appearance40.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccount.DisplayLayout.Override.CellAppearance = appearance40;
            this.cbbAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccount.DisplayLayout.Override.CellPadding = 0;
            appearance41.BackColor = System.Drawing.SystemColors.Control;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccount.DisplayLayout.Override.GroupByRowAppearance = appearance41;
            appearance42.TextHAlignAsString = "Left";
            this.cbbAccount.DisplayLayout.Override.HeaderAppearance = appearance42;
            this.cbbAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccount.DisplayLayout.Override.RowAppearance = appearance43;
            this.cbbAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance44;
            this.cbbAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccount.Location = new System.Drawing.Point(103, 68);
            this.cbbAccount.Name = "cbbAccount";
            this.cbbAccount.Size = new System.Drawing.Size(238, 22);
            this.cbbAccount.TabIndex = 2;
            this.cbbAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccount_RowSelected);
            this.cbbAccount.ValueChanged += new System.EventHandler(this.cbbAccount_ValueChanged);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.btnKetThuc);
            this.ultraGroupBox4.Controls.Add(this.btnDoiTru);
            this.ultraGroupBox4.Controls.Add(this.bntBoDoiTru);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance49.FontData.BoldAsString = "True";
            appearance49.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance49;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(941, 43);
            this.ultraGroupBox4.TabIndex = 44;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnKetThuc
            // 
            this.btnKetThuc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance46.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnKetThuc.Appearance = appearance46;
            this.btnKetThuc.Location = new System.Drawing.Point(834, 8);
            this.btnKetThuc.Name = "btnKetThuc";
            this.btnKetThuc.Size = new System.Drawing.Size(101, 27);
            this.btnKetThuc.TabIndex = 44;
            this.btnKetThuc.Text = "Kết thúc";
            this.btnKetThuc.Click += new System.EventHandler(this.btnKetThuc_Click);
            // 
            // btnDoiTru
            // 
            this.btnDoiTru.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance47.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnDoiTru.Appearance = appearance47;
            this.btnDoiTru.Location = new System.Drawing.Point(620, 8);
            this.btnDoiTru.Name = "btnDoiTru";
            this.btnDoiTru.Size = new System.Drawing.Size(101, 27);
            this.btnDoiTru.TabIndex = 43;
            this.btnDoiTru.Text = "Đối trừ";
            this.btnDoiTru.Click += new System.EventHandler(this.btnDoiTru_Click);
            // 
            // bntBoDoiTru
            // 
            this.bntBoDoiTru.Anchor = System.Windows.Forms.AnchorStyles.Right;
            appearance48.Image = global::Accounting.Properties.Resources.edit_not_validated_icon;
            this.bntBoDoiTru.Appearance = appearance48;
            this.bntBoDoiTru.Location = new System.Drawing.Point(727, 8);
            this.bntBoDoiTru.Name = "bntBoDoiTru";
            this.bntBoDoiTru.Size = new System.Drawing.Size(101, 27);
            this.bntBoDoiTru.TabIndex = 42;
            this.bntBoDoiTru.Text = "Bỏ đối trừ";
            this.bntBoDoiTru.Click += new System.EventHandler(this.bntBoDoiTru_Click);
            // 
            // uGridDsachNo
            // 
            this.uGridDsachNo.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDsachNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDsachNo.Location = new System.Drawing.Point(0, 0);
            this.uGridDsachNo.Name = "uGridDsachNo";
            this.uGridDsachNo.Size = new System.Drawing.Size(941, 179);
            this.uGridDsachNo.TabIndex = 41;
            this.uGridDsachNo.Text = "ultraGrid1";
            this.uGridDsachNo.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridDsachNo.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDsachNo_CellChange);
            this.uGridDsachNo.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.uGridDsachNo_AfterHeaderCheckStateChanged);
            // 
            // uGridDsachCtTraTien
            // 
            this.uGridDsachCtTraTien.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDsachCtTraTien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDsachCtTraTien.Location = new System.Drawing.Point(0, 0);
            this.uGridDsachCtTraTien.Name = "uGridDsachCtTraTien";
            this.uGridDsachCtTraTien.Size = new System.Drawing.Size(941, 196);
            this.uGridDsachCtTraTien.TabIndex = 41;
            this.uGridDsachCtTraTien.Text = "ultraGrid1";
            this.uGridDsachCtTraTien.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridDsachCtTraTien.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDsachCtTraTien_CellChange);
            this.uGridDsachCtTraTien.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.uGridDsachCtTraTien_AfterHeaderCheckStateChanged);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance50.FontData.BoldAsString = "True";
            appearance50.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance50;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(941, 23);
            this.ultraGroupBox3.TabIndex = 44;
            this.ultraGroupBox3.Text = "Chứng từ thu tiền";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ultraButton1.Location = new System.Drawing.Point(753, 101);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(75, 23);
            this.ultraButton1.TabIndex = 44;
            this.ultraButton1.Text = "Đối trừ";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            appearance51.FontData.BoldAsString = "True";
            appearance51.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance51;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 107);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(941, 26);
            this.ultraGroupBox1.TabIndex = 43;
            this.ultraGroupBox1.Text = "Danh sách chứng từ còn phải thu";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ultraPanel3);
            this.splitContainer1.Panel1.Controls.Add(this.ultraPanel2);
            this.splitContainer1.Panel1.Controls.Add(this.ultraPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel6);
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel5);
            this.splitContainer1.Size = new System.Drawing.Size(941, 578);
            this.splitContainer1.SplitterDistance = 335;
            this.splitContainer1.TabIndex = 45;
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.uGridDsachNo);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 133);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(941, 179);
            this.ultraPanel3.TabIndex = 48;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 312);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(941, 23);
            this.ultraPanel2.TabIndex = 47;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(941, 133);
            this.ultraPanel1.TabIndex = 46;
            // 
            // ultraPanel6
            // 
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.uGridDsachCtTraTien);
            this.ultraPanel6.ClientArea.Controls.Add(this.ultraButton1);
            this.ultraPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel6.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(941, 196);
            this.ultraPanel6.TabIndex = 47;
            // 
            // ultraPanel5
            // 
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.ultraPanel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel5.Location = new System.Drawing.Point(0, 196);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(941, 43);
            this.ultraPanel5.TabIndex = 46;
            // 
            // FSAExceptVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 578);
            this.Controls.Add(this.splitContainer1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSAExceptVoucher";
            this.Text = "Đối trừ chứng từ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultratxtSoDu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurentcy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDsachNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDsachCtTraTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ResumeLayout(false);
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurentcy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectName;
        private Infragistics.Win.Misc.UltraLabel lblAccount;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObject;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObject;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccount;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDsachNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDsachCtTraTien;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton btnKetThuc;
        private Infragistics.Win.Misc.UltraButton btnDoiTru;
        private Infragistics.Win.Misc.UltraButton bntBoDoiTru;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel6;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultratxtSoDu;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDateTo;
    }
}