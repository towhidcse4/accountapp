﻿namespace Accounting
{
    partial class FSACalculateTheCost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.UltraGridTinhGiaBan = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel7 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel9 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraMaskSoTien = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel8 = new Infragistics.Win.Misc.UltraPanel();
            this.UltraMaskTyLePhanTram = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraPanel6 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraOSThayDoiGiaBanTheo = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbGiaApDung = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbTieuChi = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraButCat = new Infragistics.Win.Misc.UltraButton();
            this.ultraButHoan = new Infragistics.Win.Misc.UltraButton();
            this.UltraDong = new Infragistics.Win.Misc.UltraButton();
            this.UltraButTinhGiaBan = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridTinhGiaBan)).BeginInit();
            this.ultraPanel4.ClientArea.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.ultraPanel7.ClientArea.SuspendLayout();
            this.ultraPanel7.SuspendLayout();
            this.ultraPanel9.ClientArea.SuspendLayout();
            this.ultraPanel9.SuspendLayout();
            this.ultraPanel8.ClientArea.SuspendLayout();
            this.ultraPanel8.SuspendLayout();
            this.ultraPanel6.ClientArea.SuspendLayout();
            this.ultraPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOSThayDoiGiaBanTheo)).BeginInit();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbGiaApDung)).BeginInit();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTieuChi)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.UltraGridTinhGiaBan);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel4);
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel3);
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel2);
            this.splitContainer1.Panel2.Controls.Add(this.ultraPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(1097, 697);
            this.splitContainer1.SplitterDistance = 545;
            this.splitContainer1.TabIndex = 0;
            // 
            // UltraGridTinhGiaBan
            // 
            this.UltraGridTinhGiaBan.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.UltraGridTinhGiaBan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UltraGridTinhGiaBan.Location = new System.Drawing.Point(0, 0);
            this.UltraGridTinhGiaBan.Name = "UltraGridTinhGiaBan";
            this.UltraGridTinhGiaBan.Size = new System.Drawing.Size(1097, 545);
            this.UltraGridTinhGiaBan.TabIndex = 0;
            this.UltraGridTinhGiaBan.Text = "Tính giá bán";
            this.UltraGridTinhGiaBan.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.UltraGridTinhGiaBan_AfterCellUpdate);
            // 
            // ultraPanel4
            // 
            // 
            // ultraPanel4.ClientArea
            // 
            this.ultraPanel4.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.ultraPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel4.Location = new System.Drawing.Point(259, 0);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(576, 101);
            this.ultraPanel4.TabIndex = 4;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraPanel7);
            this.ultraGroupBox2.Controls.Add(this.ultraPanel6);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Etched;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(576, 101);
            this.ultraGroupBox2.TabIndex = 0;
            this.ultraGroupBox2.Text = "Thay đổi giá bán theo";
            // 
            // ultraPanel7
            // 
            // 
            // ultraPanel7.ClientArea
            // 
            this.ultraPanel7.ClientArea.Controls.Add(this.ultraPanel9);
            this.ultraPanel7.ClientArea.Controls.Add(this.ultraPanel8);
            this.ultraPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel7.Location = new System.Drawing.Point(77, 20);
            this.ultraPanel7.Name = "ultraPanel7";
            this.ultraPanel7.Size = new System.Drawing.Size(496, 78);
            this.ultraPanel7.TabIndex = 2;
            // 
            // ultraPanel9
            // 
            this.ultraPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.White;
            this.ultraPanel9.Appearance = appearance1;
            // 
            // ultraPanel9.ClientArea
            // 
            this.ultraPanel9.ClientArea.Controls.Add(this.ultraMaskSoTien);
            this.ultraPanel9.Location = new System.Drawing.Point(3, 44);
            this.ultraPanel9.Name = "ultraPanel9";
            this.ultraPanel9.Size = new System.Drawing.Size(490, 26);
            this.ultraPanel9.TabIndex = 0;
            // 
            // ultraMaskSoTien
            // 
            appearance2.TextHAlignAsString = "Right";
            this.ultraMaskSoTien.Appearance = appearance2;
            this.ultraMaskSoTien.AutoSize = false;
            this.ultraMaskSoTien.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.ultraMaskSoTien.InputMask = "{double:9.0}";
            this.ultraMaskSoTien.Location = new System.Drawing.Point(0, 0);
            this.ultraMaskSoTien.Name = "ultraMaskSoTien";
            this.ultraMaskSoTien.PromptChar = ' ';
            this.ultraMaskSoTien.Size = new System.Drawing.Size(487, 22);
            this.ultraMaskSoTien.TabIndex = 0;
            // 
            // ultraPanel8
            // 
            this.ultraPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BackColor = System.Drawing.Color.White;
            this.ultraPanel8.Appearance = appearance3;
            // 
            // ultraPanel8.ClientArea
            // 
            this.ultraPanel8.ClientArea.Controls.Add(this.UltraMaskTyLePhanTram);
            this.ultraPanel8.Location = new System.Drawing.Point(3, 12);
            this.ultraPanel8.Name = "ultraPanel8";
            this.ultraPanel8.Size = new System.Drawing.Size(490, 26);
            this.ultraPanel8.TabIndex = 0;
            // 
            // UltraMaskTyLePhanTram
            // 
            appearance4.TextHAlignAsString = "Right";
            this.UltraMaskTyLePhanTram.Appearance = appearance4;
            this.UltraMaskTyLePhanTram.AutoSize = false;
            this.UltraMaskTyLePhanTram.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.UltraMaskTyLePhanTram.InputMask = "{double:9.2}";
            this.UltraMaskTyLePhanTram.Location = new System.Drawing.Point(0, 0);
            this.UltraMaskTyLePhanTram.Name = "UltraMaskTyLePhanTram";
            this.UltraMaskTyLePhanTram.PromptChar = ' ';
            this.UltraMaskTyLePhanTram.Size = new System.Drawing.Size(487, 22);
            this.UltraMaskTyLePhanTram.TabIndex = 0;
            // 
            // ultraPanel6
            // 
            // 
            // ultraPanel6.ClientArea
            // 
            this.ultraPanel6.ClientArea.Controls.Add(this.ultraOSThayDoiGiaBanTheo);
            this.ultraPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPanel6.Location = new System.Drawing.Point(3, 20);
            this.ultraPanel6.Name = "ultraPanel6";
            this.ultraPanel6.Size = new System.Drawing.Size(74, 78);
            this.ultraPanel6.TabIndex = 1;
            // 
            // ultraOSThayDoiGiaBanTheo
            // 
            this.ultraOSThayDoiGiaBanTheo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            appearance5.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraOSThayDoiGiaBanTheo.Appearance = appearance5;
            this.ultraOSThayDoiGiaBanTheo.CheckedIndex = 0;
            this.ultraOSThayDoiGiaBanTheo.ItemOrigin = new System.Drawing.Point(5, 5);
            valueListItem5.DataValue = "Default Item";
            valueListItem5.DisplayText = "Tỷ lệ %";
            valueListItem5.Tag = "1";
            valueListItem6.DataValue = "ValueListItem1";
            valueListItem6.DisplayText = "Số tiền";
            valueListItem6.Tag = "2";
            this.ultraOSThayDoiGiaBanTheo.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem5,
            valueListItem6});
            this.ultraOSThayDoiGiaBanTheo.ItemSpacingVertical = 20;
            this.ultraOSThayDoiGiaBanTheo.Location = new System.Drawing.Point(0, 0);
            this.ultraOSThayDoiGiaBanTheo.Name = "ultraOSThayDoiGiaBanTheo";
            this.ultraOSThayDoiGiaBanTheo.Size = new System.Drawing.Size(74, 80);
            this.ultraOSThayDoiGiaBanTheo.TabIndex = 0;
            this.ultraOSThayDoiGiaBanTheo.Text = "Tỷ lệ %";
            this.ultraOSThayDoiGiaBanTheo.ValueChanged += new System.EventHandler(this.ultraOSThayDoiGiaBanTheo_ValueChanged);
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel3.Location = new System.Drawing.Point(835, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(262, 101);
            this.ultraPanel3.TabIndex = 3;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.cbbGiaApDung);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraGroupBox3.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Etched;
            this.ultraGroupBox3.Location = new System.Drawing.Point(1, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(261, 101);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.Text = "Áp dụng cho giá bán nào";
            // 
            // cbbGiaApDung
            // 
            this.cbbGiaApDung.AutoSize = false;
            this.cbbGiaApDung.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbGiaApDung.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbGiaApDung.Location = new System.Drawing.Point(61, 50);
            this.cbbGiaApDung.Name = "cbbGiaApDung";
            this.cbbGiaApDung.Size = new System.Drawing.Size(188, 22);
            this.cbbGiaApDung.TabIndex = 207;
            // 
            // ultraLabel2
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance6;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 53);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(53, 18);
            this.ultraLabel2.TabIndex = 42;
            this.ultraLabel2.Text = "Loại giá:";
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(259, 101);
            this.ultraPanel2.TabIndex = 2;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.cbbTieuChi);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Etched;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(259, 101);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thay đổi giá bán dựa trên";
            // 
            // cbbTieuChi
            // 
            this.cbbTieuChi.AutoSize = false;
            this.cbbTieuChi.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbTieuChi.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbTieuChi.Location = new System.Drawing.Point(65, 48);
            this.cbbTieuChi.Name = "cbbTieuChi";
            this.cbbTieuChi.Size = new System.Drawing.Size(188, 22);
            this.cbbTieuChi.TabIndex = 206;
            // 
            // ultraLabel1
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance7;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 52);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(53, 18);
            this.ultraLabel1.TabIndex = 42;
            this.ultraLabel1.Text = "Tiêu chí:";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraPanel5);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 101);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1097, 47);
            this.ultraPanel1.TabIndex = 1;
            // 
            // ultraPanel5
            // 
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraButCat);
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraButHoan);
            this.ultraPanel5.ClientArea.Controls.Add(this.UltraDong);
            this.ultraPanel5.ClientArea.Controls.Add(this.UltraButTinhGiaBan);
            this.ultraPanel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel5.Location = new System.Drawing.Point(651, 0);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(446, 47);
            this.ultraPanel5.TabIndex = 1;
            // 
            // ultraButCat
            // 
            appearance8.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.ultraButCat.Appearance = appearance8;
            this.ultraButCat.Enabled = false;
            this.ultraButCat.Location = new System.Drawing.Point(161, 6);
            this.ultraButCat.Name = "ultraButCat";
            this.ultraButCat.Size = new System.Drawing.Size(90, 30);
            this.ultraButCat.TabIndex = 0;
            this.ultraButCat.Text = "Lưu";
            this.ultraButCat.Click += new System.EventHandler(this.ultraButCat_Click);
            // 
            // ultraButHoan
            // 
            appearance9.Image = global::Accounting.Properties.Resources.repeat;
            this.ultraButHoan.Appearance = appearance9;
            this.ultraButHoan.Enabled = false;
            this.ultraButHoan.Location = new System.Drawing.Point(257, 6);
            this.ultraButHoan.Name = "ultraButHoan";
            this.ultraButHoan.Size = new System.Drawing.Size(90, 30);
            this.ultraButHoan.TabIndex = 0;
            this.ultraButHoan.Text = "Quay lại";
            this.ultraButHoan.Click += new System.EventHandler(this.ultraButHoan_Click);
            // 
            // UltraDong
            // 
            appearance10.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.UltraDong.Appearance = appearance10;
            this.UltraDong.Location = new System.Drawing.Point(353, 6);
            this.UltraDong.Name = "UltraDong";
            this.UltraDong.Size = new System.Drawing.Size(90, 30);
            this.UltraDong.TabIndex = 0;
            this.UltraDong.Text = "Đóng";
            this.UltraDong.Click += new System.EventHandler(this.UltraDong_Click);
            // 
            // UltraButTinhGiaBan
            // 
            appearance11.Image = global::Accounting.Properties.Resources.calculator;
            this.UltraButTinhGiaBan.Appearance = appearance11;
            this.UltraButTinhGiaBan.Location = new System.Drawing.Point(47, 6);
            this.UltraButTinhGiaBan.Name = "UltraButTinhGiaBan";
            this.UltraButTinhGiaBan.Size = new System.Drawing.Size(108, 30);
            this.UltraButTinhGiaBan.TabIndex = 0;
            this.UltraButTinhGiaBan.Text = "Tính Giá Bán";
            this.UltraButTinhGiaBan.Click += new System.EventHandler(this.UltraButTinhGiaBan_Click);
            // 
            // FSACalculateTheCost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ClientSize = new System.Drawing.Size(1097, 697);
            this.Controls.Add(this.splitContainer1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSACalculateTheCost";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tính giá bán";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridTinhGiaBan)).EndInit();
            this.ultraPanel4.ClientArea.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraPanel7.ClientArea.ResumeLayout(false);
            this.ultraPanel7.ResumeLayout(false);
            this.ultraPanel9.ClientArea.ResumeLayout(false);
            this.ultraPanel9.ResumeLayout(false);
            this.ultraPanel8.ClientArea.ResumeLayout(false);
            this.ultraPanel8.ResumeLayout(false);
            this.ultraPanel6.ClientArea.ResumeLayout(false);
            this.ultraPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraOSThayDoiGiaBanTheo)).EndInit();
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbGiaApDung)).EndInit();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbTieuChi)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private Infragistics.Win.UltraWinGrid.UltraGrid UltraGridTinhGiaBan;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton ultraButCat;
        private Infragistics.Win.Misc.UltraButton ultraButHoan;
        private Infragistics.Win.Misc.UltraButton UltraDong;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private Infragistics.Win.Misc.UltraButton UltraButTinhGiaBan;
        private Infragistics.Win.Misc.UltraPanel ultraPanel6;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOSThayDoiGiaBanTheo;
        private Infragistics.Win.Misc.UltraPanel ultraPanel7;
        private Infragistics.Win.Misc.UltraPanel ultraPanel8;
        private Infragistics.Win.Misc.UltraPanel ultraPanel9;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit ultraMaskSoTien;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit UltraMaskTyLePhanTram;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbGiaApDung;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbTieuChi;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
    }
}