﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj.FSACalculateTheCost;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSACalculateTheCost : CustormForm
    {
        #region Khỏi tạo
        List<CalculateCost> lstCalculateTheCosts = new List<CalculateCost>();
        List<CalculateCost> lstCalculateTheCostsLuu = new List<CalculateCost>();
        private readonly IMaterialGoodsService _IMaterialGoodsService;

        #endregion
        #region Main
        public FSACalculateTheCost()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            LoadForm();
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region LoadFrom

        void LoadForm()
        {
            //add by cuongpv
            ValueListItem[] lstChiTieu = new[] { new ValueListItem(0, "Đơn giá mua"), new ValueListItem(1, "Giá bán cố định"), new ValueListItem(2, "Giá bán 1"), new ValueListItem(3, "Giá bán 2"), new ValueListItem(4, "Giá bán 3"),
            new ValueListItem(5, "Giá bán sau thuế 1"), new ValueListItem(6, "Giá bán sau thuế 2"), new ValueListItem(7, "Giá bán sau thuế 3")};
            cbbTieuChi.Items.AddRange(lstChiTieu);
            cbbTieuChi.SelectedIndex = 0;

            ValueListItem[] lstGiaApDung = new[] { new ValueListItem(0, "Giá bán cố định"), new ValueListItem(1, "Giá bán 1"), new ValueListItem(2, "Giá bán 2"), new ValueListItem(3, "Giá bán 3") };
            cbbGiaApDung.Items.AddRange(lstGiaApDung);
            cbbGiaApDung.SelectedIndex = 0;
            //end add by cuongpv
            //lstCalculateTheCostsLuu = _IMaterialGoodsService.GetListCalculateCosts();
            lstCalculateTheCosts = _IMaterialGoodsService.GetListCalculateCosts().OrderBy(x=>x.MaterialGoodsCode).ToList();
            ViewDsach(UltraGridTinhGiaBan, lstCalculateTheCosts);

            if (ultraOSThayDoiGiaBanTheo.CheckedIndex == 0)
            {
                UltraMaskTyLePhanTram.Enabled = true;
                ultraMaskSoTien.Enabled = false;
            }
            else
            {
                UltraMaskTyLePhanTram.Enabled = false;
                ultraMaskSoTien.Enabled = true;
            }
        }
        #endregion
        #region Cấu hình mẫu giao diện hiện thị danh sách Chiết khấu Nợ và Triết khấu Trả tiền/Thue tiền
        private List<TemplateColumn> MauDanhSach()
        {
            return new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "Status",
                                   ColumnCaption = "",
                                   ColumnWidth = 60,
                                   ColumnMaxWidth = 60,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsCategory",
                                   ColumnCaption = "Loại VTHH",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsCode",
                                   ColumnCaption = "Mã vật tư",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsName",
                                   ColumnCaption = "Tên vật tư",
                                   ColumnWidth = 250,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "PurchasePrice",
                                   ColumnCaption = "Đơn giá mua",
                                   ColumnWidth = 90,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 5,
                                   IsReadOnly = true
                               },
                            new TemplateColumn
                            {
                                ColumnName = "FixedSalePrice",
                                ColumnCaption = "Giá bán cố định",
                                ColumnWidth = 90,
                                IsVisibleCbb = true,
                                VisiblePosition = 6,
                                IsReadOnly = false


                            },
                            new TemplateColumn
                            {
                                ColumnName = "SalePrice",
                                ColumnCaption = "Giá bán 1",
                                ColumnWidth = 90,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 7,
                                IsReadOnly = false
                            },
                            new TemplateColumn
                            {
                                ColumnName = "SalePrice2",
                                ColumnCaption = "Giá bán 2",
                                ColumnWidth = 90,
                                IsVisibleCbb = true,
                                VisiblePosition = 8,
                                IsReadOnly = false
                            },
                            new TemplateColumn
                            {

                                ColumnName = "SalePrice3",
                                ColumnCaption = "Giá bán 3",
                                ColumnWidth = 90,
                                IsVisibleCbb = true,
                                VisiblePosition = 9,
                                IsReadOnly = false

                            },
                            new TemplateColumn
                            {
                                ColumnName = "SalePriceAfterTax",
                                ColumnCaption = "Giá bán sau thuế 1",
                                ColumnWidth = 120,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 10,
                                IsReadOnly = false
                            },
                            new TemplateColumn
                            {
                                ColumnName = "SalePriceAfterTax2",
                                ColumnCaption = "Giá bán sau thuế 2",
                                ColumnWidth = 120,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 11,
                                IsReadOnly = false
                            },
                            new TemplateColumn
                            {
                                ColumnName = "SalePriceAfterTax3",
                                ColumnCaption = "Giá bán sau thuế 3",
                                ColumnWidth = 120,
                                IsVisible = true,
                                IsVisibleCbb = true,
                                VisiblePosition = 12,
                                IsReadOnly = false
                            },
                       };
        }
        
        private void ViewDsach(UltraGrid uGrid, List<CalculateCost> model)
        {
            uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            uGrid.SetDataBinding(model, "");
            Utils.ConfigGrid(uGrid, "", MauDanhSach());
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            // xét style cho các cột có dạng combobox
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(uGrid.Rows, false);

            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["PurchasePrice"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["FixedSalePrice"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePrice"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePriceAfterTax"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePrice2"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePriceAfterTax2"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePrice3"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePriceAfterTax3"], ConstDatabase.Format_TienVND);


        }
        #endregion
        
        private void UltraButTinhGiaBan_Click(object sender, EventArgs e)
        {
            if (!lstCalculateTheCosts.Any(c => c.Status))
            {
                MSG.Warning("Bạn phải chọn ít nhất một vật tư để tính giá !");
                return;
            }
            if (UltraMaskTyLePhanTram.Value.ToString() == "" && ultraMaskSoTien.Value.ToString() == "")
            {
                MSG.Warning("Bạn cần nhập dữ liệu tỷ lệ hoặc số tiền mà giá bán sẽ thay đổi theo !");
                return;
            }

            lstCalculateTheCosts = lstCalculateTheCosts.Where(x => x.Status).ToList();

            if(lstCalculateTheCosts.Count > 0)
            {
                int iTieuChi = cbbTieuChi.SelectedIndex;
                int iGiaApDung = cbbGiaApDung.SelectedIndex;

                decimal tylePhanTram = 0;
                if (UltraMaskTyLePhanTram.Value.ToString() != "")
                    tylePhanTram = Convert.ToDecimal(UltraMaskTyLePhanTram.Value);

                decimal soTien = 0;
                if (ultraMaskSoTien.Value.ToString() != "")
                    soTien = Convert.ToDecimal(ultraMaskSoTien.Value);

                decimal giaTieuChi = 0;
                decimal giaMoi = 0;

                foreach (var itemUpdateGia in lstCalculateTheCosts)
                {
                    // 0: Đơn giá mua (PurchasePrice), 1: Giá bán cố định (FixedSalePrice), 2: Giá ban 1 (SalePrice), 3: Giá bán 2 (SalePrice2)
                    // 4: Giá bán 3 (SalePrice3), 5: Giá bán sau thuế 1 (SalePriceAfterTax), 6: Giá bán sau thuế 2 (SalePriceAfterTax2), 7: Giá bán sau thuế 3 (SalePriceAfterTax3)
                    // lay cot tieu chi tinh gia
                    switch (iTieuChi)
                    {
                        case 0:
                            if (itemUpdateGia.PurchasePrice != null)
                                giaTieuChi = (decimal)itemUpdateGia.PurchasePrice;
                            break;
                        case 1:
                            if (itemUpdateGia.FixedSalePrice != null)
                                giaTieuChi = (decimal)itemUpdateGia.FixedSalePrice;
                            break;
                        case 2:
                            if (itemUpdateGia.SalePrice != null)
                                giaTieuChi = (decimal)itemUpdateGia.SalePrice;
                            break;
                        case 3:
                            if (itemUpdateGia.SalePrice2 != null)
                                giaTieuChi = (decimal)itemUpdateGia.SalePrice2;
                            break;
                        case 4:
                            if (itemUpdateGia.SalePrice3 != null)
                                giaTieuChi = (decimal)itemUpdateGia.SalePrice3;
                            break;
                        case 5:
                            giaTieuChi = itemUpdateGia.SalePriceAfterTax;
                            break;
                        case 6:
                            giaTieuChi = itemUpdateGia.SalePriceAfterTax2;
                            break;
                        case 7:
                            giaTieuChi = itemUpdateGia.SalePriceAfterTax3;
                            break;
                        default:
                            break;
                    }
                    //tinh gia moi
                    if (ultraOSThayDoiGiaBanTheo.CheckedIndex == 0)
                    {
                        if (tylePhanTram > 0)
                            giaMoi = (giaTieuChi + (giaTieuChi * (tylePhanTram / 100)));
                        else
                            giaMoi = giaTieuChi;
                    }
                    else
                    {
                        if (soTien > 0)
                            giaMoi = giaTieuChi + soTien;
                        else
                            giaMoi = giaTieuChi;
                    }
                    //ap gia moi vao
                    // 0: Giá bán cố định (FixedSalePrice), 1: Giá ban 1 (SalePrice), 2: Giá bán 2 (SalePrice2), 3: Giá bán 3 (SalePrice3)
                    switch (iGiaApDung)
                    {
                        case 0:
                            itemUpdateGia.FixedSalePrice = giaMoi;
                            break;
                        case 1:
                            itemUpdateGia.SalePrice = giaMoi;
                            break;
                        case 2:
                            itemUpdateGia.SalePrice2 = giaMoi;
                            break;
                        case 3:
                            itemUpdateGia.SalePrice3 = giaMoi;
                            break;
                        default:
                            break;
                    }
                }
            }

            UltraGridTinhGiaBan.Refresh();

            MSG.Information("Tính giá bán thành công !");

        }

        private void ultraButCat_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            try
            {
                _IMaterialGoodsService.BeginTran();
                List<MaterialGoods> lstMaterialGoodses = new List<MaterialGoods>();
                lstMaterialGoodses = Accounting.Core.Utils.Convert_FromParent_ToChild<CalculateCost, MaterialGoods>(lstCalculateTheCosts.Where(c => c.Status), lstMaterialGoodses);
                foreach (var x in lstMaterialGoodses)
                {
                    _IMaterialGoodsService.Update(Core.Utils.GetAgainObject(x));
                }
                _IMaterialGoodsService.CommitTran();

                lstCalculateTheCosts = _IMaterialGoodsService.GetListCalculateCosts().OrderBy(x => x.MaterialGoodsCode).ToList();
                ViewDsach(UltraGridTinhGiaBan, lstCalculateTheCosts);

                if (ultraOSThayDoiGiaBanTheo.CheckedIndex == 0)
                {
                    UltraMaskTyLePhanTram.Enabled = true;
                    ultraMaskSoTien.Enabled = false;
                }
                else
                {
                    UltraMaskTyLePhanTram.Enabled = false;
                    ultraMaskSoTien.Enabled = true;
                }
                ultraButCat.Enabled = false;
                ultraButHoan.Enabled = false;

                WaitingFrm.StopWaiting();
                MessageBox.Show("Bạn đã cất thành công");
            }
            catch (Exception)
            {
                WaitingFrm.StopWaiting();
                MessageBox.Show("Cất không thành công");
                _IMaterialGoodsService.RolbackTran();
            }
        }

        private void ultraButHoan_Click(object sender, EventArgs e)
        {
            LoadForm();
            ultraButCat.Enabled = false;
            ultraButHoan.Enabled = false;
        }

        private void UltraDong_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void ultraOSThayDoiGiaBanTheo_ValueChanged(object sender, EventArgs e)
        {
            if(ultraOSThayDoiGiaBanTheo.CheckedIndex==0)
            {
                UltraMaskTyLePhanTram.Enabled = true;
                ultraMaskSoTien.Enabled = false;
            }
            else
            {
                UltraMaskTyLePhanTram.Enabled = false;
                ultraMaskSoTien.Enabled = true;
            }
        }

        private void UltraGridTinhGiaBan_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == "Status")
            {
                int countCheck = 0;
                countCheck = lstCalculateTheCosts.Where(c => c.Status).Count();
                if (countCheck > 0)
                {
                    ultraButCat.Enabled = true;
                    ultraButHoan.Enabled = true;
                }
                else
                {
                    ultraButCat.Enabled = false;
                    ultraButHoan.Enabled = false;
                }
            }
        }
    }
}
