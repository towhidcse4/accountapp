﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Frm.FBusiness.SalePrice.FSABill
{
    public partial class UploadFileExcel : Form
    {
        string linkFile;
        List<SABill> lstSABill = new List<SABill>();
        ExcelSA1 excel;
        public bool reset = false;
        public UploadFileExcel()
        {
            InitializeComponent();
            btnCheckData.Visible = false;
        }


        private void btnBrowse_Click(object sender, EventArgs e)
        {

            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"

            };

            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                try
                {
                    double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 1024;
                    if (fileSize > 102400)
                    {
                        MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
                        return;
                    }
                    txtFileName.Value = openFile.FileName;
                    //using (FileStream file = new FileStream(openFile.FileName, FileMode.Open, FileAccess.Read))
                    //{


                    //}

                }
                catch (Exception ex)
                {
                    MSG.Error("File này đang được mở bởi ứng dụng khác");
                    return;
                }

            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            reset = false;
            if (string.IsNullOrEmpty(txtFileName.Text))
            {
                MSG.Error("Bạn chưa chọn file hóa đơn");
                return;
            }
            if (!File.Exists(txtFileName.Text))
            {
                MSG.Error("File không tồn tại");
                return;
            }

            if (new FileInfo(txtFileName.Text).Length == 0)
            {
                MSG.Error("File trống");
                return;
            }
            string extenstion = Path.GetExtension(txtFileName.Text);
            if (extenstion == ".xlsx" || extenstion == ".xls")
            {

            }
            else
            {
                MSG.Error("File không đúng định dạng file Excel");
                return;
            }
            double fileSize = (double)new System.IO.FileInfo(txtFileName.Text).Length / 1024;
            if (fileSize > 102400)
            {
                MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
                return;
            }
            WaitingFrm.StartWaiting();
            try
            {
                excel = new ExcelSA1(txtFileName.Text);
            }
            catch
            {
                WaitingFrm.StopWaiting();
                MSG.Error("File đang được mở");
                return;
            }

            //List<String> sheetNames = excel.ReadSheetName();
            excel.ChooseSheet(1);
            Dictionary<String, int> header = excel.ReadHeader();
            foreach (var item in excel.HEADER)
            {
                if (!header.ContainsKey(item))
                {
                    excel.Exit();
                    WaitingFrm.StopWaiting();
                    if (MSG.Question("Mẫu file không đúng. Bạn có muốn tải file mẫu không?") == DialogResult.Yes)
                    {
                        SaveFileDialog sf = new SaveFileDialog
                        {
                            FileName = "Upload_Invoice.xlsx",
                            AddExtension = true,
                            Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                        };

                        if (sf.ShowDialog() == DialogResult.OK)
                        {
                            string path = System.IO.Path.GetDirectoryName(
                                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                            string filePath = string.Format("{0}\\TemplateResource\\excel\\Upload_Invoice.xlsx", path);
                            ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                            MSG.Information("Tải xuống thành công");
                        }

                        return;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            lstSABill = excel.GetObject(header);
            if (lstSABill.Count() > 0)
            {
                //Utils.ISABillService.BeginTran();
                List<SABill> eInvoices = lstSABill.Where(n => n.InvoiceForm == 2 && n.StatusInvoice == 0).ToList();
                if (eInvoices.Count > 0)
                {
                    if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data == "1")
                    {
                        SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                        if (supplierService != null && supplierService.SupplierServiceCode == "SDS")
                        {
                            RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                            if (eInvoices.GroupBy(n => new { n.InvoiceTemplate, n.InvoiceSeries }).ToList().Count > 1)
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error("Hóa đơn điện tử không cùng mẫu số và kí hiệu");
                                return;
                            }
                            if (eInvoices.Count > 5000)
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error("Số lượng hóa đơn điện tử không được vượt quá 5000");
                                return;
                            }
                            var request = new Request();
                            request.Pattern = eInvoices.FirstOrDefault().InvoiceTemplate;
                            request.Serial = eInvoices.FirstOrDefault().InvoiceSeries;
                            request.XmlData = Utils.xmlDataEInvoice(eInvoices);
                            Response response = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "TaoHoaDonChuaKiSo").ApiPath, request);
                            if (response.Status != 2)
                            {
                                WaitingFrm.StopWaiting();
                                MSG.Error(response.Message);
                                return;
                            }
                        }
                    }
                    else
                    {
                        WaitingFrm.StopWaiting();
                        if (MSG.Question("Chưa tích hợp hóa đơn. Bạn có muốn tiếp tục lưu hóa đơn?") != DialogResult.Yes)
                        {
                            return;
                        }
                    }
                }
                string query = "";
                try
                {
                    query += "Set IDENTITY_INSERT SABillDetail on";
                    foreach (var item in lstSABill)
                    {
                        //item.ID = Guid.NewGuid();
                        item.TypeID = 326;
                        //Utils.ISABillService.CreateNew(item);
                        query += " " + string.Format("INSERT INTO SABill(" +
                            "ID," +
                            "BranchID," +
                            "TypeID," +
                            "AccountingObjectID," +
                            "AccountingObjectName," +
                            "AccountingObjectAddress," +
                            "InvoiceType," +
                            "InvoiceDate," +
                            "InvoiceNo," +
                            "InvoiceSeries," +
                            "InvoiceForm," +
                            "InvoiceTypeID," +
                            "InvoiceTemplate," +
                            "CurrencyID," +
                            "ExchangeRate," +
                            "TotalAmount," +
                            "TotalAmountOriginal," +
                            "TotalDiscountAmount," +
                            "TotalDiscountAmountOriginal," +
                            "TotalVATAmount," +
                            "TotalVATAmountOriginal," +
                            "CustomProperty1," +
                            "CustomProperty2," +
                            "CustomProperty3," +
                            "RefDateTime," +
                            "TemplateID," +
                            "Reason," +
                            "ListNo," +
                            "ListDate," +
                            "CompanyTaxCode," +
                            "ContactName," +
                            "PaymentMethod," +
                            "IsAttachList," +
                            "ListCommonNameInventory," +
                            "BankAccountDetailID," +
                            "StatusInvoice," +
                            "StatusSendMail," +
                            "DateSendMail," +
                            "Email," +
                            "StatusConverted," +
                            "IDReplaceInv," +
                            "IDAdjustInv," +
                            "Type, " +
                            "AccountingObjectBankAccount, " +
                            "AccountingObjectBankName) " +
                            "VALUES ('{0}',{1},N'{2}',{3},N'{4}',N'{5}',N'{6}','{7}',N'{8}',N'{9}','{10}','{11}',N'{12}',N'{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',N'{21}',N'{22}',N'{23}','{24}',{25},N'{26}',{27},{28},N'{29}',N'{30}',N'{31}','{32}',N'{33}',{34},'{35}','{36}',{37},N'{38}','{39}',{40},{41},'{42}',N'{43}',N'{44}')",
                            item.ID,
                            item.BranchID == null ? "NULL" : "'" + item.BranchID.ToString() + "'",
                            item.TypeID,
                            item.AccountingObjectID == null ? "NULL" : "'" + item.AccountingObjectID.ToString() + "'",
                            item.AccountingObjectName,
                            item.AccountingObjectAddress,
                            item.InvoiceType,
                            (item.InvoiceDate ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss.fffffff"),
                            item.InvoiceNo,
                            item.InvoiceSeries,
                            item.InvoiceForm,
                            item.InvoiceTypeID == null ? "NULL" : item.InvoiceTypeID.ToString(),
                            item.InvoiceTemplate,
                            item.CurrencyID, item.ExchangeRate.ToString().Replace(",", "."),
                            item.TotalAmount.ToString().Replace(",", "."),
                            item.TotalAmountOriginal.ToString().Replace(",", "."),
                            item.TotalDiscountAmount.ToString().Replace(",", "."),
                            item.TotalDiscountAmountOriginal.ToString().Replace(",", "."),
                            item.TotalVATAmount.ToString().Replace(",", "."),
                            item.TotalVATAmountOriginal.ToString().Replace(",", "."),
                            item.CustomProperty1,
                            item.CustomProperty2,
                            item.CustomProperty3,
                            (item.RefDateTime ?? DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss.fffffff"),
                            item.TemplateID == null ? "NULL" : "'" + item.TemplateID.ToString() + "'",
                            item.Reason,
                            "NULL",
                            "NULL",
                            item.CompanyTaxCode,
                            item.ContactName,
                            item.PaymentMethod,
                            null,
                            item.ListCommonNameInventory,
                            item.BankAccountDetailID == null ? "NULL" : "'" + item.BankAccountDetailID.ToString() + "'",
                            item.StatusInvoice,
                            0,
                            "NULL",
                            "NULL",
                            0,
                            "NULL",
                            "NULL",
                            item.Type,
                            item.AccountingObjectBankAccount,
                            item.AccountingObjectBankName);
                        foreach (var itemD in item.SABillDetails)
                        {
                            query += " " + string.Format("INSERT INTO SABillDetail(" +
                                "ID," +
                                "SABillID," +
                                "MaterialGoodsID," +
                                "RepositoryID," +
                                "Description," +
                                "DebitAccount," +
                                "CreditAccount," +
                                "Unit," +
                                "Quantity," +
                                "UnitPrice," +
                                "UnitPriceOriginal," +
                                "Amount," +
                                "AmountOriginal," +
                                "DiscountRate," +
                                "DiscountAmount," +
                                "DiscountAmountOriginal," +
                                "DiscountAccount," +
                                "VATRate," +
                                "VATAmount," +
                                "VATAmountOriginal," +
                                "VATAccount," +
                                "RepositoryAccount," +
                                "CostAccount," +
                                "AccountingObjectID," +
                                "CostSetID," +
                                "ContractID," +
                                "StatisticsCodeID," +
                                "DepartmentID," +
                                "ExpenseItemID," +
                                "BudgetItemID," +
                                "CustomProperty1," +
                                "CustomProperty2," +
                                "CustomProperty3," +
                                "OrderPriority," +
                                "DetailID) " +
                                "VALUES ('{0}','{1}',{2},{3},N'{4}',{5},{6},N'{7}',N'{8}',N'{9}',N'{10}',N'{11}',N'{12}',N'{13}',N'{14}',N'{15}',N'{16}',N'{17}',N'{18}',N'{19}',{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},'{33}',{34})",
                                Guid.NewGuid(),
                                itemD.SABillID,
                                itemD.MaterialGoodsID == null ? "NULL" : "'" + itemD.MaterialGoodsID.ToString() + "'",
                                "NULL",
                                itemD.Description.Replace("'", "'+CHAR(39)+N'"),
                                "NULL",
                                "NULL",
                                itemD.Unit,
                                itemD.Quantity,
                                itemD.UnitPrice.ToString().Replace(",", "."),
                                itemD.UnitPriceOriginal.ToString().Replace(",", "."),
                                itemD.Amount.ToString().Replace(",", "."),
                                itemD.AmountOriginal.ToString().Replace(",", "."),
                                itemD.DiscountRate.ToString().Replace(",", "."),
                                itemD.DiscountAmount.ToString().Replace(",", "."),
                                itemD.DiscountAmountOriginal.ToString().Replace(",", "."),
                                itemD.DiscountAccount,
                                itemD.VATRate,
                                itemD.VATAmount.ToString().Replace(",", "."),
                                itemD.VATAmountOriginal.ToString().Replace(",", "."),
                                "NULL",
                                "NULL",
                                "NULL",
                                itemD.AccountingObjectID == null ? "NULL" : "'" + itemD.AccountingObjectID.ToString() + "'",
                                itemD.CostSetID == null ? "NULL" : "'" + itemD.CostSetID.ToString() + "'",
                                itemD.ContractID == null ? "NULL" : "'" + itemD.ContractID.ToString() + "'",
                                itemD.StatisticsCodeID == null ? "NULL" : "'" + itemD.StatisticsCodeID.ToString() + "'",
                                itemD.DepartmentID == null ? "NULL" : "'" + itemD.DepartmentID.ToString() + "'",
                                itemD.ExpenseItemID == null ? "NULL" : "'" + itemD.ExpenseItemID.ToString() + "'",
                                itemD.BudgetItemID == null ? "NULL" : "'" + itemD.BudgetItemID.ToString() + "'",
                                "NULL",
                                "NULL",
                                "NULL",
                                itemD.OrderPriority,
                                "NULL");
                        }
                    }
                    query += " Set IDENTITY_INSERT SABillDetail off";

                    //Utils.ISABillService.CommitTran();
                    if (!ExecQueries(query))
                    {
                        WaitingFrm.StopWaiting();
                        return;
                    }
                    Utils.ISABillService.RolbackTran();
                    Utils.ISABillDetailService.RolbackTran();
                    WaitingFrm.StopWaiting();
                    MSG.Information("Upload hóa đơn thành công");
                    reset = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    Utils.ISABillService.RolbackTran();
                    WaitingFrm.StopWaiting();
                }
            }
            else
            {
                WaitingFrm.StopWaiting();
            }

        }
        private bool ExecQueries(string query)
        {
            string serverName = Properties.Settings.Default.ServerName;
            string database = Properties.Settings.Default.DatabaseName;
            string user = Properties.Settings.Default.UserName;
            string pass = Properties.Settings.Default.Password;

            if (serverName == null || database == null || user == null || pass == null)
            {
                return false;
            }
            SqlConnection sqlConnection = new SqlConnection("");
            if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(database) && !string.IsNullOrEmpty(pass) && !serverName.Equals("14.225.3.208") && !serverName.Equals("14.225.3.218"))
            {
                string _ConnectionString = "Data Source=" + serverName + ";Initial Catalog=" + database + ";User ID=" + user + ";Password=" + pass + "";

                sqlConnection = new SqlConnection(_ConnectionString);

                SqlTransaction transaction;
                try
                {
                    sqlConnection.Open();
                }
                catch (Exception)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning("Máy chủ SQL không tồn tại. Vui lòng chọn máy chủ SQL khác.");
                    return false;
                }
                transaction = sqlConnection.BeginTransaction();
                try
                {
                    //string q = "";
                    //foreach (var query in sqlqueries)
                    //{
                    //    q += query + " ";
                    //}
                    new SqlCommand(query, sqlConnection, transaction).ExecuteNonQuery();
                    transaction.Commit();
                    transaction.Dispose();
                    Utils.ISABillService.UnbindSession(Utils.ListSABill);
                    Utils.ClearCacheByType<SABill>();
                    Utils.ISABillService.UnbindSession(Utils.ListSABillDetail);
                    Utils.ClearCacheByType<SABillDetail>();
                    //UnbindSess
                }
                catch (SqlException sqlError)
                {
                    WaitingFrm.StopWaiting();
                    MSG.Warning(sqlError.ToString());
                    transaction.Rollback();
                    return false;
                }
                try
                {
                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }
                catch
                {

                }
            }
            return true;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCheckData_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (string.IsNullOrEmpty(txtFileName.Text))
            //    {
            //        MSG.Error("Bạn chưa chọn file hóa đơn");
            //        return;
            //    }
            //    if (!File.Exists(txtFileName.Text))
            //    {
            //        MSG.Error("File không tồn tại");
            //        return;
            //    }

            //    if (new FileInfo(txtFileName.Text).Length == 0)
            //    {
            //        MSG.Error("File trống");
            //        return;
            //    }
            //    string extenstion = Path.GetExtension(txtFileName.Text);
            //    if (extenstion == ".xlsx" || extenstion == ".xls")
            //    {

            //    }
            //    else
            //    {
            //        MSG.Error("File không đúng định dạng file Excel");
            //        return;
            //    }

            //    double fileSize = (double)new System.IO.FileInfo(txtFileName.Text).Length / 1024;
            //    if (fileSize > 102400)
            //    {
            //        MSG.Error("Dung lượng file vượt quá mức cho phép (100MB)");
            //        return;
            //    }
            //    WaitingFrm.StartWaiting();
            //    excel = new ExcelSA(txtFileName.Text);

            //    List<String> sheetNames = excel.ReadSheetName();
            //    excel.ChooseSheet(1);
            //    Dictionary<String, int> header = excel.ReadHeader();
            //    foreach (var item in excel.HEADER)
            //    {
            //        if (!header.ContainsKey(item))
            //        {
            //            excel.Exit();
            //            WaitingFrm.StopWaiting();
            //            if (MSG.Question("Mẫu file không đúng. Bạn có muốn tải file mẫu không?") == DialogResult.Yes)
            //            {
            //                SaveFileDialog sf = new SaveFileDialog
            //                {
            //                    FileName = "Upload_Invoice.xlsx",
            //                    AddExtension = true,
            //                    Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
            //                };

            //                if (sf.ShowDialog() == DialogResult.OK)
            //                {
            //                    string path = System.IO.Path.GetDirectoryName(
            //                            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            //                    string filePath = string.Format("{0}\\TemplateResource\\excel\\Upload_Invoice.xlsx", path);
            //                    ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
            //                    MSG.Information("Tải xuống thành công");
            //                }

            //                return;
            //            }
            //            else
            //            {
            //                return;
            //            }
            //        }
            //    }
            //    lstSABill = excel.GetObject(header);
            //    WaitingFrm.StopWaiting();
            //    if (lstSABill.Count() > 0)
            //    {
            //        MSG.Information("Dữ liệu fill vào đã đúng ");
            //    }
            //    else
            //    {
            //    }


            //}
            //catch(Exception ex)
            //{
            //    WaitingFrm.StopWaiting();
            //}

        }

        private void linkUploadInvoice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog
            {
                FileName = "Upload_Invoice.xlsx",
                AddExtension = true,
                Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
            };

            if (sf.ShowDialog() == DialogResult.OK)
            {
                string path = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
                string filePath = string.Format("{0}\\TemplateResource\\excel\\Upload_Invoice.xlsx", path);
                ResourceHelper.MakeFileOutOfAStream(filePath, sf.FileName);
                MSG.Information("Tải xuống thành công");
            }
        }

        private void txtFileName_ValueChanged(object sender, EventArgs e)
        {
        }
    }

}
