﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.Frm.FBusiness.SalePrice.FSABill
{
    public class ExcelSA1
    {
        public readonly List<String> HEADER = new List<String> { "MaHD", "HinhThucHD", "LoaiHD", "MauHD", "KyHieuHD", "SoHD", "NgayHD", "HinhthucTT", "LoaiTien", "TrangThaiHD", "MaDoiTuong", "TenDoiTuong", "DiaChi", "MST", "TenNguoiLienHe", "DienGiai", "MaHang", "TenHang", "DVT", "SoLuong", "DonGia", "ThanhTien", "TyLeCK", "TienCK", "ThueSuat", "TienThue", "TyGia", "SoTK", "TenNganHang" };
        string path = "";
        XLWorkbook wb;
        IXLWorksheet ws;
        IXLRange xlRange;
        List<string> lstMauHD = new List<string>();
        List<string> lstKyHieuHD = new List<string>();
        List<string> lstMaDoiTg = new List<string>();
        List<string> lstLoaiTien = new List<string>();
        List<string> lstSoHD = new List<string>();
        List<string> invoiceTPL = new List<string> { "Hóa đơn điện tử", "Hóa đơn đặt in", "Hóa đơn tự in" };
        List<string> listLoaiHD = new List<string> { "01/ hoặc 02/", "01GTKT", "02GTTT", "03XKNB", "04HGDL", "06HDXK", "07KPTQ" };
        List<string> lstTTHD = new List<string> { "Hóa đơn mới tạo lập", "Hóa đơn có chữ ký số", "Hóa đơn bị thay thế", "Hóa đơn bị điều chỉnh", "Hóa đơn bị hủy" };
        List<string> lstTTHD1 = new List<string> { "Hóa đơn có chữ ký số", "Hóa đơn bị thay thế", "Hóa đơn bị điều chỉnh", "Hóa đơn bị hủy" };
        public static Dictionary<int, List<int>> ErrCell = new Dictionary<int, List<int>>();
        public List<InvoiceCheck> lstHDCheck = new List<InvoiceCheck>();
        public void ChooseSheet(int sheet)
        {
            ws = wb.Worksheet(1);
            //xlRange = ws.RangeUsed();
        }
        public ExcelSA1(string path)
        {
            if (Utils.ListTT153Report.Count() > 0)
            {
                lstMauHD = Utils.ListTT153Report.Select(n => n.InvoiceTemplate).ToList();
                foreach (var item in Utils.ListTT153Report)
                {
                    List<int> lstShd = new List<int>();
                    var lst1 = Utils.ListSAInvoice.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null);
                    var lst2 = Utils.ListSABill.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null);
                    var lst3 = Utils.ListSAReturn.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.TypeID == 340 && n.InvoiceNo != "" && n.InvoiceNo != null);
                    var lst4 = Utils.ListPPDiscountReturn.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.TypeID == 220 && n.InvoiceNo != "" && n.InvoiceNo != null);
                    if (lst1.Count() > 0)
                    {
                        lstShd.Add(lst1.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lst2.Count() > 0)
                    {
                        lstShd.Add(lst2.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lst3.Count() > 0)
                    {
                        lstShd.Add(lst3.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lst4.Count() > 0)
                    {
                        lstShd.Add(lst4.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lstShd.Count() > 0)
                    {
                        lstHDCheck.Add(new InvoiceCheck
                        {
                            InvoiceTemplate = item.InvoiceTemplate,
                            InvoiceSeries = item.InvoiceSeries,
                            InvoiceNoMax = lstShd.Max()
                        });
                    }
                }
            }
            if (Utils.ListAccountingObject.Count() > 0)
            {
                lstMaDoiTg = Utils.ListAccountingObject.Select(n => n.AccountingObjectCode).ToList();
            }
            if (Utils.ListCurrency.Count() > 0)
            {
                lstLoaiTien = Utils.ListCurrency.Select(n => n.ID).ToList();
            }
            this.path = path;
            wb = new XLWorkbook(@path);
            //wb.Activate();
        }
        public void Exit()
        {
            wb.Dispose();
            ws.Dispose();
            releaseObject(ws);
            releaseObject(wb);
        }

        public List<SABill> GetObject(Dictionary<string, int> headers)
        {
            List<SABill> lst = new List<SABill>();
            ErrCell = new Dictionary<int, List<int>>();
            int MaxOrderPriority = 0;
            if (Utils.ListSABillDetail.Count() > 0)
            {
                MaxOrderPriority = (int)Utils.ListSABillDetail.Max(n => n.OrderPriority);
            }
            var nonEmptyDataRows = ws.RowsUsed();
            DateTime now = DateTime.Now;
            int numberRow = nonEmptyDataRows.Count();
            if (numberRow < 2)
            {
                WaitingFrm.StopWaiting();
                MSG.Error("File không có dữ liệu");
                Exit();
                return new List<SABill>();
            }
            List<IXLCell> lstErrR = new List<IXLCell>();
            foreach (var item in nonEmptyDataRows.Where(n => n.RowNumber() > 1))
            {
                SABill sABill = new SABill();
                int indexRow = item.RowNumber();
                // Lấy mã HD
                string maHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[0]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();

                    maHD = xLCell.Value.ToString();
                    if (indexRow == 2)
                    {
                        if (maHD.IsNullOrEmpty())
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[0]]));
                        }
                    }
                    if (lst.Any(n => n.MaHD.Equals(maHD)))
                    {
                        sABill.isHD = false;
                    }
                    else
                    {
                        sABill.isHD = true;
                    }
                }
                catch
                {

                }
                //if (string.IsNullOrEmpty(maHD)) continue;
                // Lấy Hình thức hóa đơn
                string HTHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[1]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    HTHD = xLCell.Value.ToString();
                }
                catch { }

                // Lấy loai hd
                string loaiHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[2]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    loaiHD = xLCell.Value.ToString().ToUpper();
                }
                catch { }
                // Lấy Mau HD
                string mauHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[3]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    mauHD = xLCell.Value.ToString().ToUpper();
                }
                catch { }
                // Lấy KyHieuHD
                string kyHieuHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[4]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    kyHieuHD = xLCell.Value.ToString().ToUpper();
                }
                catch { }
                // Lấy SoHD
                string soHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[5]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    soHD = xLCell.Value.ToString().ToUpper();
                }
                catch
                {
                    try
                    {
                        int s = (int)item.Cell(headers[HEADER[5]]).Value;
                        soHD = s.ToString();
                    }
                    catch
                    {

                    }
                }
                //Lấy NgayHD
                DateTime ngayHD = DateTime.Now;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[6]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    ngayHD = Utils.StringToDateTime(xLCell.Value.ToString())??DateTime.Now;
                    ngayHD = ngayHD.AddHours(now.Hour).AddMinutes(now.Minute).AddSeconds(now.Second).AddTicks(numberRow - indexRow + 1);
                }
                catch (Exception ex) { }

                // Lấy Hinh thuc hoa don
                string hinhthucTT = "";

                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[7]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    hinhthucTT = xLCell.Value.ToString();
                }
                catch { }
                // Lấy Loai tien
                string loaiTien = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[8]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    loaiTien = xLCell.Value.ToString();
                }
                catch { }
                // Lấy TTHD
                string trangThaiHD = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[9]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    trangThaiHD = xLCell.Value.ToString();
                }
                catch { }
                // Lấy MaDoiTuong
                string MaDoiTuong = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[10]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    MaDoiTuong = xLCell.Value.ToString();
                }
                catch { }
                // Lấy TenDoituong
                string tenDoiTuong = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[11]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    tenDoiTuong = xLCell.Value.ToString();
                }
                catch { }
                // Lấy DiaChi
                string diaChi = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[12]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    diaChi = xLCell.Value.ToString();
                }
                catch { }
                // Lấy MST
                string MST = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[13]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    MST = xLCell.Value.ToString();
                }
                catch { }
                // Lấy Ten Nguoi lien he
                string tenNguoiLH = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[14]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    tenNguoiLH = xLCell.Value.ToString();
                }
                catch { }
                // Lấy Dien Giai
                string dienGiai = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[15]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    dienGiai = xLCell.Value.ToString();
                }
                catch { }
                // Lấy MaHang
                string maHang = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[16]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    maHang = xLCell.Value.ToString();
                }
                catch { }
                // Lấy Ten Hang
                string tenHang = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[17]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    tenHang = xLCell.Value.ToString();
                }
                catch { }
                // Lấy DVT
                string DVT = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[18]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    DVT = xLCell.Value.ToString();
                }
                catch { }
                // Lấy SoLuong
                decimal SoLuong = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[19]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    SoLuong = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch
                {
                    //lstErrR.Add(item.Cell(headers[HEADER[19]]));
                }
                // Lấy DonGia
                decimal donGia = 0;

                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[20]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    donGia = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch { /*lstErrR.Add(item.Cell(headers[HEADER[20]]));*/ }
                // Lấy ThanhTien
                decimal thanhTien = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[21]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    thanhTien = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch { lstErrR.Add(item.Cell(headers[HEADER[21]])); }
                // Lấy TyLeCK
                decimal TyLeCK = 0;
                IXLCell xLCellTLCK = item.Cell(headers[HEADER[22]]);
                try
                {
                    xLCellTLCK.MergedRange();
                    xLCellTLCK.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCellTLCK.HasComment)
                        xLCellTLCK.Comment.Delete();
                    TyLeCK = (decimal.Parse(xLCellTLCK.Value.ToString()));
                }
                catch
                {
                    try
                    {
                        string check = ((string)xLCellTLCK.Value);
                        if (!string.IsNullOrEmpty(check))
                            lstErrR.Add(item.Cell(headers[HEADER[22]]));
                    }
                    catch
                    {

                    }

                }
                // Lấy TienCK
                decimal tienCK = 0;
                IXLCell xLCellCK = item.Cell(headers[HEADER[23]]);
                try
                {
                    xLCellCK.MergedRange();
                    xLCellCK.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCellCK.HasComment)
                        xLCellCK.Comment.Delete();
                    tienCK = (decimal.Parse(xLCellCK.Value.ToString()));
                }
                catch
                {
                    try
                    {
                        string check = ((string)xLCellCK.Value);
                        if (!string.IsNullOrEmpty(check))
                            lstErrR.Add(item.Cell(headers[HEADER[23]]));
                    }
                    catch
                    {

                    }
                }
                // Lấy ThueSuat
                decimal thueSuat = 0;
                IXLCell xLCellTS = item.Cell(headers[HEADER[24]]);
                try
                {

                    xLCellTS.MergedRange();
                    xLCellTS.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCellTS.HasComment)
                        xLCellTS.Comment.Delete();
                    thueSuat = decimal.Parse(xLCellTS.Value.ToString().Replace("%", ""));
                }
                catch
                {
                    try
                    {
                        if ((string)xLCellTS.Value == "KCT")
                        {
                            thueSuat = -1;
                        }
                        else if ((string)xLCellTS.Value == "KTT")
                        {
                            thueSuat = -2;
                        }
                        else
                            lstErrR.Add(item.Cell(headers[HEADER[24]]));
                    }
                    catch
                    {
                        lstErrR.Add(item.Cell(headers[HEADER[24]]));
                    }

                }
                // Lấy TienThue
                decimal tienThue = 0;
                IXLCell xLCellTT = item.Cell(headers[HEADER[25]]);
                try
                {

                    xLCellTT.MergedRange();
                    xLCellTT.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCellTT.HasComment)
                        xLCellTT.Comment.Delete();
                    tienThue = (decimal.Parse(xLCellTT.Value.ToString()));
                    if (thueSuat != 0 && tienThue == 0)
                    {
                        if (xLCellTT.ValueCached != null)
                        {
                            tienThue = (decimal.Parse(xLCellTT.ValueCached.ToString()));
                        }
                    }

                }
                catch
                {
                    if (xLCellTT.Value == null)
                    {
                        if (thueSuat != -1 || thueSuat != -2) lstErrR.Add(item.Cell(headers[HEADER[25]]));
                    }
                    else
                    {
                        lstErrR.Add(item.Cell(headers[HEADER[25]]));
                    }
                }
                // Lấy TyGia
                decimal TyGia = 0;
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[26]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    TyGia = (decimal.Parse(xLCell.Value.ToString()));
                }
                catch { }
                //}
                // Lấy So Tai khoan
                string soTK = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[27]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    soTK = xLCell.Value.ToString();
                }
                catch { }
                // Lấy Ten ngan hang
                string tenNganHang = "";
                try
                {
                    IXLCell xLCell = item.Cell(headers[HEADER[28]]);
                    xLCell.MergedRange();
                    xLCell.Style.Fill.BackgroundColor = XLColor.NoColor;
                    if (xLCell.HasComment)
                        xLCell.Comment.Delete();
                    tenNganHang = xLCell.Value.ToString();
                }
                catch { }

                try
                {
                    #region Error
                    if (string.IsNullOrEmpty(maHD))
                    {
                        lstErrR.Add(item.Cell(headers[HEADER[0]]));
                    }
                    if (sABill.isHD)
                    {
                        if (lstMauHD.Count() > 0)
                            lstKyHieuHD = Utils.ListTT153Report.Where(n => n.InvoiceTemplate == mauHD).Select(n => n.InvoiceSeries).ToList();

                        if (!invoiceTPL.Contains(HTHD))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[1]]));
                        }
                        if (string.IsNullOrEmpty(loaiHD) || !listLoaiHD.Contains(loaiHD))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[2]]));
                        }
                        if (string.IsNullOrEmpty(mauHD) || !lstMauHD.Contains(mauHD))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[3]]));
                        }
                        if (string.IsNullOrEmpty(kyHieuHD) || !lstKyHieuHD.Contains(kyHieuHD))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[4]]));
                        }
                        if (ngayHD == null)
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[6]]));
                        }
                        if (string.IsNullOrEmpty(hinhthucTT))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[7]]));
                        }
                        if (string.IsNullOrEmpty(loaiTien) || !lstLoaiTien.Contains(loaiTien))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[8]]));
                        }
                        if (TyGia == 0)
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[26]]));
                        }
                        if (!string.IsNullOrEmpty(trangThaiHD) && !lstTTHD.Contains(trangThaiHD))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[9]]));
                        }
                        if (string.IsNullOrEmpty(MaDoiTuong) || !lstMaDoiTg.Contains(MaDoiTuong))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[10]]));
                        }
                        if (string.IsNullOrEmpty(tenDoiTuong))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[11]]));
                        }
                        if (!string.IsNullOrEmpty(MST) && !Core.Utils.CheckMST(MST))
                        {
                            lstErrR.Add(item.Cell(headers[HEADER[13]]));
                        }
                        if (!string.IsNullOrEmpty(trangThaiHD) && lstTTHD1.Contains(trangThaiHD))
                        {
                            if (HTHD.Equals("Hóa đơn điện tử"))
                                if (string.IsNullOrEmpty(soHD))
                                {
                                    lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                }
                        }

                        if (!string.IsNullOrEmpty(mauHD) && !string.IsNullOrEmpty(kyHieuHD))
                        {

                            if (!string.IsNullOrEmpty(soHD))
                            {
                                if (lst.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD && n.InvoiceNo == soHD.PadLeft(7, '0')))
                                {
                                    lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                }
                                else
                                {
                                    if (Utils.ListSABill.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD && n.InvoiceNo == soHD.PadLeft(7, '0')))
                                    {
                                        lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                    }
                                    else if (Utils.ListSAReturn.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD && n.InvoiceNo == soHD.PadLeft(7, '0')))
                                    {
                                        lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                    }
                                    else if (Utils.ListSAInvoice.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD && n.InvoiceNo == soHD.PadLeft(7, '0')))
                                    {
                                        lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                    }
                                    else if (Utils.ListPPDiscountReturn.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD && n.InvoiceNo == soHD.PadLeft(7, '0')))
                                    {
                                        lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                    }
                                    //int result;
                                    //if (int.TryParse(soHD, out result))
                                    //{
                                    //    if (lstHDCheck.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD))
                                    //    {
                                    //        if (result <= lstHDCheck.FirstOrDefault(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD).InvoiceNoMax)
                                    //        {
                                    //            lstErrR.Add(item.Cell(headers[HEADER[5]]));
                                    //        }
                                    //    }
                                    //}
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                    List<int> lstThueSuat = new List<int> { 0, 5, 10, -1, -2 };
                    if (!lstThueSuat.Contains(Convert.ToInt32(thueSuat)))
                    {
                        lstErrR.Add(item.Cell(headers[HEADER[24]]));
                    }
                    if (string.IsNullOrEmpty(maHang) || !Utils.ListMaterialGoods.Any(n => n.MaterialGoodsCode == maHang))
                    {
                        lstErrR.Add(item.Cell(headers[HEADER[16]]));
                    }
                    if (string.IsNullOrEmpty(tenHang))
                    {
                        lstErrR.Add(item.Cell(headers[HEADER[17]]));
                    }
                    //if (lstErrR.Count > 0)
                    //{
                    //    ErrCell.Add(i, lstErrR);
                    //}
                    #endregion
                }
                catch (Exception ex)
                {
                    Exit();
                    MSG.Error("Upload không thành công");
                    WaitingFrm.StopWaiting();
                    return new List<SABill>();
                }


                sABill.MaHD = maHD;
                if (!string.IsNullOrEmpty(HTHD))
                {
                    if (HTHD.Equals("Hóa đơn điện tử")) sABill.InvoiceForm = 2;
                    if (HTHD.Equals("Hóa đơn đặt in")) sABill.InvoiceForm = 1;
                    if (HTHD.Equals("Hóa đơn tự in")) sABill.InvoiceForm = 0;
                }

                InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.InvoiceTypeCode.Equals(loaiHD));
                if (invoiceType != null)
                {
                    sABill.InvoiceType = invoiceType._InvoiceType;
                    sABill.InvoiceTypeID = invoiceType.ID;
                }
                sABill.InvoiceTemplate = mauHD;
                sABill.InvoiceSeries = kyHieuHD;
                if (!string.IsNullOrEmpty(soHD))
                    sABill.InvoiceNo = soHD.PadLeft(7, '0');
                sABill.InvoiceDate = ngayHD;
                sABill.PaymentMethod = hinhthucTT;
                sABill.CurrencyID = loaiTien;
                sABill.ExchangeRate = TyGia;
                sABill.AccountingObjectBankAccount = soTK;
                sABill.AccountingObjectBankName = tenNganHang;
                if (!string.IsNullOrEmpty(trangThaiHD))
                {
                    if (trangThaiHD.Equals("Hóa đơn mới tạo lập")) sABill.StatusInvoice = 0;
                    if (trangThaiHD.Equals("Hóa đơn có chữ ký số")) sABill.StatusInvoice = 1;
                    if (trangThaiHD.Equals("Hóa đơn bị thay thế")) sABill.StatusInvoice = 3;
                    if (trangThaiHD.Equals("Hóa đơn bị điều chỉnh")) sABill.StatusInvoice = 4;
                    if (trangThaiHD.Equals("Hóa đơn bị huỷ")) sABill.StatusInvoice = 5;
                }

                AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.AccountingObjectCode.Equals(MaDoiTuong));
                if (accountingObject != null)
                {
                    sABill.AccountingObjectID = accountingObject.ID;
                }
                sABill.AccountingObjectName = tenDoiTuong;
                sABill.AccountingObjectAddress = diaChi;
                sABill.CompanyTaxCode = MST;
                sABill.ContactName = tenNguoiLH;
                sABill.Reason = dienGiai;

                SABillDetail sABillDetail = new SABillDetail();
                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.MaterialGoodsCode.Equals(maHang));
                if (materialGoods != null)
                {
                    sABillDetail.MaterialGoodsID = materialGoods.ID;
                }
                sABillDetail.Unit = DVT;
                sABillDetail.Description = tenHang;
                sABillDetail.Quantity = SoLuong;
                sABillDetail.UnitPriceOriginal = donGia;
                sABillDetail.UnitPrice = donGia;
                sABillDetail.Amount = thanhTien;
                sABillDetail.AmountOriginal = thanhTien;
                sABillDetail.DiscountRate = TyLeCK;
                sABillDetail.DiscountAmount = tienCK;
                sABillDetail.DiscountAmountOriginal = tienCK;
                sABillDetail.VATRate = thueSuat;
                sABillDetail.VATAmount = tienThue;
                sABillDetail.VATAmountOriginal = tienThue;
                sABillDetail.OrderPriority = MaxOrderPriority++;

                sABill.SABillDetails.Add(sABillDetail);
                lst.Add(sABill);
            }
            if (lstErrR.Count() > 0)
            {
                foreach (var item in lstErrR)
                {
                    item.Style.Fill.BackgroundColor = XLColor.Red;
                    item.MergedRange();
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[0]])
                    {
                        item.Comment.AddText("Mã hóa đơn không được để trống");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[5]])
                    {
                        item.Comment.AddText("Số hóa đơn không hợp lệ hoặc bị trùng");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[16]])
                    {
                        item.Comment.AddText("Mã hàng không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[10]])
                    {
                        item.Comment.AddText("Mã đối tượng không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[8]])
                    {
                        item.Comment.AddText("Loại tiền không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[9]])
                    {
                        item.Comment.AddText("Trạng thái hóa đơn không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[13]])
                    {
                        item.Comment.AddText("Mã số thuế không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[6]])
                    {
                        item.Comment.AddText("Sai định dạng ngày hóa đơn");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[11]])
                    {
                        item.Comment.AddText("Tên đối tượng không được để trống");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[1]])
                    {
                        item.Comment.AddText("Hình thức hóa đơn không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[2]])
                    {
                        item.Comment.AddText("Loại hóa đơn không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[3]])
                    {
                        item.Comment.AddText("Mẫu số hóa đơn không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[4]])
                    {
                        item.Comment.AddText("Ký hiệu hóa đơn không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[22]])
                    {
                        item.Comment.AddText("Tỷ lệ chiết khấu không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[23]])
                    {
                        item.Comment.AddText("Tiền chiết khấu không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[24]])
                    {
                        item.Comment.AddText("Thuế suất không hợp lệ");
                    }
                    if (item.WorksheetColumn().ColumnNumber() == headers[HEADER[25]])
                    {
                        item.Comment.AddText("Tiền thuế không hợp lệ");
                    }
                }
                WaitingFrm.StopWaiting();
                if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file lỗi về không?") == System.Windows.Forms.DialogResult.Yes)
                {
                    System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                    {
                        FileName = "Upload_Invoice_Error.xlsx",
                        AddExtension = true,
                        Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                    };
                    sf.Title = "Chọn thư mục lưu file";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        //XLWorkbook xLWorkbook = new XLWorkbook();
                        //xLWorkbook.AddWorksheet(ws);
                        try
                        {
                            wb.SaveAs(sf.FileName);
                            //xLWorkbook.SaveAs(sf.FileName);
                            //xLWorkbook.Dispose();
                            Exit();
                        }
                        catch
                        {
                            MSG.Error("Lỗi khi lưu");
                            //xLWorkbook.Dispose();
                            Exit();
                            return new List<SABill>();
                        }
                        if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                        {
                            try
                            {
                                System.Diagnostics.Process.Start(sf.FileName);
                            }
                            catch
                            {
                                return new List<SABill>();
                            }

                        }
                    }
                    else
                    {
                        Exit();
                        return new List<SABill>();
                    }
                }
                else
                {
                    Exit();
                    return new List<SABill>();
                }

                return new List<SABill>();
            }

            List<SABill> lstSAB = new List<SABill>();
            foreach (var item in lst.GroupBy(n => n.MaHD))
            {
                SABill sABill = item.FirstOrDefault(n => n.isHD);
                sABill.ID = Guid.NewGuid();
                sABill.RefDateTime = sABill.InvoiceDate;
                sABill.SABillDetails.First().SABillID = item.FirstOrDefault(n => n.isHD).ID;
                foreach (var itemD in item.Where(n => !n.isHD))
                {
                    itemD.SABillDetails.First().SABillID = item.FirstOrDefault(n => n.isHD).ID;
                    sABill.SABillDetails.Add(itemD.SABillDetails.First());
                }
                sABill.TotalAmount = sABill.SABillDetails.Sum(n => n.Amount);
                sABill.TotalAmountOriginal = sABill.SABillDetails.Sum(n => n.AmountOriginal);
                sABill.TotalDiscountAmount = sABill.SABillDetails.Sum(n => n.DiscountAmount);
                sABill.TotalDiscountAmountOriginal = sABill.SABillDetails.Sum(n => n.DiscountAmountOriginal);
                sABill.TotalVATAmount = sABill.SABillDetails.Sum(n => n.VATAmount);
                sABill.TotalVATAmountOriginal = sABill.SABillDetails.Sum(n => n.VATAmountOriginal);
                lstSAB.Add(sABill);
            }
            Exit();
            return lstSAB;
        }
        //Hàm thu hồi bộ nhớ cho COM Excel
        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                obj = null;
            }
            finally
            { GC.Collect(); }
        }

        //public string ReadCell(int i, int j)
        //{
        //    i++;
        //    j++;
        //    Range x = (Range)ws.Cells[i, j];
        //    if (x.Value2 != null) return (string)x.Value2;
        //    else return "";
        //}
        public Dictionary<string, int> ReadHeader()
        {
            Dictionary<string, int> headers = new Dictionary<string, int>();
            var head = ws.Row(1);
            for (int i = 1; i <= ws.ColumnsUsed().Count(); i++)
            {
                try
                {
                    headers.Add((string)head.Cell(i).Value, i);
                }
                catch
                {

                }
            }
            return headers;
        }
        public List<string> ReadSheetName()
        {
            List<string> result = new List<string>();
            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                result.Add(wb.Worksheet(i).Name);
            }
            return result;
        }
    }
    public class InvoiceCheck1
    {
        public string InvoiceTemplate { get; set; }
        public string InvoiceSeries { get; set; }
        public int InvoiceNoMax { get; set; }

    }
}
