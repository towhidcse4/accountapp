﻿namespace Accounting.Frm.FBusiness.SalePrice.FSABill
{
    partial class UploadFileExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadFileExcel));
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnBrowse = new Infragistics.Win.Misc.UltraButton();
            this.btnCheckData = new Infragistics.Win.Misc.UltraButton();
            this.btnOK = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.linkUploadInvoice = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileName)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(30, 23);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Tải về file mẫu :";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(30, 52);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 23);
            this.ultraLabel2.TabIndex = 2;
            this.ultraLabel2.Text = "Tải lên file dữ liệu :";
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(136, 48);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(322, 21);
            this.txtFileName.TabIndex = 3;
            this.txtFileName.ValueChanged += new System.EventHandler(this.txtFileName_ValueChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(465, 46);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(74, 23);
            this.btnBrowse.TabIndex = 4;
            this.btnBrowse.Text = "Chọn File";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnCheckData
            // 
            this.btnCheckData.Location = new System.Drawing.Point(127, 91);
            this.btnCheckData.Name = "btnCheckData";
            this.btnCheckData.Size = new System.Drawing.Size(114, 23);
            this.btnCheckData.TabIndex = 5;
            this.btnCheckData.Text = "Kiểm tra dữ liệu";
            this.btnCheckData.Click += new System.EventHandler(this.btnCheckData_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(383, 121);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "Thực hiện";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(464, 121);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // linkUploadInvoice
            // 
            this.linkUploadInvoice.AutoSize = true;
            this.linkUploadInvoice.Location = new System.Drawing.Point(124, 23);
            this.linkUploadInvoice.Name = "linkUploadInvoice";
            this.linkUploadInvoice.Size = new System.Drawing.Size(82, 13);
            this.linkUploadInvoice.TabIndex = 8;
            this.linkUploadInvoice.TabStop = true;
            this.linkUploadInvoice.Text = "Upload_Invoice";
            this.linkUploadInvoice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkUploadInvoice_LinkClicked);
            // 
            // UploadFileExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 156);
            this.Controls.Add(this.linkUploadInvoice);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCheckData);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(567, 195);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(567, 195);
            this.Name = "UploadFileExcel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upload hóa đơn";
            ((System.ComponentModel.ISupportInitialize)(this.txtFileName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFileName;
        private Infragistics.Win.Misc.UltraButton btnBrowse;
        private Infragistics.Win.Misc.UltraButton btnCheckData;
        private Infragistics.Win.Misc.UltraButton btnOK;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private System.Windows.Forms.LinkLabel linkUploadInvoice;
    }
}