﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinTabControl;

namespace Accounting
{
    public sealed partial class FSABillDetail : FSaBillDetailStand
    {
        #region Khai báo
        UltraGrid uGrid0;
        UltraGrid ugrid2 = null;
        private IViewVoucherInvisibleService _voucherInvisibleService = IoC.Resolve<IViewVoucherInvisibleService>();
        List<TIOriginalVoucher> dsTIOriginalVouchers = new List<TIOriginalVoucher>();
        private IRefVoucherService _refVoucherService { get { return IoC.Resolve<IRefVoucherService>(); } }
        #endregion
        #region khởi tạo
        public FSABillDetail(SABill temp, List<SABill> listObject, int statusForm)
        {
            InitializeComponent();
            base.InitializeComponent1();
            #region Thiết lập ban đầu           
            _statusForm = statusForm;
            if (statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.TypeID = 326;
            }
            else _select = temp;
            _listSelects.AddRange(listObject);
            InitializeGUI(_select); //khởi tạo và hiển thị giá trị ban đầu cho các control
            ReloadToolbar(_select, listObject, statusForm);  //Change Menu Top
            if (_select != null && (_select.IDAdjustInv != null || _select.IDReplaceInv != null))
            {
                panel2.Visible = true;
                if (_select.Type == 2)
                {
                    lblTttt.Text = "Điều chỉnh tăng cho HĐ số";
                }
                else if (_select.Type == 3)
                {
                    lblTttt.Text = "Điều chỉnh giảm cho HĐ số";
                }
                else if (_select.Type == 4)
                {
                    lblTttt.Text = "Điều chỉnh thông tin cho HĐ số";
                }
                if (Utils.ISAInvoiceService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty) != null && Utils.ISAInvoiceService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty).InvoiceNo != null)
                {
                    var sa = Utils.ISAInvoiceService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty);
                    txtOldInvNo.Text = sa.InvoiceNo;
                    txtOldInvDate.Text = sa.InvoiceDate == null ? "" : (sa.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                    txtOldInvTemplate.Text = sa.InvoiceTemplate;
                    txtOldInvSeries.Text = sa.InvoiceSeries;

                }
                else if (Utils.ISAReturnService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty) != null && Utils.ISAReturnService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty).InvoiceNo != null)
                {
                    var sa = Utils.ISAReturnService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty);
                    txtOldInvNo.Text = sa.InvoiceNo;
                    txtOldInvDate.Text = sa.InvoiceDate == null ? "" : (sa.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                    txtOldInvTemplate.Text = sa.InvoiceTemplate;
                    txtOldInvSeries.Text = sa.InvoiceSeries;
                }
                else if (Utils.IPPDiscountReturnService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty) != null && Utils.IPPDiscountReturnService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty).InvoiceNo != null)
                {
                    var sa = Utils.IPPDiscountReturnService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty);
                    txtOldInvNo.Text = sa.InvoiceNo;
                    txtOldInvDate.Text = sa.InvoiceDate == null ? "" : (sa.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                    txtOldInvTemplate.Text = sa.InvoiceTemplate;
                    txtOldInvSeries.Text = sa.InvoiceSeries;
                }
                else
                {
                    var sa = Utils.ISABillService.Getbykey(_select.IDAdjustInv ?? _select.IDReplaceInv ?? Guid.Empty);
                    txtOldInvNo.Text = sa.InvoiceNo;
                    txtOldInvDate.Text = sa.InvoiceDate == null ? "" : (sa.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                    txtOldInvTemplate.Text = sa.InvoiceTemplate;
                    txtOldInvSeries.Text = sa.InvoiceSeries;
                }
            }
            else panel1.Visible = false;

            #endregion
        }
        #endregion

        #region override
        public override void InitializeGUI(SABill input)
        {
            //config số đơn hàng, ngày đơn hàng
            //Config Grid
            Dictionary<string, string> lstdatasourceUcomboThanhToan = new Dictionary<string, string>();//trungnq thêm để làm task 6235, thêm combobox hình thức thanh toán
            lstdatasourceUcomboThanhToan.Add("0", "Tiền mặt");
            lstdatasourceUcomboThanhToan.Add("1", "Chuyển Khoản");
            lstdatasourceUcomboThanhToan.Add("2", "TM/CK");
            uComboThanhToan.DataSource = lstdatasourceUcomboThanhToan;
            Utils.ConfigGrid(uComboThanhToan, ConstDatabase.TMIndustryTypeGH_TableName);
            uComboThanhToan.DisplayMember = "Value";
            uComboThanhToan.ValueMember = "Key";//trungnq

            Template mauGiaoDien = Utils.GetMauGiaoDien(input.TypeID, input.TemplateID, Utils.ListTemplate);
            _select.TemplateID = _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : input.TemplateID;
            BindingList<RefVoucher> bdlRefVoucher = new BindingList<RefVoucher>();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {
                _select.ID = Guid.NewGuid();
            }
            else
                bdlRefVoucher = new BindingList<RefVoucher>(input.RefVouchers);
            _listObjectInput = new BindingList<System.Collections.IList> { new BindingList<SABillDetail>(_statusForm == ConstFrm.optStatusForm.Add ? (input != null ? input.SABillDetails : new BindingList<SABillDetail>()) : input.SABillDetails) };
            _listObjectInputPost = new BindingList<System.Collections.IList> { bdlRefVoucher };
            this.ConfigGridByTemplete_General<SABill>(pnlUgrid, mauGiaoDien);
            List<BindingList<System.Collections.IList>> manyTemp = new List<BindingList<System.Collections.IList>>() { _listObjectInput, _listObjectInputPost };
            List<Boolean> manyStandard = new List<Boolean>() { true, false };
            this.ConfigGridByManyTemplete<SABill>(input.TypeID, mauGiaoDien, true, manyTemp, manyStandard, _select.ID.ToString());
            //ConfigGridBase(input, uGridControl, pnlUgrid);
            _select.TotalAmountOriginal = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmountOriginal;
            _select.TotalAmount = _statusForm == ConstFrm.optStatusForm.Add ? 0 : input.TotalAmount;
            _select.CurrencyID = _statusForm == ConstFrm.optStatusForm.Add ? "VND" : input.CurrencyID;
            List<SABill> inputCurrency = new List<SABill> { input };
            this.ConfigGridCurrencyByTemplate<SABill>(input.TypeID, new BindingList<System.Collections.IList> { inputCurrency },
                                              uGridControl, mauGiaoDien);
            if (_statusForm != ConstFrm.optStatusForm.Add)
            {
                if (input.PaymentMethod != null && !string.IsNullOrEmpty(input.PaymentMethod)) uComboThanhToan.Text = input.PaymentMethod;
            }
            else
            {
                uComboThanhToan.Text = "TM/CK";//add by cuongpv , trungnq sửa sau
            }
            uGrid0 = (UltraGrid)palGrid.Controls.Find("uGrid0", true).FirstOrDefault();
            ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            ugrid2.DisplayLayout.Bands[0].Columns["Date"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["PostedDate"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DisplayLayout.Bands[0].Columns["No"].SortIndicator = SortIndicator.Ascending;
            ugrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(ugrid2_DoubleClickCell);

            //config combo
            this.ConfigCombo(_select, Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectID", cbbAccountingObjectBankAccount, "AccountingObjectBankAccount");
            //this.ConfigCombo(Utils.ListAccountingObject, cbbAccountingObjectIDHD, "AccountingObjectCode", "ID", input, "AccountingObjectID"); // edit by tung
            //this.ConfigCombo(Utils.ListAccountingObjectBank, cbbAccountingObjectBankAccount, "BankAccount", "BankAccount", input, "AccountingObjectBankAccount");
            //Databinding
            DataBinding(new List<Control> { txtAccountingObjectNameHD, txtAccountingObjectAddressHD, txtCompanyTaxCodeHD, txtContactNameHD, txtReasonHD }, "AccountingObjectName,AccountingObjectAddress,CompanyTaxCode,ContactName,Reason");
            DataBinding(new List<Control> { txtListNoHD, dteListDateHD, txtListCommonNameInventoryHD }, "ListNo,ListDate,ListCommonNameInventory", "0,2,0");
            DataBinding(new List<Control> { chkisAttachListHD }, "IsAttachList", "1");
            DataBinding(new List<Control> { txtDocumentNo, DocumentDate, txtDocumentNote }, "DocumentNo,DocumentDate,DocumentNote", "0,2,0");
            DataBinding(new List<Control> { txtAccountingBankName }, "AccountingObjectBankName");
            //lst control binding            
            DataBinding(new List<Control>
                           { txtTotalAmountOriginal, txtTotalVATAmountOriginal, txtTotalDiscountAmountSubOriginal, txtTotalPaymentAmountOriginalStand }, "TotalAmountOriginal, TotalVATAmountOriginal, TotalDiscountAmountOriginal, TotalPaymentAmountOriginal", "3,3,3,3", true);
            DataBinding(new List<Control>
                           { txtTotalAmount,txtTotalVATAmount,txtTotalDiscountAmountSub,txtTotalPaymentAmountStand}, "TotalAmount, TotalVATAmount, TotalDiscountAmount, TotalPaymentAmount", "3,3,3,3");
            if (_statusForm == ConstFrm.optStatusForm.Add) ChkisAttachListCheckedChanged(chkisAttachListHD, new EventArgs());
            txtListNo.Tag = dteListDate.Tag = txtListCommonNameInventory.Tag = new Dictionary<string, object> { { "Enabled", chkisAttachListHD.CheckState == CheckState.Checked } };
            UltraTabControl TabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
            TabControl.SelectedTabChanged += (s, ev) => SelectTab_Change(s, ev);
            var band = uGridControl.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                if (item.Key == "InvoiceForm" || item.Key == "InvoiceTypeID" || item.Key == "InvoiceTemplate" || item.Key == "InvoiceSeries" || item.Key == "InvoiceNo" || item.Key == "InvoiceDate")
                {
                    uGridControl.Width = uGridControl.Width - item.Width;
                    item.Width = 0;
                    item.Hidden = true;

                }
            }
            AddCbb(input);
        }
        private void SelectTab_Change(object sender, SelectedTabChangedEventArgs e)
        {
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Hidden = true;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceForm"].Width = 0;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Hidden = true;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTypeID"].Width = 0;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Hidden = true;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceTemplate"].Width = 0;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Hidden = true;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceSeries"].Width = 0;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceNo"].Width = 0;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Hidden = true;
            //uGridControl.DisplayLayout.Bands[0].Columns["InvoiceDate"].Width = 0;
            //uGridControl.Width = uGridControl.DisplayLayout.Bands[0].Columns["CurrencyID"].Width;
            //uGridControl.Anchor = AnchorStyles.Right;
        }
        #endregion

        #region Event

        private void btnSaInvoice_Click(object sender, EventArgs e)
        {
            var f = new FchoseSaInvoice<SABill>(_listObjectInput, _select, _select.AccountingObjectID);
            f.FormClosed += new FormClosedEventHandler(fSelectOrders_FormClosed);
            try
            {
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fSelectOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FchoseSaInvoice<SABill>)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectSAs = frm._lstSA;
                if (uGrid0 != null)
                {
                    var source = (BindingList<SABillDetail>)uGrid0.DataSource;
                    if (selectSAs.Count > 0) source.Clear();
                    _select.SAIDs = (from i in selectSAs select i.ID).ToList();
                    List<SAInvoiceDetail> lstSADetail = new List<SAInvoiceDetail>();
                    foreach (var sa in selectSAs)
                    {
                        lstSADetail.AddRange(sa.SAInvoiceDetails);
                    }
                    var newLst = (from a in lstSADetail
                                  group a by new { a.MaterialGoodsID, a.Description, a.VATRate, a.VATAccount } into t
                                  select new SAInvoiceDetail
                                  {
                                      MaterialGoodsID = t.Key.MaterialGoodsID,
                                      Description = t.Key.Description,
                                      Quantity = t.Sum(x => x.Quantity),
                                      AmountOriginal = t.Sum(x => x.AmountOriginal),
                                      Amount = t.Sum(x => x.Amount),
                                      VATRate = t.Key.VATRate,
                                      VATAmount = t.Sum(x => x.VATAmount),
                                      VATAmountOriginal = t.Sum(x => x.VATAmountOriginal),
                                      DiscountAmountOriginal = t.Sum(x => x.DiscountAmountOriginal),
                                      DiscountAmount = t.Sum(x => x.DiscountAmount),
                                      VATAccount = t.Key.VATAccount
                                  }).ToList();
                    foreach (var selectOrder in newLst)
                    {
                        uGrid0.AddNewRow4Grid();
                        var row = uGrid0.Rows[uGrid0.Rows.Count - 1];
                        row.Cells["MaterialGoodsID"].Value = selectOrder.MaterialGoodsID;
                        row.Cells["Description"].Value = selectOrder.Description;
                        row.Cells["Quantity"].Value = selectOrder.Quantity;
                        row.Cells["UnitPrice"].Value = selectOrder.Quantity != 0 ? selectOrder.Amount / selectOrder.Quantity : selectOrder.UnitPrice; // edit by tungnt
                        row.Cells["UnitPriceOriginal"].Value = selectOrder.Quantity != 0 ? selectOrder.AmountOriginal / selectOrder.Quantity : selectOrder.UnitPriceOriginal; // edit by tungnt
                        row.Cells["Amount"].Value = selectOrder.Amount;
                        row.Cells["AmountOriginal"].Value = selectOrder.AmountOriginal;
                        row.Cells["AccountingObjectID"].Value = selectOrder.AccountingObjectID;
                        row.Cells["VATRate"].Value = selectOrder.VATRate;
                        row.Cells["VATAmount"].Value = selectOrder.VATAmount;
                        row.Cells["VATAmountOriginal"].Value = selectOrder.VATAmountOriginal;
                        row.Cells["VATAccount"].Value = selectOrder.VATAccount;
                        row.Cells["DiscountRate"].Value = selectOrder.AmountOriginal != 0 ? (selectOrder.DiscountAmountOriginal / selectOrder.AmountOriginal) * 100 : selectOrder.DiscountRate; // edit by tungnt
                        row.Cells["DiscountAmount"].Value = selectOrder.DiscountAmount;
                        row.Cells["DiscountAmountOriginal"].Value = selectOrder.DiscountAmountOriginal;
                        row.UpdateData();
                    }
                    if (uGrid0.DisplayLayout.Bands[0].Summaries.Exists("SumAmount"))
                    {
                        this.uGrid_SummaryValueChanged<SABill>(uGrid0, new SummaryValueChangedEventArgs(uGrid0.Rows.SummaryValues["SumAmount"]));
                    }
                    if (uGrid0.DisplayLayout.Bands[0].Summaries.Exists("SumVATAmount"))
                    {
                        this.uGrid_SummaryValueChanged<SABill>(uGrid0, new SummaryValueChangedEventArgs(uGrid0.Rows.SummaryValues["SumVATAmount"]));
                    }
                    if (uGrid0.DisplayLayout.Bands[0].Summaries.Exists("SumDiscountAmount"))
                    {
                        this.uGrid_SummaryValueChanged<SABill>(uGrid0, new SummaryValueChangedEventArgs(uGrid0.Rows.SummaryValues["SumDiscountAmount"]));
                    }
                }
                if (uGrid0.Rows.Count > 0)
                    foreach (var row in uGrid0.Rows)
                        foreach (var cell in row.Cells)
                            Utils.CheckErrorInCell<SABill>(this, uGrid0, cell);
            }
        }

        #endregion

        #region Utils

        #endregion
        private void ChkisAttachListCheckedChanged(object sender, EventArgs e)
        {//In kèm bảng kê hay không?
            _select.IsAttachList = txtListNo.Enabled = dteListDate.Enabled = txtListCommonNameInventory.Enabled = chkisAttachListHD.CheckState == CheckState.Checked;
            txtListNo.Tag = dteListDate.Tag = txtListCommonNameInventory.Tag = new Dictionary<string, object> { { "Enabled", chkisAttachListHD.CheckState == CheckState.Checked } };
            if (dteListDate.Enabled)
            {
                if (dteListDate.Value != null)
                {
                    dteListDate.DateTime = DateTime.Now;
                    chkisAttachListHD.CheckState = CheckState.Checked;
                }
            }
            if (chkisAttachListHD.CheckState == CheckState.Checked) return;
            txtListNo.Text = string.Empty;
            txtListCommonNameInventory.Text = string.Empty;
            //dteListDate.Value = "";
            //dteListDate.ResetDateTime();
            dteListDate.Value = null;
        }
        private void txtThanhToan_TextChanged(object sender, EventArgs e)
        {
            _select.PaymentMethod = uComboThanhToan.Text;
        }

        private void palBottom_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnOriginalVoucher_Click(object sender, EventArgs e)
        {
            try
            {

                BindingList<RefVoucher> datasource = (BindingList<RefVoucher>)ugrid2.DataSource;
                if (datasource == null)
                    datasource = new BindingList<RefVoucher>();
                var f = new FViewVoucherOriginal(_select.CurrencyID, datasource);
                f.FormClosed += new FormClosedEventHandler(fOriginalVoucher_FormClosed);
                try
                {
                    f.ShowDialog(this);
                }
                catch (Exception ex)
                {
                    MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                }

            }
            catch (Exception ex)
            {
                //MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }
        private void fOriginalVoucher_FormClosed(object sender, FormClosedEventArgs e)
        {
            var f = (FViewVoucherOriginal)sender;
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (f.DialogResult == DialogResult.OK)
                {
                    var source = (BindingList<RefVoucher>)ugrid2.DataSource;
                    foreach (var item in f.RefVoucher)
                    {
                        source.Add(new RefVoucher
                        {
                            ID = Guid.NewGuid(),
                            RefID1 = _select.ID,
                            RefID2 = item.RefID2,
                            No = item.No,
                            Reason = item.Reason,
                            Date = item.Date,
                            PostedDate = item.PostedDate,
                            TypeID = item.TypeID,
                        });
                    }

                }
            }
        }
        private void ugrid2_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                RefVoucher temp = (RefVoucher)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    editFuntion(temp);

            }
        }

        private void editFuntion(RefVoucher temp)
        {
            var f = Utils.ViewVoucherSelected1(temp.RefID2, temp.TypeID);

        }

        private void FSABillDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void FSABillDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void txtAccountingObjectNameHD_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace("\r\n", "");
            if (txt.Text == "")
                txt.Multiline = true;
            else
                txt.Multiline = false;
        }
        private void AddCbb(SABill input)
        {
            Dictionary<int, string> listIT = new Dictionary<int, string>();
            listIT.Add(2, "Hóa đơn điện tử");
            listIT.Add(1, "Hóa đơn đặt in");
            listIT.Add(0, "Hóa đơn tự in");
            cbbHinhThucHD.DataSource = listIT;
            cbbHinhThucHD.DisplayMember = "Value";
            cbbHinhThucHD.ValueMember = "Key";//trungnq
            Utils.ConfigGrid(cbbHinhThucHD, ConstDatabase.TMIndustryTypeGH_TableName);
            cbbHinhThucHD.Value = input.InvoiceForm;
            cbbHinhThucHD.RowSelected += new RowSelectedEventHandler(combobox_selected);

            List<Guid> lstPLInvoice = Utils.ListTT153PublishInvoiceDetail.Select(n => n.InvoiceTypeID).ToList();
            var data = Utils.ListInvoiceType.Where(n => lstPLInvoice.Any(m => m == n.ID)).ToList();
            cbbLoaiHD.DataSource = data;
            cbbLoaiHD.ValueMember = "ID";
            cbbLoaiHD.DisplayMember = "InvoiceTypeName";
            cbbLoaiHD.RowSelected += new RowSelectedEventHandler(combobox_selected);
            Utils.ConfigGrid(cbbLoaiHD, ConstDatabase.InvoiceType_TableName);
            cbbLoaiHD.Value = input.InvoiceTypeID;
            dteNgayHD.Value = input.InvoiceDate;
            txtSoHD.Text = input.InvoiceNo;
            cbbMauHD.Value = new Guid();
            if (cbbLoaiHD.Value != null && cbbHinhThucHD.Value != null)
            {
                var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == input.InvoiceTemplate);
                if (templateInvoid != null)
                    cbbMauHD.Value = templateInvoid.ID;
                else
                    cbbMauHD.Value = null;
            }
            cbbMauHD.RowSelected += new RowSelectedEventHandler(combobox_selected);
        }
        private void combobox_selected(object sender, RowSelectedEventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.Name == "cbbHinhThucHD")
            {
                List<TT153Report> lstinvoice = new List<TT153Report>();
                _select.InvoiceForm = int.Parse(cbb.Value.ToString());
                if (cbbLoaiHD.Value != null)
                {
                    var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbb.Value.ToString())).ToList();
                    lstinvoice = (from a in lstinvoice1
                                  join b in Utils.ITT153PublishInvoiceDetailService.Query
                                       on a.ID equals b.TT153ReportID
                                  select a).Distinct().ToList();
                    cbbMauHD.DataSource = lstinvoice;
                    cbbMauHD.ValueMember = "ID";
                    cbbMauHD.DisplayMember = "InvoiceTemplate";
                    Utils.ConfigGrid(cbbMauHD, ConstDatabase.TT153Report_TableName);
                    _select.InvoiceTypeID = Guid.Parse(cbbLoaiHD.Value.ToString());
                    if (lstinvoice.Count == 1)
                    {
                        var gt = lstinvoice.FirstOrDefault();
                        if (gt != null)
                        {
                            cbbMauHD.Value = gt.ID != null ? gt.ID : new Guid();
                            txtKyHieuHD.Text = _select.InvoiceSeries != null ? _select.InvoiceSeries : null;
                            _select.InvoiceSeries = _select.InvoiceSeries != null ? _select.InvoiceSeries : null;
                            _select.InvoiceTemplate = _select.InvoiceTemplate != null ? _select.InvoiceTemplate : null;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {

                                List<SAInvoice> lst = new List<SAInvoice>();
                                List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                                List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                 x => new SAInvoice
                                 {
                                     InvoiceTypeID = x.InvoiceTypeID,
                                     InvoiceForm = x.InvoiceForm,
                                     InvoiceTemplate = x.InvoiceTemplate,
                                     InvoiceNo = x.InvoiceNo
                                 }
                                 ).ToList();
                                if (lstsA.Count > 0) lst.AddRange(lstsA);
                                if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                                if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                                List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                                if (ls.Count > 0)
                                {
                                    int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                    txtSoHD.Text = maxno.ToString("0000000");
                                }
                                else
                                {
                                    txtSoHD.Text = "0000001";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            cbbMauHD.Value = null;
                            txtKyHieuHD.Text = null;
                            txtSoHD.Text = null;
                        }
                        else
                        {
                            var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == _select.InvoiceTemplate);
                            if (templateInvoid != null)
                            {
                                cbbMauHD.Value = templateInvoid.ID;
                                txtKyHieuHD.Text = templateInvoid.InvoiceSeries;
                            }
                            else
                            {
                                cbbMauHD.Value = null;
                                txtKyHieuHD.Text = null;
                            }
                        }

                    }
                }
                else
                {
                    cbbMauHD.Value = null;
                    txtKyHieuHD.Text = null;
                }

            }
            if (cbb.Name == "cbbLoaiHD")
            {
                List<TT153Report> lstinvoice = new List<TT153Report>();
                _select.InvoiceTypeID = Guid.Parse(cbbLoaiHD.Value.ToString());
                if (cbbHinhThucHD.Value != null)
                {
                    var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                    lstinvoice = (from a in lstinvoice1
                                  join b in Utils.ITT153PublishInvoiceDetailService.Query
                                       on a.ID equals b.TT153ReportID
                                  select a).Distinct().ToList();
                    cbbMauHD.DataSource = lstinvoice;
                    cbbMauHD.ValueMember = "ID";
                    cbbMauHD.DisplayMember = "InvoiceTemplate";
                    Utils.ConfigGrid(cbbMauHD, ConstDatabase.TT153Report_TableName);
                    _select.InvoiceForm = int.Parse(cbbHinhThucHD.Value.ToString());
                    if (lstinvoice.Count == 1)
                    {
                        var gt = lstinvoice.FirstOrDefault();
                        if (gt != null)
                        {
                            cbbMauHD.Value = gt.ID;
                            txtKyHieuHD.Text = gt.InvoiceSeries;
                            _select.InvoiceSeries = gt.InvoiceSeries;
                            _select.InvoiceTemplate = gt.InvoiceTemplate;
                            if (_statusForm == ConstFrm.optStatusForm.Add)
                            {

                                List<SAInvoice> lst = new List<SAInvoice>();
                                List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                                List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                    x => new SAInvoice
                                    {
                                        InvoiceTypeID = x.InvoiceTypeID,
                                        InvoiceForm = x.InvoiceForm,
                                        InvoiceTemplate = x.InvoiceTemplate,
                                        InvoiceNo = x.InvoiceNo
                                    }
                                    ).ToList();
                                if (lstsA.Count > 0) lst.AddRange(lstsA);
                                if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                                if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                                List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                                if (ls.Count > 0)
                                {
                                    int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                    txtSoHD.Text = maxno.ToString("0000000");
                                }
                                else
                                {
                                    txtSoHD.Text = "0000001";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            cbbMauHD.Value = null;
                            txtKyHieuHD.Text = null;
                        }
                        else
                        {
                            var templateInvoid = lstinvoice1.FirstOrDefault(t => t.InvoiceTemplate == _select.InvoiceTemplate);
                            if (templateInvoid != null)
                            {
                                cbbMauHD.Value = templateInvoid.ID;
                                txtKyHieuHD.Text = templateInvoid.InvoiceSeries;
                            }
                            else
                            {
                                cbbMauHD.Value = null;
                                txtKyHieuHD.Text = null;
                            }
                        }
                    }

                }
                else
                {
                    cbbMauHD.Value = null;
                    txtKyHieuHD.Text = null;
                }
            }
            if (cbb.Name == "cbbMauHD")
            {
                Guid guid = cbb.Value != null ? new Guid(cbb.Value.ToString()) : new Guid();

                var lstinvoice1 = Utils.ListTT153Report.Where(t => t.InvoiceTypeID == Guid.Parse(cbbLoaiHD.Value.ToString()) && t.InvoiceForm == int.Parse(cbbHinhThucHD.Value.ToString())).ToList();
                if (lstinvoice1.Count == 1)
                {
                    var gt = lstinvoice1.FirstOrDefault();
                    if (gt != null)
                    {
                        _select.InvoiceTemplate = gt.InvoiceTemplate;
                        txtKyHieuHD.Text = gt.InvoiceSeries;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {

                            List<SAInvoice> lst = new List<SAInvoice>();
                            List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                            List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                x => new SAInvoice
                                {
                                    InvoiceTypeID = x.InvoiceTypeID,
                                    InvoiceForm = x.InvoiceForm,
                                    InvoiceTemplate = x.InvoiceTemplate,
                                    InvoiceNo = x.InvoiceNo
                                }
                                ).ToList();
                            List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                               x => new SAInvoice
                               {
                                   InvoiceTypeID = x.InvoiceTypeID,
                                   InvoiceForm = x.InvoiceForm,
                                   InvoiceTemplate = x.InvoiceTemplate,
                                   InvoiceNo = x.InvoiceNo
                               }
                               ).ToList();
                            if (lstsA.Count > 0) lst.AddRange(lstsA);
                            if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                            if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                            List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                            if (ls.Count > 0)
                            {
                                int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                txtSoHD.Text = maxno.ToString("0000000");
                            }
                            else
                            {
                                txtSoHD.Text = "0000001";
                            }
                        }
                    }
                }
                else
                {
                    var gttemp = lstinvoice1.FirstOrDefault(t => t.ID == guid);
                    if (gttemp != null)
                    {
                        txtKyHieuHD.Text = gttemp.InvoiceSeries;
                        _select.InvoiceTemplate = gttemp.InvoiceTemplate;
                        if (_statusForm == ConstFrm.optStatusForm.Add)
                        {
                            List<SAInvoice> lst = new List<SAInvoice>();
                            List<SAInvoice> lstsA = ISAInvoiceService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).ToList();
                            List<SAInvoice> lstpDr = IPPDiscountReturnService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                                x => new SAInvoice
                                {
                                    InvoiceTypeID = x.InvoiceTypeID,
                                    InvoiceForm = x.InvoiceForm,
                                    InvoiceTemplate = x.InvoiceTemplate,
                                    InvoiceNo = x.InvoiceNo
                                }
                                    ).ToList();
                            List<SAInvoice> lstSABill = ISABillService.GetAll().ToList().Where(x => x.ID != new Guid(cbbMauHD.Value.ToString())).Select(
                               x => new SAInvoice
                               {
                                   InvoiceTypeID = x.InvoiceTypeID,
                                   InvoiceForm = x.InvoiceForm,
                                   InvoiceTemplate = x.InvoiceTemplate,
                                   InvoiceNo = x.InvoiceNo
                               }
                               ).ToList();
                            if (lstsA.Count > 0) lst.AddRange(lstsA);
                            if (lstpDr.Count > 0) lst.AddRange(lstpDr);
                            if (lstSABill.Count > 0) lst.AddRange(lstSABill);
                            List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.InvoiceTypeID && x.InvoiceForm == _select.InvoiceForm && x.InvoiceTemplate == _select.InvoiceTemplate && x.InvoiceNo != null && x.InvoiceNo != "").ToList();
                            if (ls.Count > 0)
                            {
                                int maxno = ls.Select(n => int.Parse(n.InvoiceNo)).Max() + 1;
                                txtSoHD.Text = maxno.ToString("0000000");
                            }
                            else
                            {
                                txtSoHD.Text = "0000001";
                            }
                        }
                    }
                }
            }
        }

        private void dteNgayHD_Leave(object sender, EventArgs e)
        {
            try
            {
                _select.InvoiceDate = dteNgayHD.Value == null ? (DateTime?)null : Convert.ToDateTime(dteNgayHD.Value.ToString());
            }
            catch (Exception)
            {


            }

        }

        private void dteNgayHD_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                _select.InvoiceDate = dteNgayHD.Value == null ? (DateTime?)null : Convert.ToDateTime(dteNgayHD.Value.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtSoHD_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceNo = txtSoHD.Text;
        }

        private void txtKyHieuHD_TextChanged(object sender, EventArgs e)
        {
            _select.InvoiceSeries = txtKyHieuHD.Text;
        }
    }

    public class FSaBillDetailStand : DetailBase<SABill> { }
}