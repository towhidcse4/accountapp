﻿using Accounting.Core.Domain;
using Accounting.TextMessage;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace Accounting.Frm.FBusiness.SalePrice.FSABill
{
    public class ExcelSA
    {
        public readonly List<String> HEADER = new List<String> { "MaHD", "HinhThucHD", "LoaiHD", "MauHD", "KyHieuHD", "SoHD", "NgayHD", "HinhthucTT", "LoaiTien", "TrangThaiHD", "MaDoiTuong", "TenDoiTuong", "DiaChi", "MST", "TenNguoiLienHe", "DienGiai", "MaHang", "TenHang", "DVT", "SoLuong", "DonGia", "ThanhTien", "TyLeCK", "TienCK", "ThueSuat", "TienThue", "TyGia" };
        string path = "";
        _Application excel = new Application();
        Workbook wb;
        Worksheet ws;
        Range xlRange;
        List<string> lstMauHD = new List<string>();
        List<string> lstKyHieuHD = new List<string>();
        List<string> lstMaDoiTg = new List<string>();
        List<string> lstLoaiTien = new List<string>();
        List<string> lstSoHD = new List<string>();
        public static Dictionary<int, List<int>> ErrCell = new Dictionary<int, List<int>>();
        public List<InvoiceCheck> lstHDCheck = new List<InvoiceCheck>();
        public void ChooseSheet(int sheet)
        {
            ws = wb.Worksheets[sheet];
            xlRange = ws.UsedRange;
        }
        public ExcelSA(string path)
        {
            if (Utils.ListTT153Report.Count() > 0)
            {
                lstMauHD = Utils.ListTT153Report.Select(n => n.InvoiceTemplate).ToList();
                foreach (var item in Utils.ListTT153Report)
                {
                    List<int> lstShd = new List<int>();
                    var lst1 = Utils.ListSAInvoice.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null);
                    var lst2 = Utils.ListSABill.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.InvoiceNo != "" && n.InvoiceNo != null);
                    var lst3 = Utils.ListSAReturn.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.TypeID == 340 && n.InvoiceNo != "" && n.InvoiceNo != null);
                    var lst4 = Utils.ListPPDiscountReturn.Where(n => n.InvoiceTemplate == item.InvoiceTemplate && n.InvoiceSeries == item.InvoiceSeries && n.TypeID == 220 && n.InvoiceNo != "" && n.InvoiceNo != null);
                    if (lst1.Count() > 0)
                    {
                        lstShd.Add(lst1.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lst2.Count() > 0)
                    {
                        lstShd.Add(lst2.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lst3.Count() > 0)
                    {
                        lstShd.Add(lst3.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lst4.Count() > 0)
                    {
                        lstShd.Add(lst4.Select(n => int.Parse(n.InvoiceNo)).Max());
                    }
                    if (lstShd.Count() > 0)
                    {
                        lstHDCheck.Add(new InvoiceCheck
                        {
                            InvoiceTemplate = item.InvoiceTemplate,
                            InvoiceSeries = item.InvoiceSeries,
                            InvoiceNoMax = lstShd.Max()
                        });
                    }
                }
            }
            if (Utils.ListAccountingObject.Count() > 0)
            {
                lstMaDoiTg = Utils.ListAccountingObject.Select(n => n.AccountingObjectCode).ToList();
            }
            if (Utils.ListCurrency.Count() > 0)
            {
                lstLoaiTien = Utils.ListCurrency.Select(n => n.ID).ToList();
            }
            this.path = path;
            wb = excel.Workbooks.Open(@path);
            wb.Activate();
        }
        public void Exit()
        {
            object misValue = System.Reflection.Missing.Value;
            wb.Close(SaveChanges: false);
            excel.Quit();
            Marshal.FinalReleaseComObject(excel);
            releaseObject(ws);
            releaseObject(wb);
            releaseObject(excel);
        }
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);
        public List<SABill> GetObject(Dictionary<string, int> headers)
        {
            List<SABill> lst = new List<SABill>();
            ErrCell = new Dictionary<int, List<int>>();
            int MaxOrderPriority = 0;
            if (Utils.ListSABillDetail.Count() > 0)
            {
                MaxOrderPriority = (int)Utils.ListSABillDetail.Max(n => n.OrderPriority);
            }
            int x = xlRange.Rows.Count;
            int y = xlRange.Columns.Count;
            if (x < 2)
            {
                MSG.Error("File không có dữ liệu");
                Exit();
                return new List<SABill>();
            }
            for (int i = 2; i <= x; i++)
            {
                SABill sABill = new SABill();
                List<int> lstErrR = new List<int>();
                // Lấy mã HD
                string maHD = "";
                if (headers.ContainsKey(HEADER[0]))
                {
                    Range mahdr = (Range)ws.Cells[i, headers[HEADER[0]]];
                    mahdr.Merge();
                    mahdr.ClearComments();
                    mahdr.Interior.ColorIndex = 0;
                    try
                    {
                        maHD = ((string)mahdr.Value2);
                        if (lst.Any(n => n.MaHD.Equals(maHD)))
                        {
                            sABill.isHD = false;
                        }
                        else
                        {
                            sABill.isHD = true;
                        }
                    }
                    catch
                    {

                    }
                }
                if (string.IsNullOrEmpty(maHD)) continue;
                // Lấy Hình thức hóa đơn
                string HTHD = "";
                if (headers.ContainsKey(HEADER[1]))
                {
                    Range tennvr = (Range)ws.Cells[i, headers[HEADER[1]]];
                    tennvr.Merge();
                    tennvr.ClearComments();
                    tennvr.Interior.ColorIndex = 0;
                    try
                    {
                        HTHD = ((string)tennvr.Value2);
                        var item = tennvr.Value;
                    }
                    catch { }
                }

                // Lấy loai hd
                string loaiHD = "";
                if (headers.ContainsKey(HEADER[2]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[2]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        loaiHD = ((string)maPbr.Value2).ToUpper();
                    }
                    catch { }
                }
                // Lấy Mau HD
                string mauHD = "";
                if (headers.ContainsKey(HEADER[3]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[3]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        mauHD = ((string)maPbr.Value2).ToUpper();
                    }
                    catch { }
                }
                // Lấy KyHieuHD
                string kyHieuHD = "";
                if (headers.ContainsKey(HEADER[4]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[4]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        kyHieuHD = ((string)maPbr.Value2).ToUpper();
                    }
                    catch { }
                }
                // Lấy SoHD
                string soHD = "";
                if (headers.ContainsKey(HEADER[5]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[5]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        soHD = ((string)maPbr.Value2).ToUpper();
                    }
                    catch
                    {
                        try
                        {
                            int s = (int)maPbr.Value2;
                            soHD = s.ToString();
                        }
                        catch
                        {

                        }
                    }
                }
                // Lấy NgayHD
                DateTime ngayHD = DateTime.Now;
                if (headers.ContainsKey(HEADER[6]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[6]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        ngayHD = ((DateTime)maPbr.Value);
                    }
                    catch (Exception ex) { }
                }
                // Lấy Hinh thuc hoa don
                string hinhthucTT = "";
                if (headers.ContainsKey(HEADER[7]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[7]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        hinhthucTT = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy Loai tien
                string loaiTien = "";
                if (headers.ContainsKey(HEADER[8]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[8]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        loaiTien = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy TTHD
                string trangThaiHD = "";
                if (headers.ContainsKey(HEADER[9]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[9]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        trangThaiHD = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy MaDoiTuong
                string MaDoiTuong = "";
                if (headers.ContainsKey(HEADER[10]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[10]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        MaDoiTuong = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy TenDoituong
                string tenDoiTuong = "";
                if (headers.ContainsKey(HEADER[11]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[11]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        tenDoiTuong = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy DiaChi
                string diaChi = "";
                if (headers.ContainsKey(HEADER[12]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[12]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        diaChi = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy MST
                string MST = "";
                if (headers.ContainsKey(HEADER[13]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[13]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        MST = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy Ten Nguoi lien he
                string tenNguoiLH = "";
                if (headers.ContainsKey(HEADER[14]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[14]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        tenNguoiLH = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy Dien Giai
                string dienGiai = "";
                if (headers.ContainsKey(HEADER[15]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[15]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        dienGiai = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy MaHang
                string maHang = "";
                if (headers.ContainsKey(HEADER[16]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[16]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        maHang = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy Ten Hang
                string tenHang = "";
                if (headers.ContainsKey(HEADER[17]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[17]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        tenHang = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy DVT
                string DVT = "";
                if (headers.ContainsKey(HEADER[18]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[18]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        DVT = ((string)maPbr.Value2);
                    }
                    catch { }
                }
                // Lấy SoLuong
                decimal SoLuong = 0;
                if (headers.ContainsKey(HEADER[19]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[19]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        SoLuong = ((decimal)maPbr.Value2);
                    }
                    catch
                    {
                        lstErrR.Add(headers[HEADER[19]]);
                    }
                }
                // Lấy DonGia
                decimal donGia = 0;
                if (headers.ContainsKey(HEADER[20]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[20]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        donGia = ((decimal)maPbr.Value2);
                    }
                    catch { lstErrR.Add(headers[HEADER[20]]); }
                }
                // Lấy ThanhTien
                decimal thanhTien = 0;
                if (headers.ContainsKey(HEADER[21]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[21]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        thanhTien = ((decimal)maPbr.Value2);
                    }
                    catch { lstErrR.Add(headers[HEADER[21]]); }
                }
                // Lấy TyLeCK
                decimal TyLeCK = 0;
                if (headers.ContainsKey(HEADER[22]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[22]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        TyLeCK = ((decimal)maPbr.Value2);
                    }
                    catch
                    {
                        try
                        {
                            string check = ((string)maPbr.Value2);
                            if (!string.IsNullOrEmpty(check))
                                lstErrR.Add(headers[HEADER[22]]);
                        }
                        catch
                        {

                        }

                    }
                }
                // Lấy TienCK
                decimal tienCK = 0;
                if (headers.ContainsKey(HEADER[23]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[23]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        tienCK = ((decimal)maPbr.Value2);
                    }
                    catch
                    {
                        try
                        {
                            string check = ((string)maPbr.Value2);
                            if (!string.IsNullOrEmpty(check))
                                lstErrR.Add(headers[HEADER[23]]);
                        }
                        catch
                        {

                        }
                    }
                }
                // Lấy ThueSuat
                decimal thueSuat = 0;
                if (headers.ContainsKey(HEADER[24]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[24]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        thueSuat = ((decimal)maPbr.Value2);
                    }
                    catch
                    {
                        try
                        {
                            if ((string)maPbr.Value2 == "KCT")
                                thueSuat = -1;
                            else
                                lstErrR.Add(headers[HEADER[24]]);
                        }
                        catch
                        {
                            lstErrR.Add(headers[HEADER[24]]);
                        }

                    }
                }
                // Lấy TienThue
                decimal tienThue = 0;
                if (headers.ContainsKey(HEADER[25]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[25]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        tienThue = ((decimal)maPbr.Value2);
                    }
                    catch
                    {
                        if (maPbr.Value2 == null)
                        {
                            if (thueSuat != -1) lstErrR.Add(headers[HEADER[25]]);
                        }
                        else
                        {
                            lstErrR.Add(headers[HEADER[25]]);
                        }
                    }
                }
                // Lấy TyGia
                decimal TyGia = 0;
                if (headers.ContainsKey(HEADER[26]))
                {
                    Range maPbr = (Range)ws.Cells[i, headers[HEADER[26]]];
                    maPbr.Merge();
                    maPbr.ClearComments();
                    maPbr.Interior.ColorIndex = 0;
                    try
                    {
                        TyGia = ((decimal)maPbr.Value2);
                    }
                    catch { }
                }

                try
                {

                    #region Error
                    if (string.IsNullOrEmpty(maHD))
                    {
                        lstErrR.Add(headers[HEADER[0]]);
                    }
                    if (sABill.isHD)
                    {
                        if (lstMauHD.Count() > 0)
                            lstKyHieuHD = Utils.ListTT153Report.Where(n => n.InvoiceTemplate == mauHD).Select(n => n.InvoiceSeries).ToList();
                        List<string> invoiceTPL = new List<string> { "Hóa đơn điện tử", "Hóa đơn đặt in", "Hóa đơn tự in" };
                        List<string> listLoaiHD = new List<string> { "01/ hoặc 02/", "01GTKT", "02GTTT", "03XKNB", "04HGDL", "06HDXK", "07KPTQ" };
                        List<string> lstTTHD = new List<string> { "Hóa đơn mới tạo lâp", "Hóa đơn có chữ ký số", "Hóa đơn bị thay thế", "Hóa đơn bị điều chỉnh", "Hóa đơn bị huỷ" };
                        List<string> lstTTHD1 = new List<string> { "Hóa đơn có chữ ký số", "Hóa đơn bị thay thế", "Hóa đơn bị điều chỉnh", "Hóa đơn bị huỷ" };

                        if (!invoiceTPL.Contains(HTHD))
                        {
                            lstErrR.Add(headers[HEADER[1]]);
                        }
                        if (string.IsNullOrEmpty(loaiHD) || !listLoaiHD.Contains(loaiHD))
                        {
                            lstErrR.Add(headers[HEADER[2]]);
                        }
                        if (string.IsNullOrEmpty(mauHD) || !lstMauHD.Contains(mauHD))
                        {
                            lstErrR.Add(headers[HEADER[3]]);
                        }
                        if (string.IsNullOrEmpty(kyHieuHD) || !lstKyHieuHD.Contains(kyHieuHD))
                        {
                            lstErrR.Add(headers[HEADER[4]]);
                        }
                        if (ngayHD == null)
                        {
                            lstErrR.Add(headers[HEADER[6]]);
                        }
                        if (string.IsNullOrEmpty(hinhthucTT))
                        {
                            lstErrR.Add(headers[HEADER[7]]);
                        }
                        if (string.IsNullOrEmpty(loaiTien) || !lstLoaiTien.Contains(loaiTien))
                        {
                            lstErrR.Add(headers[HEADER[8]]);
                        }
                        if (TyGia == 0)
                        {
                            lstErrR.Add(headers[HEADER[26]]);
                        }
                        if (!string.IsNullOrEmpty(trangThaiHD) && !lstTTHD.Contains(trangThaiHD))
                        {
                            lstErrR.Add(headers[HEADER[9]]);
                        }
                        if (string.IsNullOrEmpty(MaDoiTuong) || !lstMaDoiTg.Contains(MaDoiTuong))
                        {
                            lstErrR.Add(headers[HEADER[10]]);
                        }
                        if (string.IsNullOrEmpty(tenDoiTuong))
                        {
                            lstErrR.Add(headers[HEADER[11]]);
                        }
                        if (!string.IsNullOrEmpty(MST) && !Core.Utils.CheckMST(MST))
                        {
                            lstErrR.Add(headers[HEADER[13]]);
                        }
                        if (!string.IsNullOrEmpty(trangThaiHD) && lstTTHD1.Contains(trangThaiHD))
                        {
                            if (HTHD.Equals("Hóa đơn điện tử"))
                                if (string.IsNullOrEmpty(soHD))
                                {
                                    lstErrR.Add(headers[HEADER[5]]);
                                }
                        }

                        if (!string.IsNullOrEmpty(mauHD) && !string.IsNullOrEmpty(kyHieuHD))
                        {
                            if (!string.IsNullOrEmpty(soHD))
                            {
                                int result;
                                if (int.TryParse(soHD, out result))
                                {
                                    if (lstHDCheck.Any(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD))
                                    {
                                        if (result <= lstHDCheck.FirstOrDefault(n => n.InvoiceTemplate == mauHD && n.InvoiceSeries == kyHieuHD).InvoiceNoMax)
                                        {
                                            lstErrR.Add(headers[HEADER[5]]);
                                        }
                                    }
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                    List<int> lstThueSuat = new List<int> { 0, 5, 10, -1 };
                    if (!lstThueSuat.Contains(Convert.ToInt32(thueSuat)))
                    {
                        lstErrR.Add(headers[HEADER[24]]);
                    }
                    if (string.IsNullOrEmpty(maHang) || !Utils.ListMaterialGoods.Any(n => n.MaterialGoodsCode == maHang))
                    {
                        lstErrR.Add(headers[HEADER[16]]);
                    }
                    if (string.IsNullOrEmpty(tenHang))
                    {
                        lstErrR.Add(headers[HEADER[17]]);
                    }
                    if (lstErrR.Count > 0)
                    {
                        ErrCell.Add(i, lstErrR);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    Exit();
                    WaitingFrm.StopWaiting();
                    return new List<SABill>();
                }


                sABill.MaHD = maHD;
                if (!string.IsNullOrEmpty(HTHD))
                {
                    if (HTHD.Equals("Hóa đơn điện tử")) sABill.InvoiceForm = 2;
                    if (HTHD.Equals("Hóa đơn đặt in")) sABill.InvoiceForm = 1;
                    if (HTHD.Equals("Hóa đơn tự in")) sABill.InvoiceForm = 0;
                }

                InvoiceType invoiceType = Utils.ListInvoiceType.FirstOrDefault(n => n.InvoiceTypeCode.Equals(loaiHD));
                if (invoiceType != null)
                {
                    sABill.InvoiceType = invoiceType._InvoiceType;
                    sABill.InvoiceTypeID = invoiceType.ID;
                }
                sABill.InvoiceTemplate = mauHD;
                sABill.InvoiceSeries = kyHieuHD;
                if (!string.IsNullOrEmpty(soHD))
                    sABill.InvoiceNo = soHD.PadLeft(7, '0');
                sABill.InvoiceDate = ngayHD;
                sABill.PaymentMethod = hinhthucTT;
                sABill.CurrencyID = loaiTien;
                sABill.ExchangeRate = TyGia;
                if (!string.IsNullOrEmpty(trangThaiHD))
                {
                    if (trangThaiHD.Equals("Hóa đơn mới tạo lâp")) sABill.StatusInvoice = 0;
                    if (trangThaiHD.Equals("Hóa đơn có chữ ký số")) sABill.StatusInvoice = 1;
                    if (trangThaiHD.Equals("Hóa đơn bị thay thế")) sABill.StatusInvoice = 3;
                    if (trangThaiHD.Equals("Hóa đơn bị điều chỉnh")) sABill.StatusInvoice = 4;
                    if (trangThaiHD.Equals("Hóa đơn bị huỷ")) sABill.StatusInvoice = 5;
                }

                AccountingObject accountingObject = Utils.ListAccountingObject.FirstOrDefault(n => n.AccountingObjectCode.Equals(MaDoiTuong));
                if (accountingObject != null)
                {
                    sABill.AccountingObjectID = accountingObject.ID;
                }
                sABill.AccountingObjectName = tenDoiTuong;
                sABill.AccountingObjectAddress = diaChi;
                sABill.CompanyTaxCode = MST;
                sABill.ContactName = tenNguoiLH;
                sABill.Reason = dienGiai;

                SABillDetail sABillDetail = new SABillDetail();
                MaterialGoods materialGoods = Utils.ListMaterialGoods.FirstOrDefault(n => n.MaterialGoodsCode.Equals(maHang));
                if (materialGoods != null)
                {
                    sABillDetail.MaterialGoodsID = materialGoods.ID;
                }
                sABillDetail.Unit = DVT;
                sABillDetail.Description = tenHang;
                sABillDetail.Quantity = SoLuong;
                sABillDetail.UnitPriceOriginal = donGia;
                sABillDetail.UnitPrice = donGia;
                sABillDetail.UnitPriceOriginal = donGia;
                sABillDetail.Amount = thanhTien;
                sABillDetail.AmountOriginal = thanhTien;
                sABillDetail.DiscountRate = TyLeCK;
                sABillDetail.DiscountAmount = tienCK;
                sABillDetail.DiscountAmountOriginal = tienCK;
                sABillDetail.VATRate = thueSuat;
                sABillDetail.VATAmount = tienThue;
                sABillDetail.VATAmountOriginal = tienThue;
                sABillDetail.OrderPriority = MaxOrderPriority++;

                sABill.SABillDetails.Add(sABillDetail);
                lst.Add(sABill);
            }
            if (ErrCell.Count() > 0)
            {
                foreach (var item in ErrCell)
                {
                    foreach (var itemD in item.Value)
                    {
                        Range maPbr = (Range)ws.Cells[item.Key, itemD];
                        maPbr.Merge();
                        maPbr.Interior.Color = XlRgbColor.rgbRed;

                        if (itemD == headers[HEADER[5]])
                        {
                            maPbr.AddComment("Số hóa đơn không hợp lệ hoặc bị trùng");
                        }
                        if (itemD == headers[HEADER[16]])
                        {
                            maPbr.AddComment("Mã hàng không hợp lệ");
                        }
                        if (itemD == headers[HEADER[10]])
                        {
                            maPbr.AddComment("Mã đối tượng không hợp lệ");
                        }
                        if (itemD == headers[HEADER[8]])
                        {
                            maPbr.AddComment("Loại tiền không hợp lệ");
                        }
                        if (itemD == headers[HEADER[9]])
                        {
                            maPbr.AddComment("Trạng thái hóa đơn không hợp lệ");
                        }
                        if (itemD == headers[HEADER[13]])
                        {
                            maPbr.AddComment("Mã số thuế không hợp lệ");
                        }
                        if (itemD == headers[HEADER[6]])
                        {
                            maPbr.AddComment("Sai định dạng ngày hóa đơn");
                        }
                        if (itemD == headers[HEADER[11]])
                        {
                            maPbr.AddComment("Tên đối tượng không được để trống");
                        }
                        if (itemD == headers[HEADER[1]])
                        {
                            maPbr.AddComment("Hình thức hóa đơn không hợp lệ");
                        }
                        if (itemD == headers[HEADER[2]])
                        {
                            maPbr.AddComment("Loại hóa đơn không hợp lệ");
                        }
                        if (itemD == headers[HEADER[3]])
                        {
                            maPbr.AddComment("Mẫu số hóa đơn không hợp lệ");
                        }
                        if (itemD == headers[HEADER[4]])
                        {
                            maPbr.AddComment("Ký hiệu hóa đơn không hợp lệ");
                        }
                        if (itemD == headers[HEADER[22]])
                        {
                            maPbr.AddComment("Tỷ lệ chiết khấu không hợp lệ");
                        }
                        if (itemD == headers[HEADER[23]])
                        {
                            maPbr.AddComment("Tiền chiết khấu không hợp lệ");
                        }
                        if (itemD == headers[HEADER[24]])
                        {
                            maPbr.AddComment("Thuế suất không hợp lệ");
                        }
                        if (itemD == headers[HEADER[25]])
                        {
                            maPbr.AddComment("Tiền thuế không hợp lệ");
                        }
                    }
                }
                WaitingFrm.StopWaiting();
                if (MSG.Question("Dữ liệu file không đúng. Bạn có muốn tải file mẫu không?") == System.Windows.Forms.DialogResult.Yes)
                {
                    System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog
                    {
                        FileName = "Upload_Invoice_Error.xlsx",
                        AddExtension = true,
                        Filter = "Excel Document(*.xlsx)|*.xlsx|Excel Document (*.xls)|*.xls"
                    };
                    sf.Title = "Chọn thư mục lưu file";
                    if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ws.SaveAs(sf.FileName);
                        Exit();
                        if (MSG.Question("Tải xuống thành công, Bạn có muốn mở file vừa tải") == System.Windows.Forms.DialogResult.Yes)
                        {
                            try
                            {
                                System.Diagnostics.Process.Start(sf.FileName);
                            }
                            catch
                            {
                                return new List<SABill>();
                            }

                        }
                    }
                    else
                    {
                        Exit();
                        return new List<SABill>();
                    }
                }
                else
                {
                    Exit();
                    return new List<SABill>();
                }

                //try
                //{
                //    int processid;
                //    int threadid = GetWindowThreadProcessId(excel.Hwnd, out processid);
                //    Process processes = Process.GetProcessById(excel.Hwnd);
                //    //if (processes != null) processes.Kill();
                //}
                //catch(Exception ex)
                //{

                //}


                return new List<SABill>();
            }

            List<SABill> lstSAB = new List<SABill>();
            foreach (var item in lst.GroupBy(n => n.MaHD))
            {
                SABill sABill = item.FirstOrDefault(n => n.isHD);
                sABill.ID = Guid.NewGuid();
                sABill.SABillDetails.First().SABillID = item.FirstOrDefault(n => n.isHD).ID;
                foreach (var itemD in item.Where(n => !n.isHD))
                {
                    itemD.SABillDetails.First().SABillID = item.FirstOrDefault(n => n.isHD).ID;
                    sABill.SABillDetails.Add(itemD.SABillDetails.First());
                }
                sABill.TotalAmount = sABill.SABillDetails.Sum(n => n.Amount);
                sABill.TotalAmountOriginal = sABill.SABillDetails.Sum(n => n.AmountOriginal);
                sABill.TotalDiscountAmount = sABill.SABillDetails.Sum(n => n.DiscountAmount);
                sABill.TotalDiscountAmountOriginal = sABill.SABillDetails.Sum(n => n.DiscountAmountOriginal);
                sABill.TotalVATAmount = sABill.SABillDetails.Sum(n => n.VATAmount);
                sABill.TotalDiscountAmountOriginal = sABill.SABillDetails.Sum(n => n.VATAmountOriginal);
                lstSAB.Add(sABill);
            }
            Exit();
            return lstSAB;
        }
        //Hàm thu hồi bộ nhớ cho COM Excel
        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                obj = null;
            }
            finally
            { GC.Collect(); }
        }

        public string ReadCell(int i, int j)
        {
            i++;
            j++;
            Range x = (Range)ws.Cells[i, j];
            if (x.Value2 != null) return (string)x.Value2;
            else return "";
        }
        public Dictionary<string, int> ReadHeader()
        {
            int y = xlRange.Columns.Count;
            Dictionary<string, int> headers = new Dictionary<string, int>();
            for (int i = 1; i <= y; i++)
            {
                Range x = (Range)ws.Cells[1, i];
                if (x.Value2 != null)
                {
                    string header = "";
                    try
                    {
                        header = ((string)x.Value2);
                    }
                    catch
                    {
                        header = ((double)x.Value2).ToString();
                    }

                    if (HEADER.Contains(header) && !headers.ContainsKey(header))
                    {
                        headers.Add(header, i);
                    }
                };
            }
            return headers;
        }
        public List<string> ReadSheetName()
        {
            List<string> result = new List<string>();
            for (int i = 1; i <= wb.Worksheets.Count; i++)
            {
                result.Add(wb.Worksheets[i].Name);
            }
            return result;
        }
    }
    public class InvoiceCheck
    {
        public string InvoiceTemplate { get; set; }
        public string InvoiceSeries { get; set; }
        public int InvoiceNoMax { get; set; }

    }
}
