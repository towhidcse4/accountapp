﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.Core.Domain;
using System.Linq;
using Accounting.TextMessage;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.ComponentModel;

namespace Accounting
{
    public partial class FchoseSaInvoice<T> : DialogForm
    {
        #region Khai báo
        private readonly BindingList<IList> _input;
        public List<SAInvoice> _lstSA { get; private set; }
        private SABill _select = new SABill();
        private Guid? AccountingObjectId;
        #endregion

        public FchoseSaInvoice(BindingList<IList> input, T @select, Guid? accountingObjectId)
        {
            InitializeComponent();
            _select = @select as SABill;
            AccountingObjectId = accountingObjectId;
            _lstSA = new List<SAInvoice>();
            var lstRefDetailId = new List<SABillDetail>();
            if (_select != null) lstRefDetailId = _select.SABillDetails.ToList();
            Utils.ProcessControls(this);
            var result = new List<SAInvoice>();
            if (accountingObjectId.HasValue)
            {
                result = Utils.ISAInvoiceService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.PostedDate <= dtEndDate.DateTime.Date && x.PostedDate >= dtBeginDate.DateTime.Date && x.CurrencyID == _select.CurrencyID && !x.IsBill && !x.IsAttachListBill).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
                Utils.ISAInvoiceService.UnbindSession(result);
                result = Utils.ISAInvoiceService.Query.Where(x => x.AccountingObjectID == (Guid)accountingObjectId && x.PostedDate <= dtEndDate.DateTime.Date && x.PostedDate >= dtBeginDate.DateTime.Date && x.CurrencyID == _select.CurrencyID && !x.IsBill && !x.IsAttachListBill).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();               
            }
            uGrid.DataSource = result;
            _lstSA = result;
            ConfigGrid(uGrid);

            _input = input;
        }

        #region Event

        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            //chọn tất cả các hàng hóa cùng đơn đặt hàng
            if (e.Cell.Column.Key == "Check")
            {
                var gid = (Guid?)e.Cell.Row.Cells["ID"].Value;
                if (gid.HasValue)
                {
                    e.Cell.Row.Cells["Check"].Value = !(bool)e.Cell.Value;
                }
                uGrid.UpdateDataGrid();
            }
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid _uGrid)
        {
            Utils.ConfigGrid(_uGrid, ConstDatabase.SAInvoice_TableName);
            _uGrid.DisplayLayout.Bands[0].Columns["InvoiceNo"].Hidden = true;
            _uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            _uGrid.DisplayLayout.UseFixedHeaders = false;
            _uGrid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
            _uGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            foreach (var column in _uGrid.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            UltraGridColumn ugc = uGrid.DisplayLayout.Bands[0].Columns["Check"];
            ugc.Hidden = false;
            ugc.Header.VisiblePosition = 0;
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellClickAction = CellClickAction.Edit;
        }       
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            var lstSAOrder = from u in _lstSA where u.Check select u;
            if (lstSAOrder.Count() == 0)
            {
                MSG.Warning("Bạn chưa chọn chứng từ nào! Xin vui lòng chọn lại!");
                return;
            }
            _lstSA = lstSAOrder.ToList();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            var result = new List<SAInvoice>();
            if (AccountingObjectId.HasValue)
            {
                result = Utils.ISAInvoiceService.Query.Where(x => x.AccountingObjectID == (Guid)AccountingObjectId && x.PostedDate <= dtEndDate.DateTime.Date && x.PostedDate >= dtBeginDate.DateTime.Date && x.CurrencyID == _select.CurrencyID && !x.IsBill && !x.IsAttachListBill).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
                Utils.ISAInvoiceService.UnbindSession(result);
                result = Utils.ISAInvoiceService.Query.Where(x => x.AccountingObjectID == (Guid)AccountingObjectId && x.PostedDate <= dtEndDate.DateTime.Date && x.PostedDate >= dtBeginDate.DateTime.Date && x.CurrencyID == _select.CurrencyID && !x.IsBill && !x.IsAttachListBill).OrderByDescending(s => s.Date).ThenByDescending(s => s.No).ToList();
            }
            uGrid.DataSource = result;
            _lstSA = result;
        }
    }
}
