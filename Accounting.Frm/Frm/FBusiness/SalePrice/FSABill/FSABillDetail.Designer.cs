﻿namespace Accounting
{
    sealed partial class FSABillDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbLoaiHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbHinhThucHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtKyHieuHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel44 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbMauHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dteNgayHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAccountingBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.btnSaInvoice = new Infragistics.Win.Misc.UltraButton();
            this.dteListDateHD = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtListCommonNameInventoryHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.txtListNoHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.chkisAttachListHD = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDHD = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCompanyTaxCodeHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtReasonHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactNameHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressHD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.palTop = new System.Windows.Forms.Panel();
            this.ultraGroupBoxTop = new Infragistics.Win.Misc.UltraGroupBox();
            this.palChung = new System.Windows.Forms.Panel();
            this.uComboThanhToan = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblTT = new Infragistics.Win.Misc.UltraLabel();
            this.palFill = new System.Windows.Forms.Panel();
            this.palGrid = new System.Windows.Forms.Panel();
            this.pnlUgrid = new Infragistics.Win.Misc.UltraPanel();
            this.btnOriginalVoucher = new Infragistics.Win.Misc.UltraButton();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.palThongTinChung = new System.Windows.Forms.Panel();
            this.group = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.palBottom = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDocumentNote = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDocumentNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.DocumentDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtOldInvSeries = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvTemplate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtOldInvNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblTttt = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalDiscountAmountSubOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalDiscountAmountSub = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountOriginalStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalPaymentAmountStand = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.txtTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalVATAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalPaymentAmount = new Infragistics.Win.Misc.UltraLabel();
            this.lblTotalAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBoxGiayBaoCo = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblKhachHangGBC = new Infragistics.Win.Misc.UltraLabel();
            this.lblDiaChiGBC = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressGBC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMReasonPayGBC = new Infragistics.Win.Misc.UltraLabel();
            this.txtMReasonPayGBC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameGBC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNopTienTaiKhoanGBC = new Infragistics.Win.Misc.UltraLabel();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBoxPhieuThu = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblKhachHangPT = new Infragistics.Win.Misc.UltraLabel();
            this.lblDiaChiPT = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtMContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblMReasonPayPT = new Infragistics.Win.Misc.UltraLabel();
            this.txtMReasonPayPT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNamePT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttachPT = new Infragistics.Win.Misc.UltraLabel();
            this.txtNumberAttachPT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblChungTuGocPT = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBoxPhieuXuatKho = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblKhachHangPXK = new Infragistics.Win.Misc.UltraLabel();
            this.lblDiaChiPXK = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddressPXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtSContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblSReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtSReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNamePXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNumberAttachPXK = new Infragistics.Win.Misc.UltraLabel();
            this.txtOriginalNoPXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblChungTuGoc = new Infragistics.Win.Misc.UltraLabel();
            this.lblCompanyTaxCodePXK = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyTaxCodePXK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBoxHoaDon = new Infragistics.Win.Misc.UltraGroupBox();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectAddress = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblContactName = new Infragistics.Win.Misc.UltraLabel();
            this.txtContactName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReason = new Infragistics.Win.Misc.UltraLabel();
            this.txtReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblCompanyTaxCode = new Infragistics.Win.Misc.UltraLabel();
            this.txtCompanyTaxCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSAOrder = new Infragistics.Win.Misc.UltraButton();
            this.btnCongNo = new Infragistics.Win.Misc.UltraButton();
            this.lblisAttachList = new Infragistics.Win.Misc.UltraLabel();
            this.chkisAttachList = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.lblListNo = new Infragistics.Win.Misc.UltraLabel();
            this.txtListNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblListDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblListCommonNameInventory = new Infragistics.Win.Misc.UltraLabel();
            this.txtListCommonNameInventory = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dteListDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridControl = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHinhThucHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMauHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDateHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventoryHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNoHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachListHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).BeginInit();
            this.ultraGroupBoxTop.SuspendLayout();
            this.palChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboThanhToan)).BeginInit();
            this.palFill.SuspendLayout();
            this.palGrid.SuspendLayout();
            this.pnlUgrid.ClientArea.SuspendLayout();
            this.pnlUgrid.SuspendLayout();
            this.palThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group)).BeginInit();
            this.group.SuspendLayout();
            this.palBottom.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentDate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxGiayBaoCo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressGBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayGBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameGBC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuThu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDate)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(927, 148);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox4.Controls.Add(this.ultraLabel40);
            this.ultraGroupBox4.Controls.Add(this.cbbLoaiHD);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel41);
            this.ultraGroupBox4.Controls.Add(this.cbbHinhThucHD);
            this.ultraGroupBox4.Controls.Add(this.txtSoHD);
            this.ultraGroupBox4.Controls.Add(this.txtKyHieuHD);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel42);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel43);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel44);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel45);
            this.ultraGroupBox4.Controls.Add(this.cbbMauHD);
            this.ultraGroupBox4.Controls.Add(this.dteNgayHD);
            this.ultraGroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox4.Location = new System.Drawing.Point(636, 2);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(290, 147);
            this.ultraGroupBox4.TabIndex = 57;
            this.ultraGroupBox4.Text = "Hóa đơn";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel40
            // 
            this.ultraLabel40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel40.Appearance = appearance1;
            this.ultraLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel40.Location = new System.Drawing.Point(7, 44);
            this.ultraLabel40.Name = "ultraLabel40";
            this.ultraLabel40.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel40.TabIndex = 75;
            this.ultraLabel40.Text = "Loại HĐ";
            // 
            // cbbLoaiHD
            // 
            this.cbbLoaiHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbLoaiHD.AutoSize = false;
            this.cbbLoaiHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbLoaiHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbLoaiHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiHD.Location = new System.Drawing.Point(78, 44);
            this.cbbLoaiHD.Name = "cbbLoaiHD";
            this.cbbLoaiHD.Size = new System.Drawing.Size(205, 22);
            this.cbbLoaiHD.TabIndex = 15;
            // 
            // ultraLabel41
            // 
            this.ultraLabel41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel41.Appearance = appearance2;
            this.ultraLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel41.Location = new System.Drawing.Point(7, 20);
            this.ultraLabel41.Name = "ultraLabel41";
            this.ultraLabel41.Size = new System.Drawing.Size(68, 22);
            this.ultraLabel41.TabIndex = 73;
            this.ultraLabel41.Text = "Hình thứcHĐ";
            // 
            // cbbHinhThucHD
            // 
            this.cbbHinhThucHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbHinhThucHD.AutoSize = false;
            this.cbbHinhThucHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbHinhThucHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbHinhThucHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbHinhThucHD.Location = new System.Drawing.Point(78, 20);
            this.cbbHinhThucHD.Name = "cbbHinhThucHD";
            this.cbbHinhThucHD.Size = new System.Drawing.Size(205, 22);
            this.cbbHinhThucHD.TabIndex = 14;
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoSize = false;
            this.txtSoHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(78, 118);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(71, 22);
            this.txtSoHD.TabIndex = 18;
            this.txtSoHD.TextChanged += new System.EventHandler(this.txtSoHD_TextChanged);
            // 
            // txtKyHieuHD
            // 
            this.txtKyHieuHD.AutoSize = false;
            this.txtKyHieuHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuHD.Location = new System.Drawing.Point(78, 93);
            this.txtKyHieuHD.Name = "txtKyHieuHD";
            this.txtKyHieuHD.Size = new System.Drawing.Size(205, 22);
            this.txtKyHieuHD.TabIndex = 17;
            this.txtKyHieuHD.TextChanged += new System.EventHandler(this.txtKyHieuHD_TextChanged);
            // 
            // ultraLabel42
            // 
            this.ultraLabel42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel42.Appearance = appearance3;
            this.ultraLabel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel42.Location = new System.Drawing.Point(150, 119);
            this.ultraLabel42.Name = "ultraLabel42";
            this.ultraLabel42.Size = new System.Drawing.Size(50, 22);
            this.ultraLabel42.TabIndex = 69;
            this.ultraLabel42.Text = "Ngày HĐ";
            // 
            // ultraLabel43
            // 
            this.ultraLabel43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel43.Appearance = appearance4;
            this.ultraLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel43.Location = new System.Drawing.Point(6, 118);
            this.ultraLabel43.Name = "ultraLabel43";
            this.ultraLabel43.Size = new System.Drawing.Size(40, 22);
            this.ultraLabel43.TabIndex = 68;
            this.ultraLabel43.Text = "Số HĐ";
            // 
            // ultraLabel44
            // 
            this.ultraLabel44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel44.Appearance = appearance5;
            this.ultraLabel44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel44.Location = new System.Drawing.Point(6, 93);
            this.ultraLabel44.Name = "ultraLabel44";
            this.ultraLabel44.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel44.TabIndex = 67;
            this.ultraLabel44.Text = "Ký hiệu HĐ";
            // 
            // ultraLabel45
            // 
            this.ultraLabel45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel45.Appearance = appearance6;
            this.ultraLabel45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel45.Location = new System.Drawing.Point(8, 72);
            this.ultraLabel45.Name = "ultraLabel45";
            this.ultraLabel45.Size = new System.Drawing.Size(62, 22);
            this.ultraLabel45.TabIndex = 66;
            this.ultraLabel45.Text = "Mẫu số HĐ";
            // 
            // cbbMauHD
            // 
            this.cbbMauHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMauHD.AutoSize = false;
            this.cbbMauHD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbMauHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbMauHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMauHD.Location = new System.Drawing.Point(78, 69);
            this.cbbMauHD.Name = "cbbMauHD";
            this.cbbMauHD.Size = new System.Drawing.Size(205, 22);
            this.cbbMauHD.TabIndex = 16;
            // 
            // dteNgayHD
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.dteNgayHD.Appearance = appearance7;
            this.dteNgayHD.AutoSize = false;
            this.dteNgayHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteNgayHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.dteNgayHD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteNgayHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteNgayHD.Location = new System.Drawing.Point(200, 119);
            this.dteNgayHD.MaskInput = "";
            this.dteNgayHD.Name = "dteNgayHD";
            this.dteNgayHD.Size = new System.Drawing.Size(84, 22);
            this.dteNgayHD.TabIndex = 19;
            this.dteNgayHD.Value = null;
            this.dteNgayHD.ValueChanged += new System.EventHandler(this.dteNgayHD_ValueChanged);
            this.dteNgayHD.Leave += new System.EventHandler(this.dteNgayHD_Leave);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.txtAccountingBankName);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectBankAccount);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel33);
            this.ultraGroupBox1.Controls.Add(this.btnSaInvoice);
            this.ultraGroupBox1.Controls.Add(this.dteListDateHD);
            this.ultraGroupBox1.Controls.Add(this.txtListCommonNameInventoryHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.txtListNoHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.chkisAttachListHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectNameHD);
            this.ultraGroupBox1.Controls.Add(this.cbbAccountingObjectIDHD);
            this.ultraGroupBox1.Controls.Add(this.txtCompanyTaxCodeHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox1.Controls.Add(this.txtReasonHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox1.Controls.Add(this.txtContactNameHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox1.Controls.Add(this.txtAccountingObjectAddressHD);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel13);
            appearance22.FontData.BoldAsString = "True";
            appearance22.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance22;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(636, 148);
            this.ultraGroupBox1.TabIndex = 56;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtAccountingBankName
            // 
            this.txtAccountingBankName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingBankName.AutoSize = false;
            this.txtAccountingBankName.Location = new System.Drawing.Point(495, 68);
            this.txtAccountingBankName.Name = "txtAccountingBankName";
            this.txtAccountingBankName.Size = new System.Drawing.Size(132, 22);
            this.txtAccountingBankName.TabIndex = 67;
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance8;
            this.ultraLabel16.Location = new System.Drawing.Point(403, 68);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(89, 22);
            this.ultraLabel16.TabIndex = 68;
            this.ultraLabel16.Text = "Tên ngân hàng";
            // 
            // cbbAccountingObjectBankAccount
            // 
            this.cbbAccountingObjectBankAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbAccountingObjectBankAccount.AutoSize = false;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(264, 68);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(138, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 66;
            // 
            // ultraLabel33
            // 
            this.ultraLabel33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextVAlignAsString = "Middle";
            this.ultraLabel33.Appearance = appearance9;
            this.ultraLabel33.Location = new System.Drawing.Point(181, 68);
            this.ultraLabel33.Name = "ultraLabel33";
            this.ultraLabel33.Size = new System.Drawing.Size(80, 21);
            this.ultraLabel33.TabIndex = 65;
            this.ultraLabel33.Text = "TK ngân hàng";
            // 
            // btnSaInvoice
            // 
            this.btnSaInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaInvoice.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnSaInvoice.Location = new System.Drawing.Point(495, 19);
            this.btnSaInvoice.Name = "btnSaInvoice";
            this.btnSaInvoice.Size = new System.Drawing.Size(132, 23);
            this.btnSaInvoice.TabIndex = 43;
            this.btnSaInvoice.Text = "Chứng từ bán hàng";
            this.btnSaInvoice.Click += new System.EventHandler(this.btnSaInvoice_Click);
            // 
            // dteListDateHD
            // 
            appearance10.TextHAlignAsString = "Center";
            appearance10.TextVAlignAsString = "Middle";
            this.dteListDateHD.Appearance = appearance10;
            this.dteListDateHD.AutoSize = false;
            this.dteListDateHD.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteListDateHD.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteListDateHD.Location = new System.Drawing.Point(331, 116);
            this.dteListDateHD.MaskInput = "";
            this.dteListDateHD.Name = "dteListDateHD";
            this.dteListDateHD.Size = new System.Drawing.Size(109, 22);
            this.dteListDateHD.TabIndex = 9;
            this.dteListDateHD.Value = null;
            // 
            // txtListCommonNameInventoryHD
            // 
            this.txtListCommonNameInventoryHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtListCommonNameInventoryHD.AutoSize = false;
            this.txtListCommonNameInventoryHD.Location = new System.Drawing.Point(545, 116);
            this.txtListCommonNameInventoryHD.Name = "txtListCommonNameInventoryHD";
            this.txtListCommonNameInventoryHD.Size = new System.Drawing.Size(82, 22);
            this.txtListCommonNameInventoryHD.TabIndex = 10;
            // 
            // ultraLabel5
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance11;
            this.ultraLabel5.Location = new System.Drawing.Point(450, 116);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(107, 22);
            this.ultraLabel5.TabIndex = 41;
            this.ultraLabel5.Text = "Mặt hàng chung";
            // 
            // ultraLabel6
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance12;
            this.ultraLabel6.Location = new System.Drawing.Point(292, 116);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(47, 22);
            this.ultraLabel6.TabIndex = 39;
            this.ultraLabel6.Text = "Ngày";
            // 
            // txtListNoHD
            // 
            this.txtListNoHD.AutoSize = false;
            this.txtListNoHD.Location = new System.Drawing.Point(156, 116);
            this.txtListNoHD.Name = "txtListNoHD";
            this.txtListNoHD.Size = new System.Drawing.Size(128, 22);
            this.txtListNoHD.TabIndex = 8;
            // 
            // ultraLabel7
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance13;
            this.ultraLabel7.Location = new System.Drawing.Point(127, 116);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(23, 22);
            this.ultraLabel7.TabIndex = 37;
            this.ultraLabel7.Text = "Số";
            // 
            // chkisAttachListHD
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            this.chkisAttachListHD.Appearance = appearance14;
            this.chkisAttachListHD.BackColor = System.Drawing.Color.Transparent;
            this.chkisAttachListHD.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkisAttachListHD.Location = new System.Drawing.Point(107, 116);
            this.chkisAttachListHD.Name = "chkisAttachListHD";
            this.chkisAttachListHD.Size = new System.Drawing.Size(14, 22);
            this.chkisAttachListHD.TabIndex = 7;
            this.chkisAttachListHD.CheckedChanged += new System.EventHandler(this.ChkisAttachListCheckedChanged);
            // 
            // ultraLabel8
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance15;
            this.ultraLabel8.Location = new System.Drawing.Point(6, 116);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel8.TabIndex = 35;
            this.ultraLabel8.Text = "In kèm bảng kê";
            // 
            // txtAccountingObjectNameHD
            // 
            this.txtAccountingObjectNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameHD.AutoSize = false;
            this.txtAccountingObjectNameHD.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNameHD.Name = "txtAccountingObjectNameHD";
            this.txtAccountingObjectNameHD.Size = new System.Drawing.Size(199, 22);
            this.txtAccountingObjectNameHD.TabIndex = 2;
            this.txtAccountingObjectNameHD.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // cbbAccountingObjectIDHD
            // 
            this.cbbAccountingObjectIDHD.AutoSize = false;
            appearance16.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance16;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectIDHD.ButtonsRight.Add(editorButton1);
            this.cbbAccountingObjectIDHD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectIDHD.Location = new System.Drawing.Point(107, 20);
            this.cbbAccountingObjectIDHD.Name = "cbbAccountingObjectIDHD";
            this.cbbAccountingObjectIDHD.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectIDHD.Size = new System.Drawing.Size(177, 22);
            this.cbbAccountingObjectIDHD.TabIndex = 1;
            // 
            // txtCompanyTaxCodeHD
            // 
            this.txtCompanyTaxCodeHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompanyTaxCodeHD.AutoSize = false;
            this.txtCompanyTaxCodeHD.Location = new System.Drawing.Point(495, 44);
            this.txtCompanyTaxCodeHD.Name = "txtCompanyTaxCodeHD";
            this.txtCompanyTaxCodeHD.Size = new System.Drawing.Size(132, 22);
            this.txtCompanyTaxCodeHD.TabIndex = 4;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance17;
            this.ultraLabel9.Location = new System.Drawing.Point(405, 44);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel9.TabIndex = 28;
            this.ultraLabel9.Text = "Mã số thuế";
            // 
            // txtReasonHD
            // 
            this.txtReasonHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonHD.AutoSize = false;
            this.txtReasonHD.Location = new System.Drawing.Point(107, 92);
            this.txtReasonHD.Name = "txtReasonHD";
            this.txtReasonHD.Size = new System.Drawing.Size(520, 22);
            this.txtReasonHD.TabIndex = 6;
            // 
            // ultraLabel10
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance18;
            this.ultraLabel10.Location = new System.Drawing.Point(6, 92);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel10.TabIndex = 26;
            this.ultraLabel10.Text = "Diễn giải";
            // 
            // txtContactNameHD
            // 
            this.txtContactNameHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactNameHD.AutoSize = false;
            this.txtContactNameHD.Location = new System.Drawing.Point(107, 68);
            this.txtContactNameHD.Name = "txtContactNameHD";
            this.txtContactNameHD.Size = new System.Drawing.Size(68, 22);
            this.txtContactNameHD.TabIndex = 5;
            // 
            // ultraLabel11
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance19;
            this.ultraLabel11.Location = new System.Drawing.Point(6, 68);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(116, 22);
            this.ultraLabel11.TabIndex = 24;
            this.ultraLabel11.Text = "Họ tên người liên hệ";
            // 
            // txtAccountingObjectAddressHD
            // 
            this.txtAccountingObjectAddressHD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressHD.AutoSize = false;
            this.txtAccountingObjectAddressHD.Location = new System.Drawing.Point(107, 44);
            this.txtAccountingObjectAddressHD.Name = "txtAccountingObjectAddressHD";
            this.txtAccountingObjectAddressHD.Size = new System.Drawing.Size(295, 22);
            this.txtAccountingObjectAddressHD.TabIndex = 3;
            this.txtAccountingObjectAddressHD.TextChanged += new System.EventHandler(this.txtAccountingObjectNameHD_TextChanged);
            // 
            // ultraLabel12
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance20;
            this.ultraLabel12.Location = new System.Drawing.Point(6, 44);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(95, 22);
            this.ultraLabel12.TabIndex = 22;
            this.ultraLabel12.Text = "Địa chỉ";
            // 
            // ultraLabel13
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance21;
            this.ultraLabel13.Location = new System.Drawing.Point(6, 19);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(95, 23);
            this.ultraLabel13.TabIndex = 0;
            this.ultraLabel13.Text = "Đối tượng";
            // 
            // palTop
            // 
            this.palTop.Controls.Add(this.ultraGroupBoxTop);
            this.palTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.palTop.Location = new System.Drawing.Point(0, 0);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(929, 82);
            this.palTop.TabIndex = 27;
            // 
            // ultraGroupBoxTop
            // 
            this.ultraGroupBoxTop.Controls.Add(this.palChung);
            this.ultraGroupBoxTop.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance24.FontData.BoldAsString = "True";
            appearance24.FontData.SizeInPoints = 13F;
            this.ultraGroupBoxTop.HeaderAppearance = appearance24;
            this.ultraGroupBoxTop.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxTop.Name = "ultraGroupBoxTop";
            this.ultraGroupBoxTop.Size = new System.Drawing.Size(929, 82);
            this.ultraGroupBoxTop.TabIndex = 43;
            this.ultraGroupBoxTop.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // palChung
            // 
            this.palChung.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.palChung.BackColor = System.Drawing.Color.Transparent;
            this.palChung.Controls.Add(this.uComboThanhToan);
            this.palChung.Controls.Add(this.lblTT);
            this.palChung.Location = new System.Drawing.Point(499, 2);
            this.palChung.Name = "palChung";
            this.palChung.Size = new System.Drawing.Size(420, 77);
            this.palChung.TabIndex = 32;
            this.palChung.TabStop = true;
            // 
            // uComboThanhToan
            // 
            this.uComboThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uComboThanhToan.AutoSize = false;
            this.uComboThanhToan.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboThanhToan.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.uComboThanhToan.Location = new System.Drawing.Point(239, 29);
            this.uComboThanhToan.Name = "uComboThanhToan";
            this.uComboThanhToan.Size = new System.Drawing.Size(177, 22);
            this.uComboThanhToan.TabIndex = 67;
            this.uComboThanhToan.TextChanged += new System.EventHandler(this.txtThanhToan_TextChanged);
            // 
            // lblTT
            // 
            appearance23.TextVAlignAsString = "Middle";
            this.lblTT.Appearance = appearance23;
            this.lblTT.Location = new System.Drawing.Point(119, 31);
            this.lblTT.Name = "lblTT";
            this.lblTT.Size = new System.Drawing.Size(132, 22);
            this.lblTT.TabIndex = 0;
            this.lblTT.Text = "Hình thức thanh toán";
            // 
            // palFill
            // 
            this.palFill.Controls.Add(this.palGrid);
            this.palFill.Controls.Add(this.ultraSplitter1);
            this.palFill.Controls.Add(this.palThongTinChung);
            this.palFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palFill.Location = new System.Drawing.Point(0, 82);
            this.palFill.Name = "palFill";
            this.palFill.Size = new System.Drawing.Size(929, 518);
            this.palFill.TabIndex = 28;
            // 
            // palGrid
            // 
            this.palGrid.Controls.Add(this.pnlUgrid);
            this.palGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palGrid.Location = new System.Drawing.Point(0, 181);
            this.palGrid.Name = "palGrid";
            this.palGrid.Size = new System.Drawing.Size(929, 337);
            this.palGrid.TabIndex = 28;
            // 
            // pnlUgrid
            // 
            // 
            // pnlUgrid.ClientArea
            // 
            this.pnlUgrid.ClientArea.Controls.Add(this.btnOriginalVoucher);
            this.pnlUgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUgrid.Location = new System.Drawing.Point(0, 0);
            this.pnlUgrid.Name = "pnlUgrid";
            this.pnlUgrid.Size = new System.Drawing.Size(929, 337);
            this.pnlUgrid.TabIndex = 11;
            // 
            // btnOriginalVoucher
            // 
            this.btnOriginalVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance25.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.btnOriginalVoucher.HotTrackAppearance = appearance25;
            this.btnOriginalVoucher.Location = new System.Drawing.Point(833, 6);
            this.btnOriginalVoucher.Name = "btnOriginalVoucher";
            this.btnOriginalVoucher.Size = new System.Drawing.Size(86, 22);
            this.btnOriginalVoucher.TabIndex = 6;
            this.btnOriginalVoucher.Text = "Tham chiếu";
            this.btnOriginalVoucher.Click += new System.EventHandler(this.btnOriginalVoucher_Click);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 171);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 171;
            this.ultraSplitter1.Size = new System.Drawing.Size(929, 10);
            this.ultraSplitter1.TabIndex = 0;
            // 
            // palThongTinChung
            // 
            this.palThongTinChung.Controls.Add(this.group);
            this.palThongTinChung.Dock = System.Windows.Forms.DockStyle.Top;
            this.palThongTinChung.Location = new System.Drawing.Point(0, 0);
            this.palThongTinChung.Name = "palThongTinChung";
            this.palThongTinChung.Size = new System.Drawing.Size(929, 171);
            this.palThongTinChung.TabIndex = 27;
            // 
            // group
            // 
            this.group.Controls.Add(this.ultraTabSharedControlsPage2);
            this.group.Controls.Add(this.ultraTabPageControl1);
            this.group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group.Location = new System.Drawing.Point(0, 0);
            this.group.Name = "group";
            this.group.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.group.Size = new System.Drawing.Size(929, 171);
            this.group.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.group.TabIndex = 89;
            ultraTab1.Key = "tabHoaDon1";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Hóa đơn";
            this.group.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1});
            this.group.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(927, 148);
            // 
            // palBottom
            // 
            this.palBottom.Controls.Add(this.ultraPanel1);
            this.palBottom.Controls.Add(this.panel2);
            this.palBottom.Controls.Add(this.panel1);
            this.palBottom.Controls.Add(this.txtTotalDiscountAmountSubOriginal);
            this.palBottom.Controls.Add(this.txtTotalDiscountAmountSub);
            this.palBottom.Controls.Add(this.ultraLabel3);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountOriginalStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalAmountOriginal);
            this.palBottom.Controls.Add(this.txtTotalPaymentAmountStand);
            this.palBottom.Controls.Add(this.txtTotalVATAmount);
            this.palBottom.Controls.Add(this.txtTotalAmount);
            this.palBottom.Controls.Add(this.lblTotalVATAmount);
            this.palBottom.Controls.Add(this.lblTotalPaymentAmount);
            this.palBottom.Controls.Add(this.lblTotalAmount);
            this.palBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.palBottom.Location = new System.Drawing.Point(0, 600);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(929, 137);
            this.palBottom.TabIndex = 2;
            this.palBottom.TabStop = true;
            this.palBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.palBottom_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtDocumentNote);
            this.panel2.Controls.Add(this.txtDocumentNo);
            this.panel2.Controls.Add(this.DocumentDate);
            this.panel2.Controls.Add(this.ultraLabel15);
            this.panel2.Controls.Add(this.ultraLabel14);
            this.panel2.Controls.Add(this.ultraLabel17);
            this.panel2.Location = new System.Drawing.Point(3, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(854, 40);
            this.panel2.TabIndex = 61;
            this.panel2.Visible = false;
            // 
            // txtDocumentNote
            // 
            this.txtDocumentNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocumentNote.AutoSize = false;
            this.txtDocumentNote.Location = new System.Drawing.Point(544, 0);
            this.txtDocumentNote.Name = "txtDocumentNote";
            this.txtDocumentNote.Size = new System.Drawing.Size(295, 22);
            this.txtDocumentNote.TabIndex = 51;
            // 
            // txtDocumentNo
            // 
            this.txtDocumentNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocumentNo.AutoSize = false;
            this.txtDocumentNo.Location = new System.Drawing.Point(177, 1);
            this.txtDocumentNo.Name = "txtDocumentNo";
            this.txtDocumentNo.Size = new System.Drawing.Size(111, 22);
            this.txtDocumentNo.TabIndex = 50;
            // 
            // DocumentDate
            // 
            this.DocumentDate.Location = new System.Drawing.Point(347, 2);
            this.DocumentDate.Name = "DocumentDate";
            this.DocumentDate.Size = new System.Drawing.Size(111, 21);
            this.DocumentDate.TabIndex = 49;
            // 
            // ultraLabel15
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance26;
            this.ultraLabel15.Location = new System.Drawing.Point(482, 5);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(56, 19);
            this.ultraLabel15.TabIndex = 47;
            this.ultraLabel15.Text = "Ghi chú";
            // 
            // ultraLabel14
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance27;
            this.ultraLabel14.Location = new System.Drawing.Point(304, 4);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(37, 19);
            this.ultraLabel14.TabIndex = 45;
            this.ultraLabel14.Text = "Ngày";
            // 
            // ultraLabel17
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance28;
            this.ultraLabel17.Location = new System.Drawing.Point(3, 3);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(172, 19);
            this.ultraLabel17.TabIndex = 26;
            this.ultraLabel17.Text = "Số văn bản";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtOldInvSeries);
            this.panel1.Controls.Add(this.ultraLabel4);
            this.panel1.Controls.Add(this.txtOldInvTemplate);
            this.panel1.Controls.Add(this.ultraLabel2);
            this.panel1.Controls.Add(this.txtOldInvDate);
            this.panel1.Controls.Add(this.ultraLabel1);
            this.panel1.Controls.Add(this.txtOldInvNo);
            this.panel1.Controls.Add(this.lblTttt);
            this.panel1.Location = new System.Drawing.Point(3, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(854, 34);
            this.panel1.TabIndex = 0;
            // 
            // txtOldInvSeries
            // 
            this.txtOldInvSeries.AutoSize = false;
            this.txtOldInvSeries.Location = new System.Drawing.Point(728, 1);
            this.txtOldInvSeries.Name = "txtOldInvSeries";
            this.txtOldInvSeries.ReadOnly = true;
            this.txtOldInvSeries.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvSeries.TabIndex = 49;
            // 
            // ultraLabel4
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance29;
            this.ultraLabel4.Location = new System.Drawing.Point(672, 3);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(43, 19);
            this.ultraLabel4.TabIndex = 48;
            this.ultraLabel4.Text = "Ký hiệu";
            // 
            // txtOldInvTemplate
            // 
            this.txtOldInvTemplate.AutoSize = false;
            this.txtOldInvTemplate.Location = new System.Drawing.Point(546, 0);
            this.txtOldInvTemplate.Name = "txtOldInvTemplate";
            this.txtOldInvTemplate.ReadOnly = true;
            this.txtOldInvTemplate.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvTemplate.TabIndex = 47;
            // 
            // ultraLabel2
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance30;
            this.ultraLabel2.Location = new System.Drawing.Point(482, 3);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(44, 19);
            this.ultraLabel2.TabIndex = 46;
            this.ultraLabel2.Text = "Mẫu số";
            // 
            // txtOldInvDate
            // 
            this.txtOldInvDate.AutoSize = false;
            this.txtOldInvDate.Location = new System.Drawing.Point(347, 1);
            this.txtOldInvDate.Name = "txtOldInvDate";
            this.txtOldInvDate.ReadOnly = true;
            this.txtOldInvDate.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvDate.TabIndex = 45;
            // 
            // ultraLabel1
            // 
            appearance31.BackColor = System.Drawing.Color.Transparent;
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance31;
            this.ultraLabel1.Location = new System.Drawing.Point(304, 4);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(37, 19);
            this.ultraLabel1.TabIndex = 44;
            this.ultraLabel1.Text = "Ngày";
            // 
            // txtOldInvNo
            // 
            this.txtOldInvNo.AutoSize = false;
            this.txtOldInvNo.Location = new System.Drawing.Point(177, 1);
            this.txtOldInvNo.Name = "txtOldInvNo";
            this.txtOldInvNo.ReadOnly = true;
            this.txtOldInvNo.Size = new System.Drawing.Size(111, 22);
            this.txtOldInvNo.TabIndex = 43;
            // 
            // lblTttt
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextVAlignAsString = "Middle";
            this.lblTttt.Appearance = appearance32;
            this.lblTttt.Location = new System.Drawing.Point(3, 3);
            this.lblTttt.Name = "lblTttt";
            this.lblTttt.Size = new System.Drawing.Size(172, 19);
            this.lblTttt.TabIndex = 26;
            this.lblTttt.Text = "Thay thế cho HĐ số";
            // 
            // txtTotalDiscountAmountSubOriginal
            // 
            this.txtTotalDiscountAmountSubOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance33.BackColor = System.Drawing.Color.Transparent;
            appearance33.TextHAlignAsString = "Right";
            appearance33.TextVAlignAsString = "Middle";
            this.txtTotalDiscountAmountSubOriginal.Appearance = appearance33;
            this.txtTotalDiscountAmountSubOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiscountAmountSubOriginal.Location = new System.Drawing.Point(584, 27);
            this.txtTotalDiscountAmountSubOriginal.Name = "txtTotalDiscountAmountSubOriginal";
            this.txtTotalDiscountAmountSubOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalDiscountAmountSubOriginal.TabIndex = 56;
            // 
            // txtTotalDiscountAmountSub
            // 
            this.txtTotalDiscountAmountSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance34.BackColor = System.Drawing.Color.Transparent;
            appearance34.TextHAlignAsString = "Right";
            appearance34.TextVAlignAsString = "Middle";
            this.txtTotalDiscountAmountSub.Appearance = appearance34;
            this.txtTotalDiscountAmountSub.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiscountAmountSub.Location = new System.Drawing.Point(759, 27);
            this.txtTotalDiscountAmountSub.Name = "txtTotalDiscountAmountSub";
            this.txtTotalDiscountAmountSub.Size = new System.Drawing.Size(169, 19);
            this.txtTotalDiscountAmountSub.TabIndex = 55;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance35;
            this.ultraLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraLabel3.Location = new System.Drawing.Point(440, 28);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(115, 19);
            this.ultraLabel3.TabIndex = 54;
            this.ultraLabel3.Text = "Tiền chiết khấu";
            // 
            // txtTotalPaymentAmountOriginalStand
            // 
            this.txtTotalPaymentAmountOriginalStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.Color.Transparent;
            appearance36.TextHAlignAsString = "Right";
            appearance36.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountOriginalStand.Appearance = appearance36;
            this.txtTotalPaymentAmountOriginalStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountOriginalStand.Location = new System.Drawing.Point(584, 69);
            this.txtTotalPaymentAmountOriginalStand.Name = "txtTotalPaymentAmountOriginalStand";
            this.txtTotalPaymentAmountOriginalStand.Size = new System.Drawing.Size(169, 19);
            this.txtTotalPaymentAmountOriginalStand.TabIndex = 53;
            // 
            // txtTotalVATAmountOriginal
            // 
            this.txtTotalVATAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.Color.Transparent;
            appearance37.TextHAlignAsString = "Right";
            appearance37.TextVAlignAsString = "Middle";
            this.txtTotalVATAmountOriginal.Appearance = appearance37;
            this.txtTotalVATAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmountOriginal.Location = new System.Drawing.Point(584, 48);
            this.txtTotalVATAmountOriginal.Name = "txtTotalVATAmountOriginal";
            this.txtTotalVATAmountOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalVATAmountOriginal.TabIndex = 52;
            // 
            // txtTotalAmountOriginal
            // 
            this.txtTotalAmountOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Right";
            appearance38.TextVAlignAsString = "Middle";
            this.txtTotalAmountOriginal.Appearance = appearance38;
            this.txtTotalAmountOriginal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmountOriginal.Location = new System.Drawing.Point(584, 6);
            this.txtTotalAmountOriginal.Name = "txtTotalAmountOriginal";
            this.txtTotalAmountOriginal.Size = new System.Drawing.Size(169, 19);
            this.txtTotalAmountOriginal.TabIndex = 51;
            // 
            // txtTotalPaymentAmountStand
            // 
            this.txtTotalPaymentAmountStand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Right";
            appearance39.TextVAlignAsString = "Middle";
            this.txtTotalPaymentAmountStand.Appearance = appearance39;
            this.txtTotalPaymentAmountStand.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaymentAmountStand.Location = new System.Drawing.Point(759, 69);
            this.txtTotalPaymentAmountStand.Name = "txtTotalPaymentAmountStand";
            this.txtTotalPaymentAmountStand.Size = new System.Drawing.Size(169, 19);
            this.txtTotalPaymentAmountStand.TabIndex = 50;
            // 
            // txtTotalVATAmount
            // 
            this.txtTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.TextHAlignAsString = "Right";
            appearance40.TextVAlignAsString = "Middle";
            this.txtTotalVATAmount.Appearance = appearance40;
            this.txtTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalVATAmount.Location = new System.Drawing.Point(759, 48);
            this.txtTotalVATAmount.Name = "txtTotalVATAmount";
            this.txtTotalVATAmount.Size = new System.Drawing.Size(169, 19);
            this.txtTotalVATAmount.TabIndex = 49;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Right";
            appearance41.TextVAlignAsString = "Middle";
            this.txtTotalAmount.Appearance = appearance41;
            this.txtTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(759, 6);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(169, 19);
            this.txtTotalAmount.TabIndex = 48;
            // 
            // lblTotalVATAmount
            // 
            this.lblTotalVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextVAlignAsString = "Middle";
            this.lblTotalVATAmount.Appearance = appearance42;
            this.lblTotalVATAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVATAmount.Location = new System.Drawing.Point(440, 49);
            this.lblTotalVATAmount.Name = "lblTotalVATAmount";
            this.lblTotalVATAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalVATAmount.TabIndex = 47;
            this.lblTotalVATAmount.Text = "Tổng tiền thuế GTGT";
            // 
            // lblTotalPaymentAmount
            // 
            this.lblTotalPaymentAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextVAlignAsString = "Middle";
            this.lblTotalPaymentAmount.Appearance = appearance43;
            this.lblTotalPaymentAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaymentAmount.Location = new System.Drawing.Point(440, 70);
            this.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount";
            this.lblTotalPaymentAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalPaymentAmount.TabIndex = 46;
            this.lblTotalPaymentAmount.Text = "Tổng tiền thanh toán";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.lblTotalAmount.Appearance = appearance44;
            this.lblTotalAmount.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(440, 6);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(115, 19);
            this.lblTotalAmount.TabIndex = 45;
            this.lblTotalAmount.Text = "Tổng tiền hàng";
            // 
            // ultraGroupBoxGiayBaoCo
            // 
            this.ultraGroupBoxGiayBaoCo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBoxGiayBaoCo.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxGiayBaoCo.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxGiayBaoCo.Name = "ultraGroupBoxGiayBaoCo";
            this.ultraGroupBoxGiayBaoCo.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxGiayBaoCo.TabIndex = 29;
            this.ultraGroupBoxGiayBaoCo.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblKhachHangGBC
            // 
            this.lblKhachHangGBC.Location = new System.Drawing.Point(6, 20);
            this.lblKhachHangGBC.Name = "lblKhachHangGBC";
            this.lblKhachHangGBC.Size = new System.Drawing.Size(115, 22);
            this.lblKhachHangGBC.TabIndex = 0;
            // 
            // lblDiaChiGBC
            // 
            this.lblDiaChiGBC.Location = new System.Drawing.Point(6, 44);
            this.lblDiaChiGBC.Name = "lblDiaChiGBC";
            this.lblDiaChiGBC.Size = new System.Drawing.Size(115, 22);
            this.lblDiaChiGBC.TabIndex = 22;
            // 
            // txtAccountingObjectAddressGBC
            // 
            this.txtAccountingObjectAddressGBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressGBC.AutoSize = false;
            this.txtAccountingObjectAddressGBC.Location = new System.Drawing.Point(128, 44);
            this.txtAccountingObjectAddressGBC.Name = "txtAccountingObjectAddressGBC";
            this.txtAccountingObjectAddressGBC.Size = new System.Drawing.Size(790, 22);
            this.txtAccountingObjectAddressGBC.TabIndex = 23;
            // 
            // lblMReasonPayGBC
            // 
            this.lblMReasonPayGBC.Location = new System.Drawing.Point(6, 92);
            this.lblMReasonPayGBC.Name = "lblMReasonPayGBC";
            this.lblMReasonPayGBC.Size = new System.Drawing.Size(115, 21);
            this.lblMReasonPayGBC.TabIndex = 26;
            // 
            // txtMReasonPayGBC
            // 
            this.txtMReasonPayGBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMReasonPayGBC.AutoSize = false;
            this.txtMReasonPayGBC.Location = new System.Drawing.Point(128, 92);
            this.txtMReasonPayGBC.Multiline = true;
            this.txtMReasonPayGBC.Name = "txtMReasonPayGBC";
            this.txtMReasonPayGBC.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMReasonPayGBC.Size = new System.Drawing.Size(790, 46);
            this.txtMReasonPayGBC.TabIndex = 27;
            // 
            // txtAccountingObjectNameGBC
            // 
            this.txtAccountingObjectNameGBC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameGBC.AutoSize = false;
            this.txtAccountingObjectNameGBC.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNameGBC.Name = "txtAccountingObjectNameGBC";
            this.txtAccountingObjectNameGBC.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectNameGBC.TabIndex = 32;
            // 
            // lblNopTienTaiKhoanGBC
            // 
            this.lblNopTienTaiKhoanGBC.Location = new System.Drawing.Point(6, 68);
            this.lblNopTienTaiKhoanGBC.Name = "lblNopTienTaiKhoanGBC";
            this.lblNopTienTaiKhoanGBC.Size = new System.Drawing.Size(115, 22);
            this.lblNopTienTaiKhoanGBC.TabIndex = 38;
            // 
            // txtBankName
            // 
            this.txtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(290, 68);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(628, 22);
            this.txtBankName.TabIndex = 40;
            // 
            // ultraButton6
            // 
            this.ultraButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton6.Location = new System.Drawing.Point(719, 20);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(108, 23);
            this.ultraButton6.TabIndex = 42;
            // 
            // ultraButton5
            // 
            this.ultraButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton5.Location = new System.Drawing.Point(833, 20);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(85, 23);
            this.ultraButton5.TabIndex = 43;
            // 
            // ultraGroupBoxPhieuThu
            // 
            this.ultraGroupBoxPhieuThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBoxPhieuThu.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuThu.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuThu.Name = "ultraGroupBoxPhieuThu";
            this.ultraGroupBoxPhieuThu.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxPhieuThu.TabIndex = 28;
            this.ultraGroupBoxPhieuThu.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblKhachHangPT
            // 
            this.lblKhachHangPT.Location = new System.Drawing.Point(6, 20);
            this.lblKhachHangPT.Name = "lblKhachHangPT";
            this.lblKhachHangPT.Size = new System.Drawing.Size(115, 22);
            this.lblKhachHangPT.TabIndex = 0;
            // 
            // lblDiaChiPT
            // 
            this.lblDiaChiPT.Location = new System.Drawing.Point(6, 44);
            this.lblDiaChiPT.Name = "lblDiaChiPT";
            this.lblDiaChiPT.Size = new System.Drawing.Size(115, 22);
            this.lblDiaChiPT.TabIndex = 22;
            // 
            // txtAccountingObjectAddressPT
            // 
            this.txtAccountingObjectAddressPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPT.AutoSize = false;
            this.txtAccountingObjectAddressPT.Location = new System.Drawing.Point(128, 44);
            this.txtAccountingObjectAddressPT.Name = "txtAccountingObjectAddressPT";
            this.txtAccountingObjectAddressPT.Size = new System.Drawing.Size(790, 22);
            this.txtAccountingObjectAddressPT.TabIndex = 23;
            // 
            // lblMContactName
            // 
            this.lblMContactName.Location = new System.Drawing.Point(6, 68);
            this.lblMContactName.Name = "lblMContactName";
            this.lblMContactName.Size = new System.Drawing.Size(115, 22);
            this.lblMContactName.TabIndex = 24;
            // 
            // txtMContactName
            // 
            this.txtMContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMContactName.AutoSize = false;
            this.txtMContactName.Location = new System.Drawing.Point(128, 68);
            this.txtMContactName.Name = "txtMContactName";
            this.txtMContactName.Size = new System.Drawing.Size(790, 22);
            this.txtMContactName.TabIndex = 25;
            // 
            // lblMReasonPayPT
            // 
            this.lblMReasonPayPT.Location = new System.Drawing.Point(6, 92);
            this.lblMReasonPayPT.Name = "lblMReasonPayPT";
            this.lblMReasonPayPT.Size = new System.Drawing.Size(115, 22);
            this.lblMReasonPayPT.TabIndex = 26;
            // 
            // txtMReasonPayPT
            // 
            this.txtMReasonPayPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMReasonPayPT.AutoSize = false;
            this.txtMReasonPayPT.Location = new System.Drawing.Point(128, 92);
            this.txtMReasonPayPT.Name = "txtMReasonPayPT";
            this.txtMReasonPayPT.Size = new System.Drawing.Size(790, 22);
            this.txtMReasonPayPT.TabIndex = 27;
            // 
            // txtAccountingObjectNamePT
            // 
            this.txtAccountingObjectNamePT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePT.AutoSize = false;
            this.txtAccountingObjectNamePT.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNamePT.Name = "txtAccountingObjectNamePT";
            this.txtAccountingObjectNamePT.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectNamePT.TabIndex = 32;
            // 
            // lblNumberAttachPT
            // 
            this.lblNumberAttachPT.Location = new System.Drawing.Point(6, 116);
            this.lblNumberAttachPT.Name = "lblNumberAttachPT";
            this.lblNumberAttachPT.Size = new System.Drawing.Size(115, 22);
            this.lblNumberAttachPT.TabIndex = 35;
            // 
            // txtNumberAttachPT
            // 
            this.txtNumberAttachPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumberAttachPT.AutoSize = false;
            this.txtNumberAttachPT.Location = new System.Drawing.Point(128, 116);
            this.txtNumberAttachPT.Name = "txtNumberAttachPT";
            this.txtNumberAttachPT.Size = new System.Drawing.Size(706, 22);
            this.txtNumberAttachPT.TabIndex = 36;
            // 
            // lblChungTuGocPT
            // 
            this.lblChungTuGocPT.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblChungTuGocPT.Location = new System.Drawing.Point(839, 116);
            this.lblChungTuGocPT.Name = "lblChungTuGocPT";
            this.lblChungTuGocPT.Size = new System.Drawing.Size(79, 22);
            this.lblChungTuGocPT.TabIndex = 37;
            // 
            // ultraButton4
            // 
            this.ultraButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton4.Location = new System.Drawing.Point(719, 20);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(108, 23);
            this.ultraButton4.TabIndex = 42;
            // 
            // ultraButton3
            // 
            this.ultraButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton3.Location = new System.Drawing.Point(833, 20);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(85, 23);
            this.ultraButton3.TabIndex = 43;
            // 
            // ultraGroupBoxPhieuXuatKho
            // 
            this.ultraGroupBoxPhieuXuatKho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBoxPhieuXuatKho.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxPhieuXuatKho.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxPhieuXuatKho.Name = "ultraGroupBoxPhieuXuatKho";
            this.ultraGroupBoxPhieuXuatKho.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxPhieuXuatKho.TabIndex = 27;
            this.ultraGroupBoxPhieuXuatKho.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblKhachHangPXK
            // 
            this.lblKhachHangPXK.Location = new System.Drawing.Point(6, 20);
            this.lblKhachHangPXK.Name = "lblKhachHangPXK";
            this.lblKhachHangPXK.Size = new System.Drawing.Size(115, 22);
            this.lblKhachHangPXK.TabIndex = 0;
            // 
            // lblDiaChiPXK
            // 
            this.lblDiaChiPXK.Location = new System.Drawing.Point(6, 44);
            this.lblDiaChiPXK.Name = "lblDiaChiPXK";
            this.lblDiaChiPXK.Size = new System.Drawing.Size(115, 22);
            this.lblDiaChiPXK.TabIndex = 22;
            // 
            // txtAccountingObjectAddressPXK
            // 
            this.txtAccountingObjectAddressPXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddressPXK.AutoSize = false;
            this.txtAccountingObjectAddressPXK.Location = new System.Drawing.Point(128, 44);
            this.txtAccountingObjectAddressPXK.Name = "txtAccountingObjectAddressPXK";
            this.txtAccountingObjectAddressPXK.Size = new System.Drawing.Size(790, 22);
            this.txtAccountingObjectAddressPXK.TabIndex = 23;
            // 
            // lblSContactName
            // 
            this.lblSContactName.Location = new System.Drawing.Point(293, 68);
            this.lblSContactName.Name = "lblSContactName";
            this.lblSContactName.Size = new System.Drawing.Size(102, 22);
            this.lblSContactName.TabIndex = 24;
            // 
            // txtSContactName
            // 
            this.txtSContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSContactName.AutoSize = false;
            this.txtSContactName.Location = new System.Drawing.Point(401, 68);
            this.txtSContactName.Name = "txtSContactName";
            this.txtSContactName.Size = new System.Drawing.Size(517, 22);
            this.txtSContactName.TabIndex = 25;
            // 
            // lblSReason
            // 
            this.lblSReason.Location = new System.Drawing.Point(6, 92);
            this.lblSReason.Name = "lblSReason";
            this.lblSReason.Size = new System.Drawing.Size(115, 22);
            this.lblSReason.TabIndex = 26;
            // 
            // txtSReason
            // 
            this.txtSReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSReason.AutoSize = false;
            this.txtSReason.Location = new System.Drawing.Point(128, 92);
            this.txtSReason.Name = "txtSReason";
            this.txtSReason.Size = new System.Drawing.Size(790, 22);
            this.txtSReason.TabIndex = 27;
            // 
            // txtAccountingObjectNamePXK
            // 
            this.txtAccountingObjectNamePXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNamePXK.AutoSize = false;
            this.txtAccountingObjectNamePXK.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectNamePXK.Name = "txtAccountingObjectNamePXK";
            this.txtAccountingObjectNamePXK.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectNamePXK.TabIndex = 32;
            // 
            // lblNumberAttachPXK
            // 
            this.lblNumberAttachPXK.Location = new System.Drawing.Point(6, 115);
            this.lblNumberAttachPXK.Name = "lblNumberAttachPXK";
            this.lblNumberAttachPXK.Size = new System.Drawing.Size(115, 23);
            this.lblNumberAttachPXK.TabIndex = 35;
            // 
            // txtOriginalNoPXK
            // 
            this.txtOriginalNoPXK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginalNoPXK.AutoSize = false;
            this.txtOriginalNoPXK.Location = new System.Drawing.Point(128, 116);
            this.txtOriginalNoPXK.Name = "txtOriginalNoPXK";
            this.txtOriginalNoPXK.Size = new System.Drawing.Size(706, 22);
            this.txtOriginalNoPXK.TabIndex = 36;
            // 
            // lblChungTuGoc
            // 
            this.lblChungTuGoc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblChungTuGoc.Location = new System.Drawing.Point(840, 116);
            this.lblChungTuGoc.Name = "lblChungTuGoc";
            this.lblChungTuGoc.Size = new System.Drawing.Size(78, 22);
            this.lblChungTuGoc.TabIndex = 37;
            // 
            // lblCompanyTaxCodePXK
            // 
            this.lblCompanyTaxCodePXK.Location = new System.Drawing.Point(6, 67);
            this.lblCompanyTaxCodePXK.Name = "lblCompanyTaxCodePXK";
            this.lblCompanyTaxCodePXK.Size = new System.Drawing.Size(115, 23);
            this.lblCompanyTaxCodePXK.TabIndex = 38;
            // 
            // txtCompanyTaxCodePXK
            // 
            this.txtCompanyTaxCodePXK.AutoSize = false;
            this.txtCompanyTaxCodePXK.Location = new System.Drawing.Point(128, 68);
            this.txtCompanyTaxCodePXK.Name = "txtCompanyTaxCodePXK";
            this.txtCompanyTaxCodePXK.Size = new System.Drawing.Size(156, 22);
            this.txtCompanyTaxCodePXK.TabIndex = 39;
            // 
            // ultraButton2
            // 
            this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton2.Location = new System.Drawing.Point(719, 20);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(108, 23);
            this.ultraButton2.TabIndex = 40;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraButton1.Location = new System.Drawing.Point(833, 20);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(85, 23);
            this.ultraButton1.TabIndex = 41;
            // 
            // ultraGroupBoxHoaDon
            // 
            this.ultraGroupBoxHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBoxHoaDon.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBoxHoaDon.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBoxHoaDon.Name = "ultraGroupBoxHoaDon";
            this.ultraGroupBoxHoaDon.Size = new System.Drawing.Size(927, 148);
            this.ultraGroupBoxHoaDon.TabIndex = 26;
            this.ultraGroupBoxHoaDon.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // lblAccountingObjectID
            // 
            this.lblAccountingObjectID.Location = new System.Drawing.Point(6, 19);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(95, 23);
            this.lblAccountingObjectID.TabIndex = 0;
            // 
            // lblAccountingObjectAddress
            // 
            this.lblAccountingObjectAddress.Location = new System.Drawing.Point(6, 44);
            this.lblAccountingObjectAddress.Name = "lblAccountingObjectAddress";
            this.lblAccountingObjectAddress.Size = new System.Drawing.Size(95, 22);
            this.lblAccountingObjectAddress.TabIndex = 22;
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(107, 44);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(811, 22);
            this.txtAccountingObjectAddress.TabIndex = 23;
            // 
            // lblContactName
            // 
            this.lblContactName.Location = new System.Drawing.Point(292, 68);
            this.lblContactName.Name = "lblContactName";
            this.lblContactName.Size = new System.Drawing.Size(107, 22);
            this.lblContactName.TabIndex = 24;
            // 
            // txtContactName
            // 
            this.txtContactName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContactName.AutoSize = false;
            this.txtContactName.Location = new System.Drawing.Point(403, 68);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(515, 22);
            this.txtContactName.TabIndex = 25;
            // 
            // lblReason
            // 
            this.lblReason.Location = new System.Drawing.Point(6, 92);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(95, 22);
            this.lblReason.TabIndex = 26;
            // 
            // txtReason
            // 
            this.txtReason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReason.AutoSize = false;
            this.txtReason.Location = new System.Drawing.Point(107, 92);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(811, 22);
            this.txtReason.TabIndex = 27;
            // 
            // lblCompanyTaxCode
            // 
            this.lblCompanyTaxCode.Location = new System.Drawing.Point(6, 68);
            this.lblCompanyTaxCode.Name = "lblCompanyTaxCode";
            this.lblCompanyTaxCode.Size = new System.Drawing.Size(95, 22);
            this.lblCompanyTaxCode.TabIndex = 28;
            // 
            // txtCompanyTaxCode
            // 
            this.txtCompanyTaxCode.AutoSize = false;
            this.txtCompanyTaxCode.Location = new System.Drawing.Point(107, 68);
            this.txtCompanyTaxCode.Name = "txtCompanyTaxCode";
            this.txtCompanyTaxCode.Size = new System.Drawing.Size(177, 22);
            this.txtCompanyTaxCode.TabIndex = 29;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(290, 20);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(423, 22);
            this.txtAccountingObjectName.TabIndex = 32;
            // 
            // btnSAOrder
            // 
            this.btnSAOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSAOrder.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnSAOrder.Location = new System.Drawing.Point(719, 20);
            this.btnSAOrder.Name = "btnSAOrder";
            this.btnSAOrder.Size = new System.Drawing.Size(108, 23);
            this.btnSAOrder.TabIndex = 33;
            // 
            // btnCongNo
            // 
            this.btnCongNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCongNo.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.btnCongNo.Location = new System.Drawing.Point(833, 20);
            this.btnCongNo.Name = "btnCongNo";
            this.btnCongNo.Size = new System.Drawing.Size(85, 23);
            this.btnCongNo.TabIndex = 34;
            // 
            // lblisAttachList
            // 
            this.lblisAttachList.Location = new System.Drawing.Point(6, 116);
            this.lblisAttachList.Name = "lblisAttachList";
            this.lblisAttachList.Size = new System.Drawing.Size(95, 22);
            this.lblisAttachList.TabIndex = 35;
            // 
            // chkisAttachList
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.ImageVAlign = Infragistics.Win.VAlign.Middle;
            appearance45.TextHAlignAsString = "Center";
            appearance45.TextVAlignAsString = "Middle";
            this.chkisAttachList.Appearance = appearance45;
            this.chkisAttachList.BackColor = System.Drawing.Color.Transparent;
            this.chkisAttachList.BackColorInternal = System.Drawing.Color.Transparent;
            this.chkisAttachList.Location = new System.Drawing.Point(107, 116);
            this.chkisAttachList.Name = "chkisAttachList";
            this.chkisAttachList.Size = new System.Drawing.Size(14, 22);
            this.chkisAttachList.TabIndex = 36;
            // 
            // lblListNo
            // 
            this.lblListNo.Location = new System.Drawing.Point(127, 116);
            this.lblListNo.Name = "lblListNo";
            this.lblListNo.Size = new System.Drawing.Size(23, 22);
            this.lblListNo.TabIndex = 37;
            // 
            // txtListNo
            // 
            this.txtListNo.AutoSize = false;
            this.txtListNo.Location = new System.Drawing.Point(156, 116);
            this.txtListNo.Name = "txtListNo";
            this.txtListNo.Size = new System.Drawing.Size(128, 22);
            this.txtListNo.TabIndex = 38;
            // 
            // lblListDate
            // 
            this.lblListDate.Location = new System.Drawing.Point(292, 116);
            this.lblListDate.Name = "lblListDate";
            this.lblListDate.Size = new System.Drawing.Size(47, 22);
            this.lblListDate.TabIndex = 39;
            // 
            // lblListCommonNameInventory
            // 
            this.lblListCommonNameInventory.Location = new System.Drawing.Point(450, 116);
            this.lblListCommonNameInventory.Name = "lblListCommonNameInventory";
            this.lblListCommonNameInventory.Size = new System.Drawing.Size(107, 22);
            this.lblListCommonNameInventory.TabIndex = 41;
            // 
            // txtListCommonNameInventory
            // 
            this.txtListCommonNameInventory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtListCommonNameInventory.AutoSize = false;
            this.txtListCommonNameInventory.Location = new System.Drawing.Point(545, 116);
            this.txtListCommonNameInventory.Name = "txtListCommonNameInventory";
            this.txtListCommonNameInventory.Size = new System.Drawing.Size(373, 22);
            this.txtListCommonNameInventory.TabIndex = 42;
            // 
            // dteListDate
            // 
            appearance46.TextHAlignAsString = "Center";
            appearance46.TextVAlignAsString = "Middle";
            this.dteListDate.Appearance = appearance46;
            this.dteListDate.AutoSize = false;
            this.dteListDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteListDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteListDate.Location = new System.Drawing.Point(331, 116);
            this.dteListDate.MaskInput = "";
            this.dteListDate.Name = "dteListDate";
            this.dteListDate.Size = new System.Drawing.Size(109, 22);
            this.dteListDate.TabIndex = 30;
            this.dteListDate.Value = null;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridControl);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 94);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(929, 43);
            this.ultraPanel1.TabIndex = 62;
            // 
            // uGridControl
            // 
            this.uGridControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.uGridControl.Location = new System.Drawing.Point(496, 0);
            this.uGridControl.Name = "uGridControl";
            this.uGridControl.Size = new System.Drawing.Size(433, 43);
            this.uGridControl.TabIndex = 13;
            this.uGridControl.Text = "ultraGrid1";
            // 
            // FSABillDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 737);
            this.Controls.Add(this.palFill);
            this.Controls.Add(this.palTop);
            this.Controls.Add(this.palBottom);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MinimumSize = new System.Drawing.Size(945, 726);
            this.Name = "FSABillDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSABillDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSABillDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHinhThucHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKyHieuHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMauHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteNgayHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDateHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventoryHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNoHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachListHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodeHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactNameHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressHD)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxTop)).EndInit();
            this.ultraGroupBoxTop.ResumeLayout(false);
            this.palChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uComboThanhToan)).EndInit();
            this.palFill.ResumeLayout(false);
            this.palGrid.ResumeLayout(false);
            this.pnlUgrid.ClientArea.ResumeLayout(false);
            this.pnlUgrid.ResumeLayout(false);
            this.palThongTinChung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.group)).EndInit();
            this.group.ResumeLayout(false);
            this.palBottom.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentDate)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldInvNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxGiayBaoCo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressGBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayGBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameGBC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuThu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMReasonPayPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttachPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxPhieuXuatKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddressPXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNamePXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalNoPXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCodePXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyTaxCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkisAttachList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtListCommonNameInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteListDate)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel palTop;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxTop;
        private System.Windows.Forms.Panel palFill;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Panel palGrid;
        private System.Windows.Forms.Panel palThongTinChung;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl group;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private System.Windows.Forms.Panel palChung;
        private Infragistics.Win.Misc.UltraPanel pnlUgrid;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountOriginalStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalPaymentAmountStand;
        private Infragistics.Win.Misc.UltraLabel txtTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel txtTotalAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalVATAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalPaymentAmount;
        private Infragistics.Win.Misc.UltraLabel lblTotalAmount;
        private Infragistics.Win.Misc.UltraLabel txtTotalDiscountAmountSubOriginal;
        private Infragistics.Win.Misc.UltraLabel txtTotalDiscountAmountSub;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraLabel lblTT;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvSeries;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvTemplate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOldInvNo;
        private Infragistics.Win.Misc.UltraLabel lblTttt;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteListDateHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListCommonNameInventoryHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListNoHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkisAttachListHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameHD;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodeHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactNameHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxGiayBaoCo;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangGBC;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressGBC;
        private Infragistics.Win.Misc.UltraLabel lblMReasonPayGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameGBC;
        private Infragistics.Win.Misc.UltraLabel lblNopTienTaiKhoanGBC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuThu;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangPT;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPT;
        private Infragistics.Win.Misc.UltraLabel lblMContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMContactName;
        private Infragistics.Win.Misc.UltraLabel lblMReasonPayPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMReasonPayPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePT;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttachPT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttachPT;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGocPT;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxPhieuXuatKho;
        private Infragistics.Win.Misc.UltraLabel lblKhachHangPXK;
        private Infragistics.Win.Misc.UltraLabel lblDiaChiPXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddressPXK;
        private Infragistics.Win.Misc.UltraLabel lblSContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSContactName;
        private Infragistics.Win.Misc.UltraLabel lblSReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNamePXK;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttachPXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtOriginalNoPXK;
        private Infragistics.Win.Misc.UltraLabel lblChungTuGoc;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCodePXK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCodePXK;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxHoaDon;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.Misc.UltraLabel lblContactName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtContactName;
        private Infragistics.Win.Misc.UltraLabel lblReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReason;
        private Infragistics.Win.Misc.UltraLabel lblCompanyTaxCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCompanyTaxCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.Misc.UltraButton btnSAOrder;
        private Infragistics.Win.Misc.UltraButton btnCongNo;
        private Infragistics.Win.Misc.UltraLabel lblisAttachList;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkisAttachList;
        private Infragistics.Win.Misc.UltraLabel lblListNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListNo;
        private Infragistics.Win.Misc.UltraLabel lblListDate;
        private Infragistics.Win.Misc.UltraLabel lblListCommonNameInventory;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtListCommonNameInventory;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteListDate;
        private Infragistics.Win.Misc.UltraButton btnSaInvoice;
        private System.Windows.Forms.Panel panel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DocumentDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDocumentNote;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDocumentNo;
        private Infragistics.Win.Misc.UltraButton btnOriginalVoucher;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel33;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingBankName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboThanhToan;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel40;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbLoaiHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel41;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbHinhThucHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSoHD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKyHieuHD;
        private Infragistics.Win.Misc.UltraLabel ultraLabel42;
        private Infragistics.Win.Misc.UltraLabel ultraLabel43;
        private Infragistics.Win.Misc.UltraLabel ultraLabel44;
        private Infragistics.Win.Misc.UltraLabel ultraLabel45;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbMauHD;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteNgayHD;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridControl;
    }
}
