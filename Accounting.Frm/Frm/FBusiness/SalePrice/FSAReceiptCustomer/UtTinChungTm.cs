﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class UtTinChungTm : UserControl
    {
        List<AccountingObject> lstAccountingObjects = new List<AccountingObject>();

       
        public List<AccountingObject> DsAccountingObjects
        {
            get { return lstAccountingObjects; }
            set
            {
                List<AccountingObject> accountingObjects = value;
                if (accountingObjects == null || accountingObjects.Count <= 0) return;
                cbbDT1.DataSource = accountingObjects;
                lstAccountingObjects = accountingObjects;
                //thiết lập
                cbbDT1.DisplayMember = "AccountingObjectCode";
                Utils.ConfigGrid(cbbDT1, ConstDatabase.AccountingObject_TableName);
            }
        }

        bool isRun = false;
        public UtTinChungTm()
        {
            InitializeComponent();
        }
        private void cbbDT1_TextChanged(object sender, EventArgs e)
        {
            if (isRun)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbDT1, DsAccountingObjects.Where(x => x.AccountingObjectCode.Contains(cbbTemp.Text) || x.AccountingObjectName.Contains(cbbTemp.Text)).ToList());
            }
            else
                cbbDT1.DataSource = DsAccountingObjects;
        }

        private void cbbDT1_KeyPress(object sender, KeyPressEventArgs e)
        {
            isRun = true;
        }

        private void cbbDT1_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            AccountingObject accountingObject = e.Row.ListObject as AccountingObject;
            txtDT.Text = accountingObject.AccountingObjectName;
            txtDC.Text = accountingObject.Address;
            txtNN.Text = accountingObject.ContactName;


        }



    }
}
