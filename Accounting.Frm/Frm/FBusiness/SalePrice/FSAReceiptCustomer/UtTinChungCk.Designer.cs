﻿namespace Accounting
{
    partial class UtTinChungCk
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            this.gbCK = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtBanks = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbBanks = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDienGiai = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNN1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDC1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDT1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbDT2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gbCK)).BeginInit();
            this.gbCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBanks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT2)).BeginInit();
            this.SuspendLayout();
            // 
            // gbCK
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.gbCK.Appearance = appearance1;
            this.gbCK.Controls.Add(this.txtBanks);
            this.gbCK.Controls.Add(this.cbbBanks);
            this.gbCK.Controls.Add(this.txtDienGiai);
            this.gbCK.Controls.Add(this.txtNN1);
            this.gbCK.Controls.Add(this.txtDC1);
            this.gbCK.Controls.Add(this.txtDT1);
            this.gbCK.Controls.Add(this.cbbDT2);
            this.gbCK.Controls.Add(this.ultraLabel3);
            this.gbCK.Controls.Add(this.ultraLabel4);
            this.gbCK.Controls.Add(this.ultraLabel5);
            this.gbCK.Controls.Add(this.ultraLabel6);
            this.gbCK.Controls.Add(this.ultraLabel7);
            this.gbCK.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance26.FontData.BoldAsString = "True";
            appearance26.FontData.SizeInPoints = 10F;
            this.gbCK.HeaderAppearance = appearance26;
            this.gbCK.Location = new System.Drawing.Point(0, 0);
            this.gbCK.Name = "gbCK";
            this.gbCK.Size = new System.Drawing.Size(547, 169);
            this.gbCK.TabIndex = 33;
            this.gbCK.Text = "Thông tin chung";
            this.gbCK.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtBanks
            // 
            this.txtBanks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBanks.Location = new System.Drawing.Point(238, 107);
            this.txtBanks.Name = "txtBanks";
            this.txtBanks.Size = new System.Drawing.Size(296, 21);
            this.txtBanks.TabIndex = 13;
            // 
            // cbbBanks
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBanks.DisplayLayout.Appearance = appearance2;
            this.cbbBanks.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBanks.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBanks.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBanks.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.cbbBanks.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBanks.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.cbbBanks.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBanks.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBanks.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBanks.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.cbbBanks.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBanks.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBanks.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBanks.DisplayLayout.Override.CellAppearance = appearance9;
            this.cbbBanks.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBanks.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBanks.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.cbbBanks.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.cbbBanks.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBanks.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.cbbBanks.DisplayLayout.Override.RowAppearance = appearance12;
            this.cbbBanks.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBanks.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.cbbBanks.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBanks.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBanks.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBanks.Location = new System.Drawing.Point(90, 107);
            this.cbbBanks.Name = "cbbBanks";
            this.cbbBanks.Size = new System.Drawing.Size(144, 22);
            this.cbbBanks.TabIndex = 12;
            this.cbbBanks.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbBanks_RowSelected);
           // this.cbbBanks.TextChanged += new System.EventHandler(this.cbbBanks_TextChanged);
            //this.cbbBanks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbBanks_KeyPress);
            // 
            // txtDienGiai
            // 
            this.txtDienGiai.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDienGiai.Location = new System.Drawing.Point(88, 135);
            this.txtDienGiai.Multiline = true;
            this.txtDienGiai.Name = "txtDienGiai";
            this.txtDienGiai.Size = new System.Drawing.Size(446, 26);
            this.txtDienGiai.TabIndex = 10;
            // 
            // txtNN1
            // 
            this.txtNN1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNN1.Location = new System.Drawing.Point(89, 82);
            this.txtNN1.Name = "txtNN1";
            this.txtNN1.Size = new System.Drawing.Size(445, 21);
            this.txtNN1.TabIndex = 8;
            // 
            // txtDC1
            // 
            this.txtDC1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDC1.Location = new System.Drawing.Point(89, 57);
            this.txtDC1.Name = "txtDC1";
            this.txtDC1.Size = new System.Drawing.Size(445, 21);
            this.txtDC1.TabIndex = 7;
            // 
            // txtDT1
            // 
            this.txtDT1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDT1.Location = new System.Drawing.Point(239, 31);
            this.txtDT1.Name = "txtDT1";
            this.txtDT1.Size = new System.Drawing.Size(296, 21);
            this.txtDT1.TabIndex = 6;
            // 
            // cbbDT2
            // 
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDT2.DisplayLayout.Appearance = appearance14;
            this.cbbDT2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDT2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT2.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.cbbDT2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT2.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.cbbDT2.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDT2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDT2.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDT2.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.cbbDT2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDT2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDT2.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDT2.DisplayLayout.Override.CellAppearance = appearance21;
            this.cbbDT2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDT2.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT2.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.cbbDT2.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.cbbDT2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDT2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.cbbDT2.DisplayLayout.Override.RowAppearance = appearance24;
            this.cbbDT2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDT2.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.cbbDT2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDT2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDT2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDT2.Location = new System.Drawing.Point(89, 30);
            this.cbbDT2.Name = "cbbDT2";
            this.cbbDT2.Size = new System.Drawing.Size(144, 22);
            this.cbbDT2.TabIndex = 5;
            this.cbbDT2.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbDT2_RowSelected);
            this.cbbDT2.TextChanged += new System.EventHandler(this.cbbDT2_TextChanged);
            this.cbbDT2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbDT2_KeyPress);
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(6, 138);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(78, 23);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "Diễn giải";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(6, 109);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(78, 23);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Nộp vào TK";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(6, 82);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(79, 23);
            this.ultraLabel5.TabIndex = 2;
            this.ultraLabel5.Text = "Người Nộp";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(6, 59);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(77, 23);
            this.ultraLabel6.TabIndex = 1;
            this.ultraLabel6.Text = "Địa Chỉ";
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(7, 30);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 23);
            this.ultraLabel7.TabIndex = 0;
            this.ultraLabel7.Text = "Đối Tượng";
            // 
            // UtTinChungCk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbCK);
            this.Name = "UtTinChungCk";
            this.Size = new System.Drawing.Size(547, 169);
            ((System.ComponentModel.ISupportInitialize)(this.gbCK)).EndInit();
            this.gbCK.ResumeLayout(false);
            this.gbCK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBanks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox gbCK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBanks;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBanks;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDienGiai;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNN1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDC1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDT1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDT2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
    }
}
