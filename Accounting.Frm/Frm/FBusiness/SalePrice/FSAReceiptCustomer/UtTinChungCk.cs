﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class UtTinChungCk : UserControl
    {
        List<AccountingObject> lstAccountingObjects = new List<AccountingObject>();
       
       
        private bool isRun = false;
      
        public List<AccountingObject> DsAccountingObjects
        {
            get { return lstAccountingObjects; }
            set
            {
                List<AccountingObject> accountingObjects = value;
                if (accountingObjects == null || accountingObjects.Count <= 0) return;
                cbbDT2.DataSource = accountingObjects;
                lstAccountingObjects = accountingObjects;
                //thiết lập
                cbbDT2.DisplayMember = "AccountingObjectCode";
                Utils.ConfigGrid(cbbDT2, ConstDatabase.AccountingObject_TableName);
            }
        }

        public UtTinChungCk()
        {
            InitializeComponent();
        }
        private void cbbDT2_RowSelected(object sender, RowSelectedEventArgs e)
        {

            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            AccountingObject accountingObject = e.Row.ListObject as AccountingObject;
            txtDC1.Text = accountingObject.Address;
            txtDT1.Text = accountingObject.AccountingObjectName;
            txtNN1.Text = accountingObject.ContactName;
        }

        private void cbbBanks_RowSelected(object sender, RowSelectedEventArgs e)
        {

            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            BankAccountDetail bankAccountDetail = e.Row.ListObject as BankAccountDetail;
            txtBanks.Text = bankAccountDetail.BankName;
        }

        private void cbbDT2_TextChanged(object sender, EventArgs e)
        {
            if (isRun)
            {
                UltraCombo cbbTemp = (UltraCombo)sender;
                Utils.AutoComplateUltraCombo(cbbDT2, DsAccountingObjects.Where(x => x.AccountingObjectCode.Contains(cbbTemp.Text) || x.AccountingObjectName.Contains(cbbTemp.Text)).ToList());
            }
            else
                cbbDT2.DataSource = DsAccountingObjects;
        }

        private void cbbDT2_KeyPress(object sender, KeyPressEventArgs e)
        {
            isRun = true;
        }

      }
}
