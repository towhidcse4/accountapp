﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinToolTip;
using AutoCompleteMode = Infragistics.Win.AutoCompleteMode;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Resources = Accounting.Properties.Resources;
using Type = System.Type;
using System.ComponentModel;

#endregion

namespace Accounting
{
    public partial class FSAReceiptCustomerDetail : CustormForm
    {
        #region khai báo
        private bool _check = false;
        private bool _statusTemp;
        private bool isRun = true;
        int _typeIdPhuongThucThanhToan = 0;
        public static bool IsClose { get; set; }
        public static CollectionCustomer CollectionCustomerRa { get; set; }
        private CollectionCustomer _select = new CollectionCustomer();
        readonly List<CollectionCustomer> _listSelects = new List<CollectionCustomer>();
        readonly Template _template = Utils.GetTemplateInDatabase(null, 360);
        readonly List<string> _lstNgoaiTe = new List<string>() { "SoPhaiThu", "SoChuaThu", "SoThu", "DiscountAmountOriginal" };
        readonly List<string> _lstTienVND = new List<string>() { "SoPhaiThuQD", "SoChuaThuQD", "SoThuQD", "DiscountAmount" };
        List<Account> _listAccountsDefault = new List<Account>();
        Dictionary<string, string> dicAccountDefault = new Dictionary<string, string>();
        int LamTron = 0;
        int LamTron1 = 2;
        #endregion

        #region Khởi tạo
        public FSAReceiptCustomerDetail(CollectionCustomer collectionCustomer, IEnumerable<CollectionCustomer> lstCollectionCustomers)
        {
            InitializeComponent();

            #region khởi tạo cho form
            Dictionary<int, string> dictionary = new Dictionary<int, string> { { 0, "Tiền mặt" }, { 1, "Chuyển khoản" } };
            cbbMethod.DataSource = new BindingSource(dictionary, null);
            cbbMethod.ValueMember = "Key";
            cbbMethod.DisplayMember = "Value";
            //WindowState = FormWindowState.Maximized;
            #endregion

            #region Sự kiện
            cbbMethod.SelectionChanged += cbbMethod_SelectionChanged;
            btnCancel.Click += btnCancel_Click;
            uGHoaDon.InitializeRow += uGHoaDon_InitializeRow;
            uGHachToan.InitializeRow += uGHachToan_InitializeRow;
            uGridCurrency.InitializeRow += uGridCurrency_InitializeRow;
            cbbAccountingObjectIDCK.RowSelected += cbbAccountingObjectID_RowSelected;
            cbbAccountingObjectIDTM.RowSelected += cbbAccountingObjectID_RowSelected;
            cbbBankAccountDetailID.RowSelected += cbbBankAccountDetailID_RowSelected;
            uGHoaDon.AfterExitEditMode += uGHoaDon_AfterExitEditMode;
            uGHoaDon.CellChange += uGHoaDon_CellChange;
            uGHoaDon.SummaryValueChanged += uGHoaDon_SummaryValueChanged;
            uGHoaDon.AfterHeaderCheckStateChanged += uGHoaDon_AfterHeaderCheckStateChanged;
            btnThucHien.Click += btnThucHien_Click;
            uGridCurrency.AfterExitEditMode += uGridCurrency_AfterExitEditMode;
            #endregion
            LamTron = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_TienVND").Data);
            LamTron1 = int.Parse(Utils.ListSystemOption.FirstOrDefault(x => x.Code == "DDSo_NgoaiTe").Data);
            _select = collectionCustomer;
            _listSelects.AddRange(lstCollectionCustomers);
            _select.ListfFlexaccounts = new List<Flexaccount>();
            InitializeGUI(_select);
            Utils.ClearCacheByType<SystemOption>();

            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                // this.StartPosition = FormStartPosition.CenterParent;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
            }
        }
        #endregion

        #region overide
        private void InitializeGUI(CollectionCustomer temp)
        {
            if (temp == null) throw new ArgumentNullException("temp");

            #region Thiết lập dữ liệu và Fill dữ liệu Obj vào control
            //Đặt giá trị mặc định vùng chứng từ
            dteDate.Value = OPNUntil.StartDateOPN;
            dtePostedDate.Value = OPNUntil.StartDateOPN;

            List<AccountingObject> lstAccountingObjects = Utils.IAccountingObjectService.GetAccountingObjects(0);
            cbbAccountingObjectIDCK.DataSource = lstAccountingObjects;
            cbbAccountingObjectIDCK.ValueMember = "AccountingObjectCode";
            cbbAccountingObjectIDCK.DisplayMember = "AccountingObjectCode";

            cbbAccountingObjectIDTM.DataSource = lstAccountingObjects;
            cbbAccountingObjectIDTM.ValueMember = "AccountingObjectCode";
            cbbAccountingObjectIDTM.DisplayMember = "AccountingObjectCode";

            cbbBankAccountDetailID.DataSource = Utils.IBankAccountDetailService.GetIsActive(true);
            cbbBankAccountDetailID.ValueMember = "ID";
            cbbBankAccountDetailID.DisplayMember = "BankAccount";

            Utils.ConfigGrid(cbbAccountingObjectIDCK, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbAccountingObjectIDTM, ConstDatabase.AccountingObject_TableName);
            Utils.ConfigGrid(cbbBankAccountDetailID, ConstDatabase.BankAccountDetail_TableName);
            cbbAccountingObjectIDCK.DataBindings.Clear();
            cbbAccountingObjectIDTM.DataBindings.Clear();
            cbbAccountingObjectIDCK.DataBindings.Add("Value", temp, "AccountingObjectCode", true, DataSourceUpdateMode.OnPropertyChanged);
            cbbAccountingObjectIDTM.DataBindings.Add("Value", temp, "AccountingObjectCode", true, DataSourceUpdateMode.OnPropertyChanged);

            uGridCurrency.SetDataBinding(new List<CollectionCustomer>() { _select }, "");
            uGHoaDon.SetDataBinding(_select.ReceiptDebits.Where(c => c.SoChuaThu > 0 && c.CurrencyID == "VND" && c.SoChuaThu > 0 && c.TypeID != 601).OrderByDescending(t => t.Date).ToList(), "");
            uGHachToan.DataSource =  new BindingList<Flexaccount>();

            //Đặt phương thức thanh toán mặc định
            cbbMethod.SelectedIndex = 0;
            #region Thiết lập vùng tiền tệ
            ViewCurrency(false);
            #endregion

            #region Thiết lập Tab Hóa đơn
            ViewTabBill(false);
            #endregion

            #region Thiết lập Tab Hạch toán
            ViewTabAccounting(false);
            #endregion

            DayDuLieuLenControl(gbTM, _select.AccountingObject);
            DayDuLieuLenControl(gbCK, _select.AccountingObject);

            //if (_statusForm == ConstFrm.optStatusForm.Add)
            //{
            if (_select.AccountingObjectID == Guid.Empty)//trungnq thêm vào để  làm task 6234
            {
                txtReasonTM.Value = "Thu tiền khách hàng bằng tiền mặt";
                txtReasonTM.Text = "Thu tiền khách hàng bằng tiền mặt";
                _select.Reason = "Thu tiền khách hàng bằng tiền mặt";
            };
            //}

            #endregion
        }
        #endregion

        #region Utils
        private void ViewCurrency(bool load = true)
        {
            UltraGridBand band = uGridCurrency.DisplayLayout.Bands[0];
            if (!load)
            {
                Utils.ConfigGrid(uGridCurrency, "", _template.TemplateDetails.Single(x => x.TabIndex == 100).TemplateColumns);
                band.Summaries.Clear();
                foreach (UltraGridColumn column in uGridCurrency.DisplayLayout.Bands[0].Columns)
                {
                    Utils.ConfigEachColumn4Grid<CollectionCustomer>(this, 360, column, uGridCurrency);
                }
                UltraCombo cbbCurrency = (UltraCombo)uGridCurrency.DisplayLayout.Bands[0].Columns["CurrencyID"].ValueList;
                cbbCurrency.RowSelected += cbbCurrency_RowSelected;
            }
            // liên quan tới tiền
            Utils.FormatNumberic(band.Columns["ExchangeRate"], ConstDatabase.Format_Rate);
            Utils.FormatNumberic(band.Columns["TotalAmountOriginal"], _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            band.Columns["ExchangeRate"].NullText = "0";
            band.Columns["TotalAmountOriginal"].NullText = "0";
            Utils.FormatNumberic(band.Columns["TotalAmount"], ConstDatabase.Format_TienVND);
            band.Columns["TotalAmount"].Hidden = _select.CurrencyID == "VND";
            band.Columns["TotalAmount"].NullText = "0";
            band.Columns["ExchangeRate"].Hidden = _select.CurrencyID == "VND";
            //
            uGridCurrency.ConfigSizeGridCurrency(this);
        }
        private void ViewTabBill(bool load = true)
        {
            UltraGridBand band = uGHoaDon.DisplayLayout.Bands[0];
            if (!load)
            {
                Utils.ConfigGrid(uGHoaDon, "", _template.TemplateDetails.Single(x => x.TabIndex == 0).TemplateColumns, true, false);
                uGHoaDon.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
                uGHoaDon.DisplayLayout.Override.FilterUIType = FilterUIType.FilterRow;
                uGHoaDon.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
                UltraGridColumn ugc = band.Columns["CheckColumn"];
                ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
                ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                band.Columns["CheckColumn"].CellActivation = Activation.AllowEdit;
                foreach (UltraGridColumn gridColumn in uGHoaDon.DisplayLayout.Bands[0].Columns)
                {
                    Utils.ConfigEachColumn4Grid<CollectionCustomer>(this, 360, gridColumn, uGHoaDon);
                }
            }
            if (uGHoaDon.DisplayLayout.Bands[0].Summaries.Count > 0)
                uGHoaDon.DisplayLayout.Bands[0].Summaries.Clear();
            band.Columns["RefVoucherExchangeRate"].Hidden = _select.CurrencyID == "VND";
            band.Columns["LastExchangeRate"].Hidden = _select.CurrencyID == "VND";
            band.Columns["DifferAmount"].Hidden = _select.CurrencyID == "VND";
            band.Columns["SoPhatSinh"].FormatNumberic(_select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            band.Columns["SoPhaiThu"].FormatNumberic(_select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            band.Columns["SoChuaThu"].FormatNumberic(_select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            band.Columns["SoThu"].FormatNumberic(_select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            band.Columns["DiscountAmountOriginal"].FormatNumberic(_select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            //Utils.AddSumColumn(uGHoaDon, "CheckColumn", true, "Số dòng = " + uGHoaDon.Rows.Count);
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["CheckColumn"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGHoaDon.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;   //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGHoaDon.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            band.SummaryFooterCaption = @"Số dòng dữ liệu: ";
            uGHoaDon.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            Utils.AddSumColumn(uGHoaDon, "DifferAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGHoaDon, "SoPhatSinh", false, "", _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGHoaDon, "SoPhaiThu", false, "", _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGHoaDon, "SoChuaThu", false, "", _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGHoaDon, "SoThu", false, "", _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGHoaDon, "DiscountAmountOriginal", false, "", _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGHoaDon, "SoPhaiThuQD", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGHoaDon, "SoChuaThuQD", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGHoaDon, "SoThuQD", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGHoaDon, "DiscountAmount", false, "", ConstDatabase.Format_TienVND);
            foreach (UltraGridColumn column in uGHoaDon.DisplayLayout.Bands[0].Columns)
            {
                //Tài khoản chiết khấu
                if (column.Key.Contains("DiscountAccount"))
                {
                    Utils.ConfigCbbToGrid<CollectionCustomer, Account>(this, uGHoaDon, column.Key, Utils.IAccountDefaultService.GetAccountDefaultByTypeId(_typeIdPhuongThucThanhToan, "DiscountAccount")
                        , "AccountNumber", "AccountNumber", ConstDatabase.Account_TableName, isThread: false);
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                }
                if (_lstTienVND.Any(c => c == column.Key))
                {
                    column.Hidden = _select.CurrencyID == "VND";
                    Utils.FormatNumberic(column, ConstDatabase.Format_TienVND);
                    column.NullText = "0";
                }

            }
            //uGridBill.ConfigSizeGridCurrency(this);
            uGHoaDon.DisplayLayout.AutoFitStyle = _select.CurrencyID == "VND" ? AutoFitStyle.ResizeAllColumns : AutoFitStyle.ExtendLastColumn;
        }
        private void ViewTabAccounting(bool load = true)
        {
            UltraGridBand band = uGHachToan.DisplayLayout.Bands[0];
            if (!load)
            {
                Utils.ConfigGrid(uGHachToan, "", _template.TemplateDetails.Single(x => x.TabIndex == 1).TemplateColumns, true);
                uGHachToan.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

                foreach (UltraGridColumn column in uGHachToan.DisplayLayout.Bands[0].Columns)
                {
                    Utils.ConfigEachColumn4Grid<CollectionCustomer>(this, _typeIdPhuongThucThanhToan, column, uGHachToan);
                    if (column.Key.Contains("DebitAccount"))
                    {
                        Utils.ConfigCbbToGrid<CollectionCustomer, Account>(this, uGHachToan, column.Key, _listAccountsDefault
                            , "AccountNumber", "AccountNumber", ConstDatabase.Account_TableName, isThread: false);
                        column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                    }
                }
            }
            band.Summaries.Clear();
            Utils.FormatNumberic(band.Columns["TotalAmountOriginal"], _select.CurrencyID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            band.Columns["TotalAmountOriginal"].NullText = "0";
            Utils.FormatNumberic(uGridCurrency.DisplayLayout.Bands[0].Columns["TotalAmount"], ConstDatabase.Format_TienVND);
            band.Columns["TotalAmount"].NullText = "0";
            band.Columns["TotalAmount"].Hidden = _select.CurrencyID == "VND";
            Utils.AddSumColumn(uGHachToan, "TotalAmountOriginal", false, "", ConstDatabase.Format_TienVND);
        }
        public static void DayDuLieuLenControl<T>(Control ctrlContainer, T doiTuong)
        {
            Type elementType = typeof(T);
            foreach (Control control in ctrlContainer.Controls)
            {
                if (control.GetType().Name.Contains(typeof(UltraTextEditor).Name))
                {
                    var textEditor = (UltraTextEditor)control;
                    foreach (var propInfo in elementType.GetProperties())
                    {
                        if (textEditor.Name.StartsWith("txt" + propInfo.Name) && propInfo.CanWrite && !propInfo.GetType().FullName.Contains("Accounting.Core.Domain") && textEditor.Name != "txtBankName")
                        {
                            string s = doiTuong.GetProperty<T, string>(propInfo.Name);
                            if (string.IsNullOrEmpty(s)) continue;
                            textEditor.Text = s;
                            //textEditor.Text = (string)(propInfo.GetValue(doiTuong, null) ?? "");
                        }
                    }
                }
                //if (control.HasChildren
                //    && (!control.GetType().Name.Contains(typeof(UltraCombo).Name)
                //        || !control.GetType().Name.Contains(typeof(UltraGrid).Name)))
                //    DayDuLieuLenControl(control, doiTuong);
            }
        }
        #endregion

        #region Event
        #region Lựa chọn phương thức thanh toán
        private void cbbMethod_SelectionChanged(object sender, EventArgs e)
        {
            switch (cbbMethod.SelectedIndex)
            {
                case 0:
                    _typeIdPhuongThucThanhToan = 101;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroup_MCReceipt));
                    pnTienMat.Visible = true;
                    pnChuyenKhoan.Visible = false;
                    pnTienMat.Dock = DockStyle.Fill;
                    _listAccountsDefault = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(361, "DebitAccount");
                    foreach (UltraGridColumn column in uGHachToan.DisplayLayout.Bands[0].Columns)
                    {
                        if (column.Key.Contains("DebitAccount"))
                        {
                            Utils.ConfigCbbToGrid<CollectionCustomer, Account>(this, uGHachToan, column.Key, _listAccountsDefault
                                , "AccountNumber", "AccountNumber", ConstDatabase.Account_TableName, isThread: false);
                            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                        }
                    }
                    
                    if (_select != null && _select.ListfFlexaccounts.Count > 0) // add by tungnt: chọn phương thức thanh toán thì fill lại dữ liệu tab hạch toán 
                    {
                        uGHachToan.DataSource = new BindingList<Flexaccount>(_select.ListfFlexaccounts);
                        if (uGHachToan.Rows.Count > 0)
                            foreach (UltraGridRow row in uGHachToan.Rows)
                            {
                                row.Cells["DebitAccount"].Value = _typeIdPhuongThucThanhToan == 101 ? (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1111" : "1112") : (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1121" : "1122");
                            }
                        uGHachToan.Refresh();
                        uGHachToan.Update();
                        uGHachToan.UpdateData();
                        uGHachToan.Refresh();
                    }
                    txtReasonTM.Value = "Thu tiền khách hàng bằng tiền mặt";
                    txtReasonTM.Text = "Thu tiền khách hàng bằng tiền mặt";
                    _select.Reason = "Thu tiền khách hàng bằng tiền mặt";
                    break;
                case 1:
                    _typeIdPhuongThucThanhToan = 161;
                    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(16));
                    pnTienMat.Visible = false;
                    pnChuyenKhoan.Visible = true;
                    pnChuyenKhoan.Dock = DockStyle.Fill;
                    _listAccountsDefault = Utils.IAccountDefaultService.GetAccountDefaultByTypeId(362, "DebitAccount");
                    foreach (UltraGridColumn column in uGHachToan.DisplayLayout.Bands[0].Columns)
                    {
                        if (column.Key.Contains("DebitAccount"))
                        {
                            Utils.ConfigCbbToGrid<CollectionCustomer, Account>(this, uGHachToan, column.Key, _listAccountsDefault
                                , "AccountNumber", "AccountNumber", ConstDatabase.Account_TableName, isThread: false);
                            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate;
                        }
                    }
                    
                    if (_select != null && _select.ListfFlexaccounts.Count > 0) // add by tungnt: chọn phương thức thanh toán thì fill lại dữ liệu tab hạch toán 
                    {
                        uGHachToan.DataSource = new BindingList<Flexaccount>(_select.ListfFlexaccounts);
                        if (uGHachToan.Rows.Count > 0)
                            foreach (UltraGridRow row in uGHachToan.Rows)
                            {
                                row.Cells["DebitAccount"].Value = _typeIdPhuongThucThanhToan == 161 ? (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1121" : "1122") : (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() == "VND" ? "1111" : "1112");
                            }
                        uGHachToan.Refresh();
                        uGHachToan.Update();
                        uGHachToan.UpdateData();
                        uGHachToan.Refresh();
                    }
                    txtReasonCK.Value = "Thu tiền KH bằng TGNH";
                    txtReasonCK.Text = "Thu tiền KH bằng TGNH";
                    _select.Reason = "Thu tiền KH bằng TGNH";
                    break;
            }
            //dicAccountDefault = Utils.IAccountDefaultService.DefaultAccount();
            //if (_select != null && _select.ListfFlexaccounts.Count > 0)
            //{
            //    ViewTabAccounting(false);
            //    foreach (Flexaccount a in _select.ListfFlexaccounts)
            //    {
            //        a.DebitAccount = dicAccountDefault.FirstOrDefault(p => p.Key.StartsWith(string.Format("{0};{1}", _typeIdPhuongThucThanhToan, "DebitAccount"))).Value;
            //    }
            //    uGHachToan.SetDataBinding(_select.ListfFlexaccounts.Where(c => c.CurrencyID == _select.CurrencyID).ToList(), "");
            //    //uGridBill.ResumeSummaryUpdates(true);
            //    //uGridBill.DisplayLayout.Bands[0].ResetSummaries();
            //    uGHachToan.Refresh();
            //    uGHachToan.Update();
            //    uGHachToan.UpdateData();
            //    uGHachToan.Refresh();
            //}

        }
        #endregion

        private void cbbAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null && (e.Row.ListObject as AccountingObject) != null)
            {
                if (e.Row.Selected) isRun = false;
                if (!isRun)
                {
                    var accountingObject = e.Row.ListObject as AccountingObject;
                    _select = _listSelects.FirstOrDefault(c => c.AccountingObjectID == accountingObject.ID);
                    if (_select != null)
                    {
                        uGridCurrency.SetDataBinding(new List<CollectionCustomer>() { _select }, "");
                        uGHoaDon.SetDataBinding(_select.ReceiptDebits.Where(c => c.SoChuaThu > 0 && c.CurrencyID == (string)uGridCurrency.Rows[0].Cells["CurrencyID"].Value && c.SoChuaThu > 0 && c.TypeID != 601).OrderByDescending(x => x.Date).ToList(), "");
                        uGHachToan.DataSource = new BindingList<Flexaccount>();
                        DayDuLieuLenControl(gbTM, _select.AccountingObject);
                        DayDuLieuLenControl(gbCK, _select.AccountingObject);
                    }
                    else
                    {
                        uGridCurrency.SetDataBinding(new List<CollectionCustomer>(), "");
                        uGHoaDon.SetDataBinding(new List<ReceiptDebit>(), "");
                        uGHachToan.DataSource = new BindingList<Flexaccount>();
                        DayDuLieuLenControl(gbTM, e.Row.ListObject as AccountingObject);
                        DayDuLieuLenControl(gbCK, e.Row.ListObject as AccountingObject);
                    }
                }
                else
                    isRun = false;
            }
        }
        private void cbbBankAccountDetailID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null && (e.Row.ListObject as BankAccountDetail) != null)
            {
                BankAccountDetail bankAccountDetail = e.Row.ListObject as BankAccountDetail;
                _select.BankAccountDetailID = bankAccountDetail.ID;
                _select.BankName = bankAccountDetail.BankName;
                txtBankName.Text = _select.BankName;
            }
        }

        #region Event uGridCurrency
        #region Lựa chọn loại tiền tệ
        private void cbbCurrency_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row != null && e.Row.Selected)
            {
                Currency currency = (Currency)((UltraCombo)sender).SelectedRow.ListObject;
                if (_select.CurrencyID != currency.ID)
                {
                    _select.CurrencyID = currency.ID;
                    _select.TotalAmountOriginal = _select.ReceiptDebits.Where(c => c.CheckColumn && c.CurrencyID == _select.CurrencyID && c.SoThu != null).Sum(c => c.SoThu) -
                         _select.ReceiptDebits.Where(c => c.CheckColumn && c.CurrencyID == _select.CurrencyID).Sum(c => c.DiscountAmountOriginal);
                    _select.TotalAmount = _select.TotalAmountOriginal * _select.ExchangeRate;
                    _select.ExchangeRate = currency.ExchangeRate;
                    if (_select.ReceiptDebits.Any(c => c.CheckColumn))
                    {
                        foreach (var receiptDebit in _select.ReceiptDebits)
                        {
                            if (receiptDebit.CheckColumn)
                            {
                                receiptDebit.SoChuaThuQD = (decimal)(receiptDebit.SoChuaThu * _select.ExchangeRate);
                            }
                        }
                    }
                    uGHoaDon.SetDataBinding(_select.ReceiptDebits.Where(c => c.SoChuaThu > 0 && c.CurrencyID == _select.CurrencyID && c.SoChuaThu > 0 && c.TypeID != 601).ToList(), "");
                    uGHachToan.DataSource = new BindingList<Flexaccount>(_select.ListfFlexaccounts.Where(c => c.CurrencyID == _select.CurrencyID).ToList());
                    if (uGHoaDon.DisplayLayout.Bands[0].Summaries.Exists("SoThuSum"))
                    {
                        uGHoaDon.DisplayLayout.Bands[0].Summaries["SoThuSum"].Refresh();
                        uGHoaDon_SummaryValueChanged(uGHoaDon, new SummaryValueChangedEventArgs(uGHoaDon.Rows.SummaryValues["SoThuSum"]));
                    }
                    uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                    uGHachToan.Rows.Refresh(RefreshRow.FireInitializeRow);
                    ViewCurrency();
                    ViewTabBill();
                    ViewTabAccounting();
                }
                if (currency.ID != "VND")
                {
                    uGHoaDon.DisplayLayout.Bands[0].Summaries.Clear();
                    Utils.AddSumColumn(uGHoaDon, "DifferAmount", false, "", ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGHoaDon, "SoPhatSinh", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGHoaDon, "SoPhaiThu", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGHoaDon, "SoChuaThu", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGHoaDon, "SoThu", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGHoaDon, "DiscountAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGHoaDon, "SoPhaiThuQD", false, "", ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGHoaDon, "SoChuaThuQD", false, "", ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGHoaDon, "SoThuQD", false, "", ConstDatabase.Format_TienVND);
                    Utils.AddSumColumn(uGHoaDon, "DiscountAmount", false, "", ConstDatabase.Format_TienVND);

                    uGHachToan.DisplayLayout.Bands[0].Summaries.Clear();
                    Utils.AddSumColumn(uGHachToan, "TotalAmountOriginal", false, "", ConstDatabase.Format_ForeignCurrency);
                    Utils.AddSumColumn(uGHachToan, "TotalAmount", false, "", ConstDatabase.Format_TienVND);
                }
            }
        }
        #endregion
        private void uGridCurrency_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //e.Row.Cells["TotalAmount"].Value = ((decimal)e.Row.Cells["TotalAmountOriginal"].Value) * e.Row.Cells["ExchangeRate"].GetValueFromTextAndNotNull();
            //e.Row.Update();
            //uGridCurrency.UpdateData();
        }
        private void uGridCurrency_AfterExitEditMode(object sender, EventArgs e)
        {
            uGridCurrency.UpdateData();
            UltraGridCell cell = uGridCurrency.ActiveCell;
            if ((cell.Column.Key == "TotalAmountOriginal" && (decimal)cell.Value > 0) || (cell.Column.Key == "ExchangeRate" && (decimal)cell.Value >= 0))
            {
                if ((cell.Column.Key == "TotalAmountOriginal" && (decimal)cell.Value > 0))
                {
                    decimal tongtien = (decimal)cell.Value;
                    foreach (ReceiptDebit receiptDebit in _select.ReceiptDebits.OrderByDescending(c => c.CheckColumn))
                    {
                        if (receiptDebit.CheckColumn)
                        {
                            tongtien = tongtien - receiptDebit.SoChuaThu;
                        }
                        else
                        {
                            if (tongtien > 0)
                            {
                                receiptDebit.SoThu = (tongtien >= receiptDebit.SoChuaThu ? receiptDebit.SoChuaThu : tongtien);
                                receiptDebit.CheckColumn = true;
                                tongtien = tongtien - receiptDebit.SoChuaThu;
                            }
                            else if (_select.ReceiptDebits.All(c => c.CheckColumn == false))
                            {
                                receiptDebit.SoThu = 0M;
                                receiptDebit.CheckColumn = false;
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }

                }
                else
                {
                    uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                }
                uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Math.Round((decimal)uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero);
                uGridCurrency.UpdateData();
                uGridCurrency.Refresh();
                uGHoaDon.UpdateData();
                uGHoaDon.Refresh();
                uGHoaDon.DisplayLayout.Bands[0].Summaries["SoThuSum"].Refresh();
            }
        }
        private void uGridCurrency_Error(object sender, ErrorEventArgs e)
        {
            UltraGridCell cell = uGridCurrency.ActiveCell;
            if (uGridCurrency.ActiveCell == null) return;
            if (cell.Column.Key.Equals("EmployeeID"))
            {
                int count = Utils.ListAccountingObject.Where(x => x.IsEmployee).ToList().Count(x => x.AccountingObjectCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
            }
            e.Cancel = true;
        }
        #endregion

        #region Event uGHoaDon
        private void uGHoaDon_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["SoThu"].Value != null)
            {
                e.Row.Cells["DifferAmount"].Value = Math.Round((((decimal)e.Row.Cells["SoThu"].Value * _select.ExchangeRate) ?? 0) - ((decimal)e.Row.Cells["LastExchangeRate"].Value == 0 ?
                  ((decimal)e.Row.Cells["SoThu"].Value * (decimal)e.Row.Cells["RefVoucherExchangeRate"].Value) : ((decimal)e.Row.Cells["SoThu"].Value * (decimal)e.Row.Cells["LastExchangeRate"].Value)), LamTron, MidpointRounding.AwayFromZero);
                e.Row.Cells["SoThuQD"].Value = Math.Round(((decimal)e.Row.Cells["SoThu"].Value * _select.ExchangeRate) ?? 0, LamTron, MidpointRounding.AwayFromZero);
                e.Row.Cells["SoThuHT"].Value = (decimal)e.Row.Cells["DifferAmount"].Value <= 0 ? Math.Round(((decimal)e.Row.Cells["SoThu"].Value * _select.ExchangeRate) ?? 0, LamTron, MidpointRounding.AwayFromZero) :
                  Math.Round(((decimal)e.Row.Cells["SoThu"].Value * ((decimal)e.Row.Cells["LastExchangeRate"].Value == 0 ? (decimal)e.Row.Cells["RefVoucherExchangeRate"].Value : (decimal)e.Row.Cells["LastExchangeRate"].Value)), LamTron, MidpointRounding.AwayFromZero);
            }
            e.Row.Cells["DiscountAmount"].Value = Math.Round(((decimal)e.Row.Cells["DiscountAmountOriginal"].Value * _select.ExchangeRate) ?? 0, LamTron, MidpointRounding.AwayFromZero);
            e.Row.Update();
            uGHoaDon.UpdateData();
        }
        private void uGHoaDon_AfterExitEditMode(object sender, EventArgs e)
        {
            AfterExitEditModeBill();
        }
        public void AfterExitEditModeBill()
        {
            UltraGridCell cell = uGHoaDon.ActiveCell;
            switch (cell.Column.Key)
            {
                case "SoThu":
                    if (!cell.Row.Cells["SoThu"].IsFilterRowCell)
                    {
                        if (cell.Row.Cells["SoThu"].Value != null)
                        {
                            if ((decimal)cell.Row.Cells["SoThu"].Value > (decimal)cell.Row.Cells["SoChuaThu"].Value)
                            {
                                Utils.NotificationCell(uGHoaDon, cell, "Số thu không được lớn hơn số chưa thu");
                                MSG.Warning("Số thu không được lớn hơn số chưa thu");
                            }
                            else
                            {
                                Utils.RemoveNotificationCell(uGHoaDon, cell);
                            }
                            if ((decimal)cell.Row.Cells["SoThu"].Value > 0)
                            {
                                cell.Row.Cells["CheckColumn"].Value = true;
                                cell.Row.Cells["DiscountAmountOriginal"].Value = Math.Round(((decimal)cell.Row.Cells["SoThu"].Value *
                                                                (decimal)cell.Row.Cells["DiscountRate"].Value) / 100, _select.ExchangeRate == 1 ? LamTron : LamTron1, MidpointRounding.AwayFromZero);
                                cell.Row.Cells["DiscountAmount"].Value = Math.Round(((decimal)cell.Row.Cells["SoThuQD"].Value *
                                                                (decimal)cell.Row.Cells["DiscountRate"].Value) / 100, LamTron, MidpointRounding.AwayFromZero);
                                uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                                uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuQDSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                                uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                            }
                        }
                        else
                        {
                            cell.Row.Cells["CheckColumn"].Value = false;
                            uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                            uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuQDSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                            uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                        }
                    }
                    break;
                case "SoThuQD":
                    uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuQDSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                    uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                    break;
                case "DiscountRate":
                    if (!cell.Row.Cells["DiscountRate"].IsFilterRowCell)
                    {
                        if (cell.Row.Cells["SoThu"].Value != null)
                        {
                            cell.Row.Cells["DiscountAmountOriginal"].Value = Math.Round(((decimal)cell.Row.Cells["SoThu"].Value *
                                                                 (decimal)cell.Row.Cells["DiscountRate"].Value) / 100, _select.ExchangeRate == 1 ? LamTron : LamTron1, MidpointRounding.AwayFromZero);
                            cell.Row.Cells["DiscountAmount"].Value = Math.Round(((decimal)cell.Row.Cells["SoThuQD"].Value *
                                                        (decimal)cell.Row.Cells["DiscountRate"].Value) / 100, LamTron, MidpointRounding.AwayFromZero);
                        }
                    }
                    break;
                case "DiscountAccount":
                    if (!cell.Row.Cells["DiscountAccount"].IsFilterRowCell)
                    {
                        //edit by tungnt: không group các chứng từ tích chọn để trả tiền mà để mỗi chứng từ sinh ra 1 dòng bên tab hạch toán
                        List<Flexaccount> lstFlexaccounts = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID).ToList()
                                                             //group c by c.CreditAccount into g
                                                             select new Flexaccount()
                                                             {
                                                                 DebitAccount = _typeIdPhuongThucThanhToan == 101 ? (_select.CurrencyID == "VND" ? "1111" : "1112") : (_select.CurrencyID == "VND" ? "1121" : "1122")/*dicAccountDefault.FirstOrDefault(p => p.Key.StartsWith(string.Format("{0};{1}", _typeIdPhuongThucThanhToan, "DebitAccount"))).Value*/,
                                                                 CreditAccount = c.CreditAccount,
                                                                 TotalAmount = Math.Round(((c.SoThu ?? 0M) - c.DiscountAmountOriginal) * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero),
                                                                 TotalAmountOriginal = (c.SoThu ?? 0M) - c.DiscountAmountOriginal,
                                                                 AccountingObjectID = _select.AccountingObject.ID,
                                                                 AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                                 CurrencyID = _select.CurrencyID,
                                                                 Description = "Thu tiền khách hàng " + _select.AccountingObjectName
                                                             }).ToList();
                        List<Flexaccount> lstFlexaccountsdiscount = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID && d.DiscountAccount != null && d.DiscountAccount != "").ToList()
                                                                     //group c by new { c.CreditAccount, c.DiscountAccount } into g
                                                                     select new Flexaccount()
                                                                     {
                                                                         DebitAccount = c.DiscountAccount,
                                                                         CreditAccount = c.CreditAccount,
                                                                         TotalAmount = c.DiscountAmount,
                                                                         TotalAmountOriginal = c.DiscountAmountOriginal,
                                                                         AccountingObjectID = _select.AccountingObject.ID,
                                                                         AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                                         CurrencyID = _select.CurrencyID,
                                                                         Description = "Chiết khấu thanh toán"
                                                                     }).ToList();
                        List<Flexaccount> lstClEx1 = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID && d.DifferAmount > 0).ToList()
                                                      //group c by new { c.CreditAccount } into g
                                                      select new Flexaccount()
                                                      {
                                                          DebitAccount = _typeIdPhuongThucThanhToan == 101 ? "1112" : "1122",
                                                          CreditAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLai").Data,
                                                          TotalAmount = c.DifferAmount,
                                                          TotalAmountOriginal = 0,
                                                          AccountingObjectID = _select.AccountingObject.ID,
                                                          AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                          CurrencyID = _select.CurrencyID,
                                                          Description = "Xử lý lãi chênh lệch tỷ giá"
                                                      }).ToList();
                        List<Flexaccount> lstClEx2 = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID && d.DifferAmount < 0).ToList()
                                                      //group c by new { c.CreditAccount } into g
                                                      select new Flexaccount()
                                                      {
                                                          DebitAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLo").Data,
                                                          CreditAccount = c.CreditAccount,
                                                          TotalAmount = Math.Abs(c.DifferAmount),
                                                          TotalAmountOriginal = 0,
                                                          AccountingObjectID = _select.AccountingObject.ID,
                                                          AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                          CurrencyID = _select.CurrencyID,
                                                          Description = "Xử lý lỗ chênh lệch tỷ giá"
                                                      }).ToList();
                        _select.ListfFlexaccounts.Clear();
                        _select.ListfFlexaccounts.AddRange(lstFlexaccounts);
                        _select.ListfFlexaccounts.AddRange(lstFlexaccountsdiscount);
                        if (lstClEx1.Count > 0) _select.ListfFlexaccounts.AddRange(lstClEx1);
                        if (lstClEx2.Count > 0) _select.ListfFlexaccounts.AddRange(lstClEx2);

                        uGHachToan.DataSource = new BindingList<Flexaccount>(_select.ListfFlexaccounts.Where(c => c.CurrencyID == _select.CurrencyID).ToList());
                        uGHachToan.UpdateData();
                        uGHachToan.Refresh();
                    }
                    break;
            }
        }
        private void uGHoaDon_CellChange(object sender, CellEventArgs e)
        {
            uGHoaDon.Update();
            if (!e.Cell.Row.Cells[e.Cell.Column.Key].IsFilterRowCell)
            {
                switch (e.Cell.Column.Key)
                {
                    case "CheckColumn":
                        if (Boolean.Parse(e.Cell.Row.Cells["CheckColumn"].Text))
                        {
                            e.Cell.Row.Cells["SoThu"].Value = (decimal)e.Cell.Row.Cells["SoChuaThu"].Value;
                            uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                            if (uGHoaDon.DisplayLayout.Bands[0].Summaries.Exists("SoThuSum"))
                                uGHoaDon.DisplayLayout.Bands[0].Summaries["SoThuSum"].Refresh();
                            uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                            uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuQDSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                        }
                        else
                        {
                            e.Cell.Row.Cells["SoThu"].Value = 0M;
                            e.Cell.Row.Cells["SoThuQD"].Value = 0M;
                            e.Cell.Row.Cells["DiscountRate"].Value = 0;
                            e.Cell.Row.Cells["DiscountAmountOriginal"].Value = 0;
                            uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                            uGHoaDon.RemoveNotificationCell(e.Cell.Row.Cells["SoThu"]);
                        }
                        break;
                }
            }
        }
        private void uGHoaDon_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (e.Column.Key == "CheckColumn")
            {
                UltraGridColumn col = uGHoaDon.DisplayLayout.Bands[0].Columns["CheckColumn"];
                switch (col.GetHeaderCheckedState(e.Rows))
                {
                    case CheckState.Checked:
                        foreach (UltraGridRow row in uGHoaDon.Rows)
                        {
                            if (!row.Cells["SoThu"].IsFilterRowCell && (decimal)row.Cells["SoThu"].Value == 0)
                            {
                                row.Cells["SoThu"].Value = row.Cells["SoChuaThu"].Value;
                            }
                        }
                        uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                        if (uGHoaDon.DisplayLayout.Bands[0].Summaries.Exists("SoThuSum"))
                            uGHoaDon.DisplayLayout.Bands[0].Summaries["SoThuSum"].Refresh();
                        uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                        uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuQDSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                        break;
                    case CheckState.Unchecked:
                        foreach (UltraGridRow row in uGHoaDon.Rows)
                        {
                            if (!row.Cells["SoThu"].IsFilterRowCell)
                            {
                                row.Cells["SoThu"].Value = 0M;
                                row.Cells["SoThuQD"].Value = 0M;
                                row.Cells["DiscountRate"].Value = 0;
                                row.Cells["DiscountAmountOriginal"].Value = 0;
                            }
                        }
                        //if (uGridCurrency.Rows.Count != 0) uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(0);
                        uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = 0M;
                        uGridCurrency.Rows[0].Cells["TotalAmount"].Value = 0M;
                        uGHoaDon.Rows.Refresh(RefreshRow.FireInitializeRow);
                        uGridCurrency.Rows.Refresh(RefreshRow.FireInitializeRow);
                        break;
                }
            }
        }
        private void uGHoaDon_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            //edit by tungnt: không group các chứng từ tích chọn để trả tiền mà để mỗi chứng từ sinh ra 1 dòng bên tab hạch toán
            if (_select == null) return;
            if (Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) != 0)
            {
                //uGridCurrency.Rows[0].Cells["TotalAmountOriginal"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountOriginalSum"].Value.ToString());
                //uGridCurrency.Rows[0].Cells["TotalAmount"].Value = Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuQDSum"].Value.ToString()) - Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["DiscountAmountSum"].Value.ToString());
                //Guid TryID = new Guid();
                List<Flexaccount> lstFlexaccounts = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID).ToList()
                                                     //group c by c.CreditAccount
                                                     //    into g
                                                     select new Flexaccount()
                                                     {
                                                         DebitAccount = _typeIdPhuongThucThanhToan == 101 ? (_select.CurrencyID == "VND" ? "1111" : "1112") : (_select.CurrencyID == "VND" ? "1121" : "1122"),
                                                         CreditAccount = c.CreditAccount,
                                                         TotalAmount = Math.Round(((c.SoThu ?? 0M) - c.DiscountAmountOriginal) * (decimal)uGridCurrency.Rows[0].Cells["ExchangeRate"].Value, LamTron, MidpointRounding.AwayFromZero),
                                                         TotalAmountOriginal = (c.SoThu ?? 0M) - c.DiscountAmountOriginal,
                                                         AccountingObjectID = _select.AccountingObject.ID,
                                                         AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                         CurrencyID = _select.CurrencyID,
                                                         Description = "Thu tiền khách hàng " + _select.AccountingObjectName
                                                     }).ToList();
                List<Flexaccount> lstFlexaccountsdiscount = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID && d.DiscountAccount != null && d.DiscountAccount != "").ToList()
                                                             //group c by new { c.CreditAccount, c.DiscountAccount }
                                                         //into g
                                                             select new Flexaccount()
                                                             {
                                                                 DebitAccount = c.DiscountAccount,
                                                                 CreditAccount = c.CreditAccount,
                                                                 TotalAmount = c.DiscountAmount,
                                                                 TotalAmountOriginal = c.DiscountAmountOriginal,
                                                                 AccountingObjectID = _select.AccountingObject.ID,
                                                                 AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                                 CurrencyID = _select.CurrencyID,
                                                                 Description = "Chiết khấu thanh toán "
                                                             }).ToList();
                List<Flexaccount> lstClEx1 = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID && d.DifferAmount > 0).ToList()
                                              //group c by new { c.CreditAccount } into g
                                              select new Flexaccount()
                                              {
                                                  DebitAccount = _typeIdPhuongThucThanhToan == 101 ? "1112" : "1122",
                                                  CreditAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLai").Data,
                                                  TotalAmount = c.DifferAmount,
                                                  TotalAmountOriginal = 0,
                                                  AccountingObjectID = _select.AccountingObject.ID,
                                                  AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                  CurrencyID = _select.CurrencyID,
                                                  Description = "Xử lý lãi chênh lệch tỷ giá"
                                              }).ToList();
                List<Flexaccount> lstClEx2 = (from c in _select.ReceiptDebits.Where(d => d.CheckColumn && d.CurrencyID == _select.CurrencyID && d.DifferAmount < 0).ToList()
                                              //group c by new { c.CreditAccount } into g
                                              select new Flexaccount()
                                              {
                                                  DebitAccount = Utils.ListSystemOption.FirstOrDefault(x => x.Code == "TCKHAC_TKCLechLo").Data,
                                                  CreditAccount = c.CreditAccount,
                                                  TotalAmount = Math.Abs(c.DifferAmount),
                                                  TotalAmountOriginal = 0,
                                                  AccountingObjectID = _select.AccountingObject.ID,
                                                  AccountingObjectCode = _select.AccountingObject.AccountingObjectCode,
                                                  CurrencyID = _select.CurrencyID,
                                                  Description = "Xử lý lỗ chênh lệch tỷ giá"
                                              }).ToList();
                _select.ListfFlexaccounts.Clear();
                _select.ListfFlexaccounts.AddRange(lstFlexaccounts);
                _select.ListfFlexaccounts.AddRange(lstFlexaccountsdiscount);
                if (lstClEx1.Count > 0) _select.ListfFlexaccounts.AddRange(lstClEx1);
                if (lstClEx2.Count > 0) _select.ListfFlexaccounts.AddRange(lstClEx2);
                _select.TotalAmountOriginal = _select.ReceiptDebits.Where(c => c.CheckColumn && c.CurrencyID == _select.CurrencyID && c.SoThu != null).Sum(c => c.SoThu) -
                    _select.ReceiptDebits.Where(c => c.CheckColumn && c.CurrencyID == _select.CurrencyID).Sum(c => c.DiscountAmountOriginal);

                uGHachToan.DataSource = new BindingList<Flexaccount>(_select.ListfFlexaccounts.Where(c => c.CurrencyID == _select.CurrencyID).ToList());
                uGHachToan.UpdateData();
                uGHachToan.Refresh();
                uGridCurrency.UpdateData();
                uGridCurrency.Refresh();
            }
            else if (_select.ReceiptDebits.All(c => c.CheckColumn == false))
            {
                if (_select != null)
                {
                    _select.ListfFlexaccounts.Clear();
                    uGHachToan.DataSource = new BindingList<Flexaccount>(_select.ListfFlexaccounts.Where(c => c.CurrencyID == _select.CurrencyID).ToList());
                    uGHachToan.UpdateData();
                    uGHachToan.Refresh();
                    uGridCurrency.UpdateData();
                    uGridCurrency.Refresh();
                }
            }


        }
        #endregion

        private void uGHachToan_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            e.Row.Cells["TotalAmountOriginal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            e.Row.Cells["TotalAmount"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
            //e.Row.Cells["TotalAmount"].Value = Math.Round(((decimal)e.Row.Cells["TotalAmountOriginal"].Value * _select.ExchangeRate) ?? 0, LamTron, MidpointRounding.AwayFromZero);
            e.Row.Update();
            uGHachToan.UpdateData();
        }
        private void dtePostedDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtePostedDate != null)
            {
                dteDate.Value = dtePostedDate.Value;
            }
        }


        #endregion

        #region btn
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnThucHien_Click(object sender, EventArgs e)
        {
            if (!_select.ReceiptDebits.Any(c => c.CurrencyID == _select.CurrencyID && c.CheckColumn))
            {
                MSG.Warning("Bạn chưa chọn hóa đơn bán hàng để thu tiền");
                return;
            }
            if ((uGHoaDon.Rows[0].Cells["DiscountAmount"].Value.ToInt() != 0 || uGHoaDon.Rows[0].Cells["DiscountAmountOriginal"].Value.ToInt() != 0) && uGHoaDon.Rows[0].Cells["DiscountAccount"].Value == null)
            {
                if (!(DialogResult.Yes == MessageBox.Show(resSystem.MSG_Warning_03, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                {
                    return;
                }
            }
            if(Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) < Convert.ToDecimal(uGHachToan.Rows.SummaryValues["TotalAmountOriginalSum"].Value.ToString()))
            {
                MSG.Warning("Tổng Số Tiền ở tab Hạch Toán phải bằng Tổng Số Thu ở tab Hóa Đơn. Vui lòng kiểm tra lại!");
                return;
            }
            if (Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) > Convert.ToDecimal(uGHachToan.Rows.SummaryValues["TotalAmountOriginalSum"].Value.ToString()))
            {
                MSG.Warning("Tổng Số Tiền ở tab Hạch Toán phải bằng Tổng Số Thu ở tab Hóa Đơn. Vui lòng kiểm tra lại!");
                return;
            }
            if (Convert.ToDecimal(uGHoaDon.Rows.SummaryValues["SoThuSum"].Value.ToString()) != 0 && Convert.ToDecimal(uGHachToan.Rows.SummaryValues["TotalAmountOriginalSum"].Value.ToString()) == 0)
            {
                MSG.Warning("Tổng Số Tiền ở tab Hạch Toán phải bằng Tổng Số Thu ở tab Hóa Đơn. Vui lòng kiểm tra lại!");
                return;
            }
            _select.ListfFlexaccounts = ((BindingList<Flexaccount>)uGHachToan.DataSource).ToList();
            _select.ReceiptDebits =
                _select.ReceiptDebits.Where(c => c.CheckColumn && c.CurrencyID == _select.CurrencyID).ToList();
            switch (cbbMethod.SelectedIndex)
            {
                //Tiền mặt
                case 0:
                    IMCReceiptService iReceiptService = IoC.Resolve<IMCReceiptService>();
                    MCReceipt mCReceipt = new MCReceipt
                    {
                        ID = Guid.NewGuid(),
                        TypeID = 101,
                        AccountingObjectName = _select.AccountingObject.AccountingObjectName,
                        AccountingObjectAddress = _select.AccountingObject.Address,
                        AccountingObjectID = _select.AccountingObject.ID,
                        CurrencyID = _select.CurrencyID,
                        Date = (DateTime)dteDate.Value,
                        ExchangeRate = _select.ExchangeRate,
                        No = txtNo.Text,
                        PostedDate = (DateTime)dtePostedDate.Value,
                        Reason = txtReasonTM.Text,
                        Payers = txtMContactNameTM.Text,
                        NumberAttach = txtNumberAttach.Text,
                        TotalAmount = (decimal)(_select.TotalAmount ?? 0),
                        TotalAmountOriginal = (decimal)(_select.TotalAmountOriginal ?? 0),
                        EmployeeID = _select.EmployeeID,
                        AccountingObjectType = 0,
                        TotalAll = (decimal)(_select.TotalAmount ?? 0),
                        TotalAllOriginal = (decimal)(_select.TotalAmountOriginal ?? 0),
                    };
                    foreach (ReceiptDebit receiptDebit in _select.ReceiptDebits.Where(c => c.CheckColumn))
                    {
                        if (receiptDebit.SoThu > receiptDebit.SoChuaThu)
                        {
                            MSG.Warning("Số thu không được lớn hơn số chưa thu");
                            return;
                        }
                        MCReceiptDetailCustomer receiptDetailCust = new MCReceiptDetailCustomer
                        {
                            //ID =  Guid.NewGuid(),
                            MCReceiptID = mCReceipt.ID,
                            CreditAccount = receiptDebit.CreditAccount,
                            VoucherDate = receiptDebit.Date,
                            VoucherNo = receiptDebit.No,
                            VoucherTypeID = receiptDebit.TypeID,
                            ReceipableAmount = receiptDebit.SoPhaiThuQD,
                            ReceipableAmountOriginal = receiptDebit.SoPhaiThu,
                            RemainingAmount = receiptDebit.SoChuaThuQD,
                            RemainingAmountOriginal = receiptDebit.SoChuaThu,
                            Amount = (decimal)receiptDebit.SoThuQD,
                            AmountOriginal = (decimal)receiptDebit.SoThu,
                            DiscountAmount = Convert.ToDecimal(receiptDebit.DiscountAmount),
                            DiscountRate = Convert.ToDecimal(receiptDebit.DiscountRate),
                            DiscountAmountOriginal = Convert.ToDecimal(receiptDebit.DiscountAmountOriginal),
                            DiscountAccount = receiptDebit.DiscountAccount,
                            AccountingObjectID = _select.AccountingObject.ID,
                            EmployeeID = _select.EmployeeID,
                            VoucherInvNo = receiptDebit.InvoiceNo,
                            SaleInvoiceID = receiptDebit.SaleInvoiceID
                        };
                        mCReceipt.MCReceiptDetailCustomers.Add(receiptDetailCust);
                    }
                    foreach (Flexaccount flexaccount in _select.ListfFlexaccounts)
                    {
                        MCReceiptDetail receiptDetail = new MCReceiptDetail
                        {
                            //ID = Guid.NewGuid(),
                            MCReceiptID = mCReceipt.ID,
                            Description = flexaccount.Description,
                            DebitAccount = flexaccount.DebitAccount,
                            CreditAccount = flexaccount.CreditAccount,
                            Amount = Convert.ToDecimal(flexaccount.TotalAmount),
                            AmountOriginal = Convert.ToDecimal(flexaccount.TotalAmountOriginal),
                            BudgetItemID =
                                flexaccount.BudgetItemID != null ? new Guid(flexaccount.BudgetItemID) : (Guid?)null,
                            CostSetID = flexaccount.CostSetID != null ? new Guid(flexaccount.CostSetID) : (Guid?)null,
                            ContractID =
                                flexaccount.EMContractID != null ? new Guid(flexaccount.EMContractID) : (Guid?)null,
                            StatisticsCodeID =
                                flexaccount.StatisticsCodeID != null
                                    ? new Guid(flexaccount.StatisticsCodeID)
                                    : (Guid?)null,
                            ExpenseItemID =
                                flexaccount.ExpenseItemID != null ? new Guid(flexaccount.ExpenseItemID) : (Guid?)null,
                            AccountingObjectID = _select.AccountingObject.ID,
                            IsIrrationalCost = flexaccount.IsIrrationalCost
                        };
                        mCReceipt.MCReceiptDetails.Add(receiptDetail);
                    }
                    try
                    {
                        //iReceiptService.BeginTran();
                        //iReceiptService.CreateNew(mCReceipt);
                        //iReceiptService.CommitTran();
                        //Utils.IGenCodeService.UpdateGenCodeForm(ConstFrm.TypeGroup_MCReceipt, mCReceipt.No, mCReceipt.ID);
                        //Utils.SaveLedger<MCReceipt>(mCReceipt);
                        //if (
                        //    MSG.Question("Đã tạo chứng từ phiếu thu số " + mCReceipt.No +
                        //                 " \nBạn có muốn xem chứng từ vừa tạo không") == DialogResult.Yes)
                        //{
                        FMCReceiptDetail frm = new FMCReceiptDetail(mCReceipt, new List<MCReceipt>() { mCReceipt }, ConstFrm.optStatusForm.Add);
                        frm.FormClosed += frm_FormClosed;
                        frm.ShowDialog(this);
                        // }
                        //else
                        //{
                        //    List<CollectionCustomer> ccu = Utils.IAccountingObjectService.GetListCustomerDebits(dteDate.DateTime);
                        //    _select = ccu.FirstOrDefault(x => x.AccountingObjectID == _select.AccountingObjectID);
                        //    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(ConstFrm.TypeGroup_MCReceipt));
                        //    InitializeGUI((_select == null ? new CollectionCustomer() : _select));
                        //}
                    }
                    catch (Exception Ex)
                    {
                        iReceiptService.RolbackTran();
                    }
                    break;
                case 1:
                    IMBDepositService iMbDepositService = IoC.Resolve<IMBDepositService>();
                    if (cbbBankAccountDetailID.Text == "" || cbbBankAccountDetailID.Text == null)
                    {
                        MSG.Warning("Không được để trống trường Nộp vào TK!");
                        cbbBankAccountDetailID.Focus();
                        return;
                    }
                    MBDeposit deposit = new MBDeposit
                    {
                        ID = Guid.NewGuid(),
                        TypeID = 161,
                        AccountingObjectName = _select.AccountingObject.AccountingObjectName,
                        AccountingObjectAddress = _select.AccountingObject.Address,
                        AccountingObjectID = _select.AccountingObject.ID,
                        CurrencyID = _select.CurrencyID,
                        Date = (DateTime)dteDate.Value,
                        ExchangeRate = _select.ExchangeRate,
                        No = txtNo.Text,
                        PostedDate = (DateTime)dtePostedDate.Value,
                        Reason = txtReasonCK.Text,
                        TotalAmount = (decimal)(_select.TotalAmount ?? 0),
                        TotalAmountOriginal = (decimal)(_select.TotalAmountOriginal ?? 0),
                        EmployeeID = _select.EmployeeID,
                        BankAccountDetailID = _select.BankAccountDetailID,
                        BankName = _select.BankName,
                        AccountingObjectType = 0,
                        TotalAll = (decimal)(_select.TotalAmount ?? 0),
                        TotalAllOriginal = (decimal)(_select.TotalAmountOriginal ?? 0),
                    };

                    foreach (ReceiptDebit receiptDebit in _select.ReceiptDebits.Where(c => c.CheckColumn))
                    {
                        if (receiptDebit.SoThu > receiptDebit.SoChuaThu)
                        {
                            MSG.Warning("Số thu không được lớn hơn số chưa thu");
                            return;
                        }
                        MBDepositDetailCustomer depositDetailCust = new MBDepositDetailCustomer
                        {
                            //ID = Guid.NewGuid(),
                            MBDepositID = deposit.ID,
                            CreditAccount = receiptDebit.CreditAccount,
                            VoucherDate = receiptDebit.Date,
                            VoucherNo = receiptDebit.No,
                            VoucherTypeID = receiptDebit.TypeID,
                            VoucherInvNo = receiptDebit.InvoiceNo,
                            ReceipableAmount = receiptDebit.SoPhaiThuQD,
                            ReceipableAmountOriginal = receiptDebit.SoPhaiThu,
                            RemainingAmount = receiptDebit.SoChuaThuQD,
                            RemainingAmountOriginal = receiptDebit.SoChuaThu,
                            Amount = (decimal)receiptDebit.SoThuQD,
                            AmountOriginal = (decimal)receiptDebit.SoThu,
                            DiscountAmount = Convert.ToDecimal(receiptDebit.DiscountAmount),
                            DiscountRate = Convert.ToDecimal(receiptDebit.DiscountRate),
                            DiscountAmountOriginal = Convert.ToDecimal(receiptDebit.DiscountAmountOriginal),
                            DiscountAccount = receiptDebit.DiscountAccount,
                            AccountingObjectID = _select.AccountingObject.ID,
                            EmployeeID = _select.EmployeeID,
                            SaleInvoiceID = receiptDebit.SaleInvoiceID
                        };
                        deposit.MBDepositDetailCustomers.Add(depositDetailCust);
                    }

                    foreach (Flexaccount flexaccount in _select.ListfFlexaccounts)
                    {
                        MBDepositDetail depositDetail = new MBDepositDetail
                        {
                            //ID = Guid.NewGuid(),
                            MBDepositID = deposit.ID,
                            Description = flexaccount.Description,
                            DebitAccount = flexaccount.DebitAccount,
                            CreditAccount = flexaccount.CreditAccount,
                            Amount = Convert.ToDecimal(flexaccount.TotalAmount),
                            AmountOriginal = Convert.ToDecimal(flexaccount.TotalAmountOriginal),
                            BudgetItemID =
                                flexaccount.BudgetItemID != null ? new Guid(flexaccount.BudgetItemID) : (Guid?)null,
                            CostSetID = flexaccount.CostSetID != null ? new Guid(flexaccount.CostSetID) : (Guid?)null,
                            ContractID =
                                flexaccount.EMContractID != null ? new Guid(flexaccount.EMContractID) : (Guid?)null,
                            StatisticsCodeID =
                                flexaccount.StatisticsCodeID != null
                                    ? new Guid(flexaccount.StatisticsCodeID)
                                    : (Guid?)null,
                            ExpenseItemID =
                                flexaccount.ExpenseItemID != null ? new Guid(flexaccount.ExpenseItemID) : (Guid?)null,
                            AccountingObjectID = _select.AccountingObject.ID,
                            IsIrrationalCost = flexaccount.IsIrrationalCost
                        };
                        deposit.MBDepositDetails.Add(depositDetail);
                    }
                    try
                    {
                        //iMbDepositService.BeginTran();
                        //iMbDepositService.CreateNew(deposit);
                        //iMbDepositService.CommitTran();
                        //Utils.IGenCodeService.UpdateGenCodeForm(16, deposit.No, deposit.ID);
                        //Utils.SaveLedger<MBDeposit>(deposit);
                        //if (MSG.Question("Đã tạo chứng từ nộp tiền vào tài khoản số " + deposit.No +
                        //                 ". \nBạn có muốn xem chứng từ vừa tạo không ?") == DialogResult.Yes)
                        //{
                        FMBDepositDetail frm = new FMBDepositDetail(deposit, new List<MBDeposit>() { deposit }, ConstFrm.optStatusForm.Add);
                        frm.FormClosed += frm_FormClosed;
                        frm.ShowDialog(this);
                        //}
                        //else
                        //{
                        //    List<CollectionCustomer> ccu = Utils.IAccountingObjectService.GetListCustomerDebits(dteDate.DateTime);
                        //    _select = ccu.FirstOrDefault(x => x.AccountingObjectID == _select.AccountingObjectID);
                        //    txtNo.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(16));
                        //    InitializeGUI(_select == null ? new CollectionCustomer() : _select);
                        //}
                    }
                    catch (Exception ex)
                    {
                        iMbDepositService.RolbackTran();
                    }
                    break;
            }
        }

        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void FSAReceiptCustomerDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        #endregion

        private void uGHoaDon_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            bool a = grid.Selected.Rows.Count > 0;
            bool b = grid.ActiveRow != null;
            if (a || b)
            {
                ReceiptDebit temp = (ReceiptDebit)(b ? grid.ActiveRow.ListObject : grid.Selected.Rows[0].ListObject);
                if (temp != null)
                    Utils.ViewVoucherSelected(temp.SaleInvoiceID, temp.TypeID);

            }
        }

        private void uGridCurrency_AfterCellUpdate(object sender, CellEventArgs e)
        {
            uGridCurrency.UpdateData();
            var need = Utils.GetValueFromTextAndNotNull(uGridCurrency.Rows[0].Cells["TotalAmountOriginal"]);
            decimal total = 0;           
            if (e.Cell.Column.Key == "TotalAmountOriginal")
            {
                if (e.Cell.Row.Cells["CurrencyID"].Value.ToString() != "VND")
                    e.Cell.Row.Cells["TotalAmount"].Value = Utils.GetValueFromTextAndNotNull(e.Cell.Row.Cells["ExchangeRate"]) * Utils.GetValueFromTextAndNotNull(e.Cell.Row.Cells["TotalAmountOriginal"]);
                if (e.Cell.Activated)
                {
                    _statusTemp = true;
                    foreach (UltraGridRow ugr in uGHoaDon.Rows)
                    {
                        if (total < need)
                        {
                            decimal temp = Convert.ToDecimal(ugr.Cells["SoChuaThu"].Value.ToString());
                            if ((temp + total) <= need)
                            {
                                ugr.Cells["CheckColumn"].Value = true;
                                ugr.Cells["SoThu"].Value = ugr.Cells["SoChuaThu"].Value;
                            }
                            else
                            {
                                ugr.Cells["CheckColumn"].Value = true;
                                ugr.Cells["SoThu"].Value = need - total;
                            }
                        }
                        else
                        {
                            ugr.Cells["CheckColumn"].Value = false;
                            ugr.Cells["SoThu"].Value = 0M;
                        }
                        total += Convert.ToDecimal(ugr.Cells["SoThu"].Value.ToString());
                        ugr.Cells["SoThuQD"].Value = Convert.ToDecimal(ugr.Cells["SoThu"].Value.ToString()) * Convert.ToDecimal(ugr.Cells["RefVoucherExchangeRate"].Value.ToString());

                    }
                    uGHoaDon.Rows[0].Cells["DifferAmount"].Value = Math.Round((((decimal)uGHoaDon.Rows[0].Cells["SoThu"].Value * _select.ExchangeRate) ?? 0) - ((decimal)uGHoaDon.Rows[0].Cells["LastExchangeRate"].Value == 0 ?
                    ((decimal)uGHoaDon.Rows[0].Cells["SoThu"].Value * (decimal)uGHoaDon.Rows[0].Cells["RefVoucherExchangeRate"].Value) : ((decimal)uGHoaDon.Rows[0].Cells["SoThu"].Value * (decimal)uGHoaDon.Rows[0].Cells["LastExchangeRate"].Value)), LamTron, MidpointRounding.AwayFromZero);
                }
            }

        }

        private void uGHachToan_BeforeRowActivate(object sender, RowEventArgs e)
        {
            // add by tungnt: copy dữ liệu ở dòng trên xuống dòng dưới
            UltraGridRow row = e.Row;
            int index = row.Index;
            if (index > 0)
            {
                foreach (var cell in row.Cells)
                {
                    if (cell.Column.Key.Contains("TotalAmountOriginal") || cell.Column.Key.Contains("CreditAccount") || cell.Column.Key.Contains("DebitAccount") || cell.Column.Key.Contains("AccountingObjectCode") || cell.Column.Key.Contains("Description"))
                        if (Utils.GetIsCopyRow())
                            if (cell.Value != null && cell.Value.ToString() != "") { }
                            else
                                cell.Value = uGHachToan.Rows[index - 1].Cells[cell.Column.Key].Value;

                }
                uGHachToan.Rows[index].UpdateData();
            }
        }

        private void uGHachToan_AfterCellUpdate(object sender, CellEventArgs e)
        {
            // add by tungnt: tính lại tiền quy đổi
            if (e.Cell.Column.Key == "TotalAmountOriginal")
            {
                if (uGridCurrency.Rows[0].Cells["CurrencyID"].Value.ToString() != "VND")
                    e.Cell.Row.Cells["TotalAmount"].Value = Convert.ToDecimal(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) * Utils.GetValueFromTextAndNotNull(uGridCurrency.Rows[0].Cells["ExchangeRate"]);
            }
        }

        private void cmbtnAddRow_Click(object sender, EventArgs e)
        {
            // tungnt: thêm dòng bằng cms
            Utils.AddNewRow4Grid(uGHachToan);
        }

        private void cmbtnDelRow_Click(object sender, EventArgs e)
        {
            // tungnt: xóa dòng bằng cms
            this.RemoveRow4Grid<FSAReceiptCustomerDetail>(uGHachToan);
        }

        private void txtAccountingObjectNameTM_TextChanged(object sender, EventArgs e)
        {
            UltraTextEditor txt = (UltraTextEditor)sender;
            txt.Text = txt.Text.Replace('\n', ' ');
        }
    }
}