﻿namespace Accounting
{
    partial class FSAReceiptCustomerDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGHoaDon = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGHachToan = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmbtnAddRow = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbtnDelRow = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbMethod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.lblThanhToan = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtePostedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblPostedDate = new Infragistics.Win.Misc.UltraLabel();
            this.txtNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblNo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.pnChuyenKhoan = new Infragistics.Win.Misc.UltraPanel();
            this.gbCK = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbBankAccountDetailID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtReasonCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMContactNameCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameCK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDCK = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.pnTienMat = new Infragistics.Win.Misc.UltraPanel();
            this.gbTM = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNumberAttach = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtReasonTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtMContactNameTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAddressTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectNameTM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectIDTM = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblKT = new Infragistics.Win.Misc.UltraLabel();
            this.lblLD = new Infragistics.Win.Misc.UltraLabel();
            this.lblNN = new Infragistics.Win.Misc.UltraLabel();
            this.lblDC = new Infragistics.Win.Misc.UltraLabel();
            this.lblDT = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.btnThucHien = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridCurrency = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGHoaDon)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGHachToan)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.pnChuyenKhoan.ClientArea.SuspendLayout();
            this.pnChuyenKhoan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbCK)).BeginInit();
            this.gbCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDCK)).BeginInit();
            this.pnTienMat.ClientArea.SuspendLayout();
            this.pnTienMat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbTM)).BeginInit();
            this.gbTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCurrency)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGHoaDon);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1015, 163);
            // 
            // uGHoaDon
            // 
            this.uGHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGHoaDon.Location = new System.Drawing.Point(0, 0);
            this.uGHoaDon.Name = "uGHoaDon";
            this.uGHoaDon.Size = new System.Drawing.Size(1015, 163);
            this.uGHoaDon.TabIndex = 0;
            this.uGHoaDon.Text = "ultraGrid1";
            this.uGHoaDon.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGHoaDon_DoubleClickRow);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGHachToan);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1015, 163);
            // 
            // uGHachToan
            // 
            this.uGHachToan.ContextMenuStrip = this.cms4Grid;
            this.uGHachToan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGHachToan.Location = new System.Drawing.Point(0, 0);
            this.uGHachToan.Name = "uGHachToan";
            this.uGHachToan.Size = new System.Drawing.Size(1015, 163);
            this.uGHachToan.TabIndex = 0;
            this.uGHachToan.Text = "ultraGrid2";
            this.uGHachToan.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGHachToan_AfterCellUpdate);
            this.uGHachToan.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGHachToan_BeforeRowActivate);
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(15, 15);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbtnAddRow,
            this.cmbtnDelRow});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(248, 48);
            // 
            // cmbtnAddRow
            // 
            this.cmbtnAddRow.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.cmbtnAddRow.Name = "cmbtnAddRow";
            this.cmbtnAddRow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Insert)));
            this.cmbtnAddRow.ShowShortcutKeys = false;
            this.cmbtnAddRow.Size = new System.Drawing.Size(247, 22);
            this.cmbtnAddRow.Text = "Thêm dòng                 Ctrl + Insert";
            this.cmbtnAddRow.Click += new System.EventHandler(this.cmbtnAddRow_Click);
            // 
            // cmbtnDelRow
            // 
            this.cmbtnDelRow.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.cmbtnDelRow.Name = "cmbtnDelRow";
            this.cmbtnDelRow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.cmbtnDelRow.ShowShortcutKeys = false;
            this.cmbtnDelRow.Size = new System.Drawing.Size(247, 22);
            this.cmbtnDelRow.Text = "Xóa dòng                     Ctrl + Delete";
            this.cmbtnDelRow.Click += new System.EventHandler(this.cmbtnDelRow_Click);
            // 
            // ultraGroupBox1
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.cbbMethod);
            this.ultraGroupBox1.Controls.Add(this.lblThanhToan);
            this.ultraGroupBox1.Controls.Add(this.dteDate);
            this.ultraGroupBox1.Controls.Add(this.dtePostedDate);
            this.ultraGroupBox1.Controls.Add(this.lblDate);
            this.ultraGroupBox1.Controls.Add(this.lblPostedDate);
            this.ultraGroupBox1.Controls.Add(this.txtNo);
            this.ultraGroupBox1.Controls.Add(this.lblNo);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance8.FontData.BoldAsString = "True";
            appearance8.FontData.SizeInPoints = 13F;
            this.ultraGroupBox1.HeaderAppearance = appearance8;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1017, 74);
            this.ultraGroupBox1.TabIndex = 31;
            this.ultraGroupBox1.Text = "Thu Tiền Khách Hàng";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbMethod
            // 
            this.cbbMethod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbMethod.AutoSize = false;
            this.cbbMethod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.cbbMethod.Location = new System.Drawing.Point(889, 28);
            this.cbbMethod.Name = "cbbMethod";
            this.cbbMethod.Size = new System.Drawing.Size(116, 21);
            this.cbbMethod.TabIndex = 36;
            // 
            // lblThanhToan
            // 
            this.lblThanhToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThanhToan.AutoSize = true;
            this.lblThanhToan.Location = new System.Drawing.Point(784, 32);
            this.lblThanhToan.Name = "lblThanhToan";
            this.lblThanhToan.Size = new System.Drawing.Size(108, 14);
            this.lblThanhToan.TabIndex = 34;
            this.lblThanhToan.Text = "Hình thức thanh toán";
            // 
            // dteDate
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance2;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(201, 48);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(100, 21);
            this.dteDate.TabIndex = 33;
            this.dteDate.Value = null;
            // 
            // dtePostedDate
            // 
            appearance3.TextHAlignAsString = "Center";
            appearance3.TextVAlignAsString = "Middle";
            this.dtePostedDate.Appearance = appearance3;
            this.dtePostedDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtePostedDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtePostedDate.Location = new System.Drawing.Point(103, 48);
            this.dtePostedDate.MaskInput = "";
            this.dtePostedDate.Name = "dtePostedDate";
            this.dtePostedDate.Size = new System.Drawing.Size(99, 21);
            this.dtePostedDate.TabIndex = 32;
            this.dtePostedDate.Value = null;
            this.dtePostedDate.ValueChanged += new System.EventHandler(this.dtePostedDate_ValueChanged);
            // 
            // lblDate
            // 
            appearance4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.White;
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.lblDate.Appearance = appearance4;
            this.lblDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblDate.Location = new System.Drawing.Point(201, 24);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(100, 24);
            this.lblDate.TabIndex = 31;
            this.lblDate.Text = "Ngày chứng từ";
            // 
            // lblPostedDate
            // 
            appearance5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance5.BackColor2 = System.Drawing.Color.Transparent;
            appearance5.ForeColor = System.Drawing.Color.White;
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.lblPostedDate.Appearance = appearance5;
            this.lblPostedDate.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblPostedDate.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblPostedDate.Location = new System.Drawing.Point(103, 24);
            this.lblPostedDate.Name = "lblPostedDate";
            this.lblPostedDate.Size = new System.Drawing.Size(100, 24);
            this.lblPostedDate.TabIndex = 30;
            this.lblPostedDate.Text = "Ngày hạch toán";
            // 
            // txtNo
            // 
            appearance6.TextHAlignAsString = "Center";
            this.txtNo.Appearance = appearance6;
            this.txtNo.Location = new System.Drawing.Point(5, 48);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(99, 21);
            this.txtNo.TabIndex = 29;
            // 
            // lblNo
            // 
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(104)))), ((int)(((byte)(189)))));
            appearance7.BackColor2 = System.Drawing.Color.Transparent;
            appearance7.ForeColor = System.Drawing.Color.White;
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Middle";
            this.lblNo.Appearance = appearance7;
            this.lblNo.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.lblNo.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.lblNo.Location = new System.Drawing.Point(5, 24);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(100, 24);
            this.lblNo.TabIndex = 28;
            this.lblNo.Text = "Số chứng từ";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.pnChuyenKhoan);
            this.ultraGroupBox3.Controls.Add(this.pnTienMat);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 74);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1017, 167);
            this.ultraGroupBox3.TabIndex = 34;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // pnChuyenKhoan
            // 
            // 
            // pnChuyenKhoan.ClientArea
            // 
            this.pnChuyenKhoan.ClientArea.Controls.Add(this.gbCK);
            this.pnChuyenKhoan.Location = new System.Drawing.Point(482, 7);
            this.pnChuyenKhoan.Name = "pnChuyenKhoan";
            this.pnChuyenKhoan.Size = new System.Drawing.Size(523, 159);
            this.pnChuyenKhoan.TabIndex = 1;
            // 
            // gbCK
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.gbCK.Appearance = appearance9;
            this.gbCK.Controls.Add(this.txtBankName);
            this.gbCK.Controls.Add(this.cbbBankAccountDetailID);
            this.gbCK.Controls.Add(this.txtReasonCK);
            this.gbCK.Controls.Add(this.txtMContactNameCK);
            this.gbCK.Controls.Add(this.txtAddress);
            this.gbCK.Controls.Add(this.txtAccountingObjectNameCK);
            this.gbCK.Controls.Add(this.cbbAccountingObjectIDCK);
            this.gbCK.Controls.Add(this.ultraLabel3);
            this.gbCK.Controls.Add(this.ultraLabel4);
            this.gbCK.Controls.Add(this.ultraLabel5);
            this.gbCK.Controls.Add(this.ultraLabel6);
            this.gbCK.Controls.Add(this.ultraLabel7);
            this.gbCK.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance39.FontData.BoldAsString = "True";
            appearance39.FontData.SizeInPoints = 10F;
            this.gbCK.HeaderAppearance = appearance39;
            this.gbCK.Location = new System.Drawing.Point(0, 0);
            this.gbCK.Name = "gbCK";
            this.gbCK.Size = new System.Drawing.Size(523, 159);
            this.gbCK.TabIndex = 34;
            this.gbCK.Text = "Thông tin chung";
            this.gbCK.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtBankName
            // 
            this.txtBankName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBankName.AutoSize = false;
            this.txtBankName.Location = new System.Drawing.Point(239, 97);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(272, 22);
            this.txtBankName.TabIndex = 13;
            // 
            // cbbBankAccountDetailID
            // 
            this.cbbBankAccountDetailID.AutoSize = false;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbBankAccountDetailID.DisplayLayout.Appearance = appearance10;
            this.cbbBankAccountDetailID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbBankAccountDetailID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.Appearance = appearance11;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance12;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance13.BackColor2 = System.Drawing.SystemColors.Control;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbBankAccountDetailID.DisplayLayout.GroupByBox.PromptAppearance = appearance13;
            this.cbbBankAccountDetailID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbBankAccountDetailID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance15.BackColor = System.Drawing.SystemColors.Highlight;
            appearance15.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.ActiveRowAppearance = appearance15;
            this.cbbBankAccountDetailID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbBankAccountDetailID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CardAreaAppearance = appearance16;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellAppearance = appearance17;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbBankAccountDetailID.DisplayLayout.Override.CellPadding = 0;
            appearance18.BackColor = System.Drawing.SystemColors.Control;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbBankAccountDetailID.DisplayLayout.Override.GroupByRowAppearance = appearance18;
            appearance19.TextHAlignAsString = "Left";
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderAppearance = appearance19;
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbBankAccountDetailID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            this.cbbBankAccountDetailID.DisplayLayout.Override.RowAppearance = appearance20;
            this.cbbBankAccountDetailID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbBankAccountDetailID.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.cbbBankAccountDetailID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbBankAccountDetailID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbBankAccountDetailID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbBankAccountDetailID.Location = new System.Drawing.Point(88, 97);
            this.cbbBankAccountDetailID.Name = "cbbBankAccountDetailID";
            this.cbbBankAccountDetailID.Size = new System.Drawing.Size(145, 22);
            this.cbbBankAccountDetailID.TabIndex = 12;
            // 
            // txtReasonCK
            // 
            this.txtReasonCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonCK.Location = new System.Drawing.Point(88, 121);
            this.txtReasonCK.Multiline = true;
            this.txtReasonCK.Name = "txtReasonCK";
            this.txtReasonCK.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReasonCK.Size = new System.Drawing.Size(423, 31);
            this.txtReasonCK.TabIndex = 10;
            // 
            // txtMContactNameCK
            // 
            this.txtMContactNameCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMContactNameCK.AutoSize = false;
            this.txtMContactNameCK.Location = new System.Drawing.Point(88, 73);
            this.txtMContactNameCK.Name = "txtMContactNameCK";
            this.txtMContactNameCK.Size = new System.Drawing.Size(423, 22);
            this.txtMContactNameCK.TabIndex = 8;
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddress.AutoSize = false;
            this.txtAddress.Location = new System.Drawing.Point(88, 49);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(423, 22);
            this.txtAddress.TabIndex = 7;
            this.txtAddress.TextChanged += new System.EventHandler(this.txtAccountingObjectNameTM_TextChanged);
            // 
            // txtAccountingObjectNameCK
            // 
            this.txtAccountingObjectNameCK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameCK.AutoSize = false;
            this.txtAccountingObjectNameCK.Location = new System.Drawing.Point(239, 25);
            this.txtAccountingObjectNameCK.Name = "txtAccountingObjectNameCK";
            this.txtAccountingObjectNameCK.Size = new System.Drawing.Size(272, 22);
            this.txtAccountingObjectNameCK.TabIndex = 6;
            this.txtAccountingObjectNameCK.TextChanged += new System.EventHandler(this.txtAccountingObjectNameTM_TextChanged);
            // 
            // cbbAccountingObjectIDCK
            // 
            this.cbbAccountingObjectIDCK.AutoSize = false;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectIDCK.DisplayLayout.Appearance = appearance22;
            this.cbbAccountingObjectIDCK.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectIDCK.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectIDCK.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectIDCK.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.cbbAccountingObjectIDCK.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectIDCK.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.cbbAccountingObjectIDCK.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectIDCK.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.CellAppearance = appearance29;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.RowAppearance = appearance32;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectIDCK.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.cbbAccountingObjectIDCK.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectIDCK.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectIDCK.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectIDCK.Location = new System.Drawing.Point(88, 25);
            this.cbbAccountingObjectIDCK.Name = "cbbAccountingObjectIDCK";
            this.cbbAccountingObjectIDCK.Size = new System.Drawing.Size(145, 22);
            this.cbbAccountingObjectIDCK.TabIndex = 5;
            // 
            // ultraLabel3
            // 
            appearance34.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance34;
            this.ultraLabel3.Location = new System.Drawing.Point(7, 121);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "Diễn giải";
            // 
            // ultraLabel4
            // 
            appearance35.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance35;
            this.ultraLabel4.Location = new System.Drawing.Point(7, 97);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(77, 22);
            this.ultraLabel4.TabIndex = 3;
            this.ultraLabel4.Text = "Nộp vào TK";
            // 
            // ultraLabel5
            // 
            appearance36.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance36;
            this.ultraLabel5.Location = new System.Drawing.Point(7, 73);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel5.TabIndex = 2;
            this.ultraLabel5.Text = "Người Nộp";
            // 
            // ultraLabel6
            // 
            appearance37.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance37;
            this.ultraLabel6.Location = new System.Drawing.Point(7, 49);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(76, 22);
            this.ultraLabel6.TabIndex = 1;
            this.ultraLabel6.Text = "Địa Chỉ";
            // 
            // ultraLabel7
            // 
            appearance38.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance38;
            this.ultraLabel7.Location = new System.Drawing.Point(7, 25);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel7.TabIndex = 0;
            this.ultraLabel7.Text = "Đối Tượng";
            // 
            // pnTienMat
            // 
            // 
            // pnTienMat.ClientArea
            // 
            this.pnTienMat.ClientArea.Controls.Add(this.gbTM);
            this.pnTienMat.Location = new System.Drawing.Point(12, 6);
            this.pnTienMat.Name = "pnTienMat";
            this.pnTienMat.Size = new System.Drawing.Size(464, 160);
            this.pnTienMat.TabIndex = 0;
            // 
            // gbTM
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            this.gbTM.Appearance = appearance40;
            this.gbTM.Controls.Add(this.ultraLabel1);
            this.gbTM.Controls.Add(this.txtNumberAttach);
            this.gbTM.Controls.Add(this.txtReasonTM);
            this.gbTM.Controls.Add(this.txtMContactNameTM);
            this.gbTM.Controls.Add(this.txtAddressTM);
            this.gbTM.Controls.Add(this.txtAccountingObjectNameTM);
            this.gbTM.Controls.Add(this.cbbAccountingObjectIDTM);
            this.gbTM.Controls.Add(this.lblKT);
            this.gbTM.Controls.Add(this.lblLD);
            this.gbTM.Controls.Add(this.lblNN);
            this.gbTM.Controls.Add(this.lblDC);
            this.gbTM.Controls.Add(this.lblDT);
            this.gbTM.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance59.FontData.BoldAsString = "True";
            appearance59.FontData.SizeInPoints = 10F;
            this.gbTM.HeaderAppearance = appearance59;
            this.gbTM.Location = new System.Drawing.Point(0, 0);
            this.gbTM.Name = "gbTM";
            this.gbTM.Size = new System.Drawing.Size(464, 160);
            this.gbTM.TabIndex = 14;
            this.gbTM.Text = "Thông tin chung";
            this.gbTM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance41.TextHAlignAsString = "Center";
            appearance41.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance41;
            this.ultraLabel1.Location = new System.Drawing.Point(365, 121);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(86, 22);
            this.ultraLabel1.TabIndex = 11;
            this.ultraLabel1.Text = "Chứng từ gốc";
            // 
            // txtNumberAttach
            // 
            this.txtNumberAttach.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumberAttach.AutoSize = false;
            this.txtNumberAttach.Location = new System.Drawing.Point(89, 121);
            this.txtNumberAttach.Name = "txtNumberAttach";
            this.txtNumberAttach.Size = new System.Drawing.Size(270, 22);
            this.txtNumberAttach.TabIndex = 10;
            // 
            // txtReasonTM
            // 
            this.txtReasonTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReasonTM.AutoSize = false;
            this.txtReasonTM.Location = new System.Drawing.Point(89, 97);
            this.txtReasonTM.Name = "txtReasonTM";
            this.txtReasonTM.Size = new System.Drawing.Size(362, 22);
            this.txtReasonTM.TabIndex = 9;
            // 
            // txtMContactNameTM
            // 
            this.txtMContactNameTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMContactNameTM.AutoSize = false;
            this.txtMContactNameTM.Location = new System.Drawing.Point(89, 73);
            this.txtMContactNameTM.Name = "txtMContactNameTM";
            this.txtMContactNameTM.Size = new System.Drawing.Size(362, 22);
            this.txtMContactNameTM.TabIndex = 8;
            // 
            // txtAddressTM
            // 
            this.txtAddressTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddressTM.AutoSize = false;
            this.txtAddressTM.Location = new System.Drawing.Point(89, 49);
            this.txtAddressTM.Name = "txtAddressTM";
            this.txtAddressTM.Size = new System.Drawing.Size(362, 22);
            this.txtAddressTM.TabIndex = 7;
            this.txtAddressTM.TextChanged += new System.EventHandler(this.txtAccountingObjectNameTM_TextChanged);
            // 
            // txtAccountingObjectNameTM
            // 
            this.txtAccountingObjectNameTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountingObjectNameTM.AutoSize = false;
            this.txtAccountingObjectNameTM.Location = new System.Drawing.Point(239, 25);
            this.txtAccountingObjectNameTM.Name = "txtAccountingObjectNameTM";
            this.txtAccountingObjectNameTM.Size = new System.Drawing.Size(212, 22);
            this.txtAccountingObjectNameTM.TabIndex = 6;
            this.txtAccountingObjectNameTM.TextChanged += new System.EventHandler(this.txtAccountingObjectNameTM_TextChanged);
            // 
            // cbbAccountingObjectIDTM
            // 
            this.cbbAccountingObjectIDTM.AutoSize = false;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectIDTM.DisplayLayout.Appearance = appearance42;
            this.cbbAccountingObjectIDTM.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectIDTM.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectIDTM.DisplayLayout.GroupByBox.Appearance = appearance43;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectIDTM.DisplayLayout.GroupByBox.BandLabelAppearance = appearance44;
            this.cbbAccountingObjectIDTM.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance45.BackColor2 = System.Drawing.SystemColors.Control;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectIDTM.DisplayLayout.GroupByBox.PromptAppearance = appearance45;
            this.cbbAccountingObjectIDTM.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectIDTM.DisplayLayout.MaxRowScrollRegions = 1;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.ActiveCellAppearance = appearance46;
            appearance47.BackColor = System.Drawing.SystemColors.Highlight;
            appearance47.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.ActiveRowAppearance = appearance47;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            appearance49.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.CellAppearance = appearance49;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.CellPadding = 0;
            appearance50.BackColor = System.Drawing.SystemColors.Control;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.GroupByRowAppearance = appearance50;
            appearance51.TextHAlignAsString = "Left";
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.HeaderAppearance = appearance51;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.RowAppearance = appearance52;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance53.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectIDTM.DisplayLayout.Override.TemplateAddRowAppearance = appearance53;
            this.cbbAccountingObjectIDTM.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectIDTM.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectIDTM.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectIDTM.Location = new System.Drawing.Point(89, 25);
            this.cbbAccountingObjectIDTM.Name = "cbbAccountingObjectIDTM";
            this.cbbAccountingObjectIDTM.Size = new System.Drawing.Size(144, 22);
            this.cbbAccountingObjectIDTM.TabIndex = 5;
            // 
            // lblKT
            // 
            appearance54.TextVAlignAsString = "Middle";
            this.lblKT.Appearance = appearance54;
            this.lblKT.Location = new System.Drawing.Point(8, 121);
            this.lblKT.Name = "lblKT";
            this.lblKT.Size = new System.Drawing.Size(78, 22);
            this.lblKT.TabIndex = 4;
            this.lblKT.Text = "Kèm Theo";
            // 
            // lblLD
            // 
            appearance55.TextVAlignAsString = "Middle";
            this.lblLD.Appearance = appearance55;
            this.lblLD.Location = new System.Drawing.Point(7, 97);
            this.lblLD.Name = "lblLD";
            this.lblLD.Size = new System.Drawing.Size(79, 22);
            this.lblLD.TabIndex = 3;
            this.lblLD.Text = "Lý Do Nộp";
            // 
            // lblNN
            // 
            appearance56.TextVAlignAsString = "Middle";
            this.lblNN.Appearance = appearance56;
            this.lblNN.Location = new System.Drawing.Point(8, 73);
            this.lblNN.Name = "lblNN";
            this.lblNN.Size = new System.Drawing.Size(79, 22);
            this.lblNN.TabIndex = 2;
            this.lblNN.Text = "Người Nộp";
            // 
            // lblDC
            // 
            appearance57.TextVAlignAsString = "Middle";
            this.lblDC.Appearance = appearance57;
            this.lblDC.Location = new System.Drawing.Point(9, 49);
            this.lblDC.Name = "lblDC";
            this.lblDC.Size = new System.Drawing.Size(77, 22);
            this.lblDC.TabIndex = 1;
            this.lblDC.Text = "Địa Chỉ";
            // 
            // lblDT
            // 
            appearance58.TextVAlignAsString = "Middle";
            this.lblDT.Appearance = appearance58;
            this.lblDT.Location = new System.Drawing.Point(10, 25);
            this.lblDT.Name = "lblDT";
            this.lblDT.Size = new System.Drawing.Size(78, 22);
            this.lblDT.TabIndex = 0;
            this.lblDT.Text = "Đối Tượng";
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 251);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1017, 186);
            this.ultraTabControl1.TabIndex = 36;
            ultraTab3.TabPage = this.ultraTabPageControl1;
            ultraTab3.Text = "Hóa đơn";
            ultraTab4.TabPage = this.ultraTabPageControl2;
            ultraTab4.Text = "Hạch toán";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab4});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1015, 163);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraPanel2);
            this.ultraGroupBox4.Controls.Add(this.ultraPanel1);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 437);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(1017, 95);
            this.ultraGroupBox4.TabIndex = 37;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.btnThucHien);
            this.ultraPanel2.ClientArea.Controls.Add(this.btnCancel);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Location = new System.Drawing.Point(3, 49);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(1011, 43);
            this.ultraPanel2.TabIndex = 3;
            // 
            // btnThucHien
            // 
            this.btnThucHien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance60.Image = global::Accounting.Properties.Resources.apply_16;
            this.btnThucHien.Appearance = appearance60;
            this.btnThucHien.Location = new System.Drawing.Point(829, 6);
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Size = new System.Drawing.Size(90, 33);
            this.btnThucHien.TabIndex = 0;
            this.btnThucHien.Text = "Thực hiện";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance61.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnCancel.Appearance = appearance61;
            this.btnCancel.Location = new System.Drawing.Point(924, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 33);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Hủy Bỏ";
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.AutoSize = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.uGridCurrency);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(3, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1011, 49);
            this.ultraPanel1.TabIndex = 2;
            // 
            // uGridCurrency
            // 
            this.uGridCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uGridCurrency.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.uGridCurrency.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.uGridCurrency.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.uGridCurrency.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.False;
            this.uGridCurrency.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.Text;
            appearance62.TextHAlignAsString = "Right";
            this.uGridCurrency.DisplayLayout.Override.RowAppearance = appearance62;
            this.uGridCurrency.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCurrency.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridCurrency.Location = new System.Drawing.Point(791, 2);
            this.uGridCurrency.Name = "uGridCurrency";
            this.uGridCurrency.Size = new System.Drawing.Size(218, 44);
            this.uGridCurrency.TabIndex = 2;
            this.uGridCurrency.Text = "ultraGrid1";
            this.uGridCurrency.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCurrency_AfterCellUpdate);
            this.uGridCurrency.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridCurrency_Error);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.AutoPopDelay = 10000;
            this.ultraToolTipManager1.ContainingControl = this;
            this.ultraToolTipManager1.InitialDelay = 2000;
            this.ultraToolTipManager1.ToolTipImage = Infragistics.Win.ToolTipImage.Error;
            appearance63.BackColor = System.Drawing.Color.Red;
            this.ultraToolTipManager1.ToolTipTitleAppearance = appearance63;
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 241);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 0;
            this.ultraSplitter1.Size = new System.Drawing.Size(1017, 10);
            this.ultraSplitter1.TabIndex = 12;
            // 
            // FSAReceiptCustomerDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 532);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraGroupBox4);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSAReceiptCustomerDetail";
            this.Text = "Thu tiền khách hàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSAReceiptCustomerDetail_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGHoaDon)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGHachToan)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePostedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.pnChuyenKhoan.ClientArea.ResumeLayout(false);
            this.pnChuyenKhoan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbCK)).EndInit();
            this.gbCK.ResumeLayout(false);
            this.gbCK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbBankAccountDetailID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDCK)).EndInit();
            this.pnTienMat.ClientArea.ResumeLayout(false);
            this.pnTienMat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbTM)).EndInit();
            this.gbTM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumberAttach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReasonTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMContactNameTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddressTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectNameTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectIDTM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCurrency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtePostedDate;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.Misc.UltraLabel lblPostedDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNo;
        private Infragistics.Win.Misc.UltraLabel lblNo;
        private Infragistics.Win.Misc.UltraLabel lblThanhToan;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor cbbMethod;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnThucHien;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraPanel pnTienMat;
        private Infragistics.Win.Misc.UltraPanel pnChuyenKhoan;
        private Infragistics.Win.Misc.UltraGroupBox gbTM;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNumberAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonTM;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMContactNameTM;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddressTM;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameTM;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDTM;
        private Infragistics.Win.Misc.UltraLabel lblKT;
        private Infragistics.Win.Misc.UltraLabel lblLD;
        private Infragistics.Win.Misc.UltraLabel lblNN;
        private Infragistics.Win.Misc.UltraLabel lblDC;
        private Infragistics.Win.Misc.UltraLabel lblDT;
        private Infragistics.Win.Misc.UltraGroupBox gbCK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtBankName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbBankAccountDetailID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtReasonCK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtMContactNameCK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectNameCK;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectIDCK;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCurrency;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGHoaDon;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGHachToan;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        public System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripMenuItem cmbtnAddRow;
        private System.Windows.Forms.ToolStripMenuItem cmbtnDelRow;
    }
}