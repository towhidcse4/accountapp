﻿namespace Accounting
{
    partial class FSAReceiptCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.txtSoDaThu = new System.Windows.Forms.Label();
            this.txtSoConPhaiThu = new System.Windows.Forms.Label();
            this.txtSoPhatSinh = new System.Windows.Forms.Label();
            this.txtSoDuDauNam = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.Label();
            this.lblSoConPhaiThu = new System.Windows.Forms.Label();
            this.lblSoDaThu = new System.Windows.Forms.Label();
            this.lblSoPhatSinh = new System.Windows.Forms.Label();
            this.lblSoDuDauNam = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridCT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.uGridIndex = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLoc = new Infragistics.Win.Misc.UltraButton();
            this.lblBeginDate = new Infragistics.Win.Misc.UltraLabel();
            this.lblEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblKyKeToan = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnRefresh = new Infragistics.Win.Misc.UltraButton();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.tabInfomation = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabInfomation)).BeginInit();
            this.tabInfomation.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.txtSoDaThu);
            this.ultraTabPageControl1.Controls.Add(this.txtSoConPhaiThu);
            this.ultraTabPageControl1.Controls.Add(this.txtSoPhatSinh);
            this.ultraTabPageControl1.Controls.Add(this.txtSoDuDauNam);
            this.ultraTabPageControl1.Controls.Add(this.txtEmail);
            this.ultraTabPageControl1.Controls.Add(this.txtPhone);
            this.ultraTabPageControl1.Controls.Add(this.txtAddress);
            this.ultraTabPageControl1.Controls.Add(this.txtCustomerName);
            this.ultraTabPageControl1.Controls.Add(this.txtCustomerCode);
            this.ultraTabPageControl1.Controls.Add(this.lblSoConPhaiThu);
            this.ultraTabPageControl1.Controls.Add(this.lblSoDaThu);
            this.ultraTabPageControl1.Controls.Add(this.lblSoPhatSinh);
            this.ultraTabPageControl1.Controls.Add(this.lblSoDuDauNam);
            this.ultraTabPageControl1.Controls.Add(this.lblEmail);
            this.ultraTabPageControl1.Controls.Add(this.lblPhone);
            this.ultraTabPageControl1.Controls.Add(this.lblAddress);
            this.ultraTabPageControl1.Controls.Add(this.lblCustomerName);
            this.ultraTabPageControl1.Controls.Add(this.lblCustomerCode);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(855, 230);
            // 
            // txtSoDaThu
            // 
            this.txtSoDaThu.BackColor = System.Drawing.Color.Transparent;
            this.txtSoDaThu.Location = new System.Drawing.Point(112, 166);
            this.txtSoDaThu.Name = "txtSoDaThu";
            this.txtSoDaThu.Size = new System.Drawing.Size(721, 21);
            this.txtSoDaThu.TabIndex = 19;
            this.txtSoDaThu.Text = ":";
            this.txtSoDaThu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSoConPhaiThu
            // 
            this.txtSoConPhaiThu.BackColor = System.Drawing.Color.Transparent;
            this.txtSoConPhaiThu.Location = new System.Drawing.Point(112, 189);
            this.txtSoConPhaiThu.Name = "txtSoConPhaiThu";
            this.txtSoConPhaiThu.Size = new System.Drawing.Size(721, 21);
            this.txtSoConPhaiThu.TabIndex = 18;
            this.txtSoConPhaiThu.Text = ":";
            this.txtSoConPhaiThu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSoPhatSinh
            // 
            this.txtSoPhatSinh.BackColor = System.Drawing.Color.Transparent;
            this.txtSoPhatSinh.Location = new System.Drawing.Point(112, 143);
            this.txtSoPhatSinh.Name = "txtSoPhatSinh";
            this.txtSoPhatSinh.Size = new System.Drawing.Size(721, 21);
            this.txtSoPhatSinh.TabIndex = 17;
            this.txtSoPhatSinh.Text = ":";
            this.txtSoPhatSinh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSoDuDauNam
            // 
            this.txtSoDuDauNam.BackColor = System.Drawing.Color.Transparent;
            this.txtSoDuDauNam.Location = new System.Drawing.Point(112, 120);
            this.txtSoDuDauNam.Name = "txtSoDuDauNam";
            this.txtSoDuDauNam.Size = new System.Drawing.Size(721, 21);
            this.txtSoDuDauNam.TabIndex = 16;
            this.txtSoDuDauNam.Text = ":";
            this.txtSoDuDauNam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Transparent;
            this.txtEmail.Location = new System.Drawing.Point(112, 97);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(721, 21);
            this.txtEmail.TabIndex = 15;
            this.txtEmail.Text = ":";
            this.txtEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.Color.Transparent;
            this.txtPhone.Location = new System.Drawing.Point(112, 74);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(721, 21);
            this.txtPhone.TabIndex = 14;
            this.txtPhone.Text = ":";
            this.txtPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.Transparent;
            this.txtAddress.Location = new System.Drawing.Point(112, 51);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(721, 21);
            this.txtAddress.TabIndex = 13;
            this.txtAddress.Text = ":";
            this.txtAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomerName.Location = new System.Drawing.Point(112, 28);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(721, 21);
            this.txtCustomerName.TabIndex = 12;
            this.txtCustomerName.Text = ":";
            this.txtCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomerCode.Location = new System.Drawing.Point(112, 5);
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.Size = new System.Drawing.Size(721, 21);
            this.txtCustomerCode.TabIndex = 11;
            this.txtCustomerCode.Text = ":";
            this.txtCustomerCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSoConPhaiThu
            // 
            this.lblSoConPhaiThu.BackColor = System.Drawing.Color.Transparent;
            this.lblSoConPhaiThu.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSoConPhaiThu.Location = new System.Drawing.Point(5, 189);
            this.lblSoConPhaiThu.Name = "lblSoConPhaiThu";
            this.lblSoConPhaiThu.Size = new System.Drawing.Size(82, 21);
            this.lblSoConPhaiThu.TabIndex = 8;
            this.lblSoConPhaiThu.Text = "Số còn phải thu";
            this.lblSoConPhaiThu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSoDaThu
            // 
            this.lblSoDaThu.BackColor = System.Drawing.Color.Transparent;
            this.lblSoDaThu.Location = new System.Drawing.Point(5, 166);
            this.lblSoDaThu.Name = "lblSoDaThu";
            this.lblSoDaThu.Size = new System.Drawing.Size(54, 21);
            this.lblSoDaThu.TabIndex = 7;
            this.lblSoDaThu.Text = "Số đã thu";
            this.lblSoDaThu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSoPhatSinh
            // 
            this.lblSoPhatSinh.BackColor = System.Drawing.Color.Transparent;
            this.lblSoPhatSinh.Location = new System.Drawing.Point(5, 143);
            this.lblSoPhatSinh.Name = "lblSoPhatSinh";
            this.lblSoPhatSinh.Size = new System.Drawing.Size(66, 21);
            this.lblSoPhatSinh.TabIndex = 6;
            this.lblSoPhatSinh.Text = "Số phát sinh";
            this.lblSoPhatSinh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSoDuDauNam
            // 
            this.lblSoDuDauNam.BackColor = System.Drawing.Color.Transparent;
            this.lblSoDuDauNam.Location = new System.Drawing.Point(5, 120);
            this.lblSoDuDauNam.Name = "lblSoDuDauNam";
            this.lblSoDuDauNam.Size = new System.Drawing.Size(80, 21);
            this.lblSoDuDauNam.TabIndex = 5;
            this.lblSoDuDauNam.Text = "Số dư đầu năm";
            this.lblSoDuDauNam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblEmail.Location = new System.Drawing.Point(5, 97);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 21);
            this.lblEmail.TabIndex = 4;
            this.lblEmail.Text = "Email";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPhone
            // 
            this.lblPhone.BackColor = System.Drawing.Color.Transparent;
            this.lblPhone.Location = new System.Drawing.Point(5, 74);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(70, 21);
            this.lblPhone.TabIndex = 3;
            this.lblPhone.Text = "Số điện thoại";
            this.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAddress
            // 
            this.lblAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAddress.Location = new System.Drawing.Point(5, 51);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(98, 21);
            this.lblAddress.TabIndex = 2;
            this.lblAddress.Text = "Địa chỉ";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomerName.Location = new System.Drawing.Point(5, 28);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(103, 21);
            this.lblCustomerName.TabIndex = 1;
            this.lblCustomerName.Text = "Tên đối tượng";
            this.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomerCode.Location = new System.Drawing.Point(5, 4);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(101, 21);
            this.lblCustomerCode.TabIndex = 0;
            this.lblCustomerCode.Text = "Mã đối tượng";
            this.lblCustomerCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridCT);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(855, 230);
            // 
            // uGridCT
            // 
            this.uGridCT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCT.Location = new System.Drawing.Point(0, 0);
            this.uGridCT.Name = "uGridCT";
            this.uGridCT.Size = new System.Drawing.Size(855, 230);
            this.uGridCT.TabIndex = 0;
            this.uGridCT.Text = "ultraGrid1";
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(150, 54);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(149, 22);
            this.tsmAdd.Text = "Thêm";
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tmsReLoad.Size = new System.Drawing.Size(149, 22);
            this.tmsReLoad.Text = "Tải Lại";
            // 
            // uGridIndex
            // 
            this.uGridIndex.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridIndex.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridIndex.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridIndex.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridIndex.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridIndex.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridIndex.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridIndex.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridIndex.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridIndex.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridIndex.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridIndex.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridIndex.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridIndex.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridIndex.Location = new System.Drawing.Point(0, 49);
            this.uGridIndex.Name = "uGridIndex";
            this.uGridIndex.Size = new System.Drawing.Size(857, 191);
            this.uGridIndex.TabIndex = 7;
            this.uGridIndex.Text = "uGrid";
            this.uGridIndex.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.uGridIndex_AfterSelectChange);
            this.uGridIndex.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridIndex_DoubleClickRow);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnLoc);
            this.panel1.Controls.Add(this.lblBeginDate);
            this.panel1.Controls.Add(this.lblEndDate);
            this.panel1.Controls.Add(this.dtBeginDate);
            this.panel1.Controls.Add(this.dtEndDate);
            this.panel1.Controls.Add(this.lblKyKeToan);
            this.panel1.Controls.Add(this.cbbDateTime);
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(857, 49);
            this.panel1.TabIndex = 6;
            // 
            // btnLoc
            // 
            this.btnLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.Color.DarkRed;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance11.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.Image = global::Accounting.Properties.Resources.filter;
            this.btnLoc.Appearance = appearance11;
            this.btnLoc.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance12.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnLoc.HotTrackAppearance = appearance12;
            this.btnLoc.Location = new System.Drawing.Point(782, 10);
            this.btnLoc.Name = "btnLoc";
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnLoc.PressedAppearance = appearance13;
            this.btnLoc.Size = new System.Drawing.Size(63, 30);
            this.btnLoc.TabIndex = 75;
            this.btnLoc.Text = "Lọc";
            this.btnLoc.Click += new System.EventHandler(this.btnLoc_Click);
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.Color.Transparent;
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Bottom";
            this.lblBeginDate.Appearance = appearance14;
            this.lblBeginDate.Location = new System.Drawing.Point(541, 19);
            this.lblBeginDate.Name = "lblBeginDate";
            this.lblBeginDate.Size = new System.Drawing.Size(24, 17);
            this.lblBeginDate.TabIndex = 71;
            this.lblBeginDate.Text = "Từ:";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.lblEndDate.Appearance = appearance15;
            this.lblEndDate.Location = new System.Drawing.Point(658, 19);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(32, 17);
            this.lblEndDate.TabIndex = 72;
            this.lblEndDate.Text = "Đến:";
            // 
            // dtBeginDate
            // 
            this.dtBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance16;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(568, 17);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(85, 21);
            this.dtBeginDate.TabIndex = 73;
            this.dtBeginDate.Value = null;
            // 
            // dtEndDate
            // 
            this.dtEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.TextHAlignAsString = "Center";
            appearance17.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance17;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(694, 17);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(83, 21);
            this.dtEndDate.TabIndex = 74;
            this.dtEndDate.Value = null;
            // 
            // lblKyKeToan
            // 
            this.lblKyKeToan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Bottom";
            this.lblKyKeToan.Appearance = appearance18;
            this.lblKyKeToan.Location = new System.Drawing.Point(372, 19);
            this.lblKyKeToan.Name = "lblKyKeToan";
            this.lblKyKeToan.Size = new System.Drawing.Size(29, 17);
            this.lblKyKeToan.TabIndex = 70;
            this.lblKyKeToan.Text = "Kỳ:";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbDateTime.Location = new System.Drawing.Point(407, 17);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(128, 22);
            this.cbbDateTime.TabIndex = 69;
            // 
            // btnRefresh
            // 
            appearance19.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.btnRefresh.Appearance = appearance19;
            this.btnRefresh.Location = new System.Drawing.Point(93, 10);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 30);
            this.btnRefresh.TabIndex = 9;
            this.btnRefresh.Text = "Nạp";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnAdd
            // 
            appearance20.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance20;
            this.btnAdd.Location = new System.Drawing.Point(12, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.TabStop = false;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tabInfomation
            // 
            this.tabInfomation.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabInfomation.Controls.Add(this.ultraTabPageControl1);
            this.tabInfomation.Controls.Add(this.ultraTabPageControl2);
            this.tabInfomation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabInfomation.Location = new System.Drawing.Point(0, 240);
            this.tabInfomation.Name = "tabInfomation";
            this.tabInfomation.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabInfomation.Size = new System.Drawing.Size(857, 253);
            this.tabInfomation.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon;
            this.tabInfomation.TabIndex = 5;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1.Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2. Chi tiết";
            this.tabInfomation.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.tabInfomation.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(855, 230);
            // 
            // FSAReceiptCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 493);
            this.Controls.Add(this.uGridIndex);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabInfomation);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSAReceiptCustomer";
            this.Text = "Thu tiền khách hàng";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSAReceiptCustomer_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSAReceiptCustomer_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCT)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridIndex)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabInfomation)).EndInit();
            this.tabInfomation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabInfomation;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCT;
        private System.Windows.Forms.Panel panel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridIndex;
        private Infragistics.Win.Misc.UltraButton btnRefresh;
        private System.Windows.Forms.Label lblCustomerCode;
        private System.Windows.Forms.Label lblSoConPhaiThu;
        private System.Windows.Forms.Label lblSoDaThu;
        private System.Windows.Forms.Label lblSoPhatSinh;
        private System.Windows.Forms.Label lblSoDuDauNam;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Label txtPhone;
        private System.Windows.Forms.Label txtAddress;
        private System.Windows.Forms.Label txtCustomerName;
        private System.Windows.Forms.Label txtCustomerCode;
        private System.Windows.Forms.Label txtSoDaThu;
        private System.Windows.Forms.Label txtSoConPhaiThu;
        private System.Windows.Forms.Label txtSoPhatSinh;
        private System.Windows.Forms.Label txtSoDuDauNam;
        private System.Windows.Forms.Label txtEmail;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        public Infragistics.Win.Misc.UltraButton btnLoc;
        private Infragistics.Win.Misc.UltraLabel lblBeginDate;
        private Infragistics.Win.Misc.UltraLabel lblEndDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.Misc.UltraLabel lblKyKeToan;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
    }
}