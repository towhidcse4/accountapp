﻿namespace Accounting
{
    partial class UtTinChungTm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.gbTM = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtKT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtlydo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtNN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDT = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbDT1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblKT = new Infragistics.Win.Misc.UltraLabel();
            this.lblLD = new Infragistics.Win.Misc.UltraLabel();
            this.lblNN = new Infragistics.Win.Misc.UltraLabel();
            this.lblDC = new Infragistics.Win.Misc.UltraLabel();
            this.lblDT = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gbTM)).BeginInit();
            this.gbTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlydo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbTM
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            this.gbTM.Appearance = appearance1;
            this.gbTM.Controls.Add(this.ultraLabel1);
            this.gbTM.Controls.Add(this.txtKT);
            this.gbTM.Controls.Add(this.txtlydo);
            this.gbTM.Controls.Add(this.txtNN);
            this.gbTM.Controls.Add(this.txtDC);
            this.gbTM.Controls.Add(this.txtDT);
            this.gbTM.Controls.Add(this.cbbDT1);
            this.gbTM.Controls.Add(this.lblKT);
            this.gbTM.Controls.Add(this.lblLD);
            this.gbTM.Controls.Add(this.lblNN);
            this.gbTM.Controls.Add(this.lblDC);
            this.gbTM.Controls.Add(this.lblDT);
            this.gbTM.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance14.FontData.BoldAsString = "True";
            appearance14.FontData.SizeInPoints = 10F;
            this.gbTM.HeaderAppearance = appearance14;
            this.gbTM.Location = new System.Drawing.Point(0, 0);
            this.gbTM.Name = "gbTM";
            this.gbTM.Size = new System.Drawing.Size(563, 160);
            this.gbTM.TabIndex = 13;
            this.gbTM.Text = "Thông tin chung";
            this.gbTM.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraLabel1.Location = new System.Drawing.Point(464, 134);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(86, 23);
            this.ultraLabel1.TabIndex = 11;
            this.ultraLabel1.Text = "Chứng từ gốc";
            // 
            // txtKT
            // 
            this.txtKT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKT.Location = new System.Drawing.Point(89, 133);
            this.txtKT.Name = "txtKT";
            this.txtKT.Size = new System.Drawing.Size(369, 21);
            this.txtKT.TabIndex = 10;
            // 
            // txtlydo
            // 
            this.txtlydo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtlydo.Location = new System.Drawing.Point(88, 107);
            this.txtlydo.Name = "txtlydo";
            this.txtlydo.Size = new System.Drawing.Size(461, 21);
            this.txtlydo.TabIndex = 9;
            // 
            // txtNN
            // 
            this.txtNN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNN.Location = new System.Drawing.Point(89, 80);
            this.txtNN.Name = "txtNN";
            this.txtNN.Size = new System.Drawing.Size(461, 21);
            this.txtNN.TabIndex = 8;
            // 
            // txtDC
            // 
            this.txtDC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDC.Location = new System.Drawing.Point(89, 53);
            this.txtDC.Name = "txtDC";
            this.txtDC.Size = new System.Drawing.Size(461, 21);
            this.txtDC.TabIndex = 7;
            // 
            // txtDT
            // 
            this.txtDT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDT.Location = new System.Drawing.Point(239, 26);
            this.txtDT.Name = "txtDT";
            this.txtDT.Size = new System.Drawing.Size(311, 21);
            this.txtDT.TabIndex = 6;
            // 
            // cbbDT1
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbDT1.DisplayLayout.Appearance = appearance2;
            this.cbbDT1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbDT1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT1.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.cbbDT1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbDT1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.cbbDT1.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbDT1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbDT1.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbDT1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.cbbDT1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbDT1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.cbbDT1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbDT1.DisplayLayout.Override.CellAppearance = appearance9;
            this.cbbDT1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbDT1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbDT1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.cbbDT1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.cbbDT1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbDT1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.cbbDT1.DisplayLayout.Override.RowAppearance = appearance12;
            this.cbbDT1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbDT1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.cbbDT1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbDT1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbDT1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbDT1.Location = new System.Drawing.Point(89, 25);
            this.cbbDT1.Name = "cbbDT1";
            this.cbbDT1.Size = new System.Drawing.Size(144, 22);
            this.cbbDT1.TabIndex = 5;
            this.cbbDT1.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbDT1_RowSelected);
            this.cbbDT1.TextChanged += new System.EventHandler(this.cbbDT1_TextChanged);
            this.cbbDT1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbDT1_KeyPress);
            // 
            // lblKT
            // 
            this.lblKT.Location = new System.Drawing.Point(4, 137);
            this.lblKT.Name = "lblKT";
            this.lblKT.Size = new System.Drawing.Size(78, 23);
            this.lblKT.TabIndex = 4;
            this.lblKT.Text = "Kèm Theo";
            // 
            // lblLD
            // 
            this.lblLD.Location = new System.Drawing.Point(3, 112);
            this.lblLD.Name = "lblLD";
            this.lblLD.Size = new System.Drawing.Size(79, 23);
            this.lblLD.TabIndex = 3;
            this.lblLD.Text = "Lý Do Nộp";
            // 
            // lblNN
            // 
            this.lblNN.Location = new System.Drawing.Point(4, 84);
            this.lblNN.Name = "lblNN";
            this.lblNN.Size = new System.Drawing.Size(79, 23);
            this.lblNN.TabIndex = 2;
            this.lblNN.Text = "Người Nộp";
            // 
            // lblDC
            // 
            this.lblDC.Location = new System.Drawing.Point(5, 55);
            this.lblDC.Name = "lblDC";
            this.lblDC.Size = new System.Drawing.Size(77, 23);
            this.lblDC.TabIndex = 1;
            this.lblDC.Text = "Địa Chỉ";
            // 
            // lblDT
            // 
            this.lblDT.Location = new System.Drawing.Point(6, 26);
            this.lblDT.Name = "lblDT";
            this.lblDT.Size = new System.Drawing.Size(78, 23);
            this.lblDT.TabIndex = 0;
            this.lblDT.Text = "Đối Tượng";
            // 
            // UtTinChungTm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbTM);
            this.Name = "UtTinChungTm";
            this.Size = new System.Drawing.Size(563, 160);
            ((System.ComponentModel.ISupportInitialize)(this.gbTM)).EndInit();
            this.gbTM.ResumeLayout(false);
            this.gbTM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlydo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDT1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox gbTM;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtKT;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtlydo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDT;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDT1;
        private Infragistics.Win.Misc.UltraLabel lblKT;
        private Infragistics.Win.Misc.UltraLabel lblLD;
        private Infragistics.Win.Misc.UltraLabel lblNN;
        private Infragistics.Win.Misc.UltraLabel lblDC;
        private Infragistics.Win.Misc.UltraLabel lblDT;
    }
}
