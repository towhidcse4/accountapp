﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.Domain.obj;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FSAReceiptCustomer : CustormForm
    {
        List<CollectionCustomer> _lstCollectionCustomers = new List<CollectionCustomer>();//danh sách cho cả form trong(detail) và ngoài
        CollectionCustomer _collectionCustomer = new CollectionCustomer();//đối tượng được chỉ đến

        public static IDateTimeCacheHistoryService _IDateTimeCacheHistoryService
        {
            get { return IoC.Resolve<IDateTimeCacheHistoryService>(); }
        }
        public FSAReceiptCustomer()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            //lstCollectionCustomers = _CollectionCustomerao.GetExample();
            List<Item> lstItems = Utils.ObjConstValue.SelectTimes;
            // load dữ liệu cho Combobox Chọn thời gian
            cbbDateTime.DataSource = lstItems;
            cbbDateTime.DisplayMember = "Name";
            Utils.ConfigGrid(cbbDateTime, ConstDatabase.ConfigXML_TableName);
            cbbDateTime.ValueChanged += cbbDateTime_ValueChanged;
            cbbDateTime.SelectedRow = cbbDateTime.Rows[7];

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode("FSAReceiptCustomer", Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                if (dateTimeCacheHistory.DtBeginDate != null && dateTimeCacheHistory.DtEndDate != null)
                {
                    cbbDateTime.SelectedRow = cbbDateTime.Rows[34];
                    dtBeginDate.DateTime = dateTimeCacheHistory.DtBeginDate ?? DateTime.Now;
                    dtEndDate.DateTime = dateTimeCacheHistory.DtEndDate ?? DateTime.Now;
                }
                else
                {
                    //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
                }
            }
            else
            {
                //dateTimeCacheHistory = new DateTimeCacheHistory();
                //dateTimeCacheHistory.ID = Guid.NewGuid();
                //dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                //dateTimeCacheHistory.SubSystemCode = "FSAReceiptCustomer";
                //dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                //dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                //dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                //_IDateTimeCacheHistoryService.BeginTran();
                //_IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                //_IDateTimeCacheHistoryService.CommitTran();
                //cbbDateTime.SelectedRow = cbbDateTime.Rows[7];
            }

            _lstCollectionCustomers = Utils.IAccountingObjectService
               .GetListCustomerDebits(Utils.StringToDateTime(Utils.GetDbStartDate()) ?? DateTime.Now, dtBeginDate.DateTime, dtEndDate.DateTime);
            LoadDuLieu(new CollectionCustomer());
            tmsReLoad.Click += tmsReLoad_Click;
            tsmAdd.Click += tsmAdd_Click;
            uGridIndex.MouseDown += uGridIndex_MouseDown;
            btnRefresh.Visible = false;


            WaitingFrm.StopWaiting();

            
        }
        #region Sự kiện
        private void cbbDateTime_ValueChanged(object sender, EventArgs e)
        {
            var cbb = (UltraCombo)sender;
            if (cbb.SelectedRow != null)
            {
                var model = cbb.SelectedRow.ListObject as Item;
                DateTime dtBegin;
                DateTime dtEnd;
                Utils.GetDateBeginDateEnd(DateTime.Now.Year, DateTime.Now, model, out dtBegin, out dtEnd);
                dtBeginDate.Value = dtBegin;
                dtEndDate.Value = dtEnd;
            }
        }
        private void uGridIndex_AfterSelectChange(object sender, AfterSelectChangeEventArgs e) //thực hiện khi lên xuống dòng
        {
            if (uGridIndex.Selected.Rows.Count > 0)
            {
                CollectionCustomer model = uGridIndex.Selected.Rows[0].ListObject as CollectionCustomer;
                ViewTabInfomation(model);
                if (model != null) ViewTabDetails(model.ReceiptDebits.OrderByDescending(x => x.Date).ToList());
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }
        private void uGridIndex_DoubleClickRow(object sender, DoubleClickRowEventArgs e)//thực hiện khi douclick vào dòng xem chi tiết
        {
            AddFunction(true);
        }
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction(false);
        }
        private void tmsReLoad_Click(object sender, EventArgs e)
        {
             _lstCollectionCustomers = Utils.IAccountingObjectService.GetListCustomerDebits(OPNUntil.StartDateOPN, null, null);
            LoadDuLieu(new CollectionCustomer(), false);
        }
        private void uGridIndex_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGridIndex, cms4Grid);
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            _lstCollectionCustomers = Utils.IAccountingObjectService.GetListCustomerDebits(OPNUntil.StartDateOPN, null, null);
            LoadDuLieu(new CollectionCustomer());
        }
        #endregion

        #region Xử lý
        private void LoadDuLieu(CollectionCustomer ctDebit, bool configGrid = true)
        {
            ViewTabDetails((_lstCollectionCustomers != null && _lstCollectionCustomers.Count > 0) ? _lstCollectionCustomers[0].ReceiptDebits : new List<ReceiptDebit>(), false);
            ViewTabIndex(_lstCollectionCustomers, configGrid);
            if (uGridIndex.Rows.Count > 0)
            {
                try
                {
                    if (ctDebit == null || ctDebit.AccountingObject == null)
                    {
                        uGridIndex.Rows[0].Selected = true;
                        uGridIndex.Rows[0].Activate();
                    }
                    else
                    {
                        try
                        {
                            foreach (UltraGridRow row in uGridIndex.Rows)
                            {
                                CollectionCustomer collectionCustomerRow = (CollectionCustomer)row.ListObject;
                                if (collectionCustomerRow.AccountingObject.ID == ctDebit.AccountingObject.ID)
                                {
                                    row.Selected = true;
                                    row.Activate();
                                    break;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            uGridIndex.Rows[0].Selected = true;
                            uGridIndex.Rows[0].Activate();
                        }

                    }
                }
                catch (Exception)
                {
                    return;
                }

            }
        }
        public void ViewTabIndex(List<CollectionCustomer> lstCollectionCustomer, bool configGrid = true)
        {
            if (!configGrid) return;
            uGridIndex.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.RowSelect;
            uGridIndex.DataSource = lstCollectionCustomer;
            Utils.ConfigGrid(uGridIndex, ConstDatabase.CollectionCustomer_TableName);
            Utils.FormatNumberic(uGridIndex.DisplayLayout.Bands[0].Columns["SoDuDauNam"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridIndex, "SoDuDauNam", false);
            // thêm tổng số ở cột "Số phát sinh"
            Utils.FormatNumberic(uGridIndex.DisplayLayout.Bands[0].Columns["SoPhatSinh"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridIndex, "SoPhatSinh", false);
            // Thêm tổng số ở cột "Số đã thu"
            Utils.FormatNumberic(uGridIndex.DisplayLayout.Bands[0].Columns["SoDaThu"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridIndex, "SoDaThu", false);
            // Thêm tổng số ở cột "Số còn phải thu"
            Utils.FormatNumberic(uGridIndex.DisplayLayout.Bands[0].Columns["SoConPhaiThu"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridIndex, "SoConPhaiThu", false);
        }
        public void ViewTabInfomation(CollectionCustomer model)
        {
            txtCustomerCode.Text = string.Format(@": {0}", model.AccountingObject.AccountingObjectCode);
            txtCustomerName.Text = string.Format(@": {0}", model.AccountingObject.AccountingObjectName);
            txtAddress.Text = string.Format(@": {0}", model.AccountingObject.Address);
            txtPhone.Text = string.Format(@": {0}", model.AccountingObject.Tel);
            txtEmail.Text = string.Format(@": {0}", model.AccountingObject.Email);
            txtSoDuDauNam.FormatNumberic(model.SoDuDauNam, ConstDatabase.Format_TienVND);
            txtSoDuDauNam.Text = string.Format(@": {0}", txtSoDuDauNam.Text);
            txtSoPhatSinh.FormatNumberic(model.SoPhatSinh, ConstDatabase.Format_TienVND);
            txtSoPhatSinh.Text = string.Format(@": {0}", txtSoPhatSinh.Text);
            txtSoDaThu.FormatNumberic(model.SoDaThu, ConstDatabase.Format_TienVND);
            txtSoDaThu.Text = string.Format(@": {0}", txtSoDaThu.Text);
            txtSoConPhaiThu.FormatNumberic(model.SoConPhaiThu, ConstDatabase.Format_TienVND);
            txtSoConPhaiThu.Text = string.Format(@": {0}", txtSoConPhaiThu.Text);
            uGridCT.DataSource = model.ReceiptDebits;
        }
        public void ViewTabDetails(List<ReceiptDebit> receiptDebits, bool configGrid = false)
        {
            if (configGrid) return;
            uGridCT.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.RowSelect;
            uGridCT.DataSource = receiptDebits;
            Utils.ConfigGrid(uGridCT, ConstDatabase.ReceiptDebit_TableName);
            //Tổng số hàng
            uGridCT.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default; //Ẩn ký hiệu tổng trên Header Caption
            UltraGridBand band = uGridCT.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            //Thêm tổng số ở cột "Số dư đầu năm"
            Utils.FormatNumberic(uGridCT.DisplayLayout.Bands[0].Columns["SoDaThu"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCT, "SoDaThu", false);
            // thêm tổng số ở cột "Số phát sinh"
            Utils.FormatNumberic(uGridCT.DisplayLayout.Bands[0].Columns["SoPhaiThuQD"], ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCT, "SoPhaiThuQD", false);
        }
        void AddFunction(bool edit)
        {
            //_collectionCustomer = uGridIndex.Selected.Rows.Count <= 0 ? new CollectionCustomer() : uGridIndex.Selected.Rows[0].ListObject as CollectionCustomer;
            if (uGridIndex.Selected.Rows.Count <= 0)
            {
                MessageBox.Show("Không có đối tượng để trả tiền");
                return;
            }
            //_collectionCustomer = uGridIndex.Selected.Rows[0].ListObject as CollectionCustomer;
            if (edit == true)
            {
                _collectionCustomer = uGridIndex.Selected.Rows.Count <= 0 ? new CollectionCustomer() : uGridIndex.Selected.Rows[0].ListObject as CollectionCustomer;
            }
            else
            {
                _collectionCustomer = new CollectionCustomer();                
            }
            FSAReceiptCustomerDetail frm = new FSAReceiptCustomerDetail(_collectionCustomer, _lstCollectionCustomers);
            //HUYPD Edit Show Form
            frm.FormClosed += FSAReceiptCustomerDetail_FormClosed;
            frm.Show();
            //if (frm.ShowDialog(this) == DialogResult.OK)
            //{
               
            //    _lstCollectionCustomers = Utils.IAccountingObjectService.GetListCustomerDebits(OPNUntil.StartDateOPN, dtBeginDate.DateTime, dtEndDate.DateTime);
            //    LoadDuLieu(new CollectionCustomer());
            //}
        }

        private void FSAReceiptCustomerDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            _lstCollectionCustomers = Utils.IAccountingObjectService.GetListCustomerDebits(OPNUntil.StartDateOPN, dtBeginDate.DateTime, dtEndDate.DateTime);
            LoadDuLieu(new CollectionCustomer());
        }
        #endregion

        private void btnLoc_Click(object sender, EventArgs e)
        {

            DateTimeCacheHistory dateTimeCacheHistory = _IDateTimeCacheHistoryService.GetDateTimeCacheHistoryBySubSystemCode("FSAReceiptCustomer", Authenticate.User.userid);
            if (dateTimeCacheHistory != null)
            {
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Update(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            else
            {
                dateTimeCacheHistory = new DateTimeCacheHistory();
                dateTimeCacheHistory.ID = Guid.NewGuid();
                dateTimeCacheHistory.IDUser = Authenticate.User.userid;
                dateTimeCacheHistory.SelectedItem = cbbDateTime.SelectedRow.Index;
                dateTimeCacheHistory.SubSystemCode = "FSAReceiptCustomer";
                dateTimeCacheHistory.DtBeginDate = dtBeginDate.DateTime;
                dateTimeCacheHistory.DtEndDate = dtEndDate.DateTime;
                _IDateTimeCacheHistoryService.BeginTran();
                _IDateTimeCacheHistoryService.Save(dateTimeCacheHistory);
                _IDateTimeCacheHistoryService.CommitTran();
            }
            _lstCollectionCustomers = Utils.IAccountingObjectService
               .GetListCustomerDebits(Utils.StringToDateTime(Utils.GetDbStartDate()) ?? DateTime.Now, dtBeginDate.DateTime, dtEndDate.DateTime);
            LoadDuLieu(new CollectionCustomer());
        }

        private void FSAReceiptCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridIndex.ResetText();
            uGridIndex.ResetUpdateMode();
            uGridIndex.ResetExitEditModeOnLeave();
            uGridIndex.ResetRowUpdateCancelAction();
            uGridIndex.DataSource = null;
            uGridIndex.Layouts.Clear();
            uGridIndex.ResetLayouts();
            uGridIndex.ResetDisplayLayout();
            uGridIndex.Refresh();
            uGridIndex.ClearUndoHistory();
            uGridIndex.ClearXsdConstraints();

            uGridCT.ResetText();
            uGridCT.ResetUpdateMode();
            uGridCT.ResetExitEditModeOnLeave();
            uGridCT.ResetRowUpdateCancelAction();
            uGridCT.DataSource = null;
            uGridCT.Layouts.Clear();
            uGridCT.ResetLayouts();
            uGridCT.ResetDisplayLayout();
            uGridCT.Refresh();
            uGridCT.ClearUndoHistory();
            uGridCT.ClearXsdConstraints();
        }

        private void FSAReceiptCustomer_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
