﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;
using Accounting.Core.DAO;
using Accounting.Core.Domain.obj.Report;

namespace Accounting
{
    public partial class FSALiabilitiesReport : CustormForm
    {
        #region khai báo
        private readonly ISAInvoiceService _ISAInvoiceService;
        List<ListLibitiesReport> lstListLibitiesReport = new List<ListLibitiesReport>();
        private string _subSystemCode;
        #endregion
        #region khởi tạo mặc định cho form
        public FSALiabilitiesReport()
        {
            WaitingFrm.StartWaiting();
            _ISAInvoiceService = IoC.Resolve<ISAInvoiceService>();
            InitializeComponent();
            _subSystemCode = "";
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            fileReportSlot1.FilePath = string.Format("{0}\\Frm\\FReport\\Template\\ThongBaoCongNo.rst", path);
            dteDate.Value = OPNUntil.StartDateOPN;
            dtDenNgay.Value = DateTime.Now.Date;
            List<Currency> lst = new List<Currency> { new Currency() { ID = "<<Tất cả>>", CurrencyName = "<<Tất cả>>" } };
            lst.AddRange(Utils.ListCurrency.ToList());
            cbbCurrency.DataSource = lst;
            if (lst.Count > 0)
            {
                cbbCurrency.Rows[0].Activated = true;
                cbbCurrency.Rows[0].Selected = true;
            }
            Utils.ConfigGrid(cbbCurrency, ConstDatabase.Currency_TableName);
            ReportUtils.ProcessControls(this);
            lstListLibitiesReport = Utils.IAccountingObjectService.ListLibitiesReport(OPNUntil.StartDateOPN, (DateTime)dtDenNgay.Value, (Currency)cbbCurrency.SelectedRow.ListObject);
            ViewListLibitiesReports(lstListLibitiesReport);
            WaitingFrm.StopWaiting();
        }

        #endregion
        #region grid danh sach
        private void ViewListLibitiesReports(List<ListLibitiesReport> model)
        {
            uGridList.DataSource = model;
            //uGridList.SetDataBinding(model,"");
            Utils.ConfigGrid(uGridList, "", mauColumns());
            uGridList.DisplayLayout.Bands[0].Summaries.Clear();
            Utils.FormatNumberic(uGridList.DisplayLayout.Bands[0].Columns["NoPhaiTra"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridList.DisplayLayout.Bands[0].Columns["LaiNoPhaiTra"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGridList.DisplayLayout.Bands[0].Columns["TongTien"], ConstDatabase.Format_TienVND);
            if (model.Count > 0)
            {
                ReportUtils.ConfigGridColumnCheck(uGridList, "Status");
                Utils.AddSumColumn(uGridList, "NoPhaiTra", false, "", ConstDatabase.Format_TienVND, false);
                Utils.AddSumColumn(uGridList, "LaiNoPhaiTra", false, "", ConstDatabase.Format_TienVND, false);
                Utils.AddSumColumn(uGridList, "TongTien", false, "", ConstDatabase.Format_TienVND, false);
            }
        }
        #region mẫu giao diện
        private List<TemplateColumn> mauColumns()
        {
            return new List<TemplateColumn>
            {
                //new TemplateColumn
                //{
                //    ColumnName = "Status",
                //    ColumnCaption = "",
                //    ColumnWidth = 50,
                //    IsVisible = true,
                //    IsVisibleCbb = false,
                //    VisiblePosition = 1,
                //    IsReadOnly = true

                //},
                new TemplateColumn
                {
                    ColumnName = "MaKhachHang",
                    ColumnCaption = "Mã khách hàng",
                    ColumnWidth = 100,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 2,
                    IsReadOnly = true

                },
                new TemplateColumn
                {
                    ColumnName = "TenKhachHang",
                    ColumnCaption = "Tên khách hàng",
                    ColumnWidth = 130,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 3,
                    IsReadOnly = true
                },
                new TemplateColumn
                {
                    ColumnName = "NoPhaiTra",
                    ColumnCaption = "Nợ phải trả",
                    ColumnWidth = 120,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 4,
                    IsReadOnly = true
                },
                new TemplateColumn
                {
                    ColumnName = "LaiNoPhaiTra",
                    ColumnCaption = "Lãi nợ phải trả",
                    ColumnWidth = 100,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 5,
                    IsReadOnly = true
                },
                new TemplateColumn
                {
                    ColumnName = "CongNoDenNgay",
                    ColumnCaption = "Tính đến ngày",
                    ColumnWidth = 100,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 6,
                    IsReadOnly = true
                },
                new TemplateColumn
                {
                    ColumnName = "NgayThongBao",
                    ColumnCaption = "Ngày thông báo",
                    ColumnWidth = 100,
                    IsVisible = false,
                    IsVisibleCbb = true,
                    VisiblePosition = 7,
                    IsReadOnly = true
                },
                new TemplateColumn
                {
                    ColumnName = "CongNoTuNgay",
                    ColumnCaption = "CongNoTuNgay",
                    ColumnWidth = 100,
                    IsVisible = false,
                    IsVisibleCbb = true,
                    VisiblePosition = 8,
                    IsReadOnly = true
                },
                new TemplateColumn
                {
                    ColumnName = "TongTien",
                    ColumnCaption = "Tổng công nợ",
                    ColumnWidth = 100,
                    IsVisible = true,
                    IsVisibleCbb = true,
                    VisiblePosition = 9,
                    IsReadOnly = true
                }
            };
        }
        #endregion

        #endregion
        #region xử lý sự kiện button cho form
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXemtb_Click(object sender, EventArgs e)
        {
            if (lstListLibitiesReport.Count < 0)
            {
                MSG.Error("Không có đối tượng để xem!");
                return;
            }
            if (dteDate.Value == null || dtDenNgay.Value == null)
            {
                MSG.Error("Bạn chưa chọn ngày");
                return;
            }
            UltraGridRow row = uGridList.ActiveRow;
            if (row == null)
            {
                MSG.Error("Bạn chưa chọn đối tượng, vui lòng thử lại");
                return;
            }
            var data = lstListLibitiesReport[row.Index];
            data.bill = data.bill.OrderByDescending(x => x.PostedDate).ToList();
            var rD = new ListLibitiesReport_period();
            rD.Period = ReportUtils.GetPeriod((DateTime)dtDenNgay.Value);
            var f = new ReportForm<ListLibitiesReport>(fileReportSlot1, _subSystemCode);
            f.AddDatasource("TBCN", data);
            f.AddDatasource("Detail", rD);
            f.LoadReport();
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }
        private void btnTinhCongNo_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            if (dteDate.Value != null && dtDenNgay.Value != null)
            {
                lstListLibitiesReport = Utils.IAccountingObjectService.ListLibitiesReport((DateTime)dteDate.Value, (DateTime)dtDenNgay.Value, (Currency)cbbCurrency.SelectedRow.ListObject);
                ViewListLibitiesReports(lstListLibitiesReport);
            }
            else
                MSG.Error("Bạn chưa chọn ngày");
        }
    }
}
