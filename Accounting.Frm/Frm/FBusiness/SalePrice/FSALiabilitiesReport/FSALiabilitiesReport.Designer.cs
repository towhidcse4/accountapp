﻿namespace Accounting
{
    partial class FSALiabilitiesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.dtDenNgay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDateEnd = new Infragistics.Win.Misc.UltraLabel();
            this.dteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnTinhCongNo = new Infragistics.Win.Misc.UltraButton();
            this.btnXemtb = new Infragistics.Win.Misc.UltraButton();
            this.uGridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.fileReportSlot1 = new PerpetuumSoft.Reporting.Components.FileReportSlot(this.components);
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDenNgay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.button1);
            this.ultraGroupBox1.Controls.Add(this.cbbCurrency);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.dtDenNgay);
            this.ultraGroupBox1.Controls.Add(this.lblDateEnd);
            this.ultraGroupBox1.Controls.Add(this.dteDate);
            this.ultraGroupBox1.Controls.Add(this.lblDate);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(896, 40);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(761, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 23);
            this.button1.TabIndex = 56;
            this.button1.Text = "Lấy dữ liệu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbbCurrency
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurrency.DisplayLayout.Appearance = appearance1;
            this.cbbCurrency.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurrency.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cbbCurrency.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cbbCurrency.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurrency.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurrency.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurrency.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurrency.DisplayLayout.Override.CellAppearance = appearance8;
            this.cbbCurrency.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurrency.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cbbCurrency.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cbbCurrency.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurrency.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurrency.DisplayLayout.Override.RowAppearance = appearance11;
            this.cbbCurrency.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurrency.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cbbCurrency.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurrency.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurrency.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurrency.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbCurrency.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbCurrency.Location = new System.Drawing.Point(480, 8);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.Size = new System.Drawing.Size(241, 22);
            this.cbbCurrency.TabIndex = 55;
            // 
            // ultraLabel1
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance13;
            this.ultraLabel1.Location = new System.Drawing.Point(428, 8);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(67, 18);
            this.ultraLabel1.TabIndex = 54;
            this.ultraLabel1.Text = "Loại tiền";
            // 
            // dtDenNgay
            // 
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            this.dtDenNgay.Appearance = appearance14;
            this.dtDenNgay.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtDenNgay.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtDenNgay.Location = new System.Drawing.Point(309, 8);
            this.dtDenNgay.MaskInput = "";
            this.dtDenNgay.Name = "dtDenNgay";
            this.dtDenNgay.Size = new System.Drawing.Size(98, 21);
            this.dtDenNgay.TabIndex = 32;
            this.dtDenNgay.Value = null;
            // 
            // lblDateEnd
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.lblDateEnd.Appearance = appearance15;
            this.lblDateEnd.AutoSize = true;
            this.lblDateEnd.Location = new System.Drawing.Point(241, 12);
            this.lblDateEnd.Name = "lblDateEnd";
            this.lblDateEnd.Size = new System.Drawing.Size(62, 14);
            this.lblDateEnd.TabIndex = 31;
            this.lblDateEnd.Text = "Đến ngày  :";
            // 
            // dteDate
            // 
            appearance16.TextHAlignAsString = "Center";
            appearance16.TextVAlignAsString = "Middle";
            this.dteDate.Appearance = appearance16;
            this.dteDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dteDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dteDate.Location = new System.Drawing.Point(92, 8);
            this.dteDate.MaskInput = "";
            this.dteDate.Name = "dteDate";
            this.dteDate.Size = new System.Drawing.Size(98, 21);
            this.dteDate.TabIndex = 28;
            this.dteDate.Value = null;
            // 
            // lblDate
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Appearance = appearance17;
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(6, 12);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(90, 14);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Ngày thông báo :";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.btnTinhCongNo);
            this.ultraGroupBox2.Controls.Add(this.btnXemtb);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 380);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(896, 40);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnTinhCongNo
            // 
            this.btnTinhCongNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnTinhCongNo.Appearance = appearance18;
            this.btnTinhCongNo.Location = new System.Drawing.Point(813, 7);
            this.btnTinhCongNo.Name = "btnTinhCongNo";
            this.btnTinhCongNo.Size = new System.Drawing.Size(77, 27);
            this.btnTinhCongNo.TabIndex = 2;
            this.btnTinhCongNo.Text = "Đóng";
            this.btnTinhCongNo.Click += new System.EventHandler(this.btnTinhCongNo_Click);
            // 
            // btnXemtb
            // 
            this.btnXemtb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.Image = global::Accounting.Properties.Resources.ubtnEdit11;
            this.btnXemtb.Appearance = appearance19;
            this.btnXemtb.Location = new System.Drawing.Point(687, 7);
            this.btnXemtb.Name = "btnXemtb";
            this.btnXemtb.Size = new System.Drawing.Size(120, 27);
            this.btnXemtb.TabIndex = 0;
            this.btnXemtb.Text = "Xem thông báo";
            this.btnXemtb.Click += new System.EventHandler(this.btnXemtb_Click);
            // 
            // uGridList
            // 
            this.uGridList.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridList.Location = new System.Drawing.Point(0, 40);
            this.uGridList.Name = "uGridList";
            this.uGridList.Size = new System.Drawing.Size(896, 340);
            this.uGridList.TabIndex = 42;
            this.uGridList.Text = "ultraGrid1";
            this.uGridList.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // fileReportSlot1
            // 
            this.fileReportSlot1.FilePath = "";
            this.fileReportSlot1.ReportName = "";
            this.fileReportSlot1.ReportScriptType = typeof(PerpetuumSoft.Reporting.Rendering.ReportScriptBase);
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[0], new object[0]);
            this.reportManager1.OwnerForm = this;
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.fileReportSlot1});
            // 
            // FSALiabilitiesReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 420);
            this.Controls.Add(this.uGridList);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSALiabilitiesReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông báo công nợ phải thu";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDenNgay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel lblDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDenNgay;
        private Infragistics.Win.Misc.UltraLabel lblDateEnd;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteDate;
        private Infragistics.Win.Misc.UltraButton btnXemtb;
        private Infragistics.Win.Misc.UltraButton btnTinhCongNo;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridList;
        private PerpetuumSoft.Reporting.Components.FileReportSlot fileReportSlot1;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private System.Windows.Forms.Button button1;
    }
}