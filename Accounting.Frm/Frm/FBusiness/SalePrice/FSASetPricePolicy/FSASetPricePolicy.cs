﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;


namespace Accounting
{
    public partial class FSASetPricePolicy : CustormForm
    {
        #region Khai báo
        private readonly ISAPolicyPriceSettingService _ISAPolicyPriceSettingService;
        private readonly ISAPolicyPriceMaterialGoodsService _ISAPolicyPriceMaterialGoodsService;
        private readonly ISAPolicyPriceTableService _ISAPolicyPriceTableService;
        private readonly ISAPolicySalePriceCustomerService _ISAPolicySalePriceCustomerService;
        private readonly ISAPolicySalePriceGroupService _ISAPolicySalePriceGroupService;
        List<SAPolicyPriceSetting> lstPolicyPriceSettings = new List<SAPolicyPriceSetting>();
        List<SAPolicyPriceTableHT> lstVTHHApDung = new List<SAPolicyPriceTableHT>();
        List<SAPolicySalePriceCustomerHT> lstKHApDung = new List<SAPolicySalePriceCustomerHT>();
        #endregion

        #region Khởi tạo
        public FSASetPricePolicy()
        {
            WaitingFrm.StartWaiting();
            _ISAPolicyPriceSettingService = IoC.Resolve<ISAPolicyPriceSettingService>();
            _ISAPolicyPriceMaterialGoodsService = IoC.Resolve<ISAPolicyPriceMaterialGoodsService>();
            _ISAPolicyPriceTableService = IoC.Resolve<ISAPolicyPriceTableService>();
            _ISAPolicySalePriceCustomerService = IoC.Resolve<ISAPolicySalePriceCustomerService>();
            _ISAPolicySalePriceGroupService = IoC.Resolve<ISAPolicySalePriceGroupService>();
            InitializeComponent();
            LoadDuLieu();
            ButGiup.Visible = false;
            WaitingFrm.StopWaiting();
        }
        #endregion

        #region Hàm xử lý nghiệp vụ
        private void LoadDuLieu()
        {
            lstPolicyPriceSettings = _ISAPolicyPriceSettingService.GetPolicyPriceSettings();
            UltraGridThietLapChinhSachGia.SetDataBinding(lstPolicyPriceSettings, "");
            ViewDsach(UltraGridThietLapChinhSachGia, lstPolicyPriceSettings);
            if (UltraGridThietLapChinhSachGia.Rows.Count > 0)
            {
                UltraGridThietLapChinhSachGia.Rows[0].Selected = true;

                UltraGridRow rowSelect = UltraGridThietLapChinhSachGia.Rows[0];

                SAPolicyPriceSetting saPolicyPriceSettingSelect = (SAPolicyPriceSetting)rowSelect.ListObject;

                lstVTHHApDung = _ISAPolicyPriceSettingService.GetPolicyPriceTabless(saPolicyPriceSettingSelect.ID);
                UltraGridVTHHApDung.SetDataBinding(lstVTHHApDung, "");
                ViewDsachVTHH(UltraGridVTHHApDung, lstVTHHApDung);

                lstKHApDung = _ISAPolicyPriceSettingService.GetSAPolicySalePriceCustomerHT(saPolicyPriceSettingSelect.ID);
                UltraGridKHApDung.SetDataBinding(lstKHApDung, "");
                ViewDsachKhApDung(UltraGridKHApDung, lstKHApDung);
            }
        }
        private void AddFunction()
        {
            AddNewPricePolicy add = new AddNewPricePolicy();
            add.ShowDialog(this);
            if (!AddNewPricePolicy.isClose) LoadDuLieu();
        }
        private void EditFunction()
        {
            if (UltraGridThietLapChinhSachGia.Selected.Rows.Count > 0)
            {
                SAPolicyPriceSetting temp = UltraGridThietLapChinhSachGia.Selected.Rows[0].ListObject as SAPolicyPriceSetting;
                new AddNewPricePolicy(temp).ShowDialog(this);
                if (!AddNewPricePolicy.isClose) LoadDuLieu();
            }
        }
        private void DeleteFunction()
        {
            if (UltraGridThietLapChinhSachGia.Selected.Rows.Count > 0)
            {
                SAPolicyPriceSetting temp = UltraGridThietLapChinhSachGia.Selected.Rows[0].ListObject as SAPolicyPriceSetting;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_78, temp.SAPolicyPriceName)) == DialogResult.Yes)
                {
                    try
                    {
                        _ISAPolicyPriceSettingService.BeginTran();
                        List<SAPolicyPriceMaterialGoods> policyPriceMaterialGoodsesDelete =
                        _ISAPolicyPriceMaterialGoodsService.GetAll().Where(p => p.SAPolicyPriceSettingID == temp.ID).ToList();
                        List<SAPolicySalePriceGroup> policySalePriceGroupsDelete =
                            _ISAPolicySalePriceGroupService.Query.Where(p => p.SAPolicyPriceSettingID == temp.ID).ToList();
                        List<SAPolicyPriceTable> policyPriceTables =
                            _ISAPolicyPriceTableService.Query.Where(p => p.SAPolicyPriceSettingID == temp.ID).ToList();
                        if (policySalePriceGroupsDelete.Count > 0)
                        {
                            foreach (var saPolicySalePriceGroup in policySalePriceGroupsDelete)
                            {
                                List<SAPolicySalePriceCustomer> policySalePriceCustomersDelete =
                                    _ISAPolicySalePriceCustomerService.Query.Where(
                                        p => p.SAPolicySalePriceGroupID == saPolicySalePriceGroup.ID).ToList();
                                if (policySalePriceCustomersDelete.Count > 0)
                                {
                                    foreach (var saPolicySalePriceCustomer in policySalePriceCustomersDelete)
                                    {
                                        _ISAPolicySalePriceCustomerService.Delete(saPolicySalePriceCustomer);
                                    }
                                }
                                _ISAPolicySalePriceGroupService.Delete(saPolicySalePriceGroup);
                            }

                        }
                        if (policyPriceMaterialGoodsesDelete.Count > 0)
                        {
                            foreach (var saPolicyPriceMaterialGood in policyPriceMaterialGoodsesDelete)
                            {
                                _ISAPolicyPriceMaterialGoodsService.Delete(saPolicyPriceMaterialGood);
                            }
                        }
                        if (policyPriceTables.Count > 0)
                        {
                            foreach (var saPolicyPriceTable in policyPriceTables)
                            {
                                _ISAPolicyPriceTableService.Delete(saPolicyPriceTable);
                            }
                        }
                        SAPolicyPriceSetting saPolicyPriceSettingDelete = _ISAPolicyPriceSettingService.Getbykey(temp.ID);
                        _ISAPolicyPriceSettingService.Delete(saPolicyPriceSettingDelete);
                        _ISAPolicyPriceSettingService.CommitTran();
                        LoadDuLieu();
                        lstVTHHApDung = _ISAPolicyPriceSettingService.GetPolicyPriceTabless(temp.ID);
                        UltraGridVTHHApDung.SetDataBinding(lstVTHHApDung, "");
                        //ViewDsachVTHH(UltraGridVTHHApDung, lstVTHHApDung);
                        lstKHApDung = _ISAPolicyPriceSettingService.GetSAPolicySalePriceCustomerHT(temp.ID);
                        UltraGridKHApDung.SetDataBinding(lstKHApDung, "");
                        lstPolicyPriceSettings = _ISAPolicyPriceSettingService.GetPolicyPriceSettings();
                        UltraGridThietLapChinhSachGia.SetDataBinding(lstPolicyPriceSettings, "");
                    }
                    catch (Exception)
                    {
                        _ISAPolicyPriceSettingService.RolbackTran();
                        throw;
                    }

                }
            }
        }
        #endregion

        #region Hàm View Grid
        private void ViewDsach(UltraGrid uGrid, List<SAPolicyPriceSetting> model)
        {
            uGrid.ConfigGrid("", MauDanhSach(), isHaveSum: true);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;

        }
        private void ViewDsachVTHH(UltraGrid uGrid, List<SAPolicyPriceTableHT> model)
        {
            //LoadDuLieu();
            Utils.ConfigGrid(uGrid, "", MauDanhSachVTHH());
            //this.ConfigEachColumn4Grid(0, uGrid.DisplayLayout.Bands[0].Columns["MaterialGoodsCodeType"], uGrid);

            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
        }
        private void ViewDsachKhApDung(UltraGrid uGrid, List<SAPolicySalePriceCustomerHT> model)
        {
            //LoadDuLieu();
            Utils.ConfigGrid(uGrid, "", MauDanhSachApDungKH());
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
        }
        #endregion

        #region Template Grid
        private List<TemplateColumn> MauDanhSach()
        {
            return new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "SAPolicyPriceName",
                                   ColumnCaption = "Tên chính sách",
                                   ColumnWidth = 250,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 0,
                                  // IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "IsActive",
                                   ColumnCaption = "Ngừng hoạt động",
                                   ColumnWidth = 50,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1,
                                   //IsReadOnly = true,
                               },
                             
                       };
        }
        private List<TemplateColumn> MauDanhSachVTHH()
        {
            return new List<TemplateColumn>
                       {
                               new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsCategoryCode",
                                   ColumnCaption = "Loại VTHH",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                  // IsReadOnly = true,
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsCode",
                                   ColumnCaption = "Mã VTHH",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 0,
                                  // IsReadOnly = true,
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "MaterialGoodsName",
                                   ColumnCaption = "Tên VTHH",
                                   ColumnWidth = 250,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1,
                                  // IsReadOnly = true,
                               },
                       };
        }
        private List<TemplateColumn> MauDanhSachApDungKH()
        {
            return new List<TemplateColumn>
                       {
                               new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectCode",
                                   ColumnCaption = "Mã khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 0,
                                 //  IsReadOnly = true,
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectName",
                                   ColumnCaption = "Tên khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1,
                                  // IsReadOnly = true,
                               },
                               new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectCategory",
                                   ColumnCaption = "Nhóm",
                                   ColumnWidth = 250,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                  // IsReadOnly = true,
                               },
                       };
        }
        #endregion

        #region Events

        private void ButThem_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void ButSua_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void UltraGridThietLapChinhSachGia_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var temp = e.Row.ListObject as SAPolicyPriceSetting;
            if (temp == null) return;
            lstVTHHApDung = _ISAPolicyPriceSettingService.GetPolicyPriceTabless(temp.ID);
            UltraGridVTHHApDung.SetDataBinding(lstVTHHApDung, "");
            //ViewDsachVTHH(UltraGridVTHHApDung, lstVTHHApDung);
            lstKHApDung = _ISAPolicyPriceSettingService.GetSAPolicySalePriceCustomerHT(temp.ID);
            UltraGridKHApDung.SetDataBinding(lstKHApDung, "");
        }

        private void UltraGridThietLapChinhSachGia_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (e.Row.Index == -1) return;
            SAPolicyPriceSetting temp = UltraGridThietLapChinhSachGia.Selected.Rows[0].ListObject as SAPolicyPriceSetting;
            new AddNewPricePolicy(temp).ShowDialog(this);
            if (!AddNewPricePolicy.isClose) LoadDuLieu();
        }

        private void ButXoa_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void ButTaiLai_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, UltraGridThietLapChinhSachGia, cms4Grid);
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
        }


        #endregion

        private void FSASetPricePolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            UltraGridThietLapChinhSachGia.ResetText();
            UltraGridThietLapChinhSachGia.ResetUpdateMode();
            UltraGridThietLapChinhSachGia.ResetExitEditModeOnLeave();
            UltraGridThietLapChinhSachGia.ResetRowUpdateCancelAction();
            UltraGridThietLapChinhSachGia.DataSource = null;
            UltraGridThietLapChinhSachGia.Layouts.Clear();
            UltraGridThietLapChinhSachGia.ResetLayouts();
            UltraGridThietLapChinhSachGia.ResetDisplayLayout();
            UltraGridThietLapChinhSachGia.Refresh();
            UltraGridThietLapChinhSachGia.ClearUndoHistory();
            UltraGridThietLapChinhSachGia.ClearXsdConstraints();

            UltraGridVTHHApDung.ResetText();
            UltraGridVTHHApDung.ResetUpdateMode();
            UltraGridVTHHApDung.ResetExitEditModeOnLeave();
            UltraGridVTHHApDung.ResetRowUpdateCancelAction();
            UltraGridVTHHApDung.DataSource = null;
            UltraGridVTHHApDung.Layouts.Clear();
            UltraGridVTHHApDung.ResetLayouts();
            UltraGridVTHHApDung.ResetDisplayLayout();
            UltraGridVTHHApDung.Refresh();
            UltraGridVTHHApDung.ClearUndoHistory();
            UltraGridVTHHApDung.ClearXsdConstraints();

            UltraGridKHApDung.ResetText();
            UltraGridKHApDung.ResetUpdateMode();
            UltraGridKHApDung.ResetExitEditModeOnLeave();
            UltraGridKHApDung.ResetRowUpdateCancelAction();
            UltraGridKHApDung.DataSource = null;
            UltraGridKHApDung.Layouts.Clear();
            UltraGridKHApDung.ResetLayouts();
            UltraGridKHApDung.ResetDisplayLayout();
            UltraGridKHApDung.Refresh();
            UltraGridKHApDung.ClearUndoHistory();
            UltraGridKHApDung.ClearXsdConstraints();
        }

        private void FSASetPricePolicy_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
