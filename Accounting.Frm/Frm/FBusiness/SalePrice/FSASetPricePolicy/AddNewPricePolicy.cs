﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class AddNewPricePolicy : CustormForm
    {
        #region Khai báo
        private readonly ISAPolicyPriceSettingService _ISAPolicyPriceSettingService;
        private readonly ISAPolicyPriceMaterialGoodsService _ISAPolicyPriceMaterialGoodsService;
        private readonly ISAPolicySalePriceGroupService _ISAPolicySalePriceGroupService;
        private readonly ISAPolicyPriceTableService _ISAPolicyPriceTableService;
        private readonly ISAPolicySalePriceCustomerService _ISAPolicySalePriceCustomerService;
        private readonly ISalePriceGroupService _ISalePriceGroupService;
        private readonly IMaterialGoodsService _IMaterialGoodsService;
        int vitri;
        int vitri1;
        private int indexCurrent;
        List<Control> lstControl = new List<Control>();
        List<MaterialGoodHT> lstChonVTTHH = new List<MaterialGoodHT>();
        List<GroupSalePriceHT> lstGroupSalePriceHts = new List<GroupSalePriceHT>();
        List<SAPolicySalePriceCustomerHT> lstPolicySalePriceCustomerHts = new List<SAPolicySalePriceCustomerHT>();
        List<SAPolicySalePriceCustomerHT> lstNhomGiaBAn = new List<SAPolicySalePriceCustomerHT>();
        List<PriceUpHT> lstPriceUpHts = new List<PriceUpHT>();
        DataTable dt = new DataTable();
        UltraCombo cbbDuaTren = new UltraCombo();
        UltraCombo cbbPhuongPhap = new UltraCombo();
        List<string> lItem2 = new List<string>() { "Tăng số tiền", "Giảm số tiền", "Tăng %", "Giảm %" };
        List<string> lItem = new List<string>() { "Đơn giá bán 1", "Đơn giá bán 2", "Đơn giá bán 3", "Đơn giá cố định", "Giá nhập gần nhất", "Giá chính sách hiện tại" };
        public static bool isClose = true;
        bool IsAdd = true;
        bool IsLoadMg = true;
        bool IsLoadSpGroup = true;
        bool IsLoadTable = true;
        bool IsLoadCustomer = true;
        bool IsAddColumn = true;
        private SAPolicyPriceSetting _select;
        List<SAPolicySalePriceCustomer> listSelectPolicySalePriceCustomers = new List<SAPolicySalePriceCustomer>();
        List<SAPolicySalePriceGroup> listPolicySalePriceGroup = new List<SAPolicySalePriceGroup>();

        List<SAPolicyPriceMaterialGoods> _policyPriceMaterialGoodsesDb = new List<SAPolicyPriceMaterialGoods>();
        List<SAPolicySalePriceGroup> _policySalePriceGroupsDb = new List<SAPolicySalePriceGroup>();
        List<SAPolicyPriceTable> _policyPriceTablesDb = new List<SAPolicyPriceTable>();
        List<SAPolicySalePriceCustomer> _policySalePriceCustomersDb = new List<SAPolicySalePriceCustomer>();
        #endregion

        #region Khởi tạo
        public AddNewPricePolicy()
        {
            _ISAPolicyPriceSettingService = IoC.Resolve<ISAPolicyPriceSettingService>();
            _ISAPolicyPriceMaterialGoodsService = IoC.Resolve<ISAPolicyPriceMaterialGoodsService>();
            _ISAPolicySalePriceGroupService = IoC.Resolve<ISAPolicySalePriceGroupService>();
            _ISAPolicyPriceTableService = IoC.Resolve<ISAPolicyPriceTableService>();
            _ISAPolicySalePriceCustomerService = IoC.Resolve<ISAPolicySalePriceCustomerService>();
            _ISalePriceGroupService = IoC.Resolve<ISalePriceGroupService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            InitializeComponent();
            Text = "Thêm chính sách";
            LoadChung();
            ConfigButton();
            ShowPal();
        }

        public AddNewPricePolicy(SAPolicyPriceSetting temp)
        {
            _ISAPolicyPriceSettingService = IoC.Resolve<ISAPolicyPriceSettingService>();
            _ISAPolicyPriceMaterialGoodsService = IoC.Resolve<ISAPolicyPriceMaterialGoodsService>();
            _ISAPolicySalePriceGroupService = IoC.Resolve<ISAPolicySalePriceGroupService>();
            _ISAPolicyPriceTableService = IoC.Resolve<ISAPolicyPriceTableService>();
            _ISAPolicySalePriceCustomerService = IoC.Resolve<ISAPolicySalePriceCustomerService>();
            _ISalePriceGroupService = IoC.Resolve<ISalePriceGroupService>();
            _IMaterialGoodsService = IoC.Resolve<IMaterialGoodsService>();
            InitializeComponent();
            _policyPriceMaterialGoodsesDb = _ISAPolicyPriceMaterialGoodsService.Query.Where(p => p.SAPolicyPriceSettingID == temp.ID).ToList();
            _policySalePriceGroupsDb = _ISAPolicySalePriceGroupService.Query.Where(p => p.SAPolicyPriceSettingID == temp.ID).ToList();
            _policyPriceTablesDb = _ISAPolicyPriceTableService.Query.Where(p => p.SAPolicyPriceSettingID == temp.ID).ToList();
            if (_policySalePriceGroupsDb.Count > 0)
            {
                foreach (var saPolicySalePriceGroup in _policySalePriceGroupsDb)
                {
                    SAPolicySalePriceGroup @group = saPolicySalePriceGroup;
                    _policySalePriceCustomersDb.AddRange(_ISAPolicySalePriceCustomerService.Query.Where(p => p.SAPolicySalePriceGroupID == @group.ID));
                }
            }

            Text = "Sửa chính sách";
            IsAdd = false;
            _select = temp;
            LoadChung();
            ConfigButton();
            ShowPal();
            ObjandGUI(temp, true);
        }
        #endregion

        #region Hàm xử lý nghiệp vụ
        void LoadChung()
        {
            lblChuy.Text = "Để thiết lập giá bán bạn cần thực hiện các bước sau : \n - Chọn vật tư, hàng hóa \n - Thiết lập nhóm giá bán \n - Lập bảng giá \n - Chọn khách hàng cho các nhóm";
            lstControl = new List<Control> { Panel1, Panel2, Panel3, Panel4, Panel5 };
            lstChonVTTHH = _ISAPolicyPriceSettingService.GetMaterialGoodHts(IsAdd, _select != null ? _select.ID : Guid.Empty).OrderBy(x => x.MaVTHH).ToList();
            ViewDsach(GridChonvattuhanghoa, lstChonVTTHH);
            const string nhom = "Kéo một cột vào đây để nhóm cột";
            GridChonvattuhanghoa.DisplayLayout.GroupByBox.Prompt = nhom;
            lstGroupSalePriceHts = _ISAPolicyPriceSettingService.GetGroupSalePriceHts();
            lstPolicySalePriceCustomerHts = _ISAPolicyPriceSettingService.GetLstChoiceAccountingOBJ();
            lstPriceUpHts = _ISAPolicyPriceSettingService.GetListPriceUpHT(lstChonVTTHH.Where(c => c.Status).ToList());
            dt = ToDataSet(lstPriceUpHts);
            if (!IsAdd) cbkActive.Checked = _select.IsActive == true ? true : false;
            btnSave.Enabled = false;

            ViewDSLapBangGia(GridLapBangGia, dt);

            ViewDsachNhomGB(GridThietlapnhombaogia, lstGroupSalePriceHts);

            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);

            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
        }
        #endregion

        #region Hàm xử lý Grid
        private void ConfigButton()
        {
            if (indexCurrent == 0)
            {//đầu tiên
                btnBack.Enabled = false;
                btnNext.Enabled = true;
            }
            else if (indexCurrent == lstControl.Count - 1)
            {
                btnBack.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnBack.Enabled = true;
                btnNext.Enabled = true;
            }
        }
        private void ShowPal()
        {
            foreach (Control control in lstControl) control.Visible = false;
            lstControl[indexCurrent].Visible = true;
            lstControl[indexCurrent].Dock = DockStyle.Fill;
        }

        #region Cấu hình template
        #region Chọn vật tư hàng hóa
        private void ViewDsach(UltraGrid uGrid, List<MaterialGoodHT> model)
        {
            uGrid.SetDataBinding(model, "");
            //Utils.ConfigGrid(uGrid, "", MauDanhSach());
            Utils.ConfigGrid(uGrid, ConstDatabase.SAPolicyPriceMaterialGoods_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            uGrid.DisplayLayout.Bands[0].Columns["MaVTHH"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["TenVTHH"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["LoaiVTHH"].CellActivation = Activation.NoEdit;
            GetValue(uGrid);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["GiaNhapGanNhat"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["FixedSalePrice"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePrice"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePrice2"], ConstDatabase.Format_TienVND);
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["SalePrice3"], ConstDatabase.Format_TienVND);
            if (!IsAdd) uGrid.DisplayLayout.Bands[0].Columns["Status"].Hidden = true;
        }
        #endregion
        #region Thiết lập nhóm giá bán
        private void ViewDsachNhomGB(UltraGrid uGrid, List<GroupSalePriceHT> model)
        {

            uGrid.SetDataBinding(model.OrderBy(c => c.NhomGia).ToArray(), "");
            //Utils.ConfigGrid(uGrid, MauDanhSach());
            Utils.ConfigGrid(uGrid, ConstDatabase.SAPolicySalePriceGroup_TableName);
            GetValue(uGrid);

            cbbDuaTren.SetDataBinding(lItem, "");
            cbbDuaTren.DisplayLayout.Bands[0].ColHeadersVisible = false;
            uGrid.DisplayLayout.Bands[0].Columns["DuaTren"].ValueList = cbbDuaTren;
            uGrid.DisplayLayout.Bands[0].Columns["DuaTren"].Style = ColumnStyle.DropDownList;

            cbbPhuongPhap.SetDataBinding(lItem2, "");
            cbbPhuongPhap.DisplayLayout.Bands[0].ColHeadersVisible = false;
            uGrid.DisplayLayout.Bands[0].Columns["PhuongPhap"].ValueList = cbbPhuongPhap;
            uGrid.DisplayLayout.Bands[0].Columns["PhuongPhap"].Style = ColumnStyle.DropDownList;
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["PhanTramHoacSTTG"], ConstDatabase.Format_TienVND);

            cbbDuaTren.DropDownStyle = UltraComboStyle.DropDownList;
            cbbPhuongPhap.DropDownStyle = UltraComboStyle.DropDownList;
            if (!IsAdd) uGrid.DisplayLayout.Bands[0].Columns["Status"].Hidden = true;
            uGrid.AfterExitEditMode += new EventHandler(ultraGrid_AfterExitEditMode);
        }

        void ultraGrid_AfterExitEditMode(object sender, EventArgs e)
        {
            //  Unhook from the editor's TextBox.MouseWheel event when edit mode ends
            UltraGrid control = sender as UltraGrid;
            UltraGridCell activeCell = control.ActiveCell;
            control.UpdateDataGrid();
            UltraGridCell cell = GridThietlapnhombaogia.ActiveCell;
            switch (cell.Column.Key)
            {
                case "PhanTramHoacSTTG":
                    if (cell.Row.Cells["PhanTramHoacSTTG"].Value == null)
                    {
                        if ((string)cell.Row.Cells["PhuongPhap"].Value == "Tăng số tiền" || (string)cell.Row.Cells["PhuongPhap"].Value == "Giảm số tiền")
                            cell.Row.Cells["PhanTramHoacSTTG"].Value = 0.FormatNumberic(ConstDatabase.Format_Rate);
                        else cell.Row.Cells["PhanTramHoacSTTG"].Value = 0.FormatNumberic(ConstDatabase.Format_Rate);
                    }
                    break;
            }
            if (activeCell != null)
            {
                if (control.DisplayLayout.Bands[0].Columns.Exists("PhuongPhap"))
                {
                    if ((activeCell.Row.Cells["PhuongPhap"].Value != null) && ((activeCell.Row.Cells["PhuongPhap"].Value.ToString() == "Tăng số tiền") || (activeCell.Row.Cells["PhuongPhap"].Value.ToString() == "Giảm số tiền")))
                    {
                        Utils.FormatNumberic(control.DisplayLayout.Bands[0].Columns["PhanTramHoacSTTG"], ConstDatabase.Format_TienVND);
                    }
                    else
                    {
                        Utils.FormatNumberic(control.DisplayLayout.Bands[0].Columns["PhanTramHoacSTTG"], ConstDatabase.Format_Rate);
                    }
                }
            }
            IsAddColumn = true;
        }
        private static void GetValue(UltraGrid uGrid)
        {
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            // xét style cho các cột có dạng combobox
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            UltraGridColumn ugc = band.Columns["Status"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;
            ugc.SetHeaderCheckedState(uGrid.Rows, false);
        }

        #endregion
        #region Lập bảng giá
        private List<TemplateColumn> MaudsLapBangGia()
        {
            List<TemplateColumn> listTemplate = new List<TemplateColumn>();
            listTemplate = new List<TemplateColumn>
                       {
                           new TemplateColumn
                               {
                                   ColumnName = "Status",
                                   ColumnCaption = "",
                                   ColumnWidth = 60,
                                   ColumnMaxWidth = 60,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 0
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "MaVTHH",
                                   ColumnCaption = "Mã VTHH",
                                   ColumnWidth = 50,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1,
                                   //IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "TenVTHH",
                                   ColumnCaption = "Tên VTHH",
                                   ColumnWidth = 80,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   //IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "NhomVTHH",
                                   ColumnCaption = "Nhóm VTHH",
                                   ColumnWidth = 80,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3,
                                   //IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "GiaBanGanNhat",
                                   ColumnCaption = "Giá nhập gần nhất",
                                   ColumnWidth = 80,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4,
                                   //IsReadOnly = true
                               },
                       };
            int i = 4;
            int k = lstGroupSalePriceHts.Count(itemGroupSalePrice => itemGroupSalePrice.Status);
            foreach (var itemGroupSalePrice in lstGroupSalePriceHts.OrderBy(c => c.NhomGia).ToArray())
            {
                if (itemGroupSalePrice.Status)
                {
                    TemplateColumn templateColumn = new TemplateColumn
                    {
                        ColumnName = itemGroupSalePrice.NhomGia,
                        ColumnCaption = itemGroupSalePrice.NhomGia,
                        ColumnWidth = 200 / k,
                        IsVisible = true,
                        IsVisibleCbb = true,
                        VisiblePosition = i + 1,
                    };
                    listTemplate.Add(templateColumn);
                    i++;
                }
            }
            return listTemplate;
        }
        private void ViewDSLapBangGia(UltraGrid uGrid, DataTable model)
        {
            uGrid.SetDataBinding(model, "");
            Utils.ConfigGrid(uGrid, "", MaudsLapBangGia());
            //Utils.ConfigGrid(uGrid, ConstDatabase.SAPolicyPriceMaterialGoods_TableName);
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns["GiaBanGanNhat"], ConstDatabase.Format_TienVND);
            uGrid.DisplayLayout.Bands[0].Columns["MaVTHH"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["TenVTHH"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["NhomVTHH"].CellActivation = Activation.NoEdit;
            uGrid.DisplayLayout.Bands[0].Columns["GiaBanGanNhat"].CellActivation = Activation.NoEdit;
            //lstGroupSalePriceHts.OrderBy(c => c.NhomGia);
            foreach (var itemGroupSalePrice in lstGroupSalePriceHts)
            {
                if (itemGroupSalePrice.Status)
                {
                    Utils.FormatNumberic(uGrid.DisplayLayout.Bands[0].Columns[itemGroupSalePrice.NhomGia], ConstDatabase.Format_TienVND);
                }
            }
        }
        #endregion
        #region Chọn nhóm khách hàng
        private List<TemplateColumn> MauDSChonKH()
        {
            return new List<TemplateColumn>
                       {

                           new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectCode",
                                   ColumnCaption = "Mã khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectName",
                                   ColumnCaption = "Tên khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectCategory",
                                   ColumnCaption = "Loại",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 3,
                                   IsReadOnly = true
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "AccountinggObjectCategoryCode",
                                   ColumnCaption = "Nhóm khách hàng",
                                   ColumnWidth = 120,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 4,
                                   IsReadOnly = true
                               },



                       };
        }
        private void ViewDSChonKH(UltraGrid uGrid, List<SAPolicySalePriceCustomerHT> model)
        {
            uGrid.SetDataBinding(model, "");
            Utils.ConfigGrid(uGrid, "", MauDSChonKH());
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
        }
        private List<TemplateColumn> mauNhomGiaBAnColumns()
        {
            return new List<TemplateColumn>
                       {

                           new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectCode",
                                   ColumnCaption = "Mã khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 1,
                                   IsReadOnly = true,
                               },
                           new TemplateColumn
                               {
                                   ColumnName = "AccountingObjectName",
                                   ColumnCaption = "Tên khách hàng",
                                   ColumnWidth = 100,
                                   IsVisible = true,
                                   IsVisibleCbb = true,
                                   VisiblePosition = 2,
                                   IsReadOnly = true
                               },
                       };
        }
        private void ViewDSChonKHNhomGIaBAn(UltraGrid uGrid, List<SAPolicySalePriceCustomerHT> model)
        {
            List<SAPolicySalePriceCustomerHT> modelView = new List<SAPolicySalePriceCustomerHT>();
            if (!string.IsNullOrEmpty(cbbNhomGiaBan.Text))
            {
                foreach (var saPolicySalePriceCustomerHt in model)
                {
                    if (saPolicySalePriceCustomerHt.SalePriceGroupCode == cbbNhomGiaBan.Text)
                        modelView.Add(saPolicySalePriceCustomerHt);
                }
            }
            else
            {
                modelView = model;
            }

            uGrid.SetDataBinding(modelView, "");
            Utils.ConfigGrid(uGrid, "", mauNhomGiaBAnColumns());
            uGrid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGrid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGrid.DisplayLayout.Bands[0].Summaries.Clear();
            uGrid.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
        }
        #endregion
        #endregion

        #region Cấu hình Addcolum và trừ cột
        private void AddColumn(DataTable dt, string text)
        {
            DataColumn dc = new DataColumn(text, typeof(decimal))
            {
                ColumnName = text,
                AllowDBNull = true,
                DefaultValue = 0
            };
            dt.Columns.Add(dc);
        }
        private void ClearColumn(DataTable dt, string text)
        {
            int i = -1;
            foreach (DataColumn column in dt.Columns)
            {
                if (text == column.ColumnName)
                {
                    i = column.Ordinal;
                }
            }
            if (i == -1) return;
            dt.Columns.RemoveAt(i);
        }
        public static DataTable ToDataSet<T>(List<T> list)
        {
            System.Type elementType = typeof(T);
            System.Data.DataTable t = new System.Data.DataTable();

            foreach (var propInfo in elementType.GetProperties())
            {
                System.Type colType = System.Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, colType);
            }

            foreach (T item in list)
            {
                System.Data.DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }
                t.Rows.Add(row);
            }
            return t;
        }
        #endregion
        #endregion

        #region Events
        private void btnBack_Click(object sender, EventArgs e)
        {
            indexCurrent -= 1;
            if (indexCurrent == 2)
            {
                if (IsAdd)
                {
                    List<GroupSalePriceHT> lst = lstGroupSalePriceHts.Where(c => c.Status).ToList();
                    foreach (GroupSalePriceHT groupSalePriceHt in lst)
                    {
                        ClearColumn(dt, groupSalePriceHt.NhomGia);
                    }
                    IsAddColumn = true;
                }

            }
            ConfigButton();
            ShowPal();
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (indexCurrent == 0)
            {
                if (IsAdd)
                {
                    List<SAPolicyPriceSetting> policyPriceSettingsDb = _ISAPolicyPriceSettingService.GetPolicyPriceSettings();
                    foreach (var saPolicyPriceSetting in policyPriceSettingsDb)
                    {
                        if (txtTenChinhSachGia.Text == saPolicyPriceSetting.SAPolicyPriceName)
                        {
                            MSG.Warning(string.Format(resSystem.MSG_Catalog, "Mã", "chính sách"));
                            return;
                        }
                    }
                }

                if (txtTenChinhSachGia.Text == "")
                {
                    MSG.Warning(resSystem.MSG_SAPolicyPriceSetting1);
                    return;
                }
                if (!IsAdd && IsLoadMg)
                {
                    //GetSelectedItemMg();
                    //IsLoadMg = false;
                }
            }
            else if (indexCurrent == 1)
            {
                GridChonvattuhanghoa.UpdateData();
                if (lstChonVTTHH.All(c => c.Status != true))
                {
                    MSG.Warning(string.Format(resSystem.MSG_SAPolicyPriceSetting, "VTHH"));
                    return;
                }
                if (!IsAdd && IsLoadSpGroup)
                {
                    GetSelectedSalePriceGroup();
                    IsLoadSpGroup = false;
                }
            }
            else if (indexCurrent == 2)
            {
                GridThietlapnhombaogia.UpdateData();
                if (lstGroupSalePriceHts.All(c => c.Status != true))
                {
                    MSG.Warning(string.Format(resSystem.MSG_SAPolicyPriceSetting, "nhóm giá bán"));
                    return;
                }
                if (IsAddColumn)
                {

                    lstPriceUpHts = _ISAPolicyPriceSettingService.GetListPriceUpHT(lstChonVTTHH.Where(c => c.Status).ToList());
                    dt = ToDataSet(lstPriceUpHts);
                    List<GroupSalePriceHT> lst = lstGroupSalePriceHts.Where(c => c.Status).ToList();
                    foreach (GroupSalePriceHT groupSalePriceHt in lst)
                    {
                        AddColumn(dt, groupSalePriceHt.NhomGia);
                    }
                    IsAddColumn = false;
                    if (lst.Count > 0)
                    {
                        GetPriceInPriceTable(dt, lst, lstChonVTTHH.Where(c => c.Status).ToList());
                    }
                }



                ViewDSLapBangGia(GridLapBangGia, dt);

            }
            else if (indexCurrent == 3)
            {
                if (lstGroupSalePriceHts.Any(c => c.Status))
                {
                    cbbNhomGiaBan.SetDataBinding(
                        lstGroupSalePriceHts.Where(c => c.Status)
                            .Select(c => new { c.NhomGia, c.TenNhomGias }).OrderBy(c => c.NhomGia)
                            .ToList(), "");
                    cbbNhomGiaBan.SelectedRow = cbbNhomGiaBan.Rows[0];
                    cbbNhomGiaBan.DisplayLayout.Bands[0].Columns["NhomGia"].Header.Caption = "Nhóm giá";
                    cbbNhomGiaBan.DisplayLayout.Bands[0].Columns["TenNhomGias"].Header.Caption = "Tên nhóm giá";
                    btnSave.Enabled = true;
                }
                if (!IsAdd && IsLoadCustomer)
                {
                    GetPriceCustomerDb(cbbNhomGiaBan.Text);
                    IsLoadCustomer = false;
                }
                ConfigSelectCancel();
            }
            indexCurrent += 1;
            ConfigButton();
            ShowPal();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            List<SAPolicyPriceSetting> LstChinhSachGia = new List<SAPolicyPriceSetting>();
            List<SAPolicyPriceMaterialGoods> LstChonVatTuHH = new List<SAPolicyPriceMaterialGoods>();
            List<SAPolicySalePriceGroup> LstChonNhomGiaBan = new List<SAPolicySalePriceGroup>();
            List<SAPolicyPriceTable> LstLapBangGia = new List<SAPolicyPriceTable>();
            SAPolicyPriceSetting CsGia = new SAPolicyPriceSetting();

            GridChonKhachHang2.UpdateData();
            if (indexCurrent == 4 && ((ICollection<SAPolicySalePriceCustomerHT>)GridChonKhachHang2.DataSource).Count == 0)
            {
                MSG.Warning(string.Format(resSystem.MSG_SAPolicyPriceSetting, "khách hàng"));
                return;
            }
            try
            {
                _ISAPolicyPriceSettingService.BeginTran();

                #region Tên chính sách

                if (IsAdd)
                {
                    CsGia.ID = Guid.NewGuid();
                    CsGia.SAPolicyPriceName = txtTenChinhSachGia.Text;
                    CsGia.IsActive = cbkActive.Checked == true ? true : false;
                    _ISAPolicyPriceSettingService.CreateNew(CsGia);
                }
                else
                {
                    SAPolicyPriceSetting tempUpdate = _ISAPolicyPriceSettingService.Getbykey(_select.ID);
                    tempUpdate.SAPolicyPriceName = txtTenChinhSachGia.Text;
                    tempUpdate.IsActive = cbkActive.Checked == true ? true : false;
                    _ISAPolicyPriceSettingService.Update(tempUpdate);
                    if (_policySalePriceCustomersDb.Count > 0)
                    {
                        foreach (var saPolicySalePriceCustomer in _policySalePriceCustomersDb)
                        {
                            _ISAPolicySalePriceCustomerService.Delete(saPolicySalePriceCustomer);
                        }
                    }
                    if (_policyPriceMaterialGoodsesDb.Count > 0)
                    {
                        foreach (var saPolicyPriceMaterialGoodse in _policyPriceMaterialGoodsesDb)
                        {
                            _ISAPolicyPriceMaterialGoodsService.Delete(saPolicyPriceMaterialGoodse);
                        }
                    }
                    if (_policySalePriceGroupsDb.Count > 0)
                    {
                        foreach (var saPolicySalePriceGroup in _policySalePriceGroupsDb)
                        {
                            _ISAPolicySalePriceGroupService.Delete(saPolicySalePriceGroup);
                        }
                    }
                    if (_policyPriceTablesDb.Count > 0)
                    {
                        foreach (var saPolicyPriceTable in _policyPriceTablesDb)
                        {
                            _ISAPolicyPriceTableService.Delete(saPolicyPriceTable);
                        }
                    }
                }
                #endregion

                LstChinhSachGia = _ISAPolicyPriceSettingService.GetAll();

                if (LstChinhSachGia.Count > 0)
                {
                    var saPolicyPriceSetting = LstChinhSachGia.FirstOrDefault(c => c.SAPolicyPriceName == txtTenChinhSachGia.Text);
                    if (saPolicyPriceSetting != null)
                    {
                        var chinhSachGiaId = saPolicyPriceSetting.ID;

                        #region Chọn vật tư hàng hóa

                        if (!IsAdd)
                        {
                            //if (_policyPriceMaterialGoodsesDb.Count > 0)
                            //{
                            //    foreach (var saPolicyPriceMaterialGoodse in _policyPriceMaterialGoodsesDb)
                            //    {
                            //        _ISAPolicyPriceMaterialGoodsService.Delete(saPolicyPriceMaterialGoodse);
                            //    }
                            //}
                        }
                        foreach(var x in lstChonVTTHH)
                        {
                            var model = Utils.ListMaterialGoods.FirstOrDefault(c => c.MaterialGoodsCode == x.MaVTHH);
                            model.FixedSalePrice = x.FixedSalePrice;
                            model.SalePrice = x.SalePrice;
                            model.SalePrice2 = x.SalePrice2;
                            model.SalePrice3 = x.SalePrice3;
                            model.PurchasePrice = x.GiaNhapGanNhat;
                            _IMaterialGoodsService.Update(model);

                        }
                        LstChonVatTuHH = (from a in lstChonVTTHH
                                          where a.Status
                                          select new SAPolicyPriceMaterialGoods()
                                          {
                                              MaterialGoodsID = a.ID,
                                              SAPolicyPriceSettingID = chinhSachGiaId,
                                          }).ToList();
                        foreach (var x in LstChonVatTuHH)
                        {
                            x.ID = Guid.NewGuid();
                            _ISAPolicyPriceMaterialGoodsService.CreateNew(x);
                        }

                        #endregion

                        #region Thiết lập nhóm giá bán

                        if (!IsAdd)
                        {
                            //if (_policySalePriceGroupsDb.Count > 0)
                            //{
                            //    foreach (var saPolicySalePriceGroup in _policySalePriceGroupsDb)
                            //    {
                            //        _ISAPolicySalePriceGroupService.Delete(saPolicySalePriceGroup);
                            //    }
                            //}
                        }

                        LstChonNhomGiaBan = (from a in lstGroupSalePriceHts
                                             where a.Status
                                             select new SAPolicySalePriceGroup()
                                             {
                                                 SAPolicyPriceSettingID = chinhSachGiaId,
                                                 SalePriceGroupID = a.Id,
                                                 BasedOn =
                                                     a.DuaTren == "Đơn giá bán 1" ? 0
                                                         : a.DuaTren == "Đơn giá bán 2" ? 1
                                                             : a.DuaTren == "Đơn giá bán 3" ? 2
                                                                : a.DuaTren == "Đơn giá cố định" ? 3
                                                                    : a.DuaTren == "Giá nhập gần nhất" ? 4 : 5,
                                                 Method = a.PhuongPhap == "Tăng số tiền" ? 0
                                                        : a.PhuongPhap == "Giảm số tiền" ? 1
                                                            : a.PhuongPhap == "Tăng %" ? 2 : 3,
                                                 AmountAdjust = a.PhanTramHoacSTTG,
                                             }).ToList();
                        foreach (var x in LstChonNhomGiaBan)
                        {
                            x.ID = Guid.NewGuid();
                            listPolicySalePriceGroup.Add(x);
                            _ISAPolicySalePriceGroupService.CreateNew(x);
                        }

                        #endregion

                        #region Lập bảng giá

                        if (!IsAdd)
                        {
                            //if (_policyPriceTablesDb.Count > 0)
                            //{
                            //    foreach (var saPolicyPriceTable in _policyPriceTablesDb)
                            //    {
                            //        _ISAPolicyPriceTableService.Delete(saPolicyPriceTable);
                            //    }
                            //}
                        }

                        foreach (var row in GridLapBangGia.Rows)
                        {
                            var id = (Guid?)row.Cells["ID"].Value;

                            if (id.HasValue)
                            {
                                foreach (var cell in row.Cells)
                                {
                                    decimal lastPrice = 0;
                                    if (new[] { "ID", "MaVTHH", "TenVTHH", "NhomVTHH" }.Any(col => col.Equals(cell.Column.Key))) continue;
                                    if (cell.Column.Key == "GiaBanGanNhat")
                                    {
                                        lastPrice = (decimal)cell.Value;
                                    }
                                    else
                                    {
                                        var salePrice = lstGroupSalePriceHts.SingleOrDefault(c => c.NhomGia == cell.Column.Key);
                                        var price = (decimal?)cell.Value;
                                        LstLapBangGia.Add(new SAPolicyPriceTable()
                                        {
                                            SAPolicyPriceSettingID = chinhSachGiaId,
                                            MaterialGoodsID = (Guid)id,
                                            SalePiceGroupID = salePrice.Id,
                                            LastPrice = lastPrice,
                                            Price = (decimal)price,
                                        });
                                    }
                                }
                            }
                        }
                        foreach (var y in LstLapBangGia)
                        {
                            _ISAPolicyPriceTableService.CreateNew(y);
                        }

                        #endregion
                    }

                }

                #region Chọn khách hàng

                //if (listSelectPolicySalePriceCustomers.Count > 0)
                //{
                //    foreach (var selectPolicySalePriceCustomer in listSelectPolicySalePriceCustomers)
                //    {
                //        SAPolicySalePriceGroup saPolicySalePriceGroup =
                //            listPolicySalePriceGroup.SingleOrDefault(p => p.SAPolicyPriceSettingID == _select.ID &&
                //                                                          p.SalePriceGroupID == selectPolicySalePriceCustomer.SAPolicySalePriceGroupID);
                //        if (saPolicySalePriceGroup != null)
                //        {
                //            selectPolicySalePriceCustomer.SAPolicySalePriceGroupID = saPolicySalePriceGroup.ID;
                //        }

                //        _ISAPolicySalePriceCustomerService.CreateNew(selectPolicySalePriceCustomer);
                //    }
                //}

                if (lstNhomGiaBAn.Count > 0)
                {
                    foreach (var nhomGiaBan in lstNhomGiaBAn)
                    {
                        foreach (var saPolicySalePriceGroup in LstChonNhomGiaBan)
                        {
                            if (saPolicySalePriceGroup.SalePriceGroupID ==
                                _ISalePriceGroupService.GetSalePriceGroupByCode(nhomGiaBan.SalePriceGroupCode).ID)
                            {
                                SAPolicySalePriceCustomer policySalePriceCustomer = new SAPolicySalePriceCustomer();

                                policySalePriceCustomer.ID = Guid.NewGuid();
                                policySalePriceCustomer.AccountingObjectID = nhomGiaBan.ID;
                                policySalePriceCustomer.SAPolicySalePriceGroupID = saPolicySalePriceGroup.ID;

                                _ISAPolicySalePriceCustomerService.CreateNew(policySalePriceCustomer);
                            }

                        }
                    }
                }

                #endregion

                _ISAPolicyPriceSettingService.CommitTran();
                Utils.ClearCacheByType<MaterialGoodsCustom>();
                this.Close();
                isClose = false;
            }
            catch (Exception ex)
            {
                _ISAPolicyPriceSettingService.RolbackTran();
            }
        }
        private void btnEscape_Click(object sender, EventArgs e)
        {
            Close();
            isClose = true;
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            lstNhomGiaBAn = new List<SAPolicySalePriceCustomerHT>();
            lstNhomGiaBAn.AddRange(lstPolicySalePriceCustomerHts);
            foreach (var saPolicySalePriceCustomerHt in lstNhomGiaBAn)
            {
                saPolicySalePriceCustomerHt.SalePriceGroupCode = cbbNhomGiaBan.Text;
            }
            lstPolicySalePriceCustomerHts.Clear();
            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);
            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
            if (indexCurrent == 3 && lstNhomGiaBAn.Count > 0)
            {
                btnSave.Enabled = true;
            }
            foreach (var saPolicySalePriceCustomerHt in lstNhomGiaBAn)
            {
                SAPolicySalePriceCustomer saPolicySalePriceCustomer = new SAPolicySalePriceCustomer();
                saPolicySalePriceCustomer.ID = Guid.NewGuid();
                saPolicySalePriceCustomer.AccountingObjectID = saPolicySalePriceCustomerHt.ID;
                SalePriceGroup salePriceGroup = _ISalePriceGroupService.GetAll().SingleOrDefault(p => p.SalePriceGroupCode == cbbNhomGiaBan.Text);
                if (salePriceGroup != null)
                    saPolicySalePriceCustomer.SAPolicySalePriceGroupID = salePriceGroup.ID;
                listSelectPolicySalePriceCustomers.Add(saPolicySalePriceCustomer);
            }
            ConfigSelectCancel();
        }
        private void btnCancelAll_Click(object sender, EventArgs e)
        {
            lstPolicySalePriceCustomerHts.AddRange(lstNhomGiaBAn);
            lstNhomGiaBAn.Clear();
            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);
            if (indexCurrent == 3 && lstNhomGiaBAn.Count > 0)
            {
                btnSave.Enabled = true;
            }
            listSelectPolicySalePriceCustomers.Clear();
            ConfigSelectCancel();
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            //lay thang dang duoc chon

            //add vao lstNhomGiaBAn
            if (vitri == -1)
            {
                MSG.Warning("Bạn cần cho khách hàng cho nhóm giá bán");
                return;
            }
            lstPolicySalePriceCustomerHts[vitri].SalePriceGroupCode = cbbNhomGiaBan.Text;
            lstNhomGiaBAn.Add(lstPolicySalePriceCustomerHts[vitri]);
            //xoa thang dang chon o lstPolicySalePriceCustomerHts
            lstPolicySalePriceCustomerHts.RemoveAt(vitri);

            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);
            if (indexCurrent == 3 && lstNhomGiaBAn.Count > 0)
            {
                btnSave.Enabled = true;
            }
            SAPolicySalePriceCustomer saPolicySalePriceCustomer = new SAPolicySalePriceCustomer();
            saPolicySalePriceCustomer.ID = Guid.NewGuid();
            try
            {
                saPolicySalePriceCustomer.AccountingObjectID = lstPolicySalePriceCustomerHts[vitri].ID;
            }
            catch (Exception ex)
            {
                vitri = -1;
            }
            SalePriceGroup salePriceGroup = _ISalePriceGroupService.GetAll().SingleOrDefault(p => p.SalePriceGroupCode == cbbNhomGiaBan.Text);
            if (salePriceGroup != null)
                saPolicySalePriceCustomer.SAPolicySalePriceGroupID = salePriceGroup.ID;
            listSelectPolicySalePriceCustomers.Add(saPolicySalePriceCustomer);
            ConfigSelectCancel();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //lay thang dang duoc chon
            lstPolicySalePriceCustomerHts.Add(lstNhomGiaBAn[vitri1]);
            //add vao lstNhomGiaBAn
            SAPolicySalePriceCustomerHT tempRemove = lstNhomGiaBAn[vitri1];
            lstNhomGiaBAn.RemoveAt(vitri1);
            //xoa thang dang chon o lstPolicySalePriceCustomerHts
            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);
            if (indexCurrent == 3 && lstNhomGiaBAn.Count > 0)
            {
                btnSave.Enabled = true;
            }
            if (tempRemove == null) return;
            SAPolicySalePriceCustomer saPolicySalePriceCustomer =
                listSelectPolicySalePriceCustomers.SingleOrDefault(p => p.AccountingObjectID == tempRemove.ID);
            listSelectPolicySalePriceCustomers.Remove(saPolicySalePriceCustomer);
            ConfigSelectCancel();
        }

        private void GridChonKhachHang1_AfterRowActivate(object sender, EventArgs e)
        {
            vitri = GridChonKhachHang1.ActiveRow.Index;
        }
        private void GridChonKhachHang2_AfterRowActivate(object sender, EventArgs e)
        {
            vitri1 = GridChonKhachHang2.ActiveRow.Index;
        }
        private void cbbNhomGiaBan_ValueChanged(object sender, EventArgs e)
        {
            //if (cbbNhomGiaBan.SelectedRow != null)
            //{
            //    //    var model = cbbNhomGiaBan.SelectedRow.ListObject;
            //    if (!IsAdd && IsLoadCustomer)
            //    {
            //        GetPriceCustomerDb(cbbNhomGiaBan.Text);
            //    }
            //}
            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);

            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
        }
        #endregion

        #region Utils
        private void GetSelectedItemMg()
        {
            if (_policyPriceMaterialGoodsesDb.Count > 0)
            {
                foreach (var itemChonVTHH in lstChonVTTHH)
                {
                    foreach (var itemMgSelect in _policyPriceMaterialGoodsesDb)
                    {
                        if (itemChonVTHH.ID == itemMgSelect.MaterialGoodsID)
                            itemChonVTHH.Status = true;
                    }
                }
            }
        }

        private void GetSelectedSalePriceGroup()
        {
            if (_policySalePriceGroupsDb.Count > 0)
            {
                var querySalePriceGroup = (from spGroup in lstGroupSalePriceHts
                                           join spPriceGroup in _policySalePriceGroupsDb on spGroup.Id equals spPriceGroup.SalePriceGroupID into gj
                                           from subspPriceGroup in gj.DefaultIfEmpty()
                                           select new GroupSalePriceHT
                                           {
                                               Id = spGroup.Id,
                                               NhomGia = spGroup.NhomGia ?? "",
                                               TenNhomGias = spGroup.TenNhomGias ?? "",
                                               DuaTren = subspPriceGroup == null ? ""
                                               : subspPriceGroup.BasedOn == 0 ? "Đơn giá bán 1"
                                               : subspPriceGroup.BasedOn == 1 ? "Đơn giá bán 2"
                                               : subspPriceGroup.BasedOn == 2 ? "Đơn giá bán 3"
                                               : subspPriceGroup.BasedOn == 3 ? "Đơn giá cố định"
                                               : subspPriceGroup.BasedOn == 4 ? "Giá nhập gần nhất"
                                               : subspPriceGroup.BasedOn == 5 ? "Giá chính sách hiện tại" : "",
                                               PhuongPhap = subspPriceGroup == null ? ""
                                               : subspPriceGroup.Method == 0 ? "Tăng số tiền"
                                               : subspPriceGroup.Method == 1 ? "Giảm số tiền"
                                               : subspPriceGroup.Method == 2 ? "Tăng %"
                                               : subspPriceGroup.Method == 3 ? "Giảm %" : "",
                                               PhanTramHoacSTTG = subspPriceGroup == null ? 0
                                               : subspPriceGroup.AmountAdjust,
                                           }).ToList();
                lstGroupSalePriceHts.Clear();
                foreach (var groupSalePriceHt in querySalePriceGroup)
                {
                    lstGroupSalePriceHts.Add(groupSalePriceHt);
                }
                foreach (var itemSlGroup in lstGroupSalePriceHts)
                {
                    foreach (var itemSlGroupSelect in _policySalePriceGroupsDb)
                    {
                        if (itemSlGroup.Id == itemSlGroupSelect.SalePriceGroupID)
                        {
                            itemSlGroup.Status = true;
                        }
                    }
                }
            }
            ViewDsachNhomGB(GridThietlapnhombaogia, lstGroupSalePriceHts);
        }
        #region Tính giá bán
        private void GetPriceInPriceTable(DataTable dt, List<GroupSalePriceHT> lst, List<MaterialGoodHT> lstChonVTTHH)
        {
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {
                for (int j = 5; j < dt.Columns.Count; j++)
                {
                    foreach (GroupSalePriceHT gsht in lst)
                    {
                        if ((dt.Columns[j].ColumnName == gsht.NhomGia))
                        {
                            row[j] = Tinhgia(gsht, lstChonVTTHH[i]);
                            j++;
                        }
                    }
                }
                i++;
            }
        }

        private decimal Tinhgia(GroupSalePriceHT gsht, MaterialGoodHT VTTHH)
        {
            decimal gia = 0;
            if (gsht.DuaTren == "Đơn giá bán 1")
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.SalePrice + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.SalePrice - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.SalePrice + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.SalePrice) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.SalePrice - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.SalePrice) / 100;
                        break;
                    default:
                        gia = VTTHH.SalePrice;
                        break;
                }
            }
            else if (gsht.DuaTren == "Đơn giá bán 2")
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.SalePrice2 + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.SalePrice2 - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.SalePrice2 + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.SalePrice2) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.SalePrice2 - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.SalePrice2) / 100;
                        break;
                    default:
                        gia = VTTHH.SalePrice2;
                        break;
                }
            }
            else if (gsht.DuaTren == "Đơn giá bán 3")
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.SalePrice3 + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.SalePrice3 - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.SalePrice3 + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.SalePrice3) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.SalePrice3 - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.SalePrice3) / 100;
                        break;
                    default:
                        gia = VTTHH.SalePrice3;
                        break;
                }
            }
            else if (gsht.DuaTren == "Đơn giá cố định")
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.FixedSalePrice + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.FixedSalePrice - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.FixedSalePrice + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.FixedSalePrice) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.FixedSalePrice - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.FixedSalePrice) / 100;
                        break;
                    default:
                        gia = VTTHH.FixedSalePrice;
                        break;
                }
            }
            else if (gsht.DuaTren == "Giá nhập gần nhất")
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.GiaNhapGanNhat + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.GiaNhapGanNhat - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.GiaNhapGanNhat + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.GiaNhapGanNhat) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.GiaNhapGanNhat - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.GiaNhapGanNhat) / 100;
                        break;
                    default:
                        gia = VTTHH.GiaNhapGanNhat;
                        break;
                }
            }
            else if (gsht.DuaTren == "Giá chính sách hiện tại")
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.GiaNhapGanNhat + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.GiaNhapGanNhat - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.GiaNhapGanNhat + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.GiaNhapGanNhat) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.GiaNhapGanNhat - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.GiaNhapGanNhat) / 100;
                        break;
                    default:
                        gia = VTTHH.GiaNhapGanNhat;
                        break;
                }
            }
            else
            {
                switch (gsht.PhuongPhap)
                {
                    case "Tăng số tiền":
                        gia = VTTHH.GiaNhapGanNhat + gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Giảm số tiền":
                        gia = VTTHH.GiaNhapGanNhat - gsht.PhanTramHoacSTTG ?? 0;
                        break;
                    case "Tăng %":
                        gia = VTTHH.GiaNhapGanNhat + ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.GiaNhapGanNhat) / 100;
                        break;
                    case "Giảm %":
                        gia = VTTHH.GiaNhapGanNhat - ((gsht.PhanTramHoacSTTG ?? 1) * VTTHH.GiaNhapGanNhat) / 100;
                        break;
                    default:
                        gia = VTTHH.GiaNhapGanNhat;
                        break;
                }
            }
            return gia;
        }
        #endregion
        private List<SAPolicySalePriceCustomer> GetPriceCustomer(Guid temp)
        {
            List<SAPolicySalePriceGroup> lstSalePriceGroupsSelect =
    _ISAPolicySalePriceGroupService.Query.Where(p => p.SAPolicyPriceSettingID == temp).ToList();
            List<SAPolicySalePriceCustomer> policySalePriceCustomers = new List<SAPolicySalePriceCustomer>();
            if (lstSalePriceGroupsSelect.Count > 0)
            {
                foreach (var saPolicySalePriceGroup in lstSalePriceGroupsSelect)
                {
                    SAPolicySalePriceGroup @group = saPolicySalePriceGroup;
                    policySalePriceCustomers.AddRange(_ISAPolicySalePriceCustomerService.Query.Where(p => p.SAPolicySalePriceGroupID == @group.ID));
                }
            }
            return policySalePriceCustomers;
        }

        private void GetPriceCustomerDb(string code)
        {
            //Lấy ra dữ liệu bảng SAPolicySalePriceCustomer(ứng với nhóm chính sách giá)
            List<SAPolicySalePriceCustomer> policySalePriceCustomers = GetPriceCustomer(_select.ID);
            List<SalePriceGroup> salePriceGroups = _ISalePriceGroupService.GetAll();

            var query = (from saPspCus in policySalePriceCustomers
                         from saPspGroup in
                             _policySalePriceGroupsDb.Where(gr => gr.ID == saPspCus.SAPolicySalePriceGroupID).DefaultIfEmpty()
                         from saPrGroup in salePriceGroups.Where(spG => spG.ID == saPspGroup.SalePriceGroupID).DefaultIfEmpty()
                         from saPcusHt in lstPolicySalePriceCustomerHts.Where(cus => cus.ID == saPspCus.AccountingObjectID).DefaultIfEmpty()
                         select new SAPolicySalePriceCustomerHT
                         {
                             ID = saPspCus.AccountingObjectID,
                             AccountingObjectCode = saPcusHt.AccountingObjectCode,
                             AccountingObjectName = saPcusHt.AccountingObjectName,
                             AccountingObjectCategory = saPcusHt.AccountingObjectCategory,
                             AccountinggObjectCategoryCode = saPcusHt.AccountinggObjectCategoryCode,
                             SalePriceGroupCode = saPrGroup.SalePriceGroupCode
                         }).ToList();
            //SalePriceGroup salePriceGroupSelected =
            //    _ISalePriceGroupService.GetSalePriceGroupByCode(code);
            //if (salePriceGroupSelected != null)
            //{
            //List<SAPolicySalePriceGroup> policySalePriceGroups =
            //    _policySalePriceGroupsDb.Where(p => p.SalePriceGroupID == salePriceGroupSelected.ID).ToList();
            // List<SAPolicySalePriceCustomer> policySalePriceCustomersSelect = new List<SAPolicySalePriceCustomer>();
            //foreach (var saPolicySalePriceCustomer in policySalePriceCustomers)
            //{
            //    foreach (var saPolicySalePriceGroup in policySalePriceGroups)
            //    {
            //        if (saPolicySalePriceGroup.ID == saPolicySalePriceCustomer.SAPolicySalePriceGroupID)
            //        {
            //            policySalePriceCustomersSelect.Add(saPolicySalePriceCustomer);
            //        }
            //    }
            //}

            //foreach (var policySalePriceCustomerHt in lstPolicySalePriceCustomerHts)
            //{
            //    foreach (var saPolicySalePriceCustomer in policySalePriceCustomersSelect)
            //    {
            //        if (saPolicySalePriceCustomer.AccountingObjectID == policySalePriceCustomerHt.ID)
            //        {
            //            policySalePriceCustomerHt.SalePriceGroupCode = code;
            //            lstNhomGiaBAn.Add(policySalePriceCustomerHt);
            //        }
            //    }
            //}
            //}
            foreach (var saPolicySalePriceCustomerHt in query)
            {
                lstNhomGiaBAn.Add(saPolicySalePriceCustomerHt);
            }


            if (lstNhomGiaBAn.Count > 0)
            {
                foreach (var nhomGiaBan in lstNhomGiaBAn)
                {
                    var tempPolicySalePriceCustomerHtDel =
                    lstPolicySalePriceCustomerHts.Where(p => p.AccountingObjectCode == nhomGiaBan.AccountingObjectCode).FirstOrDefault();
                    lstPolicySalePriceCustomerHts.Remove(tempPolicySalePriceCustomerHtDel);
                }
            }
            ViewDSChonKHNhomGIaBAn(GridChonKhachHang2, lstNhomGiaBAn);
            ViewDSChonKH(GridChonKhachHang1, lstPolicySalePriceCustomerHts);

        }

        private void ObjandGUI(SAPolicyPriceSetting input, bool isGet)
        {
            if (isGet)
            {
                txtTenChinhSachGia.Text = input.SAPolicyPriceName;
                GetSelectedItemMg();
                GetSelectedSalePriceGroup();
            }
        }

        private void ConfigSelectCancel()
        {
            if (lstNhomGiaBAn.Count > 0)
            {
                btnCancel.Enabled = true;
                btnCancelAll.Enabled = true;
            }
            else
            {
                btnCancel.Enabled = false;
                btnCancelAll.Enabled = false;
            }
            if (lstPolicySalePriceCustomerHts.Count > 0)
            {
                btnSelect.Enabled = true;
                btnSelectAll.Enabled = true;
            }
            else
            {
                btnSelect.Enabled = false;
                btnSelectAll.Enabled = false;
            }
        }
        #endregion

        private void AddNewPricePolicy_Load(object sender, EventArgs e)
        {

        }

        private void ultraLabel2_Click(object sender, EventArgs e)
        {

        }

        private void ultraPictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void GridThietlapnhombaogia_CellChange(object sender, CellEventArgs e)
        {
            //switch (e.Cell.Column.Key)
            //{
            //    case "PhanTramHoacSTTG":
            //        if ((e.Cell.Row.Cells["PhanTramHoacSTTG"].Value == null))
            //        {
            //            e.Cell.Row.Cells["PhanTramHoacSTTG"].Value = 0.FormatNumberic(ConstDatabase.Format_Rate);
            //            GridThietlapnhombaogia.Rows.Refresh(RefreshRow.FireInitializeRow);
            //        }
            //        break;
            //    default:
            //        break;
            //}
        }
    }
}
