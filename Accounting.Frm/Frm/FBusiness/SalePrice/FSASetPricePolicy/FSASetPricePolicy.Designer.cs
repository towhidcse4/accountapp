﻿namespace Accounting
{
    partial class FSASetPricePolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.UltraGridVTHHApDung = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.UltraGridKHApDung = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.UltraGridThietLapChinhSachGia = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraSplitter1 = new Infragistics.Win.Misc.UltraSplitter();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ButGiup = new Infragistics.Win.Misc.UltraButton();
            this.ButIn = new Infragistics.Win.Misc.UltraButton();
            this.ButTaiLai = new Infragistics.Win.Misc.UltraButton();
            this.ButThem = new Infragistics.Win.Misc.UltraButton();
            this.ButXoa = new Infragistics.Win.Misc.UltraButton();
            this.ButSua = new Infragistics.Win.Misc.UltraButton();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridVTHHApDung)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridKHApDung)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridThietLapChinhSachGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            this.cms4Grid.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.UltraGridVTHHApDung);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1010, 234);
            // 
            // UltraGridVTHHApDung
            // 
            this.UltraGridVTHHApDung.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.UltraGridVTHHApDung.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.UltraGridVTHHApDung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UltraGridVTHHApDung.Location = new System.Drawing.Point(0, 0);
            this.UltraGridVTHHApDung.Name = "UltraGridVTHHApDung";
            this.UltraGridVTHHApDung.Size = new System.Drawing.Size(1010, 234);
            this.UltraGridVTHHApDung.TabIndex = 0;
            this.UltraGridVTHHApDung.Text = "Vật tư hàng hóa áp dụng";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.UltraGridKHApDung);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(2, 21);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1010, 234);
            // 
            // UltraGridKHApDung
            // 
            this.UltraGridKHApDung.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UltraGridKHApDung.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.UltraGridKHApDung.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.UltraGridKHApDung.Location = new System.Drawing.Point(0, 3);
            this.UltraGridKHApDung.Name = "UltraGridKHApDung";
            this.UltraGridKHApDung.Size = new System.Drawing.Size(1011, 228);
            this.UltraGridKHApDung.TabIndex = 0;
            this.UltraGridKHApDung.Text = "Khách Hàng Áp Dụng";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.UltraGridThietLapChinhSachGia);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 52);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(1014, 290);
            this.ultraPanel1.TabIndex = 48;
            // 
            // UltraGridThietLapChinhSachGia
            // 
            this.UltraGridThietLapChinhSachGia.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.UltraGridThietLapChinhSachGia.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.UltraGridThietLapChinhSachGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UltraGridThietLapChinhSachGia.Location = new System.Drawing.Point(0, 0);
            this.UltraGridThietLapChinhSachGia.Name = "UltraGridThietLapChinhSachGia";
            this.UltraGridThietLapChinhSachGia.Size = new System.Drawing.Size(1014, 290);
            this.UltraGridThietLapChinhSachGia.TabIndex = 46;
            this.UltraGridThietLapChinhSachGia.Text = "Thiết lập chính sách giá";
            this.UltraGridThietLapChinhSachGia.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.UltraGridThietLapChinhSachGia_BeforeRowActivate);
            this.UltraGridThietLapChinhSachGia.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.UltraGridThietLapChinhSachGia_DoubleClickRow);
            this.UltraGridThietLapChinhSachGia.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGrid_MouseDown);
            // 
            // ultraSplitter1
            // 
            this.ultraSplitter1.BackColor = System.Drawing.SystemColors.Control;
            this.ultraSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraSplitter1.Location = new System.Drawing.Point(0, 342);
            this.ultraSplitter1.Name = "ultraSplitter1";
            this.ultraSplitter1.RestoreExtent = 257;
            this.ultraSplitter1.Size = new System.Drawing.Size(1014, 10);
            this.ultraSplitter1.TabIndex = 47;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 352);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1014, 257);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "1.VTHH áp dụng";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "2.Khách hàng áp dụng";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.VisualStudio2005;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1010, 234);
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ButGiup);
            this.ultraGroupBox5.Controls.Add(this.ButIn);
            this.ultraGroupBox5.Controls.Add(this.ButTaiLai);
            this.ultraGroupBox5.Controls.Add(this.ButThem);
            this.ultraGroupBox5.Controls.Add(this.ButXoa);
            this.ultraGroupBox5.Controls.Add(this.ButSua);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            appearance14.FontData.BoldAsString = "True";
            appearance14.FontData.SizeInPoints = 10F;
            this.ultraGroupBox5.HeaderAppearance = appearance14;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(1014, 52);
            this.ultraGroupBox5.TabIndex = 45;
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ButGiup
            // 
            appearance8.Image = global::Accounting.Properties.Resources.ubtnHelp;
            this.ButGiup.Appearance = appearance8;
            this.ButGiup.Location = new System.Drawing.Point(415, 12);
            this.ButGiup.Name = "ButGiup";
            this.ButGiup.Size = new System.Drawing.Size(75, 30);
            this.ButGiup.TabIndex = 27;
            this.ButGiup.Text = "Giúp";
            // 
            // ButIn
            // 
            appearance9.Image = global::Accounting.Properties.Resources.ubtnPrint;
            this.ButIn.Appearance = appearance9;
            this.ButIn.Location = new System.Drawing.Point(334, 12);
            this.ButIn.Name = "ButIn";
            this.ButIn.Size = new System.Drawing.Size(75, 30);
            this.ButIn.TabIndex = 26;
            this.ButIn.Text = "In";
            // 
            // ButTaiLai
            // 
            appearance10.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.ButTaiLai.Appearance = appearance10;
            this.ButTaiLai.Location = new System.Drawing.Point(253, 12);
            this.ButTaiLai.Name = "ButTaiLai";
            this.ButTaiLai.Size = new System.Drawing.Size(75, 30);
            this.ButTaiLai.TabIndex = 25;
            this.ButTaiLai.Text = "Tải lại";
            this.ButTaiLai.Click += new System.EventHandler(this.ButTaiLai_Click);
            // 
            // ButThem
            // 
            appearance11.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.ButThem.Appearance = appearance11;
            this.ButThem.Location = new System.Drawing.Point(10, 12);
            this.ButThem.Name = "ButThem";
            this.ButThem.Size = new System.Drawing.Size(75, 30);
            this.ButThem.TabIndex = 22;
            this.ButThem.Text = "Thêm";
            this.ButThem.Click += new System.EventHandler(this.ButThem_Click);
            // 
            // ButXoa
            // 
            appearance12.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.ButXoa.Appearance = appearance12;
            this.ButXoa.Location = new System.Drawing.Point(172, 12);
            this.ButXoa.Name = "ButXoa";
            this.ButXoa.Size = new System.Drawing.Size(75, 30);
            this.ButXoa.TabIndex = 21;
            this.ButXoa.Text = "Xóa";
            this.ButXoa.Click += new System.EventHandler(this.ButXoa_Click);
            // 
            // ButSua
            // 
            appearance13.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.ButSua.Appearance = appearance13;
            this.ButSua.Location = new System.Drawing.Point(91, 12);
            this.ButSua.Name = "ButSua";
            this.ButSua.Size = new System.Drawing.Size(75, 30);
            this.ButSua.TabIndex = 20;
            this.ButSua.Text = "Sửa";
            this.ButSua.Click += new System.EventHandler(this.ButSua_Click);
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(153, 114);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(152, 26);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(152, 26);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(152, 26);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(152, 26);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // FSASetPricePolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 609);
            this.Controls.Add(this.ultraPanel1);
            this.Controls.Add(this.ultraSplitter1);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraGroupBox5);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FSASetPricePolicy";
            this.Text = "Thiết lập chính sách giá";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FSASetPricePolicy_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSASetPricePolicy_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridVTHHApDung)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridKHApDung)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UltraGridThietLapChinhSachGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.cms4Grid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinGrid.UltraGrid UltraGridThietLapChinhSachGia;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid UltraGridVTHHApDung;
        private Infragistics.Win.UltraWinGrid.UltraGrid UltraGridKHApDung;
        private Infragistics.Win.Misc.UltraButton ButGiup;
        private Infragistics.Win.Misc.UltraButton ButIn;
        private Infragistics.Win.Misc.UltraButton ButTaiLai;
        private Infragistics.Win.Misc.UltraButton ButThem;
        private Infragistics.Win.Misc.UltraButton ButXoa;
        private Infragistics.Win.Misc.UltraButton ButSua;
        private Infragistics.Win.Misc.UltraSplitter ultraSplitter1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
    }
}