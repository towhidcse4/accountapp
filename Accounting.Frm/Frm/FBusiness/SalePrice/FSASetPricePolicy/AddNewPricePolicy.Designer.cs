﻿namespace Accounting
{
    partial class AddNewPricePolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddNewPricePolicy));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.Panel1 = new Infragistics.Win.Misc.UltraPanel();
            this.txtTenChinhSachGia = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraPictureBox1 = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.lblChuy = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnBack = new Infragistics.Win.Misc.UltraButton();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.btnEscape = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.Panel3 = new Infragistics.Win.Misc.UltraPanel();
            this.GridThietlapnhombaogia = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.Panel2 = new Infragistics.Win.Misc.UltraPanel();
            this.GridChonvattuhanghoa = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.cbkActive = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.Panel4 = new Infragistics.Win.Misc.UltraPanel();
            this.GridLapBangGia = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.Panel5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.GridChonKhachHang2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cbbNhomGiaBan = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.btnCancelAll = new Infragistics.Win.Misc.UltraButton();
            this.btnCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnSelect = new Infragistics.Win.Misc.UltraButton();
            this.btnSelectAll = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.GridChonKhachHang1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.C = new Infragistics.Win.Misc.UltraGroupBox();
            this.Panel1.ClientArea.SuspendLayout();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenChinhSachGia)).BeginInit();
            this.Panel3.ClientArea.SuspendLayout();
            this.Panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridThietlapnhombaogia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.Panel2.ClientArea.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChonvattuhanghoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbkActive)).BeginInit();
            this.Panel4.ClientArea.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridLapBangGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.Panel5.ClientArea.SuspendLayout();
            this.Panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChonKhachHang2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhomGiaBan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChonKhachHang1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            // 
            // Panel1.ClientArea
            // 
            this.Panel1.ClientArea.Controls.Add(this.txtTenChinhSachGia);
            this.Panel1.ClientArea.Controls.Add(this.ultraPictureBox1);
            this.Panel1.ClientArea.Controls.Add(this.lblChuy);
            this.Panel1.ClientArea.Controls.Add(this.ultraLabel1);
            this.Panel1.Location = new System.Drawing.Point(12, 12);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(214, 121);
            this.Panel1.TabIndex = 0;
            // 
            // txtTenChinhSachGia
            // 
            this.txtTenChinhSachGia.Location = new System.Drawing.Point(168, 19);
            this.txtTenChinhSachGia.Name = "txtTenChinhSachGia";
            this.txtTenChinhSachGia.Size = new System.Drawing.Size(591, 21);
            this.txtTenChinhSachGia.TabIndex = 1;
            // 
            // ultraPictureBox1
            // 
            appearance1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraPictureBox1.Appearance = appearance1;
            this.ultraPictureBox1.BorderShadowColor = System.Drawing.Color.Empty;
            this.ultraPictureBox1.Image = ((object)(resources.GetObject("ultraPictureBox1.Image")));
            this.ultraPictureBox1.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.ultraPictureBox1.Location = new System.Drawing.Point(93, 46);
            this.ultraPictureBox1.Name = "ultraPictureBox1";
            this.ultraPictureBox1.Size = new System.Drawing.Size(69, 69);
            this.ultraPictureBox1.TabIndex = 2;
            // 
            // lblChuy
            // 
            this.lblChuy.Location = new System.Drawing.Point(168, 46);
            this.lblChuy.Name = "lblChuy";
            this.lblChuy.Size = new System.Drawing.Size(306, 75);
            this.lblChuy.TabIndex = 0;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(30, 17);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(132, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Tên chính sách giá (*)";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.ubtnBack;
            this.btnBack.Appearance = appearance3;
            this.btnBack.Location = new System.Drawing.Point(525, 586);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 30);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "Quay lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Right;
            this.btnNext.Appearance = appearance4;
            this.btnNext.Location = new System.Drawing.Point(629, 586);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(98, 30);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Tiếp theo";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnEscape.Appearance = appearance5;
            this.btnEscape.Location = new System.Drawing.Point(837, 586);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(98, 30);
            this.btnEscape.TabIndex = 4;
            this.btnEscape.Text = "Hủy bỏ";
            this.btnEscape.Click += new System.EventHandler(this.btnEscape_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance6;
            this.btnSave.Location = new System.Drawing.Point(733, 586);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Panel3
            // 
            // 
            // Panel3.ClientArea
            // 
            this.Panel3.ClientArea.Controls.Add(this.GridThietlapnhombaogia);
            this.Panel3.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.Panel3.Location = new System.Drawing.Point(247, 12);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(155, 121);
            this.Panel3.TabIndex = 0;
            // 
            // GridThietlapnhombaogia
            // 
            this.GridThietlapnhombaogia.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridThietlapnhombaogia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridThietlapnhombaogia.Location = new System.Drawing.Point(0, 28);
            this.GridThietlapnhombaogia.Name = "GridThietlapnhombaogia";
            this.GridThietlapnhombaogia.Size = new System.Drawing.Size(155, 93);
            this.GridThietlapnhombaogia.TabIndex = 48;
            this.GridThietlapnhombaogia.Text = "ultraGrid2";
            this.GridThietlapnhombaogia.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridThietlapnhombaogia_CellChange);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            appearance7.FontData.BoldAsString = "True";
            appearance7.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance7;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(155, 28);
            this.ultraGroupBox1.TabIndex = 47;
            this.ultraGroupBox1.Text = "Thiết lập nhóm giá bán";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // Panel2
            // 
            // 
            // Panel2.ClientArea
            // 
            this.Panel2.ClientArea.Controls.Add(this.GridChonvattuhanghoa);
            this.Panel2.ClientArea.Controls.Add(this.ultraGroupBox5);
            this.Panel2.Location = new System.Drawing.Point(408, 12);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(257, 136);
            this.Panel2.TabIndex = 0;
            // 
            // GridChonvattuhanghoa
            // 
            this.GridChonvattuhanghoa.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridChonvattuhanghoa.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.GridChonvattuhanghoa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridChonvattuhanghoa.Location = new System.Drawing.Point(0, 28);
            this.GridChonvattuhanghoa.Name = "GridChonvattuhanghoa";
            this.GridChonvattuhanghoa.Size = new System.Drawing.Size(257, 108);
            this.GridChonvattuhanghoa.TabIndex = 47;
            this.GridChonvattuhanghoa.Text = "ultraGrid1";
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            appearance8.FontData.BoldAsString = "True";
            appearance8.FontData.SizeInPoints = 10F;
            this.ultraGroupBox5.HeaderAppearance = appearance8;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(257, 28);
            this.ultraGroupBox5.TabIndex = 46;
            this.ultraGroupBox5.Text = "Chọn vật tư, hàng hóa";
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.cbkActive);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 572);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(947, 56);
            this.ultraPanel1.TabIndex = 5;
            // 
            // cbkActive
            // 
            this.cbkActive.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbkActive.Location = new System.Drawing.Point(32, 18);
            this.cbkActive.Name = "cbkActive";
            this.cbkActive.Size = new System.Drawing.Size(156, 22);
            this.cbkActive.TabIndex = 43;
            this.cbkActive.Text = "Ngừng hoạt động";
            // 
            // Panel4
            // 
            // 
            // Panel4.ClientArea
            // 
            this.Panel4.ClientArea.Controls.Add(this.GridLapBangGia);
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.Panel4.Location = new System.Drawing.Point(733, 29);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(162, 84);
            this.Panel4.TabIndex = 6;
            // 
            // GridLapBangGia
            // 
            this.GridLapBangGia.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridLapBangGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridLapBangGia.Location = new System.Drawing.Point(0, 28);
            this.GridLapBangGia.Name = "GridLapBangGia";
            this.GridLapBangGia.Size = new System.Drawing.Size(162, 56);
            this.GridLapBangGia.TabIndex = 47;
            this.GridLapBangGia.Text = "ultraGrid3";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance9;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(162, 28);
            this.ultraGroupBox2.TabIndex = 46;
            this.ultraGroupBox2.Text = "Lập bảng giá";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // Panel5
            // 
            // 
            // Panel5.ClientArea
            // 
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.Panel5.ClientArea.Controls.Add(this.btnCancelAll);
            this.Panel5.ClientArea.Controls.Add(this.btnCancel);
            this.Panel5.ClientArea.Controls.Add(this.btnSelect);
            this.Panel5.ClientArea.Controls.Add(this.btnSelectAll);
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.Panel5.ClientArea.Controls.Add(this.C);
            this.Panel5.Location = new System.Drawing.Point(32, 154);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(828, 391);
            this.Panel5.TabIndex = 7;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.GridChonKhachHang2);
            this.ultraGroupBox4.Controls.Add(this.cbbNhomGiaBan);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraGroupBox4.Location = new System.Drawing.Point(453, 28);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(375, 363);
            this.ultraGroupBox4.TabIndex = 49;
            this.ultraGroupBox4.Text = "Nhóm giá bán";
            // 
            // GridChonKhachHang2
            // 
            this.GridChonKhachHang2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridChonKhachHang2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridChonKhachHang2.Location = new System.Drawing.Point(6, 47);
            this.GridChonKhachHang2.Name = "GridChonKhachHang2";
            this.GridChonKhachHang2.Size = new System.Drawing.Size(363, 310);
            this.GridChonKhachHang2.TabIndex = 50;
            this.GridChonKhachHang2.Text = "ultraGrid5";
            this.GridChonKhachHang2.AfterRowActivate += new System.EventHandler(this.GridChonKhachHang2_AfterRowActivate);
            // 
            // cbbNhomGiaBan
            // 
            this.cbbNhomGiaBan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbbNhomGiaBan.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.cbbNhomGiaBan.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbNhomGiaBan.Location = new System.Drawing.Point(6, 19);
            this.cbbNhomGiaBan.Name = "cbbNhomGiaBan";
            this.cbbNhomGiaBan.Size = new System.Drawing.Size(363, 22);
            this.cbbNhomGiaBan.TabIndex = 0;
            this.cbbNhomGiaBan.ValueChanged += new System.EventHandler(this.cbbNhomGiaBan_ValueChanged);
            // 
            // btnCancelAll
            // 
            this.btnCancelAll.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancelAll.Location = new System.Drawing.Point(399, 228);
            this.btnCancelAll.Name = "btnCancelAll";
            this.btnCancelAll.Size = new System.Drawing.Size(33, 23);
            this.btnCancelAll.TabIndex = 48;
            this.btnCancelAll.Text = "<<";
            this.btnCancelAll.Click += new System.EventHandler(this.btnCancelAll_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancel.Location = new System.Drawing.Point(399, 199);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(33, 23);
            this.btnCancel.TabIndex = 48;
            this.btnCancel.Text = "<";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSelect.Location = new System.Drawing.Point(399, 170);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(33, 23);
            this.btnSelect.TabIndex = 48;
            this.btnSelect.Text = ">";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSelectAll.Location = new System.Drawing.Point(399, 141);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(33, 23);
            this.btnSelectAll.TabIndex = 48;
            this.btnSelectAll.Text = ">>";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox3.Controls.Add(this.GridChonKhachHang1);
            this.ultraGroupBox3.Location = new System.Drawing.Point(10, 28);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(375, 360);
            this.ultraGroupBox3.TabIndex = 47;
            this.ultraGroupBox3.Text = "Danh sách khách hàng";
            // 
            // GridChonKhachHang1
            // 
            this.GridChonKhachHang1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridChonKhachHang1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridChonKhachHang1.Location = new System.Drawing.Point(6, 19);
            this.GridChonKhachHang1.Name = "GridChonKhachHang1";
            this.GridChonKhachHang1.Size = new System.Drawing.Size(363, 335);
            this.GridChonKhachHang1.TabIndex = 49;
            this.GridChonKhachHang1.Text = "ultraGrid4";
            this.GridChonKhachHang1.AfterRowActivate += new System.EventHandler(this.GridChonKhachHang1_AfterRowActivate);
            // 
            // C
            // 
            this.C.Dock = System.Windows.Forms.DockStyle.Top;
            appearance10.FontData.BoldAsString = "True";
            appearance10.FontData.SizeInPoints = 10F;
            this.C.HeaderAppearance = appearance10;
            this.C.Location = new System.Drawing.Point(0, 0);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(828, 28);
            this.C.TabIndex = 46;
            this.C.Text = "Chọn khách hàng cho các nhóm";
            this.C.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // AddNewPricePolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 628);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnEscape);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "AddNewPricePolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chính sách giá";
            this.Panel1.ClientArea.ResumeLayout(false);
            this.Panel1.ClientArea.PerformLayout();
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenChinhSachGia)).EndInit();
            this.Panel3.ClientArea.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridThietlapnhombaogia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.Panel2.ClientArea.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridChonvattuhanghoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbkActive)).EndInit();
            this.Panel4.ClientArea.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridLapBangGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.Panel5.ClientArea.ResumeLayout(false);
            this.Panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChonKhachHang2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNhomGiaBan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridChonKhachHang1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnBack;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton btnEscape;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraPanel Panel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTenChinhSachGia;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox ultraPictureBox1;
        private Infragistics.Win.Misc.UltraLabel lblChuy;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraPanel Panel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridThietlapnhombaogia;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraPanel Panel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridChonvattuhanghoa;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel Panel4;
        private Infragistics.Win.Misc.UltraPanel Panel5;
        private Infragistics.Win.Misc.UltraGroupBox C;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridLapBangGia;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton btnCancelAll;
        private Infragistics.Win.Misc.UltraButton btnCancel;
        private Infragistics.Win.Misc.UltraButton btnSelect;
        private Infragistics.Win.Misc.UltraButton btnSelectAll;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridChonKhachHang1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbNhomGiaBan;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridChonKhachHang2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor cbkActive;
    }
}