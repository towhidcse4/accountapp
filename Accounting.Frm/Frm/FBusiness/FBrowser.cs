﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FBrowser<T> : CustormForm
    {
        public T select = (T)Activator.CreateInstance(typeof(T));
        public FBrowser(IList<T> listInput, int typeID, T input)
        {
            InitializeComponent();
            int? typeGroupID = Utils.ListType.FirstOrDefault(p => p.ID == typeID).TypeGroupID;
            string nameTable = String.Empty;
            foreach (KeyValuePair<string, Item> item in Utils.DicBrowser)
            {
                if (item.Key.Equals(typeGroupID.ToString()))
                {
                    nameTable = item.Value.Value;
                    break;
                }
            }

            //string key = Activator.CreateInstance(typeof(T)).HasProperty("PostedDate") ? "PostedDate" : "Date";
            //listInput = listInput.OrderByStand(key);

            uGrid.DataSource = listInput;

            this.uGrid.DisplayLayout.EmptyRowSettings.Style =Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell; //thêm dòng trống để tìm kiếm

            Utils.ConfigGrid(uGrid, nameTable);
            Utils.ConfigSummaryOfGrid(uGrid);

            foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
            {
                column.ConfigColumnByNumberic();
                this.ConfigEachColumn4Grid(0, column, uGrid);
            }
            uGrid.ActiveRow =
                uGrid.Rows.FirstOrDefault(r => (Guid?)r.Cells["ID"].Value == input.GetProperty<T, Guid?>("ID"));
            if (uGrid.ActiveRow != null)
            {
                uGrid.ActiveRow.Selected = true;
                uGrid.ActiveRow.ScrollRowToMiddle();
            }
        }

        private void utmUtils_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "btnClose":
                    this.DialogResult = DialogResult.No;
                    this.Close();
                    break;
                case "btnApply":
                    this.DialogResult = DialogResult.OK;
                    select = (T)uGrid.ActiveRow.ListObject;
                    this.Close();
                    break;
            }
        }

        private void FBrowser_Load(object sender, EventArgs e)
        {

        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            select = (T)uGrid.ActiveRow.ListObject;
            this.Close();
        }

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            if (grid.Selected.Rows.Count < 1) return;
            object listObject = grid.Selected.Rows[grid.Selected.Rows.Count - 1].ListObject;
            if (listObject == null) return;
            T _select = (T)listObject;
            if (_select.HasProperty("Recorded"))
            {
                var recorded = _select.GetProperty<T, bool?>("Recorded");
                if (recorded.HasValue)
                    grid.DisplayLayout.Override.ActiveRowAppearance.ForeColor = recorded.Value ? Utils.DefaultRowSelectedColor : Utils.GetColorNotRecorded();
            }
        }
    }
}
