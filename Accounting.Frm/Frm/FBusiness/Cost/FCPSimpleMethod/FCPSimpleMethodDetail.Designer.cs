﻿namespace Accounting
{
    partial class FCPSimpleMethodDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            this.btnBack = new Infragistics.Win.Misc.UltraButton();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.btnEscape = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnUpdateImport = new Infragistics.Win.Misc.UltraButton();
            this.btnUpdateExport = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridCostset = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Panel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCpExpenseList3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Panel3 = new Infragistics.Win.Misc.UltraPanel();
            this.Panel4 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.btnAttribution = new Infragistics.Win.Misc.UltraButton();
            this.uGridAllocationGeneralExpenseResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridAllocationGeneralExpense = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.Panel2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridCpExpenseList2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.Panel6 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridCosting = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.uGridUncomplete = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridUncompleteDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDetermine = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.Panel5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox8 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCostset)).BeginInit();
            this.Panel1.ClientArea.SuspendLayout();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList3)).BeginInit();
            this.Panel3.ClientArea.SuspendLayout();
            this.Panel3.SuspendLayout();
            this.Panel4.ClientArea.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpenseResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod4)).BeginInit();
            this.Panel2.ClientArea.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod2)).BeginInit();
            this.Panel6.ClientArea.SuspendLayout();
            this.Panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCosting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod6)).BeginInit();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncomplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncompleteDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.Panel5.ClientArea.SuspendLayout();
            this.Panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).BeginInit();
            this.ultraGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod5)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance33.Image = global::Accounting.Properties.Resources.ubtnBack;
            this.btnBack.Appearance = appearance33;
            this.btnBack.Location = new System.Drawing.Point(445, 14);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 30);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "Quay lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance34.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Right;
            this.btnNext.Appearance = appearance34;
            this.btnNext.Location = new System.Drawing.Point(549, 14);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(98, 30);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Tiếp theo";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnEscape.Appearance = appearance35;
            this.btnEscape.Location = new System.Drawing.Point(757, 525);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(98, 30);
            this.btnEscape.TabIndex = 4;
            this.btnEscape.Text = "Hủy bỏ";
            this.btnEscape.Click += new System.EventHandler(this.btnEscape_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance36;
            this.btnSave.Location = new System.Drawing.Point(653, 14);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdateImport
            // 
            this.btnUpdateImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateImport.Location = new System.Drawing.Point(15, 14);
            this.btnUpdateImport.Name = "btnUpdateImport";
            this.btnUpdateImport.Size = new System.Drawing.Size(142, 30);
            this.btnUpdateImport.TabIndex = 5;
            this.btnUpdateImport.Text = "Cập nhật giá nhập kho";
            this.btnUpdateImport.Click += new System.EventHandler(this.btnUpdateImport_Click);
            // 
            // btnUpdateExport
            // 
            this.btnUpdateExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateExport.Location = new System.Drawing.Point(163, 14);
            this.btnUpdateExport.Name = "btnUpdateExport";
            this.btnUpdateExport.Size = new System.Drawing.Size(140, 30);
            this.btnUpdateExport.TabIndex = 6;
            this.btnUpdateExport.Text = "Cập nhật giá xuất kho";
            this.btnUpdateExport.Click += new System.EventHandler(this.btnUpdateExport_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnNext);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnBack);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnUpdateImport);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnUpdateExport);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 511);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(865, 56);
            this.ultraPanel1.TabIndex = 5;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.dtEndDate);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.txtCPPeriod);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox2.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance44.FontData.BoldAsString = "True";
            appearance44.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance44;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(280, 118);
            this.ultraGroupBox2.TabIndex = 47;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dtEndDate
            // 
            appearance37.TextHAlignAsString = "Center";
            appearance37.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance37;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(575, 10);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(99, 21);
            this.dtEndDate.TabIndex = 67;
            this.dtEndDate.Value = null;
            this.dtEndDate.ValueChanged += new System.EventHandler(this.dtEndDate_ValueChanged);
            // 
            // ultraLabel3
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            appearance38.TextHAlignAsString = "Left";
            appearance38.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance38;
            this.ultraLabel3.Location = new System.Drawing.Point(9, 14);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(27, 17);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Kỳ";
            // 
            // txtCPPeriod
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.txtCPPeriod.Appearance = appearance7;
            this.txtCPPeriod.AutoSize = false;
            this.txtCPPeriod.Location = new System.Drawing.Point(147, 52);
            this.txtCPPeriod.Name = "txtCPPeriod";
            this.txtCPPeriod.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod.TabIndex = 64;
            // 
            // ultraLabel5
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            appearance39.TextHAlignAsString = "Left";
            appearance39.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance39;
            this.ultraLabel5.Location = new System.Drawing.Point(8, 52);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel5.TabIndex = 63;
            this.ultraLabel5.Text = "Tên kỳ tính giá thành";
            // 
            // ultraLabel6
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            appearance40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance40.TextHAlignAsString = "Left";
            appearance40.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance40;
            this.ultraLabel6.Location = new System.Drawing.Point(8, 85);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel6.TabIndex = 62;
            this.ultraLabel6.Text = "Đối tượng tập hợp chi phí";
            // 
            // ultraLabel7
            // 
            appearance41.BackColor = System.Drawing.Color.Transparent;
            appearance41.TextHAlignAsString = "Left";
            appearance41.TextVAlignAsString = "Bottom";
            this.ultraLabel7.Appearance = appearance41;
            this.ultraLabel7.Location = new System.Drawing.Point(306, 14);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(48, 17);
            this.ultraLabel7.TabIndex = 57;
            this.ultraLabel7.Text = "Từ ngày";
            // 
            // ultraLabel8
            // 
            appearance42.BackColor = System.Drawing.Color.Transparent;
            appearance42.TextHAlignAsString = "Left";
            appearance42.TextVAlignAsString = "Bottom";
            this.ultraLabel8.Appearance = appearance42;
            this.ultraLabel8.Location = new System.Drawing.Point(496, 15);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(57, 17);
            this.ultraLabel8.TabIndex = 58;
            this.ultraLabel8.Text = "Đến ngày";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(41, 10);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(240, 22);
            this.cbbDateTime.TabIndex = 59;
            // 
            // dtBeginDate
            // 
            appearance43.TextHAlignAsString = "Center";
            appearance43.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance43;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(374, 10);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(99, 21);
            this.dtBeginDate.TabIndex = 61;
            this.dtBeginDate.Value = null;
            this.dtBeginDate.ValueChanged += new System.EventHandler(this.dtBeginDate_ValueChanged);
            // 
            // uGridCostset
            // 
            this.uGridCostset.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCostset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCostset.Location = new System.Drawing.Point(0, 118);
            this.uGridCostset.Name = "uGridCostset";
            this.uGridCostset.Size = new System.Drawing.Size(280, 108);
            this.uGridCostset.TabIndex = 48;
            this.uGridCostset.Text = "ultraGrid2";
            // 
            // Panel1
            // 
            // 
            // Panel1.ClientArea
            // 
            this.Panel1.ClientArea.Controls.Add(this.uGridCostset);
            this.Panel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.Panel1.Location = new System.Drawing.Point(6, 3);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(280, 226);
            this.Panel1.TabIndex = 6;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.txtCPPeriod3);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            appearance46.FontData.BoldAsString = "True";
            appearance46.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance46;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(261, 62);
            this.ultraGroupBox3.TabIndex = 47;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod3
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.txtCPPeriod3.Appearance = appearance14;
            this.txtCPPeriod3.AutoSize = false;
            this.txtCPPeriod3.Location = new System.Drawing.Point(163, 20);
            this.txtCPPeriod3.Name = "txtCPPeriod3";
            this.txtCPPeriod3.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod3.TabIndex = 64;
            // 
            // ultraLabel1
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextHAlignAsString = "Left";
            appearance45.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance45;
            this.ultraLabel1.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel1.TabIndex = 63;
            this.ultraLabel1.Text = "Tên kỳ tính giá thành";
            // 
            // uGridCpExpenseList3
            // 
            this.uGridCpExpenseList3.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCpExpenseList3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCpExpenseList3.Location = new System.Drawing.Point(0, 62);
            this.uGridCpExpenseList3.Name = "uGridCpExpenseList3";
            this.uGridCpExpenseList3.Size = new System.Drawing.Size(261, 167);
            this.uGridCpExpenseList3.TabIndex = 48;
            this.uGridCpExpenseList3.Text = "ultraGrid3";
            this.uGridCpExpenseList3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGridCpExpenseList3_MouseDown);
            // 
            // Panel3
            // 
            // 
            // Panel3.ClientArea
            // 
            this.Panel3.ClientArea.Controls.Add(this.uGridCpExpenseList3);
            this.Panel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.Panel3.Location = new System.Drawing.Point(604, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(261, 229);
            this.Panel3.TabIndex = 8;
            // 
            // Panel4
            // 
            // 
            // Panel4.ClientArea
            // 
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox5);
            this.Panel4.ClientArea.Controls.Add(this.uGridAllocationGeneralExpenseResult);
            this.Panel4.ClientArea.Controls.Add(this.uGridAllocationGeneralExpense);
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.Panel4.Location = new System.Drawing.Point(0, 247);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(246, 258);
            this.Panel4.TabIndex = 9;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox5.Controls.Add(this.btnAttribution);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 48);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(246, 35);
            this.ultraGroupBox5.TabIndex = 53;
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel9
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            appearance50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance50.TextHAlignAsString = "Left";
            appearance50.TextVAlignAsString = "Bottom";
            this.ultraLabel9.Appearance = appearance50;
            this.ultraLabel9.Location = new System.Drawing.Point(6, 8);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel9.TabIndex = 57;
            this.ultraLabel9.Text = "Kết quả phân bổ";
            // 
            // btnAttribution
            // 
            this.btnAttribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAttribution.Location = new System.Drawing.Point(155, 6);
            this.btnAttribution.Name = "btnAttribution";
            this.btnAttribution.Size = new System.Drawing.Size(75, 23);
            this.btnAttribution.TabIndex = 10;
            this.btnAttribution.Text = "Phân bổ";
            this.btnAttribution.Click += new System.EventHandler(this.btnAttribution_Click);
            // 
            // uGridAllocationGeneralExpenseResult
            // 
            this.uGridAllocationGeneralExpenseResult.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocationGeneralExpenseResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGridAllocationGeneralExpenseResult.Location = new System.Drawing.Point(0, 83);
            this.uGridAllocationGeneralExpenseResult.Name = "uGridAllocationGeneralExpenseResult";
            this.uGridAllocationGeneralExpenseResult.Size = new System.Drawing.Size(246, 175);
            this.uGridAllocationGeneralExpenseResult.TabIndex = 54;
            this.uGridAllocationGeneralExpenseResult.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridAllocationGeneralExpenseResult.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpenseResult_AfterCellUpdate);
            this.uGridAllocationGeneralExpenseResult.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridAllocationGeneralExpenseResult_BeforeExitEditMode);
            // 
            // uGridAllocationGeneralExpense
            // 
            this.uGridAllocationGeneralExpense.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocationGeneralExpense.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridAllocationGeneralExpense.Location = new System.Drawing.Point(0, 60);
            this.uGridAllocationGeneralExpense.Name = "uGridAllocationGeneralExpense";
            this.uGridAllocationGeneralExpense.Size = new System.Drawing.Size(246, 235);
            this.uGridAllocationGeneralExpense.TabIndex = 48;
            this.uGridAllocationGeneralExpense.Text = "ultraGrid4";
            this.uGridAllocationGeneralExpense.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpense_AfterCellUpdate);
            this.uGridAllocationGeneralExpense.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpense_CellChange);
            this.uGridAllocationGeneralExpense.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridAllocationGeneralExpense_BeforeExitEditMode);
            this.uGridAllocationGeneralExpense.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGridAllocationGeneralExpense_MouseDown);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox4.Controls.Add(this.txtCPPeriod4);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            appearance49.FontData.BoldAsString = "True";
            appearance49.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance49;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(246, 60);
            this.ultraGroupBox4.TabIndex = 47;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel2
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            appearance47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance47.TextHAlignAsString = "Left";
            appearance47.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance47;
            this.ultraLabel2.Location = new System.Drawing.Point(24, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel2.TabIndex = 65;
            this.ultraLabel2.Text = "Chi phí phân bổ";
            // 
            // txtCPPeriod4
            // 
            appearance19.TextVAlignAsString = "Middle";
            this.txtCPPeriod4.Appearance = appearance19;
            this.txtCPPeriod4.AutoSize = false;
            this.txtCPPeriod4.Location = new System.Drawing.Point(163, 12);
            this.txtCPPeriod4.Name = "txtCPPeriod4";
            this.txtCPPeriod4.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod4.TabIndex = 64;
            // 
            // ultraLabel4
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            appearance48.TextHAlignAsString = "Left";
            appearance48.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance48;
            this.ultraLabel4.Location = new System.Drawing.Point(24, 12);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel4.TabIndex = 63;
            this.ultraLabel4.Text = "Tên kỳ tính giá thành";
            // 
            // Panel2
            // 
            // 
            // Panel2.ClientArea
            // 
            this.Panel2.ClientArea.Controls.Add(this.uGridCpExpenseList2);
            this.Panel2.ClientArea.Controls.Add(this.ultraGroupBox6);
            this.Panel2.Location = new System.Drawing.Point(292, 0);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(261, 229);
            this.Panel2.TabIndex = 10;
            // 
            // uGridCpExpenseList2
            // 
            this.uGridCpExpenseList2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCpExpenseList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCpExpenseList2.Location = new System.Drawing.Point(0, 62);
            this.uGridCpExpenseList2.Name = "uGridCpExpenseList2";
            this.uGridCpExpenseList2.Size = new System.Drawing.Size(261, 167);
            this.uGridCpExpenseList2.TabIndex = 48;
            this.uGridCpExpenseList2.Text = "ultraGrid5";
            this.uGridCpExpenseList2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGridCpExpenseList2_MouseDown);
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.txtCPPeriod2);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            appearance52.FontData.BoldAsString = "True";
            appearance52.FontData.SizeInPoints = 10F;
            this.ultraGroupBox6.HeaderAppearance = appearance52;
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(261, 62);
            this.ultraGroupBox6.TabIndex = 47;
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod2
            // 
            appearance22.TextVAlignAsString = "Middle";
            this.txtCPPeriod2.Appearance = appearance22;
            this.txtCPPeriod2.AutoSize = false;
            this.txtCPPeriod2.Location = new System.Drawing.Point(163, 20);
            this.txtCPPeriod2.Name = "txtCPPeriod2";
            this.txtCPPeriod2.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod2.TabIndex = 64;
            // 
            // ultraLabel10
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            appearance51.TextHAlignAsString = "Left";
            appearance51.TextVAlignAsString = "Bottom";
            this.ultraLabel10.Appearance = appearance51;
            this.ultraLabel10.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel10.TabIndex = 63;
            this.ultraLabel10.Text = "Tên kỳ tính giá thành";
            // 
            // Panel6
            // 
            // 
            // Panel6.ClientArea
            // 
            this.Panel6.ClientArea.Controls.Add(this.uGridCosting);
            this.Panel6.ClientArea.Controls.Add(this.ultraGroupBox7);
            this.Panel6.Location = new System.Drawing.Point(603, 247);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(261, 258);
            this.Panel6.TabIndex = 56;
            // 
            // uGridCosting
            // 
            this.uGridCosting.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCosting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCosting.Location = new System.Drawing.Point(0, 62);
            this.uGridCosting.Name = "uGridCosting";
            this.uGridCosting.Size = new System.Drawing.Size(261, 196);
            this.uGridCosting.TabIndex = 48;
            this.uGridCosting.Text = "uGridCosting";
            this.uGridCosting.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCosting_AfterCellUpdate);
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Controls.Add(this.txtCPPeriod6);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            appearance54.FontData.BoldAsString = "True";
            appearance54.FontData.SizeInPoints = 10F;
            this.ultraGroupBox7.HeaderAppearance = appearance54;
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(261, 62);
            this.ultraGroupBox7.TabIndex = 47;
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod6
            // 
            appearance25.TextVAlignAsString = "Middle";
            this.txtCPPeriod6.Appearance = appearance25;
            this.txtCPPeriod6.AutoSize = false;
            this.txtCPPeriod6.Location = new System.Drawing.Point(163, 20);
            this.txtCPPeriod6.Name = "txtCPPeriod6";
            this.txtCPPeriod6.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod6.TabIndex = 64;
            // 
            // ultraLabel11
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            appearance53.TextHAlignAsString = "Left";
            appearance53.TextVAlignAsString = "Bottom";
            this.ultraLabel11.Appearance = appearance53;
            this.ultraLabel11.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel11.TabIndex = 63;
            this.ultraLabel11.Text = "Tên kỳ tính giá thành";
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(148, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(147, 22);
            this.tsmAdd.Text = "Thêm";
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(147, 22);
            this.tsmEdit.Text = "Sửa";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(147, 22);
            this.tsmDelete.Text = "Xóa";
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(147, 22);
            this.tmsReLoad.Text = "Tải Lại";
            // 
            // uGridUncomplete
            // 
            this.uGridUncomplete.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridUncomplete.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGridUncomplete.Location = new System.Drawing.Point(0, 83);
            this.uGridUncomplete.Name = "uGridUncomplete";
            this.uGridUncomplete.Size = new System.Drawing.Size(261, 175);
            this.uGridUncomplete.TabIndex = 48;
            this.uGridUncomplete.Text = "ultraGrid4";
            this.uGridUncomplete.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncomplete_AfterCellUpdate);
            this.uGridUncomplete.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncomplete_CellChange);
            this.uGridUncomplete.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridUncomplete_BeforeExitEditMode);
            // 
            // uGridUncompleteDetail
            // 
            this.uGridUncompleteDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridUncompleteDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridUncompleteDetail.Location = new System.Drawing.Point(0, 77);
            this.uGridUncompleteDetail.Name = "uGridUncompleteDetail";
            this.uGridUncompleteDetail.Size = new System.Drawing.Size(261, 218);
            this.uGridUncompleteDetail.TabIndex = 54;
            this.uGridUncompleteDetail.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridUncompleteDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncompleteDetail_AfterCellUpdate);
            this.uGridUncompleteDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncompleteDetail_CellChange);
            this.uGridUncompleteDetail.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridUncompleteDetail_BeforeExitEditMode);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.btnDetermine);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.ultraButton1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 48);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(261, 35);
            this.ultraGroupBox1.TabIndex = 53;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnDetermine
            // 
            this.btnDetermine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetermine.Location = new System.Drawing.Point(125, 6);
            this.btnDetermine.Name = "btnDetermine";
            this.btnDetermine.Size = new System.Drawing.Size(126, 23);
            this.btnDetermine.TabIndex = 58;
            this.btnDetermine.Text = "Xác định dở dang";
            this.btnDetermine.Click += new System.EventHandler(this.btnDetermine_Click);
            // 
            // ultraLabel12
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            appearance55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance55.TextHAlignAsString = "Left";
            appearance55.TextVAlignAsString = "Bottom";
            this.ultraLabel12.Appearance = appearance55;
            this.ultraLabel12.Location = new System.Drawing.Point(6, 8);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(212, 17);
            this.ultraLabel12.TabIndex = 57;
            this.ultraLabel12.Text = "Kết quả đánh giá chi phí dở dang cuối kỳ";
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraButton1.Location = new System.Drawing.Point(578, 6);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(115, 23);
            this.ultraButton1.TabIndex = 10;
            this.ultraButton1.Text = "Xác định dở dang";
            // 
            // Panel5
            // 
            // 
            // Panel5.ClientArea
            // 
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.Panel5.ClientArea.Controls.Add(this.uGridUncompleteDetail);
            this.Panel5.ClientArea.Controls.Add(this.uGridUncomplete);
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox8);
            this.Panel5.Location = new System.Drawing.Point(292, 247);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(261, 258);
            this.Panel5.TabIndex = 55;
            // 
            // ultraGroupBox8
            // 
            this.ultraGroupBox8.Controls.Add(this.optType);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox8.Controls.Add(this.txtCPPeriod5);
            this.ultraGroupBox8.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            appearance58.FontData.BoldAsString = "True";
            appearance58.FontData.SizeInPoints = 10F;
            this.ultraGroupBox8.HeaderAppearance = appearance58;
            this.ultraGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox8.Name = "ultraGroupBox8";
            this.ultraGroupBox8.Size = new System.Drawing.Size(261, 77);
            this.ultraGroupBox8.TabIndex = 47;
            this.ultraGroupBox8.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optType
            // 
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            valueListItem1.DisplayText = "Theo sản phẩm hoàn thành tương đương";
            valueListItem1.Tag = "SPHTTD";
            valueListItem2.DataValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            valueListItem2.DisplayText = "Theo NVL trực tiếp";
            valueListItem2.Tag = "NVLTT";
            valueListItem3.DataValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            valueListItem3.DisplayText = "Theo định mức ";
            valueListItem3.Tag = "DM";
            this.optType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.optType.Location = new System.Drawing.Point(350, 49);
            this.optType.Name = "optType";
            this.optType.Size = new System.Drawing.Size(446, 17);
            this.optType.TabIndex = 66;
            this.optType.ValueChanged += new System.EventHandler(this.optType_ValueChanged);
            // 
            // ultraLabel13
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            appearance56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance56.TextHAlignAsString = "Left";
            appearance56.TextVAlignAsString = "Bottom";
            this.ultraLabel13.Appearance = appearance56;
            this.ultraLabel13.Location = new System.Drawing.Point(24, 48);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel13.TabIndex = 65;
            this.ultraLabel13.Text = "Xác định dở dang";
            // 
            // txtCPPeriod5
            // 
            appearance30.TextVAlignAsString = "Middle";
            this.txtCPPeriod5.Appearance = appearance30;
            this.txtCPPeriod5.AutoSize = false;
            this.txtCPPeriod5.Location = new System.Drawing.Point(163, 12);
            this.txtCPPeriod5.Name = "txtCPPeriod5";
            this.txtCPPeriod5.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod5.TabIndex = 64;
            // 
            // ultraLabel14
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            appearance57.TextHAlignAsString = "Left";
            appearance57.TextVAlignAsString = "Bottom";
            this.ultraLabel14.Appearance = appearance57;
            this.ultraLabel14.Location = new System.Drawing.Point(24, 12);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel14.TabIndex = 63;
            this.ultraLabel14.Text = "Tên kỳ tính giá thành";
            // 
            // FCPSimpleMethodDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 567);
            this.Controls.Add(this.Panel6);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnEscape);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FCPSimpleMethodDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xác định kỳ giá thành";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FCPSimpleMethodDetail_FormClosing_1);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FCPSimpleMethodDetail_FormClosed);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCostset)).EndInit();
            this.Panel1.ClientArea.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList3)).EndInit();
            this.Panel3.ClientArea.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            this.Panel4.ClientArea.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpenseResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod4)).EndInit();
            this.Panel2.ClientArea.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod2)).EndInit();
            this.Panel6.ClientArea.ResumeLayout(false);
            this.Panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCosting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod6)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncomplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncompleteDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.Panel5.ClientArea.ResumeLayout(false);
            this.Panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox8)).EndInit();
            this.ultraGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnBack;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton btnEscape;
        private Infragistics.Win.Misc.UltraButton btnSave; 
        private Infragistics.Win.Misc.UltraButton btnUpdateImport;
        private Infragistics.Win.Misc.UltraButton btnUpdateExport;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCostset;
        private Infragistics.Win.Misc.UltraPanel Panel1;
        private Infragistics.Win.Misc.UltraPanel Panel4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocationGeneralExpense;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCpExpenseList3;
        private Infragistics.Win.Misc.UltraPanel Panel3;
        private Infragistics.Win.Misc.UltraPanel Panel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCpExpenseList2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraButton btnAttribution;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocationGeneralExpenseResult;
        private Infragistics.Win.Misc.UltraPanel Panel6;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCosting;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUncomplete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUncompleteDetail;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnDetermine;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraPanel Panel5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox8;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
    }
}