﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.ComponentModel;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using Accounting.Core.DAO;

namespace Accounting
{
    public partial class FCPSimpleMethodDetail : CustormForm
    {
        #region Khai báo
        private int indexCurrent = 0;
        List<Control> lstControl = new List<Control>();
        List<string> text = new List<string>();
        public static bool isClose = true;
        bool IsAdd = true;
        public List<SelectCostSet> _LstCostSet;
        List<Guid> listCostSetID;
        List<CPAllocationGeneralExpense> lstAllocationGeneralExpense;
        List<CPAllocationGeneralExpenseDetail> lstAllocationGeneralExpenseDetail;
        List<CPExpenseList> _LstCPExpense;
        List<CPExpenseList> _LstCPExpense2;
        List<CPUncompleteDetail> _LstCPUncompleteDetail;
        List<CPOPN> _LstCPOPN;
        List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail;
        List<RSInwardOutward> _LstRSInwardOutward;
        List<ExpenseItem> _LstExpenseItem;
        List<CPUncomplete> _LstCPUncomplete;
        List<CPResult> _LstCPResult;
        CPPeriod _select = new CPPeriod();
        CPPeriod cpp = new CPPeriod();
        List<SelectCostSet> listcostset;
        CPAllocationRate cpar;
        int checkItem;
        int checkAtt = 1;
        int checkDtr = 1;
        int checkupdate = 0;
        int save = 0;
        DateTime fromDate;
        DateTime toDate;
        int lamtron = int.Parse(Utils.ListSystemOption.FirstOrDefault(c => c.Code == "DDSo_TienVND").Data);
        private ICPPeriodService ICPPeriodService
        {
            get { return IoC.Resolve<ICPPeriodService>(); }
        }
        private ICPPeriodDetailService ICPPeriodDetailService
        {
            get { return IoC.Resolve<ICPPeriodDetailService>(); }
        }
        private ICPExpenseListService ICPExpenseListService
        {
            get { return IoC.Resolve<ICPExpenseListService>(); }
        }
        private ICPAllocationGeneralExpenseService ICPAllocationGeneralExpenseService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseService>(); }
        }
        private ICPAllocationGeneralExpenseDetailService ICPAllocationGeneralExpenseDetailService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseDetailService>(); }
        }
        private ICPUncompleteService ICPUncompleteService
        {
            get { return IoC.Resolve<ICPUncompleteService>(); }
        }
        private ICPUncompleteDetailService ICPUncompleteDetailService
        {
            get { return IoC.Resolve<ICPUncompleteDetailService>(); }
        }
        private ICPResultService ICPResultService
        {
            get { return IoC.Resolve<ICPResultService>(); }
        }
        private ICostSetMaterialGoodsService ICostSetMaterialGoodsService
        {
            get { return IoC.Resolve<ICostSetMaterialGoodsService>(); }
        }
        #endregion

        #region Khởi tạo
        public FCPSimpleMethodDetail()
        {
            InitializeComponent();
            LoadChung();
            ConfigButton();
            ShowPal();
        }
        public FCPSimpleMethodDetail(CPPeriod temp)
        {
            InitializeComponent();
            IsAdd = false;
            _select = temp;
            txtCPPeriod.Text = _select.Name;
            txtCPPeriod2.ReadOnly = true;
            txtCPPeriod3.ReadOnly = true;
            txtCPPeriod4.ReadOnly = true;
            txtCPPeriod5.ReadOnly = true;
            txtCPPeriod6.ReadOnly = true;
            dtBeginDate.ReadOnly = true;
            dtEndDate.ReadOnly = true;
            cbbDateTime.ReadOnly = true;
            fromDate = temp.FromDate;
            toDate = temp.ToDate;
            LoadChung();
            ConfigButton();
            ShowPal();
        }
        #endregion

        #region Hàm xử lý nghiệp vụ
        void LoadChung()
        {
            text = new List<string> { "Xác định kỳ tính giá thành", "Tập hợp chi phí trực tiếp", "Tập hợp các khoản giảm giá thành", "Phân bổ chi phí chung", "Ðánh giá dở dang cuối kỳ", "Bảng kết quả tính giá thành" };
            lstControl = new List<Control> { Panel1, Panel2, Panel3, Panel4, Panel5, Panel6 };
            ViewGridPanel1();
        }
        #endregion

        #region Hàm xử lý Grid
        private void ConfigButton()
        {
            if (indexCurrent == 0)
            {//d?u tiên
                btnBack.Enabled = false;
                btnNext.Enabled = true;
                btnSave.Enabled = false;
                btnUpdateImport.Visible = false;
                btnUpdateExport.Visible = false;
            }
            else if (indexCurrent == lstControl.Count - 1)
            {
                btnBack.Enabled = true;
                btnNext.Enabled = false;
                //btnSave.Enabled = true;comment by cuongpv
                btnUpdateImport.Visible = true;
                btnUpdateExport.Visible = true;
                if (IsAdd)
                {
                    btnSave.Enabled = true;//add by cuongpv
                    btnUpdateImport.Enabled = false;
                    btnUpdateExport.Enabled = false;
                }
                else
                {
                    //add by cuongpv
                    if (checkupdate == 1)
                    {
                        btnSave.Enabled = true;
                        btnUpdateImport.Enabled = false;
                        btnUpdateExport.Enabled = false;
                    }
                    else
                    {
                        btnSave.Enabled = false;
                        btnUpdateImport.Enabled = true;
                        btnUpdateExport.Enabled = true;
                    }
                    //end add by cuongpv
                }
            }
            else
            {
                btnBack.Enabled = true;
                btnNext.Enabled = true;
                btnSave.Enabled = false;
                btnUpdateImport.Visible = false;
                btnUpdateExport.Visible = false;
            }
        }
        private void ShowPal()
        {
            this.Text = text[indexCurrent];
            foreach (Control control in lstControl) control.Visible = false;
            lstControl[indexCurrent].Visible = true;
            lstControl[indexCurrent].Dock = DockStyle.Fill;
        }
        #endregion

        #region Events
        private void btnBack_Click(object sender, EventArgs e)
        {
            indexCurrent -= 1;
            ConfigButton();
            ShowPal();
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (indexCurrent == 0)
            {
                _LstCostSet = new List<SelectCostSet>();
                listCostSetID = new List<Guid>();
                if (IsAdd)
                {
                    for (int i = 0; i < uGridCostset.Rows.Count; i++)
                    {
                        if (uGridCostset.Rows[i].Cells["Select"].Value == null)
                        {
                            uGridCostset.Rows[i].Cells["Select"].Value = false;
                        }
                        if (bool.Parse(uGridCostset.Rows[i].Cells["Select"].Value.ToString()) == true)
                        {
                            SelectCostSet cs = uGridCostset.Rows[i].ListObject as SelectCostSet;
                            _LstCostSet.Add(cs);
                            listCostSetID.Add(cs.ID);
                            string costsetname = Utils.ICostSetService.Getbykey(cs.ID).CostSetName;
                            if (!CheckCostSet(fromDate, toDate, cs.ID))
                            {
                                MSG.Error("Đối tượng tập hợp chi phí được chọn đã có trong kỳ tính giá thành khác trùng khoảng thời gian với kỳ tính giá thành này. Vui lòng chọn lại");
                                return;
                            }
                        }
                    }

                    if (listCostSetID.Count == 0)
                    {
                        MSG.Error("Bạn chưa chọn đối tượng tính giá thành!");
                        return;
                    }
                }
                else
                {
                    for (int i = 0; i < uGridCostset.Rows.Count; i++)
                    {
                        SelectCostSet cs = uGridCostset.Rows[i].ListObject as SelectCostSet;
                        _LstCostSet.Add(cs);
                        listCostSetID.Add(cs.ID);
                    }
                }
                txtCPPeriod2.Text = txtCPPeriod.Text;
                ViewGridPanel2();
            }
            else if (indexCurrent == 1)
            {
                txtCPPeriod3.Text = txtCPPeriod.Text;
                ViewGridPanel3();
            }
            else if (indexCurrent == 2)
            {
                WaitingFrm.StartWaiting();
                txtCPPeriod4.Text = txtCPPeriod.Text;
                ViewGridPanel4();
                WaitingFrm.StopWaiting();
            }
            else if (indexCurrent == 3)
            {
                if (lstAllocationGeneralExpense.Sum(x => x.AllocatedRate) == 0)
                {
                    lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();
                }
                else if (lstAllocationGeneralExpenseDetail.Count == 0)
                {
                    MSG.Warning("Chi phí chung chưa được phân bổ ");
                    return;
                }
                if (checkAtt == 0)
                {
                    MSG.Warning("Chưa phân bổ chi phí chung với số liệu vừa thay đổi");
                    return;
                }
                WaitingFrm.StartWaiting();
                if (IsAdd)
                {
                    optType.CheckedIndex = 0;
                }
                else
                {
                    List<CPUncomplete> lstUncomplete = /*ICPUncompleteService.GetList().Where(x => x.CPPeriodID == _select.ID).ToList()*/_select.CPUncompletes.ToList();
                    List<CPUncompleteDetail> lstUncompleteDetail = new List<CPUncompleteDetail>();
                    if (lstUncomplete.Count > 0)
                    {
                        foreach (var item in lstUncomplete)
                        {
                            lstUncompleteDetail.AddRange(item.CPUncompleteDetails);
                        }
                    }
                    if (lstUncompleteDetail.Count > 0)
                        optType.CheckedIndex = lstUncompleteDetail.FirstOrDefault().UncompleteType;
                }
                txtCPPeriod5.Text = txtCPPeriod.Text;
                //ViewGridPanel5();
                //add by cuongpv
                if (IsAdd)
                    btnDetermine_Click(sender, e);
                //end add by cuongpv
                WaitingFrm.StopWaiting();
            }
            else if (indexCurrent == 4)
            {
                //if (checkDtr == 0)
                //{
                //    MSG.Warning("Chưa đánh giá dở dang với số liệu vừa nhập");
                //    return;
                //}
                txtCPPeriod6.Text = txtCPPeriod.Text;
                ViewGridPanel6();
            }
            indexCurrent += 1;
            ConfigButton();
            ShowPal();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            Utils.ICPPeriodService.BeginTran();
            try
            {
                if (IsAdd)
                {
                    if (fromDate > toDate)
                    {
                        MSG.Error("Ngày bắt đầu phải nhỏ hơn ngày kết thúc!");
                        return;
                    }
                    WaitingFrm.StartWaiting();
                    CPPeriod lstPeriod = new CPPeriod
                    {
                        ID = Guid.NewGuid(),
                        Type = 0,
                        FromDate = fromDate,
                        ToDate = toDate,
                        Name = txtCPPeriod.Text,
                        IsDelete = true
                    };

                    foreach (var cs in _LstCostSet)
                    {
                        CPPeriodDetail detail = new CPPeriodDetail();
                        detail.ID = Guid.NewGuid();
                        detail.CPPeriodID = lstPeriod.ID;
                        detail.CostSetID = cs.ID;
                        lstPeriod.CPPeriodDetails.Add(detail);
                    }

                    lstPeriod.CPExpenseLists = ((BindingList<CPExpenseList>)uGridCpExpenseList2.DataSource).ToList();
                    foreach (var cpe in lstPeriod.CPExpenseLists)
                    {
                        cpe.CPPeriodID = lstPeriod.ID;
                    }
                    foreach (var cpe in ((BindingList<CPExpenseList>)uGridCpExpenseList3.DataSource).ToList())
                    {
                        cpe.CPPeriodID = lstPeriod.ID;
                        lstPeriod.CPExpenseLists.Add(cpe);
                    }

                    lstPeriod.CPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                    foreach (var cpa in lstPeriod.CPAllocationGeneralExpenses)
                    {
                        cpa.CPPeriodID = lstPeriod.ID;
                        cpa.CPAllocationGeneralExpenseDetails = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).Where(c => c.CPAllocationGeneralExpenseID == cpa.ID && c.AllocatedAmount > 0).ToList();
                    }

                    lstPeriod.CPUncompletes = ((BindingList<CPUncomplete>)uGridUncomplete.DataSource).ToList();
                    foreach (var cpu in lstPeriod.CPUncompletes)
                    {
                        cpu.CPPeriodID = lstPeriod.ID;
                        cpu.CPUncompleteDetails = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cpu.CostSetID).ToList();
                        foreach (var cpud in cpu.CPUncompleteDetails)
                        {
                            cpud.CPUncompleteID = cpu.ID;
                            cpud.UncompleteType = checkItem;
                        }
                    }

                    lstPeriod.CPResults = ((BindingList<CPResult>)uGridCosting.DataSource).ToList();
                    foreach (var cpr in lstPeriod.CPResults)
                    {
                        cpr.CPPeriodID = lstPeriod.ID;
                        cpr.CPPeriodDetailID = lstPeriod.CPPeriodDetails.FirstOrDefault(x => x.CostSetID == cpr.CostSetID).ID;
                        cpr.MaterialGoodsID = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cpr.CostSetID).First().MaterialGoodsID;
                    }

                    Utils.ICPPeriodService.CreateNew(lstPeriod);
                    Utils.ICPPeriodService.CommitTran();
                    WaitingFrm.StopWaiting();
                    MSG.Information("Lưu dữ liệu thành công");
                    btnNext.Enabled = false;
                    save = 1;
                    //cpp = lstPeriod;comment by cuongpv
                }
                else
                {
                    WaitingFrm.StartWaiting();
                    if (_select != null)
                    {
                        _select.CPAllocationGeneralExpenses.Clear();
                        List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ICPAllocationGeneralExpenseService.GetAll().Where(x => x.CPPeriodID == _select.ID && x.AllocatedRate > 0).ToList();
                        if (cPAllocationGeneralExpenses.Count > 0)
                        {
                            List<CPAllocationGeneralExpense> lst = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).Where(c => c.CPPeriodID == _select.ID && c.AllocatedRate > 0).ToList();
                            if (lst.Count > 0)
                                foreach (var item in cPAllocationGeneralExpenses)
                                {
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense = lst.FirstOrDefault(x => x.ID == item.ID);
                                    if (cPAllocationGeneralExpense != null)
                                    {
                                        item.AllocatedRate = cPAllocationGeneralExpense.AllocatedRate;
                                        item.AllocatedAmount = cPAllocationGeneralExpense.AllocatedAmount;
                                        item.UnallocatedAmount = cPAllocationGeneralExpense.UnallocatedAmount;
                                        item.AllocationMethod = cPAllocationGeneralExpense.AllocationMethod;
                                        ICPAllocationGeneralExpenseService.Update(item);
                                        _select.CPAllocationGeneralExpenses.Add(item);
                                    }

                                    item.CPAllocationGeneralExpenseDetails.Clear();
                                    List<CPAllocationGeneralExpenseDetail> cPAllocationGeneralExpenseDetails = ICPAllocationGeneralExpenseDetailService.GetAll().Where(x => x.CPAllocationGeneralExpenseID == item.ID && x.AllocatedRate > 0).ToList();
                                    List<CPAllocationGeneralExpenseDetail> lst1 = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).Where(c => c.CPAllocationGeneralExpenseID == item.ID && c.AllocatedRate > 0).ToList();
                                    if (cPAllocationGeneralExpenseDetails.Count > 0)
                                    {
                                        foreach (var item1 in cPAllocationGeneralExpenseDetails)
                                        {
                                            CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = lst1.FirstOrDefault(x => x.ID == item1.ID);
                                            CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail1 = lst1.FirstOrDefault(x => x.CPAllocationGeneralExpenseID == item1.CPAllocationGeneralExpenseID && x.CostSetID == item1.CostSetID);
                                            if (cPAllocationGeneralExpenseDetail != null)
                                            {
                                                item1.AllocatedRate = cPAllocationGeneralExpenseDetail.AllocatedRate;
                                                item1.AllocatedAmount = cPAllocationGeneralExpenseDetail.AllocatedAmount;
                                                ICPAllocationGeneralExpenseDetailService.Update(item1);
                                                item.CPAllocationGeneralExpenseDetails.Add(item1);
                                            }
                                            else
                                            {
                                                if (cPAllocationGeneralExpenseDetail1 != null)
                                                {
                                                    ICPAllocationGeneralExpenseDetailService.Delete(item1);
                                                    ICPAllocationGeneralExpenseDetailService.CreateNew(cPAllocationGeneralExpenseDetail1);
                                                    item.CPAllocationGeneralExpenseDetails.Add(cPAllocationGeneralExpenseDetail1);
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        else
                        {
                            _select.CPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                            foreach (var cpa in _select.CPAllocationGeneralExpenses)
                            {
                                cpa.CPPeriodID = _select.ID;
                                cpa.CPAllocationGeneralExpenseDetails = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).Where(c => c.CPAllocationGeneralExpenseID == cpa.ID && c.AllocatedAmount > 0).ToList();
                            }
                        }

                        _select.CPUncompletes.Clear();
                        bool update = true;
                        List<CPUncomplete> cPUncompletes = ICPUncompleteService.Query.Where(x => x.CPPeriodID == _select.ID).ToList();
                        if (cPUncompletes.Count > 0)
                        {
                            List<CPUncomplete> lst = ((BindingList<CPUncomplete>)uGridUncomplete.DataSource).ToList();
                            if (lst.Count > 0)
                                foreach (var item in cPUncompletes)
                                {
                                    CPUncomplete cPUncomplete = lst.FirstOrDefault(x => x.ID == item.ID);
                                    CPUncomplete cPUncomplete1 = lst.FirstOrDefault(x => x.CostSetID == item.CostSetID);
                                    if (cPUncomplete != null)
                                    {
                                        item.CPUncompleteDetails.Clear();
                                        List<CPUncompleteDetail> cPUncompleteDetails = ICPUncompleteDetailService.GetAll().Where(x => x.CPUncompleteID == item.ID).ToList();
                                        List<CPUncompleteDetail> lst1 = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == item.CostSetID).ToList();
                                        if (cPUncompleteDetails.Count > 0)
                                        {
                                            foreach (var item1 in cPUncompleteDetails)
                                            {
                                                CPUncompleteDetail cPUncompleteDetail = lst1.FirstOrDefault(x => x.ID == item1.ID);
                                                CPUncompleteDetail cPUncompleteDetail1 = lst1.FirstOrDefault(x => x.MaterialGoodsID == item1.MaterialGoodsID);
                                                item1.Quantity = cPUncompleteDetail.Quantity;
                                                item1.PercentComplete = cPUncompleteDetail.PercentComplete;
                                                item1.UnitPrice = cPUncompleteDetail.UnitPrice;
                                                item1.UncompleteType = optType.CheckedIndex;
                                                ICPUncompleteDetailService.Update(item1);
                                                item.CPUncompleteDetails.Add(item1);
                                            }
                                        }

                                        item.DirectMaterialAmount = Math.Round(cPUncomplete.DirectMaterialAmount, lamtron, MidpointRounding.AwayFromZero);
                                        item.DirectLaborAmount = Math.Round(cPUncomplete.DirectLaborAmount, lamtron, MidpointRounding.AwayFromZero);
                                        item.GeneralExpensesAmount = Math.Round(cPUncomplete.GeneralExpensesAmount, lamtron, MidpointRounding.AwayFromZero);
                                        item.TotalCostAmount = Math.Round(cPUncomplete.TotalCostAmount, lamtron, MidpointRounding.AwayFromZero);
                                        update = true;
                                    }
                                    else
                                    {
                                        cPUncomplete1.CPPeriodID = item.CPPeriodID;

                                        cPUncomplete1.CPUncompleteDetails.Clear();
                                        item.CPUncompleteDetails.Clear();
                                        List<CPUncompleteDetail> cPUncompleteDetails = ICPUncompleteDetailService.GetAll().Where(x => x.CPUncompleteID == item.ID).ToList();
                                        List<CPUncompleteDetail> lst1 = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cPUncomplete1.CostSetID).ToList();
                                        if (cPUncompleteDetails.Count > 0)
                                        {
                                            foreach (var item1 in cPUncompleteDetails)
                                            {
                                                CPUncompleteDetail cPUncompleteDetail = lst1.FirstOrDefault(x => x.ID == item1.ID);
                                                CPUncompleteDetail cPUncompleteDetail1 = lst1.FirstOrDefault(x => x.MaterialGoodsID == item1.MaterialGoodsID);
                                                if (cPUncompleteDetail != null)
                                                {
                                                    item.DirectMaterialAmount = Math.Round(cPUncomplete1.DirectMaterialAmount, lamtron, MidpointRounding.AwayFromZero);
                                                    item.DirectLaborAmount = Math.Round(cPUncomplete1.DirectLaborAmount, lamtron, MidpointRounding.AwayFromZero);
                                                    item.GeneralExpensesAmount = Math.Round(cPUncomplete1.GeneralExpensesAmount, lamtron, MidpointRounding.AwayFromZero);
                                                    item.TotalCostAmount = Math.Round(cPUncomplete1.TotalCostAmount, lamtron, MidpointRounding.AwayFromZero);//add by cuongpv
                                                    item.CPUncompleteDetails.Add(cPUncompleteDetail);
                                                    update = true;
                                                }
                                                else
                                                {
                                                    ICPUncompleteService.CreateNew(cPUncomplete1);
                                                    cPUncompleteDetail1.CPUncompleteID = cPUncomplete1.ID;
                                                    cPUncompleteDetail1.UncompleteType = optType.CheckedIndex;
                                                    ICPUncompleteDetailService.Delete(item1);
                                                    ICPUncompleteDetailService.CreateNew(cPUncompleteDetail1);
                                                    cPUncomplete1.CPUncompleteDetails.Add(cPUncompleteDetail1);
                                                    update = false;
                                                }
                                            }
                                        }

                                    }
                                    if (update)
                                    {
                                        if (item != null)
                                        {
                                            ICPUncompleteService.Update(item);
                                            _select.CPUncompletes.Add(item);
                                        }
                                    }
                                    else
                                    {
                                        if (item != null)
                                        {
                                            ICPUncompleteService.Delete(item);
                                            _select.CPUncompletes.Add(cPUncomplete1);
                                        }
                                    }
                                }
                        }
                        else
                        {
                            _select.CPUncompletes = ((BindingList<CPUncomplete>)uGridUncomplete.DataSource).ToList();
                            foreach (var cpu in _select.CPUncompletes)
                            {
                                cpu.CPPeriodID = _select.ID;
                                cpu.CPUncompleteDetails = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cpu.CostSetID).ToList();
                                foreach (var cpud in cpu.CPUncompleteDetails)
                                {
                                    cpud.CPUncompleteID = cpu.ID;
                                    cpud.UncompleteType = checkItem;
                                }
                            }
                        }

                        _select.Name = txtCPPeriod.Text;

                        if(checkupdate == 1)
                        {
                            foreach (var item in _select.CPResults)
                            {
                                ICPResultService.Delete(item);
                            }
                        }

                        _select.CPResults.Clear();
                        _select.CPResults = ((BindingList<CPResult>)uGridCosting.DataSource).ToList();
                        foreach (var cpr in _select.CPResults)
                        {
                            cpr.CPPeriodID = _select.ID;
                            cpr.CPPeriodDetailID = _select.CPPeriodDetails.FirstOrDefault(x => x.CostSetID == cpr.CostSetID).ID;
                        }
                    }
                    Utils.ICPPeriodService.Update(_select);
                    Utils.ICPPeriodService.CommitTran();
                    WaitingFrm.StartWaiting();
                    MSG.Information("Cập nhật dữ liệu thành công");
                    foreach (var col in uGridCosting.DisplayLayout.Bands[0].Columns)
                    {
                        col.CellActivation = Activation.NoEdit;
                    }
                    save = 1;
                }

                btnSave.Enabled = false;
                btnBack.Enabled = false;
                btnUpdateImport.Enabled = true;
                btnUpdateExport.Enabled = true;

                //add by cuongpv
                Utils.ClearCacheByType<CPExpenseList>();
                Utils.ClearCacheByType<CPPeriod>();
                Utils.ClearCacheByType<CPPeriodDetail>();
                Utils.ClearCacheByType<CPAllocationGeneralExpense>();
                Utils.ClearCacheByType<CPAllocationGeneralExpenseDetail>();
                Utils.ClearCacheByType<CPAllocationRate>();
                Utils.ClearCacheByType<CPResult>();

                Utils.ICPExpenseListService.UnbindSession(Utils.ListCPExpenseList);
                Utils.ICPPeriodService.UnbindSession(Utils.ListCPPeriod);
                Utils.ICPPeriodDetailService.UnbindSession(Utils.ListCPPeriodDetail);
                Utils.ICPAllocationGeneralExpenseService.UnbindSession(Utils.ListCPAllocationGeneralExpense);
                Utils.ICPAllocationGeneralExpenseDetailService.UnbindSession(Utils.ListCPAllocationGeneralExpenseDetail);
                Utils.ICPAllocationRateService.UnbindSession(Utils.ListCPAllocationRate);
                Utils.ICPResultService.UnbindSession(Utils.ListCPResult);
                //end add by cuongpv
            }
            catch (Exception ex)
            {
                Utils.ICPPeriodService.RolbackTran();
                WaitingFrm.StartWaiting();
            }
        }
        private void btnEscape_Click(object sender, EventArgs e)
        {
            if (save == 1)
            {
                isClose = true;
            }
            else
            {
                if (lstAllocationGeneralExpense != null)
                    ICPAllocationGeneralExpenseService.UnbindSession(lstAllocationGeneralExpense);
                if (lstAllocationGeneralExpenseDetail != null)
                    ICPAllocationGeneralExpenseDetailService.UnbindSession(lstAllocationGeneralExpenseDetail);
                if (_LstCPUncomplete != null)
                    ICPUncompleteService.UnbindSession(_LstCPUncomplete);
                if (_LstCPUncompleteDetail != null)
                    ICPUncompleteDetailService.UnbindSession(_LstCPUncompleteDetail);
                if (_LstCPResult != null)
                    ICPResultService.UnbindSession(_LstCPResult);
                isClose = false;
            }
            Close();
        }

        private void dtEndDate_ValueChanged(object sender, EventArgs e)
        {
            if (IsAdd)
            {
                if (dtBeginDate.Value != null && dtEndDate.Value != null)
                {
                    txtCPPeriod.Text = "Kỳ tính giá thành từ ngày " + dtBeginDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến ngày " + dtEndDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dtend = dtEndDate.DateTime;
                    toDate = new DateTime(dtend.Year, dtend.Month, dtend.Day, 23, 59, 59);
                }
            }
        }

        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {
            if (IsAdd)
            {
                if (dtBeginDate.Value != null)
                {
                    txtCPPeriod.Text = "Kỳ tính giá thành từ ngày " + dtBeginDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến ngày " + dtEndDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dtbegin = dtBeginDate.DateTime;
                    fromDate = new DateTime(dtbegin.Year, dtbegin.Month, dtbegin.Day, 0, 0, 0);
                }
            }
        }

        private void btnDetermine_Click(object sender, EventArgs e)
        {
            //checkDtr = 1;
            _LstCPUncomplete = new List<CPUncomplete>();
            _LstExpenseItem = Utils.ListExpenseItem.ToList();
            _LstCPOPN = Utils.ListCPOPN.ToList();
            List<CPOPN> _LstCPOPN2 = new List<CPOPN>();
            List<CPExpenseList> _LstCPExpense3 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense4 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense5 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense6 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense7 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense8 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst4 = new List<CPAllocationGeneralExpenseDetail>();
            _LstCPOPN2 = (from g in _LstCPOPN where listCostSetID.Any(x => x == g.CostSetID) select g).ToList(); //DirectMatetialAmount
            _LstCPExpense3 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            _LstCPExpense4 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            lst2 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 0
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense5 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            _LstCPExpense6 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            lst3 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 1
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense7 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            _LstCPExpense8 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            lst4 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 2
                    select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
            var lst5 = (from a in lst4
                        group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            CostsetCode = t.Key.CostsetCode,
                            CostsetName = t.Key.CostsetName,
                            Amount = t.Sum(x => x.AllocatedAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                lst6.Add(cPAllocationGeneralExpenseDetail);
            }
            foreach (var item in uGridUncompleteDetail.DataSource as BindingList<CPUncompleteDetail>)
                if (item.PercentComplete > 100)
                {
                    MSG.Warning("% hoàn thành không được quá 100%");
                    return;
                }

            switch (checkItem)
            {
                case 0:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
                case 1:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
                case 2:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
                default:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
            }
            Utils.AddSumColumn(uGridUncomplete, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);
            foreach (var col in uGridUncomplete.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "DirectMaterialAmount" || col.Key == "DirectLaborAmount" || col.Key == "GeneralExpensesAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }

        private void btnUpdateImport_Click(object sender, EventArgs e)
        {
            #region ...
            //try
            //{
            //    ReportProcedureSDS procedureSDS = new ReportProcedureSDS();
            //    if (IsAdd)
            //        procedureSDS.UpdateImport(fromDate, toDate, cpp.ID);
            //    else
            //        procedureSDS.UpdateImport(fromDate, toDate, _select.ID);
            //    MSG.Information("Cập nhật dữ liệu thành công");
            //    btnUpdateImport.Enabled = false;
            //}
            //catch (Exception)
            //{
            //    return;
            //}
            //Utils.IRSInwardOutwardDetailService.BeginTran();
            //try
            //{
            //    List<RSInwardOutward> _LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
            //    List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.ToList();
            //    _LstRSInwardOutwardDetail1 = (from g in _LstRSInwardOutward1
            //                                  join h in _LstRSInwardOutwardDetail1 on g.ID equals h.RSInwardOutwardID
            //                                  where g.TypeID == 400 && listCostSetID.Any(x => x == h.CostSetID) && h.DebitAccount == "155" && h.CreditAccount == "154"
            //                                  && g.PostedDate >= fromDate && g.PostedDate <= toDate
            //                                  select h).ToList();
            //    List<Guid> rsIDs = new List<Guid>();
            //    foreach (var item in _LstCPResult)
            //    {
            //        List<RSInwardOutwardDetail> lstrsid = _LstRSInwardOutwardDetail1.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
            //        if (lstrsid.Count > 0)
            //        {
            //            foreach (var rsid in lstrsid)
            //            {
            //                rsid.UnitPrice = item.UnitPrice;
            //                rsid.UnitPriceOriginal = item.UnitPrice;
            //                rsid.Amount = Math.Round((rsid.UnitPrice * (decimal)rsid.Quantity), 0);
            //                rsid.AmountOriginal = Math.Round((rsid.UnitPriceOriginal * (decimal)rsid.Quantity), 0);
            //                Utils.IRSInwardOutwardDetailService.Update(rsid);
            //                rsIDs.Add((Guid)rsid.RSInwardOutwardID);
            //            }
            //        }

            //        List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.TypeID == 400 && x.MaterialGoodsID == item.MaterialGoodsID
            //                                  && x.PostedDate >= fromDate && x.PostedDate <= toDate).ToList();
            //        repositoryLedgers = (from g in repositoryLedgers where listCostSetID.Any(x => x == g.CostSetID) select g).ToList();
            //        if (repositoryLedgers.Count > 0)
            //        {
            //            foreach (var rl in repositoryLedgers)
            //            {
            //                rl.UnitPrice = item.UnitPrice;
            //                rl.IWAmount = Math.Round((decimal)(rl.UnitPrice * rl.IWQuantity), 0);
            //                rl.IWAmountBalance = Math.Round((rl.UnitPrice * (decimal)rl.IWQuantityBalance), 0);
            //                Utils.IRepositoryLedgerService.Update(rl);
            //            }
            //        }

            //        List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList();
            //        generalLedgers = (from g in generalLedgers
            //                          join h in _LstRSInwardOutwardDetail1 on g.DetailID equals h.ID
            //                          where h.MaterialGoodsID == item.MaterialGoodsID
            //                          select g).ToList();
            //        if (generalLedgers.Count > 0)
            //        {
            //            foreach (var gl in generalLedgers)
            //            {
            //                RSInwardOutwardDetail rSInwardOutwardDetail = _LstRSInwardOutwardDetail1.FirstOrDefault(x => x.ID == gl.DetailID);
            //                if (gl.Account == "154")
            //                {
            //                    gl.CreditAmount = rSInwardOutwardDetail.Amount;
            //                    gl.CreditAmountOriginal = rSInwardOutwardDetail.Amount;
            //                    gl.DebitAmount = 0;
            //                    gl.DebitAmountOriginal = 0;
            //                }
            //                else if (gl.Account == "155")
            //                {
            //                    gl.CreditAmount = 0;
            //                    gl.CreditAmountOriginal = 0;
            //                    gl.DebitAmount = rSInwardOutwardDetail.Amount;
            //                    gl.DebitAmountOriginal = rSInwardOutwardDetail.Amount;
            //                }
            //                Utils.IGeneralLedgerService.Update(gl);
            //            }
            //        }
            //    }

            //    Decimal totalAmount = 0;
            //    rsIDs = rsIDs.Distinct().ToList();
            //    foreach (var item in rsIDs)
            //    {
            //        RSInwardOutward rSInwardOutward = _LstRSInwardOutward1.FirstOrDefault(x => x.ID == item);
            //        if (rSInwardOutward != null)
            //        {
            //            totalAmount = 0;
            //            List<RSInwardOutwardDetail> rSInwardOutwardDetails = rSInwardOutward.RSInwardOutwardDetails.ToList();
            //            foreach (var item1 in rSInwardOutwardDetails)
            //            {
            //                totalAmount += item1.Amount;
            //            }
            //            rSInwardOutward.TotalAmount = totalAmount;
            //            rSInwardOutward.TotalAmountOriginal = totalAmount;
            //            Utils.IRSInwardOutwardService.Update(rSInwardOutward);
            //        }
            //    }

            //    if (cpp != null)
            //    {
            //        if (!IsAdd)
            //            cpp = _select;
            //        cpp.IsDelete = false;
            //        Utils.ICPPeriodService.Update(cpp);
            //    }
            //    Utils.IRSInwardOutwardDetailService.CommitTran();
            //    MSG.Information("Cập nhật dữ liệu thành công");
            //    btnUpdateImport.Enabled = false;
            //}
            //catch (Exception)
            //{
            //    Utils.IRSInwardOutwardDetailService.RolbackTran();
            //}
            #endregion
            WaitingFrm.StartWaiting();
            List<Guid> lstMaterialGoods = _LstCPResult.Select(x => x.MaterialGoodsID).ToList();
            Utils.IRSInwardOutwardDetailService.BeginTran();
            try
            {

                List<RSInwardOutward> _LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.TypeID == 400 && x.Recorded == true).ToList();
                List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.ToList().Where(g => listCostSetID.Any(x => x == g.CostSetID) && g.DebitAccount.StartsWith("155") && g.CreditAccount.StartsWith("154")
                                              && g.PostedDate >= fromDate && g.PostedDate <= toDate && lstMaterialGoods.Any(d => d == g.MaterialGoodsID) && _LstRSInwardOutward1.Any(d => d.ID == g.RSInwardOutwardID)).ToList();
                List<Guid> rsIDs = new List<Guid>();
                foreach (var item in _LstCPResult)
                {
                    List<RSInwardOutwardDetail> lstrsid = _LstRSInwardOutwardDetail1.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
                    if (lstrsid.Count > 0)
                    {
                        foreach (var rsid in lstrsid)
                        {
                            rsid.UnitPrice = item.UnitPrice;
                            rsid.UnitPriceOriginal = item.UnitPrice;
                            rsid.Amount = (rsid.UnitPrice * (decimal)rsid.Quantity);
                            rsid.AmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                            Utils.IRSInwardOutwardDetailService.Update(rsid);
                            if (!rsIDs.Any(x => x == (Guid)rsid.RSInwardOutwardID))
                                rsIDs.Add((Guid)rsid.RSInwardOutwardID);

                            // lưu gl
                            List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList().Where(x => x.DetailID == rsid.ID).ToList();
                            if (generalLedgers.Count > 0)
                            {
                                foreach (var gl in generalLedgers)
                                {
                                    if (gl.Account.StartsWith("154"))
                                    {
                                        gl.CreditAmount = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                        gl.CreditAmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                        gl.DebitAmount = 0;
                                        gl.DebitAmountOriginal = 0;
                                    }
                                    else if (gl.Account.StartsWith("155"))
                                    {
                                        gl.CreditAmount = 0;
                                        gl.CreditAmountOriginal = 0;
                                        gl.DebitAmount = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                        gl.DebitAmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                    }
                                    Utils.IGeneralLedgerService.Update(gl);
                                }
                            }
                            //lưu rl
                            List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.DetailID == rsid.ID).ToList();
                            if (repositoryLedgers.Count > 0)
                            {
                                foreach (var rl in repositoryLedgers)
                                {
                                    rl.UnitPrice = item.UnitPrice;
                                    rl.IWAmount = (decimal)(rl.UnitPrice * rl.IWQuantity);
                                    rl.IWAmountBalance = (rl.UnitPrice * (decimal)rl.IWQuantityBalance);
                                    Utils.IRepositoryLedgerService.Update(rl);
                                }
                            }
                        }
                    }
                }
                foreach (var item in rsIDs)
                {
                    RSInwardOutward rSInwardOutward = _LstRSInwardOutward1.FirstOrDefault(x => x.ID == item);
                    if (rSInwardOutward != null)
                    {
                        rSInwardOutward.TotalAmount = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        rSInwardOutward.TotalAmountOriginal = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        Utils.IRSInwardOutwardService.Update(rSInwardOutward);
                    }
                }

                //comment by cuongpv
                //if (cpp != null)
                //{
                //    if (!IsAdd)
                //        cpp = _select;
                //    cpp.IsDelete = false;
                //    Utils.ICPPeriodService.Update(cpp);
                //}
                //end comment by cuongpv
                Utils.IRSInwardOutwardDetailService.CommitTran();
                WaitingFrm.StopWaiting();
                MSG.Information("Cập nhật dữ liệu thành công");
                btnUpdateImport.Enabled = false;
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                Utils.IRSInwardOutwardDetailService.RolbackTran();
            }

        }

        private void btnUpdateExport_Click(object sender, EventArgs e)
        {
            #region ...
            //try
            //{
            //    ReportProcedureSDS procedureSDS = new ReportProcedureSDS();
            //    if (IsAdd)
            //        procedureSDS.UpdateExport(fromDate, toDate, cpp.ID);
            //    else
            //        procedureSDS.UpdateExport(fromDate, toDate, _select.ID);
            //    MSG.Information("Cập nhật dữ liệu thành công");
            //    btnUpdateExport.Enabled = false;
            //}
            //catch (Exception ex)
            //{
            //    return;
            //}

            //Utils.IRSInwardOutwardDetailService.BeginTran();
            //List<int> typeids = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420 };

            //try
            //{
            //    List<RSInwardOutward> _LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
            //    List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.Where(x => x.PostedDate >= fromDate && x.PostedDate <= toDate).ToList();
            //    List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail2 = (from g in _LstRSInwardOutward1
            //                                                              join h in _LstRSInwardOutwardDetail1 on g.ID equals h.RSInwardOutwardID
            //                                                              where typeids.Any(x => x == g.TypeID)
            //                                                              select h).ToList();

            //    List<SAInvoiceDetail> _LstSAInvoiceDetail = Utils.ListSAInvoiceDetail.ToList();
            //    List<SAInvoice> sAInvoices = Utils.ListSAInvoice.Where(x => x.PostedDate >= fromDate && x.PostedDate <= toDate).ToList();
            //    List<Guid> sainvoiceIDs = new List<Guid>();
            //    List<Guid> rsIDs = new List<Guid>();

            //    foreach (var item in _LstCPResult)
            //    {
            //        List<RSInwardOutwardDetail> lstrsid = _LstRSInwardOutwardDetail2.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
            //        List<SAInvoiceDetail> lstsaid = (from g in _LstSAInvoiceDetail join h in sAInvoices on g.SAInvoiceID equals h.ID where g.MaterialGoodsID == item.MaterialGoodsID select g).ToList();

            //        if (lstrsid.Count > 0)
            //        {
            //            foreach (var rsid in lstrsid)
            //            {
            //                rsid.UnitPrice = item.UnitPrice;
            //                rsid.UnitPriceOriginal = item.UnitPrice;
            //                rsid.Amount = Math.Round((rsid.UnitPrice * (decimal)rsid.Quantity), 0);
            //                rsid.AmountOriginal = Math.Round((rsid.UnitPriceOriginal * (decimal)rsid.Quantity), 0);
            //                Utils.IRSInwardOutwardDetailService.Update(rsid);
            //                rsIDs.Add((Guid)rsid.RSInwardOutwardID);
            //            }
            //        }

            //        if (lstsaid.Count > 0)
            //        {
            //            foreach (var said in lstsaid)
            //            {
            //                said.OWPrice = item.UnitPrice;
            //                said.OWPriceOriginal = item.UnitPrice;
            //                said.OWAmount = Math.Round((said.OWPrice * (decimal)said.Quantity), 0);
            //                said.OWAmountOriginal = Math.Round((said.OWPriceOriginal * (decimal)said.Quantity), 0);
            //                Utils.ISAInvoiceDetailService.Update(said);
            //                sainvoiceIDs.Add(said.SAInvoiceID);
            //            }
            //        }

            //        List<GeneralLedger> generalLedgers = Utils.IGeneralLedgerService.GetAll();// Utils.ListGeneralLedger.ToList();

            //        List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.MaterialGoodsID == item.MaterialGoodsID
            //        && x.PostedDate >= fromDate && x.PostedDate <= toDate).ToList();
            //        repositoryLedgers = (from g in repositoryLedgers
            //                             where listCostSetID.Any(x => x == g.CostSetID) && typeids.Any(x => x == g.TypeID)
            //                             select g).ToList();
            //        if (repositoryLedgers.Count > 0)
            //        {
            //            foreach (var rl in repositoryLedgers)
            //            {
            //                rl.UnitPrice = item.UnitPrice;
            //                rl.OWAmount = Math.Round((decimal)(rl.UnitPrice * rl.OWQuantity), 0);
            //                Utils.IRepositoryLedgerService.Update(rl);
            //            }
            //        }

            //        generalLedgers = (from g in generalLedgers
            //                          join h in repositoryLedgers on g.ReferenceID equals h.ReferenceID
            //                          select g).ToList();
            //        if (generalLedgers.Count > 0)
            //        {
            //            foreach (var gl in generalLedgers)
            //            {
            //                SAInvoiceDetail sAInvoiceDetail = lstsaid.FirstOrDefault(x => x.ID == gl.DetailID);
            //                if (sAInvoiceDetail != null)
            //                {
            //                    if (gl.Account == "632")
            //                    {
            //                        gl.CreditAmount = 0;
            //                        gl.CreditAmountOriginal = 0;
            //                        gl.DebitAmount = sAInvoiceDetail.OWAmount;
            //                        gl.DebitAmountOriginal = sAInvoiceDetail.OWAmountOriginal;
            //                    }
            //                    else if (gl.AccountCorresponding == "632")
            //                    {
            //                        gl.CreditAmount = sAInvoiceDetail.OWAmount;
            //                        gl.CreditAmountOriginal = sAInvoiceDetail.OWAmountOriginal;
            //                        gl.DebitAmount = 0;
            //                        gl.DebitAmountOriginal = 0;
            //                    }
            //                    Utils.IGeneralLedgerService.Update(gl);
            //                }
            //            }
            //        }
            //    }

            //    Decimal totalAmount = 0;
            //    Decimal totalAmount1 = 0;
            //    rsIDs = rsIDs.Distinct().ToList();
            //    foreach (var item in rsIDs)
            //    {
            //        RSInwardOutward rSInwardOutward = _LstRSInwardOutward1.FirstOrDefault(x => x.ID == item);
            //        if (rSInwardOutward != null)
            //        {
            //            totalAmount = 0;
            //            List<RSInwardOutwardDetail> rSInwardOutwardDetails = rSInwardOutward.RSInwardOutwardDetails.ToList();
            //            foreach (var item1 in rSInwardOutwardDetails)
            //            {
            //                totalAmount += item1.Amount;
            //            }
            //            rSInwardOutward.TotalAmount = totalAmount;
            //            rSInwardOutward.TotalAmountOriginal = totalAmount;
            //            Utils.IRSInwardOutwardService.Update(rSInwardOutward);
            //        }
            //    }

            //    _LstRSInwardOutward1 = (from g in _LstRSInwardOutward1
            //                            where typeids.Any(x => x == g.TypeID) && g.RSInwardOutwardDetails.Count == 0
            //                            && g.PostedDate >= fromDate && g.PostedDate <= toDate
            //                            select g).ToList();
            //    sainvoiceIDs = sainvoiceIDs.Distinct().ToList();
            //    foreach (var item in sainvoiceIDs)
            //    {
            //        if (item != null)
            //        {
            //            SAInvoice sAInvoice = sAInvoices.FirstOrDefault(x => x.ID == item);
            //            if (sAInvoice != null)
            //            {
            //                totalAmount1 = 0;
            //                List<SAInvoiceDetail> sAInvoiceDetails = sAInvoice.SAInvoiceDetails.ToList();
            //                foreach (var item1 in sAInvoiceDetails)
            //                {
            //                    totalAmount1 += item1.OWAmount;
            //                }
            //                sAInvoice.TotalCapitalAmount = totalAmount1;
            //                sAInvoice.TotalCapitalAmountOriginal = totalAmount1;
            //                Utils.ISAInvoiceService.Update(sAInvoice);
            //            }
            //            RSInwardOutward rSInwardOutward = (from g in _LstRSInwardOutward1 where g.ID == item select g).FirstOrDefault();
            //            if (rSInwardOutward != null)
            //            {
            //                rSInwardOutward.TotalAmount = totalAmount1;
            //                rSInwardOutward.TotalAmountOriginal = totalAmount1;
            //                Utils.IRSInwardOutwardService.Update(rSInwardOutward);
            //            }
            //        }
            //    }

            //    if (cpp != null)
            //    {
            //        if (!IsAdd)
            //            cpp = _select;
            //        cpp.IsDelete = false;
            //        Utils.ICPPeriodService.Update(cpp);
            //    }
            //    Utils.IRSInwardOutwardDetailService.CommitTran();
            //    MSG.Information("Cập nhật dữ liệu thành công");
            //    btnUpdateExport.Enabled = false;
            //}
            //catch (Exception ex)
            //{
            //    Utils.IRSInwardOutwardDetailService.RolbackTran();
            //}
            #endregion
            WaitingFrm.StartWaiting();
            List<Guid> lstMaterialGoods = _LstCPResult.Select(x => x.MaterialGoodsID).ToList();
            Utils.IRSInwardOutwardDetailService.BeginTran();
            List<int> typeids = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420 };

            try
            {
                List<RSInwardOutward> _LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.Recorded == true && typeids.Any(c => c == x.TypeID) && x.PostedDate >= fromDate && x.PostedDate <= toDate).ToList();
                List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.Where(x => lstMaterialGoods.Any(d => d == x.MaterialGoodsID) && _LstRSInwardOutward1.Any(d => d.ID == x.RSInwardOutwardID)).ToList();
                List<SAInvoice> sAInvoices = Utils.ListSAInvoice.Where(x => x.PostedDate >= fromDate && x.PostedDate <= toDate && x.Recorded == true).ToList();
                List<SAInvoiceDetail> _LstSAInvoiceDetail = Utils.ListSAInvoiceDetail.ToList().Where(x => lstMaterialGoods.Any(d => d == x.MaterialGoodsID) && sAInvoices.Any(d => d.ID == x.SAInvoiceID)).ToList(); ;
                List<Guid> sainvoiceIDs = new List<Guid>();
                List<Guid> rsIDs = new List<Guid>();

                foreach (var item in _LstCPResult)
                {
                    //với chứng từ là xuất kho
                    List<RSInwardOutwardDetail> lstrsid = _LstRSInwardOutwardDetail1.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
                    if (lstrsid.Count > 0)
                    {
                        foreach (var rsid in lstrsid)
                        {
                            rsid.UnitPrice = item.UnitPrice;
                            rsid.UnitPriceOriginal = item.UnitPrice;
                            rsid.Amount = (rsid.UnitPrice * (decimal)rsid.Quantity);
                            rsid.AmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                            Utils.IRSInwardOutwardDetailService.Update(rsid);
                            if (!rsIDs.Any(x => x == (Guid)rsid.RSInwardOutwardID))
                                rsIDs.Add((Guid)rsid.RSInwardOutwardID);

                            // lưu gl
                            List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList().Where(x => x.DetailID == rsid.ID).ToList();
                            if (generalLedgers.Count > 0)
                            {
                                foreach (var gl in generalLedgers)
                                {
                                    if (gl.Account == rsid.CreditAccount)
                                    {
                                        gl.CreditAmount = (item.UnitPrice * (decimal)rsid.Quantity);
                                        gl.CreditAmountOriginal = (item.UnitPrice * (decimal)rsid.Quantity);
                                    }
                                    else if (gl.Account == rsid.DebitAccount)
                                    {
                                        gl.DebitAmount = (item.UnitPrice * (decimal)rsid.Quantity);
                                        gl.DebitAmountOriginal = (item.UnitPrice * (decimal)rsid.Quantity);
                                    }
                                    Utils.IGeneralLedgerService.Update(gl);
                                }
                            }
                            //lưu rl
                            List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.DetailID == rsid.ID).ToList();
                            if (repositoryLedgers.Count > 0)
                            {
                                foreach (var rl in repositoryLedgers)
                                {
                                    rl.UnitPrice = item.UnitPrice;
                                    rl.IWAmount = (decimal)(rl.UnitPrice * rl.IWQuantity);
                                    rl.IWAmountBalance = (rl.UnitPrice * (decimal)rl.IWQuantity);
                                    Utils.IRepositoryLedgerService.Update(rl);
                                }
                            }
                        }
                    }
                    //với chứng từ là xuất kho từ bán hàng
                    List<SAInvoiceDetail> lstsaid = _LstSAInvoiceDetail.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
                    if (lstsaid.Count > 0)
                    {
                        foreach (var said in lstsaid)
                        {
                            said.OWPrice = item.UnitPrice;
                            said.OWPriceOriginal = item.UnitPrice;
                            said.OWAmount = (said.OWPrice * (decimal)said.Quantity);
                            said.OWAmountOriginal = (said.OWPriceOriginal * (decimal)said.Quantity);
                            Utils.ISAInvoiceDetailService.Update(said);
                            if (!sainvoiceIDs.Any(x => x == (Guid)said.SAInvoiceID))
                                sainvoiceIDs.Add(said.SAInvoiceID);
                            // lưu gl
                            List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList().Where(x => x.DetailID == said.ID).ToList();
                            if (generalLedgers.Count > 0)
                            {
                                foreach (var gl in generalLedgers)
                                {
                                    if (gl.AccountCorresponding == "632")
                                    {
                                        gl.CreditAmount = (item.UnitPrice * ((decimal)said.Quantity));
                                        gl.CreditAmountOriginal = (item.UnitPrice * (decimal)said.Quantity);
                                    }
                                    else if (gl.Account == "632")
                                    {
                                        gl.DebitAmount = (item.UnitPrice * (decimal)said.Quantity);
                                        gl.DebitAmountOriginal = (item.UnitPrice * (decimal)said.Quantity);
                                    }
                                    Utils.IGeneralLedgerService.Update(gl);
                                }
                            }
                            //lưu rl
                            List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.DetailID == said.ID).ToList();
                            if (repositoryLedgers.Count > 0)
                            {
                                foreach (var rl in repositoryLedgers)
                                {
                                    rl.UnitPrice = item.UnitPrice;
                                    rl.IWAmount = (decimal)(rl.UnitPrice * rl.IWQuantity);
                                    rl.IWAmountBalance = (rl.UnitPrice * (decimal)rl.IWQuantity);
                                    Utils.IRepositoryLedgerService.Update(rl);
                                }
                            }
                        }

                    }
                }
                foreach (var item in rsIDs)
                {
                    RSInwardOutward rSInwardOutward = _LstRSInwardOutward1.FirstOrDefault(x => x.ID == item);
                    if (rSInwardOutward != null)
                    {
                        rSInwardOutward.TotalAmount = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        rSInwardOutward.TotalAmountOriginal = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        Utils.IRSInwardOutwardService.Update(rSInwardOutward);
                    }
                }
                foreach (var item in sainvoiceIDs)
                {
                    if (item != null)
                    {
                        SAInvoice sAInvoice = sAInvoices.FirstOrDefault(x => x.ID == item);
                        if (sAInvoice != null)
                        {
                            sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            Utils.ISAInvoiceService.Update(sAInvoice);
                        }
                        RSInwardOutward rSInwardOutward = (from g in _LstRSInwardOutward1 where g.ID == item select g).FirstOrDefault();
                        if (rSInwardOutward != null)
                        {
                            rSInwardOutward.TotalAmount = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            rSInwardOutward.TotalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            Utils.IRSInwardOutwardService.Update(rSInwardOutward);
                        }
                    }
                }

                //comment by cuongpv
                //if (cpp != null)
                //{
                //    if (!IsAdd)
                //        cpp = _select;
                //    cpp.IsDelete = false;
                //    Utils.ICPPeriodService.Update(cpp);
                //}
                //end comment by cuongpv
                Utils.IRSInwardOutwardDetailService.CommitTran();
                WaitingFrm.StopWaiting();
                MSG.Information("Cập nhật dữ liệu thành công");
                btnUpdateExport.Enabled = false;
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                Utils.IRSInwardOutwardDetailService.RolbackTran();
            }

        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteExpenseList1();
            DeleteExpenseList2();
            DeleteAllocationGeneralExpense();
        }

        private void btnAttribution_Click(object sender, EventArgs e)
        {
            checkAtt = 1;
            lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();
            List<ExpenseItem> lstExpenseItem = Utils.ListExpenseItem.ToList();
            List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpenseUgrid = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList();

            foreach (var item in lstCPAllocationGeneralExpenseUgrid)
            {
                if (item.AllocatedRate > 100)
                {
                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    return;
                }
                else
                {
                    if (item.AllocationMethod == 0)
                    {
                        List<CPExpenseList> lstCPExpenseList = (from g in _LstCPExpense
                                                                join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                where h.ExpenseType == 0
                                                                select g).ToList();
                        decimal sumAmount = (from g in lstCPExpenseList select g.Amount).Sum();
                        List<CPExpenseList> lstCPExpenseList1 = lstCPExpenseList.GroupBy(x => x.CostsetCode).Select(x => x.First()).ToList();

                        if (lstCPExpenseList1.Count > 0)
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstCPExpenseList1
                                                                           join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                           where h.ExpenseType == 0
                                                                           group g by new { g.CostSetID, g.CostsetCode, g.CostsetName, g.ExpenseItemCode, g.Amount }
                                        into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.CostSetID,
                                                                               CostsetCode = t.Key.CostsetCode,
                                                                               CostsetName = t.Key.CostsetName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               AllocatedRate = (from g in lstCPExpenseList
                                                                                                where g.CostSetID == t.Key.CostSetID
                                                                                                select g.Amount).Sum() / sumAmount * 100,
                                                                               AllocatedAmount = Math.Round(((from g in lstCPExpenseList
                                                                                                              where g.CostSetID == t.Key.CostSetID
                                                                                                              select g.Amount).Sum() / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero),
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               CPAllocationGeneralExpenseID = item.ID
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                        else
                        {
                            foreach (var item1 in listCostSetID)
                            {
                                CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                                cpg.CostSetID = item1;
                                cpg.CostsetName = Utils.ICostSetService.Getbykey(item1).CostSetName;
                                cpg.CostsetCode = Utils.ICostSetService.Getbykey(item1).CostSetCode;
                                cpg.ExpenseItemCode = item.ExpenseItemCode;
                                cpg.AllocatedRate = 0;
                                cpg.AllocatedAmount = 0;
                                cpg.ExpenseItemID = item.ExpenseItemID;
                                cpg.CPAllocationGeneralExpenseID = item.ID;
                                lstAllocationGeneralExpenseDetail.Add(cpg);
                            }
                        }
                    }
                    else if (item.AllocationMethod == 1)
                    {
                        List<CPExpenseList> lstCPExpenseList = (from g in _LstCPExpense
                                                                join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                where h.ExpenseType == 1
                                                                select g).ToList();
                        decimal sumAmount = (from g in lstCPExpenseList select g.Amount).Sum();
                        List<CPExpenseList> lstCPExpenseList1 = lstCPExpenseList.GroupBy(x => x.CostsetCode).Select(x => x.First()).ToList();

                        if (lstCPExpenseList1.Count > 0)
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstCPExpenseList1
                                                                           group g by new { g.CostSetID, g.CostsetCode, g.CostsetName, g.ExpenseItemCode, g.Amount, g.ExpenseItemID }
                                    into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.CostSetID,
                                                                               CostsetCode = t.Key.CostsetCode,
                                                                               CostsetName = t.Key.CostsetName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               AllocatedRate = (from g in lstCPExpenseList
                                                                                                where g.CostSetID == t.Key.CostSetID
                                                                                                select g.Amount).Sum() / sumAmount * 100,
                                                                               AllocatedAmount = Math.Round(((from g in lstCPExpenseList
                                                                                                              where g.CostSetID == t.Key.CostSetID
                                                                                                              select g.Amount).Sum() / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero),
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               CPAllocationGeneralExpenseID = item.ID
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                        else
                        {
                            foreach (var item1 in listCostSetID)
                            {
                                CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                                cpg.CostSetID = item1;
                                cpg.CostsetName = Utils.ICostSetService.Getbykey(item1).CostSetName;
                                cpg.CostsetCode = Utils.ICostSetService.Getbykey(item1).CostSetCode;
                                cpg.ExpenseItemCode = item.ExpenseItemCode;
                                cpg.AllocatedRate = 0;
                                cpg.AllocatedAmount = 0;
                                cpg.ExpenseItemID = item.ExpenseItemID;
                                cpg.CPAllocationGeneralExpenseID = item.ID;
                                lstAllocationGeneralExpenseDetail.Add(cpg);
                            }
                        }
                    }
                    else if (item.AllocationMethod == 2)
                    {
                        List<CPExpenseList> lstCPExpenseList = (from g in _LstCPExpense
                                                                join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                where (h.ExpenseType == 1 || h.ExpenseType == 0)
                                                                select g).ToList();
                        decimal sumAmount = (from g in lstCPExpenseList select g.Amount).Sum();
                        var lstCPExpenseList2 = (from a in lstCPExpenseList
                                                 group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                                                 select new
                                                 {
                                                     CostSetID = t.Key.CostSetID,
                                                     CostsetCode = t.Key.CostsetCode,
                                                     CostsetName = t.Key.CostsetName,
                                                     Amount = t.Sum(x => x.Amount),
                                                 }).ToList();
                        if (lstCPExpenseList2.Count > 0)
                        {
                            foreach (var model in lstCPExpenseList2)
                            {
                                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                                cPAllocationGeneralExpenseDetail.CPAllocationGeneralExpenseID = item.ID;
                                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                                cPAllocationGeneralExpenseDetail.ExpenseItemID = item.ExpenseItemID;
                                cPAllocationGeneralExpenseDetail.ExpenseItemCode = item.ExpenseItemCode;
                                cPAllocationGeneralExpenseDetail.AllocatedRate = model.Amount * 100 / sumAmount;
                                cPAllocationGeneralExpenseDetail.AllocatedAmount = Math.Round((model.Amount / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero);
                                lstAllocationGeneralExpenseDetail.Add(cPAllocationGeneralExpenseDetail);
                            }
                        }
                        else
                        {
                            foreach (var item1 in listCostSetID)
                            {
                                CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                                cpg.CostSetID = item1;
                                cpg.CostsetName = Utils.ICostSetService.Getbykey(item1).CostSetName;
                                cpg.CostsetCode = Utils.ICostSetService.Getbykey(item1).CostSetCode;
                                cpg.ExpenseItemCode = item.ExpenseItemCode;
                                cpg.AllocatedRate = 0;
                                cpg.AllocatedAmount = 0;
                                cpg.ExpenseItemID = item.ExpenseItemID;
                                cpg.CPAllocationGeneralExpenseID = item.ID;
                                lstAllocationGeneralExpenseDetail.Add(cpg);
                            }
                        }
                    }
                    else
                    {
                        List<CPAccountAllocationQuantum> lstAllocationQuantum = GetCPAccountAllocationQuantum();
                        List<CPAccountAllocationQuantum> lstAllocationQuantum1 = (from g in lstAllocationQuantum
                                                                                  where listCostSetID.Any(x => x == g.AccountingObjectID)
                                                                                  select g).ToList();
                        decimal sumAmount = (decimal)(from g in lstAllocationQuantum1
                                                      select g.TotalCostAmount).Sum();
                        try
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstAllocationQuantum1
                                                                           group g by new { g.AccountingObjectID, g.AccountingObjectCode, g.AccountingObjectName, g.TotalCostAmount }
                                        into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.AccountingObjectID,
                                                                               CostsetCode = t.Key.AccountingObjectCode,
                                                                               CostsetName = t.Key.AccountingObjectName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               CPAllocationGeneralExpenseID = item.ID,
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               AllocatedRate = (decimal)t.Key.TotalCostAmount / sumAmount * 100,
                                                                               AllocatedAmount = Math.Round(((decimal)t.Key.TotalCostAmount / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero),
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                        catch
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstAllocationQuantum1
                                                                           group g by new { g.AccountingObjectID, g.AccountingObjectCode, g.AccountingObjectName, g.TotalCostAmount }
                                    into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.AccountingObjectID,
                                                                               CostsetCode = t.Key.AccountingObjectCode,
                                                                               CostsetName = t.Key.AccountingObjectName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               CPAllocationGeneralExpenseID = item.ID,
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               AllocatedRate = 0,
                                                                               AllocatedAmount = 0,
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                    }
                }
            }
            lstAllocationGeneralExpenseDetail = lstAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).Where(x => x.AllocatedAmount > 0).ToList();

            //add by cuongpv don gia tri con lai vao dong cuoi cung
            decimal sumCPPhanBo = lstCPAllocationGeneralExpenseUgrid.Sum(x => x.AllocatedAmount);
            decimal sumCPDaPhanBoDetail = lstAllocationGeneralExpenseDetail.Sum(x => x.AllocatedAmount);
            if (sumCPPhanBo != sumCPDaPhanBoDetail)
            {
                int iCount = lstAllocationGeneralExpenseDetail.Count;
                int iMaxCount = iCount - 1;
                for (int i = 0; i < iCount; i++)
                {
                    if (i == iMaxCount)
                        lstAllocationGeneralExpenseDetail[i].AllocatedAmount = sumCPPhanBo;
                    else
                        sumCPPhanBo = sumCPPhanBo - lstAllocationGeneralExpenseDetail[i].AllocatedAmount;
                }
            }
            //end add by cuongpv

            uGridAllocationGeneralExpenseResult.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstAllocationGeneralExpenseDetail);
            Utils.ConfigGrid(uGridAllocationGeneralExpenseResult, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
            Utils.AddSumColumn(uGridAllocationGeneralExpenseResult, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            foreach (var col in uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "AllocatedRate" || col.Key == "AllocatedAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }
        private void uGridAllocationGeneralExpense_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                try
                {
                    if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Text.Trim('_').ToString()) <= 100)
                    {
                        e.Cell.Row.Cells["AllocatedAmount"].Value = Math.Round((Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) / 100), lamtron, MidpointRounding.AwayFromZero);
                        uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                    }
                    else
                    {
                        MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                        return;
                    }
                }
                catch { }
            }
        }

        private void uGridAllocationGeneralExpense_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //checkAtt = 0;
            //checkupdate = 1;
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "AllocatedRate" || activeCell.Column.Key == "AllocatedAmount")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
            if (activeCell.Column.Key == "AllocatedRate")
            {
                if (Decimal.Parse(activeCell.Text.Trim('_').ToString()) > 100)
                {
                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    return;
                }
            }
        }

        private void uGridAllocationGeneralExpense_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkAtt = 0;
            checkupdate = 1;
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) <= 100)
                {
                    e.Cell.Row.Cells["AllocatedAmount"].Value = Math.Round((Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) / 100), lamtron, MidpointRounding.AwayFromZero);
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
            if (e.Cell.Column.Key.Equals("AllocatedAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) != 100)
                {
                    e.Cell.Row.Cells["AllocatedRate"].Value = Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * 100;
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
                else { }
            }
            List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).Where(x => x.AllocatedRate > 0).ToList();
            if (cPAllocationGeneralExpenses.Count == 0)
                checkAtt = 1;
        }
        private void uGridAllocationGeneralExpenseResult_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //checkupdate = 1;
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "AllocatedRate" || activeCell.Column.Key == "AllocatedAmount")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }
        private void optType_ValueChanged(object sender, EventArgs e)
        {
            #region comment by cuongpv code cu
            //checkupdate = 1;
            //_LstCPUncompleteDetail = GetCPUncompleteDetail();
            //uGridUncompleteDetail.DataSource = new BindingList<CPUncompleteDetail>(_LstCPUncompleteDetail);
            //Utils.ConfigGrid(uGridUncompleteDetail, ConstDatabase.CPUncompleteDetail_TableName);
            //uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["Quantity"].CellClickAction = CellClickAction.EditAndSelectText;
            //if (optType.CheckedIndex == 0)
            //{
            //    checkItem = 0;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            //}
            //else if (optType.CheckedIndex == 1)
            //{
            //    checkItem = 1;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].Hidden = true;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            //}
            //else
            //{
            //    checkItem = 2;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Width = 150;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].Width = 130;
            //}
            #endregion
            #region add by cuongpv code moi
            ViewGridPanel5();
            btnDetermine_Click(sender, e);
            #endregion
        }

        private void uGridCpExpenseList2_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uGridCpExpenseList3_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void uGridAllocationGeneralExpense_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }
        #endregion

        #region hàm xử lý
        private void DeleteExpenseList1()
        {
            if (!IsAdd)
            {
                if (uGridCpExpenseList2.Selected.Rows.Count > 0)
                {
                    CPExpenseList temp = uGridCpExpenseList2.Selected.Rows[0].ListObject as CPExpenseList;
                    var model = Utils.ListCPExpenseList.FirstOrDefault(x => x.ID == temp.ID);
                    if (model != null && MSG.Question(string.Format(resSystem.MSG_System_78, model.CostsetName)) == DialogResult.Yes)
                    {
                        try
                        {
                            Utils.ICPExpenseListService.BeginTran();
                            Utils.ICPExpenseListService.Delete(model);
                            Utils.ICPExpenseListService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            Utils.ICPExpenseListService.RolbackTran();
                        }
                        Utils.ClearCacheByType<CPExpenseList>();
                        ViewGridPanel2();
                    }
                }
            }
            else
            {
                if (uGridCpExpenseList2.Selected.Rows.Count > 0)
                {
                    CPExpenseList temp = uGridCpExpenseList2.Selected.Rows[0].ListObject as CPExpenseList;
                    _LstCPExpense.Remove(temp);
                    uGridCpExpenseList2.DataSource = new BindingList<CPExpenseList>(_LstCPExpense);
                    Utils.ConfigGrid(uGridCpExpenseList2, ConstDatabase.CPExpenseList_TableName);
                    uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["CostsetCode"].Header.Caption = "Đối tượng THCP";
                    uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
        }

        private void DeleteExpenseList2()
        {
            if (!IsAdd)
            {
                if (uGridCpExpenseList3.Selected.Rows.Count > 0)
                {
                    CPExpenseList temp = uGridCpExpenseList3.Selected.Rows[0].ListObject as CPExpenseList;
                    var model = Utils.ListCPExpenseList.FirstOrDefault(x => x.ID == temp.ID);
                    if (model != null && MSG.Question(string.Format(resSystem.MSG_System_78, model.CostsetName)) == DialogResult.Yes)
                    {
                        try
                        {
                            Utils.ICPExpenseListService.BeginTran();
                            Utils.ICPExpenseListService.Delete(model);
                            Utils.ICPExpenseListService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            Utils.ICPExpenseListService.RolbackTran();
                        }
                        Utils.ClearCacheByType<CPExpenseList>();
                        ViewGridPanel3();
                    }
                }
            }
            else
            {
                if (uGridCpExpenseList3.Selected.Rows.Count > 0)
                {
                    CPExpenseList temp = uGridCpExpenseList3.Selected.Rows[0].ListObject as CPExpenseList;
                    _LstCPExpense2.Remove(temp);
                    uGridCpExpenseList3.DataSource = new BindingList<CPExpenseList>(_LstCPExpense2);
                    Utils.ConfigGrid(uGridCpExpenseList3, ConstDatabase.CPExpenseList_TableName);
                    uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["CostsetCode"].Header.Caption = "Đối tượng THCP";
                    uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
        }

        private void DeleteAllocationGeneralExpense()
        {
            if (!IsAdd)
            {
                if (uGridAllocationGeneralExpense.Selected.Rows.Count > 0)
                {
                    CPAllocationGeneralExpense temp = uGridAllocationGeneralExpense.Selected.Rows[0].ListObject as CPAllocationGeneralExpense;
                    var model = Utils.ListCPAllocationGeneralExpense.FirstOrDefault(x => x.ID == temp.ID);
                    if (model != null && MSG.Question(string.Format(resSystem.MSG_System_78, model.ExpenseItemCode)) == DialogResult.Yes)
                    {
                        try
                        {
                            Utils.ICPAllocationGeneralExpenseService.BeginTran();
                            Utils.ICPAllocationGeneralExpenseService.Delete(model);
                            Utils.ICPAllocationGeneralExpenseService.CommitTran();
                        }
                        catch (Exception ex)
                        {
                            Utils.ICPAllocationGeneralExpenseService.RolbackTran();
                        }
                        Utils.ClearCacheByType<CPAllocationGeneralExpense>();
                        ViewGridPanel4();
                    }
                }
            }
            else
            {
                if (uGridAllocationGeneralExpense.Selected.Rows.Count > 0)
                {
                    CPAllocationGeneralExpense temp = uGridAllocationGeneralExpense.Selected.Rows[0].ListObject as CPAllocationGeneralExpense;
                    lstAllocationGeneralExpense.Remove(temp);
                    uGridAllocationGeneralExpense.DataSource = new BindingList<CPAllocationGeneralExpense>(lstAllocationGeneralExpense);
                    Utils.ConfigGrid(uGridAllocationGeneralExpense, ConstDatabase.CPAllocationGeneralExpense_TableName);
                    var cbbMethods = new UltraComboEditor();
                    cbbMethods.Items.Add(0, "Nguyên vật liệu trực tiếp");
                    cbbMethods.Items.Add(1, "Nhân công trực tiếp");
                    cbbMethods.Items.Add(2, "Chi phí trực tiếp");
                    cbbMethods.Items.Add(3, "Định mức");
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethodView"].Hidden = true;
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].Hidden = false;
                    foreach (var column in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
                    {
                        if (column.Key.Equals("AllocationMethod"))
                        {
                            column.EditorComponent = cbbMethods;
                            column.Style = ColumnStyle.DropDownList;
                        }
                        if (column.Key == "AllocatedRate")
                        {
                            column.CellClickAction = CellClickAction.EditAndSelectText;
                        }
                        else if (column.Key == "AllocationMethod")
                        {
                            column.CellClickAction = CellClickAction.EditAndSelectText;
                        }
                        else
                        {
                            column.CellActivation = Activation.NoEdit;
                        }
                    }
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["TotalCost"].FormatNumberic(ConstDatabase.Format_TienVND);
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["UnallocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
        }

        private void ViewGridPanel1()
        {
            listcostset = new List<SelectCostSet>();
            if (IsAdd)
            {
                listcostset = Utils.ICostSetService.GetListByType(4);
                if (listcostset.Count == 0)
                    listcostset = new List<SelectCostSet>();
            }
            else
            {
                List<CPPeriodDetail> lstCPPeriodDetail = ICPPeriodDetailService.GetList().Where(x => x.CPPeriodID == _select.ID).ToList();
                foreach (var item in lstCPPeriodDetail)
                {
                    CostSet cs = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID);
                    SelectCostSet scs = new SelectCostSet();
                    scs.ID = cs.ID;
                    scs.Select = true;
                    scs.CostSetCode = cs.CostSetCode;
                    scs.CostSetName = cs.CostSetName;
                    scs.CostSetType = cs.CostSetType;
                    scs.CostSetTypeView = cs.CostSetTypeView;
                    listcostset.Add(scs);
                }
            }
            if (listcostset.Count > 0)
                listcostset = listcostset.OrderBy(x => x.CostSetCode).ToList();
            uGridCostset.DataSource = new BindingList<SelectCostSet>(listcostset);
            Utils.ConfigGrid(uGridCostset, ConstDatabase.SelectCostSet_TableName);
            var gridBand = uGridCostset.DisplayLayout.Bands[0];
            gridBand.Columns["Select"].Header.VisiblePosition = 0;
            gridBand.Columns["Select"].Hidden = false;
            gridBand.Columns["Select"].Style = ColumnStyle.CheckBox;
            gridBand.Columns["Select"].AutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
            gridBand.Columns["Select"].CellClickAction = CellClickAction.Edit;
            uGridCostset.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCostset.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            gridBand.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            gridBand.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            gridBand.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            gridBand.Columns["Select"].Header.Fixed = true;
            gridBand.Columns["CostSetCode"].Width = 250;
            Utils.ProcessControls(this);
            if (!IsAdd)
            {
                gridBand.Columns["Select"].Hidden = true;
                cbbDateTime.Text = "";
                dtBeginDate.Value = _select.FromDate;
                dtEndDate.Value = _select.ToDate;
            }

            if (IsAdd)
            {
                DateTime dtbegin = dtBeginDate.DateTime;
                DateTime dtend = dtEndDate.DateTime;
                fromDate = new DateTime(dtbegin.Year, dtbegin.Month, dtbegin.Day, 0, 0, 0);
                toDate = new DateTime(dtend.Year, dtend.Month, dtend.Day, 23, 59, 59);
            }
        }
        private void ViewGridPanel2()
        {
            _LstCPExpense = new List<CPExpenseList>();
            if (IsAdd)
            {
                _LstCPExpense = Utils.IGeneralLedgerService.GetCPExpenseListsOnGL(fromDate, toDate, listCostSetID);
                _LstCPExpense = _LstCPExpense.OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
                if (_LstCPExpense.Count == 0)
                    _LstCPExpense = new List<CPExpenseList>();
            }
            else
            {
                foreach (var item in _select.CPExpenseLists)
                {
                    item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                    item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                }
                _LstCPExpense = _select.CPExpenseLists.Where(x => x.TypeVoucher == 0).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();

            }

            uGridCpExpenseList2.DataSource = new BindingList<CPExpenseList>(_LstCPExpense.OrderBy(x => x.PostedDate).ToList());
            Utils.ConfigGrid(uGridCpExpenseList2, ConstDatabase.CPExpenseList_TableName);
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["CostsetCode"].Header.Caption = "Đối tượng THCP";
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCpExpenseList2, "Amount", false, "", ConstDatabase.Format_TienVND);
        }
        private void ViewGridPanel3()
        {
            _LstCPExpense2 = new List<CPExpenseList>();
            if (IsAdd)
            {
                _LstCPExpense2 = Utils.IGeneralLedgerService.GetCPExpenseListsOnGL2(fromDate, toDate, listCostSetID);
                _LstCPExpense2 = _LstCPExpense2.OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
                if (_LstCPExpense2.Count == 0)
                    _LstCPExpense2 = new List<CPExpenseList>();
            }
            else
            {
                foreach (var item in _select.CPExpenseLists)
                {
                    item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                    item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                }
                _LstCPExpense2 = _select.CPExpenseLists.Where(x => x.TypeVoucher == 1).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
            }

            uGridCpExpenseList3.DataSource = new BindingList<CPExpenseList>(_LstCPExpense2.OrderBy(x => x.PostedDate).ToList());
            Utils.ConfigGrid(uGridCpExpenseList3, ConstDatabase.CPExpenseList_TableName);
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["CostsetCode"].Header.Caption = "Đối tượng THCP";
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCpExpenseList3, "Amount", false, "", ConstDatabase.Format_TienVND);
        }
        private void ViewGridPanel4()
        {
            lstAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
            lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();

            //lay tat ca cac chi phi da luu trong CPAllocationGeneralExpense
            List<CPAllocationGeneralExpense> listCPAllocationGeneralExpenseDB = ICPAllocationGeneralExpenseService.GetList();
            List<Guid> lstReferenceIDDB = (from g in listCPAllocationGeneralExpenseDB select g.ReferenceID).ToList();

            //lay cac chi phi chua phan bo het trong bang CPAllocationGeneralExpense
            List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpenseUnallocated = (from g in listCPAllocationGeneralExpenseDB
                                                                                         group g by new { g.ReferenceID }
                                                    into t
                                                                                         select new CPAllocationGeneralExpense()
                                                                                         {
                                                                                             ReferenceID = (Guid)t.Key.ReferenceID,
                                                                                             TotalCost = t.Max(x => x.TotalCost),
                                                                                             AllocatedRate = t.Sum(x => x.AllocatedRate),
                                                                                             AllocatedAmount = t.Sum(x => x.AllocatedAmount)
                                                                                         }).ToList();

            lstCPAllocationGeneralExpenseUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.AllocatedAmount < x.TotalCost).ToList();

            List<Guid> lstReferenceIDUnallocated = (from g in lstCPAllocationGeneralExpenseUnallocated select g.ReferenceID).ToList();
            
            if (IsAdd)
            {
                #region lay CP ky truoc

                //lay chi phi cac ky truoc tu GL
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBefore = Utils.IGeneralLedgerService.GetCPAllocationGeneralExpenseOnGLBefore(fromDate);

                //lay chi phi chua phan bo cua cac ky truoc
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBeforeNotUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseBeforeNotUnallocated = (from g in lstAllocationGeneralExpenseBefore where !lstReferenceIDDB.Any(x => x == g.ReferenceID) select g).ToList();
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseBeforeNotUnallocated);

                //lay cac chi phi chua phan bo het cua cac ky truoc
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBeforeUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseBeforeUnallocated = (from g in lstAllocationGeneralExpenseBefore where lstReferenceIDUnallocated.Any(x => x == g.ReferenceID) select g).ToList();
                var itemNotUnallocated = new CPAllocationGeneralExpense();
                foreach (var itemBeforeUnallocated in lstAllocationGeneralExpenseBeforeUnallocated)
                {
                    itemNotUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.ReferenceID == itemBeforeUnallocated.ReferenceID).FirstOrDefault();
                    if (!itemNotUnallocated.IsNullOrEmpty())
                    {
                        itemBeforeUnallocated.UnallocatedAmount = itemNotUnallocated.TotalCost - itemNotUnallocated.AllocatedAmount;
                    }
                }
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseBeforeUnallocated);
                #endregion

                #region lay CP ky nay
                //lay chi phi ky nay tu GL
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHere = Utils.IGeneralLedgerService.GetCPAllocationGeneralExpenseOnGL(fromDate, toDate);

                //lay chi phi chua phan bo cua ky nay
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHereNotUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseHereNotUnallocated = (from g in lstAllocationGeneralExpenseHere where !lstReferenceIDDB.Any(x => x == g.ReferenceID) select g).ToList();
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseHereNotUnallocated);

                //lay chi phi chua phan bo het cua ky nay
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHereUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseHereUnallocated = (from g in lstAllocationGeneralExpenseHere where lstReferenceIDUnallocated.Any(x => x == g.ReferenceID) select g).ToList();
                var itemHereNotUnallocated = new CPAllocationGeneralExpense();
                foreach (var itemBeforeUnallocated in lstAllocationGeneralExpenseHereUnallocated)
                {
                    itemHereNotUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.ReferenceID == itemBeforeUnallocated.ReferenceID).FirstOrDefault();
                    if (!itemHereNotUnallocated.IsNullOrEmpty())
                    {
                        itemBeforeUnallocated.UnallocatedAmount = itemHereNotUnallocated.TotalCost - itemHereNotUnallocated.AllocatedAmount;
                    }
                }
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseHereUnallocated);

                #endregion

                if (lstAllocationGeneralExpense.Count == 0)
                    lstAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
                else
                {
                    foreach (var item in lstAllocationGeneralExpense)
                    {
                        item.ExpenseItemCode = Utils.ListExpenseItem.Where(x => x.ID == item.ExpenseItemID).FirstOrDefault().ExpenseItemCode;
                        item.AllocatedRate = 100;
                        item.AllocatedAmount = item.UnallocatedAmount;
                    }
                }

                lstAllocationGeneralExpense = lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList();

                foreach (var item in lstAllocationGeneralExpense)
                {
                    if (item.AllocationMethod == 0)
                        item.AllocationMethodView = "Nguyên vật liệu trực tiếp";
                    else if (item.AllocationMethod == 1)
                        item.AllocationMethodView = "Nhân công trực tiếp";
                    else if (item.AllocationMethod == 2)
                        item.AllocationMethodView = "Chi phí trực tiếp";
                    else if (item.AllocationMethod == 3)
                        item.AllocationMethodView = "Định mức";
                }
                uGridAllocationGeneralExpense.DataSource = new BindingList<CPAllocationGeneralExpense>(lstAllocationGeneralExpense);
                Utils.ConfigGrid(uGridAllocationGeneralExpense, ConstDatabase.CPAllocationGeneralExpense_TableName);
                var cbbMethods = new UltraComboEditor();
                cbbMethods.Items.Add(0, "Nguyên vật liệu trực tiếp");
                cbbMethods.Items.Add(1, "Nhân công trực tiếp");
                cbbMethods.Items.Add(2, "Chi phí trực tiếp");
                cbbMethods.Items.Add(3, "Định mức");
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethodView"].Hidden = true;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].Hidden = false;
                foreach (var column in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
                {
                    if (column.Key.Equals("AllocationMethod"))
                    {
                        column.EditorComponent = cbbMethods;
                        column.Style = ColumnStyle.DropDownList;
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    if (column.Key == "AllocatedRate" || column.Key == "AllocatedAmount")
                    {
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    else
                    {
                        column.CellActivation = Activation.NoEdit;
                    }
                }
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].CellActivation = Activation.AllowEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].CellActivation = Activation.AllowEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].CellActivation = Activation.NoEdit;
            }
            else
            {
                lstAllocationGeneralExpense = ICPAllocationGeneralExpenseService.GetAll().Where(x => x.CPPeriodID == _select.ID && x.AllocatedRate > 0).ToList();
                foreach (var item in lstAllocationGeneralExpense)
                {
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                    if (item.AllocationMethod == 0)
                        item.AllocationMethodView = "Nguyên vật liệu trực tiếp";
                    else if (item.AllocationMethod == 1)
                        item.AllocationMethodView = "Nhân công trực tiếp";
                    else if (item.AllocationMethod == 2)
                        item.AllocationMethodView = "Chi phí trực tiếp";
                    else if (item.AllocationMethod == 3)
                        item.AllocationMethodView = "Định mức";

                    List<CPAllocationGeneralExpenseDetail> lst = item.CPAllocationGeneralExpenseDetails.ToList();
                    lstAllocationGeneralExpenseDetail.AddRange(lst);
                }
                foreach (var item1 in lstAllocationGeneralExpenseDetail)
                {
                    item1.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item1.CostSetID).CostSetCode;
                    item1.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item1.CostSetID).CostSetName;
                    item1.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item1.ExpenseItemID).ExpenseItemCode;
                }
                uGridAllocationGeneralExpense.DataSource = new BindingList<CPAllocationGeneralExpense>(lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList());
                Utils.ConfigGrid(uGridAllocationGeneralExpense, ConstDatabase.CPAllocationGeneralExpense_TableName);
                var cbbMethods = new UltraComboEditor();
                cbbMethods.Items.Add(0, "Nguyên vật liệu trực tiếp");
                cbbMethods.Items.Add(1, "Nhân công trực tiếp");
                cbbMethods.Items.Add(2, "Chi phí trực tiếp");
                cbbMethods.Items.Add(3, "Định mức");
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethodView"].Hidden = true;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].Hidden = false;
                foreach (var column in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
                {
                    if (column.Key.Equals("AllocationMethod"))
                    {
                        column.EditorComponent = cbbMethods;
                        column.Style = ColumnStyle.DropDownList;
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    if (column.Key == "AllocatedRate" || column.Key == "AllocatedAmount")
                    {
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    else
                    {
                        column.CellActivation = Activation.NoEdit;
                    }
                }
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].CellActivation = Activation.NoEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].CellActivation = Activation.AllowEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].CellActivation = Activation.NoEdit;
            }

            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["TotalCost"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "TotalCost", false, "", ConstDatabase.Format_TienVND);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["UnallocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "UnallocatedAmount", false, "", ConstDatabase.Format_TienVND);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            lstAllocationGeneralExpenseDetail = lstAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).ToList();
            uGridAllocationGeneralExpenseResult.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstAllocationGeneralExpenseDetail);
            Utils.ConfigGrid(uGridAllocationGeneralExpenseResult, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1); //rate2
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpenseResult, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            foreach (var col in uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "AllocatedRate" || col.Key == "AllocatedAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }
        private void ViewGridPanel5()
        {
            _LstCPUncomplete = new List<CPUncomplete>();
            _LstCPUncompleteDetail = new List<CPUncompleteDetail>();
            if (IsAdd)
            {
                _LstCPUncompleteDetail = GetCPUncompleteDetail();
                if (_LstCPUncompleteDetail.Count == 0)
                    _LstCPUncompleteDetail = new List<CPUncompleteDetail>();
            }
            else
            {
                _LstCPUncomplete = _select.CPUncompletes.ToList();
                if (_LstCPUncomplete.Count > 0)
                {
                    foreach (var item in _LstCPUncomplete)
                    {
                        item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                        List<CPUncompleteDetail> lst = item.CPUncompleteDetails.ToList();
                        _LstCPUncompleteDetail.AddRange(lst);
                    }
                }

                if (_LstCPUncompleteDetail.Count > 0)
                {
                    foreach (var item in _LstCPUncompleteDetail)
                    {
                        item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                        item.MaterialGoodCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                        item.MaterialGoodName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
                    }

                    if (optType.CheckedIndex != _LstCPUncompleteDetail.FirstOrDefault().UncompleteType)
                    {
                        checkupdate = 1;
                    }
                }
                _LstCPUncomplete = _LstCPUncomplete.OrderBy(x => x.CostsetCode).ToList();
            }

            _LstCPUncompleteDetail = _LstCPUncompleteDetail.OrderBy(x => x.MaterialGoodCode).ToList();

            uGridUncomplete.DataSource = new BindingList<CPUncomplete>(_LstCPUncomplete);
            Utils.ConfigGrid(uGridUncomplete, ConstDatabase.CPUncomplete_TableName);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["TotalCostAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);

            foreach (var col in uGridUncomplete.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "DirectMaterialAmount" || col.Key == "DirectLaborAmount" || col.Key == "GeneralExpensesAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }

            uGridUncompleteDetail.DataSource = new BindingList<CPUncompleteDetail>(_LstCPUncompleteDetail);
            Utils.ConfigGrid(uGridUncompleteDetail, ConstDatabase.CPUncompleteDetail_TableName);
            foreach (var column in uGridUncompleteDetail.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key == "PercentComplete")
                {
                    column.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else if (column.Key == "Quantity")
                {
                    column.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    column.CellActivation = Activation.NoEdit;
                }
            }
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Width = 150;
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].FormatNumberic(-1);

            if (optType.CheckedIndex == 0)
            {
                checkItem = 0;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            }
            else if (optType.CheckedIndex == 1)
            {
                checkItem = 1;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].Hidden = true;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            }
            else
            {
                checkItem = 2;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }
        private void ViewGridPanel6()
        {
            btnEscape.Text = "Ðóng";
            if (!IsAdd)
            {
                _LstCPResult = new List<CPResult>();
                if (checkupdate == 0)
                    _LstCPResult = _select.CPResults.OrderBy(x => x.CostSetCode).ToList();
                else
                    _LstCPResult = GetCPResults();
                if (_LstCPResult.Count > 0)
                {
                    foreach (var item in _LstCPResult)
                    {
                        item.CostSetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.MaterialGoodsCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                        item.MaterialGoodsName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
                    }
                }
            }
            else
            {
                _LstCPResult = GetCPResults();
                if (_LstCPResult.Count == 0)
                    _LstCPResult = new List<CPResult>();
            }

            uGridCosting.DataSource = new BindingList<CPResult>(_LstCPResult);
            Utils.ConfigGrid(uGridCosting, ConstDatabase.CPCostResult_TableName);
            uGridCosting.DisplayLayout.Bands[0].Columns["Coefficien"].Hidden = true;
            uGridCosting.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCosting, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCosting.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCosting, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCosting.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCosting, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCosting.DisplayLayout.Bands[0].Columns["TotalCostAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCosting, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCosting.DisplayLayout.Bands[0].Columns["TotalQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(uGridCosting, "TotalQuantity", false, "", ConstDatabase.Format_Quantity);
            uGridCosting.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGridCosting, "UnitPrice", false, "", ConstDatabase.Format_ForeignCurrency);
            uGridCosting.DisplayLayout.Bands[0].Columns["Coefficien"].FormatNumberic(-1); //rate2

            foreach (var col in uGridCosting.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "TotalQuantity")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }

        private List<CPAccountAllocationQuantum> GetCPAccountAllocationQuantum()
        {
            var listCostset = Utils.ListCostSet.ToList();
            var listContractSale = Utils.ListEmContractSA.ToList();
            var listAllocationQuantum = Utils.ListCPAllocationQuantum.ToList();
            var lst1 = (from a in listCostset
                        select new CPAccountAllocationQuantum()
                        {
                            AccountingObjectID = a.ID,
                            AccountingObjectCode = a.CostSetCode,
                            AccountingObjectName = a.CostSetName,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0
                        }).ToList();
            var lst2 = (from a in listContractSale
                        select new CPAccountAllocationQuantum()
                        {
                            AccountingObjectID = a.ID,
                            AccountingObjectCode = a.Code,
                            AccountingObjectName = a.Name,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0
                        }).ToList();
            List<CPAccountAllocationQuantum> lst = new List<CPAccountAllocationQuantum>();
            lst.AddRange(lst1);
            lst.AddRange(lst2);
            foreach (var x in lst)
            {
                var a = listAllocationQuantum.FirstOrDefault(d => d.ID == x.AccountingObjectID);
                if (a != null)
                {
                    x.DirectMaterialAmount = a.DirectMaterialAmount;
                    x.DirectLaborAmount = a.DirectLaborAmount;
                    x.GeneralExpensesAmount = a.GeneralExpensesAmount;
                    x.TotalCostAmount = a.TotalCostAmount;
                }
            }
            return lst;
        }

        private void GetCPUncomplete(List<CPOPN> lstOPN, List<CPExpenseList> lstExpense1,
            List<CPExpenseList> lstExpense2, List<CPExpenseList> lstExpense3, List<CPExpenseList> lstExpense4, List<CPExpenseList> lstExpense5, List<CPExpenseList> lstExpense6,
            List<CPAllocationGeneralExpenseDetail> lst1, List<CPAllocationGeneralExpenseDetail> lst2, List<CPAllocationGeneralExpenseDetail> lst3)
        {
            foreach (var item in uGridUncompleteDetail.DataSource as BindingList<CPUncompleteDetail>)
            {
                CPUncomplete cpu = new CPUncomplete();
                CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == item.CostSetID);
                Decimal directMatetialAmount = 0;
                Decimal directLaborAmount = 0;
                Decimal generalExpensesAmount = 0;
                List<CPPeriod> cPPeriod = ICPPeriodService.GetAll().Where(x => x.ToDate < toDate && x.Type == 0).OrderByDescending(x => x.ToDate).ToList();
                List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll().Where(x => x.CostSetID == item.CostSetID).ToList();

                if (cPPeriod.Count > 0)
                {
                    CPPeriod period1 = (from g in cPPeriod join h in cPPeriodDetails on g.ID equals h.CPPeriodID select g).FirstOrDefault();
                    if (period1 != null)
                    {
                        CPUncomplete uncomplete = period1.CPUncompletes.FirstOrDefault(x => x.CostSetID == item.CostSetID);
                        if (uncomplete != null)
                        {
                            directMatetialAmount = uncomplete.DirectMaterialAmount;
                            directLaborAmount = uncomplete.DirectLaborAmount;
                            generalExpensesAmount = uncomplete.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                    else
                    {
                        if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                        {
                            directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                            directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                            generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }

                Decimal amount = (from g in lstExpense1 where g.CostSetID == item.CostSetID select g.Amount).Sum();
                Decimal amount2 = (from g in lstExpense2 where g.CostSetID == item.CostSetID select g.Amount).Sum();
                Decimal amount3 = (from g in lstExpense3 where g.CostSetID == item.CostSetID select g.Amount).Sum();
                Decimal amount4 = (from g in lstExpense4 where g.CostSetID == item.CostSetID select g.Amount).Sum();
                Decimal amount5 = (from g in lstExpense5 where g.CostSetID == item.CostSetID select g.Amount).Sum();
                Decimal amount6 = (from g in lstExpense6 where g.CostSetID == item.CostSetID select g.Amount).Sum();
                Decimal allocatedAmount = (from g in lst1 where g.CostSetID == item.CostSetID select g.AllocatedAmount).Sum();
                Decimal allocatedAmount1 = (from g in lst2 where g.CostSetID == item.CostSetID select g.AllocatedAmount).Sum();
                Decimal allocatedAmount2 = (from g in lst3 where g.CostSetID == item.CostSetID select g.AllocatedAmount).Sum();
                Decimal quantity = item.Quantity;
                Decimal percentComplete = item.PercentComplete;
                Decimal totalQuantity = 0;

                //edit by cuongpv
                if (IsAdd)
                {
                    if (_LstRSInwardOutwardDetail != null)
                    {
                        totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item.CostSetID select (Decimal)g.Quantity).Sum();
                    }
                }
                else
                {
                    _LstRSInwardOutward = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
                    _LstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
                    _LstRSInwardOutwardDetail = (from g in _LstRSInwardOutward
                                                 join h in _LstRSInwardOutwardDetail on g.ID equals h.RSInwardOutwardID
                                                 where g.TypeID == 400 && listCostSetID.Any(x => x == h.CostSetID) && h.PostedDate >= fromDate && h.PostedDate <= toDate
                                                 select h).ToList();
                    totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item.CostSetID select (Decimal)g.Quantity).Sum();
                }
                //end edit cuongpv
                #region comment by cuongpv
                //if (_LstRSInwardOutwardDetail != null)
                //{
                //    if (IsAdd)
                //        totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item.CostSetID select (Decimal)g.Quantity).Sum();
                //    else
                //    {
                //        _LstRSInwardOutward = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
                //        _LstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
                //        _LstRSInwardOutwardDetail = (from g in _LstRSInwardOutward
                //                                     join h in _LstRSInwardOutwardDetail on g.ID equals h.RSInwardOutwardID
                //                                     where g.TypeID == 400 && listCostSetID.Any(x => x == h.CostSetID) && h.PostedDate >= fromDate && h.PostedDate <= toDate
                //                                     select h).ToList();
                //        totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item.CostSetID select (Decimal)g.Quantity).Sum();
                //    }
                //}
                #endregion

                cpu.ID = Guid.NewGuid();
                cpu.CostSetID = item.CostSetID;
                string costsetCode = Utils.ICostSetService.Getbykey(item.CostSetID).CostSetCode;
                string costsetName = Utils.ICostSetService.Getbykey(item.CostSetID).CostSetName;
                string materialgoodsCode = Utils.IMaterialGoodsService.Getbykey(item.MaterialGoodsID).MaterialGoodsCode;
                string materialgoodsName = Utils.IMaterialGoodsService.Getbykey(item.MaterialGoodsID).MaterialGoodsName;
                cpu.CostsetCode = costsetCode;
                cpu.CostsetName = costsetName;
                if (checkItem == 0)
                {
                    if ((totalQuantity + percentComplete * quantity / 100) != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round((directMatetialAmount + amount + allocatedAmount - amount2) * quantity * percentComplete / ((totalQuantity + quantity * percentComplete / 100) * 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = Math.Round((directLaborAmount + amount3 + allocatedAmount1 - amount4) * quantity * percentComplete / ((totalQuantity + quantity * percentComplete / 100) * 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu.GeneralExpensesAmount = Math.Round((generalExpensesAmount + amount5 + allocatedAmount2 - amount6) * quantity * percentComplete / ((totalQuantity + quantity * percentComplete / 100) * 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }
                else if (checkItem == 1)
                {
                    if ((totalQuantity + quantity) != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round((directMatetialAmount + amount + allocatedAmount - amount2) * quantity / (totalQuantity + quantity), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = 0;
                        cpu.GeneralExpensesAmount = 0;
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }
                else if (checkItem == 2)
                {
                    List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
                    Decimal directMatetialAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectMaterialAmount).FirstOrDefault();
                    Decimal directLaborAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectLaborAmount).FirstOrDefault();
                    Decimal generalExpensesAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.GeneralExpensesAmount).FirstOrDefault();
                    string materialgoodCode = Utils.IMaterialGoodsService.Getbykey(item.MaterialGoodsID).MaterialGoodsCode;
                    string materialgoodName = Utils.IMaterialGoodsService.Getbykey(item.MaterialGoodsID).MaterialGoodsName;
                    cpu.CostsetCode = costsetCode;
                    cpu.CostsetName = costsetName;
                    if (percentComplete != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round((directMatetialAmount1 * quantity * percentComplete / 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = Math.Round((directLaborAmount1 * quantity * percentComplete / 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu.GeneralExpensesAmount = Math.Round((generalExpensesAmount1 * quantity * percentComplete / 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }

                _LstCPUncomplete.Add(cpu);
            }

            if (_LstCPUncomplete != null)
                _LstCPUncomplete = _LstCPUncomplete.OrderBy(x => x.CostsetCode).ToList();
            uGridUncomplete.DataSource = new BindingList<CPUncomplete>(_LstCPUncomplete);
            Utils.ConfigGrid(uGridUncomplete, ConstDatabase.CPUncomplete_TableName);
        }

        private void GetLoopCPUncomplete(CPUncomplete cpu)
        {
            Utils.ICPUncompleteService.BeginTran();
            List<CPExpenseList> lstCPExpense = Utils.ListCPExpenseList.Where(x => x.CPPeriodID == cpu.CPPeriodID && x.TypeVoucher == 0 && x.CostSetID == cpu.CostSetID).ToList(); // NVL trực tiếp
            List<CPExpenseList> lstCPExpense2 = Utils.ListCPExpenseList.Where(x => x.CPPeriodID == cpu.CPPeriodID && x.TypeVoucher == 1 && x.CostSetID == cpu.CostSetID).ToList(); // Giảm giá thành
            List<CPAllocationGeneralExpense> lstAllocationGeneralExpense1 = Utils.ListCPAllocationGeneralExpense.Where(x => x.CPPeriodID == cpu.CPPeriodID).ToList(); // Phân bổ chung 
            List<CPAllocationGeneralExpenseDetail> lstAllocationGeneralExpenseDetail1 = new List<CPAllocationGeneralExpenseDetail>();
            foreach (var item in lstAllocationGeneralExpense1)
            {
                lstAllocationGeneralExpenseDetail1.AddRange(item.CPAllocationGeneralExpenseDetails);
            }
            lstAllocationGeneralExpenseDetail1 = lstAllocationGeneralExpenseDetail1.Where(x => x.CostSetID == cpu.CostSetID).ToList();
            List<CPExpenseList> lstCPExpense3 = new List<CPExpenseList>();
            List<CPExpenseList> lstCPExpense4 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> lstCPExpense5 = new List<CPExpenseList>();
            List<CPExpenseList> lstCPExpense6 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> lstCPExpense7 = new List<CPExpenseList>();
            List<CPExpenseList> lstCPExpense8 = new List<CPExpenseList>();
            lstCPExpense3 = (from g in lstCPExpense
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 0
                             select g).ToList(); //Amount 
            lstCPExpense4 = (from g in lstCPExpense2
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 0
                             select g).ToList(); //Amount 
            lstCPExpense5 = (from g in lstCPExpense
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 1
                             select g).ToList(); //Amount 
            lstCPExpense6 = (from g in lstCPExpense2
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 1
                             select g).ToList(); //Amount 
            lstCPExpense7 = (from g in lstCPExpense
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 2
                             select g).ToList(); //Amount 
            lstCPExpense8 = (from g in lstCPExpense2
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 2
                             select g).ToList(); //Amount
            lst2 = (from g in lstAllocationGeneralExpenseDetail1
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 0
                    select g).ToList(); //AllocatedAmount   
            lst3 = (from g in lstAllocationGeneralExpenseDetail1
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 1
                    select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst4 = (from g in lstAllocationGeneralExpenseDetail1
                                                           join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                                           where h.ExpenseType == 2
                                                           select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
            var lst5 = (from a in lst4
                        group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            CostsetCode = t.Key.CostsetCode,
                            CostsetName = t.Key.CostsetName,
                            Amount = t.Sum(x => x.AllocatedAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                lst6.Add(cPAllocationGeneralExpenseDetail);
            }
            List<CPPeriod> cPPeriod = ICPPeriodService.GetAll().Where(x => x.Type == 0).ToList();
            CPPeriod period = cPPeriod.FirstOrDefault(x => x.ID == cpu.CPPeriodID);
            //List<CPUncomplete> cPUncomplete = ICPUncompleteService.GetAll().Where(x => x.CostSetID == cpu.CostSetID).ToList();
            CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == cpu.CostSetID);
            List<CPPeriod> cPPeriod1 = cPPeriod.Where(x => x.ToDate < period.FromDate).OrderByDescending(x => x.ToDate).ToList();
            List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll().Where(x => x.CostSetID == cpu.CostSetID).ToList();

            List<RSInwardOutward> LstRSInwardOutward1 = Utils.ListRSInwardOutward.ToList();
            List<RSInwardOutwardDetail> LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
            Decimal totalQuantity = 0;
            LstRSInwardOutwardDetail1 = (from g in LstRSInwardOutward1
                                         join h in LstRSInwardOutwardDetail1 on g.ID equals h.RSInwardOutwardID
                                         where g.TypeID == 400 && listCostSetID.Any(x => x == h.CostSetID) && h.PostedDate >= period.FromDate && h.PostedDate <= period.ToDate
                                         select h).ToList();

            Decimal directMatetialAmount = 0;
            Decimal directLaborAmount = 0;
            Decimal generalExpensesAmount = 0;
            Decimal convertquantity = 0;
            Decimal quantity = 0;

            List<CPUncompleteDetail> lstUncompleteDetail = new List<CPUncompleteDetail>();
            if (cpu != null)
            {
                lstUncompleteDetail = cpu.CPUncompleteDetails.ToList();
                foreach (var item1 in lstUncompleteDetail)
                {
                    Decimal quantity1 = item1.Quantity;
                    Decimal percentComplete1 = item1.PercentComplete;
                    convertquantity += quantity1 * percentComplete1 / 100;
                    quantity += quantity1;
                }
            }

            Decimal amount = (from g in lstCPExpense3 select g.Amount).Sum();
            Decimal amount2 = (from g in lstCPExpense4 select g.Amount).Sum();
            Decimal amount3 = (from g in lstCPExpense5 select g.Amount).Sum();
            Decimal amount4 = (from g in lstCPExpense6 select g.Amount).Sum();
            Decimal amount5 = (from g in lstCPExpense7 select g.Amount).Sum();
            Decimal amount6 = (from g in lstCPExpense8 select g.Amount).Sum();
            Decimal allocatedAmount = (from g in lst2 select g.AllocatedAmount).Sum();
            Decimal allocatedAmount1 = (from g in lst3 select g.AllocatedAmount).Sum();
            Decimal allocatedAmount2 = (from g in lst6 select g.AllocatedAmount).Sum();

            if (cPPeriod.Count > 0)
            {
                CPPeriod period1 = (from g in cPPeriod1 join h in cPPeriodDetails on g.ID equals h.CPPeriodID select g).FirstOrDefault();
                if (period1 != null)
                {
                    CPUncomplete uncomplete1 = period1.CPUncompletes.FirstOrDefault(x => x.CostSetID == cpu.CostSetID);
                    if (uncomplete1 != null)
                    {
                        directMatetialAmount = uncomplete1.DirectMaterialAmount;
                        directLaborAmount = uncomplete1.DirectLaborAmount;
                        generalExpensesAmount = uncomplete1.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }
            }
            else
            {
                if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                {
                    directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                    directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                    generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                }
                else
                {
                    directMatetialAmount = 0;
                    directLaborAmount = 0;
                    generalExpensesAmount = 0;
                }
            }

            int uncompletetype = lstUncompleteDetail.FirstOrDefault().UncompleteType;
            totalQuantity = (from g in LstRSInwardOutwardDetail1 where g.CostSetID == cpu.CostSetID select (Decimal)g.Quantity).Sum();
            if (uncompletetype == 0)
            {
                if ((totalQuantity + convertquantity) != 0)
                {
                    cpu.DirectMaterialAmount = (directMatetialAmount + amount + allocatedAmount - amount2) * convertquantity / (totalQuantity + convertquantity);
                    cpu.DirectLaborAmount = (directLaborAmount + amount3 + allocatedAmount1 - amount4) * convertquantity / (totalQuantity + convertquantity);
                    cpu.GeneralExpensesAmount = (generalExpensesAmount + amount5 + allocatedAmount2 - amount6) * convertquantity / (totalQuantity + convertquantity);
                    cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                }
            }
            else if (uncompletetype == 1)
            {
                if ((totalQuantity + quantity) != 0)
                {
                    cpu.DirectMaterialAmount = (directMatetialAmount + amount + allocatedAmount - amount2) * quantity / (totalQuantity + quantity);
                    cpu.DirectLaborAmount = 0;
                    cpu.GeneralExpensesAmount = 0;
                    cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                }
            }
            else if (checkItem == 2)
            {
                List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
                List<CPUncompleteDetail> cpUncompleteDetails = cpu.CPUncompleteDetails.ToList();
                List<CPUncomplete> lstUncomplete = new List<CPUncomplete>();
                foreach (var item in cpUncompleteDetails)
                {
                    Decimal directMatetialAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectMaterialAmount).FirstOrDefault();
                    Decimal directLaborAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectLaborAmount).FirstOrDefault();
                    Decimal generalExpensesAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.GeneralExpensesAmount).FirstOrDefault();
                    Decimal quantity1 = item.Quantity;
                    Decimal percentcomplete = item.PercentComplete;
                    CPUncomplete cpu1 = new CPUncomplete();
                    cpu1.CostSetID = item.CostSetID;
                    cpu1.CostsetCode = item.CostsetCode;
                    cpu1.CostsetName = item.CostsetName;
                    cpu1.DirectMaterialAmount = directMatetialAmount1 * quantity * percentcomplete / 100;
                    cpu1.DirectLaborAmount = directLaborAmount1 * quantity * percentcomplete / 100;
                    cpu1.GeneralExpensesAmount = generalExpensesAmount1 * quantity * percentcomplete / 100;
                    cpu1.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    lstUncomplete.Add(cpu1);
                }

                var lst = (from g in lstUncomplete
                           group g by new { g.CostSetID, g.CostsetCode, g.CostsetName } into t
                           select new
                           {
                               CostSetID = t.Key.CostSetID,
                               CostsetCode = t.Key.CostsetCode,
                               CostsetName = t.Key.CostsetName,
                               DirectMaterialAmount = t.Sum(x => x.DirectMaterialAmount),
                               DirectLaborAmount = t.Sum(x => x.DirectLaborAmount),
                               GeneralExpensesAmount = t.Sum(x => x.GeneralExpensesAmount),
                               TotalCostAmount = t.Sum(x => x.TotalCostAmount)
                           }).ToList();

                foreach (var item1 in lst)
                {
                    cpu.DirectMaterialAmount = item1.DirectMaterialAmount;
                    cpu.DirectLaborAmount = item1.DirectLaborAmount;
                    cpu.GeneralExpensesAmount = item1.GeneralExpensesAmount;
                    cpu.TotalCostAmount = item1.TotalCostAmount;
                }
            }

            List<CPUncompleteDetail> cPUncompleteDetails = cpu.CPUncompleteDetails.ToList();
            foreach (var item in cPUncompleteDetails)
            {
                CPResult cpr = ICPResultService.GetAll().FirstOrDefault(x => x.CPPeriodID == cpu.CPPeriodID && x.MaterialGoodsID == item.MaterialGoodsID);
                if (cpr != null)
                {
                    cpr.DirectMaterialAmount = directMatetialAmount + amount + allocatedAmount - amount2 - cpu.DirectMaterialAmount;
                    cpr.DirectLaborAmount = directLaborAmount + amount3 + allocatedAmount1 - amount4 - cpu.DirectLaborAmount;
                    cpr.GeneralExpensesAmount = generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - cpu.GeneralExpensesAmount;
                    cpr.TotalCostAmount = cpr.DirectMaterialAmount + cpr.DirectLaborAmount + cpr.GeneralExpensesAmount;
                    if (cpr.TotalQuantity != 0)
                        cpr.UnitPrice = cpr.TotalCostAmount / cpr.TotalQuantity;
                    else
                        cpr.UnitPrice = 0;
                    Utils.ICPResultService.Update(cpr);
                }
            }

            Utils.ICPUncompleteService.Update(cpu);
            Utils.ICPUncompleteService.CommitTran();
        }

        private List<CPResult> GetCPResults()
        {
            _LstCPResult = new List<CPResult>();
            _LstExpenseItem = Utils.ListExpenseItem.ToList();
            _LstCPOPN = Utils.ListCPOPN.ToList();
            List<CPOPN> _LstCPOPN2 = new List<CPOPN>();
            List<CPExpenseList> _LstCPExpense3 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense4 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense5 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense6 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense7 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense8 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst4 = new List<CPAllocationGeneralExpenseDetail>();
            _LstCPOPN2 = (from g in _LstCPOPN where listCostSetID.Any(x => x == g.CostSetID) select g).ToList(); //DirectMatetialAmount
            _LstCPExpense3 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            _LstCPExpense4 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            lst2 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 0
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense5 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            _LstCPExpense6 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            lst3 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 1
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense7 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            _LstCPExpense8 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            lst4 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 2
                    select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
            var lst5 = (from a in lst4
                        group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            CostsetCode = t.Key.CostsetCode,
                            CostsetName = t.Key.CostsetName,
                            Amount = t.Sum(x => x.AllocatedAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                lst6.Add(cPAllocationGeneralExpenseDetail);
            }

            CPUncomplete cpu = new CPUncomplete();
            Decimal directMatetialAmount = 0;
            Decimal directLaborAmount = 0;
            Decimal generalExpensesAmount = 0;
            List<CPPeriod> cPPeriod = ICPPeriodService.GetAll();
            List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll();

            foreach (var item in listCostSetID)
            {
                List<CPUncomplete> cPUncomplete = ICPUncompleteService.Query.Where(x => x.CostSetID == item).ToList();
                CPUncompleteDetail cPUncompleteDetail = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).FirstOrDefault(x => x.CostSetID == item);
                CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == item);
                Decimal totalQuantity = 0;
                if (_LstRSInwardOutwardDetail != null)
                    totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item select (Decimal)g.Quantity).Sum();
                CPResult cpr = new CPResult();
                cpr.ID = Guid.NewGuid();
                cpr.CostSetID = item;
                cpr.CostSetCode = Utils.ICostSetService.Getbykey(item).CostSetCode;
                cpr.CostSetName = Utils.ICostSetService.Getbykey(item).CostSetName;
                if (cPUncompleteDetail != null)
                {
                    cpr.MaterialGoodsID = cPUncompleteDetail.MaterialGoodsID;
                    cpr.MaterialGoodsCode = cPUncompleteDetail.MaterialGoodCode;
                    cpr.MaterialGoodsName = cPUncompleteDetail.MaterialGoodName;
                }
                cpr.TotalQuantity = totalQuantity;

                if (cPPeriod.Count > 0)
                {
                    CPPeriod period1 = (from g in cPPeriod
                                        join h in cPPeriodDetails on g.ID equals h.CPPeriodID
                                        where g.ToDate < fromDate
                                        && h.CostSetID == item
                                        select g).OrderByDescending(x => x.ToDate).FirstOrDefault();
                    if (period1 != null)
                    {
                        CPUncomplete uncomplete = period1.CPUncompletes.FirstOrDefault(x => x.CostSetID == item);
                        if (uncomplete != null)
                        {
                            directMatetialAmount = uncomplete.DirectMaterialAmount;
                            directLaborAmount = uncomplete.DirectLaborAmount;
                            generalExpensesAmount = uncomplete.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                    else
                    {
                        if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                        {
                            directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                            directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                            generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }

                Decimal amount = (from g in _LstCPExpense3 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount2 = (from g in _LstCPExpense4 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount3 = (from g in _LstCPExpense5 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount4 = (from g in _LstCPExpense6 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount5 = (from g in _LstCPExpense7 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount6 = (from g in _LstCPExpense8 where g.CostSetID == item select g.Amount).Sum();
                Decimal allocatedAmount = (from g in lst2 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal allocatedAmount1 = (from g in lst3 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal allocatedAmount2 = (from g in lst6 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal directMatetialAmount1 = (from g in _LstCPUncomplete where g.CostSetID == item select g.DirectMaterialAmount).Sum();
                Decimal directLaborAmount1 = (from g in _LstCPUncomplete where g.CostSetID == item select g.DirectLaborAmount).Sum();
                Decimal generalExpensesAmount1 = (from g in _LstCPUncomplete where g.CostSetID == item select g.GeneralExpensesAmount).Sum();

                cpr.DirectMaterialAmount = Math.Round((directMatetialAmount + amount + allocatedAmount - amount2 - directMatetialAmount1), lamtron, MidpointRounding.AwayFromZero);
                cpr.DirectLaborAmount = Math.Round((directLaborAmount + amount3 + allocatedAmount1 - amount4 - directLaborAmount1), lamtron, MidpointRounding.AwayFromZero);
                cpr.GeneralExpensesAmount = Math.Round((generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - generalExpensesAmount1), lamtron, MidpointRounding.AwayFromZero);
                cpr.TotalCostAmount = cpr.DirectMaterialAmount + cpr.DirectLaborAmount + cpr.GeneralExpensesAmount;
                if (cpr.TotalQuantity != 0)
                {
                    cpr.UnitPrice = cpr.TotalCostAmount / cpr.TotalQuantity;
                }
                _LstCPResult.Add(cpr);
            }

            if (_LstCPResult != null)
                _LstCPResult = _LstCPResult.OrderBy(x => x.MaterialGoodsCode).ToList();
            else
                _LstCPResult = new List<CPResult>();
            return _LstCPResult;
        }

        private List<CPUncompleteDetail> GetCPUncompleteDetail()
        {
            List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
            List<MaterialGoods> materialGoods = Utils.ListMaterialGoods.ToList();
            _LstCPUncompleteDetail = new List<CPUncompleteDetail>();
            _LstRSInwardOutward = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
            _LstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
            _LstRSInwardOutwardDetail = (from g in _LstRSInwardOutward
                                         join h in _LstRSInwardOutwardDetail on g.ID equals h.RSInwardOutwardID
                                         where g.TypeID == 400 && listCostSetID.Any(x => x == h.CostSetID) && h.PostedDate >= fromDate && h.PostedDate <= toDate
                                         select h).ToList();
            var lst5 = (from a in _LstRSInwardOutwardDetail
                        group a by new { a.MaterialGoodsID, a.CostSetID } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            MaterialGoodsID = t.Key.MaterialGoodsID,
                            Quantity = t.Sum(x => x.Quantity)
                        }).ToList();
            foreach (var item in listCostSetID)
            {
                string costSetCode = Utils.ICostSetService.Getbykey(item).CostSetCode;
                string costSetName = Utils.ICostSetService.Getbykey(item).CostSetName;
                if (lst5.Count > 0)
                {
                    _LstCPUncompleteDetail = lst5.Where(x => listCostSetID.Contains(x.CostSetID ?? Guid.Empty))
                    .Select(g => new CPUncompleteDetail()
                    {
                        ID = Guid.NewGuid(),
                        CostSetID = (Guid)g.CostSetID,
                        CostsetCode = Utils.ICostSetService.Getbykey(g.CostSetID ?? Guid.Empty).CostSetCode,
                        MaterialGoodsID = (Guid)g.MaterialGoodsID,
                        MaterialGoodCode = Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsCode,
                        MaterialGoodName = Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsName,
                        UnitPrice = (from h in lstCPMaterialProductQuantum where h.MaterialGoodsCode == Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsCode select (Decimal)h.TotalCostAmount).FirstOrDefault()
                    }).ToList();
                }
                else
                {
                    CPUncompleteDetail cPUncompleteDetail = new CPUncompleteDetail();
                    cPUncompleteDetail.ID = Guid.NewGuid();
                    cPUncompleteDetail.CostSetID = item;
                    cPUncompleteDetail.CostsetCode = costSetCode;
                    cPUncompleteDetail.MaterialGoodCode = costSetCode;
                    cPUncompleteDetail.MaterialGoodName = costSetName;
                    cPUncompleteDetail.MaterialGoodsID = (from g in materialGoods where g.MaterialGoodsCode == costSetCode select g.ID).FirstOrDefault();
                    cPUncompleteDetail.UnitPrice = (from h in lstCPMaterialProductQuantum where h.MaterialGoodsCode == costSetCode select (Decimal)h.TotalCostAmount).FirstOrDefault();
                    _LstCPUncompleteDetail.Add(cPUncompleteDetail);
                }
            }

            if (_LstCPUncompleteDetail != null)
                _LstCPUncompleteDetail = _LstCPUncompleteDetail.OrderBy(x => x.MaterialGoodCode).ToList();
            return _LstCPUncompleteDetail;
        }

        private bool CheckCostSet(DateTime fromdate, DateTime todate, Guid costsetID)
        {
            List<CPPeriod> lstCPPeriod = Utils.ListCPPeriod.Where(x => !(x.FromDate > todate || x.ToDate < fromdate) && x.Type == 0).ToList();
            List<CPPeriodDetail> lst = new List<CPPeriodDetail>();
            foreach (var item in lstCPPeriod)
            {
                List<CPPeriodDetail> lstCPPeriodDetail = item.CPPeriodDetails.Where(x => x.CostSetID == costsetID).ToList();
                lst.AddRange(lstCPPeriodDetail);
            }
            if (lst.Count > 0)
                return false;
            else
                return true;
        }


        #endregion #region hàm xử lý   

        private void uGridUncompleteDetail_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "PercentComplete" || activeCell.Column.Key == "Quantity")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }

        private void uGridUncomplete_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "DirectMaterialAmount" || activeCell.Column.Key == "DirectLaborAmount" || activeCell.Column.Key == "GeneralExpensesAmount")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }

        private void uGridUncomplete_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("DirectMaterialAmount") || e.Cell.Column.Key.Equals("DirectLaborAmount") || e.Cell.Column.Key.Equals("GeneralExpensesAmount") || e.Cell.Column.Key.Equals("AcceptedAmount"))
            {
                UltraGrid ultraGrid = (UltraGrid)sender;
                UltraGridRow row = ultraGrid.ActiveRow;
                if (row.Cells["DirectMaterialAmount"].Text.Trim('_') == "") row.Cells["DirectMaterialAmount"].Value = 0;
                if (row.Cells["DirectLaborAmount"].Text.Trim('_') == "") row.Cells["DirectLaborAmount"].Value = 0;
                if (row.Cells["GeneralExpensesAmount"].Text.Trim('_') == "") row.Cells["GeneralExpensesAmount"].Value = 0;
                ultraGrid.UpdateData();
                if (row != null)
                {
                    row.Cells["TotalCostAmount"].Value = ((decimal)row.Cells["DirectLaborAmount"].Value + (decimal)row.Cells["DirectMaterialAmount"].Value + (decimal)row.Cells["GeneralExpensesAmount"].Value);
                }
            }
        }

        private void uGridUncompleteDetail_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("PercentComplete"))
            {
                try
                {
                    if (Decimal.Parse(e.Cell.Row.Cells["PercentComplete"].Text.Trim('_').ToString()) <= 100)
                    {
                        uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].FormatNumberic(-1);
                    }
                    else
                    {
                        MSG.Warning("% hoàn thành không được quá 100%");
                        return;
                    }
                }
                catch { }
            }
        }

        private void uGridAllocationGeneralExpenseResult_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkupdate = 1;
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) <= 100)
                {
                    var row = uGridAllocationGeneralExpenseResult.ActiveRow;
                    if (row != null)
                    {
                        decimal amount = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList().FirstOrDefault(x => x.ID == (Guid)row.Cells["CPAllocationGeneralExpenseID"].Value).AllocatedAmount;
                        e.Cell.Row.Cells["AllocatedAmount"].Value = (amount * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString())) / 100;
                    }
                }
                else
                {

                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    var row = uGridAllocationGeneralExpenseResult.ActiveRow;
                    if (row != null)
                    {
                        decimal amount = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList().FirstOrDefault(x => x.ID == (Guid)row.Cells["CPAllocationGeneralExpenseID"].Value).AllocatedAmount;
                        e.Cell.Row.Cells["AllocatedRate"].Value = (Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / amount) * 100;
                    }
                }
            }
        }

        private void uGridUncompleteDetail_AfterCellUpdate(object sender, CellEventArgs e)
        {
            //checkDtr = 0;
            //checkupdate = 1;
            //List<CPUncompleteDetail> cPUncompleteDetails = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(x => x.Quantity > 0 || x.PercentComplete > 0).ToList();
            //if (cPUncompleteDetails.Count == 0)
            //    checkDtr = 1;
            if (!IsAdd)
                checkupdate = 1;

            btnDetermine_Click(sender, e);
        }

        private void uGridUncomplete_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkupdate = 1;
        }

        private void FCPSimpleMethodDetail_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            if (save == 1)
                isClose = true;
            else
            {
                if (lstAllocationGeneralExpense != null)
                    ICPAllocationGeneralExpenseService.UnbindSession(lstAllocationGeneralExpense);
                if (lstAllocationGeneralExpenseDetail != null)
                    ICPAllocationGeneralExpenseDetailService.UnbindSession(lstAllocationGeneralExpenseDetail);
                if (_LstCPUncomplete != null)
                    ICPUncompleteService.UnbindSession(_LstCPUncomplete);
                if (_LstCPUncompleteDetail != null)
                    ICPUncompleteDetailService.UnbindSession(_LstCPUncompleteDetail);
                if (_LstCPResult != null)
                    ICPResultService.UnbindSession(_LstCPResult);
                isClose = false;
            }
        }

        private void FCPSimpleMethodDetail_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void uGridCosting_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("TotalQuantity"))
            {
                var TotalCostAmount = Convert.ToDecimal(e.Cell.Row.Cells["TotalCostAmount"].Value.ToString());
                var TotalQuantity = Decimal.Parse(e.Cell.Row.Cells["TotalQuantity"].Value.ToString());
                if (TotalQuantity > 0)
                    e.Cell.Row.Cells["UnitPrice"].Value = TotalCostAmount / TotalQuantity;
                else
                    e.Cell.Row.Cells["UnitPrice"].Value = 0;

                btnSave.Enabled = true;
            }
        }
    }
}
