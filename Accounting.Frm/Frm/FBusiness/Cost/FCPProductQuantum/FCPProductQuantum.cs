﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FCPProductQuantum : DialogForm //UserControl
    {
        #region khai báo
        private readonly ICPProductQuantumService _ICPProductQuantumService;
        CPMaterialProductQuantum _Select = new CPMaterialProductQuantum();
        List<CPMaterialProductQuantum> _backList = new List<CPMaterialProductQuantum>();
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FCPProductQuantum()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _ICPProductQuantumService = IoC.Resolve<ICPProductQuantumService>();
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            #region lay du lieu tu db do ra list
            List<CPMaterialProductQuantum> lst = _ICPProductQuantumService.GetAllByType().OrderBy(x=>x.MaterialGoodsCode).ToList();
            BindingList<CPMaterialProductQuantum> bdlCPProductQuantumDetail = new BindingList<CPMaterialProductQuantum>(lst);
            #endregion

            #region load
            uGrid.DataSource = bdlCPProductQuantumDetail;
            //hien nhung dong trong
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            CreaterColumsStyle(uGrid);            
            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        #endregion

        #region nghiệp vụ
        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            var lstbd = (BindingList<CPMaterialProductQuantum>)uGrid.DataSource;
            var lst = lstbd.ToList();
            _ICPProductQuantumService.BeginTran();
            foreach (var x in lst)
            {
                if (!(Utils.ListCPProductQuantum.Any(c => c.ID == x.MaterialGoodsID)))
                {
                    CPProductQuantum model = new CPProductQuantum();
                    model.ID = x.MaterialGoodsID;
                    model.DirectMaterialAmount = x.DirectMaterialAmount??0;
                    model.DirectLaborAmount = x.DirectLaborAmount??0;
                    model.GeneralExpensesAmount = x.GeneralExpensesAmount ?? 0;
                    model.TotalCostAmount = x.TotalCostAmount ?? 0;
                    _ICPProductQuantumService.CreateNew(model);
                }
                else
                {
                    CPProductQuantum model = Utils.ListCPProductQuantum.FirstOrDefault(c => c.ID == x.MaterialGoodsID);
                    model.DirectLaborAmount = x.DirectLaborAmount ?? 0;
                    model.DirectMaterialAmount = x.DirectMaterialAmount ?? 0;
                    model.GeneralExpensesAmount = x.GeneralExpensesAmount ?? 0;
                    model.TotalCostAmount = x.TotalCostAmount ?? 0;
                    _ICPProductQuantumService.Update(model);
                }
            }
            try
            {
                _ICPProductQuantumService.CommitTran();
                Utils.ClearCacheByType<CPProductQuantum>();
                MSG.Information("Lưu dữ liệu thành công!");
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                _ICPProductQuantumService.RolbackTran();
            }

        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.CPMaterialProductQuantumDetail_TableName);
            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            //this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            //this.uGrid.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            //this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //this.uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            //this.uGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            //this.uGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            //this.uGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            //this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            //this.uGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            //this.uGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            //this.uGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            //this.uGrid.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            //this.uGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            uGrid.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].CellClickAction = CellClickAction.EditAndSelectText;
        }
        #endregion

        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            #region grid dong
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("DirectMaterialAmount") || item.Key.Equals("DirectLaborAmount") || item.Key.Equals("GeneralExpensesAmount") || item.Key.Equals("TotalCostAmount"))
                {
                    item.FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
            #endregion
        }
        #region sự kiện khi thay đổi dữ liệu trên Grid con
        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("DirectMaterialAmount") || e.Cell.Column.Key.Equals("DirectLaborAmount") || e.Cell.Column.Key.Equals("GeneralExpensesAmount"))
            {
                UltraGrid ultraGrid = (UltraGrid)sender;
                ultraGrid.UpdateData();
                UltraGridRow row = ultraGrid.ActiveRow;
                if (/*!e.Cell.Row.Cells["DirectLaborAmount"].IsFilterRowCell && !e.Cell.Row.Cells["DirectMatetialAmount"].IsFilterRowCell && !e.Cell.Row.Cells["GeneralExpensesAmount"].IsFilterRowCell*/row != null)
                {
                    row.Cells["TotalCostAmount"].Value = ((decimal)row.Cells["DirectLaborAmount"].Value + (decimal)row.Cells["DirectMaterialAmount"].Value + (decimal)row.Cells["GeneralExpensesAmount"].Value);
                }
            }
        }
        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
            var cell = uGrid.ActiveCell;
            if (cell != null)
            {
                if ((cell.Column.Key.Equals("DirectMaterialAmount") || cell.Column.Key.Equals("DirectLaborAmount") || cell.Column.Key.Equals("GeneralExpensesAmount")) && (cell.Value == null || cell.Text == "")) cell.Value = (decimal)0;
            }
        }
        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
        }
        #endregion

        private void FCPProductQuantum_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
