﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Facilities.TypedFactory.Internal;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FCPCostOfContract : CatalogBase
    {
        private IGOtherVoucherService _IGOtherVoucherService
        {
            get { return IoC.Resolve<IGOtherVoucherService>(); }
        }
        private ICPPeriodService _ICPPeriodService
        {
            get { return IoC.Resolve<ICPPeriodService>(); }
        }
        public FCPCostOfContract()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            LoadCogfig();
            WaitingFrm.StopWaiting();
        }

        void LoadCogfig()
        {
            List<CostPeriod> lst = new List<CostPeriod>();
            lst = _ICPPeriodService.GetAllCostPeriod(5);
            uGridDS.DataSource = new BindingList<CostPeriod>(lst);
            Utils.ConfigGrid(uGridDS, ConstDatabase.CostPeriod_TableName);
            uGridDS.DisplayLayout.Bands[0].Columns["AcceptedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "AcceptedAmount", false);
            uGridDS.DisplayLayout.Bands[0].Columns["AccumulatedAllocateAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "AccumulatedAllocateAmount", false);
            uGridDS.DisplayLayout.Bands[0].Columns["ExpensesDuringPeriod"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "ExpensesDuringPeriod", false);
            uGridDS.DisplayLayout.Bands[0].Columns["UnAcceptedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS, "UnAcceptedAmount", false);
            if (uGridDS.Rows.Count > 0)
            {
                uGridDS.Rows[0].Selected = true;
                UltraGridRow rowSelect = uGridDS.Rows[0];
                CostPeriod model = (CostPeriod)rowSelect.ListObject;
                CPPeriod select = Utils.ICPPeriodService.GetAll().FirstOrDefault(x => x.ID == model.ID);
                foreach (var x in select.CPExpenseLists)
                {
                    x.ContractCode = Utils.ListEmContract.Any(c => c.ID == x.ContractID) ? Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).Code : "";
                    x.ExpenseItemCode = Utils.ListExpenseItem.Any(c => c.ID == x.ExpenseItemID) ? Utils.ListExpenseItem.FirstOrDefault(c => c.ID == x.ExpenseItemID).ExpenseItemCode : "";
                }
                uGridCPExpenseList1.DataSource = new BindingList<CPExpenseList>(select.CPExpenseLists.Where(x => x.TypeVoucher == 0).ToList());
                Utils.ConfigGrid(uGridCPExpenseList1, ConstDatabase.CPExpenseList_TableName);
                uGridCPExpenseList1.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGridCPExpenseList1, "Amount", false);
                var grdband = uGridCPExpenseList1.DisplayLayout.Bands[0];
                grdband.Columns["ExpenseItemCode"].Hidden = true;
                grdband.Columns["CostSetCode"].Hidden = true;
                grdband.Columns["ContractCode"].Hidden = false;
                grdband.Columns["ContractCode"].Header.VisiblePosition = 1;
                uGridCPExpenseList1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                uGridCPExpenseList2.DataSource = new BindingList<CPExpenseList>(select.CPExpenseLists.Where(x => x.TypeVoucher == 1).ToList());
                Utils.ConfigGrid(uGridCPExpenseList2, ConstDatabase.CPExpenseList_TableName);
                uGridCPExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGridCPExpenseList2, "Amount", false);
                var grdband1 = uGridCPExpenseList2.DisplayLayout.Bands[0];
                grdband1.Columns["ExpenseItemCode"].Hidden = true;
                grdband1.Columns["CostSetCode"].Hidden = true;
                grdband1.Columns["ContractCode"].Hidden = false;
                grdband1.Columns["ContractCode"].Header.VisiblePosition = 1;
                uGridCPExpenseList2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                List<CPAllocationGeneralExpenseDetail> listpb = new List<CPAllocationGeneralExpenseDetail>();
                foreach (var a in select.CPAllocationGeneralExpenses)
                {
                    List<CPAllocationGeneralExpenseDetail> lstpbdetail = a.CPAllocationGeneralExpenseDetails.Where(x => x.ContractID != null && x.ContractID != Guid.Empty && select.CPPeriodDetails.Any(d=>d.ContractID == x.ContractID)).ToList().Where(x => x.AllocatedRate > 0).ToList();
                    foreach (var b in lstpbdetail)
                    {
                        b.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(c => c.ID == b.ExpenseItemID).ExpenseItemCode;
                        b.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == b.ContractID).Code;
                        b.SignedDate = Utils.ListEmContract.FirstOrDefault(c => c.ID == b.ContractID).SignedDate;
                        b.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(c => c.ID == b.ContractID).AccountingObjectName;
                    }
                    listpb.AddRange(lstpbdetail);
                }
                uGridCPAllocationGeneralExpenseDetail.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(listpb);
                Utils.ConfigGrid(uGridCPAllocationGeneralExpenseDetail, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(ConstDatabase.Format_Rate2);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGridCPAllocationGeneralExpenseDetail, "AllocatedAmount", false);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = true;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["ContractCode"].Hidden = false;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["ContractCode"].Header.VisiblePosition = 1;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = false;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["SignedDate"].Header.VisiblePosition = 2;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = false;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.VisiblePosition = 3;
            }
            else
            {

                uGridCPExpenseList1.DataSource = new BindingList<CPExpenseList>(new List<CPExpenseList>());
                Utils.ConfigGrid(uGridCPExpenseList1, ConstDatabase.CPExpenseList_TableName);
                uGridCPExpenseList1.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGridCPExpenseList1, "Amount", false);
                var grdband = uGridCPExpenseList1.DisplayLayout.Bands[0];
                grdband.Columns["ExpenseItemCode"].Hidden = true;
                grdband.Columns["CostSetCode"].Hidden = true;
                grdband.Columns["ContractCode"].Hidden = false;
                grdband.Columns["ContractCode"].Header.VisiblePosition = 1;

                uGridCPExpenseList2.DataSource = new BindingList<CPExpenseList>(new List<CPExpenseList>());
                Utils.ConfigGrid(uGridCPExpenseList2, ConstDatabase.CPExpenseList_TableName);
                uGridCPExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGridCPExpenseList2, "Amount", false);
                var grdband1 = uGridCPExpenseList2.DisplayLayout.Bands[0];
                grdband1.Columns["ExpenseItemCode"].Hidden = true;
                grdband1.Columns["CostSetCode"].Hidden = true;
                grdband1.Columns["ContractCode"].Hidden = false;
                grdband1.Columns["ContractCode"].Header.VisiblePosition = 1;
                uGridCPExpenseList2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                uGridCPAllocationGeneralExpenseDetail.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(new List<CPAllocationGeneralExpenseDetail>());
                Utils.ConfigGrid(uGridCPAllocationGeneralExpenseDetail, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(ConstDatabase.Format_Rate2);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                Utils.AddSumColumn(uGridCPAllocationGeneralExpenseDetail, "AllocatedAmount", false);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = true;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["ContractCode"].Hidden = false;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["ContractCode"].Header.VisiblePosition = 1;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = false;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["SignedDate"].Header.VisiblePosition = 2;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = false;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.VisiblePosition = 3;
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
        }
        #region Event
        protected override void AddFunction()
        {
        }
        #region button
        private void btnAddContract_Click(object sender, EventArgs e)
        {
            FCPCostOfContractDetail frm = new FCPCostOfContractDetail();
            frm.StartPosition = FormStartPosition.CenterScreen;
            //frm.ShowDialog();
            //if (!FCPCostOfContractDetail.isClose)
            //{
            //    Utils.ClearCacheByType<CPPeriod>();
            //    LoadCogfig();
            //}
            //HUYPD Edit Show Form
            frm.FormClosed += new FormClosedEventHandler(FCPCostOfContractDetail_FormClosed);
            frm.ShowDialogHD(this);

        }
        private void FCPCostOfContractDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ClearCacheByType<CPPeriod>();
            LoadCogfig();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        protected override void DeleteFunction()
        {
            List<CPPeriod> lstCPPeriod = Utils.ICPPeriodService.GetAll();
            if (uGridDS.Selected.Rows.Count > 0)
            {
                CostPeriod temp = uGridDS.Selected.Rows[0].ListObject as CostPeriod;
                var model = Utils.ListCPPeriod.FirstOrDefault(x => x.ID == temp.ID);
                List<CPPeriod> cPPeriods = lstCPPeriod.Where(x => x.FromDate > model.ToDate).ToList();
                if (_IGOtherVoucherService.Query.Any(x => x.CPPeriodID == model.ID))
                {
                    MSG.Warning("Kỳ tính giá thành này đã phát sinh chứng từ nghiệm thu. Vui lòng kiểm tra lại trước khi thực hiện việc xóa kỳ tính giá thành này!");
                    return;
                }
                else if (model != null && MSG.Question(string.Format("Bạn có chắc chắn muốn xóa {0} này không?", model.Name)) == DialogResult.Yes)
                {
                    List<CPAllocationGeneralExpense> lst = Utils.ICPAllocationGeneralExpenseService.GetAll();
                    List<CPAllocationGeneralExpense> lst1 = lst.Where(x => x.CPPeriodID == model.ID && x.AllocatedRate > 0).ToList();
                    List<CPAllocationGeneralExpense> lst2 = lst.Where(x => x.CPPeriodID == model.ID && x.AllocatedRate == 0).ToList();
                    try
                    {
                        Utils.ICPPeriodService.BeginTran();
                        foreach (var item in lst1)
                        {
                            CPAllocationGeneralExpense item1 = Utils.ICPAllocationGeneralExpenseService.GetAll().FirstOrDefault(x => x.ReferenceID == item.ReferenceID && x.AllocatedRate == 0);
                            lst2.Remove(item1);
                            model.CPAllocationGeneralExpenses.Remove(item);
                        }
                        foreach (var item2 in lst2)
                        {
                            item2.CPPeriodID = Guid.NewGuid();
                            Utils.ICPAllocationGeneralExpenseService.Update(item2);
                            model.CPAllocationGeneralExpenses.Remove(item2);
                        }
                        foreach (var item in lst1)
                        {
                            if (item.AllocatedRate == 100)
                            {
                                item.CPPeriodID = Guid.NewGuid();
                                item.AllocatedRate = 0;
                                item.UnallocatedAmount = item.TotalCost;
                                item.AllocatedAmount = 0;
                                Utils.ICPAllocationGeneralExpenseService.Update(item);
                            }
                            else
                            {
                                int i = 0;
                                List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = (from g in lst
                                                                                                join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                                where g.ReferenceID == item.ReferenceID && g.AllocatedRate > 0
                                                                                                orderby h.FromDate
                                                                                                select g).ToList();
                                if (cPAllocationGeneralExpenses.Count > 0)
                                {
                                    foreach (var item1 in cPAllocationGeneralExpenses)
                                    {
                                        if (i == 0)
                                        {
                                            item1.UnallocatedAmount += item.AllocatedAmount;
                                        }
                                        else
                                        {
                                            CPPeriod period = cPPeriods.FirstOrDefault(x => x.ID == item1.CPPeriodID);
                                            CPAllocationGeneralExpense cPAllocationGeneralExpense = (from g in cPAllocationGeneralExpenses
                                                                                                     join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                                     where h.ToDate < period.FromDate
                                                                                                     select g).FirstOrDefault();
                                            if (cPAllocationGeneralExpense != null)
                                            {
                                                item1.UnallocatedAmount += cPAllocationGeneralExpense.AllocatedAmount;
                                            }
                                            else
                                            {
                                                item1.UnallocatedAmount += item.AllocatedAmount;
                                            }
                                        }
                                        item1.AllocatedAmount = item1.UnallocatedAmount * item1.AllocatedRate / 100;
                                        Utils.ICPAllocationGeneralExpenseService.Update(item1);
                                        i++;
                                    }
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense1 = (from g in lst
                                                                                              join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                              where g.ReferenceID == item.ReferenceID && g.AllocatedRate == 0
                                                                                              select g).FirstOrDefault();
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense2 = (from g in lst
                                                                                              join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                              where g.ReferenceID == item.ReferenceID && g.AllocatedRate > 0
                                                                                              select g).OrderByDescending(x => x.UnallocatedAmount).FirstOrDefault();
                                    if (cPAllocationGeneralExpense1 != null)
                                    {
                                        cPAllocationGeneralExpense1.UnallocatedAmount += cPAllocationGeneralExpense2.AllocatedAmount;
                                        cPAllocationGeneralExpense1.AllocatedAmount = cPAllocationGeneralExpense1.UnallocatedAmount * cPAllocationGeneralExpense1.AllocatedRate / 100;
                                        Utils.ICPAllocationGeneralExpenseService.Update(cPAllocationGeneralExpense1);
                                    }
                                }
                                else
                                {
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense = (from g in lst
                                                                                             join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                             where g.ReferenceID == item.ReferenceID && g.AllocatedRate == 0
                                                                                             orderby h.FromDate
                                                                                             select g).FirstOrDefault();
                                    if (cPAllocationGeneralExpense != null)
                                        Utils.ICPAllocationGeneralExpenseService.Delete(cPAllocationGeneralExpense);
                                }
                            }

                            List<CPAllocationGeneralExpenseDetail> cPAllocationGeneralExpenseDetails = Utils.ICPAllocationGeneralExpenseDetailService.GetAll().Where(x => x.CPAllocationGeneralExpenseID == item.ID).ToList();
                            foreach (var itemdetail in cPAllocationGeneralExpenseDetails)
                            {
                                Utils.ICPAllocationGeneralExpenseDetailService.Delete(itemdetail);
                            }
                            Utils.ICPAllocationGeneralExpenseService.Delete(item);
                        }

                        Utils.ICPPeriodService.Delete(model);
                        Utils.ICPPeriodService.CommitTran();

                        List<CPPeriod> lstCPPeriod1 = Utils.ICPPeriodService.GetAll();
                        if (lstCPPeriod1.Count == 0)
                        {
                            Utils.ICPAllocationGeneralExpenseService.BeginTran();
                            List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = Utils.ICPAllocationGeneralExpenseService.GetAll();
                            foreach (var item in cPAllocationGeneralExpenses)
                            {
                                Utils.ICPAllocationGeneralExpenseService.Delete(item);
                            }
                            Utils.ICPAllocationGeneralExpenseService.CommitTran();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.ICPPeriodService.RolbackTran();
                    }
                    Utils.ClearCacheByType<CPPeriod>();
                    LoadCogfig();
                }
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        protected override void EditFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                CostPeriod temp = uGridDS.ActiveRow != null ? uGridDS.ActiveRow.ListObject as CostPeriod : uGridDS.Selected.Rows[0].ListObject as CostPeriod;
                var model = _ICPPeriodService.Getbykey(temp.ID);
                _ICPPeriodService.UnbindSession(model);
                model = _ICPPeriodService.Getbykey(temp.ID);
                //new FCPCostOfContractDetail(model).ShowDialog(this);

                //if (!FCPCostOfContractDetail.isClose)
                //{
                //    Utils.ClearCacheByType<CPPeriod>();
                //    LoadCogfig();
                //}
                FCPCostOfContractDetail frm = new FCPCostOfContractDetail(model);
                frm.FormClosed += new FormClosedEventHandler(FCPCostOfContractDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadCogfig();
        }

        private void uGridDS_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();            
        }

        private void uGridDS_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }
        private void uGridDS_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (e.Row == null) return;
            var temp = e.Row.ListObject as CostPeriod;
            if (temp == null) return;
            CPPeriod select = Utils.ListCPPeriod.FirstOrDefault(x => x.ID == temp.ID);
            foreach (var x in select.CPExpenseLists)
            {
                x.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(c => c.ID == x.ExpenseItemID).ExpenseItemCode;
                x.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).Code;
            }
            uGridCPExpenseList1.SetDataBinding(select.CPExpenseLists.Where(x => x.TypeVoucher == 0).ToList(), "");
            uGridCPExpenseList2.SetDataBinding(select.CPExpenseLists.Where(x => x.TypeVoucher == 1).ToList(), "");

            List<CPAllocationGeneralExpenseDetail> listpb = new List<CPAllocationGeneralExpenseDetail>();
            foreach (var a in select.CPAllocationGeneralExpenses)
            {
                List<CPAllocationGeneralExpenseDetail> lstpbdetail = a.CPAllocationGeneralExpenseDetails.Where(x => x.ContractID != null && x.ContractID != Guid.Empty && select.CPPeriodDetails.Any(d => d.ContractID == x.ContractID)).ToList().Where(x => x.AllocatedRate > 0).ToList();
                foreach (var b in lstpbdetail)
                {
                    b.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(c => c.ID == b.ExpenseItemID).ExpenseItemCode;
                    b.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == b.ContractID).Code;
                    b.SignedDate = Utils.ListEmContract.FirstOrDefault(c => c.ID == b.ContractID).SignedDate;
                    b.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(c => c.ID == b.ContractID).AccountingObjectName;
                }
                listpb.AddRange(lstpbdetail);
            }
            uGridCPAllocationGeneralExpenseDetail.SetDataBinding(listpb, "");
        }
        #endregion

        #endregion

        private void btnNghiemThu_Click(object sender, EventArgs e)
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                CostPeriod temp = uGridDS.ActiveRow != null ? uGridDS.ActiveRow.ListObject as CostPeriod : uGridDS.Selected.Rows[0].ListObject as CostPeriod;
                if (temp.UnAcceptedAmount < (decimal)0.01)
                {
                    MSG.Information("Kỳ tính giá thành này đã nghiệm thu hết giá trị");
                    return;
                }
                var model = Utils.ListCPPeriod.FirstOrDefault(x => x.ID == temp.ID);
                new FCPCostOfContractDetail(model, true).ShowDialog(this);

                if (!FCPCostOfContractDetail.isClose)
                {
                    Utils.ClearCacheByType<CPPeriod>();
                    LoadCogfig();
                }
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGridDS, "Giá thành theo hợp đồng");
        }

        private void FCPCostOfContract_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDS.ResetText();
            uGridDS.ResetUpdateMode();
            uGridDS.ResetExitEditModeOnLeave();
            uGridDS.ResetRowUpdateCancelAction();
            uGridDS.DataSource = null;
            uGridDS.Layouts.Clear();
            uGridDS.ResetLayouts();
            uGridDS.ResetDisplayLayout();
            uGridDS.Refresh();
            uGridDS.ClearUndoHistory();
            uGridDS.ClearXsdConstraints();

            uGridCPExpenseList1.ResetText();
            uGridCPExpenseList1.ResetUpdateMode();
            uGridCPExpenseList1.ResetExitEditModeOnLeave();
            uGridCPExpenseList1.ResetRowUpdateCancelAction();
            uGridCPExpenseList1.DataSource = null;
            uGridCPExpenseList1.Layouts.Clear();
            uGridCPExpenseList1.ResetLayouts();
            uGridCPExpenseList1.ResetDisplayLayout();
            uGridCPExpenseList1.Refresh();
            uGridCPExpenseList1.ClearUndoHistory();
            uGridCPExpenseList1.ClearXsdConstraints();

            uGridCPExpenseList2.ResetText();
            uGridCPExpenseList2.ResetUpdateMode();
            uGridCPExpenseList2.ResetExitEditModeOnLeave();
            uGridCPExpenseList2.ResetRowUpdateCancelAction();
            uGridCPExpenseList2.DataSource = null;
            uGridCPExpenseList2.Layouts.Clear();
            uGridCPExpenseList2.ResetLayouts();
            uGridCPExpenseList2.ResetDisplayLayout();
            uGridCPExpenseList2.Refresh();
            uGridCPExpenseList2.ClearUndoHistory();
            uGridCPExpenseList2.ClearXsdConstraints();

            uGridCPAllocationGeneralExpenseDetail.ResetText();
            uGridCPAllocationGeneralExpenseDetail.ResetUpdateMode();
            uGridCPAllocationGeneralExpenseDetail.ResetExitEditModeOnLeave();
            uGridCPAllocationGeneralExpenseDetail.ResetRowUpdateCancelAction();
            uGridCPAllocationGeneralExpenseDetail.DataSource = null;
            uGridCPAllocationGeneralExpenseDetail.Layouts.Clear();
            uGridCPAllocationGeneralExpenseDetail.ResetLayouts();
            uGridCPAllocationGeneralExpenseDetail.ResetDisplayLayout();
            uGridCPAllocationGeneralExpenseDetail.Refresh();
            uGridCPAllocationGeneralExpenseDetail.ClearUndoHistory();
            uGridCPAllocationGeneralExpenseDetail.ClearXsdConstraints();
        }

        private void FCPCostOfContract_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
