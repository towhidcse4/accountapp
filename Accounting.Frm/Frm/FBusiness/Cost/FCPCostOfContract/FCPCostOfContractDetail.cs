﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.ComponentModel;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;

namespace Accounting
{
    public partial class FCPCostOfContractDetail : CustormForm
    {
        #region Khai báo
        private IEMContractService _IEmContractService
        {
            get { return IoC.Resolve<IEMContractService>(); }
        }
        private ICPPeriodService _ICPPeriodService
        {
            get { return IoC.Resolve<ICPPeriodService>(); }
        }
        private ISAInvoiceService _ISAInvoiceService
        {
            get { return IoC.Resolve<ISAInvoiceService>(); }
        }
        private ICPAllocationGeneralExpenseDetailService _ICPAllocationGeneralExpenseDetailService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseDetailService>(); }
        }
        private ICPAllocationGeneralExpenseService _ICPAllocationGeneralExpenseService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseService>(); }
        }
        private ICPAcceptanceDetailService _ICPAcceptanceDetailService
        {
            get { return IoC.Resolve<ICPAcceptanceDetailService>(); }
        }
        private IGOtherVoucherService _IGOtherVoucherService
        {
            get { return IoC.Resolve<IGOtherVoucherService>(); }
        }
        private int indexCurrent = 0;
        List<Control> lstControl = new List<Control>();
        List<string> text = new List<string>();
        public static bool isClose = true;
        bool IsAdd = true;
        public List<EMContract> _LstContract;
        List<Guid> listContractID;
        List<CPAllocationGeneralExpense> dsAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
        public CPPeriod _select = new CPPeriod();
        List<CPExpenseList> LstCPExpense1;
        List<CPExpenseList> LstCPExpense2;
        static Dictionary<int, string> DicCostSetType;
        bool IsNT = false;
        DateTime fromDate;
        DateTime toDate;
        private List<Account> _dsAccount = new List<Account>(); //Bảng Account
        public static Dictionary<int, string> dicCostSetType
        {
            get { DicCostSetType = DicCostSetType ?? (Dictionary<int, string>)BuildConfig(1); return DicCostSetType; }
        }
        int lamtron = int.Parse(Utils.ListSystemOption.FirstOrDefault(c => c.Code == "DDSo_TienVND").Data);
        #endregion

        #region Khởi tạo
        public FCPCostOfContractDetail()
        {
            InitializeComponent();
            LoadChung();
            ConfigButton();
            ShowPal();
        }
        public FCPCostOfContractDetail(CPPeriod temp)
        {
            InitializeComponent();
            IsAdd = false;
            _select = temp;
            fromDate = temp.FromDate;
            toDate = temp.ToDate;
            LoadChung();
            ConfigButton();
            ShowPal();
            ObjandGUI(temp, true);
        }
        public FCPCostOfContractDetail(CPPeriod temp, bool NT)
        {
            InitializeComponent();
            IsAdd = false;
            IsNT = true;
            _select = temp;
            fromDate = temp.FromDate;
            toDate = temp.ToDate;
            LoadChung();
        }
        #endregion

        #region Hàm xử lý nghiệp vụ
        void LoadChung()
        {
            _dsAccount = Utils.IAccountService.GetListAccountIsActive(true).Where(x => x.AccountNumber.StartsWith("154") || x.AccountNumber.StartsWith("632")).ToList();
            uGridAllocationGeneralExpense.Error += new ErrorEventHandler((s, e) => uGridAllocationGeneralExpense_Error(s, e));
            uGridNT.Error += new ErrorEventHandler((s, e) => uGridAllocationGeneralExpense_Error(s, e));
            uGridAllocationGeneralExpenseResult.Error += new ErrorEventHandler((s, e) => uGridAllocationGeneralExpense_Error(s, e));
            text = new List<string> { "Xác định kỳ tính giá thành", "Tập hợp chi phí trực tiếp", "Tập hợp các khoản giảm giá thành", "Phân bổ chi phí chung" };
            lstControl = new List<Control> { Panel1, Panel2, Panel3, Panel4 };
            ViewGridPanel1(new List<EMContract>());
            ViewGridPanel5();
            if (IsAdd)
            {
                List<EMContract> list1 = Utils.ListEmContract.Where(x => x.IsWatchForCostPrice && x.TypeID == 860).ToList();
                list1 = list1 == null ? new List<EMContract>() : list1;
                foreach (var item in list1)
                {
                    item.Select = false;
                }
                uGridCostset.SetDataBinding(new BindingList<EMContract>(list1), "");
                txtCPPeriod.Text = "Nghiệm thu hợp đồng của kỳ tính giá thành từ " + fromDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến " + toDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else if (IsNT)
            {
                this.Text = "Nghiệm thu kỳ tính giá thành";
                indexCurrent = 4;
                Panel5.Visible = true;
                Panel4.Visible = false;
                Panel5.Dock = DockStyle.Fill;
                btnBack.Enabled = false;
                btnNext.Enabled = false;
                btnSave.Enabled = true;
                btnNghiemThu.Enabled = false;
                txtCPPeriod5.Text = _select.Name;
                List<CPAcceptanceDetail> listNT = new List<CPAcceptanceDetail>();
                if ((_select != null && _select.CPAcceptances.Count == 0))
                {
                    var lstntdetail = _ICPAcceptanceDetailService.GetAll();
                    foreach (var x in Utils.ListEmContract.Where(c => _select.CPPeriodDetails.Any(d => d.ContractID == c.ID)).ToList())
                    {
                        CPAcceptanceDetail model = new CPAcceptanceDetail();
                        model.ContractID = x.ID;
                        model.ContractCode = x.Code;
                        model.SignedDate = x.SignedDate;
                        model.AccountingObjectName = x.AccountingObjectName;
                        model.RevenueAmount = _ISAInvoiceService.DoanhSoHD(x.ID, _select.FromDate, _select.ToDate);
                        decimal a = _select.CPExpenseLists.Where(c => c.ContractID == x.ID && c.TypeVoucher == 0).Sum(c => c.Amount);
                        decimal b = _select.CPExpenseLists.Where(c => c.ContractID == x.ID && c.TypeVoucher == 1).Sum(c => c.Amount);
                        decimal h = 0;
                        foreach (var allo in _select.CPAllocationGeneralExpenses.Where(C => C.AllocatedRate > 0).ToList())
                        {
                            h = h + allo.CPAllocationGeneralExpenseDetails.Where(c => c.ContractID == x.ID).Sum(c => c.AllocatedAmount);
                        }
                        decimal f = !lstntdetail.Any(c => c.ContractID == x.ID) ? (Utils.ListCPOPN.Any(c => c.ContractID == x.ID) == true ? Utils.ListCPOPN.FirstOrDefault(c => c.ContractID == x.ID).NotAcceptedAmount ?? 0 : 0) : LuyKe(x.ID);
                        model.Amount = (f + a - b + h);
                        model.AcceptedRate = 100;
                        model.TotalAcceptedAmount = (f + a - b + h);
                        listNT.Add(model);
                    }
                }
                else if (_select.CPAcceptances.Count > 0)
                {

                    foreach (var x in Utils.ListEmContract.Where(c => _select.CPPeriodDetails.Any(d => d.ContractID == c.ID)).ToList())
                    {
                        CPAcceptanceDetail model = new CPAcceptanceDetail();
                        model.ContractID = x.ID;
                        model.ContractCode = x.Code;
                        model.SignedDate = x.SignedDate;
                        model.AccountingObjectName = x.AccountingObjectName;
                        model.RevenueAmount = _select.CPAcceptances[0].CPAcceptanceDetails.FirstOrDefault(c => c.ContractID == x.ID).RevenueAmount;
                        model.Amount = (_select.CPAcceptances.FirstOrDefault(d => d.OrderPriority == (_select.CPAcceptances.Count - 1)).CPAcceptanceDetails.FirstOrDefault(c => c.ContractID == x.ID).Amount - _select.CPAcceptances.FirstOrDefault(d => d.OrderPriority == (_select.CPAcceptances.Count - 1)).CPAcceptanceDetails.FirstOrDefault(c => c.ContractID == x.ID).TotalAcceptedAmount);
                        model.AcceptedRate = 100;
                        model.TotalAcceptedAmount = model.Amount;
                        listNT.Add(model);
                    }
                }
                uGridNT.SetDataBinding(new BindingList<CPAcceptanceDetail>(listNT), "");
            }
            else
            {
                ViewGridPanel1(Utils.ListEmContract.ToList().Where(x => _select.CPPeriodDetails.Any(d => d.ContractID == x.ID)).ToList());
                dtBeginDate.Value = _select.FromDate;
                dtEndDate.Value = _select.ToDate;
                txtCPPeriod.Text = _select.Name;
                _LstContract = Utils.ListEmContract.ToList().Where(x => _select.CPPeriodDetails.Any(d => d.ContractID == x.ID)).ToList();
            }

        }
        #endregion

        #region Hàm xử lý Grid
        private void ConfigButton()
        {
            if (indexCurrent == 0)
            {//đầu tiên
                btnBack.Enabled = false;
                btnNext.Enabled = true;
                btnSave.Enabled = false;
                btnNghiemThu.Enabled = false;
            }
            else if (indexCurrent == lstControl.Count - 1)
            {
                btnBack.Enabled = true;
                btnNext.Enabled = false;
                btnSave.Enabled = true;
                btnNghiemThu.Enabled = !IsAdd;
            }
            else
            {
                btnBack.Enabled = true;
                btnNext.Enabled = true;
                btnSave.Enabled = false;
                btnNghiemThu.Enabled = false;
            }
            if (!IsAdd)
            {
                txtCPPeriod.ReadOnly = true;
                txtCPPeriod2.ReadOnly = true;
                txtCPPeriod3.ReadOnly = true;
                txtCPPeriod4.ReadOnly = true;
                txtCPPeriod5.ReadOnly = true;
                dtBeginDate.ReadOnly = true;
                dtEndDate.ReadOnly = true;
                cbbDateTime.ReadOnly = true;
            }
        }
        private void ShowPal()
        {
            this.Text = text[indexCurrent];
            foreach (Control control in lstControl) control.Visible = false;
            Panel5.Visible = false;
            lstControl[indexCurrent].Visible = true;
            lstControl[indexCurrent].Dock = DockStyle.Fill;
            if (indexCurrent == 0 && !IsAdd)
            {
                uGridCostset.DisplayLayout.Bands[0].Columns["Select"].CellActivation = Activation.NoEdit;
            }
        }
        #endregion

        #region Events
        private void btnBack_Click(object sender, EventArgs e)
        {
            indexCurrent -= 1;
            ConfigButton();
            ShowPal();
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (indexCurrent == 0)
            {
                listContractID = new List<Guid>();
                _LstContract = new List<EMContract>();
                if (IsAdd)
                {
                    for (int i = 0; i < uGridCostset.Rows.Count; i++)
                    {
                        if (uGridCostset.Rows[i].Cells["Select"].Value == null)
                        {
                            uGridCostset.Rows[i].Cells["Select"].Value = false;
                        }
                        if (bool.Parse(uGridCostset.Rows[i].Cells["Select"].Value.ToString()) == true)
                        {
                            EMContract cs = uGridCostset.Rows[i].ListObject as EMContract;
                            _LstContract.Add(cs);
                            listContractID.Add(cs.ID);
                            if (!CheckCostSet(fromDate, toDate, cs.ID))
                            {
                                MSG.Error("Hợp đồng được chọn đã có trong kỳ tính giá thành khác trùng khoảng thời gian với kỳ tính giá thành này. Vui lòng chọn lại");
                                return;
                            }
                        }
                    }
                    if (listContractID.Count == 0)
                    {
                        MSG.Error("Bạn chưa chọn đối tượng tính giá thành!");
                        return;
                    }
                    txtCPPeriod2.Text = txtCPPeriod.Text;
                    ViewGridPanel2(Utils.IGeneralLedgerService.GetCPExpenseListsContractOnGL(fromDate, toDate, listContractID).OrderBy(x => x.ContractCode).ToList());
                }
                else
                {
                    for (int i = 0; i < uGridCostset.Rows.Count; i++)
                    {
                        EMContract cs = uGridCostset.Rows[i].ListObject as EMContract;
                        _LstContract.Add(cs);
                        listContractID.Add(cs.ID);
                    }
                    txtCPPeriod2.Text = txtCPPeriod.Text;
                    foreach (var x in _select.CPExpenseLists.Where(x => x.TypeVoucher == 0))
                    {
                        x.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(c => c.ID == x.ExpenseItemID).ExpenseItemCode;
                        x.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).Code;
                    }
                    ViewGridPanel2(_select.CPExpenseLists.Where(x => x.TypeVoucher == 0).ToList().OrderBy(x => x.ContractCode).ToList());
                }

            }
            else if (indexCurrent == 1)
            {
                if (IsAdd)
                {
                    txtCPPeriod3.Text = txtCPPeriod.Text;
                    ViewGridPanel3(Utils.IGeneralLedgerService.GetCPExpenseListsContractOnGL2(fromDate, toDate, listContractID).OrderBy(x => x.ContractCode).ToList());
                }
                else
                {
                    txtCPPeriod3.Text = txtCPPeriod.Text;
                    foreach (var x in _select.CPExpenseLists.Where(x => x.TypeVoucher == 1))
                    {
                        x.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(c => c.ID == x.ExpenseItemID).ExpenseItemCode;
                        x.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).Code;
                    }
                    ViewGridPanel3(_select.CPExpenseLists.Where(x => x.TypeVoucher == 1).ToList().OrderBy(x => x.ContractCode).ToList());
                }

            }
            else if (indexCurrent == 2)
            {
                //edit by cuongpv
                txtCPPeriod4.Text = txtCPPeriod.Text;
                ViewGridPanel4();
                //end edit by cuongpv
            }
            indexCurrent += 1;
            ConfigButton();
            ShowPal();
        }
        private void btnNghiemThu_Click(object sender, EventArgs e)
        {
            this.Text = "Nghiệm thu kỳ tính giá thành";
            indexCurrent = 4;
            Panel5.Visible = true;
            Panel4.Visible = false;
            Panel5.Dock = DockStyle.Fill;
            btnBack.Enabled = true;
            btnNext.Enabled = false;
            btnSave.Enabled = true;
            btnNghiemThu.Enabled = false;
            txtCPPeriod5.Text = txtCPPeriod.Text;
            List<CPAcceptanceDetail> listNT = new List<CPAcceptanceDetail>();
            if ((_select != null && _select.CPAcceptances.Count == 0))
            {
                var lstntdetail = _ICPAcceptanceDetailService.GetAll();
                foreach (var x in _LstContract)
                {
                    CPAcceptanceDetail model = new CPAcceptanceDetail();
                    model.ContractID = x.ID;
                    model.ContractCode = x.Code;
                    model.SignedDate = x.SignedDate ?? DateTime.Now;
                    model.AccountingObjectName = x.AccountingObjectName;
                    model.RevenueAmount = Math.Round((_ISAInvoiceService.DoanhSoHD(x.ID, fromDate, toDate)), lamtron, MidpointRounding.AwayFromZero);
                    decimal a = ((BindingList<CPExpenseList>)uGridCpExpenseList2.DataSource).ToList().Where(c => c.ContractID == x.ID).Sum(c => c.Amount);
                    decimal b = ((BindingList<CPExpenseList>)uGridCpExpenseList3.DataSource).ToList().Where(c => c.ContractID == x.ID).Sum(c => c.Amount);
                    decimal h = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).ToList().Where(c => c.ContractID == x.ID).Sum(c => c.AllocatedAmount);
                    decimal f = !lstntdetail.Any(c => c.ContractID == x.ID) ? (Utils.ListCPOPN.Any(c => c.ContractID == x.ID) == true ? Utils.ListCPOPN.FirstOrDefault(c => c.ContractID == x.ID).NotAcceptedAmount ?? 0 : 0) : LuyKe(x.ID);
                    model.Amount = Math.Round(((f + a - b + h)), lamtron, MidpointRounding.AwayFromZero); ;
                    model.AcceptedRate = 100;
                    model.TotalAcceptedAmount = Math.Round(((f + a - b + h)), lamtron, MidpointRounding.AwayFromZero); ;
                    listNT.Add(model);
                }
            }
            else
            {
                //listNT = _select.CPAcceptances[0].CPAcceptanceDetails.ToList();
                //foreach (var x in listNT)
                //{
                //    x.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).Code;
                //    x.SignedDate = Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).SignedDate ?? DateTime.Now;
                //    x.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(c => c.ID == x.ContractID).AccountingObjectName;
                //}
                decimal total = _select.CPAcceptances.FirstOrDefault(x => x.OrderPriority == 0).CPAcceptanceDetails.Sum(c => c.Amount);
                List<CPAcceptance> cPAcceptances = _select.CPAcceptances.ToList();
                List<CPAcceptanceDetail> cPAcceptanceDetails = new List<CPAcceptanceDetail>();
                List<CPPeriodDetail> cPPeriodDetails = Utils.ListCPPeriodDetail.ToList();
                foreach (var item in cPAcceptances)
                {
                    cPAcceptanceDetails.AddRange(item.CPAcceptanceDetails);
                    foreach (var item1 in cPAcceptanceDetails)
                    {
                        item1.CPPeriodID = item.CPPeriodID;
                        //item1.ContractID = cPPeriodDetails.FirstOrDefault(x => x.CPPeriodID == item1.CPPeriodID);
                    }
                }
                listNT = (from b in cPAcceptanceDetails
                          group b by new { b.ContractID } into g
                          select new CPAcceptanceDetail
                          {
                              ContractID = g.Key.ContractID,
                              ContractCode = Utils.ListEmContract.FirstOrDefault(x => x.ID == g.Key.ContractID).Code,
                              AccountingObjectName = Utils.ListEmContract.FirstOrDefault(x => x.ID == g.Key.ContractID).AccountingObjectName,
                              SignedDate = Utils.ListEmContract.FirstOrDefault(x => x.ID == g.Key.ContractID).SignedDate,
                              RevenueAmount = Math.Round(_ISAInvoiceService.DoanhSoHD(g.Key.ContractID, _select.FromDate, _select.ToDate), lamtron, MidpointRounding.AwayFromZero),
                              Amount = Math.Round(Amount(_select, g.Key.ContractID), lamtron, MidpointRounding.AwayFromZero),
                              TotalAcceptedAmount = Math.Round(g.Sum(x => x.TotalAcceptedAmount), lamtron, MidpointRounding.AwayFromZero)
                          }).ToList();
                foreach (var x in listNT)
                {
                    if (x.Amount != 0)
                        x.AcceptedRate = (x.TotalAcceptedAmount / x.Amount) * 100;
                    else
                        x.AcceptedRate = 0;
                    x.Amount = Math.Round((x.Amount * (100 - x.AcceptedRate)) / 100, lamtron, MidpointRounding.AwayFromZero);
                }
            }
            uGridNT.SetDataBinding(new BindingList<CPAcceptanceDetail>(listNT), "");
        }
        decimal Amount(CPPeriod model, Guid ContractID)
        {
            var lstAlloDetail = new List<CPAllocationGeneralExpenseDetail>();
            foreach (var x in model.CPAllocationGeneralExpenses)
            {
                var lst = x.CPAllocationGeneralExpenseDetails.Where(d => d.ContractID == ContractID).ToList();
                if (lst.Count > 0) lstAlloDetail.AddRange(lst);
            }
            var lstid = model.CPAcceptances.Select(x => x.ID).ToList();
            var lstntdetail = _ICPAcceptanceDetailService.GetAll().ToList().Where(d => !lstid.Any(c => c == d.CPAcceptanceID)).ToList();
            decimal a = model.CPExpenseLists.Where(x => x.TypeVoucher == 0).ToList().Where(c => c.ContractID == ContractID).Sum(c => c.Amount);
            decimal b = model.CPExpenseLists.Where(x => x.TypeVoucher == 1).ToList().Where(c => c.ContractID == ContractID).Sum(c => c.Amount);
            decimal h = lstAlloDetail.Where(c => c.ContractID == ContractID).Sum(c => c.AllocatedAmount);
            decimal f = !lstntdetail.Any(c => c.ContractID == ContractID) ? (Utils.ListCPOPN.Any(c => c.ContractID == ContractID) == true ? Utils.ListCPOPN.FirstOrDefault(c => c.ContractID == ContractID).NotAcceptedAmount ?? 0 : 0) : LuyKe(ContractID);
            return (f + a - b + h);
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            _ICPPeriodService.BeginTran();
            try
            {
                if (!IsNT)
                {
                    List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                    List<CPAllocationGeneralExpenseDetail> cPAllocationGeneralExpenseDetails = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).ToList();
                    if (cPAllocationGeneralExpenses.Sum(x => x.AllocatedRate) != 0)
                    {
                        if (cPAllocationGeneralExpenseDetails.Count == 0)
                        {
                            MSG.Warning("Chi phí chung chưa được phân bổ ");
                            return;
                        }
                    }
                }
                if (IsAdd)
                {
                    if (fromDate > toDate)
                    {
                        MSG.Error("Ngày bắt đầu phải nhỏ hơn ngày kết thúc!");
                        return;
                    }
                    CPPeriod model = new CPPeriod();//Kỳ tính giá thành
                    model.ID = Guid.NewGuid();
                    model.Name = txtCPPeriod.Text;
                    model.FromDate = fromDate;
                    model.ToDate = toDate;
                    model.Type = 5;
                    foreach (var x in _LstContract)
                    {
                        CPPeriodDetail detail = new CPPeriodDetail();//Chi tiết kỳ tính giá thành
                        detail.ID = Guid.NewGuid();
                        detail.CPPeriodID = model.ID;
                        detail.ContractID = x.ID;
                        model.CPPeriodDetails.Add(detail);
                    }
                    foreach (var item in model.CPPeriodDetails)
                    {
                        if (Utils.ListCPPeriod.Count(x => x.FromDate > model.ToDate && x.CPPeriodDetails.Any(c => c.CostSetID == item.CostSetID) && x.CPAcceptances.Count > 0) == 1)
                        {
                            MSG.Warning("Đã phát sinh kỳ tính giá thành được nghiệm thu sau kỳ tính giá thành này. \nViệc tạo kỳ tính giá thành mới sẽ làm thay đổi dữ liệu của những kỳ tính sau nó. \nVui lòng xóa chứng từ nghiệm thu của các kỳ tính giá thành sau kỳ tính này và thực hiện lại việc tính giá!");
                            return;
                        }
                    }
                    model.CPExpenseLists = ((BindingList<CPExpenseList>)uGridCpExpenseList2.DataSource).ToList();//THCP TT
                    foreach (var x in model.CPExpenseLists)
                    {
                        x.ID = Guid.NewGuid();
                        x.CPPeriodID = model.ID;
                        x.TypeVoucher = 0;
                    }
                    foreach (var x in ((BindingList<CPExpenseList>)uGridCpExpenseList3.DataSource).ToList())// THCK GGT
                    {
                        x.ID = Guid.NewGuid();
                        x.CPPeriodID = model.ID;
                        x.TypeVoucher = 1;
                        model.CPExpenseLists.Add(x);
                    }
                    model.CPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();//Phân bổ chi phí
                    foreach (var x in model.CPAllocationGeneralExpenses)
                    {
                        x.CPPeriodID = model.ID;
                        x.CPAllocationGeneralExpenseDetails = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).ToList().Where(c => c.CPAllocationGeneralExpenseID == x.ID && c.AllocatedAmount > 0).ToList(); // chi tiết phân bổ chi phí                      
                    }
                    #region comment by cuongpv vi lay luy ke phan bo theo cach khac cach nay ko dung duoc
                    //List<CPAllocationGeneralExpense> listAllocationGeneralExpense2 = model.CPAllocationGeneralExpenses.Where(x => x.AllocatedRate < 100 && x.AllocatedRate > 0).ToList();
                    //foreach (var item in listAllocationGeneralExpense2)
                    //{
                    //    CPAllocationGeneralExpense cpe = new CPAllocationGeneralExpense();
                    //    cpe.ID = Guid.NewGuid();
                    //    cpe.CPPeriodID = item.CPPeriodID;
                    //    cpe.ExpenseItemID = item.ExpenseItemID;
                    //    cpe.TotalCost = item.TotalCost;
                    //    cpe.UnallocatedAmount = item.UnallocatedAmount - item.AllocatedAmount;
                    //    cpe.AllocatedRate = 0;
                    //    cpe.AllocatedAmount = 0;
                    //    cpe.ReferenceID = item.ReferenceID;
                    //    Utils.ICPAllocationGeneralExpenseService.CreateNew(cpe);
                    //}
                    #endregion
                    _ICPPeriodService.CreateNew(model);
                    _ICPPeriodService.CommitTran();
                    MSG.Information("Lưu dữ liệu thành công!");
                    btnBack.Enabled = false;
                    _select = model;
                    IsAdd = false;
                    btnNghiemThu.Enabled = true;
                }
                else
                {
                    GOtherVoucher govoucher = new GOtherVoucher();
                    if (indexCurrent == 4)// Nghiệm thu
                    {

                        if (Utils.ListGOtherVoucher.Any(x => x.CPPeriodID == _select.ID) && !IsNT)
                        {
                            MSG.Warning("Kỳ tính giá thành này đã phát sinh chứng từ nghiệm thu. Vui lòng kiểm tra lại trước khi thực hiện việc sửa kỳ tính giá thành này!");
                            return;
                        }
                        if (((BindingList<CPAcceptanceDetail>)uGridNT.DataSource).ToList().Sum(c => c.TotalAcceptedAmount) == 0)
                        {
                            MSG.Warning("Số tiền nghiệm thu cần lớn hơn 0!");
                            return;
                        }
                        if (cbbAccountNo.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn Tài khoản nợ.");
                            return;
                        }
                        if (cbbAccountCo.Value == null)
                        {
                            MSG.Error("Bạn chưa chọn Tài khoản có");
                            return;
                        }
                        CPAcceptance cpa = new CPAcceptance();
                        cpa.ID = Guid.NewGuid();

                        govoucher.ID = Guid.NewGuid();
                        govoucher.TypeID = 600;
                        govoucher.Date = _select.ToDate;
                        govoucher.PostedDate = _select.ToDate;
                        govoucher.No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(60));
                        govoucher.Reason = "Nghiệm thu hợp đồng của kỳ tính giá thành từ " + _select.FromDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến " + _select.ToDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        govoucher.TotalAmount = ((BindingList<CPAcceptanceDetail>)uGridNT.DataSource).ToList().Sum(c => c.TotalAcceptedAmount);
                        govoucher.TotalAmountOriginal = govoucher.TotalAmount;
                        govoucher.CPPeriodID = _select.ID;
                        govoucher.CPAcceptanceID = cpa.ID;
                        govoucher.CurrencyID = "VND";
                        govoucher.ExchangeRate = 1;
                        foreach (var x in ((BindingList<CPAcceptanceDetail>)uGridNT.DataSource).ToList().Where(c => c.TotalAcceptedAmount > 0).ToList())
                        {
                            GOtherVoucherDetail detail = new GOtherVoucherDetail();
                            detail.Amount = x.TotalAcceptedAmount;
                            detail.AmountOriginal = x.TotalAcceptedAmount;
                            detail.CreditAccount = cbbAccountCo.Value.ToString();
                            detail.DebitAccount = cbbAccountNo.Value.ToString();
                            detail.CurrencyID = "VND";
                            detail.ContractID = x.ContractID;
                            detail.Description = govoucher.Reason;
                            detail.GOtherVoucherID = govoucher.ID;
                            govoucher.GOtherVoucherDetails.Add(detail);
                        }
                        _IGOtherVoucherService.CreateNew(govoucher);

                        cpa.TypeID = 600;
                        cpa.Date = _select.ToDate;
                        cpa.PostedDate = _select.ToDate;
                        cpa.No = govoucher.No;
                        cpa.Description = govoucher.Reason;
                        cpa.TotalAmount = govoucher.TotalAmount;
                        cpa.TotalAmountOriginal = govoucher.TotalAmountOriginal;
                        cpa.CPAcceptanceDetails = ((BindingList<CPAcceptanceDetail>)uGridNT.DataSource).ToList().Where(c => c.TotalAcceptedAmount > 0).ToList();
                        cpa.CPPeriodID = _select.ID;
                        cpa.OrderPriority = _select.CPAcceptances.Count;
                        if (cpa.CPAcceptanceDetails.Count > 0)
                            foreach (var a in cpa.CPAcceptanceDetails)
                            {
                                a.CPAcceptanceID = cpa.ID;
                            }
                        _select.CPAcceptances.Add(cpa);

                    }
                    if (!IsNT)
                    {
                        foreach (var x in _select.CPAllocationGeneralExpenses)// update chi tiết phân bổ
                        {
                            foreach (var a in x.CPAllocationGeneralExpenseDetails)
                            {
                                _ICPAllocationGeneralExpenseDetailService.Delete(a);
                            }
                        }
                        _select.CPAllocationGeneralExpenses.Clear();
                        _select.CPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                        foreach (var x in _select.CPAllocationGeneralExpenses)
                        {
                            List<CPAllocationGeneralExpenseDetail> lst = new List<CPAllocationGeneralExpenseDetail>();
                            lst = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).ToList().Where(c => c.CPAllocationGeneralExpenseID == x.ID).ToList().CloneObject();
                            foreach (var a in lst)
                            {
                                if (a.ID != Guid.Empty) a.ID = Guid.Empty;
                            }
                            x.CPAllocationGeneralExpenseDetails = lst;
                        }
                    }
                    _ICPPeriodService.Update(_select);
                    _ICPPeriodService.CommitTran();
                    if (indexCurrent == 4)
                    {
                        Utils.IGenCodeService.UpdateGenCodeForm(60, govoucher.No, govoucher.ID);
                        Utils.SaveLedger<GOtherVoucher>(govoucher);
                        if (
                            MSG.Question("Hệ thống đã sinh chứng từ số " + govoucher.No + " để hạch toán nghiệm thu" +
                                         " \nBạn có muốn xem chứng từ vừa tạo không") == DialogResult.Yes)
                        {
                            FGOtherVoucherDetail frm = new FGOtherVoucherDetail(govoucher, new List<GOtherVoucher>() { govoucher }, 1);
                            frm.ShowDialog(this);
                        }
                        Utils.ClearCacheByType<GOtherVoucher>();
                    }
                    else MSG.Information("Lưu dữ liệu thành công!");
                    isClose = false;
                    Close();
                }
            }
            catch (Exception ex)
            {
                _ICPPeriodService.RolbackTran();
            }
            isClose = false;
            ultraButton1.Visible = true;
            btnEscape.Visible = false;
        }
        private void btnEscape_Click(object sender, EventArgs e)
        {
            Close();
            isClose = true;
        }
        private void ultraButton1_Click(object sender, EventArgs e)
        {
            Close();
            isClose = false;
        }
        private void uGridAllocationGeneralExpenseResult_CellChange(object sender, CellEventArgs e)
        {

        }
        private decimal LuyKe(Guid ContractID)// tính lũy kế của costset ở kỳ trước
        {
            decimal lk = 0;
            var LK = Utils.ListCPPeriod.Where(x => x.ToDate < _select.FromDate && x.CPPeriodDetails.Any(c => c.ContractID == ContractID) && x.CPAcceptances.Count > 0).OrderByDescending(c => c.ToDate).ToList();
            var lstLK = LK[0].CPAcceptances.OrderBy(c => c.OrderPriority).ToList();
            var NotAccept = lstLK[0].CPAcceptanceDetails.FirstOrDefault(x => x.ContractID == ContractID).Amount;
            foreach (var item in lstLK)
            {
                var md = item.CPAcceptanceDetails.FirstOrDefault(x => x.ContractID == ContractID);
                if (md != null) lk = lk + md.TotalAcceptedAmount;
            }
            return NotAccept - lk;
        }
        private void uGridAllocationGeneralExpenseResult_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) <= 100)
                {
                    var row = uGridAllocationGeneralExpenseResult.ActiveRow;
                    if (row != null)
                    {
                        decimal amount = Math.Round(((uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList().FirstOrDefault(x => x.ID == (Guid)row.Cells["CPAllocationGeneralExpenseID"].Value).AllocatedAmount),lamtron, MidpointRounding.AwayFromZero);
                        e.Cell.Row.Cells["AllocatedAmount"].Value = Math.Round(((amount * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString())) / 100), lamtron, MidpointRounding.AwayFromZero);
                    }
                }
                else
                {

                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    var row = uGridAllocationGeneralExpenseResult.ActiveRow;
                    if (row != null)
                    {
                        decimal amount = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList().FirstOrDefault(x => x.ID == (Guid)row.Cells["CPAllocationGeneralExpenseID"].Value).AllocatedAmount;
                        e.Cell.Row.Cells["AllocatedRate"].Value = (Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / amount) * 100;
                    }
                }
            }
        }
        private void uGridAllocationGeneralExpense_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
            if (e.ErrorType == ErrorType.Data && e.ErrorText.Contains("System.Decimal"))
            {
                switch (e.DataErrorInfo.Cell.Column.Key)
                {
                    case "AllocatedRate":
                    case "AcceptedRate":
                        e.DataErrorInfo.Cell.Row.Cells[e.DataErrorInfo.Cell.Column.Key].Value = 0;
                        break;
                }
            }
        }
        #endregion

        #region Utils      
        private void ObjandGUI(CPPeriod input, bool isGet)
        {

        }
        private void ViewGridPanel1(List<EMContract> lst)
        {
            uGridCostset.DataSource = new BindingList<EMContract>(lst);
            Utils.ConfigGrid(uGridCostset, ConstDatabase.EMContract_TableName);
            foreach (var col in uGridCostset.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "Select" || col.Key == "Code" || col.Key == "SignedDate" || col.Key == "AccountingObjectName") col.Hidden = false;
                else col.Hidden = true;
            }
            var gridBand = uGridCostset.DisplayLayout.Bands[0];
            gridBand.Columns["Select"].Header.VisiblePosition = 0;
            gridBand.Columns["Select"].Hidden = false;
            gridBand.Columns["Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            gridBand.Columns["Select"].AutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
            gridBand.Columns["Select"].CellClickAction = CellClickAction.Edit;
            uGridCostset.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCostset.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            gridBand.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            gridBand.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            gridBand.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            gridBand.Columns["Select"].Header.Fixed = true;
            gridBand.Columns["Description"].Hidden = true;
            gridBand.Columns["IsActive"].Hidden = true;
            uGridCostset.DisplayLayout.Bands[0].Columns["SignedDate"].Format = "dd/MM/yyyy";
            if (!IsAdd) gridBand.Columns["Select"].Hidden = true;
            Utils.ProcessControls(this);
            if (!IsAdd) cbbDateTime.Text = "";

            if (IsAdd)
            {
                DateTime dtbegin = dtBeginDate.DateTime;
                DateTime dtend = dtEndDate.DateTime;
                fromDate = new DateTime(dtbegin.Year, dtbegin.Month, dtbegin.Day, 0, 0, 0);
                toDate = new DateTime(dtend.Year, dtend.Month, dtend.Day, 23, 59, 59);
            }
        }
        private void ViewGridPanel2(List<CPExpenseList> _LstCPExpense)
        {
            uGridCpExpenseList2.DataSource = new BindingList<CPExpenseList>(_LstCPExpense.OrderBy(x => x.PostedDate).ToList());
            Utils.ConfigGrid(uGridCpExpenseList2, ConstDatabase.CPExpenseList_TableName);
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["ContractCode"].Hidden = false;
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["ContractCode"].Header.VisiblePosition = 1;
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCpExpenseList2, "Amount", false);
        }
        private void ViewGridPanel3(List<CPExpenseList> _LstCPExpense2)
        {
            uGridCpExpenseList3.DataSource = new BindingList<CPExpenseList>(_LstCPExpense2.OrderBy(x => x.PostedDate).ToList());
            Utils.ConfigGrid(uGridCpExpenseList3, ConstDatabase.CPExpenseList_TableName);
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["ContractCode"].Hidden = false;
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["ContractCode"].Header.VisiblePosition = 1;
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCpExpenseList3, "Amount", false);
        }
        private void ViewGridPanel4()
        {
            #region edit by cuongpv chinh sua cach lay phan bo

            List<CPAllocationGeneralExpense> lstAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
            List<CPAllocationGeneralExpenseDetail> lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();

            //lay tat ca cac chi phi da luu trong CPAllocationGeneralExpense
            List<CPAllocationGeneralExpense> listCPAllocationGeneralExpenseDB = _ICPAllocationGeneralExpenseService.GetList();
            List<Guid> lstReferenceIDDB = (from g in listCPAllocationGeneralExpenseDB select g.ReferenceID).ToList();

            //lay cac chi phi chua phan bo het trong bang CPAllocationGeneralExpense
            List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpenseUnallocated = (from g in listCPAllocationGeneralExpenseDB
                                                                                         group g by new { g.ReferenceID }
                                                    into t
                                                                                         select new CPAllocationGeneralExpense()
                                                                                         {
                                                                                             ReferenceID = (Guid)t.Key.ReferenceID,
                                                                                             TotalCost = t.Max(x => x.TotalCost),
                                                                                             AllocatedRate = t.Sum(x => x.AllocatedRate),
                                                                                             AllocatedAmount = t.Sum(x => x.AllocatedAmount)
                                                                                         }).ToList();

            lstCPAllocationGeneralExpenseUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.AllocatedAmount < x.TotalCost).ToList();

            List<Guid> lstReferenceIDUnallocated = (from g in lstCPAllocationGeneralExpenseUnallocated select g.ReferenceID).ToList();

            if (IsAdd)
            {
                #region lay CP ky truoc
                //lay chi phi cac ky truoc tu GL
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBefore = Utils.IGeneralLedgerService.GetCPAllocationGeneralExpenseOnGLBefore(fromDate);

                //lay chi phi chua phan bo cua cac ky truoc
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBeforeNotUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseBeforeNotUnallocated = (from g in lstAllocationGeneralExpenseBefore where !lstReferenceIDDB.Any(x => x == g.ReferenceID) select g).ToList();
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseBeforeNotUnallocated);

                //lay cac chi phi chua phan bo het cua cac ky truoc
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBeforeUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseBeforeUnallocated = (from g in lstAllocationGeneralExpenseBefore where lstReferenceIDUnallocated.Any(x => x == g.ReferenceID) select g).ToList();
                var itemNotUnallocated = new CPAllocationGeneralExpense();
                foreach (var itemBeforeUnallocated in lstAllocationGeneralExpenseBeforeUnallocated)
                {
                    itemNotUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.ReferenceID == itemBeforeUnallocated.ReferenceID).FirstOrDefault();
                    if (!itemNotUnallocated.IsNullOrEmpty())
                    {
                        itemBeforeUnallocated.UnallocatedAmount = itemNotUnallocated.TotalCost - itemNotUnallocated.AllocatedAmount;
                    }
                }
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseBeforeUnallocated);
                #endregion

                #region lay CP ky nay
                //lay chi phi ky nay tu GL
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHere = Utils.IGeneralLedgerService.GetCPAllocationGeneralExpenseOnGL(fromDate, toDate);

                //lay chi phi chua phan bo cua ky nay
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHereNotUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseHereNotUnallocated = (from g in lstAllocationGeneralExpenseHere where !lstReferenceIDDB.Any(x => x == g.ReferenceID) select g).ToList();
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseHereNotUnallocated);

                //lay chi phi chua phan bo het cua ky nay
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHereUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseHereUnallocated = (from g in lstAllocationGeneralExpenseHere where lstReferenceIDUnallocated.Any(x => x == g.ReferenceID) select g).ToList();
                var itemHereNotUnallocated = new CPAllocationGeneralExpense();
                foreach (var itemBeforeUnallocated in lstAllocationGeneralExpenseHereUnallocated)
                {
                    itemHereNotUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.ReferenceID == itemBeforeUnallocated.ReferenceID).FirstOrDefault();
                    if (!itemHereNotUnallocated.IsNullOrEmpty())
                    {
                        itemBeforeUnallocated.UnallocatedAmount = itemHereNotUnallocated.TotalCost - itemHereNotUnallocated.AllocatedAmount;
                    }
                }
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseHereUnallocated);

                #endregion

                if (lstAllocationGeneralExpense.Count == 0)
                    lstAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
                else
                {
                    foreach (var item in lstAllocationGeneralExpense)
                    {
                        item.ExpenseItemCode = Utils.ListExpenseItem.Where(x => x.ID == item.ExpenseItemID).FirstOrDefault().ExpenseItemCode;
                        item.AllocatedRate = 100;
                        item.AllocatedAmount = item.UnallocatedAmount;
                    }
                }

                lstAllocationGeneralExpense = lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList();
            }
            else
            {
                lstAllocationGeneralExpense = _ICPAllocationGeneralExpenseService.GetAll().Where(x => x.CPPeriodID == _select.ID && x.AllocatedRate > 0).ToList();
                foreach (var item in lstAllocationGeneralExpense)
                {
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                    if (item.AllocationMethod == 0)
                        item.AllocationMethodView = "Nguyên vật liệu trực tiếp";
                    else if (item.AllocationMethod == 1)
                        item.AllocationMethodView = "Nhân công trực tiếp";
                    else if (item.AllocationMethod == 2)
                        item.AllocationMethodView = "Chi phí trực tiếp";
                    else if (item.AllocationMethod == 3)
                        item.AllocationMethodView = "Doanh số";

                    List<CPAllocationGeneralExpenseDetail> lst = item.CPAllocationGeneralExpenseDetails.ToList();
                    lstAllocationGeneralExpenseDetail.AddRange(lst);
                }
                foreach (var item1 in lstAllocationGeneralExpenseDetail)
                {
                    item1.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item1.ExpenseItemID).ExpenseItemCode;
                    item1.ContractCode = Utils.ListEmContract.FirstOrDefault(c => c.ID == item1.ContractID).Code;
                    item1.SignedDate = Utils.ListEmContract.FirstOrDefault(c => c.ID == item1.ContractID).SignedDate;
                    item1.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(c => c.ID == item1.ContractID).AccountingObjectName;
                }

                lstAllocationGeneralExpense = lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList();
                lstAllocationGeneralExpenseDetail = lstAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).ToList();
            }

            #endregion
            
            uGridAllocationGeneralExpense.DataSource = new BindingList<CPAllocationGeneralExpense>(lstAllocationGeneralExpense);
            Utils.ConfigGrid(uGridAllocationGeneralExpense, ConstDatabase.CPAllocationGeneralExpense_TableName);
            foreach (var col in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "AllocatedRate" || col.Key == "AllocationMethod" || col.Key == "AllocatedAmount") col.CellClickAction = CellClickAction.EditAndSelectText;
            }
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["TotalCost"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "TotalCost", false);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["UnallocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "UnallocatedAmount", false);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(ConstDatabase.Format_Rate);
            //Utils.AddSumColumn(uGridAllocationGeneralExpense, "AllocatedRate", false);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "AllocatedAmount", false);
            var cbbMethods = new UltraComboEditor();
            cbbMethods.Items.Add(0, "Nguyên vật liệu trực tiếp");
            cbbMethods.Items.Add(1, "Nhân công trực tiếp");
            cbbMethods.Items.Add(2, "Chi phí trực tiếp");
            cbbMethods.Items.Add(3, "Doanh số");
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethodView"].Hidden = true;
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].Hidden = false;
            foreach (var column in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key.Equals("AllocationMethod"))
                {
                    column.EditorComponent = cbbMethods;
                    column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                }
            }
            //
            uGridAllocationGeneralExpenseResult.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstAllocationGeneralExpenseDetail);
            Utils.ConfigGrid(uGridAllocationGeneralExpenseResult, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(ConstDatabase.Format_Rate2);
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpenseResult, "AllocatedAmount", false);
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = true;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["ContractCode"].Hidden = false;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["ContractCode"].Header.VisiblePosition = 1;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = false;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["SignedDate"].Header.VisiblePosition = 2;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = false;
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.VisiblePosition = 3;
            foreach (var col in uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "AllocatedRate" || col.Key == "AllocatedAmount") col.CellClickAction = CellClickAction.EditAndSelectText;
            }
        }
        private void ViewGridPanel5()
        {
            this.Text = "Nghiệm thu Hợp đồng";
            cbbAccountNo.DataSource = _dsAccount.Where(x => x.AccountNumber.StartsWith("632")).ToList();
            cbbAccountNo.DisplayMember = "AccountNumber";
            cbbAccountNo.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            cbbAccountNo.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            Utils.ConfigGrid(cbbAccountNo, ConstDatabase.Account_TableName);

            cbbAccountCo.DataSource = _dsAccount.Where(x => x.AccountNumber.StartsWith("154")).ToList();
            cbbAccountCo.DisplayMember = "AccountNumber";
            cbbAccountCo.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            cbbAccountCo.AutoSuggestFilterMode = AutoSuggestFilterMode.Contains;
            Utils.ConfigGrid(cbbAccountCo, ConstDatabase.Account_TableName);
            uGridNT.DataSource = new BindingList<CPAcceptanceDetail>();
            Utils.ConfigGrid(uGridNT, ConstDatabase.CPAcceptanceDetail_TableName);
            uGridNT.DisplayLayout.Bands[0].Columns["TotalAcceptedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridNT, "TotalAcceptedAmount", false);
            uGridNT.DisplayLayout.Bands[0].Columns["RevenueAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridNT, "RevenueAmount", false);
            uGridNT.DisplayLayout.Bands[0].Columns["AcceptedRate"].FormatNumberic(ConstDatabase.Format_Rate);
            uGridNT.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridNT, "Amount", false);
            uGridNT.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
            uGridNT.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = true;
            uGridNT.DisplayLayout.Bands[0].Columns["ContractCode"].Hidden = false;
            uGridNT.DisplayLayout.Bands[0].Columns["ContractCode"].Header.VisiblePosition = 1;
            uGridNT.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = false;
            uGridNT.DisplayLayout.Bands[0].Columns["SignedDate"].Header.VisiblePosition = 2;
            uGridNT.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = false;
            uGridNT.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.VisiblePosition = 3;
            uGridNT.DisplayLayout.Bands[0].Columns["SignedDate"].Format = "dd/MM/yyyy";
            if (IsAdd || IsNT || (_select != null && _select.CPAcceptances.Count == 0)) uGridNT.DisplayLayout.Bands[0].Columns["AcceptedRate"].CellClickAction = CellClickAction.EditAndSelectText;

        }
        #region Phân bổ
        private void btnAttribution_Click(object sender, EventArgs e)
        {
            List<CPAllocationGeneralExpenseDetail> lstpb = new List<CPAllocationGeneralExpenseDetail>();
            List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpenseUgrid = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList();//add by cuongpv
            foreach (var item in lstCPAllocationGeneralExpenseUgrid.Where(x => x.AllocatedRate > 0).ToList())
            {
                BindingList<CPExpenseList> lstExpenseListtt = (BindingList<CPExpenseList>)uGridCpExpenseList2.DataSource;
                if (item.AllocationMethod == 0)
                {
                    decimal total = 0;
                    var listID = Utils.ListExpenseItem.Where(x => x.ExpenseType == 0).ToList();
                    var listnvltt = lstExpenseListtt.Where(x => listID.Any(d => d.ID == x.ExpenseItemID)).ToList();
                    total = listnvltt.Sum(x => x.Amount);
                    var newlist = (from a in listnvltt
                                   group a by new { a.ContractID, a.ContractCode } into t
                                   select new
                                   {
                                       ContractID = t.Key.ContractID,
                                       ContractCode = t.Key.ContractCode,
                                       Amount = t.Sum(x => x.Amount),
                                   }).ToList();
                    if (newlist.Count > 0)
                    {
                        foreach (var model in newlist)
                        {
                            CPAllocationGeneralExpenseDetail md = new CPAllocationGeneralExpenseDetail();
                            md.CPAllocationGeneralExpenseID = item.ID;
                            md.ContractID = model.ContractID;
                            md.ContractCode = model.ContractCode;
                            md.SignedDate = Utils.ListEmContract.FirstOrDefault(x => x.ID == model.ContractID).SignedDate;
                            md.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(x => x.ID == model.ContractID).AccountingObjectName;
                            md.ExpenseItemID = item.ExpenseItemID;
                            md.ExpenseItemCode = item.ExpenseItemCode;
                            md.AllocatedRate = model.Amount * 100 / total;
                            md.AllocatedAmount = Math.Round((model.Amount / total * item.AllocatedAmount),lamtron, MidpointRounding.AwayFromZero);
                            lstpb.Add(md);
                        }
                    }
                    else
                    {
                        foreach (var item1 in listContractID)
                        {
                            CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                            cpg.ContractID = item1;
                            cpg.ContractCode = Utils.IEMContractService.Getbykey(item1).Code;
                            cpg.ExpenseItemCode = item.ExpenseItemCode;
                            cpg.AllocatedRate = 0;
                            cpg.AllocatedAmount = 0;
                            cpg.ExpenseItemID = item.ExpenseItemID;
                            cpg.CPAllocationGeneralExpenseID = item.ID;
                            lstpb.Add(cpg);
                        }
                    }
                }
                else if (item.AllocationMethod == 1)
                {
                    decimal total = 0;
                    var listID = Utils.ListExpenseItem.Where(x => x.ExpenseType == 1).ToList();
                    var listnvltt = lstExpenseListtt.Where(x => listID.Any(d => d.ID == x.ExpenseItemID)).ToList();
                    total = listnvltt.Sum(x => x.Amount);
                    var newlist = (from a in listnvltt
                                   group a by new { a.ContractID, a.ContractCode } into t
                                   select new
                                   {
                                       ContractID = t.Key.ContractID,
                                       ContractCode = t.Key.ContractCode,
                                       Amount = t.Sum(x => x.Amount),
                                   }).ToList();
                    if (newlist.Count > 0)
                    {
                        foreach (var model in newlist)
                        {
                            CPAllocationGeneralExpenseDetail md = new CPAllocationGeneralExpenseDetail();
                            md.CPAllocationGeneralExpenseID = item.ID;
                            md.ContractID = model.ContractID;
                            md.ContractCode = model.ContractCode;
                            md.SignedDate = Utils.ListEmContract.FirstOrDefault(x => x.ID == model.ContractID).SignedDate;
                            md.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(x => x.ID == model.ContractID).AccountingObjectName;
                            md.ExpenseItemID = item.ExpenseItemID;
                            md.ExpenseItemCode = item.ExpenseItemCode;
                            md.AllocatedRate = model.Amount * 100 / total;
                            md.AllocatedAmount = Math.Round((model.Amount / total * item.AllocatedAmount),lamtron, MidpointRounding.AwayFromZero);
                            lstpb.Add(md);
                        }
                    }
                    else
                    {
                        foreach (var item1 in listContractID)
                        {
                            CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                            cpg.ContractID = item1;
                            cpg.ContractCode = Utils.IEMContractService.Getbykey(item1).Code;
                            cpg.ExpenseItemCode = item.ExpenseItemCode;
                            cpg.AllocatedRate = 0;
                            cpg.AllocatedAmount = 0;
                            cpg.ExpenseItemID = item.ExpenseItemID;
                            cpg.CPAllocationGeneralExpenseID = item.ID;
                            lstpb.Add(cpg);
                        }
                    }
                }
                else if (item.AllocationMethod == 2)
                {
                    decimal total = 0;
                    var listID = Utils.ListExpenseItem.Where(x => x.ExpenseType == 0 || x.ExpenseType == 1).ToList();
                    var listnvltt = lstExpenseListtt.Where(x => listID.Any(d => d.ID == x.ExpenseItemID)).ToList();
                    var newlist = (from a in listnvltt
                                   group a by new { a.ContractID, a.ContractCode } into t
                                   select new
                                   {
                                       ContractID = t.Key.ContractID,
                                       ContractCode = t.Key.ContractCode,
                                       Amount = t.Sum(x => x.Amount),
                                   }).ToList();
                    total = listnvltt.Sum(x => x.Amount);
                    if (newlist.Count > 0)
                    {
                        foreach (var model in newlist)
                        {
                            CPAllocationGeneralExpenseDetail md = new CPAllocationGeneralExpenseDetail();
                            md.CPAllocationGeneralExpenseID = item.ID;
                            md.ContractID = model.ContractID;
                            md.ContractCode = model.ContractCode;
                            md.SignedDate = Utils.ListEmContract.FirstOrDefault(x => x.ID == model.ContractID).SignedDate;
                            md.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(x => x.ID == model.ContractID).AccountingObjectName;
                            md.ExpenseItemID = item.ExpenseItemID;
                            md.ExpenseItemCode = item.ExpenseItemCode;
                            md.AllocatedRate = model.Amount * 100 / total;
                            md.AllocatedAmount = Math.Round((model.Amount / total * item.AllocatedAmount),lamtron, MidpointRounding.AwayFromZero);
                            lstpb.Add(md);
                        }
                    }
                    else
                    {
                        foreach (var item1 in listContractID)
                        {
                            CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                            cpg.ContractID = item1;
                            cpg.ContractCode = Utils.IEMContractService.Getbykey(item1).Code;
                            cpg.ExpenseItemCode = item.ExpenseItemCode;
                            cpg.AllocatedRate = 0;
                            cpg.AllocatedAmount = 0;
                            cpg.ExpenseItemID = item.ExpenseItemID;
                            cpg.CPAllocationGeneralExpenseID = item.ID;
                            lstpb.Add(cpg);
                        }
                    }
                }
                else if (item.AllocationMethod == 3)
                {
                    var listCostSet = _LstContract;
                    decimal total = 0;
                    var listnvltt = new List<SAInvoiceDetail>();
                    listnvltt = _ISAInvoiceService.DoanhSoHD(listContractID, fromDate, toDate);
                    if (listnvltt.Count == 0)
                    {
                        MSG.Information("Chưa có dữ liệu doanh số để thực hiện tiêu thức này!");
                        foreach (var item1 in listContractID)
                        {
                            CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                            cpg.ContractID = item1;
                            cpg.ContractCode = Utils.IEMContractService.Getbykey(item1).Code;
                            cpg.ExpenseItemCode = item.ExpenseItemCode;
                            cpg.AllocatedRate = 0;
                            cpg.AllocatedAmount = 0;
                            cpg.ExpenseItemID = item.ExpenseItemID;
                            cpg.CPAllocationGeneralExpenseID = item.ID;
                            lstpb.Add(cpg);
                        }
                    }
                    else
                    {
                        total = listnvltt.Sum(x => x.Amount) - listnvltt.Sum(x => x.DiscountAmount) + listnvltt.Sum(x => x.VATAmount);
                        foreach (var model in listnvltt)
                        {
                            CPAllocationGeneralExpenseDetail md = new CPAllocationGeneralExpenseDetail();
                            md.CPAllocationGeneralExpenseID = item.ID;
                            md.ContractID = model.ContractID ?? Guid.Empty;
                            md.ContractCode = Utils.ListEmContract.FirstOrDefault(x => x.ID == (model.ContractID ?? Guid.Empty)).Code;
                            md.SignedDate = Utils.ListEmContract.FirstOrDefault(x => x.ID == (model.ContractID ?? Guid.Empty)).SignedDate;
                            md.AccountingObjectName = Utils.ListEmContract.FirstOrDefault(x => x.ID == (model.ContractID ?? Guid.Empty)).AccountingObjectName;
                            md.ExpenseItemID = item.ExpenseItemID;
                            md.ExpenseItemCode = item.ExpenseItemCode;
                            md.AllocatedRate = (model.Amount + model.VATAmount - model.DiscountAmount) * 100 / total;
                            md.AllocatedAmount = Math.Round(((model.Amount + model.VATAmount - model.DiscountAmount) / total * item.AllocatedAmount),lamtron, MidpointRounding.AwayFromZero);
                            lstpb.Add(md);

                        }
                    }
                }
            }
            uGridAllocationGeneralExpenseResult.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstpb);
        }
        #endregion 
        private void uGridAllocationGeneralExpense_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) <= 100)
                {
                    e.Cell.Row.Cells["AllocatedAmount"].Value = Math.Round((Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) / 100),lamtron, MidpointRounding.AwayFromZero);
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
                else
                {
                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    e.Cell.Row.Cells["AllocatedRate"].Value = Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * 100; ;
                }
            }
            if (e.Cell.Column.Key.Equals("AllocatedAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) != 100)
                {
                    e.Cell.Row.Cells["AllocatedRate"].Value = Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * 100;
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
                else { }
            }
        }
        private void uGridNT_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("AcceptedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AcceptedRate"].Value.ToString()) <= 100)
                {
                    e.Cell.Row.Cells["TotalAcceptedAmount"].Value = Math.Round((Decimal.Parse(e.Cell.Row.Cells["Amount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["AcceptedRate"].Value.ToString()) / 100),lamtron, MidpointRounding.AwayFromZero);
                }
                else
                {
                    MSG.Warning("Tỷ lệ nghiệm thu không được quá 100%");
                    e.Cell.Row.Cells["AcceptedRate"].Value = Decimal.Parse(e.Cell.Row.Cells["TotalAcceptedAmount"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["Amount"].Value.ToString()) * 100;
                }
            }
            else if (e.Cell.Column.Key.Equals("TotalAcceptedAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAcceptedAmount"].Value.ToString()) <= Decimal.Parse(e.Cell.Row.Cells["Amount"].Value.ToString()))
                {
                    e.Cell.Row.Cells["AcceptedRate"].Value = Decimal.Parse(e.Cell.Row.Cells["TotalAcceptedAmount"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["Amount"].Value.ToString()) * 100;
                }
                else
                {
                    MSG.Warning("Số nghiệm thu không được quá số chưa nghiệm thu!");
                    e.Cell.Row.Cells["TotalAcceptedAmount"].Value = Decimal.Parse(e.Cell.Row.Cells["Amount"].Value.ToString());
                }
            }
        }
        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtBeginDate.Value != null && dtEndDate.Value != null)
            {
                txtCPPeriod.Text = "Kỳ tính giá thành từ ngày " + dtBeginDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến ngày " + dtEndDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dtbegin = dtBeginDate.DateTime;
                fromDate = new DateTime(dtbegin.Year, dtbegin.Month, dtbegin.Day, 0, 0, 0);
            }
        }

        private void dtEndDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtBeginDate.Value != null && dtEndDate.Value != null)
            {
                txtCPPeriod.Text = "Kỳ tính giá thành từ ngày " + dtBeginDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến ngày " + dtEndDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dtend = dtEndDate.DateTime;
                toDate = new DateTime(dtend.Year, dtend.Month, dtend.Day, 23, 59, 59);
            }
        }
        static object BuildConfig(int obj)
        {
            Dictionary<int, string> temp = new Dictionary<int, string>();
            switch (obj)
            {
                case 1:
                    {
                        temp.Add(0, "Đơn hàng");
                        temp.Add(1, "Công trình, vụ việc");
                        temp.Add(2, "Phân xưởng, phòng ban");
                        temp.Add(3, "Quy trình công nghệ sản xuất");
                        temp.Add(4, "Sản phẩm");
                        temp.Add(5, "Khác");
                    }
                    break;
            }
            return temp;
        }


        #endregion

        #region check DTTHCP đã tính kỳ giá thành hiện tại chưa
        private bool CheckCostSet(DateTime fromdate, DateTime todate, Guid costsetID)
        {
            List<CPPeriod> lstCPPeriod = Utils.ListCPPeriod.Where(x => !(x.FromDate > todate || x.ToDate < fromdate) && x.Type == 5).ToList();
            List<CPPeriodDetail> lst = new List<CPPeriodDetail>();
            foreach (var item in lstCPPeriod)
            {
                List<CPPeriodDetail> lstCPPeriodDetail = item.CPPeriodDetails.Where(x => x.ContractID == costsetID).ToList();
                lst.AddRange(lstCPPeriodDetail);
            }
            if (lst.Count > 0)
                return false;
            else
                return true;
        }


        #endregion

        private void cbbAccountNo_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbAccountNo.Text != b.AccountNumber && cbbAccountNo.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbAccountNo.Focus();
                return;
            }
        }

        private void cbbAccountCo_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            Account b = new Account();
            if (cbbAccountCo.Text != b.AccountNumber && cbbAccountCo.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục!");
                cbbAccountCo.Focus();
                return;
            }
        }

        private void FCPCostOfContractDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
