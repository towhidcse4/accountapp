﻿namespace Accounting
{
    partial class FCPCostOfContractDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            this.btnBack = new Infragistics.Win.Misc.UltraButton();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.btnEscape = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.btnNghiemThu = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridCostset = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Panel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCpExpenseList3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Panel3 = new Infragistics.Win.Misc.UltraPanel();
            this.Panel4 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.btnAttribution = new Infragistics.Win.Misc.UltraButton();
            this.uGridAllocationGeneralExpenseResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridAllocationGeneralExpense = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.Panel2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridCpExpenseList2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.Panel5 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridNT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountCo = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbAccountNo = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCPPeriod5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCostset)).BeginInit();
            this.Panel1.ClientArea.SuspendLayout();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList3)).BeginInit();
            this.Panel3.ClientArea.SuspendLayout();
            this.Panel3.SuspendLayout();
            this.Panel4.ClientArea.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpenseResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod4)).BeginInit();
            this.Panel2.ClientArea.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod2)).BeginInit();
            this.Panel5.ClientArea.SuspendLayout();
            this.Panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridNT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountCo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod5)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.ubtnBack;
            this.btnBack.Appearance = appearance1;
            this.btnBack.Location = new System.Drawing.Point(342, 525);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 30);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "Quay lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Right;
            this.btnNext.Appearance = appearance2;
            this.btnNext.Location = new System.Drawing.Point(446, 13);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(98, 30);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Tiếp theo";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnEscape.Appearance = appearance3;
            this.btnEscape.Location = new System.Drawing.Point(758, 14);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(98, 30);
            this.btnEscape.TabIndex = 4;
            this.btnEscape.Text = "Hủy bỏ";
            this.btnEscape.Click += new System.EventHandler(this.btnEscape_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance4;
            this.btnSave.Location = new System.Drawing.Point(550, 525);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraButton1);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnNghiemThu);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnNext);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnEscape);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 511);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(865, 56);
            this.ultraPanel1.TabIndex = 5;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.Image = global::Accounting.Properties.Resources.cancel_16;
            this.ultraButton1.Appearance = appearance5;
            this.ultraButton1.Location = new System.Drawing.Point(758, 14);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(98, 30);
            this.ultraButton1.TabIndex = 6;
            this.ultraButton1.Text = "Đóng";
            this.ultraButton1.Visible = false;
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // btnNghiemThu
            // 
            this.btnNghiemThu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnNghiemThu.Appearance = appearance6;
            this.btnNghiemThu.Location = new System.Drawing.Point(654, 14);
            this.btnNghiemThu.Name = "btnNghiemThu";
            this.btnNghiemThu.Size = new System.Drawing.Size(98, 30);
            this.btnNghiemThu.TabIndex = 5;
            this.btnNghiemThu.Text = "Nghiệm Thu";
            this.btnNghiemThu.Click += new System.EventHandler(this.btnNghiemThu_Click);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.txtCPPeriod);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox2.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox2.Controls.Add(this.dtEndDate);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance15.FontData.BoldAsString = "True";
            appearance15.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance15;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(297, 118);
            this.ultraGroupBox2.TabIndex = 47;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel3
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance7;
            this.ultraLabel3.Location = new System.Drawing.Point(9, 14);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(27, 17);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Kỳ";
            // 
            // txtCPPeriod
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.txtCPPeriod.Appearance = appearance8;
            this.txtCPPeriod.AutoSize = false;
            this.txtCPPeriod.Location = new System.Drawing.Point(147, 52);
            this.txtCPPeriod.Name = "txtCPPeriod";
            this.txtCPPeriod.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod.TabIndex = 64;
            // 
            // ultraLabel5
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance9;
            this.ultraLabel5.Location = new System.Drawing.Point(8, 52);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel5.TabIndex = 63;
            this.ultraLabel5.Text = "Tên kỳ tính giá thành";
            // 
            // ultraLabel6
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance10;
            this.ultraLabel6.Location = new System.Drawing.Point(8, 85);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel6.TabIndex = 62;
            this.ultraLabel6.Text = "Hợp đồng";
            // 
            // ultraLabel7
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Bottom";
            this.ultraLabel7.Appearance = appearance11;
            this.ultraLabel7.Location = new System.Drawing.Point(306, 14);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(48, 17);
            this.ultraLabel7.TabIndex = 57;
            this.ultraLabel7.Text = "Từ ngày";
            // 
            // ultraLabel8
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Bottom";
            this.ultraLabel8.Appearance = appearance12;
            this.ultraLabel8.Location = new System.Drawing.Point(496, 15);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(57, 17);
            this.ultraLabel8.TabIndex = 58;
            this.ultraLabel8.Text = "Đến ngày";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(41, 10);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(240, 22);
            this.cbbDateTime.TabIndex = 59;
            // 
            // dtBeginDate
            // 
            appearance13.TextHAlignAsString = "Center";
            appearance13.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance13;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(374, 10);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(99, 21);
            this.dtBeginDate.TabIndex = 61;
            this.dtBeginDate.Value = null;
            this.dtBeginDate.ValueChanged += new System.EventHandler(this.dtBeginDate_ValueChanged);
            // 
            // dtEndDate
            // 
            appearance14.TextHAlignAsString = "Center";
            appearance14.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance14;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(571, 11);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(99, 21);
            this.dtEndDate.TabIndex = 60;
            this.dtEndDate.Value = null;
            this.dtEndDate.ValueChanged += new System.EventHandler(this.dtEndDate_ValueChanged);
            // 
            // uGridCostset
            // 
            this.uGridCostset.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCostset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCostset.Location = new System.Drawing.Point(0, 118);
            this.uGridCostset.Name = "uGridCostset";
            this.uGridCostset.Size = new System.Drawing.Size(297, 111);
            this.uGridCostset.TabIndex = 48;
            this.uGridCostset.Text = "ultraGrid2";
            // 
            // Panel1
            // 
            // 
            // Panel1.ClientArea
            // 
            this.Panel1.ClientArea.Controls.Add(this.uGridCostset);
            this.Panel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(297, 229);
            this.Panel1.TabIndex = 6;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.txtCPPeriod3);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            appearance18.FontData.BoldAsString = "True";
            appearance18.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance18;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(261, 62);
            this.ultraGroupBox3.TabIndex = 47;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod3
            // 
            appearance16.TextVAlignAsString = "Middle";
            this.txtCPPeriod3.Appearance = appearance16;
            this.txtCPPeriod3.AutoSize = false;
            this.txtCPPeriod3.Location = new System.Drawing.Point(163, 20);
            this.txtCPPeriod3.Name = "txtCPPeriod3";
            this.txtCPPeriod3.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod3.TabIndex = 64;
            // 
            // ultraLabel1
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance17;
            this.ultraLabel1.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel1.TabIndex = 63;
            this.ultraLabel1.Text = "Tên kỳ tính giá thành";
            // 
            // uGridCpExpenseList3
            // 
            this.uGridCpExpenseList3.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCpExpenseList3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCpExpenseList3.Location = new System.Drawing.Point(0, 62);
            this.uGridCpExpenseList3.Name = "uGridCpExpenseList3";
            this.uGridCpExpenseList3.Size = new System.Drawing.Size(261, 167);
            this.uGridCpExpenseList3.TabIndex = 48;
            this.uGridCpExpenseList3.Text = "ultraGrid3";
            // 
            // Panel3
            // 
            // 
            // Panel3.ClientArea
            // 
            this.Panel3.ClientArea.Controls.Add(this.uGridCpExpenseList3);
            this.Panel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.Panel3.Location = new System.Drawing.Point(604, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(261, 229);
            this.Panel3.TabIndex = 8;
            // 
            // Panel4
            // 
            // 
            // Panel4.ClientArea
            // 
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox5);
            this.Panel4.ClientArea.Controls.Add(this.uGridAllocationGeneralExpenseResult);
            this.Panel4.ClientArea.Controls.Add(this.uGridAllocationGeneralExpense);
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.Panel4.Location = new System.Drawing.Point(0, 247);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(281, 258);
            this.Panel4.TabIndex = 9;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox5.Controls.Add(this.btnAttribution);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 48);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(281, 35);
            this.ultraGroupBox5.TabIndex = 53;
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel9
            // 
            appearance19.BackColor = System.Drawing.Color.Transparent;
            appearance19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Bottom";
            this.ultraLabel9.Appearance = appearance19;
            this.ultraLabel9.Location = new System.Drawing.Point(6, 8);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel9.TabIndex = 57;
            this.ultraLabel9.Text = "Kết quả phân bổ";
            // 
            // btnAttribution
            // 
            this.btnAttribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAttribution.Location = new System.Drawing.Point(201, 6);
            this.btnAttribution.Name = "btnAttribution";
            this.btnAttribution.Size = new System.Drawing.Size(75, 23);
            this.btnAttribution.TabIndex = 10;
            this.btnAttribution.Text = "Phân bổ";
            this.btnAttribution.Click += new System.EventHandler(this.btnAttribution_Click);
            // 
            // uGridAllocationGeneralExpenseResult
            // 
            this.uGridAllocationGeneralExpenseResult.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocationGeneralExpenseResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGridAllocationGeneralExpenseResult.Location = new System.Drawing.Point(0, 83);
            this.uGridAllocationGeneralExpenseResult.Name = "uGridAllocationGeneralExpenseResult";
            this.uGridAllocationGeneralExpenseResult.Size = new System.Drawing.Size(281, 175);
            this.uGridAllocationGeneralExpenseResult.TabIndex = 54;
            this.uGridAllocationGeneralExpenseResult.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridAllocationGeneralExpenseResult.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpenseResult_AfterCellUpdate);
            this.uGridAllocationGeneralExpenseResult.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpenseResult_CellChange);
            // 
            // uGridAllocationGeneralExpense
            // 
            this.uGridAllocationGeneralExpense.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocationGeneralExpense.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridAllocationGeneralExpense.Location = new System.Drawing.Point(0, 60);
            this.uGridAllocationGeneralExpense.Name = "uGridAllocationGeneralExpense";
            this.uGridAllocationGeneralExpense.Size = new System.Drawing.Size(281, 235);
            this.uGridAllocationGeneralExpense.TabIndex = 48;
            this.uGridAllocationGeneralExpense.Text = "ultraGrid4";
            this.uGridAllocationGeneralExpense.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpense_AfterCellUpdate);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox4.Controls.Add(this.txtCPPeriod4);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            appearance23.FontData.BoldAsString = "True";
            appearance23.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance23;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(281, 60);
            this.ultraGroupBox4.TabIndex = 47;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel2
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance20;
            this.ultraLabel2.Location = new System.Drawing.Point(24, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel2.TabIndex = 65;
            this.ultraLabel2.Text = "Chi phí phân bổ";
            // 
            // txtCPPeriod4
            // 
            appearance21.TextVAlignAsString = "Middle";
            this.txtCPPeriod4.Appearance = appearance21;
            this.txtCPPeriod4.AutoSize = false;
            this.txtCPPeriod4.Location = new System.Drawing.Point(163, 12);
            this.txtCPPeriod4.Name = "txtCPPeriod4";
            this.txtCPPeriod4.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod4.TabIndex = 64;
            // 
            // ultraLabel4
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance22;
            this.ultraLabel4.Location = new System.Drawing.Point(24, 12);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel4.TabIndex = 63;
            this.ultraLabel4.Text = "Tên kỳ tính giá thành";
            // 
            // Panel2
            // 
            // 
            // Panel2.ClientArea
            // 
            this.Panel2.ClientArea.Controls.Add(this.uGridCpExpenseList2);
            this.Panel2.ClientArea.Controls.Add(this.ultraGroupBox6);
            this.Panel2.Location = new System.Drawing.Point(320, 0);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(261, 229);
            this.Panel2.TabIndex = 10;
            // 
            // uGridCpExpenseList2
            // 
            this.uGridCpExpenseList2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCpExpenseList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCpExpenseList2.Location = new System.Drawing.Point(0, 62);
            this.uGridCpExpenseList2.Name = "uGridCpExpenseList2";
            this.uGridCpExpenseList2.Size = new System.Drawing.Size(261, 167);
            this.uGridCpExpenseList2.TabIndex = 48;
            this.uGridCpExpenseList2.Text = "ultraGrid5";
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.txtCPPeriod2);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            appearance26.FontData.BoldAsString = "True";
            appearance26.FontData.SizeInPoints = 10F;
            this.ultraGroupBox6.HeaderAppearance = appearance26;
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(261, 62);
            this.ultraGroupBox6.TabIndex = 47;
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod2
            // 
            appearance24.TextVAlignAsString = "Middle";
            this.txtCPPeriod2.Appearance = appearance24;
            this.txtCPPeriod2.AutoSize = false;
            this.txtCPPeriod2.Location = new System.Drawing.Point(163, 20);
            this.txtCPPeriod2.Name = "txtCPPeriod2";
            this.txtCPPeriod2.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod2.TabIndex = 64;
            // 
            // ultraLabel10
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextHAlignAsString = "Left";
            appearance25.TextVAlignAsString = "Bottom";
            this.ultraLabel10.Appearance = appearance25;
            this.ultraLabel10.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel10.TabIndex = 63;
            this.ultraLabel10.Text = "Tên kỳ tính giá thành";
            // 
            // Panel5
            // 
            // 
            // Panel5.ClientArea
            // 
            this.Panel5.ClientArea.Controls.Add(this.uGridNT);
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox7);
            this.Panel5.Location = new System.Drawing.Point(320, 258);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(545, 229);
            this.Panel5.TabIndex = 11;
            // 
            // uGridNT
            // 
            this.uGridNT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridNT.Location = new System.Drawing.Point(0, 79);
            this.uGridNT.Name = "uGridNT";
            this.uGridNT.Size = new System.Drawing.Size(545, 150);
            this.uGridNT.TabIndex = 48;
            this.uGridNT.Text = "ultraGrid6";
            this.uGridNT.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridNT_AfterCellUpdate);
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Controls.Add(this.cbbAccountCo);
            this.ultraGroupBox7.Controls.Add(this.label3);
            this.ultraGroupBox7.Controls.Add(this.cbbAccountNo);
            this.ultraGroupBox7.Controls.Add(this.label2);
            this.ultraGroupBox7.Controls.Add(this.txtCPPeriod5);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            appearance53.FontData.BoldAsString = "True";
            appearance53.FontData.SizeInPoints = 10F;
            this.ultraGroupBox7.HeaderAppearance = appearance53;
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(545, 79);
            this.ultraGroupBox7.TabIndex = 47;
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountCo
            // 
            this.cbbAccountCo.AutoSize = false;
            this.cbbAccountCo.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountCo.DisplayLayout.Appearance = appearance27;
            this.cbbAccountCo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountCo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountCo.DisplayLayout.GroupByBox.Appearance = appearance28;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountCo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.cbbAccountCo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance30.BackColor2 = System.Drawing.SystemColors.Control;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountCo.DisplayLayout.GroupByBox.PromptAppearance = appearance30;
            this.cbbAccountCo.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountCo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountCo.DisplayLayout.Override.ActiveCellAppearance = appearance31;
            appearance32.BackColor = System.Drawing.SystemColors.Highlight;
            appearance32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountCo.DisplayLayout.Override.ActiveRowAppearance = appearance32;
            this.cbbAccountCo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountCo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountCo.DisplayLayout.Override.CardAreaAppearance = appearance33;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            appearance34.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountCo.DisplayLayout.Override.CellAppearance = appearance34;
            this.cbbAccountCo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountCo.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountCo.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance36.TextHAlignAsString = "Left";
            this.cbbAccountCo.DisplayLayout.Override.HeaderAppearance = appearance36;
            this.cbbAccountCo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountCo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountCo.DisplayLayout.Override.RowAppearance = appearance37;
            this.cbbAccountCo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountCo.DisplayLayout.Override.TemplateAddRowAppearance = appearance38;
            this.cbbAccountCo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountCo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountCo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountCo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon;
            this.cbbAccountCo.Location = new System.Drawing.Point(330, 48);
            this.cbbAccountCo.Name = "cbbAccountCo";
            this.cbbAccountCo.Size = new System.Drawing.Size(84, 22);
            this.cbbAccountCo.TabIndex = 76;
            this.cbbAccountCo.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbAccountCo_ItemNotInList);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(284, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 75;
            this.label3.Text = "TK Có";
            // 
            // cbbAccountNo
            // 
            this.cbbAccountNo.AutoSize = false;
            this.cbbAccountNo.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountNo.DisplayLayout.Appearance = appearance39;
            this.cbbAccountNo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountNo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountNo.DisplayLayout.GroupByBox.Appearance = appearance40;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountNo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
            this.cbbAccountNo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountNo.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.cbbAccountNo.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountNo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountNo.DisplayLayout.Override.ActiveCellAppearance = appearance43;
            appearance44.BackColor = System.Drawing.SystemColors.Highlight;
            appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountNo.DisplayLayout.Override.ActiveRowAppearance = appearance44;
            this.cbbAccountNo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountNo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountNo.DisplayLayout.Override.CardAreaAppearance = appearance45;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountNo.DisplayLayout.Override.CellAppearance = appearance46;
            this.cbbAccountNo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountNo.DisplayLayout.Override.CellPadding = 0;
            appearance47.BackColor = System.Drawing.SystemColors.Control;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountNo.DisplayLayout.Override.GroupByRowAppearance = appearance47;
            appearance48.TextHAlignAsString = "Left";
            this.cbbAccountNo.DisplayLayout.Override.HeaderAppearance = appearance48;
            this.cbbAccountNo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountNo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountNo.DisplayLayout.Override.RowAppearance = appearance49;
            this.cbbAccountNo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountNo.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.cbbAccountNo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountNo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountNo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon;
            this.cbbAccountNo.Location = new System.Drawing.Point(181, 48);
            this.cbbAccountNo.Name = "cbbAccountNo";
            this.cbbAccountNo.Size = new System.Drawing.Size(84, 22);
            this.cbbAccountNo.TabIndex = 74;
            this.cbbAccountNo.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbAccountNo_ItemNotInList);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(21, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 13);
            this.label2.TabIndex = 73;
            this.label2.Text = "Hạch toán nghiệm thu: TK Nợ";
            // 
            // txtCPPeriod5
            // 
            appearance51.TextVAlignAsString = "Middle";
            this.txtCPPeriod5.Appearance = appearance51;
            this.txtCPPeriod5.AutoSize = false;
            this.txtCPPeriod5.Location = new System.Drawing.Point(163, 20);
            this.txtCPPeriod5.Name = "txtCPPeriod5";
            this.txtCPPeriod5.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod5.TabIndex = 64;
            // 
            // ultraLabel11
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            appearance52.TextHAlignAsString = "Left";
            appearance52.TextVAlignAsString = "Bottom";
            this.ultraLabel11.Appearance = appearance52;
            this.ultraLabel11.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel11.TabIndex = 63;
            this.ultraLabel11.Text = "Tên kỳ tính giá thành";
            // 
            // FCPCostOfContractDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 567);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FCPCostOfContractDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xác định kỳ giá thành";
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCostset)).EndInit();
            this.Panel1.ClientArea.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList3)).EndInit();
            this.Panel3.ClientArea.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            this.Panel4.ClientArea.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpenseResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod4)).EndInit();
            this.Panel2.ClientArea.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod2)).EndInit();
            this.Panel5.ClientArea.ResumeLayout(false);
            this.Panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridNT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            this.ultraGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountCo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnBack;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton btnEscape;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCostset;
        private Infragistics.Win.Misc.UltraPanel Panel1;
        private Infragistics.Win.Misc.UltraPanel Panel4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocationGeneralExpense;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCpExpenseList3;
        private Infragistics.Win.Misc.UltraPanel Panel3;
        private Infragistics.Win.Misc.UltraPanel Panel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCpExpenseList2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraButton btnNghiemThu;
        private Infragistics.Win.Misc.UltraPanel Panel5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridNT;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraButton btnAttribution;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocationGeneralExpenseResult;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountCo;
        private System.Windows.Forms.Label label3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountNo;
        private System.Windows.Forms.Label label2;
    }
}