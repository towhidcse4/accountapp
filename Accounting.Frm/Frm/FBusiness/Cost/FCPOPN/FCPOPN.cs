﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FCPOPN : DialogForm //UserControl
    {
        #region khai báo
        private readonly ICPOPNService _ICPOPNService;
        CPOPN _Select = new CPOPN();
        BindingList<CPOPN> bdlCPOPN = new BindingList<CPOPN>(new List<CPOPN>());
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FCPOPN()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion            
            #region Thiết lập ban đầu cho Form
            _ICPOPNService = IoC.Resolve<ICPOPNService>();
            cbbObjectType.SelectedIndex = 0;
            LoadDuLieu();
            #endregion           
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            BindingList<CPOPN> bdlCPOPN = new BindingList<CPOPN>(GetData().OrderBy(x => x.CostSetCode).ThenBy(d=>d.Code).ToList());

            #region load
            uGrid.DataSource = bdlCPOPN;
            CreaterColumsStyle(uGrid);            
            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        List<CPOPN> GetData()
        {
            var listCostset1 = Utils.ListCostSet.ToList().Where(x=>x.CostSetType == 2);
            var listCostset0 = Utils.ListCostSet.ToList().Where(x => x.CostSetType == 4 );
            var listCostset2 = Utils.ListCostSet.ToList().Where(x => x.CostSetType == 1);
            var listCostset3 = Utils.ListCostSet.ToList().Where(x => x.CostSetType == 0);
            var listContractSale = Utils.ListEmContract.Where(x=>x.IsWatchForCostPrice && x.TypeID == 860).ToList();
            var listAllocationQuantum = Utils.ListCPOPN.ToList();
            var lst1 = (from a in listCostset1
                        select new CPOPN()
                        {
                            CostSetID = a.ID,
                            CostSets = a,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0,
                            AcceptedAmount = 0,
                            NotAcceptedAmount = 0,
                            UncompletedAccount = "154"
                        }).ToList();
            var lst0 = (from a in listCostset0
                        select new CPOPN()
                        {
                            CostSetID = a.ID,
                            CostSets = a,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0,
                            AcceptedAmount = 0,
                            NotAcceptedAmount = 0,
                            UncompletedAccount = "154"
                        }).ToList();
            var lst3 = (from a in listCostset3
                        select new CPOPN()
                        {
                            CostSetID = a.ID,
                            CostSets = a,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0,
                            AcceptedAmount = 0,
                            NotAcceptedAmount = 0,
                            UncompletedAccount = "154"
                        }).ToList();
            var lst2 = (from a in listCostset2
                        select new CPOPN()
                        {
                            CostSetID = a.ID,
                            CostSets = a,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0,
                            AcceptedAmount = 0,
                            NotAcceptedAmount = 0,
                            UncompletedAccount = "154"
                        }).ToList();
            var lst4 = (from a in listContractSale
                        select new CPOPN()
                        {
                            ContractID = a.ID,
                            EMContractSales = a,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0,
                            AcceptedAmount = 0,
                            NotAcceptedAmount = 0,
                            UncompletedAccount = "154"
                        }).ToList();
            List<CPOPN> lst = new List<CPOPN>();
            switch ((int)cbbObjectType.SelectedIndex)
            {
                case 0:
                    lst.Clear();
                    lst.AddRange(lst0);
                    break;
                case 1:
                    lst.Clear();
                    lst.AddRange(lst1);
                    break;
                case 2:
                    lst.Clear();
                    lst.AddRange(lst2);
                    break;
                case 3:
                    lst.Clear();
                    lst.AddRange(lst3);
                    break;
                case 4:
                    lst.Clear();
                    lst.AddRange(lst4);
                    break;
            }                       
            foreach (var x in lst)
            {
                var a = new CPOPN();
                if (x.ContractID == null) a = listAllocationQuantum.FirstOrDefault(d => d.CostSetID == x.CostSetID);
                else a = listAllocationQuantum.FirstOrDefault(d => d.ContractID == x.ContractID);
                if (a != null)
                {
                    x.DirectMaterialAmount = a.DirectMaterialAmount;
                    x.DirectLaborAmount = a.DirectLaborAmount;
                    x.GeneralExpensesAmount = a.GeneralExpensesAmount;
                    x.TotalCostAmount = a.TotalCostAmount;
                    x.AcceptedAmount = a.AcceptedAmount;
                    x.NotAcceptedAmount = a.NotAcceptedAmount;
                }
            }
            return lst;
        }
        #endregion

        #region nghiệp vụ
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            var lstbd = (BindingList<CPOPN>)uGrid.DataSource;
            var lst = lstbd.ToList();
            _ICPOPNService.BeginTran();
            foreach (var x in lst)
            {
                if (!(x.ContractID == null ? Utils.ListCPOPN.Any(c => c.CostSetID == x.CostSetID) : Utils.ListCPOPN.Any(c => c.ContractID == x.ContractID)))
                {
                    CPOPN model = new CPOPN();
                    model.ID = Guid.NewGuid();
                    model.ContractID = x.ContractID;
                    model.CostSetID = x.CostSetID;
                    model.ObjectType = cbbObjectType.SelectedIndex;
                    model.DirectMaterialAmount = x.DirectMaterialAmount;
                    model.DirectLaborAmount = x.DirectLaborAmount;
                    model.GeneralExpensesAmount = x.GeneralExpensesAmount;
                    model.TotalCostAmount = x.TotalCostAmount;
                    model.AcceptedAmount = x.AcceptedAmount;
                    model.NotAcceptedAmount = x.NotAcceptedAmount;
                    model.UncompletedAccount = "154";
                    _ICPOPNService.CreateNew(model);
                }
                else
                {
                    CPOPN model = x.ContractID == null ? Utils.ListCPOPN.FirstOrDefault(c => c.CostSetID == x.CostSetID) : Utils.ListCPOPN.FirstOrDefault(c => c.ContractID == x.ContractID);
                    model.DirectLaborAmount = x.DirectLaborAmount;
                    model.DirectMaterialAmount = x.DirectMaterialAmount;
                    model.GeneralExpensesAmount = x.GeneralExpensesAmount;
                    model.TotalCostAmount = x.TotalCostAmount;
                    model.AcceptedAmount = x.AcceptedAmount;
                    model.NotAcceptedAmount = x.NotAcceptedAmount;
                    _ICPOPNService.Update(model);
                }
            }
            try
            {
                _ICPOPNService.CommitTran();
                Utils.ClearCacheByType<CPOPN>();
                MSG.Information("Lưu dữ liệu thành công!");
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                _ICPOPNService.RolbackTran();
            }

        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.CPOPNSP_TableName);
            switch ((int)cbbObjectType.SelectedIndex)
            {
                case 0:
                case 1:
                    uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = false;
                    uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = false;
                    //uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Header.VisiblePosition = 0;
                    //uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Header.VisiblePosition = 1;
                    uGrid.DisplayLayout.Bands[0].Columns["Code"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["AcceptedAmount"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["NotAcceptedAmount"].Hidden = true;
                    break;
                case 2:
                case 3:
                    uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = false;
                    uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = false;
                    //uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Header.VisiblePosition = 0;
                    //uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Header.VisiblePosition = 1;
                    uGrid.DisplayLayout.Bands[0].Columns["Code"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["AcceptedAmount"].Hidden = false;
                    uGrid.DisplayLayout.Bands[0].Columns["NotAcceptedAmount"].Hidden = false;
                    break;
                case 4:
                    uGrid.DisplayLayout.Bands[0].Columns["CostSetCode"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["CostSetName"].Hidden = true;
                    uGrid.DisplayLayout.Bands[0].Columns["Code"].Hidden = false;
                    uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Hidden = false;
                    uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Hidden = false;
                    //uGrid.DisplayLayout.Bands[0].Columns["Code"].Header.VisiblePosition = 0;
                    //uGrid.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.VisiblePosition = 2;
                    //uGrid.DisplayLayout.Bands[0].Columns["SignedDate"].Header.VisiblePosition = 1;
                    uGrid.DisplayLayout.Bands[0].Columns["AcceptedAmount"].Hidden = false;
                    uGrid.DisplayLayout.Bands[0].Columns["NotAcceptedAmount"].Hidden = false;
                    break;
            }
            uGrid.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["AcceptedAmount"].CellClickAction = CellClickAction.EditAndSelectText;
        }
        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("DirectMaterialAmount") || item.Key.Equals("DirectLaborAmount") || item.Key.Equals("GeneralExpensesAmount") || item.Key.Equals("TotalCostAmount") || item.Key.Equals("AcceptedAmount") || item.Key.Equals("NotAcceptedAmount") )
                {
                    item.FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
        }
        #endregion

        #region Event
        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("DirectMaterialAmount") || e.Cell.Column.Key.Equals("DirectLaborAmount") || e.Cell.Column.Key.Equals("GeneralExpensesAmount") || e.Cell.Column.Key.Equals("AcceptedAmount"))
            {
                UltraGrid ultraGrid = (UltraGrid)sender;
                ultraGrid.UpdateData();
                UltraGridRow row = ultraGrid.ActiveRow;
                if (row != null)
                {
                    row.Cells["TotalCostAmount"].Value = ((decimal)row.Cells["DirectLaborAmount"].Value + (decimal)row.Cells["DirectMaterialAmount"].Value + (decimal)row.Cells["GeneralExpensesAmount"].Value);
                    row.Cells["NotAcceptedAmount"].Value = ((decimal)row.Cells["TotalCostAmount"].Value - (decimal)row.Cells["AcceptedAmount"].Value);
                }
            }
        }
        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
            var cell = uGrid.ActiveCell;
            if (cell != null)
            {
                if ((e.Cell.Column.Key.Equals("DirectMaterialAmount") || e.Cell.Column.Key.Equals("DirectLaborAmount") || e.Cell.Column.Key.Equals("GeneralExpensesAmount") || e.Cell.Column.Key.Equals("AcceptedAmount")) && (cell.Value == null || cell.Text == "")) cell.Value = (decimal)0;
            }
        }
        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
        }
        private void cbbObjectType_ValueChanged(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        #endregion

        private void FCPOPN_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
