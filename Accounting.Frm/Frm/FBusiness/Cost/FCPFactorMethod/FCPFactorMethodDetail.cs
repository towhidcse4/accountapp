﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using System.ComponentModel;
using Infragistics.Win.UltraWinEditors;
using System.Globalization;
using Accounting.Core.DAO;

namespace Accounting
{
    public partial class FCPFactorMethodDetail : CustormForm
    {
        #region Khai báo

        private int indexCurrent = 0;
        List<Control> lstControl = new List<Control>();
        List<string> text = new List<string>();
        public static bool isClose = true;
        bool IsAdd = true;
        public List<SelectCostSet> _LstCostSet;
        List<CPExpenseList> _LstCPExpense;
        List<CPExpenseList> _LstCPExpense2;
        List<Guid> _ListCostSet;
        List<CPUncompleteDetail> _LstCPUncompleteDetail;
        List<CPUncomplete> _LstCPUncomplete;
        List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail;
        List<RSInwardOutward> _LstRSInwardOutward;
        List<ExpenseItem> _LstExpenseItem;
        List<CPOPN> _LstCPOPN;
        List<CPAllocationGeneralExpense> lstAllocationGeneralExpense;
        List<CPAllocationGeneralExpenseDetail> lstAllocationGeneralExpenseDetail;
        List<CPAllocationRate> lstAllocationRate;
        List<CPAllocationRate> lstAllocationRate1;
        CPAllocationRate cpar;
        List<CPResult> _LstCPResult;
        CPPeriod _select = new CPPeriod();
        CPPeriod cpp = new CPPeriod();
        List<SelectCostSet> listcostset;
        int checkItem;
        int checkAtt = 1;
        int checkDtr = 1;
        int checkupdate = 0;
        int save = 0;
        DateTime fromDate;
        DateTime toDate;
        int lamtron = int.Parse(Utils.ListSystemOption.FirstOrDefault(c => c.Code == "DDSo_TienVND").Data);
        private ICPPeriodService ICPPeriodService
        {
            get { return IoC.Resolve<ICPPeriodService>(); }
        }
        private ICPPeriodDetailService ICPPeriodDetailService
        {
            get { return IoC.Resolve<ICPPeriodDetailService>(); }
        }
        private ICPExpenseListService ICPExpenseListService
        {
            get { return IoC.Resolve<ICPExpenseListService>(); }
        }
        private ICPAllocationGeneralExpenseService ICPAllocationGeneralExpenseService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseService>(); }
        }
        private ICPAllocationGeneralExpenseDetailService ICPAllocationGeneralExpenseDetailService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseDetailService>(); }
        }
        private ICPUncompleteService ICPUncompleteService
        {
            get { return IoC.Resolve<ICPUncompleteService>(); }
        }
        private ICPUncompleteDetailService ICPUncompleteDetailService
        {
            get { return IoC.Resolve<ICPUncompleteDetailService>(); }
        }
        private ICPAllocationRateService ICPAllocationRateService
        {
            get { return IoC.Resolve<ICPAllocationRateService>(); }
        }
        private ICPResultService ICPResultService
        {
            get { return IoC.Resolve<ICPResultService>(); }
        }
        private ICostSetMaterialGoodsService ICostSetMaterialGoodsService
        {
            get { return IoC.Resolve<ICostSetMaterialGoodsService>(); }
        }
        #endregion

        #region Khởi tạo
        public FCPFactorMethodDetail()
        {
            InitializeComponent();
            LoadChung();
            ConfigButton();
            ShowPal();
        }
        public FCPFactorMethodDetail(CPPeriod temp)
        {
            InitializeComponent();
            IsAdd = false;
            _select = temp;
            fromDate = temp.FromDate;
            toDate = temp.ToDate;
            txtCPPeriod.Text = _select.Name;
            txtCPPeriod2.ReadOnly = true;
            txtCPPeriod3.ReadOnly = true;
            txtCPPeriod4.ReadOnly = true;
            txtCPPeriod8.ReadOnly = true;
            txtCPPeriod6.ReadOnly = true;
            txtCPPeriod7.ReadOnly = true;
            dtBeginDate.ReadOnly = true;
            dtEndDate.ReadOnly = true;
            cbbDateTime.ReadOnly = true;
            LoadChung();
            ConfigButton();
            ShowPal();
        }
        #endregion

        #region Hàm xử lý nghiệp vụ
        void LoadChung()
        {
            text = new List<string> { "Xác định kỳ tính giá thành", "Tập hợp chi phí trực tiếp", "Tập hợp các khoản giảm giá thành", "Phân bổ chi phí chung", "Đánh giá dở dang cuối kỳ", "Xác định hệ số phân bổ", "Bảng kết quả tính giá thành" };
            lstControl = new List<Control> { Panel1, Panel2, Panel3, Panel4, Panel5, Panel6, Panel7 };
            ViewGridPanel1();
        }
        #endregion

        #region Hàm xử lý Grid
        private void ConfigButton()
        {
            if (indexCurrent == 0)
            {//d?u tiên
                btnBack.Enabled = false;
                btnNext.Enabled = true;
                btnSave.Enabled = false;
                btnUpdateImport.Visible = false;
                btnUpdateExport.Visible = false;
            }
            else if (indexCurrent == lstControl.Count - 1)
            {
                btnBack.Enabled = true;
                btnNext.Enabled = false;
                //btnSave.Enabled = true;comment by cuongpv
                btnUpdateImport.Visible = true;
                btnUpdateExport.Visible = true;
                if (IsAdd)
                {
                    btnSave.Enabled = true;//add by cuongpv
                    btnUpdateImport.Enabled = false;
                    btnUpdateExport.Enabled = false;
                }
                else
                {
                    //add by cuongpv
                    if (checkupdate == 1)
                    {
                        btnSave.Enabled = true;
                        btnUpdateImport.Enabled = false;
                        btnUpdateExport.Enabled = false;
                    }
                    else
                    {
                        btnSave.Enabled = false;
                        btnUpdateImport.Enabled = true;
                        btnUpdateExport.Enabled = true;
                    }
                    //end add by cuongpv
                }
            }
            else
            {
                btnBack.Enabled = true;
                btnNext.Enabled = true;
                btnSave.Enabled = false;
                btnUpdateImport.Visible = false;
                btnUpdateExport.Visible = false;
            }
        }
        private void ShowPal()
        {
            this.Text = text[indexCurrent];
            foreach (Control control in lstControl) control.Visible = false;
            Panel6.Visible = false;
            lstControl[indexCurrent].Visible = true;
            lstControl[indexCurrent].Dock = DockStyle.Fill;
        }
        #endregion

        #region Events
        private void btnBack_Click(object sender, EventArgs e)
        {
            indexCurrent -= 1;
            ConfigButton();
            ShowPal();
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (indexCurrent == 0)
            {
                _LstCostSet = new List<SelectCostSet>();
                _ListCostSet = new List<Guid>();
                if (IsAdd)
                {
                    for (int i = 0; i < uGridCostset.Rows.Count; i++)
                    {
                        if (uGridCostset.Rows[i].Cells["Select"].Value == null)
                        {
                            uGridCostset.Rows[i].Cells["Select"].Value = false;
                        }
                        if (bool.Parse(uGridCostset.Rows[i].Cells["Select"].Value.ToString()) == true)
                        {
                            SelectCostSet cs = uGridCostset.Rows[i].ListObject as SelectCostSet;
                            _LstCostSet.Add(cs);
                            _ListCostSet.Add(cs.ID);
                            string costsetname = Utils.ICostSetService.Getbykey(cs.ID).CostSetName;
                            if (!CheckCostSet(fromDate, toDate, cs.ID))
                            {
                                MSG.Error("Đối tượng tập hợp chi phí được chọn đã có trong kỳ tính giá thành khác trùng khoảng thời gian với kỳ tính giá thành này. Vui lòng chọn lại");
                                return;
                            }
                        }
                    }
                    if (_ListCostSet.Count == 0)
                    {
                        MSG.Error("Bạn chưa chọn đối tượng tính giá thành!");
                        return;
                    }
                }
                else
                {
                    for (int i = 0; i < uGridCostset.Rows.Count; i++)
                    {
                        SelectCostSet cs = uGridCostset.Rows[i].ListObject as SelectCostSet;
                        _LstCostSet.Add(cs);
                        _ListCostSet.Add(cs.ID);
                    }
                }

                txtCPPeriod2.Text = txtCPPeriod.Text;
                ViewGridPanel2();
            }
            else if (indexCurrent == 1)
            {
                txtCPPeriod3.Text = txtCPPeriod.Text;
                ViewGridPanel3();
            }
            else if (indexCurrent == 2)
            {
                WaitingFrm.StartWaiting();
                txtCPPeriod4.Text = txtCPPeriod.Text;
                ViewGridPanel4();
                WaitingFrm.StopWaiting();
            }
            else if (indexCurrent == 3)
            {
                if (lstAllocationGeneralExpense.Sum(x => x.AllocatedRate) == 0)
                {
                    lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();
                }
                else if (lstAllocationGeneralExpenseDetail.Count == 0)
                {
                    MSG.Warning("Chi phí chung chưa được phân bổ ");
                    return;
                }
                if (checkAtt == 0)
                {
                    MSG.Warning("Chưa phân bổ lại chi phí với số liệu vừa thay đổi");
                    return;
                }
                WaitingFrm.StartWaiting();
                if (IsAdd)
                {
                    optType.CheckedIndex = 0;
                }
                else
                {
                    List<CPUncomplete> lstUncomplete = /*ICPUncompleteService.GetList().Where(x => x.CPPeriodID == _select.ID).ToList()*/_select.CPUncompletes.ToList();
                    List<CPUncompleteDetail> lstUncompleteDetail = new List<CPUncompleteDetail>();
                    if (lstUncomplete.Count > 0)
                    {
                        foreach (var item in lstUncomplete)
                        {
                            lstUncompleteDetail.AddRange(item.CPUncompleteDetails);
                        }
                    }
                    if (lstUncompleteDetail.Count > 0)
                        optType.CheckedIndex = lstUncompleteDetail.FirstOrDefault().UncompleteType;
                }
                txtCPPeriod8.Text = txtCPPeriod.Text;
                //ViewGridPanel5();comment by cuongpv chuyen code vao OptType_valueChanged
                //add by cuongpv
                if (IsAdd)
                    btnUnDetermined_Click(sender, e);
                WaitingFrm.StopWaiting();
                //end add by cuongpv
            }
            else if (indexCurrent == 4)
            {
                //if (checkDtr == 0)
                //{
                //    MSG.Warning("Chưa đánh giá dở dang với số liệu vừa thay đổi");
                //    return;
                //}
                txtCPPeriod6.Text = txtCPPeriod.Text;
                ViewGridPanel6();
            }
            else if (indexCurrent == 5)
            {
                Decimal countStan = 0;
                Guid cs = new Guid();
                if (lstAllocationRate1 != null)
                {
                    foreach (var z in lstAllocationRate1)
                    {
                        if (cs != z.CostSetID)
                        {
                            countStan++;
                            cs = z.CostSetID;
                        }
                    }
                }
                Decimal stan = 0;
                foreach (var x in uGridAllocatedRate.Rows)
                {
                    if ((bool)x.Cells["IsStandardItem"].Value)
                        stan++;
                }
                if (stan < countStan)
                {
                    MSG.Warning("Chưa lựa chọn thành phẩm chuẩn để xác định tỷ lệ phân bổ giá thành !");
                    return;
                }
                else
                {
                    txtCPPeriod7.Text = txtCPPeriod.Text;
                    //edit by tungnt: gán lại dữ liệu cho list CPAllocationRate để fill số lượng TP
                    lstAllocationRate = ((BindingList<CPAllocationRate>)uGridAllocatedRate.DataSource).ToList();
                    ViewGridPanel7();
                }
            }

            indexCurrent += 1;
            ConfigButton();
            ShowPal();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            Utils.ICPPeriodService.BeginTran();
            try
            {
                if (IsAdd)
                {
                    if (fromDate > toDate)
                    {
                        MSG.Error("Ngày bắt đầu phải nhỏ hơn ngày kết thúc!");
                        return;
                    }
                    WaitingFrm.StartWaiting();
                    CPPeriod lstPeriod = new CPPeriod
                    {
                        ID = Guid.NewGuid(),
                        Type = 1,
                        FromDate = fromDate,
                        ToDate = toDate,
                        Name = txtCPPeriod.Text,
                        IsDelete = true
                    };

                    foreach (var cs in _LstCostSet)
                    {
                        CPPeriodDetail detail = new CPPeriodDetail();
                        detail.ID = Guid.NewGuid();
                        detail.CPPeriodID = lstPeriod.ID;
                        detail.CostSetID = cs.ID;
                        lstPeriod.CPPeriodDetails.Add(detail);
                    }

                    lstPeriod.CPExpenseLists = ((BindingList<CPExpenseList>)uGridCpExpenseList2.DataSource).ToList();
                    foreach (var cpe in lstPeriod.CPExpenseLists)
                    {
                        cpe.CPPeriodID = lstPeriod.ID;
                    }

                    foreach (var cpe in ((BindingList<CPExpenseList>)uGridCpExpenseList3.DataSource).ToList())
                    {
                        cpe.CPPeriodID = lstPeriod.ID;
                        lstPeriod.CPExpenseLists.Add(cpe);
                    }

                    //List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ICPAllocationGeneralExpenseService.GetAll();
                    //List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses2 = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                    //foreach (var item in cPAllocationGeneralExpenses2)
                    //{
                    //    foreach(var item1 in cPAllocationGeneralExpenses)
                    //    {
                    //        if(item.ID == item1.ID)
                    //        {
                    //            ICPAllocationGeneralExpenseService.Delete(item1);
                    //            ICPAllocationGeneralExpenseService.CommitChanges();
                    //        }
                    //    }
                    //}

                    lstPeriod.CPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                    foreach (var cpa in lstPeriod.CPAllocationGeneralExpenses)
                    {
                        cpa.CPPeriodID = lstPeriod.ID;
                        cpa.CPAllocationGeneralExpenseDetails = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).ToList().Where(c => c.CPAllocationGeneralExpenseID == cpa.ID && c.AllocatedAmount > 0).ToList();
                    }

                    lstPeriod.CPUncompletes = ((BindingList<CPUncomplete>)uGridUncomplete.DataSource).ToList();
                    foreach (var cpu in lstPeriod.CPUncompletes)
                    {
                        cpu.CPPeriodID = lstPeriod.ID;
                        cpu.CPUncompleteDetails = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cpu.CostSetID).ToList();
                        foreach (var cpud in cpu.CPUncompleteDetails)
                        {
                            cpud.CPUncompleteID = cpu.ID;
                            cpud.UncompleteType = checkItem;
                        }
                    }

                    lstPeriod.CPAllocationRates = ((BindingList<CPAllocationRate>)uGridAllocatedRate.DataSource).ToList();
                    foreach (var cpa in lstPeriod.CPAllocationRates)
                    {
                        cpa.CPPeriodID = lstPeriod.ID;
                        cpa.CPPeriodDetailID = lstPeriod.CPPeriodDetails.FirstOrDefault(x => x.CostSetID == cpa.CostSetID).ID;
                    }

                    lstPeriod.CPResults = ((BindingList<CPResult>)ugridCostResult.DataSource).ToList();
                    foreach (var cpr in lstPeriod.CPResults)
                    {
                        cpr.CPPeriodID = lstPeriod.ID;
                        cpr.CPPeriodDetailID = lstPeriod.CPPeriodDetails.FirstOrDefault(x => x.CostSetID == cpr.CostSetID).ID;
                    }
                    #region comment by cuongpv ko dung de lay luy ke phan bo theo cach nay nua
                    //List<CPAllocationGeneralExpense> listAllocationGeneralExpense2 = lstPeriod.CPAllocationGeneralExpenses.Where(x => x.AllocatedRate > 0).Where(x => x.AllocatedRate < 100).ToList();
                    //foreach (var item in listAllocationGeneralExpense2)
                    //{
                    //    CPAllocationGeneralExpense cpe = new CPAllocationGeneralExpense();
                    //    cpe.ID = Guid.NewGuid();
                    //    cpe.CPPeriodID = item.CPPeriodID;
                    //    cpe.ExpenseItemID = item.ExpenseItemID;
                    //    cpe.TotalCost = item.TotalCost;
                    //    cpe.UnallocatedAmount = item.UnallocatedAmount - item.AllocatedAmount;
                    //    cpe.AllocatedRate = 0;
                    //    cpe.AllocatedAmount = 0;
                    //    cpe.ReferenceID = item.ReferenceID;
                    //    Utils.ICPAllocationGeneralExpenseService.CreateNew(cpe);
                    //}
                    #endregion
                    Utils.ICPPeriodService.CreateNew(lstPeriod);
                    Utils.ICPPeriodService.CommitTran();
                    WaitingFrm.StopWaiting();
                    MSG.Information("Lưu dữ liệu thành công");
                    save = 1;
                    btnNext.Enabled = false;
                    //cpp = lstPeriod;
                    //Cập nhật lại dở dang
                    //foreach (var item in _ListCostSet)
                    //{
                    //    List<CPUncomplete> lstUncomplete = ICPUncompleteService.GetAll().Where(x => x.CostSetID == item).ToList();
                    //    List<CPPeriod> cPPeriods = ICPPeriodService.GetAll().Where(x => x.Type == 1 && x.FromDate > lstPeriod.ToDate).ToList();
                    //    if (cPPeriods.Count > 0)
                    //    {
                    //        List<CPUncomplete> lstUncomplete1 = (from g in lstUncomplete
                    //                                             join h in cPPeriods on g.CPPeriodID equals h.ID
                    //                                             orderby h.FromDate
                    //                                             select g).ToList();
                    //        foreach (var item1 in lstUncomplete1)
                    //        {
                    //            GetLoopCPUncomplete(item1);
                    //        }
                    //    }
                    //}

                }
                else
                {
                    WaitingFrm.StartWaiting();
                    if (_select != null)
                    {
                        _select.CPAllocationGeneralExpenses.Clear();
                        List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ICPAllocationGeneralExpenseService.GetAll().Where(x => x.CPPeriodID == _select.ID && x.AllocatedRate > 0).ToList();
                        if (cPAllocationGeneralExpenses.Count > 0)
                        {
                            List<CPAllocationGeneralExpense> lst = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).Where(c => c.CPPeriodID == _select.ID && c.AllocatedRate > 0).ToList();
                            if (lst.Count > 0)
                                foreach (var item in cPAllocationGeneralExpenses)
                                {
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense = lst.FirstOrDefault(x => x.ID == item.ID);
                                    if (cPAllocationGeneralExpense != null)
                                    {
                                        item.AllocatedRate = cPAllocationGeneralExpense.AllocatedRate;
                                        item.AllocatedAmount = cPAllocationGeneralExpense.AllocatedAmount;
                                        item.UnallocatedAmount = cPAllocationGeneralExpense.UnallocatedAmount;
                                        item.AllocationMethod = cPAllocationGeneralExpense.AllocationMethod;
                                        ICPAllocationGeneralExpenseService.Update(item);
                                        _select.CPAllocationGeneralExpenses.Add(item);
                                    }

                                    item.CPAllocationGeneralExpenseDetails.Clear();
                                    List<CPAllocationGeneralExpenseDetail> cPAllocationGeneralExpenseDetails = ICPAllocationGeneralExpenseDetailService.GetAll().Where(x => x.CPAllocationGeneralExpenseID == item.ID && x.AllocatedRate > 0).ToList();
                                    List<CPAllocationGeneralExpenseDetail> lst1 = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).Where(c => c.CPAllocationGeneralExpenseID == item.ID && c.AllocatedRate > 0).ToList();
                                    if (cPAllocationGeneralExpenseDetails.Count > 0)
                                    {
                                        foreach (var item1 in cPAllocationGeneralExpenseDetails)
                                        {
                                            CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = lst1.FirstOrDefault(x => x.ID == item1.ID);
                                            CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail1 = lst1.FirstOrDefault(x => x.CPAllocationGeneralExpenseID == item1.CPAllocationGeneralExpenseID && x.CostSetID == item1.CostSetID);
                                            if (cPAllocationGeneralExpenseDetail != null)
                                            {
                                                item1.AllocatedRate = cPAllocationGeneralExpenseDetail.AllocatedRate;
                                                item1.AllocatedAmount = cPAllocationGeneralExpenseDetail.AllocatedAmount;
                                                ICPAllocationGeneralExpenseDetailService.Update(item1);
                                                item.CPAllocationGeneralExpenseDetails.Add(item1);
                                            }
                                            else
                                            {
                                                if (cPAllocationGeneralExpenseDetail1 != null)
                                                {
                                                    ICPAllocationGeneralExpenseDetailService.Delete(item1);
                                                    ICPAllocationGeneralExpenseDetailService.CreateNew(cPAllocationGeneralExpenseDetail1);
                                                    item.CPAllocationGeneralExpenseDetails.Add(cPAllocationGeneralExpenseDetail1);
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        else
                        {
                            _select.CPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).ToList();
                            foreach (var cpa in _select.CPAllocationGeneralExpenses)
                            {
                                cpa.CPPeriodID = _select.ID;
                                cpa.CPAllocationGeneralExpenseDetails = ((BindingList<CPAllocationGeneralExpenseDetail>)uGridAllocationGeneralExpenseResult.DataSource).Where(c => c.CPAllocationGeneralExpenseID == cpa.ID && c.AllocatedAmount > 0).ToList();
                            }
                        }

                        _select.CPUncompletes.Clear();
                        bool update = true;
                        List<CPUncomplete> cPUncompletes = ICPUncompleteService.Query.Where(x => x.CPPeriodID == _select.ID).ToList();
                        if (cPUncompletes.Count > 0)
                        {
                            List<CPUncomplete> lst = ((BindingList<CPUncomplete>)uGridUncomplete.DataSource).ToList();
                            if (lst.Count > 0)
                                foreach (var item in cPUncompletes)
                                {
                                    CPUncomplete cPUncomplete = lst.FirstOrDefault(x => x.ID == item.ID);
                                    CPUncomplete cPUncomplete1 = lst.FirstOrDefault(x => x.CostSetID == item.CostSetID);
                                    if (cPUncomplete != null)
                                    {
                                        item.CPUncompleteDetails.Clear();
                                        List<CPUncompleteDetail> cPUncompleteDetails = ICPUncompleteDetailService.GetAll().Where(x => x.CPUncompleteID == item.ID).ToList();
                                        List<CPUncompleteDetail> lst1 = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == item.CostSetID).ToList();
                                        if (cPUncompleteDetails.Count > 0)
                                        {
                                            foreach (var item1 in cPUncompleteDetails)
                                            {
                                                CPUncompleteDetail cPUncompleteDetail = lst1.FirstOrDefault(x => x.ID == item1.ID);
                                                CPUncompleteDetail cPUncompleteDetail1 = lst1.FirstOrDefault(x => x.MaterialGoodsID == item1.MaterialGoodsID);
                                                item1.Quantity = cPUncompleteDetail.Quantity;
                                                item1.PercentComplete = cPUncompleteDetail.PercentComplete;
                                                item1.UnitPrice = cPUncompleteDetail.UnitPrice;
                                                item1.UncompleteType = optType.CheckedIndex;
                                                ICPUncompleteDetailService.Update(item1);
                                                item.CPUncompleteDetails.Add(item1);
                                            }
                                        }

                                        item.DirectMaterialAmount = Math.Round(cPUncomplete.DirectMaterialAmount, lamtron, MidpointRounding.AwayFromZero);
                                        item.DirectLaborAmount = Math.Round(cPUncomplete.DirectLaborAmount, lamtron, MidpointRounding.AwayFromZero);
                                        item.GeneralExpensesAmount = Math.Round(cPUncomplete.GeneralExpensesAmount, lamtron, MidpointRounding.AwayFromZero);
                                        item.TotalCostAmount = Math.Round(cPUncomplete.TotalCostAmount, lamtron, MidpointRounding.AwayFromZero);
                                        update = true;
                                    }
                                    else
                                    {
                                        cPUncomplete1.CPPeriodID = item.CPPeriodID;

                                        cPUncomplete1.CPUncompleteDetails.Clear();
                                        item.CPUncompleteDetails.Clear();
                                        List<CPUncompleteDetail> cPUncompleteDetails = ICPUncompleteDetailService.GetAll().Where(x => x.CPUncompleteID == item.ID).ToList();
                                        List<CPUncompleteDetail> lst1 = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cPUncomplete1.CostSetID).ToList();
                                        if (cPUncompleteDetails.Count > 0)
                                        {
                                            foreach (var item1 in cPUncompleteDetails)
                                            {
                                                CPUncompleteDetail cPUncompleteDetail = lst1.FirstOrDefault(x => x.ID == item1.ID);
                                                CPUncompleteDetail cPUncompleteDetail1 = lst1.FirstOrDefault(x => x.MaterialGoodsID == item1.MaterialGoodsID);
                                                if (cPUncompleteDetail != null)
                                                {
                                                    item.DirectMaterialAmount = Math.Round(cPUncomplete1.DirectMaterialAmount, lamtron, MidpointRounding.AwayFromZero);
                                                    item.DirectLaborAmount = Math.Round(cPUncomplete1.DirectLaborAmount, lamtron, MidpointRounding.AwayFromZero);
                                                    item.GeneralExpensesAmount = Math.Round(cPUncomplete1.GeneralExpensesAmount, lamtron, MidpointRounding.AwayFromZero);
                                                    item.TotalCostAmount = Math.Round(cPUncomplete1.TotalCostAmount, lamtron, MidpointRounding.AwayFromZero);//add by cuongpv
                                                    item.CPUncompleteDetails.Add(cPUncompleteDetail);
                                                    update = true;
                                                }
                                                else
                                                {
                                                    ICPUncompleteService.CreateNew(cPUncomplete1);
                                                    cPUncompleteDetail1.CPUncompleteID = cPUncomplete1.ID;
                                                    cPUncompleteDetail1.UncompleteType = optType.CheckedIndex;
                                                    ICPUncompleteDetailService.Delete(item1);
                                                    ICPUncompleteDetailService.CreateNew(cPUncompleteDetail1);
                                                    cPUncomplete1.CPUncompleteDetails.Add(cPUncompleteDetail1);
                                                    update = false;
                                                }
                                            }
                                        }

                                    }
                                    if (update)
                                    {
                                        if (item != null)
                                        {
                                            ICPUncompleteService.Update(item);
                                            _select.CPUncompletes.Add(item);
                                        }
                                    }
                                    else
                                    {
                                        if (item != null)
                                        {
                                            ICPUncompleteService.Delete(item);
                                            _select.CPUncompletes.Add(cPUncomplete1);
                                        }
                                    }
                                }
                        }
                        else
                        {
                            _select.CPUncompletes = ((BindingList<CPUncomplete>)uGridUncomplete.DataSource).ToList();
                            foreach (var cpu in _select.CPUncompletes)
                            {
                                cpu.CPPeriodID = _select.ID;
                                cpu.CPUncompleteDetails = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cpu.CostSetID).ToList();
                                foreach (var cpud in cpu.CPUncompleteDetails)
                                {
                                    cpud.CPUncompleteID = cpu.ID;
                                    cpud.UncompleteType = checkItem;
                                }
                            }
                        }

                        _select.Name = txtCPPeriod.Text;
                        _select.CPAllocationRates = ((BindingList<CPAllocationRate>)uGridAllocatedRate.DataSource).ToList();

                        foreach (var item in _select.CPResults)
                        {
                            ICPResultService.Delete(item);
                        }

                        _select.CPResults.Clear();
                        _select.CPResults = ((BindingList<CPResult>)ugridCostResult.DataSource).ToList();
                        foreach (var cpr in _select.CPResults)
                        {
                            cpr.CPPeriodID = _select.ID;
                            cpr.CPPeriodDetailID = _select.CPPeriodDetails.FirstOrDefault(x => x.CostSetID == cpr.CostSetID).ID;
                            //if (((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Count != 0)
                            //    cpr.MaterialGoodsID = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(c => c.CostSetID == cpr.CostSetID).First().MaterialGoodsID;
                            //else
                            //    cpr.MaterialGoodsID = cpr.MaterialGoodsID;
                        }
                    }
                    Utils.ICPPeriodService.Update(_select);
                    Utils.ICPPeriodService.CommitTran();
                    WaitingFrm.StopWaiting();
                    MSG.Information("Cập nhật dữ liệu thành công");
                    save = 1;

                    // Cập nhật dở dang 
                    //foreach (var item in _ListCostSet)
                    //{
                    //    List<CPUncomplete> lstUncomplete = ICPUncompleteService.GetAll().Where(x => x.CostSetID == item).ToList();
                    //    List<CPPeriod> cPPeriods = ICPPeriodService.GetAll().Where(x => x.Type == 1 && x.FromDate > _select.ToDate).ToList();
                    //    if (cPPeriods.Count > 0)
                    //    {
                    //        List<CPUncomplete> lstUncomplete1 = (from g in lstUncomplete
                    //                                             join h in cPPeriods on g.CPPeriodID equals h.ID
                    //                                             orderby h.FromDate
                    //                                             select g).ToList();
                    //        foreach (var item1 in lstUncomplete1)
                    //        {
                    //            GetLoopCPUncomplete(item1);
                    //        }
                    //    }
                    //}
                }

                btnSave.Enabled = false;
                btnBack.Enabled = false;
                btnUpdateImport.Enabled = true;
                btnUpdateExport.Enabled = true;

                //add by cuongpv
                Utils.ClearCacheByType<CPExpenseList>();
                Utils.ClearCacheByType<CPPeriod>();
                Utils.ClearCacheByType<CPPeriodDetail>();
                Utils.ClearCacheByType<CPAllocationGeneralExpense>();
                Utils.ClearCacheByType<CPAllocationGeneralExpenseDetail>();
                Utils.ClearCacheByType<CPAllocationRate>();
                Utils.ClearCacheByType<CPResult>();
                
                Utils.ICPExpenseListService.UnbindSession(Utils.ListCPExpenseList);
                Utils.ICPPeriodService.UnbindSession(Utils.ListCPPeriod);
                Utils.ICPPeriodDetailService.UnbindSession(Utils.ListCPPeriodDetail);
                Utils.ICPAllocationGeneralExpenseService.UnbindSession(Utils.ListCPAllocationGeneralExpense);
                Utils.ICPAllocationGeneralExpenseDetailService.UnbindSession(Utils.ListCPAllocationGeneralExpenseDetail);
                Utils.ICPAllocationRateService.UnbindSession(Utils.ListCPAllocationRate);
                Utils.ICPResultService.UnbindSession(Utils.ListCPResult);
                //end add by cuongpv
            }
            catch (Exception ex)
            {
                Utils.ICPPeriodService.RolbackTran();
                WaitingFrm.StopWaiting();
            }
        }

        private void btnUpdateImport_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            List<Guid> lstMaterialGoods = _LstCPResult.Select(x => x.MaterialGoodsID).ToList();
            Utils.IRSInwardOutwardDetailService.BeginTran();
            try
            {

                List<RSInwardOutward> _LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.TypeID == 400 && x.Recorded == true).ToList();
                List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.ToList().Where(g => _ListCostSet.Any(x => x == g.CostSetID) && g.DebitAccount.StartsWith("155") && g.CreditAccount.StartsWith("154")
                                              && g.PostedDate >= fromDate && g.PostedDate <= toDate && lstMaterialGoods.Any(d => d == g.MaterialGoodsID) && _LstRSInwardOutward1.Any(d => d.ID == g.RSInwardOutwardID)).ToList();
                List<Guid> rsIDs = new List<Guid>();
                foreach (var item in _LstCPResult)
                {
                    List<RSInwardOutwardDetail> lstrsid = _LstRSInwardOutwardDetail1.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
                    if (lstrsid.Count > 0)
                    {
                        foreach (var rsid in lstrsid)
                        {
                            rsid.UnitPrice = item.UnitPrice;
                            rsid.UnitPriceOriginal = item.UnitPrice;
                            rsid.Amount = (rsid.UnitPrice * (decimal)rsid.Quantity);
                            rsid.AmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                            Utils.IRSInwardOutwardDetailService.Update(rsid);
                            if (!rsIDs.Any(x => x == (Guid)rsid.RSInwardOutwardID))
                                rsIDs.Add((Guid)rsid.RSInwardOutwardID);

                            // lưu gl
                            List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList().Where(x => x.DetailID == rsid.ID).ToList();
                            if (generalLedgers.Count > 0)
                            {
                                foreach (var gl in generalLedgers)
                                {
                                    if (gl.Account.StartsWith("154"))
                                    {
                                        gl.CreditAmount = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                        gl.CreditAmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                        gl.DebitAmount = 0;
                                        gl.DebitAmountOriginal = 0;
                                    }
                                    else if (gl.Account.StartsWith("155"))
                                    {
                                        gl.CreditAmount = 0;
                                        gl.CreditAmountOriginal = 0;
                                        gl.DebitAmount = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                        gl.DebitAmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                                    }
                                    Utils.IGeneralLedgerService.Update(gl);
                                }
                            }
                            //lưu rl
                            List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.DetailID == rsid.ID).ToList();
                            if (repositoryLedgers.Count > 0)
                            {
                                foreach (var rl in repositoryLedgers)
                                {
                                    rl.UnitPrice = item.UnitPrice;
                                    rl.IWAmount = (decimal)(rl.UnitPrice * rl.IWQuantity);
                                    rl.IWAmountBalance = (rl.UnitPrice * (decimal)rl.IWQuantityBalance);
                                    Utils.IRepositoryLedgerService.Update(rl);
                                }
                            }
                        }
                    }
                }
                foreach (var item in rsIDs)
                {
                    RSInwardOutward rSInwardOutward = _LstRSInwardOutward1.FirstOrDefault(x => x.ID == item);
                    if (rSInwardOutward != null)
                    {
                        rSInwardOutward.TotalAmount = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        rSInwardOutward.TotalAmountOriginal = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        Utils.IRSInwardOutwardService.Update(rSInwardOutward);
                    }
                }

                //if (cpp != null)
                //{
                //    if (!IsAdd)
                //        cpp = _select;
                //    cpp.IsDelete = false;
                //    Utils.ICPPeriodService.Update(cpp);
                //}
                Utils.IRSInwardOutwardDetailService.CommitTran();
                WaitingFrm.StopWaiting();
                MSG.Information("Cập nhật dữ liệu thành công");
                btnUpdateImport.Enabled = false;
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                Utils.IRSInwardOutwardDetailService.RolbackTran();
            }

        }

        private void btnUpdateExport_Click(object sender, EventArgs e)
        {
            WaitingFrm.StartWaiting();
            List<Guid> lstMaterialGoods = _LstCPResult.Select(x => x.MaterialGoodsID).ToList();
            Utils.IRSInwardOutwardDetailService.BeginTran();
            List<int> typeids = new List<int>() { 410, 411, 412, 415, 414, 320, 321, 322, 323, 324, 325, 420 };

            try
            {
                List<RSInwardOutward> _LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.Recorded == true && typeids.Any(c => c == x.TypeID) && x.PostedDate >= fromDate && x.PostedDate <= toDate).ToList();
                List<RSInwardOutwardDetail> _LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.Where(x => lstMaterialGoods.Any(d => d == x.MaterialGoodsID) && _LstRSInwardOutward1.Any(d => d.ID == x.RSInwardOutwardID)).ToList();
                List<SAInvoice> sAInvoices = Utils.ListSAInvoice.Where(x => x.PostedDate >= fromDate && x.PostedDate <= toDate && x.Recorded == true).ToList();
                List<SAInvoiceDetail> _LstSAInvoiceDetail = Utils.ListSAInvoiceDetail.ToList().Where(x => lstMaterialGoods.Any(d => d == x.MaterialGoodsID) && sAInvoices.Any(d => d.ID == x.SAInvoiceID)).ToList(); ;
                List<Guid> sainvoiceIDs = new List<Guid>();
                List<Guid> rsIDs = new List<Guid>();

                foreach (var item in _LstCPResult)
                {
                    //với chứng từ là xuất kho
                    List<RSInwardOutwardDetail> lstrsid = _LstRSInwardOutwardDetail1.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
                    if (lstrsid.Count > 0)
                    {
                        foreach (var rsid in lstrsid)
                        {
                            rsid.UnitPrice = item.UnitPrice;
                            rsid.UnitPriceOriginal = item.UnitPrice;
                            rsid.Amount = (rsid.UnitPrice * (decimal)rsid.Quantity);
                            rsid.AmountOriginal = (rsid.UnitPriceOriginal * (decimal)rsid.Quantity);
                            Utils.IRSInwardOutwardDetailService.Update(rsid);
                            if (!rsIDs.Any(x => x == (Guid)rsid.RSInwardOutwardID))
                                rsIDs.Add((Guid)rsid.RSInwardOutwardID);

                            // lưu gl
                            List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList().Where(x => x.DetailID == rsid.ID).ToList();
                            if (generalLedgers.Count > 0)
                            {
                                foreach (var gl in generalLedgers)
                                {
                                    if (gl.Account == rsid.CreditAccount)
                                    {
                                        gl.CreditAmount = (item.UnitPrice * (decimal)rsid.Quantity);
                                        gl.CreditAmountOriginal = (item.UnitPrice * (decimal)rsid.Quantity);
                                    }
                                    else if (gl.Account == rsid.DebitAccount)
                                    {
                                        gl.DebitAmount = (item.UnitPrice * (decimal)rsid.Quantity);
                                        gl.DebitAmountOriginal = (item.UnitPrice * (decimal)rsid.Quantity);
                                    }
                                    Utils.IGeneralLedgerService.Update(gl);
                                }
                            }
                            //lưu rl
                            List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.DetailID == rsid.ID).ToList();
                            if (repositoryLedgers.Count > 0)
                            {
                                foreach (var rl in repositoryLedgers)
                                {
                                    rl.UnitPrice = item.UnitPrice;
                                    rl.IWAmount = (decimal)(rl.UnitPrice * rl.IWQuantity);
                                    rl.IWAmountBalance = (rl.UnitPrice * (decimal)rl.IWQuantity);
                                    Utils.IRepositoryLedgerService.Update(rl);
                                }
                            }
                        }
                    }
                    //với chứng từ là xuất kho từ bán hàng
                    List<SAInvoiceDetail> lstsaid = _LstSAInvoiceDetail.Where(x => x.MaterialGoodsID == item.MaterialGoodsID).ToList();
                    if (lstsaid.Count > 0)
                    {
                        foreach (var said in lstsaid)
                        {
                            said.OWPrice = item.UnitPrice;
                            said.OWPriceOriginal = item.UnitPrice;
                            said.OWAmount = (said.OWPrice * (decimal)said.Quantity);
                            said.OWAmountOriginal = (said.OWPriceOriginal * (decimal)said.Quantity);
                            Utils.ISAInvoiceDetailService.Update(said);
                            if (!sainvoiceIDs.Any(x => x == (Guid)said.SAInvoiceID))
                                sainvoiceIDs.Add(said.SAInvoiceID);
                            // lưu gl
                            List<GeneralLedger> generalLedgers = Utils.ListGeneralLedger.ToList().Where(x => x.DetailID == said.ID).ToList();
                            if (generalLedgers.Count > 0)
                            {
                                foreach (var gl in generalLedgers)
                                {
                                    if (gl.AccountCorresponding == "632")
                                    {
                                        gl.CreditAmount = (item.UnitPrice * ((decimal)said.Quantity));
                                        gl.CreditAmountOriginal = (item.UnitPrice * (decimal)said.Quantity);
                                    }
                                    else if (gl.Account == "632")
                                    {
                                        gl.DebitAmount = (item.UnitPrice * (decimal)said.Quantity);
                                        gl.DebitAmountOriginal = (item.UnitPrice * (decimal)said.Quantity);
                                    }
                                    Utils.IGeneralLedgerService.Update(gl);
                                }
                            }
                            //lưu rl
                            List<RepositoryLedger> repositoryLedgers = Utils.ListRepositoryLedger.Where(x => x.DetailID == said.ID).ToList();
                            if (repositoryLedgers.Count > 0)
                            {
                                foreach (var rl in repositoryLedgers)
                                {
                                    rl.UnitPrice = item.UnitPrice;
                                    rl.IWAmount = (decimal)(rl.UnitPrice * rl.IWQuantity);
                                    rl.IWAmountBalance = (rl.UnitPrice * (decimal)rl.IWQuantity);
                                    Utils.IRepositoryLedgerService.Update(rl);
                                }
                            }
                        }

                    }
                }
                foreach (var item in rsIDs)
                {
                    RSInwardOutward rSInwardOutward = _LstRSInwardOutward1.FirstOrDefault(x => x.ID == item);
                    if (rSInwardOutward != null)
                    {
                        rSInwardOutward.TotalAmount = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        rSInwardOutward.TotalAmountOriginal = rSInwardOutward.RSInwardOutwardDetails.Sum(x => x.Amount);
                        Utils.IRSInwardOutwardService.Update(rSInwardOutward);
                    }
                }
                foreach (var item in sainvoiceIDs)
                {
                    if (item != null)
                    {
                        SAInvoice sAInvoice = sAInvoices.FirstOrDefault(x => x.ID == item);
                        if (sAInvoice != null)
                        {
                            sAInvoice.TotalCapitalAmount = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            sAInvoice.TotalCapitalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            Utils.ISAInvoiceService.Update(sAInvoice);
                        }
                        RSInwardOutward rSInwardOutward = (from g in _LstRSInwardOutward1 where g.ID == item select g).FirstOrDefault();
                        if (rSInwardOutward != null)
                        {
                            rSInwardOutward.TotalAmount = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            rSInwardOutward.TotalAmountOriginal = sAInvoice.SAInvoiceDetails.Sum(x => x.OWAmount);
                            Utils.IRSInwardOutwardService.Update(rSInwardOutward);
                        }
                    }
                }

                //if (cpp != null)
                //{
                //    if (!IsAdd)
                //        cpp = _select;
                //    cpp.IsDelete = false;
                //    Utils.ICPPeriodService.Update(cpp);
                //}
                Utils.IRSInwardOutwardDetailService.CommitTran();
                WaitingFrm.StopWaiting();
                MSG.Information("Cập nhật dữ liệu thành công");
                btnUpdateExport.Enabled = false;
            }
            catch (Exception ex)
            {
                WaitingFrm.StopWaiting();
                Utils.IRSInwardOutwardDetailService.RolbackTran();
            }

        }

        private void btnEscape_Click(object sender, EventArgs e)
        {
            if (save == 1)
                isClose = true;
            else
            {
                if (lstAllocationGeneralExpense != null)
                    ICPAllocationGeneralExpenseService.UnbindSession(lstAllocationGeneralExpense);
                if (lstAllocationGeneralExpenseDetail != null)
                    ICPAllocationGeneralExpenseDetailService.UnbindSession(lstAllocationGeneralExpenseDetail);
                if (_LstCPUncomplete != null)
                    ICPUncompleteService.UnbindSession(_LstCPUncomplete);
                if (_LstCPUncompleteDetail != null)
                    ICPUncompleteDetailService.UnbindSession(_LstCPUncompleteDetail);
                if (lstAllocationRate != null)
                    ICPAllocationRateService.UnbindSession(lstAllocationRate);
                if (lstAllocationRate1 != null)
                    ICPAllocationRateService.UnbindSession(lstAllocationRate1);
                if (cpar != null)
                    ICPAllocationRateService.UnbindSession(cpar);
                if (_LstCPResult != null)
                    ICPResultService.UnbindSession(_LstCPResult);
                isClose = false;
            }
            Close();
        }

        private void btnAttribution_Click(object sender, EventArgs e)
        {
            checkAtt = 1;
            lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();
            List<ExpenseItem> lstExpenseItem = Utils.ListExpenseItem.ToList();
            List<CostSet> lstCostSet = Utils.ListCostSet.ToList();
            List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpenseUgrid = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList();
            foreach (var item in lstCPAllocationGeneralExpenseUgrid)
            {
                if (item.AllocatedRate > 100)
                {
                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    return;
                }
                else
                {
                    if (item.AllocationMethod == 0)
                    {
                        List<CPExpenseList> lstCPExpenseList = (from g in _LstCPExpense
                                                                join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                where h.ExpenseType == 0
                                                                select g).ToList();
                        decimal sumAmount = (from g in lstCPExpenseList select g.Amount).Sum();
                        List<CPExpenseList> lstCPExpenseList1 = lstCPExpenseList.GroupBy(x => x.CostsetCode).Select(x => x.First()).ToList();

                        if (lstCPExpenseList1.Count > 0)
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstCPExpenseList1
                                                                           join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                           where h.ExpenseType == 0
                                                                           group g by new { g.CostSetID, g.CostsetCode, g.CostsetName, g.ExpenseItemCode, g.Amount }
                                        into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.CostSetID,
                                                                               CostsetCode = t.Key.CostsetCode,
                                                                               CostsetName = t.Key.CostsetName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               AllocatedRate = (from g in lstCPExpenseList
                                                                                                where g.CostSetID == t.Key.CostSetID
                                                                                                select g.Amount).Sum() / sumAmount * 100,
                                                                               AllocatedAmount = Math.Round(((from g in lstCPExpenseList
                                                                                                              where g.CostSetID == t.Key.CostSetID
                                                                                                              select g.Amount).Sum() / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero),
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               CPAllocationGeneralExpenseID = item.ID
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                        else
                        {
                            foreach (var item1 in _ListCostSet)
                            {
                                CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                                cpg.CostSetID = item1;
                                cpg.CostsetName = Utils.ICostSetService.Getbykey(item1).CostSetName;
                                cpg.CostsetCode = Utils.ICostSetService.Getbykey(item1).CostSetCode;
                                cpg.ExpenseItemCode = item.ExpenseItemCode;
                                cpg.AllocatedRate = 0;
                                cpg.AllocatedAmount = 0;
                                cpg.ExpenseItemID = item.ExpenseItemID;
                                cpg.CPAllocationGeneralExpenseID = item.ID;
                                lstAllocationGeneralExpenseDetail.Add(cpg);
                            }
                        }
                    }
                    else if (item.AllocationMethod == 1)
                    {
                        List<CPExpenseList> lstCPExpenseList = (from g in _LstCPExpense
                                                                join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                where h.ExpenseType == 1
                                                                select g).ToList();
                        decimal sumAmount = (from g in lstCPExpenseList select g.Amount).Sum();
                        List<CPExpenseList> lstCPExpenseList1 = lstCPExpenseList.GroupBy(x => x.CostsetCode).Select(x => x.First()).ToList();

                        if (lstCPExpenseList1.Count > 0)
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstCPExpenseList1
                                                                           group g by new { g.CostSetID, g.CostsetCode, g.CostsetName, g.ExpenseItemCode, g.Amount, g.ExpenseItemID }
                                        into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.CostSetID,
                                                                               CostsetCode = t.Key.CostsetCode,
                                                                               CostsetName = t.Key.CostsetName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               AllocatedRate = (from g in lstCPExpenseList
                                                                                                where g.CostSetID == t.Key.CostSetID
                                                                                                select g.Amount).Sum() / sumAmount * 100,
                                                                               AllocatedAmount = Math.Round(((from g in lstCPExpenseList
                                                                                                              where g.CostSetID == t.Key.CostSetID
                                                                                                              select g.Amount).Sum() / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero),
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               CPAllocationGeneralExpenseID = item.ID
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                        else
                        {
                            foreach (var item1 in _ListCostSet)
                            {
                                CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                                cpg.CostSetID = item1;
                                cpg.CostsetName = Utils.ICostSetService.Getbykey(item1).CostSetName;
                                cpg.CostsetCode = Utils.ICostSetService.Getbykey(item1).CostSetCode;
                                cpg.ExpenseItemCode = item.ExpenseItemCode;
                                cpg.AllocatedRate = 0;
                                cpg.AllocatedAmount = 0;
                                cpg.ExpenseItemID = item.ExpenseItemID;
                                cpg.CPAllocationGeneralExpenseID = item.ID;
                                lstAllocationGeneralExpenseDetail.Add(cpg);
                            }
                        }
                    }
                    else if (item.AllocationMethod == 2)
                    {
                        List<CPExpenseList> lstCPExpenseList = (from g in _LstCPExpense
                                                                join h in lstExpenseItem on g.ExpenseItemID equals h.ID
                                                                where (h.ExpenseType == 1 || h.ExpenseType == 0)
                                                                select g).ToList();
                        decimal sumAmount = (from g in lstCPExpenseList select g.Amount).Sum();
                        var lstCPExpenseList2 = (from a in lstCPExpenseList
                                                 group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                                                 select new
                                                 {
                                                     CostSetID = t.Key.CostSetID,
                                                     CostsetCode = t.Key.CostsetCode,
                                                     CostsetName = t.Key.CostsetName,
                                                     Amount = t.Sum(x => x.Amount),
                                                 }).ToList();
                        if (lstCPExpenseList2.Count > 0)
                        {
                            foreach (var model in lstCPExpenseList2)
                            {
                                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                                cPAllocationGeneralExpenseDetail.CPAllocationGeneralExpenseID = item.ID;
                                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                                cPAllocationGeneralExpenseDetail.ExpenseItemID = item.ExpenseItemID;
                                cPAllocationGeneralExpenseDetail.ExpenseItemCode = item.ExpenseItemCode;
                                cPAllocationGeneralExpenseDetail.AllocatedRate = model.Amount * 100 / sumAmount;
                                cPAllocationGeneralExpenseDetail.AllocatedAmount = Math.Round((model.Amount / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero);
                                lstAllocationGeneralExpenseDetail.Add(cPAllocationGeneralExpenseDetail);
                            }
                        }
                        else
                        {
                            foreach (var item1 in _ListCostSet)
                            {
                                CPAllocationGeneralExpenseDetail cpg = new CPAllocationGeneralExpenseDetail();
                                cpg.CostSetID = item1;
                                cpg.CostsetName = Utils.ICostSetService.Getbykey(item1).CostSetName;
                                cpg.CostsetCode = Utils.ICostSetService.Getbykey(item1).CostSetCode;
                                cpg.ExpenseItemCode = item.ExpenseItemCode;
                                cpg.AllocatedRate = 0;
                                cpg.AllocatedAmount = 0;
                                cpg.ExpenseItemID = item.ExpenseItemID;
                                cpg.CPAllocationGeneralExpenseID = item.ID;
                                lstAllocationGeneralExpenseDetail.Add(cpg);
                            }
                        }
                    }
                    else
                    {
                        List<CPAccountAllocationQuantum> lstAllocationQuantum = GetCPAccountAllocationQuantum();
                        List<CPAccountAllocationQuantum> lstAllocationQuantum1 = (from g in lstAllocationQuantum
                                                                                  where _ListCostSet.Any(x => x == g.AccountingObjectID)
                                                                                  select g).ToList();
                        decimal sumAmount = (decimal)(from g in lstAllocationQuantum1
                                                      select g.TotalCostAmount).Sum();
                        try
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstAllocationQuantum1
                                                                           group g by new { g.AccountingObjectID, g.AccountingObjectCode, g.AccountingObjectName, g.TotalCostAmount }
                                        into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.AccountingObjectID,
                                                                               CostsetCode = t.Key.AccountingObjectCode,
                                                                               CostsetName = t.Key.AccountingObjectName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               CPAllocationGeneralExpenseID = item.ID,
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               AllocatedRate = (decimal)t.Key.TotalCostAmount / sumAmount * 100,
                                                                               AllocatedAmount = Math.Round(((decimal)t.Key.TotalCostAmount / sumAmount * item.AllocatedAmount), lamtron, MidpointRounding.AwayFromZero),
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                        catch
                        {
                            List<CPAllocationGeneralExpenseDetail> lst1 = (from g in lstAllocationQuantum1
                                                                           group g by new { g.AccountingObjectID, g.AccountingObjectCode, g.AccountingObjectName, g.TotalCostAmount }
                                    into t
                                                                           select new CPAllocationGeneralExpenseDetail()
                                                                           {
                                                                               CostSetID = t.Key.AccountingObjectID,
                                                                               CostsetCode = t.Key.AccountingObjectCode,
                                                                               CostsetName = t.Key.AccountingObjectName,
                                                                               ExpenseItemCode = item.ExpenseItemCode,
                                                                               CPAllocationGeneralExpenseID = item.ID,
                                                                               ExpenseItemID = item.ExpenseItemID,
                                                                               AllocatedRate = 0,
                                                                               AllocatedAmount = 0,
                                                                           }).ToList();
                            lstAllocationGeneralExpenseDetail.AddRange(lst1);
                        }
                    }
                }
            }
            lstAllocationGeneralExpenseDetail = lstAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).Where(x => x.AllocatedAmount > 0).ToList();

            //add by cuongpv don gia tri con lai vao dong cuoi cung
            decimal sumCPPhanBo = lstCPAllocationGeneralExpenseUgrid.Sum(x => x.AllocatedAmount);
            decimal sumCPDaPhanBoDetail = lstAllocationGeneralExpenseDetail.Sum(x => x.AllocatedAmount);
            if (sumCPPhanBo != sumCPDaPhanBoDetail)
            {
                int iCount = lstAllocationGeneralExpenseDetail.Count;
                int iMaxCount = iCount - 1;
                for (int i = 0; i < iCount; i++)
                {
                    if (i == iMaxCount)
                        lstAllocationGeneralExpenseDetail[i].AllocatedAmount = sumCPPhanBo;
                    else
                        sumCPPhanBo = sumCPPhanBo - lstAllocationGeneralExpenseDetail[i].AllocatedAmount;
                }
            }
            //end add by cuongpv

            uGridAllocationGeneralExpenseResult.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstAllocationGeneralExpenseDetail);
            Utils.ConfigGrid(uGridAllocationGeneralExpenseResult, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
            Utils.AddSumColumn(uGridAllocationGeneralExpenseResult, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            foreach (var col in uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "AllocatedRate" || col.Key == "AllocatedAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }
        private void uGridAllocationGeneralExpense_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                //checkAtt = 0;
                //checkupdate = 1;
                try
                {
                    if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Text.Trim('_').ToString()) <= 100)
                    {
                        e.Cell.Row.Cells["AllocatedAmount"].Value = Math.Round((Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) / 100), lamtron, MidpointRounding.AwayFromZero);
                        uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                    }
                    else
                    {
                        MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                        return;
                    }
                }
                catch { }
            }
        }

        private void uGridAllocationGeneralExpense_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //checkAtt = 0;
            //checkupdate = 1;
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "AllocatedRate" || activeCell.Column.Key == "AllocatedAmount")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }

        private void uGridAllocationGeneralExpense_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkupdate = 1;
            checkAtt = 0;
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) <= 100)
                {
                    e.Cell.Row.Cells["AllocatedAmount"].Value = Math.Round((Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) / 100), lamtron, MidpointRounding.AwayFromZero);
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
                else
                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
            }
            if (e.Cell.Column.Key.Equals("AllocatedAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) != 100)
                {
                    e.Cell.Row.Cells["AllocatedRate"].Value = Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["UnallocatedAmount"].Value.ToString()) * 100;
                    uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
                }
                else { }
            }
            List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ((BindingList<CPAllocationGeneralExpense>)uGridAllocationGeneralExpense.DataSource).Where(x => x.AllocatedRate > 0).ToList();
            if (cPAllocationGeneralExpenses.Count == 0)
                checkAtt = 1;
        }

        private void optType_ValueChanged(object sender, EventArgs e)
        {
            #region comment by cuongpv
            //checkupdate = 1;
            //_LstCPUncompleteDetail = GetCPUncompleteDetail();
            //uGridUncompleteDetail.DataSource = new BindingList<CPUncompleteDetail>(_LstCPUncompleteDetail);
            //Utils.ConfigGrid(uGridUncompleteDetail, ConstDatabase.CPUncompleteDetail_TableName);
            //uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["Quantity"].CellClickAction = CellClickAction.EditAndSelectText;
            //if (optType.CheckedItem.Tag.ToString().Equals("SPHTTD"))
            //{
            //    checkItem = 0;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            //}
            //else if (optType.CheckedItem.Tag.ToString().Equals("NVLTT"))
            //{
            //    checkItem = 1;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].Hidden = true;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            //}
            //else if (optType.CheckedItem.Tag.ToString().Equals("DM"))
            //{
            //    checkItem = 2;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Width = 150;
            //    uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].Width = 130;
            //}
            #endregion
            ViewGridPanel5();
            btnUnDetermined_Click(sender, e);
        }
        private void dtEndDate_ValueChanged(object sender, EventArgs e)
        {
            if (IsAdd)
            {
                if (dtBeginDate.Value != null && dtEndDate.Value != null)
                {
                    txtCPPeriod.Text = "Kỳ tính giá thành từ ngày " + dtBeginDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến ngày " + dtEndDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dtend = dtEndDate.DateTime;
                    toDate = new DateTime(dtend.Year, dtend.Month, dtend.Day, 23, 59, 59);
                }
            }
        }

        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {
            if (IsAdd)
            {
                if (dtBeginDate.Value != null)
                {
                    txtCPPeriod.Text = "Kỳ tính giá thành từ ngày " + dtBeginDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " đến ngày " + dtEndDate.DateTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dtbegin = dtBeginDate.DateTime;
                    fromDate = new DateTime(dtbegin.Year, dtbegin.Month, dtbegin.Day, 0, 0, 0);
                }
            }
        }

        private void btnUnDetermined_Click(object sender, EventArgs e)
        {
            checkDtr = 1;
            _LstCPUncomplete = new List<CPUncomplete>();
            _LstCPResult = new List<CPResult>();
            _LstExpenseItem = Utils.ListExpenseItem.ToList();
            _LstCPOPN = Utils.ListCPOPN.ToList();
            List<CPOPN> _LstCPOPN2 = new List<CPOPN>();
            List<CPExpenseList> _LstCPExpense3 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense4 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense5 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense6 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense7 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense8 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst4 = new List<CPAllocationGeneralExpenseDetail>();
            _LstCPOPN2 = (from g in _LstCPOPN where _ListCostSet.Any(x => x == g.CostSetID) select g).ToList(); //DirectMatetialAmount
            _LstCPExpense3 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            _LstCPExpense4 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            lst2 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 0
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense5 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            _LstCPExpense6 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            lst3 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 1
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense7 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            _LstCPExpense8 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            lst4 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 2
                    select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
            var lst5 = (from a in lst4
                        group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            CostsetCode = t.Key.CostsetCode,
                            CostsetName = t.Key.CostsetName,
                            Amount = t.Sum(x => x.AllocatedAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                lst6.Add(cPAllocationGeneralExpenseDetail);
            }
            foreach (var item in uGridUncompleteDetail.DataSource as BindingList<CPUncompleteDetail>)
                if (item.PercentComplete > 100)
                {
                    MSG.Warning("% hoàn thành không được quá 100%");
                    return;
                }
            switch (checkItem)
            {
                case 0:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
                case 1:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
                case 2:
                    {
                        List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
                        List<CPUncomplete> lstUncomplete = new List<CPUncomplete>();
                        foreach (var item in (BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource)
                        {
                            CPUncomplete cpu = new CPUncomplete();
                            Decimal directMatetialAmount = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectMaterialAmount).FirstOrDefault();
                            Decimal directLaborAmount = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectLaborAmount).FirstOrDefault();
                            Decimal generalExpensesAmount = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.GeneralExpensesAmount).FirstOrDefault();
                            Decimal quantity = item.Quantity;
                            Decimal percentcomplete = item.PercentComplete;
                            string costsetCode = Utils.ICostSetService.Getbykey(item.CostSetID).CostSetCode;
                            string costsetName = Utils.ICostSetService.Getbykey(item.CostSetID).CostSetName;
                            cpu.CostsetCode = costsetCode;
                            cpu.CostsetName = costsetName;
                            cpu.CostSetID = item.CostSetID;
                            cpu.DirectMaterialAmount = Math.Round((directMatetialAmount * quantity * percentcomplete / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpu.DirectLaborAmount = Math.Round((directLaborAmount * quantity * percentcomplete / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpu.GeneralExpensesAmount = Math.Round((generalExpensesAmount * quantity * percentcomplete / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                            lstUncomplete.Add(cpu);
                        }

                        var lst = (from g in lstUncomplete
                                   group g by new { g.CostSetID, g.CostsetCode, g.CostsetName } into t
                                   select new
                                   {
                                       CostSetID = t.Key.CostSetID,
                                       CostsetCode = t.Key.CostsetCode,
                                       CostsetName = t.Key.CostsetName,
                                       DirectMaterialAmount = t.Sum(x => x.DirectMaterialAmount),
                                       DirectLaborAmount = t.Sum(x => x.DirectLaborAmount),
                                       GeneralExpensesAmount = t.Sum(x => x.GeneralExpensesAmount),
                                       TotalCostAmount = t.Sum(x => x.TotalCostAmount)
                                   }).ToList();

                        foreach (var item1 in lst)
                        {
                            CPUncomplete cpt = new CPUncomplete();
                            cpt.ID = Guid.NewGuid();
                            cpt.CostSetID = item1.CostSetID;
                            cpt.CostsetCode = item1.CostsetCode;
                            cpt.CostsetName = item1.CostsetName;
                            cpt.DirectMaterialAmount = item1.DirectMaterialAmount;
                            cpt.DirectLaborAmount = item1.DirectLaborAmount;
                            cpt.GeneralExpensesAmount = item1.GeneralExpensesAmount;
                            cpt.TotalCostAmount = item1.TotalCostAmount;
                            _LstCPUncomplete.Add(cpt);
                        }

                        if (_LstCPUncomplete != null)
                            _LstCPUncomplete = _LstCPUncomplete.OrderBy(x => x.CostsetCode).ToList();
                        uGridUncomplete.DataSource = new BindingList<CPUncomplete>(_LstCPUncomplete);
                        Utils.ConfigGrid(uGridUncomplete, ConstDatabase.CPUncomplete_TableName);
                        break;
                    }
                default:
                    {
                        GetCPUncomplete(_LstCPOPN2, _LstCPExpense3, _LstCPExpense4, _LstCPExpense5, _LstCPExpense6, _LstCPExpense7, _LstCPExpense8, lst2, lst3, lst6);
                        break;
                    }
            }
            Utils.AddSumColumn(uGridUncomplete, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);
            foreach (var col in uGridUncomplete.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "DirectMaterialAmount" || col.Key == "DirectLaborAmount" || col.Key == "GeneralExpensesAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }

        #endregion events

        #region Funcition

        private void ViewGridPanel1()           //Xác d?nh k? tính giá thành
        {
            listcostset = new List<SelectCostSet>();
            if (IsAdd)
            {
                listcostset = Utils.ICostSetService.GetListByType(2);
                if (listcostset.Count == 0)
                    listcostset = new List<SelectCostSet>();
            }
            else
            {
                List<CPPeriodDetail> lstCPPeriodDetail = ICPPeriodDetailService.GetList().Where(x => x.CPPeriodID == _select.ID).ToList();
                foreach (var item in lstCPPeriodDetail)
                {
                    CostSet cs = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID);
                    SelectCostSet scs = new SelectCostSet();
                    scs.ID = cs.ID;
                    scs.Select = true;
                    scs.CostSetCode = cs.CostSetCode;
                    scs.CostSetName = cs.CostSetName;
                    scs.CostSetType = cs.CostSetType;
                    scs.CostSetTypeView = cs.CostSetTypeView;
                    listcostset.Add(scs);
                }
            }
            if (listcostset.Count > 0)
                listcostset = listcostset.OrderBy(x => x.CostSetCode).ToList();
            uGridCostset.DataSource = new BindingList<SelectCostSet>(listcostset);
            Utils.ConfigGrid(uGridCostset, ConstDatabase.SelectCostSet_TableName);
            var gridBand = uGridCostset.DisplayLayout.Bands[0];
            foreach (var col in uGridCostset.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key != "Select") col.CellActivation = Activation.NoEdit;
            }
            gridBand.Columns["Select"].Header.VisiblePosition = 0;
            gridBand.Columns["Select"].Hidden = false;
            gridBand.Columns["Select"].Style = ColumnStyle.CheckBox;
            gridBand.Columns["Select"].AutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
            gridBand.Columns["Select"].CellClickAction = CellClickAction.Edit;
            uGridCostset.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCostset.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            gridBand.Columns["Select"].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            gridBand.Columns["Select"].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            gridBand.Columns["Select"].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            gridBand.Columns["Select"].Header.Fixed = true;
            Utils.ProcessControls(this);
            if (!IsAdd)
            {
                gridBand.Columns["Select"].Hidden = true;
                cbbDateTime.Text = "";
                dtBeginDate.Value = _select.FromDate;
                dtEndDate.Value = _select.ToDate;
            }

            if (IsAdd)
            {
                DateTime dtbegin = dtBeginDate.DateTime;
                DateTime dtend = dtEndDate.DateTime;
                fromDate = new DateTime(dtbegin.Year, dtbegin.Month, dtbegin.Day, 0, 0, 0);
                toDate = new DateTime(dtend.Year, dtend.Month, dtend.Day, 23, 59, 59);
            }
        }

        private void ViewGridPanel2()           //T?p h?p Chi phí trực tiếp
        {
            _LstCPExpense = new List<CPExpenseList>();
            if (IsAdd)
            {
                _LstCPExpense = Utils.IGeneralLedgerService.GetCPExpenseListsOnGL(fromDate, toDate, _ListCostSet);
                _LstCPExpense = _LstCPExpense.OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
                if (_LstCPExpense.Count == 0)
                    _LstCPExpense = new List<CPExpenseList>();
            }
            else
            {
                foreach (var item in _select.CPExpenseLists)
                {
                    item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                    item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                }
                _LstCPExpense = _select.CPExpenseLists.Where(x => x.TypeVoucher == 0).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
            }
            uGridCpExpenseList2.DataSource = new BindingList<CPExpenseList>(_LstCPExpense.OrderBy(x => x.PostedDate).ToList());
            Utils.ConfigGrid(uGridCpExpenseList2, ConstDatabase.CPExpenseList_TableName);
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["CostsetCode"].Header.Caption = "Đối tượng THCP";
            uGridCpExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCpExpenseList2, "Amount", false, "", ConstDatabase.Format_TienVND);
        }

        private void ViewGridPanel3()           //T?p h?p các kho?n gi?m giá thành
        {
            _LstCPExpense2 = new List<CPExpenseList>();
            if (IsAdd)
            {
                _LstCPExpense2 = Utils.IGeneralLedgerService.GetCPExpenseListsOnGL2(fromDate, toDate, _ListCostSet);
                _LstCPExpense2 = _LstCPExpense2.OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
                if (_LstCPExpense2.Count == 0)
                    _LstCPExpense2 = new List<CPExpenseList>();
            }
            else
            {
                foreach (var item in _select.CPExpenseLists)
                {
                    item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                    item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                }
                _LstCPExpense2 = _select.CPExpenseLists.Where(x => x.TypeVoucher == 1).OrderBy(x => x.CostsetCode).ThenBy(x => x.ExpenseItemCode).ToList();
            }
            uGridCpExpenseList3.DataSource = new BindingList<CPExpenseList>(_LstCPExpense2.OrderBy(x => x.PostedDate).ToList());
            Utils.ConfigGrid(uGridCpExpenseList3, ConstDatabase.CPExpenseList_TableName);
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["CostsetCode"].Header.Caption = "Đối tượng THCP";
            uGridCpExpenseList3.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCpExpenseList3, "Amount", false, "", ConstDatabase.Format_TienVND);
        }

        private void ViewGridPanel4()           //Phân bổ chi phí chung
        {
            lstAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
            lstAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();

            //lay tat ca cac chi phi da luu trong CPAllocationGeneralExpense
            List<CPAllocationGeneralExpense> listCPAllocationGeneralExpenseDB = ICPAllocationGeneralExpenseService.GetList();
            List<Guid> lstReferenceIDDB = (from g in listCPAllocationGeneralExpenseDB select g.ReferenceID).ToList();

            //lay cac chi phi chua phan bo het trong bang CPAllocationGeneralExpense
            List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpenseUnallocated = (from g in listCPAllocationGeneralExpenseDB
                                                                                         group g by new { g.ReferenceID }
                                                                                         into t
                                                                                         select new CPAllocationGeneralExpense()
                                                                                         {
                                                                                             ReferenceID = (Guid)t.Key.ReferenceID,
                                                                                             TotalCost = t.Max(x => x.TotalCost),
                                                                                             AllocatedRate = t.Sum(x => x.AllocatedRate),
                                                                                             AllocatedAmount = t.Sum(x => x.AllocatedAmount)
                                                                                         }).ToList();

            lstCPAllocationGeneralExpenseUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.AllocatedAmount < x.TotalCost).ToList();

            List<Guid> lstReferenceIDUnallocated = (from g in lstCPAllocationGeneralExpenseUnallocated select g.ReferenceID).ToList();
            
            if (IsAdd)
            {
                #region Lay CP ky truoc

                //lay chi phi cac ky truoc tu GL
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBefore = Utils.IGeneralLedgerService.GetCPAllocationGeneralExpenseOnGLBefore(fromDate);

                //lay chi phi chua phan bo cua cac ky truoc
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBeforeNotUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseBeforeNotUnallocated = (from g in lstAllocationGeneralExpenseBefore where !lstReferenceIDDB.Any(x => x == g.ReferenceID) select g).ToList();
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseBeforeNotUnallocated);

                //lay cac chi phi chua phan bo het cua cac ky truoc
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseBeforeUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseBeforeUnallocated = (from g in lstAllocationGeneralExpenseBefore where lstReferenceIDUnallocated.Any(x => x == g.ReferenceID) select g).ToList();
                var itemNotUnallocated = new CPAllocationGeneralExpense();
                foreach (var itemBeforeUnallocated in lstAllocationGeneralExpenseBeforeUnallocated)
                {
                    itemNotUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.ReferenceID == itemBeforeUnallocated.ReferenceID).FirstOrDefault();
                    if (!itemNotUnallocated.IsNullOrEmpty())
                    {
                        itemBeforeUnallocated.UnallocatedAmount = itemNotUnallocated.TotalCost - itemNotUnallocated.AllocatedAmount;
                    }
                }
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseBeforeUnallocated);
                #endregion

                #region lay CP ky nay
                //lay chi phi ky nay tu GL
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHere = Utils.IGeneralLedgerService.GetCPAllocationGeneralExpenseOnGL(fromDate, toDate);

                //lay chi phi chua phan bo cua ky nay
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHereNotUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseHereNotUnallocated = (from g in lstAllocationGeneralExpenseHere where !lstReferenceIDDB.Any(x => x == g.ReferenceID) select g).ToList();
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseHereNotUnallocated);

                //lay chi phi chua phan bo het cua ky nay
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpenseHereUnallocated = new List<CPAllocationGeneralExpense>();
                lstAllocationGeneralExpenseHereUnallocated = (from g in lstAllocationGeneralExpenseHere where lstReferenceIDUnallocated.Any(x => x == g.ReferenceID) select g).ToList();
                var itemHereNotUnallocated = new CPAllocationGeneralExpense();
                foreach (var itemBeforeUnallocated in lstAllocationGeneralExpenseHereUnallocated)
                {
                    itemHereNotUnallocated = lstCPAllocationGeneralExpenseUnallocated.Where(x => x.ReferenceID == itemBeforeUnallocated.ReferenceID).FirstOrDefault();
                    if (!itemHereNotUnallocated.IsNullOrEmpty())
                    {
                        itemBeforeUnallocated.UnallocatedAmount = itemHereNotUnallocated.TotalCost - itemHereNotUnallocated.AllocatedAmount;
                    }
                }
                lstAllocationGeneralExpense.AddRange(lstAllocationGeneralExpenseHereUnallocated);

                #endregion

                if (lstAllocationGeneralExpense.Count == 0)
                    lstAllocationGeneralExpense = new List<CPAllocationGeneralExpense>();
                else
                {
                    foreach (var item in lstAllocationGeneralExpense)
                    {
                        item.ExpenseItemCode = Utils.ListExpenseItem.Where(x => x.ID == item.ExpenseItemID).FirstOrDefault().ExpenseItemCode;
                        item.AllocatedRate = 100;
                        item.AllocatedAmount = item.UnallocatedAmount;
                    }
                }

                lstAllocationGeneralExpense = lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList();
                //lstAllocationGeneralExpenseDetail = lstAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).ToList();

                uGridAllocationGeneralExpense.DataSource = new BindingList<CPAllocationGeneralExpense>(lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList());
                Utils.ConfigGrid(uGridAllocationGeneralExpense, ConstDatabase.CPAllocationGeneralExpense_TableName);
                var cbbMethods = new UltraComboEditor();
                cbbMethods.Items.Add(0, "Nguyên vật liệu trực tiếp");
                cbbMethods.Items.Add(1, "Nhân công trực tiếp");
                cbbMethods.Items.Add(2, "Chi phí trực tiếp");
                cbbMethods.Items.Add(3, "Định mức");
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethodView"].Hidden = true;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].Hidden = false;
                foreach (var column in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
                {
                    if (column.Key.Equals("AllocationMethod"))
                    {
                        column.EditorComponent = cbbMethods;
                        column.Style = ColumnStyle.DropDownList;
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    if (column.Key == "AllocatedRate" || column.Key == "AllocatedAmount")
                    {
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    else
                    {
                        column.CellActivation = Activation.NoEdit;
                    }
                }
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].CellActivation = Activation.AllowEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].CellActivation = Activation.AllowEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].CellActivation = Activation.NoEdit;
            }
            else
            {
                //lstAllocationGeneralExpense = _select.CPAllocationGeneralExpenses.Where(x => x.CPPeriodID == _select.ID).Where(x => x.AllocatedRate > 0).ToList();
                lstAllocationGeneralExpense = ICPAllocationGeneralExpenseService.GetAll().Where(x => x.CPPeriodID == _select.ID && x.AllocatedRate > 0).ToList();
                foreach (var item in lstAllocationGeneralExpense)
                {
                    item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                    if (item.AllocationMethod == 0)
                        item.AllocationMethodView = "Nguyên vật liệu trực tiếp";
                    else if (item.AllocationMethod == 1)
                        item.AllocationMethodView = "Nhân công trực tiếp";
                    else if (item.AllocationMethod == 2)
                        item.AllocationMethodView = "Chi phí trực tiếp";
                    else if (item.AllocationMethod == 3)
                        item.AllocationMethodView = "Định mức";

                    List<CPAllocationGeneralExpenseDetail> lst = item.CPAllocationGeneralExpenseDetails.ToList();
                    lstAllocationGeneralExpenseDetail.AddRange(lst);
                }
                foreach (var item1 in lstAllocationGeneralExpenseDetail)
                {
                    item1.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item1.CostSetID).CostSetCode;
                    item1.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item1.CostSetID).CostSetName;
                    item1.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item1.ExpenseItemID).ExpenseItemCode;
                }

                lstAllocationGeneralExpense = lstAllocationGeneralExpense.OrderBy(x => x.ExpenseItemCode).ToList();
                lstAllocationGeneralExpenseDetail = lstAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).ToList();
                
                uGridAllocationGeneralExpense.DataSource = new BindingList<CPAllocationGeneralExpense>(lstAllocationGeneralExpense);
                Utils.ConfigGrid(uGridAllocationGeneralExpense, ConstDatabase.CPAllocationGeneralExpense_TableName);
                var cbbMethods = new UltraComboEditor();
                cbbMethods.Items.Add(0, "Nguyên vật liệu trực tiếp");
                cbbMethods.Items.Add(1, "Nhân công trực tiếp");
                cbbMethods.Items.Add(2, "Chi phí trực tiếp");
                cbbMethods.Items.Add(3, "Định mức");
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethodView"].Hidden = true;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].Hidden = false;
                foreach (var column in uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns)
                {
                    if (column.Key.Equals("AllocationMethod"))
                    {
                        column.EditorComponent = cbbMethods;
                        column.Style = ColumnStyle.DropDownList;
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    if (column.Key == "AllocatedRate" || column.Key == "AllocatedAmount")
                    {
                        column.CellClickAction = CellClickAction.EditAndSelectText;
                    }
                    else
                    {
                        column.CellActivation = Activation.NoEdit;
                    }
                }
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].CellActivation = Activation.NoEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocationMethod"].CellActivation = Activation.AllowEdit;
                uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].CellActivation = Activation.NoEdit;
            }

            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["TotalCost"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "TotalCost", false, "", ConstDatabase.Format_TienVND);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["UnallocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "UnallocatedAmount", false, "", ConstDatabase.Format_TienVND);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
            uGridAllocationGeneralExpense.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpense, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            uGridAllocationGeneralExpenseResult.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstAllocationGeneralExpenseDetail);
            Utils.ConfigGrid(uGridAllocationGeneralExpenseResult, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
            uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridAllocationGeneralExpenseResult, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            foreach (var col in uGridAllocationGeneralExpenseResult.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "AllocatedRate" || col.Key == "AllocatedAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }
        }

        private void ViewGridPanel5()           //Ðánh giá d? dang cu?i k?
        {
            _LstCPUncomplete = new List<CPUncomplete>();
            _LstCPUncompleteDetail = new List<CPUncompleteDetail>();
            if (IsAdd)
            {
                _LstCPUncompleteDetail = GetCPUncompleteDetail();
                if (_LstCPUncompleteDetail.Count == 0)
                    _LstCPUncompleteDetail = new List<CPUncompleteDetail>();
            }
            else
            {
                _LstCPUncomplete = _select.CPUncompletes.ToList();
                if(_LstCPUncomplete.Count > 0)
                {
                    foreach (var item in _LstCPUncomplete)
                    {
                        item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;

                        List<CPUncompleteDetail> lst = item.CPUncompleteDetails.ToList();
                        _LstCPUncompleteDetail.AddRange(lst);
                    }
                }

                if (_LstCPUncompleteDetail.Count > 0)
                {
                    foreach (var item in _LstCPUncompleteDetail)
                    {
                        item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                        item.MaterialGoodCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                        item.MaterialGoodName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
                    }

                    if (optType.CheckedIndex != _LstCPUncompleteDetail.FirstOrDefault().UncompleteType)
                    {
                        checkupdate = 1;
                    }
                }

                _LstCPUncomplete = _LstCPUncomplete.OrderBy(x => x.CostsetCode).ToList();
            }

            _LstCPUncompleteDetail = _LstCPUncompleteDetail.OrderBy(x => x.MaterialGoodCode).ToList();

            uGridUncomplete.DataSource = new BindingList<CPUncomplete>(_LstCPUncomplete);
            Utils.ConfigGrid(uGridUncomplete, ConstDatabase.CPUncomplete_TableName);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            uGridUncomplete.DisplayLayout.Bands[0].Columns["TotalCostAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridUncomplete, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);

            foreach (var col in uGridUncomplete.DisplayLayout.Bands[0].Columns)
            {
                if (col.Key == "DirectMaterialAmount" || col.Key == "DirectLaborAmount" || col.Key == "GeneralExpensesAmount")
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    col.CellActivation = Activation.NoEdit;
                }
            }

            uGridUncompleteDetail.DataSource = new BindingList<CPUncompleteDetail>(_LstCPUncompleteDetail);
            Utils.ConfigGrid(uGridUncompleteDetail, ConstDatabase.CPUncompleteDetail_TableName);
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Width = 120;
            foreach (var column in uGridUncompleteDetail.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key == "PercentComplete")
                {
                    column.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else if (column.Key == "Quantity")
                {
                    column.CellClickAction = CellClickAction.EditAndSelectText;
                }
                else
                {
                    column.CellActivation = Activation.NoEdit;
                }
            }

            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].FormatNumberic(-1);

            if (optType.CheckedIndex == 0)
            {
                checkItem = 0;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            }
            else if (optType.CheckedIndex == 1)
            {
                checkItem = 1;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].Hidden = true;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["UnitPrice"].Hidden = true;
            }
            else
            {
                checkItem = 2;
                uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].CellClickAction = CellClickAction.EditAndSelectText;
            }
        }

        private void ViewGridPanel6()           //Xác d?nh h? s? phân b?
        {
            lstAllocationRate = new List<CPAllocationRate>();
            if (IsAdd)
            {
                lstAllocationRate = GetCPAllocationRates();
                if (lstAllocationRate.Count == 0)
                    lstAllocationRate = new List<CPAllocationRate>();
            }
            else
            {
                if (_select.CPAllocationRates.Count > 0)
                {
                    foreach (var item in _select.CPAllocationRates)
                    {
                        item.CostSetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.MaterialGoodsCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                        item.MaterialGoodsName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
                    }
                }
                lstAllocationRate = _select.CPAllocationRates.OrderBy(x => x.MaterialGoodsCode).ToList();
            }
            uGridAllocatedRate.DataSource = new BindingList<CPAllocationRate>(lstAllocationRate);
            Utils.ConfigGrid(uGridAllocatedRate, ConstDatabase.CPAllocationRate_TableName);
            var gridBand = uGridAllocatedRate.DisplayLayout.Bands[0];
            foreach (var col in uGridAllocatedRate.DisplayLayout.Bands[0].Columns)
            {
                //if (col.Key != "IsStandardItem") col.CellActivation = Activation.NoEdit;comment by cuongpv
                //edit by cuongpv
                if (col.Key == "IsStandardItem")
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                else if (col.Key == "Quantity")
                    col.CellClickAction = CellClickAction.EditAndSelectText;
                else
                    col.CellActivation = Activation.NoEdit;
                //end edit by cuongpv
            }
            gridBand.Columns["IsStandardItem"].CellClickAction = CellClickAction.Edit;
            uGridAllocatedRate.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            //Utils.AddSumColumn(uGridAllocatedRate, "Quantity", false, "", ConstDatabase.Format_Quantity);
            uGridAllocatedRate.DisplayLayout.Bands[0].Columns["PriceQuantum"].FormatNumberic(ConstDatabase.Format_TienVND);
            //Utils.AddSumColumn(uGridAllocatedRate, "PriceQuantum", false, "", ConstDatabase.Format_TienVND);
            uGridAllocatedRate.DisplayLayout.Bands[0].Columns["Coefficien"].FormatNumberic(-1);
            //Utils.AddSumColumn(uGridAllocatedRate, "Coefficien", false, "", -1);
            uGridAllocatedRate.DisplayLayout.Bands[0].Columns["QuantityStandard"].FormatNumberic(ConstDatabase.Format_Quantity);
            //Utils.AddSumColumn(uGridAllocatedRate, "QuantityStandard", false, "", ConstDatabase.Format_Quantity);
            uGridAllocatedRate.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
            //Utils.AddSumColumn(uGridAllocatedRate, "AllocatedRate", false, "", -1);
            uGridAllocatedRate.DisplayLayout.Bands[0].Columns["AllocationStandard"].Hidden = true;
            //uGridAllocatedRate.DisplayLayout.Bands[0].Columns["Quantity"].CellClickAction = CellClickAction.Edit;
        }

        private void ViewGridPanel7()           //B?ng k?t qu? tính giá thành
        {
            _LstCPResult = new List<CPResult>();
            btnEscape.Text = "Ðóng";
            if (!IsAdd)
            {
                if (checkupdate == 0)
                    _LstCPResult = _select.CPResults.ToList();//edit by cuongpv
                else
                    _LstCPResult = GetCPResults();
                if (_select.CPResults.Count > 0)
                {
                    foreach (var item in _select.CPResults)
                    {
                        item.CostSetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                        item.MaterialGoodsCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                        item.MaterialGoodsName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
                    }
                }
            }
            else
            {
                _LstCPResult = GetCPResults();
                if (_LstCPResult.Count == 0)
                    _LstCPResult = new List<CPResult>();
            }

            ugridCostResult.DataSource = new BindingList<CPResult>(_LstCPResult.OrderBy(x => x.MaterialGoodsCode).ToList());//edit by cuongpv
            Utils.ConfigGrid(ugridCostResult, ConstDatabase.CPCostResult_TableName);
            ugridCostResult.DisplayLayout.Bands[0].Columns["Coefficien"].FormatNumberic(-1);
            //Utils.AddSumColumn(ugridCostResult, "Coefficien", false, "", -1);
            ugridCostResult.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ugridCostResult, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            ugridCostResult.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ugridCostResult, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            ugridCostResult.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ugridCostResult, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            ugridCostResult.DisplayLayout.Bands[0].Columns["TotalCostAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(ugridCostResult, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);
            ugridCostResult.DisplayLayout.Bands[0].Columns["TotalQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            //Utils.AddSumColumn(ugridCostResult, "TotalQuantity", false, "", ConstDatabase.Format_Quantity);
            ugridCostResult.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            //Utils.AddSumColumn(ugridCostResult, "UnitPrice", false, "", ConstDatabase.Format_TienVND);
        }

        private List<CPUncompleteDetail> GetCPUncompleteDetail()
        {
            List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
            _LstCPUncompleteDetail = new List<CPUncompleteDetail>();
            _LstRSInwardOutward = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
            _LstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
            _LstRSInwardOutwardDetail = (from g in _LstRSInwardOutward
                                         join h in _LstRSInwardOutwardDetail on g.ID equals h.RSInwardOutwardID
                                         where g.TypeID == 400 && _ListCostSet.Any(x => x == h.CostSetID) && h.PostedDate >= fromDate && h.PostedDate <= toDate
                                         select h).ToList();

            var lst5 = (from a in _LstRSInwardOutwardDetail
                        group a by new { a.MaterialGoodsID, a.CostSetID } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            MaterialGoodsID = t.Key.MaterialGoodsID,
                            Quantity = t.Sum(x => x.Quantity)
                        }).ToList();

            foreach (var item in _ListCostSet)
            {
                string costSetCode = Utils.ICostSetService.Getbykey(item).CostSetCode;
                List<CostSetMaterialGoods> costSetMaterialGoods = ICostSetMaterialGoodsService.GetAll().Where(x => x.CostSetID == item).ToList();
                List<Guid> materialGoods = (from g in costSetMaterialGoods select g.MaterialGoodsID).ToList();
                if (lst5.Count > 0)
                {
                    _LstCPUncompleteDetail = lst5.Where(x => _ListCostSet.Contains(x.CostSetID ?? Guid.Empty))
                    .Select(g => new CPUncompleteDetail()
                    {
                        ID = Guid.NewGuid(),
                        CostSetID = (Guid)g.CostSetID,
                        CostsetCode = costSetCode,
                        MaterialGoodsID = (Guid)g.MaterialGoodsID,
                        MaterialGoodCode = Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsCode,
                        MaterialGoodName = Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsName,
                        UnitPrice = (from h in lstCPMaterialProductQuantum where h.MaterialGoodsCode == Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsCode select (Decimal)h.TotalCostAmount).FirstOrDefault()
                    }).ToList();
                }
                else
                {
                    if (materialGoods.Count > 0)
                    {
                        foreach (var item1 in materialGoods)
                        {
                            CPUncompleteDetail cPUncompleteDetail = new CPUncompleteDetail();
                            cPUncompleteDetail.ID = Guid.NewGuid();
                            cPUncompleteDetail.CostSetID = item;
                            cPUncompleteDetail.CostsetCode = costSetCode;
                            cPUncompleteDetail.MaterialGoodsID = item1;
                            cPUncompleteDetail.MaterialGoodCode = Utils.IMaterialGoodsService.Getbykey(item1).MaterialGoodsCode;
                            cPUncompleteDetail.MaterialGoodName = Utils.IMaterialGoodsService.Getbykey(item1).MaterialGoodsName;
                            cPUncompleteDetail.UnitPrice = (from h in lstCPMaterialProductQuantum where h.MaterialGoodsCode == Utils.IMaterialGoodsService.Getbykey(item1).MaterialGoodsCode select (Decimal)h.TotalCostAmount).FirstOrDefault();
                            _LstCPUncompleteDetail.Add(cPUncompleteDetail);
                        }
                    }
                }
            }

            if (_LstCPUncompleteDetail != null)
                _LstCPUncompleteDetail = _LstCPUncompleteDetail.OrderBy(x => x.MaterialGoodCode).ToList();
            return _LstCPUncompleteDetail;
        }

        private void GetCPUncomplete(List<CPOPN> lstOPN, List<CPExpenseList> lstExpense1,
            List<CPExpenseList> lstExpense2, List<CPExpenseList> lstExpense3, List<CPExpenseList> lstExpense4, List<CPExpenseList> lstExpense5, List<CPExpenseList> lstExpense6,
            List<CPAllocationGeneralExpenseDetail> lst1, List<CPAllocationGeneralExpenseDetail> lst2, List<CPAllocationGeneralExpenseDetail> lst3)
        {
            foreach (var item in _ListCostSet)
            {
                CPUncomplete cpu = new CPUncomplete();
                Decimal convertquantity = 0;
                Decimal quantity = 0;
                CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == item);
                Decimal directMatetialAmount = 0;
                Decimal directLaborAmount = 0;
                Decimal generalExpensesAmount = 0;
                List<CPPeriod> cPPeriod = ICPPeriodService.GetAll().Where(x => x.ToDate < fromDate && x.Type == 1).OrderByDescending(x => x.ToDate).ToList();
                List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll().Where(x => x.CostSetID == item).ToList();

                if (cPPeriod.Count > 0)
                {
                    CPPeriod period1 = (from g in cPPeriod join h in cPPeriodDetails on g.ID equals h.CPPeriodID select g).FirstOrDefault();
                    if (period1 != null)
                    {
                        CPUncomplete uncomplete = period1.CPUncompletes.FirstOrDefault(x => x.CostSetID == item);
                        if (uncomplete != null)
                        {
                            directMatetialAmount = uncomplete.DirectMaterialAmount;
                            directLaborAmount = uncomplete.DirectLaborAmount;
                            generalExpensesAmount = uncomplete.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                    else
                    {
                        if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                        {
                            directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                            directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                            generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }
                Decimal amount = (from g in lstExpense1 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount2 = (from g in lstExpense2 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount3 = (from g in lstExpense3 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount4 = (from g in lstExpense4 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount5 = (from g in lstExpense5 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount6 = (from g in lstExpense6 where g.CostSetID == item select g.Amount).Sum();
                Decimal allocatedAmount = (from g in lst1 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal allocatedAmount1 = (from g in lst2 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal allocatedAmount2 = (from g in lst3 where g.CostSetID == item select g.AllocatedAmount).Sum();
                List<CPUncompleteDetail> lstUncompleteDetail = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(x => x.CostSetID == item).ToList();
                foreach (var item1 in lstUncompleteDetail)
                {
                    Decimal quantity1 = item1.Quantity;
                    Decimal percentComplete1 = item1.PercentComplete;
                    convertquantity += quantity1 * percentComplete1 / 100;
                    quantity += quantity1;
                }
                Decimal totalQuantity = 0;
                //edit by cuongpv
                if (IsAdd)
                {
                    if (_LstRSInwardOutwardDetail != null)
                    {
                        totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item select (Decimal)g.Quantity).Sum();
                    }
                }
                else
                {
                    _LstRSInwardOutward = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
                    _LstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
                    _LstRSInwardOutwardDetail = (from g in _LstRSInwardOutward
                                                 join h in _LstRSInwardOutwardDetail on g.ID equals h.RSInwardOutwardID
                                                 where g.TypeID == 400 && _ListCostSet.Any(x => x == h.CostSetID) && h.PostedDate >= fromDate && h.PostedDate <= toDate
                                                 select h).ToList();
                    totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item select (Decimal)g.Quantity).Sum();
                }
                //end edit cuongpv
                #region comment by cuongpv
                //if (_LstRSInwardOutwardDetail != null)
                //{
                //    if (IsAdd)
                //        totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item select (Decimal)g.Quantity).Sum();
                //    else
                //    {
                //        _LstRSInwardOutward = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
                //        _LstRSInwardOutwardDetail = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
                //        _LstRSInwardOutwardDetail = (from g in _LstRSInwardOutward
                //                                     join h in _LstRSInwardOutwardDetail on g.ID equals h.RSInwardOutwardID
                //                                     where g.TypeID == 400 && _ListCostSet.Any(x => x == h.CostSetID) && h.PostedDate >= fromDate && h.PostedDate <= toDate
                //                                     select h).ToList();
                //        totalQuantity = (from g in _LstRSInwardOutwardDetail where g.CostSetID == item select (Decimal)g.Quantity).Sum();
                //    }
                //}
                #endregion
                cpu.ID = Guid.NewGuid();
                cpu.CostSetID = item;
                string costsetCode = Utils.ICostSetService.Getbykey(item).CostSetCode;
                string costsetName = Utils.ICostSetService.Getbykey(item).CostSetName;
                cpu.CostsetCode = costsetCode;
                cpu.CostsetName = costsetName;
                if (checkItem == 0)
                {
                    if ((totalQuantity + convertquantity) != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round(((directMatetialAmount + amount + allocatedAmount - amount2) * convertquantity / (totalQuantity + convertquantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = Math.Round(((directLaborAmount + amount3 + allocatedAmount1 - amount4) * convertquantity / (totalQuantity + convertquantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.GeneralExpensesAmount = Math.Round(((generalExpensesAmount + amount5 + allocatedAmount2 - amount6) * convertquantity / (totalQuantity + convertquantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }
                else if (checkItem == 1)
                {
                    if ((totalQuantity + quantity) != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round(((directMatetialAmount + amount + allocatedAmount - amount2) * quantity / (totalQuantity + quantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = 0;
                        cpu.GeneralExpensesAmount = 0;
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }

                _LstCPUncomplete.Add(cpu);
            }

            if (_LstCPUncomplete != null)
                _LstCPUncomplete = _LstCPUncomplete.OrderBy(x => x.CostsetCode).ToList();
            uGridUncomplete.DataSource = new BindingList<CPUncomplete>(_LstCPUncomplete);
            Utils.ConfigGrid(uGridUncomplete, ConstDatabase.CPUncomplete_TableName);
        }

        private void GetLoopCPUncomplete(CPUncomplete cpu)
        {
            Utils.ICPUncompleteService.BeginTran();
            List<CPExpenseList> lstCPExpense = ICPExpenseListService.GetAll().Where(x => x.CPPeriodID == cpu.CPPeriodID && x.TypeVoucher == 0 && x.CostSetID == cpu.CostSetID).ToList(); // NVL trực tiếp
            List<CPExpenseList> lstCPExpense2 = ICPExpenseListService.GetAll().Where(x => x.CPPeriodID == cpu.CPPeriodID && x.TypeVoucher == 1 && x.CostSetID == cpu.CostSetID).ToList(); // Giảm giá thành
            List<CPAllocationGeneralExpense> lstAllocationGeneralExpense1 = ICPAllocationGeneralExpenseService.GetAll().Where(x => x.CPPeriodID == cpu.CPPeriodID).ToList(); // Phân bổ chung 
            List<CPAllocationGeneralExpenseDetail> lstAllocationGeneralExpenseDetail1 = new List<CPAllocationGeneralExpenseDetail>();
            foreach (var item in lstAllocationGeneralExpense1)
            {
                lstAllocationGeneralExpenseDetail1.AddRange(item.CPAllocationGeneralExpenseDetails);
            }
            lstAllocationGeneralExpenseDetail1 = lstAllocationGeneralExpenseDetail1.Where(x => x.CostSetID == cpu.CostSetID).ToList();
            List<CPExpenseList> lstCPExpense3 = new List<CPExpenseList>();
            List<CPExpenseList> lstCPExpense4 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> lstCPExpense5 = new List<CPExpenseList>();
            List<CPExpenseList> lstCPExpense6 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> lstCPExpense7 = new List<CPExpenseList>();
            List<CPExpenseList> lstCPExpense8 = new List<CPExpenseList>();
            lstCPExpense3 = (from g in lstCPExpense
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 0
                             select g).ToList(); //Amount 
            lstCPExpense4 = (from g in lstCPExpense2
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 0
                             select g).ToList(); //Amount 
            lstCPExpense5 = (from g in lstCPExpense
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 1
                             select g).ToList(); //Amount 
            lstCPExpense6 = (from g in lstCPExpense2
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 1
                             select g).ToList(); //Amount 
            lstCPExpense7 = (from g in lstCPExpense
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 2
                             select g).ToList(); //Amount 
            lstCPExpense8 = (from g in lstCPExpense2
                             join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                             where h.ExpenseType == 2
                             select g).ToList(); //Amount
            lst2 = (from g in lstAllocationGeneralExpenseDetail1
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 0
                    select g).ToList(); //AllocatedAmount   
            lst3 = (from g in lstAllocationGeneralExpenseDetail1
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 1
                    select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst4 = (from g in lstAllocationGeneralExpenseDetail1
                                                           join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                                           where h.ExpenseType == 2
                                                           select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
            var lst5 = (from a in lst4
                        group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            CostsetCode = t.Key.CostsetCode,
                            CostsetName = t.Key.CostsetName,
                            Amount = t.Sum(x => x.AllocatedAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                lst6.Add(cPAllocationGeneralExpenseDetail);
            }
            List<CPPeriod> cPPeriod = ICPPeriodService.GetAll().Where(x => x.Type == 1).ToList();
            CPPeriod period = cPPeriod.FirstOrDefault(x => x.ID == cpu.CPPeriodID);
            CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == cpu.CostSetID);
            List<CPPeriod> cPPeriod1 = cPPeriod.Where(x => x.ToDate < period.FromDate).OrderByDescending(x => x.ToDate).ToList();
            List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll().Where(x => x.CostSetID == cpu.CostSetID).ToList();

            List<RSInwardOutward> LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.Recorded == true).ToList();
            List<RSInwardOutwardDetail> LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.Where(x => x.DebitAccount.StartsWith("155") && x.CreditAccount.StartsWith("154")).ToList();
            Decimal totalQuantity = 0;
            LstRSInwardOutwardDetail1 = (from g in LstRSInwardOutward1
                                         join h in LstRSInwardOutwardDetail1 on g.ID equals h.RSInwardOutwardID
                                         where g.TypeID == 400 && _ListCostSet.Any(x => x == h.CostSetID) && h.PostedDate >= period.FromDate && h.PostedDate <= period.ToDate
                                         select h).ToList();

            Decimal directMatetialAmount = 0;
            Decimal directLaborAmount = 0;
            Decimal generalExpensesAmount = 0;
            Decimal convertquantity = 0;
            Decimal quantity = 0;

            List<CPUncompleteDetail> lstUncompleteDetail = new List<CPUncompleteDetail>();
            if (cpu != null)
            {
                lstUncompleteDetail = cpu.CPUncompleteDetails.ToList();
                foreach (var item1 in lstUncompleteDetail)
                {
                    Decimal quantity1 = item1.Quantity;
                    Decimal percentComplete1 = item1.PercentComplete;
                    convertquantity += quantity1 * percentComplete1 / 100;
                    quantity += quantity1;
                }
            }

            Decimal amount = (from g in lstCPExpense3 select g.Amount).Sum();
            Decimal amount2 = (from g in lstCPExpense4 select g.Amount).Sum();
            Decimal amount3 = (from g in lstCPExpense5 select g.Amount).Sum();
            Decimal amount4 = (from g in lstCPExpense6 select g.Amount).Sum();
            Decimal amount5 = (from g in lstCPExpense7 select g.Amount).Sum();
            Decimal amount6 = (from g in lstCPExpense8 select g.Amount).Sum();
            Decimal allocatedAmount = (from g in lst2 select g.AllocatedAmount).Sum();
            Decimal allocatedAmount1 = (from g in lst3 select g.AllocatedAmount).Sum();
            Decimal allocatedAmount2 = (from g in lst6 select g.AllocatedAmount).Sum();

            if (cPPeriod.Count > 0)
            {
                CPPeriod period1 = (from g in cPPeriod1 join h in cPPeriodDetails on g.ID equals h.CPPeriodID select g).FirstOrDefault();
                if (period1 != null)
                {
                    CPUncomplete uncomplete1 = period1.CPUncompletes.FirstOrDefault(x => x.CostSetID == cpu.CostSetID);
                    if (uncomplete1 != null)
                    {
                        directMatetialAmount = uncomplete1.DirectMaterialAmount;
                        directLaborAmount = uncomplete1.DirectLaborAmount;
                        generalExpensesAmount = uncomplete1.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }
            }
            else
            {
                if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                {
                    directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                    directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                    generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                }
                else
                {
                    directMatetialAmount = 0;
                    directLaborAmount = 0;
                    generalExpensesAmount = 0;
                }
            }

            int uncompletetype = lstUncompleteDetail.FirstOrDefault().UncompleteType;
            totalQuantity = (from g in LstRSInwardOutwardDetail1 where g.CostSetID == cpu.CostSetID select (Decimal)g.Quantity).Sum();
            if (uncompletetype == 0)
            {
                if ((totalQuantity + convertquantity) != 0)
                {
                    cpu.DirectMaterialAmount = (directMatetialAmount + amount + allocatedAmount - amount2) * convertquantity / (totalQuantity + convertquantity);
                    cpu.DirectLaborAmount = (directLaborAmount + amount3 + allocatedAmount1 - amount4) * convertquantity / (totalQuantity + convertquantity);
                    cpu.GeneralExpensesAmount = (generalExpensesAmount + amount5 + allocatedAmount2 - amount6) * convertquantity / (totalQuantity + convertquantity);
                    cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                }
            }
            else if (uncompletetype == 1)
            {
                if ((totalQuantity + quantity) != 0)
                {
                    cpu.DirectMaterialAmount = (directMatetialAmount + amount + allocatedAmount - amount2) * quantity / (totalQuantity + quantity);
                    cpu.DirectLaborAmount = 0;
                    cpu.GeneralExpensesAmount = 0;
                    cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                }
            }
            else if (uncompletetype == 2)
            {
                List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
                List<CPUncompleteDetail> cpUncompleteDetails = cpu.CPUncompleteDetails.ToList();
                List<CPUncomplete> lstUncomplete = new List<CPUncomplete>();
                foreach (var item in cpUncompleteDetails)
                {
                    Decimal directMatetialAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectMaterialAmount).FirstOrDefault();
                    Decimal directLaborAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectLaborAmount).FirstOrDefault();
                    Decimal generalExpensesAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.GeneralExpensesAmount).FirstOrDefault();
                    Decimal quantity1 = item.Quantity;
                    Decimal percentcomplete = item.PercentComplete;
                    CPUncomplete cpu1 = new CPUncomplete();
                    cpu1.CostSetID = item.CostSetID;
                    cpu1.CostsetCode = item.CostsetCode;
                    cpu1.CostsetName = item.CostsetName;
                    cpu1.DirectMaterialAmount = directMatetialAmount1 * quantity * percentcomplete / 100;
                    cpu1.DirectLaborAmount = directLaborAmount1 * quantity * percentcomplete / 100;
                    cpu1.GeneralExpensesAmount = generalExpensesAmount1 * quantity * percentcomplete / 100;
                    cpu1.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    lstUncomplete.Add(cpu1);
                }

                var lst = (from g in lstUncomplete
                           group g by new { g.CostSetID, g.CostsetCode, g.CostsetName } into t
                           select new
                           {
                               CostSetID = t.Key.CostSetID,
                               CostsetCode = t.Key.CostsetCode,
                               CostsetName = t.Key.CostsetName,
                               DirectMaterialAmount = t.Sum(x => x.DirectMaterialAmount),
                               DirectLaborAmount = t.Sum(x => x.DirectLaborAmount),
                               GeneralExpensesAmount = t.Sum(x => x.GeneralExpensesAmount),
                               TotalCostAmount = t.Sum(x => x.TotalCostAmount)
                           }).ToList();

                foreach (var item1 in lst)
                {
                    cpu.DirectMaterialAmount = item1.DirectMaterialAmount;
                    cpu.DirectLaborAmount = item1.DirectLaborAmount;
                    cpu.GeneralExpensesAmount = item1.GeneralExpensesAmount;
                    cpu.TotalCostAmount = item1.TotalCostAmount;
                }
            }

            List<CPUncompleteDetail> cPUncompleteDetails = cpu.CPUncompleteDetails.ToList();
            foreach (var item in cPUncompleteDetails)
            {
                CPResult cpr = ICPResultService.GetAll().FirstOrDefault(x => x.CPPeriodID == cpu.CPPeriodID && x.MaterialGoodsID == item.MaterialGoodsID);
                if (cpr != null)
                {
                    cpr.DirectMaterialAmount = (directMatetialAmount + amount + allocatedAmount - amount2 - cpu.DirectMaterialAmount) * cpr.Coefficien / 100;
                    cpr.DirectLaborAmount = (directLaborAmount + amount3 + allocatedAmount1 - amount4 - cpu.DirectLaborAmount) * cpr.Coefficien / 100;
                    cpr.GeneralExpensesAmount = (generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - cpu.GeneralExpensesAmount) * cpr.Coefficien / 100;
                    cpr.TotalCostAmount = cpr.DirectMaterialAmount + cpr.DirectLaborAmount + cpr.GeneralExpensesAmount;
                    if (cpr.TotalQuantity != 0)
                        cpr.UnitPrice = cpr.TotalCostAmount / cpr.TotalQuantity;
                    else
                        cpr.UnitPrice = 0;
                    Utils.ICPResultService.Update(cpr);
                }
            }

            Utils.ICPUncompleteService.Update(cpu);
            Utils.ICPUncompleteService.CommitTran();
        }

        private List<CPAccountAllocationQuantum> GetCPAccountAllocationQuantum()
        {
            var listCostset = Utils.ListCostSet.ToList();
            var listContractSale = Utils.ListEmContractSA.ToList();
            var listAllocationQuantum = Utils.ListCPAllocationQuantum.ToList();
            var lst1 = (from a in listCostset
                        select new CPAccountAllocationQuantum()
                        {
                            AccountingObjectID = a.ID,
                            AccountingObjectCode = a.CostSetCode,
                            AccountingObjectName = a.CostSetName,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0
                        }).ToList();
            var lst2 = (from a in listContractSale
                        select new CPAccountAllocationQuantum()
                        {
                            AccountingObjectID = a.ID,
                            AccountingObjectCode = a.Code,
                            AccountingObjectName = a.Name,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0
                        }).ToList();
            List<CPAccountAllocationQuantum> lst = new List<CPAccountAllocationQuantum>();
            lst.AddRange(lst1);
            lst.AddRange(lst2);
            foreach (var x in lst)
            {
                var a = listAllocationQuantum.FirstOrDefault(d => d.ID == x.AccountingObjectID);
                if (a != null)
                {
                    x.DirectMaterialAmount = a.DirectMaterialAmount;
                    x.DirectLaborAmount = a.DirectLaborAmount;
                    x.GeneralExpensesAmount = a.GeneralExpensesAmount;
                    x.TotalCostAmount = a.TotalCostAmount;
                }
            }
            return lst;
        }

        private List<CPAllocationRate> GetCPAllocationRates()
        {
            List<CPProductQuantum> lstCPProductQuantum = Utils.ICPProductQuantumService.GetAll();
            lstAllocationRate = new List<CPAllocationRate>();
            if (_LstRSInwardOutwardDetail != null)
                lstAllocationRate = _LstRSInwardOutwardDetail.Where(y => _ListCostSet.Contains(y.CostSetID ?? Guid.Empty))
            .Select(g => new CPAllocationRate()
            {
                CostSetID = (Guid)g.CostSetID,
                CostSetCode = Utils.ICostSetService.Getbykey(g.CostSetID ?? Guid.Empty).CostSetCode,
                MaterialGoodsID = (Guid)g.MaterialGoodsID,
                MaterialGoodsCode = Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsCode,
                MaterialGoodsName = Utils.IMaterialGoodsService.Getbykey((Guid)g.MaterialGoodsID).MaterialGoodsName,
                Quantity = (Decimal)g.Quantity,
                PriceQuantum = (Decimal)(from i in lstCPProductQuantum
                                         where i.ID == g.MaterialGoodsID
                                         select i.TotalCostAmount).FirstOrDefault(),
                Coefficien = 0,
                QuantityStandard = 0,
                AllocatedRate = 0
            }).ToList();

            var lstAlt = (from a in lstAllocationRate
                          group a by new { a.MaterialGoodsID, a.CostSetID, a.MaterialGoodsName, a.CostSetCode, a.MaterialGoodsCode, a.PriceQuantum } into t
                          select new
                          {
                              CostSetID = t.Key.CostSetID,
                              MaterialGoodsID = t.Key.MaterialGoodsID,
                              MaterialGoodsName = t.Key.MaterialGoodsName,
                              MaterialGoodsCode = t.Key.MaterialGoodsCode,
                              CostSetCode = t.Key.CostSetCode,
                              PriceQuantum = t.Key.PriceQuantum,
                              Coefficien = t.Sum(x => x.Coefficien),
                              QuantityStandard = t.Sum(x => x.QuantityStandard),
                              AllocatedRate = t.Sum(x => x.AllocatedRate),
                              Quantity = t.Sum(x => x.Quantity)
                          }).ToList();

            lstAllocationRate1 = new List<CPAllocationRate>();
            if (lstAlt.Count > 0)
            {
                foreach (var item in lstAlt)
                {
                    cpar = new CPAllocationRate();
                    cpar.ID = Guid.NewGuid();
                    cpar.CostSetID = item.CostSetID;
                    cpar.MaterialGoodsID = item.MaterialGoodsID;
                    cpar.MaterialGoodsName = item.MaterialGoodsName;
                    cpar.CostSetCode = item.CostSetCode;
                    cpar.MaterialGoodsCode = item.MaterialGoodsCode;
                    cpar.Coefficien = item.Coefficien;
                    cpar.QuantityStandard = item.QuantityStandard;
                    cpar.Quantity = item.Quantity;
                    cpar.AllocatedRate = item.AllocatedRate;
                    cpar.PriceQuantum = item.PriceQuantum;
                    lstAllocationRate1.Add(cpar);
                }
            }
            else
            {
                lstAllocationRate1 = new List<CPAllocationRate>();
            }


            if (lstAllocationRate1 != null)
                lstAllocationRate1 = lstAllocationRate1.OrderBy(x => x.MaterialGoodsCode).ToList();
            return lstAllocationRate1;
        }

        private List<CPResult> GetCPResults()
        {
            _LstCPResult = new List<CPResult>();
            List<CPResult> lstResult = new List<CPResult>();
            _LstExpenseItem = Utils.ListExpenseItem.ToList();
            _LstCPOPN = Utils.ListCPOPN.ToList();
            List<CPOPN> _LstCPOPN2 = new List<CPOPN>();
            List<CPExpenseList> _LstCPExpense3 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense4 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense5 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense6 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
            List<CPExpenseList> _LstCPExpense7 = new List<CPExpenseList>();
            List<CPExpenseList> _LstCPExpense8 = new List<CPExpenseList>();
            List<CPAllocationGeneralExpenseDetail> lst4 = new List<CPAllocationGeneralExpenseDetail>();
            _LstCPOPN2 = (from g in _LstCPOPN where _ListCostSet.Any(x => x == g.CostSetID) select g).ToList(); //DirectMatetialAmount
            _LstCPExpense3 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            _LstCPExpense4 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 0
                              select g).ToList(); //Amount 
            lst2 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 0
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense5 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            _LstCPExpense6 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 1
                              select g).ToList(); //Amount 
            lst3 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 1
                    select g).ToList(); //AllocatedAmount 
            _LstCPExpense7 = (from g in _LstCPExpense
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 0 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            _LstCPExpense8 = (from g in _LstCPExpense2
                              join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                              where g.TypeVoucher == 1 && h.ExpenseType == 2
                              select g).ToList(); //Amount 
            lst4 = (from g in lstAllocationGeneralExpenseDetail
                    join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                    where h.ExpenseType == 2
                    select g).ToList(); //AllocatedAmount
            List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
            var lst5 = (from a in lst4
                        group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                        select new
                        {
                            CostSetID = t.Key.CostSetID,
                            CostsetCode = t.Key.CostsetCode,
                            CostsetName = t.Key.CostsetName,
                            Amount = t.Sum(x => x.AllocatedAmount)
                        }).ToList();
            foreach (var model in lst5)
            {
                CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                lst6.Add(cPAllocationGeneralExpenseDetail);
            }

            Decimal directMatetialAmount = 0;
            Decimal directLaborAmount = 0;
            Decimal generalExpensesAmount = 0;
            List<CPPeriod> cPPeriod = ICPPeriodService.GetAll();
            List<CPPeriodDetail> cPPeriodDetails = ICPPeriodDetailService.GetAll();

            foreach (var item in _ListCostSet)
            {
                CPResult cpu = new CPResult();
                List<CPUncomplete> cPUncomplete = ICPUncompleteService.Query.Where(x => x.CostSetID == item).ToList();
                CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == item);

                if (cPPeriod.Count > 0)
                {
                    CPPeriod period1 = (from g in cPPeriod
                                        join h in cPPeriodDetails on g.ID equals h.CPPeriodID
                                        where g.ToDate < fromDate
                                        && h.CostSetID == item
                                        select g).OrderByDescending(x => x.ToDate).FirstOrDefault();
                    if (period1 != null)
                    {
                        CPUncomplete uncomplete = cPUncomplete.FirstOrDefault(x => x.CPPeriodID == period1.ID);
                        if (uncomplete != null)
                        {
                            directMatetialAmount = uncomplete.DirectMaterialAmount;
                            directLaborAmount = uncomplete.DirectLaborAmount;
                            generalExpensesAmount = uncomplete.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                    else
                    {
                        if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                        {
                            directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                            directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                            generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }

                Decimal amount = (from g in _LstCPExpense3 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount2 = (from g in _LstCPExpense4 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount3 = (from g in _LstCPExpense5 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount4 = (from g in _LstCPExpense6 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount5 = (from g in _LstCPExpense7 where g.CostSetID == item select g.Amount).Sum();
                Decimal amount6 = (from g in _LstCPExpense8 where g.CostSetID == item select g.Amount).Sum();
                Decimal allocatedAmount = (from g in lst2 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal allocatedAmount1 = (from g in lst3 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal allocatedAmount2 = (from g in lst6 where g.CostSetID == item select g.AllocatedAmount).Sum();
                Decimal directMatetialAmount1 = (from g in _LstCPUncomplete where g.CostSetID == item select g.DirectMaterialAmount).Sum();
                Decimal directLaborAmount1 = (from g in _LstCPUncomplete where g.CostSetID == item select g.DirectLaborAmount).Sum();
                Decimal generalExpensesAmount1 = (from g in _LstCPUncomplete where g.CostSetID == item select g.GeneralExpensesAmount).Sum();

                cpu.DirectMaterialAmount = Math.Round((directMatetialAmount + amount + allocatedAmount - amount2 - directMatetialAmount1), lamtron, MidpointRounding.AwayFromZero);
                cpu.DirectLaborAmount = Math.Round((directLaborAmount + amount3 + allocatedAmount1 - amount4 - directLaborAmount1), lamtron, MidpointRounding.AwayFromZero);
                cpu.GeneralExpensesAmount = Math.Round((generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - generalExpensesAmount1), lamtron, MidpointRounding.AwayFromZero);
                cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                cpu.CostSetID = item;

                lstResult.Add(cpu);
            }

            if (lstAllocationRate.Count > 0)
            {
                #region comment by cuongpv
                //foreach (var item1 in lstAllocationRate)
                //{
                //    CPResult cpu = new CPResult();
                //    cpu.ID = Guid.NewGuid();
                //    cpu.CostSetID = item1.CostSetID;
                //    string costsetCode = Utils.ICostSetService.Getbykey(item1.CostSetID).CostSetCode;
                //    string costsetName = Utils.ICostSetService.Getbykey(item1.CostSetID).CostSetName;
                //    cpu.CostSetCode = costsetCode;
                //    cpu.CostSetName = costsetName;
                //    cpu.MaterialGoodsID = item1.MaterialGoodsID;
                //    cpu.MaterialGoodsCode = item1.MaterialGoodsCode;
                //    cpu.MaterialGoodsName = item1.MaterialGoodsName;
                //    cpu.Coefficien = item1.AllocatedRate;
                //    cpu.TotalQuantity = item1.Quantity;
                //    cpu.DirectMaterialAmount = Math.Round(((from g in lstResult where g.CostSetID == item1.CostSetID select g.DirectMaterialAmount).FirstOrDefault() * cpu.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                //    cpu.DirectLaborAmount = Math.Round(((from g in lstResult where g.CostSetID == item1.CostSetID select g.DirectLaborAmount).FirstOrDefault() * cpu.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                //    cpu.GeneralExpensesAmount = Math.Round(((from g in lstResult where g.CostSetID == item1.CostSetID select g.GeneralExpensesAmount).FirstOrDefault() * cpu.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                //    cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                //    if (cpu.Coefficien != 0)
                //        cpu.UnitPrice = (cpu.TotalCostAmount / cpu.TotalQuantity);
                //    _LstCPResult.Add(cpu);
                //}
                #endregion
                #region add by cuongpv
                foreach (var itemlstResult in lstResult)
                {
                    var _LstCPAllocationRateItemlstResult = lstAllocationRate.Where(x => x.CostSetID == itemlstResult.CostSetID).ToList();
                    _LstCPAllocationRateItemlstResult.OrderBy(x => x.MaterialGoodsCode);
                    int countCPAllocationRate = _LstCPAllocationRateItemlstResult.Count;
                    int maxCount = countCPAllocationRate - 1;
                    Decimal DirectMaterialAmountItemlstResult = itemlstResult.DirectMaterialAmount;
                    Decimal DirectLaborAmountItemlstResult = itemlstResult.DirectLaborAmount;
                    Decimal GeneralExpensesAmountItemlstResult = itemlstResult.GeneralExpensesAmount;
                    for (int i = 0; i < countCPAllocationRate; i++)
                    {
                        CPResult cpu = new CPResult();
                        cpu.ID = Guid.NewGuid();
                        cpu.CostSetID = _LstCPAllocationRateItemlstResult[i].CostSetID;
                        string costsetCode = Utils.ICostSetService.Getbykey(_LstCPAllocationRateItemlstResult[i].CostSetID).CostSetCode;
                        string costsetName = Utils.ICostSetService.Getbykey(_LstCPAllocationRateItemlstResult[i].CostSetID).CostSetName;
                        cpu.CostSetCode = costsetCode;
                        cpu.CostSetName = costsetName;
                        cpu.MaterialGoodsID = _LstCPAllocationRateItemlstResult[i].MaterialGoodsID;
                        cpu.MaterialGoodsCode = _LstCPAllocationRateItemlstResult[i].MaterialGoodsCode;
                        cpu.MaterialGoodsName = _LstCPAllocationRateItemlstResult[i].MaterialGoodsName;
                        cpu.Coefficien = _LstCPAllocationRateItemlstResult[i].AllocatedRate;
                        cpu.TotalQuantity = _LstCPAllocationRateItemlstResult[i].Quantity;
                        if (i == maxCount)
                        {
                            cpu.DirectMaterialAmount = DirectMaterialAmountItemlstResult;
                            cpu.DirectLaborAmount = DirectLaborAmountItemlstResult;
                            cpu.GeneralExpensesAmount = GeneralExpensesAmountItemlstResult;
                        }
                        else
                        {
                            cpu.DirectMaterialAmount = Math.Round((itemlstResult.DirectMaterialAmount * cpu.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpu.DirectLaborAmount = Math.Round((itemlstResult.DirectLaborAmount * cpu.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpu.GeneralExpensesAmount = Math.Round((itemlstResult.GeneralExpensesAmount * cpu.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                        }
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;

                        DirectMaterialAmountItemlstResult = DirectMaterialAmountItemlstResult - cpu.DirectMaterialAmount;
                        DirectLaborAmountItemlstResult = DirectLaborAmountItemlstResult - cpu.DirectLaborAmount;
                        GeneralExpensesAmountItemlstResult = GeneralExpensesAmountItemlstResult - cpu.GeneralExpensesAmount;

                        if (cpu.Coefficien != 0)
                            cpu.UnitPrice = (cpu.TotalCostAmount / cpu.TotalQuantity);
                        _LstCPResult.Add(cpu);
                    }
                }
                #endregion
            }
            else
            {
                _LstCPResult = new List<CPResult>();
            }

            if (_LstCPResult != null)
                _LstCPResult = _LstCPResult.OrderBy(x => x.MaterialGoodsCode).ToList();
            return _LstCPResult;
        }

        private void uGridAllocatedRate_CellChange(object sender, CellEventArgs e)
        {
            //checkupdate = 1;
            var costset = e.Cell.Row.Cells["CostSetID"].Value;

            if (e.Cell.Column.ToString() == "IsStandardItem")
            {
                e.Cell.Row.Cells["Coefficien"].Value = 1;

                foreach (var row in uGridAllocatedRate.Rows)
                {
                    if (!row.Cells["IsStandardItem"].Equals(e.Cell) && costset.ToString() == row.Cells["CostSetID"].Value.ToString())
                    {
                        row.Cells["IsStandardItem"].Value = false;
                        row.Cells["Coefficien"].Value = (decimal)e.Cell.Row.Cells["PriceQuantum"].Value == 0 ? 0 : (Decimal.Parse(row.Cells["PriceQuantum"].Value.ToString()) / Decimal.Parse(e.Cell.Row.Cells["PriceQuantum"].Value.ToString()));
                    }
                }
            }

            //foreach (var y in uGridAllocatedRate.Rows)
            //{
            //    Decimal price = 0;
            //    foreach (var x in uGridAllocatedRate.Rows)
            //    {
            //        Decimal value = Decimal.Parse(x.Cells["Coefficien"].Value.ToString()) * Decimal.Parse(x.Cells["Quantity"].Value.ToString());
            //        if (costset.ToString() == x.Cells["CostSetID"].Value.ToString())
            //        {
            //            x.Cells["QuantityStandard"].Value = value;
            //            price += value;
            //        }
            //    }
            //    if (y.Cells["IsStandardItem"].Equals(e.Cell) && costset.ToString() == y.Cells["CostSetID"].Value.ToString() && price != 0)
            //        y.Cells["AllocatedRate"].Value = Decimal.Parse(y.Cells["QuantityStandard"].Value.ToString()) / price * 100;
            //    if (!y.Cells["IsStandardItem"].Equals(e.Cell) && costset.ToString() == y.Cells["CostSetID"].Value.ToString() && price != 0)
            //        y.Cells["AllocatedRate"].Value = Decimal.Parse(y.Cells["QuantityStandard"].Value.ToString()) / price * 100;
            //}
        }
        private bool CheckCostSet(DateTime fromdate, DateTime todate, Guid costsetID)
        {
            List<CPPeriod> lstCPPeriod = Utils.ListCPPeriod.Where(x => !(x.FromDate > todate || x.ToDate < fromdate) && x.Type == 1).ToList();
            List<CPPeriodDetail> lst = new List<CPPeriodDetail>();
            foreach (var item in lstCPPeriod)
            {
                List<CPPeriodDetail> lstCPPeriodDetail = item.CPPeriodDetails.Where(x => x.CostSetID == costsetID).ToList();
                lst.AddRange(lstCPPeriodDetail);
            }
            if (lst.Count > 0)
                return false;
            else
                return true;
        }
        #endregion Funcition        

        private void uGridUncomplete_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //checkupdate = 1;
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "DirectMaterialAmount" || activeCell.Column.Key == "DirectLaborAmount" || activeCell.Column.Key == "GeneralExpensesAmount")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }

        private void uGridUncompleteDetail_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //checkDtr = 0;
            //checkupdate = 1;
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "PercentComplete" || activeCell.Column.Key == "Quantity")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }

        private void uGridUncomplete_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("DirectMaterialAmount") || e.Cell.Column.Key.Equals("DirectLaborAmount") || e.Cell.Column.Key.Equals("GeneralExpensesAmount") || e.Cell.Column.Key.Equals("AcceptedAmount"))
            {
                //checkupdate = 1;
                UltraGrid ultraGrid = (UltraGrid)sender;
                UltraGridRow row = ultraGrid.ActiveRow;
                if (row.Cells["DirectMaterialAmount"].Text.Trim('_') == "") row.Cells["DirectMaterialAmount"].Value = 0;
                if (row.Cells["DirectLaborAmount"].Text.Trim('_') == "") row.Cells["DirectLaborAmount"].Value = 0;
                if (row.Cells["GeneralExpensesAmount"].Text.Trim('_') == "") row.Cells["GeneralExpensesAmount"].Value = 0;
                ultraGrid.UpdateData();
                if (row != null)
                {
                    row.Cells["TotalCostAmount"].Value = ((decimal)row.Cells["DirectLaborAmount"].Value + (decimal)row.Cells["DirectMaterialAmount"].Value + (decimal)row.Cells["GeneralExpensesAmount"].Value);
                }
            }
        }

        private void uGridAllocationGeneralExpenseResult_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //checkupdate = 1;
            UltraGrid grid = (UltraGrid)sender;
            UltraGridCell activeCell = grid.ActiveCell;
            if (activeCell.Column.Key == "AllocatedRate" || activeCell.Column.Key == "AllocatedAmount")
            {
                if (activeCell.Text.Trim('_', ',') == "")
                    activeCell.Value = 0;
            }
        }

        private void uGridUncompleteDetail_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("PercentComplete"))
            {
                //checkDtr = 0;
                //checkupdate = 1;
                try
                {
                    if (Decimal.Parse(e.Cell.Row.Cells["PercentComplete"].Text.Trim('_').ToString()) <= 100)
                    {
                        uGridUncompleteDetail.DisplayLayout.Bands[0].Columns["PercentComplete"].FormatNumberic(-1);
                    }
                    else
                    {
                        MSG.Warning("% hoàn thành không được quá 100%");
                        return;
                    }
                }
                catch { }
            }
        }

        private void uGridAllocationGeneralExpenseResult_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkupdate = 1;
            if (e.Cell.Column.Key.Equals("AllocatedRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString()) <= 100)
                {
                    var row = uGridAllocationGeneralExpenseResult.ActiveRow;
                    if (row != null)
                    {
                        decimal amount = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList().FirstOrDefault(x => x.ID == (Guid)row.Cells["CPAllocationGeneralExpenseID"].Value).AllocatedAmount;
                        e.Cell.Row.Cells["AllocatedAmount"].Value = (amount * Decimal.Parse(e.Cell.Row.Cells["AllocatedRate"].Value.ToString())) / 100;
                    }
                }
                else
                {

                    MSG.Warning("Tỷ lệ phân bổ không được quá 100%");
                    var row = uGridAllocationGeneralExpenseResult.ActiveRow;
                    if (row != null)
                    {
                        decimal amount = (uGridAllocationGeneralExpense.DataSource as BindingList<CPAllocationGeneralExpense>).ToList().FirstOrDefault(x => x.ID == (Guid)row.Cells["CPAllocationGeneralExpenseID"].Value).AllocatedAmount;
                        e.Cell.Row.Cells["AllocatedRate"].Value = (Decimal.Parse(e.Cell.Row.Cells["AllocatedAmount"].Value.ToString()) / amount) * 100;
                    }
                }
            }
        }

        private void uGridUncompleteDetail_AfterCellUpdate(object sender, CellEventArgs e)
        {
            //checkDtr = 0;
            //checkupdate = 1;
            //List<CPUncompleteDetail> cPUncompleteDetails = ((BindingList<CPUncompleteDetail>)uGridUncompleteDetail.DataSource).Where(x => x.Quantity > 0 || x.PercentComplete > 0).ToList();
            //if (cPUncompleteDetails.Count == 0)
            //    checkDtr = 1;
            if (!IsAdd)
                checkupdate = 1;

            btnUnDetermined_Click(sender, e);
        }

        private void uGridUncomplete_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkupdate = 1;
        }

        private void FCPFactorMethodDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (save == 1)
                isClose = true;
            else
            {
                if (_LstCPExpense != null)
                    ICPExpenseListService.UnbindSession(_LstCPExpense);
                if (_LstCPExpense2 != null)
                    ICPExpenseListService.UnbindSession(_LstCPExpense2);
                if (lstAllocationGeneralExpense != null)
                    ICPAllocationGeneralExpenseService.UnbindSession(lstAllocationGeneralExpense);
                if (lstAllocationGeneralExpenseDetail != null)
                    ICPAllocationGeneralExpenseDetailService.UnbindSession(lstAllocationGeneralExpenseDetail);
                if (_LstCPUncomplete != null)
                    ICPUncompleteService.UnbindSession(_LstCPUncomplete);
                if (_LstCPUncompleteDetail != null)
                    ICPUncompleteDetailService.UnbindSession(_LstCPUncompleteDetail);
                if (lstAllocationRate != null)
                    ICPAllocationRateService.UnbindSession(lstAllocationRate);
                if (lstAllocationRate1 != null)
                    ICPAllocationRateService.UnbindSession(lstAllocationRate1);
                if (cpar != null)
                    ICPAllocationRateService.UnbindSession(cpar);
                if (_LstCPResult != null)
                    ICPResultService.UnbindSession(_LstCPResult);
                isClose = false;
            }
        }

        private void uGridAllocatedRate_AfterCellUpdate(object sender, CellEventArgs e)
        {
            checkupdate = 1;

            var costset = e.Cell.Row.Cells["CostSetID"].Value;

            Decimal price = 0;
            foreach (var x in uGridAllocatedRate.Rows)
            {
                Decimal value = Decimal.Parse(x.Cells["Coefficien"].Value.ToString()) * Decimal.Parse(x.Cells["Quantity"].Value.ToString());
                if (costset.ToString() == x.Cells["CostSetID"].Value.ToString())
                {
                    x.Cells["QuantityStandard"].Value = value;
                    price += value;
                }
            }

            foreach (var y in uGridAllocatedRate.Rows)
            {
                if (costset.ToString() == y.Cells["CostSetID"].Value.ToString() && price != 0)
                    y.Cells["AllocatedRate"].Value = Decimal.Parse(y.Cells["QuantityStandard"].Value.ToString()) / price * 100;
            }
        }
    }
}
