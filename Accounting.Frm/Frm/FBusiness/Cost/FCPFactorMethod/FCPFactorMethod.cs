﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Facilities.TypedFactory.Internal;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FCPFactorMethod : CatalogBase
    {

        #region Khai báo
        List<CPCostingPeriod> lstCPCostingPeriod;
        Decimal uncompleteAmount;
        Decimal expensesDuringPeriod;
        Decimal uncompleteEndOfPeriod;
        Decimal totalCost;
        int lamtron = int.Parse(Utils.ListSystemOption.FirstOrDefault(c => c.Code == "DDSo_TienVND").Data);
        private ICPPeriodService ICPPeriodService
        {
            get { return IoC.Resolve<ICPPeriodService>(); }
        }
        private ICPPeriodDetailService ICPPeriodDetailService
        {
            get { return IoC.Resolve<ICPPeriodDetailService>(); }
        }
        private ICPExpenseListService ICPExpenseListService
        {
            get { return IoC.Resolve<ICPExpenseListService>(); }
        }
        private ICPAllocationGeneralExpenseService ICPAllocationGeneralExpenseService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseService>(); }
        }
        private ICPAllocationGeneralExpenseDetailService ICPAllocationGeneralExpenseDetailService
        {
            get { return IoC.Resolve<ICPAllocationGeneralExpenseDetailService>(); }
        }
        private ICPUncompleteService ICPUncompleteService
        {
            get { return IoC.Resolve<ICPUncompleteService>(); }
        }
        private ICPUncompleteDetailService ICPUncompleteDetailService
        {
            get { return IoC.Resolve<ICPUncompleteDetailService>(); }
        }
        private ICPAllocationRateService ICPAllocationRateService
        {
            get { return IoC.Resolve<ICPAllocationRateService>(); }
        }
        private ICPResultService ICPResultService
        {
            get { return IoC.Resolve<ICPResultService>(); }
        }
        #endregion Khai báo
        #region Khởi tạo
        public FCPFactorMethod()
        {
            WaitingFrm.StartWaiting();
            InitializeComponent();
            LoadCogfig();
            WaitingFrm.StopWaiting();
        }
        #endregion
        #region Hàm xử lý nghiệp vụ
        void LoadCogfig()
        {
            lstCPCostingPeriod = new List<CPCostingPeriod>();
            List<CPPeriod> listCPPeriod = Utils.ListCPPeriod.Where(x => x.Type == 1).ToList();           
            foreach (var item in listCPPeriod)
            {
                List<CPPeriod> cPPeriods = listCPPeriod.Where(x => x.ToDate < item.FromDate).ToList();
                List<CPPeriodDetail> lstCPPeriodDetails = Utils.ListCPPeriodDetail.ToList();
                List<CPPeriodDetail> lstCPPeriodDetail = item.CPPeriodDetails.ToList();
                List<Guid> lstCostset = (from g in lstCPPeriodDetail select g.CostSetID).ToList();
                List<CPOPN> lstCPOPN = Utils.ListCPOPN.ToList();
                List<CPUncomplete> lstCPUncomplete1 = item.CPUncompletes.ToList();
                uncompleteAmount = 0;

                if (cPPeriods.Count == 0)
                {
                    uncompleteAmount = (decimal)(from a in lstCPOPN
                                                 where lstCostset.Any(x => x == a.CostSetID)
                                                 select a.TotalCostAmount).Sum(); // dở dang đầu kỳ
                }
                else
                {
                    List<Guid> listCostSetID = (from g in lstCPPeriodDetail where g.CPPeriodID == item.ID select g.CostSetID).ToList();
                    foreach (var item1 in listCostSetID)
                    {
                        CPPeriod cPPeriod = (from g in cPPeriods join h in lstCPPeriodDetails on g.ID equals h.CPPeriodID where h.CostSetID == item1 select g).OrderByDescending(x => x.ToDate).FirstOrDefault();
                        if (cPPeriod != null)
                        {
                            Decimal uncompleteAmount1 = (from g in cPPeriod.CPUncompletes where g.CostSetID == item1 select g.TotalCostAmount).Sum();
                            uncompleteAmount += uncompleteAmount1; // dở dang đầu kỳ
                        }
                        else
                        {
                            Decimal uncompleteAmount1 = (decimal)(from g in lstCPOPN where g.CostSetID == item1 select g.TotalCostAmount).Sum();
                            uncompleteAmount += uncompleteAmount1; // dở dang đầu kỳ
                        }
                    }
                }

                List<CPExpenseList> listExpene1 = Utils.ListCPExpenseList.Where(x => x.TypeVoucher == 0 && x.CPPeriodID == item.ID).ToList();
                List<CPExpenseList> listExpene2 = Utils.ListCPExpenseList.Where(x => x.TypeVoucher == 1 && x.CPPeriodID == item.ID).ToList();

                Decimal expenseAmount1 = (from a in listExpene1 where lstCostset.Any(x => x == a.CostSetID) select a.Amount).Sum();
                Decimal expenseAmount2 = (from a in listExpene2 where lstCostset.Any(x => x == a.CostSetID) select a.Amount).Sum();
                List<CPAllocationGeneralExpense> lstCPAllocationGeneralExpense1 = Utils.ListCPAllocationGeneralExpense.ToList();
                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail1 = Utils.ListCPAllocationGeneralExpenseDetail.ToList();
                Decimal allocationGeneralExpense = (from g in lstCPAllocationGeneralExpense1
                                                    join h in lstCPAllocationGeneralExpenseDetail1
                                                    on g.ID equals h.CPAllocationGeneralExpenseID
                                                    where g.CPPeriodID == item.ID
                                                    select h.AllocatedAmount).Sum();
                expensesDuringPeriod = expenseAmount1 - expenseAmount2 + allocationGeneralExpense;
                uncompleteEndOfPeriod = (from g in lstCPUncomplete1
                                         select g.TotalCostAmount).Sum();
                try
                {
                    //totalCost =  ICPResultService.Query.Where(x => x.CPPeriodID == item.ID).Sum(c => c.TotalCostAmount);
                    totalCost = Utils.ListCPResult.Where(x => x.CPPeriodID == item.ID).Sum(c => c.TotalCostAmount);
                }
                catch(Exception ex)
                {

                }
                
                CPCostingPeriod cpp = new CPCostingPeriod();
                cpp.ID = item.ID;
                cpp.FromDate = item.FromDate;
                cpp.ToDate = item.ToDate;
                cpp.CostingPeriodName = item.Name;
                cpp.Uncomplete = uncompleteAmount;
                cpp.ExpensesDuringPeriod = expensesDuringPeriod;
                cpp.UncompleteEndOfPeriod = uncompleteEndOfPeriod;
                cpp.TotalCost = totalCost;
                lstCPCostingPeriod.Add(cpp);
            }
            if (lstCPCostingPeriod != null)
                lstCPCostingPeriod = lstCPCostingPeriod.OrderByDescending(x => x.FromDate).ToList();
            uGridDS1.DataSource = new BindingList<CPCostingPeriod>(lstCPCostingPeriod);
            Utils.ConfigGrid(uGridDS1, ConstDatabase.CPCostingPeriod_TableName);
            uGridDS1.DisplayLayout.Bands[0].Columns["Uncomplete"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS1.DisplayLayout.Bands[0].Columns["ExpensesDuringPeriod"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS1.DisplayLayout.Bands[0].Columns["UncompleteEndOfPeriod"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS1.DisplayLayout.Bands[0].Columns["TotalCost"].FormatNumberic(ConstDatabase.Format_TienVND);

            Utils.AddSumColumn(uGridDS1, "Uncomplete", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS1, "ExpensesDuringPeriod", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS1, "UncompleteEndOfPeriod", false, "", ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridDS1, "TotalCost", false, "", ConstDatabase.Format_TienVND);

            if (uGridDS1.Rows.Count > 0)
            {
                CPCostingPeriod cpp = uGridDS1.Rows[0].ListObject as CPCostingPeriod;
                ConfigControl(cpp);
            }

            else
            {
                List<CPExpenseList> lstCPExpenseList = new List<CPExpenseList>();
                uGridCPExpenseList1.DataSource = new BindingList<CPExpenseList>(lstCPExpenseList);
                Utils.ConfigGrid(uGridCPExpenseList1, ConstDatabase.CPExpenseList_TableName);
                var grdband = uGridCPExpenseList1.DisplayLayout.Bands[0];
                grdband.Columns["ExpenseItemCode"].Hidden = true;
                uGridCPExpenseList1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                List<CPExpenseList> lstCPExpenseList2 = new List<CPExpenseList>();
                uGridCPExpenseList2.DataSource = new BindingList<CPExpenseList>(lstCPExpenseList2);
                Utils.ConfigGrid(uGridCPExpenseList2, ConstDatabase.CPExpenseList_TableName);
                var grdband1 = uGridCPExpenseList2.DisplayLayout.Bands[0];
                grdband1.Columns["ExpenseItemCode"].Hidden = true;
                uGridCPExpenseList2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                List<CPAllocationGeneralExpenseDetail> lstCPAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();
                uGridCPAllocationGeneralExpenseDetail.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstCPAllocationGeneralExpenseDetail);
                Utils.ConfigGrid(uGridCPAllocationGeneralExpenseDetail, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
                uGridCPAllocationGeneralExpenseDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                List<CPUncomplete> lstCPUncomplete = new List<CPUncomplete>();
                uGridCPUncomplete.DataSource = new BindingList<CPUncomplete>(lstCPUncomplete);
                Utils.ConfigGrid(uGridCPUncomplete, ConstDatabase.CPUncomplete_TableName);
                uGridCPUncomplete.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                List<CPAllocationRate> lstAllocationRate = new List<CPAllocationRate>();
                uGridCPAllowcationRate.DataSource = new BindingList<CPAllocationRate>(lstAllocationRate);
                Utils.ConfigGrid(uGridCPAllowcationRate, ConstDatabase.CPAllocationRate_TableName);
                uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["AllocationStandard"].Hidden = true;
                uGridCPAllowcationRate.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                List<CPResult> lstCPResult = new List<CPResult>();
                uGridResult.DataSource = new BindingList<CPResult>(lstCPResult);
                Utils.ConfigGrid(uGridResult, ConstDatabase.CPCostResult_TableName);
                uGridResult.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
        }
        #endregion
        #region events
        private void btnAddContract_Click(object sender, EventArgs e)
        {
            FCPFactorMethodDetail frm = new FCPFactorMethodDetail();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog();
            if (FCPFactorMethodDetail.isClose)
            {
                Utils.ClearCacheByType<CPExpenseList>();
                Utils.ClearCacheByType<CPPeriod>();
                Utils.ClearCacheByType<CPPeriodDetail>();
                Utils.ClearCacheByType<CPAllocationGeneralExpense>();
                Utils.ClearCacheByType<CPAllocationGeneralExpenseDetail>();
                Utils.ClearCacheByType<CPAllocationRate>();
                Update(Utils.ListCPPeriod.Where(x => x.Type == 1).ToList());
                LoadCogfig();
            }
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        protected override void DeleteFunction()
        {
            List<CPPeriod> lstCPPeriod = ICPPeriodService.GetAll();
            if (uGridDS1.Selected.Rows.Count > 0)
            {
                CPCostingPeriod temp = uGridDS1.Selected.Rows[0].ListObject as CPCostingPeriod;
                CPPeriod temp1 = ICPPeriodService.GetAll().Where(x => x.ID == temp.ID).FirstOrDefault();
                //if (temp1.IsDelete)
                //{
                var model = ICPPeriodService.GetAll().FirstOrDefault(x => x.ID == temp.ID);
                List<CPPeriod> cPPeriods = lstCPPeriod.Where(x => x.FromDate > model.ToDate).ToList();
                if (model != null && MSG.Question(string.Format(resSystem.MSG_System_81, model.Name)) == DialogResult.Yes)
                    {
                    List<CPAllocationGeneralExpense> lst = ICPAllocationGeneralExpenseService.GetAll();
                    List<CPAllocationGeneralExpense> lst1 = lst.Where(x => x.CPPeriodID == model.ID && x.AllocatedRate > 0).ToList();
                    List<CPAllocationGeneralExpense> lst2 = lst.Where(x => x.CPPeriodID == model.ID && x.AllocatedRate == 0).ToList();
                    try
                    {
                        Utils.ICPPeriodService.BeginTran();
                        foreach (var item in lst1)
                        {
                            CPAllocationGeneralExpense item1 = ICPAllocationGeneralExpenseService.GetAll().FirstOrDefault(x => x.ReferenceID == item.ReferenceID && x.AllocatedRate == 0);
                            lst2.Remove(item1);
                            model.CPAllocationGeneralExpenses.Remove(item);
                        }
                        foreach (var item2 in lst2)
                        {
                            item2.CPPeriodID = Guid.NewGuid();
                            Utils.ICPAllocationGeneralExpenseService.Update(item2);
                            model.CPAllocationGeneralExpenses.Remove(item2);
                        }
                        foreach (var item in lst1)
                        {
                            if (item.AllocatedRate == 100)
                            {
                                item.CPPeriodID = Guid.NewGuid();
                                item.AllocatedRate = 0;
                                item.UnallocatedAmount = item.TotalCost;
                                item.AllocatedAmount = 0;
                                Utils.ICPAllocationGeneralExpenseService.Update(item);
                            }
                            else
                            {
                                int i = 0;
                                List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = (from g in lst
                                                                                                join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                                where g.ReferenceID == item.ReferenceID && g.AllocatedRate > 0
                                                                                                orderby h.FromDate
                                                                                                select g).ToList();
                                if (cPAllocationGeneralExpenses.Count > 0)
                                {
                                    foreach (var item1 in cPAllocationGeneralExpenses)
                                    {
                                        if (i == 0)
                                        {
                                            item1.UnallocatedAmount += item.AllocatedAmount;
                                        }
                                        else
                                        {
                                            CPPeriod period = cPPeriods.FirstOrDefault(x => x.ID == item1.CPPeriodID);
                                            CPAllocationGeneralExpense cPAllocationGeneralExpense = (from g in cPAllocationGeneralExpenses
                                                                                                     join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                                     where h.ToDate < period.FromDate
                                                                                                     select g).FirstOrDefault();
                                            if (cPAllocationGeneralExpense != null)
                                            {
                                                item1.UnallocatedAmount += cPAllocationGeneralExpense.AllocatedAmount;
                                            }
                                            else
                                            {
                                                item1.UnallocatedAmount += item.AllocatedAmount;
                                            }
                                        }
                                        item1.AllocatedAmount = item1.UnallocatedAmount * item1.AllocatedRate / 100;
                                        ICPAllocationGeneralExpenseService.Update(item1);
                                        i++;
                                    }
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense1 = (from g in lst
                                                                                              join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                              where g.ReferenceID == item.ReferenceID && g.AllocatedRate == 0
                                                                                              select g).FirstOrDefault();
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense2 = (from g in lst
                                                                                              join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                              where g.ReferenceID == item.ReferenceID && g.AllocatedRate > 0
                                                                                              select g).OrderByDescending(x => x.UnallocatedAmount).FirstOrDefault();
                                    if (cPAllocationGeneralExpense1 != null)
                                    {
                                        cPAllocationGeneralExpense1.UnallocatedAmount += cPAllocationGeneralExpense2.AllocatedAmount;
                                        cPAllocationGeneralExpense1.AllocatedAmount = cPAllocationGeneralExpense1.UnallocatedAmount * cPAllocationGeneralExpense1.AllocatedRate / 100;
                                        ICPAllocationGeneralExpenseService.Update(cPAllocationGeneralExpense1);
                                    }
                                }
                                else
                                {
                                    CPAllocationGeneralExpense cPAllocationGeneralExpense = (from g in lst
                                                                                             join h in cPPeriods on g.CPPeriodID equals h.ID
                                                                                             where g.ReferenceID == item.ReferenceID && g.AllocatedRate == 0
                                                                                             orderby h.FromDate
                                                                                             select g).FirstOrDefault();
                                    if (cPAllocationGeneralExpense != null)
                                        ICPAllocationGeneralExpenseService.Delete(cPAllocationGeneralExpense);
                                }
                            }

                            List<CPAllocationGeneralExpenseDetail> cPAllocationGeneralExpenseDetails = ICPAllocationGeneralExpenseDetailService.GetAll().Where(x => x.CPAllocationGeneralExpenseID == item.ID).ToList();
                            foreach (var itemdetail in cPAllocationGeneralExpenseDetails)
                            {
                                ICPAllocationGeneralExpenseDetailService.Delete(itemdetail);
                            }
                            Utils.ICPAllocationGeneralExpenseService.Delete(item);
                        }

                        Utils.ICPPeriodService.Delete(model);
                        Utils.ICPPeriodService.CommitTran();

                        // Cập nhật dở dang
                        if (cPPeriods.Count > 0)
                        {
                            foreach (var item in cPPeriods)
                            {
                                List<CPUncomplete> cPUncompletes = item.CPUncompletes.ToList();
                                if (cPUncompletes.Count > 0)
                                {
                                    foreach (var item1 in cPUncompletes)
                                    {
                                        GetLoopCPUncomplete(item1, item);
                                    }
                                }
                            }
                        }

                        List<CPPeriod> lstCPPeriod1 = ICPPeriodService.GetAll();
                        if (lstCPPeriod1.Count == 0)
                        {
                            ICPAllocationGeneralExpenseService.BeginTran();
                            List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = ICPAllocationGeneralExpenseService.GetAll();
                            foreach (var item in cPAllocationGeneralExpenses)
                            {
                                ICPAllocationGeneralExpenseService.Delete(item);
                            }
                            ICPAllocationGeneralExpenseService.CommitTran();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.ICPPeriodService.RolbackTran();
                    }
                    Utils.ClearCacheByType<CPExpenseList>();
                    Utils.ClearCacheByType<CPPeriod>();
                    Utils.ClearCacheByType<CPPeriodDetail>();
                    Utils.ClearCacheByType<CPAllocationGeneralExpense>();
                    Utils.ClearCacheByType<CPAllocationGeneralExpenseDetail>();
                    Utils.ClearCacheByType<CPAllocationRate>();
                    Update(Utils.ListCPPeriod.Where(x => x.Type == 1).ToList());
                    LoadCogfig();
                }
                //}
                //else
                //{
                //    MSG.Warning("Không thể xóa kỳ tính giá thành này vì đã cập nhật giá nhập/xuất kho");
                //    return;
                //}
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        protected override void EditFunction()
        {
            if (uGridDS1.Selected.Rows.Count > 0)
            {
                CPCostingPeriod temp = uGridDS1.ActiveRow != null ? uGridDS1.ActiveRow.ListObject as CPCostingPeriod : uGridDS1.Selected.Rows[0].ListObject as CPCostingPeriod;
                var model = ICPPeriodService.Getbykey(temp.ID);
                ICPPeriodService.UnbindSession(model);
                model = ICPPeriodService.Getbykey(temp.ID);
                new FCPFactorMethodDetail(model).ShowDialog(this);

                if (FCPFactorMethodDetail.isClose)
                {
                    Utils.ClearCacheByType<CPExpenseList>();
                    Utils.ClearCacheByType<CPPeriod>();
                    Utils.ClearCacheByType<CPPeriodDetail>();
                    Utils.ClearCacheByType<CPAllocationGeneralExpense>();
                    Utils.ClearCacheByType<CPAllocationGeneralExpenseDetail>();
                    Utils.ClearCacheByType<CPAllocationRate>();
                    Update(Utils.ListCPPeriod.Where(x => x.Type == 1).ToList());
                    LoadCogfig();
                }
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadCogfig();
        }

        private void uGridDS1_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        #endregion

        private void uGridDS1_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }

        private void uGridDS1_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGridDS1.Rows.Count > 0)
            {
                if (uGridDS1.Selected.Rows.Count > 0)
                {
                    CPCostingPeriod cpp = uGridDS1.Selected.Rows[0].ListObject as CPCostingPeriod;
                    ConfigControl(cpp);
                }
            }
        }

        private void ConfigControl(CPCostingPeriod cpp)
        {
            List<CPExpenseList> lstCPExpenseList = Utils.ListCPExpenseList.Where(x => x.TypeVoucher == 0 && x.CPPeriodID == cpp.ID).ToList();
            foreach (var item in lstCPExpenseList)
            {
                item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
            }
            uGridCPExpenseList1.DataSource = new BindingList<CPExpenseList>(lstCPExpenseList.OrderBy(x => x.CostsetCode).ToList());
            Utils.ConfigGrid(uGridCPExpenseList1, ConstDatabase.CPExpenseList_TableName);
            uGridCPExpenseList1.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].Hidden = true;
            uGridCPExpenseList1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCPExpenseList1.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPExpenseList1, "Amount", false, "", ConstDatabase.Format_TienVND);

            List<CPExpenseList> lstCPExpenseList2 = Utils.ListCPExpenseList.Where(x => x.TypeVoucher == 1 && x.CPPeriodID == cpp.ID).ToList();
            foreach (var item in lstCPExpenseList2)
            {
                item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
                item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
            }
            uGridCPExpenseList2.DataSource = new BindingList<CPExpenseList>(lstCPExpenseList2.OrderBy(x => x.CostsetCode).ToList());
            Utils.ConfigGrid(uGridCPExpenseList2, ConstDatabase.CPExpenseList_TableName);
            uGridCPExpenseList2.DisplayLayout.Bands[0].Columns["ExpenseItemCode"].Hidden = true;
            uGridCPExpenseList2.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCPExpenseList2.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPExpenseList2, "Amount", false, "", ConstDatabase.Format_TienVND);

            List<CPAllocationGeneralExpenseDetail>  lstCPAllocationGeneralExpenseDetail = new List<CPAllocationGeneralExpenseDetail>();
            lstCPAllocationGeneralExpenseDetail = Utils.ListCPAllocationGeneralExpense.Where(x => x.CPPeriodID == cpp.ID).SelectMany(x => x.CPAllocationGeneralExpenseDetails).ToList();
            foreach (var item in lstCPAllocationGeneralExpenseDetail)
            {
                item.ExpenseItemCode = Utils.ListExpenseItem.FirstOrDefault(x => x.ID == item.ExpenseItemID).ExpenseItemCode;
                item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
            }
            uGridCPAllocationGeneralExpenseDetail.DataSource = new BindingList<CPAllocationGeneralExpenseDetail>(lstCPAllocationGeneralExpenseDetail.OrderBy(x => x.CostsetCode).ToList());
            Utils.ConfigGrid(uGridCPAllocationGeneralExpenseDetail, ConstDatabase.CPAllocationGeneralExpenseDetail_TableName);
            uGridCPAllocationGeneralExpenseDetail.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
            uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Bands[0].Columns["AllocatedAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPAllocationGeneralExpenseDetail, "AllocatedAmount", false, "", ConstDatabase.Format_TienVND);

            List<CPUncomplete> lstCPUncomplete = /*ICPUncompleteService.Query*/Utils.ListCPUncomplete.Where(x => x.CPPeriodID == cpp.ID).ToList();
            foreach (var item in lstCPUncomplete)
            {
                item.CostsetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                item.CostsetName = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetName;
            }
            uGridCPUncomplete.DataSource = new BindingList<CPUncomplete>(lstCPUncomplete.OrderBy(x => x.CostsetCode).ToList());
            Utils.ConfigGrid(uGridCPUncomplete, ConstDatabase.CPUncomplete_TableName);
            uGridCPUncomplete.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCPUncomplete.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPUncomplete, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCPUncomplete.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPUncomplete, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCPUncomplete.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPUncomplete, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            uGridCPUncomplete.DisplayLayout.Bands[0].Columns["TotalCostAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridCPUncomplete, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);

            List<CPAllocationRate> lstAllocationRate = Utils.ListCPAllocationRate.Where(x => x.CPPeriodID == cpp.ID).ToList();
            foreach (var item in lstAllocationRate)
            {
                item.CostSetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                item.MaterialGoodsCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                item.MaterialGoodsName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
            }
            uGridCPAllowcationRate.DataSource = new BindingList<CPAllocationRate>(lstAllocationRate.OrderBy(x => x.MaterialGoodsCode).ToList());
            Utils.ConfigGrid(uGridCPAllowcationRate, ConstDatabase.CPAllocationRate_TableName);
            uGridCPAllowcationRate.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["Quantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            //Utils.AddSumColumn(uGridCPAllowcationRate, "Quantity", false, "", ConstDatabase.Format_Quantity);
            uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["PriceQuantum"].FormatNumberic(ConstDatabase.Format_TienVND);
            //Utils.AddSumColumn(uGridCPAllowcationRate, "PriceQuantum", false, "", ConstDatabase.Format_TienVND);
            uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["Coefficien"].FormatNumberic(-1);
            //Utils.AddSumColumn(uGridCPAllowcationRate, "Coefficien", false, "", -1);
            uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["QuantityStandard"].FormatNumberic(ConstDatabase.Format_Quantity);
            //Utils.AddSumColumn(uGridCPAllowcationRate, "QuantityStandard", false, "", ConstDatabase.Format_Quantity);
            uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["AllocatedRate"].FormatNumberic(-1);
            //Utils.AddSumColumn(uGridCPAllowcationRate, "AllocatedRate ", false, "", -1);
            uGridCPAllowcationRate.DisplayLayout.Bands[0].Columns["AllocationStandard"].Hidden = true;

            List<CPResult> lstCPResult = ICPResultService.Query.Where(x => x.CPPeriodID == cpp.ID).ToList();
            foreach (var item in lstCPResult)
            {
                item.CostSetCode = Utils.ListCostSet.FirstOrDefault(x => x.ID == item.CostSetID).CostSetCode;
                item.MaterialGoodsCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsCode;
                item.MaterialGoodsName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == item.MaterialGoodsID).MaterialGoodsName;
            }
            uGridResult.DataSource = new BindingList<CPResult>(lstCPResult.OrderBy(x => x.MaterialGoodsCode).ToList());//edit by cuongpv CostSetCode -> MaterialGoodsCode
            Utils.ConfigGrid(uGridResult, ConstDatabase.CPCostResult_TableName);
            uGridResult.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridResult.DisplayLayout.Bands[0].Columns["Coefficien"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridResult, "Coefficien", false, "", ConstDatabase.Format_TienVND);
            uGridResult.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridResult, "DirectMaterialAmount", false, "", ConstDatabase.Format_TienVND);
            uGridResult.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridResult, "DirectLaborAmount", false, "", ConstDatabase.Format_TienVND);
            uGridResult.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridResult, "GeneralExpensesAmount", false, "", ConstDatabase.Format_TienVND);
            uGridResult.DisplayLayout.Bands[0].Columns["TotalCostAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            Utils.AddSumColumn(uGridResult, "TotalCostAmount", false, "", ConstDatabase.Format_TienVND);
            uGridResult.DisplayLayout.Bands[0].Columns["TotalQuantity"].FormatNumberic(ConstDatabase.Format_Quantity);
            Utils.AddSumColumn(uGridResult, "TotalQuantity", false, "", ConstDatabase.Format_Quantity);
            uGridResult.DisplayLayout.Bands[0].Columns["UnitPrice"].FormatNumberic(ConstDatabase.Format_ForeignCurrency);
            Utils.AddSumColumn(uGridResult, "UnitPrice", false, "", ConstDatabase.Format_ForeignCurrency);
        }

        private void GetLoopCPUncomplete(CPUncomplete cpu, CPPeriod cpp)
        {
            Utils.ICPUncompleteService.BeginTran();//add by cuongpv
            try
            {
                Utils.ICPExpenseListService.UnbindSession(Utils.ListCPExpenseList);
                Utils.ICPAllocationRateService.UnbindSession(Utils.ListCPAllocationRate);
                List<CPExpenseList> lstCPExpense = cpp.CPExpenseLists.Where(x => x.TypeVoucher == 0 && x.CostSetID == cpu.CostSetID).ToList().CloneObject(); // NVL trực tiếp
                List<CPExpenseList> lstCPExpense2 = cpp.CPExpenseLists.Where(x => x.TypeVoucher == 1 && x.CostSetID == cpu.CostSetID).ToList().CloneObject(); // Giảm giá thành
                List<CPAllocationGeneralExpense> lstAllocationGeneralExpense1 = cpp.CPAllocationGeneralExpenses.ToList(); // Phân bổ chung 
                List<CPAllocationGeneralExpenseDetail> lstAllocationGeneralExpenseDetail1 = lstAllocationGeneralExpense1.SelectMany(x=>x.CPAllocationGeneralExpenseDetails).ToList().Where(x => x.CostSetID == cpu.CostSetID).ToList();
                List<ExpenseItem> _LstExpenseItem = Utils.ListExpenseItem.ToList();
                List<CPOPN> _LstCPOPN = Utils.ListCPOPN.ToList();
                List<Guid> listCostSetID = cpp.CPPeriodDetails.Select(c=>c.CostSetID).ToList();
                List<CPAllocationGeneralExpenseDetail> lst2 = new List<CPAllocationGeneralExpenseDetail>();
                List<CPAllocationGeneralExpenseDetail> lst3 = new List<CPAllocationGeneralExpenseDetail>();
                lst2 = (from g in lstAllocationGeneralExpenseDetail1
                        join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                        where h.ExpenseType == 0
                        select g).ToList(); //AllocatedAmount   
                lst3 = (from g in lstAllocationGeneralExpenseDetail1
                        join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                        where h.ExpenseType == 1
                        select g).ToList(); //AllocatedAmount
                List<CPAllocationGeneralExpenseDetail> lst4 = (from g in lstAllocationGeneralExpenseDetail1
                                                               join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                                               where h.ExpenseType == 2
                                                               select g).ToList(); //AllocatedAmount
                List<CPAllocationGeneralExpenseDetail> lst6 = new List<CPAllocationGeneralExpenseDetail>();
                var lst5 = (from a in lst4
                            group a by new { a.CostSetID, a.CostsetCode, a.CostsetName } into t
                            select new
                            {
                                CostSetID = t.Key.CostSetID,
                                CostsetCode = t.Key.CostsetCode,
                                CostsetName = t.Key.CostsetName,
                                Amount = t.Sum(x => x.AllocatedAmount)
                            }).ToList();
                foreach (var model in lst5)
                {
                    CPAllocationGeneralExpenseDetail cPAllocationGeneralExpenseDetail = new CPAllocationGeneralExpenseDetail();
                    cPAllocationGeneralExpenseDetail.CostSetID = model.CostSetID;
                    cPAllocationGeneralExpenseDetail.CostsetCode = model.CostsetCode;
                    cPAllocationGeneralExpenseDetail.CostsetName = model.CostsetName;
                    cPAllocationGeneralExpenseDetail.AllocatedAmount = model.Amount;
                    lst6.Add(cPAllocationGeneralExpenseDetail);
                }
                List<CPPeriod> cPPeriod = Utils.ListCPPeriod.Where(x => x.Type == 1).ToList();
                CPPeriod period = cPPeriod.FirstOrDefault(x => x.ID == cpu.CPPeriodID);
                CPOPN cpopn = _LstCPOPN.FirstOrDefault(x => x.CostSetID == cpu.CostSetID);
                List<CPPeriod> cPPeriod1 = cPPeriod.Where(x => x.ToDate < period.FromDate).OrderByDescending(x => x.ToDate).ToList();
                List<CPPeriodDetail> cPPeriodDetails = Utils.ListCPPeriodDetail.Where(x => x.CostSetID == cpu.CostSetID).ToList();

                List<RSInwardOutward> LstRSInwardOutward1 = Utils.ListRSInwardOutward.Where(x => x.Recorded == true && x.TypeID == 400).ToList();
                List<RSInwardOutwardDetail> LstRSInwardOutwardDetail1 = Utils.ListRSInwardOutwardDetail.ToList().Where(h =>
                listCostSetID.Any(x => x == h.CostSetID) && h.DebitAccount.StartsWith("155") && h.CreditAccount.StartsWith("154") && h.PostedDate >= period.FromDate && h.PostedDate <= period.ToDate
                && LstRSInwardOutward1.Any(d => d.ID == h.RSInwardOutwardID)).ToList();
                Decimal totalQuantity = 0;
                Decimal directMatetialAmount = 0;
                Decimal directLaborAmount = 0;
                Decimal generalExpensesAmount = 0;
                Decimal convertquantity = 0;
                Decimal quantity = 0;

                List<CPUncompleteDetail> lstUncompleteDetail = new List<CPUncompleteDetail>();
                if (cpu != null)
                {
                    lstUncompleteDetail = cpu.CPUncompleteDetails.ToList();
                    foreach (var item1 in lstUncompleteDetail)
                    {
                        Decimal quantity1 = item1.Quantity;
                        Decimal percentComplete1 = item1.PercentComplete;
                        convertquantity += quantity1 * percentComplete1 / 100;
                        quantity += quantity1;
                    }
                }

                Decimal amount = (from g in lstCPExpense
                                   join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                   where h.ExpenseType == 0
                                   select g.Amount).Sum();
                Decimal amount2 = (from g in lstCPExpense2
                                   join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                   where h.ExpenseType == 0
                                   select g.Amount).Sum();
                Decimal amount3 = (from g in lstCPExpense
                                   join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                   where h.ExpenseType == 1
                                   select g.Amount).Sum();
                Decimal amount4 = (from g in lstCPExpense2
                                   join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                   where h.ExpenseType == 1
                                   select g.Amount).Sum();
                Decimal amount5 = (from g in lstCPExpense
                                   join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                   where h.ExpenseType == 2
                                   select g.Amount).Sum();
                Decimal amount6 = (from g in lstCPExpense2
                                   join h in _LstExpenseItem on g.ExpenseItemID equals h.ID
                                   where h.ExpenseType == 2
                                   select g.Amount).Sum();
                Decimal allocatedAmount = (from g in lst2 select g.AllocatedAmount).Sum();
                Decimal allocatedAmount1 = (from g in lst3 select g.AllocatedAmount).Sum();
                Decimal allocatedAmount2 = (from g in lst6 select g.AllocatedAmount).Sum();

                if (cPPeriod.Count > 0)
                {
                    CPPeriod period1 = (from g in cPPeriod1 join h in cPPeriodDetails on g.ID equals h.CPPeriodID select g).FirstOrDefault();
                    if (period1 != null)
                    {
                        CPUncomplete uncomplete1 = period1.CPUncompletes.FirstOrDefault(x => x.CostSetID == cpu.CostSetID);
                        if (uncomplete1 != null)
                        {
                            directMatetialAmount = uncomplete1.DirectMaterialAmount;
                            directLaborAmount = uncomplete1.DirectLaborAmount;
                            generalExpensesAmount = uncomplete1.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                    else
                    {
                        if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                        {
                            directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                            directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                            generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                        }
                        else
                        {
                            directMatetialAmount = 0;
                            directLaborAmount = 0;
                            generalExpensesAmount = 0;
                        }
                    }
                }
                else
                {
                    if ((cpopn != null && cpopn.TotalCostAmount != 0) || cpopn != null)
                    {
                        directMatetialAmount = (Decimal)cpopn.DirectMaterialAmount;
                        directLaborAmount = (Decimal)cpopn.DirectLaborAmount;
                        generalExpensesAmount = (Decimal)cpopn.GeneralExpensesAmount;
                    }
                    else
                    {
                        directMatetialAmount = 0;
                        directLaborAmount = 0;
                        generalExpensesAmount = 0;
                    }
                }

                int uncompletetype = lstUncompleteDetail.FirstOrDefault().UncompleteType;
                totalQuantity = (from g in LstRSInwardOutwardDetail1 where g.CostSetID == cpu.CostSetID select (Decimal)g.Quantity).Sum();
                if (uncompletetype == 0)
                {
                    if ((totalQuantity + convertquantity) != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round(((directMatetialAmount + amount + allocatedAmount - amount2) * convertquantity / (totalQuantity + convertquantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = Math.Round(((directLaborAmount + amount3 + allocatedAmount1 - amount4) * convertquantity / (totalQuantity + convertquantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.GeneralExpensesAmount = Math.Round(((generalExpensesAmount + amount5 + allocatedAmount2 - amount6) * convertquantity / (totalQuantity + convertquantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }
                else if (uncompletetype == 1)
                {
                    if ((totalQuantity + quantity) != 0)
                    {
                        cpu.DirectMaterialAmount = Math.Round(((directMatetialAmount + amount + allocatedAmount - amount2) *quantity / (totalQuantity + quantity)), lamtron, MidpointRounding.AwayFromZero);
                        cpu.DirectLaborAmount = 0;
                        cpu.GeneralExpensesAmount = 0;
                        cpu.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                    }
                }
                else if (uncompletetype == 2)
                {
                    List<CPMaterialProductQuantum> lstCPMaterialProductQuantum = Utils.ICPProductQuantumService.GetAllByType();
                    List<CPUncompleteDetail> cpUncompleteDetails = cpu.CPUncompleteDetails.ToList();
                    List<CPUncomplete> lstUncomplete = new List<CPUncomplete>();
                    foreach (var item in cpUncompleteDetails)
                    {
                        Decimal directMatetialAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectMaterialAmount).FirstOrDefault();
                        Decimal directLaborAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.DirectLaborAmount).FirstOrDefault();
                        Decimal generalExpensesAmount1 = (from g in lstCPMaterialProductQuantum where g.MaterialGoodsID == item.MaterialGoodsID select (Decimal)g.GeneralExpensesAmount).FirstOrDefault();
                        Decimal quantity1 = item.Quantity;
                        Decimal percentcomplete = item.PercentComplete;
                        CPUncomplete cpu1 = new CPUncomplete();
                        cpu1.CostSetID = item.CostSetID;
                        cpu1.CostsetCode = item.CostsetCode;
                        cpu1.CostsetName = item.CostsetName;
                        cpu1.DirectMaterialAmount = Math.Round((directMatetialAmount1 * quantity * percentcomplete / 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu1.DirectLaborAmount = Math.Round((directLaborAmount1 * quantity * percentcomplete / 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu1.GeneralExpensesAmount = Math.Round((generalExpensesAmount1 * quantity * percentcomplete / 100), lamtron, MidpointRounding.AwayFromZero);
                        cpu1.TotalCostAmount = cpu.DirectMaterialAmount + cpu.DirectLaborAmount + cpu.GeneralExpensesAmount;
                        lstUncomplete.Add(cpu1);
                    }

                    var lst = (from g in lstUncomplete
                               group g by new { g.CostSetID, g.CostsetCode, g.CostsetName } into t
                               select new
                               {
                                   CostSetID = t.Key.CostSetID,
                                   CostsetCode = t.Key.CostsetCode,
                                   CostsetName = t.Key.CostsetName,
                                   DirectMaterialAmount = t.Sum(x => x.DirectMaterialAmount),
                                   DirectLaborAmount = t.Sum(x => x.DirectLaborAmount),
                                   GeneralExpensesAmount = t.Sum(x => x.GeneralExpensesAmount),
                                   TotalCostAmount = t.Sum(x => x.TotalCostAmount)
                               }).ToList();

                    foreach (var item1 in lst)
                    {
                        cpu.DirectMaterialAmount = item1.DirectMaterialAmount;
                        cpu.DirectLaborAmount = item1.DirectLaborAmount;
                        cpu.GeneralExpensesAmount = item1.GeneralExpensesAmount;
                        cpu.TotalCostAmount = item1.TotalCostAmount;
                    }
                }
                //Utils.ICPUncompleteService.BeginTran();comment by cuongpv
                List<CPUncompleteDetail> cPUncompleteDetails = cpu.CPUncompleteDetails.ToList();
                #region comment by cuongpv
                //foreach (var item in cPUncompleteDetails)
                //{
                //    CPResult cpr = cpp.CPResults.FirstOrDefault(x => x.MaterialGoodsID == item.MaterialGoodsID);
                //    if (cpr != null)
                //    {
                //        cpr.DirectMaterialAmount = Math.Round(((directMatetialAmount + amount + allocatedAmount - amount2 - cpu.DirectMaterialAmount) * cpr.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                //        cpr.DirectLaborAmount = Math.Round(((directLaborAmount + amount3 + allocatedAmount1 - amount4 - cpu.DirectLaborAmount) * cpr.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                //        cpr.GeneralExpensesAmount = Math.Round(((generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - cpu.GeneralExpensesAmount) * cpr.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                //        cpr.TotalCostAmount = cpr.DirectMaterialAmount + cpr.DirectLaborAmount + cpr.GeneralExpensesAmount;
                //        if (cpr.TotalQuantity != 0)
                //            cpr.UnitPrice = cpr.TotalCostAmount / cpr.TotalQuantity;
                //        else
                //            cpr.UnitPrice = 0;
                //        Utils.ICPResultService.Update(cpr);
                //    }
                //}
                #endregion
                #region add by cuongpv
                foreach (var itemcPUncompleteDetails in cPUncompleteDetails)
                {
                    itemcPUncompleteDetails.MaterialGoodCode = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == itemcPUncompleteDetails.MaterialGoodsID).MaterialGoodsCode;
                    itemcPUncompleteDetails.MaterialGoodName = Utils.ListMaterialGoods.FirstOrDefault(x => x.ID == itemcPUncompleteDetails.MaterialGoodsID).MaterialGoodsName;
                }
                cPUncompleteDetails = cPUncompleteDetails.OrderBy(c => c.MaterialGoodCode).ToList();

                Decimal DirectMaterialAmountTotal = (directMatetialAmount + amount + allocatedAmount - amount2 - cpu.DirectMaterialAmount);
                Decimal DirectLaborAmountTotal = (directLaborAmount + amount3 + allocatedAmount1 - amount4 - cpu.DirectLaborAmount);
                Decimal GeneralExpensesAmountTotal = (generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - cpu.GeneralExpensesAmount);
                int maxCount = cPUncompleteDetails.Count - 1;
                for (int i = 0; i < cPUncompleteDetails.Count; i++)
                {
                    CPResult cpr = cpp.CPResults.FirstOrDefault(x => x.MaterialGoodsID == cPUncompleteDetails[i].MaterialGoodsID);
                    if (cpr != null)
                    {
                        if (i == maxCount)
                        {
                            cpr.DirectMaterialAmount = DirectMaterialAmountTotal;
                            cpr.DirectLaborAmount = DirectLaborAmountTotal;
                            cpr.GeneralExpensesAmount = GeneralExpensesAmountTotal;
                        }
                        else
                        {
                            cpr.DirectMaterialAmount = Math.Round(((directMatetialAmount + amount + allocatedAmount - amount2 - cpu.DirectMaterialAmount) * cpr.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpr.DirectLaborAmount = Math.Round(((directLaborAmount + amount3 + allocatedAmount1 - amount4 - cpu.DirectLaborAmount) * cpr.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                            cpr.GeneralExpensesAmount = Math.Round(((generalExpensesAmount + amount5 + allocatedAmount2 - amount6 - cpu.GeneralExpensesAmount) * cpr.Coefficien / 100), lamtron, MidpointRounding.AwayFromZero);
                        }
                        cpr.TotalCostAmount = cpr.DirectMaterialAmount + cpr.DirectLaborAmount + cpr.GeneralExpensesAmount;
                        if (cpr.TotalQuantity != 0)
                            cpr.UnitPrice = cpr.TotalCostAmount / cpr.TotalQuantity;
                        else
                            cpr.UnitPrice = 0;
                        Utils.ICPResultService.Update(cpr);

                        DirectMaterialAmountTotal = DirectMaterialAmountTotal - cpr.DirectMaterialAmount;
                        DirectLaborAmountTotal = DirectLaborAmountTotal - cpr.DirectLaborAmount;
                        GeneralExpensesAmountTotal = GeneralExpensesAmountTotal - cpr.GeneralExpensesAmount;
                    }
                }
                #endregion
                Utils.ICPUncompleteService.Update(cpu);
                Utils.ICPUncompleteService.CommitTran();
            }
            catch (Exception ex)
            {
                Utils.ICPUncompleteService.RolbackTran();
            }
        }
        void Update(List<CPPeriod> listCPPeriod)
        {
            try
            {
                foreach (var item1 in listCPPeriod)
                {
                    List<CPAllocationGeneralExpense> cPAllocationGeneralExpenses = item1.CPAllocationGeneralExpenses.Where(x => x.AllocationMethod == 3).ToList();
                    List<Guid> lstCostSetID = item1.CPPeriodDetails.Select(c => c.CostSetID).ToList();
                    if (cPAllocationGeneralExpenses.Count > 0)
                    {
                        ICPAllocationGeneralExpenseDetailService.BeginTran();
                        foreach (var item2 in cPAllocationGeneralExpenses)
                        {
                            List<CPAllocationGeneralExpenseDetail> cPAllocationGeneralExpenseDetails = item2.CPAllocationGeneralExpenseDetails.ToList();
                            if (cPAllocationGeneralExpenseDetails.Count > 0)
                            {
                                foreach (var item3 in cPAllocationGeneralExpenseDetails)
                                {
                                    List<CPAccountAllocationQuantum> lstAllocationQuantum = GetCPAccountAllocationQuantum(lstCostSetID);
                                    decimal sumAmount = (decimal)(from g in lstAllocationQuantum select g.TotalCostAmount).Sum();
                                    decimal Amount = (decimal)(from g in lstAllocationQuantum.Where(x => x.AccountingObjectID == item3.CostSetID) select g.TotalCostAmount).Sum();
                                    var model = ICPAllocationGeneralExpenseDetailService.Getbykey(item3.ID);
                                    model.AllocatedRate = Amount / sumAmount * 100;
                                    model.AllocatedAmount = Amount / sumAmount * item2.AllocatedAmount;
                                    ICPAllocationGeneralExpenseDetailService.Update(model);
                                }
                            }
                        }
                        ICPAllocationGeneralExpenseDetailService.CommitTran();
                    }
                }
                foreach (var item1 in listCPPeriod)
                {
                    List<CPUncomplete> cPUncompletes = item1.CPUncompletes.ToList();
                    if (cPUncompletes.Count > 0)
                    {
                        foreach (var item2 in cPUncompletes)
                        {
                            GetLoopCPUncomplete(item2,item1);
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            
        }
        private List<CPAccountAllocationQuantum> GetCPAccountAllocationQuantum( List<Guid> lstCostSet)
        {
            var listCostset = Utils.ListCostSet.Where(x=> lstCostSet.Any(d=>d == x.ID)).ToList();
            var listContractSale = Utils.ListEmContractSA.Where(x => lstCostSet.Any(d => d == x.ID)).ToList();
            var listAllocationQuantum = Utils.ListCPAllocationQuantum.ToList();
            var lst1 = (from a in listCostset
                        select new CPAccountAllocationQuantum()
                        {
                            AccountingObjectID = a.ID,
                            AccountingObjectCode = a.CostSetCode,
                            AccountingObjectName = a.CostSetName,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0
                        }).ToList();
            var lst2 = (from a in listContractSale
                        select new CPAccountAllocationQuantum()
                        {
                            AccountingObjectID = a.ID,
                            AccountingObjectCode = a.Code,
                            AccountingObjectName = a.Name,
                            DirectMaterialAmount = 0,
                            DirectLaborAmount = 0,
                            GeneralExpensesAmount = 0,
                            TotalCostAmount = 0
                        }).ToList();
            List<CPAccountAllocationQuantum> lst = new List<CPAccountAllocationQuantum>();
            lst.AddRange(lst1);
            lst.AddRange(lst2);
            foreach (var x in lst)
            {
                var a = listAllocationQuantum.FirstOrDefault(d => d.ID == x.AccountingObjectID);
                if (a != null)
                {
                    x.DirectMaterialAmount = a.DirectMaterialAmount;
                    x.DirectLaborAmount = a.DirectLaborAmount;
                    x.GeneralExpensesAmount = a.GeneralExpensesAmount;
                    x.TotalCostAmount = a.TotalCostAmount;
                }
            }
            return lst;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGridDS1, "Phương pháp hệ số");
        }

        private void FCPFactorMethod_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDS1.ResetText();
            uGridDS1.ResetUpdateMode();
            uGridDS1.ResetExitEditModeOnLeave();
            uGridDS1.ResetRowUpdateCancelAction();
            uGridDS1.DataSource = null;
            uGridDS1.Layouts.Clear();
            uGridDS1.ResetLayouts();
            uGridDS1.ResetDisplayLayout();
            uGridDS1.Refresh();
            uGridDS1.ClearUndoHistory();
            uGridDS1.ClearXsdConstraints();

            uGridCPExpenseList1.ResetText();
            uGridCPExpenseList1.ResetUpdateMode();
            uGridCPExpenseList1.ResetExitEditModeOnLeave();
            uGridCPExpenseList1.ResetRowUpdateCancelAction();
            uGridCPExpenseList1.DataSource = null;
            uGridCPExpenseList1.Layouts.Clear();
            uGridCPExpenseList1.ResetLayouts();
            uGridCPExpenseList1.ResetDisplayLayout();
            uGridCPExpenseList1.Refresh();
            uGridCPExpenseList1.ClearUndoHistory();
            uGridCPExpenseList1.ClearXsdConstraints();

            uGridCPExpenseList2.ResetText();
            uGridCPExpenseList2.ResetUpdateMode();
            uGridCPExpenseList2.ResetExitEditModeOnLeave();
            uGridCPExpenseList2.ResetRowUpdateCancelAction();
            uGridCPExpenseList2.DataSource = null;
            uGridCPExpenseList2.Layouts.Clear();
            uGridCPExpenseList2.ResetLayouts();
            uGridCPExpenseList2.ResetDisplayLayout();
            uGridCPExpenseList2.Refresh();
            uGridCPExpenseList2.ClearUndoHistory();
            uGridCPExpenseList2.ClearXsdConstraints();

            uGridCPAllocationGeneralExpenseDetail.ResetText();
            uGridCPAllocationGeneralExpenseDetail.ResetUpdateMode();
            uGridCPAllocationGeneralExpenseDetail.ResetExitEditModeOnLeave();
            uGridCPAllocationGeneralExpenseDetail.ResetRowUpdateCancelAction();
            uGridCPAllocationGeneralExpenseDetail.DataSource = null;
            uGridCPAllocationGeneralExpenseDetail.Layouts.Clear();
            uGridCPAllocationGeneralExpenseDetail.ResetLayouts();
            uGridCPAllocationGeneralExpenseDetail.ResetDisplayLayout();
            uGridCPAllocationGeneralExpenseDetail.Refresh();
            uGridCPAllocationGeneralExpenseDetail.ClearUndoHistory();
            uGridCPAllocationGeneralExpenseDetail.ClearXsdConstraints();

            uGridCPUncomplete.ResetText();
            uGridCPUncomplete.ResetUpdateMode();
            uGridCPUncomplete.ResetExitEditModeOnLeave();
            uGridCPUncomplete.ResetRowUpdateCancelAction();
            uGridCPUncomplete.DataSource = null;
            uGridCPUncomplete.Layouts.Clear();
            uGridCPUncomplete.ResetLayouts();
            uGridCPUncomplete.ResetDisplayLayout();
            uGridCPUncomplete.Refresh();
            uGridCPUncomplete.ClearUndoHistory();
            uGridCPUncomplete.ClearXsdConstraints();

            uGridCPAllowcationRate.ResetText();
            uGridCPAllowcationRate.ResetUpdateMode();
            uGridCPAllowcationRate.ResetExitEditModeOnLeave();
            uGridCPAllowcationRate.ResetRowUpdateCancelAction();
            uGridCPAllowcationRate.DataSource = null;
            uGridCPAllowcationRate.Layouts.Clear();
            uGridCPAllowcationRate.ResetLayouts();
            uGridCPAllowcationRate.ResetDisplayLayout();
            uGridCPAllowcationRate.Refresh();
            uGridCPAllowcationRate.ClearUndoHistory();
            uGridCPAllowcationRate.ClearXsdConstraints();

            uGridResult.ResetText();
            uGridResult.ResetUpdateMode();
            uGridResult.ResetExitEditModeOnLeave();
            uGridResult.ResetRowUpdateCancelAction();
            uGridResult.DataSource = null;
            uGridResult.Layouts.Clear();
            uGridResult.ResetLayouts();
            uGridResult.ResetDisplayLayout();
            uGridResult.Refresh();
            uGridResult.ClearUndoHistory();
            uGridResult.ClearXsdConstraints();
        }

        private void FCPFactorMethod_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
