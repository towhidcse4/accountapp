﻿namespace Accounting
{
    partial class FCPFactorMethodDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            this.btnBack = new Infragistics.Win.Misc.UltraButton();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.btnEscape = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnUpdateExport = new Infragistics.Win.Misc.UltraButton();
            this.btnUpdateImport = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dtEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.cbbDateTime = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.dtBeginDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridCostset = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Panel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCpExpenseList3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Panel3 = new Infragistics.Win.Misc.UltraPanel();
            this.Panel4 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.btnAttribution = new Infragistics.Win.Misc.UltraButton();
            this.uGridAllocationGeneralExpenseResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridAllocationGeneralExpense = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCPPeriod8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.Panel2 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridCpExpenseList2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.btnUnDetermined = new Infragistics.Win.Misc.UltraButton();
            this.uGridUncomplete = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridUncompleteDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.Panel5 = new Infragistics.Win.Misc.UltraPanel();
            this.Panel7 = new Infragistics.Win.Misc.UltraPanel();
            this.ugridCostResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox9 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.Panel6 = new Infragistics.Win.Misc.UltraPanel();
            this.uGridAllocatedRate = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox10 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtCPPeriod6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCostset)).BeginInit();
            this.Panel1.ClientArea.SuspendLayout();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList3)).BeginInit();
            this.Panel3.ClientArea.SuspendLayout();
            this.Panel3.SuspendLayout();
            this.Panel4.ClientArea.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpenseResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpense)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod8)).BeginInit();
            this.Panel2.ClientArea.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncomplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncompleteDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optType)).BeginInit();
            this.Panel5.ClientArea.SuspendLayout();
            this.Panel5.SuspendLayout();
            this.Panel7.ClientArea.SuspendLayout();
            this.Panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugridCostResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).BeginInit();
            this.ultraGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod7)).BeginInit();
            this.Panel6.ClientArea.SuspendLayout();
            this.Panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocatedRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox10)).BeginInit();
            this.ultraGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod6)).BeginInit();
            this.cms4Grid.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Accounting.Properties.Resources.ubtnBack;
            this.btnBack.Appearance = appearance1;
            this.btnBack.Location = new System.Drawing.Point(443, 14);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 30);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "Quay lại";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Accounting.Properties.Resources.ubtnForward;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Right;
            this.btnNext.Appearance = appearance2;
            this.btnNext.Location = new System.Drawing.Point(547, 14);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(98, 30);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Tiếp theo";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Accounting.Properties.Resources.cancel_16;
            this.btnEscape.Appearance = appearance3;
            this.btnEscape.Location = new System.Drawing.Point(757, 525);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(98, 30);
            this.btnEscape.TabIndex = 4;
            this.btnEscape.Text = "Hủy bỏ";
            this.btnEscape.Click += new System.EventHandler(this.btnEscape_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance4;
            this.btnSave.Location = new System.Drawing.Point(651, 14);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 30);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnUpdateExport);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnUpdateImport);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnNext);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnSave);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnBack);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 511);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(865, 56);
            this.ultraPanel1.TabIndex = 5;
            // 
            // btnUpdateExport
            // 
            this.btnUpdateExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateExport.Location = new System.Drawing.Point(160, 14);
            this.btnUpdateExport.Name = "btnUpdateExport";
            this.btnUpdateExport.Size = new System.Drawing.Size(140, 30);
            this.btnUpdateExport.TabIndex = 8;
            this.btnUpdateExport.Text = "Cập nhật giá xuất kho";
            this.btnUpdateExport.Click += new System.EventHandler(this.btnUpdateExport_Click);
            // 
            // btnUpdateImport
            // 
            this.btnUpdateImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateImport.Location = new System.Drawing.Point(12, 14);
            this.btnUpdateImport.Name = "btnUpdateImport";
            this.btnUpdateImport.Size = new System.Drawing.Size(142, 30);
            this.btnUpdateImport.TabIndex = 7;
            this.btnUpdateImport.Text = "Cập nhật giá nhập kho";
            this.btnUpdateImport.Click += new System.EventHandler(this.btnUpdateImport_Click);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.dtEndDate);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.txtCPPeriod);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.cbbDateTime);
            this.ultraGroupBox2.Controls.Add(this.dtBeginDate);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            appearance13.FontData.BoldAsString = "True";
            appearance13.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance13;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(205, 118);
            this.ultraGroupBox2.TabIndex = 47;
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dtEndDate
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dtEndDate.Appearance = appearance5;
            this.dtEndDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtEndDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtEndDate.Location = new System.Drawing.Point(575, 11);
            this.dtEndDate.MaskInput = "";
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(99, 21);
            this.dtEndDate.TabIndex = 66;
            this.dtEndDate.Value = null;
            this.dtEndDate.ValueChanged += new System.EventHandler(this.dtEndDate_ValueChanged);
            // 
            // ultraLabel3
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Bottom";
            this.ultraLabel3.Appearance = appearance6;
            this.ultraLabel3.Location = new System.Drawing.Point(9, 14);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(27, 17);
            this.ultraLabel3.TabIndex = 65;
            this.ultraLabel3.Text = "Kỳ";
            // 
            // txtCPPeriod
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.txtCPPeriod.Appearance = appearance7;
            this.txtCPPeriod.AutoSize = false;
            this.txtCPPeriod.Location = new System.Drawing.Point(147, 52);
            this.txtCPPeriod.Name = "txtCPPeriod";
            this.txtCPPeriod.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod.TabIndex = 64;
            // 
            // ultraLabel5
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Bottom";
            this.ultraLabel5.Appearance = appearance8;
            this.ultraLabel5.Location = new System.Drawing.Point(8, 52);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel5.TabIndex = 63;
            this.ultraLabel5.Text = "Tên kỳ tính giá thành";
            // 
            // ultraLabel6
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Bottom";
            this.ultraLabel6.Appearance = appearance9;
            this.ultraLabel6.Location = new System.Drawing.Point(8, 85);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel6.TabIndex = 62;
            this.ultraLabel6.Text = "Đối tượng tập hợp chi phí";
            // 
            // ultraLabel7
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Bottom";
            this.ultraLabel7.Appearance = appearance10;
            this.ultraLabel7.Location = new System.Drawing.Point(306, 14);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(48, 17);
            this.ultraLabel7.TabIndex = 57;
            this.ultraLabel7.Text = "Từ ngày";
            // 
            // ultraLabel8
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Bottom";
            this.ultraLabel8.Appearance = appearance11;
            this.ultraLabel8.Location = new System.Drawing.Point(496, 15);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(57, 17);
            this.ultraLabel8.TabIndex = 58;
            this.ultraLabel8.Text = "Đến ngày";
            // 
            // cbbDateTime
            // 
            this.cbbDateTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbDateTime.Location = new System.Drawing.Point(41, 10);
            this.cbbDateTime.Name = "cbbDateTime";
            this.cbbDateTime.NullText = "<chọn dữ liệu>";
            this.cbbDateTime.Size = new System.Drawing.Size(240, 22);
            this.cbbDateTime.TabIndex = 59;
            // 
            // dtBeginDate
            // 
            appearance12.TextHAlignAsString = "Center";
            appearance12.TextVAlignAsString = "Middle";
            this.dtBeginDate.Appearance = appearance12;
            this.dtBeginDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtBeginDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtBeginDate.Location = new System.Drawing.Point(374, 10);
            this.dtBeginDate.MaskInput = "";
            this.dtBeginDate.Name = "dtBeginDate";
            this.dtBeginDate.Size = new System.Drawing.Size(99, 21);
            this.dtBeginDate.TabIndex = 61;
            this.dtBeginDate.Value = null;
            this.dtBeginDate.ValueChanged += new System.EventHandler(this.dtBeginDate_ValueChanged);
            // 
            // uGridCostset
            // 
            this.uGridCostset.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCostset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCostset.Location = new System.Drawing.Point(0, 118);
            this.uGridCostset.Name = "uGridCostset";
            this.uGridCostset.Size = new System.Drawing.Size(205, 71);
            this.uGridCostset.TabIndex = 48;
            this.uGridCostset.Text = "ultraGrid2";
            // 
            // Panel1
            // 
            // 
            // Panel1.ClientArea
            // 
            this.Panel1.ClientArea.Controls.Add(this.uGridCostset);
            this.Panel1.ClientArea.Controls.Add(this.ultraGroupBox2);
            this.Panel1.Location = new System.Drawing.Point(6, 3);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(205, 189);
            this.Panel1.TabIndex = 6;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.txtCPPeriod3);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            appearance16.FontData.BoldAsString = "True";
            appearance16.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance16;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(209, 62);
            this.ultraGroupBox3.TabIndex = 47;
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod3
            // 
            appearance14.TextVAlignAsString = "Middle";
            this.txtCPPeriod3.Appearance = appearance14;
            this.txtCPPeriod3.AutoSize = false;
            this.txtCPPeriod3.Location = new System.Drawing.Point(163, 18);
            this.txtCPPeriod3.Name = "txtCPPeriod3";
            this.txtCPPeriod3.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod3.TabIndex = 64;
            // 
            // ultraLabel1
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Bottom";
            this.ultraLabel1.Appearance = appearance15;
            this.ultraLabel1.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel1.TabIndex = 63;
            this.ultraLabel1.Text = "Tên kỳ tính giá thành";
            // 
            // uGridCpExpenseList3
            // 
            this.uGridCpExpenseList3.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCpExpenseList3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCpExpenseList3.Location = new System.Drawing.Point(0, 62);
            this.uGridCpExpenseList3.Name = "uGridCpExpenseList3";
            this.uGridCpExpenseList3.Size = new System.Drawing.Size(209, 130);
            this.uGridCpExpenseList3.TabIndex = 48;
            this.uGridCpExpenseList3.Text = "ultraGrid3";
            // 
            // Panel3
            // 
            // 
            // Panel3.ClientArea
            // 
            this.Panel3.ClientArea.Controls.Add(this.uGridCpExpenseList3);
            this.Panel3.ClientArea.Controls.Add(this.ultraGroupBox3);
            this.Panel3.Location = new System.Drawing.Point(436, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(209, 192);
            this.Panel3.TabIndex = 8;
            // 
            // Panel4
            // 
            // 
            // Panel4.ClientArea
            // 
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox5);
            this.Panel4.ClientArea.Controls.Add(this.uGridAllocationGeneralExpenseResult);
            this.Panel4.ClientArea.Controls.Add(this.uGridAllocationGeneralExpense);
            this.Panel4.ClientArea.Controls.Add(this.ultraGroupBox4);
            this.Panel4.Location = new System.Drawing.Point(0, 247);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(281, 258);
            this.Panel4.TabIndex = 9;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox5.Controls.Add(this.btnAttribution);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox5.Location = new System.Drawing.Point(0, 49);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(281, 35);
            this.ultraGroupBox5.TabIndex = 53;
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel9
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            appearance17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Bottom";
            this.ultraLabel9.Appearance = appearance17;
            this.ultraLabel9.Location = new System.Drawing.Point(24, 8);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel9.TabIndex = 57;
            this.ultraLabel9.Text = "Kết quả phân bổ";
            // 
            // btnAttribution
            // 
            this.btnAttribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAttribution.Location = new System.Drawing.Point(190, 6);
            this.btnAttribution.Name = "btnAttribution";
            this.btnAttribution.Size = new System.Drawing.Size(75, 23);
            this.btnAttribution.TabIndex = 10;
            this.btnAttribution.Text = "Phân bổ";
            this.btnAttribution.Click += new System.EventHandler(this.btnAttribution_Click);
            // 
            // uGridAllocationGeneralExpenseResult
            // 
            this.uGridAllocationGeneralExpenseResult.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocationGeneralExpenseResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGridAllocationGeneralExpenseResult.Location = new System.Drawing.Point(0, 84);
            this.uGridAllocationGeneralExpenseResult.Name = "uGridAllocationGeneralExpenseResult";
            this.uGridAllocationGeneralExpenseResult.Size = new System.Drawing.Size(281, 174);
            this.uGridAllocationGeneralExpenseResult.TabIndex = 54;
            this.uGridAllocationGeneralExpenseResult.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridAllocationGeneralExpenseResult.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpenseResult_AfterCellUpdate);
            this.uGridAllocationGeneralExpenseResult.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridAllocationGeneralExpenseResult_BeforeExitEditMode);
            // 
            // uGridAllocationGeneralExpense
            // 
            this.uGridAllocationGeneralExpense.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocationGeneralExpense.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridAllocationGeneralExpense.Location = new System.Drawing.Point(0, 62);
            this.uGridAllocationGeneralExpense.Name = "uGridAllocationGeneralExpense";
            this.uGridAllocationGeneralExpense.Size = new System.Drawing.Size(281, 235);
            this.uGridAllocationGeneralExpense.TabIndex = 48;
            this.uGridAllocationGeneralExpense.Text = "ultraGrid4";
            this.uGridAllocationGeneralExpense.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpense_AfterCellUpdate);
            this.uGridAllocationGeneralExpense.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocationGeneralExpense_CellChange);
            this.uGridAllocationGeneralExpense.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridAllocationGeneralExpense_BeforeExitEditMode);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox4.Controls.Add(this.txtCPPeriod4);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            appearance21.FontData.BoldAsString = "True";
            appearance21.FontData.SizeInPoints = 10F;
            this.ultraGroupBox4.HeaderAppearance = appearance21;
            this.ultraGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(281, 62);
            this.ultraGroupBox4.TabIndex = 47;
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel2
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            appearance18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Bottom";
            this.ultraLabel2.Appearance = appearance18;
            this.ultraLabel2.Location = new System.Drawing.Point(24, 39);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel2.TabIndex = 65;
            this.ultraLabel2.Text = "Chi phí phân bổ";
            // 
            // txtCPPeriod4
            // 
            appearance19.TextVAlignAsString = "Middle";
            this.txtCPPeriod4.Appearance = appearance19;
            this.txtCPPeriod4.AutoSize = false;
            this.txtCPPeriod4.Location = new System.Drawing.Point(163, 18);
            this.txtCPPeriod4.Name = "txtCPPeriod4";
            this.txtCPPeriod4.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod4.TabIndex = 64;
            // 
            // ultraLabel4
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Bottom";
            this.ultraLabel4.Appearance = appearance20;
            this.ultraLabel4.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel4.TabIndex = 63;
            this.ultraLabel4.Text = "Tên kỳ tính giá thành";
            // 
            // txtCPPeriod8
            // 
            appearance22.TextVAlignAsString = "Middle";
            this.txtCPPeriod8.Appearance = appearance22;
            this.txtCPPeriod8.AutoSize = false;
            this.txtCPPeriod8.Location = new System.Drawing.Point(163, 18);
            this.txtCPPeriod8.Name = "txtCPPeriod8";
            this.txtCPPeriod8.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod8.TabIndex = 64;
            // 
            // Panel2
            // 
            // 
            // Panel2.ClientArea
            // 
            this.Panel2.ClientArea.Controls.Add(this.uGridCpExpenseList2);
            this.Panel2.ClientArea.Controls.Add(this.ultraGroupBox6);
            this.Panel2.Location = new System.Drawing.Point(217, 0);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(213, 192);
            this.Panel2.TabIndex = 10;
            // 
            // uGridCpExpenseList2
            // 
            this.uGridCpExpenseList2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCpExpenseList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCpExpenseList2.Location = new System.Drawing.Point(0, 62);
            this.uGridCpExpenseList2.Name = "uGridCpExpenseList2";
            this.uGridCpExpenseList2.Size = new System.Drawing.Size(213, 130);
            this.uGridCpExpenseList2.TabIndex = 48;
            this.uGridCpExpenseList2.Text = "ultraGrid5";
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.txtCPPeriod2);
            this.ultraGroupBox6.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            appearance25.FontData.BoldAsString = "True";
            appearance25.FontData.SizeInPoints = 10F;
            this.ultraGroupBox6.HeaderAppearance = appearance25;
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(213, 62);
            this.ultraGroupBox6.TabIndex = 47;
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod2
            // 
            appearance23.TextVAlignAsString = "Middle";
            this.txtCPPeriod2.Appearance = appearance23;
            this.txtCPPeriod2.AutoSize = false;
            this.txtCPPeriod2.Location = new System.Drawing.Point(163, 18);
            this.txtCPPeriod2.Name = "txtCPPeriod2";
            this.txtCPPeriod2.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod2.TabIndex = 64;
            // 
            // ultraLabel10
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Bottom";
            this.ultraLabel10.Appearance = appearance24;
            this.ultraLabel10.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel10.TabIndex = 63;
            this.ultraLabel10.Text = "Tên kỳ tính giá thành";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox1.Controls.Add(this.btnUnDetermined);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 49);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(299, 35);
            this.ultraGroupBox1.TabIndex = 53;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraLabel12
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance26.TextHAlignAsString = "Left";
            appearance26.TextVAlignAsString = "Bottom";
            this.ultraLabel12.Appearance = appearance26;
            this.ultraLabel12.Location = new System.Drawing.Point(24, 8);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(207, 17);
            this.ultraLabel12.TabIndex = 57;
            this.ultraLabel12.Text = "Kết quả đánh giá chi phí dở dang cuối kỳ";
            // 
            // btnUnDetermined
            // 
            this.btnUnDetermined.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUnDetermined.Location = new System.Drawing.Point(149, 6);
            this.btnUnDetermined.Name = "btnUnDetermined";
            this.btnUnDetermined.Size = new System.Drawing.Size(135, 23);
            this.btnUnDetermined.TabIndex = 10;
            this.btnUnDetermined.Text = "Xác định dở dang";
            this.btnUnDetermined.Click += new System.EventHandler(this.btnUnDetermined_Click);
            // 
            // uGridUncomplete
            // 
            this.uGridUncomplete.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridUncomplete.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGridUncomplete.Location = new System.Drawing.Point(0, 84);
            this.uGridUncomplete.Name = "uGridUncomplete";
            this.uGridUncomplete.Size = new System.Drawing.Size(299, 174);
            this.uGridUncomplete.TabIndex = 54;
            this.uGridUncomplete.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uGridUncomplete.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncomplete_AfterCellUpdate);
            this.uGridUncomplete.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncomplete_CellChange);
            this.uGridUncomplete.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridUncomplete_BeforeExitEditMode);
            // 
            // uGridUncompleteDetail
            // 
            this.uGridUncompleteDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridUncompleteDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGridUncompleteDetail.Location = new System.Drawing.Point(0, 62);
            this.uGridUncompleteDetail.Name = "uGridUncompleteDetail";
            this.uGridUncompleteDetail.Size = new System.Drawing.Size(299, 235);
            this.uGridUncompleteDetail.TabIndex = 48;
            this.uGridUncompleteDetail.Text = "ultraGrid4";
            this.uGridUncompleteDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncompleteDetail_AfterCellUpdate);
            this.uGridUncompleteDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUncompleteDetail_CellChange);
            this.uGridUncompleteDetail.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.uGridUncompleteDetail_BeforeExitEditMode);
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Controls.Add(this.optType);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox7.Controls.Add(this.txtCPPeriod8);
            this.ultraGroupBox7.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            appearance29.FontData.BoldAsString = "True";
            appearance29.FontData.SizeInPoints = 10F;
            this.ultraGroupBox7.HeaderAppearance = appearance29;
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(299, 62);
            this.ultraGroupBox7.TabIndex = 47;
            this.ultraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // optType
            // 
            valueListItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            valueListItem1.DataValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            valueListItem1.DisplayText = "Theo sản phẩm hoàn thành tương đương";
            valueListItem1.Tag = "SPHTTD";
            valueListItem2.DataValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            valueListItem2.DisplayText = "Theo NVL trực tiếp";
            valueListItem2.Tag = "NVLTT";
            valueListItem3.DataValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            valueListItem3.DisplayText = "Theo định mức";
            valueListItem3.Tag = "DM";
            this.optType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.optType.Location = new System.Drawing.Point(350, 45);
            this.optType.Name = "optType";
            this.optType.Size = new System.Drawing.Size(440, 17);
            this.optType.TabIndex = 66;
            this.optType.ValueChanged += new System.EventHandler(this.optType_ValueChanged);
            // 
            // ultraLabel13
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(111)))), ((int)(((byte)(33)))));
            appearance27.TextHAlignAsString = "Left";
            appearance27.TextVAlignAsString = "Bottom";
            this.ultraLabel13.Appearance = appearance27;
            this.ultraLabel13.Location = new System.Drawing.Point(24, 39);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel13.TabIndex = 65;
            this.ultraLabel13.Text = "Xác định dở dang";
            // 
            // ultraLabel14
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextHAlignAsString = "Left";
            appearance28.TextVAlignAsString = "Bottom";
            this.ultraLabel14.Appearance = appearance28;
            this.ultraLabel14.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel14.TabIndex = 63;
            this.ultraLabel14.Text = "Tên kỳ tính giá thành";
            // 
            // ultraLabel11
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextHAlignAsString = "Left";
            appearance30.TextVAlignAsString = "Bottom";
            this.ultraLabel11.Appearance = appearance30;
            this.ultraLabel11.Location = new System.Drawing.Point(24, 19);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel11.TabIndex = 63;
            this.ultraLabel11.Text = "Tên kỳ tính giá thành";
            // 
            // Panel5
            // 
            // 
            // Panel5.ClientArea
            // 
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.Panel5.ClientArea.Controls.Add(this.uGridUncomplete);
            this.Panel5.ClientArea.Controls.Add(this.uGridUncompleteDetail);
            this.Panel5.ClientArea.Controls.Add(this.ultraGroupBox7);
            this.Panel5.Location = new System.Drawing.Point(287, 247);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(299, 258);
            this.Panel5.TabIndex = 12;
            // 
            // Panel7
            // 
            // 
            // Panel7.ClientArea
            // 
            this.Panel7.ClientArea.Controls.Add(this.ugridCostResult);
            this.Panel7.ClientArea.Controls.Add(this.ultraGroupBox9);
            this.Panel7.Location = new System.Drawing.Point(592, 247);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(261, 192);
            this.Panel7.TabIndex = 13;
            // 
            // ugridCostResult
            // 
            this.ugridCostResult.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugridCostResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ugridCostResult.Location = new System.Drawing.Point(0, 62);
            this.ugridCostResult.Name = "ugridCostResult";
            this.ugridCostResult.Size = new System.Drawing.Size(261, 130);
            this.ugridCostResult.TabIndex = 48;
            this.ugridCostResult.Text = "ultraGrid6";
            // 
            // ultraGroupBox9
            // 
            this.ultraGroupBox9.Controls.Add(this.txtCPPeriod7);
            this.ultraGroupBox9.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            appearance33.FontData.BoldAsString = "True";
            appearance33.FontData.SizeInPoints = 10F;
            this.ultraGroupBox9.HeaderAppearance = appearance33;
            this.ultraGroupBox9.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox9.Name = "ultraGroupBox9";
            this.ultraGroupBox9.Size = new System.Drawing.Size(261, 62);
            this.ultraGroupBox9.TabIndex = 47;
            this.ultraGroupBox9.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod7
            // 
            appearance31.TextVAlignAsString = "Middle";
            this.txtCPPeriod7.Appearance = appearance31;
            this.txtCPPeriod7.AutoSize = false;
            this.txtCPPeriod7.Location = new System.Drawing.Point(163, 18);
            this.txtCPPeriod7.Name = "txtCPPeriod7";
            this.txtCPPeriod7.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod7.TabIndex = 64;
            // 
            // ultraLabel15
            // 
            appearance32.BackColor = System.Drawing.Color.Transparent;
            appearance32.TextHAlignAsString = "Left";
            appearance32.TextVAlignAsString = "Bottom";
            this.ultraLabel15.Appearance = appearance32;
            this.ultraLabel15.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel15.TabIndex = 63;
            this.ultraLabel15.Text = "Tên kỳ tính giá thành";
            // 
            // Panel6
            // 
            // 
            // Panel6.ClientArea
            // 
            this.Panel6.ClientArea.Controls.Add(this.uGridAllocatedRate);
            this.Panel6.ClientArea.Controls.Add(this.ultraGroupBox10);
            this.Panel6.Location = new System.Drawing.Point(651, 0);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(209, 192);
            this.Panel6.TabIndex = 14;
            // 
            // uGridAllocatedRate
            // 
            this.uGridAllocatedRate.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridAllocatedRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAllocatedRate.Location = new System.Drawing.Point(0, 62);
            this.uGridAllocatedRate.Name = "uGridAllocatedRate";
            this.uGridAllocatedRate.Size = new System.Drawing.Size(209, 130);
            this.uGridAllocatedRate.TabIndex = 48;
            this.uGridAllocatedRate.Text = "ultraGrid3";
            this.uGridAllocatedRate.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocatedRate_AfterCellUpdate);
            this.uGridAllocatedRate.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAllocatedRate_CellChange);
            // 
            // ultraGroupBox10
            // 
            this.ultraGroupBox10.Controls.Add(this.txtCPPeriod6);
            this.ultraGroupBox10.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            appearance36.FontData.BoldAsString = "True";
            appearance36.FontData.SizeInPoints = 10F;
            this.ultraGroupBox10.HeaderAppearance = appearance36;
            this.ultraGroupBox10.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox10.Name = "ultraGroupBox10";
            this.ultraGroupBox10.Size = new System.Drawing.Size(209, 62);
            this.ultraGroupBox10.TabIndex = 47;
            this.ultraGroupBox10.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtCPPeriod6
            // 
            appearance34.TextVAlignAsString = "Middle";
            this.txtCPPeriod6.Appearance = appearance34;
            this.txtCPPeriod6.AutoSize = false;
            this.txtCPPeriod6.Location = new System.Drawing.Point(163, 18);
            this.txtCPPeriod6.Name = "txtCPPeriod6";
            this.txtCPPeriod6.Size = new System.Drawing.Size(639, 22);
            this.txtCPPeriod6.TabIndex = 64;
            // 
            // ultraLabel16
            // 
            appearance35.BackColor = System.Drawing.Color.Transparent;
            appearance35.TextHAlignAsString = "Left";
            appearance35.TextVAlignAsString = "Bottom";
            this.ultraLabel16.Appearance = appearance35;
            this.ultraLabel16.Location = new System.Drawing.Point(24, 20);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(133, 17);
            this.ultraLabel16.TabIndex = 63;
            this.ultraLabel16.Text = "Tên kỳ tính giá thành";
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(149, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(145, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(148, 22);
            this.tsmAdd.Text = "Thêm";
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(148, 22);
            this.tsmEdit.Text = "Sửa";
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(148, 22);
            this.tsmDelete.Text = "Xóa";
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(148, 22);
            this.tmsReLoad.Text = "Tải Lại";
            // 
            // FCPFactorMethodDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 567);
            this.Controls.Add(this.Panel6);
            this.Controls.Add(this.Panel7);
            this.Controls.Add(this.Panel5);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnEscape);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FCPFactorMethodDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xác định kỳ giá thành";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FCPFactorMethodDetail_FormClosing);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCostset)).EndInit();
            this.Panel1.ClientArea.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList3)).EndInit();
            this.Panel3.ClientArea.ResumeLayout(false);
            this.Panel3.ResumeLayout(false);
            this.Panel4.ClientArea.ResumeLayout(false);
            this.Panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpenseResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocationGeneralExpense)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod8)).EndInit();
            this.Panel2.ClientArea.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCpExpenseList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncomplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUncompleteDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.optType)).EndInit();
            this.Panel5.ClientArea.ResumeLayout(false);
            this.Panel5.ResumeLayout(false);
            this.Panel7.ClientArea.ResumeLayout(false);
            this.Panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugridCostResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox9)).EndInit();
            this.ultraGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod7)).EndInit();
            this.Panel6.ClientArea.ResumeLayout(false);
            this.Panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAllocatedRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox10)).EndInit();
            this.ultraGroupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCPPeriod6)).EndInit();
            this.cms4Grid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnBack;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton btnEscape;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbDateTime;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtBeginDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCostset;
        private Infragistics.Win.Misc.UltraPanel Panel1;
        private Infragistics.Win.Misc.UltraPanel Panel4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocationGeneralExpense;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCpExpenseList3;
        private Infragistics.Win.Misc.UltraPanel Panel3;
        private Infragistics.Win.Misc.UltraPanel Panel2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCpExpenseList2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraButton btnAttribution;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocationGeneralExpenseResult;
        private Infragistics.Win.Misc.UltraPanel Panel5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraButton btnUnDetermined;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUncomplete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUncompleteDetail;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraPanel Panel7;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugridCostResult;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optType;
        private Infragistics.Win.Misc.UltraPanel Panel6;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAllocatedRate;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCPPeriod6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraButton btnUpdateImport;
        private Infragistics.Win.Misc.UltraButton btnUpdateExport;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEndDate;
    }
}