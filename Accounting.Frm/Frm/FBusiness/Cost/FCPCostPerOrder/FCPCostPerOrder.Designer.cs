﻿namespace Accounting
{
    partial class FCPCostPerOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridCPExpenseList1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridCPExpenseList2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridCPAllocationGeneralExpenseDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.btnNghiemThu = new Infragistics.Win.Misc.UltraButton();
            this.btnAddContract = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsReLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.uGridDS = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCPExpenseList1)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCPExpenseList2)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCPAllocationGeneralExpenseDetail)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.cms4Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridCPExpenseList1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(888, 188);
            // 
            // uGridCPExpenseList1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCPExpenseList1.DisplayLayout.Appearance = appearance1;
            this.uGridCPExpenseList1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCPExpenseList1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCPExpenseList1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCPExpenseList1.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCPExpenseList1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCPExpenseList1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridCPExpenseList1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCPExpenseList1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridCPExpenseList1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCPExpenseList1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCPExpenseList1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCPExpenseList1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridCPExpenseList1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCPExpenseList1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCPExpenseList1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCPExpenseList1.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridCPExpenseList1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCPExpenseList1.DisplayLayout.Override.CellPadding = 0;
            this.uGridCPExpenseList1.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCPExpenseList1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridCPExpenseList1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridCPExpenseList1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCPExpenseList1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridCPExpenseList1.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridCPExpenseList1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCPExpenseList1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridCPExpenseList1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCPExpenseList1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCPExpenseList1.DisplayLayout.UseFixedHeaders = true;
            this.uGridCPExpenseList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCPExpenseList1.Location = new System.Drawing.Point(0, 0);
            this.uGridCPExpenseList1.Name = "uGridCPExpenseList1";
            this.uGridCPExpenseList1.Size = new System.Drawing.Size(888, 188);
            this.uGridCPExpenseList1.TabIndex = 2;
            this.uGridCPExpenseList1.Text = "ultraGrid2";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridCPExpenseList2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(888, 188);
            // 
            // uGridCPExpenseList2
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCPExpenseList2.DisplayLayout.Appearance = appearance13;
            this.uGridCPExpenseList2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridCPExpenseList2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCPExpenseList2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCPExpenseList2.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            this.uGridCPExpenseList2.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.HideRowSelector;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCPExpenseList2.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCPExpenseList2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridCPExpenseList2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCPExpenseList2.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridCPExpenseList2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCPExpenseList2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCPExpenseList2.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCPExpenseList2.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridCPExpenseList2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCPExpenseList2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCPExpenseList2.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCPExpenseList2.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridCPExpenseList2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCPExpenseList2.DisplayLayout.Override.CellPadding = 0;
            this.uGridCPExpenseList2.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCPExpenseList2.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridCPExpenseList2.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridCPExpenseList2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCPExpenseList2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridCPExpenseList2.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridCPExpenseList2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCPExpenseList2.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridCPExpenseList2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCPExpenseList2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCPExpenseList2.DisplayLayout.UseFixedHeaders = true;
            this.uGridCPExpenseList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCPExpenseList2.Location = new System.Drawing.Point(0, 0);
            this.uGridCPExpenseList2.Name = "uGridCPExpenseList2";
            this.uGridCPExpenseList2.Size = new System.Drawing.Size(888, 188);
            this.uGridCPExpenseList2.TabIndex = 2;
            this.uGridCPExpenseList2.Text = "ultraGrid3";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGridCPAllocationGeneralExpenseDetail);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(888, 188);
            // 
            // uGridCPAllocationGeneralExpenseDetail
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Appearance = appearance25;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.CellPadding = 0;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCPAllocationGeneralExpenseDetail.DisplayLayout.UseFixedHeaders = true;
            this.uGridCPAllocationGeneralExpenseDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCPAllocationGeneralExpenseDetail.Location = new System.Drawing.Point(0, 0);
            this.uGridCPAllocationGeneralExpenseDetail.Name = "uGridCPAllocationGeneralExpenseDetail";
            this.uGridCPAllocationGeneralExpenseDetail.Size = new System.Drawing.Size(888, 188);
            this.uGridCPAllocationGeneralExpenseDetail.TabIndex = 2;
            this.uGridCPAllocationGeneralExpenseDetail.Text = "ultraGrid4";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraGroupBox1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(890, 57);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            appearance56.FontData.BoldAsString = "True";
            appearance56.FontData.SizeInPoints = 12F;
            this.ultraGroupBox1.Appearance = appearance56;
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.btnExportExcel);
            this.ultraGroupBox1.Controls.Add(this.btnNghiemThu);
            this.ultraGroupBox1.Controls.Add(this.btnAddContract);
            this.ultraGroupBox1.Controls.Add(this.btnDelete);
            this.ultraGroupBox1.Controls.Add(this.btnEdit);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(890, 57);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnExportExcel
            // 
            appearance57.BackColor = System.Drawing.Color.DarkRed;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance57.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance57.Image = global::Accounting.Properties.Resources.Excel;
            this.btnExportExcel.Appearance = appearance57;
            this.btnExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance58.BorderAlpha = Infragistics.Win.Alpha.Opaque;
            this.btnExportExcel.HotTrackAppearance = appearance58;
            this.btnExportExcel.Location = new System.Drawing.Point(356, 20);
            this.btnExportExcel.Name = "btnExportExcel";
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.btnExportExcel.PressedAppearance = appearance59;
            this.btnExportExcel.Size = new System.Drawing.Size(95, 30);
            this.btnExportExcel.TabIndex = 66;
            this.btnExportExcel.Text = "Kết xuất";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnNghiemThu
            // 
            appearance60.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnNghiemThu.Appearance = appearance60;
            this.btnNghiemThu.Location = new System.Drawing.Point(255, 20);
            this.btnNghiemThu.Name = "btnNghiemThu";
            this.btnNghiemThu.Size = new System.Drawing.Size(95, 30);
            this.btnNghiemThu.TabIndex = 16;
            this.btnNghiemThu.Text = "Nghiệm Thu";
            this.btnNghiemThu.Click += new System.EventHandler(this.btnNghiemThu_Click);
            // 
            // btnAddContract
            // 
            appearance61.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAddContract.Appearance = appearance61;
            this.btnAddContract.Location = new System.Drawing.Point(12, 20);
            this.btnAddContract.Name = "btnAddContract";
            this.btnAddContract.Size = new System.Drawing.Size(75, 30);
            this.btnAddContract.TabIndex = 14;
            this.btnAddContract.Text = "Thêm";
            this.btnAddContract.Click += new System.EventHandler(this.btnAddContract_Click);
            // 
            // btnDelete
            // 
            appearance62.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance62;
            this.btnDelete.Location = new System.Drawing.Point(174, 20);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance63.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance63;
            this.btnEdit.Location = new System.Drawing.Point(93, 20);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // cms4Grid
            // 
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tsmAdd,
            this.tsmEdit,
            this.tsmDelete,
            this.tmsReLoad});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(149, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(145, 6);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmAdd.Size = new System.Drawing.Size(148, 22);
            this.tsmAdd.Text = "Thêm";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmEdit.Size = new System.Drawing.Size(148, 22);
            this.tsmEdit.Text = "Sửa";
            this.tsmEdit.Click += new System.EventHandler(this.tsmEdit_Click);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(148, 22);
            this.tsmDelete.Text = "Xóa";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // tmsReLoad
            // 
            this.tmsReLoad.Image = global::Accounting.Properties.Resources.ubtnReset;
            this.tmsReLoad.Name = "tmsReLoad";
            this.tmsReLoad.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tmsReLoad.Size = new System.Drawing.Size(148, 22);
            this.tmsReLoad.Text = "Tải Lại";
            this.tmsReLoad.Click += new System.EventHandler(this.tmsReLoad_Click);
            // 
            // uGridDS
            // 
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDS.DisplayLayout.Appearance = appearance45;
            this.uGridDS.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDS.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDS.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDS.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance46.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance46.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDS.DisplayLayout.GroupByBox.Appearance = appearance46;
            appearance47.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDS.DisplayLayout.GroupByBox.BandLabelAppearance = appearance47;
            this.uGridDS.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance48.BackColor2 = System.Drawing.SystemColors.Control;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDS.DisplayLayout.GroupByBox.PromptAppearance = appearance48;
            this.uGridDS.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDS.DisplayLayout.MaxRowScrollRegions = 1;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDS.DisplayLayout.Override.ActiveCellAppearance = appearance49;
            appearance50.BackColor = System.Drawing.SystemColors.Highlight;
            appearance50.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDS.DisplayLayout.Override.ActiveRowAppearance = appearance50;
            this.uGridDS.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDS.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDS.DisplayLayout.Override.CardAreaAppearance = appearance51;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            appearance52.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDS.DisplayLayout.Override.CellAppearance = appearance52;
            this.uGridDS.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDS.DisplayLayout.Override.CellPadding = 0;
            this.uGridDS.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            appearance53.BackColor = System.Drawing.SystemColors.Control;
            appearance53.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance53.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance53.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDS.DisplayLayout.Override.GroupByRowAppearance = appearance53;
            this.uGridDS.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDS.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            appearance54.BorderColor = System.Drawing.Color.Silver;
            this.uGridDS.DisplayLayout.Override.RowAppearance = appearance54;
            this.uGridDS.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance55.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDS.DisplayLayout.Override.TemplateAddRowAppearance = appearance55;
            this.uGridDS.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDS.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDS.DisplayLayout.UseFixedHeaders = true;
            this.uGridDS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDS.Location = new System.Drawing.Point(0, 57);
            this.uGridDS.Name = "uGridDS";
            this.uGridDS.Size = new System.Drawing.Size(890, 167);
            this.uGridDS.TabIndex = 3;
            this.uGridDS.Text = "ultraGrid1";
            this.uGridDS.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridDS_BeforeRowActivate);
            this.uGridDS.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridDS_DoubleClickRow);
            this.uGridDS.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uGridDS_MouseDown);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(888, 188);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 224);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(890, 211);
            this.ultraTabControl1.TabIndex = 2;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Tập hợp chi phí trực tiếp";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Các khoản giảm giá thành";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "Phân bổ chi phí chung";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // FCPCostPerOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 435);
            this.Controls.Add(this.uGridDS);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.ultraPanel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "FCPCostPerOrder";
            this.Text = "FCPBase";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FCPCostPerOrder_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FCPCostPerOrder_FormClosed);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCPExpenseList1)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCPExpenseList2)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCPAllocationGeneralExpenseDetail)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.cms4Grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton btnAddContract;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private System.Windows.Forms.ContextMenuStrip cms4Grid;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
        private System.Windows.Forms.ToolStripMenuItem tmsReLoad;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDS;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCPAllocationGeneralExpenseDetail;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCPExpenseList2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCPExpenseList1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.Misc.UltraButton btnNghiemThu;
        public Infragistics.Win.Misc.UltraButton btnExportExcel;
    }
}