﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FCPAllocationQuantum : DialogForm //UserControl
    {
        #region khai báo
        private readonly ICPAllocationQuantumService _ICPAllocationQuantumService;
        CPAccountAllocationQuantum _Select = new CPAccountAllocationQuantum();
        BindingList<CPAccountAllocationQuantum> bdlCPProductQuantumDetail = new BindingList<CPAccountAllocationQuantum>(new List<CPAccountAllocationQuantum>());
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FCPAllocationQuantum()
        {//Thêm
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _ICPAllocationQuantumService = IoC.Resolve<ICPAllocationQuantumService>();
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            BindingList<CPAccountAllocationQuantum> bdlCPProductQuantumDetail = new BindingList<CPAccountAllocationQuantum>(GetData().OrderBy(x=>x.AccountingObjectCode).ToList());

            #region load
            uGrid.DataSource = bdlCPProductQuantumDetail;
            //hien nhung dong trong
            uGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            CreaterColumsStyle(uGrid);            
            if (configGrid) ConfigGrid(uGrid);
            #endregion
        }
        List<CPAccountAllocationQuantum> GetData()
        {
            var listCostset = Utils.ListCostSet.Where(x=>x.CostSetType == 2 || x.CostSetType == 4 || x.CostSetType == 5).ToList();
            var listAllocationQuantum = Utils.ListCPAllocationQuantum.ToList();
            var lst1 = (from a in listCostset 
                  select new CPAccountAllocationQuantum()
                  {
                      AccountingObjectID = a.ID,
                      AccountingObjectCode = a.CostSetCode,
                      AccountingObjectName = a.CostSetName,
                      DirectMaterialAmount = 0,
                      DirectLaborAmount = 0,
                      GeneralExpensesAmount = 0,
                      TotalCostAmount = 0
                  }).ToList();           
            List<CPAccountAllocationQuantum> lst = new List<CPAccountAllocationQuantum>();
            lst.AddRange(lst1);
            foreach (var x in lst)
            {
                var a = listAllocationQuantum.FirstOrDefault(d => d.ID == x.AccountingObjectID);
                if (a != null)
                {
                    x.DirectMaterialAmount = a.DirectMaterialAmount;
                    x.DirectLaborAmount = a.DirectLaborAmount;
                    x.GeneralExpensesAmount = a.GeneralExpensesAmount;
                    x.TotalCostAmount = a.TotalCostAmount;
                }
            }
            return lst;
        }
        #endregion

        #region nghiệp vụ
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            var lstbd = (BindingList<CPAccountAllocationQuantum>)uGrid.DataSource;
            var lst = lstbd.ToList();
            _ICPAllocationQuantumService.BeginTran();
            foreach (var x in lst)
            {
                if (!(Utils.ListCPAllocationQuantum.Any(c => c.ID == x.AccountingObjectID)))
                {
                    CPAllocationQuantum model = new CPAllocationQuantum();
                    model.ID = x.AccountingObjectID;
                    model.DirectMaterialAmount = x.DirectMaterialAmount??0;
                    model.DirectLaborAmount = x.DirectLaborAmount??0;
                    model.GeneralExpensesAmount = x.GeneralExpensesAmount??0;
                    model.TotalCostAmount = x.TotalCostAmount??0;
                    _ICPAllocationQuantumService.CreateNew(model);
                }
                else
                {
                    CPAllocationQuantum model = Utils.ListCPAllocationQuantum.FirstOrDefault(c => c.ID == x.AccountingObjectID);
                    model.DirectLaborAmount = x.DirectLaborAmount??0;
                    model.DirectMaterialAmount = x.DirectMaterialAmount??0;
                    model.GeneralExpensesAmount = x.GeneralExpensesAmount??0;
                    model.TotalCostAmount = x.TotalCostAmount??0;
                    _ICPAllocationQuantumService.Update(model);
                }
            }
            try
            {
                _ICPAllocationQuantumService.CommitTran();
                Utils.ClearCacheByType<CPAllocationQuantum>();
                MSG.Information("Lưu dữ liệu thành công!");
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                _ICPAllocationQuantumService.RolbackTran();
            }

        }

        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.CPAccountAllocationQuantumDetail_TableName);            
            uGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            uGrid.DisplayLayout.Bands[0].Columns["DirectMaterialAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["DirectLaborAmount"].CellClickAction = CellClickAction.EditAndSelectText;
            uGrid.DisplayLayout.Bands[0].Columns["GeneralExpensesAmount"].CellClickAction = CellClickAction.EditAndSelectText;
        }
        void CreaterColumsStyle(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGrid.DisplayLayout.Bands[0];
            foreach (var item in band.Columns)
            {
                if (item.Key.Equals("DirectMaterialAmount") || item.Key.Equals("DirectLaborAmount") || item.Key.Equals("GeneralExpensesAmount") || item.Key.Equals("TotalCostAmount"))
                {
                    item.FormatNumberic(ConstDatabase.Format_TienVND);
                }
            }
        }
        #endregion

        #region sự kiện khi thay đổi dữ liệu trên Grid con
        private void uGrid_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("DirectMaterialAmount") || e.Cell.Column.Key.Equals("DirectLaborAmount") || e.Cell.Column.Key.Equals("GeneralExpensesAmount"))
            {
                UltraGrid ultraGrid = (UltraGrid)sender;
                ultraGrid.UpdateData();
                UltraGridRow row = ultraGrid.ActiveRow;
                if (/*!e.Cell.Row.Cells["DirectLaborAmount"].IsFilterRowCell && !e.Cell.Row.Cells["DirectMatetialAmount"].IsFilterRowCell && !e.Cell.Row.Cells["GeneralExpensesAmount"].IsFilterRowCell*/row != null)
                {
                    row.Cells["TotalCostAmount"].Value = ((decimal)row.Cells["DirectLaborAmount"].Value + (decimal)row.Cells["DirectMaterialAmount"].Value + (decimal)row.Cells["GeneralExpensesAmount"].Value);
                }
            }
        }

        private void uGrid_AfterCellUpdate(object sender, CellEventArgs e)
        {
            var cell = uGrid.ActiveCell;
            if(cell != null)
            {
                if ((cell.Column.Key.Equals("DirectMaterialAmount") || cell.Column.Key.Equals("DirectLaborAmount") || cell.Column.Key.Equals("GeneralExpensesAmount")) && (cell.Value == null || cell.Text == "")) cell.Value = (decimal)0;
            }
        }
        private void uGrid_Error(object sender, ErrorEventArgs e)
        {
            e.Cancel = true;
        }

        #endregion

        private void FCPAllocationQuantum_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
