﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using FX.Data;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinStatusBar;
using Infragistics.Win.UltraWinTabControl;
using Infragistics.Win.UltraWinToolbars;
using Type = System.Type;
using System.Drawing;
using Accounting.Core.DAO;

namespace Accounting
{
    public class DetailBase<T> : BaseForm
    {
        #region Khai báo

        /// <summary>
        /// Chứa list các object tag
        /// </summary>
        public ISystemOptionService _ISystemOptionService { get { return IoC.Resolve<ISystemOptionService>(); } }
        public IFADepreciationPostService _IFADepreciationPostService { get { return IoC.Resolve<IFADepreciationPostService>(); } }
        public IPPInvoiceDetailService _IPPInvoiceDetailService { get { return IoC.Resolve<IPPInvoiceDetailService>(); } }
        public ISAInvoiceDetailService _ISAInvoiceDetailService { get { return IoC.Resolve<ISAInvoiceDetailService>(); } }
        public ITIAllocationPostService _ITIAllocationPostService { get { return IoC.Resolve<ITIAllocationPostService>(); } }
        public IGOtherVoucherDetailService _IGOtherVoucherDetailService { get { return IoC.Resolve<IGOtherVoucherDetailService>(); } }
        public IRepositoryLedgerService _IRepositoryLedgerService { get { return IoC.Resolve<IRepositoryLedgerService>(); } }
        public IViewVoucherInvisibleService _voucherInvisibleService { get { return IoC.Resolve<IViewVoucherInvisibleService>(); } }

        protected ValueList _editorsValueList;    //Chứa các control và dữ liệu của nó
        public T _select = (T)Activator.CreateInstance(typeof(T));//= new T();  
        public T _backSelect = (T)Activator.CreateInstance(typeof(T));
        public T initSelect = (T)Activator.CreateInstance(typeof(T));
        public IList<T> _listInput = new List<T>();
        public object _selectJoin;//Mặc định phải để = null
        public object _backSelectJoin;
        protected int _typeID = 0;
        protected int oldTypeID = 0;
        public int defaultTypeId;
        public bool XoaNT = false;
        public bool checkErrorEmployeeID;
        public bool isFirst;
        private FADecrement FADecrement = null;
        private TIDecrement TIDecrement = null;
        private TIIncrement TIIncrement = null;
        private MCPayment MCPayment = null;
        private MCReceipt MCReceipt = null;
        private int check = 0;
        public Dictionary<Guid, List<Department>> department2s = new Dictionary<Guid, List<Department>>();
        public int TypeID
        {
            get
            {
                try
                {
                    //if (_select.HasProperty("TypeID"))
                    if (_select != null && _select.GetType() != null)
                    {
                        PropertyInfo propInfo = _select.GetType().GetProperty("TypeID");
                        if (propInfo != null && propInfo.CanWrite && propInfo.CanRead)
                            //_typeID = _select.GetProperty<T, int>("TypeID");
                            _typeID = (int)Convert.ChangeType(propInfo.GetValue(_select, null), propInfo.PropertyType);
                    }
                }
                catch (Exception ex)
                {
                    MSG.Error(ex.ToString());
                }
                return _typeID;
            }
        }
        public decimal? ExchangeRate = 1;
        public bool Checked = true;
        private int? _typeGroup;
        public int? TypeGroup
        {
            get
            {
                Accounting.Core.Domain.Type type = Utils.ListType.FirstOrDefault(p => p.ID.Equals(TypeID));
                if (type != null)
                {
                    _typeGroup = type.TypeGroupID;
                }
                return _typeGroup;
            }
        }
        public Guid? ID
        {
            get
            {
                Guid? id = null;
                PropertyInfo propID = _select.GetType().GetProperty("ID");
                if (propID != null && propID.CanRead && propID.CanWrite)
                {
                    id = (Guid?)propID.GetValue(_select, null);
                }
                return id;
            }
        }
        //private readonly int[] typePureVouchers = { 100, 110, 120, 130, 140, 150, 160, 170, 200, 210, 220, 230, 240, 400, 410 };//Danh sách type Chứng từ thuần túy (không móc hay phát sinh từ nghiệp vụ khác)
        static List<int> TypeHaveAccountingObjectType
        {
            get
            {
                List<int> temp = new List<int>();
                for (int i = 100; i < 175; i++)
                {
                    temp.Add(i);
                }
                return temp;
            }
        }


        public Guid? _templateID = null;
        protected bool _isBusiness = true;
        public BindingList<IList> _listObjectInput = new BindingList<IList>();
        public BindingList<IList> _listObjectInputGroup = new BindingList<IList>();
        public BindingList<IList> _listObjectInputPost = new BindingList<IList>();
        protected BindingList<IList> _backListObjectInput = new BindingList<IList>();
        protected BindingList<IList> _backListObjectInputPost = new BindingList<IList>();
        protected BindingList<IList> _backListObjectInputGroup = new BindingList<IList>();
        public static bool IsClose = false; //Khi = true thì Load lại dữ liệu
        public bool IsCloseNonStatic { get { return IsClose; } }
        protected bool IsAddNew = false;
        public int _statusForm = ConstFrm.optStatusForm.View;
        public Dictionary<string, bool> _isStandardGrid = new Dictionary<string, bool>();
        protected readonly List<T> _listSelects = new List<T>();
        private System.ComponentModel.IContainer components;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _FMCReceiptDetail_Toolbars_Dock_Area_Top;    //danh sách các phiếu thu (phục vụ cho việc TIẾN, LÙI dữ liệu)
        public UltraGrid uGridControl;
        private UltraStatusBar uStatusBar;
        public ContextMenuStrip cms4Grid;
        private ToolStripMenuItem cmbtnAddRow;
        private ToolStripMenuItem cmbtnDelRow;
        public Infragistics.Win.UltraWinToolbars.UltraToolbarsManager utmDetailBaseToolBar;
        //public Infragistics.Win.UltraWinToolbars.RibbonGroup utmDetailBaseRibbon;
        public UltraCombo _cbbAccountingObject;
        protected UltraPanel FMCReceiptDetailBase_Fill_Panel;
        private bool IsClosing = false;
        private bool _cleaned = false;
        private bool _saveSuccessfull = false;
        public bool? IsSaveGeneralLedger = null;
        public Infragistics.Win.UltraWinCalcManager.UltraCalcManager UCalcManager;
        public string SubSystemCode = "";
        public bool IsCloseByButton = false;//add by cuongpv

        public RowSelectedEventHandler ComboAutoFilterRowSelected { get; set; }
        private Thread _trigger;
        private bool _isRestart = false;
        public bool RestartForm { get { return _isRestart; } }
        #endregion

        public virtual void InitializeGUI(T input)
        {

        }
        public virtual void SetLaiUgridControl(T input)
        {

        }
        public virtual void ShowPopup(T input, int status)
        {

        }

        public IEnumerable<Control> GetAll(Control control, System.Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        public virtual void ChangeStatusControl(bool isEnable)
        {
            ChangeReadOnlyControls(this, isEnable);
            cmbtnAddRow.Enabled = isEnable;
            cmbtnDelRow.Enabled = isEnable;
        }
        #region ConfigForm
        private void ChangeReadOnlyControls(Control ctrlContainer, bool isEnable)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (!(ctrl is UltraGrid))
                {
                    PropertyInfo propInfo = ctrl.GetType().GetProperty("ReadOnly");
                    if (propInfo != null && propInfo.CanWrite)
                        propInfo.SetValue(ctrl, !isEnable, null);
                    if (ctrl is UltraCombo)
                    {
                        var combo = (UltraCombo)ctrl;
                        if (combo.ButtonsRight.Count > 0)
                        {
                            var button = (EditorButton)combo.ButtonsRight[0];
                            button.Enabled = isEnable;
                        }
                    }
                    else if (ctrl is UltraComboEditor)
                    {
                        var combo = (UltraComboEditor)ctrl;
                        if (combo.ButtonsRight.Count > 0)
                        {
                            var button = (EditorButton)combo.ButtonsRight[0];
                            button.Enabled = isEnable;
                            //button.
                        }
                    }
                    else if (ctrl is UltraDateTimeEditor)
                    {
                        var dteEditor = (UltraDateTimeEditor)ctrl;
                        if (dteEditor.ButtonsRight.Count > 0)
                        {
                            var button = (EditorButton)dteEditor.ButtonsRight[0];
                            button.Enabled = isEnable;
                        }
                    }
                    else if (ctrl is UltraButton ||
                             ctrl is UltraCheckEditor)
                    {
                        ctrl.Enabled = isEnable;
                    }
                    if (ctrl is UltraButton ||
                             ctrl is UltraCheckEditor || ctrl is UltraComboEditor || ctrl is UltraDateTimeEditor
                        || ctrl is UltraTextEditor)
                    {
                        if (ctrl.Tag != null && (ctrl.Tag is Dictionary<string, object>))
                        {
                            var dicTag = (Dictionary<string, object>)ctrl.Tag;
                            if (dicTag.Any(x => x.Key == "Enabled"))
                            {
                                if (_statusForm != ConstFrm.optStatusForm.View)
                                    ctrl.Enabled = (bool)dicTag.FirstOrDefault(x => x.Key == "Enabled").Value;
                                else
                                    ctrl.Enabled = isEnable;
                            }
                        }
                    }
                }
                else
                {

                    UltraGrid uGrid = (UltraGrid)ctrl;
                    foreach (UltraGridColumn column in uGrid.DisplayLayout.Bands[0].Columns)
                    {
                        if (!isEnable)
                        {
                            if (column.CellActivation == Activation.AllowEdit)
                                column.CellActivation = Activation.ActivateOnly;
                        }
                        else
                        {
                            if (column.CellActivation == Activation.ActivateOnly)
                                column.CellActivation = Activation.AllowEdit;
                        }
                        if ((column.Key == "UnitPriceOriginal" || column.Key == "AmountOriginal") && _statusForm == 3)
                        {
                            if (TypeID == 410)//trungnq thêm vào để  làm task 6240 mở/ khóa cột đơn giá, thành tiền của màn xuất kho
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                        }
                        if ((column.Key == "UnitPrice" || column.Key == "Amount") && _statusForm == 3)
                        {
                            if (TypeID == 420)//trungnq thêm vào để  làm task 6240 mở/ khóa cột đơn giá, thành tiền của màn chuyển kho
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                        }
                        if ((column.Key == "IsImportPurchase" || column.Key == "Exported" || column.Key == "OWPrice" || column.Key == "OWAmount") && _statusForm == 3)
                        {
                            if (TypeID == 330 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn hàng bán trả lại
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else if (TypeID == 320 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn hàng bán trả lại
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else if (TypeID == 321 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn hàng bán trả lại
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else if (TypeID == 322 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn sainvoice
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else if (TypeID == 323 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn hàng bán trả lại
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else if (TypeID == 324 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn sainvoice
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else if (TypeID == 325 && (column.Key == "OWPrice" || column.Key == "OWAmount"))//trungnq thêm vào để  làm task 6240 mở/ khóa cột giá vốn, đơn giá vốn của màn sainvoice
                            {
                                if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "AllowInputCostUnit").Data == "1")
                                {
                                    column.CellActivation = Activation.AllowEdit;

                                }
                                else
                                {
                                    column.CellActivation = Activation.NoEdit;

                                }
                            }
                            else
                            {
                                column.CellActivation = Activation.NoEdit;
                            }
                        }
                        if (_select != null && _statusForm == 1 && _select.HasProperty("IsBill") && _select.GetProperty<T, bool>("IsBill"))
                        {
                            if ((column.Key == "InvoiceForm" || column.Key == "InvoiceTypeID" || column.Key == "InvoiceTemplate" || column.Key == "InvoiceSeries" || column.Key == "InvoiceNo" || column.Key == "InvoiceDate") && uGrid.Name == "uGridControl")
                            {
                                column.Hidden = false;
                                //Quân ẩn các cột hóa đơn
                                if (new int[] { 220, 320, 321, 322, 323, 324, 325 }.Contains(_typeID))
                                    column.Hidden = true;
                                if ((_typeID == 230 || _typeID == 330) && (column.Key == "InvoiceForm" || column.Key == "InvoiceTypeID")) column.Hidden = true;
                                uGrid.ConfigSizeGridCurrency(this, false);
                                if (_statusForm == ConstFrm.optStatusForm.Edit) column.CellActivation = Activation.NoEdit;
                            }
                        }
                        if (column.Key == "VATRate" && new int[] { 320, 321, 322, 323, 324, 325 }.Contains(_typeID) && _select.GetProperty<T, bool>("Exported"))
                        {
                            var cbbVatRate = new UltraComboEditor();
                            cbbVatRate.Items.Add((decimal)-2, "Không tính thuế");
                            cbbVatRate.Items.Add((decimal)-1, "Không chịu thuế");
                            cbbVatRate.Items.Add((decimal)0, "0%");
                            column.EditorComponent = cbbVatRate;
                            column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                        }
                        if (column.Key == "VATAmountOriginal" && (!_select.GetProperty<T, bool>("IsImportPurchase") || !_select.GetProperty<T, bool>("Exported")) && _select.GetProperty<T, string>("CurrencyID") != "VND" && uGrid.Name == "uGrid1" && uGrid.DisplayLayout.Bands[0].Columns["VATAmount"].Hidden == false)
                        {
                            column.Hidden = false;
                            column.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                        }
                        if (column.Key == "VATAmountOriginal" && (new int[] { 200, 220, 230, 310, 330, 340 }.Contains(_typeID)) && _select.GetProperty<T, string>("CurrencyID") != "VND" && uGrid.Name == "uGrid1" && uGrid.DisplayLayout.Bands[0].Columns["VATAmount"].Hidden == false)
                        {
                            column.Hidden = false;
                            column.FormatNumberic(ConstDatabase.Format_ForeignCurrency);
                        }
                        if (column.Key == "LotNo")
                            if (new int[] { 320, 321, 322, 323, 324, 325, 330, 410, 411, 412, 413, 414, 415, 420, 326, 340, 220 }.Contains(_typeID) && uGrid.Name.Equals("uGrid0"))
                            {
                                foreach (var row in uGrid.Rows)
                                {
                                    var lstrl = Utils.ListRepositoryLedger.Where(x => x.MaterialGoodsID == (Guid)row.Cells["MaterialGoodsID"].Value && x.LotNo != null).ToList();
                                    var newlst = (from a in lstrl
                                                  group a by new { a.MaterialGoodsID, a.LotNo } into g
                                                  select new RepositoryLedger
                                                  {
                                                      MaterialGoodsID = g.Key.MaterialGoodsID,
                                                      LotNo = g.Key.LotNo,
                                                      IWQuantity = (g.Sum(x => x.IWQuantity) - g.Sum(x => x.OWQuantity))
                                                  }).ToList();
                                    var lstlotno = Utils.ListViewRSLotNo.ToList().Where(X => lstrl.Any(d => d.LotNo == X.LotNo)).ToList().OrderBy(c => c.LotNo).ToList();
                                    foreach (var x in lstlotno)
                                    {
                                        x.TotalIOQuantityBalance = newlst.FirstOrDefault(c => c.LotNo == x.LotNo).IWQuantity ?? 0;
                                    }
                                    UltraCombo cbbDebitAccount = new UltraCombo();
                                    cbbDebitAccount.DataSource = lstlotno;
                                    cbbDebitAccount.DisplayMember = "LotNo";
                                    Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.ViewRSLotNo_TableName);
                                    //cbbDebitAccount.RowSelected += (s, e) => cbbLotNo_RowSelected(s, e);
                                    row.Cells["LotNo"].ValueList = cbbDebitAccount;
                                }
                                //column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            }
                        if ((column.Key == "InvoiceForm" || column.Key == "InvoiceTypeID" || column.Key == "InvoiceTemplate" || column.Key == "InvoiceSeries" || column.Key == "InvoiceNo" || column.Key == "InvoiceDate") && _statusForm == 3 && uGrid.Name == "uGridControl")
                        {
                            if (new int[] { 220, 320, 321, 322, 323, 324, 325, 326, 340 }.Contains(_typeID))
                            {
                                //column.CellActivation = Activation.NoEdit;
                                if (column.Key == "InvoiceSeries") column.CellActivation = Activation.NoEdit;
                                UltraGridRow row = uGrid.Rows[0];
                                if (row.Cells["InvoiceForm"].Value != null && row.Cells["InvoiceTypeID"].Value != null)
                                {
                                    List<TT153Report> lstinvoice1 = new List<TT153Report>();
                                    lstinvoice1 = Utils.ListTT153Report.ToList().Where(x => x.InvoiceTypeID == (Guid)row.Cells["InvoiceTypeID"].Value && x.InvoiceForm == int.Parse(row.Cells["InvoiceForm"].Value.ToString())).ToList();
                                    lstinvoice1 = Utils.ListTT153Report.Where(x => x.InvoiceTypeID == Guid.Parse(row.Cells["InvoiceTypeID"].Value.ToString()) && x.InvoiceForm == int.Parse(row.Cells["InvoiceForm"].Value.ToString())).ToList();
                                    List<TT153Report> lstinvoice = (from a in lstinvoice1
                                                                    join b in Utils.ITT153PublishInvoiceDetailService.Query
                                                                         on a.ID equals b.TT153ReportID
                                                                    select a).ToList();
                                    lstinvoice = lstinvoice.GroupBy(x => x.ID).Select(x => x.First()).ToList();
                                    UltraCombo cbbDebitAccount = new UltraCombo();
                                    cbbDebitAccount.DataSource = lstinvoice;
                                    cbbDebitAccount.ValueMember = "ID";
                                    cbbDebitAccount.DisplayMember = "InvoiceTemplate";
                                    Utils.ConfigGrid(cbbDebitAccount, ConstDatabase.TT153Report_TableName);
                                    cbbDebitAccount.RowSelected += (s, e) => cbbInvoiceTemplate_RowSelected(s, e);
                                    uGrid.Rows[0].Cells["InvoiceTemplate"].ValueList = cbbDebitAccount;
                                }
                            }
                        }
                        if (column.Key == "InvoiceNo" && new int[] { 220, 320, 321, 322, 323, 324, 325, 326, 340 }.Contains(_typeID))
                        {
                            column.CellActivation = Activation.NoEdit;
                            if (_select.GetProperty<T, int>("InvoiceForm") != 2 && _statusForm != 1)
                                column.CellActivation = Activation.AllowEdit;
                            if (_select.GetProperty<T, int>("InvoiceForm") == 2 && Utils.ListSystemOption.FirstOrDefault(n => n.Code == "HDDT").Data != "1" && _statusForm != 1)
                            {
                                column.CellActivation = Activation.AllowEdit;
                            }
                        }
                        if (new int[] { 101, 118, 128, 134, 144, 161, 174 }.Contains(_typeID) && (column.Key == "AmountOriginal" || column.Key == "Amount"))
                        {
                            column.LockColumn(true);
                        }
                        #region xử lý cột cho PP tính thuế GTGT
                        if (new List<int> { 320, 321, 322, 323, 324, 325, 326, 330 }.Any(x => x == _typeID) && Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
                        {
                            if ((column.Key == "VATRate" || column.Key == "VATAmountOriginal" || column.Key == "VATAmount" || column.Key == "VATAccount" || column.Key == "VATDescription"))
                            {
                                column.Hidden = true;
                            }
                            if ((column.Key == "Description" || column.Key == "CareerGroupID") && uGrid.Name == "uGrid1")
                            {
                                column.Hidden = false;
                            }
                        }
                        if (new List<int> { 210, 260, 261, 262, 263, 264, 220, 230 }.Any(x => x == _typeID) && Utils.ListSystemOption.FirstOrDefault(x => x.Code == "PPTTGTGT").Data == "Phương pháp trực tiếp trên doanh thu")
                        {
                            if ((column.Key == "GoodsServicePurchaseID") && uGrid.Name == "uGrid1")
                            {
                                column.Hidden = true;
                            }
                        }
                        #endregion
                        #region Xử lý ngoại tệ
                        if (new List<int> { 110, 116, 117, 119, 120, 126, 127, 129, 130, 131, 132, 133, 140, 141, 142, 143, 170, 171, 172, 173, 150, 260, 261, 262, 263, 264, 330, 340, 600, 902, 903, 904, 905, 906 }.Any(x => x == _typeID) && _statusForm == 1)
                        {
                            if ((column.Key == "CashOutExchangeRate" || column.Key == "CashOutAmount" || column.Key == "CashOutDifferAmount"
                              || column.Key == "CashOutDifferAccount") && uGrid.Name == "uGrid0")
                            {
                                column.Hidden = _select.GetProperty<T, string>("CurrencyID") == "VND";
                            }
                            else if ((column.Key == "CashOutVATAmount" || column.Key == "CashOutDifferVATAmount") && uGrid.Name == "uGrid1")
                            {
                                column.Hidden = _select.GetProperty<T, string>("CurrencyID") == "VND";
                            }
                        }
                        #endregion
                    }
                    if (uGrid.TopLevelControl.AccessibilityObject.Name == "Điều chỉnh TSCĐ" && uGrid.Name == "uGrid0" && uGrid.Visible == false)
                        continue;
                    else
                        uGrid.DisplayLayout.Override.AllowAddNew = isEnable && _isStandardGrid.FirstOrDefault(p => p.Key.Equals(uGrid.Name)).Value
                                                                       ? AllowAddNew.TemplateOnBottom
                                                                                   : AllowAddNew.No;

                }
                if (ctrl.HasChildren)
                    ChangeReadOnlyControls(ctrl, isEnable);
                if ((new string[] { "btnOutwardNo" }.Contains(ctrl.Name)) && (new int[] { 210, 220, 240, 260, 261, 262, 263, 264, 320, 321, 322, 323, 324, 325, 330 }.Contains(_typeID)) && _statusForm == 3)
                {//trungnq bỏ "optThanhToan" , "cbbChonThanhToan" để mở khóa chọn phương thức thanh toán , làm task 6242
                    ctrl.SetProperty("ReadOnly", true);
                }
                if ((new string[] { "optPhieuXuatKho", "optThanhToan", "cbbChonThanhToan", "optHoaDon" }.Contains(ctrl.Name)) && (new int[] { 210, 220, 240, 260, 261, 262, 263, 264, 320, 321, 322, 323, 324, 325, 330 }.Contains(_typeID)) && _statusForm == 3)
                {//trungnq bỏ "optThanhToan" , "cbbChonThanhToan" để mở khóa chọn phương thức thanh toán , làm task 6242
                    if (isEnable)
                    {
                        ctrl.SetProperty("ReadOnly", false);

                    }
                    else
                    {
                        ctrl.SetProperty("ReadOnly", true);
                    }

                }
                if ((new string[] { "optPayment", "cbbPayment" }.Contains(ctrl.Name)) && (new int[] { 430, 902, 903, 904, 905, 906, 500, 119, 129, 132, 142, 172 }.Contains(_typeID)) && _statusForm == 3)
                {//trungnq bỏ  mở khóa chọn phương thức thanh toán , làm task 6242
                    if (isEnable)
                    {
                        ctrl.SetProperty("Enabled", true);

                    }
                    else
                    {
                        ctrl.SetProperty("Enabled", false);
                    }
                    if (ctrl.Name == "cbbPayment" && (_typeID == 430 || _typeID == 500))
                    {
                        ctrl.SetProperty("Enabled", false);
                    }
                }
                if ((new string[] { "optAccountingObjectType" }.Contains(ctrl.Name)) && (new int[] { 120 }.Contains(_typeID)) && _statusForm == 3)
                {
                    //ctrl.SetProperty("ReadOnly", true);
                    if (isEnable) //trungnq thêm để sửa bug 6409
                    {
                        ctrl.SetProperty("ReadOnly", false);

                    }
                    else
                    {
                        ctrl.SetProperty("ReadOnly", true);
                    }
                }
                if ((new string[] { "cbbCurrency", "txtBalanceAmount", "txtAuditAmount", "txtDifferAmount" }.Contains(ctrl.Name)) && (new int[] { 180 }.Contains(_typeID)) && _statusForm == 3)
                {
                    ctrl.SetProperty("ReadOnly", true);
                }
                else if ((new string[] { "optThanhToan", "optPhieuXuatKho", "optHoaDon" }.Contains(ctrl.Name)) && (new int[] { 210, 220, 240, 260, 261, 262, 263, 264, 320, 321, 322, 323, 324, 325, 330 }.Contains(_typeID)) && _statusForm == 2)
                {
                    ctrl.SetProperty("ReadOnly", false);
                }
                if ((new string[] { "txtOldInvNo", "txtOldInvDate", "txtOldInvTemplate", "txtOldInvSeries" }.Contains(ctrl.Name)))
                {
                    ctrl.SetProperty("ReadOnly", true);
                }
                if (_statusForm == 1 && _select.HasProperty("BillRefID") && _select.GetProperty<object, Guid?>("BillRefID") != null)
                {
                    var HD = Utils.ListSABill.FirstOrDefault(x => x.ID == _select.GetProperty<object, Guid>("BillRefID"));
                    switch (ctrl.Name)
                    {
                        case "txtOldInvNo":
                            ctrl.Text = HD.InvoiceNo;
                            break;
                        case "txtOldInvDate":
                            ctrl.Text = HD.InvoiceDate == null ? "" : (HD.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                            break;
                        case "txtOldInvTemplate":
                            ctrl.Text = HD.InvoiceTemplate;
                            break;
                        case "txtOldInvSeries":
                            ctrl.Text = HD.InvoiceSeries;
                            break;
                        case "panel1":
                            ctrl.Visible = true;
                            break;
                        case "txtDocumentNo":
                            ctrl.Text = (_select as SABill).DocumentNo;
                            break;
                        case "txtDocumentDate":
                            break;
                        case "txtDocumentNote":
                            ctrl.Text = (_select as SABill).DocumentNote;
                            break;
                        case "panel2":
                            ctrl.Visible = true;
                            break;
                        default:
                            break;
                    }
                }


            }
        }
        #endregion
        protected void cbbInvoiceTemplate_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null) return;
            //var @base = @this as DetailBase<TK>;
            TT153Report model = (TT153Report)e.Row.ListObject;
            UltraGrid uGrid = this.FindGridIsActived();
            if (uGrid != null)
            {
                UltraGridRow row = uGrid.ActiveRow;
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("InvoiceSeries"))
                {
                    uGrid.Rows[0].Cells["InvoiceSeries"].Value = model.InvoiceSeries;
                }
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("InvoiceTemplate"))
                {
                    uGrid.Rows[0].Cells["InvoiceTemplate"].Value = model.InvoiceTemplate;
                }
            }

        }
        protected void GetValue<T2>(T2 input)
        {
            UltraTabControl _tabControl =
                (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
            //Logs.WriteTxt("GetValue _ line 232");
            if (_tabControl != null)
            {
                foreach (UltraTab tab in _tabControl.Tabs)
                {
                    //Logs.WriteTxt("GetValue _ line 237 _ Index = " + tab.Index);
                    int index = tab.Index;

                    UltraGrid uGrid =
                        (UltraGrid)
                        _tabControl.Tabs[index].TabPage.Controls.Find("uGrid" + index, true).FirstOrDefault();

                    if (new int[] { 907 }.Contains(TypeID) && index == 2)
                    {
                        continue;
                    }
                    //Logs.WriteTxt("GetValue _ line 242");
                    if (uGrid != null)
                        AddChildToSelect(uGrid, input, index);
                    //Logs.WriteTxt("GetValue _ line 244");
                }
            }
            else
            {
                UltraGrid uGrid =
                    (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid != null)
                    AddChildToSelect(uGrid, input, 0);
            }
            //PropertyInfo
        }
        void sortdetailofvouchers(IList list)//trungnq sắp xếp 1 list theo index và orderpriotyty tăng dần
        {
            int newmin = list[0].GetProperty("OrderPriority").ToInt();
            //int oldmin = list[0].GetProperty("OrderPriority").ToInt();

            IList list1 = list;
            IList list2 = list.CloneObject();
            for (int i = 0; i < list.Count; i++)
            {
                foreach (var child in list2)
                {


                    int currentOrderPriority = child.GetProperty("OrderPriority").ToInt();
                    if (currentOrderPriority <= newmin)
                    {
                        if (currentOrderPriority == newmin)
                            continue;


                        newmin = currentOrderPriority;

                    }
                }
                foreach (var child2 in list2)
                {
                    if (child2.GetProperty("OrderPriority").ToInt() == newmin)
                    {
                        list2.Remove(child2);
                        if (list2.Count != 0)
                        { newmin = list2[0].GetProperty("OrderPriority").ToInt(); }
                        list1[i] = child2;
                        break;
                    }
                }
                //foreach (var child1 in list)
                //{
                //    if(child1.GetProperty("OrderPriority").ToInt()==newmin)
                //    { list1[i]=child1;
                //        break;
                //    }
                //}
            }
            list = list1;
        }
        /// <summary>
        /// Add datasource cuar Grid vaof _select
        /// </summary>
        /// <param name="uGrid"></param> 
        /// <param name="input"></param>
        void AddChildToSelect<T2>(UltraGrid uGrid, T2 input, int index)

        {//1 thuật toán sắp xếp index và order priority
            Dictionary<System.Type, IList> dicObject =
                        ConstDatabase.DicObjectForTemplete().FirstOrDefault(p => p.Key.Equals(TypeID == 840 || TypeID == 660 ? 600 : (TypeID == 421 || TypeID == 422) ? 420 : TypeID)).Value;
            //Logs.WriteTxt("AddChildToSelect _ line 266");
            //PropertyInfo propID = input.GetType().GetProperty("ID");
            //Logs.WriteTxt("AddChildToSelect _ line 268");
            IList lstchild = (IList)uGrid.DataSource;
            if (input.HasProperty("ID"))
            {
                //foreach (UltraGridRow row in uGrid.Rows)
                //{
                //    Logs.WriteTxt("AddChildToSelect _ line 266 _ RowIndex = " + row.Index);
                //    var gId = input.GetProperty<T2, Guid?>("ID");
                //    Logs.WriteTxt("AddChildToSelect _ line 274");
                //    var sKey = input.GetType().Name + "ID";
                //    Logs.WriteTxt("AddChildToSelect _ line 276 _ sKey = " + sKey);
                //    if (row.Cells.Exists(sKey))
                //    {
                //        Logs.WriteTxt("AddChildToSelect _ line 282 _ gId = " + gId);
                //        row.Cells[sKey].Value = gId;
                //        Logs.WriteTxt("AddChildToSelect _ line 284");
                //    }
                //    Logs.WriteTxt("AddChildToSelect _ line 286");
                //    if (typeof(T) == typeof(PPInvoice) && (Guid)row.Cells["ID"].Value == Guid.Empty)
                //        row.Cells["ID"].Value = Guid.NewGuid();
                //    Logs.WriteTxt("AddChildToSelect _ line 289");
                //    if (row.Cells.Exists("RepositoryID") && new int[] { 210, 260, 261, 262, 263, 264 }.Contains(TypeID) && input.HasProperty("InwardNo") && string.IsNullOrEmpty(input.GetProperty<T2, string>("InwardNo")))
                //        row.Cells["RepositoryID"].Value = null;
                //    Logs.WriteTxt("AddChildToSelect _ line 292");
                //}
                bool sort = false;//trungnq
                                  // if (lstchild.Count == 0) { sort = false; };
                if (lstchild.Count != 0)
                {
                    foreach (var child in lstchild)
                    {
                        if (child.HasProperty("OrderPriority"))
                        {
                            if (!child.GetProperty("OrderPriority").IsNullOrEmpty())
                                if (child.GetProperty("OrderPriority").ToString() != "")
                                { sort = true; }

                        }
                    }
                }
                if (sort) { sortdetailofvouchers(lstchild); }//trungnq
                foreach (var child in lstchild)
                {

                    //Logs.WriteTxt("AddChildToSelect _ line 266 _ RowIndex = " + lstchild.IndexOf(child));
                    var gId = input.GetProperty<T2, Guid?>("ID");
                    //Logs.WriteTxt("AddChildToSelect");
                    var sKey = input.GetType().Name + "ID";
                    //Logs.WriteTxt("AddChildToSelect _ sKey = " + sKey);
                    if (child.HasProperty(sKey))
                    {
                        //Logs.WriteTxt("AddChildToSelect _ gId = " + gId);
                        child.SetProperty(sKey, gId);
                        //Logs.WriteTxt("AddChildToSelect");
                    }
                    if (new int[] { 119, 129, 132, 142, 172, 500, 510, 430, 902, 903, 904, 905, 906 }.Contains(TypeID) && index == 3)
                        child.SetProperty("PPInvoiceID", gId);
                    //Logs.WriteTxt("AddChildToSelect _ line 286");
                    if (typeof(T) == typeof(PPInvoice) && (Guid)child.GetProperty("ID") == Guid.Empty)
                        child.SetProperty("ID", Guid.NewGuid());
                    //Logs.WriteTxt("AddChildToSelect _ line 289");
                    if (child.HasProperty("RepositoryID") && new int[] { 210, 260, 261, 262, 263, 264 }.Contains(TypeID) && input.HasProperty("InwardNo") && string.IsNullOrEmpty(input.GetProperty<T2, string>("InwardNo")))
                        child.SetProperty("RepositoryID", null);
                    //Logs.WriteTxt("AddChildToSelect _ line 292");
                }
            }
            //Logs.WriteTxt("AddChildToSelect _ line 294");
            uGrid.UpdateData();
            //Logs.WriteTxt("AddChildToSelect _ line 296");
            string objectName = string.Empty;
            //Logs.WriteTxt("AddChildToSelect _ line 298");
            if (uGrid.Tag != null && uGrid.Tag.GetType().Equals(typeof(System.Type)))
                objectName = ((System.Type)uGrid.Tag).Name;
            else
                objectName = uGrid.DataSource.GetType().FullName.GetObjectNameFromBindingName();
            //Logs.WriteTxt("AddChildToSelect _ line 303");
            KeyValuePair<System.Type, IList> keyValue = dicObject.FirstOrDefault(p => p.Key.Name.Equals(objectName));
            //Logs.WriteTxt("AddChildToSelect _ line 293");
            string propName = keyValue.Key.Name;
            //Logs.WriteTxt("AddChildToSelect _ line 295");
            PropertyInfo propertyInfo = input.GetType().GetProperty(propName + "s");
            if (new int[] { 520 }.Contains(TypeID) && index == 1)
            {
                // Nếu là ghi giảm, thêm chứng từ hạch toán
                BindingList<FADecrementDetail> details = input.GetProperty<T2, BindingList<FADecrementDetail>>("FADecrementDetails");
                decimal? TotalAmount = 0;
                for (int i = 0; i < details.Count; i++)
                {
                    var post = new List<FADecrementDetailPost>();
                    details[i].ID = Guid.NewGuid();
                    lstchild[i * 2].SetProperty("FADecrementDetailID", details[i].ID);
                    lstchild[i * 2].SetProperty("OrderPriority", 0);
                    lstchild[i * 2 + 1].SetProperty("FADecrementDetailID", details[i].ID);
                    lstchild[i * 2 + 1].SetProperty("OrderPriority", 1);

                    post.Add((FADecrementDetailPost)lstchild[i * 2]);
                    post.Add((FADecrementDetailPost)lstchild[i * 2 + 1]);
                    details[i].FADecrementDetailPosts = post;
                }
                for (int i = 0; i < lstchild.Count; i++)
                {
                    TotalAmount += ((FADecrementDetailPost)lstchild[i]).Description == "Xử lý giá trị còn lại" ? ((FADecrementDetailPost)lstchild[i]).Amount : 0;
                }
                input.SetProperty("TotalAmount", TotalAmount);
                input.SetProperty("TotalAmountOriginal", TotalAmount);
                if (propertyInfo != null && propertyInfo.CanWrite)
                {
                    propertyInfo.SetValue(input, details, null);
                }
            }
            else if (new int[] { 500 }.Contains(TypeID) && index == 0)
            {
                // Nếu là ghi tăng, tính tổng nguyên giá của tất cả TSCĐ
                var key = "TotalOrgPrice";
                var keyOrg = "TotalOrgPriceOriginal";
                decimal total = 0;
                foreach (var child in lstchild)
                {
                    total += ((FAIncrementDetail)(object)child).OrgPrice;
                }
                input.SetProperty(key, total);
                input.SetProperty(keyOrg, total);
            }
            else if (new int[] { 510 }.Contains(TypeID) && index == 0)//trungnq
            {
                // Nếu là ghi tăng, tính tổng nguyên giá của tất cả TSCĐ
                var key = "TotalOrgPrice";
                var keyOrg = "TotalOrgPriceOriginal";
                decimal total = 0;
                foreach (var child in lstchild)
                {
                    total += ((FAIncrementDetail)(object)child).OrgPriceOriginal;
                }
                input.SetProperty(key, total);
                input.SetProperty(keyOrg, total);
            }//trungnq
            else if (new int[] { 540 }.Contains(TypeID) && index == 0)
            {
                // Nếu là ghi tăng, tính tổng nguyên giá của tất cả TSCĐ
                var key = "TotalAmount";
                var keyOrg = "TotalAmountOriginal";
                decimal total = 0;
                foreach (var child in lstchild)
                {
                    total += ((FADepreciationDetail)(object)child).MonthlyDepreciationAmountOriginal;
                }
                input.SetProperty(key, total);
                input.SetProperty(keyOrg, total);
                if (propertyInfo != null && propertyInfo.CanWrite)
                {
                    propertyInfo.SetValue(input, lstchild, null);
                }
            }
            else
            {
                if (propertyInfo != null && propertyInfo.CanWrite)
                {
                    propertyInfo.SetValue(input, lstchild, null);
                }
            }
        }

        void AddChildToSelect<T2>(UltraGrid uGrid, T2 input)
        {
            Dictionary<System.Type, IList> dicObject =
                        ConstDatabase.DicObjectForTemplete().FirstOrDefault(p => p.Key.Equals(TypeID)).Value;

            IList lstchild = (IList)uGrid.DataSource;
            if (input.HasProperty("ID"))
                foreach (var child in lstchild)
                {
                    var gId = input.GetProperty<T2, Guid?>("ID");
                    var sKey = input.GetType().Name + "ID";
                    if (child.HasProperty(sKey))
                    {
                        child.SetProperty(sKey, gId);
                    }
                }
            uGrid.UpdateData();
            string objectName = string.Empty;
            if (uGrid.Tag != null && uGrid.Tag.GetType().Equals(typeof(System.Type)))
                objectName = ((System.Type)uGrid.Tag).Name;
            else
                objectName = uGrid.DataSource.GetType().FullName.GetObjectNameFromBindingName();
            KeyValuePair<System.Type, IList> keyValue = dicObject.FirstOrDefault(p => p.Key.Name.Equals(objectName));
            string propName = keyValue.Key.Name;
            PropertyInfo propertyInfo = input.GetType().GetProperty(propName + "s");
            if (propertyInfo != null && propertyInfo.CanWrite)
            {
                propertyInfo.SetValue(input, lstchild, null);
            }
        }
        protected virtual string BuildConfigButton(T activeSelect, IList<T> listInput, int statusForm)
        {
            string followStr;
            string statusStr = null;
            string typeStr;
            string recordStr;
            string locationStr;
            string resultStr = null;
            // Xác định điều kiện
            // 1. Luồng là gì ? ConstFrm.optBizFollowConst.status
            followStr = ConstFrm.optBizFollowConst.status == 1 ? "2" : "1";
            // 2. Thao tác gì ? statusForm

            //_select.ID = Guid.NewGuid();
            //Utils.ClearCacheByType<SystemOption>();





            switch (statusForm)
            {
                case ConstFrm.optStatusForm.View:
                    statusStr = "1";
                    break;
                case ConstFrm.optStatusForm.Add:
                    statusStr = "2";
                    break;
                case ConstFrm.optStatusForm.Edit:
                    statusStr = "3";
                    break;
            }
            // 3. Loại chứng từ là gì activeSelect.TypeID
            //typeStr = typeNotBusiness.Any(p => p.Equals(TypeID)) ? "1" : "2";
            if (Utils.TypePureVouchers.Any(p => p.Equals(TypeID)))
            {
                typeStr = "1";
            }
            else if (Utils.TypeNotPost.Any(p => p.Equals(TypeID)))
            {
                typeStr = "3";
            }
            else if (Utils.TypeOnlyView.Any(p => p.Equals(TypeID)))
            {
                typeStr = "4";
            }
            else
            {
                typeStr = "2";
            }
            // 4. Trạng thái ghi sổ chứng từ activeSelect.Recorded
            PropertyInfo propRecorded = activeSelect.GetType().GetProperty("Recorded", BindingFlags.Public | BindingFlags.Instance);
            if (null != propRecorded && propRecorded.CanRead)
            {
                object temp = propRecorded.GetValue(activeSelect, null);
                if (temp is bool)
                {
                    recordStr = (bool)temp ? "1" : "2";
                }
                else
                    recordStr = "2";
            }
            else
                recordStr = "2";
            // 5. Vị trí chứng từ activeSelect, dsMCReceipt

            List<Guid> listGuid = new List<Guid>();
            foreach (T item in listInput)
            {
                PropertyInfo propTempId = item.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
                if (null != propTempId && propTempId.CanRead)
                {
                    object temp = propTempId.GetValue(item, null);
                    if (temp is Guid)
                    {
                        listGuid.Add((Guid)temp);
                    }
                }
            }
            PropertyInfo propId = activeSelect.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            Guid currentID = new Guid();
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(activeSelect, null);
                if (temp is Guid)
                {
                    currentID = (Guid)temp;
                }
            }
            int indexActive = listGuid.IndexOf(currentID);
            if (listInput.Count == 1)
            {
                locationStr = "4";
            }
            else if (indexActive == 0)
            {
                locationStr = "3";
            }
            else if (indexActive == listInput.Count - 1)
            {
                locationStr = "1";
            }
            else
            {
                locationStr = "2";
            }
            //Xây dựng chuỗi
            //A. Từ List -> Detail
            //1.Có tính năng ghi sổ
            if ((followStr == "1"))
            {
                //1.1 Hiển thị(View)
                if ((statusStr == "1"))
                {
                    if (typeStr == "1" || typeStr == "2")
                    {
                        resultStr = followStr + "-" + statusStr + "-" + typeStr + "-" + recordStr + "-" + locationStr;
                    }
                    else if (typeStr == "3" || typeStr == "4")
                    {
                        resultStr = followStr + "-" + statusStr + "-" + typeStr + "-" + locationStr;
                    }
                }
                //1.2Thêm mới(Add)
                else if ((statusStr == "2"))
                {
                    resultStr = followStr + "-" + statusStr + "-" + typeStr;
                }
                //1.3 Sửa
                if (statusStr == "3")
                {
                    resultStr = followStr + "-" + statusStr + "-" + typeStr;
                }
            }
            //2.Không có tính năng ghi sổ
            else if (followStr == "2")
            {
                //2.1 Hiển thị(View)
                if (statusStr == "1")
                {
                    resultStr = followStr + "-" + statusStr + "-" + typeStr + "-" + locationStr;
                }
                //2.2 Thêm mới(Add)
                else if (statusStr == "2")
                {
                    resultStr = followStr + "-" + statusStr + "-" + typeStr;
                }
                //2.3 Thao tác Edit
                else if (statusStr == "3")
                {
                    resultStr = followStr + "-" + statusStr + "-" + typeStr;
                }
            }

            return resultStr;
        }


        private void ResetForm()
        {
            if (TypeID == 680)
            {
                RSForm();
                return;
            }
            #region phần đầu
            //số chứng từ
            T newSelect = (T)Activator.CreateInstance(typeof(T));
            if (newSelect.HasProperty("TypeID") && new int[] { 220, 230, 330, 340 }.Contains(oldTypeID)) newSelect.SetProperty("TypeID", oldTypeID);
            if (_statusForm == ConstFrm.optStatusForm.Add && !IsAddNew)
            {
                //PropertyInfo propTypeID = newSelect.GetType().GetProperty("TypeID");
                //if (propTypeID != null && propTypeID.CanWrite)
                //{
                //    Accounting.Core.Domain.Type newType = Utils.ListType.OrderBy(p => p.ID)
                //                        .FirstOrDefault(p => p.TypeGroupID.Equals(TypeGroup));
                //    if (newType != null)
                //    {

                //        propTypeID.SetValue(newSelect, newType.ID == 500 ? 500 : defaultTypeId != 0 ? defaultTypeId : newType.ID, null);
                //        if (new int[] { 435, 560, 821, 820, 810, 811, 830, 831, 832, 834, 434, 840, 180 }.Contains(newType.ID))
                //        {
                //            //_select = newSelect;
                //            ShowPopup(newSelect, ConstFrm.optStatusForm.Add);
                //        }
                //        else
                //        {
                //            _select = newSelect;
                //            InitializeGUI(_select);
                //        }

                //    }
                //}
                _isRestart = true;
                initSelect = newSelect;
                Close();
            }
            else
            {
                PropertyInfo[] propertyInfo = newSelect.GetType().GetProperties();
                if (propertyInfo.Length > 0)
                {
                    foreach (PropertyInfo propItem in propertyInfo)
                    {
                        if (propItem.CanWrite)
                        {
                            if (propItem.Name.Equals("No"))
                            {
                                if (TypeGroup != null)
                                    propItem.SetValue(newSelect, Utils.TaoMaChungTu(IGenCodeService.getGenCode((int)TypeGroup)),
                                                      null);
                            }

                            else if (propItem.Name.Equals("PostedDate") || propItem.Name.Equals("Date") || propItem.Name.Equals("OPostedDate") || propItem.Name.Equals("ODate")
                                || propItem.Name.Equals("MPostedDate") || propItem.Name.Equals("MDate") || propItem.Name.Equals("SPostedDate") || propItem.Name.Equals("SDate"))//edit by cuonngpv: them 2 truong MPostedDate MDate, SPostedDate SDate
                            {
                                propItem.SetValue(newSelect, Utils.StringToDateTime(ConstFrm.DbStartDate) ?? DateTime.Now, null);
                            }
                            else if (propItem.Name.Equals("MNo"))
                            {
                                if (TypeID == 322)
                                {
                                    propItem.SetValue(newSelect, Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroupMBDeposit)), null);
                                }
                                else
                                    propItem.SetValue(newSelect, Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_MCReceipt)), null);
                            }
                            else if (propItem.Name.Equals("OutwardNo"))
                            {
                                propItem.SetValue(newSelect, Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_RSInwardOutwardOutput)), null);
                            }
                            else if ((propItem.Name.Equals("InwardNo")) || (propItem.Name.Equals("IWNo")))
                            {
                                if (!string.IsNullOrEmpty(_select.GetProperty<T, string>("InwardNo")) || !string.IsNullOrEmpty(_select.GetProperty<T, string>("IWNo")))
                                {
                                    propItem.SetValue(newSelect, Utils.TaoMaChungTu(IGenCodeService.getGenCode(ConstFrm.TypeGroup_RSInwardOutward)), null);
                                }
                                else
                                {
                                    propItem.SetValue(newSelect, null, null);
                                }
                            }
                            else if (propItem.Name.Equals("InvoiceNo") || propItem.Name.Equals("InvoiceDate"))
                            {
                                propItem.SetValue(newSelect, null, null);
                            }
                            else if (propItem.PropertyType.Name.Contains("IList"))
                            {
                                if (!propItem.Name.Contains("RefVoucher"))//trungnq có chứng từ tham chiếu thì không copy chứng từ tham chiếu
                                {
                                    //Sửa lại thêm check TH nếu danh sách con gốc có count = 0 thì không sao chép
                                    PropertyInfo propSelect = _select.GetType().GetProperty(propItem.Name);
                                    if (propSelect != null && propSelect.CanWrite)
                                    {
                                        var oldItem = propSelect.GetValue(_select, null);
                                        IList listChild = (IList)oldItem;  //trungnq
                                        bool sort = false;//trungnq
                                                          // if (lstchild.Count == 0) { sort = false; };
                                        if (listChild.Count != 0)
                                        {
                                            foreach (var child in listChild)
                                            {
                                                if (child.HasProperty("OrderPriority"))
                                                {
                                                    if (!child.GetProperty("OrderPriority").IsNullOrEmpty())
                                                        if (child.GetProperty("OrderPriority").ToString() != "")
                                                        { sort = true; }

                                                }
                                            }
                                        }
                                        if (sort) { sortdetailofvouchers(listChild); }//trungnq
                                        if (oldItem != null)
                                        {
                                            int count = ((IList)oldItem).Count;
                                            if (count > 0)
                                            {
                                                propItem.SetValue(newSelect, oldItem, null);//Để lấy đc một ds con mẫu do ở đây ko thể create new List
                                                var newItem = propItem.GetValue(newSelect, null);
                                                for (int i = 0; i < count; i++)
                                                {
                                                    //Fix lại Clone từng dòng con trong ds con rồi bắn trả lại cho ds con mới
                                                    var newSubItem = ((IList)oldItem)[i].CloneObject();
                                                    PropertyInfo propSubItemId = newSubItem.GetType().GetProperty("ID");
                                                    if (propSubItemId != null && propSubItemId.CanWrite)
                                                    {
                                                        propSubItemId.SetValue(((IList)oldItem)[i], Guid.Empty, null);
                                                    }
                                                    PropertyInfo propSubItem1 = newSubItem.GetType().GetProperty("OWPrice");
                                                    if (propSubItem1 != null && propSubItem1.CanWrite)
                                                    {
                                                        propSubItem1.SetValue(((IList)oldItem)[i], (decimal)0, null);
                                                    }
                                                    PropertyInfo propSubItem2 = newSubItem.GetType().GetProperty("OWPriceOriginal");
                                                    if (propSubItem2 != null && propSubItem2.CanWrite)
                                                    {
                                                        propSubItem2.SetValue(((IList)oldItem)[i], (decimal)0, null);
                                                    }
                                                    PropertyInfo propSubItem3 = newSubItem.GetType().GetProperty("OWAmount");
                                                    if (propSubItem3 != null && propSubItem3.CanWrite)
                                                    {
                                                        propSubItem3.SetValue(((IList)oldItem)[i], (decimal)0, null);
                                                    }
                                                    PropertyInfo propSubItem4 = newSubItem.GetType().GetProperty("OWAmountOriginal");
                                                    if (propSubItem4 != null && propSubItem4.CanWrite)
                                                    {
                                                        propSubItem4.SetValue(((IList)oldItem)[i], (decimal)0, null);
                                                    }
                                                    ((IList)newItem)[i] = ((IList)oldItem)[i];
                                                }
                                            }
                                        }
                                    }
                                    var subItems = propItem.GetValue(newSelect, null);
                                    if (subItems != null)
                                    {
                                        foreach (var subItem in (IList)subItems)
                                        {
                                            PropertyInfo propSubItemId = subItem.GetType().GetProperty("ID");
                                            if (propSubItemId != null && propSubItemId.CanWrite)
                                            {
                                                propSubItemId.SetValue(subItem, Guid.Empty, null);
                                            }
                                        }
                                    }
                                }
                            }
                            else if (!propItem.Name.Equals("ID"))
                            {
                                PropertyInfo propSelect = _select.GetType().GetProperty(propItem.Name);
                                if (propSelect != null && propSelect.CanWrite)
                                {
                                    propItem.SetValue(newSelect, propSelect.GetValue(_select, null), null);
                                }
                            }
                        }
                    }
                    BaseService<T, Guid> services = this.GetIService(_select);
                    services.UnbindSession(_select);
                    _select = newSelect;

                    if (_select.HasProperty("StatusSendMail"))
                        _select.SetProperty("StatusSendMail", false);
                    if (_select.HasProperty("StatusInvoice"))
                        _select.SetProperty("StatusInvoice", 0);
                    if (_select.HasProperty("IDAdjustInv"))
                        _select.SetProperty("IDAdjustInv", null);
                    if (_select.HasProperty("IDReplaceInv"))
                        _select.SetProperty("IDReplaceInv", null);
                    _statusForm = ConstFrm.optStatusForm.Edit;
                    _select.CopyTo(ref _backSelect);
                    if (_selectJoin != null)
                        _selectJoin.CopyTo(ref _backSelectJoin);
                    InitializeGUI(_select);

                    if (TypeID == 322)
                    {
                        UltraPanel palVouchersNoGbc = (UltraPanel)Controls.Find("palVouchersNoGbc", true).FirstOrDefault();
                        this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoGbc, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroupMBDeposit, objectDataSource: _select);
                    }
                    else if (TypeID == 321)
                    {
                        UltraPanel palVouchersNoTt = (UltraPanel)Controls.Find("palVouchersNoTt", true).FirstOrDefault();
                        this.ConfigTopVouchersNo<SAInvoice>(palVouchersNoTt, "MNo", "MPostedDate", "MDate", ConstFrm.TypeGroup_MCReceipt, objectDataSource: _select);
                    }
                    _statusForm = ConstFrm.optStatusForm.Add;
                    ReloadToolbar(_select, _listSelects, _statusForm);
                }
            }

            #endregion
            //ResetControlForm();
        }
        protected virtual void RSForm()
        {

        }
        /// <summary>
        /// Reset giá trị của Form về trống hết
        /// </summary>
        protected virtual void ResetControlForm()
        {
            List<UTopVouchers<T>> topVouchers = GetTopVoucher(this);
            if (topVouchers != null && topVouchers.Count > 0)
            {
                foreach (UTopVouchers<T> topVoucher in topVouchers)
                {
                    UltraPanel palTopVouchers = (UltraPanel)topVoucher.Parent.Parent;
                    this.ConfigTopVouchersNo<T>(palTopVouchers, topVoucher.NoBindName, topVoucher.PostedDateBindName,
                                                topVoucher.DateBindName, topVoucher.TypeGroup,
                                                topVoucher.PostedDateVisible, topVoucher.DateVisible,
                                                topVoucher.HaveType, topVoucher.TypeId, topVoucher.CbbType_RowSelected,
                                                topVoucher.ObjectDataSource);
                }
            }
        }

        private List<UTopVouchers<T>> GetTopVoucher(Control control)
        {
            List<UTopVouchers<T>> topVouchers = new List<UTopVouchers<T>>();
            foreach (Control ctrl in control.Controls)
            {
                if (ctrl.Name.Equals("TopVouchersControl") || ctrl.GetType() == typeof(UTopVouchers<T>))
                {
                    topVouchers.Add((UTopVouchers<T>)ctrl);
                }
                else if (ctrl.HasChildren)
                    topVouchers.AddRange(GetTopVoucher(ctrl));
            }
            return topVouchers;
        }
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý
        /// </summary>
        /// <param name="input">đối tượng</param>
        /// <param name="isGet">True: get giá trị từ control gán vào đối tượng| False: set giá trị đối tượng vào control</param>
        /// <returns></returns>
        protected virtual T2 SetOrGetGuiObject<T2>(T2 input, bool isGet)
        {
            SetOrGetGuiObject(isGet);
            if (isGet)
            {//một số trường chưa databinding thì set tay
                if (input.HasProperty("MobilizationOrderDate") && input.GetProperty<T2, DateTime>("MobilizationOrderDate") == Utils.GetDateTimeDefault() || input.GetProperty<T2, DateTime>("MobilizationOrderDate") == Utils.GetDateTimeEmpty()) input.SetProperty("MobilizationOrderDate", null);
                //Logs.WriteTxt("GetObject fnc _ line 594");
                if (_statusForm.Equals(ConstFrm.optStatusForm.Add))
                {
                    //Logs.WriteTxt("GetObject fnc _ line 596");
                    string key = "ID"; if (input.HasProperty(key) && input.GetProperty<T2, Guid>(key) == Guid.Empty) input.SetProperty(key, Guid.NewGuid());  //Thêm mới
                                                                                                                                                              //Logs.WriteTxt("GetObject fnc _ line 598");
                    key = "Recorded"; if (input.HasProperty(key)) input.SetProperty(key, false); //trạng thái ghi sổ mặc định là 1
                                                                                                 //Logs.WriteTxt("GetObject fnc _ line 600");
                                                                                                 //key = "Exported"; if (input.HasProperty(key)) input.SetProperty(key, true); //trạng thái kết xuất (mặc định là 1)
                                                                                                 //Logs.WriteTxt("GetObject fnc _ line 602");
                    key = "IsActive"; if (input.HasProperty(key)) input.SetProperty(key, true); //trạng thái hoạt động
                                                                                                //Logs.WriteTxt("GetObject fnc _ line 604");
                    key = "ExchangeRate"; if (input.HasProperty(key) && input.GetProperty<T2, Decimal?>(key) == null) input.SetProperty(key, (decimal)1); //Tỷ giá
                                                                                                                                                          //Logs.WriteTxt("GetObject fnc _ line 606");
                    GetValue(input); //get value các grid
                                     //Logs.WriteTxt("GetObject fnc _ line 608");
                                     //if (input.HasProperty("RepositoryID") && input.HasProperty("InwardNo") && string.IsNullOrEmpty(input.GetProperty<T2, string>("InwardNo")))
                                     //{
                                     //    input.SetProperty("RepositoryID", null);
                                     //}
                }
                else
                {
                    PropertyInfo propInfoParentId = input.GetType().GetProperty("ID");
                    if (propInfoParentId != null && propInfoParentId.CanWrite)
                    {
                        var id = (Guid?)propInfoParentId.GetValue(input, null);
                        if (id != null)
                        {
                            PropertyInfo[] arrayPInfoParent = input.GetType().GetProperties();
                            foreach (PropertyInfo propInfoParent in arrayPInfoParent)
                            {
                                if (propInfoParent != null && propInfoParent.CanWrite)
                                {
                                    if (propInfoParent.PropertyType.Name.Contains("IList"))
                                    {
                                        IList child = (IList)propInfoParent.GetValue(input, null);
                                        if (child != null && child.Count != 0)
                                        {
                                            foreach (var item in child)
                                            {
                                                PropertyInfo propInfoChildId =
                                                    item.GetType().GetProperty(input.GetType().Name + "ID");
                                                if (propInfoChildId != null && propInfoChildId.CanWrite)
                                                {
                                                    propInfoChildId.SetValue(item, id, null);
                                                }
                                                PropertyInfo propInfoChildResId =
                                                    item.GetType().GetProperty("RepositoryID");
                                                if (propInfoChildResId != null && input.HasProperty("InwardNo") && string.IsNullOrEmpty(input.GetProperty<T2, string>("InwardNo")))
                                                {
                                                    if (propInfoChildResId.CanWrite && propInfoChildResId.CanRead)
                                                        propInfoChildResId.SetValue(item, null, null);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (new int[] { 520 }.Contains(TypeID))
                            {
                                UltraTabControl _tabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                                UltraGrid uGrid0 = (UltraGrid)_tabControl.Tabs[0].TabPage.Controls.Find("uGrid0", true).FirstOrDefault();
                                UltraGrid uGrid = (UltraGrid)_tabControl.Tabs[1].TabPage.Controls.Find("uGrid1", true).FirstOrDefault();
                                BindingList<FADecrementDetail> details = (BindingList<FADecrementDetail>)uGrid0.DataSource;
                                IList lstchild = (IList)uGrid.DataSource;
                                // Nếu là ghi giảm, thêm chứng từ hạch toán
                                //List<FADecrementDetail> details = input.GetProperty<T2, List<FADecrementDetail>>("FADecrementDetails");
                                PropertyInfo propertyInfo = input.GetType().GetProperty("FADecrementDetails");
                                decimal? TotalAmount = 0;
                                for (int i = 0; i < details.Count; i++)
                                {
                                    var post = new List<FADecrementDetailPost>();
                                    lstchild[i * 2].SetProperty("FADecrementDetailID", details[i].ID);
                                    lstchild[i * 2].SetProperty("OrderPriority", 0);
                                    lstchild[i * 2 + 1].SetProperty("FADecrementDetailID", details[i].ID);
                                    lstchild[i * 2 + 1].SetProperty("OrderPriority", 1);

                                    post.Add((FADecrementDetailPost)lstchild[i * 2]);
                                    post.Add((FADecrementDetailPost)lstchild[i * 2 + 1]);
                                    details[i].FADecrementDetailPosts = post;
                                }
                                for (int i = 0; i < lstchild.Count; i++)
                                {
                                    TotalAmount += ((FADecrementDetailPost)lstchild[i]).Description == "Xử lý giá trị còn lại" ? ((FADecrementDetailPost)lstchild[i]).Amount : 0;
                                }
                                input.SetProperty("TotalAmount", TotalAmount);
                                input.SetProperty("TotalAmountOriginal", TotalAmount);
                                if (propertyInfo != null && propertyInfo.CanWrite)
                                {
                                    propertyInfo.SetValue(input, details, null);
                                }
                            }
                            else if (TypeID == 540)
                            {
                                UltraTabControl _tabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                                UltraGrid uGrid1 = (UltraGrid)_tabControl.Tabs[1].TabPage.Controls.Find("uGrid1", true).FirstOrDefault();
                                UltraGrid uGrid2 = (UltraGrid)_tabControl.Tabs[2].TabPage.Controls.Find("uGrid2", true).FirstOrDefault();
                                BindingList<FADepreciationAllocation> lstchild1 = (BindingList<FADepreciationAllocation>)uGrid1.DataSource;
                                BindingList<FADepreciationPost> lstchild12 = ((BindingList<FADepreciationPost>)uGrid2.DataSource).CloneObject();
                                for (int i = 0; i < lstchild1.Count; i++)
                                {
                                    lstchild12[i].ID = Guid.Empty;
                                    lstchild12[i].CostSetID = lstchild1[i].CostSetID;
                                    lstchild12[i].ExpenseItemID = lstchild1[i].ExpenseItemID;
                                    lstchild12[i].FADepreciationID = (Guid)input.GetProperty("ID");
                                }
                                PropertyInfo propertyInfo = input.GetType().GetProperty("FADepreciationPosts");
                                if (propertyInfo != null && propertyInfo.CanWrite)
                                {
                                    propertyInfo.SetValue(input, lstchild12, null);
                                }
                            }
                            else if (TypeID == 434)
                            {
                                UltraTabControl _tabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                                UltraGrid uGrid1 = (UltraGrid)_tabControl.Tabs[1].TabPage.Controls.Find("uGrid1", true).FirstOrDefault();
                                UltraGrid uGrid2 = (UltraGrid)_tabControl.Tabs[2].TabPage.Controls.Find("uGrid2", true).FirstOrDefault();
                                BindingList<TIAllocationAllocated> lstchild1 = ((BindingList<TIAllocationAllocated>)uGrid1.DataSource);
                                BindingList<TIAllocationPost> lstchild12 = (BindingList<TIAllocationPost>)uGrid2.DataSource;
                                for (int i = 0; i < lstchild1.Count; i++)
                                {
                                    lstchild12[i].ID = Guid.Empty;
                                    lstchild12[i].TIAllocationID = (Guid)input.GetProperty("ID");
                                    lstchild12[i].ExpenseItemID = lstchild1[i].ExpenseItemID;
                                }
                                PropertyInfo propertyInfo = input.GetType().GetProperty("TIAllocationPosts");
                                if (propertyInfo != null && propertyInfo.CanWrite)
                                {
                                    propertyInfo.SetValue(input, lstchild12, null);
                                }
                            }
                            else if (TypeID == 690)
                            {
                                UltraTabControl _tabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                                UltraGrid uGrid1 = (UltraGrid)_tabControl.Tabs[1].TabPage.Controls.Find("uGrid1", true).FirstOrDefault();
                                UltraGrid uGrid2 = (UltraGrid)_tabControl.Tabs[2].TabPage.Controls.Find("uGrid2", true).FirstOrDefault();
                                BindingList<GOtherVoucherDetailExpenseAllocation> lstchild1 = ((BindingList<GOtherVoucherDetailExpenseAllocation>)uGrid1.DataSource);
                                BindingList<GOtherVoucherDetail> lstchild12 = (BindingList<GOtherVoucherDetail>)uGrid2.DataSource;
                                for (int i = 0; i < lstchild1.Count; i++)
                                {
                                    lstchild12[i].ID = Guid.Empty;
                                    lstchild12[i].GOtherVoucherID = (Guid)input.GetProperty("ID");
                                    //lstchild12[i].ExpenseItemID = lstchild1[i].ExpenseItemID;comment by cuongpv
                                }
                                PropertyInfo propertyInfo = input.GetType().GetProperty("GOtherVoucherDetails");
                                if (propertyInfo != null && propertyInfo.CanWrite)
                                {
                                    propertyInfo.SetValue(input, lstchild12, null);
                                }
                            }
                        }

                    }
                }
            }
            return input;
            //throw new NotImplementedException();
        }

        protected virtual void SetOrGetGuiObject(bool isGet)
        {
            //
        }

        protected virtual bool CheckError()
        {
            if (_typeID == 120)
            {
                UltraCombo ucombo;
                ucombo = this.Controls.Find("cbbBankAccount", true).FirstOrDefault() as UltraCombo;
                if (!(ucombo.Text.IsNullOrEmpty()))
                {
                    if (!(ucombo.Value is Guid))
                    { //MSG.Warning("Dữ liệu không có trong danh mục");
                        return false;
                    }
                }
                ucombo = this.Controls.Find("cbbAccountingObjectID", true).FirstOrDefault() as UltraCombo;
                if (!(ucombo.Text.IsNullOrEmpty()))
                {
                    if (!(ucombo.Value is Guid))
                    {
                        //MSG.Warning("Dữ liệu không có trong danh mục");
                        return false;
                    }
                }
                ucombo = this.Controls.Find("cbbAccountingObjBank", true).FirstOrDefault() as UltraCombo;
                if (!(ucombo.Text.IsNullOrEmpty()))
                {
                    if (!(ucombo.Value is Guid))
                    {
                        // MSG.Warning("Dữ liệu không có trong danh mục");
                        return false;
                    }
                }
                return true;
            }
            else
                return true;
            //throw new NotImplementedException();
        }
        protected bool CheckErrorBase()
        {
            List<ErrorObj> lstError = new List<ErrorObj>();     //danh sách lỗi
            List<ErrorObj> lstWarning = new List<ErrorObj>();     //danh sách cảnh báo
            List<string> lstIdProperty;                            //key cần check
            string locationProperty = string.Empty;             //control bị lỗi (để focus đến control đó)
            List<string> lstMsgProperty;                        //thông báo lỗi tiếng việt
            string key;
            List<string> blackList = new List<string>();        //danh sách bỏ qua (Black List)
            List<int> allowList = new List<int>();        //danh sách cho phép (Allow List)
            var lstBListType = new List<int[]>();         //danh sách typeID sẽ bỏ qua không check theo từng thuộc tính
            int _typeId = _select.GetProperty<T, int>("TypeID");
            bool err = true;
            try
            {
                #region Header
                {
                    #region ngày chứng từ, ngày hạch toán không được phép rỗng (hiện tại chỉ check ngày ko check đến giờ phút giây)
                    blackList = "FADepreciation".Split(',').ToList();
                    if (!blackList.Contains(_select.GetType().Name))
                    {
                        //bổ sung thêm KEY vào lstIdProperty và lstMsgProperty tương ứng
                        lstIdProperty = new List<string> { "Date", "PostedDate", "ODate", "OPostedDate", "IWDate", "IWPostedDate" };
                        lstBListType = new List<int[]>
                                               {
                                                   new int[] {},
                                                   new int[] {},
                                                   new int[] {230},
                                                   new int[] {230},
                                                   new int[] {},
                                                   new int[] {}
                                               };
                        lstMsgProperty = new List<string> { resSystem.MSG_Error_01, resSystem.MSG_Error_02, resSystem.MSG_Error_01, resSystem.MSG_Error_02, resSystem.MSG_Error_01, resSystem.MSG_Error_02 };
                        for (int i = 0; i < lstIdProperty.Count; i++)
                        {
                            key = lstIdProperty[i];
                            if (!_select.HasProperty(key)) continue;
                            if (lstBListType[i].Contains(TypeID)) continue;
                            DateTime date = _select.GetProperty<T, DateTime>(key);
                            if (!(_typeId == 340 && key == "IWDate" || key == "IWPostedDate"))
                            {
                                if (date == null || date.Date == Utils.GetDateTimeDefault().Date || date.Date == new DateTime().Date)
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                            }
                        }
                        //[Huy Anh]
                        List<UTopVouchers<T>> topVouchers = GetTopVoucher(this);
                        if (topVouchers != null && topVouchers.Count > 0)
                        {
                            foreach (UTopVouchers<T> topVoucher in topVouchers)
                            {
                                if (topVoucher.dteDate.Visible)
                                {
                                    key = topVoucher.dteDate.DataBindings[0].BindingMemberInfo.BindingMember;
                                    int i = lstIdProperty.IndexOf(key);
                                    if (string.IsNullOrEmpty(topVoucher.dteDate.Text) || topVoucher.dteDate.DateTime.Date == Utils.GetDateTimeDefault().Date || topVoucher.dteDate.DateTime.Date == new DateTime().Date)
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                                }
                                if (topVoucher.dtePostedDate.Visible && topVoucher.dtePostedDate.DataBindings.Count > 0)
                                {
                                    key = topVoucher.dtePostedDate.DataBindings[0].BindingMemberInfo.BindingMember;
                                    int j = lstIdProperty.IndexOf(key);
                                    if (string.IsNullOrEmpty(topVoucher.dtePostedDate.Text) || topVoucher.dtePostedDate.DateTime.Date == Utils.GetDateTimeDefault().Date || topVoucher.dtePostedDate.DateTime.Date == new DateTime().Date)
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[j] });
                                }
                            }
                        }
                    }
                    #endregion

                    #region ngày hoạch toán lớn hơn hoặc bằng ngày chứng từ (hiện tại chỉ check ngày ko check đến giờ phút giây)
                    blackList = "".Split(',').ToList();
                    if (!blackList.Contains(_select.GetType().Name))
                    {
                        //bổ sung thêm CẶP KEY vào lstIdProperty và lstMsgProperty tương ứng
                        lstIdProperty = new List<string> { "Date,PostedDate", "ODate,OPostedDate", "IWDate,IWPostedDate" };
                        lstMsgProperty = new List<string> { resSystem.MSG_Error_03 };
                        for (int i = 0; i < lstIdProperty.Count; i++)
                        {
                            string[] lstIdPropertySplit = lstIdProperty[i].Split(',');
                            if (!_select.HasProperty(lstIdPropertySplit[0]) ||
                                !_select.HasProperty(lstIdPropertySplit[1])) continue;
                            key = string.Format("{0} < {1}", lstIdPropertySplit[1], lstIdPropertySplit[0]);
                            DateTime date = _select.GetProperty<T, DateTime>(lstIdPropertySplit[0]);
                            DateTime postdate = _select.GetProperty<T, DateTime>(lstIdPropertySplit[1]);
                            if (postdate.Date < date.Date)
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                            if (_select.GetProperty<T, int>("TypeID") == 220)
                            {
                                var lstdetail = (_select as PPDiscountReturn).PPDiscountReturnDetails;
                                if (lstdetail != null)
                                {
                                    foreach (var detail in lstdetail)
                                    {
                                        if (detail.ConfrontID != null && detail.ConfrontID != Guid.Empty)
                                        {
                                            var ppdetail = IPPInvoiceService.Getbykey(detail.ConfrontID ?? Guid.Empty);
                                            if (ppdetail != null && ppdetail.Date > date)
                                            {
                                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Ngày của chứng từ hàng mua trả lại đang nhỏ hơn chứng từ mua hàng" });
                                            }
                                        }
                                    }
                                }
                            }
                            else if (_select.GetProperty<T, int>("TypeID") == 330)
                            {
                                var lstdetail = (_select as SAReturn).SAReturnDetails;
                                if (lstdetail != null)
                                {
                                    foreach (var detail in lstdetail)
                                    {
                                        if (detail.SAInvoiceID != null && detail.SAInvoiceID != Guid.Empty)
                                        {
                                            var sadetail = ISAInvoiceService.Getbykey(detail.SAInvoiceID ?? Guid.Empty);
                                            if (sadetail != null && sadetail.Date > date)
                                            {
                                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Ngày của chứng từ hàng bán trả lại đang nhỏ hơn chứng từ bán hàng" });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region ngày hoạch toán lớn hơn ngày khóa sổ (hiện tại chỉ check ngày ko check đến giờ phút giây)
                    blackList = "".Split(',').ToList();
                    if (!blackList.Contains(_select.GetType().Name))
                    {
                        DateTime? dbdateClosed = Utils.GetDBDateClosed().StringToDateTime();
                        if (dbdateClosed.HasValue)
                        {
                            lstIdProperty = new List<string> { "PostedDate", "OPostedDate", "IWPostedDate" };
                            lstMsgProperty = new List<string> { string.Format(resSystem.MSG_Error_23, dbdateClosed.Value.ToString("dd/MM/yyyy")) };
                            for (int i = 0; i < lstIdProperty.Count; i++)
                            {
                                if (!_select.HasProperty(lstIdProperty[i]) ||
                                    !_select.HasProperty(lstIdProperty[i])) continue;
                                key = lstIdProperty[i];
                                DateTime postdate = _select.GetProperty<T, DateTime>(key);
                                if (!(_typeId == 340 && key == "IWPostedDate") && !(_typeID == 230 && key == "OPostedDate"))
                                {
                                    if (postdate.Date <= dbdateClosed.Value.Date)
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[0] });
                                }
                            }
                        }
                    }
                    #endregion

                    #region check mã chứng từ tự sinh cho phép người dùng nhập có đúng chuẩn hay không?
                    blackList = "".Split(',').ToList();
                    if (!blackList.Contains(_select.GetType().Name))
                    {
                        List<int> lstIsOutwardNo = new List<int> { 320, 321, 322, 323, 324, 325, 220 };  //check có kiêm phiếu xuất hay ko (IsDeliveryVoucher)
                                                                                                         //bổ sung thêm KEY vào lstIdProperty và lstMsgProperty tương ứng
                        lstIdProperty = new List<string> { "No", "OutwardNo", "IWNo", "InwardNo", "MNo" };//trungnq thêm mno để check số chứng từ phiếu thu, báo có

                        lstBListType = new List<int[]>
                                               {
                                                   new int[] {},
                                                   new int[] {230},
                                                   new int[] {340},
                                                   new int[] {},
                                                   new int[] {320,323}//trungnq thêm để sửa bug 6253 , bán hàg chưa thu tiền thì bỏ qa , không check số chứg từ MNo
                                               
                                                };

                        lstMsgProperty = new List<string> { resSystem.MSG_Error_07, resSystem.MSG_Error_08, resSystem.MSG_Error_16, resSystem.MSG_Error_16, "Số chứng từ phiếu thu hoặc báo có chưa hợp lệ! Xin vui lòng xem lại!" };//trungnq thêm để sửa bug check số chứng từ phiếu thu khi lập kèm chứg từ bán hàg 

                        if (_typeId == 322)
                        {
                            lstMsgProperty = new List<string> { resSystem.MSG_Error_07, resSystem.MSG_Error_08, resSystem.MSG_Error_16, resSystem.MSG_Error_16, "Số chứng từ phiếu thu hoặc báo có chưa hợp lệ! Xin vui lòng xem lại!" };//trungnq thêm để sửa bug check số chứng từ báo có khi lập kèm chứg từ bán hàg 
                        }
                        for (int i = 0; i < lstIdProperty.Count; i++)
                        {
                            key = lstIdProperty[i];
                            if (new[] { "InwardNo" }.Any(x => x == key) && _select.HasProperty("StoredInRepository") && !_select.GetProperty<T, bool>("StoredInRepository")) continue;
                            if (!_select.HasProperty(key)) continue;
                            if (lstBListType[i].Contains(TypeID)) continue;
                            string no = _select.GetProperty<T, string>(key);

                            //trường hợp bán hàng. nếu kiêm phiếu xuất kho thì không check OutwardNo
                            if ((lstIsOutwardNo.Contains(_typeId) && key.Equals("OutwardNo")) || _typeId == 330)
                            {
                                if (!_select.HasProperty("IsDeliveryVoucher")) continue;
                                if (!_select.GetProperty<T, bool>("IsDeliveryVoucher")) continue;
                            }

                            if (string.IsNullOrEmpty(no) || (!string.IsNullOrEmpty(no) && Utils.CheckNo(no) == null))
                            {
                                //if ((_select is PPInvoice) && key == "No")trungnq bỏ đi để thêm lỗi check số chứng từ phiếu thu của ppinvoice
                                //{
                                //    if (_select.HasProperty("InwardNo"))
                                //    {
                                //        string iWNo = _select.GetProperty<T, string>("InwardNo");
                                //        if (!string.IsNullOrEmpty(iWNo))
                                //            goto Next;
                                //    }
                                //}
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });
                            }
                        Next:
                            ;
                        }
                    }
                    #endregion
                }
                #endregion

                #region Thông tin chung
                #region Check Not Null
                #region không được phép để trống đối tượng
                blackList = "".Split(',').ToList();
                allowList = new List<int> { 510, 902, 903, 904, 905, 906, 907, 100, 110, 410, 119, 129, 132, 142, 172, 400, 121, 111, 120, 420, 112, 122 };//edit by cuongpv add 112,122
                if (!blackList.Contains(_select.GetType().Name) && !allowList.Contains(TypeID))//Bỏ qua TH Gi tăng khác
                {
                    key = "AccountingObjectID";
                    if (_select.HasProperty(key))
                    {
                        Guid? guid = _select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_10 });
                    }
                }
                #endregion

                #region Chuyển tiền nội bộ: "Từ TK ngân hàng" & "Đến TK ngân hàng" bắt buộc nhập
                allowList = new List<int> { 150 };
                if (allowList.Contains(_typeId))
                {
                    lstIdProperty = new List<string> { "FromBankAccountDetailID", "ToBankAccountDetailID" };
                    lstMsgProperty = new List<string> { resSystem.MSG_Error_15, resSystem.MSG_Error_15 };
                    for (int i = 0; i < lstIdProperty.Count; i++)
                    {
                        key = lstIdProperty[i];
                        IList iList = _listObjectInput.FirstOrDefault(x => (x is BindingList<MBInternalTransferDetail>));
                        if (iList != null)
                            foreach (var detail in iList)
                            {
                                if (!detail.HasProperty(key)) continue;
                                Guid? guid = (Guid?)detail.GetProperty(key);
                                if (guid == null || guid == Guid.Empty)
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = lstMsgProperty[i] });

                            }
                    }
                }
                #endregion

                #region Mua hàng thanh toán: "Tài khoản đơn vị trả tiền" không được để trống
                //allowList = new List<int> { 260, 261, 262, 263, 264 }; // THOHD sửa ngày 10/11/2017: thanh toán tiền mặt, thẻ tín dụng không cần tài khoản ngân hàng

                allowList = new List<int> { 261, 262, 264 };
                if (allowList.Contains(_typeId))
                {
                    key = "BankAccountDetailID";
                    if (_select.HasProperty(key))
                    {
                        Guid? guid = _select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_28 });
                    }
                }
                #endregion

                #region Nộp tiền tài khoản: bắt buộc phải nhập tài khoản ngân hàng
                allowList = new List<int> { 160, 161, 162, 163 };
                if (allowList.Contains(_typeId))
                {
                    key = "BankAccountDetailID";
                    if (_select.HasProperty(key))
                    {
                        Guid? guid = _select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_15 });
                    }
                }
                #endregion

                #region Thẻ tín dụng: bắt buộc phải nhập số thẻ tín dụng
                allowList = new List<int> { 170, 171, 172, 173, 174, 263 };
                if (allowList.Contains(_typeId))
                {
                    key = "CreditCardNumber";
                    if (_select.HasProperty(key))
                    {
                        string guid = _select.GetProperty<T, string>(key);
                        if (string.IsNullOrEmpty(guid))
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_17 });
                    }
                }
                #endregion

                #region CCDC bắt buộc nhập mã công cụ dụng cụ vào detail
                allowList = new List<int> { 430, 431, 432, 433, 434, 435, 436, 907 };
                if (allowList.Contains(_typeId))
                {

                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty("ToolsID"))
                            {
                                Guid FixedAssetID = item.GetProperty<object, Guid>("ToolsID");
                                if (FixedAssetID.IsNullOrEmpty() || FixedAssetID.Equals(Guid.Empty))
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Mã CCDC không được để trống" });
                            }
                        }
                    }
                }
                #endregion

                #region CCDC bắt buộc nhập tranfer quantity
                allowList = new List<int> { 430, 431, 432, 433, 434, 435, 436 };
                if (allowList.Contains(_typeId))
                {

                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty("TransferQuantity"))
                            {
                                decimal TransferQuantity = item.GetProperty<object, decimal>("TransferQuantity");
                                if (TransferQuantity == 0)
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Số lượng điều chuyển phải lớn hơn 0" });
                                else
                                {
                                    decimal Quantity = item.GetProperty<object, decimal>("Quantity");
                                    if (Quantity < TransferQuantity)
                                    {
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Số lượng điều chuyển phải ít hơn hoặc bằng số lượng đang dùng" });
                                    }
                                }

                            }
                        }
                    }
                }
                #endregion

                #region CCDC Ghi giảm bắt buộc nhập phòng ban
                allowList = new List<int> { 431 };
                if (allowList.Contains(_typeId))
                {

                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty("DepartmentID"))
                            {
                                Guid? DepartmentID = item.GetProperty<object, Guid?>("DepartmentID");
                                if (DepartmentID == null || DepartmentID == Guid.Empty)
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Phòng ban không được để trống" });

                            }
                        }
                    }
                }
                #endregion

                #region TSCĐ: bắt buộc phải nhập mã tài sản
                allowList = new List<int> { 119, 129, 132, 142, 172, 500, 510, 520, 540, 560, 550, 530 };
                if (allowList.Contains(_typeId))
                {
                    key = "FixedAssetID";
                    if (_select.HasProperty(key))
                    {
                        Guid guid = _select.GetProperty<T, Guid>(key);
                        string guid2 = _select.GetProperty<T, string>(key);
                        if (guid.IsNullOrEmpty() && guid2.IsNullOrEmpty())
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_54 });
                    }
                }
                #endregion

                #region TSCĐ: bắt buộc phải nhập mã tài sản vào detail
                allowList = new List<int> { 500, 510, 520, 530, 540, 560, 550 };
                if (allowList.Contains(_typeId))
                {

                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty("FixedAssetID"))
                            {
                                Guid FixedAssetID = item.GetProperty<object, Guid>("FixedAssetID");
                                if (FixedAssetID.IsNullOrEmpty() || FixedAssetID.Equals(Guid.Empty))
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_54 });
                            }
                        }
                    }
                    if (_select.HasProperty(key))
                    {
                        string guid = _select.GetProperty<T, string>(key);
                        if (string.IsNullOrEmpty(guid))
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_54 });
                    }
                }
                #endregion

                #region TH Lắp ráp/ tháo rỡ
                #region Mã hàng: bắt buộc phải nhập mã hàng hóa
                allowList = new List<int> { 900, 901 };
                if (allowList.Contains(_typeId))
                {
                    key = "MaterialGoodsID";
                    if (_select.HasProperty(key))
                    {
                        Guid? guid = _select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_61 });
                    }
                }
                #endregion

                #region Kho: bắt buộc phải nhập Kho
                allowList = new List<int> { 900 };
                if (allowList.Contains(_typeId))
                {
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {

                                if (item.HasProperty("RepositoryID"))
                                {
                                    Guid? guid = item.GetProperty<object, Guid?>("RepositoryID");
                                    if (guid == null || guid == Guid.Empty)
                                    {
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_62 });
                                        this.AddErrorCellIntoAllGridDetail(key, "RepositoryID", item, resSystem.MSG_System_62);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region TK Kho: bắt buộc phải nhập TK Kho
                allowList = new List<int> { 900, 901 };
                if (allowList.Contains(_typeId))
                {
                    key = _typeId.Equals(901) ? "CreditAccount" : "DebitAccount";
                    if (_select.HasProperty(key))
                    {
                        string guid = _select.GetProperty<T, string>(key);
                        if (string.IsNullOrEmpty(guid))
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_63 });
                    }
                }
                #endregion
                #region Phòng ban từ và phòng ban đến không được trùng nhau
                allowList = new List<int> { 550, 433 };
                if (allowList.Contains(_typeId))
                {
                    lstIdProperty = new List<string> { "FromDepartment", "ToDepartment", "FromDepartmentID", "ToDepartmentID" };
                    lstMsgProperty = new List<string> { "Phòng ban từ không được để trống", "Phòng ban đến không được để trống", "Phòng ban từ và phòng ban đến không được trùng nhau" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {
                                for (int i = 0; i < lstIdProperty.Count; i++)
                                {
                                    if (!item.HasProperty(lstIdProperty[i])) continue;
                                    Guid? repositoryId = item.GetProperty<object, Guid?>(lstIdProperty[i]);

                                    if (repositoryId == null || repositoryId == Guid.Empty)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = lstMsgProperty[i % 2] });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[i % 2], item, lstMsgProperty[i % 2]);
                                    }
                                }
                                if (item.HasProperty("FromDepartment") && item.HasProperty("ToDepartment"))
                                {
                                    Guid? ToDepartment = item.GetProperty<object, Guid?>("ToDepartment");
                                    Guid? FromDepartment = item.GetProperty<object, Guid?>("FromDepartment");

                                    if (ToDepartment == FromDepartment)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = lstMsgProperty[2] });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[1], item, lstMsgProperty[2]);
                                    }
                                }

                                if (item.HasProperty("FromDepartmentID") && item.HasProperty("ToDepartmentID"))
                                {
                                    Guid? ToDepartmentID = item.GetProperty<object, Guid?>("ToDepartmentID");
                                    Guid? FromDepartmentID = item.GetProperty<object, Guid?>("FromDepartmentID");

                                    if (ToDepartmentID == FromDepartmentID)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = lstMsgProperty[2] });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[1], item, lstMsgProperty[2]);
                                    }
                                }


                            }
                        }
                    }
                }
                #endregion
                #endregion

                #region Nếu tích chọn "In kèm bảng kê" thì "Số  bảng kê", "Ngày bảng kê", "Tên mặt hàng chung" không được để trống
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name) && _select.HasProperty("isAttachList") && _select.GetProperty<T, bool?>("isAttachList") == true)
                {
                    key = "ListNo";
                    if (_select.HasProperty(key))
                    {
                        string listNo = _select.GetProperty<T, string>(key);
                        if (string.IsNullOrEmpty(listNo))
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_24 });
                    }
                    key = "ListDate";
                    if (_select.HasProperty(key))
                    {
                        DateTime? listDate = _select.GetProperty<T, DateTime?>(key);
                        if (!listDate.HasValue)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_26 });
                    }
                    key = "ListCommonNameInventory";
                    if (_select.HasProperty(key))
                    {
                        string listCommonNameInventory = _select.GetProperty<T, string>(key);
                        if (string.IsNullOrEmpty(listCommonNameInventory))
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_25 });
                    }
                }
                #endregion

                #endregion

                #region Nhập mã số thuế theo chuẩn
                blackList = "PPService".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name))
                {
                    lstIdProperty = new List<string> { "CompanyTaxCode", "TaxCode" };
                    for (int i = 0; i < lstIdProperty.Count; i++)
                    {
                        key = lstIdProperty[i];
                        if (_select.HasProperty(key))
                        {
                            string str = _select.GetProperty<T, string>(key);
                            if (!string.IsNullOrEmpty(str) && !Core.Utils.CheckMST(str))
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_System_44 });
                        }
                    }
                }
                #endregion

                #region Check ngày hóa đơn
                if (new int[] { 320, 321, 322, 323, 324, 325, 220, 326 }.Contains(TypeID) && _select.GetProperty<T, DateTime?>("InvoiceDate") != null && _select.GetProperty<T, Guid?>("InvoiceTypeID") != null && _select.GetProperty<T, string>("InvoiceNo") != null && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo")) && _select.GetProperty<T, string>("InvoiceTemplate") != null && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceTemplate")) && _select.GetProperty<T, int?>("InvoiceForm") != null)
                {
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    var md = Utils.ListTT153PublishInvoiceDetail.Where(x => x.InvoiceTypeID == _select.GetProperty<T, Guid>("InvoiceTypeID") && x.InvoiceTemplate == _select.GetProperty<T, string>("InvoiceTemplate") && x.InvoiceForm == _select.GetProperty<T, int>("InvoiceForm")).ToList();
                    var md1 = md.Where(n => int.Parse(_select.GetProperty<T, string>("InvoiceNo")) >= int.Parse(n.FromNo) && int.Parse(_select.GetProperty<T, string>("InvoiceNo")) <= int.Parse(n.ToNo)).ToList();
                    if (md1.Count != 0)
                    {
                        DateTime time = md1[0].StartUsing;
                        if (md != null)
                        {
                            if (new DateTime(time.Year, time.Month, time.Day) > _select.GetProperty<T, DateTime?>("InvoiceDate"))
                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Ngày hóa đơn phải lớn hơn hoặc bằng ngày bắt đầu sử dụng trên thông báo phát hành!" });
                        }
                    }
                }
                #endregion

                #region Check số hóa đơn
                if (new int[] { 320, 321, 322, 323, 324, 325, 220, 326 }.Contains(TypeID) && _select.GetProperty<T, DateTime?>("InvoiceDate") != null && _select.GetProperty<T, Guid?>("InvoiceTypeID") != null && _select.GetProperty<T, string>("InvoiceNo") != null && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo")) && _select.GetProperty<T, string>("InvoiceTemplate") != null && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceTemplate")) && _select.GetProperty<T, int?>("InvoiceForm") != null)
                {
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    var md = Utils.ListTT153PublishInvoiceDetail.Where(x => x.InvoiceTypeID == _select.GetProperty<T, Guid>("InvoiceTypeID") && x.InvoiceTemplate == _select.GetProperty<T, string>("InvoiceTemplate") && x.InvoiceForm == _select.GetProperty<T, int>("InvoiceForm")).ToList();
                    if (md != null || md.Count > 0)
                    {
                        int a = int.Parse(_select.GetProperty<T, string>("InvoiceNo"));
                        int b = md.Select(n => int.Parse(n.FromNo)).Min();
                        int c = md.Select(n => int.Parse(n.ToNo)).Max();
                        //Check Bán hàng
                        string InvoiceNo = "";
                        var listSAcheck = ISAInvoiceService.GetAll();
                        if (listSAcheck.FirstOrDefault(n => n.ID == _select.GetProperty<T, Guid>("ID")) != null)
                            InvoiceNo = listSAcheck.FirstOrDefault(n => n.ID == _select.GetProperty<T, Guid>("ID")).InvoiceNo;
                        List<SAInvoice> lst = listSAcheck.ToList().Where(x => x.ID != _select.GetProperty<T, Guid>("ID")).ToList();
                        List<SAInvoice> ls = lst.Where(x => x.InvoiceTypeID == _select.GetProperty<T, Guid>("InvoiceTypeID") && x.InvoiceForm == _select.GetProperty<T, int>("InvoiceForm") && x.InvoiceTemplate == _select.GetProperty<T, string>("InvoiceTemplate") && x.InvoiceNo != null && x.InvoiceNo != "").Where(n => n.InvoiceNo == a.ToString().PadLeft(7, '0') && InvoiceNo != a.ToString().PadLeft(7, '0')).ToList();
                        if (a < b || a > c || ls != null)
                        {
                            if (a < b || a > c)
                            {
                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số hóa đơn chưa được thông báo phát hành!" });
                            }
                            else if (ls.Count > 0)
                            {

                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số hóa đơn đã tồn tại!" });
                            }
                        }
                        //Check mua hàng
                        string InvoiceNopDr = "";
                        var listpDrcheck = IPPDiscountReturnService.GetAll();
                        if (listpDrcheck.FirstOrDefault(n => n.ID == _select.GetProperty<T, Guid>("ID")) != null)
                            InvoiceNopDr = listpDrcheck.FirstOrDefault(n => n.ID == _select.GetProperty<T, Guid>("ID")).InvoiceNo;
                        List<PPDiscountReturn> lstpDr = listpDrcheck.ToList().Where(x => x.ID != _select.GetProperty<T, Guid>("ID")).ToList();
                        List<PPDiscountReturn> lspDr = lstpDr.Where(x => x.InvoiceTypeID == _select.GetProperty<T, Guid>("InvoiceTypeID") && x.InvoiceForm == _select.GetProperty<T, int>("InvoiceForm") && x.InvoiceTemplate == _select.GetProperty<T, string>("InvoiceTemplate") && x.InvoiceNo != null && x.InvoiceNo != "").Where(n => n.InvoiceNo == a.ToString().PadLeft(7, '0') && InvoiceNo != a.ToString().PadLeft(7, '0')).ToList();
                        if (a < b || a > c || lspDr != null)
                        {
                            if (a < b || a > c)
                            {
                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số hóa đơn chưa được thông báo phát hành!" });
                            }
                            else if (lspDr.Count > 0)
                            {

                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số hóa đơn đã tồn tại!" });

                            }
                        }
                        //Check xuất hoá đơn
                        string InvoiceNoSaBill = "";
                        var listSaBillcheck = ISABillService.GetAll();
                        if (listSaBillcheck.FirstOrDefault(n => n.ID == _select.GetProperty<T, Guid>("ID")) != null)
                            InvoiceNoSaBill = listSaBillcheck.FirstOrDefault(n => n.ID == _select.GetProperty<T, Guid>("ID")).InvoiceNo;
                        List<SABill> lstSaBill = listSaBillcheck.ToList().Where(x => x.ID != _select.GetProperty<T, Guid>("ID")).ToList();
                        List<SABill> lspSaBill = lstSaBill.Where(x => x.InvoiceTypeID == _select.GetProperty<T, Guid>("InvoiceTypeID") && x.InvoiceForm == _select.GetProperty<T, int>("InvoiceForm") && x.InvoiceTemplate == _select.GetProperty<T, string>("InvoiceTemplate") && x.InvoiceNo != null && x.InvoiceNo != "").Where(n => n.InvoiceNo == a.ToString().PadLeft(7, '0') && InvoiceNo != a.ToString().PadLeft(7, '0')).ToList();
                        if (a < b || a > c || lspSaBill != null)
                        {
                            if (a < b || a > c)
                            {
                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số hóa đơn chưa được thông báo phát hành!" });
                            }
                            else if (lspSaBill.Count > 0)
                            {

                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số hóa đơn đã tồn tại!" });

                            }
                        }
                    }
                }
                #endregion
                #endregion

                #region Grid
                #region không có chi tiết thì ko lưu
                blackList = "GVoucherList".Split(',').ToList();//loai bo truong hop form GVoucherList [DuyNT]
                string typeName = string.Empty;
                if (_selectJoin == null)
                    typeName = _select.GetType().Name;
                else
                {
                    //SaInvoice
                    if (Utils.TypesSaInvoice.Any(p => p == TypeID))
                        typeName = typeof(SAInvoice).Name;
                    //PPInvoice
                    else if (Utils.TypesPpInvoice.Any(p => p == TypeID))
                        typeName = typeof(PPInvoice).Name;
                    //FAIncrement
                    else if (Utils.TypesFaIncrement.Any(p => p == TypeID))
                        typeName = typeof(FAIncrement).Name;
                    //TIIncrement
                    else if (Utils.TypesTiIncrement.Any(p => p == TypeID))
                        typeName = typeof(TIIncrement).Name;
                    //PPService
                    else if (Utils.TypesPpService.Any(p => p == TypeID))
                        typeName = typeof(PPService).Name;
                    //PPDiscountReturn
                    else if (Utils.TypesPpDiscountReturn.Any(p => p == TypeID))
                        typeName = typeof(PPDiscountReturn).Name;
                    //SAReturn
                    else if (Utils.TypesSaReturn.Any(p => p == TypeID))
                        typeName = typeof(SAReturn).Name;
                }
                if (!blackList.Contains(typeName))
                {
                    key = string.Format("{0}Detail", typeName);
                    string keys = string.Format("{0}s", key);
                    IList iList = _listObjectInput.FirstOrDefault(k =>
                                                                      {
                                                                          var fullName = k.GetType().FullName;
                                                                          return fullName != null &&
                                                                                 fullName.Contains(key);
                                                                      });
                    if ((_selectJoin == null ? _select.HasProperty(keys) : _selectJoin.HasProperty(keys)) && iList.Count == 0)
                        if (keys == "FAAdjustmentDetails")
                        {
                            FAAdjustment FAAdjustment = (FAAdjustment)(object)_select;
                            if (FAAdjustment.OldUsedTime == FAAdjustment.NewUsedTime)
                            {
                                lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_Error_04 });
                            }
                            else if (FAAdjustment.FAAdjustmentDetails == null)
                            {
                                FAAdjustment.FAAdjustmentDetails = new List<FAAdjustmentDetail>();
                            }
                        }
                        else
                        {
                            lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_Error_04 });
                        }
                }
                #endregion

                #region TK nợ, có không được để trống. nếu bên Nợ, Có là TK đầu 0 thì ko bắt buộc nhập cả 2, khác không thì bắt buộc nhập cả 2 -  Check chi tiết theo
                blackList = "PPOrder,SAOrder,SAQuote,TITransfer".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name))
                {
                    lstIdProperty = new List<string> { "DebitAccount", "CreditAccount" };
                    key = TypeID != 434 ? string.Format("{0}Detail", _select.GetType().Name) : string.Format("{0}Post", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        // edit tungnt: check nếu là trả tiền NCC hoặc thu tiền KH thì lấy data ở tab2 của grid để check TK nợ , TK có
                        IList lstT = null;
                        if (new int[] { 118, 101, 128, 134, 144, 161, 174 }.Contains(TypeID))
                        {
                            lstT = (IList)(Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid).DataSource;
                        }
                        else
                            lstT = TypeID != 434 ? _listObjectInput.FirstOrDefault(k =>
                                                                            {
                                                                                var fullName = k.GetType().FullName;
                                                                                return fullName != null &&
                                                                                       fullName.Contains(key);
                                                                            })
                                                                            : (IList)(Controls.Find("uGrid2", true).FirstOrDefault() as UltraGrid).DataSource;
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {
                                if (!item.HasProperty(lstIdProperty[0]) || !item.HasProperty(lstIdProperty[1]) || TypeID == 520 || TypeID == 907 || TypeID == 510 || TypeID == 432 || TypeID == 690) continue;
                                string tkno = "";
                                try
                                { tkno = item.GetProperty<object, string>(lstIdProperty[0]); }
                                catch
                                { }
                                string tkco = "";
                                try
                                { tkco = item.GetProperty<object, string>(lstIdProperty[1]); }
                                catch
                                { }
                                //K nợ, có không được để trống. nếu bên Nợ, Có là TK đầu 0 thì ko bắt buộc nhập cả 2, khác không thì bắt buộc nhập cả 2
                                bool a = (string.IsNullOrEmpty(tkno) &&
                                          ((!string.IsNullOrEmpty(tkco)) && (tkco.Substring(0, 1).Equals("0"))))
                                         ||
                                         (string.IsNullOrEmpty(tkco) &&
                                          ((!string.IsNullOrEmpty(tkno)) && (tkno.Substring(0, 1).Equals("0"))));
                                bool b = (!string.IsNullOrEmpty(tkno) && !tkno.Substring(0, 1).Equals("0"))
                                         && (!string.IsNullOrEmpty(tkco) && !tkco.Substring(0, 1).Equals("0"));
                                if (!(a || b))
                                    lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_Error_09 });

                                #region check chi tiết theo
                                Account tkNo = Utils.ListAccount.FirstOrDefault(k => k.AccountNumber.Equals(tkno));
                                Account tkCo = Utils.ListAccount.FirstOrDefault(k => k.AccountNumber.Equals(tkco));
                                //=> cùng chi tiết theo một cái
                                string msg;
                                if (!ReflectionUtils.CheckDetailTypeAccount(_select, item, tkNo, out msg))
                                    lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = msg });
                                if (!ReflectionUtils.CheckDetailTypeAccount(_select, item, tkCo, out msg))
                                    lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = msg });
                                #endregion
                            }
                        }
                    }
                }
                #endregion

                #region Kho, TK Kho không được để trống
                //Kho không được để trống nếu là chứng từ Xuất kho, Nhập kho
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name)
                    || !(new int[] { 210, 260, 261, 262, 263, 264 }.Contains(TypeID) && _select.HasProperty("InwardNo") && string.IsNullOrEmpty(_select.GetProperty<T, string>("InwardNo")))
                    || !(new int[] { 320, 321, 322, 323, 324, 325 }.Contains(TypeID) && _select.HasProperty("OutwardNo") && !string.IsNullOrEmpty(_select.GetProperty<T, string>("OutwardNo"))))
                {
                    lstIdProperty = new List<string> { "RepositoryID" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        if (_select.HasProperty("StoredInRepository") && _select.GetProperty<T, int>("StoredInRepository") != 0)
                        {
                            IList lstT = _listObjectInput.FirstOrDefault(k =>
                            {
                                var fullName = k.GetType().FullName;
                                return fullName != null &&
                                       fullName.Contains(key);
                            });
                            if (lstT != null && lstT.Count > 0)
                            {
                                foreach (object item in lstT)
                                {
                                    if (!item.HasProperty(lstIdProperty[0])) continue;
                                    if (item.HasProperty("MaterialGoodsID"))
                                    {//Nếu hàng hóa là loại dịch vụ thì không check
                                        Guid? mGid = item.GetProperty<object, Guid?>("MaterialGoodsID");
                                        var t = Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == mGid);
                                        if (t != null)
                                        {
                                            if (t.MaterialGoodsType == 2) continue;
                                        }
                                    }
                                    Guid? repositoryId = item.GetProperty<object, Guid?>(lstIdProperty[0]);
                                    if (repositoryId == null || repositoryId == Guid.Empty && _typeID != 210)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_System_51 });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[0], item, resSystem.MSG_System_51);
                                    }
                                }
                            }
                        }

                    }
                }

                //Kho đến, Kho đi không được để trống nếu là chứng từ Chuyển kho
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name) && !(new int[] { 420 }.Contains(TypeID) && _select.HasProperty("InwardNo") && string.IsNullOrEmpty(_select.GetProperty<T, string>("InwardNo"))))
                {
                    lstIdProperty = new List<string> { "FromRepositoryID", "ToRepositoryID" };
                    lstMsgProperty = new List<string> { resSystem.MSG_System_64, resSystem.MSG_System_65, resSystem.MSG_System_80 };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {
                                if (item.HasProperty("FromRepositoryID") && item.HasProperty("ToRepositoryID"))
                                {
                                    Guid? toRepositoryId = item.GetProperty<object, Guid?>("ToRepositoryID");
                                    Guid? fromRepositoryId = item.GetProperty<object, Guid?>("FromRepositoryID");

                                    if (toRepositoryId == fromRepositoryId)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = lstMsgProperty[2] });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[1], item, lstMsgProperty[2]);
                                    }
                                }

                                for (int i = 0; i < lstIdProperty.Count; i++)
                                {
                                    if (!item.HasProperty(lstIdProperty[i])) continue;
                                    Guid? repositoryId = item.GetProperty<object, Guid?>(lstIdProperty[i]);

                                    if (repositoryId == null || repositoryId == Guid.Empty)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = lstMsgProperty[i] });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[i], item, lstMsgProperty[i]);
                                    }
                                }
                            }

                        }
                    }
                }

                //TK Kho của phân hệ Bán hàng
                blackList = "FATransfer,SAQuote,SAOrder".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name))
                {
                    if (new int[] { 320, 321, 322, 323, 324, 325 }.Contains(TypeID) && _select.HasProperty("OutwardNo") && !string.IsNullOrEmpty(_select.GetProperty<T, string>("OutwardNo")))
                    {
                        lstIdProperty = new List<string> { "RepositoryAccount" };
                        key = string.Format("{0}Detail", _select.GetType().Name);
                        string keys = string.Format("{0}s", key);
                        if (_select.HasProperty(keys))
                        {
                            IList lstT = _listObjectInput.FirstOrDefault(k =>
                            {
                                var fullName = k.GetType().FullName;
                                return fullName != null &&
                                       fullName.Contains(key);
                            });
                            if (lstT != null && lstT.Count > 0)
                            {
                                foreach (object item in lstT)
                                {
                                    if (!item.HasProperty(lstIdProperty[0])) continue;
                                    string account = item.GetProperty<object, string>(lstIdProperty[0]);

                                    if (string.IsNullOrEmpty(account))
                                    {
                                        if (item.HasProperty("MaterialGoodsID"))
                                        {//Nếu hàng hóa là loại dịch vụ thì không check
                                            Guid? mGid = item.GetProperty<object, Guid?>("MaterialGoodsID");
                                            var t = Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == mGid);
                                            if (t != null)
                                            {
                                                if (t.MaterialGoodsType == 2) continue;
                                            }
                                        }
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_System_59 });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[0], item, resSystem.MSG_System_59);
                                    }
                                }
                            }
                        }
                    }
                    if (new int[] { 330 }.Contains(TypeID) && _select.HasProperty("IWNo") && !string.IsNullOrEmpty(_select.GetProperty<T, string>("IWNo")))
                    {
                        lstIdProperty = new List<string> { "RepositoryAccount" };
                        key = string.Format("{0}Detail", _select.GetType().Name);
                        string keys = string.Format("{0}s", key);
                        if (_select.HasProperty(keys))
                        {
                            IList lstT = _listObjectInput.FirstOrDefault(k =>
                            {
                                var fullName = k.GetType().FullName;
                                return fullName != null &&
                                       fullName.Contains(key);
                            });
                            if (lstT != null && lstT.Count > 0)
                            {
                                foreach (object item in lstT)
                                {
                                    if (!item.HasProperty(lstIdProperty[0])) continue;
                                    string account = item.GetProperty<object, string>(lstIdProperty[0]);

                                    if (string.IsNullOrEmpty(account))
                                    {
                                        if (item.HasProperty("MaterialGoodsID"))
                                        {//Nếu hàng hóa là loại dịch vụ thì không check
                                            Guid? mGid = item.GetProperty<object, Guid?>("MaterialGoodsID");
                                            var t = Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == mGid);
                                            if (t != null)
                                            {
                                                if (t.MaterialGoodsType == 2) continue;
                                            }
                                        }
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_System_59 });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[0], item, resSystem.MSG_System_59);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region TK Giá vốn của phân hệ Bán hàng
                blackList = "SAQuote,SAOrder".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name))
                {
                    if (new int[] { 320, 321, 322, 323, 324, 325 }.Contains(TypeID) && _select.HasProperty("OutwardNo") && !string.IsNullOrEmpty(_select.GetProperty<T, string>("OutwardNo")))
                    {
                        lstIdProperty = new List<string> { "CostAccount" };
                        key = string.Format("{0}Detail", _select.GetType().Name);
                        string keys = string.Format("{0}s", key);
                        if (_select.HasProperty(keys))
                        {
                            IList lstT = _listObjectInput.FirstOrDefault(k =>
                            {
                                var fullName = k.GetType().FullName;
                                return fullName != null &&
                                       fullName.Contains(key);
                            });
                            if (lstT != null && lstT.Count > 0)
                            {
                                foreach (object item in lstT)
                                {
                                    if (!item.HasProperty(lstIdProperty[0])) continue;
                                    if (item.HasProperty("MaterialGoodsID"))
                                    {//Nếu hàng hóa là loại dịch vụ thì không check
                                        Guid? mGid = item.GetProperty<object, Guid?>("MaterialGoodsID");
                                        var t = Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == mGid);
                                        if (t != null)
                                        {
                                            if (t.MaterialGoodsType == 2)
                                                err = false;
                                        }
                                    }
                                    string account = item.GetProperty<object, string>(lstIdProperty[0]);

                                    if (string.IsNullOrEmpty(account) && err)
                                    {
                                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_System_60 });
                                        this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[0], item, resSystem.MSG_System_60);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region check SL tồn kho theo kho
                //var allowList = new List<int> { 102, 103, 162, 163, 320, 321, 322, 323, 324, 325 };
                //if (allowList.Contains(TypeID))
                //{
                //    lstIdProperty = new List<string> { "Quantity", "MaterialGoodsID", "RepositoryID" };
                //    key = string.Format("{0}Detail", _select.GetType().Name);
                //    string keys = string.Format("{0}s", key);
                //    if (_select.HasProperty(keys))
                //    {
                //        IList lstT = _listObjectInput.FirstOrDefault(k =>
                //        {
                //            var fullName = k.GetType().FullName;
                //            return fullName != null &&
                //                   fullName.Contains(key);
                //        });
                //        if (lstT != null && lstT.Count > 0)
                //        {
                //            foreach (object item in lstT)
                //            {
                //                if (!item.HasProperty(lstIdProperty[0]) || !item.HasProperty(lstIdProperty[1]) || !item.HasProperty(lstIdProperty[2])) continue;
                //                var quantity = item.GetProperty<object, decimal?>(lstIdProperty[0]);
                //                var mGoodsId = item.GetProperty<object, Guid?>(lstIdProperty[1]);
                //                var resId = item.GetProperty<object, Guid?>(lstIdProperty[2]);
                //                if (quantity.HasValue && mGoodsId.HasValue && mGoodsId != Guid.Empty && resId.HasValue && resId != Guid.Empty)
                //                {
                //                    var sum = IRepositoryLedgerService.Query.Where(r => r.MaterialGoodsID == mGoodsId && r.RepositoryID == resId).ToList().Sum(r => r == null ? 0 : ((r.IWQuantity ?? 0) - (r.OWQuantity ?? 0)));
                //                    if (quantity > sum)
                //                    {
                //                        var mGoods = Utils.ListMaterialGoodsCustom.FirstOrDefault(x => x.ID == mGoodsId.Value);
                //                        var repository = Utils.ListRepository.FirstOrDefault(r => r.ID == resId.Value);
                //                        if (mGoods == null || repository == null) continue;
                //                        lstMsgProperty = new List<string> { string.Format("Không thể xuất vật tư, hàng hóa [ {0} - {1} ] quá số lượng tồn trong kho [ {2} - {3} ]. \nSố lượng tồn trong kho [ {2} - {3} ] là: {4} ({5}).\r\nBạn có muốn tiếp tục thực hiện lưu chứng từ này không?", mGoods.MaterialGoodsCode, mGoods.MaterialGoodsName, repository.RepositoryCode, repository.RepositoryName, sum.FormatNumberic(ConstDatabase.Format_Quantity), string.IsNullOrEmpty(mGoods.Unit) ? "Chiếc" : mGoods.Unit) };
                //                        lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = lstMsgProperty[0] });
                //                        //this.AddErrorCellIntoAllGridDetail(key, lstIdProperty[0], item, lstMsgProperty[0]);
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                // Utils.IRefVoucherRSInwardOutwardService.UnbindSession(_select); trungnq
                if (Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_ChoXuatQuaLuongTon").Data != "1")
                {
                    var warnings = new List<ErrorObj>();
                    if (!_select.CheckQuantityByRepository(_listObjectInput, ref warnings))
                    {
                        foreach (var item in warnings)
                        {
                            item.Msg += "\r\nBạn có muốn tiếp tục thực hiện lưu chứng từ này không?";
                        }
                    }
                    lstWarning.AddRange(warnings);
                }


                #endregion

                #region Check số lượng tồn tối thiểu
                if (typeof(T) == typeof(SAInvoice) && Utils.ListSystemOption.FirstOrDefault(x => x.Code == "VTHH_ChoXuatQuaLuongTon").Data != "1")
                {
                    if (_select.GetProperty<object, bool>("IsDeliveryVoucher"))
                    {
                        key = string.Format("{0}Detail", _select.GetType().Name);
                        string keys = string.Format("{0}s", key);
                        if (_select.HasProperty(keys))
                        {
                            IList lstT = _listObjectInput.FirstOrDefault(k =>
                            {
                                var fullName = k.GetType().FullName;
                                return fullName != null &&
                                       fullName.Contains(key);
                            });
                            if (lstT != null && lstT.Count > 0)
                            {
                                foreach (object item in lstT)
                                {
                                    var m = item.GetProperty<object, Guid?>("MaterialGoodsID");
                                    if (!m.HasValue) continue;
                                    var sl = item.GetProperty<object, decimal>("Quantity");
                                    var first = Utils.ListMaterialGoodsCustom.FirstOrDefault(ma => ma.ID == m.Value);
                                    if (first == null) continue;
                                    var mater = Utils.ListMaterialGoods.FirstOrDefault(ma => ma.ID == m.Value);
                                    if (mater == null) continue;
                                    if (first.SumIWQuantity - sl < mater.MinimumStock)
                                    {
                                        lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "VTHH xuất đi đã bằng hoặc vượt quá số lượng tối thiểu! Bạn có muốn lưu chứng từ này không?" });
                                    }

                                }
                            }
                        }
                    }
                }
                #endregion

                #region Check SL nhận/SL xuất theo Đơn mua hàng/Đơn đặt hàng
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name) && (typeof(T) == typeof(PPInvoice) || typeof(T) == typeof(SAInvoice) || typeof(T) == typeof(RSInwardOutward)))
                {
                    lstIdProperty = new List<string> { "MaterialGoodsID", "PPOrderNo", "SAOrderNo", "DetailID", "Quantity" };
                    lstMsgProperty = new List<string> { "Số lượng vật tư [ {0} ] của {2} [ {1} ] đã vượt qua số lượng theo {2}.\r\nBạn có muốn thực hiện việc lưu chứng từ không?" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {
                                if (!item.HasProperty(lstIdProperty[0]) || !item.HasProperty(lstIdProperty[4])) break;
                                var m = item.GetProperty<object, Guid?>(lstIdProperty[0]);
                                if (!m.HasValue) continue;
                                var first = Utils.ListMaterialGoodsCustom.FirstOrDefault(ma => ma.ID == m.Value);
                                if (first == null) continue;
                                var code = first.MaterialGoodsCode;
                                var q = item.GetProperty<object, decimal?>(lstIdProperty[4]);


                                if (item.HasProperty(lstIdProperty[1]))
                                {

                                    var no = item.GetProperty<object, string>(lstIdProperty[1]);
                                    if (item.HasProperty(lstIdProperty[3]))
                                    {
                                        var detailId = item.GetProperty<object, Guid?>(lstIdProperty[3]);
                                        if (detailId.HasValue)
                                        {
                                            var ppOrderDetail = IPPOrderDetailService.Getbykey(detailId.Value);
                                            if (_statusForm == ConstFrm.optStatusForm.Add)
                                            {
                                                if (q > (ppOrderDetail.Quantity - ppOrderDetail.QuantityReceipt))
                                                    lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = string.Format(lstMsgProperty[0], code, no, "đơn mua hàng") });
                                            }
                                            else
                                            {
                                                var model = _IPPInvoiceDetailService.Getbykey(item.GetProperty<object, Guid>("ID"));
                                                if (model != null)
                                                {
                                                    _IPPInvoiceDetailService.UnbindSession(model);
                                                    if (q > (ppOrderDetail.Quantity - ppOrderDetail.QuantityReceipt + model.Quantity))
                                                        lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = string.Format(lstMsgProperty[0], code, no, "đơn mua hàng") });
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (item.HasProperty(lstIdProperty[2]))
                                {
                                    var no = item.GetProperty<object, string>(lstIdProperty[2]);
                                    if (item.HasProperty(lstIdProperty[3]))
                                    {
                                        var detailId = item.GetProperty<object, Guid?>(lstIdProperty[3]);
                                        if (detailId.HasValue)
                                        {
                                            var saOrderDetail = ISAOrderDetailService.Getbykey(detailId.Value);
                                            if (_statusForm == ConstFrm.optStatusForm.Add)
                                            {
                                                if (saOrderDetail != null && q > (saOrderDetail.Quantity - saOrderDetail.QuantityReceipt))
                                                    lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = string.Format(lstMsgProperty[0], code, no, "đơn đặt hàng") });
                                            }
                                            else
                                            {
                                                var model = _ISAInvoiceDetailService.Getbykey(item.GetProperty<object, Guid>("ID"));
                                                if (model != null)
                                                {
                                                    _ISAInvoiceDetailService.UnbindSession(model);
                                                    if (saOrderDetail != null && q > (saOrderDetail.Quantity - saOrderDetail.QuantityReceipt + model.Quantity))
                                                        lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = string.Format(lstMsgProperty[0], code, no, "đơn đặt hàng") });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Check tổng TK thuế 133x, 3331x của 2 Tab hạch toán và Tab Thuế phải bằng nhau
                if (Utils.ListTypeByCreditAccount.Any(p => p.Equals(TypeID)) || Utils.ListTypeByDebitAccount.Any(p => p.Equals(TypeID)) || Utils.ListTypeByBothAccount.Any(p => p.Equals(TypeID)))
                {
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    var iListDetail = _listObjectInput.FirstOrDefault(k =>
                    {
                        var fullName = k.GetType().FullName;
                        return fullName != null &&
                               fullName.Contains(key);
                    });
                    var iListVat = _listObjectInput.FirstOrDefault(k => (from object y in k select y.HasProperty("VATAmount")).FirstOrDefault());
                    if (iListDetail != null && iListVat != null)
                    {
                        decimal totalVatDetail = 0;
                        decimal totalVat = 0;
                        if (Utils.ListTypeByCreditAccount.Any(p => p.Equals(TypeID)))
                        {
                            totalVatDetail = (from object x in iListDetail where x.HasProperty("CreditAccount") && (((string)x.GetProperty("CreditAccount")).StartsWith("133") || ((string)x.GetProperty("CreditAccount")).StartsWith("3331")) && x.HasProperty("Amount") select (decimal)x.GetProperty("Amount")).Sum();
                        }
                        else if (Utils.ListTypeByDebitAccount.Any(p => p.Equals(TypeID)))
                        {
                            totalVatDetail = (from object x in iListDetail where x.HasProperty("DebitAccount") && (((string)x.GetProperty("DebitAccount")).StartsWith("133") || ((string)x.GetProperty("DebitAccount")).StartsWith("3331")) && x.HasProperty("Amount") select (decimal)x.GetProperty("Amount")).Sum();
                        }
                        else
                        {
                            if (_typeID == 600)
                            {
                                //iListDetail = (IList)(from object x in iListDetail where !(((string)x.GetProperty("CreditAccount")).StartsWith("1331") && ((string)x.GetProperty("DebitAccount")).StartsWith("3331")) select x);
                                //for (int i = 0; i < iListDetail.Count; i++)
                                //{
                                //    if (iListDetail[i].HasProperty("CreditAccount") && ((string)iListDetail[i].GetProperty("CreditAccount")).StartsWith("1331") && iListDetail[i].HasProperty("DebitAccount") && ((string)iListDetail[i].GetProperty("DebitAccount")).StartsWith("3331"))
                                //    {
                                //        iListDetail.RemoveAt(i);
                                //        i--;
                                //    }
                                //}
                                totalVatDetail = (from object x in iListDetail where (((x.HasProperty("CreditAccount") && (((string)x.GetProperty("CreditAccount")).StartsWith("133") || ((string)x.GetProperty("CreditAccount")).StartsWith("3331"))) || (x.HasProperty("DebitAccount") && (((string)x.GetProperty("DebitAccount")).StartsWith("133") || ((string)x.GetProperty("DebitAccount")).StartsWith("3331")))) && x.HasProperty("Amount")) && !(((string)x.GetProperty("CreditAccount")).StartsWith("1331") && ((string)x.GetProperty("DebitAccount")).StartsWith("3331")) select (decimal)x.GetProperty("Amount")).Sum();
                            }
                            else
                                totalVatDetail = (from object x in iListDetail where ((x.HasProperty("CreditAccount") && (((string)x.GetProperty("CreditAccount")).StartsWith("133") || ((string)x.GetProperty("CreditAccount")).StartsWith("3331"))) || (x.HasProperty("DebitAccount") && (((string)x.GetProperty("DebitAccount")).StartsWith("133") || ((string)x.GetProperty("DebitAccount")).StartsWith("3331")))) && x.HasProperty("Amount") select (decimal)x.GetProperty("Amount")).Sum();
                        }
                        if (iListVat.Count > 0)
                            totalVat = (from object x in iListVat where x.HasProperty("VATAccount") && x.GetProperty("VATAccount") != null && (((string)x.GetProperty("VATAccount")).StartsWith("133") || ((string)x.GetProperty("VATAccount")).StartsWith("3331")) && x.HasProperty("VATAmount") select (decimal)x.GetProperty("VATAmount")).Sum();
                        if (totalVatDetail != totalVat)
                            lstWarning.Add(new ErrorObj { Id = iListVat.GetType().FullName.GetObjectNameFromBindingName(), Location = locationProperty, Msg = string.Format(resSystem.MSG_Warning_01, _select.GetProperty("No")) });
                    }
                }
                #endregion

                #region Cảnh báo khi không chọn TK chiết khấu khi tiền chiết khấu khác 0
                blackList = "PPService,SABill,SAQuote,SAOrder".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name))
                {
                    lstIdProperty = new List<string> { "DiscountAmountOriginal", "DiscountAccount" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string keys = string.Format("{0}s", key);
                    if (_select.HasProperty(keys))
                    {
                        IList lstT = _listObjectInput.FirstOrDefault(k =>
                        {
                            var fullName = k.GetType().FullName;
                            return fullName != null &&
                                   fullName.Contains(key);
                        });
                        if (lstT != null && lstT.Count > 0)
                        {
                            foreach (object item in lstT)
                            {
                                if (!item.HasProperty(lstIdProperty[0]) || !item.HasProperty(lstIdProperty[1])) continue;
                                if (item.GetProperty<object, decimal>(lstIdProperty[0]) != 0 && string.IsNullOrEmpty(item.GetProperty<object, string>(lstIdProperty[1])))
                                    lstWarning.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = resSystem.MSG_Warning_03 });
                            }
                        }
                    }
                }
                #endregion

                #region Check xuất quá số tồn lô
                //if (new int[] { 410, 411, 412, 413, 414, 415, 420 }.Contains(_typeID) || (new int[] { 320, 321, 322, 323, 324, 325, 330 }.Contains(_typeID) && !string.IsNullOrEmpty(_select.GetProperty<object, string>("OutwardNo"))))
                //{
                //    lstIdProperty = new List<string> { "MaterialGoodsID", "Quantity" };
                //    key = string.Format("{0}Detail", _select.GetType().Name);
                //    string keys = string.Format("{0}s", key);
                //    if (_select.HasProperty(keys))
                //    {
                //        IList lstT = _listObjectInput.FirstOrDefault(k =>
                //        {
                //            var fullName = k.GetType().FullName;
                //            return fullName != null &&
                //                   fullName.Contains(key);
                //        });
                //        IList lstCheck = lstT.CloneObject();
                //        if (lstCheck != null && lstCheck.Count > 0)
                //        {
                //            for (int i = 0; i < lstCheck.Count - 1; i++)
                //            {
                //                var sl = lstCheck[i].GetProperty<object, decimal>("Quantity");
                //                var mid = lstCheck[i].GetProperty<object, Guid>("MaterialGoodsID");
                //                var lotno = lstCheck[i].GetProperty<object, string>("LotNo");
                //                if (lotno != null || !string.IsNullOrEmpty(lotno))
                //                {
                //                    for (int j = i + 1; j < lstCheck.Count; j++)
                //                    {
                //                        var sl1 = lstCheck[j].GetProperty<object, decimal>("Quantity");
                //                        var mid1 = lstCheck[j].GetProperty<object, Guid>("MaterialGoodsID");
                //                        var lotno1 = lstCheck[j].GetProperty<object, string>("LotNo");
                //                        if (mid == mid1 && lotno == lotno1)
                //                        {
                //                            sl = sl + sl1;
                //                            lstCheck.RemoveAt(j);
                //                        }
                //                    }
                //                    var slton = Utils.ListViewRSLotNo.FirstOrDefault(x => x.LotNo == lotno).TotalIOQuantityBalance;
                //                    if (sl > slton)
                //                    {
                //                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Không được xuất quá số tồn của lô!" });
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion

                #region Check chi quá tồn quỹ
                //if (_select.HasProperty("No"))
                //{
                //    key = string.Format("{0}Detail", _select.GetType().Name);
                //    string keys = string.Format("{0}s", key);
                //    if (_select.HasProperty(keys))
                //    {
                //        IList lstT = _listObjectInput.FirstOrDefault(k =>
                //        {
                //            var fullName = k.GetType().FullName;
                //            return fullName != null &&
                //                   fullName.Contains(key);
                //        });
                //        IList lstCheck = lstT.CloneObject();
                //        List<string> lstacc = Utils.ListViewGLPayExceedCash.Select(x => x.Account).ToList();
                //        var lstViewGLPayExceedCash = Utils.ListViewGLPayExceedCash.ToList().CloneObject();
                //        if (lstCheck != null && lstCheck.Count > 0)
                //        {
                //            for (int i = 0; i < lstCheck.Count - 1; i++)
                //            {
                //                var acc = "";
                //                if (lstCheck[i].HasProperty("CreditAccount")) acc = lstCheck[i].GetProperty<object, string>("CreditAccount");                               
                //                var accdis = "";
                //                if (lstCheck[i].HasProperty("DebitAccount")) accdis = lstCheck[i].GetProperty<object, string>("DebitAccount");
                //                var accvat = "";
                //                if (lstCheck[i].HasProperty("DeductionDebitAccount")) accvat = lstCheck[i].GetProperty<object, string>("DeductionDebitAccount");
                //                var accvatspecial = "";
                //                if (lstCheck[i].HasProperty("SpecialConsumeTaxAccount")) accvatspecial = lstCheck[i].GetProperty<object, string>("SpecialConsumeTaxAccount");
                //                var accimptax = "";
                //                if (lstCheck[i].HasProperty("ImportTaxAccount")) accimptax = lstCheck[i].GetProperty<object, string>("ImportTaxAccount");
                //                var accexptax = "";
                //                if (lstCheck[i].HasProperty("ExportTaxAccount")) accexptax = lstCheck[i].GetProperty<object, string>("ExportTaxAccount");
                //                decimal amount = 0;
                //                if (lstCheck[i].HasProperty("Amount") && lstacc.Any(x=>x == acc)) amount = lstCheck[i].GetProperty<object, decimal>("Amount");
                //                decimal discount = 0;
                //                if (lstCheck[i].HasProperty("DiscountAmount") && lstacc.Any(x => x == accdis)) discount = lstCheck[i].GetProperty<object, decimal>("DiscountAmount");                              
                //                decimal vatamcount = 0;
                //                if (lstCheck[i].HasProperty("VATAmount") && lstacc.Any(x => x == accvat)) vatamcount = lstCheck[i].GetProperty<object, decimal>("VATAmount");
                //                decimal vatspecial = 0;
                //                if (lstCheck[i].HasProperty("SpecialConsumeTaxAmount") && lstacc.Any(x => x == accvatspecial)) vatspecial = lstCheck[i].GetProperty<object, decimal>("SpecialConsumeTaxAmount");
                //                decimal imptax = 0;
                //                if (lstCheck[i].HasProperty("ImportTaxAmount") && lstacc.Any(x => x == accimptax)) imptax = lstCheck[i].GetProperty<object, decimal>("ImportTaxAmount");
                //                decimal exptax = 0;
                //                if (lstCheck[i].HasProperty("ExportTaxAmount") && lstacc.Any(x => x == accexptax)) exptax = lstCheck[i].GetProperty<object, decimal>("ExportTaxAmount");

                //                //lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Không được xuất quá số tồn của lô!" });
                //            }
                //        }
                //    }
                //}
                #endregion

                #region Check số lô
                //if (new int[] { 320, 321, 322, 323, 324, 325, 330, 410, 411, 412, 413, 414, 415, 420 }.Contains(_typeID))
                //{
                //    key = string.Format("{0}Detail", _select.GetType().Name);
                //    string keys = string.Format("{0}s", key);
                //    if (_select.HasProperty(keys))
                //    {
                //        IList lstT = _listObjectInput.FirstOrDefault(k =>
                //        {
                //            var fullName = k.GetType().FullName;
                //            return fullName != null &&
                //                   fullName.Contains(key);
                //        });
                //        IList lstCheck = lstT.CloneObject();
                //        if (lstCheck != null && lstCheck.Count > 0)
                //        {
                //            for (int i = 0; i < lstCheck.Count; i++)
                //            {
                //                var lotno = lstCheck[i].GetProperty<object, string>("LotNo");
                //                var mid = lstCheck[i].GetProperty<object, Guid>("MaterialGoodsID");
                //                if (lotno != null || !string.IsNullOrEmpty(lotno))
                //                {                                    
                //                    var check = Utils.ListViewRSLotNo.Any(x => x.LotNo == lotno);
                //                    if (!check)
                //                    {
                //                        lstError.Add(new ErrorObj { Id = keys, Location = locationProperty, Msg = "Số lô không không có trong danh mục!" });
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion

                #region Check TTHĐ
                if (new int[] { 220, 320, 321, 322, 323, 324, 325, 326, 340 }.Contains(_typeID))
                {
                    lstIdProperty = new List<string> { "InvoiceForm", "InvoiceTypeID", "InvoiceTemplate", "InvoiceSeries", "InvoiceNo", "InvoiceDate", "DocumentNo", "DocumentNote" };
                    if (_select.GetProperty<object, bool>("IsBill") || _typeID == 326)
                    {

                        if (_select.GetProperty<object, string>("PaymentMethod") == null || string.IsNullOrEmpty(_select.GetProperty<object, string>("PaymentMethod")))
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[0], Location = locationProperty, Msg = "Hình thức thanh toán không được để trống!" });
                        }
                        if (_select.GetProperty<object, int?>("InvoiceForm") == null)
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[0], Location = locationProperty, Msg = "Hình thức hóa đơn không được để trống!" });
                        }
                        if (_select.GetProperty<object, Guid?>("InvoiceTypeID") == null)
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[1], Location = locationProperty, Msg = "Loại hóa đơn không được để trống!" });
                        }
                        if (_select.GetProperty<object, string>("InvoiceTemplate") == null || string.IsNullOrEmpty(_select.GetProperty<object, string>("InvoiceTemplate")))
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[2], Location = locationProperty, Msg = "Mẫu số hóa đơn không được để trống!" });
                        }
                        if (_select.GetProperty<object, string>("InvoiceSeries") == null || string.IsNullOrEmpty(_select.GetProperty<object, string>("InvoiceSeries")))
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[3], Location = locationProperty, Msg = "Ký hiệu hóa đơn không được để trống!" });
                        }
                        if ((_select.GetProperty<object, string>("InvoiceNo") == null || string.IsNullOrEmpty(_select.GetProperty<object, string>("InvoiceNo"))) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data == "0")
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[4], Location = locationProperty, Msg = "Số hóa đơn không được để trống!" });
                        }
                        if (_select.GetProperty<object, string>("InvoiceNo") != null && !string.IsNullOrEmpty(_select.GetProperty<object, string>("InvoiceNo")) && _select.GetProperty<object, string>("InvoiceNo").Length != 7)
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[4], Location = locationProperty, Msg = "Số hoá đơn phải là kiểu số và đúng 7 chữ số." });
                        }
                        if (_select.GetProperty<object, DateTime?>("InvoiceDate") == null)
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[5], Location = locationProperty, Msg = "Ngày hóa đơn không được để trống!" });
                        }
                        if (_select is SABill)
                        {
                            if ((_select as SABill).StatusInvoice == 8)
                            {
                                SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                                if (!string.IsNullOrEmpty(systemOption.Data))
                                {
                                    SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                                    if (supplierService != null)
                                    {
                                        if (supplierService.SupplierServiceCode == "MIV")
                                        {
                                            if ((_select is SABill) && _select.GetProperty<object, string>("DocumentNo") == null || string.IsNullOrEmpty(_select.GetProperty<object, string>("DocumentNo")))
                                            {
                                                lstError.Add(new ErrorObj { Id = lstIdProperty[6], Location = locationProperty, Msg = "Số văn bản không được để trống!" });
                                            }
                                            if ((_select is SABill) && _select.GetProperty<object, string>("DocumentNote") == null || string.IsNullOrEmpty(_select.GetProperty<object, string>("DocumentNote")))
                                            {
                                                lstError.Add(new ErrorObj { Id = lstIdProperty[7], Location = locationProperty, Msg = "Ghi chú không được để trống!" });
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (_select.GetProperty<object, bool>("IsAttachListBill"))
                    {
                        if (_select.GetProperty<object, Guid?>("BillRefID") == null)
                        {
                            lstError.Add(new ErrorObj { Id = lstIdProperty[2], Location = locationProperty, Msg = "Chưa chọn hóa đơn xuất rời!" });
                        }
                    }
                }
                #endregion
                #endregion

                #region Other
                #region check chứng từ đã tồn tại
                blackList = "".Split(',').ToList();
                if (!blackList.Contains(_select.GetType().Name))
                {
                    //bổ sung thêm KEY vào lstIdProperty và lstMsgProperty tương ứng
                    lstIdProperty = new List<string> { "No", "OutwardNo", "IWNo", "InwardNo", "MNo" };
                    lstMsgProperty = new List<string> { resSystem.MSG_Error_05, resSystem.MSG_Error_06, resSystem.MSG_Error_05, resSystem.MSG_Error_05, resSystem.MSG_Error_05 };
                    for (int i = 0; i < lstIdProperty.Count; i++)
                    {
                        key = lstIdProperty[i];
                        if (!_select.HasProperty(key)) continue;

                        //trường hợp bán hàng. nếu kiêm phiếu xuất kho thì không check OutwardNo
                        if ((new List<int> { 320, 321, 322, 323, 324, 325 }).Contains(_typeId) && key.Equals("OutwardNo"))
                        {
                            if (!_select.HasProperty("IsDeliveryVoucher")) continue;
                            if (!_select.GetProperty<T, bool>("IsDeliveryVoucher")) continue;
                        }

                        if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                        {//[Huy Anh] Thêm check trường hợp Edit nhưng đã thay đổi chứng từ
                            if (_backSelect.HasProperty(key))
                            {
                                string no1 = _select.GetProperty<T, string>(key) == null ? "" : _select.GetProperty<T, string>(key);
                                string no2 = _backSelect.GetProperty<T, string>(key) == null ? "" : _select.GetProperty<T, string>(key);
                                if ((_select.GetProperty<T, string>(key) == null && _backSelect.GetProperty<T, string>(key) == null) || no1 == no2)
                                    continue;
                            }

                        }

                        try
                        {
                            if (key.Equals("OutwardNo") || key.Equals("IWNo") || key.Equals("InwardNo"))
                            {
                                string temp = _select.GetProperty<T, string>(key) == null ? "" : _select.GetProperty<T, string>(key);
                                var check = IRSInwardOutwardService.Query.Any(x => x.No == temp);
                                if (check)
                                {
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], temp) });
                                }
                                //RSInwardOutward @object =
                                //    Utils.IRSInwardOutwardService.Query.FirstOrDefault(
                                //        k =>
                                //        k.ID == _select.GetProperty<T, Guid>(key) &&
                                //        k.IsInwardOutwardType == (key.Equals("OutwardNo") ? 0 : 1));
                                //if (@object != null)
                                //    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], @object.GetProperty<RSInwardOutward, string>(key)) });
                            }
                            else if (key.Equals("MNo"))
                            {
                                if (new int[] { 321, 324 }.Contains(_typeId))
                                {
                                    string temp = _select.GetProperty<T, string>(key) == null ? "" : _select.GetProperty<T, string>(key);
                                    var check = IMCReceiptService.Query.Any(x => x.No == temp);
                                    if (check)
                                    {
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], temp) });
                                    }
                                }
                                if (new int[] { 322, 325 }.Contains(_typeId))
                                {
                                    string temp = _select.GetProperty<T, string>(key) == null ? "" : _select.GetProperty<T, string>(key);
                                    var check = IMBDepositService.Query.Any(x => x.No == temp);
                                    if (check)
                                    {
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], temp) });
                                    }
                                }
                            }
                            else if (key.Equals("No"))
                            {
                                string temp = _select.GetProperty<T, string>(key);
                                var specialList = new List<int> { 119, 260, 261, 262, 263, 264 };
                                if (specialList.Contains(TypeID))//TH check chứng từ móc đặc biệt
                                {
                                    switch (TypeID)
                                    {
                                        case 260:
                                        case 119:
                                            {
                                                var check = IMCPaymentService.GetByNo(temp);
                                                if (check != null)
                                                {
                                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], temp) });
                                                    if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                                                        IMCPaymentService.UnbindSession(check);
                                                }
                                            }
                                            break;
                                        case 261:
                                        case 262:
                                        case 264:
                                        case 129 - 132 - 142:
                                            {
                                                var check = IMBTellerPaperService.GetByNo(temp);
                                                if (check != null)
                                                {
                                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], temp) });
                                                    if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                                                        IMBTellerPaperService.UnbindSession(check);
                                                }
                                            }
                                            break;
                                        case 263:
                                        case 172:
                                            {
                                                var check = IMBCreditCardService.GetByNo(temp);
                                                if (check != null)
                                                {
                                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], temp) });
                                                    if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                                                        IMBCreditCardService.UnbindSession(check);
                                                }
                                            }
                                            break;
                                    }
                                }
                                // Hautv comment tối ưu tốc độ
                                //BaseService<T, Guid> service = this.GetIService(_select);
                                //List<T> tempp = service.Query.ToList().CloneObject();
                                //T @object = (T)Activator.CreateInstance(typeof(T));
                                //foreach (T item in tempp)
                                //{
                                //    string temppp = item.GetProperty<T, string>(key);
                                //    if (!temppp.Equals(temp)) continue;
                                //    @object = item;
                                //    break;
                                //}
                                BaseService<T, Guid> service = this.GetIService(_select);
                                T @object = (T)Activator.CreateInstance(typeof(T));
                                T obj = service.Query.ToList().Where(n => (n.GetProperty(key) == null ? "" : n.GetProperty(key).ToString()).Equals(temp)).FirstOrDefault();
                                if (obj != null)
                                {
                                    @object = obj.CloneObject();
                                }

                                // T @object = .FirstOrDefault(k => k.GetProperty<T, string>(key).Equals(test));
                                //if (@object != null && !string.IsNullOrEmpty(@object.GetProperty<T, string>(key)))
                                if (!string.IsNullOrEmpty(@object.GetProperty<T, string>(key)))
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = string.Format(lstMsgProperty[i], @object.GetProperty<T, string>(key)) });
                                //if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                                ////[H.A] Thêm UnbindSession cho TH Edit để tránh lỗi difference session
                                //service.UnbindSession(tempp);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Mất mạng hay gì gì đó! bị lỗi hay bị catch gì đó....
                            string msgLocation = string.Format(resSystem.MSG_Error_11, _select.GetProperty<T, string>(key));
                            lstError.Add(new ErrorObj
                            {
                                Id = key,
                                Location = locationProperty,
                                Msg = msgLocation,
                                Exception = ex
                            });
                        }
                    }
                }
                #endregion
                #endregion

                #region Check những trường not null thì không được null default
                //cách 1: for properties
                //cách 2: get data from csdl sys.colums, sys.tables
                #region Báo lỗi nếu bị yêu cầu trường NV bán hàng ko được để trống
                if (Utils.GetIsCheckEmployee())
                {
                    blackList = "".Split(',').ToList();
                    allowList = new List<int> { 102, 103, 162, 163, 300, 310, 320, 321, 322, 323, 324, 325, 330, 340, 403, 412 };
                    if (!blackList.Contains(_select.GetType().Name) && allowList.Contains(TypeID))
                    {
                        key = "EmployeeID";
                        if (_select.HasProperty(key))
                        {
                            Guid? guid = _select.GetProperty<T, Guid?>(key);
                            if (guid == null || guid == Guid.Empty)
                                lstWarning.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Warning_02 });
                        }
                    }
                }
                #endregion

                #region Check Template
                //[Huy Anh]
                //if (this.CheckExistObject<Template>(((Guid)_templateID))) lstError.Add(new ErrorObj { Id = "Utils.IsTemplateError", Location = locationProperty, Msg = resSystem.MSG_System_46 });
                //if(Utils.Checked.Equals(false))  
                var lstErrorForm = this.CheckGridForm();
                if (lstErrorForm.Count > 0)
                {
                    KeyValuePair<UltraGridCell, string> keyError = new KeyValuePair<UltraGridCell, string>(null, null);
                    //UltraGridCell cell = null;
                    keyError = GetErrorFromListErrorForm(lstErrorForm, true);
                    //= lstErrorForm.FirstOrDefault(c => !((UltraGridCell)c.Key).Column.Hidden).Key;
                    //if (keyError.Key == null && keyError.Value == null) keyError = lstErrorForm.FirstOrDefault();                                      
                    if (!(keyError.Key == null && keyError.Value == null))
                    {
                        if ((new int[] { 320, 321, 322, 323, 324, 325 }.Contains(TypeID) && _select.HasProperty("OutwardNo") && string.IsNullOrEmpty(_select.GetProperty<T, string>("OutwardNo"))) || !err) { }
                        else lstError.Add(new ErrorObj { Id = "IsFormError", Location = locationProperty, Object = keyError.Key, Msg = keyError.Value });
                    }

                }
                #endregion
                #endregion

                // THOHD
                #region Kiểm tra tài khoản tiền tệ phải giống với loại tiền tệ được chọn
                // check trường CurrencyID của bảng chính
                /*
                if (_select.HasProperty("CurrencyID"))
                {
                    lstIdProperty = new List<string> { "DebitAccount", "CreditAccount" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    string curencyID = _select.GetProperty<object, string>("CurrencyID").Trim();
                    List<string> accountNgoaite = new List<string>() { "1112", "1122" };
                    List<string> accountVND = new List<string>() { "1111", "1121" };
                    List<string> accountDisable = String.IsNullOrEmpty(curencyID) ? new List<string>() : (curencyID == "VND" ? accountNgoaite : accountVND);
                    IList lstT = _listObjectInput.FirstOrDefault(k =>
                    {
                        var fullName = k.GetType().FullName;
                        return fullName != null && fullName.Contains(key);
                    });
                    if (lstT != null && lstT.Count > 0)
                    {
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty(lstIdProperty[0]))
                            {
                                string DebitAccount = item.GetProperty<object, string>(lstIdProperty[0]);
                                if (!string.IsNullOrEmpty(DebitAccount) && accountDisable.Contains(DebitAccount))
                                {
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Tài khoản nợ không khớp với loại tiền tệ. Vui lòng kiểm tra lại!" });
                                }
                            }

                            if (item.HasProperty(lstIdProperty[1]))
                            {
                                string CreditAccount = item.GetProperty<object, string>(lstIdProperty[1]);
                                if (!string.IsNullOrEmpty(CreditAccount) && accountDisable.Contains(CreditAccount))
                                {
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Tài khoản có không khớp với loại tiền tệ. Vui lòng kiểm tra lại!" });
                                }
                            }
                        }
                    }
                }
                // check trường CurrencyID của bảng chi tiết
                else
                {
                    //lstIdProperty = new List<string> { "DebitAccount", "CreditAccount" };
                    lstIdProperty = new List<string> { "CreditAccount" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    IList lstT = _listObjectInput.FirstOrDefault(k =>
                    {
                        var fullName = k.GetType().FullName;
                        return fullName != null && fullName.Contains(key);
                    });
                    if (lstT != null && lstT.Count > 0)
                    {
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty("CurrencyID"))
                            {
                                string curencyID = item.GetProperty<object, string>("CurrencyID").Trim();
                                List<string> accountNgoaite = new List<string>() { "1112", "1122" };
                                List<string> accountVND = new List<string>() { "1111", "1121" };
                                List<string> accountDisable = String.IsNullOrEmpty(curencyID) ? new List<string>() : (curencyID == "VND" ? accountNgoaite : accountVND);

                                if (item.HasProperty(lstIdProperty[0]))
                                {
                                    string DebitAccount = item.GetProperty<object, string>(lstIdProperty[0]);
                                    if (!string.IsNullOrEmpty(DebitAccount) && accountDisable.Contains(DebitAccount))
                                    {
                                        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Tài khoản nợ không khớp với loại tiền tệ. Vui lòng kiểm tra lại!" });
                                    }
                                }

                                //if (item.HasProperty(lstIdProperty[1]))
                                //{
                                //    string CreditAccount = item.GetProperty<object, string>(lstIdProperty[1]);
                                //    if (!string.IsNullOrEmpty(CreditAccount) && accountDisable.Contains(CreditAccount))
                                //    {
                                //        lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Tài khoản có không khớp với loại tiền tệ. Vui lòng kiểm tra lại!" });
                                //    }
                                //}
                            }
                        }
                    }
                }
                */
                #endregion
                #region kiểm tra tài khoản chuyển tiền và nhận tiền không trùng nhau
                allowList = new List<int> { 150 };
                if (allowList.Contains(TypeID))
                {
                    lstIdProperty = new List<string> { "FromBankAccountDetailID", "ToBankAccountDetailID" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    IList lstT = _listObjectInput.FirstOrDefault(k =>
                    {
                        var fullName = k.GetType().FullName;
                        return fullName != null && fullName.Contains(key);
                    });
                    if (lstT != null && lstT.Count > 0)
                    {
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty(lstIdProperty[0]) && item.HasProperty(lstIdProperty[1]))
                            {
                                Guid? FromAccount = item.GetProperty<object, Guid?>(lstIdProperty[0]);
                                Guid? ToAccount = item.GetProperty<object, Guid?>(lstIdProperty[1]);
                                if (FromAccount != null && ToAccount != null && FromAccount == ToAccount)
                                {
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_35 });
                                }
                            }
                        }
                    }
                }
                #endregion

                // [Hiếu Giề]
                #region kiểm tra tài khoản nợ và tài khoản có phải khác nhau
                allowList = new List<int> { 530 };
                if (allowList.Contains(TypeID))
                {
                    lstIdProperty = new List<string> { "DebitAccount", "CreditAccount" };
                    key = string.Format("{0}Detail", _select.GetType().Name);
                    IList lstT = _listObjectInput.FirstOrDefault(k =>
                    {
                        var fullName = k.GetType().FullName;
                        return fullName != null && fullName.Contains(key);
                    });
                    if (lstT != null && lstT.Count > 0)
                    {
                        foreach (object item in lstT)
                        {
                            if (item.HasProperty(lstIdProperty[0]) && item.HasProperty(lstIdProperty[1]))
                            {
                                string DebitAccount = item.GetProperty<object, string>(lstIdProperty[0]);
                                string CreditAccount = item.GetProperty<object, string>(lstIdProperty[1]);
                                if (DebitAccount != null && CreditAccount != null && DebitAccount.CompareTo(CreditAccount) == 0)
                                {
                                    lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Định khoản không hợp lệ. Vui lòng thực hiện lại!" });
                                }
                            }
                        }
                    }
                }
                #endregion

                #region KH TSCĐ, kiểm tra tháng khấu hao và ngày hạch toán
                allowList = new List<int> { 540 };
                if (allowList.Contains(TypeID))
                {
                    var PostedDate = _select.GetProperty<T, DateTime>("PostedDate");
                    var Month = _select.GetProperty<T, int>("Month");
                    var Year = _select.GetProperty<T, int>("Year");
                    if (PostedDate.Month != Month || PostedDate.Year != Year)
                    {
                        lstError.Add(new ErrorObj { Id = "PostedDate", Location = locationProperty, Msg = "Ngày hạch toán không thuộc tháng tính khấu hao. Vui lòng điều chỉnh ngày hạch toán và thực hiện lại!" });
                    }
                    else
                    {
                        key = "BankAccountDetailID";
                        if (_select.HasProperty(key))
                        {
                            Guid? guid = _select.GetProperty<T, Guid?>(key);
                            if (guid == null || guid == Guid.Empty)
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = resSystem.MSG_Error_10 });
                        }

                    }
                }
                #endregion
                #region Kiểm tra tài khoản đơn vị nhân tiền
                allowList = new List<int> { 129 };
                if (allowList.Contains(TypeID))
                {
                    key = "BankAccountDetailID";
                    if (_select.HasProperty(key))
                    {
                        Guid? guid = _select.GetProperty<T, Guid?>(key);
                        if (guid == null || guid == Guid.Empty)
                            lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Bạn phải chọn tài khoản của đơn vị trả tiền" });
                    }

                }
                #endregion
                // Doanhnn
                #region Check Mã Nhân Viên không được để trống
                #region Bảng tổng hợp chấm công
                allowList = new List<int> { 810, 811 };
                if (allowList.Contains(TypeID))
                {
                    key = "EmployeeID";
                    foreach (PSTimeSheetSummaryDetail item in _select.GetProperty<T, IList>("PSTimeSheetSummaryDetails"))
                    {
                        if (item.HasProperty(key))
                        {
                            Guid? guid = item.EmployeeID;
                            if (guid == null || guid == Guid.Empty)
                            {
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Mã Nhân Viên không được để trống" });
                                break;
                            }
                        }
                    }
                }
                #endregion
                #region Bảng chấm công
                allowList = new List<int> { 820, 821 };
                if (allowList.Contains(TypeID))
                {
                    key = "EmployeeID";
                    foreach (PSTimeSheetDetail item in _select.GetProperty<T, IList>("PSTimeSheetDetails"))
                    {
                        if (item.HasProperty(key))
                        {
                            Guid? guid = item.EmployeeID;
                            if (guid == null || guid == Guid.Empty)
                            {
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Mã Nhân Viên không được để trống" });
                                break;
                            }
                        }
                    }
                }
                #endregion
                #region Bảng lương
                allowList = new List<int> { 830, 831, 832, 833, 834 };
                if (allowList.Contains(TypeID))
                {
                    key = "EmployeeID";
                    foreach (PSSalarySheetDetail item in _select.GetProperty<T, IList>("PSSalarySheetDetails"))
                    {
                        if (item.HasProperty(key))
                        {
                            Guid? guid = item.EmployeeID;
                            if (guid == null || guid == Guid.Empty)
                            {
                                lstError.Add(new ErrorObj { Id = key, Location = locationProperty, Msg = "Mã Nhân Viên không được để trống" });
                                break;
                            }
                        }
                    }
                }
                #endregion

                #endregion
            }
            catch (Exception exception)
            {
                MSG.Warning(string.Format("Có lỗi xảy ra khi check lỗi! \r\n Chi tiết lỗi: \r\n\r\n {0} \r\n\r\n {1}", exception.Message, exception.StackTrace));
                return false;
            }
            if (lstError.Count == 0)
            {
                if (lstWarning.Count == 0)
                    return true;
                return MSG.MessageBoxStand(lstWarning[0].Msg, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes;
            }
            MSG.Warning(lstError[0].Msg);
            if (lstError[0].Object != null)
            {
                if (lstError[0].Object is UltraGridCell)
                {
                    var cell = (UltraGridCell)lstError[0].Object;
                    var grid = (UltraGrid)cell.Band.Layout.Grid;
                    if (grid.Parent is UltraTabPageControl)
                    {
                        var tab = (UltraTabPageControl)(grid.Parent);
                        var tabControl = (UltraTabControl)grid.Parent.Parent;
                        tabControl.Focus();
                        tabControl.ActiveTab = tab.Tab;
                        tabControl.SelectedTab = tab.Tab;
                    }
                    grid.Focus();
                    grid.ActiveCell = cell;
                    cell.Activate();
                    cell.Row.ScrollRowToMiddle();
                    grid.PerformAction(UltraGridAction.EnterEditMode, false, false);
                }
            }
            return false;
            //throw new NotImplementedException();
        }
        private KeyValuePair<UltraGridCell, string> GetErrorFromListErrorForm(Dictionary<UltraGridCell, string> lstErrorForm, bool checkHidden)
        {
            KeyValuePair<UltraGridCell, string> keyError = new KeyValuePair<UltraGridCell, string>(null, null);
            foreach (var item in lstErrorForm)
            {
                if (checkHidden && ((UltraGridCell)item.Key).Column.Hidden) continue;
                UltraGridCell cell = item.Key;
                if (new int[] { 210, 260, 261, 262, 263, 264 }.Contains(TypeID) && _select.HasProperty("InwardNo") && string.IsNullOrEmpty(_select.GetProperty<T, string>("InwardNo")))
                {
                    if (item.Value == string.Format("Lỗi: {0} \nVui lòng kiểm tra lại dòng thứ [{1}] của cột \"{2}\" !", resSystem.MSG_System_51, cell.Row.Index + 1, cell.Column.Header.Caption))
                        continue;
                }
                keyError = item;
                break;
            }
            if (keyError.Key == null && keyError.Value == null)
                keyError = GetErrorFromListErrorForm(lstErrorForm, false);
            return keyError;
        }

        protected virtual bool Saveleged()
        {//nếu frm nào không dùng ghi sổ có thể over lại và để trống
            string msg = "";
            return _select.Saveleged(_selectJoin, TypeID, ref msg);
        }
        protected virtual bool Removeleged()
        {//nếu frm nào không dùng ghi sổ có thể over lại và để trống
            return _select.Removeleged(_selectJoin, TypeID);
        }
        protected virtual bool Delete()
        {
            bool status = true;
            try
            {
                BaseService<T, Guid> services = this.GetIService(_select);
                //bool runRsInwardOutward = _select.HasProperty("OutwardNo") && !string.IsNullOrEmpty(_select.GetProperty<T, string>("OutwardNo"));
                //RSInwardOutward rsInwardOutward = runRsInwardOutward ? IRSInwardOutwardService.getMCRSInwardOutwardbyNo(_select.GetProperty<T, string>("OutwardNo")) : new RSInwardOutward();
                try
                {
                    services.BeginTran();
                    _statusForm = ConstFrm.optStatusForm.Delete;
                    //if (runRsInwardOutward) IRSInwardOutwardService.Delete(rsInwardOutward);
                    var guid = _select.GetProperty<T, Guid>("ID");
                    services.Delete(guid);

                    #region Nếu là kiểm kê TSCĐ, recommendation == ghi giảm => tạo chứng từ ghi giảm
                    if (TypeID == 560)
                    {
                        FAAudit fAAudit = (FAAudit)(object)_select;
                        FADecrement oldFaDecrement = IFADecrementService.findByRefID(fAAudit.ID);
                        if (oldFaDecrement != null)
                        {
                            // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                            if (oldFaDecrement.Recorded)
                            {
                                MSG.Warning("Bảng kiểm kê tài sản đã có phát sinh chứng từ xử lý ghi giảm. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                return false;
                            }
                            IFADecrementService.Delete(oldFaDecrement);
                        }

                    }
                    #endregion
                    #region Nếu là kiểm kê CCDC, kiểm tra recommendation == ghi giảm || ghi tăng
                    if (TypeID == 435)
                    {
                        TIAudit fAAudit = (TIAudit)(object)_select;
                        TIDecrement oldTIDecrement = ITIDecrementService.findByRefID(fAAudit.ID);
                        TIIncrement oldTIIncrement = ITIIncrementService.findByRefID(fAAudit.ID);
                        if ((oldTIDecrement != null && oldTIDecrement.Recorded) || (oldTIIncrement != null && oldTIIncrement.Recorded))
                        {
                            // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                            MSG.Warning("Bảng kiểm kê CCDC đã có phát sinh chứng từ xử lý ghi giảm hoặc ghi tăng. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                            services.RolbackTran();
                            IFADecrementService.RolbackTran();
                            return false;
                        }
                        else
                        {
                            if (oldTIDecrement != null)
                                ITIDecrementService.Delete(oldTIDecrement);
                            if (oldTIIncrement != null)
                                ITIIncrementService.Delete(oldTIIncrement);
                        }

                    }
                    #endregion
                    #region Nếu là kiểm kê qũy
                    if (TypeID == 180)
                    {
                        MCAudit mcAudit = (MCAudit)(object)_select;
                        MCPayment mcPay = IMCPaymentService.findByAuditID(mcAudit.ID);
                        MCReceipt mcRec = IMCReceiptService.findByAuditID(mcAudit.ID);
                        if ((mcPay != null && mcPay.Recorded) || (mcRec != null && mcRec.Recorded))
                        {
                            // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                            MSG.Warning("Bảng kiểm kê quỹ đã có phát sinh chứng từ xử lý phiếu thu hoặc phiếu chi. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                            services.RolbackTran();
                            IMCReceiptService.RolbackTran();
                            IMCPaymentService.RolbackTran();
                            return false;
                        }
                        else
                        {
                            if (mcPay != null)
                                IMCPaymentService.Delete(mcPay);
                            if (mcRec != null)
                                IMCReceiptService.Delete(mcRec);
                        }

                    }
                    #endregion
                    #region Nếu là bảng lương, check chứng từ nghiệp vụ khác
                    if (typeof(T) == typeof(PSSalarySheet))
                    {
                        PSSalarySheet PSSalarySheet = (PSSalarySheet)(object)_select;
                        if (PSSalarySheet.GOtherVoucherID != null)
                        {
                            GOtherVoucher gOther = IGOtherVoucherService.Getbykey(PSSalarySheet.GOtherVoucherID ?? Guid.Empty);
                            if (gOther != null)
                            {
                                MSG.Warning("Không thể xóa bảng lương, đã thanh toán cho bảng lương này");
                                services.RolbackTran();
                                return false;
                            }
                        }
                    }
                    #endregion
                    #region Xóa chứng từ móc
                    if (!SaveSelectJoin(true))
                    {
                        services.RolbackTran();
                        return false;
                    }
                    #endregion
                    #region Chứng từ tham chiếu nào đã bị xóa thì xóa luôn trong db lưu tham chiếu
                    Guid iD = _select.GetProperty<T, Guid>("ID");

                    List<RefVoucherRSInwardOutward> lstRefVoucherRS = Utils.IRefVoucherRSInwardOutwardService.GetByRefID2(iD).ToList();
                    if (lstRefVoucherRS.Count != 0)
                    {
                        Utils.IRefVoucherRSInwardOutwardService.BeginTran();
                        foreach (RefVoucherRSInwardOutward item in lstRefVoucherRS)
                        {
                            Utils.IRefVoucherRSInwardOutwardService.Delete(item);
                        }
                        Utils.IRefVoucherRSInwardOutwardService.CommitTran();
                    }


                    //Guid iD = temp.GetProperty<T, Guid>("ID");
                    List<RefVoucher> lstRefVoucher = Utils.IRefVoucherService.GetByRefID2(iD).ToList();
                    if (lstRefVoucher.Count != 0)
                    {
                        Utils.IRefVoucherService.BeginTran();
                        foreach (RefVoucher item in lstRefVoucher)
                        {
                            Utils.IRefVoucherService.Delete(item);
                        }
                        Utils.IRefVoucherService.CommitTran();
                    }
                    #endregion

                    if (!GenVoucherOther(_select, _backSelect, _statusForm, remove: true))
                    {
                        services.RolbackTran();
                        return false;
                    }
                    #region Xóa nghiệm thu
                    if (TypeID == 600 && _select.GetProperty<T, Guid?>("CPPeriodID") != null && _select.GetProperty<T, Guid?>("CPAcceptanceID") != null)
                    {
                        var md = Utils.ListCPPeriod.FirstOrDefault(x => x.ID == _select.GetProperty<T, Guid>("CPPeriodID"));
                        var act = md.CPAcceptances.FirstOrDefault(c => c.ID == _select.GetProperty<T, Guid>("CPAcceptanceID"));
                        md.CPAcceptances.Remove(act);
                        ICPAcceptanceService.Delete(act);
                        XoaNT = true;
                    }
                    #endregion
                    services.CommitTran();
                    Logs.SaveBusiness(_select, TypeID, Logs.Action.Delete);
                    if (XoaNT) Utils.ClearCacheByType<GOtherVoucher>();
                }
                catch (Exception ex)
                {//lỗi khi thực thi services
                    services.RolbackTran();
                    return false;
                }
            }
            catch (Exception)
            {
                //lỗi khi tạo services chung
                status = false;
            }
            return status;
        }
        protected virtual bool Add()
        {
            try
            {
                BaseService<T, Guid> services = this.GetIService(_select);
                if (_select.HasProperty("RefDateTime"))
                {
                    DateTime? dt = _select.GetProperty<T, DateTime?>("RefDateTime");
                    if (dt == null)
                        _select.SetProperty("RefDateTime", DateTime.Now);
                }

                SetOrGetGuiObject(_select, true);

                #region Check Mã Nhân Viên không được để trống
                List<int> allowList;
                var key = "";

                //add by cuongpv sua bug MaterialGoods no session khi dung chuc nang them moi tu chung tu hien thoi
                allowList = new List<int> { 300, 310, 320, 321, 322, 323, 324, 325, 330, 340 };
                if (allowList.Contains(TypeID))
                {
                    if (_select.HasProperty("SAInvoiceDetails"))
                    {
                        key = "MaterialGoods";
                        foreach (SAInvoiceDetail item in _select.GetProperty<T, IList>("SAInvoiceDetails"))
                        {
                            if (item.HasProperty(key))
                            {
                                item.SetProperty("MaterialGoods", null);
                            }
                        }
                    }
                }
                //end add by cuongpv

                #region Bảng tổng hợp chấm công
                allowList = new List<int> { 810, 811 };
                if (allowList.Contains(TypeID))
                {
                    key = "EmployeeID";
                    foreach (PSTimeSheetSummaryDetail item in _select.GetProperty<T, IList>("PSTimeSheetSummaryDetails"))
                    {
                        if (item.HasProperty(key))
                        {
                            Guid? guid = item.EmployeeID;
                            if (guid == null || guid == Guid.Empty)
                            {
                                MSG.Warning("Mã Nhân Viên không được để trống");
                                checkErrorEmployeeID = true;
                                return false;
                            }
                        }
                    }
                }
                #endregion
                #region Bảng chấm công
                allowList = new List<int> { 820, 821 };
                if (allowList.Contains(TypeID))
                {
                    key = "EmployeeID";
                    foreach (PSTimeSheetDetail item in _select.GetProperty<T, IList>("PSTimeSheetDetails"))
                    {
                        if (item.HasProperty(key))
                        {
                            Guid? guid = item.EmployeeID;
                            if (guid == null || guid == Guid.Empty)
                            {
                                MSG.Warning("Mã Nhân Viên không được để trống");
                                checkErrorEmployeeID = true;
                                return false;
                            }
                        }
                    }
                }
                #endregion
                #region Bảng lương
                allowList = new List<int> { 830, 831, 832, 833, 834 };
                if (allowList.Contains(TypeID))
                {
                    key = "EmployeeID";
                    foreach (PSSalarySheetDetail item in _select.GetProperty<T, IList>("PSSalarySheetDetails"))
                    {
                        if (item.HasProperty(key))
                        {
                            Guid? guid = item.EmployeeID;
                            if (guid == null || guid == Guid.Empty)
                            {
                                MSG.Warning("Mã Nhân Viên không được để trống");
                                checkErrorEmployeeID = true;
                                return false;
                            }
                        }
                    }
                }
                #endregion

                #endregion
                allowList = new List<int> { 431, 520 };
                if (allowList.Contains(TypeID))
                {
                    if (TypeID == 431)
                    {
                        key = "ToolsID";
                        foreach (TIDecrementDetail item in _select.GetProperty<T, IList>("TIDecrementDetails"))
                        {
                            if (item.HasProperty(key))
                            {
                                List<Guid> fixedAssetIdsInFaIncrement = null;
                                Guid? guid = item.ToolsID;
                                fixedAssetIdsInFaIncrement = ITIInitService.FindByType(0, null, DateTime.Now);
                                if (fixedAssetIdsInFaIncrement.Contains((Guid)guid))
                                {

                                }
                                else
                                {
                                    MSG.Error("CCDC này chưa được ghi tăng vui lòng kiểm tra lại");
                                    return false;
                                }
                            }
                        }
                    }
                    if (TypeID == 520)
                    {
                        key = "FixedAssetID";
                        foreach (FADecrementDetail item in _select.GetProperty<T, IList>("FADecrementDetails"))
                        {
                            if (item.HasProperty(key))
                            {
                                List<Guid> fixedAssetIdsInFaIncrement = null;
                                Guid? guid = item.FixedAssetID;
                                fixedAssetIdsInFaIncrement = IFixedAssetService.GetFixedAssetWithoutDecrement(null, DateTime.Now);
                                if (fixedAssetIdsInFaIncrement.Contains((Guid)guid))
                                {

                                }
                                else
                                {
                                    MSG.Error("TSCĐ này chưa được ghi tăng vui lòng kiểm tra lại");
                                    return false;
                                }
                            }
                        }
                    }
                }
                UltraGrid grid = new UltraGrid();
                if (TypeID == 821)
                {
                    grid = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
                }
                else
                {
                    //Hautv sửa lưu bảng tổng hợp chấm công
                    UltraTabControl _tabControl = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
                    if (_tabControl != null)
                    {
                        grid = (UltraGrid)_tabControl.Tabs[0].TabPage.Controls.Find("uGrid0", true).FirstOrDefault();
                        //if (grid == null)
                        //{
                        //    if (TypeID == 530) // diẻu chỉnh tài sản cố định
                        //    grid = (UltraGrid)_tabControl.Tabs[1].TabPage.Controls.Find("uGrid1", true).FirstOrDefault();//trungnq them
                        //    //grid = (UltraGrid)_tabControl.Tabs[1].TabPage.Controls.Find("uGrid0", true).FirstOrDefault();
                        //}
                        if (grid == null)
                            grid = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
                    }
                    else
                        grid = (UltraGrid)this.Controls.Find("uGrid0", true).FirstOrDefault();
                }
                var lstError = new List<string>();
                foreach (UltraGridRow row in grid.Rows)
                {
                    if (typeof(T) == typeof(PPInvoice) || typeof(T) == typeof(PPService) || typeof(T) == typeof(PPOrder) || typeof(T) == typeof(SAInvoice) || typeof(T) == typeof(SAReturn) || typeof(T) == typeof(SAOrder))
                    {
                        if (!grid.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].Hidden && row.Cells["MaterialGoodsID"].Text != null && row.Cells["MaterialGoodsID"].Text != "")
                        {
                            if ((Guid?)row.Cells["ContractID"].Value != null)
                            {
                                var lst = Utils.ListEMContractDetailMG.Where(x => x.ContractID == (Guid?)row.Cells["ContractID"].Value).ToList();
                                if (lst.Any(x => x.MaterialGoodsID == ((Guid?)row.Cells["MaterialGoodsID"].Value)))
                                {
                                    //isContractErr = 0;
                                    //RemoveNotificationCell(grid, cell.Row.Cells["ContractID"]);
                                }
                                else
                                {
                                    lstError.Add(row.Cells["MaterialGoodsID"].Text);
                                    //if (!(DialogResult.Yes == MessageBox.Show(row.Cells["MaterialGoodsID"].Text + " không tồn tại trên hợp đồng " + row.Cells["ContractID"].Text + "\nBạn có muốn lưu không", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                                    //{
                                    //    return false;
                                    //}
                                }
                            }
                        }
                    }
                }
                if (lstError.Count > 0)
                {
                    string er = "";
                    foreach (var itemE in lstError)
                    {
                        er += itemE + ", ";
                    }
                    if (!(DialogResult.Yes == MessageBox.Show(er + "không tồn tại trên hợp đồng tương ứng. Bạn có muốn lưu không", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question)))
                    {
                        check = 1;
                        return false;
                    }
                }
                try
                {
                    services.BeginTran();
                    if (typeof(T) == typeof(GOtherVoucher))
                    {
                        GOtherVoucher data = _select as GOtherVoucher;
                        var details = data.GOtherVoucherDetails;
                        decimal totalAmount = details.Sum(o => o.Amount);
                        _select.SetProperty("TotalAmount", totalAmount);
                        _select.SetProperty("TotalAmountOriginal", (totalAmount / (data.ExchangeRate ?? 1)));
                    }
                    #region Nếu là kiểm kê TSCĐ, recommendation == ghi giảm => tạo chứng từ ghi giảm
                    if (TypeID == 560)
                    {
                        FADecrement decrement = GetDecrementFromAudit((FAAudit)(object)_select);
                        if (decrement != null)
                        {
                            foreach (var FADecrementDetail in decrement.FADecrementDetails)
                            {
                                int result = IFixedAssetLedgerService.CheckLedgerForDecrement(FADecrementDetail.FixedAssetID ?? Guid.Empty, decrement.PostedDate, false);
                                if (result == 2)
                                {
                                    MSG.Warning("Ghi sổ thất bại! Tài sản này đã có phát sinh điều chỉnh, điều chuyển, tính khấu hao hoặc ghi giảm sau ngày hạch toán của chứng từ tính khấu hao này");
                                    services.RolbackTran();
                                    IFADecrementService.RolbackTran();
                                    isFirst = false;
                                    return false;
                                }
                                else if (result == 1)
                                {
                                    MSG.Warning("Ghi sổ thất bại! Tài sản này đã được ghi giảm, vui lòng kiểm tra lại");
                                    services.RolbackTran();
                                    IFADecrementService.RolbackTran();
                                    isFirst = false;
                                    return false;
                                }

                            }
                            IFADecrementService.CreateNew(decrement);
                            bool status = IGenCodeService.UpdateGenCodeForm(52, decrement.No, decrement.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                return false;
                            }
                            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
                            List<FixedAssetLedger> listFixedAssetTemp = new List<FixedAssetLedger>();
                            listGenTemp = this.GenGeneralLedgers(decrement);
                            listFixedAssetTemp = this.GenFixedAssetLedgers(decrement);
                            IGeneralLedgerService.BeginTran();
                            if ((listGenTemp != null) && (listGenTemp.Count > 0))
                            {
                                foreach (var generalLedger in listGenTemp)
                                {
                                    IGeneralLedgerService.CreateNew(generalLedger);
                                }
                            }
                            IFixedAssetLedgerService.BeginTran();
                            if ((listFixedAssetTemp != null) && (listFixedAssetTemp.Count > 0))
                            {
                                foreach (var fixedAssetLedger in listFixedAssetTemp)
                                {
                                    IFixedAssetLedgerService.CreateNew(fixedAssetLedger);
                                }
                            }
                            IFixedAssetLedgerService.CommitTran();
                            IGeneralLedgerService.CommitTran();
                            FADecrement = decrement;
                        }

                    }
                    if (TypeID == 435)
                    {
                        TIAudit tIAudit = (TIAudit)(object)_select;
                        TIDecrement tIDecrement = GetDecrementFromAudit(tIAudit);
                        TIIncrement tIIncrement = GetIncrementFromAudit(tIAudit);
                        List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
                        List<ToolLedger> listToolTemp = new List<ToolLedger>();
                        if (tIDecrement != null)
                        {
                            ITIDecrementService.CreateNew(tIDecrement);
                            bool status = IGenCodeService.UpdateGenCodeForm(431, tIDecrement.No, tIDecrement.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                isFirst = false;
                                return false;
                            }
                            ITIDecrementService.CommitTran();
                            listGenTemp.AddRange(this.GenGeneralLedgers(tIDecrement));
                            listToolTemp.AddRange(this.GenToolLedgers(tIDecrement, tIIncrement));

                        }
                        if (tIIncrement != null)
                        {
                            ITIIncrementService.CreateNew(tIIncrement);
                            bool status = IGenCodeService.UpdateGenCodeForm(907, tIIncrement.No, tIDecrement.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                isFirst = false;
                                return false;
                            }
                            ITIIncrementService.CommitTran();
                            listGenTemp.AddRange(this.GenGeneralLedgers(tIIncrement));
                            listToolTemp.AddRange(this.GenToolLedgers(tIIncrement, TIDecrement));

                        }
                        IGeneralLedgerService.BeginTran();
                        if ((listGenTemp != null) && (listGenTemp.Count > 0))
                        {
                            foreach (var generalLedger in listGenTemp)
                            {
                                IGeneralLedgerService.CreateNew(generalLedger);
                            }
                        }
                        if ((listToolTemp != null) && (listToolTemp.Count > 0))
                        {
                            foreach (var fixedAssetLedger in listToolTemp)
                            {
                                IToolLedgerService.CreateNew(fixedAssetLedger);
                            }
                        }
                        IGeneralLedgerService.CommitTran();
                        TIDecrement = tIDecrement;
                        TIIncrement = tIIncrement;
                    }
                    // CCDC
                    if (TypeID == 900 || TypeID == 901)
                    {
                        if (_select.HasProperty("Recorded"))
                        {
                            _select.SetProperty("Recorded", true);
                        }
                    }
                    #endregion
                    services.CreateNew(_select);
                    #region Nếu là kiểm kê quỹ                    
                    if (TypeID == 180)
                    {
                        MCAudit mcAudit = (MCAudit)(object)_select;
                        MCReceipt mcReceipt = null;
                        MCPayment mcPayment = null;
                        if (mcAudit.TotalAuditAmount > mcAudit.TotalBalanceAmount)
                            mcReceipt = GetReceiptFromAudit(mcAudit);
                        else if (mcAudit.TotalAuditAmount < mcAudit.TotalBalanceAmount)
                            mcPayment = GetPaymentFromAudit(mcAudit);
                        List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
                        if (mcReceipt != null)
                        {
                            IMCReceiptService.CreateNew(mcReceipt);
                            bool status = IGenCodeService.UpdateGenCodeForm(10, mcReceipt.No, mcReceipt.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IMCReceiptService.RolbackTran();
                                isFirst = false;
                                return false;
                            }
                            IMCReceiptService.CommitTran();
                            listGenTemp.AddRange(this.GenGeneralLedgers(mcReceipt));
                            MCReceipt = mcReceipt;
                        }
                        if (mcPayment != null)
                        {
                            IMCPaymentService.CreateNew(mcPayment);
                            bool status = IGenCodeService.UpdateGenCodeForm(11, mcPayment.No, mcPayment.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IMCPaymentService.RolbackTran();
                                isFirst = false;
                                return false;
                            }
                            IMCPaymentService.CommitTran();
                            listGenTemp.AddRange(this.GenGeneralLedgers(mcPayment));
                            MCPayment = mcPayment;
                        }
                        IGeneralLedgerService.BeginTran();
                        if ((listGenTemp != null) && (listGenTemp.Count > 0))
                        {
                            foreach (var generalLedger in listGenTemp)
                            {
                                IGeneralLedgerService.CreateNew(generalLedger);
                            }
                        }
                        IGeneralLedgerService.CommitTran();
                    }
                    #endregion
                    #region Up bảng gencode sinh chứng từ tiếp theo
                    //if (!new int[] { 900, 901, 821, 820, 810, 811, 830, 831, 832, 834, 326, 126, 116, 133, 173, 143 }.Any(x => x == TypeID))
                    if (!new int[] { 821, 820, 810, 811, 830, 831, 832, 834, 326 }.Any(x => x == TypeID))
                    {
                        if (TypeID == 210 && _select.GetProperty<T, bool>("StoredInRepository")) { }
                        else
                        {
                            bool status = TypeGroup != null && IGenCodeService.UpdateGenCodeForm((int)TypeGroup == 84 ? 60 : (int)TypeGroup, _select.GetProperty<T, string>("No"), _select.GetProperty<T, Guid>("ID"));
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                return false;
                            }
                        }
                    }

                    #endregion

                    if (!GenVoucherOther(_select, _backSelect, _statusForm))
                    {
                        services.RolbackTran();
                        return false;
                    }

                    services.CommitTran();
                    Logs.SaveBusiness(_select, TypeID, Logs.Action.Add);
                    return true;
                }
                catch (Exception e)
                {//lỗi khi thực thi services
                    MessageBox.Show(e.ToString());
                    ViewException(e);
                    services.RolbackTran();
                    services.BindSession(_listSelects);
                    return false;
                }
            }
            catch (Exception e)
            {
                var mes = e.Message;
                var backtrack = e.StackTrace;
                //lỗi khi tạo services chung
                return false;
            }
        }

        #region tao chung tu ghi so
        protected List<FixedAssetLedger> GenFixedAssetLedgers(FADecrement faDecrement)
        {
            List<FixedAssetLedger> listFixAssetLedger = new List<FixedAssetLedger>();
            for (int i = 0; i < faDecrement.FADecrementDetails.Count; i++)
            {
                var fixedAssetId = faDecrement.FADecrementDetails[i].FixedAssetID;
                var fixedAsset = new FixedAsset();
                if (fixedAssetId != null)
                {
                    fixedAsset = IFixedAssetService.Getbykey((Guid)fixedAssetId);
                }
                FixedAssetLedger FixedAssetLedger = IGeneralLedgerService.GetLastFixedAssetLedger(fixedAssetId, faDecrement.PostedDate);
                FixedAssetLedger.ID = Guid.NewGuid();
                FixedAssetLedger.ReferenceID = faDecrement.ID;
                FixedAssetLedger.No = faDecrement.No;
                FixedAssetLedger.TypeID = faDecrement.TypeID;
                FixedAssetLedger.Date = faDecrement.Date;
                FixedAssetLedger.PostedDate = faDecrement.PostedDate;
                FixedAssetLedger.FixedAssetID = faDecrement.FADecrementDetails[i].FixedAssetID ?? Guid.Empty;
                FixedAssetLedger.DepartmentID = faDecrement.FADecrementDetails[i].DepartmentID ?? Guid.Empty;
                //OriginalPriceAccount = faDecrement.FADecrementDetails[i].CreditAccount,
                FixedAssetLedger.OriginalPriceDebitAmount = 0;
                //OriginalPriceCreditAmount = faDecrement.FADecrementDetails[i].AmountOriginal,
                //DepreciationAccount = faDecrement.FADecrementDetails[i].DebitAccount,
                FixedAssetLedger.DepreciationDebitAmount = 0;
                //DepreciationCreditAmount = faDecrement.FADecrementDetails[i].AmountOriginal,
                FixedAssetLedger.Reason = faDecrement.Reason;
                //Description = faDecrement.FADecrementDetails[i].Description,
                //IsIrrationalCost = faDecrement.FADecrementDetails[i].IsIrrationalCost,
                listFixAssetLedger.Add(FixedAssetLedger);
            }

            return listFixAssetLedger;
        }
        protected List<GeneralLedger> GenGeneralLedgers(MCReceipt mcReceipt)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mcReceipt.MCReceiptDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcReceipt.ID,
                    TypeID = mcReceipt.TypeID,
                    Date = mcReceipt.Date,
                    PostedDate = mcReceipt.PostedDate,
                    No = mcReceipt.No,
                    InvoiceDate = mcReceipt.InvoiceDate,
                    InvoiceNo = mcReceipt.InvoiceNo,
                    Account = mcReceipt.MCReceiptDetails[i].DebitAccount,
                    AccountCorresponding = mcReceipt.MCReceiptDetails[i].CreditAccount,
                    BankAccountDetailID = mcReceipt.MCReceiptDetails[i].BankAccountDetailID,
                    CurrencyID = mcReceipt.CurrencyID,
                    ExchangeRate = mcReceipt.ExchangeRate,
                    DebitAmount = Utils.calculateAmountFromOriginal(mcReceipt.MCReceiptDetails[i].AmountOriginal, mcReceipt.ExchangeRate),
                    DebitAmountOriginal = mcReceipt.MCReceiptDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcReceipt.Reason,
                    Description = mcReceipt.MCReceiptDetails[i].Description,
                    AccountingObjectID = mcReceipt.MCReceiptDetails[i].AccountingObjectID ?? mcReceipt.AccountingObjectID,
                    EmployeeID = mcReceipt.EmployeeID,
                    BudgetItemID = mcReceipt.MCReceiptDetails[i].BudgetItemID,
                    CostSetID = mcReceipt.MCReceiptDetails[i].CostSetID,
                    ContractID = mcReceipt.MCReceiptDetails[i].ContractID,
                    StatisticsCodeID = mcReceipt.MCReceiptDetails[i].StatisticsCodeID,
                    InvoiceSeries = mcReceipt.InvoiceSeries,
                    ContactName = mcReceipt.Payers,
                    DetailID = mcReceipt.MCReceiptDetails[i].ID,
                    RefNo = mcReceipt.No,
                    RefDate = mcReceipt.Date,
                    DepartmentID = mcReceipt.MCReceiptDetails[i].DepartmentID,
                    ExpenseItemID = mcReceipt.MCReceiptDetails[i].ExpenseItemID,
                    IsIrrationalCost = mcReceipt.MCReceiptDetails[i].IsIrrationalCost
                };
                if (!(string.IsNullOrEmpty(mcReceipt.MCReceiptDetails[i].CreditAccount)) &&
                    (((mcReceipt.MCReceiptDetails[i].CreditAccount.StartsWith("133"))
                      || mcReceipt.MCReceiptDetails[i].CreditAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mcReceipt.MCReceiptDetailTaxs.Count > i ? mcReceipt.MCReceiptDetailTaxs[i].Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcReceipt.ID,
                    TypeID = mcReceipt.TypeID,
                    Date = mcReceipt.Date,
                    PostedDate = mcReceipt.PostedDate,
                    No = mcReceipt.No,
                    InvoiceDate = mcReceipt.InvoiceDate,
                    InvoiceNo = mcReceipt.InvoiceNo,
                    Account = mcReceipt.MCReceiptDetails[i].CreditAccount,
                    AccountCorresponding = mcReceipt.MCReceiptDetails[i].DebitAccount,
                    BankAccountDetailID = mcReceipt.MCReceiptDetails[i].BankAccountDetailID,
                    CurrencyID = mcReceipt.CurrencyID,
                    ExchangeRate = mcReceipt.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = Utils.calculateAmountFromOriginal(mcReceipt.MCReceiptDetails[i].AmountOriginal, mcReceipt.ExchangeRate),
                    CreditAmountOriginal = mcReceipt.MCReceiptDetails[i].AmountOriginal,
                    Reason = mcReceipt.Reason,
                    Description = mcReceipt.MCReceiptDetails[i].Description,
                    AccountingObjectID = mcReceipt.MCReceiptDetails[i].AccountingObjectID ?? mcReceipt.AccountingObjectID,
                    EmployeeID = mcReceipt.EmployeeID,
                    BudgetItemID = mcReceipt.MCReceiptDetails[i].BudgetItemID,
                    CostSetID = mcReceipt.MCReceiptDetails[i].CostSetID,
                    ContractID = mcReceipt.MCReceiptDetails[i].ContractID,
                    StatisticsCodeID = mcReceipt.MCReceiptDetails[i].StatisticsCodeID,
                    InvoiceSeries = mcReceipt.InvoiceSeries,
                    ContactName = mcReceipt.Payers,
                    DetailID = mcReceipt.MCReceiptDetails[i].ID,
                    RefNo = mcReceipt.No,
                    RefDate = mcReceipt.Date,
                    DepartmentID = mcReceipt.MCReceiptDetails[i].DepartmentID,
                    ExpenseItemID = mcReceipt.MCReceiptDetails[i].ExpenseItemID,
                    IsIrrationalCost = mcReceipt.MCReceiptDetails[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(mcReceipt.MCReceiptDetails[i].CreditAccount)) &&
                    (((mcReceipt.MCReceiptDetails[i].CreditAccount.StartsWith("133"))
                      || mcReceipt.MCReceiptDetails[i].CreditAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mcReceipt.MCReceiptDetailTaxs.Count > i ? mcReceipt.MCReceiptDetailTaxs[i].Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(MCPayment mcPayment)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có

            for (int i = 0; i < mcPayment.MCPaymentDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mcPayment.MCPaymentDetails[i].DebitAccount,
                    AccountCorresponding = mcPayment.MCPaymentDetails[i].CreditAccount,
                    BankAccountDetailID = mcPayment.MCPaymentDetails[i].BankAccountDetailID,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = mcPayment.MCPaymentDetails[i].Amount != 0 ? mcPayment.MCPaymentDetails[i].Amount : Utils.calculateAmountFromOriginal(mcPayment.MCPaymentDetails[i].AmountOriginal, mcPayment.ExchangeRate),
                    DebitAmountOriginal = mcPayment.MCPaymentDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetails[i].Description,
                    AccountingObjectID = mcPayment.MCPaymentDetails[i].AccountingObjectID ?? mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetails[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetails[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetails[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetails[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetails[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetails[i].DepartmentID,
                    ExpenseItemID = mcPayment.MCPaymentDetails[i].ExpenseItemID,
                    IsIrrationalCost = mcPayment.MCPaymentDetails[i].IsIrrationalCost
                };
                if (!(string.IsNullOrEmpty(mcPayment.MCPaymentDetails[i].DebitAccount)) &&
                    (((mcPayment.MCPaymentDetails[i].DebitAccount.StartsWith("133"))
                      || mcPayment.MCPaymentDetails[i].DebitAccount.StartsWith("33311"))))
                {
                    genTemp.VATDescription = mcPayment.MCPaymentDetailTaxs.Count > i ? mcPayment.MCPaymentDetailTaxs[i].Description : null;
                }
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = mcPayment.ID,
                    TypeID = mcPayment.TypeID,
                    Date = mcPayment.Date,
                    PostedDate = mcPayment.PostedDate,
                    No = mcPayment.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = mcPayment.MCPaymentDetails[i].CreditAccount,
                    AccountCorresponding = mcPayment.MCPaymentDetails[i].DebitAccount,
                    BankAccountDetailID = mcPayment.MCPaymentDetails[i].BankAccountDetailID,
                    CurrencyID = mcPayment.CurrencyID,
                    ExchangeRate = mcPayment.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = mcPayment.MCPaymentDetails[i].Amount != 0 ? mcPayment.MCPaymentDetails[i].Amount : Utils.calculateAmountFromOriginal(mcPayment.MCPaymentDetails[i].AmountOriginal, mcPayment.ExchangeRate),
                    CreditAmountOriginal = mcPayment.MCPaymentDetails[i].AmountOriginal,
                    Reason = mcPayment.Reason,
                    Description = mcPayment.MCPaymentDetails[i].Description,
                    AccountingObjectID = mcPayment.MCPaymentDetails[i].AccountingObjectID ?? mcPayment.AccountingObjectID,
                    EmployeeID = mcPayment.EmployeeID,
                    BudgetItemID = mcPayment.MCPaymentDetails[i].BudgetItemID,
                    CostSetID = mcPayment.MCPaymentDetails[i].CostSetID,
                    ContractID = mcPayment.MCPaymentDetails[i].ContractID,
                    StatisticsCodeID = mcPayment.MCPaymentDetails[i].StatisticsCodeID,
                    InvoiceSeries = "",
                    ContactName = mcPayment.Receiver,
                    DetailID = mcPayment.MCPaymentDetails[i].ID,
                    RefNo = mcPayment.No,
                    RefDate = mcPayment.Date,
                    DepartmentID = mcPayment.MCPaymentDetails[i].DepartmentID,
                    ExpenseItemID = mcPayment.MCPaymentDetails[i].ExpenseItemID,
                    IsIrrationalCost = mcPayment.MCPaymentDetails[i].IsIrrationalCost
                };
                if ((!string.IsNullOrEmpty(mcPayment.MCPaymentDetails[i].DebitAccount)) &&
                    (((mcPayment.MCPaymentDetails[i].DebitAccount.StartsWith("133"))
                      || mcPayment.MCPaymentDetails[i].DebitAccount.StartsWith("33311"))))
                {
                    genTempCorresponding.VATDescription = mcPayment.MCPaymentDetailTaxs.Count > i ? mcPayment.MCPaymentDetailTaxs[i].Description : null;
                }
                listGenTemp.Add(genTempCorresponding);
            }

            #endregion

            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(FADecrement faDecrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            for (int i = 0; i < faDecrement.FADecrementDetails.Count; i++)
            {
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faDecrement.ID,
                    TypeID = faDecrement.TypeID,
                    Date = faDecrement.Date,
                    PostedDate = faDecrement.PostedDate,
                    No = faDecrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].DebitAccount,
                    AccountCorresponding = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].CreditAccount,
                    CurrencyID = faDecrement.CurrencyID,
                    ExchangeRate = faDecrement.ExchangeRate,
                    DebitAmount = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].Amount,
                    DebitAmountOriginal = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = faDecrement.Reason,
                    Description = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].Description,
                    AccountingObjectID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].AccountingObjectID,
                    EmployeeID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].EmployeeID,
                    BudgetItemID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].BudgetItemID,
                    CostSetID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].CostSetID,
                    ContractID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].ContractID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = faDecrement.FADecrementDetails[i].ID,
                    RefNo = faDecrement.No,
                    RefDate = faDecrement.Date,
                    DepartmentID = faDecrement.FADecrementDetails[i].DepartmentID,
                    ExpenseItemID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[0].IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = faDecrement.ID,
                    TypeID = faDecrement.TypeID,
                    Date = faDecrement.Date,
                    PostedDate = faDecrement.PostedDate,
                    No = faDecrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].CreditAccount,
                    AccountCorresponding = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].DebitAccount,
                    //BankAccountDetailID = faDecrement.BankAccountDetailID,
                    CurrencyID = faDecrement.CurrencyID,
                    ExchangeRate = faDecrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].Amount,
                    CreditAmountOriginal = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].AmountOriginal,
                    Reason = faDecrement.Reason,
                    Description = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].Description,
                    AccountingObjectID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].AccountingObjectID,
                    EmployeeID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].EmployeeID,
                    BudgetItemID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].BudgetItemID,
                    CostSetID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].CostSetID,
                    ContractID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].ContractID,
                    //StatisticsCodeID = faDecrement.FaDecrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = faDecrement.FADecrementDetails[i].ID,
                    RefNo = faDecrement.No,
                    RefDate = faDecrement.Date,
                    DepartmentID = faDecrement.FADecrementDetails[i].DepartmentID,
                    ExpenseItemID = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = faDecrement.FADecrementDetails[i].FADecrementDetailPosts[1].IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);
            }
            #endregion

            return listGenTemp;
        }
        protected List<GeneralLedger> GenGeneralLedgers(TIDecrement tiDecrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();

            #region Cặp Nợ-Có
            for (int i = 0; i < tiDecrement.TIDecrementDetails.Count; i++)
            {
                TIDecrementDetail post = tiDecrement.TIDecrementDetails[i];
                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiDecrement.ID,
                    TypeID = tiDecrement.TypeID,
                    Date = tiDecrement.Date,
                    PostedDate = tiDecrement.PostedDate,
                    No = tiDecrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    //Account = post.DebitAccount,
                    //AccountCorresponding = post.CreditAccount,
                    //CurrencyID = tiDecrement.CurrencyID,
                    //ExchangeRate = tiDecrement.ExchangeRate,
                    DebitAmount = post.Amount,
                    //DebitAmountOriginal = post.AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = tiDecrement.Reason,
                    Description = post.Description,
                    //AccountingObjectID = post.AccountingObjectID,
                    //EmployeeID = post.EmployeeID,
                    //BudgetItemID = post.BudgetItemID,
                    //CostSetID = post.CostSet,
                    //ContractID = post.ContractID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = tiDecrement.TIDecrementDetails[i].ID,
                    RefNo = tiDecrement.No,
                    RefDate = tiDecrement.Date,
                    DepartmentID = tiDecrement.TIDecrementDetails[i].DepartmentID,
                    //ExpenseItemID = post.ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = post.IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiDecrement.ID,
                    TypeID = tiDecrement.TypeID,
                    Date = tiDecrement.Date,
                    PostedDate = tiDecrement.PostedDate,
                    No = tiDecrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    //Account = post.CreditAccount,
                    //AccountCorresponding = post.DebitAccount,
                    //BankAccountDetailID = faDecrement.BankAccountDetailID,
                    //CurrencyID = tiDecrement.CurrencyID,
                    //ExchangeRate = tiDecrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = post.Amount,
                    //CreditAmountOriginal = post.AmountOriginal,
                    Reason = tiDecrement.Reason,
                    Description = post.Description,
                    //AccountingObjectID = post.AccountingObjectID,
                    //EmployeeID = post.EmployeeID,
                    //BudgetItemID = post.BudgetItemID,
                    //CostSetID = post.CostSetID,
                    //ContractID = post.ContractID,
                    //StatisticsCodeID = faDecrement.TIIncrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = null,
                    DetailID = tiDecrement.TIDecrementDetails[i].ID,
                    RefNo = tiDecrement.No,
                    RefDate = tiDecrement.Date,
                    DepartmentID = tiDecrement.TIDecrementDetails[i].DepartmentID,
                    //ExpenseItemID = post.ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = post.IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);

            }
            #endregion

            return listGenTemp;
        }
        protected List<ToolLedger> GenToolLedgers(TIDecrement TIDecrement, TIIncrement tIIncrement)
        {
            List<ToolLedger> listFixAssetLedger = new List<ToolLedger>();
            for (int i = 0; i < TIDecrement.TIDecrementDetails.Count; i++)
            {
                var toolsID = TIDecrement.TIDecrementDetails[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (toolsID != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)toolsID);
                //}
                ToolLedger ToolLedger = IGeneralLedgerService.GetLastestToolLedger(toolsID, TIDecrement.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = TIDecrement.ID;
                ToolLedger.No = TIDecrement.No;
                ToolLedger.TypeID = TIDecrement.TypeID;
                ToolLedger.Date = TIDecrement.Date;
                ToolLedger.PostedDate = TIDecrement.PostedDate;
                ToolLedger.ToolsID = TIDecrement.TIDecrementDetails[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = TIDecrement.Reason;
                ToolLedger.Description = TIDecrement.TIDecrementDetails[i].Description;
                ToolLedger.DecrementQuantity = TIDecrement.TIDecrementDetails[i].DecrementQuantity;
                ToolLedger.DepartmentID = TIDecrement.TIDecrementDetails[i].DepartmentID;
                ToolLedger.UnitPrice = TIDecrement.TIDecrementDetails[i].UnitPrice;
                ToolLedger.IncrementQuantity = TIDecrement.TIDecrementDetails[i].Quantity;
                ToolLedger.IncrementAmount = TIDecrement.TIDecrementDetails[i].Quantity * TIDecrement.TIDecrementDetails[i].UnitPrice;
                ToolLedger.RemainingQuantity = ToolLedger.RemainingQuantity + (tIIncrement != null ? tIIncrement.TIIncrementDetails.Where(x => x.DepartmentID == ToolLedger.DepartmentID).Sum(x => x.Quantity) : 0) - (TIDecrement.TIDecrementDetails.Where(x => x.DepartmentID == ToolLedger.DepartmentID).Sum(x => x.Quantity) ?? 0);
                listFixAssetLedger.Add(ToolLedger);
            }

            return listFixAssetLedger;
        }
        protected List<ToolLedger> GenToolLedgers(TIIncrement tiIncrement, TIDecrement tiDecrement)
        {
            List<ToolLedger> listToolLedger = new List<ToolLedger>();
            for (int i = 0; i < tiIncrement.TIIncrementDetails.Count; i++)
            {
                var fixedAssetId = tiIncrement.TIIncrementDetails[i].ToolsID;
                //var fixedAsset = new FixedAsset();
                //if (fixedAssetId != null)
                //{
                //    fixedAsset = _IFixedAssetService.Getbykey((Guid)fixedAssetId);
                //}
                ToolLedger ToolLedger = IGeneralLedgerService.GetLastestToolLedger(fixedAssetId, tiIncrement.PostedDate);
                ToolLedger.ID = Guid.NewGuid();
                ToolLedger.ReferenceID = tiIncrement.ID;
                ToolLedger.No = tiIncrement.No;
                ToolLedger.TypeID = tiIncrement.TypeID;
                ToolLedger.Date = tiIncrement.Date;
                ToolLedger.PostedDate = tiIncrement.PostedDate;
                ToolLedger.ToolsID = tiIncrement.TIIncrementDetails[i].ToolsID ?? Guid.Empty;
                ToolLedger.Reason = tiIncrement.Reason;
                ToolLedger.Description = tiIncrement.TIIncrementDetails[i].Description;

                ToolLedger.IncrementAllocationTime = tiIncrement.TIIncrementDetails[i].AllocationTimes;
                ToolLedger.IncrementQuantity = tiIncrement.TIIncrementDetails[i].Quantity;
                ToolLedger.IncrementAmount = ToolLedger.IncrementQuantity * tiIncrement.TIIncrementDetails[i].UnitPrice;
                ToolLedger.AllocatedAmount = tiIncrement.TIIncrementDetails[i].AllocationTimes != 0 ? tiIncrement.TIIncrementDetails[i].AmountOriginal / tiIncrement.TIIncrementDetails[i].AllocationTimes : 0;
                ToolLedger.UnitPrice = tiIncrement.TIIncrementDetails[i].UnitPrice;
                ToolLedger.DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID;
                ToolLedger.IncrementQuantity = tiIncrement.TIIncrementDetails[i].Quantity;
                ToolLedger.RemainingQuantity = ToolLedger.RemainingQuantity + (tiIncrement.TIIncrementDetails.Where(x => x.DepartmentID == ToolLedger.DepartmentID).Sum(x => x.Quantity)) - (tiDecrement != null ? (tiDecrement.TIDecrementDetails.Where(x => x.DepartmentID == ToolLedger.DepartmentID).Sum(x => x.Quantity) ?? 0) : 0);

                ToolLedger.ReferenceID = tiIncrement.ID;


                listToolLedger.Add(ToolLedger);
            }

            return listToolLedger;
        }
        protected List<GeneralLedger> GenGeneralLedgers(TIIncrement tiIncrement)
        {
            List<GeneralLedger> listGenTemp = new List<GeneralLedger>();
            Currency iCurrency = ICurrencyService.Getbykey(tiIncrement.CurrencyID);
            string currencyAccount = (tiIncrement.TypeID == 430) ? iCurrency.CashAccount : iCurrency.BankAccount;

            for (int i = 0; i < tiIncrement.TIIncrementDetails.Count; i++)
            {
                #region Cặp Nợ-Có

                GeneralLedger genTemp = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiIncrement.ID,
                    TypeID = tiIncrement.TypeID,
                    Date = tiIncrement.Date,
                    PostedDate = tiIncrement.PostedDate,
                    No = tiIncrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = tiIncrement.TIIncrementDetails[i].DebitAccount,
                    AccountCorresponding = tiIncrement.TIIncrementDetails[i].CreditAccount,
                    BankAccountDetailID = tiIncrement.BankAccountDetailID,
                    CurrencyID = tiIncrement.CurrencyID,
                    ExchangeRate = tiIncrement.ExchangeRate,
                    DebitAmount = tiIncrement.TIIncrementDetails[i].Amount,
                    DebitAmountOriginal = tiIncrement.TIIncrementDetails[i].AmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = tiIncrement.Reason,
                    Description = tiIncrement.TIIncrementDetails[i].Description,
                    AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                    EmployeeID = tiIncrement.EmployeeID,
                    BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                    CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                    ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                    //StatisticsCodeID = faIncrement.FaIncrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = tiIncrement.MContactName,
                    DetailID = tiIncrement.TIIncrementDetails[i].ID,
                    RefNo = tiIncrement.No,
                    RefDate = tiIncrement.Date,
                    DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                    ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                };
                listGenTemp.Add(genTemp);
                GeneralLedger genTempCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiIncrement.ID,
                    TypeID = tiIncrement.TypeID,
                    Date = tiIncrement.Date,
                    PostedDate = tiIncrement.PostedDate,
                    No = tiIncrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = tiIncrement.TIIncrementDetails[i].CreditAccount,
                    AccountCorresponding = tiIncrement.TIIncrementDetails[i].DebitAccount,
                    BankAccountDetailID = tiIncrement.BankAccountDetailID,
                    CurrencyID = tiIncrement.CurrencyID,
                    ExchangeRate = tiIncrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = tiIncrement.TIIncrementDetails[i].Amount,
                    CreditAmountOriginal = tiIncrement.TIIncrementDetails[i].AmountOriginal,
                    Reason = tiIncrement.Reason,
                    Description = tiIncrement.TIIncrementDetails[i].Description,
                    AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                    EmployeeID = tiIncrement.EmployeeID,
                    BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                    CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                    ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                    //StatisticsCodeID = faIncrement.TIIncrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = tiIncrement.MContactName,
                    DetailID = tiIncrement.TIIncrementDetails[i].ID,
                    RefNo = tiIncrement.No,
                    RefDate = tiIncrement.Date,
                    DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                    ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                };
                listGenTemp.Add(genTempCorresponding);

                #endregion

                #region Cặp Chi phí mua - Có
                GeneralLedger genOut = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiIncrement.ID,
                    TypeID = tiIncrement.TypeID,
                    Date = tiIncrement.Date,
                    PostedDate = tiIncrement.PostedDate,
                    No = tiIncrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = tiIncrement.TIIncrementDetails[i].DebitAccount,
                    AccountCorresponding = tiIncrement.TIIncrementDetails[i].CreditAccount,
                    BankAccountDetailID = tiIncrement.BankAccountDetailID,
                    CurrencyID = tiIncrement.CurrencyID,
                    ExchangeRate = tiIncrement.ExchangeRate,
                    DebitAmount = tiIncrement.TIIncrementDetails[i].FreightAmount,
                    DebitAmountOriginal = tiIncrement.TIIncrementDetails[i].FreightAmountOriginal,
                    CreditAmount = 0,
                    CreditAmountOriginal = 0,
                    Reason = tiIncrement.Reason,
                    Description = "Chi phí mua",
                    AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                    EmployeeID = tiIncrement.EmployeeID,
                    BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                    CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                    ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                    //StatisticsCodeID = faIncrement.TIIncrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = tiIncrement.MContactName,
                    DetailID = tiIncrement.TIIncrementDetails[i].ID,
                    RefNo = tiIncrement.No,
                    RefDate = tiIncrement.Date,
                    DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                    ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                };
                listGenTemp.Add(genOut);
                GeneralLedger genOutCorresponding = new GeneralLedger
                {
                    ID = Guid.NewGuid(),
                    BranchID = null,
                    ReferenceID = tiIncrement.ID,
                    TypeID = tiIncrement.TypeID,
                    Date = tiIncrement.Date,
                    PostedDate = tiIncrement.PostedDate,
                    No = tiIncrement.No,
                    InvoiceDate = null,
                    InvoiceNo = null,
                    Account = tiIncrement.TIIncrementDetails[i].CreditAccount,
                    AccountCorresponding = tiIncrement.TIIncrementDetails[i].DebitAccount,
                    BankAccountDetailID = tiIncrement.BankAccountDetailID,
                    CurrencyID = tiIncrement.CurrencyID,
                    ExchangeRate = tiIncrement.ExchangeRate,
                    DebitAmount = 0,
                    DebitAmountOriginal = 0,
                    CreditAmount = tiIncrement.TIIncrementDetails[i].FreightAmount,
                    CreditAmountOriginal = tiIncrement.TIIncrementDetails[i].FreightAmountOriginal,
                    Reason = tiIncrement.Reason,
                    Description = "Chi phí mua",
                    AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                    EmployeeID = tiIncrement.EmployeeID,
                    BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                    CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                    ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                    //StatisticsCodeID = faIncrement.TIIncrementDetails[i].StatisticCodeID,
                    InvoiceSeries = null,
                    ContactName = tiIncrement.MContactName,
                    DetailID = tiIncrement.TIIncrementDetails[i].ID,
                    RefNo = tiIncrement.No,
                    RefDate = tiIncrement.Date,
                    DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                    ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                    VATDescription = null,
                    IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                };
                listGenTemp.Add(genOutCorresponding);
                #endregion

                if (!(string.IsNullOrEmpty(tiIncrement.TIIncrementDetails[i].DebitAccount)))
                {
                    #region Cặp Nợ - Thuế GTGT

                    GeneralLedger genTempVAT = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = tiIncrement.TIIncrementDetails[i].VATAccount,
                        //AccountCorresponding = faIncrement.TIIncrementDetails[i].CreditAccount,
                        AccountCorresponding = currencyAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = (decimal)tiIncrement.TIIncrementDetails[i].VATAmount,
                        DebitAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].VATAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = tiIncrement.Reason,
                        Description = tiIncrement.TIIncrementDetails[i].Description,
                        AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        VATDescription = null,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVAT);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        //Account = faIncrement.TIIncrementDetails[i].CreditAccount,
                        Account = currencyAccount,
                        AccountCorresponding = tiIncrement.TIIncrementDetails[i].VATAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)tiIncrement.TIIncrementDetails[i].VATAmount,
                        CreditAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].VATAmountOriginal,
                        Reason = tiIncrement.Reason,
                        Description = tiIncrement.TIIncrementDetails[i].Description,
                        AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost,
                        VATDescription = null
                    };

                    listGenTemp.Add(genTempVATCorresponding);

                    #endregion
                }
                else if (tiIncrement.IsSpecialConsumeTax == true)
                {
                    #region Cặp Nợ - Thuế tiêu thụ đặc biệt: Nếu hàng hóa có thuế TTĐB

                    GeneralLedger genTempVATTTDB = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = tiIncrement.TIIncrementDetails[i].SpecialConsumeTaxAccount,
                        AccountCorresponding = currencyAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = (decimal)tiIncrement.TIIncrementDetails[i].SpecialConsumeTaxAmount,
                        DebitAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].SpecialConsumeTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = tiIncrement.Reason,
                        Description = tiIncrement.TIIncrementDetails[i].Description,
                        AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        VATDescription = null,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATTTDB);
                    GeneralLedger genTempVATTTDBCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = currencyAccount,
                        AccountCorresponding = tiIncrement.TIIncrementDetails[i].SpecialConsumeTaxAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)tiIncrement.TIIncrementDetails[i].SpecialConsumeTaxAmount,
                        CreditAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].SpecialConsumeTaxAmountOriginal,
                        Reason = tiIncrement.Reason,
                        Description = tiIncrement.TIIncrementDetails[i].Description,
                        AccountingObjectID = tiIncrement.TIIncrementDetails[i].AccountingObjectID ?? tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost,
                        VATDescription = null,
                    };

                    listGenTemp.Add(genTempVATTTDBCorresponding);

                    #endregion
                }
                else if (tiIncrement.IsImportPurchase == true)
                {
                    #region Cặp Nợ - Thuế Nhập khẩu: Nếu chọn Mua hàng = Nhập khẩu

                    GeneralLedger genTempVATNK = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = tiIncrement.TIIncrementDetails[i].DebitAccount,
                        AccountCorresponding = tiIncrement.TIIncrementDetails[i].ImportTaxAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = (decimal)tiIncrement.TIIncrementDetails[i].ImportTaxAmount,
                        DebitAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].ImportTaxAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = tiIncrement.Reason,
                        Description = tiIncrement.TIIncrementDetails[i].Description,
                        AccountingObjectID = tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        VATDescription = null,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATNK);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = tiIncrement.TIIncrementDetails[i].ImportTaxAccount,
                        AccountCorresponding = tiIncrement.TIIncrementDetails[i].DebitAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)tiIncrement.TIIncrementDetails[i].ImportTaxAmount,
                        CreditAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].ImportTaxAmountOriginal,
                        Reason = tiIncrement.Reason,
                        //Description = tiIncrement.TIIncrementDetails[i].VATDescription,
                        AccountingObjectID = tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost,
                        VATDescription = null
                    };
                    listGenTemp.Add(genTempVATCorresponding);
                    #endregion
                }

                else if (tiIncrement.TIIncrementDetails[i].DebitAccount == "")
                {
                    #region Cặp Nợ TK Đối ứng thuế GTGT - Có TK thuế GTGT

                    GeneralLedger genTempVATGTGT = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = tiIncrement.TIIncrementDetails[i].DeductionDebitAccount,
                        AccountCorresponding = tiIncrement.TIIncrementDetails[i].VATAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = (decimal)tiIncrement.TIIncrementDetails[i].VATAmount,
                        DebitAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].VATAmountOriginal,
                        CreditAmount = 0,
                        CreditAmountOriginal = 0,
                        Reason = tiIncrement.Reason,
                        Description = tiIncrement.TIIncrementDetails[i].Description,
                        AccountingObjectID = tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        VATDescription = null,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost
                    };

                    listGenTemp.Add(genTempVATGTGT);
                    GeneralLedger genTempVATCorresponding = new GeneralLedger
                    {
                        ID = Guid.NewGuid(),
                        BranchID = null,
                        ReferenceID = tiIncrement.ID,
                        TypeID = tiIncrement.TypeID,
                        Date = tiIncrement.Date,
                        PostedDate = tiIncrement.PostedDate,
                        No = tiIncrement.No,
                        InvoiceDate = tiIncrement.TIIncrementDetails[i].InvoiceDate,
                        InvoiceNo = tiIncrement.TIIncrementDetails[i].InvoiceNo,
                        Account = tiIncrement.TIIncrementDetails[i].VATAccount,
                        AccountCorresponding = tiIncrement.TIIncrementDetails[i].DeductionDebitAccount,
                        BankAccountDetailID = tiIncrement.BankAccountDetailID,
                        CurrencyID = tiIncrement.CurrencyID,
                        ExchangeRate = tiIncrement.ExchangeRate,
                        DebitAmount = 0,
                        DebitAmountOriginal = 0,
                        CreditAmount = (decimal)tiIncrement.TIIncrementDetails[i].VATAmount,
                        CreditAmountOriginal = (decimal)tiIncrement.TIIncrementDetails[i].VATAmountOriginal,
                        Reason = tiIncrement.Reason,
                        AccountingObjectID = tiIncrement.AccountingObjectID,
                        EmployeeID = tiIncrement.EmployeeID,
                        BudgetItemID = tiIncrement.TIIncrementDetails[i].BudgetItemID,
                        CostSetID = tiIncrement.TIIncrementDetails[i].CostSetID,
                        ContractID = tiIncrement.TIIncrementDetails[i].ContractID,
                        StatisticsCodeID = tiIncrement.TIIncrementDetails[i].StatisticsCodeID,
                        InvoiceSeries = null,
                        ContactName = tiIncrement.MContactName,
                        DetailID = tiIncrement.TIIncrementDetails[i].ID,
                        RefNo = tiIncrement.No,
                        RefDate = tiIncrement.Date,
                        DepartmentID = tiIncrement.TIIncrementDetails[i].DepartmentID,
                        ExpenseItemID = tiIncrement.TIIncrementDetails[i].ExpenseItemID,
                        IsIrrationalCost = tiIncrement.TIIncrementDetails[i].IsIrrationalCost,
                        VATDescription = null
                    };

                    listGenTemp.Add(genTempVATCorresponding);

                    #endregion
                }
            }
            return listGenTemp;
        }
        #endregion
        private MCPayment GetPaymentFromAudit(MCAudit MCAudit)
        {
            MCPayment MCPayment = new MCPayment
            {
                ID = Guid.NewGuid(),
                TypeID = 110,
                Date = MCAudit.Date,
                PostedDate = MCAudit.AuditDate,
                No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(11)),
                Reason = "Xử lý chênh lệch do kiểm kê thiếu quỹ",
                TemplateID = new Guid("768F630D-FB71-403C-8BF4-93B42AF1ADBA"),
                CurrencyID = MCAudit.CurrencyID,
                AuditID = MCAudit.ID,
                TotalAmount = Math.Abs(MCAudit.DifferAmount),
                TotalAmountOriginal = Math.Abs(MCAudit.DifferAmount),
                TotalAll = Math.Abs(MCAudit.DifferAmount),
                TotalAllOriginal = Math.Abs(MCAudit.DifferAmount),
                Recorded = true,
            };
            var lst = new List<MCPaymentDetail>();
            MCPaymentDetail detail = new MCPaymentDetail
            {
                MCPaymentID = MCPayment.ID,
                Description = "Xử lý chênh lệch do kiểm kê thiếu quỹ",
                DebitAccount = "1381",
                CreditAccount = MCPayment.CurrencyID == "VND" ? "1111" : "1112",
                AmountOriginal = Math.Abs(MCAudit.DifferAmount),
                Amount = Math.Abs(MCAudit.DifferAmount)
            };
            lst.Add(detail);
            MCPayment.MCPaymentDetails = lst;
            return MCPayment;
        }
        private MCReceipt GetReceiptFromAudit(MCAudit MCAudit)
        {
            MCReceipt MCReceipt = new MCReceipt
            {
                ID = Guid.NewGuid(),
                TypeID = 100,
                Date = MCAudit.Date,
                PostedDate = MCAudit.AuditDate,
                No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(10)),
                Reason = "Xử lý chênh lệch do kiểm kê thừa quỹ",
                TemplateID = new Guid("2D5A2A11-06E7-440D-BA5B-B9B2A67D951C"),
                CurrencyID = MCAudit.CurrencyID,
                AuditID = MCAudit.ID,
                TotalAmount = MCAudit.DifferAmount,
                TotalAmountOriginal = MCAudit.DifferAmount,
                TotalAll = MCAudit.DifferAmount,
                TotalAllOriginal = MCAudit.DifferAmount,
                Recorded = true,
            };
            var lst = new List<MCReceiptDetail>();
            MCReceiptDetail detail = new MCReceiptDetail
            {
                MCReceiptID = MCReceipt.ID,
                Description = "Xử lý chênh lệch do kiểm kê thừa quỹ",
                DebitAccount = MCReceipt.CurrencyID == "VND" ? "1111" : "1112",
                CreditAccount = "3381",
                AmountOriginal = MCAudit.DifferAmount,
                Amount = MCAudit.DifferAmount

            };
            lst.Add(detail);
            MCReceipt.MCReceiptDetails = lst;
            return MCReceipt;
        }
        private FADecrement GetDecrementFromAudit(FAAudit FAAudit)
        {
            FADecrement FADecrement = new FADecrement
            {
                ID = Guid.NewGuid(),
                TypeID = 520,
                Date = FAAudit.Date,
                PostedDate = FAAudit.PostedDate,
                No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(52)),
                Reason = "Ghi giảm từ kiểm kê TSCĐ đến ngày " + FAAudit.PostedDate.ToString("dd/MM/yyyy"),
                TemplateID = new Guid("AA090801-E3A4-4AD2-8847-3064D45C5E29"),
                CurrencyID = "VND",
                RefID = FAAudit.ID,
                Recorded = true,


            };
            int numberOfLostAsset = 0;
            var lstDetail = new List<FADecrementDetail>();
            decimal totalAmount = 0;
            decimal acdeprecaition = 0;
            for (int i = 0; i < FAAudit.FAAuditDetails.Count; i++)
            {
                if (FAAudit.FAAuditDetails[i].Recommendation == 1)
                {
                    var fad = FAAudit.FAAuditDetails[i];
                    var fal = IGeneralLedgerService.GetLastFixedAssetLedger(fad.FixedAssetID, FAAudit.PostedDate);
                    var fa = IFixedAssetService.Getbykey(fad.FixedAssetID ?? Guid.Empty);
                    numberOfLostAsset++;
                    FADecrementDetail detail = new FADecrementDetail
                    {
                        ID = Guid.NewGuid(),
                        FADecrementID = FADecrement.ID,
                        FixedAssetID = fad.FixedAssetID,
                        DepartmentID = fad.DepartmentID,
                        OriginalPrice = fad.OriginalPrice,
                        AcDepreciationAmount = fad.AcDepreciationAmount,
                        RemainingAmount = fad.RemainingAmount,
                        OriginalPriceAccount = fal.OriginalPriceAccount,
                        DepreciationAccount = fal.DepreciationAccount,
                        ExpenditureAccount = fa.ExpenditureAccount,
                        PurchasePrice = fa.PurchasePrice,
                        HandlingResidualValueAccount = "811",
                    };
                    decimal ac = IFixedAssetLedgerService.GetTotalAcDepreciationAmount(fad.FixedAssetID ?? Guid.Empty, FAAudit.PostedDate);
                    var lstPost = new List<FADecrementDetailPost>
                    {
                        new FADecrementDetailPost
                        {
                            FADecrementDetailID = detail.ID,
                            DebitAccount = fal.DepreciationAccount,
                            CreditAccount = fal.OriginalPriceAccount,
                            Description = "Giá trị hao mòn lũy kế tài sản",
                            OrderPriority = 0,
                            AmountOriginal = ac,
                        },
                        new FADecrementDetailPost
                        {
                            FADecrementDetailID = detail.ID,
                            DebitAccount = "1111",
                            CreditAccount = fal.OriginalPriceAccount,
                            Description = "Xử lý giá trị còn lại",
                            OrderPriority = 1,
                            AmountOriginal = fad.RemainingAmount ?? 0,
                        }
                    };
                    totalAmount += fad.RemainingAmount ?? 0;
                    acdeprecaition += ac;
                    detail.FADecrementDetailPosts = lstPost;
                    lstDetail.Add(detail);
                }
            }
            FADecrement.FADecrementDetails = lstDetail;
            FADecrement.TotalAmountOriginal = FADecrement.TotalAmount = totalAmount;
            return numberOfLostAsset > 0 ? FADecrement : null;
        }

        private TIDecrement GetDecrementFromAudit(TIAudit TIAudit)
        {
            TIDecrement TiDecrement = new TIDecrement
            {
                ID = Guid.NewGuid(),
                TypeID = 431,
                Date = TIAudit.Date,
                PostedDate = TIAudit.PostedDate,
                No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(431)),
                Reason = "Ghi giảm từ kiểm kê TSCĐ đến ngày " + TIAudit.PostedDate.ToString("dd/MM/yyyy"),
                TemplateID = new Guid("8E5BC949-73B3-42EA-B7C4-74D397B7EACB"),
                RefID = TIAudit.ID,
                Recorded = true,
            };
            int numberOfLostAsset = 0;
            decimal toltalRemainAmount = 0;
            var lstDetail = new List<TIDecrementDetail>();
            for (int i = 0; i < TIAudit.TIAuditDetails.Count; i++)
            {
                if (TIAudit.TIAuditDetails[i].Recommendation == 1 && TIAudit.TIAuditDetails[i].ExecuteQuantity > 0)
                {
                    var auditDetail = TIAudit.TIAuditDetails[i];
                    var fal = IGeneralLedgerService.GetLastestToolLedger(auditDetail.ToolsID, TIAudit.PostedDate);
                    var tool = ITIInitService.Getbykey(auditDetail.ToolsID ?? Guid.Empty);
                    numberOfLostAsset++;
                    TIDecrementDetail detail = new TIDecrementDetail
                    {
                        TIDecrementID = TiDecrement.ID,
                        ToolsID = tool.ID,
                        Description = tool.ToolsName,
                        ToolsName = tool.ToolsName,
                        DepartmentID = TIAudit.TIAuditDetails[i].DepartmentID,
                        Quantity = TIAudit.TIAuditDetails[i].QuantityOnBook,
                        DecrementQuantity = TIAudit.TIAuditDetails[i].ExecuteQuantity,
                        TIAuditID = TIAudit.TIAuditDetails[i].ID,
                        UnitPrice = fal.UnitPrice ?? 0,
                        RemainingAmount = fal.RemainingQuantity == 0 ? 0 : (fal.RemainingAmount / fal.RemainingQuantity * TIAudit.TIAuditDetails[i].ExecuteQuantity)
                    };
                    toltalRemainAmount += detail.RemainingAmount;
                    lstDetail.Add(detail);
                }
            }
            TiDecrement.TotalRemainingAmount = toltalRemainAmount;
            TiDecrement.TIDecrementDetails = lstDetail;

            return numberOfLostAsset > 0 ? TiDecrement : null;
        }

        private TIIncrement GetIncrementFromAudit(TIAudit TIAudit)
        {
            TIIncrement TiIncrement = new TIIncrement
            {
                ID = Guid.NewGuid(),
                TypeID = 907,
                Date = TIAudit.Date,
                PostedDate = TIAudit.PostedDate,
                No = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(907)),
                Reason = "Ghi tăng từ kiểm kê TSCĐ đến ngày " + TIAudit.PostedDate.ToString("dd/MM/yyyy"),
                TemplateID = new Guid("B3C2576A-51F8-4AB8-AA56-802518AC1F35"),
                RefID = TIAudit.ID,
                Recorded = true,
                CurrencyID = "VND"
            };
            int numberOfLostAsset = 0;
            decimal totalamount = 0;
            var lstDetail = new List<TIIncrementDetail>();
            for (int i = 0; i < TIAudit.TIAuditDetails.Count; i++)
            {
                if (TIAudit.TIAuditDetails[i].Recommendation == 2 && TIAudit.TIAuditDetails[i].ExecuteQuantity > 0)
                {
                    var auditDetail = TIAudit.TIAuditDetails[i];
                    var fal = IGeneralLedgerService.GetLastestToolLedger(auditDetail.ToolsID, TIAudit.PostedDate);
                    var tool = ITIInitService.Getbykey(auditDetail.ToolsID ?? Guid.Empty);
                    numberOfLostAsset++;
                    TIIncrementDetail detail = new TIIncrementDetail
                    {
                        TIIncrementID = TiIncrement.ID,
                        ToolsID = tool.ID,
                        Description = tool.ToolsName,
                        ToolsName = tool.ToolsName,
                        DepartmentID = TIAudit.TIAuditDetails[i].DepartmentID,
                        Quantity = TIAudit.TIAuditDetails[i].ExecuteQuantity,
                        TIAuditID = TIAudit.TIAuditDetails[i].ID,
                        DebitAccount = "153",
                        CreditAccount = "3388",
                        UnitPrice = fal.UnitPrice ?? 0,
                        UnitPriceOriginal = fal.UnitPrice ?? 0,
                        AmountOriginal = (fal.UnitPrice ?? 0) * TIAudit.TIAuditDetails[i].ExecuteQuantity
                    };
                    totalamount += detail.AmountOriginal;
                    lstDetail.Add(detail);
                }
            }
            TiIncrement.TotalAmount = TiIncrement.TotalAmountOriginal = totalamount;
            TiIncrement.TIIncrementDetails = lstDetail;
            return numberOfLostAsset > 0 ? TiIncrement : null;
        }

        private void ViewException(Exception ex)
        {
            var mes = ex.Message;
            var backtrack = ex.StackTrace;
            Logs.WriteTxt(ex.InnerException.ToString());
            Logs.WriteTxt(ex.Message);
            Logs.WriteTxt(ex.StackTrace);
            Logs.WriteTxt(ex.Data.ToString());
        }
        protected virtual bool Edit()
        {
            try
            {
                BaseService<T, Guid> services = this.GetIService(_select);
            Begin:
                try
                {
                    ISAInvoiceService.GetSession().Clear();
                    services.BeginTran();
                    #region Update TT hóa đơn
                    if (typeof(T) == typeof(SAInvoice))
                    {
                        if ((_select as SAInvoice).BillRefID != null)
                        {
                            (_select as SAInvoice).SABills = null;
                            (_select as SAInvoice).InvoiceNo = null;
                            (_select as SAInvoice).InvoiceDate = null;
                        }
                        else if (!(_select as SAInvoice).IsBill)
                        {
                            (_select as SAInvoice).SABills = null;
                            (_select as SAInvoice).InvoiceNo = null;
                            (_select as SAInvoice).InvoiceDate = null;
                        }
                    }
                    #endregion
                    #region Lưu chứng từ móc
                    if (_selectJoin != null && _selectJoin.GetType().Name != typeof(object).Name)
                    {

                        if (!SaveSelectJoin())
                        {
                            services.RolbackTran();
                            return false;
                        }
                    }
                    else
                    {
                        if (TypeID == 540)
                        {
                            var lst = ((FADepreciation)(object)_backSelect).FADepreciationPosts;
                            if (lst.Count() > 0)
                                foreach (var x in lst) _IFADepreciationPostService.Delete(x);
                        }
                        if (TypeID == 434)
                        {
                            var lst = ((TIAllocation)(object)_backSelect).TIAllocationPosts;
                            if (lst.Count() > 0)
                                foreach (var x in lst) _ITIAllocationPostService.Delete(x);
                        }
                        if (TypeID == 690)
                        {
                            var lst = ((GOtherVoucher)(object)_backSelect).GOtherVoucherDetails;
                            if (lst.Count() > 0)
                                foreach (var x in lst) _IGOtherVoucherDetailService.Delete(x);
                        }
                        SetOrGetGuiObject(_select, true);//Thêm phần get lại ds con để fix lỗi ko lấy ID của đối tượng cha trong đối tượng con [Huy Anh]                             
                    }
                    #endregion
                    #region Set ID cho mua hàng
                    if (typeof(T) == typeof(PPInvoice))
                    {
                        foreach (var x in (_select as PPInvoice).PPInvoiceDetails)
                        {
                            if (x.ID == null || x.ID == Guid.Empty) x.ID = Guid.NewGuid();
                        }
                    }
                    #endregion
                    #region Nếu là kiểm kê TSCĐ, recommendation == ghi giảm => tạo chứng từ ghi giảm
                    if (TypeID == 560)
                    {
                        FAAudit fAAudit = (FAAudit)(object)_select;
                        FADecrement decrement = GetDecrementFromAudit(fAAudit);
                        FADecrement oldFaDecrement = IFADecrementService.findByRefID(fAAudit.ID);
                        if (oldFaDecrement != null)
                        {
                            // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                            if (oldFaDecrement.Recorded)
                            {
                                MSG.Warning("Bảng kiểm kê tài sản đã có phát sinh chứng từ xử lý ghi giảm. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                return false;
                            }
                            IFADecrementService.Delete(oldFaDecrement);
                            decrement.No = oldFaDecrement.No;
                            decrement.Date = oldFaDecrement.Date;
                            decrement.PostedDate = oldFaDecrement.PostedDate;
                        }


                        if (decrement != null)
                        {
                            foreach (var FADecrementDetail in decrement.FADecrementDetails)
                            {
                                int result = IFixedAssetLedgerService.CheckLedgerForDecrement(FADecrementDetail.FixedAssetID ?? Guid.Empty, decrement.PostedDate, false);
                                if (result == 2)
                                {
                                    MSG.Warning("Ghi sổ thất bại! Tài sản này đã có phát sinh điều chỉnh, điều chuyển, tính khấu hao hoặc ghi giảm sau ngày hạch toán của chứng từ tính khấu hao này");
                                    services.RolbackTran();
                                    IFADecrementService.RolbackTran();
                                    isFirst = false;
                                    return false;
                                }
                                else if (result == 1)
                                {
                                    MSG.Warning("Ghi sổ thất bại! Tài sản này đã được ghi giảm, vui lòng kiểm tra lại");
                                    services.RolbackTran();
                                    IFADecrementService.RolbackTran();
                                    isFirst = false;
                                    return false;
                                }

                            }
                            IFADecrementService.CreateNew(decrement);
                            bool status = IGenCodeService.UpdateGenCodeForm(56, decrement.No, decrement.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IFADecrementService.RolbackTran();
                                return true;
                            }
                            FADecrement = decrement;
                        }

                    }
                    #endregion
                    #region Nếu là kiem ke quy
                    if (TypeID == 180)
                    {
                        MCAudit mcAudit = (MCAudit)(object)_select;
                        MCReceipt mcReceipt = null;
                        MCPayment mcPayment = null;
                        if (mcAudit.TotalAuditAmount > mcAudit.TotalBalanceAmount)
                            mcReceipt = GetReceiptFromAudit(mcAudit);
                        else if (mcAudit.TotalAuditAmount < mcAudit.TotalBalanceAmount)
                            mcPayment = GetPaymentFromAudit(mcAudit);
                        MCPayment oldmcPayment = IMCPaymentService.findByAuditID(mcAudit.ID);
                        MCReceipt oldmcReceipt = IMCReceiptService.findByAuditID(mcAudit.ID);
                        if (oldmcPayment != null)
                        {
                            // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                            if (oldmcPayment.Recorded)
                            {
                                MSG.Warning("Bảng kiểm kê quỹ đã có phát sinh phiếu chi. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                                services.RolbackTran();
                                IMCPaymentService.RolbackTran();
                                return false;
                            }
                            IMCPaymentService.Delete(oldmcPayment);
                        }
                        if (mcPayment != null)
                        {
                            if (oldmcPayment != null) mcPayment.No = oldmcPayment.No;
                            IMCPaymentService.CreateNew(mcPayment);
                            bool status = IGenCodeService.UpdateGenCodeForm(11, mcPayment.No, mcPayment.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IMCPaymentService.RolbackTran();
                                return true;
                            }
                        }
                        if (oldmcReceipt != null)
                        {
                            // Nếu ghi giảm đã ghi sổ, thì không cho sửa kiểm kê
                            if (oldmcReceipt.Recorded)
                            {
                                MSG.Warning("Bảng kiểm kê tài sản đã có phát sinh phiếu thu. Vui lòng kiểm tra lại trước khi thực hiện việc sửa đổi bảng kiểm kê này!");
                                services.RolbackTran();
                                IMCPaymentService.RolbackTran();
                                return false;
                            }
                            IMCPaymentService.Delete(oldmcPayment);
                        }
                        if (mcReceipt != null)
                        {
                            if (oldmcReceipt != null) mcReceipt.No = oldmcReceipt.No;
                            IMCReceiptService.CreateNew(mcReceipt);
                            bool status = IGenCodeService.UpdateGenCodeForm(10, mcReceipt.No, mcReceipt.ID);
                            if (!status)
                            {
                                MSG.Warning(resSystem.MSG_System_52);
                                services.RolbackTran();
                                IMCPaymentService.RolbackTran();
                                return true;
                            }
                        }

                    }
                    #endregion
                    IFAIncrementDetailService.UnbindSession(Utils.ListFAIncrementDetail);
                    #region trungnq check trường hợp nếu chứng từ sửa đính đèm thành không đính kèm thì xóa chứng từ đính kèm và các tham chiếu liên quan đến nó
                    if (_select.HasProperty("IsBill"))
                    {

                    }
                    if (_select.HasProperty("IsDeliveryVoucher"))
                    {
                        bool? IsDeliveryVoucherNew = _select.GetProperty<T, bool?>("IsDeliveryVoucher");
                        bool? IsDeliveryVoucherOld = _backSelect.GetProperty<T, bool?>("IsDeliveryVoucher");
                        if (IsDeliveryVoucherNew == false && IsDeliveryVoucherOld == true)
                        {
                            List<string> lstPropertyNo = new List<string> { "OutwardNo", "IWNo", "InwardNo" };
                            foreach (var item in lstPropertyNo)
                            {
                                if (_select.HasProperty(item))
                                {
                                    string no = _backSelect.GetProperty<T, string>(item) == null ? "" : _backSelect.GetProperty<T, string>(item);
                                    if (!no.IsNullOrEmpty())
                                    {
                                        IRSInwardOutwardService.BeginTran();
                                        RSInwardOutward rSInwardOutward = IRSInwardOutwardService.getMCRSInwardOutwardbyNo(no);
                                        List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == no).ToList();
                                        if (listrefVouchersRSInwardOutward.Count != 0)
                                        {
                                            IRefVoucherRSInwardOutwardService.BeginTran();
                                            foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                            {

                                                IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                            }
                                            IRefVoucherRSInwardOutwardService.CommitTran();
                                        }
                                        List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == no).ToList();
                                        if (listrefVouchers.Count != 0)
                                        {
                                            IRefVoucherService.BeginTran();
                                            foreach (var refVouchers in listrefVouchers)
                                            {

                                                IRefVoucherService.Delete(refVouchers);

                                            }
                                            IRefVoucherService.CommitTran();
                                        }
                                        IRSInwardOutwardService.Delete(rSInwardOutward);
                                        IRSInwardOutwardService.CommitTran();
                                    }

                                }
                            }
                        }
                        if (IsDeliveryVoucherNew == true && IsDeliveryVoucherOld == true)
                        {
                            List<string> lstPropertyNo = new List<string> { "OutwardNo", "IWNo", "InwardNo" };
                            foreach (var item in lstPropertyNo)
                            {
                                if (_select.HasProperty(item))
                                {
                                    string oldNo = _backSelect.GetProperty<T, string>(item) == null ? "" : _backSelect.GetProperty<T, string>(item);
                                    string newNo = _select.GetProperty<T, string>(item) == null ? "" : _select.GetProperty<T, string>(item);
                                    if (oldNo.IsNullOrEmpty())
                                    { continue; }
                                    List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == oldNo).ToList();
                                    if (listrefVouchersRSInwardOutward.Count != 0)
                                    {
                                        IRefVoucherRSInwardOutwardService.BeginTran();
                                        foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                        {

                                            IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                        }
                                        IRefVoucherRSInwardOutwardService.CommitTran();
                                    }
                                    List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == oldNo).ToList();
                                    if (listrefVouchers.Count != 0)
                                    {
                                        IRefVoucherService.BeginTran();
                                        foreach (var refVouchers in listrefVouchers)
                                        {

                                            IRefVoucherService.Delete(refVouchers);

                                        }
                                        IRefVoucherService.CommitTran();
                                    }

                                }
                            }
                        }
                    }
                    if (_select.HasProperty("MNo"))
                    {
                        string NewMNo = _select.GetProperty<T, string>("MNo") == null ? "" : _select.GetProperty<T, string>("MNo");
                        string OldMNo = _backSelect.GetProperty<T, string>("MNo") == null ? "" : _select.GetProperty<T, string>("MNo");
                        int NewTypeID = 0;
                        int OldTypeID = 0;
                        if (_select.HasProperty("TypeID"))
                        {
                            NewTypeID = _select.GetProperty<T, int>("TypeID") == null ? 0 : _select.GetProperty<T, int>("TypeID");
                            OldTypeID = _backSelect.GetProperty<T, int>("TypeID") == null ? 0 : _backSelect.GetProperty<T, int>("TypeID");
                            if (OldTypeID == 321 && NewTypeID == 322) //bán hàng thu tiền ngay chuyển từ kèm phiếu thu sang kèm báo có => xóa tham chiếu phiếu thu cũ
                            {
                                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchersRSInwardOutward.Count != 0)
                                {
                                    IRefVoucherRSInwardOutwardService.BeginTran();
                                    foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                    {

                                        IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                    }
                                    IRefVoucherRSInwardOutwardService.CommitTran();
                                }
                                List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchers.Count != 0)
                                {
                                    IRefVoucherService.BeginTran();
                                    foreach (var refVouchers in listrefVouchers)
                                    {

                                        IRefVoucherService.Delete(refVouchers);

                                    }
                                    IRefVoucherService.CommitTran();
                                }
                            }
                            if (OldTypeID == 322 && NewTypeID == 321)  //bán hàng thu tiền ngay chuyển từ kèm báo có sang kèm phiếu thu => xóa tham chiếu báo có cũ
                            {
                                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchersRSInwardOutward.Count != 0)
                                {
                                    IRefVoucherRSInwardOutwardService.BeginTran();
                                    foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                    {

                                        IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                    }
                                    IRefVoucherRSInwardOutwardService.CommitTran();
                                }
                                List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchers.Count != 0)
                                {
                                    IRefVoucherService.BeginTran();
                                    foreach (var refVouchers in listrefVouchers)
                                    {

                                        IRefVoucherService.Delete(refVouchers);

                                    }
                                    IRefVoucherService.CommitTran();
                                }
                            }
                            if (OldTypeID == 324 && NewTypeID == 325)  //bán hàng đại lí bán đúng giá chuyển từ kèm phiếu thu sang kèm báo có => xóa tham chiếu phiếu thu cũ
                            {
                                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchersRSInwardOutward.Count != 0)
                                {
                                    IRefVoucherRSInwardOutwardService.BeginTran();
                                    foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                    {

                                        IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                    }
                                    IRefVoucherRSInwardOutwardService.CommitTran();
                                }
                                List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchers.Count != 0)
                                {
                                    IRefVoucherService.BeginTran();
                                    foreach (var refVouchers in listrefVouchers)
                                    {

                                        IRefVoucherService.Delete(refVouchers);

                                    }
                                    IRefVoucherService.CommitTran();
                                }
                            }
                            if (OldTypeID == 325 && NewTypeID == 324)  //bán hàng đại lí bán đúng giá chuyển từ kèm báo có sang kèm phiếu thu => xóa tham chiếu báo có cũ
                            {
                                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchersRSInwardOutward.Count != 0)
                                {
                                    IRefVoucherRSInwardOutwardService.BeginTran();
                                    foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                    {

                                        IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                    }
                                    IRefVoucherRSInwardOutwardService.CommitTran();
                                }
                                List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchers.Count != 0)
                                {
                                    IRefVoucherService.BeginTran();
                                    foreach (var refVouchers in listrefVouchers)
                                    {

                                        IRefVoucherService.Delete(refVouchers);

                                    }
                                    IRefVoucherService.CommitTran();
                                }
                            }
                            if (OldTypeID == 324 && NewTypeID == 323)  //bán hàng đại lí bán đúng giá chuyển từ kèm phiếu thu sang không kèm gì => xóa tham chiếu phiếu thu cũ
                            {
                                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchersRSInwardOutward.Count != 0)
                                {
                                    IRefVoucherRSInwardOutwardService.BeginTran();
                                    foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                    {

                                        IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                    }
                                    IRefVoucherRSInwardOutwardService.CommitTran();
                                }
                                List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchers.Count != 0)
                                {
                                    IRefVoucherService.BeginTran();
                                    foreach (var refVouchers in listrefVouchers)
                                    {

                                        IRefVoucherService.Delete(refVouchers);

                                    }
                                    IRefVoucherService.CommitTran();
                                }
                            }
                            if (OldTypeID == 325 && NewTypeID == 323)  //bán hàng đại lí bán đúng giá chuyển từ kèm báo có sang không kèm gì => xóa tham chiếu báo có cũ
                            {
                                List<RefVoucherRSInwardOutward> listrefVouchersRSInwardOutward = Utils.ListRefVoucherRSInwardOutward.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchersRSInwardOutward.Count != 0)
                                {
                                    IRefVoucherRSInwardOutwardService.BeginTran();
                                    foreach (var refVouchersRSInwardOutward in listrefVouchersRSInwardOutward)
                                    {

                                        IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                    }
                                    IRefVoucherRSInwardOutwardService.CommitTran();
                                }
                                List<RefVoucher> listrefVouchers = Utils.ListRefVoucher.Where(n => n.RefID2 == _select.GetProperty<T, Guid>("ID") && n.No == OldMNo).ToList();
                                if (listrefVouchers.Count != 0)
                                {
                                    IRefVoucherService.BeginTran();
                                    foreach (var refVouchers in listrefVouchers)
                                    {

                                        IRefVoucherService.Delete(refVouchers);

                                    }
                                    IRefVoucherService.CommitTran();
                                }
                            }

                        }


                    }

                    #endregion

                    #region trungnq trường hợp mua và ghi tăng công cụ dụng cụ và ghi tăng tscđ thay đổi khác typeid thì xóa chứng từ tham chiếu đến nó
                    if (_select.HasProperty("TypeID") && _backSelect.HasProperty("TypeID"))
                    {
                        if ((_select.GetProperty("TypeID") != null) && (_backSelect.GetProperty("TypeID") != null))
                        {
                            if ((_select.GetProperty("TypeID").ToString() != "") && (_backSelect.GetProperty("TypeID").ToString() != ""))
                            {
                                int typeidNew = (int)_select.GetProperty("TypeID");
                                int typeidOld = (int)_backSelect.GetProperty("TypeID");
                                if (typeidOld != typeidNew)
                                {
                                    if (new List<int> { 430, 902, 903, 904, 905, 906, 500, 119, 129, 132, 142, 172 }.Contains(typeidNew))
                                    {
                                        Guid guid = (Guid)_backSelect.GetProperty("ID");
                                        string no = (string)_backSelect.GetProperty("No");
                                        List<RefVoucher> refVouchers = Utils.IRefVoucherService.Query.Where(n => n.RefID2 == guid && n.TypeID == typeidOld && n.No == no).ToList();
                                        if (refVouchers.Count != 0)
                                        {
                                            IRefVoucherService.BeginTran();
                                            foreach (var refVouchers1 in refVouchers)
                                            {

                                                IRefVoucherService.Delete(refVouchers1);

                                            }
                                            IRefVoucherService.CommitTran();
                                        }
                                        List<RefVoucherRSInwardOutward> rSInwardOutwards = Utils.IRefVoucherRSInwardOutwardService.Query.Where(n => n.RefID2 == guid && n.TypeID == typeidOld && n.No == no).ToList();
                                        if (rSInwardOutwards.Count != 0)
                                        {
                                            IRefVoucherRSInwardOutwardService.BeginTran();
                                            foreach (var refVouchersRSInwardOutward in rSInwardOutwards)
                                            {

                                                IRefVoucherRSInwardOutwardService.Delete(refVouchersRSInwardOutward);

                                            }
                                            IRefVoucherRSInwardOutwardService.CommitTran();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    services.Update(_select);
                    if (!GenVoucherOther(_select, _backSelect, _statusForm))
                    {
                        services.RolbackTran();
                        return false;
                    }
                    services.CommitTran();
                    Logs.SaveBusiness(_select, TypeID, Logs.Action.Edit);
                    //DialogResult = DialogResult.OK;
                    return true;
                }
                catch (Exception ex)
                {//lỗi khi thực thi services
                    ViewException(ex);
                    if (ex.Message == "Illegal attempt to associate a collection with two open sessions")
                    {
                        Guid id = _select.GetProperty<T, Guid>("ID");
                        var temp = (T)Activator.CreateInstance(typeof(T));
                        //foreach (var propertyInfo in temp.GetType().GetProperties())
                        //{
                        //    if (_select.HasProperty(propertyInfo.Name))
                        //        propertyInfo.SetValue(temp, _select.GetProperty(propertyInfo.Name), null);
                        //}
                        //_select = services.Getbykey(id);
                        //services.UnbindSession(temp);
                        //_select = services.Getbykey(id);
                        //foreach (var propertyInfo in _select.GetType().GetProperties())
                        //{
                        //    if (temp.HasProperty(propertyInfo.Name))
                        //    {
                        //        var tmp = temp.GetProperty(propertyInfo.Name);
                        //        if (tmp.GetType().Name.Contains("List"))
                        //        {
                        //            IList ilist;
                        //            foreach (var item in (IList)temp)
                        //            {
                        //                ilist.Add(item.CloneObject());
                        //            }
                        //            continue;
                        //        }
                        //        propertyInfo.SetValue(_select, tmp, null);
                        //    }
                        //}
                        _select.CopyTo(ref temp);
                        _select = services.Getbykey(id);
                        services.UnbindSession(_select);
                        _select = services.Getbykey(id);
                        foreach (var propertyInfo in _select.GetType().GetProperties())
                        {
                            if (temp.HasProperty(propertyInfo.Name))
                            {
                                var tmp = temp.GetProperty(propertyInfo.Name);
                                //if (tmp.GetType().Name.Contains("List"))
                                //{
                                //    IList ilist;
                                //    foreach (var item in (IList)temp)
                                //    {
                                //        ilist.Add(item.CloneObject());
                                //    }
                                //    continue;
                                //}
                                propertyInfo.SetValue(_select, tmp.CloneObject(), null);
                            }
                        }
                        goto Begin;
                    }
                    services.RolbackTran();
                    //MSG.Error("abcdefgh....");
                    services.BindSession(_listSelects);
                    return false;
                }
            }
            catch (Exception)
            {
                //lỗi khi tạo services chung
                //MSG.Error("abcdefgh....");
                return false;
            }
            //throw new NotImplementedException();
        }

        private bool SaveSelectJoin(bool remove = false)
        {
            bool status = true;
            try
            {
                #region Lưu chứng từ móc
                if (!remove)
                {
                    PropertyInfo[] propertyInfos = _select.GetType().GetProperties();
                    foreach (PropertyInfo propertyInfo in propertyInfos)
                    {
                        if (propertyInfo.CanRead)
                        {
                            if (!propertyInfo.Name.Equals("ID") && !propertyInfo.Name.Contains("No") &&
                                !propertyInfo.Name.Contains("Date") && !propertyInfo.Name.Contains("TypeID") && !propertyInfo.Name.Contains("TemplateID") && !propertyInfo.Name.Equals("Type"))
                            {
                                PropertyInfo propJoin = _selectJoin.GetType().GetProperty(propertyInfo.Name);
                                if (propJoin != null && propJoin.CanWrite)
                                {
                                    var value = propertyInfo.GetValue(_select, null);
                                    propJoin.SetValue(_selectJoin, value, null);
                                }
                            }
                        }
                    }
                    SetOrGetGuiObject(_selectJoin, true);
                }
                if (ID == null) return false;
                var guid = (Guid)ID;
                //SaInvoice
                if (Utils.TypesSaInvoice.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = ISAInvoiceService.Getbykey(guid);
                        if (temp != null)
                            ISAInvoiceService.Delete(temp);
                    }
                    else ISAInvoiceService.Update((SAInvoice)_selectJoin);
                }
                //PPInvoice
                else if (Utils.TypesPpInvoice.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = IPPInvoiceService.Getbykey(guid);
                        if (temp != null)
                            IPPInvoiceService.Delete(temp);
                    }
                    else IPPInvoiceService.Update((PPInvoice)_selectJoin);
                }
                //FAIncrement
                else if (Utils.TypesTiIncrement.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = ITIIncrementService.Getbykey(guid);
                        if (temp != null)
                            ITIIncrementService.Delete(temp);
                    }
                    else if (_selectJoin.GetType().Name == typeof(TIIncrement).Name) ITIIncrementService.Update((TIIncrement)_selectJoin);
                }
                //FAIncrement
                else if (Utils.TypesFaIncrement.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = IFAIncrementService.Getbykey(guid);
                        if (temp != null)
                            IFAIncrementService.Delete(temp);
                    }
                    else if (_selectJoin.GetType().Name == typeof(FAIncrement).Name) IFAIncrementService.Update((FAIncrement)_selectJoin);
                }
                //PPService
                else if (Utils.TypesPpService.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = IPPServiceService.Getbykey(guid);
                        if (temp != null)
                            IPPServiceService.Delete(temp);
                    }
                    else IPPServiceService.Update((PPService)_selectJoin);
                }
                //PPDiscountReturn
                else if (Utils.TypesPpDiscountReturn.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = IPPDiscountReturnService.Getbykey(guid);
                        if (temp != null)
                            IPPDiscountReturnService.Delete(temp);
                    }
                    else IPPDiscountReturnService.Update((PPDiscountReturn)_selectJoin);
                }
                //SAReturn
                else if (Utils.TypesSaReturn.Any(p => p == TypeID))
                {
                    if (remove)
                    {
                        var temp = ISAReturnService.Getbykey(guid);
                        if (temp != null)
                            ISAReturnService.Delete(temp);
                    }
                    else ISAReturnService.Update((SAReturn)_selectJoin);
                }
                //======================================================//
                #region Lưu thêm TH chứng từ móc 3
                #region Nếu TH hiện là Thu/Chi
                if (new int[] { 102, 117, 127, 131, 141, 171 }.Any(p => p == TypeID))
                {
                    status = SaveLeftJoinSelect<T, RSInwardOutward>(_select, ID, remove);
                }
                #endregion
                #region Nếu TH hiện là Nhập kho
                else if (new int[] { 402 }.Any(p => p == TypeID))
                {
                    status = SaveLeftJoinSelect<T, MCPayment>(_select, ID, remove);
                }
                #endregion
                #region Nếu TH hiện là Xuất kho
                else if (new int[] { 412 }.Any(p => p == TypeID))
                {
                    status = SaveLeftJoinSelect<T, MCReceipt>(_select, ID, remove);
                }
                #endregion
                #endregion
                #endregion

                #region Cập nhật lại Số lượng nhận vào Đơn mua hàng nếu là Mua hàng
                if (_selectJoin is PPInvoice)
                    status = _statusForm == ConstFrm.optStatusForm.Delete
                                 ? RemoveQuantityReceipt(_selectJoin as PPInvoice)
                                 : UpdateQuantityReceipt(_selectJoin as PPInvoice, _statusForm == ConstFrm.optStatusForm.Edit ? _backSelectJoin as PPInvoice : null);
                #endregion

                #region Cập nhật lại Số lượng nhận vào Đơn đặt hàng nếu là Bán hàng
                if (_selectJoin is SAInvoice)
                    status = _statusForm == ConstFrm.optStatusForm.Delete
                                 ? RemoveQuantityReceipt(_selectJoin as SAInvoice)
                                 : UpdateQuantityReceipt(_selectJoin as SAInvoice, _statusForm == ConstFrm.optStatusForm.Edit ? _backSelectJoin as SAInvoice : null);
                #endregion
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        private bool RemoveVoucherOther()
        {
            bool status = true;
            try
            {
                //có key và key có value
                if (!_select.HasProperty("ID")) return true;
                Guid guid = _select.GetProperty<T, Guid>("ID");
                if (!_select.HasProperty("TypeID")) return true;
                int typeid = _select.GetProperty<T, int>("TypeID");

            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }

        private bool SaveLeftJoinSelect<TL>(TL leftJoin, bool remove) where TL : class
        {
            if (ID == null) return false;
            var services = this.GetIService(leftJoin);
            if (ID != null)
            {
                if (remove)
                {
                    services.Delete((Guid)ID);
                    return true;
                }
                leftJoin = services.Getbykey((Guid)ID);
            }
            if (leftJoin != null)
            {
                PropertyInfo propAccountingObjectId = _select.GetType().GetProperty("AccountingObjectID");
                if (propAccountingObjectId != null && propAccountingObjectId.CanWrite && propAccountingObjectId.CanRead)
                {
                    PropertyInfo propLjAccountingObjectId = leftJoin.GetType().GetProperty("AccountingObjectID");
                    if (propLjAccountingObjectId != null && propLjAccountingObjectId.CanWrite)
                        propLjAccountingObjectId.SetValue(leftJoin,
                                                          (Guid)propAccountingObjectId.GetValue(_select, null), null);
                }
                PropertyInfo propAccountingObjectName = _select.GetType().GetProperty("AccountingObjectName");
                if (propAccountingObjectName != null && propAccountingObjectName.CanWrite && propAccountingObjectName.CanRead)
                {
                    PropertyInfo propLjAccountingObjectName = leftJoin.GetType().GetProperty("AccountingObjectName");
                    propLjAccountingObjectName.SetValue(leftJoin,
                                                        (string)propAccountingObjectName.GetValue(_select, null), null);
                }
                PropertyInfo propAccountingObjectAddress = _select.GetType().GetProperty("AccountingObjectAddress");
                if (propAccountingObjectAddress != null && propAccountingObjectAddress.CanWrite && propAccountingObjectAddress.CanRead)
                {
                    PropertyInfo propLjAccountingObjectAddress = leftJoin.GetType().GetProperty("AccountingObjectAddress");
                    propLjAccountingObjectAddress.SetValue(leftJoin,
                                                           (string)propAccountingObjectAddress.GetValue(_select, null),
                                                           null);
                }
                services.Update(leftJoin);
            }
            return true;
        }

        public virtual void Reset()
        {
            //Utils.ClearCache();
            BaseService<T, Guid> testInvoke = this.GetIService(_select);
            PropertyInfo propId = _select.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(_select, null);
                if (temp is Guid)
                {
                    var currentId = (Guid)temp;
                    try
                    {
                        _select = testInvoke.Getbykey(currentId);
                        testInvoke.UnbindSession(_select);
                        _select = testInvoke.Getbykey(currentId);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(resSystem.MSG_Error_13);
                        IsClose = true;
                        Close();
                    }
                }
            }
            //testInvoke.UnbindSession(_select);
            //InitializeGUI(_select);
            //Hautv sửa tiến lùi sau khi thêm mới
            if (!_listSelects.Any(n => n.GetProperty("ID").ToString() == _select.GetProperty("ID").ToString()))
            {
                _listSelects.Insert(0, _select);
            }
            ReloadToolbar(_select, _listSelects, _statusForm);
        }

        #region Cấu hình phần Tiện ích
        public void ConfigUtilities(int typeId, int statusForm)
        {
            string str = String.Empty;
            //RibbonGroup ribbonGroup = new RibbonGroup("ribbonGroup4");
            //ribbonGroup.Caption = "";
            //if (!this.utmDetailBaseToolBar.Ribbon.Tabs["ribbon2"].Groups.Exists(ribbonGroup.Key))
            //{
            //    this.utmDetailBaseToolBar.Ribbon.Tabs["ribbon2"].Groups.Add(ribbonGroup);
            //}
            RibbonGroup ribbonGroup = new RibbonGroup("ribbonGroup4");
            ribbonGroup.Caption = "";
            if (this.utmDetailBaseToolBar.Ribbon.Tabs["ribbon2"].Groups.Exists(ribbonGroup.Key))
            {
                this.utmDetailBaseToolBar.Ribbon.Tabs["ribbon2"].Groups.Remove(ribbonGroup.Key);
            }
            this.utmDetailBaseToolBar.Ribbon.Tabs["ribbon2"].Groups.Add(ribbonGroup);
            var popupMenuTool = (PopupMenuTool)utmDetailBaseToolBar.Tools["mnpopUtilities"];
            if (popupMenuTool != null)
            {
                foreach (var item in Utils.DicUtilities) if (item.Key.Equals(typeId.ToString(CultureInfo.InvariantCulture))) str = item.Value.Value;
                if (typeId == 430 || typeId == 907 || typeId == 540 || typeId == 550 || typeId == 560 || typeId == 431 || typeId == 432 || typeId == 433 || typeId == 434 || typeId == 435 || typeId == 326)
                {
                    str = "AccountingObject;Employee;Repository;MaterialGoods;BudgetItem;StatisticsCode;CostSet";//trungnq
                }
                List<Item> listCaptions = Utils.DicUtilitiesCaption.Values.ToList();
                List<string> lstUtilities = str.Split(';').ToList();
                foreach (string utility in lstUtilities)
                {
                    if (!utility.Equals("Contract"))
                    {
                        if (!string.IsNullOrEmpty(utility) && !popupMenuTool.Tools.Exists("mnbtnUtilitie_" + utility))
                        {
                            if (utility.Equals("Repository") && new int[] { 210, 260, 261, 262, 263, 264 }.Any(x => x == TypeID))
                            {
                                var frm = this as FPPInvoiceDetail;
                                if (frm != null)
                                {
                                    if (frm.TypeFrm == 1)
                                        continue;
                                }
                            }
                            ButtonTool buttonTool = new ButtonTool("mnbtnUtilitie_" + utility);
                            var firstOrDefault = listCaptions.FirstOrDefault(k => k.Name == utility);
                            if (firstOrDefault != null)
                            {
                                string caption = firstOrDefault.Value;
                                buttonTool.SharedPropsInternal.Caption = caption;
                            }
                            switch (utility)
                            {
                                case "AccountingObject":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.khachhang_ncc;
                                    break;
                                case "Employee":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.NHAN_VIEN;
                                    break;
                                case "BudgetItem":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.mucthuchi;
                                    break;
                                case "StatisticsCode":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.NHOMCODONG;
                                    break;
                                //case "Contract":
                                //    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.HOPDONGBAN;
                                //    break;
                                case "CostSet":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.loai_co_phan;
                                    break;
                                case "Repository":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.kho;
                                    break;
                                case "MaterialGoods":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.Shipping;
                                    break;
                                default:
                                    break;
                            }
                            buttonTool.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                            AddTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonTool);
                            ribbonGroup.Tools.AddTool("mnbtnUtilitie_" + utility);
                        }
                        else
                        {
                            ButtonTool buttonTool = new ButtonTool("mnbtnUtilitie_" + utility);
                            RemoveTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonTool);
                            var firstOrDefault = listCaptions.FirstOrDefault(k => k.Name == utility);
                            if (firstOrDefault != null)
                            {
                                string caption = firstOrDefault.Value;
                                buttonTool.SharedPropsInternal.Caption = caption;
                            }
                            switch (utility)
                            {
                                case "AccountingObject":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.khachhang_ncc;
                                    break;
                                case "Employee":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.NHAN_VIEN;
                                    break;
                                case "BudgetItem":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.mucthuchi;
                                    break;
                                case "StatisticsCode":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.NHOMCODONG;
                                    break;
                                //case "Contract":
                                //    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.HOPDONGBAN;
                                //    break;
                                case "CostSet":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.loai_co_phan;
                                    break;
                                case "Repository":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.kho;
                                    break;
                                case "MaterialGoods":
                                    buttonTool.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.Shipping;
                                    break;
                                default:
                                    break;
                            }
                            buttonTool.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                            AddTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonTool);
                            ribbonGroup.Tools.AddTool("mnbtnUtilitie_" + utility);
                        }
                    }
                }
                if (_selectJoin != null || new[] { 404, 411, 430, 431, 432, 433, 434, 435, 500, 510, 520, 530, 540, 550, 560, 907, 326 }.Contains(TypeID)) return;//Neu la chung tu moc thi ko dc Them moi tu chung tu hien thoi
                if (!popupMenuTool.Tools.Exists("mnbtnUtilitie_AddNew"))
                {

                    var buttonToolAddNew = new ButtonTool("mnbtnUtilitie_AddNew");
                    buttonToolAddNew.SharedPropsInternal.Caption = resSystem.String_04;
                    buttonToolAddNew.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                    buttonToolAddNew.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.iAdd;
                    AddTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonToolAddNew);
                    ribbonGroup.Tools.AddTool("mnbtnUtilitie_AddNew");
                }
                else
                {
                    var buttonToolAddNew = new ButtonTool("mnbtnUtilitie_AddNew");
                    RemoveTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonToolAddNew);
                    buttonToolAddNew.SharedPropsInternal.Caption = resSystem.String_04;
                    buttonToolAddNew.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                    buttonToolAddNew.SharedPropsInternal.AppearancesSmall.Appearance.Image = global::Accounting.Properties.Resources.iAdd;
                    AddTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonToolAddNew);
                    ribbonGroup.Tools.AddTool("mnbtnUtilitie_AddNew");
                }
                popupMenuTool.Tools["mnbtnUtilitie_AddNew"].SharedPropsInternal.Enabled = statusForm.Equals(ConstFrm.optStatusForm.View);
                if ((typeId == 840 && statusForm == 1) || (typeId == 434 && statusForm == 1))
                    popupMenuTool.Tools["mnbtnUtilitie_AddNew"].SharedPropsInternal.Enabled = false;//trungnq

                // button cuối cùng là bắt đầu 1 group
                popupMenuTool.Tools[popupMenuTool.Tools.Count - 1].InstanceProps.IsFirstInGroup = true;
                popupMenuTool.Tools[popupMenuTool.Tools.Count - 1].CustomizedIsFirstInGroup = DefaultableBoolean.True;
            }
        }
        public void AddTools(UltraToolbarsManager toolMgr, ToolsCollection tools, ToolBase button)
        {
            if (button != null)
            {
                toolMgr.Tools.Add(button);
                tools.AddTool(button.Key);
            }
        }

        private void RemoveTools(UltraToolbarsManager toolMgr, ToolsCollection tools, ToolBase button)
        {
            if (button != null)
            {
                toolMgr.Tools.Remove(button);
                tools.Remove(button);
            }
        }

        #endregion

        #region Cấu hình phần in ấn DuyNT
        public static IVoucherPatternsReportService IVoucherPatternsReportService
        {
            get { return IoC.Resolve<IVoucherPatternsReportService>(); }
        }
        public void ConfigPrintf<T>(int typeID, int statusForm)
        {
            string str = String.Empty;
            PopupMenuTool popupMenuTool = (PopupMenuTool)this.utmDetailBaseToolBar.Tools["mnbtnPrint"];
            List<VoucherPatternsReport> lstVoucherPatternsReports = new List<VoucherPatternsReport>();
            lstVoucherPatternsReports = IVoucherPatternsReportService.GetAll_ByTypeID(typeID);
            if (popupMenuTool != null)
            {
                for (int i = popupMenuTool.Tools.Count - 1; i >= 0; i--)
                {
                    RemoveTools(utmDetailBaseToolBar, popupMenuTool.Tools, popupMenuTool.Tools[i]);
                }
                if (lstVoucherPatternsReports.Count > 0)
                {
                    foreach (VoucherPatternsReport vpr in lstVoucherPatternsReports)
                    {
                        if (vpr.VoucherPatternsCode == "MCPayment-04-TT" && _select is MCPayment)
                        {
                            MCPayment _data = _select as MCPayment;
                            if (!_data.MCPaymentDetails.Any(o => o.DebitAccount.StartsWith("141")))
                            {
                                continue;
                            }
                        }

                        if (vpr.VoucherPatternsCode == "MCReceipt-04-TT" && _select is MCReceipt)
                        {
                            MCReceipt _data = _select as MCReceipt;
                            if (!_data.MCReceiptDetails.Any(o => o.CreditAccount.StartsWith("141")))
                            {
                                continue;
                            }
                        }

                        //if(vpr.VoucherPatternsCode == "UNC_Agribank" && _select is MBTellerPaperReport)
                        //{
                        //    MBTellerPaperReport _data = _select as MBTellerPaperReport;
                        //    if (_data.BankCode == "Agribank")
                        //        continue;
                        //}

                        if (!popupMenuTool.Tools.Exists("mnbtnPrint_" + vpr.VoucherPatternsCode))
                        {
                            ButtonTool buttonTool = new ButtonTool("mnbtnPrint_" + vpr.VoucherPatternsCode);
                            string caption = vpr.VoucherPatternsName;
                            buttonTool.Tag = vpr;
                            buttonTool.SharedPropsInternal.Caption = caption;
                            buttonTool.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                            AddTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonTool);
                        }
                    }
                }
                if (!popupMenuTool.Tools.Exists("mnbtnPrint_Option"))
                {
                    ButtonTool buttonToolOption = new ButtonTool("mnbtnPrint_Option");
                    const string captionOption = "Tùy chọn";
                    buttonToolOption.SharedPropsInternal.Caption = captionOption;
                    buttonToolOption.SharedPropsInternal.DisplayStyle = ToolDisplayStyle.TextOnlyInMenus;
                    AddTools(utmDetailBaseToolBar, popupMenuTool.Tools, buttonToolOption);
                }
                popupMenuTool.Tools[popupMenuTool.Tools.Count - 1].InstanceProps.IsFirstInGroup = true;
                popupMenuTool.Tools[popupMenuTool.Tools.Count - 1].CustomizedIsFirstInGroup = DefaultableBoolean.True;
            }
        }

        #endregion

        /// <summary>
        /// DeActive hoặc Active control khi ở trạng thái xem hoặc không xem
        /// </summary>
        public void BackFunction()
        {
            //lấy vị trí trước đó
            int indexBack = Utils.GetIndexOfList<T>(_select, _listSelects) + 1;
            if (indexBack >= 0)
                _select = (T)_listSelects[indexBack];
            //add by cuongpv
            _backSelectJoin = null;
            //end add by cuongpv
            InitializeGUI(_select);
            //Config Menu
            ReloadToolbar(_select, _listSelects, _statusForm);
            // set lại ugrid control về trạng thái lúc đầu, thêm vào để sửa bug ghi sổ, bỏ ghi sổ , lùi, tiến, grid dưỡi cùng bị hiện thêm hóa đơn
            SetLaiUgridControl(_select);
        }

        public void ForwardFunction()
        {
            //lấy vị trí trước đó
            int indexForward = Utils.GetIndexOfList<T>(_select, _listSelects) - 1;
            if (indexForward < _listSelects.Count)
                _select = (T)_listSelects[indexForward];

            //add by cuongpv
            _backSelectJoin = null;
            //end add by cuongpv

            InitializeGUI(_select);
            //Config Menu
            ReloadToolbar(_select, _listSelects, _statusForm);
            // set lại ugrid control về trạng thái lúc đầu, thêm vào để sửa bug ghi sổ, bỏ ghi sổ , lùi, tiến, grid dưỡi cùng bị hiện thêm hóa đơn
            SetLaiUgridControl(_select);
        }
        bool IsAddInform = false;
        public void Add4Function()
        {
            if (!Authenticate.Permissions("TH", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            _statusForm = ConstFrm.optStatusForm.Add;
            IsAddInform = true;
            //Config Menu
            ResetForm();
            IsAddInform = false;
            //ReloadToolbar(_select, _listSelects, _statusForm);
            //ChangeStatusControl(true);
        }

        public void Edit1Function()
        {
            if (!Authenticate.Permissions("SU", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }

            //if (new int[] { 220, 320, 321, 322, 323, 324, 325, 326, 340 }.Contains(_typeID))
            //{
            //    if (((_select.GetProperty<T, string>("InvoiceNo") != null && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo"))) || (_select.GetProperty<T, Guid?>("BillRefID") != null && Utils.ListSABill.FirstOrDefault(x => x.ID == (_select.GetProperty<T, Guid?>("BillRefID") ?? Guid.Empty)).InvoiceNo != null && !string.IsNullOrEmpty(Utils.ListSABill.FirstOrDefault(x => x.ID == (_select.GetProperty<T, Guid?>("BillRefID") ?? Guid.Empty)).InvoiceNo))) && new int[] { 1 }.Contains(_select.GetProperty<T, int>("StatusInvoice")))
            //    {
            //        MSG.Warning("Hóa đơn đã được cấp số. Việc sửa đổi hóa đơn phải được thực hiện ở nghiệp vụ thay thế hoặc điều chỉnh!");
            //        return;
            //    }
            //}
            if (new int[] { 173, 133, 143, 126, 116 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPService))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var a = IPPServiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(a.TypeID);
                    if (a != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + a.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 172, 129, 132, 142, 119 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(FAIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var b = IFAIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(b.TypeID);
                    if (b != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + b.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 906, 903, 904, 905, 902 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(TIIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var c = ITIIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(c.TypeID);
                    if (c != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + c.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 171, 127, 131, 141, 117, 402, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var d = IPPInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(d.TypeID);
                    if (d != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + d.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 162, 163, 102, 103, 412, 415, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var e = ISAInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(e.TypeID);
                    if (e != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + e.No + " tại chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 403 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var f = ISAReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(f.TypeID);
                    if (f != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + f.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 413 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPDiscountReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var g = IPPDiscountReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(g.TypeID);
                    if (g != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + g.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }

            }
            _statusForm = ConstFrm.optStatusForm.Edit;

            //Config Menu
            ReloadToolbar(_select, _listSelects, _statusForm);
            //ChangeStatusControl(true);
        }


        /// <summary>
        /// Xử lý TopMenu 
        /// </summary>
        /// <param name="activeSelect">Chứng từ đang thao tác</param>
        /// <param name="listInput">Danh sách chứng từ (phục vụ cho nút Tiến Lùi)</param>
        /// <param name="statusForm">Trạng thái Frm hiện tại là Xem, Thêm, Sửa hay Xóa</param>
        public virtual void ReloadToolbar(T activeSelect, IList<T> listInput, int statusForm)
        {
            var resultStr = BuildConfigButton(activeSelect, listInput, statusForm);
            // switch case dieu kien

            ConfigControlsForm(this);
            ConfigToolBarButtons(resultStr);
            if (utmDetailBaseToolBar.Tools["mnbtnUnPost"].SharedProps.Visible && statusForm == ConstFrm.optStatusForm.View)
            {
                if (_select.HasProperty("PostedDate") && _select.HasProperty("Recorded"))
                {
                    var date = _select.GetProperty<T, DateTime>("PostedDate");
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", (date.Date > Utils.GetDBDateClosed().StringToDateTime()) ? 2 : 1);
                }
            }
            if (utmDetailBaseToolBar.Tools["mnbtnDelete"].SharedProps.Visible && statusForm == ConstFrm.optStatusForm.View)
            {
                if (_select.HasProperty("PostedDate") && _select.HasProperty("Recorded"))
                {
                    var date = _select.GetProperty<T, DateTime>("PostedDate");
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", (date.Date > Utils.GetDBDateClosed().StringToDateTime()) ? 2 : 1);
                }
            }
            if (typeof(T) == typeof(SAOrder) || typeof(T) == typeof(PPOrder) || typeof(T) == typeof(MCAudit) || typeof(T) == typeof(MCAudit) || typeof(T) == typeof(SAQuote) || typeof(T) == typeof(RSAssemblyDismantlement) || typeof(T) == typeof(FAAudit) || typeof(T) == typeof(TIAudit))
            {
                if (_select != null)
                {
                    if (_select.HasProperty("Date"))
                    {
                        var date = _select.GetProperty<T, DateTime>("Date");
                        Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", (date.Date > Utils.GetDBDateClosed().StringToDateTime() && statusForm == ConstFrm.optStatusForm.View) ? 2 : 1);
                    }
                }
            }
            ConfigUtilities(TypeID, statusForm);
            this.ConfigTemplateEdit<T>(utmDetailBaseToolBar, TypeID, statusForm, _templateID);
            ConfigPrintf<T>(TypeID, statusForm);
            Accounting.Core.Domain.Type type = Utils.ListType.FirstOrDefault(p => p.ID == TypeID);
            if (type != null)
            {
                Text = typeof(T) == typeof(PPInvoice) && (this as FPPInvoiceDetail) != null && (this as FPPInvoiceDetail).TypeFrm == 1 ? string.Format("{0} (Không qua kho)", type.TypeName) : type.TypeName;
                //WindowState = FormWindowState.Maximized;
                utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[0].Caption = @"Chức năng";
                //utmDetailBaseToolBar.Office2007UICompatibility = false;
                utmDetailBaseToolBar.Toolbars.Clear();
                utmDetailBaseToolBar.Ribbon.Tabs[0].Groups[0].Caption = string.Empty;
            }
            PropertyInfo propId = activeSelect.GetType().GetProperty("TemplateID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(activeSelect, null);
                if (temp is Guid)
                {
                    _templateID = (Guid)temp;
                }
            }
            ChangeStatusControl(!statusForm.Equals(ConstFrm.optStatusForm.View));
            _select.CopyTo(ref _backSelect);
            if (_selectJoin != null)
                _selectJoin.CopyTo(ref _backSelectJoin);
            _backListObjectInput = _listObjectInput.CloneObject();
            _backListObjectInputPost = _listObjectInputPost.CloneObject();
            _backListObjectInputGroup = _listObjectInputGroup.CloneObject();
            _listInput = listInput;
            this.SetMaxLengthForTextBox();
            this.DeleteSelectedBorderOfUltraTabControl();

            if ((TypeID == 263 || TypeID == 173) && statusForm.Equals(ConstFrm.optStatusForm.View))
            {
                UltraPictureBox picture = this.Controls.Find("picCreditCardType", true).FirstOrDefault() as UltraPictureBox;
                UltraLabel creditCardType = this.Controls.Find("txtCreditCardType", true).FirstOrDefault() as UltraLabel;
                Label ownerCard = this.Controls.Find("txtOwnerCard", true).FirstOrDefault() as Label;
                CreditCard creditCard = Utils.ListCreditCard.FirstOrDefault(x => x.CreditCardNumber == (string)_select.GetProperty("CreditCardNumber"));
                if (creditCard != null)
                {
                    Size sizeImg = new Size(0, 0);
                    picture.Visible = true;
                    ((UltraPictureBox)picture).BorderStyle = UIElementBorderStyle.None;
                    if (creditCard.CreditCardType.Contains("VisaCard"))
                    {
                        ((UltraPictureBox)picture).Image = Properties.Resources.visacard;
                        sizeImg = Properties.Resources.visacard.Size;
                    }
                    else if (creditCard.CreditCardType.Contains("MasterCard"))
                    {
                        ((UltraPictureBox)picture).Image = Properties.Resources.mastercard;
                        sizeImg = Properties.Resources.mastercard.Size;
                    }
                    else if (creditCard.CreditCardType.Contains("Discover"))
                    {
                        ((UltraPictureBox)picture).Image = Properties.Resources.discover_card;
                        sizeImg = Properties.Resources.discover_card.Size;
                    }
                    else if (creditCard.CreditCardType.Contains("Diners Club"))
                    {
                        ((UltraPictureBox)picture).Image = Properties.Resources.DinersClub;
                        sizeImg = Properties.Resources.DinersClub.Size;
                    }
                    else if (creditCard.CreditCardType.Contains("American Express"))
                    {
                        ((UltraPictureBox)picture).Image = Properties.Resources.american_express;
                        sizeImg = Properties.Resources.american_express.Size;
                    }
                    picture.BackColor = Color.Transparent;
                    ((UltraPictureBox)picture).ImageTransparentColor = Color.Transparent;
                    picture.Width = sizeImg.Width / (sizeImg.Height / picture.Height);

                    creditCardType.Text = creditCard.CreditCardType;
                    creditCardType.Location = new Point(picture.Location.X + picture.Width + 5, creditCardType.Location.Y);
                    ownerCard.Text = creditCard.OwnerCard;
                }
            }
            //Đóng form waiting nếu bật
            WaitingFrm.StopWaiting();
            //this.TopMost = true;
        }

        private void ConfigControlsForm(Control ctrlContainer)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (!(ctrl is UltraGrid))
                {
                    EventInfo eventInfo = ctrl.GetType().GetEvent("KeyDown");
                    if (eventInfo != null)
                    {
                        var handler = new KeyEventHandler(Control_KeyDown);
                        eventInfo.AddEventHandler(ctrl, handler);
                    }
                    if ((ctrl is UltraOptionSet_Ex) && TypeHaveAccountingObjectType.Any(p => p.Equals(TypeID)))
                    {
                        EventInfo eventValueChanged = ctrl.GetType().GetEvent("ValueChanged");
                        if (eventValueChanged != null)
                        {
                            var handler = new EventHandler(optAccountingObjectType_ValueChanged);
                            eventValueChanged.AddEventHandler(ctrl, handler);
                        }
                        EventInfo eventLeave = ctrl.GetType().GetEvent("Leave");
                        if (eventLeave != null)
                        {
                            var handler = new EventHandler(optAccountingObjectType_Leave);
                            eventLeave.AddEventHandler(ctrl, handler);
                        }
                        EventInfo eventClick = ctrl.GetType().GetEvent("Click");
                        if (eventClick != null)
                        {
                            var handler = new EventHandler(optAccountingObjectType_Click);
                            eventClick.AddEventHandler(ctrl, handler);
                        }
                    }
                    if (ctrl.HasChildren)
                        ConfigControlsForm(ctrl);
                }
                else
                {
                    UltraGrid ultraGrid = ctrl as UltraGrid;
                    if (ultraGrid != null && ultraGrid.DisplayLayout.Bands[0].Columns.Exists("OrderPriority"))
                        ultraGrid.DisplayLayout.Bands[0].Columns["OrderPriority"].SortIndicator = SortIndicator.Ascending;
                }
            }
        }

        private int? _optionCheckedIndex = null;
        private bool _isClickOption = false;
        private void optAccountingObjectType_ValueChanged(object sender, EventArgs e)
        {
            _cleaned = false;
            var optAccountingObjectType = (UltraOptionSet_Ex)sender;
            if (optAccountingObjectType.CheckedIndex.Equals(_optionCheckedIndex)) return;
            _optionCheckedIndex = optAccountingObjectType.CheckedIndex;
            if (_isClickOption)
            {
                ClearComboAccObj();
            }
            GetDataComboByOption(optAccountingObjectType);
            if (_cbbAccountingObject != null)
                if (_cbbAccountingObject.IsDroppedDown) _cbbAccountingObject.ToggleDropdown();
            optAccountingObjectType.Focus();
        }

        private void ClearComboAccObj()
        {
            PropertyInfo propAccObjId = _select.GetType().GetProperty("AccountingObjectID");
            if (propAccObjId != null && propAccObjId.CanWrite)
            {
                if (propAccObjId.GetValue(_select, null) != null)
                {
                    propAccObjId.SetValue(_select, null, null);
                    if (_cbbAccountingObject != null)
                    {
                        _cbbAccountingObject.Refresh();
                        _cbbAccountingObject.Focus();
                    }
                }
            }
            PropertyInfo propAccObjName = _select.GetType().GetProperty("AccountingObjectName");
            if (propAccObjName != null && propAccObjName.CanWrite)
            {
                if (propAccObjName.GetValue(_select, null) != null && !string.IsNullOrEmpty(propAccObjName.GetValue(_select, null).ToString()))
                {
                    propAccObjName.SetValue(_select, string.Empty, null);
                    foreach (var textEditor in GetTextAccountingByDataBindingMember(this, "AccountingObjectName"))
                    {
                        textEditor.Value = string.Empty;
                    }
                }
            }
            PropertyInfo propAccObjAddress = _select.GetType().GetProperty("AccountingObjectAddress");
            if (propAccObjAddress != null && propAccObjAddress.CanWrite)
            {
                if (propAccObjAddress.GetValue(_select, null) != null && !string.IsNullOrEmpty(propAccObjAddress.GetValue(_select, null).ToString()))
                {
                    propAccObjAddress.SetValue(_select, string.Empty, null);
                    foreach (var textEditor in GetTextAccountingByDataBindingMember(this, "AccountingObjectAddress"))
                    {
                        textEditor.Value = string.Empty;
                    }
                }
            }
            PropertyInfo propReceiver = _select.GetType().GetProperty("Receiver");
            if (propReceiver != null && propReceiver.CanWrite)
            {
                if (propReceiver.GetValue(_select, null) != null && !string.IsNullOrEmpty(propReceiver.GetValue(_select, null).ToString()))
                {
                    propReceiver.SetValue(_select, string.Empty, null);
                    foreach (UltraTextEditor textEditor in GetTextAccountingByDataBindingMember(this, "Receiver"))
                    {
                        textEditor.Value = string.Empty;
                    }
                }
            }
            _cleaned = true;
        }

        private IEnumerable<UltraTextEditor> GetTextAccountingByDataBindingMember(Control ctrlContainer, string memberName)
        {
            var list = new List<UltraTextEditor>();
            if (!(ctrlContainer is UltraGrid))
            {
                foreach (Control control in ctrlContainer.Controls)
                {
                    if (control is UltraTextEditor)
                    {
                        var txtEditor = (UltraTextEditor)control;
                        if (txtEditor.DataBindings != null && txtEditor.DataBindings.Count > 0 && txtEditor.DataBindings[0].BindingMemberInfo.BindingMember.Equals(memberName) && ((txtEditor.DataBindings[0].DataSource is T) || ((txtEditor.DataBindings[0].DataSource is BindingSource) && (((BindingSource)txtEditor.DataBindings[0].DataSource).DataSource is T))))
                        {
                            list.Add(txtEditor);
                        }
                    }
                    else if (control.HasChildren)
                        list.AddRange(GetTextAccountingByDataBindingMember(control, memberName));
                }
            }
            return list;
        }

        protected void GetDataComboByOption(UltraOptionSet_Ex optAccountingObjectType)
        {
            foreach (UltraCombo ultraCombo in FindComboAccountingObject(this))
            {
                if (ultraCombo != null)
                {
                    ultraCombo.DataSource = Utils.ListAccountingObject;
                    foreach (UltraGridRow row in ultraCombo.Rows)
                    {
                        row.Hidden = false;
                        row.Tag = null;
                    }
                    ultraCombo.ConfigComboAccouting(optAccountingObjectType.CheckedIndex);
                }
            }
        }

        IEnumerable<UltraCombo> FindComboAccountingObject(Control ctrlContainer)
        {
            var lstCombo = new List<UltraCombo>();
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (!(ctrl is UltraGrid))
                {
                    if (ctrl is UltraCombo)
                    {
                        var combo = (UltraCombo)ctrl;
                        if (!string.IsNullOrEmpty(combo.Name) && combo.DataSource != null && combo.DataSource.GetType().FullName.GetObjectNameFromBindingName().Equals(typeof(AccountingObject).Name))
                        {
                            lstCombo.Add(combo);
                        }
                    }
                    else if (ctrl.HasChildren)
                        lstCombo.AddRange(FindComboAccountingObject(ctrl));
                }
            }
            return lstCombo;
        }

        private void optAccountingObjectType_Leave(object sender, EventArgs e)
        {
            _isClickOption = false;
        }

        private void optAccountingObjectType_Click(object sender, EventArgs e)
        {
            _isClickOption = true;
        }

        public void Control_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    SendKeys.Send("{TAB}");
            //}

            //Tự động thay đổi CheckedIndex khi sử dụng các phím mũi tên
            if (sender is UltraOptionSet_Ex)
            {
                var opSet = (UltraOptionSet_Ex)sender;
                if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Down || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up)
                {
                    _isClickOption = true;
                    opSet.CheckedIndex = opSet.FocusedIndex;
                }
            }
        }

        private void ConfigToolBarButtons(string resultStr)
        {

            switch (resultStr)
            {
                #region Luồng có tính năng ghi sổ

                #region Hiển thị

                #region Chứng từ thông thường

                #region Đang ở trạng thái ghi sổ
                //1.1.Nằm ở vị trí đầu danh sách
                case "1-1-1-1-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //2. Nằm ở vị trí giữa danh sách
                case "1-1-1-1-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3. Nằm ở vị trí cuối danh sách
                case "1-1-1-1-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //4. Danh sách chỉ có một đối tượng
                case "1-1-1-1-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #region Đang ở trạng thái chưa ghi sổ
                //1. Nằm ở vị trí đầu danh sách
                case "1-1-1-2-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //2.Nằm ở vị trí giữa danh sách
                case "1-1-1-2-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3. Nằm ở vị trí cuối danh sách
                case "1-1-1-2-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //4. Danh sách chỉ có một đối tượng
                case "1-1-1-2-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #endregion

                #region Chứng từ nghiệp vụ

                #region Đang ở trạng thái ghi sổ
                //1. Nằm ở đầu danh sách
                case "1-1-2-1-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //2. Nằm ở giữa danh sách
                case "1-1-2-1-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3. Nằm ở cuối danh sách
                case "1-1-2-1-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    ////Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //4. Danh sách chỉ có một đối tượng
                case "1-1-2-1-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #region Đang ở trạng thái chưa ghi sổ
                //1. Nằm ở đầu danh sách
                case "1-1-2-2-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //2. Nằm ở giữa danh sách
                case "1-1-2-2-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3. Nằm ở cuối danh sách
                case "1-1-2-2-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //4. Danh sách chỉ có một đối tượng
                case "1-1-2-2-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #endregion

                #region Chứng từ không có tính năng ghi sổ
                //1.1.Nằm ở vị trí đầu danh sách
                case "1-1-3-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //2. Nằm ở vị trí giữa danh sách
                case "1-1-3-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3. Nằm ở vị trí cuối danh sách
                case "1-1-3-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //4. Danh sách chỉ có một đối tượng
                case "1-1-3-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #region Chứng từ chỉ có tính năng xem
                //1.1.Nằm ở vị trí đầu danh sách
                case "1-1-4-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 0);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //1.2.Nằm ở giữa danh sách
                case "1-1-4-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 0);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //1.3.Nằm ở cuối danh sách
                case "1-1-4-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 0);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //1.4.Nằm một mình
                case "1-1-4-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 0);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #endregion

                #region Thêm mới
                //1.Chứng từ thông thường + 2.Chứng từ nghiệp vụ
                case "1-2-1":
                case "1-2-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3.Chứng từ không có tính năng ghi sổ
                case "1-2-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #region Sửa
                //1.Chứng từ thông thường + 2.Chứng từ nghiệp vụ
                case "1-3-1":
                case "1-3-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //3.Chứng từ không có tính năng ghi sổ
                case "1-3-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #endregion

                #region Luồng không có tính năng ghi sổ

                #region Hiển thị
                case "2-1-1-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                case "2-1-1-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                case "2-1-1-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                case "2-1-1-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                //2.Chứng từ nghiệp vụ
                case "2-1-2-1":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                case "2-1-2-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                case "2-1-2-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                case "2-1-2-4":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #region Thêm mới
                //2.Thêm mới(Dùng chung)
                case "2-2":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
                #endregion

                #region Sửa

                case "2-3":
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;

                #endregion

                #endregion

                default:
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnForward", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnEdit", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnSave", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 2);
                    //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopUtilities", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 2);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnHelp", 0);
                    Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnClose", 2);
                    break;
            }
            if (TypeID == 560 || TypeID == 326 || TypeID == 435)
            {
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
            }
            if (TypeID == 680)
            {
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPost", 0);
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnUnPost", 0);
                //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnComeBack", 0);
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnpopTemplate", 0);
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnViewList", 0);
                Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 0);
                utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs[1].Visible = false;
            }
            Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnReset", 0);
            if (!Authenticate.Permissions("PR", SubSystemCode)) Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnPrint", 1);
        }


        public void ComeBackFunction()
        {
            //Trở về trạng thái trước đó, nếu đang sửa thì quay về trạng thái xem chứng từ đó, nếu đang thêm thì reset toàn bộ hoặc quay về chứng từ trước đó
            //if (_statusForm == ConstFrm.optStatusForm.Edit)
            //{//Nếu đang SỬA thì quay về trạng thái XEM của chứng từ hiện tại
            //    _statusForm = ConstFrm.optStatusForm.View;
            //}
            //else if (_statusForm == ConstFrm.optStatusForm.Add)
            //{
            //    //Nếu đang THÊM thì quay về chứng từ trước đó, nếu chứng từ trước đó không có thì để TRẮNG hoặc thoát??
            //    //int indexActive = _dsMcReceipt.IndexOf(_select);    //vị trí của chứng từ hiện tại

            //    //==> tạm thời để trắng
            //    //ResetForm
            //    ResetControlForm();
            //    //Vô hiệu hóa Control
            //    ChangeStatusControl(false);
            //    _statusForm = ConstFrm.optStatusForm.View;
            //}
            //ReloadToolbar(_select, _listSelects, _statusForm);
            this.Close();
        }

        public void PostFunction()
        {
            if (!Authenticate.Permissions("GH", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            if (_statusForm != ConstFrm.optStatusForm.View) return;
            if (!this.CheckExistVoucher(_select))
            {
                Close();
            }
            if (new int[] { 173, 133, 143, 126, 116 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPService))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var a = IPPServiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(a.TypeID);
                    if (a != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + a.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 172, 129, 132, 142, 119 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(FAIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var b = IFAIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(b.TypeID);
                    if (b != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + b.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 906, 903, 904, 905, 902 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(TIIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var c = ITIIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(c.TypeID);
                    if (c != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + c.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 171, 127, 131, 141, 117, 402, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var d = IPPInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(d.TypeID);
                    if (d != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + d.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 162, 163, 102, 103, 412, 415, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var e = ISAInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(e.TypeID);
                    if (e != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + e.No + " tại chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 403 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var f = ISAReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(f.TypeID);
                    if (f != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + f.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 413 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPDiscountReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var g = IPPDiscountReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(g.TypeID);
                    if (g != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + g.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }

            }

            if (!Saveleged())
            {
                var lstIdProperty = new List<string> { "No", "OutwardNo", "IWNo", "InwardNo" };
                for (int i = 0; i < lstIdProperty.Count; i++)
                {
                    var key = lstIdProperty[i];
                    if (new[] { "InwardNo" }.Any(x => x == key) && _select.HasProperty("StoredInRepository") && !_select.GetProperty<T, bool>("StoredInRepository")) continue;
                    if (!_select.HasProperty(key)) continue;
                    string no = _select.GetProperty<T, string>(key);
                    MSG.Warning(string.IsNullOrEmpty(no) ? resSystem.MSG_System_72 : string.Format(resSystem.MSG_System_71, no));
                    return;
                }
                MSG.Warning(resSystem.MSG_System_72);
                return;
            }
            else
            {
                TDTGia_NgayLapCTu(_select);
            }
            ReloadToolbar(_select, _listSelects, _statusForm);
            // set lại ugrid control về trạng thái lúc đầu, thêm vào để sửa bug ghi sổ, bỏ ghi sổ , lùi, tiến, grid dưỡi cùng bị hiện thêm hóa đơn
            SetLaiUgridControl(_select);
            IsClose = true;
        }
        public void TDTGia_NgayLapCTu<T>(T input)
        {
            string VTHH_PPTinhGiaXKho = _ISystemOptionService.GetByCode("VTHH_PPTinhGiaXKho").Data;
            string VTHH_TDTGia_NgayLapCTu = _ISystemOptionService.GetByCode("VTHH_TDTGia_NgayLapCTu").Data;
            string tinhRiengTungKho = Utils.ListSystemOption.FirstOrDefault(x => x.ID == 47).Data;
            if (VTHH_TDTGia_NgayLapCTu == "1")
            {
                switch (input.GetType().ToString())
                {
                    #region SAInvoice
                    case "Accounting.Core.Domain.SAInvoice":
                        SAInvoice saInvoice = (SAInvoice)(object)input;
                        List<MaterialGoods> lstmtg = new List<MaterialGoods>();
                        List<Guid> lstRepositoryID = new List<Guid>();
                        foreach (var x in saInvoice.SAInvoiceDetails)
                        {
                            var md = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == x.MaterialGoodsID);
                            lstmtg.Add(md);
                            lstRepositoryID.Add(x.RepositoryID ?? Guid.NewGuid());
                        }
                        if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                        {
                            _IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(lstmtg, saInvoice.PostedDate, saInvoice.PostedDate);
                        }
                        else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                        {
                            if (tinhRiengTungKho == "1")
                            {
                                foreach (var repositoryID in lstRepositoryID)
                                {
                                    _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg, saInvoice.PostedDate, saInvoice.PostedDate, repositoryID);
                                }
                            }
                            else
                            {
                                _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg, saInvoice.PostedDate, saInvoice.PostedDate);

                            }
                        }
                        break;
                    #endregion

                    #region RSInwardOutward
                    case "Accounting.Core.Domain.RSInwardOutward":
                        RSInwardOutward rsInwardOutward = (RSInwardOutward)(object)input;
                        if (rsInwardOutward.TypeID.ToString().StartsWith("41"))
                        {
                            List<MaterialGoods> lstmtg1 = new List<MaterialGoods>();
                            List<Guid> lstRepositoryID1 = new List<Guid>();
                            foreach (var x in rsInwardOutward.RSInwardOutwardDetails)
                            {
                                lstmtg1.Add(Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == x.MaterialGoodsID));
                                lstRepositoryID1.Add(x.RepositoryID ?? Guid.NewGuid());
                            }
                            if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                                _IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(lstmtg1, rsInwardOutward.PostedDate, rsInwardOutward.PostedDate);
                            else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                            {
                                if (tinhRiengTungKho == "1")
                                {
                                    foreach (var repositoryID in lstRepositoryID1)
                                    {
                                        _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg1, rsInwardOutward.PostedDate, rsInwardOutward.PostedDate, repositoryID);
                                    }
                                }
                                else
                                {
                                    _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg1, rsInwardOutward.PostedDate, rsInwardOutward.PostedDate);
                                }
                            }
                        }
                        break;
                    #endregion

                    #region RSTransfer
                    case "Accounting.Core.Domain.RSTransfer":
                        RSTransfer rsTransfer = (RSTransfer)(object)input;
                        List<MaterialGoods> lstmtg2 = new List<MaterialGoods>();
                        List<Guid> lstRepositoryID2 = new List<Guid>();
                        foreach (var x in rsTransfer.RSTransferDetails)
                        {
                            var md = Utils.ListMaterialGoods.FirstOrDefault(c => c.ID == x.MaterialGoodsID);
                            lstmtg2.Add(md);
                            lstRepositoryID2.Add(x.FromRepositoryID ?? Guid.NewGuid());
                        }
                        if (VTHH_PPTinhGiaXKho == "Bình quân tức thời")
                            _IRepositoryLedgerService.PricingAndUpdateOW_AverageForInstantaneous(lstmtg2, rsTransfer.PostedDate, rsTransfer.PostedDate);
                        else if (VTHH_PPTinhGiaXKho == "Nhập trước xuất trước")
                        {
                            if (tinhRiengTungKho == "1")
                            {
                                foreach (var repositoryID in lstRepositoryID2)
                                {
                                    _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg2, rsTransfer.PostedDate, rsTransfer.PostedDate, repositoryID);
                                }
                            }
                            else
                            {
                                _IRepositoryLedgerService.PricingAndUpdateOW_InFirstOutFirst(lstmtg2, rsTransfer.PostedDate, rsTransfer.PostedDate);
                            }
                        }
                        break;
                        #endregion
                }
            }
        }
        public void UnPostFunction()
        {
            if (!Authenticate.Permissions("BO", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            if (_statusForm != ConstFrm.optStatusForm.View) return;
            if (!this.CheckExistVoucher(_select))
            {
                Close();
            }
            Guid iD_ = _select.GetProperty<T, Guid>("ID");
            if (iD_ != null)
            {
                if (Utils.IGVoucherListDetailService.Query.Any(n => n.VoucherID == iD_))
                {
                    MSG.Warning("Bỏ ghi sổ thất bại! Chứng từ đã được lập chứng từ ghi sổ, vui lòng kiểm tra lại");
                    return;
                }
                else
                if (Utils.ListExceptVoucher.Any(n => n.GLVoucherID == iD_ || n.GLVoucherExceptID == iD_))
                {
                    MSG.Warning("Bỏ ghi sổ thất bại! Chứng từ đã được đối trừ, vui lòng bỏ đối trừ rồi thử lại!");
                    return;
                }
            }
            if (typeof(T) == typeof(MBCreditCard) || typeof(T) == typeof(MBTellerPaper) || typeof(T) == typeof(MBDeposit))
            {
                bool IsMatch = _select.GetProperty<T, bool>("IsMatch");
                if (IsMatch)
                {
                    MSG.Warning("Bỏ ghi sổ thất bại! chứng từ đã đối chiếu ngân hàng, vui lòng kiểm tra lại!");
                    return;
                }

            }
            if (new int[] { 173, 133, 143, 126, 116 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPService))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var a = IPPServiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(a.TypeID);
                    if (a != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + a.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 172, 129, 132, 142, 119 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(FAIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var b = IFAIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(b.TypeID);
                    if (b != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + b.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 906, 903, 904, 905, 902 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(TIIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var c = ITIIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(c.TypeID);
                    if (c != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + c.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 171, 127, 131, 141, 117, 402, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var d = IPPInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(d.TypeID);
                    if (d != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + d.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 162, 163, 102, 103, 412, 415, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var e = ISAInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(e.TypeID);
                    if (e != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + e.No + " tại chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 403 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var f = ISAReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(f.TypeID);
                    if (f != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + f.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 413 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPDiscountReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var g = IPPDiscountReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(g.TypeID);
                    if (g != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + g.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }

            }
            if (!Removeleged())
            {
                var lstIdProperty = new List<string> { "No", "OutwardNo", "IWNo", "InwardNo" };
                for (int i = 0; i < lstIdProperty.Count; i++)
                {
                    var key = lstIdProperty[i];
                    if (new[] { "InwardNo" }.Any(x => x == key) && _select.HasProperty("StoredInRepository") && !_select.GetProperty<T, bool>("StoredInRepository")) continue;
                    if (!_select.HasProperty(key)) continue;
                    string no = _select.GetProperty<T, string>(key);
                    MSG.Warning(string.IsNullOrEmpty(no) ? resSystem.MSG_System_74 : string.Format(resSystem.MSG_System_73, no));
                    return;
                }
                MSG.Warning(resSystem.MSG_System_74);
                return;
            }
            //Config Menu
            ReloadToolbar(_select, _listSelects, _statusForm);
            Utils.ClearCacheByType<RepositoryLedger>();
            // set lại ugrid control về trạng thái lúc đầu, thêm vào để sửa bug ghi sổ, bỏ ghi sổ , lùi, tiến, grid dưỡi cùng bị hiện thêm hóa đơn
            SetLaiUgridControl(_select);
            IsClose = true;
        }


        protected void CloseFunction()
        {
            //add by cuongpv
            if (!_statusForm.Equals(ConstFrm.optStatusForm.View) && !_statusForm.Equals(ConstFrm.optStatusForm.Delete)
                    && ((!Utils.Compare(_backSelect, _select) || (_backListObjectInput != null && _backListObjectInput.Count > 0 && !Utils.Compare(_backListObjectInput, _listObjectInput))
                    || (_backListObjectInputPost != null && _backListObjectInputPost.Count > 0 && !Utils.Compare(_backListObjectInputPost, _listObjectInputPost))
                    || (_backListObjectInputGroup != null && _backListObjectInputGroup.Count > 0 && !Utils.Compare(_backListObjectInputGroup, _listObjectInputGroup)))))
            {
                IsClosing = true;
                bool status = true;
                switch (MSG.MessageBoxStand(resSystem.MSG_System_45, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning))
                {
                    case DialogResult.Yes:
                        if (_statusForm != ConstFrm.optStatusForm.View)
                            SendKeys.Send("{TAB}");
                        status = SaveFunction();
                        break;
                    case DialogResult.No:
                        if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))
                            IsClose = true;
                        IsCloseByButton = true;//add by cuongpv
                        this.Close();//add by cuongpv
                        break;
                    case DialogResult.Cancel:
                        if (_statusForm != ConstFrm.optStatusForm.View)
                            //SendKeys.Send("{TAB}");
                            status = false;
                        break;
                    default:
                        break;
                }
                IsClosing = false;

                Utils.isPPInvoice = false;
                Utils.isSAInvoice = false;
            }
            else
            {
                Utils.isPPInvoice = false;
                Utils.isSAInvoice = false;
                IsCloseByButton = true;//add by cuongpv
                this.Close();//add by cuongpv
            }
            //end add by cuongpv
            //this.Close();//commnet by cuongpv
        }

        IList<T> GetIsActive(IEnumerable<T> input)
        {
            IList<T> lstTemp = new List<T>();
            foreach (var item in input)
            {
                PropertyInfo propInfo = item.GetType().GetProperty("IsActive");
                if (propInfo != null && propInfo.CanWrite)
                {
                    var changeType = Convert.ChangeType(propInfo.GetValue(item, null), propInfo.PropertyType);
                    bool isActive = changeType != null && (bool)changeType;
                    if (isActive)
                        lstTemp.Add(item);
                }
                else
                {
                    lstTemp.Add(item);
                }
            }
            return lstTemp;
        }

        protected void ViewListFunction()
        {
            try
            {
                if (_listSelects.IndexOf(_select) < 0) _listSelects.Add(_select);
                var frm = new FBrowser<T>(_listSelects, TypeID, _select);
                frm.FormClosed += new FormClosedEventHandler(frmBrowser_FormClosed);
                frm.Show(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        void frmBrowser_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FBrowser<T>)sender;
            if (e.CloseReason == CloseReason.UserClosing)
                if (frm.DialogResult == DialogResult.OK)
                {
                    if (frm.select != null)
                    {
                        _select = frm.select;
                        InitializeGUI(_select);
                        //Config Menu
                        ReloadToolbar(_select, _listSelects, _statusForm);
                    }
                }
        }

        protected void UtilitiesFunction()
        {
            MessageBox.Show(@"UNDER CONSTRUCTION!");
        }

        protected void TemplateFunction()
        {
            if (!Authenticate.Permissions("ELP", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            try
            {
                //var typeID= from v in _select 
                if (TypeID.Equals(0)) return;
                var fTemplate = new FTemplate(TypeID, _templateID);
                fTemplate.FormClosed += new FormClosedEventHandler(FTemplate_FormClosed);
                fTemplate.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FTemplate_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FTemplate)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                this.ReloadTemplate<T>(frm.TemplateID);
            }
            this.ConfigTemplateEdit<T>(utmDetailBaseToolBar, TypeID, _statusForm, _templateID);
        }

        protected void PrintFunction()
        {
            if (!Authenticate.Permissions("PR", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            ((PopupMenuTool)utmDetailBaseToolBar.Ribbon.Tabs[0].Groups[1].Tools["mnbtnPrint"]).DropDown();
        }
        public void Print(string code)
        {

            IVoucherPatternsReportService vprSrv = IoC.Resolve<IVoucherPatternsReportService>();
            VoucherPatternsReport vpr = vprSrv.Query.ToList().FirstOrDefault(v => v.VoucherPatternsCode == code);
            if (vpr == null) return;
            string path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
            string filePath = string.Format("{0}\\Frm\\FPrint\\Template\\{1}", path, vpr.FilePath);

            PerpetuumSoft.Reporting.Components.ReportManager rm = new PerpetuumSoft.Reporting.Components.ReportManager();
            PerpetuumSoft.Reporting.Components.FileReportSlot frs = new PerpetuumSoft.Reporting.Components.FileReportSlot();
            rm.Reports.Add(frs);
            frs.FilePath = filePath;
            ReportForm<int> f = new ReportForm<int>(frs, SubSystemCode);

            // các trường hợp này sử dụng dữ liệu _selectJoin,
            //int[] arrTypeJoin = { 102, 103, 116, 117, 119, 127, 129, 131, 132, 133, 141, 142, 143 };
            if (_selectJoin != null && (!((vpr.FilePath == "FAIncrement-ChungTuTSCD.rst" || vpr.FilePath == "FAIncrement-ChungTuTSCD-2.rst") && _select is FAIncrement) && !((vpr.FilePath == "TIIncrement-ChungTuKT.rst" || vpr.FilePath == "TIIncrement-ChungTuKT-2.rst") && _select is TIIncrement)
                && !(((vpr.FilePath == "MCPayment-02-TT-TSCD.rst" || vpr.FilePath == "MCPayment-02-TT-Double-TSCD.rst" || vpr.FilePath == "MCPayment-02-TT-A5-TSCD.rst") && typeof(T) == typeof(FAIncrement)) ||
                ((vpr.FilePath == "MCPayment-02-TT-CCDC.rst" || vpr.FilePath == "MCPayment-02-TT-Double-CCDC.rst" || vpr.FilePath == "MCPayment-02-TT-A5-CCDC.rst") && typeof(T) == typeof(TIIncrement)))))   //arrTypeJoin.Contains(_typeID))
            {
                if (_selectJoin is PPInvoice)
                {
                    f.AddDatasource("Data", _selectJoin as PPInvoice);
                    Utils.IPPInvoiceService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as PPInvoice);
                }
                else if (_selectJoin is PPService)
                {
                    f.AddDatasource("Data", _selectJoin as PPService);
                    Utils.IPPServiceService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as PPService);
                }
                else if (_selectJoin is SAInvoice)
                {

                    f.AddDatasource("Data", _selectJoin as SAInvoice);
                    Utils.ISAInvoiceService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as SAInvoice);
                }
                else if (_selectJoin is SAReturn)
                {
                    f.AddDatasource("Data", _selectJoin as SAReturn);
                    Utils.ISAReturnService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as SAReturn);
                }
                else if (_selectJoin is PPDiscountReturn)
                {
                    f.AddDatasource("Data", _selectJoin as PPDiscountReturn);
                    Utils.IPPDiscountReturnService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as PPDiscountReturn);
                }
                else if (_selectJoin is FAIncrement)
                {
                    f.AddDatasource("Data", _selectJoin as FAIncrement);
                    Utils.IFAIncrementService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as FAIncrement);
                }
                else if (_selectJoin is MCPayment)
                {
                    f.AddDatasource("Data", _selectJoin as MCPayment);
                    Utils.IMCPaymentService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as MCPayment);
                }
                else if (_selectJoin is TIIncrement)
                {
                    f.AddDatasource("Data", _selectJoin as TIIncrement);
                    Utils.ITIIncrementService.UnbindSession(_selectJoin);
                    f.AddDatasource("Data", _selectJoin as TIIncrement);
                }
            }
            else if (_typeID == 520 && code.Contains("FADecrement-ChungTuTSCD"))
            {
                f.AddDatasource("Data", _select);
                FADecrement fADecrement = (FADecrement)(object)_select;
                List<FADecrementDetailPost> lst = new List<FADecrementDetailPost>();
                for (int i = 0; i < fADecrement.FADecrementDetails.Count; i++)
                {
                    lst.AddRange(fADecrement.FADecrementDetails[i].FADecrementDetailPosts);
                }
                f.AddDatasource("DataPost", lst);
            }
            else if (_typeID == 540)
            {

                FADepreciation FADepreciation = (FADepreciation)(object)_select;
                for (int i = 0; i < FADepreciation.FADepreciationDetails.Count; i++)
                {
                    var detail = FADepreciation.FADepreciationDetails[i];
                    var model = IGeneralLedgerService.GetLastFixedAssetLedger(detail.FixedAssetID, FADepreciation.PostedDate);
                    decimal AcDepreciationAmount = IFixedAssetLedgerService.GetTotalAcDepreciationAmount(detail.FixedAssetID ?? Guid.Empty, FADepreciation.PostedDate);
                    FAInit fAInit = IFAInitService.FindByFixedAssetID(detail.FixedAssetID ?? Guid.Empty);
                    FADepreciation.FADepreciationDetails[i].Amount = !FADepreciation.Recorded ? FADepreciation.FADepreciationDetails[i].MonthlyDepreciationAmountOriginal + AcDepreciationAmount : AcDepreciationAmount;
                    FADepreciation.FADepreciationDetails[i].AmountOriginal = (model.DepreciationAmount ?? 0) - FADepreciation.FADepreciationDetails[i].Amount;
                    FADepreciation.FADepreciationDetails[i].OrgPrice = model.DepreciationAmount ?? 0;
                }
                f.AddDatasource("Data", _select);
            }
            else if ((_typeID == 210 || _typeID == 260 || _typeID == 261 || _typeID == 262 || _typeID == 263 || _typeID == 264) && (vpr.FilePath == "PPInvoice-InwardStockaa.rst" || vpr.FilePath == "PPInvoice-InwardStockaa-SL-HD.rst"))
            {
                if (_select.GetProperty<T, bool>("StoredInRepository"))
                    f.AddDatasource("Data", _select);
                else
                {
                    MSG.Warning("Không thể in Phiếu Nhập Kho trong Mua Hàng Không qua kho");
                    return;
                }
            }
            else if (_typeID == 126 && code.Contains("MBTellerPaper-Voucher"))
            {
                foreach (var x in (_select as PPService).PPServiceDetails)
                {
                    var model = new MBTellerPaperDetail();
                    model.ID = x.ID;
                    model.Description = x.Description;
                    model.DebitAccount = x.DebitAccount;
                    model.CreditAccount = x.CreditAccount;
                    model.Amount = x.Amount ?? 0;
                    model.AmountOriginal = x.AmountOriginal ?? 0;
                    (_select as PPService).MBTellerPaperDetails.Add(model);
                }
                f.AddDatasource("Data", _select);
            }
            else if (_typeID == 128 && code.Contains("MBTellerPaper-Voucher"))
            {
                var x = (_select as MBTellerPaper).MBTellerPaperDetails.Sum(c => c.Amount);
                (_select as MBTellerPaper).TotalAll = x;
                (_select as MBTellerPaper).TotalAmount = x;
                f.AddDatasource("Data", _select);
            }
            else if (_typeID == 101 && code.Contains("MCPayment-Voucher"))
            {
                var x = (_select as MCPayment).MCPaymentDetails.Sum(c => c.Amount);
                (_select as MCPayment).TotalAll = x;
                (_select as MCPayment).TotalAmount = x;
                f.AddDatasource("Data", _select);
            }
            else if (_typeID == 174 && code.Contains("MBCredit-Voucher-TTBT"))
            {
                var x = (_select as MBCreditCard).MBCreditCardDetails.Sum(c => c.Amount);
                (_select as MBCreditCard).TotalAll = x;
                (_select as MBCreditCard).TotalAmount = x;
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "MBTransfer-CreditNote.rst" || vpr.FilePath == "MBInternalTransfer_GBN.rst")
            {
                var lst = (_select as MBInternalTransfer).MBInternalTransferDetails.Where(x => x.AmountOriginal > 0).ToList();
                (_select as MBInternalTransfer).MBInternalTransferDetails = lst;
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "MBDeposit-CreditNote.rst")
            {
                var lst = (_select as MBDeposit).MBDepositDetails.Where(x => x.AmountOriginal > 0).ToList();
                (_select as MBDeposit).MBDepositDetails = lst;
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "MBTellerPaper-DebitNote.rst")
            {
                var lst = (_select as MBTellerPaper).MBTellerPaperDetails.Where(x => x.AmountOriginal > 0).ToList();
                (_select as MBTellerPaper).MBTellerPaperDetails = lst;
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "BangLuong.rst")
            {
                var lst = (_select as PSSalarySheet).PSSalarySheetDetails.OrderBy(x => x.DepartmentCode).ThenBy(x => x.accountingObjectCode).ToList();
                foreach (var item in lst)
                {
                    item.AgreementSalary = Math.Round(item.AgreementSalary, MidpointRounding.AwayFromZero);
                    item.PaidWorkingDayAmount = Math.Round(item.PaidWorkingDayAmount, MidpointRounding.AwayFromZero);
                    item.OverTimeAmount = Math.Round(item.OverTimeAmount, MidpointRounding.AwayFromZero);
                    item.NonWorkingDayAmount = Math.Round(item.NonWorkingDayAmount, MidpointRounding.AwayFromZero);
                    item.PayrollFundAllowance = Math.Round(item.PayrollFundAllowance ?? 0, MidpointRounding.AwayFromZero);
                    item.EmployeeSocityInsuranceAmount = Math.Round(item.EmployeeSocityInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.EmployeeMedicalInsuranceAmount = Math.Round(item.EmployeeMedicalInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.EmployeeUnEmployeeInsuranceAmount = Math.Round(item.EmployeeUnEmployeeInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.EmployeeTradeUnionInsuranceAmount = Math.Round(item.EmployeeTradeUnionInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.IncomeTaxAmount = Math.Round(item.IncomeTaxAmount, MidpointRounding.AwayFromZero);
                    item.TemporaryAmount = Math.Round(item.TemporaryAmount, MidpointRounding.AwayFromZero);
                    item.NetAmount = Math.Round(item.NetAmount, MidpointRounding.AwayFromZero);

                }
                (_select as PSSalarySheet).PSSalarySheetDetails = lst;
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "BangLuong_TPB.rst")
            {
                var lst = (_select as PSSalarySheet).PSSalarySheetDetails.OrderBy(x => x.DepartmentCode).ThenBy(x => x.accountingObjectCode).ToList();
                foreach (var item in lst)
                {
                    item.AgreementSalary = Math.Round(item.AgreementSalary, MidpointRounding.AwayFromZero);
                    item.PaidWorkingDayAmount = Math.Round(item.PaidWorkingDayAmount, MidpointRounding.AwayFromZero);
                    item.OverTimeAmount = Math.Round(item.OverTimeAmount, MidpointRounding.AwayFromZero);
                    item.NonWorkingDayAmount = Math.Round(item.NonWorkingDayAmount, MidpointRounding.AwayFromZero);
                    item.PayrollFundAllowance = Math.Round(item.PayrollFundAllowance ?? 0, MidpointRounding.AwayFromZero);
                    item.EmployeeSocityInsuranceAmount = Math.Round(item.EmployeeSocityInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.EmployeeMedicalInsuranceAmount = Math.Round(item.EmployeeMedicalInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.EmployeeUnEmployeeInsuranceAmount = Math.Round(item.EmployeeUnEmployeeInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.EmployeeTradeUnionInsuranceAmount = Math.Round(item.EmployeeTradeUnionInsuranceAmount, MidpointRounding.AwayFromZero);
                    item.IncomeTaxAmount = Math.Round(item.IncomeTaxAmount, MidpointRounding.AwayFromZero);
                    item.TemporaryAmount = Math.Round(item.TemporaryAmount, MidpointRounding.AwayFromZero);
                    item.NetAmount = Math.Round(item.NetAmount, MidpointRounding.AwayFromZero);

                }
                (_select as PSSalarySheet).PSSalarySheetDetails = lst;
                f.AddDatasource("Data", _select);
            }
            //add by namnh
            else if (vpr.FilePath == "ChungTuGhiSoS02b-DNN.rst")
            {
                var lst = (from a in (_select as GVoucherList).GVoucherListDetails
                           group a by new { a.VoucherID }
                       into g
                           select new GVoucherListDetail
                           {
                               VoucherID = g.Key.VoucherID,
                               //AmountOriginal = g.Sum(x => x.AmountOriginal),
                           }).ToList();
                GVoucherList gVoucherList = new GVoucherList();
                int dem = lst.Count;
                if (vpr.VoucherPatternsCode == "GvoucherList-CTGSS02bDNN1")
                {
                    gVoucherList.dem = dem;
                    gVoucherList.No = (_select as GVoucherList).No;
                    gVoucherList.Date = (_select as GVoucherList).Date;
                    lst = (_select as GVoucherList).GVoucherListDetails.OrderBy(x => x.VoucherDate).ThenBy(x => x.VoucherNo).ThenBy(x => x.OrderPriority).ToList();
                    gVoucherList.GVoucherListDetails = lst.OrderBy(x => x.VoucherDate).ThenBy(x => x.VoucherNo).ThenBy(x => x.OrderPriority).ToList();
                    f.AddDatasource("Data", gVoucherList);
                }
                else if (vpr.VoucherPatternsCode == "GvoucherList-CTGSS02bDNN2")
                {

                    lst = (from a in (_select as GVoucherList).GVoucherListDetails
                           group a by new { a.VoucherID, a.VoucherDebitAccount, a.VoucherCreditAccount }
                       into g
                           select new GVoucherListDetail
                           {
                               VoucherID = g.Key.VoucherID,
                               Reason = g.Max(x => x.Reason),
                               VoucherDescription = g.Max(x => x.Reason),
                               VoucherDate = g.Max(x => x.VoucherDate),
                               VoucherDebitAccount = g.Key.VoucherDebitAccount,
                               VoucherCreditAccount = g.Key.VoucherCreditAccount,
                               VoucherAmount = g.Sum(x => x.VoucherAmount),
                               VoucherNo = g.Max(x => x.VoucherNo),
                               //AmountOriginal = g.Sum(x => x.AmountOriginal),
                           }).ToList();
                    gVoucherList = new GVoucherList();
                    gVoucherList.ID = (_select as GVoucherList).ID;
                    gVoucherList.No = (_select as GVoucherList).No;
                    gVoucherList.Date = (_select as GVoucherList).Date;
                    gVoucherList.dem = dem;
                    gVoucherList.GVoucherListDetails = lst.OrderBy(x => x.VoucherDate).ThenBy(x => x.VoucherNo).ThenBy(x => x.OrderPriority).ToList();

                    f.AddDatasource("Data", gVoucherList);

                }
                else if (vpr.VoucherPatternsCode == "GvoucherList-CTGSS02bDNN3")
                {
                    List<GVoucherListDetail> lst1 = new List<GVoucherListDetail>();
                    lst = (from a in (_select as GVoucherList).GVoucherListDetails
                           group a by new { a.VoucherDebitAccount, a.VoucherCreditAccount }
                       into g
                           select new GVoucherListDetail
                           {
                               VoucherDescription = "Chứng từ ghi sổ",
                               VoucherDebitAccount = g.Key.VoucherDebitAccount,
                               VoucherCreditAccount = g.Key.VoucherCreditAccount,
                               VoucherAmount = g.Sum(x => x.VoucherAmount),
                               VoucherNo = "",
                               //AmountOriginal = g.Sum(x => x.AmountOriginal),
                           }).ToList();
                    //(_select as GVoucherList).GVoucherListDetails = lst.OrderBy(x => x.VoucherDescription).ThenBy(x => x.VoucherDebitAccount).ToList();
                    gVoucherList = new GVoucherList();
                    gVoucherList.ID = (_select as GVoucherList).ID;
                    gVoucherList.No = (_select as GVoucherList).No;
                    gVoucherList.Date = (_select as GVoucherList).Date;
                    gVoucherList.dem = dem;
                    //var Count = new GVoucherList_Count();
                    //Count.dem = lst.Count;
                    gVoucherList.GVoucherListDetails = lst.OrderBy(x => x.VoucherDate).ThenBy(x => x.VoucherNo).ThenBy(x => x.OrderPriority).ToList();
                    f.AddDatasource("Data", gVoucherList);
                    //f.AddDatasource("Count", Count.dem);
                }
            }
            //end
            else if (vpr.FilePath == "TIAudit-ChungTuCCDC.rst")
            {
                var lst = (_select as TIAudit).TIAuditDetails.OrderBy(x => x.DepartmentCode).ThenBy(c => c.ToolCode).ToList();
                (_select as TIAudit).TIAuditDetails = lst;
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "ChungTuKeToan.rst" || vpr.FilePath == "GOtherVoucher-CT.rst")
            {
                if (_typeID == 620 || _typeID == 660)
                {
                    var lst = (from a in (_select as GOtherVoucher).GOtherVoucherDetails
                               group a by new { a.Description, a.DebitAccount, a.CreditAccount, a.OrderPriority }
                           into g
                               select new GOtherVoucherDetail
                               {
                                   Description = g.Key.Description,
                                   DebitAccount = g.Key.DebitAccount,
                                   CreditAccount = g.Key.CreditAccount,
                                   OrderPriority = g.Key.OrderPriority,
                                   AmountOriginal = g.Sum(x => x.AmountOriginal),
                                   Amount = g.Sum(x => x.Amount)
                               }).ToList();
                    (_select as GOtherVoucher).GOtherVoucherDetails = lst.OrderBy(x => x.Description).ThenBy(x => x.DebitAccount).ToList();
                    f.AddDatasource("Data", _select);
                }
                else
                {
                    var lst = (from a in (_select as GOtherVoucher).GOtherVoucherDetails
                               group a by new { a.Description, a.DebitAccount, a.CreditAccount, a.OrderPriority }
                              into g
                               select new GOtherVoucherDetail
                               {
                                   Description = g.Key.Description,
                                   DebitAccount = g.Key.DebitAccount,
                                   CreditAccount = g.Key.CreditAccount,
                                   AmountOriginal = g.Sum(x => x.AmountOriginal),
                                   Amount = g.Sum(x => x.Amount),
                                   OrderPriority = g.Key.OrderPriority
                               }).ToList();
                    (_select as GOtherVoucher).GOtherVoucherDetails = lst.OrderBy(n => n.OrderPriority).ToList();
                    f.AddDatasource("Data", _select);
                }
            }
            else if (vpr.FilePath == "BangTongHopChamCong.rst")
            {
                foreach (var x in (_select as PSTimeSheetSummary).PSTimeSheetSummaryDetails)
                {
                    x.DepartmentCode = Utils.ListDepartment.FirstOrDefault(c => c.ID == x.DepartmentID).DepartmentCode;
                    x.EmployeeCode = Utils.ListAccountingObject.FirstOrDefault(c => c.ID == x.EmployeeID).AccountingObjectCode;
                }
                (_select as PSTimeSheetSummary).PSTimeSheetSummaryDetails = (_select as PSTimeSheetSummary).PSTimeSheetSummaryDetails.OrderBy(x => x.DepartmentCode).ThenBy(x => x.EmployeeCode).ToList();
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "BangChamCong.rst")
            {
                foreach (var x in (_select as PSTimeSheet).PSTimeSheetDetails)
                {
                    x.DepartmentCode = Utils.ListDepartment.FirstOrDefault(c => c.ID == x.DepartmentID).DepartmentCode;
                    x.EmployeeCode = Utils.ListAccountingObject.FirstOrDefault(c => c.ID == x.EmployeeID).AccountingObjectCode;
                }
               (_select as PSTimeSheet).PSTimeSheetDetails = (_select as PSTimeSheet).PSTimeSheetDetails.OrderBy(x => x.DepartmentCode).ThenBy(x => x.EmployeeCode).ToList();
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "BangTinhBaoHiemXaHoi.rst")
            {
                (_select as PSSalarySheet).PSSalarySheetDetails = (_select as PSSalarySheet).PSSalarySheetDetails.Where(x => (x.EmployeeTradeUnionInsuranceAmount +
                x.CompanySocityInsuranceAmount +
                x.CompanyMedicalInsuranceAmount +
                x.CompanyUnEmployeeInsuranceAmount +
                x.CompanytAccidentInsuranceAmount +
                x.CompanyTradeUnionInsuranceAmount +
                x.EmployeeSocityInsuranceAmount +
                x.EmployeeMedicalInsuranceAmount +
                x.EmployeeUnEmployeeInsuranceAmount +
                x.EmployeeAccidentInsuranceAmount +
                x.EmployeeTradeUnionInsuranceAmount) != 0
                ).OrderBy(x => (x.AccountingObjectName.Split()[x.AccountingObjectName.Split().Length - 1])).ThenBy(x => x.DepartmentCode).ToList();
                f.AddDatasource("Data", _select);
            }
            else if (vpr.FilePath == "SAReturn-PNK.rst" || vpr.FilePath == "SAReturn-PNK-SL-HD.rst")
            {
                if ((_select as SAReturn).IsDeliveryVoucher == false)
                {
                    MSG.Warning("Đây là chứng từ bán hàng không kiêm phiếu nhập kho.Vui lòng kiểm tra lại!");
                    return;
                }
                else
                    f.AddDatasource("Data", _select);
            }
            else
            {
                f.AddDatasource("Data", _select);
            }


            switch (code)
            {
                case "UNC_Agribank":
                case "UNC_BIDV":
                case "UNC_Techcombank":
                case "UNC_Vietinbank":
                case "UNC_Vietcombank":
                    ReportProcedureSDS sp = new ReportProcedureSDS();
                    List<MBTellerPaperReport> mbTellerPaperReport = new List<MBTellerPaperReport>();
                    if (code == "UNC_Agribank")
                        mbTellerPaperReport = sp.GetAccreditative("Agribank");
                    else if (code == "UNC_BIDV")
                        mbTellerPaperReport = sp.GetAccreditative("BIDV");
                    else if (code == "UNC_Techcombank")
                        mbTellerPaperReport = sp.GetAccreditative("Techcombank");
                    else if (code == "UNC_Vietinbank")
                        mbTellerPaperReport = sp.GetAccreditative("Vietinbank");
                    else
                        mbTellerPaperReport = sp.GetAccreditative("Vietcombank");
                    if (mbTellerPaperReport.Where(n => n.ID == (Guid)_select.GetProperty("ID")).ToList().Count > 0)
                    {
                        if (_select.HasProperty("ID"))
                            f.AddDatasource("MBTellerPaper", mbTellerPaperReport.FirstOrDefault(n => n.ID == (Guid)_select.GetProperty("ID")));
                    }
                    else
                    {
                        MSG.Information("Không có dữ liệu.");
                        return;
                    }

                    break;
                case "FA_UNC_Agribank":
                case "FA_UNC_BIDV":
                case "FA_UNC_Techcombank":
                case "FA_UNC_Vietinbank":
                case "FA_UNC_Vietcombank":
                    ReportProcedureSDS sp1 = new ReportProcedureSDS();
                    List<FAIncrementReport> faIncrementReport = new List<FAIncrementReport>();
                    if (code == "FA_UNC_Agribank")
                        faIncrementReport = sp1.GetAccreditative1("Agribank");
                    else if (code == "FA_UNC_BIDV")
                        faIncrementReport = sp1.GetAccreditative1("BIDV");
                    else if (code == "FA_UNC_Techcombank")
                        faIncrementReport = sp1.GetAccreditative1("Techcombank");
                    else if (code == "FA_UNC_Vietinbank")
                        faIncrementReport = sp1.GetAccreditative1("Vietinbank");
                    else
                        faIncrementReport = sp1.GetAccreditative1("Vietcombank");
                    if (faIncrementReport.Where(n => n.ID == (Guid)_select.GetProperty("ID")).ToList().Count > 0)
                    {
                        if (_select.HasProperty("ID"))
                            f.AddDatasource("FAIncrement", faIncrementReport.Where(n => n.ID == (Guid)_select.GetProperty("ID")).ToList());
                    }
                    else
                    {
                        MSG.Information("Không có dữ liệu.");
                        return;
                    }

                    break;
                case "TI_UNC_Agribank":
                case "TI_UNC_BIDV":
                case "TI_UNC_Techcombank":
                case "TI_UNC_Vietinbank":
                case "TI_UNC_Vietcombank":
                    ReportProcedureSDS sp2 = new ReportProcedureSDS();
                    List<TIIncrementReport> tiIncrementReport = new List<TIIncrementReport>();
                    if (code == "TI_UNC_Agribank")
                        tiIncrementReport = sp2.GetAccreditative2("Agribank");
                    else if (code == "TI_UNC_BIDV")
                        tiIncrementReport = sp2.GetAccreditative2("BIDV");
                    else if (code == "TI_UNC_Techcombank")
                        tiIncrementReport = sp2.GetAccreditative2("Techcombank");
                    else if (code == "TI_UNC_Vietinbank")
                        tiIncrementReport = sp2.GetAccreditative2("Vietinbank");
                    else
                        tiIncrementReport = sp2.GetAccreditative2("Vietcombank");
                    if (tiIncrementReport.Where(n => n.ID == (Guid)_select.GetProperty("ID")).ToList().Count > 0)
                    {
                        if (_select.HasProperty("ID"))
                            f.AddDatasource("TIIncrement", tiIncrementReport.Where(n => n.ID == (Guid)_select.GetProperty("ID")).ToList());
                    }
                    else
                    {
                        MSG.Information("Không có dữ liệu.");
                        return;
                    }

                    break;
                case "FAAdjustment-ChungTuTSCD":
                    var FAAdjustmentDetails = typeof(T).GetProperty("FAAdjustmentDetails").GetValue(_select, null);
                    var lst = ((System.Collections.IEnumerable)FAAdjustmentDetails).Cast<FAAdjustmentDetail>().ToList();
                    string debit = string.Join(", ", lst.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit = string.Join(", ", lst.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit, CreditAccount = credit });
                    break;
                case "MCReceipt-01-TT":
                case "MCReceipt-01-TT-A5"://add by cuongpv
                case "MCReceipt-01-TT-Double":
                    var lstData01TT = typeof(T).GetProperty("MCReceiptDetails").GetValue(_select, null);
                    var list01TT = ((System.Collections.IEnumerable)lstData01TT).Cast<MCReceiptDetail>().ToList();
                    string debit01TT = string.Join(", ", list01TT.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit01TT = string.Join(", ", list01TT.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit01TT, CreditAccount = credit01TT });
                    break;
                case "MCReceipt-04-TT":
                    var lstData04TTReceipt = typeof(T).GetProperty("MCReceiptDetails").GetValue(_select, null);
                    var list04TTReceipt = ((System.Collections.IEnumerable)lstData04TTReceipt).Cast<MCReceiptDetail>().Where(c => c.CreditAccount.StartsWith("141")).ToList();
                    string debit04TTReceipt = string.Join(", ", list04TTReceipt.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit04TTReceipt = string.Join(", ", list04TTReceipt.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit04TTReceipt, CreditAccount = credit04TTReceipt });
                    break;
                case "MCPayment-02-TT":
                case "MCPayment-02-TT-A5"://add by cuongpv
                case "MCPayment-02-TT-Double":
                    if (_selectJoin is FAIncrement)
                    {
                        var lstData02TT = /*_select.GetProperty("FAIncrementDetails")*/(_selectJoin as FAIncrement).FAIncrementDetails;
                        var list02TT = ((System.Collections.IEnumerable)lstData02TT).Cast<FAIncrementDetail>().ToList();
                        string debit02TT = string.Join(", ", list02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                        string credit02TT = string.Join(", ", list02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                        f.AddDatasource("Detail", new { DebitAccount = debit02TT, CreditAccount = credit02TT });
                    }
                    else if (_selectJoin is TIIncrement)
                    {
                        var lstData02TT = (_selectJoin as TIIncrement).TIIncrementDetails;
                        var list02TT = ((System.Collections.IEnumerable)lstData02TT).Cast<TIIncrementDetail>().ToList();
                        string debit02TT = string.Join(", ", list02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                        if (list02TT.Any(n => !n.VATAccount.IsNullOrEmpty())) // Edit by Hautv
                            debit02TT += ", " + string.Join(", ", list02TT.Select(c => c.VATAccount).Distinct().ToArray());
                        string credit02TT = string.Join(", ", list02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                        f.AddDatasource("Detail", new { DebitAccount = debit02TT, CreditAccount = credit02TT });
                    }
                    else
                    {

                        if (_select is MCPayment)
                        {
                            var lstData02TT = typeof(MCPayment).GetProperty("MCPaymentDetails").GetValue((_selectJoin != null && _selectJoin is MCPayment) ? (_selectJoin as MCPayment) : (_select as MCPayment), null);
                            var list02TT = ((System.Collections.IEnumerable)lstData02TT).Cast<MCPaymentDetail>().ToList();
                            string debit02TT = string.Join(", ", list02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                            string credit02TT = string.Join(", ", list02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                            f.AddDatasource("Detail", new { DebitAccount = debit02TT, CreditAccount = credit02TT });
                        }
                        else if (_select is FAIncrement)
                        {
                            var list02TT = ((System.Collections.IEnumerable)_select.GetProperty("FAIncrementDetails")).Cast<FAIncrementDetail>().ToList();
                            string debit02TT = string.Join(", ", list02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                            string credit02TT = string.Join(", ", list02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                            f.AddDatasource("Detail", new { DebitAccount = debit02TT, CreditAccount = credit02TT });
                        }
                        else if (_select is TIIncrement)
                        {
                            var list02TT = ((System.Collections.IEnumerable)_select.GetProperty("TIIncrementDetails")).Cast<TIIncrementDetail>().ToList();
                            string debit02TT = string.Join(", ", list02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                            string credit02TT = string.Join(", ", list02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                            f.AddDatasource("Detail", new { DebitAccount = debit02TT, CreditAccount = credit02TT });
                        }
                    }
                    break;
                case "MCPayment-04-TT":
                    var lstData04TTPayment = typeof(T).GetProperty("MCPaymentDetails").GetValue(_select, null);
                    var list04TTPayment = ((System.Collections.IEnumerable)lstData04TTPayment).Cast<MCPaymentDetail>().Where(c => c.DebitAccount.StartsWith("141")).ToList();
                    string debit04TTPayment = string.Join(", ", list04TTPayment.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit04TTPayment = string.Join(", ", list04TTPayment.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit04TTPayment, CreditAccount = credit04TTPayment });
                    break;
                case "PPInvoice-InwardStock":
                    string key = "InwardNo";
                    bool IsInwardNo = _select.HasProperty(key) && !string.IsNullOrEmpty(_select.GetProperty<T, string>(key));
                    if (!IsInwardNo) return;
                    var lstDataPPInvoiceInwardStock = typeof(T).GetProperty("PPInvoiceDetails").GetValue(_select, null);
                    var lstPPInvoiceInwardStock = ((System.Collections.IEnumerable)lstDataPPInvoiceInwardStock).Cast<PPInvoiceDetail>().ToList();
                    string debitPPInvoiceInwardStock = string.Join(", ", lstPPInvoiceInwardStock.Select(c => c.DebitAccount).Distinct().ToArray());
                    string creditPPInvoiceInwardStock = string.Join(", ", lstPPInvoiceInwardStock.Select(c => c.CreditAccount).Distinct().ToArray());
                    string stockNamePPInvoiceInwardStock = string.Join(", ", lstPPInvoiceInwardStock.Select(c => c.Description).Distinct().ToArray());
                    f.AddDatasource("Detail", new
                    {
                        DebitAccount = debitPPInvoiceInwardStock,
                        CreditAccount = creditPPInvoiceInwardStock,
                        StockName = stockNamePPInvoiceInwardStock
                    });
                    break;
                case "NhapKho.PNK":
                case "NhapKho.PNK-SL-HD":
                    var lstData004TTPayment = typeof(T).GetProperty("RSInwardOutwardDetails").GetValue(_select, null);
                    var list004TTPayment = ((System.Collections.IEnumerable)lstData004TTPayment).Cast<RSInwardOutwardDetail>().ToList();
                    string debit004TTPayment = string.Join(", ", list004TTPayment.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit004TTPayment = string.Join(", ", list004TTPayment.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit004TTPayment, CreditAccount = credit004TTPayment });
                    break;
                //case "XuatKho.PXK":
                //    var lstData004TTPayments = typeof(T).GetProperty("RSInwardOutwardDetails").GetValue(_select, null);
                //    var list004TTPayments = ((System.Collections.IEnumerable)lstData004TTPayments).Cast<RSInwardOutwardDetail>().ToList();
                //    string debit004TTPayments = string.Join(", ", list004TTPayments.Select(c => c.DebitAccount).Distinct().ToArray());
                //    string credit004TTPayments = string.Join(", ", list004TTPayments.Select(c => c.CreditAccount).Distinct().ToArray());
                //    f.AddDatasource("Detail", new { DebitAccount = debit004TTPayments, CreditAccount = credit004TTPayments });
                //    break;
                case "XuatKho.PXK":
                case "XuatKho.PXK-A4":
                case "XuatKho.PXK-A4-SL-HD":
                    {
                        var temp = (_selectJoin != null && _selectJoin is RSInwardOutward) ? (_selectJoin as RSInwardOutward) : (_select as RSInwardOutward);
                        int rcount = code == "XuatKho.PXK-A4" || code == "XuatKho.PXK-A4-SL-HD" ? 25 : (code == "XuatKho.PXK" ? 3 : 10);
                        f.DefaultPaperSize = code == "XuatKho.PXK" ? ConstFrm.Paper.PAPER_SIZE_A5 : ConstFrm.Paper.PAPER_SIZE_A4;

                        var max = temp.RSInwardOutwardDetails.Count < rcount ? 0 : temp.RSInwardOutwardDetails.Count / rcount;
                        if (temp.RSInwardOutwardDetails.Count % rcount != 0) max = max + 1;
                        var source = new List<RSInwardOutward>();
                        for (int i = 1; i <= max; i++)
                        {
                            RSInwardOutward select1 = new RSInwardOutward();
                            temp.CopyTo(ref select1);
                            var query = from c in temp.RSInwardOutwardDetails orderby c.MaterialGoodsID ascending select c;
                            var list = query.Skip(rcount * (i - 1)).Take(rcount).ToList();
                            int h = rcount - list.Count;
                            select1.RSInwardOutwardDetails = list;
                            select1.TotalAmount = list.Sum(x => x.Amount);
                            select1.TotalAmountOriginal = list.Sum(x => x.AmountOriginal);
                            source.Add(select1);
                        }
                        f.AddDatasource("Data", source);

                        var lstData004TTPayments = typeof(RSInwardOutward).GetProperty("RSInwardOutwardDetails").GetValue((_selectJoin != null && _selectJoin is RSInwardOutward) ? (_selectJoin as RSInwardOutward) : (_select as RSInwardOutward), null);
                        var list004TTPayments = ((System.Collections.IEnumerable)lstData004TTPayments).Cast<RSInwardOutwardDetail>().ToList();
                        string debit004TTPayments = string.Join(", ", list004TTPayments.Select(c => c.DebitAccount).Distinct().ToArray());
                        string credit004TTPayments = string.Join(", ", list004TTPayments.Select(c => c.CreditAccount).Distinct().ToArray());
                        string repository = string.Join(", ", list004TTPayments.Select(c => Accounting.ReportUtils.GetRepositoryName(c.RepositoryID.ToString())).Distinct().ToArray());
                        f.AddDatasource("Detail", new { DebitAccount = debit004TTPayments, CreditAccount = credit004TTPayments, Repository = repository });
                    }
                    break;
                case "ChuyenKho.PXK":
                    var lstData004TTPaymentss = typeof(T).GetProperty("RSTransferDetails").GetValue(_select, null);
                    var list004TTPaymentss = ((System.Collections.IEnumerable)lstData004TTPaymentss).Cast<RSTransferDetail>().ToList();
                    string debit004TTPaymentss = string.Join(", ", list004TTPaymentss.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit004TTPaymentss = string.Join(", ", list004TTPaymentss.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit004TTPaymentss, CreditAccount = credit004TTPaymentss });
                    break;
                case "ChuyenKho.PNK":
                    var lstData004TTPaymentsss = typeof(T).GetProperty("RSTransferDetails").GetValue(_select, null);
                    var list004TTPaymentsss = ((System.Collections.IEnumerable)lstData004TTPaymentsss).Cast<RSTransferDetail>().ToList();

                    string debit004TTPaymentsss = string.Join(", ", list004TTPaymentsss.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit004TTPaymentsss = string.Join(", ", list004TTPaymentsss.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit004TTPaymentsss, CreditAccount = credit004TTPaymentsss });
                    break;
                case "PPInvoice-InwardStockaa":
                case "PPInvoice-InwardStockaa-SL-HD":
                    var lstData004TTPaymentssss = typeof(PPInvoice).GetProperty("PPInvoiceDetails").GetValue((_selectJoin != null && _selectJoin is PPInvoice) ? (_selectJoin as PPInvoice) : (_select as PPInvoice), null);
                    var list004TTPaymentssss = ((System.Collections.IEnumerable)lstData004TTPaymentssss).Cast<PPInvoiceDetail>().ToList();
                    string debit004TTPaymentssss = string.Join(", ", list004TTPaymentssss.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit004TTPaymentssss = string.Join(", ", list004TTPaymentssss.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit004TTPaymentssss, CreditAccount = credit004TTPaymentssss });
                    break;
                case "PPDiscountReturn.PT":
                case "PPDiscountReturn.PT-A5"://add by cuongpv
                    var lstData004TTPaymentsssss = typeof(T).GetProperty("PPDiscountReturnDetails").GetValue(_select, null);
                    var list004TTPaymentsssss = ((System.Collections.IEnumerable)lstData004TTPaymentsssss).Cast<PPDiscountReturnDetail>().ToList();
                    string debit004TTPaymentsssss = string.Join(", ", list004TTPaymentsssss.Select(c => c.DebitAccount).Distinct().ToArray());
                    string credit004TTPaymentsssss = string.Join(", ", list004TTPaymentsssss.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debit004TTPaymentsssss, CreditAccount = credit004TTPaymentsssss });
                    break;
                case "PPInvoice-02-TT":
                case "PPInvoice-02-TT-A5"://add by cuongpv
                    if (_typeID == 260 && _select is PPInvoice)
                    {
                        var data = _select as PPInvoice;
                        var inputReport = Utils.IMCPaymentService.Getbykey(data.ID);
                        inputReport.TotalAll = data.TotalAll ?? 0;
                        inputReport.TotalAllOriginal = data.TotalAllOriginal ?? 0;
                        f.AddDatasource("Data2", inputReport);
                    }
                    //Guid itemID = new Guid(_select.GetProperty("ID").ToString());
                    //var GLData = Utils.IGeneralLedgerService.Query.Where(o => o.ReferenceID == itemID);
                    //string GLDebit = string.Join(", ", GLData.Where(o => o.DebitAmount > 0).Select(o => o.Account).Distinct().ToArray());
                    //string GLCredit = string.Join(", ", GLData.Where(o => o.CreditAmount > 0).Select(o => o.Account).Distinct().ToArray());
                    //f.AddDatasource("Detail", new { DebitAccount = GLDebit, CreditAccount = GLCredit });
                    var lstDataInvoice02TT = typeof(PPInvoice).GetProperty("PPInvoiceDetails").GetValue((_selectJoin != null && _selectJoin is PPInvoice) ? (_selectJoin as PPInvoice) : (_select as PPInvoice), null);
                    var listInvoice02TT = ((System.Collections.IEnumerable)lstDataInvoice02TT).Cast<PPInvoiceDetail>().ToList();
                    string debitInvoice02TT = string.Join(", ", listInvoice02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                    string creditInvoice02TT = string.Join(", ", listInvoice02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debitInvoice02TT, CreditAccount = creditInvoice02TT });
                    break;
                case "PPInvoice-GTTTTU":
                    if (_typeID == 260 && _select is PPInvoice)
                    {
                        var data = _select as PPInvoice;
                        var mcpayment = Utils.IMCPaymentService.Getbykey(data.ID);
                        f.AddDatasource("Data", mcpayment);
                    }
                    var lstDataInvoice04TT = typeof(PPInvoice).GetProperty("PPInvoiceDetails").GetValue((_selectJoin != null && _selectJoin is PPInvoice) ? (_selectJoin as PPInvoice) : (_select as PPInvoice), null);
                    var listInvoice04TT = ((System.Collections.IEnumerable)lstDataInvoice04TT).Cast<PPInvoiceDetail>().ToList();
                    string debitInvoice04TT = string.Join(", ", listInvoice04TT.Select(c => c.DebitAccount).Distinct().ToArray());
                    string creditInvoice04TT = string.Join(", ", listInvoice04TT.Select(c => c.CreditAccount).Distinct().ToArray());
                    f.AddDatasource("Detail", new { DebitAccount = debitInvoice04TT, CreditAccount = creditInvoice04TT });
                    break;
                case "PPService-02-TT":
                case "PPService-02-TT-A5"://add by cuongpv
                    if (_typeID == 260 && _select is PPService)
                    {
                        var data = _select as PPService;
                        var inputReport = Utils.IMCPaymentService.Getbykey(data.ID);
                        inputReport.TotalAll = data.TotalAll ?? 0;
                        inputReport.TotalAllOriginal = data.TotalAllOriginal ?? 0;
                        f.AddDatasource("Data", inputReport);
                    }
                    var lstDataService02TT = typeof(PPService).GetProperty("PPServiceDetails").GetValue((_selectJoin != null && _selectJoin is PPService) ? (_selectJoin as PPService) : (_select as PPService), null);
                    var listService02TT = ((System.Collections.IEnumerable)lstDataService02TT).Cast<PPServiceDetail>().ToList();
                    string debitService02TT = string.Join(", ", listService02TT.Select(c => c.DebitAccount).Distinct().ToArray());
                    string creditService02TT = string.Join(", ", listService02TT.Select(c => c.CreditAccount).Distinct().ToArray());
                    string Vatacc = string.Join(", ", listService02TT.Select(c => c.VATAccount).Distinct().ToArray());
                    string tkno = string.Format("{0}, {1}", debitService02TT, Vatacc);
                    f.AddDatasource("Detail", new { DebitAccount = tkno, CreditAccount = creditService02TT });
                    break;
                case "SAQuote-BH-HDBH":
                case "SAQuote-BH-HDBHTT":
                case "SAQuote-BH-PXK":
                case "SAQuote-BH-PXKBH":
                case "SAQuote-BH-PXK-A4":
                case "SAQuote-BH-PXK-SL-HD-A4":
                case "SAQuote-BH-PXKNB":
                case "SAQuote-BH-PXKNB-A4":
                case "SAQuote-BH-PXKBH-SL-HD":
                    {
                        var temp = (_selectJoin != null && _selectJoin is SAInvoice) ? (_selectJoin as SAInvoice) : (_select as SAInvoice);
                        //add by namnh
                        if ((temp.IsBill == false) && (code == "SAQuote-BH-HDBHTT" || code == "SAQuote-BH-HDBH") &&
                            (_typeID == 320 || _typeID == 321 || _typeID == 322 || _typeID == 323 || _typeID == 324 || _typeID == 325 || _typeID == 340 || _typeID == 220))
                        {
                            MSG.Warning("Đây là chứng từ bán hàng không kèm hóa đơn.");
                            return;
                        }
                        //end
                        //int rcount = code == "SAQuote-BH-HDBHTT" || code == "SAQuote-BH-PXKBH" || code == "SAQuote-BH-PXKBH-SL-HD" ? 13 : (code == "SAQuote-BH-HDBH" ? 10 : (code == "SAQuote-BH-PXK-A4" || code == "SAQuote-BH-PXKNB-A4"  || code == "SAQuote-BH-PXK-SL-HD-A4" ? 20 : 3));
                        int rcount = code == "SAQuote-BH-HDBHTT" ? 13 : (code == "SAQuote-BH-HDBH" ? 10 : (code == "SAQuote-BH-PXK-A4" || code == "SAQuote-BH-PXK-SL-HD-A4" || code == "SAQuote-BH-PXKBH" || code == "SAQuote-BH-PXKBH-SL-HD" || code == "SAQuote-BH-PXK" || code == "SAQuote-BH-PXKBH-A5" ? temp.SAInvoiceDetails.Count : (code == "SAQuote-BH-PXKNB-A4" ? 20 : 3)));
                        f.DefaultPaperSize = (code == "SAQuote-BH-PXK" || code == "SAQuote-BH-PXKNB") ? ConstFrm.Paper.PAPER_SIZE_A5 : ConstFrm.Paper.PAPER_SIZE_A4;
                        if (temp.IsAttachList == true && !new[] { "SAQuote-BH-PXK", "SAQuote-BH-PXK-A4", "SAQuote-BH-PXK-SL-HD-A4", "SAQuote-BH-PXKNB", "SAQuote-BH-PXKNB-A4", "SAQuote-BH-PXKBH", "SAQuote-BH-PXKBH-SL-HD" }.Contains(code))
                        {
                            SAInvoice t = new SAInvoice();
                            temp.CopyTo(ref t);
                            var source = new List<SAInvoiceDetail>();
                            var source1 = new List<SAInvoice>();
                            foreach (var x in t.SAInvoiceDetails)
                            {
                                if (x.MaterialGoodsID.HasValue)
                                    x.MaterialGoodsName = x.Description/*Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == x.MaterialGoodsID.Value).MaterialGoodsName*/;
                                source.Add(x);
                            }
                            int h = rcount - source.Count;
                            for (int j = 0; j < h; j++)
                            {
                                source.Add(new SAInvoiceDetail { VATRate = temp.MaxVATRate });
                            }
                            t.SAInvoiceDetails = source;
                            t.ExchangeRate = temp.ExchangeRate;
                            t.CurrencyID = temp.CurrencyID;
                            //source1.Add(t);
                            f.AddDatasource("Data", t);
                        }
                        else //if (temp.SAInvoiceDetails.Count > 10)
                        {
                            //var max = (temp.SAInvoiceDetails.Count % rcount) > 0 ? (temp.SAInvoiceDetails.Count / rcount + 1) : (temp.SAInvoiceDetails.Count / rcount);
                            var max = temp.SAInvoiceDetails.Count < rcount ? 0 : temp.SAInvoiceDetails.Count / rcount;
                            if (temp.SAInvoiceDetails.Count % rcount != 0) max = max + 1;
                            var source = new List<SAInvoice>();
                            for (int i = 1; i <= max; i++)
                            {
                                SAInvoice select1 = new SAInvoice();
                                temp.CopyTo(ref select1);
                                var query = from c in temp.SAInvoiceDetails orderby c.OrderPriority ascending select c;
                                foreach (var item in query)
                                {
                                    if (item.MaterialGoodsID.HasValue)
                                        item.MaterialGoodsName = item.Description/*Utils.ListMaterialGoodsCustom.FirstOrDefault(m => m.ID == item.MaterialGoodsID.Value).MaterialGoodsName*/;
                                }
                                var list = query.Skip(rcount * (i - 1)).Take(rcount).ToList();
                                int h = rcount - list.Count;
                                //bool isDiscount = false;
                                if (h > 0)
                                {
                                    if (code != "SAQuote-BH-PXK" && code != "SAQuote-BH-PXK-A4" && code != "SAQuote-BH-PXK-SL-HD-A4" && code != "SAQuote-BH-PXKNB" && code != "SAQuote-BH-PXKNB-A4" && code != "SAQuote-BH-PXKBH" && code != "SAQuote-BH-PXKBH-SL-HD")
                                        //if (select1.TotalDiscountAmount != (decimal)0)
                                        //{
                                        //    isDiscount = true;
                                        //    list.Add(new SAInvoiceDetail
                                        //    {
                                        //        ID = Guid.NewGuid(),
                                        //        MaterialGoodsName = "Chiết khấu",
                                        //        Amount = select1.TotalDiscountAmount
                                        //    });
                                        //    h--;
                                        //}
                                        for (int j = 0; j < h; j++)
                                        {
                                            list.Add(new SAInvoiceDetail());
                                        }
                                }
                                select1.SAInvoiceDetails = list.OrderBy(n => n.OrderPriority).ToList();
                                select1.TotalAmount = list.Sum(x => x.Amount);
                                select1.TotalAmountOriginal = list.Sum(x => x.AmountOriginal);
                                select1.TotalDiscountAmount = list.Sum(x => x.DiscountAmount);
                                select1.TotalDiscountAmountOriginal = list.Sum(x => x.DiscountAmountOriginal);
                                select1.TotalVATAmount = list.Sum(x => x.VATAmount);
                                select1.TotalVATAmountOriginal = list.Sum(x => x.VATAmountOriginal);
                                select1.TotalDiscountRate = (decimal)((select1.TotalDiscountAmountOriginal / (select1.TotalAmountOriginal == 0 ? 1 : select1.TotalAmountOriginal)) * 100);
                                select1.TotalVATRate = (decimal)((select1.TotalVATAmountOriginal / (select1.TotalAmountOriginal == 0 ? 1 : select1.TotalAmountOriginal)) * 100);
                                //if (temp.TotalDiscountAmount != (decimal)0 && isDiscount)
                                //    select1.TotalDiscountAmount = temp.TotalDiscountAmount * 2;
                                source.Add(select1);
                            }
                            f.AddDatasource("Data", source);
                        }
                        if (code == "SAQuote-BH-PXK" || code == "SAQuote-BH-PXK-A4" || code == "SAQuote-BH-PXKNB" || code == "SAQuote-BH-PXKNB-A4" || code == "SAQuote-BH-PXK-SL-HD-A4")
                        {
                            if (temp.IsDeliveryVoucher == false)
                            {
                                MSG.Warning("Đây là chứng từ bán hàng không kiêm phiếu xuất kho. Vui lòng kiểm tra lại!");
                                return;
                            }
                            var lstData004TTPayments = typeof(SAInvoice).GetProperty("SAInvoiceDetails").GetValue((_selectJoin != null && _selectJoin is SAInvoice) ? (_selectJoin as SAInvoice) : (_select as SAInvoice), null);
                            var list004TTPayments = ((System.Collections.IEnumerable)lstData004TTPayments).Cast<SAInvoiceDetail>().ToList();
                            string debit004TTPayments = string.Join(", ", list004TTPayments.Select(c => c.CostAccount).Distinct().ToArray());
                            string credit004TTPayments = string.Join(", ", list004TTPayments.Select(c => c.RepositoryAccount).Distinct().ToArray());
                            string repository = string.Join(", ", list004TTPayments.Select(c => Accounting.ReportUtils.GetRepositoryName(c.RepositoryID.ToString())).Distinct().ToArray());
                            f.AddDatasource("Detail", new { CostAccount = debit004TTPayments, RepositoryAccount = credit004TTPayments, Repository = repository });
                        }
                    }
                    break;
                case "SAReturn-BH-HDBH":
                case "SAReturn-BH-HDBHTT":
                    {
                        var temp = (_selectJoin != null && _selectJoin is SAReturn) ? (_selectJoin as SAReturn) : (_select as SAReturn);
                        if (temp.IsBill == false)
                        {
                            if (TypeID == 330)
                                MSG.Warning("Đây là chứng từ hàng bán trả lại không lập kèm hóa đơn. Vui lòng kiểm tra lại!");
                            else
                                MSG.Warning("Đây là chứng từ hàng bán giảm giá không lập kèm hóa đơn. Vui lòng kiểm tra lại!");
                            return;
                        }
                        int rcount = code == "SAReturn-BH-HDBHTT" ? 13 : (code == "SAReturn-BH-HDBH" ? 10 : 3);
                        f.DefaultPaperSize = ConstFrm.Paper.PAPER_SIZE_A4;
                        var max = temp.SAReturnDetails.Count < rcount ? 0 : temp.SAReturnDetails.Count / rcount;
                        if (temp.SAReturnDetails.Count % rcount != 0) max = max + 1;
                        var source = new List<SAReturn>();
                        for (int i = 1; i <= max; i++)
                        {
                            SAReturn select1 = new SAReturn();
                            temp.CopyTo(ref select1);
                            var query = from c in temp.SAReturnDetails orderby c.OrderPriority ascending select c;
                            foreach (var item in query)
                            {
                                if (item.MaterialGoodsID.HasValue)
                                    item.MaterialGoodsName = item.Description;
                            }
                            var list = query.Skip(rcount * (i - 1)).Take(rcount).ToList();
                            int h = rcount - list.Count;
                            if (h > 0)
                            {
                                for (int j = 0; j < h; j++)
                                {
                                    list.Add(new SAReturnDetail());
                                }
                            }
                            select1.SAReturnDetails = list;
                            select1.TotalAmount = list.Sum(x => x.Amount);
                            select1.TotalAmountOriginal = list.Sum(x => x.AmountOriginal);
                            select1.TotalDiscountAmount = list.Sum(x => x.DiscountAmount);
                            select1.TotalDiscountAmountOriginal = list.Sum(x => x.DiscountAmountOriginal);
                            select1.TotalVATAmount = list.Sum(x => x.VATAmount);
                            select1.TotalVATAmountOriginal = list.Sum(x => x.VATAmountOriginal);

                            source.Add(select1);
                        }
                        f.AddDatasource("Data", source);

                    }
                    break;
                case "SABill-BH-HDBH":
                case "SABill-BH-HDBHTT":
                    {
                        var temp = (_selectJoin != null && _selectJoin is SABill) ? (_selectJoin as SABill) : (_select as SABill);

                        int rcount = code == "SABill-BH-HDBHTT" ? 13 : (code == "SABill-BH-HDBH" ? 10 : 3);
                        var max = temp.SABillDetails.Count < rcount ? 0 : temp.SABillDetails.Count / rcount;
                        if (temp.SABillDetails.Count % rcount != 0) max = max + 1;
                        var source = new List<SABill>();
                        for (int i = 1; i <= max; i++)
                        {
                            SABill select1 = new SABill();
                            temp.CopyTo(ref select1);
                            var query = from c in temp.SABillDetails orderby c.OrderPriority ascending select c;
                            foreach (var item in query)
                            {
                                if (item.MaterialGoodsID.HasValue)
                                    item.MaterialGoodsName = item.Description;
                            }
                            var list = query.Skip(rcount * (i - 1)).Take(rcount).ToList();
                            int h = rcount - list.Count;

                            if (h > 0)
                            {
                                for (int j = 0; j < h; j++)
                                {
                                    list.Add(new SABillDetail());
                                }
                            }
                            select1.SABillDetails = list;
                            select1.TotalAmount = list.Sum(x => x.Amount);
                            select1.TotalAmountOriginal = list.Sum(x => x.AmountOriginal);
                            select1.TotalDiscountAmount = list.Sum(x => x.DiscountAmount);
                            select1.TotalDiscountAmountOriginal = list.Sum(x => x.DiscountAmountOriginal);
                            select1.TotalVATAmount = list.Sum(x => x.VATAmount);
                            select1.TotalVATAmountOriginal = list.Sum(x => x.VATAmountOriginal);

                            source.Add(select1);
                        }
                        f.AddDatasource("Data", source);

                    }
                    break;
                case "PPDiscount-MH-HDBH":
                case "PPDiscount-MH-HDBHTT":
                    {
                        var temp = (_selectJoin != null && _selectJoin is PPDiscountReturn) ? (_selectJoin as PPDiscountReturn) : (_select as PPDiscountReturn);
                        if (temp.IsBill == false)
                        {
                            if (TypeID == 220)
                                MSG.Warning("Đây là chứng từ hàng mua trả lại không lập kèm hóa đơn. Vui lòng kiểm tra lại!");
                            else
                                MSG.Warning("Đây là chứng từ hàng mua giảm giá không lập kèm hóa đơn. Vui lòng kiểm tra lại!");
                            return;
                        }
                        int rcount = code == "PPDiscount-MH-HDBHTT" ? 13 : (code == "PPDiscount-MH-HDBH" ? 10 : 3);
                        var max = temp.PPDiscountReturnDetails.Count < rcount ? 0 : temp.PPDiscountReturnDetails.Count / rcount;
                        if (temp.PPDiscountReturnDetails.Count % rcount != 0) max = max + 1;
                        var source = new List<PPDiscountReturn>();
                        for (int i = 1; i <= max; i++)
                        {
                            PPDiscountReturn select1 = new PPDiscountReturn();
                            temp.CopyTo(ref select1);
                            var query = from c in temp.PPDiscountReturnDetails orderby c.OrderPriority ascending select c;
                            foreach (var item in query)
                            {
                                if (item.MaterialGoodsID.HasValue)
                                    item.MaterialGoodsName = item.Description;
                            }
                            var list = query.Skip(rcount * (i - 1)).Take(rcount).ToList();
                            int h = rcount - list.Count;
                            if (h > 0)
                            {
                                for (int j = 0; j < h; j++)
                                {
                                    list.Add(new PPDiscountReturnDetail());
                                }
                            }
                            select1.PPDiscountReturnDetails = list;
                            select1.TotalAmount = (decimal)list.Sum(x => x.Amount);
                            select1.TotalAmountOriginal = (decimal)list.Sum(x => x.AmountOriginal);
                            //select1.TotalDiscountAmount = (decimal)list.Sum(x => x.DiscountAmount);
                            //select1.TotalDiscountAmountOriginal = (decimal)list.Sum(x => x.DiscountAmountOriginal);
                            select1.TotalVATAmount = (decimal)list.Sum(x => x.VATAmount);
                            select1.TotalVATAmountOriginal = (decimal)list.Sum(x => x.VATAmountOriginal);

                            source.Add(select1);
                        }
                        f.AddDatasource("Data", source);

                    }
                    break;
            }

            try
            {
                f.LoadReport();
                f.WindowState = FormWindowState.Maximized;
                f.Show(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetTextStatusBar(string str)
        {
            if (uStatusBar == null)
                uStatusBar = new UltraStatusBar();
            uStatusBar.Text = str;
        }

        protected void HelpFunction()
        {
            MessageBox.Show(@"UNDER CONSTRUCTION!");
        }

        protected void utmDetailBaseToolBar_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            if (_select == null)
            {
                return;
            }
            if (_select is GOtherVoucher)
            {
                if (typeof(T) == typeof(GOtherVoucher))
                {
                    GOtherVoucher data = _select as GOtherVoucher;
                    var details = data.GOtherVoucherDetails;
                    decimal totalAmount = details.Sum(o => o.Amount);
                    _select.SetProperty("TotalAmount", totalAmount);
                }
            }
            PropertyInfo propExchange = _select.GetType().GetProperty("ExchangeRate");
            if (propExchange != null && propExchange.CanWrite && propExchange.CanRead)
            {
                if (propExchange.GetValue(_select, null) == null)
                {
                    propExchange.SetValue(_select, (decimal)1, null);
                }
            }
            if (!Checked)
            {
                UltraGrid uGrid = this.FindGridIsActived();
                if (uGrid != null)
                {
                    uGrid.PerformAction(UltraGridAction.ExitEditMode);
                    uGrid.Update();
                    this.DoEverythingInGrid<T>(uGrid);
                }
            }
            switch (e.Tool.Key)
            {
                case "mnbtnBack":
                    BackFunction();
                    break;

                case "mnbtnForward":
                    ForwardFunction();
                    break;

                case "mnbtnAdd":
                    Add4Function();
                    break;

                case "mnbtnEdit":
                    Edit1Function();
                    break;

                case "mnbtnSave":
                    if (_statusForm != ConstFrm.optStatusForm.View)
                        SendKeys.Send("+{TAB}");
                    SaveFunction();
                    break;

                case "mnbtnDelete":
                    DeleteFunction();
                    break;

                //case "mnbtnComeBack":
                //    ComeBackFunction();
                //    break;

                case "mnbtnPost":
                    PostFunction();
                    break;

                case "mnbtnUnPost":
                    UnPostFunction();
                    break;

                case "mnbtnReset":
                    Reset();
                    break;

                case "mnbtnViewList":
                    ViewListFunction();
                    break;

                case "mnbtnTemplateEdit":
                    TemplateFunction();
                    break;

                case "mnbtnPrint":
                    PrintFunction();
                    break;

                case "mnbtnHelp":
                    HelpFunction();
                    break;

                case "mnbtnClose":
                    CloseFunction();
                    break;
            }
            if (e.Tool.Key.Contains("mnbtnUtilitie_"))
            {
                //string str = string.Empty;
                string[] temp = e.Tool.Key.Split(new string[] { "mnbtnUtilitie_" }, StringSplitOptions.RemoveEmptyEntries);
                switch (temp[0])
                {
                    case "MaterialGoods":
                        OpenUtilitieDialog<MaterialGoodsCustom>(new FMaterialGoodsDetail());
                        break;

                    case "AccountingObject":
                        OpenUtilitieDialog<AccountingObject>(TypeGroup.HasValue && Utils.PurchaseTypeGroup.Contains(TypeGroup.Value) ?
        new FAccountingObjectCustomersDetail(new AccountingObject { ObjectType = 1 }, true) :
        new FAccountingObjectCustomersDetail());
                        break;

                    case "Employee":
                        OpenUtilitieDialog<AccountingObject>(new FAccountingObjectEmployeeDetail());
                        break;

                    //case "Contract":
                    //    OpenUtilitieDialog<EMContract>(new FEMContractQuickCreate(_select.GetProperty<T, DateTime>("Date"), TypeGroup));
                    //    break;

                    case "Repository":
                        OpenUtilitieDialog<Repository>(new FRepositoryDetail());
                        break;

                    case "BudgetItem":
                        OpenUtilitieDialog<BudgetItem>(new FBudgetItemDetail());
                        break;

                    case "StatisticsCode":
                        OpenUtilitieDialog<StatisticsCode>(new FStatisticsCodeDetail());
                        break;

                    case "CostSet":
                        OpenUtilitieDialog<CostSet>(new FCostSetDetail());
                        break;
                    case "FixedAsset":
                        OpenUtilitieDialog<FixedAsset>(new FFixedAssetDetail());
                        break;

                    case "AddNew":
                        //Config Menu
                        IsAddNew = true;
                        ResetForm();

                        IsAddNew = false;
                        IsClose = true;
                        break;
                }
            }
            else if (e.Tool.Key.Contains("mnbtnTemplate_"))
            {
                if (!Authenticate.Permissions("ELP", SubSystemCode))
                {
                    MSG.Warning(resSystem.MSG_Permission);
                    return;
                }
                string[] temp = e.Tool.Key.Split(new string[] { "mnbtnTemplate_" }, StringSplitOptions.RemoveEmptyEntries);
                var templateId = new Guid(temp[0]);
                this.ReloadTemplate<T>(templateId, false);
                this.ConfigTemplateEdit<T>(utmDetailBaseToolBar, TypeID, _statusForm, _templateID);
            }
            else if (e.Tool.Key.Contains("mnbtnPrint_"))
            {
                if (!Authenticate.Permissions("EXR", SubSystemCode))
                {
                    MSG.Warning(resSystem.MSG_Permission);
                    return;
                }
                string[] temp1 = e.Tool.Key.Split(new string[] { "mnbtnPrint_" }, StringSplitOptions.RemoveEmptyEntries);
                if (temp1.Any())
                {
                    if (temp1[0] != "Option") Print(temp1[0] ?? "");
                    else if (temp1[0] == "Option")
                    {
                        var frm = new FPrintOption(TypeID);
                        try
                        {
                            frm.ShowDialog(this);
                        }
                        catch (Exception ex)
                        {
                            MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                            return;
                        }
                        if (frm.isClose) Print(frm.VcRa.VoucherPatternsCode);
                    }
                }
            }
            if (TypeID == 560 && FADecrement != null)
            {
                if (MSG.MessageBoxStand(string.Format("Hệ thống đã sinh tự động chứng từ ghi giảm Số {0} để hạch toán ghi giảm tài sản. Bạn có muốn xem chứng từ này không?", FADecrement.No),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.No)
                {
                    //Close();//comment by cuongpv 201909181632
                    new FFADecrementDetail(FADecrement, new List<FADecrement> { FADecrement }, ConstFrm.optStatusForm.View).ShowDialog(this);

                }
                FADecrement = null;
            }
            if (TypeID == 180)
            {
                if (MCReceipt != null)
                {
                    if (MSG.MessageBoxStand(string.Format("Hệ thống đã sinh tự động phiếu thu Số {0} . Bạn có muốn xem chứng từ này không?", MCReceipt.No),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.No)
                    {
                        //Close();
                        new FMCReceiptDetail(MCReceipt, new List<MCReceipt> { MCReceipt }, ConstFrm.optStatusForm.View).ShowDialog(this);

                    }
                    MCReceipt = null;
                }
                else if (MCPayment != null)
                {
                    if (MSG.MessageBoxStand(string.Format("Hệ thống đã sinh tự động phiếu chi Số {0} . Bạn có muốn xem chứng từ này không?", MCPayment.No),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.No)
                    {
                        //Close();
                        new FMCPaymentDetail(MCPayment, new List<MCPayment> { MCPayment }, ConstFrm.optStatusForm.View).ShowDialog(this);

                    }
                    MCPayment = null;
                }

            }
            if (TypeID == 435 && (TIIncrement != null || TIDecrement != null))
            {
                MSG.Information("Hệ thống đã sinh tự động chứng từ ghi giảm và ghi tăng tương ứng");
                TIIncrement = null;
                TIDecrement = null;
            }
        }

        private void OpenUtilitieDialog<T2>(Form frmUtilitie)
        {
            try
            {
                frmUtilitie.FormClosed += new FormClosedEventHandler(FrmUtilitie_FormClosed<T2>);
                frmUtilitie.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
            }
        }

        private void FrmUtilitie_FormClosed<T2>(object sender, FormClosedEventArgs e)
        {
            var frm = (Form)sender;
            Utils.ClearCacheByType<T2>();
            ReloadCombo(this, frm);
            //string str = frm.GetType().Name;
        }
        /// <summary>
        /// Load lai DL Combo theo Form Tiện ích
        /// </summary>
        /// <param name="ctrlContainer"></param>
        /// <param name="frmUtilitie"></param>
        private void ReloadCombo(Control ctrlContainer, Form frmUtilitie)
        {
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (!string.IsNullOrEmpty(ctrl.Name) && (ctrl is UltraGrid))
                {
                    var uGrid = (UltraGrid)ctrl;
                    foreach (var column in uGrid.DisplayLayout.Bands[0].Columns)
                    {
                        this.ConfigEachColumn4Grid<T>(TypeID, column, uGrid);
                    }
                }
                else if (!string.IsNullOrEmpty(ctrl.Name) && (ctrl is UltraCombo))
                {
                    if ((frmUtilitie is FAccountingObjectCustomersDetail) || (frmUtilitie is FAccountingObjectEmployeeDetail) || (frmUtilitie is FBankAccountDetailDetail) || (frmUtilitie is FCreditCardDetail))
                    {
                        //Làm mới hết tất cả Combo
                        var combo = (UltraCombo)ctrl;
                        if (combo.Tag != null)
                            combo.RefreshSource<T>(this);
                    }
                }
                else if (ctrl.HasChildren)
                    ReloadCombo(ctrl, frmUtilitie);
            }
        }

        protected virtual bool AdditionalFunc()
        {
            if (_typeID == 434)
            {
                UltraGrid ugrid = this.Controls.Find("ugrid1", true).FirstOrDefault() as UltraGrid;
                BindingList<TIAllocationAllocated> allo = (BindingList<TIAllocationAllocated>)ugrid.DataSource;
                var a = (from p in allo
                         group p by p.ToolsID into g
                         select new { key = g.Key, allo = g.ToList() }).ToList();
                foreach (var b in a)
                {
                    if (b.allo.Count > 0 && (100 - b.allo.Sum(x => Math.Round(x.Rate, 2))) > (decimal)0.1)
                    {
                        MSG.Warning(string.Format("Tổng tỷ lệ phân bổ của CCDC {0} phải bằng 100%", b.allo[0].ToolsName));
                        return false;
                    }
                }

            }
            return true;

        }

        protected bool SaveFunction()
        {
            try { UltraTextEditor ucombo1 = this.Controls.Find("txtBankName", true).FirstOrDefault() as UltraTextEditor; ucombo1.Focus(); }
            catch { }
            // utmDetailBaseToolBar.Tools["mnbtnSave"].IsActiveTool = true;
            if (!Authenticate.Permissions(_statusForm == ConstFrm.optStatusForm.Add ? "TH" : "SU", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return false;
            }
            if (!Utils.validateRegistration(Utils.Production, false))
            {
                return false;
            }
            #region Bổ sung cập nhật InwardNo sang No cho TH Mua hàng qua kho
            //Logs.WriteTxt("line 3852");
            if (!AdditionalFunc()) return false;
            //Logs.WriteTxt("line 3854");
            #endregion
            #region Xử lý số chứng từ phiếu thu nhận từ nhập vào
            List<UTopVouchers<T>> topVouchers = GetTopVoucher(this);
            foreach (UTopVouchers<T> topVoucher in topVouchers)
            {
                if (topVoucher.TypeGroup == 10)
                {
                    _select.SetProperty("MNo", topVoucher.No);
                }
            }
            #endregion
            #region Check Lỗi
            if (_statusForm != ConstFrm.optStatusForm.Add && !this.CheckExistVoucher(_select))
                if (IsClosing) return true;
            //else Close();
            if (_typeID == 510 || _typeID == 907)
            {
                decimal a = _select.GetProperty<T, decimal>("TotalAmountFromVoucher");
                decimal b = _typeID == 907 ? _select.GetProperty<T, decimal>("TotalAmountOriginal") : _select.GetProperty<T, decimal>("TotalOrgPriceOriginal");
                if (a != 0 && a != b)
                {
                    string msg = _typeID == 907 ? "Giá trị của CCDC ghi tăng không khớp với số tiền trên chứng từ nguồn gốc hình thành. Bạn có muốn tiếp tục lưu chứng từ này không ?" : "Nguyên giá của TSCĐ ghi tăng không khớp với số tiền trên chứng từ nguồn gốc hình thành. Bạn có muốn tiếp tục lưu chứng từ này không ?";
                    if (MSG.Question(msg) == DialogResult.No)
                    {
                        return false;
                    }
                }
            }
            if (typeof(T) == typeof(SAInvoice))
            {
                SAInvoice data = _select as SAInvoice;
                if (data.BillRefID != null)
                {
                    SABill sabill = Utils.ListSABill.Where(x => x.ID == new Guid(data.BillRefID.ToString())).FirstOrDefault();
                    string sabillDate = sabill.InvoiceDate == null ? "" : (sabill.InvoiceDate ?? DateTime.Now).ToString("dd/MM/yyyy");
                    var aff = string.Format("{0}/{1}/{2}/{3}", sabill.InvoiceTemplate, sabill.InvoiceSeries, sabill.InvoiceNo, sabillDate);
                    string msg = "Chứng từ bán hàng đã sinh hóa đơn <" + aff + "> \nNếu sửa chứng từ thì phần mềm sẽ không cập nhật lên hóa đơn đó.Bạn có muốn sửa không ?";
                    if (MSG.Question(msg) == DialogResult.No)
                    {
                        return false;
                    }
                }
            }
            if (_typeID == 120)
            {

            }
            UltraTabControl ultraTabControlCheck = (UltraTabControl)this.Controls.Find("ultraTabControl", true).FirstOrDefault();
            if (ultraTabControlCheck != null)//trungnq thêm vào để thông báo lỗi đỏ ở cell trong grid khi ấn lưu, bug 6426 6427
            {
                foreach (var tab in ultraTabControlCheck.Tabs)
                {
                    string ugridName = "uGrid" + tab.Index;
                    UltraGrid ultraGridCheck = (UltraGrid)ultraTabControlCheck.Controls.Find(ugridName, true).FirstOrDefault();
                    if (ultraGridCheck != null)
                    {
                        foreach (var ultraRow in ultraGridCheck.Rows)
                        {
                            foreach (var ultraCell in ultraRow.Cells)
                            {
                                if (ultraCell != null)
                                {
                                    Utils.CheckErrorInCell<T>(this, ultraGridCheck, ultraCell);
                                }
                            }
                        }
                        //if (ultraGridCheck.Rows.Count!=0)
                        //    for(int i=0;i<ultraGridCheck.Rows.Count;i++)
                        //    {
                        //        if(ultraGridCheck.Rows[i].Cells.Count!=0)
                        //        {
                        //            for(int j =0;j< ultraGridCheck.Rows[i].Cells.Count;j++)
                        //            {
                        //                Utils.CheckErrorInCell<T>(this,ultraGridCheck, ultraGridCheck.Rows[i].Cells[j]);
                        //            }
                        //        }
                        //    }
                    }
                }
            }

            if (TypeID != 680)
                //Logs.WriteTxt("line 3859");
                if (!CheckErrorBase() || !CheckError()) return false;
            //Logs.WriteTxt("line 3861");
            #endregion
            if (_typeID == 326)//trungnq thêm để sửa khi sửa ngày hóa đơn thì cặp nhật luôn trường refdatetime theo invoicedate nhưng ẫn giữ nguyên giờ của refdatetime cũ ở màn xuất hóa đơn
            {
                DateTime? dateInvoiceDate = null;
                DateTime? dateRefDateTime = null;
                if (_select.GetProperty("InvoiceDate") != null)
                {
                    dateInvoiceDate = (DateTime)_select.GetProperty("InvoiceDate");
                }
                if (_select.GetProperty("RefDateTime") != null)
                {
                    dateRefDateTime = (DateTime)_select.GetProperty("RefDateTime");
                }



                if ((dateInvoiceDate == null && dateRefDateTime != null) || (dateInvoiceDate == null && dateRefDateTime == null))
                { }
                else if (dateRefDateTime == null && dateInvoiceDate != null)
                {
                    _select.SetProperty("RefDateTime", dateInvoiceDate);
                }
                else if (dateInvoiceDate != null && dateRefDateTime != null)
                {
                    if (dateInvoiceDate.Value.Date != dateRefDateTime.Value.Date)
                    {
                        string timeRefdatetime = dateRefDateTime.Value == null ? null : dateRefDateTime.Value.TimeOfDay.ToString();
                        string dateInvoicedate = dateInvoiceDate.Value.Date.ToString();
                        string update = dateInvoicedate.Replace("00:00:00", timeRefdatetime);
                        _select.SetProperty("RefDateTime", DateTime.Parse(update));
                    }
                }

            }
            #region Thao  sổ
            if (_statusForm == ConstFrm.optStatusForm.Edit && ConstFrm.optBizFollowConst.status == ConstFrm.optBizFollowConst.NotSaveleged)
                Removeleged();
            //Logs.WriteTxt("line 3867");
            #endregion

            #region xử lý csdl
            isFirst = true;
            checkErrorEmployeeID = false;
            bool AddHD = false;
            if (_select.GetProperty<T, int>("TypeID") == 326 && _statusForm == ConstFrm.optStatusForm.Edit)
            {
                var id = _select.GetProperty<T, Guid>("ID");
                var md = Utils.ISABillService.Getbykey(id);
                AddHD = md == null;
                if (md != null) Utils.ISABillService.UnbindSession(md);
            }
            else if (_select.GetProperty<T, int>("TypeID") == 115 && _statusForm == ConstFrm.optStatusForm.Edit)
            {
                var id = _select.GetProperty<T, Guid>("ID");
                var md = Utils.IMCPaymentService.Getbykey(id);
                AddHD = md == null;
                if (md != null) Utils.IMCPaymentService.UnbindSession(md);
            }
            else if (_select.GetProperty<T, int>("TypeID") == 125 && _statusForm == ConstFrm.optStatusForm.Edit)
            {
                var id = _select.GetProperty<T, Guid>("ID");
                var md = Utils.IMBTellerPaperService.Getbykey(id);
                AddHD = md == null;
                if (md != null) Utils.IMBTellerPaperService.UnbindSession(md);
            }
            //add by cuongpv
            else if (_select.GetProperty<T, int>("TypeID") == 112 && _statusForm == ConstFrm.optStatusForm.Edit)
            {
                var id = _select.GetProperty<T, Guid>("ID");
                var md = Utils.IMCPaymentService.Getbykey(id);
                AddHD = md == null;
                if (md != null) Utils.IMBTellerPaperService.UnbindSession(md);
            }
            else if (_select.GetProperty<T, int>("TypeID") == 122 && _statusForm == ConstFrm.optStatusForm.Edit)
            {
                var id = _select.GetProperty<T, Guid>("ID");
                var md = Utils.IMBTellerPaperService.Getbykey(id);
                AddHD = md == null;
                if (md != null) Utils.IMBTellerPaperService.UnbindSession(md);
            }
            //end add by cuongpv
            bool status = (_statusForm == ConstFrm.optStatusForm.Add || AddHD) ? Add() : Edit();
            //Logs.WriteTxt("line 3872");
            if (!status)
            {
                if (check == 0)
                {
                    if (!checkErrorEmployeeID && isFirst)
                    {
                        MSG.Warning(string.Format("{0} chứng từ thất bại. Vui lòng kiểm tra lại.", _statusForm == ConstFrm.optStatusForm.Add ? "Thêm mới" : "Cập nhật"));

                        ReloadToolbar(_select, _listInput, _statusForm);
                        //Logs.WriteTxt("line 3877");
                        return false;
                    }
                    else
                    {
                        isFirst = true;
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            #endregion

            #region Thao tác ghi sổ
            if (ConstFrm.optBizFollowConst.status == ConstFrm.optBizFollowConst.NotSaveleged || ConstFrm.optBizFollowConst.status == ConstFrm.optBizFollowConst.SavelegedSave)
            {
                if (((IsSaveGeneralLedger.HasValue && IsSaveGeneralLedger.Value) || Utils.GetIsSaveGeneralLedger()))
                {
                    status = Saveleged();
                    if (status) TDTGia_NgayLapCTu(_select);
                }
            }


            #endregion

            #region xử lý form, kết thúc form
            //Clear danh sách chung [Báo giá] nếu là Form Báo giá
            if (TypeID.Equals(300)) Utils.ListSaQuote.Clear();
            //Clear danh sách chung [Vật tư/Hàng hóa] nếu là Form Nhập kho hoặc Xuất kho
            //if (Utils.ListTypeInward.Any(x => x == TypeID) || Utils.ListTypeOutward.Any(x => x == TypeID)) Utils.ListMaterialGoodsCustom.Clear(); Hautv comment vì làm mất config combobox ugrid
            // Set dữ liệu in chứng từ cho phiếu chi mua TSCĐ
            List<int> TypeOfBuyFixedAsset = new List<int> { 119 };
            if (TypeOfBuyFixedAsset.Contains(TypeID))
            {
                Guid inputID = (Guid)_select.GetProperty("ID");
                _selectJoin = Utils.IMCPaymentService.Getbykey(inputID);
            }
            //Config Menu
            _statusForm = ConstFrm.optStatusForm.View;
            //Config Menu
            //ReloadToolbar(_select, _listSelects, _statusForm);
            Reset();
            Checked = false;
            IsClose = true;
            try
            {
                InitializeGUI(_select); //Reload lại Form, khi sửa lưu lại
            }
            catch
            {

            }
            #endregion
            //DialogResult = DialogResult.OK;
            _saveSuccessfull = status;
            return status;
        }
        protected void DeleteFunction()
        {
            if (!Authenticate.Permissions("XO", SubSystemCode))
            {
                MSG.Warning(resSystem.MSG_Permission);
                return;
            }
            if (new int[] { 173, 133, 143, 126, 116 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPService))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var a = IPPServiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(a.TypeID);
                    if (a != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + a.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 172, 129, 132, 142, 119 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(FAIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var b = IFAIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(b.TypeID);
                    if (b != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + b.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 906, 903, 904, 905, 902 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(TIIncrement))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var c = ITIIncrementService.Getbykey(iD);
                    var module = ITypeService.Getbykey(c.TypeID);
                    if (c != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + c.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 171, 127, 131, 141, 117, 402, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var d = IPPInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(d.TypeID);
                    if (d != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + d.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 162, 163, 102, 103, 412, 415, }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAInvoice))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var e = ISAInvoiceService.Getbykey(iD);
                    var module = ITypeService.Getbykey(e.TypeID);
                    if (e != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + e.No + " tại chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 403 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(SAReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var f = ISAReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(f.TypeID);
                    if (f != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + f.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }
            }
            else if (new int[] { 413 }.Contains(_typeID))
            {
                if (typeof(T) != typeof(PPDiscountReturn))
                {
                    Guid iD = _select.GetProperty<T, Guid>("ID");
                    var g = IPPDiscountReturnService.Getbykey(iD);
                    var module = ITypeService.Getbykey(g.TypeID);
                    if (g != null)
                    {
                        MSG.Warning("Chứng từ này được phát sinh từ nghiệp vụ khác.\nVui lòng kiểm tra nghiệp vụ đã phát sinh và sửa trực tiếp tại chứng từ " + g.No + " của chức năng " + module.TypeName);
                        return;
                    }
                }

            }
            if (_select.GetProperty<T, int>("TypeID") == 326)
            {
                if (MSG.MessageBoxStand("Bạn có chắc chắn muốn xóa hóa đơn này?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            }
            else
            {
                if (MSG.MessageBoxStand(resSystem.MSG_System_40, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            }
            #region Xóa chứng từ bán hàng kèm hóa đơn đã phát hành
            if (new int[] { 326 }.Any(x => x == _select.GetProperty<T, int>("TypeID")) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data == "1" && _select.GetProperty<T, int>("InvoiceForm") == 2 && _select.GetProperty<T, string>("InvoiceNo") != "" && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo")))
            {
                MSG.Warning("Xóa hóa đơn thất bại! Hóa đơn này đã được cấp số.");
                return;
            }
            if (new int[] { 320, 321, 322, 323, 324, 325, 340, 220 }.Any(x => x == _select.GetProperty<T, int>("TypeID")) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data == "1" && _select.GetProperty<T, int>("InvoiceForm") == 2 && _select.GetProperty<T, string>("InvoiceNo") != "" && !string.IsNullOrEmpty(_select.GetProperty<T, string>("InvoiceNo")))
            {
                MSG.Warning("Xóa chứng từ thất bại! Chứng từ này đã phát sinh hóa đơn được cấp số.");
                return;
            }
            else if (new int[] { 320, 321, 322, 323, 324, 325, 326 }.Any(x => x == _select.GetProperty<T, int>("TypeID")) && Utils.ListSystemOption.FirstOrDefault(o => o.Code == "HDDT").Data != "1")
            {
                if (_select.GetProperty<T, int>("TypeID") == 326)
                {
                    var sabill = _select as SABill;
                    if (Utils.ListSAInvoice.Where(x => x.BillRefID == sabill.ID).ToList().Count == 0 &&
                        sabill.InvoiceNo != null && !string.IsNullOrEmpty(sabill.InvoiceNo))
                    {
                        if (MSG.MessageBoxStand("Hóa đơn này đã có số. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                    }
                    else if (Utils.ListSAInvoice.Where(x => x.BillRefID == sabill.ID).ToList().Count > 0)
                    {
                        if (MSG.MessageBoxStand("Hóa đơn này phát sinh liên quan đến chứng từ khác. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                    }
                }
                else
                {
                    var saIn = _select as SAInvoice;
                    if (saIn.BillRefID == null && saIn.InvoiceNo != null && !string.IsNullOrEmpty(saIn.InvoiceNo))
                    {
                        if (MSG.MessageBoxStand("Chứng từ này đã phát sinh hóa đơn. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                    }
                    else if (saIn.BillRefID != null && saIn.BillRefID != Guid.Empty)
                    {
                        if (MSG.MessageBoxStand("Chứng từ này đã phát sinh hóa đơn. Bạn có chắc chắn muốn xóa?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                    }
                }
            }
            #endregion

            #region kiểm tra điều kiện ràng buộc và bỏ ghi sổ
            if (_select != null && _select.HasProperty("Recorded") && _select.GetProperty<T, bool>("Recorded") && TypeID != 901 && TypeID != 900)
            {
                MSG.Warning(resSystem.MSG_Error_14);
                return;
            }
            CheckTSCD(TypeID, _select);
            if (ConstFrm.optBizFollowConst.status == ConstFrm.optBizFollowConst.NotSaveleged)
            {
                Removeleged();
            }
            #endregion
            #region Xóa hóa đơn (điện tử mới tạo lập) add by Hautv
            if (typeof(T) == typeof(SABill) || typeof(T) == typeof(SAInvoice) || typeof(T) == typeof(SAReturn) || typeof(T) == typeof(PPDiscountReturn))
            {
                try
                {
                    if (Utils.tichHopHDDT() && _select.GetProperty("InvoiceForm") != null && _select.GetProperty("InvoiceForm").ToString() == "2")
                    {
                        string mess = "";
                        SystemOption systemOption = Utils.ListSystemOption.FirstOrDefault(n => n.Code == "EI_IDNhaCungCapDichVu");
                        if (!string.IsNullOrEmpty(systemOption.Data))
                        {
                            SupplierService supplierService = Utils.ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                            if (supplierService.SupplierServiceCode == "SDS")
                            {
                                RestApiService client = new RestApiService(Utils.GetPathAccess(), Utils.GetUserName(), Core.ITripleDes.TripleDesDefaultDecryption(Utils.GetPassWords()));
                                var requestDL = new Request();
                                requestDL.Ikey = _select.GetProperty("ID").ToString();
                                requestDL.Pattern = _select.GetProperty("InvoiceTemplate").ToString();
                                requestDL.Serial = _select.GetProperty("InvoiceSeries").ToString();
                                Response responseDL = client.Post(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "XoaHDChuaPhatHanh-Svr").ApiPath, requestDL);
                                if (responseDL != null && responseDL.Status != 2) // Xóa hóa đơn không thành công hoặc không tồn tại trong hệ thống
                                {
                                    mess += responseDL.Message;
                                    if (!mess.IsNullOrEmpty())
                                    {
                                        if (DialogResult.Yes != MessageBox.Show(mess +
                                        "\n Nếu tiếp tục thực hiện dữ liệu sẽ không được đồng bộ giữa phần mềm kế toán và hóa đơn điện tử " +
                                        "\n Bạn có chắc chắn muốn tiếp tục thao tác này ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                        {
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            mess += "Chưa kết nối hóa đơn điện tử";
                            if (DialogResult.Yes != MessageBox.Show("Đồng bộ dữ liệu thất bại do mất kết nối hóa đơn điện tử " +
                                        "\n Nếu tiếp tục thực hiện dữ liệu sẽ không được đồng bộ giữa phần mềm kế toán và hóa đơn điện tử " +
                                        "\n Bạn có chắc chắn muốn tiếp tục thao tác này ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                return;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (DialogResult.Yes != MessageBox.Show("Đồng bộ dữ liệu thất bại do mất kết nối hóa đơn điện tử " +
                                        "\n Nếu tiếp tục thực hiện dữ liệu sẽ không được đồng bộ giữa phần mềm kế toán và hóa đơn điện tử " +
                                        "\n Bạn có chắc chắn muốn tiếp tục thao tác này ? ", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        return;
                    }
                }
            }
            #endregion
            #region xử lý csdl
            if (!Delete())
            {
                MSG.Warning(resSystem.MSG_System_53);
                return;
            }
            #endregion

            #region xử lý kết thúc nghiệp vụ
            //có thể đóng form
            //có thể nhảy đến một chứng từ trước đó
            #endregion

            //Config Menu
            IsClose = true;
            Close();
            //_select = (T)Activator.CreateInstance(typeof(T));
            //PropertyInfo propTypeID = _select.GetType().GetProperty("TypeID");
            //if (propTypeID != null && propTypeID.CanWrite)
            //    propTypeID.SetValue(_select,
            //                        Utils.ListType.OrderBy(p => p.ID).FirstOrDefault(
            //                            p => p.TypeGroupID.Equals(TypeGroup)).ID, null);
            //PropertyInfo propCurrencyID = _select.GetType().GetProperty("CurrencyID");
            //if (propCurrencyID != null && propCurrencyID.CanWrite)
            //    propCurrencyID.SetValue(_select, "VND", null);
            //InitializeGUI(_select);
            //ReloadToolbar(_select, _listSelects, _statusForm);
            //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnAdd", 2);
            //Utils.ConfigTopMenuDetails(utmDetailBaseToolBar, "mnbtnDelete", 1);
        }

        private bool CheckTSCD(int? typeId, T temp)
        {
            string fixedassetid = temp.GetProperty<T, string>("FixedAssetID");
            const string message = "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!";
            // Nếu là danh mục TSCĐ, kiểm tra xem đã ghi tăng chưa
            // Nếu đã ghi tăng, thông báo
            // "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!"
            if (typeof(T) == typeof(FixedAsset) && IFixedAssetService.CheckDeleteFixedAsset(new Guid(fixedassetid)))
            {
                MSG.Warning(message);
                return true;
            }
            // Nếu là ghi tăng tài sản cố định, kiểm tra xem đã giảm, tính khấu hao, điều chuyển, điều chỉnh chưa
            // Nếu đã thực hiện 1 trong những tác vụ trên thông báo :
            // "Không thể xóa tài sản này vì tài sản này đã có phát sinh chứng từ liên quan!"
            if (new[] { 500, 510 }.Contains(typeId.Value))
            {
                var FAIncrement = (FAIncrement)(object)temp;
                List<Guid?> fixedassetids = new List<Guid?>();
                foreach (FAIncrementDetail detail in FAIncrement.FAIncrementDetails)
                {
                    fixedassetids.Add(detail.FixedAssetID);
                }
                if (IFixedAssetService.CheckDeleteIncrement(fixedassetids))
                {
                    MSG.Warning(message);
                    return true;
                }
            }
            else if (new[] { 430, 902, 903, 904, 905, 906, 907 }.Contains(typeId.Value))
            {
                var TIIncrement = (TIIncrement)(object)temp;
                List<Guid?> TIIncrementIDs = new List<Guid?>();
                foreach (TIIncrementDetail detail in TIIncrement.TIIncrementDetails)
                {
                    TIIncrementIDs.Add(detail.ToolsID);
                }
                if (ITIInitService.CheckDeleteIncrement(TIIncrementIDs))
                {
                    MSG.Warning(message);
                    return true;
                }
            }
            return false;
        }

        protected void InitializeComponent(bool isBusiness)
        {
            InitializeComponent();
            if (!isBusiness)
            {

                utmDetailBaseToolBar.Visible = false;
                utmDetailBaseToolBar.Enabled = false;
            }
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
        }
        protected void InitializeComponent1()
        {
            InitializeComponent();
            if (Utils.ListSystemOption.FirstOrDefault(n => n.Code == "IsMinimized").Data == "1")
            {
                this.WindowState = FormWindowState.Normal;


                this.Width = Screen.PrimaryScreen.WorkingArea.Width - 200;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height - 50;
                if ((this.Text == "FFAIncrementDetail") || (this.Text == "FAAdjustmentDetail"))
                { this.Height = Screen.PrimaryScreen.WorkingArea.Height; }
                this.Location = new System.Drawing.Point(((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2), ((Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2));

            }
        }
        #region Khởi tạo Control trong DetailBase
        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnBack");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnForward");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup2 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup2");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnAdd");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool59 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnPost");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool60 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnUnPost");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool34 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnEdit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool35 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnSave");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool49 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnDelete");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool1 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnbtnPrint");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool62 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnViewList");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool5 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopTemplate");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool61 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnReset");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool64 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnHelp");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnClose");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab2 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon2");
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("UltraToolbar1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool39 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnBack");
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool40 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnForward");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnAdd");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool42 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnEdit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool43 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnSave");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool44 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnDelete");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool46 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnPost");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool47 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnReset");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool48 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnViewList");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool8 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopUtilities");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool2 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopTemplate");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool6 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnbtnPrint");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool52 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnHelp");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool53 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnClose");
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar2 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("UltraToolbar2");
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnBack");
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool17 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnForward");
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnEdit");
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnSave");
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool21 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnDelete");
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnComeBack");
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnPost");
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnReset");
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnViewList");
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool29 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnHelp");
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool30 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnClose");
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.TaskPaneTool taskPaneTool4 = new Infragistics.Win.UltraWinToolbars.TaskPaneTool("TaskPaneTool1");
            Infragistics.Win.UltraWinToolbars.TaskPaneTool taskPaneTool5 = new Infragistics.Win.UltraWinToolbars.TaskPaneTool("TaskPaneTool2");
            Infragistics.Win.UltraWinToolbars.TaskPaneTool taskPaneTool6 = new Infragistics.Win.UltraWinToolbars.TaskPaneTool("TaskPaneTool3");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnUnPost");
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool36 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnTemplate0");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool3 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopTemplate");
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool9 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnpopUtilities");
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool7 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("mnbtnPrint");
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ButtonTool1");
            Infragistics.Win.UltraWinToolbars.FontListTool fontListTool2 = new Infragistics.Win.UltraWinToolbars.FontListTool("FontListTool1");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("mnbtnAdd");
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.cms4Grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmbtnAddRow = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbtnDelRow = new System.Windows.Forms.ToolStripMenuItem();
            this.FMCReceiptDetailBase_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.utmDetailBaseToolBar = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.uStatusBar = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.UCalcManager = new Infragistics.Win.UltraWinCalcManager.UltraCalcManager(this.components);
            this.cms4Grid.SuspendLayout();
            this.FMCReceiptDetailBase_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utmDetailBaseToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uStatusBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UCalcManager)).BeginInit();
            this.SuspendLayout();
            // 
            // cms4Grid
            // 
            this.cms4Grid.ImageScalingSize = new System.Drawing.Size(15, 15);
            this.cms4Grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbtnAddRow,
            this.cmbtnDelRow});
            this.cms4Grid.Name = "cms4Grid";
            this.cms4Grid.Size = new System.Drawing.Size(303, 48);
            this.cms4Grid.Opening += new System.ComponentModel.CancelEventHandler(this.cms4Grid_Opening);
            // 
            // cmbtnAddRow
            // 
            this.cmbtnAddRow.Image = global::Accounting.Properties.Resources._1437136799_add;
            this.cmbtnAddRow.Name = "cmbtnAddRow";
            this.cmbtnAddRow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Insert)));
            this.cmbtnAddRow.ShowShortcutKeys = false;
            this.cmbtnAddRow.Size = new System.Drawing.Size(302, 22);
            this.cmbtnAddRow.Text = global::Accounting.TextMessage.resSystem.String_02;
            this.cmbtnAddRow.Click += new System.EventHandler(this.cmbtnAddRow_Click);
            // 
            // cmbtnDelRow
            // 
            this.cmbtnDelRow.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.cmbtnDelRow.Name = "cmbtnDelRow";
            this.cmbtnDelRow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.cmbtnDelRow.ShowShortcutKeys = false;
            this.cmbtnDelRow.Size = new System.Drawing.Size(302, 22);
            this.cmbtnDelRow.Text = global::Accounting.TextMessage.resSystem.String_03;
            this.cmbtnDelRow.Click += new System.EventHandler(this.cmbtnDelRow_Click);
            // 
            // FMCReceiptDetailBase_Fill_Panel
            // 
            this.FMCReceiptDetailBase_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.FMCReceiptDetailBase_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FMCReceiptDetailBase_Fill_Panel.Location = new System.Drawing.Point(4, 179);
            this.FMCReceiptDetailBase_Fill_Panel.Name = "FMCReceiptDetailBase_Fill_Panel";
            this.FMCReceiptDetailBase_Fill_Panel.Size = new System.Drawing.Size(1059, 193);
            this.FMCReceiptDetailBase_Fill_Panel.TabIndex = 8;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Left
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 4;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 179);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Left";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(4, 193);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Left.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // utmDetailBaseToolBar
            // 
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Appearance = appearance1;
            this.utmDetailBaseToolBar.DesignerFlags = 1;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.DockAreaAppearance = appearance2;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.DockAreaVerticalAppearance = appearance3;
            this.utmDetailBaseToolBar.DockWithinContainer = this;
            this.utmDetailBaseToolBar.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.utmDetailBaseToolBar.FormDisplayStyle = Infragistics.Win.UltraWinToolbars.FormDisplayStyle.RoundedSizable;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.HorizontalToolbarGrabHandleAppearance = appearance5;
            this.utmDetailBaseToolBar.MenuAnimationStyle = Infragistics.Win.UltraWinToolbars.MenuAnimationStyle.Slide;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.Appearance = appearance6;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.EditAppearance = appearance7;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.HotTrackAppearance = appearance8;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.IconAreaAppearance = appearance9;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.IconAreaExpandedAppearance = appearance10;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.PressedAppearance = appearance11;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.SideStripAppearance = appearance12;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.MenuSettings.ToolAppearance = appearance13;
            this.utmDetailBaseToolBar.MiniToolbar.ToolRowCount = 1;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.Appearance = appearance14;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.HotTrackAppearance = appearance15;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.IconAreaAppearance = appearance16;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.NavigationToolbar.MenuSettings.PressedAppearance = appearance17;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.ApplicationMenu2010.HeaderAppearance = appearance18;
            this.utmDetailBaseToolBar.Ribbon.ApplicationMenuButtonImage = global::Accounting.Properties.Resources.logox48;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.CaptionAreaActiveAppearance = appearance19;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.CaptionAreaAppearance = appearance20;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonActiveAppearance = appearance21;
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.BackColor2 = System.Drawing.Color.Transparent;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance22.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance22.ImageAlpha = Infragistics.Win.Alpha.Transparent;
            appearance22.ImageBackgroundAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonAppearance = appearance22;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonCaption = "";
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonCaptionVisible = false;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance23.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.Ribbon.FileMenuButtonHotTrackAppearance = appearance23;
            this.utmDetailBaseToolBar.Ribbon.GroupBorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            appearance24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance24.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance24.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance24.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance24.TextHAlignAsString = "Center";
            appearance24.TextVAlignAsString = "Middle";
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.Appearance = appearance24;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.CanCollapse = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance25.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.CaptionAppearance = appearance25;
            appearance26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.CollapsedAppearance = appearance26;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.EditAppearance = appearance27;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance28.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance28.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance28.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackAppearance = appearance28;
            appearance29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance29.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance29.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackCaptionAppearance = appearance29;
            appearance30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackCollapsedAppearance = appearance30;
            appearance31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance31.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance31.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance31.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.HotTrackGroupAppearance = appearance31;
            appearance32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.PressedAppearance = appearance32;
            appearance33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.PressedCollapsedAppearance = appearance33;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance34.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance34.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(227)))), ((int)(((byte)(228)))));
            appearance34.Cursor = System.Windows.Forms.Cursors.Hand;
            appearance34.ForeColor = System.Drawing.Color.Black;
            appearance34.ImageBackgroundAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.Ribbon.GroupSettings.ToolAppearance = appearance34;
            ribbonTab1.Caption = "Home";
            ribbonGroup1.Caption = "";
            buttonTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool2.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1,
            buttonTool2});
            ribbonGroup2.Caption = "";
            buttonTool8.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool59.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool60.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool34.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool35.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool49.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            popupMenuTool1.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool62.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            popupMenuTool5.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool61.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool64.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.MinimumSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            buttonTool11.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large;
            ribbonGroup2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool8,
            buttonTool59,
            buttonTool60,
            buttonTool34,
            buttonTool35,
            buttonTool49,
            popupMenuTool1,
            buttonTool62,
            popupMenuTool5,
            buttonTool61,
            buttonTool64,
            buttonTool11});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1,
            ribbonGroup2});
            ribbonTab2.Caption = "Tiện ích";
            this.utmDetailBaseToolBar.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1,
            ribbonTab2});
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.RibbonAreaAppearance = appearance35;
            appearance36.BackColor = System.Drawing.Color.White;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance36.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance36.BorderColor2 = System.Drawing.Color.Transparent;
            appearance36.BorderColor3DBase = System.Drawing.Color.Transparent;
            appearance36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(91)))));
            this.utmDetailBaseToolBar.Ribbon.TabAreaAppearance = appearance36;
            appearance37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance37.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance37.ForeColor = System.Drawing.Color.Black;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.ActiveTabItemAppearance = appearance37;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance38.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance38.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance38.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(91)))));
            this.utmDetailBaseToolBar.Ribbon.TabSettings.Appearance = appearance38;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.ClientAreaAppearance = appearance39;
            appearance40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance40.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(219)))), ((int)(((byte)(220)))));
            appearance40.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance40.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.utmDetailBaseToolBar.Ribbon.TabSettings.HotTrackSelectedTabItemAppearance = appearance40;
            appearance41.BackColor = System.Drawing.Color.White;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance41.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            appearance41.BorderColor2 = System.Drawing.Color.White;
            appearance41.BorderColor3DBase = System.Drawing.Color.White;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.HotTrackTabItemAppearance = appearance41;
            appearance42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance42.ForeColor = System.Drawing.Color.Black;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.SelectedAppearance = appearance42;
            appearance43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance43.ForeColor = System.Drawing.Color.Black;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.SelectedTabItemAppearance = appearance43;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.Ribbon.TabSettings.TabItemAppearance = appearance44;
            this.utmDetailBaseToolBar.Ribbon.Visible = true;
            this.utmDetailBaseToolBar.ShowFullMenusDelay = 500;
            this.utmDetailBaseToolBar.ShowShortcutsInToolTips = true;
            this.utmDetailBaseToolBar.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2007;
            ultraToolbar1.DockedColumn = 0;
            ultraToolbar1.DockedRow = 0;
            ultraToolbar1.FloatingLocation = new System.Drawing.Point(124, 412);
            ultraToolbar1.FloatingSize = new System.Drawing.Size(107, 24);
            ultraToolbar1.IsMainMenuBar = true;
            appearance45.ImageVAlign = Infragistics.Win.VAlign.Bottom;
            buttonTool39.InstanceProps.AppearancesSmall.AppearanceOnToolbar = appearance45;
            ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool39,
            buttonTool40,
            buttonTool5,
            buttonTool42,
            buttonTool43,
            buttonTool44,
            buttonTool46,
            buttonTool47,
            buttonTool48,
            popupMenuTool8,
            popupMenuTool2,
            popupMenuTool6,
            buttonTool52,
            buttonTool53});
            ultraToolbar2.DockedColumn = 0;
            ultraToolbar2.DockedRow = 1;
            appearance46.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance46.ImageVAlign = Infragistics.Win.VAlign.Bottom;
            ultraToolbar2.Settings.Appearance = appearance46;
            this.utmDetailBaseToolBar.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1,
            ultraToolbar2});
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.Appearance = appearance47;
            this.utmDetailBaseToolBar.ToolbarSettings.BorderStyleDocked = Infragistics.Win.UIElementBorderStyle.None;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.DockedAppearance = appearance48;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.EditAppearance = appearance49;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.FloatingAppearance = appearance50;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.HotTrackAppearance = appearance51;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.PressedAppearance = appearance52;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.utmDetailBaseToolBar.ToolbarSettings.ToolAppearance = appearance53;
            this.utmDetailBaseToolBar.ToolbarSettings.UseLargeImages = Infragistics.Win.DefaultableBoolean.False;
            appearance54.Image = global::Accounting.Properties.Resources.ubtnBack;
            buttonTool16.SharedPropsInternal.AppearancesSmall.Appearance = appearance54;
            buttonTool16.SharedPropsInternal.Caption = "Lùi";
            buttonTool16.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance55.Image = global::Accounting.Properties.Resources.ubtnForward;
            buttonTool17.SharedPropsInternal.AppearancesSmall.Appearance = appearance55;
            buttonTool17.SharedPropsInternal.Caption = "Tiến";
            buttonTool17.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance56.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            buttonTool19.SharedPropsInternal.AppearancesLarge.Appearance = appearance56;
            appearance57.Image = global::Accounting.Properties.Resources.iEdit;
            buttonTool19.SharedPropsInternal.AppearancesSmall.Appearance = appearance57;
            buttonTool19.SharedPropsInternal.Caption = "Sửa";
            buttonTool19.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool19.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlE;
            appearance58.Image = global::Accounting.Properties.Resources.ubtnSave1;
            buttonTool20.SharedPropsInternal.AppearancesLarge.Appearance = appearance58;
            appearance59.Image = global::Accounting.Properties.Resources.ubtnSave;
            buttonTool20.SharedPropsInternal.AppearancesSmall.Appearance = appearance59;
            buttonTool20.SharedPropsInternal.Caption = "Lưu";
            buttonTool20.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool20.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            appearance60.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            buttonTool21.SharedPropsInternal.AppearancesLarge.Appearance = appearance60;
            appearance61.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            buttonTool21.SharedPropsInternal.AppearancesSmall.Appearance = appearance61;
            buttonTool21.SharedPropsInternal.Caption = "Xóa";
            buttonTool21.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool21.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            appearance62.Image = global::Accounting.Properties.Resources.ubtnComeBack;
            buttonTool22.SharedPropsInternal.AppearancesSmall.Appearance = appearance62;
            buttonTool22.SharedPropsInternal.Caption = "Trở về";
            buttonTool22.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance63.Image = global::Accounting.Properties.Resources.ubtnPost;
            buttonTool23.SharedPropsInternal.AppearancesLarge.Appearance = appearance63;
            appearance64.Image = global::Accounting.Properties.Resources.ubtnPost;
            buttonTool23.SharedPropsInternal.AppearancesSmall.Appearance = appearance64;
            buttonTool23.SharedPropsInternal.Caption = "Ghi sổ";
            buttonTool23.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool23.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            appearance65.Image = global::Accounting.Properties.Resources.ubtnReset;
            buttonTool24.SharedPropsInternal.AppearancesSmall.Appearance = appearance65;
            buttonTool24.SharedPropsInternal.Caption = "Làm mới";
            buttonTool24.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool24.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.F5;
            appearance66.Image = global::Accounting.Properties.Resources.ubtnViewList;
            buttonTool25.SharedPropsInternal.AppearancesLarge.Appearance = appearance66;
            appearance67.Image = global::Accounting.Properties.Resources.ubtnViewList;
            buttonTool25.SharedPropsInternal.AppearancesSmall.Appearance = appearance67;
            buttonTool25.SharedPropsInternal.Caption = "Duyệt";
            buttonTool25.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance68.Image = global::Accounting.Properties.Resources.ubtnHelp;
            buttonTool29.SharedPropsInternal.AppearancesSmall.Appearance = appearance68;
            buttonTool29.SharedPropsInternal.Caption = "Giúp";
            buttonTool29.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance69.Image = global::Accounting.Properties.Resources.ubtnClose;
            buttonTool30.SharedPropsInternal.AppearancesSmall.Appearance = appearance69;
            buttonTool30.SharedPropsInternal.Caption = "Đóng";
            buttonTool30.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool30.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.AltF4;
            taskPaneTool4.SharedPropsInternal.Caption = "TaskPaneTool1";
            taskPaneTool5.SharedPropsInternal.Caption = "TaskPaneTool2";
            taskPaneTool6.SharedPropsInternal.Caption = "TaskPaneTool3";
            appearance70.Image = global::Accounting.Properties.Resources.boghi;
            buttonTool32.SharedPropsInternal.AppearancesLarge.Appearance = appearance70;
            appearance71.Image = global::Accounting.Properties.Resources.boghi;
            buttonTool32.SharedPropsInternal.AppearancesSmall.Appearance = appearance71;
            buttonTool32.SharedPropsInternal.Caption = "Bỏ ghi sổ";
            buttonTool32.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlB;
            buttonTool36.SharedPropsInternal.Caption = "Mẫu chuẩn";
            appearance72.Image = global::Accounting.Properties.Resources.ubtnTemplate;
            popupMenuTool3.SharedPropsInternal.AppearancesSmall.Appearance = appearance72;
            popupMenuTool3.SharedPropsInternal.Caption = "Mẫu";
            popupMenuTool3.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance73.Image = global::Accounting.Properties.Resources.ubtnUtilities;
            popupMenuTool9.SharedPropsInternal.AppearancesSmall.Appearance = appearance73;
            popupMenuTool9.SharedPropsInternal.Caption = "Tiện ích     ";
            popupMenuTool9.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance74.Image = global::Accounting.Properties.Resources.ubtnPrint;
            popupMenuTool7.SharedPropsInternal.AppearancesSmall.Appearance = appearance74;
            popupMenuTool7.SharedPropsInternal.Caption = "In";
            popupMenuTool7.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            popupMenuTool7.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            buttonTool4.SharedPropsInternal.Caption = "ButtonTool1";
            fontListTool2.SharedPropsInternal.Caption = "FontListTool1";
            appearance75.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            buttonTool6.SharedPropsInternal.AppearancesLarge.Appearance = appearance75;
            appearance76.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            buttonTool6.SharedPropsInternal.AppearancesSmall.Appearance = appearance76;
            buttonTool6.SharedPropsInternal.Caption = "Thêm";
            buttonTool6.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            buttonTool6.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.utmDetailBaseToolBar.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool16,
            buttonTool17,
            buttonTool19,
            buttonTool20,
            buttonTool21,
            buttonTool22,
            buttonTool23,
            buttonTool24,
            buttonTool25,
            buttonTool29,
            buttonTool30,
            taskPaneTool4,
            taskPaneTool5,
            taskPaneTool6,
            buttonTool32,
            buttonTool36,
            popupMenuTool3,
            popupMenuTool9,
            popupMenuTool7,
            buttonTool4,
            fontListTool2,
            buttonTool6});
            appearance77.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance77.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.utmDetailBaseToolBar.VerticalToolbarGrabHandleAppearance = appearance77;
            this.utmDetailBaseToolBar.BeforeApplicationMenuDropDown += new System.ComponentModel.CancelEventHandler(this.utmDetailBaseToolBar_BeforeApplicationMenuDropDown);
            this.utmDetailBaseToolBar.BeforeToolActivate += new Infragistics.Win.UltraWinToolbars.CancelableToolEventHandler(this.utmDetailBaseToolBar_BeforeToolActivate);
            this.utmDetailBaseToolBar.BeforeToolbarListDropdown += new Infragistics.Win.UltraWinToolbars.BeforeToolbarListDropdownEventHandler(this.utmDetailBaseToolBar_BeforeToolbarListDropdown);
            this.utmDetailBaseToolBar.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.utmDetailBaseToolBar_ToolClick);
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Right
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 4;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(1063, 179);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Right";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(4, 193);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Right.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Bottom
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 372);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Bottom";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(1067, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // uStatusBar
            // 
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            this.uStatusBar.Appearance = appearance4;
            this.uStatusBar.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uStatusBar.Location = new System.Drawing.Point(0, 372);
            this.uStatusBar.Name = "uStatusBar";
            this.uStatusBar.Size = new System.Drawing.Size(1067, 24);
            this.uStatusBar.TabIndex = 13;
            // 
            // _FMCReceiptDetail_Toolbars_Dock_Area_Top
            // 
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Name = "_FMCReceiptDetail_Toolbars_Dock_Area_Top";
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(1067, 179);
            this._FMCReceiptDetail_Toolbars_Dock_Area_Top.ToolbarsManager = this.utmDetailBaseToolBar;
            // 
            // UCalcManager
            // 
            this.UCalcManager.ContainingControl = this;
            // 
            // DetailBase
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(243)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(1067, 396);
            this.Controls.Add(this.FMCReceiptDetailBase_Fill_Panel);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this.uStatusBar);
            this.Controls.Add(this._FMCReceiptDetail_Toolbars_Dock_Area_Top);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "DetailBase";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.DetailBase_Closing);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DetailBase_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DetailBase_FormClosed);
            this.Load += new System.EventHandler(this.DetailBase_Load);
            this.Shown += new System.EventHandler(this.DetailBase_Shown);
            this.cms4Grid.ResumeLayout(false);
            this.FMCReceiptDetailBase_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utmDetailBaseToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uStatusBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UCalcManager)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        void DetailBase_Closing(object sender, CancelEventArgs e)
        {
            //Utils.DisableAllEvent = false;
        }

        private void utmDetailBaseToolBar_BeforeApplicationMenuDropDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void cmbtnDelRow_Click(object sender, EventArgs e)
        {
            UltraGrid uGrid = this.FindGridIsActived();
            if (uGrid == null)
            {
                var @tabControl =
                        (UltraTabControl)Controls.Find("ultraTabControl", true).FirstOrDefault();
                if (@tabControl != null)
                {
                    int index = @tabControl.ActiveTab.Index;
                    uGrid = (UltraGrid)@tabControl.ActiveTab.TabPage.Controls.Find("uGrid" + index, true).FirstOrDefault();
                    if (uGrid == null) return;
                }
                else
                {
                    uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGrid == null) return;
                }
            }
            if (uGrid.ExistsTag("Type"))
            {
                var val = ((Dictionary<string, object>)uGrid.Tag).FirstOrDefault(p => p.Key.Equals("Type")).Value;
                if (val == typeof(PPService)) return;
            }
            this.RemoveRow4Grid<T>(uGrid);
        }

        private void cmbtnAddRow_Click(object sender, EventArgs e)
        {
            UltraGrid uGrid = this.FindGridIsActived();
            if (uGrid == null || uGrid.Name.Equals("uGridControl"))
            {
                var @tabControl =
                        (UltraTabControl)Controls.Find("ultraTabControl", true).FirstOrDefault();
                if (@tabControl != null)
                {
                    int index = @tabControl.ActiveTab.Index;
                    uGrid = (UltraGrid)@tabControl.ActiveTab.TabControl.Controls.Find("uGrid" + index, true).FirstOrDefault();
                    if (uGrid == null) return;
                }
                else
                {
                    uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                    if (uGrid == null) return;
                }
            }
            if (uGrid.ExistsTag("Type"))
            {
                if (typeof(T) == typeof(FADepreciation))
                {
                    this.AddNewRow4GridFAD<T>(uGrid);
                    return;
                }
                else if (typeof(T) == typeof(TIAllocation))
                {
                    this.AddNewRow4GridTIA<T>(uGrid);
                    return;
                }
                else if (typeof(T) == typeof(GOtherVoucher) && _typeID == 690)
                {
                    this.AddNewRow4GridGOV<T>(uGrid);
                    return;
                }
                if (typeof(T) == typeof(PPService)) return;
            }
            Utils.AddNewRow4Grid(uGrid);
        }

        #region Utils
        protected List<Control> _controls = new List<Control>();
        protected void AutoCaculatorTotal(List<Control> controls)
        {
            foreach (Control control in controls)
                control.TextChanged += DetailBase_TextChanged;
        }

        void DetailBase_TextChanged(object sender, EventArgs e)
        {
            var ultraLabel = (UltraLabel)sender;
            if (!string.IsNullOrEmpty(ultraLabel.Text))
                ultraLabel.FormatNumberic(ultraLabel.Text, ConstDatabase.Format_TienVND);
            if (ultraLabel.Name.Contains("Amount") && !ultraLabel.Name.Contains("Stand")) CaculatorAmout();
        }

        public virtual void CaculatorAmout()
        {
            decimal total = 0;
            decimal totalOriginal = 0;
            foreach (Control control in _controls)
            {
                if (!control.Name.Contains("Amount") || control.Name.Contains("Stand")) continue;
                //Control tiền, không phải tổng tiền, không phải nguyên tệ (Original)
                if (!control.Name.Contains("Original"))
                    total += control.Name.Contains("Sub") ? decimal.Parse(control.Text) * (-1) : decimal.Parse(control.Text);
                else
                    totalOriginal += control.Name.Contains("Sub") ? decimal.Parse(control.Text) * (-1) : decimal.Parse(control.Text);
            }
            Control controlTotal = _controls.FirstOrDefault(k => k.Name.Contains("Amount") && k.Name.Contains("Stand") && !k.Name.Contains("Original"));
            if (controlTotal != null) controlTotal.Text = total.ToString();
            Control controlTotalOriginal = _controls.FirstOrDefault(k => k.Name.Contains("Amount") && k.Name.Contains("Stand") && k.Name.Contains("Original"));
            if (controlTotalOriginal != null) controlTotalOriginal.Text = totalOriginal.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="strKeys"></param>
        /// <param name="strTypes">0:TextBox, 1:CheckBox, 2:Datetime, 3:Label</param>
        protected void DataBinding(List<Control> controls, string strKeys, string strTypes = "", bool autoFormat = false)
        {
            List<string> types = new List<string>();
            if (string.IsNullOrEmpty(strTypes)) types.AddRange(controls.Select(control => "0"));    //mặc định là toàn textbox
            else types = strTypes.Split(',').ToList();

            List<string> keys = strKeys.Split(',').ToList();
            for (int i = 0; i < controls.Count; i++)
            {
                try
                {
                    controls[i].DataBindings.Clear();
                    var key = keys[i].Trim();
                    var bsource = new BindingSource { DataSource = _select };
                    //bsource.ListChanged += new ListChangedEventHandler(bsource_ListChanged);
                    bsource.CurrentItemChanged += new EventHandler((s, e) => bsource_CurrentItemChanged(this, s, e));
                    bsource.CurrencyManager.ItemChanged += new ItemChangedEventHandler((s, e) => CurrencyManager_ItemChanged(this, s, e));
                    if (types[i].Trim().Equals("0")) controls[i].DataBindings.Add("Text", bsource, key, true, DataSourceUpdateMode.OnPropertyChanged);
                    else if (types[i].Trim().Equals("1")) controls[i].DataBindings.Add("CheckedValue", bsource, key, false, DataSourceUpdateMode.OnPropertyChanged);
                    else if (types[i].Trim().Equals("2")) controls[i].DataBindings.Add("Value", bsource, key, true, DataSourceUpdateMode.OnPropertyChanged);
                    else if (types[i].Trim().Equals("3"))
                    {
                        if (autoFormat)
                            controls[i].BindControl("Text", _select, key, ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
                        else
                            controls[i].BindControl("Text", _select, key, ConstDatabase.Format_TienVND);
                    }
                    else if (types[i].Trim().Equals("4")) controls[i].DataBindings.Add("CheckedIndex", bsource, key, true, DataSourceUpdateMode.OnPropertyChanged);
                }
                catch (Exception ex)
                {

                }
            }
        }

        void bsource_CurrentItemChanged(Form @this, object sender, EventArgs e)
        {
            var bsource = (BindingSource)sender;
            var key = "";
            if (@this.ActiveControl != null)
            {
                var control = @this.ActiveControl.Parent;
                if (control.DataBindings.Count > 0)
                {
                    if (typeof(T) == typeof(PPInvoice))
                    {
                        var key1s = new[] { "ContactName"/*, "Reason"*/ };
                        var key2s = new[] { "MContactName"/*, "MReasonPay"*/ };
                        var bmember = control.DataBindings[0].BindingMemberInfo.BindingMember;
                        if (bsource.Current.HasProperty(bmember))
                        {
                            for (int i = 0; i < key1s.Length; i++)
                            {
                                if (key1s[i] == bmember)
                                {
                                    if (!bsource.Current.HasProperty(key2s[i])) continue;
                                    key = key2s[i];
                                    break;
                                }
                                else if (key2s[i] == bmember)
                                {
                                    if (!bsource.Current.HasProperty(key1s[i])) continue;
                                    key = key1s[i];
                                    break;
                                }
                            }
                            if (!string.IsNullOrEmpty(key))
                            {
                                bsource.Current.SetProperty(key, bsource.Current.GetProperty(bmember));
                                bsource.EndEdit();
                                //bsource.ResetBindings(false);
                            }
                        }
                    }
                }
            }
        }

        void CurrencyManager_ItemChanged(Form @this, object sender, ItemChangedEventArgs e)
        {
            var curMng = (System.Windows.Forms.CurrencyManager)sender;
            if (curMng.Current != null)
            {

            }
        }

        void bsource_ListChanged(object sender, ListChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="strKey1s"></param>
        /// <param name="strKey2s"></param>
        /// <param name="strTypes">0:TextBox, 1:CheckBox, 2:Datetime, 3:Label</param>
        protected void DataBinding2(List<Control> controls, string strKey1s, string strKey2s, string strTypes = "", bool autoFormat = false)
        {
            List<string> types = new List<string>();
            if (string.IsNullOrEmpty(strTypes)) types.AddRange(controls.Select(control => "0"));    //mặc định là toàn textbox
            else types = strTypes.Split(',').ToList();

            List<string> key1s = strKey1s.Split(',').ToList();
            List<string> key2s = strKey2s.Split(',').ToList();
            for (int i = 0; i < controls.Count; i++)
            {
                controls[i].DataBindings.Clear();
                if (types[i].Trim().Equals("0"))
                {
                    //                    var multiBinding = new MultiBinding();
                    //multiBinding.Bindings.Add(new Binding("Text", this.bev_BindingSource, "countryRegion", true));
                    //multiBinding.Bindings.Add(new Binding("Text", this.bev_BindingSource, "country", true));
                    //multiBinding.Converter = new MyMultiValueConverter();
                    controls[i].DataBindings.Add("Text", new BindingSource { DataSource = _select }, key1s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                    controls[i].DataBindings.Add("Text", new BindingSource { DataSource = _select }, key2s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
                else if (types[i].Trim().Equals("1"))
                {
                    controls[i].DataBindings.Add("CheckedValue", new BindingSource { DataSource = _select }, key1s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                    controls[i].DataBindings.Add("CheckedValue", new BindingSource { DataSource = _select }, key2s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
                else if (types[i].Trim().Equals("2"))
                {
                    controls[i].DataBindings.Add("Value", new BindingSource { DataSource = _select }, key1s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                    controls[i].DataBindings.Add("Value", new BindingSource { DataSource = _select }, key2s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
                else if (types[i].Trim().Equals("3"))
                {
                    if (autoFormat)
                    {
                        controls[i].BindControl("Text", _select, key1s[i].Trim(), ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
                        controls[i].BindControl("Text", _select, key2s[i].Trim(), ConstDatabase.Format_TienVND, ConstDatabase.Format_ForeignCurrency);
                    }
                    else
                    {
                        controls[i].BindControl("Text", _select, key1s[i].Trim(), ConstDatabase.Format_TienVND);
                        controls[i].BindControl("Text", _select, key2s[i].Trim(), ConstDatabase.Format_TienVND);
                    }
                }
                else if (types[i].Trim().Equals("4"))
                {
                    controls[i].DataBindings.Add("CheckedIndex", new BindingSource { DataSource = _select }, key1s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                    controls[i].DataBindings.Add("CheckedIndex", new BindingSource { DataSource = _select }, key2s[i].Trim(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
            }
        }

        protected void ConfigGridBase(T input, UltraGrid uGridControlInput, UltraPanel pnlUgrid, bool haveUgridControl = true)
        {
            #region Config Grid
            if (!input.HasProperty("TypeID") || !input.HasProperty("TemplateID")) return;
            int typeIdInput = input.GetProperty<T, int>("TypeID");
            Guid? templateIdInput = input.GetProperty<T, Guid?>("TemplateID");
            //Load config hiển thị dữ liệu từ Database
            Template mauGiaoDien = Utils.GetMauGiaoDien(typeIdInput, templateIdInput, Utils.ListTemplate);
            _select.SetProperty("TemplateID", _statusForm == ConstFrm.optStatusForm.Add ? mauGiaoDien.ID : templateIdInput);
            this.ConfigGridByTemplete_General<T>(pnlUgrid, mauGiaoDien);
            this.ConfigGridByTemplete<T>(typeIdInput, mauGiaoDien, true, _listObjectInput);
            if (haveUgridControl && input.HasProperty("TotalAmountOriginal") && input.HasProperty("TotalAmount") && input.HasProperty("CurrencyID"))
            {
                decimal totalAmountOriginalInput = input.GetProperty<T, decimal>("TotalAmountOriginal");
                decimal totalAmountInput = input.GetProperty<T, decimal>("TotalAmount");
                string currencyIdInput = input.GetProperty<T, string>("CurrencyID");
                //config Grid
                _select.SetProperty("TotalAmountOriginal", _statusForm == ConstFrm.optStatusForm.Add ? 0 : totalAmountOriginalInput);
                _select.SetProperty("TotalAmount", _statusForm == ConstFrm.optStatusForm.Add ? 0 : totalAmountInput);
                _select.SetProperty("CurrencyID", _statusForm == ConstFrm.optStatusForm.Add ? "VND" : currencyIdInput);
                if (_select.HasProperty("IsImportPurchase")) _select.SetProperty("IsImportPurchase", _statusForm == ConstFrm.optStatusForm.Add ? false : input.GetProperty<T, bool?>("IsImportPurchase"));
                this.ConfigGridCurrencyByTemplate<T>(typeIdInput, new BindingList<IList> { new List<T> { _select } }, uGridControlInput, mauGiaoDien);
            }
            if (input.HasProperty("TypeID") && input.HasProperty("Exported"))
            {
                int type = int.Parse(_select.GetProperty("TypeID").ToString());
                var isExported = (bool)_select.GetProperty("Exported");
                var ultraGrid = (UltraGrid)pnlUgrid.Controls.Find("uGrid1", true).FirstOrDefault();
                ultraGrid.ConfigColumnByExported(isExported, _select.GetProperty("CurrencyID").ToString(), type);
                //ultraGrid.ConfigColumnByImportPurchase(isExported, _select.GetProperty("CurrencyID").ToString(), _statusForm);
            }
            var uGrid0 = (UltraGrid)pnlUgrid.Controls.Find("uGrid0", true).FirstOrDefault();
            #endregion
        }

        RSInwardOutward GetRsInwardOutward()
        {
            RSInwardOutward temp = new RSInwardOutward();
            if (_statusForm == ConstFrm.optStatusForm.Add)
            {//tạo mới phiếu xuất kho
             //
            }
            else if (_statusForm == ConstFrm.optStatusForm.Edit)
            {//sửa phiếu xuất kho
                temp = new RSInwardOutward();
            }
            return temp;
        }

        protected void fSelectType_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                var frm = (FSelectType)sender;
                if (e.CloseReason != CloseReason.UserClosing) return;
                if (frm.DialogResult == DialogResult.OK)
                {
                    int TypeID = frm.TypeID;
                    switch (TypeID)
                    {
                        case 900:
                        case 901:
                            var f = new FIWSelectOrders(TypeID);
                            f.FormClosed += new FormClosedEventHandler(fIWSelectOrders_FormClosed);
                            f.ShowDialog(this);
                            break;
                        case 310:
                            var fsa = new FSASelectOrders(_select.GetProperty<T, Guid?>("AccountingObjectID"));
                            fsa.FormClosed += new FormClosedEventHandler(fSASelectOrders_FormClosed);
                            fsa.ShowDialog(this);
                            break;
                        case 200:
                            var fpp = new FPPSelectOrderrs(_select.GetProperty<T, Guid?>("AccountingObjectID"));
                            fpp.FormClosed += new FormClosedEventHandler(fPPSelectOrders_FormClosed);
                            fpp.ShowDialog(this);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MSG.Warning(string.Format(MSG.MSGErrorForm, ex.ToString()));
                return;
            }
        }
        private void fSASelectOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FSASelectOrders)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectOrders = frm.SelectOrders;
                var uGrid1 = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid1 != null)
                {
                    var source = (BindingList<RSInwardOutwardDetail>)uGrid1.DataSource;
                    if (selectOrders.Count > 0) source.Clear();
                    if (_select.HasProperty("RSAssemblyDismantlementID"))
                    {
                        _select.SetProperty("RSAssemblyDismantlementID", null);
                    }
                    foreach (var selectOrder in selectOrders)
                    {
                        source.Add(new RSInwardOutwardDetail()
                        {
                            MaterialGoodsID = selectOrder.MaterialGoodsID,
                            Description = selectOrder.MaterialGoodsName,
                            RepositoryID = selectOrder.SaOrderDetail.RepositoryID,
                            DebitAccount = selectOrder.SaOrderDetail.DebitAccount,
                            CreditAccount = selectOrder.SaOrderDetail.CreditAccount,
                            Unit = selectOrder.SaOrderDetail.Unit,
                            UnitConvert = selectOrder.SaOrderDetail.UnitConvert,
                            //UnitPrice = selectOrder.SaOrderDetail.UnitPrice,
                            //UnitPriceOriginal = selectOrder.SaOrderDetail.UnitPriceOriginal,
                            Quantity = selectOrder.QuantityOutward,
                            //Amount = selectOrder.SaOrderDetail.UnitPrice * (selectOrder.QuantityOutward ?? 0),
                            //AmountOriginal = selectOrder.SaOrderDetail.UnitPriceOriginal * (selectOrder.QuantityOutward ?? 0),
                            AccountingObjectID = selectOrder.AccountingObjectID,
                            DetailID = selectOrder.SaOrderDetail.ID,
                            SAOrderNo = selectOrder.No
                        });
                    }
                }
                foreach (var x in uGrid1.Rows)
                {
                    x.Cells["OWPurpose"].Value = 1;
                }
            }
        }

        private void fPPSelectOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FPPSelectOrderrs)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectOrders = frm.SelectOrders;
                var uGrid1 = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid1 != null)
                {
                    var source = (BindingList<RSInwardOutwardDetail>)uGrid1.DataSource;
                    if (selectOrders.Count > 0) source.Clear();
                    foreach (var selectOrder in selectOrders)
                    {
                        source.Add(new RSInwardOutwardDetail()
                        {
                            MaterialGoodsID = selectOrder.MaterialGoodsID,
                            Description = selectOrder.MaterialGoodsName,
                            RepositoryID = selectOrder.PpOrderDetail.RepositoryID,
                            DebitAccount = selectOrder.PpOrderDetail.DebitAccount,
                            CreditAccount = selectOrder.PpOrderDetail.CreditAccount,
                            Unit = selectOrder.PpOrderDetail.Unit,
                            UnitConvert = selectOrder.PpOrderDetail.UnitConvert,
                            UnitPrice = selectOrder.PpOrderDetail.UnitPrice,
                            UnitPriceOriginal = selectOrder.PpOrderDetail.UnitPriceOriginal,
                            Quantity = selectOrder.QuantityInward,
                            Amount = selectOrder.PpOrderDetail.UnitPrice * (selectOrder.QuantityInward ?? 0),
                            AmountOriginal = selectOrder.PpOrderDetail.UnitPriceOriginal * (selectOrder.QuantityInward ?? 0),
                            AccountingObjectID = selectOrder.AccountingObjectID,
                            DetailID = selectOrder.PpOrderDetail.ID,
                            SAOrderNo = selectOrder.No
                        });
                    }

                }
            }
        }
        protected void fIWSelectOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            var frm = (FIWSelectOrders)sender;
            if (e.CloseReason != CloseReason.UserClosing) return;
            if (frm.DialogResult == DialogResult.OK)
            {
                var selectOrders = frm.SelectOrders;
                var uGrid1 = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                var uGrid2 = (UltraGrid)Controls.Find("uGrid2", true).FirstOrDefault();
                if (uGrid1 != null)
                {
                    var source = (BindingList<RSInwardOutwardDetail>)uGrid1.DataSource;
                    var source1 = (BindingList<RefVoucherRSInwardOutward>)uGrid2.DataSource;
                    if (selectOrders != null) source.Clear();

                    if (selectOrders is RSAssemblyDismantlement)
                    {
                        RSAssemblyDismantlement order = selectOrders as RSAssemblyDismantlement;
                        if (_select.HasProperty("Reason"))
                        {
                            _select.SetProperty("Reason", order.Reason);
                        }

                        if (_select.HasProperty("RSAssemblyDismantlementID"))
                        {
                            _select.SetProperty("RSAssemblyDismantlementID", order.ID);
                        }

                        foreach (var item in order.RefVouchers)
                        {
                            source1.Add(new RefVoucherRSInwardOutward
                            {
                                ID = Guid.NewGuid(),
                                RefID1 = _select.GetProperty<T, Guid>("ID"),
                                RefID2 = item.RefID2,
                                No = item.No,
                                Reason = item.Reason,
                                Date = item.Date,
                                PostedDate = item.PostedDate
                            });
                        }

                        if ((order.TypeID == 900 && TypeID == 400) || (order.TypeID == 901 && TypeID == 410))
                        {
                            source.Add(new RSInwardOutwardDetail()
                            {
                                MaterialGoodsID = order.MaterialGoodsID,
                                Description = order.MaterialGoodsName,
                                RepositoryID = order.RepositoryID,
                                Unit = order.Unit,
                                UnitPrice = order.UnitPrice,
                                UnitPriceOriginal = order.UnitPriceOriginal,
                                Quantity = order.Quantity,
                                Amount = order.Amount,
                                AmountOriginal = order.AmountOriginal,
                                DetailID = order.ID,
                            });
                        }

                        if ((order.TypeID == 900 && TypeID == 410) || (order.TypeID == 901 && TypeID == 400))
                        {
                            foreach (var detail in order.RSAssemblyDismantlementDetails)
                            {
                                source.Add(new RSInwardOutwardDetail()
                                {
                                    MaterialGoodsID = detail.MaterialGoodsID,
                                    Description = detail.Description,
                                    RepositoryID = detail.RepositoryID,
                                    //DebitAccount = detail.SaOrderDetail.DebitAccount,
                                    //CreditAccount = detail.SaOrderDetail.CreditAccount,
                                    Unit = detail.Unit,
                                    UnitConvert = detail.UnitConvert,
                                    UnitPrice = detail.UnitPrice,
                                    UnitPriceOriginal = detail.UnitPriceOriginal,
                                    Quantity = detail.Quantity,
                                    Amount = detail.Amount,
                                    AmountOriginal = detail.AmountOriginal,
                                    //AccountingObjectID = detail.AccountingObjectID,
                                    DetailID = detail.ID,
                                    LotNo = detail.LotNo,
                                    ExpiryDate = detail.ExpiryDate,
                                    ContractID = detail.ContractID,
                                    //SAOrderNo = detail.No
                                });
                            }
                        }
                    }
                }
            }
        }
        #endregion

        protected void DetailBase_Shown(object sender, EventArgs e)
        {
            if (Utils.StopWatch.IsRunning)
            {
                Utils.StopWatch.Stop();
                Utils.StopWatch.Reset();
            }
        }

        private void utmDetailBaseToolBar_BeforeToolActivate(object sender, CancelableToolEventArgs e)
        {
            //if (e.Tool.Key.Equals("mnbtnSave") || e.Tool.Key.Equals("mnbtnClose"))
            //{
            //    PropertyInfo propExchange = _select.GetType().GetProperty("ExchangeRate");
            //    if (propExchange != null && propExchange.CanWrite && propExchange.CanRead)
            //    {
            //        if (propExchange.GetValue(_select, null) == null)
            //        {
            //            propExchange.SetValue(_select, (decimal)1, null);
            //        }
            //    }
            //    if (!Utils.Checked)
            //    {
            //        UltraGrid uGrid = this.FindGridIsActived();
            //        if (uGrid != null)
            //        {
            //            uGrid.PerformAction(UltraGridAction.ExitEditMode);
            //            uGrid.Update();
            //            this.CheckAnythingOfGrid<T>(uGrid);
            //        }
            //    }
            //}
        }

        private void DetailBase_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ResetVariable();
            Utils.isSAInvoice = false;
            Utils.isPPInvoice = false;
        }

        private void DetailBase_Load(object sender, EventArgs e)
        {
            //Utils.@Form = this;
            #region Khởi chạy Trigger
            _trigger = new Thread(new ThreadStart(TriggerObject));
            _trigger.IsBackground = true;
            _trigger.Start();
            #endregion
            UCalcManager.DeferredCalculationsEnabled = true;
        }

        #region Trigger
        /// <summary>
        /// Hàm theo dõi sự thay đổi của đối tượng _select
        /// </summary>
        private void TriggerObject()
        {
            int prevTypeId = TypeID.CloneObject();
            while (true)
            {
                if (TypeID != prevTypeId)
                {
                    var type = Utils.ListType.FirstOrDefault(x => x.ID == TypeID);
                    if (type != null)
                        this.SetPropertyValue(a => a.Text, typeof(T) == typeof(PPInvoice) && (this as FPPInvoiceDetail) != null && (this as FPPInvoiceDetail).TypeFrm == 1 ? string.Format("{0} (Không qua kho)", type.TypeName) : type.TypeName);
                    prevTypeId = TypeID.CloneObject();
                }
                Thread.Sleep(1);
            }
        }
        #endregion

        protected void DetailBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsCloseByButton) //add by cuongpv
            {
                if (!IsAddInform && !_statusForm.Equals(ConstFrm.optStatusForm.View) && !_statusForm.Equals(ConstFrm.optStatusForm.Delete)
                    && ((!Utils.Compare(_backSelect, _select) || (_backListObjectInput != null && _backListObjectInput.Count > 0 && !Utils.Compare(_backListObjectInput, _listObjectInput))
                    || (_backListObjectInputPost != null && _backListObjectInputPost.Count > 0 && !Utils.Compare(_backListObjectInputPost, _listObjectInputPost))
                    || (_backListObjectInputGroup != null && _backListObjectInputGroup.Count > 0 && !Utils.Compare(_backListObjectInputGroup, _listObjectInputGroup)))))
                {
                    IsClosing = true;
                    bool status = true;
                    switch (MSG.MessageBoxStand(resSystem.MSG_System_45, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning))
                    {
                        case DialogResult.Yes:
                            if (_statusForm != ConstFrm.optStatusForm.View)
                                SendKeys.Send("{TAB}");
                            status = SaveFunction();
                            break;
                        case DialogResult.No:
                            if (!_statusForm.Equals(ConstFrm.optStatusForm.Add))

                                IsClose = true;

                            break;
                        case DialogResult.Cancel:
                            if (_statusForm != ConstFrm.optStatusForm.View)
                                //SendKeys.Send("{TAB}");
                                status = false;

                            break;
                        default:
                            break;
                    }
                    IsClosing = false;
                    e.Cancel = !status;
                }
            }//add by cuongpv

            IsCloseByButton = false;//add by cuongpv

            #region Đóng Trigger
            if (_trigger.IsAlive && _trigger.ThreadState == ThreadState.Running)
                _trigger.Abort();
            #endregion
            Utils.isSAInvoice = false;
            Utils.isPPInvoice = false;
            if (_saveSuccessfull)
                //if(typeof(T) == typeof(PPService)) 
                DialogResult = DialogResult.OK;
        }

        private void cms4Grid_Opening(object sender, CancelEventArgs e)
        {
            UltraGrid uGrid;
            var @tabControl =
                        (UltraTabControl)Controls.Find("ultraTabControl", true).FirstOrDefault();
            if (@tabControl != null)
            {
                int index = @tabControl.ActiveTab.Index;
                uGrid = (UltraGrid)@tabControl.ActiveTab.TabPage.Controls.Find("uGrid" + index, true).FirstOrDefault();
                if (uGrid == null) return;
                uGrid.Focus();
                ActiveControl = uGrid;
            }
            else
            {
                uGrid = (UltraGrid)Controls.Find("uGrid0", true).FirstOrDefault();
                if (uGrid == null) return;
                uGrid.Focus();
                ActiveControl = uGrid;
            }
            cmbtnAddRow.Enabled = _statusForm != ConstFrm.optStatusForm.View && _typeID != 620;
            if (uGrid.ExistsTag("Type"))
            {
                var val = ((Dictionary<string, object>)uGrid.Tag).FirstOrDefault(p => p.Key.Equals("Type")).Value;
                cmbtnAddRow.Enabled = !(val == typeof(PPService)) && _statusForm != ConstFrm.optStatusForm.View;
            }
            var ugrid2 = Controls.Find("uGrid1", true).FirstOrDefault() as UltraGrid;
            if (_typeID == 620)
            {
                if (uGrid == ugrid2)
                    cmbtnDelRow.Enabled = uGrid.Rows.Count > 0 && _statusForm != ConstFrm.optStatusForm.View ? true : false;
                else
                    cmbtnDelRow.Enabled = uGrid.Rows.Count > 0 && _statusForm != ConstFrm.optStatusForm.View && _typeID != 620;
            }
            else
                cmbtnDelRow.Enabled = uGrid.Rows.Count > 0 && _statusForm != ConstFrm.optStatusForm.View;


        }

        private void utmDetailBaseToolBar_BeforeToolbarListDropdown(object sender, BeforeToolbarListDropdownEventArgs e)
        {
            e.Cancel = true;
        }
    }
}