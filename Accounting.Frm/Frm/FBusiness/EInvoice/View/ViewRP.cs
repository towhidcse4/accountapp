﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Microsoft.Win32;
using Newtonsoft.Json.Linq;

namespace Accounting
{
    [ComVisible(true)]
    public partial class ViewRP : Form
    {
        private readonly bool _isCancel = false;
        int height = 0;
        ISystemOptionService _ISystemOptionService;
        ISupplierServiceService _ISupplierServiceService;
        Request request;
        SystemOption systemOption;
        int _XML;
        public ViewRP(string html, Request _request,int XML)
        {
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ISupplierServiceService = IoC.Resolve<ISupplierServiceService>();
            systemOption = _ISystemOptionService.GetByCode("EI_IDNhaCungCapDichVu");
            request = _request;
            _XML = XML;
            //_isCancel = status == 5;
            InitializeComponent();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(ViewRP));
            height = Screen.PrimaryScreen.WorkingArea.Size.Height - 30;
            var width = Screen.PrimaryScreen.WorkingArea.Size.Width;
            this.ultraPanel2.Size = new Size(width, height - 31);
            this.browser.Size = new Size(width, height - 31);
            this.btnPrint.Image = (Image)componentResourceManager.GetObject("btnPrint.Image");
            //this.btnPrint.Location = new System.Drawing.Point(width / 2 - this.btnPrint.Size.Width / 2, -1);
            this.btnPrint.Location = new System.Drawing.Point(width - this.btnPrint.Size.Width, -1);
            this.btnDownloadXLS.Location = new System.Drawing.Point(width - this.btnDownloadXLS.Size.Width - this.btnPrint.Size.Width, -1);
            this.btnDownloadXML.Location = new System.Drawing.Point(width - this.btnDownloadXML.Size.Width - this.btnPrint.Size.Width - this.btnDownloadXLS.Size.Width, -1);
            InternetExplorerBrowserEmulation.SetBrowserEmulationVersion();
            WindowState = FormWindowState.Maximized;
            this.btnPrint.Enabled = !_isCancel;
            this.cancelImage.Visible = _isCancel;
            this.labelCancel.Visible = _isCancel;
            this.ClientSize = new Size(width, height);

            this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
            this.browser.ObjectForScripting = new ScriptManager(this);
            this.LoadData(html);
            btnDownloadXML.Visible = XML==0;
        }
        public ViewRP(byte[] html, Request _request)
        {
            _ISystemOptionService = IoC.Resolve<ISystemOptionService>();
            _ISupplierServiceService = IoC.Resolve<ISupplierServiceService>();
            systemOption = _ISystemOptionService.GetByCode("EI_IDNhaCungCapDichVu");
            request = _request;
            //_isCancel = status == 5;
            InitializeComponent();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(ViewRP));
            height = Screen.PrimaryScreen.WorkingArea.Size.Height - 30;
            var width = Screen.PrimaryScreen.WorkingArea.Size.Width;
            this.ultraPanel2.Size = new Size(width, height - 31);
            this.browser.Size = new Size(width, height - 31);
            this.btnPrint.Image = (Image)componentResourceManager.GetObject("btnPrint.Image");
            //this.btnPrint.Location = new System.Drawing.Point(width / 2 - this.btnPrint.Size.Width / 2, -1);
            this.btnPrint.Location = new System.Drawing.Point(width - this.btnPrint.Size.Width, -1);
            this.btnDownloadXML.Location = new System.Drawing.Point(width - this.btnDownloadXML.Size.Width - this.btnPrint.Size.Width, -1);
            this.btnDownloadXLS.Location = new System.Drawing.Point(width - this.btnDownloadXML.Size.Width - this.btnPrint.Size.Width - this.btnDownloadXLS.Size.Width, -1);
            InternetExplorerBrowserEmulation.SetBrowserEmulationVersion();
            WindowState = FormWindowState.Maximized;
            this.btnPrint.Enabled = !_isCancel;
            this.cancelImage.Visible = _isCancel;
            this.labelCancel.Visible = _isCancel;
            this.ClientSize = new Size(width, height);

            this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
            //this.browser.ObjectForScripting = new ScriptManager(this);
            this.browser.DocumentStream = new MemoryStream(html);
        }
        public void View()
        {
            this.Show();
        }
        private void LoadData(string html)
        {
            try
            {
                int products = Regex.Matches(html, "/Product", RegexOptions.IgnoreCase).Count / 10 + 1;
                if (string.IsNullOrWhiteSpace(html))
                {
                    int num2 = (int)MessageBox.Show("Lỗi dữ liệu, vui lòng thực hiện lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    this.browser.ObjectForScripting = (object)this;
                    string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    string str1 = "<script src=\"" + directoryName + "\\Content\\viewer\\jquery.min.js\"></script>" + "<script src=\"" + directoryName + "\\Content\\viewer\\main.js\"></script>" + "<link href=\"" + directoryName + "\\Content\\viewer\\styles.css\" rel=\"stylesheet\" type=\"text/css\">";
                    string str2 = html.Replace("<head>", "<head>" + str1);
                    if (products > 1)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("<div class='pagination'><a href='#' class='number' id='ap0' onclick='showPageContent(1); return false;'><<</a><a href='#' id='prev' class='prevnext' onclick='previewPage()'><</a>");
                        for (int index = 1; index <= products; ++index)
                            stringBuilder.Append("<a href='#' class='number' id='ap" + (object)index + "' onclick='showPageContent(" + (object)index + "); return false;'>" + (object)index + "</a>");
                        stringBuilder.Append("<a href='#' id='next' class='prevnext' onclick='nextPage()'>></a></div>");
                        stringBuilder.Append("<script type='text/javascript'>$(document).ready(function () {$('.VATTEMP .invtable .prds').ProductNumberPagination({ number: 10 });});</script></body>");
                        str2 = str2.Replace("</body>", stringBuilder.ToString());
                    }
                    this.browser.DocumentText = str2;
                    this.browser.ScriptErrorsSuppressed = true;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //Log.Error(ex);
                MessageBox.Show("Bạn cần sử dụng với quyền Administartor, vui lòng thực hiện lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
                MessageBox.Show("Có lỗi xảy ra, vui lòng thực hiện lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void EInvBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                RegistryKey registryKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.CurrentUser, "").OpenSubKey("Software\\Microsoft\\Internet Explorer\\PageSetup", RegistryKeyPermissionCheck.ReadWriteSubTree);
                string name1 = "(Default)";
                string str1 = "";
                registryKey.SetValue(name1, (object)str1);
                string name2 = "font";
                string str2 = "";
                registryKey.SetValue(name2, (object)str2);
                string name3 = "footer";
                string str3 = "";
                registryKey.SetValue(name3, (object)str3);
                string name4 = "header";
                string str4 = "";
                registryKey.SetValue(name4, (object)str4);
                string name5 = "margin_bottom";
                string str5 = "0.750000";
                registryKey.SetValue(name5, (object)str5);
                string name6 = "margin_left";
                string str6 = "0.750000";
                registryKey.SetValue(name6, (object)str6);
                string name7 = "margin_right";
                string str7 = "0.750000";
                registryKey.SetValue(name7, (object)str7);
                string name8 = "margin_top";
                string str8 = "0.750000";
                registryKey.SetValue(name8, (object)str8);
                string name9 = "Print_Background";
                string str9 = "yes";
                registryKey.SetValue(name9, (object)str9);
                string name10 = "Shrink_To_Fit";
                string str10 = "yes";
                registryKey.SetValue(name10, (object)str10);
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
                MessageBox.Show("Có lỗi xảy ra, vui lòng thực hiện lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            this.browser.Size = new Size(1024, height - 31);
            this.browser.ShowPrintPreviewDialog();
        }
        [ComVisible(true)]
        public class ScriptManager
        {
            private ViewRP _form;

            public ScriptManager(ViewRP form)
            {
                this._form = form;
            }

            //public void displayCert(string name)
            //{
            //    if (name.Equals("client"))
            //    {
            //        SignatureDetails signatureDetails = new SignatureDetails(InvViewer.xmlData, true);
            //        int num1 = 4;
            //        signatureDetails.StartPosition = (FormStartPosition)num1;
            //        int num2 = (int)signatureDetails.ShowDialog();
            //    }
            //    if (!name.Equals("server"))
            //        return;
            //    SignatureDetails signatureDetails1 = new SignatureDetails(InvViewer.xmlData, false);
            //    int num3 = 4;
            //    signatureDetails1.StartPosition = (FormStartPosition)num3;
            //    int num4 = (int)signatureDetails1.ShowDialog();
            //}
        }

        private void ultraPanel1_PaintClient(object sender, PaintEventArgs e)
        {

        }

        private void btnDownloadXLS_Click(object sender, EventArgs e)
        {
            if (_XML==0)
            {
                contextMenuStrip1.Show(btnDownloadXLS, new Point(0, btnDownloadXLS.Height));
            }
            else
            {
                if (systemOption != null)
                    if (!string.IsNullOrEmpty(systemOption.Data))
                    {

                        SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                        if (supplierService.SupplierServiceCode == "SDS")
                        {
                            RestApiService client = new RestApiService(_ISystemOptionService.GetByCode("EI_Path").Data, _ISystemOptionService.GetByCode("EI_TenDangNhap").Data, Core.ITripleDes.TripleDesDefaultDecryption(_ISystemOptionService.GetByCode("EI_MatKhau").Data));
                            try
                            {
                                WaitingFrm.StartWaiting();
                                request.Option = 1;
                                var response = new ResponseFile();
                                if (_XML == 1)
                                {
                                    response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BangKeHDChungTuHHDV").ApiPath, request);
                                }
                                else if (_XML == 2)
                                {
                                    response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoDoanhThuTheoSP").ApiPath, request);
                                }
                                else if (_XML == 3)
                                {
                                    response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoDoanhThuTheoBenMua").ApiPath, request);
                                }
                                
                                WaitingFrm.StopWaiting();
                                if (!string.IsNullOrEmpty(response.Content))
                                    if (Utils.IsValidJson(response.Content))
                                    {
                                        var parsed = JObject.Parse(response.Content.ToString());
                                        MSG.Error(parsed.SelectToken("Message").Value<string>());
                                        return;
                                    }
                                SaveFileDialog savefile = new SaveFileDialog();
                                // set a default file name
                                savefile.FileName = response.FileName;
                                // set filters - this can be done in properties as well
                                savefile.Filter = "Text files (*.xls)|*.xls|Text files (*.xlsx)|*.xlsx|All files (*.*)|*.*";

                                if (savefile.ShowDialog(this) == DialogResult.OK)
                                {
                                    string path = savefile.FileName;
                                    File.WriteAllBytes(path, response.RawBytes);
                                    MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            catch (Exception ex)
                            {

                                MSG.Error(ex.Message);

                            }
                        }
                    }
            }
        }

        private void btnDownloadXML_Click(object sender, EventArgs e)
        {
            if (systemOption != null)
                if (!string.IsNullOrEmpty(systemOption.Data))
                {

                    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                    if (supplierService.SupplierServiceCode == "SDS")
                    {
                        RestApiService client = new RestApiService(_ISystemOptionService.GetByCode("EI_Path").Data, _ISystemOptionService.GetByCode("EI_TenDangNhap").Data, Core.ITripleDes.TripleDesDefaultDecryption(_ISystemOptionService.GetByCode("EI_MatKhau").Data));
                        try
                        {
                            WaitingFrm.StartWaiting();
                            request.Option = 2;
                            var response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoTinhHinhSDHD").ApiPath, request);
                            WaitingFrm.StopWaiting();
                            if (!string.IsNullOrEmpty(response.Content))
                                if (Utils.IsValidJson(response.Content))
                                {
                                    var parsed = JObject.Parse(response.Content.ToString());
                                    MSG.Error(parsed.SelectToken("Message").Value<string>());
                                    return;
                                }
                            SaveFileDialog savefile = new SaveFileDialog();
                            // set a default file name
                            savefile.FileName = response.FileName;
                            // set filters - this can be done in properties as well
                            savefile.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";

                            if (savefile.ShowDialog(this) == DialogResult.OK)
                            {
                                string path = savefile.FileName;
                                File.WriteAllBytes(path, response.RawBytes);
                                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        catch (Exception ex)
                        {

                            MSG.Error(ex.Message);

                        }
                    }
                }
        }

        private void ImportHTKK_Click(object sender, EventArgs e)
        {
            if (systemOption != null)
                if (!string.IsNullOrEmpty(systemOption.Data))
                {
                    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                    if (supplierService.SupplierServiceCode == "SDS")
                    {
                        RestApiService client = new RestApiService(_ISystemOptionService.GetByCode("EI_Path").Data, _ISystemOptionService.GetByCode("EI_TenDangNhap").Data, Core.ITripleDes.TripleDesDefaultDecryption(_ISystemOptionService.GetByCode("EI_MatKhau").Data));
                        try
                        {
                            WaitingFrm.StartWaiting();
                            request.Option = 1;
                            var response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoTinhHinhSDHD").ApiPath, request);
                            WaitingFrm.StopWaiting();
                            if (!string.IsNullOrEmpty(response.Content))
                                if (Utils.IsValidJson(response.Content))
                                {
                                    var parsed = JObject.Parse(response.Content.ToString());
                                    MSG.Error(parsed.SelectToken("Message").Value<string>());
                                    return;
                                }
                            SaveFileDialog savefile = new SaveFileDialog();
                            // set a default file name
                            savefile.FileName = response.FileName;
                            // set filters - this can be done in properties as well
                            savefile.Filter = "Text files (*.xls)|*.xls|Text files (*.xlsx)|*.xlsx|All files (*.*)|*.*";

                            if (savefile.ShowDialog() == DialogResult.OK)
                            {
                                string path = savefile.FileName;
                                File.WriteAllBytes(path, response.RawBytes);
                                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        catch (Exception ex)
                        {

                            MSG.Error(ex.Message);
                        }
                    }
                }
        }

        private void ToSave_Click(object sender, EventArgs e)
        {
            if (systemOption != null)
                if (!string.IsNullOrEmpty(systemOption.Data))
                {
                    SupplierService supplierService = _ISupplierServiceService.Getbykey(Guid.Parse(systemOption.Data));
                    if (supplierService.SupplierServiceCode == "SDS")
                    {
                        RestApiService client = new RestApiService(_ISystemOptionService.GetByCode("EI_Path").Data, _ISystemOptionService.GetByCode("EI_TenDangNhap").Data, Core.ITripleDes.TripleDesDefaultDecryption(_ISystemOptionService.GetByCode("EI_MatKhau").Data));
                        try
                        {
                            request.Option = 0;
                            WaitingFrm.StartWaiting();
                            var response = client.Download(supplierService.ApiService.FirstOrDefault(n => n.ApiName == "BaoCaoTinhHinhSDHD").ApiPath, request);
                            WaitingFrm.StopWaiting();
                            if (!string.IsNullOrEmpty(response.Content))
                                if (Utils.IsValidJson(response.Content))
                                {
                                    var parsed = JObject.Parse(response.Content.ToString());
                                    MSG.Error(parsed.SelectToken("Message").Value<string>());
                                    return;
                                }
                            SaveFileDialog savefile = new SaveFileDialog();
                            // set a default file name
                            savefile.FileName = response.FileName;
                            // set filters - this can be done in properties as well
                            savefile.Filter = "Text files (*.xls)|*.xls|Text files (*.xlsx)|*.xlsx|All files (*.*)|*.*";

                            if (savefile.ShowDialog() == DialogResult.OK)
                            {
                                string path = savefile.FileName;
                                File.WriteAllBytes(path, response.RawBytes);
                                MSG.MessageBoxStand("Tải xuống thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        catch (Exception ex)
                        {

                            MSG.Error(ex.Message);
                        }
                    }
                }
        }

        private void ViewRP_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
