﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Accounting
{
    partial class ViewRP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinScrollBar.ScrollBarLook scrollBarLook1 = new Infragistics.Win.UltraWinScrollBar.ScrollBarLook();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewRP));
            this.browser = new System.Windows.Forms.WebBrowser();
            this.cancelImage = new System.Windows.Forms.PictureBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.labelCancel = new System.Windows.Forms.Label();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnDownloadXML = new System.Windows.Forms.Button();
            this.btnDownloadXLS = new System.Windows.Forms.Button();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ImportHTKK = new System.Windows.Forms.ToolStripMenuItem();
            this.ToSave = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.cancelImage)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 0);
            this.browser.MinimumSize = new System.Drawing.Size(20, 0);
            this.browser.Name = "browser";
            this.browser.ScriptErrorsSuppressed = true;
            this.browser.Size = new System.Drawing.Size(535, 291);
            this.browser.TabIndex = 0;
            this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.EInvBrowser_DocumentCompleted);
            // 
            // cancelImage
            // 
            this.cancelImage.BackColor = System.Drawing.Color.Transparent;
            this.cancelImage.Location = new System.Drawing.Point(120, 30);
            this.cancelImage.Name = "cancelImage";
            this.cancelImage.Size = new System.Drawing.Size(100, 50);
            this.cancelImage.TabIndex = 1;
            this.cancelImage.TabStop = false;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(412, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(120, 32);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "In báo cáo";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // labelCancel
            // 
            this.labelCancel.AutoSize = true;
            this.labelCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCancel.ForeColor = System.Drawing.Color.Red;
            this.labelCancel.Location = new System.Drawing.Point(104, 107);
            this.labelCancel.Name = "labelCancel";
            this.labelCancel.Size = new System.Drawing.Size(94, 13);
            this.labelCancel.TabIndex = 3;
            this.labelCancel.Text = "Hóa đơn bị hủy";
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnDownloadXML);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnDownloadXLS);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnPrint);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 297);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(535, 38);
            this.ultraPanel1.TabIndex = 4;
            this.ultraPanel1.PaintClient += new System.Windows.Forms.PaintEventHandler(this.ultraPanel1_PaintClient);
            // 
            // btnDownloadXML
            // 
            this.btnDownloadXML.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadXML.Location = new System.Drawing.Point(160, 3);
            this.btnDownloadXML.Name = "btnDownloadXML";
            this.btnDownloadXML.Size = new System.Drawing.Size(120, 32);
            this.btnDownloadXML.TabIndex = 4;
            this.btnDownloadXML.Text = "Tải file XML";
            this.btnDownloadXML.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDownloadXML.UseVisualStyleBackColor = true;
            this.btnDownloadXML.Click += new System.EventHandler(this.btnDownloadXML_Click);
            // 
            // btnDownloadXLS
            // 
            this.btnDownloadXLS.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadXLS.Location = new System.Drawing.Point(286, 3);
            this.btnDownloadXLS.Name = "btnDownloadXLS";
            this.btnDownloadXLS.Size = new System.Drawing.Size(120, 32);
            this.btnDownloadXLS.TabIndex = 3;
            this.btnDownloadXLS.Text = "Tải file XLS";
            this.btnDownloadXLS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDownloadXLS.UseVisualStyleBackColor = true;
            this.btnDownloadXLS.Click += new System.EventHandler(this.btnDownloadXLS_Click);
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.cancelImage);
            this.ultraPanel2.ClientArea.Controls.Add(this.browser);
            this.ultraPanel2.ClientArea.Controls.Add(this.labelCancel);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraPanel2.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel2.Name = "ultraPanel2";
            scrollBarLook1.ScrollBarArrowStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarArrowStyle.None;
            this.ultraPanel2.ScrollBarLook = scrollBarLook1;
            this.ultraPanel2.Size = new System.Drawing.Size(535, 291);
            this.ultraPanel2.TabIndex = 5;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportHTKK,
            this.ToSave});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(183, 48);
            // 
            // ImportHTKK
            // 
            this.ImportHTKK.Name = "ImportHTKK";
            this.ImportHTKK.Size = new System.Drawing.Size(182, 22);
            this.ImportHTKK.Text = "Để import vào HTKK";
            this.ImportHTKK.Click += new System.EventHandler(this.ImportHTKK_Click);
            // 
            // ToSave
            // 
            this.ToSave.Name = "ToSave";
            this.ToSave.Size = new System.Drawing.Size(182, 22);
            this.ToSave.Text = "Để lưu trữ";
            this.ToSave.Click += new System.EventHandler(this.ToSave_Click);
            // 
            // ViewRP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(535, 335);
            this.Controls.Add(this.ultraPanel2);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewRP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo tình hình sử dụng hóa đơn";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewRP_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.cancelImage)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ClientArea.PerformLayout();
            this.ultraPanel2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser browser;
        private System.Windows.Forms.PictureBox cancelImage;
        private Button btnPrint;
        private Label labelCancel;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Button btnDownloadXML;
        private Button btnDownloadXLS;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem ImportHTKK;
        private ToolStripMenuItem ToSave;
    }
}