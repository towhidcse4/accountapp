﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using Microsoft.Win32;

namespace Accounting
{
    [ComVisible(true)]
    public class InvoiceViewer : Form
    {
        //private static ILog Log = LogManager.GetLogger(typeof (InvViewer));
        private IContainer components;
        private WebBrowser _browser;
        private Button _btnPrint;
        private Button btnAdjustInvoice;
        private Button btnReplaceInvoice;
        private Button btnViewVoucher;
        private ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem AdjustmentOfInvoiceIncrease;
        private System.Windows.Forms.ToolStripMenuItem AdjustInvoiceReduction;
        private System.Windows.Forms.ToolStripMenuItem AdjustInformation;
        private readonly bool _isCancel;
        Bitmap _icon;
        private EInvoice _eInvoice;
        string html_ = "";
        private InvoiceViewer()
        {

        }
        public InvoiceViewer(string html, int? status, EInvoice eInvoice)
        {
            
            _isCancel = status == 1;
            html_ = html;
            this.InitializeComponent();
            if (status == null)
            {
                this.btnReplaceInvoice.Visible = false;
                this.btnAdjustInvoice.Visible = false;
                this._browser.Size = new Size(this._browser.Size.Width + 10, this._browser.Size.Height);
            }
            InternetExplorerBrowserEmulation.SetBrowserEmulationVersion();
            //this._cancelImage.Visible = _isCancel;
            //this._labelCancel.Visible = _isCancel;
            this._browser.ObjectForScripting = new ScriptManager(this);
            this.LoadData(html);
            
            this._icon = Properties.Resources.logox48;
            IntPtr Hicon = _icon.GetHicon();
            this.Icon = Icon.FromHandle(Hicon);
            _eInvoice = eInvoice;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewRP_FormClosed);
        }

        public InvoiceViewer(string html, int? status, ViewEInvoice eInvoice)
        {

            _isCancel = status == 1;
            html_ = html;
            this.InitializeComponent();
            if (status == null)
            {
                this.btnReplaceInvoice.Visible = false;
                this.btnAdjustInvoice.Visible = false;
                this._browser.Size = new Size(this._browser.Size.Width + 10, this._browser.Size.Height);
            }
            InternetExplorerBrowserEmulation.SetBrowserEmulationVersion();
            //this._cancelImage.Visible = _isCancel;
            //this._labelCancel.Visible = _isCancel;
            this._browser.ObjectForScripting = new ScriptManager(this);
            this.LoadData(html);

            this._icon = Properties.Resources.logox48;
            IntPtr Hicon = _icon.GetHicon();
            this.Icon = Icon.FromHandle(Hicon);
            EInvoice eIv = new EInvoice();
            eIv.ID = eInvoice.ID;
            _eInvoice = eIv;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewRP_FormClosed);
        }

        private void ViewRP_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
        public void View()
        {
            this.ShowDialog();
        }

        private void LoadData(string html)
        {
            try
            {
                int products = Regex.Matches(html, "/Product", RegexOptions.IgnoreCase).Count / 10 + 1;
                if (string.IsNullOrWhiteSpace(html))
                {
                    int num2 = (int)MessageBox.Show("Lỗi dữ liệu, vui lòng thực hiện lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    this._browser.ObjectForScripting = (object)this;
                    string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    string str1 = "<script src=\"" + directoryName + "\\Content\\viewer\\jquery.min.js\"></script>" + "<script src=\"" + directoryName + "\\Content\\viewer\\main.js\"></script>" + "<link href=\"" + directoryName + "\\Content\\viewer\\styles.css\" rel=\"stylesheet\" type=\"text/css\">";
                    string str2 = html.Replace("<head>", "<head>" + str1);
                    if (products > 1)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("<div class='pagination'><a href='#' class='number' id='ap0' onclick='showPageContent(1); return false;'><<</a><a href='#' id='prev' class='prevnext' onclick='previewPage()'><</a>");
                        for (int index = 1; index <= products; index++)
                            stringBuilder.Append("<a href='#' class='number' id='ap" + (object)index + "' onclick='showPageContent(" + (object)index + "); return false;'>" + (object)index + "</a>");
                        stringBuilder.Append("<a href='#' id='next' class='prevnext' onclick='nextPage()'>></a></div>");
                        stringBuilder.Append("<script type='text/javascript'>$(document).ready(function () {$('.VATTEMP .invtable .prds').ProductNumberPagination({ number: 10 });});</script></body>");
                        str2 = str2.Replace("</body>", stringBuilder.ToString());
                    }
                    this._browser.DocumentText = str2;
                    this._browser.ScriptErrorsSuppressed = true;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                //Log.Error(ex);
                MessageBox.Show("Bạn cần sử dụng với quyền Administartor, vui lòng thực hiện lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
                MessageBox.Show("Có lỗi xảy ra, vui lòng thực hiện lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            this._browser.ShowPrintPreviewDialog();
        }

        private void btnReplaceInvoice_Click(object sender, EventArgs e)
        {
            ChangetoSABill(0, 0);
        }
        private void btnAdjustInvoice_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(btnAdjustInvoice, new Point(0, btnAdjustInvoice.Height));
        }

        private void ViewRP_SizeChanged(object sender, EventArgs e)
        {
            //this.btnReplaceInvoice.Size = new Size(120, 32);
            //this.btnReplaceInvoice.Location = new Point(width - this._btnPrint.Size.Width - this.btnReplaceInvoice.Size.Width - 2, height - 32);

        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            SAInvoice sAInvoice = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == _eInvoice.ID);
            if (sAInvoice == null)
            {
                SAReturn sAReturn = Utils.ListSAReturn.FirstOrDefault(n => n.ID == _eInvoice.ID);
                if (sAReturn == null)
                {
                    SABill sABill = Utils.ListSABill.FirstOrDefault(n => n.ID == _eInvoice.ID);
                    if (sABill == null)
                    {
                        PPDiscountReturn pPDiscountReturn = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == _eInvoice.ID);
                        WaitingFrm.StartWaiting();
                        new FPPDiscountReturnDetail(pPDiscountReturn, new List<PPDiscountReturn> { pPDiscountReturn },
                                               ConstFrm.optStatusForm.View).ShowDialog(this);
                        WaitingFrm.StopWaiting();
                    }
                    else
                    {
                        WaitingFrm.StartWaiting();
                        new FSABillDetail(sABill, new List<SABill> { sABill },
                                               ConstFrm.optStatusForm.View).ShowDialog(this);
                        WaitingFrm.StopWaiting();
                    }
                }
                else
                {
                    WaitingFrm.StartWaiting();
                    new FSAReturnDetail(sAReturn, new List<SAReturn> { sAReturn },
                                           ConstFrm.optStatusForm.View).ShowDialog(this);
                    WaitingFrm.StopWaiting();
                }
            }
            else
            {
                WaitingFrm.StartWaiting();
                new FSAInvoiceDetail(sAInvoice, new List<SAInvoice> { sAInvoice },
                                       ConstFrm.optStatusForm.View).ShowDialog(this);
                WaitingFrm.StopWaiting();
            }
        }

        private void AdjustmentOfInvoiceIncrease_Click(object sender, EventArgs e)
        {
            ChangetoSABill(1, 2);
        }

        private void AdjustInvoiceReduction_Click(object sender, EventArgs e)
        {
            ChangetoSABill(2, 3);
        }

        private void AdjustInformation_Click(object sender, EventArgs e)
        {
            ChangetoSABill(3, 4);
        }

        public void ChangetoSABill(int checkInvoice, int Type = 2)
        {
            WaitingFrm.StartWaiting();
            var saInvoice = Utils.ISAInvoiceService.Getbykey(_eInvoice.ID);
            SABill sABill = new SABill();
            if (saInvoice == null)
            {
                var sABillold = Utils.ISABillService.Getbykey(_eInvoice.ID);
                if (sABillold == null)
                {
                    SAReturn sAReturn = Utils.ISAReturnService.Getbykey(_eInvoice.ID);
                    if (sAReturn == null)
                    {
                        PPDiscountReturn pPDiscountReturn = Utils.IPPDiscountReturnService.Getbykey(_eInvoice.ID);
                        sABill = new SABill();
                        sABill.ID = Guid.NewGuid();
                        sABill.AccountingObjectID = pPDiscountReturn.AccountingObjectID;
                        sABill.AccountingObjectAddress = pPDiscountReturn.AccountingObjectAddress;
                        sABill.AccountingObjectName = pPDiscountReturn.AccountingObjectName;
                        sABill.PaymentMethod = pPDiscountReturn.PaymentMethod;
                        sABill.Reason = pPDiscountReturn.Reason;
                        sABill.AccountingObjectBankAccount = pPDiscountReturn.AccountingObjectBankAccount;
                        sABill.AccountingObjectBankName = pPDiscountReturn.AccountingObjectBankName;
                        sABill.InvoiceForm = pPDiscountReturn.InvoiceForm;
                        sABill.InvoiceTypeID = pPDiscountReturn.InvoiceTypeID;
                        sABill.InvoiceTemplate = pPDiscountReturn.InvoiceTemplate;
                        sABill.InvoiceSeries = pPDiscountReturn.InvoiceSeries;
                        //sABill.RefDateTime = pPDiscountReturn.RefDateTime;
                        if (checkInvoice == 0) sABill.StatusInvoice = 7;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                        sABill.TotalAmount = pPDiscountReturn.TotalAmount;
                        sABill.TotalAmountOriginal = pPDiscountReturn.TotalAmountOriginal;
                        sABill.TotalDiscountAmount = pPDiscountReturn.TotalDiscountAmount;
                        sABill.TotalDiscountAmountOriginal = pPDiscountReturn.TotalDiscountAmountOriginal;
                        sABill.TotalPaymentAmount = pPDiscountReturn.TotalPaymentAmount;
                        sABill.TotalPaymentAmountOriginal = pPDiscountReturn.TotalPaymentAmountOriginal;
                        sABill.TotalVATAmount = pPDiscountReturn.TotalVATAmount;
                        sABill.TotalVATAmountOriginal = pPDiscountReturn.TotalVATAmountOriginal;
                        sABill.CheckInvoice = checkInvoice;
                        sABill.CurrencyID = pPDiscountReturn.CurrencyID;
                        sABill.ExchangeRate = pPDiscountReturn.ExchangeRate ?? 1;
                        sABill.TypeID = 326;
                        sABill.Type = Type;
                        sABill.ContactName = pPDiscountReturn.OContactName;
                        sABill.ID_MIVAdjust = pPDiscountReturn.ID_MIV;
                        if (checkInvoice == 0) sABill.IDReplaceInv = pPDiscountReturn.ID;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = pPDiscountReturn.ID;
                        foreach (var item in pPDiscountReturn.PPDiscountReturnDetails)
                        {
                            SABillDetail sABillDetail = new SABillDetail();
                            sABillDetail.SABillID = sABill.ID;
                            sABillDetail.AccountingObjectID = item.AccountingObjectID;
                            sABillDetail.Amount = item.Amount ?? 0;
                            sABillDetail.AmountOriginal = item.AmountOriginal ?? 0;
                            sABillDetail.BudgetItemID = item.BudgetItemID;
                            //sABillDetail.CostAccount = item.CostAccount;
                            sABillDetail.ContractID = item.ContractID;
                            sABillDetail.CostSetID = item.CostSetID;
                            sABillDetail.CustomProperty1 = item.CustomProperty1;
                            sABillDetail.CustomProperty2 = item.CustomProperty2;
                            sABillDetail.CustomProperty3 = item.CustomProperty3;
                            sABillDetail.DepartmentID = item.DepartmentID;
                            sABillDetail.Description = item.Description;
                            //sABillDetail.DiscountAccount = item.DiscountAccount;
                            //sABillDetail.DiscountAmount = item.DiscountAmount;
                            //sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                            //sABillDetail.DiscountRate = item.DiscountRate ?? 0;
                            sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                            sABillDetail.OrderPriority = item.OrderPriority ?? 0;
                            sABillDetail.Quantity = item.Quantity ?? 0;
                            //sABillDetail.RepositoryAccount = repository.RepositoryAccount;
                            sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                            sABillDetail.Unit = item.Unit;
                            sABillDetail.UnitPrice = item.UnitPrice ?? 0;
                            sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal ?? 0;
                            sABillDetail.VATAccount = item.VATAccount;
                            sABillDetail.VATAmount = item.VATAmount ?? 0;
                            sABillDetail.VATAmountOriginal = item.VATAmountOriginal ?? 0;
                            sABillDetail.VATRate = item.VATRate ?? 0;
                            sABillDetail.LotNo = item.LotNo;
                            sABillDetail.ExpiryDate = item.ExpiryDate;
                            sABill.SABillDetails.Add(sABillDetail);
                        }
                    }
                    else
                    {
                        sABill = new SABill();
                        sABill.ID = Guid.NewGuid();
                        sABill.AccountingObjectID = sAReturn.AccountingObjectID;
                        sABill.AccountingObjectAddress = sAReturn.AccountingObjectAddress;
                        sABill.AccountingObjectName = sAReturn.AccountingObjectName;
                        sABill.PaymentMethod = sAReturn.PaymentMethod;
                        sABill.Reason = sAReturn.Reason;
                        sABill.AccountingObjectBankAccount = sAReturn.AccountingObjectBankAccount;
                        sABill.AccountingObjectBankName = sAReturn.AccountingObjectBankName;
                        sABill.InvoiceForm = sAReturn.InvoiceForm;
                        sABill.InvoiceTypeID = sAReturn.InvoiceTypeID;
                        sABill.InvoiceTemplate = sAReturn.InvoiceTemplate;
                        sABill.InvoiceSeries = sAReturn.InvoiceSeries;
                        sABill.RefDateTime = sAReturn.RefDateTime;
                        if (checkInvoice == 0) sABill.StatusInvoice = 7;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                        sABill.TotalAmount = sAReturn.TotalAmount;
                        sABill.TotalAmountOriginal = sAReturn.TotalAmountOriginal;
                        sABill.TotalDiscountAmount = sAReturn.TotalDiscountAmount;
                        sABill.TotalDiscountAmountOriginal = sAReturn.TotalDiscountAmountOriginal;
                        sABill.TotalPaymentAmount = sAReturn.TotalPaymentAmount;
                        sABill.TotalPaymentAmountOriginal = sAReturn.TotalPaymentAmountOriginal;
                        sABill.TotalVATAmount = sAReturn.TotalVATAmount;
                        sABill.TotalVATAmountOriginal = sAReturn.TotalVATAmountOriginal;
                        sABill.CheckInvoice = checkInvoice;
                        sABill.CurrencyID = sAReturn.CurrencyID;
                        sABill.ExchangeRate = sAReturn.ExchangeRate ?? 1;
                        sABill.TypeID = 326;
                        sABill.Type = Type;
                        sABill.ContactName = sAReturn.ContactName;
                        sABill.ID_MIVAdjust = sAReturn.ID_MIV;
                        if (checkInvoice == 0) sABill.IDReplaceInv = sAReturn.ID;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = sAReturn.ID;
                        foreach (var item in sAReturn.SAReturnDetails)
                        {
                            SABillDetail sABillDetail = new SABillDetail();
                            sABillDetail.AccountingObjectID = item.AccountingObjectID;
                            sABillDetail.Amount = item.Amount;
                            sABillDetail.AmountOriginal = item.AmountOriginal;
                            sABillDetail.BudgetItemID = item.BudgetItemID;
                            sABillDetail.CostAccount = item.CostAccount;
                            sABillDetail.ContractID = item.ContractID;
                            sABillDetail.CostSetID = item.CostSetID;
                            sABillDetail.CustomProperty1 = item.CustomProperty1;
                            sABillDetail.CustomProperty2 = item.CustomProperty2;
                            sABillDetail.CustomProperty3 = item.CustomProperty3;
                            sABillDetail.DepartmentID = item.DepartmentID;
                            sABillDetail.Description = item.Description;
                            sABillDetail.DiscountAccount = item.DiscountAccount;
                            sABillDetail.DiscountAmount = item.DiscountAmount;
                            sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                            sABillDetail.DiscountRate = item.DiscountRate ?? 0;
                            sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                            sABillDetail.OrderPriority = item.OrderPriority ?? 0;
                            sABillDetail.Quantity = item.Quantity ?? 0;
                            sABillDetail.RepositoryAccount = item.RepositoryAccount;
                            sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                            sABillDetail.Unit = item.Unit;
                            sABillDetail.UnitPrice = item.UnitPrice;
                            sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal;
                            sABillDetail.VATAccount = item.VATAccount;
                            sABillDetail.VATAmount = item.VATAmount;
                            sABillDetail.VATAmountOriginal = item.VATAmountOriginal;
                            sABillDetail.VATRate = item.VATRate ?? 0;
                            sABillDetail.SABillID = sABill.ID;
                            sABillDetail.IsPromotion = item.IsPromotion;
                            sABillDetail.LotNo = item.LotNo;
                            sABillDetail.ExpiryDate = item.ExpiryDate;
                            sABill.SABillDetails.Add(sABillDetail);
                        }
                    }
                }
                else
                {
                    sABill.ID = Guid.NewGuid();
                    sABill.AccountingObjectID = sABillold.AccountingObjectID;
                    sABill.AccountingObjectAddress = sABillold.AccountingObjectAddress;
                    sABill.AccountingObjectName = sABillold.AccountingObjectName;
                    sABill.PaymentMethod = sABillold.PaymentMethod;
                    sABill.Reason = sABillold.Reason;
                    sABill.AccountingObjectBankAccount = sABillold.AccountingObjectBankAccount;
                    sABill.AccountingObjectBankName = sABillold.AccountingObjectBankName;
                    sABill.InvoiceForm = sABillold.InvoiceForm;
                    sABill.InvoiceTypeID = sABillold.InvoiceTypeID;
                    sABill.InvoiceTemplate = sABillold.InvoiceTemplate;
                    sABill.InvoiceSeries = sABillold.InvoiceSeries;
                    sABill.RefDateTime = sABillold.RefDateTime;
                    if (checkInvoice == 0) sABill.StatusInvoice = 7;
                    else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                    sABill.TotalAmount = sABillold.TotalAmount;
                    sABill.TotalAmountOriginal = sABillold.TotalAmountOriginal;
                    sABill.TotalDiscountAmount = sABillold.TotalDiscountAmount;
                    sABill.TotalDiscountAmountOriginal = sABillold.TotalDiscountAmountOriginal;
                    sABill.TotalPaymentAmount = sABillold.TotalPaymentAmount;
                    sABill.TotalPaymentAmountOriginal = sABillold.TotalPaymentAmountOriginal;
                    sABill.TotalVATAmount = sABillold.TotalVATAmount;
                    sABill.TotalVATAmountOriginal = sABillold.TotalVATAmountOriginal;
                    sABill.CheckInvoice = checkInvoice;
                    sABill.CurrencyID = sABillold.CurrencyID;
                    sABill.ExchangeRate = sABillold.ExchangeRate;
                    sABill.TypeID = 326;
                    sABill.Type = Type;
                    sABill.ContactName = sABillold.ContactName;
                    sABill.ID_MIVAdjust = sABillold.ID_MIV;
                    if (checkInvoice == 0) sABill.IDReplaceInv = sABillold.ID;
                    else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = sABillold.ID;
                    foreach (var item in sABillold.SABillDetails)
                    {
                        SABillDetail sABillDetail = new SABillDetail();
                        sABillDetail.AccountingObjectID = item.AccountingObjectID;
                        sABillDetail.Amount = item.Amount;
                        sABillDetail.AmountOriginal = item.AmountOriginal;
                        sABillDetail.BudgetItemID = item.BudgetItemID;
                        sABillDetail.CostAccount = item.CostAccount;
                        sABillDetail.ContractID = item.ContractID;
                        sABillDetail.CostSetID = item.CostSetID;
                        sABillDetail.CustomProperty1 = item.CustomProperty1;
                        sABillDetail.CustomProperty2 = item.CustomProperty2;
                        sABillDetail.CustomProperty3 = item.CustomProperty3;
                        sABillDetail.DepartmentID = item.DepartmentID;
                        sABillDetail.Description = item.Description;
                        sABillDetail.DiscountAccount = item.DiscountAccount;
                        sABillDetail.DiscountAmount = item.DiscountAmount;
                        sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                        sABillDetail.DiscountRate = item.DiscountRate;
                        sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                        sABillDetail.OrderPriority = item.OrderPriority;
                        sABillDetail.Quantity = item.Quantity;
                        sABillDetail.RepositoryAccount = item.RepositoryAccount;
                        sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                        sABillDetail.Unit = item.Unit;
                        sABillDetail.UnitPrice = item.UnitPrice;
                        sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal;
                        sABillDetail.VATAccount = item.VATAccount;
                        sABillDetail.VATAmount = item.VATAmount;
                        sABillDetail.VATAmountOriginal = item.VATAmountOriginal;
                        sABillDetail.VATRate = item.VATRate;
                        sABillDetail.SABillID = sABill.ID;
                        sABillDetail.IsPromotion = item.IsPromotion;
                        sABillDetail.LotNo = item.LotNo;
                        sABillDetail.ExpiryDate = item.ExpiryDate;
                        sABill.SABillDetails.Add(sABillDetail);
                    }
                }
            }
            else
            {
                sABill.ID = Guid.NewGuid();
                sABill.AccountingObjectID = saInvoice.AccountingObjectID;
                sABill.AccountingObjectAddress = saInvoice.AccountingObjectAddress;
                sABill.AccountingObjectName = saInvoice.AccountingObjectName;
                sABill.BankAccountDetailID = saInvoice.BankAccountDetailID;
                sABill.AccountingObjectBankAccount = saInvoice.AccountingObjectBankAccount;
                sABill.AccountingObjectBankName = saInvoice.AccountingObjectBankName;
                sABill.ListDate = saInvoice.ListDate;
                sABill.ListNo = saInvoice.ListNo;
                sABill.PaymentMethod = saInvoice.PaymentMethod;
                sABill.Reason = saInvoice.Reason;
                sABill.InvoiceForm = saInvoice.InvoiceForm;
                sABill.InvoiceTypeID = saInvoice.InvoiceTypeID;
                sABill.InvoiceTemplate = saInvoice.InvoiceTemplate;
                sABill.InvoiceSeries = saInvoice.InvoiceSeries;
                sABill.RefDateTime = saInvoice.RefDateTime;
                if (checkInvoice == 0) sABill.StatusInvoice = 7;
                else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                sABill.TotalAmount = saInvoice.TotalAmount;
                sABill.TotalAmountOriginal = saInvoice.TotalAmountOriginal;
                sABill.TotalDiscountAmount = saInvoice.TotalDiscountAmount;
                sABill.TotalDiscountAmountOriginal = saInvoice.TotalDiscountAmountOriginal;
                sABill.TotalPaymentAmount = saInvoice.TotalPaymentAmount;
                sABill.TotalPaymentAmountOriginal = saInvoice.TotalPaymentAmountOriginal;
                sABill.TotalVATAmount = saInvoice.TotalVATAmount;
                sABill.TotalVATAmountOriginal = saInvoice.TotalVATAmountOriginal;
                sABill.CheckInvoice = checkInvoice;
                sABill.CurrencyID = saInvoice.CurrencyID;
                sABill.ExchangeRate = saInvoice.ExchangeRate ?? 1;
                sABill.TypeID = 326;
                sABill.Type = Type;
                sABill.ContactName = saInvoice.ContactName;
                sABill.ID_MIVAdjust = saInvoice.ID_MIV;
                if (checkInvoice == 0) sABill.IDReplaceInv = saInvoice.ID;
                else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = saInvoice.ID;
                foreach (var item in saInvoice.SAInvoiceDetails)
                {
                    SABillDetail sABillDetail = new SABillDetail();
                    sABillDetail.AccountingObjectID = item.AccountingObjectID;
                    sABillDetail.Amount = item.Amount;
                    sABillDetail.AmountOriginal = item.AmountOriginal;
                    sABillDetail.BudgetItemID = item.BudgetItemID;
                    sABillDetail.CostAccount = item.CostAccount;
                    sABillDetail.ContractID = item.ContractID;
                    sABillDetail.CostSetID = item.CostSetID;
                    sABillDetail.CustomProperty1 = item.CustomProperty1;
                    sABillDetail.CustomProperty2 = item.CustomProperty2;
                    sABillDetail.CustomProperty3 = item.CustomProperty3;
                    sABillDetail.DepartmentID = item.DepartmentID;
                    sABillDetail.Description = item.Description;
                    sABillDetail.DiscountAccount = item.DiscountAccount;
                    sABillDetail.DiscountAmount = item.DiscountAmount;
                    sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                    sABillDetail.DiscountRate = item.DiscountRate ?? 0;
                    sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                    sABillDetail.OrderPriority = item.OrderPriority ?? 0;
                    sABillDetail.Quantity = item.Quantity ?? 0;
                    sABillDetail.RepositoryAccount = item.RepositoryAccount;
                    sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                    sABillDetail.Unit = item.Unit;
                    sABillDetail.UnitPrice = item.UnitPrice;
                    sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal;
                    sABillDetail.VATAccount = item.VATAccount;
                    sABillDetail.VATAmount = item.VATAmount;
                    sABillDetail.VATAmountOriginal = item.VATAmountOriginal;
                    sABillDetail.VATRate = item.VATRate ?? 0;
                    sABillDetail.SABillID = sABill.ID;
                    sABillDetail.IsPromotion = item.IsPromotion;
                    sABillDetail.LotNo = item.LotNo;
                    sABillDetail.ExpiryDate = item.ExpiryDate;
                    sABill.SABillDetails.Add(sABillDetail);
                }
            }
            new FSABillDetail(sABill, new List<SABill> { sABill },
                                   ConstFrm.optStatusForm.Edit).ShowDialog(this);
            WaitingFrm.StopWaiting();
        }

        private void EInvBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                RegistryKey registryKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.CurrentUser, "").OpenSubKey("Software\\Microsoft\\Internet Explorer\\PageSetup", RegistryKeyPermissionCheck.ReadWriteSubTree);
                string name1 = "(Default)";
                string str1 = "";
                registryKey.SetValue(name1, (object)str1);
                string name2 = "font";
                string str2 = "";
                registryKey.SetValue(name2, (object)str2);
                string name3 = "footer";
                string str3 = "";
                registryKey.SetValue(name3, (object)str3);
                string name4 = "header";
                string str4 = "";
                registryKey.SetValue(name4, (object)str4);
                string name5 = "margin_bottom";
                string str5 = "0.750000";
                registryKey.SetValue(name5, (object)str5);
                string name6 = "margin_left";
                string str6 = "0.750000";
                registryKey.SetValue(name6, (object)str6);
                string name7 = "margin_right";
                string str7 = "0.750000";
                registryKey.SetValue(name7, (object)str7);
                string name8 = "margin_top";
                string str8 = "0.750000";
                registryKey.SetValue(name8, (object)str8);
                string name9 = "Print_Background";
                string str9 = "yes";
                registryKey.SetValue(name9, (object)str9);
                string name10 = "Shrink_To_Fit";
                string str10 = "yes";
                registryKey.SetValue(name10, (object)str10);
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
                MessageBox.Show("Có lỗi xảy ra, vui lòng thực hiện lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(InvoiceViewer));
            this._browser = new WebBrowser();
            //this._cancelImage = new PictureBox();
            //this._labelCancel = new Label();
            this._btnPrint = new Button();
            this.btnReplaceInvoice = new Button();
            this.btnAdjustInvoice = new Button();
            this.btnViewVoucher = new Button();
            this.contextMenuStrip1 = new ContextMenuStrip();
            this.AdjustmentOfInvoiceIncrease = new System.Windows.Forms.ToolStripMenuItem();
            this.AdjustInvoiceReduction = new System.Windows.Forms.ToolStripMenuItem();
            this.AdjustInformation = new System.Windows.Forms.ToolStripMenuItem();
            // ((ISupportInitialize)((ISupportInitialize)this._cancelImage)).BeginInit();
            this.SuspendLayout();
            this._browser.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this._browser.Location = new Point(0, 0);
            this._browser.MinimumSize = new Size(20, 0);
            this._browser.Name = "_browser";
            this._browser.ScriptErrorsSuppressed = true;
            var height = Screen.PrimaryScreen.WorkingArea.Size.Height - 30;
            //var width = Screen.PrimaryScreen.WorkingArea.Size.Width * 60 / 100;
            int w;
            if (html_.Contains("Số lô") && html_.Contains("Hạn dùng"))
            {
                w = 1100;
            }
            else
            {
                w = 840;
            }
            var width= w;
            
            
            this._browser.Size = new Size(width, height - 31);
            this._browser.TabIndex = 0;
            this._browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.EInvBrowser_DocumentCompleted);
            //this._cancelImage.BackColor = System.Drawing.Color.Transparent;
            //this._cancelImage.Image = (Image)componentResourceManager.GetObject("cancelImage.Image");
            //this._cancelImage.Location = new Point(1, 1);
            //this._cancelImage.Name = "cancelImage";
            //this._cancelImage.AutoSize = true;
            //this._cancelImage.TabIndex = 1;
            //this._cancelImage.TabStop = false;

            //this._labelCancel.AutoSize = true;
            //this._labelCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte)0);
            //this._labelCancel.ForeColor = System.Drawing.Color.Red;
            //this._labelCancel.Location = new Point(10, 105);
            //this._labelCancel.Name = "labelCancel";
            //this._labelCancel.TabIndex = 2;
            //this._labelCancel.Text = "Hóa đơn bị hủy";

            this.SizeChanged += new System.EventHandler(this.ViewRP_SizeChanged);

            this._btnPrint.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this._btnPrint.Font = new Font("Times New Roman", 11.25f, FontStyle.Bold, GraphicsUnit.Point, (byte)0);
            this._btnPrint.Image = (Image)componentResourceManager.GetObject("btnPrint.Image");
            //this._btnPrint.Location = new Point(width/2 - this._btnPrint.Size.Width/2, height - 32);

            this._btnPrint.Name = "_btnPrint";
            this._btnPrint.Size = new Size(120, 32);
            this._btnPrint.Location = new Point(width - this._btnPrint.Size.Width, height - 32);
            this._btnPrint.TabIndex = 4;
            this._btnPrint.Visible = true;
            this._btnPrint.Enabled = true;
            this._btnPrint.Text = "In hóa đơn";
            this._btnPrint.TextAlign = ContentAlignment.MiddleCenter;
            this._btnPrint.TextImageRelation = TextImageRelation.ImageBeforeText;
            this._btnPrint.UseVisualStyleBackColor = true;
            this._btnPrint.Click += new EventHandler(this.btnPrint_Click);


            this.btnReplaceInvoice.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.btnReplaceInvoice.Font = new Font("Times New Roman", 11.25f, FontStyle.Bold, GraphicsUnit.Point, (byte)0);
            this.btnReplaceInvoice.Name = "btnReplaceInvoice";
            this.btnReplaceInvoice.Size = new Size(120, 32);
            this.btnReplaceInvoice.Location = new Point(width - this._btnPrint.Size.Width - this.btnReplaceInvoice.Size.Width - 2, height - 32);
            this.btnReplaceInvoice.TabIndex = 3;
            this.btnReplaceInvoice.Visible = true;
            this.btnReplaceInvoice.Enabled = _isCancel;
            this.btnReplaceInvoice.Text = "Thay thế hóa đơn";
            this.btnReplaceInvoice.TextAlign = ContentAlignment.MiddleCenter;
            this.btnReplaceInvoice.TextImageRelation = TextImageRelation.ImageBeforeText;
            this.btnReplaceInvoice.UseVisualStyleBackColor = true;
            this.btnReplaceInvoice.Click += new EventHandler(this.btnReplaceInvoice_Click);

            this.btnAdjustInvoice.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.btnAdjustInvoice.Font = new Font("Times New Roman", 11.25f, FontStyle.Bold, GraphicsUnit.Point, (byte)0);
            this.btnAdjustInvoice.Name = "btnReplaceInvoice";
            this.btnAdjustInvoice.Size = new Size(120, 32);
            this.btnAdjustInvoice.Location = new Point(width - this._btnPrint.Size.Width - this.btnAdjustInvoice.Size.Width - this.btnReplaceInvoice.Size.Width - 2, height - 32);
            this.btnAdjustInvoice.TabIndex = 3;
            this.btnAdjustInvoice.Visible = true;
            this.btnAdjustInvoice.Enabled = _isCancel;
            this.btnAdjustInvoice.Text = "Điều chỉnh HĐ";
            this.btnAdjustInvoice.TextAlign = ContentAlignment.MiddleCenter;
            this.btnAdjustInvoice.TextImageRelation = TextImageRelation.ImageBeforeText;
            this.btnAdjustInvoice.UseVisualStyleBackColor = true;
            this.btnAdjustInvoice.Click += new EventHandler(this.btnAdjustInvoice_Click);

            this.btnViewVoucher.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.btnViewVoucher.Font = new Font("Times New Roman", 11.25f, FontStyle.Bold, GraphicsUnit.Point, (byte)0);
            this.btnViewVoucher.Name = "btnReplaceInvoice";
            this.btnViewVoucher.Size = new Size(120, 32);
            this.btnViewVoucher.Location = new Point(width - this._btnPrint.Size.Width - this.btnAdjustInvoice.Size.Width - this.btnReplaceInvoice.Size.Width - this.btnViewVoucher.Size.Width - 2, height - 32);
            this.btnViewVoucher.TabIndex = 3;
            this.btnViewVoucher.Visible = true;
            //this.btnViewVoucher.Enabled = _isCancel;
            this.btnViewVoucher.Text = "Xem chứng từ";
            this.btnViewVoucher.TextAlign = ContentAlignment.MiddleCenter;
            this.btnViewVoucher.TextImageRelation = TextImageRelation.ImageBeforeText;
            this.btnViewVoucher.UseVisualStyleBackColor = true;
            this.btnViewVoucher.Click += new EventHandler(this.btnViewVoucher_Click);

            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdjustmentOfInvoiceIncrease,
            this.AdjustInvoiceReduction,
            this.AdjustInformation});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(232, 70);
            // 
            // AdjustmentOfInvoiceIncrease
            // 
            this.AdjustmentOfInvoiceIncrease.Name = "AdjustmentOfInvoiceIncrease";
            this.AdjustmentOfInvoiceIncrease.Size = new System.Drawing.Size(231, 22);
            this.AdjustmentOfInvoiceIncrease.Text = "Hóa đơn điều chỉnh tăng";
            this.AdjustmentOfInvoiceIncrease.Click += new System.EventHandler(this.AdjustmentOfInvoiceIncrease_Click);
            // 
            // AdjustInvoiceReduction
            // 
            this.AdjustInvoiceReduction.Name = "AdjustInvoiceReduction";
            this.AdjustInvoiceReduction.Size = new System.Drawing.Size(231, 22);
            this.AdjustInvoiceReduction.Text = "Hóa đơn điều chỉnh giảm";
            this.AdjustInvoiceReduction.Click += new System.EventHandler(this.AdjustInvoiceReduction_Click);
            // 
            // AdjustInformation
            // 
            this.AdjustInformation.Name = "AdjustInformation";
            this.AdjustInformation.Size = new System.Drawing.Size(231, 22);
            this.AdjustInformation.Text = "Hóa đơn điều chỉnh thông tin";
            this.AdjustInformation.Click += new System.EventHandler(this.AdjustInformation_Click);

            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = SystemColors.Control;
            this.ClientSize = new Size(width, height);
            this.Controls.Add((Control)this._btnPrint);
            this.Controls.Add((Control)this.btnReplaceInvoice);
            this.Controls.Add((Control)this.btnAdjustInvoice);
            this.Controls.Add((Control)this.btnViewVoucher);
            //this.Controls.Add((Control)this._labelCancel);
            //this.Controls.Add((Control)this._cancelImage);
            this.Controls.Add((Control)this._browser);
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
            this.Name = nameof(InvoiceViewer);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Thông tin chi tiết hóa đơn";
            //((ISupportInitialize)((ISupportInitialize)this._cancelImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        [ComVisible(true)]
        public class ScriptManager
        {
            private InvoiceViewer _form;

            public ScriptManager(InvoiceViewer form)
            {
                this._form = form;
            }

            //public void displayCert(string name)
            //{
            //    if (name.Equals("client"))
            //    {
            //        SignatureDetails signatureDetails = new SignatureDetails(InvViewer.xmlData, true);
            //        int num1 = 4;
            //        signatureDetails.StartPosition = (FormStartPosition)num1;
            //        int num2 = (int)signatureDetails.ShowDialog();
            //    }
            //    if (!name.Equals("server"))
            //        return;
            //    SignatureDetails signatureDetails1 = new SignatureDetails(InvViewer.xmlData, false);
            //    int num3 = 4;
            //    signatureDetails1.StartPosition = (FormStartPosition)num3;
            //    int num4 = (int)signatureDetails1.ShowDialog();
            //}
        }
    }
}
