﻿using Accounting.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Spire.PdfViewer.Forms;
using System.IO;
using Infragistics.Win.Misc;
using Accounting.TextMessage;
using System.Diagnostics;
using Infragistics.Win.UltraWinEditors;

namespace Accounting
{
    public partial class ViewPDF : Form
    {
        private PdfDocumentViewer viewer = null;
        ResponseFile _responseFile;
        public ViewPDF(ResponseFile responseFile)
        {
            InitializeComponent();
            _responseFile = responseFile;
            Stream stream = new MemoryStream(responseFile.RawBytes);
            viewer = new PdfDocumentViewer();
            viewer.LoadFromStream(stream);
            combobox.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            //form and child components
            this.Text = "View hóa đơn";
            this.StartPosition = FormStartPosition.CenterScreen;
            TableLayoutPanel table = new TableLayoutPanel();
            table.ColumnCount = 3;
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            table.RowCount = 2;
            table.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            table.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));
            table.Controls.Add(viewer, 0, 0);
            table.SetColumnSpan(viewer, 3);
            viewer.Dock = DockStyle.Fill;

            //btn Print
            UltraButton button = new UltraButton();
            button.Text = "Lưu";
            button.Size = new Size(150, 24);
            //button.TextAlign = ContentAlignment.MiddleCenter;
            table.Controls.Add(button, 0, 1);
            button.Dock = DockStyle.Right;
            button.Click += Save;
            var appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.ubtnSave,
                ImageHAlign = Infragistics.Win.HAlign.Left,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            button.Appearance = appearance;
            

            //btn Save
            button = new UltraButton();
            button.Click += Print;
            button.Text = "In hóa đơn";
            button.Size = new Size(150, 24);
            table.Controls.Add(button, 2, 1);
            button.Dock = DockStyle.Left;
            appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.ubtnPrint,
                ImageHAlign = Infragistics.Win.HAlign.Left,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            button.Appearance = appearance;
            this.Size = new Size(840, 1024 - 30);
            table.Dock = DockStyle.Fill;
            table.Size = this.Size;
            GroupBox_Table.Controls.Add(table);
            //this.Controls.Add(table);

            UltraComboEditor comBoxZoom = new UltraComboEditor();
            //add zoom values to combox
            int[] intZooms = new Int32[] { 25, 50, 75, 100, 125, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
            foreach (int zoom in intZooms)
            {
                combobox.Items.Add(zoom.ToString() + "%");
            }
            combobox.SelectedIndex = 5;
            this.Width = 1200;
        }
        public ViewPDF(ResponseFile responseFile, int cbbSize)
        {
            InitializeComponent();
            _responseFile = responseFile;
            Stream stream = new MemoryStream(responseFile.RawBytes);
            viewer = new PdfDocumentViewer();
            viewer.LoadFromStream(stream);
            combobox.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            //form and child components
            this.Text = "View hóa đơn";
            this.StartPosition = FormStartPosition.CenterScreen;
            TableLayoutPanel table = new TableLayoutPanel();
            table.ColumnCount = 3;
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            table.RowCount = 2;
            table.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            table.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));
            table.Controls.Add(viewer, 0, 0);
            table.SetColumnSpan(viewer, 3);
            viewer.Dock = DockStyle.Fill;

            //btn Print
            UltraButton button = new UltraButton();
            button.Text = "Lưu";
            button.Size = new Size(150, 24);
            //button.TextAlign = ContentAlignment.MiddleCenter;
            table.Controls.Add(button, 0, 1);
            button.Dock = DockStyle.Right;
            button.Click += Save;
            var appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.ubtnSave,
                ImageHAlign = Infragistics.Win.HAlign.Left,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            button.Appearance = appearance;


            //btn Save
            button = new UltraButton();
            button.Click += Print;
            button.Text = "In hóa đơn";
            button.Size = new Size(150, 24);
            table.Controls.Add(button, 2, 1);
            button.Dock = DockStyle.Left;
            appearance = new Infragistics.Win.Appearance
            {
                Image = Properties.Resources.ubtnPrint,
                ImageHAlign = Infragistics.Win.HAlign.Left,
                ImageVAlign = Infragistics.Win.VAlign.Middle
            };
            button.Appearance = appearance;
            this.Size = new Size(840, 1024 - 30);
            table.Dock = DockStyle.Fill;
            table.Size = this.Size;
            GroupBox_Table.Controls.Add(table);
            //this.Controls.Add(table);

            UltraComboEditor comBoxZoom = new UltraComboEditor();
            //add zoom values to combox
            int[] intZooms = new Int32[] { 25, 50, 75, 100, 125, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
            foreach (int zoom in intZooms)
            {
                combobox.Items.Add(zoom.ToString() + "%");
            }
            combobox.SelectedIndex = cbbSize;
            this.Width = 900;
        }
        private void Save(object sender, EventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            // set filters - this can be done in properties as well
            savefile.Filter = "Text files (*.pdf)|*.pdf|All files (*.*)|*.*";
            savefile.FileName = _responseFile.FileName;
            if (savefile.ShowDialog(this) == DialogResult.OK)
            {
                string path = savefile.FileName;
                File.WriteAllBytes(path, _responseFile.RawBytes);
                MSG.MessageBoxStand("Lưu thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void Print(object sender, EventArgs e)
        {
            if (this.viewer.PageCount > 0)
            {
                this.viewer.Print();
            }

        }
        private void ExportToOneImage(object sender, EventArgs e)
        {
            if (viewer.PageCount > 0)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "PNG Format(*.png)|*.png";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    int currentPage = viewer.CurrentPageNumber;
                    //Bitmap image = viewer.SaveAsImage(currentPage - 1);
                    //image.Save(dialog.FileName);
                }
            }
        }

        private void ExportToMultipleImages(object sender, EventArgs e)
        {
            if (viewer.PageCount > 0)
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    int currentPage = viewer.CurrentPageNumber;
                    //Bitmap[] images = viewer.SaveAsImage(0, currentPage - 1);
                    //for (int i = 0; i < images.Length; i++)
                    //{
                    //    String fileName = Path.Combine(dialog.SelectedPath, String.Format("PDFViewer-{0}.png", i));
                    //    images[i].Save(fileName);
                    //}
                }
            }
        }

        private void combobox_ValueChanged(object sender, EventArgs e)
        {
            if (this.viewer.PageCount > 0)
            {
                int zoomValue = Int32.Parse(combobox.SelectedItem.ToString().Trim('%'));
                this.viewer.ZoomTo(zoomValue);
            }
        }
    }
}
