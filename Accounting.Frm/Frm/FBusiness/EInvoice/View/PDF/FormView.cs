﻿using Accounting.Core;
using Accounting.Core.Domain;
using Accounting.TextMessage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Winsoft.PDFium;

namespace Accounting
{
    /*
     * Author Hautv
     * Add 07/ 12/ 2019
     * 
     * **/
    public partial class FormView : Form
    {
        double PixelsPerInchX = 0.0;
        double PixelsPerInchY = 0.0;

        bool Selecting = false;
        int SelectionStart = -1;
        int SelectionEnd = -1;

        bool Cancel = false;
        bool DisableBookmarks = false;
        int SearchStart = -1;
        int SearchEnd = -1;

        private EInvoice _eInvoice;
        private bool _isCancel;
        private string _fileName;

        public FormView(ResponseFile responseFile)
        {
            InitializeComponent();
            setDefault();
            this.LoadPDF(responseFile);
            ultraPanel1.Hide();
            panelScrollBox.Height = panelScrollBox.Height + 10;
            this.Text = !String.IsNullOrEmpty(responseFile.FileName) ? responseFile.FileName : "View";
        }

        public FormView(ResponseFile responseFile, int? status, EInvoice eInvoice)
        {
            InitializeComponent();
            _isCancel = status == 1;
            setDefault();
            _eInvoice = eInvoice;
            this.Text = "Thông tin chi tiết hóa đơn";
            this.LoadPDF(responseFile);
        }

        public FormView(ResponseFile responseFile, int? status, ViewEInvoice viewEInvoice)
        {
            InitializeComponent();
            _isCancel = status == 1;
            setDefault();
            EInvoice eInvoice = new EInvoice();
            eInvoice.ID = viewEInvoice.ID;
            _eInvoice = eInvoice;
            this.Text = "Thông tin chi tiết hóa đơn";
            this.LoadPDF(responseFile);
        }

        private void setDefault()
        {
            toolStripButtonOpenPdf.Visible = false;
            toolStripButtonSaveBitmap.Visible = false;
            toolStripButtonShowInfo.Visible = false;
            toolStripCancel_ClientSizeChanged(null, null);
            splitContainer.Panel1Collapsed = true;
            toolStripComboBoxZoom.SelectedIndex = 10;
            using (Graphics g = CreateGraphics())
            {
                PixelsPerInchX = g.DpiX;
                PixelsPerInchY = g.DpiY;
            }
            //this.Size = new Size(840, 1024 - 30);
            this.Size = new Size(900, Screen.PrimaryScreen.Bounds.Height - 50);
            //this.StartPosition = FormStartPosition.CenterParent;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Height / 2, 10);
            this.ReplaceInvoice.Enabled = _isCancel;
            this.AdjustInvoice.Enabled = _isCancel;
        }

        private void setTool()
        {
            toolStripButtonSaveAs.Enabled = false;
            toolStripButtonSaveBitmap.Enabled = false;
            toolStripButtonPrint.Enabled = false;
            toolStripButtonShowInfo.Enabled = false;
            toolStripButtonShowText.Enabled = false;
            toolStripButtonFirstPage.Enabled = false;
            toolStripButtonPreviousPage.Enabled = false;
            toolStripButtonPageNumber.Enabled = false;
            toolStripButtonPageNumber.Text = "";
            toolStripButtonNextPage.Enabled = false;
            toolStripButtonLastPage.Enabled = false;
            toolStripButtonZoomIn.Enabled = false;
            toolStripButtonZoomOut.Enabled = false;
            toolStripComboBoxZoom.Enabled = false;
            toolStripButtonRotateLeft.Enabled = false;
            toolStripButtonRotateRight.Enabled = false;
            toolStripButtonSettings.Enabled = false;
            toolStripButtonSearch.Enabled = false;
            toolStripTextBoxSearch.Enabled = false;
            splitContainer.Panel1Collapsed = true;
        }

        public void LoadPDF(ResponseFile responseFile)
        {
            if (!String.IsNullOrEmpty(responseFile.FileName))
            {
                _fileName = responseFile.FileName;
            }
            Selecting = false;
            SelectionStart = -1;
            SelectionEnd = -1;
            setTool();
            pdf.Active = false;
            File.WriteAllBytes("file.pdf", responseFile.RawBytes);
            pdf.FileName = "file.pdf";
            pdf.Password = "";
            pdfViewer.PageNumber = 1;
            try
            {
                pdfViewer.Active = true;
            }
            catch (PDFiumException exception)
            {
                if (exception.Message == "Password required or incorrect password")
                {
                    FormPrompt formPrompt = new FormPrompt();
                    formPrompt.Text = "Enter Password";
                    formPrompt.label.Text = "Password:";
                    formPrompt.textBox.PasswordChar = '*';
                    formPrompt.textBox.Text = "";
                    if (formPrompt.ShowDialog() != DialogResult.OK)
                    {
                        MessageBox.Show(exception.Message);
                        return;
                    }

                    pdf.Password = formPrompt.textBox.Text;
                    try
                    {
                        pdfViewer.Active = true;
                    }
                    catch (PDFiumException pdfiumException)
                    {
                        MessageBox.Show(pdfiumException.Message);
                        return;
                    }
                }
            }

            var bookmarks = pdf.Bookmarks;
            treeViewBookmarks.Nodes.Clear();

            if (bookmarks != null)
                for (int i = 0; i < bookmarks.Length; ++i)
                {
                    TreeNode node = treeViewBookmarks.Nodes.Add(bookmarks[i].Title);
                    node.Tag = bookmarks[i].PageNumber;
                    AddChildBookmarks(node, bookmarks[i]);
                }

            if (treeViewBookmarks.Nodes.Count > 0)
                splitContainer.Panel1Collapsed = false;
            Zoom();
        }

        private void toolStripButtonOpenPdf_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Selecting = false;
                SelectionStart = -1;
                SelectionEnd = -1;

                toolStripButtonSaveAs.Enabled = false;
                toolStripButtonSaveBitmap.Enabled = false;
                toolStripButtonPrint.Enabled = false;
                toolStripButtonShowInfo.Enabled = false;
                toolStripButtonShowText.Enabled = false;
                toolStripButtonFirstPage.Enabled = false;
                toolStripButtonPreviousPage.Enabled = false;
                toolStripButtonPageNumber.Enabled = false;
                toolStripButtonPageNumber.Text = "";
                toolStripButtonNextPage.Enabled = false;
                toolStripButtonLastPage.Enabled = false;
                toolStripButtonZoomIn.Enabled = false;
                toolStripButtonZoomOut.Enabled = false;
                toolStripComboBoxZoom.Enabled = false;
                toolStripButtonRotateLeft.Enabled = false;
                toolStripButtonRotateRight.Enabled = false;
                toolStripButtonSettings.Enabled = false;
                toolStripButtonSearch.Enabled = false;
                toolStripTextBoxSearch.Enabled = false;
                splitContainer.Panel1Collapsed = true;
                pdf.Active = false;
                pdf.FileName = openFileDialog.FileName;
                pdf.Password = "";
                pdfViewer.PageNumber = 1;
                try
                {
                    pdfViewer.Active = true;
                }
                catch (PDFiumException exception)
                {
                    if (exception.Message == "Password required or incorrect password")
                    {
                        FormPrompt formPrompt = new FormPrompt();
                        formPrompt.Text = "Enter Password";
                        formPrompt.label.Text = "Password:";
                        formPrompt.textBox.PasswordChar = '*';
                        formPrompt.textBox.Text = "";
                        if (formPrompt.ShowDialog() != DialogResult.OK)
                        {
                            MessageBox.Show(exception.Message);
                            return;
                        }

                        pdf.Password = formPrompt.textBox.Text;
                        try
                        {
                            pdfViewer.Active = true;
                        }
                        catch (PDFiumException pdfiumException)
                        {
                            MessageBox.Show(pdfiumException.Message);
                            return;
                        }
                    }
                }

                var bookmarks = pdf.Bookmarks;
                treeViewBookmarks.Nodes.Clear();

                if (bookmarks != null)
                    for (int i = 0; i < bookmarks.Length; ++i)
                    {
                        TreeNode node = treeViewBookmarks.Nodes.Add(bookmarks[i].Title);
                        node.Tag = bookmarks[i].PageNumber;
                        AddChildBookmarks(node, bookmarks[i]);
                    }

                if (treeViewBookmarks.Nodes.Count > 0)
                    splitContainer.Panel1Collapsed = false;
            }
            Zoom();
        }

        private void AddChildBookmarks(TreeNode node, Bookmark bookmark)
        {
            Bookmark[] bookmarks = pdf.GetBookmarkChildren(bookmark);
            if (bookmarks != null)
                for (int i = 0; i < bookmarks.Length; ++i)
                {
                    TreeNode childNode = node.Nodes.Add(bookmarks[i].Title);
                    childNode.Tag = bookmarks[i].PageNumber;
                    AddChildBookmarks(childNode, bookmarks[i]);
                }
        }

        private TreeNode FindBookmark(TreeNode node, int pageNumber)
        {
            if ((int)node.Tag >= pageNumber)
                return node;
            else
                for (int i = 0; i < node.Nodes.Count - 1; ++i)
                {
                    TreeNode result = FindBookmark(node.Nodes[i], pageNumber);
                    if (result != null)
                        return result;
                }
            return null;
        }

        private TreeNode FindBookmark(TreeView treeView, int pageNumber)
        {
            TreeNode result = null;
            for (int i = 0; i < treeView.Nodes.Count; ++i)
            {
                result = FindBookmark(treeView.Nodes[i], pageNumber);
                if (result != null)
                    break;
            }

            if (result != null)
                if ((int)result.Tag > pageNumber)
                    if (result.Parent != null)
                        result = PreviousItem(result.Parent, result);
                    else
                        result = PreviousItem(treeView, result);

            return result;
        }

        private TreeNode PreviousItem(TreeNode parentItem, TreeNode item)
        {
            TreeNode result = item;
            for (int i = 0; i < parentItem.Nodes.Count; ++i)
                if (parentItem.Nodes[i] == item)
                {
                    if (i == 0)
                        result = parentItem;
                    else
                    {
                        result = parentItem.Nodes[i - 1];
                        while (result.Nodes.Count > 0)
                            result = result.Nodes[result.Nodes.Count - 1];
                    }
                    break;
                }
            return result;
        }

        private TreeNode PreviousItem(TreeView parent, TreeNode item)
        {
            TreeNode result = item;
            for (int i = 0; i < parent.Nodes.Count; ++i)
                if (parent.Nodes[i] == item)
                {
                    if (i > 0)
                    {
                        result = parent.Nodes[i - 1];
                        while (result.Nodes.Count > 0)
                            result = result.Nodes[result.Nodes.Count - 1];
                    }
                    break;
                }
            return result;
        }

        private void pdfViewer_PageChange(object sender, EventArgs e)
        {
            if (pdfViewer.Active)
            {
                toolStripButtonSaveAs.Enabled = true;
                toolStripButtonSaveBitmap.Enabled = true;
                toolStripButtonPrint.Enabled = true;
                toolStripButtonShowInfo.Enabled = true;
                toolStripButtonShowText.Enabled = true;
                toolStripButtonFirstPage.Enabled = pdfViewer.PageNumber > 1;
                toolStripButtonPreviousPage.Enabled = pdfViewer.PageNumber > 1;
                toolStripButtonPageNumber.Enabled = pdfViewer.PageCount > 1;
                toolStripButtonPageNumber.Text = pdfViewer.PageNumber + " of " + pdfViewer.PageCount;
                toolStripButtonNextPage.Enabled = pdfViewer.PageNumber < pdfViewer.PageCount;
                toolStripButtonLastPage.Enabled = pdfViewer.PageNumber < pdfViewer.PageCount;
                toolStripButtonZoomIn.Enabled = true;
                toolStripButtonZoomOut.Enabled = true;
                toolStripComboBoxZoom.Enabled = true;
                toolStripButtonRotateLeft.Enabled = true;
                toolStripButtonRotateRight.Enabled = true;
                toolStripButtonSettings.Enabled = true;
                toolStripButtonSearch.Enabled = toolStripTextBoxSearch.Text.Length > 0;
                toolStripTextBoxSearch.Enabled = true;
            }
            else
            {
                toolStripButtonSaveAs.Enabled = false;
                toolStripButtonSaveBitmap.Enabled = false;
                toolStripButtonPrint.Enabled = false;
                toolStripButtonShowInfo.Enabled = false;
                toolStripButtonShowText.Enabled = false;
                toolStripButtonFirstPage.Enabled = false;
                toolStripButtonPreviousPage.Enabled = false;
                toolStripButtonPageNumber.Enabled = false;
                toolStripButtonPageNumber.Text = "";
                toolStripButtonNextPage.Enabled = false;
                toolStripButtonLastPage.Enabled = false;
                toolStripButtonZoomIn.Enabled = false;
                toolStripButtonZoomOut.Enabled = false;
                toolStripComboBoxZoom.Enabled = false;
                toolStripButtonRotateLeft.Enabled = false;
                toolStripButtonRotateRight.Enabled = false;
                toolStripButtonSettings.Enabled = false;
                toolStripButtonSearch.Enabled = false;
                toolStripTextBoxSearch.Enabled = false;
            }

            Selecting = false;
            SelectionStart = -1;
            SelectionEnd = -1;

            SearchStart = -1;
            SearchEnd = -1;

            Zoom();

            // update bookmark
            if (!DisableBookmarks)
            {
                TreeNode node = FindBookmark(treeViewBookmarks, pdfViewer.PageNumber);
                if (node != null)
                    treeViewBookmarks.SelectedNode = node;
                else
                    treeViewBookmarks.SelectedNode = null;
            }
        }

        private void toolStripButtonShowInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Author: " + pdf.Author + Environment.NewLine +
                "Creator: " + pdf.Creator + Environment.NewLine +
                "Keywords: " + pdf.Keywords + Environment.NewLine +
                "Producer: " + pdf.Producer + Environment.NewLine +
                "Subject: " + pdf.Subject + Environment.NewLine +
                "Title: " + pdf.Title + Environment.NewLine +
                "Creation date: " + pdf.CreationDate + Environment.NewLine +
                "Modified date: " + pdf.ModifiedDate);
        }

        private void toolStripButtonShowText_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(pdfViewer.GetText());
            MSG.Information(pdfViewer.GetText());
        }

        private void toolStripButtonFirstPage_Click(object sender, EventArgs e)
        {
            pdfViewer.PageNumber = 1;
        }

        private void toolStripButtonPreviousPage_Click(object sender, EventArgs e)
        {
            pdfViewer.PageNumber = pdfViewer.PageNumber - 1;
        }

        private void toolStripButtonNextPage_Click(object sender, EventArgs e)
        {
            pdfViewer.PageNumber = pdfViewer.PageNumber + 1;
        }

        private void toolStripButtonLastPage_Click(object sender, EventArgs e)
        {
            pdfViewer.PageNumber = pdfViewer.PageCount;
        }

        private void Zoom()
        {
            if (pdfViewer.Active)
            {
                try
                {
                    double pdfPageWidth;
                    double pdfPageHeight;
                    if (pdfViewer.Rotation == Rotation.Degree0 || pdfViewer.Rotation == Rotation.Degree180)
                    {
                        pdfPageWidth = pdfViewer.PageWidth;
                        pdfPageHeight = pdfViewer.PageHeight;
                    }
                    else
                    {
                        pdfPageWidth = pdfViewer.PageHeight;
                        pdfPageHeight = pdfViewer.PageWidth;
                    }

                    double pageWidth = 0.0;
                    double pageHeight = 0.0;

                    switch (toolStripComboBoxZoom.SelectedIndex)
                    {
                        case 0:
                            pageWidth = 0.1 * pdfPageWidth;
                            pageHeight = 0.1 * pdfPageHeight;
                            break;

                        case 1:
                            pageWidth = 0.25 * pdfPageWidth;
                            pageHeight = 0.25 * pdfPageHeight;
                            break;

                        case 2:
                            pageWidth = 0.5 * pdfPageWidth;
                            pageHeight = 0.5 * pdfPageHeight;
                            break;

                        case 3:
                            pageWidth = 0.75 * pdfPageWidth;
                            pageHeight = 0.75 * pdfPageHeight;
                            break;

                        case 5:
                            pageWidth = 1.25 * pdfPageWidth;
                            pageHeight = 1.25 * pdfPageHeight;
                            break;

                        case 6:
                            pageWidth = 1.5 * pdfPageWidth;
                            pageHeight = 1.5 * pdfPageHeight;
                            break;

                        case 7:
                            pageWidth = 2.0 * pdfPageWidth;
                            pageHeight = 2.0 * pdfPageHeight;
                            break;

                        case 8:
                            pageWidth = 4.0 * pdfPageWidth;
                            pageHeight = 4.0 * pdfPageHeight;
                            break;

                        case 9:
                            pageWidth = 8.0 * pdfPageWidth;
                            pageHeight = 8.0 * pdfPageHeight;
                            break;

                        case 11:
                            panelScrollBox.HorizontalScroll.Visible = false;
                            panelScrollBox.VerticalScroll.Visible = false;

                            if (panelScrollBox.Width / pdfPageWidth > panelScrollBox.Height / pdfPageHeight)
                            {
                                pdfViewer.SetBounds(0, 0, (int)Math.Round(panelScrollBox.ClientRectangle.Height * pdfPageWidth / pdfPageHeight), panelScrollBox.Height);
                                double pageX = (panelScrollBox.ClientRectangle.Width - pdfViewer.Width) / 2.0;
                                if (pageX < 0.0)
                                    pageX = 0.0;
                                pdfViewer.SetBounds((int)Math.Round(pageX) - panelScrollBox.HorizontalScroll.Value, 0, (int)Math.Round(panelScrollBox.ClientRectangle.Height * pdfPageWidth / pdfPageHeight), panelScrollBox.ClientRectangle.Height);
                            }
                            else
                            {
                                pdfViewer.SetBounds(0, 0, panelScrollBox.Width, (int)Math.Round(panelScrollBox.Width * pdfPageHeight / pdfPageWidth));
                                double pageY = (panelScrollBox.ClientRectangle.Height - pdfViewer.Height) / 2.0;
                                if (pageY < 0.0)
                                    pageY = 0.0;
                                pdfViewer.SetBounds(0, (int)Math.Round(pageY) - panelScrollBox.VerticalScroll.Value, panelScrollBox.ClientRectangle.Width, (int)Math.Round(panelScrollBox.ClientRectangle.Width * pdfPageHeight / pdfPageWidth));
                            }
                            break;

                        case 12:
                            {
                                panelScrollBox.HorizontalScroll.Visible = false;
                                panelScrollBox.VerticalScroll.Visible = true;

                                pdfViewer.SetBounds(0, -panelScrollBox.VerticalScroll.Value, panelScrollBox.Width, (int)Math.Round(panelScrollBox.Width * pdfPageHeight / pdfPageWidth));
                                double pageY = (panelScrollBox.ClientRectangle.Height - pdfViewer.Height) / 2.0;
                                if (pageY < 0.0)
                                    pageY = 0.0;
                                pdfViewer.SetBounds(0, (int)Math.Round(pageY) - panelScrollBox.VerticalScroll.Value, panelScrollBox.ClientRectangle.Width, (int)Math.Round(panelScrollBox.ClientRectangle.Width * pdfPageHeight / pdfPageWidth));
                                break;
                            }

                        default:
                            pageWidth = pdfPageWidth;
                            pageHeight = pdfPageHeight;
                            break;
                    }

                    if (toolStripComboBoxZoom.SelectedIndex < 11)
                    {
                        panelScrollBox.HorizontalScroll.Visible = true;
                        panelScrollBox.VerticalScroll.Visible = true;

                        pageWidth = PixelsPerInchX * pageWidth / 72.0;
                        pageHeight = PixelsPerInchY * pageHeight / 72.0;

                        pdfViewer.SetBounds(-panelScrollBox.HorizontalScroll.Value, -panelScrollBox.VerticalScroll.Value, (int)Math.Round(pageWidth), (int)Math.Round(pageHeight));

                        double pageX = (panelScrollBox.Width - pageWidth) / 2.0;
                        if (pageX < 0.0)
                            pageX = 0.0;

                        double pageY = (panelScrollBox.Height - pageHeight) / 2.0;
                        if (pageY < 0.0)
                            pageY = 0.0;

                        pdfViewer.SetBounds((int)Math.Round(pageX) - panelScrollBox.HorizontalScroll.Value, (int)Math.Round(pageY) - panelScrollBox.VerticalScroll.Value, (int)Math.Round(pageWidth), (int)Math.Round(pageHeight));
                    }

                    // update zoom buttons visibility
                    toolStripButtonZoomIn.Enabled = toolStripComboBoxZoom.SelectedIndex < 9;
                    toolStripButtonZoomOut.Enabled = (toolStripComboBoxZoom.SelectedIndex > 0) && (toolStripComboBoxZoom.SelectedIndex < 10);
                }
                finally
                {
                    pdfViewer.Invalidate();
                }
            }
        }

        private void FormView_SizeChanged(object sender, EventArgs e)
        {
            Zoom();
        }

        private void toolStripButtonSaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog.FileName = String.IsNullOrEmpty(_fileName) ? "File.pdf" : _fileName;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                pdf.SaveAs(saveFileDialog.FileName);
        }

        private void toolStripButtonSaveBitmap_Click(object sender, EventArgs e)
        {
            if (savePictureDialog.ShowDialog() == DialogResult.OK)
                using (var bitmap = pdfViewer.RenderPage(0, 0, pdfViewer.Width, pdfViewer.Height, Rotation.Degree0, pdfViewer.Options))
                    bitmap.Save(savePictureDialog.FileName);
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e)
        {
            var printDocument = new PrintDocument();
            printDialog.Document = printDocument;

            PrinterSettings printerSettings = printDocument.PrinterSettings;
            printerSettings.MinimumPage = 1;
            printerSettings.MaximumPage = pdf.PageCount;
            printerSettings.FromPage = pdfViewer.PageNumber;
            printerSettings.ToPage = pdfViewer.PageNumber;
            printerSettings.PrintRange = PrintRange.CurrentPage;
            IEnumerable<PaperSize> paperSizes = printerSettings.PaperSizes.Cast<PaperSize>();
            PaperSize sizeA4 = paperSizes.First<PaperSize>(size => size.Kind == PaperKind.A4); // setting paper size to A4 size
            printDocument.DefaultPageSettings.PaperSize = sizeA4;
            printerSettings.Duplex = Duplex.Simplex;
            printDocument.DefaultPageSettings.Landscape = false;

            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                switch (printerSettings.PrintRange)
                {
                    case PrintRange.AllPages:
                    case PrintRange.Selection:
                        fromPage = 1;
                        toPage = pdf.PageCount;
                        break;

                    case PrintRange.CurrentPage:
                        fromPage = pdfViewer.PageNumber;
                        toPage = pdfViewer.PageNumber;
                        break;

                    case PrintRange.SomePages:
                        fromPage = printerSettings.FromPage;
                        toPage = printerSettings.ToPage;
                        break;
                }

                if (printerSettings.Collate)
                {
                    collateCopyCount = printerSettings.Copies;
                    copyCount = 1;
                }
                else
                {
                    collateCopyCount = 1;
                    copyCount = printerSettings.Copies;
                }

                collateCopy = 1;
                page = fromPage;
                copy = 1;

                printDocument.PrintPage += printDocument_PrintPage;
                printDocument.Print();
            }
        }

        int collateCopyCount = 1;
        int collateCopy;

        int copyCount = 1;
        int copy = 1;

        int fromPage;
        int toPage;
        int page;

        void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.HasMorePages = false;
            if (e.Cancel)
                return;

            if (collateCopy <= collateCopyCount)
                if (page <= toPage)
                    if (copy <= copyCount)
                    {
                        e.Graphics.PageUnit = GraphicsUnit.Pixel;
                        pdf.PageNumber = page;
                        double width = e.Graphics.VisibleClipBounds.Width;
                        double height = e.Graphics.VisibleClipBounds.Height;
                        pdf.RenderPage(e.Graphics.GetHdc(), 0, 0, (int)width, (int)height, Rotation.Degree0, RenderOption.Printing);
                        e.HasMorePages = true;

                        if (++copy > copyCount)
                        {
                            copy = 1;
                            if (++page > toPage)
                            {
                                page = fromPage;
                                if (++collateCopy > collateCopyCount)
                                    e.HasMorePages = false;
                            }
                        }
                    }
        }

        private void toolStripButtonPageNumber_Click(object sender, EventArgs e)
        {
            FormPrompt formPrompt = new FormPrompt();
            formPrompt.Text = "Select Page";
            formPrompt.label.Text = "Page number:";
            formPrompt.textBox.Text = pdfViewer.PageNumber.ToString();
            if (formPrompt.ShowDialog() == DialogResult.OK)
            {
                int newPageNumber = 0;
                if (int.TryParse(formPrompt.textBox.Text, out newPageNumber))
                    if (newPageNumber >= 1 && newPageNumber <= pdf.PageCount)
                        pdfViewer.PageNumber = newPageNumber;
            }
        }

        private void toolStripButtonZoomOut_Click(object sender, EventArgs e)
        {
            toolStripComboBoxZoom.SelectedIndex -= 1;
            Zoom();
        }

        private void toolStripButtonZoomIn_Click(object sender, EventArgs e)
        {
            toolStripComboBoxZoom.SelectedIndex += 1;
            Zoom();
        }

        private void splitContainer_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Zoom();
        }

        private void toolStripButtonRotateLeft_Click(object sender, EventArgs e)
        {
            switch (pdfViewer.Rotation)
            {
                case Rotation.Degree0: pdfViewer.Rotation = Rotation.Degree270; break;
                case Rotation.Degree90: pdfViewer.Rotation = Rotation.Degree0; break;
                case Rotation.Degree180: pdfViewer.Rotation = Rotation.Degree90; break;
                case Rotation.Degree270: pdfViewer.Rotation = Rotation.Degree180; break;
            }
            Zoom();
        }

        private void toolStripButtonRotateRight_Click(object sender, EventArgs e)
        {
            switch (pdfViewer.Rotation)
            {
                case Rotation.Degree0: pdfViewer.Rotation = Rotation.Degree90; break;
                case Rotation.Degree90: pdfViewer.Rotation = Rotation.Degree180; break;
                case Rotation.Degree180: pdfViewer.Rotation = Rotation.Degree270; break;
                case Rotation.Degree270: pdfViewer.Rotation = Rotation.Degree0; break;
            }
            Zoom();
        }

        private void toolStripComboBoxZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Zoom();
        }

        private void pdfViewer_MouseDown(object sender, MouseEventArgs e)
        {
            if (!pdfViewer.Active)
                return;

            int webLinkIndex = pdfViewer.GetWebLinkAtPos(e.X, e.Y);
            if (webLinkIndex != -1)
                Process.Start(pdfViewer.GetWebLink(webLinkIndex).Url);
            else
            {
                if (e.Clicks == 2) // double click
                {
                    const double Tolerance = 2.0;

                    // select current word
                    SelectionStart = pdfViewer.GetCharacterIndexAtPos(e.X, e.Y, Tolerance, Tolerance);
                    SelectionEnd = SelectionStart;
                    if (SelectionStart >= 0)
                    {
                        while ((SelectionStart > 0) && char.IsLetterOrDigit(pdfViewer.GetCharacter(SelectionStart - 1)))
                            --SelectionStart;

                        while ((SelectionEnd < pdfViewer.CharacterCount - 1) && char.IsLetterOrDigit(pdfViewer.GetCharacter(SelectionEnd + 1)))
                            ++SelectionEnd;

                        pdfViewer.Invalidate();
                    }
                }
                else
                {
                    Selecting = true;
                    SelectionStart = -1;
                    SelectionEnd = -1;
                }
            }
        }

        private void pdfViewer_MouseMove(object sender, MouseEventArgs e)
        {
            if (!pdfViewer.Active)
                return;

            const double Tolerance = 2.0;

            int SelectedIndex = pdfViewer.GetCharacterIndexAtPos(e.X, e.Y, Tolerance, Tolerance);
            if (!Selecting && pdfViewer.GetWebLinkAtPos(e.X, e.Y) != -1)
                pdfViewer.Cursor = Cursors.Hand;
            else if (SelectedIndex >= 0)
                pdfViewer.Cursor = Cursors.IBeam;
            else
                pdfViewer.Cursor = Cursors.Default;

            if (Selecting)
                if (SelectedIndex >= 0)
                {
                    bool NeedRepaint = false;

                    if (SelectionStart == -1)
                    {
                        SelectionStart = SelectedIndex;
                        NeedRepaint = true;
                    }

                    if (SelectionEnd != SelectedIndex)
                    {
                        SelectionEnd = SelectedIndex;
                        NeedRepaint = true;
                    }

                    if (NeedRepaint)
                        pdfViewer.Invalidate();
                }
        }

        private void pdfViewer_MouseUp(object sender, MouseEventArgs e)
        {
            if (Selecting)
            {
                Selecting = false;
                if ((SelectionStart >= 0) && (SelectionEnd >= 0))
                {
                    string Text;

                    if (SelectionEnd < SelectionStart)
                        Text = pdfViewer.GetText(SelectionEnd, SelectionStart - SelectionEnd + 1);
                    else
                        Text = pdfViewer.GetText(SelectionStart, SelectionEnd - SelectionStart + 1);

                    Clipboard.SetText(Text);
                }
            }
        }

        private void pdfViewer_Paint(object sender, PaintEventArgs e)
        {
            pdfViewer.PaintSelection(e.Graphics, SelectionStart, SelectionEnd, Color.FromArgb(0x80, 0x80, 0xC0, 0xF0));
            pdfViewer.PaintSelection(e.Graphics, SearchStart, SearchEnd, Color.FromArgb(0x80, 0x00, 0xE0, 0x00));
        }

        private void toolStripButtonSettings_Click(object sender, EventArgs e)
        {
            using (FormSettings formSettings = new FormSettings())
            {
                CheckedListBox checkedListBox = formSettings.checkedListBox;

                if ((pdfViewer.Options & RenderOption.Annotations) != 0)
                    checkedListBox.SetItemChecked(0, true);
                if ((pdfViewer.Options & RenderOption.Lcd) != 0)
                    checkedListBox.SetItemChecked(1, true);
                if ((pdfViewer.Options & RenderOption.NoNativeText) == 0)
                    checkedListBox.SetItemChecked(2, true);
                if ((pdfViewer.Options & RenderOption.Grayscale) != 0)
                    checkedListBox.SetItemChecked(3, true);
                if ((pdfViewer.Options & RenderOption.LimitCache) != 0)
                    checkedListBox.SetItemChecked(4, true);
                if ((pdfViewer.Options & RenderOption.Halftone) != 0)
                    checkedListBox.SetItemChecked(5, true);
                if ((pdfViewer.Options & RenderOption.Printing) != 0)
                    checkedListBox.SetItemChecked(6, true);
                if ((pdfViewer.Options & RenderOption.ReverseByteOrder) != 0)
                    checkedListBox.SetItemChecked(7, true);
                if ((pdfViewer.Options & RenderOption.NoSmoothText) == 0)
                    checkedListBox.SetItemChecked(8, true);
                if ((pdfViewer.Options & RenderOption.NoSmoothImage) == 0)
                    checkedListBox.SetItemChecked(9, true);
                if ((pdfViewer.Options & RenderOption.NoSmoothPath) == 0)
                    checkedListBox.SetItemChecked(10, true);

                if (formSettings.ShowDialog() == DialogResult.OK)
                {
                    RenderOption renderOptions = 0;
                    if (checkedListBox.GetItemChecked(0))
                        renderOptions |= RenderOption.Annotations;
                    if (checkedListBox.GetItemChecked(1))
                        renderOptions |= RenderOption.Lcd;
                    if (!checkedListBox.GetItemChecked(2))
                        renderOptions |= RenderOption.NoNativeText;
                    if (checkedListBox.GetItemChecked(3))
                        renderOptions |= RenderOption.Grayscale;
                    if (checkedListBox.GetItemChecked(4))
                        renderOptions |= RenderOption.LimitCache;
                    if (checkedListBox.GetItemChecked(5))
                        renderOptions |= RenderOption.Halftone;
                    if (checkedListBox.GetItemChecked(6))
                        renderOptions |= RenderOption.Printing;
                    if (checkedListBox.GetItemChecked(7))
                        renderOptions |= RenderOption.ReverseByteOrder;
                    if (!checkedListBox.GetItemChecked(8))
                        renderOptions |= RenderOption.NoSmoothText;
                    if (!checkedListBox.GetItemChecked(9))
                        renderOptions |= RenderOption.NoSmoothImage;
                    if (!checkedListBox.GetItemChecked(10))
                        renderOptions |= RenderOption.NoSmoothPath;

                    pdfViewer.Options = renderOptions;
                    pdfViewer.Invalidate();
                }
            }
        }

        private void toolStripCancel_ClientSizeChanged(object sender, EventArgs e)
        {
            toolStripProgressBar.Width = toolStripCancel.ClientRectangle.Width - toolStripButtonCancel.Width - 20;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Left
                || keyData == Keys.Right
                || keyData == Keys.Home
                || keyData == Keys.End)
                if (toolStripTextBoxSearch.Focused)
                    return base.ProcessCmdKey(ref msg, keyData);

            switch (keyData)
            {
                case Keys.Prior:
                case Keys.Up:
                case Keys.Left:
                    if (toolStripButtonPreviousPage.Enabled)
                        toolStripButtonPreviousPage.PerformClick();
                    return true;

                case Keys.Next:
                case Keys.Down:
                case Keys.Right:
                    if (toolStripButtonNextPage.Enabled)
                        toolStripButtonNextPage.PerformClick();
                    return true;

                case Keys.Home:
                    if (toolStripButtonFirstPage.Enabled)
                        toolStripButtonFirstPage.PerformClick();
                    return true;

                case Keys.End:
                    if (toolStripButtonLastPage.Enabled)
                        toolStripButtonLastPage.PerformClick();
                    return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void treeViewBookmarks_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (pdfViewer.Active)
                if (e.Node != null)
                {
                    int pageNumber = (int)e.Node.Tag;
                    if (pageNumber >= 1 && pageNumber <= pdf.PageCount)
                        try
                        {
                            DisableBookmarks = true;
                            pdfViewer.PageNumber = pageNumber;
                        }
                        finally
                        {
                            DisableBookmarks = false;
                        }
                }
        }

        private void toolStripTextBoxSearch_TextChanged(object sender, EventArgs e)
        {
            if (SearchStart != -1)
            {
                SearchStart = -1;
                SearchEnd = -1;
                pdfViewer.Invalidate();
            }
            toolStripButtonSearch.Enabled = toolStripTextBoxSearch.Text.Length > 0;
        }

        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            if (toolStripTextBoxSearch.Text.Length > 0)
            {
                Cancel = false;
                toolStripProgressBar.Maximum = pdf.PageCount - pdfViewer.PageNumber;
                toolStripProgressBar.Value = 0;
                toolStripButtons.Visible = false;
                toolStripCancel.Visible = true;

                int foundIndex = -1;
                pdf.PageNumber = pdfViewer.PageNumber;
                if (SearchStart == -1)
                    foundIndex = pdf.FindFirst(toolStripTextBoxSearch.Text);
                else
                    foundIndex = pdf.FindNext();

                SearchStart = -1;
                SearchEnd = -1;

                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerReportsProgress = true;
                backgroundWorker.DoWork += BackgroundWorker_DoWork;
                backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
                backgroundWorker.ProgressChanged += BackgroundWorker_ProgressChanged;
                backgroundWorker.RunWorkerAsync(foundIndex);
            }
        }

        private void BackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ++toolStripProgressBar.Value;
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                int foundIndex = (int)e.Result;

                if (foundIndex != -1)
                {
                    pdfViewer.PageNumber = pdf.PageNumber;
                    SearchStart = foundIndex;
                    SearchEnd = foundIndex + toolStripTextBoxSearch.Text.Length - 1;
                    pdfViewer.Invalidate();
                }
                else
                {
                    pdfViewer.Invalidate();
                    if (!Cancel)
                        MessageBox.Show("Not found");
                }
            }
            finally
            {
                toolStripCancel.Visible = false;
                toolStripButtons.Visible = true;
            }
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int foundIndex = (int)e.Argument;
            while ((foundIndex == -1) && (pdf.PageNumber < pdf.PageCount))
            {
                ((BackgroundWorker)sender).ReportProgress(0);
                if (Cancel)
                    break;

                ++pdf.PageNumber;
                foundIndex = pdf.FindFirst(toolStripTextBoxSearch.Text);
            }
            e.Result = foundIndex;
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            Cancel = true;
        }

        private void btnReplaceInvoice_Click(object sender, EventArgs e)
        {
            ChangetoSABill(0, 0);
        }
        private void btnAdjustInvoice_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(AdjustInvoice, new Point(0, AdjustInvoice.Height));
        }

        private void btnViewVoucher_Click(object sender, EventArgs e)
        {
            SAInvoice sAInvoice = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == _eInvoice.ID);
            if (sAInvoice == null)
            {
                SAReturn sAReturn = Utils.ListSAReturn.FirstOrDefault(n => n.ID == _eInvoice.ID);
                if (sAReturn == null)
                {
                    SABill sABill = Utils.ListSABill.FirstOrDefault(n => n.ID == _eInvoice.ID);
                    if (sABill == null)
                    {
                        PPDiscountReturn pPDiscountReturn = Utils.ListPPDiscountReturn.FirstOrDefault(n => n.ID == _eInvoice.ID);
                        WaitingFrm.StartWaiting();
                        new FPPDiscountReturnDetail(pPDiscountReturn, new List<PPDiscountReturn> { pPDiscountReturn },
                                               ConstFrm.optStatusForm.View).ShowDialog(this);
                        WaitingFrm.StopWaiting();
                    }
                    else
                    {
                        WaitingFrm.StartWaiting();
                        new FSABillDetail(sABill, new List<SABill> { sABill },
                                               ConstFrm.optStatusForm.View).ShowDialog(this);
                        WaitingFrm.StopWaiting();
                    }
                }
                else
                {
                    WaitingFrm.StartWaiting();
                    new FSAReturnDetail(sAReturn, new List<SAReturn> { sAReturn },
                                           ConstFrm.optStatusForm.View).ShowDialog(this);
                    WaitingFrm.StopWaiting();
                }
            }
            else
            {
                WaitingFrm.StartWaiting();
                new FSAInvoiceDetail(sAInvoice, new List<SAInvoice> { sAInvoice },
                                       ConstFrm.optStatusForm.View).ShowDialog(this);
                WaitingFrm.StopWaiting();
            }
        }
        public void ChangetoSABill(int checkInvoice, int Type = 2)
        {
            WaitingFrm.StartWaiting();
            var saInvoice = Utils.ISAInvoiceService.Getbykey(_eInvoice.ID);
            SABill sABill = new SABill();
            if (saInvoice == null)
            {
                var sABillold = Utils.ISABillService.Getbykey(_eInvoice.ID);
                if (sABillold == null)
                {
                    SAReturn sAReturn = Utils.ISAReturnService.Getbykey(_eInvoice.ID);
                    if (sAReturn == null)
                    {
                        PPDiscountReturn pPDiscountReturn = Utils.IPPDiscountReturnService.Getbykey(_eInvoice.ID);
                        sABill = new SABill();
                        sABill.ID = Guid.NewGuid();
                        sABill.AccountingObjectID = pPDiscountReturn.AccountingObjectID;
                        sABill.AccountingObjectAddress = pPDiscountReturn.AccountingObjectAddress;
                        sABill.AccountingObjectName = pPDiscountReturn.AccountingObjectName;
                        sABill.PaymentMethod = pPDiscountReturn.PaymentMethod;
                        sABill.Reason = pPDiscountReturn.Reason;
                        sABill.AccountingObjectBankAccount = pPDiscountReturn.AccountingObjectBankAccount;
                        sABill.AccountingObjectBankName = pPDiscountReturn.AccountingObjectBankName;
                        sABill.InvoiceForm = pPDiscountReturn.InvoiceForm;
                        sABill.InvoiceTypeID = pPDiscountReturn.InvoiceTypeID;
                        sABill.InvoiceTemplate = pPDiscountReturn.InvoiceTemplate;
                        sABill.InvoiceSeries = pPDiscountReturn.InvoiceSeries;
                        //sABill.RefDateTime = pPDiscountReturn.RefDateTime;
                        if (checkInvoice == 0) sABill.StatusInvoice = 7;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                        sABill.TotalAmount = pPDiscountReturn.TotalAmount;
                        sABill.TotalAmountOriginal = pPDiscountReturn.TotalAmountOriginal;
                        sABill.TotalDiscountAmount = pPDiscountReturn.TotalDiscountAmount;
                        sABill.TotalDiscountAmountOriginal = pPDiscountReturn.TotalDiscountAmountOriginal;
                        sABill.TotalPaymentAmount = pPDiscountReturn.TotalPaymentAmount;
                        sABill.TotalPaymentAmountOriginal = pPDiscountReturn.TotalPaymentAmountOriginal;
                        sABill.TotalVATAmount = pPDiscountReturn.TotalVATAmount;
                        sABill.TotalVATAmountOriginal = pPDiscountReturn.TotalVATAmountOriginal;
                        sABill.CheckInvoice = checkInvoice;
                        sABill.CurrencyID = pPDiscountReturn.CurrencyID;
                        sABill.ExchangeRate = pPDiscountReturn.ExchangeRate ?? 1;
                        sABill.TypeID = 326;
                        sABill.Type = Type;
                        sABill.ContactName = pPDiscountReturn.OContactName;
                        sABill.ID_MIVAdjust = pPDiscountReturn.ID_MIV;
                        if (checkInvoice == 0) sABill.IDReplaceInv = pPDiscountReturn.ID;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = pPDiscountReturn.ID;
                        foreach (var item in pPDiscountReturn.PPDiscountReturnDetails)
                        {
                            SABillDetail sABillDetail = new SABillDetail();
                            sABillDetail.SABillID = sABill.ID;
                            sABillDetail.AccountingObjectID = item.AccountingObjectID;
                            sABillDetail.Amount = item.Amount ?? 0;
                            sABillDetail.AmountOriginal = item.AmountOriginal ?? 0;
                            sABillDetail.BudgetItemID = item.BudgetItemID;
                            //sABillDetail.CostAccount = item.CostAccount;
                            sABillDetail.ContractID = item.ContractID;
                            sABillDetail.CostSetID = item.CostSetID;
                            sABillDetail.CustomProperty1 = item.CustomProperty1;
                            sABillDetail.CustomProperty2 = item.CustomProperty2;
                            sABillDetail.CustomProperty3 = item.CustomProperty3;
                            sABillDetail.DepartmentID = item.DepartmentID;
                            sABillDetail.Description = item.Description;
                            //sABillDetail.DiscountAccount = item.DiscountAccount;
                            //sABillDetail.DiscountAmount = item.DiscountAmount;
                            //sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                            //sABillDetail.DiscountRate = item.DiscountRate ?? 0;
                            sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                            sABillDetail.OrderPriority = item.OrderPriority ?? 0;
                            sABillDetail.Quantity = item.Quantity ?? 0;
                            //sABillDetail.RepositoryAccount = repository.RepositoryAccount;
                            sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                            sABillDetail.Unit = item.Unit;
                            sABillDetail.UnitPrice = item.UnitPrice ?? 0;
                            sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal ?? 0;
                            sABillDetail.VATAccount = item.VATAccount;
                            sABillDetail.VATAmount = item.VATAmount ?? 0;
                            sABillDetail.VATAmountOriginal = item.VATAmountOriginal ?? 0;
                            sABillDetail.VATRate = item.VATRate ?? 0;
                            sABillDetail.LotNo = item.LotNo;
                            sABillDetail.ExpiryDate = item.ExpiryDate;
                            sABill.SABillDetails.Add(sABillDetail);
                        }
                    }
                    else
                    {
                        sABill = new SABill();
                        sABill.ID = Guid.NewGuid();
                        sABill.AccountingObjectID = sAReturn.AccountingObjectID;
                        sABill.AccountingObjectAddress = sAReturn.AccountingObjectAddress;
                        sABill.AccountingObjectName = sAReturn.AccountingObjectName;
                        sABill.PaymentMethod = sAReturn.PaymentMethod;
                        sABill.Reason = sAReturn.Reason;
                        sABill.AccountingObjectBankAccount = sAReturn.AccountingObjectBankAccount;
                        sABill.AccountingObjectBankName = sAReturn.AccountingObjectBankName;
                        sABill.InvoiceForm = sAReturn.InvoiceForm;
                        sABill.InvoiceTypeID = sAReturn.InvoiceTypeID;
                        sABill.InvoiceTemplate = sAReturn.InvoiceTemplate;
                        sABill.InvoiceSeries = sAReturn.InvoiceSeries;
                        sABill.RefDateTime = sAReturn.RefDateTime;
                        if (checkInvoice == 0) sABill.StatusInvoice = 7;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                        sABill.TotalAmount = sAReturn.TotalAmount;
                        sABill.TotalAmountOriginal = sAReturn.TotalAmountOriginal;
                        sABill.TotalDiscountAmount = sAReturn.TotalDiscountAmount;
                        sABill.TotalDiscountAmountOriginal = sAReturn.TotalDiscountAmountOriginal;
                        sABill.TotalPaymentAmount = sAReturn.TotalPaymentAmount;
                        sABill.TotalPaymentAmountOriginal = sAReturn.TotalPaymentAmountOriginal;
                        sABill.TotalVATAmount = sAReturn.TotalVATAmount;
                        sABill.TotalVATAmountOriginal = sAReturn.TotalVATAmountOriginal;
                        sABill.CheckInvoice = checkInvoice;
                        sABill.CurrencyID = sAReturn.CurrencyID;
                        sABill.ExchangeRate = sAReturn.ExchangeRate ?? 1;
                        sABill.TypeID = 326;
                        sABill.Type = Type;
                        sABill.ContactName = sAReturn.ContactName;
                        sABill.ID_MIVAdjust = sAReturn.ID_MIV;
                        if (checkInvoice == 0) sABill.IDReplaceInv = sAReturn.ID;
                        else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = sAReturn.ID;
                        foreach (var item in sAReturn.SAReturnDetails)
                        {
                            SABillDetail sABillDetail = new SABillDetail();
                            sABillDetail.AccountingObjectID = item.AccountingObjectID;
                            sABillDetail.Amount = item.Amount;
                            sABillDetail.AmountOriginal = item.AmountOriginal;
                            sABillDetail.BudgetItemID = item.BudgetItemID;
                            sABillDetail.CostAccount = item.CostAccount;
                            sABillDetail.ContractID = item.ContractID;
                            sABillDetail.CostSetID = item.CostSetID;
                            sABillDetail.CustomProperty1 = item.CustomProperty1;
                            sABillDetail.CustomProperty2 = item.CustomProperty2;
                            sABillDetail.CustomProperty3 = item.CustomProperty3;
                            sABillDetail.DepartmentID = item.DepartmentID;
                            sABillDetail.Description = item.Description;
                            sABillDetail.DiscountAccount = item.DiscountAccount;
                            sABillDetail.DiscountAmount = item.DiscountAmount;
                            sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                            sABillDetail.DiscountRate = item.DiscountRate ?? 0;
                            sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                            sABillDetail.OrderPriority = item.OrderPriority ?? 0;
                            sABillDetail.Quantity = item.Quantity ?? 0;
                            sABillDetail.RepositoryAccount = item.RepositoryAccount;
                            sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                            sABillDetail.Unit = item.Unit;
                            sABillDetail.UnitPrice = item.UnitPrice;
                            sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal;
                            sABillDetail.VATAccount = item.VATAccount;
                            sABillDetail.VATAmount = item.VATAmount;
                            sABillDetail.VATAmountOriginal = item.VATAmountOriginal;
                            sABillDetail.VATRate = item.VATRate ?? 0;
                            sABillDetail.SABillID = sABill.ID;
                            sABillDetail.IsPromotion = item.IsPromotion;
                            sABillDetail.LotNo = item.LotNo;
                            sABillDetail.ExpiryDate = item.ExpiryDate;
                            sABill.SABillDetails.Add(sABillDetail);
                        }
                    }
                }
                else
                {
                    sABill.ID = Guid.NewGuid();
                    sABill.AccountingObjectID = sABillold.AccountingObjectID;
                    sABill.AccountingObjectAddress = sABillold.AccountingObjectAddress;
                    sABill.AccountingObjectName = sABillold.AccountingObjectName;
                    sABill.PaymentMethod = sABillold.PaymentMethod;
                    sABill.Reason = sABillold.Reason;
                    sABill.AccountingObjectBankAccount = sABillold.AccountingObjectBankAccount;
                    sABill.AccountingObjectBankName = sABillold.AccountingObjectBankName;
                    sABill.InvoiceForm = sABillold.InvoiceForm;
                    sABill.InvoiceTypeID = sABillold.InvoiceTypeID;
                    sABill.InvoiceTemplate = sABillold.InvoiceTemplate;
                    sABill.InvoiceSeries = sABillold.InvoiceSeries;
                    sABill.RefDateTime = sABillold.RefDateTime;
                    if (checkInvoice == 0) sABill.StatusInvoice = 7;
                    else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                    sABill.TotalAmount = sABillold.TotalAmount;
                    sABill.TotalAmountOriginal = sABillold.TotalAmountOriginal;
                    sABill.TotalDiscountAmount = sABillold.TotalDiscountAmount;
                    sABill.TotalDiscountAmountOriginal = sABillold.TotalDiscountAmountOriginal;
                    sABill.TotalPaymentAmount = sABillold.TotalPaymentAmount;
                    sABill.TotalPaymentAmountOriginal = sABillold.TotalPaymentAmountOriginal;
                    sABill.TotalVATAmount = sABillold.TotalVATAmount;
                    sABill.TotalVATAmountOriginal = sABillold.TotalVATAmountOriginal;
                    sABill.CheckInvoice = checkInvoice;
                    sABill.CurrencyID = sABillold.CurrencyID;
                    sABill.ExchangeRate = sABillold.ExchangeRate;
                    sABill.TypeID = 326;
                    sABill.Type = Type;
                    sABill.ContactName = sABillold.ContactName;
                    sABill.ID_MIVAdjust = sABillold.ID_MIV;
                    if (checkInvoice == 0) sABill.IDReplaceInv = sABillold.ID;
                    else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = sABillold.ID;
                    foreach (var item in sABillold.SABillDetails)
                    {
                        SABillDetail sABillDetail = new SABillDetail();
                        sABillDetail.AccountingObjectID = item.AccountingObjectID;
                        sABillDetail.Amount = item.Amount;
                        sABillDetail.AmountOriginal = item.AmountOriginal;
                        sABillDetail.BudgetItemID = item.BudgetItemID;
                        sABillDetail.CostAccount = item.CostAccount;
                        sABillDetail.ContractID = item.ContractID;
                        sABillDetail.CostSetID = item.CostSetID;
                        sABillDetail.CustomProperty1 = item.CustomProperty1;
                        sABillDetail.CustomProperty2 = item.CustomProperty2;
                        sABillDetail.CustomProperty3 = item.CustomProperty3;
                        sABillDetail.DepartmentID = item.DepartmentID;
                        sABillDetail.Description = item.Description;
                        sABillDetail.DiscountAccount = item.DiscountAccount;
                        sABillDetail.DiscountAmount = item.DiscountAmount;
                        sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                        sABillDetail.DiscountRate = item.DiscountRate;
                        sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                        sABillDetail.OrderPriority = item.OrderPriority;
                        sABillDetail.Quantity = item.Quantity;
                        sABillDetail.RepositoryAccount = item.RepositoryAccount;
                        sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                        sABillDetail.Unit = item.Unit;
                        sABillDetail.UnitPrice = item.UnitPrice;
                        sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal;
                        sABillDetail.VATAccount = item.VATAccount;
                        sABillDetail.VATAmount = item.VATAmount;
                        sABillDetail.VATAmountOriginal = item.VATAmountOriginal;
                        sABillDetail.VATRate = item.VATRate;
                        sABillDetail.SABillID = sABill.ID;
                        sABillDetail.IsPromotion = item.IsPromotion;
                        sABillDetail.LotNo = item.LotNo;
                        sABillDetail.ExpiryDate = item.ExpiryDate;
                        sABill.SABillDetails.Add(sABillDetail);
                    }
                }
            }
            else
            {
                sABill.ID = Guid.NewGuid();
                sABill.AccountingObjectID = saInvoice.AccountingObjectID;
                sABill.AccountingObjectAddress = saInvoice.AccountingObjectAddress;
                sABill.AccountingObjectName = saInvoice.AccountingObjectName;
                sABill.BankAccountDetailID = saInvoice.BankAccountDetailID;
                sABill.AccountingObjectBankAccount = saInvoice.AccountingObjectBankAccount;
                sABill.AccountingObjectBankName = saInvoice.AccountingObjectBankName;
                sABill.ListDate = saInvoice.ListDate;
                sABill.ListNo = saInvoice.ListNo;
                sABill.PaymentMethod = saInvoice.PaymentMethod;
                sABill.Reason = saInvoice.Reason;
                sABill.InvoiceForm = saInvoice.InvoiceForm;
                sABill.InvoiceTypeID = saInvoice.InvoiceTypeID;
                sABill.InvoiceTemplate = saInvoice.InvoiceTemplate;
                sABill.InvoiceSeries = saInvoice.InvoiceSeries;
                sABill.RefDateTime = saInvoice.RefDateTime;
                if (checkInvoice == 0) sABill.StatusInvoice = 7;
                else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.StatusInvoice = 8;
                sABill.TotalAmount = saInvoice.TotalAmount;
                sABill.TotalAmountOriginal = saInvoice.TotalAmountOriginal;
                sABill.TotalDiscountAmount = saInvoice.TotalDiscountAmount;
                sABill.TotalDiscountAmountOriginal = saInvoice.TotalDiscountAmountOriginal;
                sABill.TotalPaymentAmount = saInvoice.TotalPaymentAmount;
                sABill.TotalPaymentAmountOriginal = saInvoice.TotalPaymentAmountOriginal;
                sABill.TotalVATAmount = saInvoice.TotalVATAmount;
                sABill.TotalVATAmountOriginal = saInvoice.TotalVATAmountOriginal;
                sABill.CheckInvoice = checkInvoice;
                sABill.CurrencyID = saInvoice.CurrencyID;
                sABill.ExchangeRate = saInvoice.ExchangeRate ?? 1;
                sABill.TypeID = 326;
                sABill.Type = Type;
                sABill.ContactName = saInvoice.ContactName;
                sABill.ID_MIVAdjust = saInvoice.ID_MIV;
                if (checkInvoice == 0) sABill.IDReplaceInv = saInvoice.ID;
                else if (new int[] { 1, 2, 3 }.Any(x => x == checkInvoice)) sABill.IDAdjustInv = saInvoice.ID;
                foreach (var item in saInvoice.SAInvoiceDetails)
                {
                    SABillDetail sABillDetail = new SABillDetail();
                    sABillDetail.AccountingObjectID = item.AccountingObjectID;
                    sABillDetail.Amount = item.Amount;
                    sABillDetail.AmountOriginal = item.AmountOriginal;
                    sABillDetail.BudgetItemID = item.BudgetItemID;
                    sABillDetail.CostAccount = item.CostAccount;
                    sABillDetail.ContractID = item.ContractID;
                    sABillDetail.CostSetID = item.CostSetID;
                    sABillDetail.CustomProperty1 = item.CustomProperty1;
                    sABillDetail.CustomProperty2 = item.CustomProperty2;
                    sABillDetail.CustomProperty3 = item.CustomProperty3;
                    sABillDetail.DepartmentID = item.DepartmentID;
                    sABillDetail.Description = item.Description;
                    sABillDetail.DiscountAccount = item.DiscountAccount;
                    sABillDetail.DiscountAmount = item.DiscountAmount;
                    sABillDetail.DiscountAmountOriginal = item.DiscountAmountOriginal;
                    sABillDetail.DiscountRate = item.DiscountRate ?? 0;
                    sABillDetail.MaterialGoodsID = item.MaterialGoodsID;
                    sABillDetail.OrderPriority = item.OrderPriority ?? 0;
                    sABillDetail.Quantity = item.Quantity ?? 0;
                    sABillDetail.RepositoryAccount = item.RepositoryAccount;
                    sABillDetail.StatisticsCodeID = item.StatisticsCodeID;
                    sABillDetail.Unit = item.Unit;
                    sABillDetail.UnitPrice = item.UnitPrice;
                    sABillDetail.UnitPriceOriginal = item.UnitPriceOriginal;
                    sABillDetail.VATAccount = item.VATAccount;
                    sABillDetail.VATAmount = item.VATAmount;
                    sABillDetail.VATAmountOriginal = item.VATAmountOriginal;
                    sABillDetail.VATRate = item.VATRate ?? 0;
                    sABillDetail.SABillID = sABill.ID;
                    sABillDetail.IsPromotion = item.IsPromotion;
                    sABillDetail.LotNo = item.LotNo;
                    sABillDetail.ExpiryDate = item.ExpiryDate;
                    sABill.SABillDetails.Add(sABillDetail);
                }
            }
            new FSABillDetail(sABill, new List<SABill> { sABill },
                                   ConstFrm.optStatusForm.Edit).ShowDialog(this);
            WaitingFrm.StopWaiting();
        }

        private void AdjustmentOfInvoiceIncrease_Click(object sender, EventArgs e)
        {
            ChangetoSABill(1, 2);
        }

        private void AdjustInvoiceReduction_Click(object sender, EventArgs e)
        {
            ChangetoSABill(2, 3);
        }

        private void AdjustInformation_Click(object sender, EventArgs e)
        {
            ChangetoSABill(3, 4);
        }
    }
}