﻿namespace Accounting
{
	partial class FormView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormView));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.pdfViewer = new Winsoft.PDFium.PdfViewer(this.components);
            this.pdf = new Winsoft.PDFium.Pdf(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.savePictureDialog = new System.Windows.Forms.SaveFileDialog();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.panelScrollBox = new System.Windows.Forms.Panel();
            this.toolStripButtons = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOpenPdf = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveAs = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveBitmap = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonShowInfo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonShowText = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonFirstPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPreviousPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPageNumber = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonNextPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLastPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonZoomOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomIn = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBoxZoom = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRotateLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRotateRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBoxSearch = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.treeViewBookmarks = new System.Windows.Forms.TreeView();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ViewVoucher = new Infragistics.Win.Misc.UltraButton();
            this.AdjustInvoice = new Infragistics.Win.Misc.UltraButton();
            this.ReplaceInvoice = new Infragistics.Win.Misc.UltraButton();
            this.toolStripCancel = new System.Windows.Forms.ToolStrip();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.panel = new System.Windows.Forms.Panel();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AdjustmentOfInvoiceIncrease = new System.Windows.Forms.ToolStripMenuItem();
            this.AdjustInvoiceReduction = new System.Windows.Forms.ToolStripMenuItem();
            this.AdjustInformation = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pdfViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            this.panelScrollBox.SuspendLayout();
            this.toolStripButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.toolStripCancel.SuspendLayout();
            this.panel.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pdfViewer
            // 
            this.pdfViewer.BackColor = System.Drawing.Color.White;
            this.pdfViewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pdfViewer.Location = new System.Drawing.Point(0, 0);
            this.pdfViewer.Margin = new System.Windows.Forms.Padding(0);
            this.pdfViewer.Name = "pdfViewer";
            this.pdfViewer.Pdf = this.pdf;
            this.pdfViewer.Size = new System.Drawing.Size(0, 0);
            this.pdfViewer.TabIndex = 0;
            this.pdfViewer.PageChange += new System.EventHandler(this.pdfViewer_PageChange);
            this.pdfViewer.Paint += new System.Windows.Forms.PaintEventHandler(this.pdfViewer_Paint);
            this.pdfViewer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pdfViewer_MouseDown);
            this.pdfViewer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pdfViewer_MouseMove);
            this.pdfViewer.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pdfViewer_MouseUp);
            // 
            // pdf
            // 
            this.pdf.FileName = null;
            this.pdf.FormFill = true;
            this.pdf.Password = null;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "pdf";
            this.openFileDialog.Filter = "PDF file (*.pdf)|*.pdf|All files( *.*)|*.*";
            this.openFileDialog.Title = "Select PDF file";
            // 
            // savePictureDialog
            // 
            this.savePictureDialog.DefaultExt = "bmp";
            this.savePictureDialog.Filter = "Bitmaps (*.bmp)|*.bmp";
            // 
            // printDialog
            // 
            this.printDialog.AllowCurrentPage = true;
            this.printDialog.AllowSelection = true;
            this.printDialog.AllowSomePages = true;
            this.printDialog.UseEXDialog = true;
            // 
            // panelScrollBox
            // 
            this.panelScrollBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelScrollBox.AutoScroll = true;
            this.panelScrollBox.Controls.Add(this.pdfViewer);
            this.panelScrollBox.Location = new System.Drawing.Point(0, 0);
            this.panelScrollBox.Margin = new System.Windows.Forms.Padding(0);
            this.panelScrollBox.Name = "panelScrollBox";
            this.panelScrollBox.Size = new System.Drawing.Size(710, 452);
            this.panelScrollBox.TabIndex = 0;
            // 
            // toolStripButtons
            // 
            this.toolStripButtons.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripButtons.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpenPdf,
            this.toolStripButtonSaveAs,
            this.toolStripButtonSaveBitmap,
            this.toolStripButtonPrint,
            this.toolStripSeparator1,
            this.toolStripButtonShowInfo,
            this.toolStripButtonShowText,
            this.toolStripSeparator2,
            this.toolStripButtonFirstPage,
            this.toolStripButtonPreviousPage,
            this.toolStripButtonPageNumber,
            this.toolStripButtonNextPage,
            this.toolStripButtonLastPage,
            this.toolStripSeparator3,
            this.toolStripButtonZoomOut,
            this.toolStripButtonZoomIn,
            this.toolStripComboBoxZoom,
            this.toolStripSeparator4,
            this.toolStripButtonRotateLeft,
            this.toolStripButtonRotateRight,
            this.toolStripSeparator5,
            this.toolStripButtonSettings,
            this.toolStripSeparator6,
            this.toolStripTextBoxSearch,
            this.toolStripButtonSearch});
            this.toolStripButtons.Location = new System.Drawing.Point(0, 0);
            this.toolStripButtons.Name = "toolStripButtons";
            this.toolStripButtons.Size = new System.Drawing.Size(919, 31);
            this.toolStripButtons.TabIndex = 0;
            // 
            // toolStripButtonOpenPdf
            // 
            this.toolStripButtonOpenPdf.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpenPdf.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpenPdf.Image")));
            this.toolStripButtonOpenPdf.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonOpenPdf.Name = "toolStripButtonOpenPdf";
            this.toolStripButtonOpenPdf.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonOpenPdf.ToolTipText = "Open PDF File";
            this.toolStripButtonOpenPdf.Click += new System.EventHandler(this.toolStripButtonOpenPdf_Click);
            // 
            // toolStripButtonSaveAs
            // 
            this.toolStripButtonSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveAs.Enabled = false;
            this.toolStripButtonSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveAs.Image")));
            this.toolStripButtonSaveAs.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonSaveAs.Name = "toolStripButtonSaveAs";
            this.toolStripButtonSaveAs.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonSaveAs.ToolTipText = "Save As";
            this.toolStripButtonSaveAs.Click += new System.EventHandler(this.toolStripButtonSaveAs_Click);
            // 
            // toolStripButtonSaveBitmap
            // 
            this.toolStripButtonSaveBitmap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveBitmap.Enabled = false;
            this.toolStripButtonSaveBitmap.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveBitmap.Image")));
            this.toolStripButtonSaveBitmap.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonSaveBitmap.Name = "toolStripButtonSaveBitmap";
            this.toolStripButtonSaveBitmap.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonSaveBitmap.ToolTipText = "Save Bitmap";
            this.toolStripButtonSaveBitmap.Click += new System.EventHandler(this.toolStripButtonSaveBitmap_Click);
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPrint.Enabled = false;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonPrint.ToolTipText = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButtonPrint_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonShowInfo
            // 
            this.toolStripButtonShowInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowInfo.Enabled = false;
            this.toolStripButtonShowInfo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowInfo.Image")));
            this.toolStripButtonShowInfo.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonShowInfo.Name = "toolStripButtonShowInfo";
            this.toolStripButtonShowInfo.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonShowInfo.ToolTipText = "Show Info";
            this.toolStripButtonShowInfo.Click += new System.EventHandler(this.toolStripButtonShowInfo_Click);
            // 
            // toolStripButtonShowText
            // 
            this.toolStripButtonShowText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowText.Enabled = false;
            this.toolStripButtonShowText.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonShowText.Image")));
            this.toolStripButtonShowText.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonShowText.Name = "toolStripButtonShowText";
            this.toolStripButtonShowText.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonShowText.ToolTipText = "Show Text";
            this.toolStripButtonShowText.Click += new System.EventHandler(this.toolStripButtonShowText_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonFirstPage
            // 
            this.toolStripButtonFirstPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFirstPage.Enabled = false;
            this.toolStripButtonFirstPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFirstPage.Image")));
            this.toolStripButtonFirstPage.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonFirstPage.Name = "toolStripButtonFirstPage";
            this.toolStripButtonFirstPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonFirstPage.ToolTipText = "First Page";
            this.toolStripButtonFirstPage.Click += new System.EventHandler(this.toolStripButtonFirstPage_Click);
            // 
            // toolStripButtonPreviousPage
            // 
            this.toolStripButtonPreviousPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPreviousPage.Enabled = false;
            this.toolStripButtonPreviousPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPreviousPage.Image")));
            this.toolStripButtonPreviousPage.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonPreviousPage.Name = "toolStripButtonPreviousPage";
            this.toolStripButtonPreviousPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonPreviousPage.ToolTipText = "Previous Page";
            this.toolStripButtonPreviousPage.Click += new System.EventHandler(this.toolStripButtonPreviousPage_Click);
            // 
            // toolStripButtonPageNumber
            // 
            this.toolStripButtonPageNumber.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPageNumber.Enabled = false;
            this.toolStripButtonPageNumber.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPageNumber.Image")));
            this.toolStripButtonPageNumber.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPageNumber.Name = "toolStripButtonPageNumber";
            this.toolStripButtonPageNumber.Size = new System.Drawing.Size(40, 28);
            this.toolStripButtonPageNumber.Text = "0 of 0";
            this.toolStripButtonPageNumber.ToolTipText = "Select Page";
            this.toolStripButtonPageNumber.Click += new System.EventHandler(this.toolStripButtonPageNumber_Click);
            // 
            // toolStripButtonNextPage
            // 
            this.toolStripButtonNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNextPage.Enabled = false;
            this.toolStripButtonNextPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNextPage.Image")));
            this.toolStripButtonNextPage.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonNextPage.Name = "toolStripButtonNextPage";
            this.toolStripButtonNextPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonNextPage.ToolTipText = "Next Page";
            this.toolStripButtonNextPage.Click += new System.EventHandler(this.toolStripButtonNextPage_Click);
            // 
            // toolStripButtonLastPage
            // 
            this.toolStripButtonLastPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLastPage.Enabled = false;
            this.toolStripButtonLastPage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLastPage.Image")));
            this.toolStripButtonLastPage.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonLastPage.Name = "toolStripButtonLastPage";
            this.toolStripButtonLastPage.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonLastPage.ToolTipText = "Last Page";
            this.toolStripButtonLastPage.Click += new System.EventHandler(this.toolStripButtonLastPage_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonZoomOut
            // 
            this.toolStripButtonZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomOut.Enabled = false;
            this.toolStripButtonZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomOut.Image")));
            this.toolStripButtonZoomOut.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonZoomOut.Name = "toolStripButtonZoomOut";
            this.toolStripButtonZoomOut.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonZoomOut.ToolTipText = "Zoom Out";
            this.toolStripButtonZoomOut.Click += new System.EventHandler(this.toolStripButtonZoomOut_Click);
            // 
            // toolStripButtonZoomIn
            // 
            this.toolStripButtonZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomIn.Enabled = false;
            this.toolStripButtonZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomIn.Image")));
            this.toolStripButtonZoomIn.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonZoomIn.Name = "toolStripButtonZoomIn";
            this.toolStripButtonZoomIn.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonZoomIn.ToolTipText = "Zoom In";
            this.toolStripButtonZoomIn.Click += new System.EventHandler(this.toolStripButtonZoomIn_Click);
            // 
            // toolStripComboBoxZoom
            // 
            this.toolStripComboBoxZoom.AutoToolTip = true;
            this.toolStripComboBoxZoom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxZoom.Enabled = false;
            this.toolStripComboBoxZoom.Items.AddRange(new object[] {
            "10%",
            "25%",
            "50%",
            "75%",
            "100%",
            "125%",
            "150%",
            "200%",
            "400%",
            "800%",
            "Actual Size",
            "Zoom to Page",
            "Fit Width"});
            this.toolStripComboBoxZoom.Name = "toolStripComboBoxZoom";
            this.toolStripComboBoxZoom.Size = new System.Drawing.Size(121, 31);
            this.toolStripComboBoxZoom.ToolTipText = "Zoom";
            this.toolStripComboBoxZoom.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxZoom_SelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonRotateLeft
            // 
            this.toolStripButtonRotateLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotateLeft.Enabled = false;
            this.toolStripButtonRotateLeft.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotateLeft.Image")));
            this.toolStripButtonRotateLeft.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonRotateLeft.Name = "toolStripButtonRotateLeft";
            this.toolStripButtonRotateLeft.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonRotateLeft.ToolTipText = "Rotate Left";
            this.toolStripButtonRotateLeft.Click += new System.EventHandler(this.toolStripButtonRotateLeft_Click);
            // 
            // toolStripButtonRotateRight
            // 
            this.toolStripButtonRotateRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRotateRight.Enabled = false;
            this.toolStripButtonRotateRight.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRotateRight.Image")));
            this.toolStripButtonRotateRight.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonRotateRight.Name = "toolStripButtonRotateRight";
            this.toolStripButtonRotateRight.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonRotateRight.ToolTipText = "Rotate Right";
            this.toolStripButtonRotateRight.Click += new System.EventHandler(this.toolStripButtonRotateRight_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonSettings
            // 
            this.toolStripButtonSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSettings.Enabled = false;
            this.toolStripButtonSettings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSettings.Image")));
            this.toolStripButtonSettings.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonSettings.Name = "toolStripButtonSettings";
            this.toolStripButtonSettings.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonSettings.Text = "Settings...";
            this.toolStripButtonSettings.Click += new System.EventHandler(this.toolStripButtonSettings_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripTextBoxSearch
            // 
            this.toolStripTextBoxSearch.Enabled = false;
            this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
            this.toolStripTextBoxSearch.Size = new System.Drawing.Size(121, 31);
            this.toolStripTextBoxSearch.ToolTipText = "Text to search";
            this.toolStripTextBoxSearch.TextChanged += new System.EventHandler(this.toolStripTextBoxSearch_TextChanged);
            // 
            // toolStripButtonSearch
            // 
            this.toolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSearch.Enabled = false;
            this.toolStripButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSearch.Image")));
            this.toolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSearch.Name = "toolStripButtonSearch";
            this.toolStripButtonSearch.Size = new System.Drawing.Size(28, 28);
            this.toolStripButtonSearch.Text = "toolStripButton1";
            this.toolStripButtonSearch.ToolTipText = "Search";
            this.toolStripButtonSearch.Click += new System.EventHandler(this.toolStripButtonSearch_Click);
            // 
            // treeViewBookmarks
            // 
            this.treeViewBookmarks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewBookmarks.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewBookmarks.FullRowSelect = true;
            this.treeViewBookmarks.HideSelection = false;
            this.treeViewBookmarks.Location = new System.Drawing.Point(0, 0);
            this.treeViewBookmarks.Name = "treeViewBookmarks";
            this.treeViewBookmarks.Size = new System.Drawing.Size(204, 494);
            this.treeViewBookmarks.TabIndex = 0;
            this.treeViewBookmarks.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewBookmarks_NodeMouseClick);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 31);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeViewBookmarks);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.ultraPanel1);
            this.splitContainer.Panel2.Controls.Add(this.panelScrollBox);
            this.splitContainer.Size = new System.Drawing.Size(921, 494);
            this.splitContainer.SplitterDistance = 204;
            this.splitContainer.TabIndex = 0;
            this.splitContainer.TabStop = false;
            this.splitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer_SplitterMoved);
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.ultraPanel1.Appearance = appearance2;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ViewVoucher);
            this.ultraPanel1.ClientArea.Controls.Add(this.AdjustInvoice);
            this.ultraPanel1.ClientArea.Controls.Add(this.ReplaceInvoice);
            this.ultraPanel1.Location = new System.Drawing.Point(2, 452);
            this.ultraPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(708, 39);
            this.ultraPanel1.TabIndex = 1;
            // 
            // ViewVoucher
            // 
            this.ViewVoucher.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ViewVoucher.Location = new System.Drawing.Point(340, 3);
            this.ViewVoucher.Name = "ViewVoucher";
            this.ViewVoucher.Size = new System.Drawing.Size(120, 32);
            this.ViewVoucher.TabIndex = 2;
            this.ViewVoucher.Text = "Xem chứng từ";
            this.ViewVoucher.Click += new System.EventHandler(this.btnViewVoucher_Click);
            // 
            // AdjustInvoice
            // 
            this.AdjustInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AdjustInvoice.Location = new System.Drawing.Point(463, 3);
            this.AdjustInvoice.Name = "AdjustInvoice";
            this.AdjustInvoice.Size = new System.Drawing.Size(120, 32);
            this.AdjustInvoice.TabIndex = 1;
            this.AdjustInvoice.Text = "Điều chỉnh HĐ";
            this.AdjustInvoice.Click += new System.EventHandler(this.btnAdjustInvoice_Click);
            // 
            // ReplaceInvoice
            // 
            this.ReplaceInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReplaceInvoice.Location = new System.Drawing.Point(585, 3);
            this.ReplaceInvoice.Name = "ReplaceInvoice";
            this.ReplaceInvoice.Size = new System.Drawing.Size(120, 32);
            this.ReplaceInvoice.TabIndex = 0;
            this.ReplaceInvoice.Text = "Thay thế hóa đơn";
            this.ReplaceInvoice.Click += new System.EventHandler(this.btnReplaceInvoice_Click);
            // 
            // toolStripCancel
            // 
            this.toolStripCancel.AutoSize = false;
            this.toolStripCancel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar,
            this.toolStripButtonCancel});
            this.toolStripCancel.Location = new System.Drawing.Point(0, 31);
            this.toolStripCancel.Name = "toolStripCancel";
            this.toolStripCancel.Size = new System.Drawing.Size(919, 31);
            this.toolStripCancel.TabIndex = 2;
            this.toolStripCancel.Text = "toolStrip1";
            this.toolStripCancel.Visible = false;
            this.toolStripCancel.ClientSizeChanged += new System.EventHandler(this.toolStripCancel_ClientSizeChanged);
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.AutoSize = false;
            this.toolStripProgressBar.MarqueeAnimationSpeed = 0;
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 28);
            this.toolStripProgressBar.Step = 1;
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoToolTip = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(47, 28);
            this.toolStripButtonCancel.Text = "Cancel";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // panel
            // 
            this.panel.AutoSize = true;
            this.panel.Controls.Add(this.toolStripCancel);
            this.panel.Controls.Add(this.toolStripButtons);
            this.panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(919, 31);
            this.panel.TabIndex = 2;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "pdf";
            this.saveFileDialog.Filter = "PDF file (*.pdf)|*.pdf|All files( *.*)|*.*";
            this.saveFileDialog.Title = "Save As";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdjustmentOfInvoiceIncrease,
            this.AdjustInvoiceReduction,
            this.AdjustInformation});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(232, 92);
            // 
            // AdjustmentOfInvoiceIncrease
            // 
            this.AdjustmentOfInvoiceIncrease.Name = "AdjustmentOfInvoiceIncrease";
            this.AdjustmentOfInvoiceIncrease.Size = new System.Drawing.Size(231, 22);
            this.AdjustmentOfInvoiceIncrease.Text = "Hóa đơn điều chỉnh tăng";
            this.AdjustmentOfInvoiceIncrease.Click += new System.EventHandler(this.AdjustmentOfInvoiceIncrease_Click);
            // 
            // AdjustInvoiceReduction
            // 
            this.AdjustInvoiceReduction.Name = "AdjustInvoiceReduction";
            this.AdjustInvoiceReduction.Size = new System.Drawing.Size(231, 22);
            this.AdjustInvoiceReduction.Text = "Hóa đơn điều chỉnh giảm";
            this.AdjustInvoiceReduction.Click += new System.EventHandler(this.AdjustInvoiceReduction_Click);
            // 
            // AdjustInformation
            // 
            this.AdjustInformation.Name = "AdjustInformation";
            this.AdjustInformation.Size = new System.Drawing.Size(231, 22);
            this.AdjustInformation.Text = "Hóa đơn điều chỉnh thông tin";
            this.AdjustInformation.Click += new System.EventHandler(this.AdjustInformation_Click);
            // 
            // FormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(919, 525);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.splitContainer);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FormView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PDFium viewer";
            this.SizeChanged += new System.EventHandler(this.FormView_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pdfViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            this.panelScrollBox.ResumeLayout(false);
            this.toolStripButtons.ResumeLayout(false);
            this.toolStripButtons.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.toolStripCancel.ResumeLayout(false);
            this.toolStripCancel.PerformLayout();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private Winsoft.PDFium.PdfViewer pdfViewer;
		private Winsoft.PDFium.Pdf pdf;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SaveFileDialog savePictureDialog;
		private System.Windows.Forms.PrintDialog printDialog;
		private System.Windows.Forms.Panel panelScrollBox;
		private System.Windows.Forms.ToolStrip toolStripButtons;
		private System.Windows.Forms.ToolStripButton toolStripButtonOpenPdf;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton toolStripButtonShowInfo;
		private System.Windows.Forms.ToolStripButton toolStripButtonShowText;
		private System.Windows.Forms.ToolStripButton toolStripButtonSaveAs;
		private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton toolStripButtonFirstPage;
		private System.Windows.Forms.ToolStripButton toolStripButtonPreviousPage;
		private System.Windows.Forms.ToolStripButton toolStripButtonPageNumber;
		private System.Windows.Forms.ToolStripButton toolStripButtonNextPage;
		private System.Windows.Forms.ToolStripButton toolStripButtonLastPage;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton toolStripButtonZoomOut;
		private System.Windows.Forms.ToolStripButton toolStripButtonZoomIn;
		private System.Windows.Forms.ToolStripComboBox toolStripComboBoxZoom;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton toolStripButtonRotateLeft;
		private System.Windows.Forms.ToolStripButton toolStripButtonRotateRight;
		private System.Windows.Forms.TreeView treeViewBookmarks;
		private System.Windows.Forms.SplitContainer splitContainer;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripButton toolStripButtonSettings;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSearch;
		private System.Windows.Forms.ToolStripButton toolStripButtonSearch;
		private System.Windows.Forms.ToolStrip toolStripCancel;
		private System.Windows.Forms.Panel panel;
		private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
		private System.Windows.Forms.ToolStripButton toolStripButtonCancel;
		private System.ComponentModel.BackgroundWorker backgroundWorker;
		private System.Windows.Forms.ToolStripButton toolStripButtonSaveBitmap;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton ReplaceInvoice;
        private Infragistics.Win.Misc.UltraButton AdjustInvoice;
        private Infragistics.Win.Misc.UltraButton ViewVoucher;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem AdjustmentOfInvoiceIncrease;
        private System.Windows.Forms.ToolStripMenuItem AdjustInvoiceReduction;
        private System.Windows.Forms.ToolStripMenuItem AdjustInformation;
    }
}

