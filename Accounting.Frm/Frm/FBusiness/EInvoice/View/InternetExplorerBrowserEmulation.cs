﻿using System;
using System.IO;
using Microsoft.Win32;

namespace Accounting
{
    internal static class InternetExplorerBrowserEmulation
    {
        internal static BrowserEmulationVersion GetBrowserEmulationVersion()
        {
            BrowserEmulationVersion emulationVersion = BrowserEmulationVersion.Default;
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);
            if (registryKey != null)
            {
                string fileName = Path.GetFileName(Environment.GetCommandLineArgs()[0]);
                object obj = registryKey.GetValue(fileName, (object)null);
                if (obj != null)
                    emulationVersion = (BrowserEmulationVersion)Convert.ToInt32(obj);
            }
            return emulationVersion;
        }

        internal static int GetInternetExplorerMajorVersion()
        {
            int result = 0;
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Internet Explorer");
            if (registryKey != null)
            {
                object obj = registryKey.GetValue("svcVersion", (object)null) ?? registryKey.GetValue("Version", (object)null);
                if (obj != null)
                {
                    string str = obj.ToString();
                    int length = str.IndexOf('.');
                    if (length != -1)
                        int.TryParse(str.Substring(0, length), out result);
                }
            }
            return result;
        }

        internal static bool IsBrowserEmulationSet()
        {
            return (uint)InternetExplorerBrowserEmulation.GetBrowserEmulationVersion() > 0U;
        }

        internal static bool SetBrowserEmulationVersion(BrowserEmulationVersion browserEmulationVersion)
        {
            bool flag = false;
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", true) ?? Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION");
            string fileName = Path.GetFileName(Environment.GetCommandLineArgs()[0]);
            if (browserEmulationVersion != BrowserEmulationVersion.Default)
                registryKey.SetValue(fileName, (object)browserEmulationVersion, RegistryValueKind.DWord);
            else
                registryKey.DeleteValue(fileName, false);
            flag = true;
            return flag;
        }

        internal static bool SetBrowserEmulationVersion()
        {
            int explorerMajorVersion = InternetExplorerBrowserEmulation.GetInternetExplorerMajorVersion();
            BrowserEmulationVersion browserEmulationVersion;
            if (explorerMajorVersion >= 11)
            {
                browserEmulationVersion = BrowserEmulationVersion.const_8;
            }
            else
            {
                switch (explorerMajorVersion - 8)
                {
                    case 0:
                        browserEmulationVersion = BrowserEmulationVersion.Version8;
                        break;
                    case 1:
                        browserEmulationVersion = BrowserEmulationVersion.Version9;
                        break;
                    case 2:
                        browserEmulationVersion = BrowserEmulationVersion.const_6;
                        break;
                    default:
                        browserEmulationVersion = BrowserEmulationVersion.Version7;
                        break;
                }
            }
            return InternetExplorerBrowserEmulation.SetBrowserEmulationVersion(browserEmulationVersion);
        }
    }
    internal enum BrowserEmulationVersion
    {
        Default = 0,
        Version7 = 7000,
        Version8 = 8000,
        Version8Standards = 8888,
        Version9 = 9000,
        Version9Standards = 9999,
        const_6 = 10000,
        Version10Standards = 10001,
        const_8 = 11000,
        Version11Edge = 11001,
    }
}
