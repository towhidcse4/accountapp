﻿namespace Accounting
{
    partial class ViewPDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewPDF));
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.combobox = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.GroupBox_Table = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.combobox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.combobox);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(800, 31);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // combobox
            // 
            this.combobox.Location = new System.Drawing.Point(6, 4);
            this.combobox.Name = "combobox";
            this.combobox.Size = new System.Drawing.Size(90, 21);
            this.combobox.TabIndex = 0;
            this.combobox.ValueChanged += new System.EventHandler(this.combobox_ValueChanged);
            // 
            // GroupBox_Table
            // 
            this.GroupBox_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBox_Table.Location = new System.Drawing.Point(0, 31);
            this.GroupBox_Table.Name = "GroupBox_Table";
            this.GroupBox_Table.Size = new System.Drawing.Size(800, 419);
            this.GroupBox_Table.TabIndex = 1;
            // 
            // ViewPDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GroupBox_Table);
            this.Controls.Add(this.ultraGroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewPDF";
            this.Text = "ViewPDF";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.combobox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox_Table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor combobox;
        private Infragistics.Win.Misc.UltraGroupBox GroupBox_Table;
    }
}