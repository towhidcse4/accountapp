﻿namespace Accounting
{
    partial class FEMAllocationRequiredDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.dtRequestDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtApovedReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReceiver = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtBudgetYear = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.txtBudgetMonth = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridPosted = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtRequestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApovedReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.TextHAlignAsString = "Left";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.dtRequestDate);
            this.ultraGroupBox1.Controls.Add(this.txtApovedReason);
            this.ultraGroupBox1.Controls.Add(this.lblReceiver);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectID);
            appearance5.FontData.BoldAsString = "True";
            appearance5.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance5;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(827, 112);
            this.ultraGroupBox1.TabIndex = 30;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // dtRequestDate
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.dtRequestDate.Appearance = appearance2;
            this.dtRequestDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtRequestDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtRequestDate.Location = new System.Drawing.Point(134, 28);
            this.dtRequestDate.MaskInput = "";
            this.dtRequestDate.Name = "dtRequestDate";
            this.dtRequestDate.Size = new System.Drawing.Size(140, 21);
            this.dtRequestDate.TabIndex = 1;
            this.dtRequestDate.Value = null;
            // 
            // txtApovedReason
            // 
            this.txtApovedReason.Location = new System.Drawing.Point(134, 60);
            this.txtApovedReason.Multiline = true;
            this.txtApovedReason.Name = "txtApovedReason";
            this.txtApovedReason.Size = new System.Drawing.Size(687, 41);
            this.txtApovedReason.TabIndex = 2;
            // 
            // lblReceiver
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.lblReceiver.Appearance = appearance3;
            this.lblReceiver.Location = new System.Drawing.Point(9, 60);
            this.lblReceiver.Name = "lblReceiver";
            this.lblReceiver.Size = new System.Drawing.Size(98, 19);
            this.lblReceiver.TabIndex = 24;
            this.lblReceiver.Text = "Diễn giải :";
            // 
            // lblAccountingObjectID
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectID.Appearance = appearance4;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(9, 30);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(98, 19);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Ngày yêu cầu :";
            // 
            // ultraGroupBox2
            // 
            appearance6.TextHAlignAsString = "Left";
            this.ultraGroupBox2.Appearance = appearance6;
            this.ultraGroupBox2.Controls.Add(this.txtBudgetYear);
            this.ultraGroupBox2.Controls.Add(this.txtBudgetMonth);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance9;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(827, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(197, 112);
            this.ultraGroupBox2.TabIndex = 31;
            this.ultraGroupBox2.Text = "Kỳ cấp kinh phí";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtBudgetYear
            // 
            this.txtBudgetYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtBudgetYear.Location = new System.Drawing.Point(74, 56);
            this.txtBudgetYear.MaxValue = 9999;
            this.txtBudgetYear.MinValue = 0;
            this.txtBudgetYear.Name = "txtBudgetYear";
            this.txtBudgetYear.Nullable = true;
            this.txtBudgetYear.Size = new System.Drawing.Size(117, 21);
            this.txtBudgetYear.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left;
            this.txtBudgetYear.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtBudgetYear.TabIndex = 4;
            // 
            // txtBudgetMonth
            // 
            this.txtBudgetMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.txtBudgetMonth.Location = new System.Drawing.Point(74, 26);
            this.txtBudgetMonth.MaxValue = 12;
            this.txtBudgetMonth.MinValue = 0;
            this.txtBudgetMonth.Name = "txtBudgetMonth";
            this.txtBudgetMonth.Nullable = true;
            this.txtBudgetMonth.Size = new System.Drawing.Size(117, 21);
            this.txtBudgetMonth.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left;
            this.txtBudgetMonth.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.OnMouseEnter;
            this.txtBudgetMonth.TabIndex = 3;
            // 
            // ultraLabel4
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance7;
            this.ultraLabel4.Location = new System.Drawing.Point(9, 63);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(59, 19);
            this.ultraLabel4.TabIndex = 24;
            this.ultraLabel4.Text = "Năm :";
            // 
            // ultraLabel5
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance8;
            this.ultraLabel5.Location = new System.Drawing.Point(9, 30);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(59, 19);
            this.ultraLabel5.TabIndex = 0;
            this.ultraLabel5.Text = "Tháng :";
            // 
            // uGridPosted
            // 
            this.uGridPosted.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridPosted.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridPosted.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridPosted.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            this.uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridPosted.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridPosted.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridPosted.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridPosted.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridPosted.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridPosted.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridPosted.Location = new System.Drawing.Point(0, 118);
            this.uGridPosted.Name = "uGridPosted";
            this.uGridPosted.Size = new System.Drawing.Size(1024, 350);
            this.uGridPosted.TabIndex = 32;
            this.uGridPosted.Text = "ultraGrid1";
            // 
            // FEMAllocationRequiredDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 471);
            this.Controls.Add(this.uGridPosted);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Name = "FEMAllocationRequiredDetail";
            this.Text = "Yêu cầu cấp kinh phí.";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtRequestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApovedReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBudgetMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtApovedReason;
        private Infragistics.Win.Misc.UltraLabel lblReceiver;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtBudgetYear;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor txtBudgetMonth;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
    }
}