﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Infragistics.Win.UltraWinGrid;
using Accounting.TextMessage;
using System.Linq;


namespace Accounting
{
    public partial class FEMAllocationRequired : CustormForm
    {
        #region Khai Báo
        private readonly IEMAllocationService _IEMAllocationService;
        private readonly IEMAllocationDetailService _IEMAllocationDetailService;
        private readonly IEMAllocationRequiredService _IEMAllocationRequiredService;
        private readonly IEMAllocationRequiredDetailService _IEMAllocationRequiredDetailService;
        private readonly IBudgetItemService _IBudgetItemService;
        #endregion

        #region khởi tạo
        public FEMAllocationRequired()
        {
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.CenterToScreen();
            #endregion

            #region Thiết lập ban đầu cho Form

            _IEMAllocationService = IoC.Resolve<IEMAllocationService>();
            _IEMAllocationDetailService = IoC.Resolve<IEMAllocationDetailService>();
            _IEMAllocationRequiredService = IoC.Resolve<IEMAllocationRequiredService>();
            _IEMAllocationRequiredDetailService = IoC.Resolve<IEMAllocationRequiredDetailService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();

            this.uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
            LoadDuLieu();
            #endregion
        }

        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }
        private void LoadDuLieu(bool configGrid = true)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMAllocation> list = _IEMAllocationService.GetAll();
            List<EMAllocationDetail> list1 = _IEMAllocationDetailService.GetAll();
            List<EMAllocationRequired> list2 = _IEMAllocationRequiredService.GetAll();
            List<EMAllocationRequiredDetail> list3 = _IEMAllocationRequiredDetailService.GetAll();
            //List<BudgetItem> list4 = _IBudgetItemService.Query.Where(c => c.BudgetItemName.StartsWith("1") || c.BudgetItemName.StartsWith("2") || c.BudgetItemName.StartsWith("3") || c.BudgetItemName.StartsWith("4")).ToList();
            List<BudgetItem> list4 = _IBudgetItemService.GetListBudgetItemBudgetItemNameStartWith("1;2;3;4");
            #endregion

            #region Xử lý dữ liệu
            foreach (var item in list3)
            {
                item.RequestAmount = (item.AprovedAmount - item.Amount);
            }

            foreach (var item in list3)
            {
                foreach (var ls2 in list4)
                {
                    if (item.BudgetItemID == ls2.ID)
                    {
                        item.BudgetItemName = ls2.BudgetItemName;

                    }
                }
            }


            foreach (var item in list3)
            {

                foreach (var ls2 in list2)
                {
                    if (item.EMAllocationRequiredID == ls2.ID)
                    {
                        foreach (var ls1 in list)
                        {
                            if (ls2.ID == ls1.EMAllocationRequiredID)
                            {
                                foreach (var ls in list1)
                                {
                                    if (ls.EMAllocationID == ls1.ID)
                                    {
                                        item.Amount = ls.Amount;
                                    }
                                }
                            }

                        }
                    }

                }
            }

            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = list2.ToArray();
            uGridPosted.DataSource = list3.ToArray();
            //uGrid3.DataSource = List2.ToArray();

            if (uGrid.Rows.Count > 0)
            {
                uGrid.Rows[0].Selected = true;
            }

            if (configGrid) ConfigGrid(uGrid);
            if (configGrid) ConfigGrid1(uGridPosted);

            #endregion
        }
        #endregion

        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            addFunction();
        }

        private void tsmEdit_Click_1(object sender, EventArgs e)
        {
            editFunction();
        }

        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            deleteFunction();
        }

        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }
        #endregion

        #region Button
        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            addFunction();
        }
        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            editFunction();
        }
        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            deleteFunction();
        }
        #endregion

        private void uGrid_MouseDown_1(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow_1(object sender, DoubleClickRowEventArgs e)
        {
            editFunction();
        }



        // <summary>
        // Nghiệp vụ Add
        // </summary>
        void addFunction()
        {
            new FEMAllocationRequiredDetail().ShowDialog(this);
            if (!FEMAllocationRequiredDetail.isClose) LoadDuLieu();

        }


        // Nghiệp vụ Edit
        // </summary>
        void editFunction()
        {

            if (uGrid.Selected.Rows.Count > 0)
            {
                EMAllocationRequired temp = uGrid.Selected.Rows[0].ListObject as EMAllocationRequired;
                new FEMAllocationRequiredDetail(temp).ShowDialog(this);
                if (!FEMAllocationRequiredDetail.isClose) LoadDuLieu();
            }
            else
                Accounting.TextMessage.MSG.Error(Accounting.TextMessage.resSystem.MSG_System_04);
        }


        // Nghiệp vụ Delete
        // </summary>
        void deleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMAllocationRequired temp = uGrid.Selected.Rows[0].ListObject as EMAllocationRequired;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp)) == DialogResult.Yes)
                {
                    _IEMAllocationService.BeginTran();
                    //List<EMShareHolderTransaction> listGenTemp = _IEMShareHolderTransactionService.Query.Where(p => p.EMShareHolderID == temp.ID).ToList();
                    //foreach (var shareholdertransaction in listGenTemp)
                    //{
                    //    _IEMShareHolderTransactionService.Delete(shareholdertransaction);
                    //}
                    _IEMAllocationRequiredService.Delete(temp);
                    _IEMAllocationRequiredService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion
        #region Utils
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMAllocationRequired_TableName);
            //uGrid1.DisplayLayout.Bands[0].Columns["BookIssuedDate"].Format = Constants.DdMMyyyy;
        }

        void ConfigGrid1(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMAllocationRequiredDetail_TableName);
            //uGrid1.DisplayLayout.Bands[0].Columns["BookIssuedDate"].Format = Constants.DdMMyyyy;
        }
        #endregion

        private void uGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGrid.Selected.Rows.Count < 1) return;
            object listObject = uGrid.Selected.Rows[0].ListObject;
            if (listObject == null) return;
            EMAllocationRequired cd = listObject as EMAllocationRequired;
            if (cd == null) return;

            if (cd.EMAllocationRequiredDetails.Count == 0) cd.EMAllocationRequiredDetails = new List<EMAllocationRequiredDetail>();
            uGridPosted.DataSource = cd.EMAllocationRequiredDetails.ToList();

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;
            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();


            //Thêm tổng số ở cột 
            SummarySettings summary = band.Summaries.Add("RequestAmount", SummaryType.Sum, band.Columns["RequestAmount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            SummarySettings summary1 = band.Summaries.Add("AprovedAmount", SummaryType.Sum, band.Columns["AprovedAmount"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary1.DisplayFormat = "{0:N0}";
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            SummarySettings summary2 = band.Summaries.Add("Amount", SummaryType.Sum, band.Columns["Amount"]);
            summary2.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary2.DisplayFormat = "{0:N0}";
            summary2.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            SummarySettings summary3 = band.Summaries.Add("RestAmount", SummaryType.Sum, band.Columns["RestAmount"]);
            summary3.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary3.DisplayFormat = "{0:N0}";
            summary3.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            #endregion

        }


    }
}
