﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinSchedule.CalendarCombo;
using Accounting.Model;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace Accounting
{
    public partial class FEMAllocationRequiredDetail : CustormForm
    {

        #region khai báo
        private readonly IEMAllocationService _IEMAllocationService;
        private readonly IEMAllocationDetailService _IEMAllocationDetailService;
        private readonly IEMAllocationRequiredService _IEMAllocationRequiredService;
        private readonly IEMAllocationRequiredDetailService _IEMAllocationRequiredDetailService;
        private readonly IBudgetItemService _IBudgetItemService;

        EMAllocationRequired _Select = new EMAllocationRequired();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FEMAllocationRequiredDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IEMAllocationService = IoC.Resolve<IEMAllocationService>();
            _IEMAllocationDetailService = IoC.Resolve<IEMAllocationDetailService>();
            _IEMAllocationRequiredService = IoC.Resolve<IEMAllocationRequiredService>();
            _IEMAllocationRequiredDetailService = IoC.Resolve<IEMAllocationRequiredDetailService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();

            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            #endregion

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion
        }

        public FEMAllocationRequiredDetail(EMAllocationRequired temp)
        {//Sửa
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();

            #endregion

            #region Thiết lập ban đầu cho Form
            _Select = temp;
            Them = false;
            //Khai báo các webservices
            _IEMAllocationService = IoC.Resolve<IEMAllocationService>();
            _IEMAllocationDetailService = IoC.Resolve<IEMAllocationDetailService>();
            _IEMAllocationRequiredService = IoC.Resolve<IEMAllocationRequiredService>();
            _IEMAllocationRequiredDetailService = IoC.Resolve<IEMAllocationRequiredDetailService>();
            _IBudgetItemService = IoC.Resolve<IBudgetItemService>();


            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            #endregion

            #region Fill dữ liệu Obj vào control
            ObjandGUI(temp, false);

            this.Text = "Yêu cầu cấp chi phí";

            #endregion
        }

        private void InitializeGUI()
        {

            #region Lấy dữ liệu từ CSDL
            List<EMAllocationRequiredDetail> list = _IEMAllocationRequiredDetailService.GetAll();
            //List<BudgetItem> list1 = _IBudgetItemService.Query.Where(c => c.BudgetItemName.StartsWith("1") || c.BudgetItemName.StartsWith("2") || c.BudgetItemName.StartsWith("3") || c.BudgetItemName.StartsWith("4")).ToList();
            List<BudgetItem> list1 = _IBudgetItemService.GetListBudgetItemBudgetItemNameStartWith("1;2;3;4");
            #endregion


            #region hiển thị và xử lý hiển thị

            foreach (var item in list)
            {
                foreach (var ls2 in list1)
                {
                    if (item.BudgetItemID == ls2.ID)
                    {
                        item.BudgetItemName = ls2.BudgetItemName;

                    }
                }
            }

            uGridPosted.DataSource = list;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;

            ConfigGridDong(mauGiaoDien);
            ConfigGrid1(uGridPosted);
            #endregion


        }
        public Template mauGiaoDien { get; set; }
        #endregion

        void ConfigGrid1(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {
            List<TemplateColumn> dstemplatecolums = new List<TemplateColumn>();

            Dictionary<string, Dictionary<string, TemplateColumn>> dicTemp = new Dictionary<string, Dictionary<string, TemplateColumn>>();
            string nameTable = string.Empty;
            List<string> strColumnName, strColumnCaption, strColumnToolTip = new List<string>();
            List<bool> bolIsReadOnly, bolIsVisible, bolIsVisibleCbb = new List<bool>();
            List<int> intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition = new List<int>();
            List<int> VTbolIsVisible, VTbolIsVisibleCbb = new List<int>();

            strColumnName = new List<string>() { "ID", "EMAllocationRequiredID", "BudgetItemID", "BudgetItemName", "RequestAmount", "AprovedAmount", "Amount", "RestAmount", "RequestDescription", "AprovedDescription", "OrderPriority" };
            strColumnCaption = strColumnToolTip = new List<string>() { "ID ", "EMAllocationRequiredID", "BudgetItemID ", "Mục chi", "Số yêu cầu", "Số được duyệt", "Số cấp phát", "Số còn lại", "Ghi chú", "AprovedDescription", "OrderPriority" };
            bolIsReadOnly = new List<bool>();
            bolIsVisible = new List<bool>();
            bolIsVisibleCbb = new List<bool>();
            intColumnWidth = new List<int>();
            intColumnMaxWidth = new List<int>();
            intColumnMinWidth = new List<int>();
            intVisiblePosition = new List<int>();
            VTbolIsVisible = new List<int>() { 3, 4, 8 };    //vị trí có giá trị bằng true
            VTbolIsVisibleCbb = new List<int>() { 1, 2 };
            for (int i = 0; i < 11; i++)
            {
                bolIsVisible.Add(VTbolIsVisible.Contains(i));
                bolIsVisibleCbb.Add(VTbolIsVisibleCbb.Contains(i));
                bolIsReadOnly.Add(false);
                intColumnWidth.Add(-1);
                intColumnMaxWidth.Add(-1);
                intColumnMinWidth.Add(-1);
                intVisiblePosition.Add(-1);
            }
            dstemplatecolums = ConstDatabase.CreateData(strColumnName, strColumnCaption, strColumnToolTip, bolIsReadOnly, bolIsVisible, bolIsVisibleCbb, intColumnWidth, intColumnMaxWidth, intColumnMinWidth, intVisiblePosition).Values.ToList();

            Utils.ConfigGrid(utralGrid, fEMShareHolder.database.EMAllocationRequiredDetail_TableName, dstemplatecolums, 0);
        }

        private void ConfigGridDong(Template mauGiaoDien)
        {
            #region Grid động

            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGridPosted.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            uGridPosted.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];

            #endregion
        }


        #region Utils
        EMAllocationRequired ObjandGUI(EMAllocationRequired input, bool isGet)
        {
            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới
                input.RequestDate = dtRequestDate.DateTime;
                input.ApovedReason = txtApovedReason.Text;

                if (!string.IsNullOrEmpty(txtBudgetMonth.Value.ToString()))
                {
                    input.BudgetMonth = Int32.Parse(txtBudgetMonth.Value.ToString());
                }

                if (!string.IsNullOrEmpty(txtBudgetYear.Value.ToString()))
                {
                    input.BudgetYear = Int32.Parse(txtBudgetYear.Value.ToString());
                }


            }
            else
            {
                dtRequestDate.DateTime = input.RequestDate;
                txtApovedReason.Text = input.ApovedReason;

                if (input.BudgetMonth > 0)
                {
                    txtBudgetMonth.Value = input.BudgetMonth.ToString();
                }

                if (input.BudgetYear > 0)
                {
                    txtBudgetYear.Value = input.BudgetYear.ToString();
                }


            }
            return input;
        }
        #endregion

    }
}
