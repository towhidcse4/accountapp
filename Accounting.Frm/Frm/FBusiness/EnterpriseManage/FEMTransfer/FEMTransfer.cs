﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FEMTransfer : CustormForm
    {
        #region khai báo
        private readonly IEMTransferService _IEMTransferService;
        private readonly IEMTransferDetailService _IEMTransferDetailService;
        private readonly IStockCategoryService _IStockCategoryService;
       
        List<EMTransfer> _dsEMTransfer = new List<EMTransfer>();
        #endregion

         #region khởi tạo
        public FEMTransfer()
        {
            WaitingFrm.StartWaiting();
            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();
            #endregion

            #region Thiết lập ban đầu cho Form
            _IEMTransferService = IoC.Resolve<IEMTransferService>();
            _IEMTransferDetailService = IoC.Resolve<IEMTransferDetailService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();

            uGrid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            #endregion

            #region Tạo dữ liệu ban đầu
          

            LoadDuLieu();

            //chọn tự select dòng đầu tiên
            if (uGrid.Rows.Count > 0) uGrid.Rows[0].Selected = true;
            #endregion
            WaitingFrm.StopWaiting();
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL
            List<EMTransfer> lt = _IEMTransferService.GetAll();
            List<EMTransferDetail> list = _IEMTransferDetailService.GetAll();
            List<StockCategory> list1 = _IStockCategoryService.GetAll();
            #endregion

            #region Xử lý dữ liệu
            foreach (var item in list)
            {


                foreach (var ls2 in list1)
                {
                    if (item.StockCategoryID == ls2.ID)
                    {
                        item.StockCategoryView = ls2.StockCategoryName;
                    }
                }
            }
            foreach (var item in list)
            {


                foreach (var ls2 in lt)
                {
                    if (item.EMTransferID == ls2.ID)
                    {
                        item.QuantityTransfered = ls2.Quantity;
                        item.AmountTransfered = ls2.Amount;
                    }
                }
            }
            #endregion

            #region hiển thị và xử lý hiển thị
            uGrid.DataSource = lt.ToArray();
            uGridPosted.DataSource = list.ToArray();

            if (configGrid) ConfigGrid(uGrid);
            if (configGrid) ConfigGrid1(uGridPosted);

            #endregion
        }
        #endregion


        #region Nghiệp vụ
        #region ContentMenu
        private void tsmAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }
        private void tsmDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        private void tmsReLoad_Click_1(object sender, EventArgs e)
        {
            LoadDuLieu(false);
        }

        #endregion

        #region Button

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }
        #endregion

        private void uGrid_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, uGrid, cms4Grid);
        }

        private void uGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            AddFunction();
        }


        /// <summary>
        /// Nghiệp vụ Add
        /// </summary>
        void AddFunction()
        {

            EMTransfer temp = uGrid.Selected.Rows.Count <= 0 ? new EMTransfer() : uGrid.Selected.Rows[0].ListObject as EMTransfer;
            new FEMTransferDetail().ShowDialog(this);
            if (!FEMTransferDetail.isClose) LoadDuLieu();
        }

        /// <summary>
        /// Nghiệp vụ Edit
        /// </summary>
        //void EditFunction()
        //{
        //    if (uGrid.Selected.Rows.Count > 0)
        //    {
        //        EMTransfer temp = uGrid.Selected.Rows[0].ListObject as EMTransfer;
        //        new FEMTransferDetail(temp).ShowDialog(this);
        //        if (!FEMTransferDetail.isClose) LoadDuLieu();
        //    }
        //    else
        //        MSG.Error(resSystem.MSG_HeThong_04);
        //}

        /// <summary>
        /// Nghiệp vụ Delete
        /// </summary>
        void DeleteFunction()
        {
            if (uGrid.Selected.Rows.Count > 0)
            {
                EMTransfer temp = uGrid.Selected.Rows[0].ListObject as EMTransfer;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, temp.TransferCode)) == DialogResult.Yes)
                {
                    _IEMTransferService.BeginTran();
                    //List<EMTransferDetail> listGenTemp = _IEMTransferDetailService.Query.Where(p => p.EMTransferID == temp.ID).ToList();
                    //foreach (var i in listGenTemp)
                    //{
                    //    _IEMTransferDetailService.Delete(i);
                    //}
                    _IEMTransferService.Delete(temp);
                    _IEMTransferService.CommitTran();
                    LoadDuLieu();
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }
        #endregion

        #region Utils
        void ConfigGrid(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.EMTransfer_TableName);
            utralGrid.DisplayLayout.Bands[0].Columns["TransferDate"].Format = "dd/MM/yyyy";

            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];

            Infragistics.Win.UltraWinGrid.SummarySettings summaryQuantity = band.Summaries.Add("Quantity", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns["Quantity"]);
            summaryQuantity.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryQuantity.DisplayFormat = "{0:N0}";
            summaryQuantity.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;


            Infragistics.Win.UltraWinGrid.SummarySettings summaryAmount = band.Summaries.Add("Amount", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band.Columns["Amount"]);
            summaryAmount.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryAmount.DisplayFormat = "{0:N0}";
            summaryAmount.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;
        }
        #endregion     
        void ConfigGrid1(UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, ConstDatabase.EMTransferDetail_TableName);
            //Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMTransferDetail_TableName, new List<TemplateColumn>(), 0);
            Infragistics.Win.UltraWinGrid.UltraGridBand band1 = utralGrid.DisplayLayout.Bands[0];

            Infragistics.Win.UltraWinGrid.SummarySettings summaryQuantity = band1.Summaries.Add("QuantityTransfered", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band1.Columns["QuantityTransfered"]);
            summaryQuantity.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryQuantity.DisplayFormat = "{0:N0}";
            summaryQuantity.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;

            Infragistics.Win.UltraWinGrid.SummarySettings summaryAmount = band1.Summaries.Add("AmountTransfered", Infragistics.Win.UltraWinGrid.SummaryType.Sum, band1.Columns["AmountTransfered"]);
            summaryAmount.SummaryPosition = Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn;
            summaryAmount.DisplayFormat = "{0:N0}";
            summaryAmount.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed;
          
        }



        private void uGrid_AfterSelectChange_1(object sender, AfterSelectChangeEventArgs e)
        {
          
                if (uGrid.Selected.Rows.Count < 1) return;
                object listObject = uGrid.Selected.Rows[0].ListObject;
                if (listObject == null) return;
                EMTransfer cd = listObject as EMTransfer;
                if (cd == null) return;
                //

                //Tab thông tin chung
                lblTransferDate.Text = cd.TransferDate.ToString("dd/MM/yyyy");
                lblDescription.Text = cd.Description;
                lblAmount.Text = cd.Amount.ToString();
                lblQuantity.Text = cd.Quantity.ToString();
                lblTransferCode.Text = cd.TransferCode.ToString();
                lblToEMShareHolderName.Text = cd.ToEMShareHolderName.ToString();
                lblFromEMShareHolderName.Text = cd.FromEMShareHolderName.ToString();

                //LoadDuLieu(true);
                ////Tab hoạch toán

                //if (cd.EMTransferDetails.Count == 0) cd.EMTransferDetails = new List<EMTransferDetail>();
                uGridPosted.DataSource = cd.EMTransferDetails.ToList();
           

            #region Config Grid
            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;
            //Fix Header
            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
            band.Summaries.Clear();
            //Thêm tổng số ở cột "Số tiền"
            SummarySettings summary = band.Summaries.Add("QuantityTransfered", SummaryType.Sum, band.Columns["QuantityTransfered"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;

            SummarySettings summary1 = band.Summaries.Add("AmountTransfered", SummaryType.Sum, band.Columns["AmountTransfered"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary1.DisplayFormat = "{0:N0}";
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
          
            #endregion
        }

        private void FEMTransfer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGrid.ResetText();
            uGrid.ResetUpdateMode();
            uGrid.ResetExitEditModeOnLeave();
            uGrid.ResetRowUpdateCancelAction();
            uGrid.DataSource = null;
            uGrid.Layouts.Clear();
            uGrid.ResetLayouts();
            uGrid.ResetDisplayLayout();
            uGrid.Refresh();
            uGrid.ClearUndoHistory();
            uGrid.ClearXsdConstraints();
            
            uGridPosted.ResetText();
            uGridPosted.ResetUpdateMode();
            uGridPosted.ResetExitEditModeOnLeave();
            uGridPosted.ResetRowUpdateCancelAction();
            uGridPosted.DataSource = null;
            uGridPosted.Layouts.Clear();
            uGridPosted.ResetLayouts();
            uGridPosted.ResetDisplayLayout();
            uGridPosted.Refresh();
            uGridPosted.ClearUndoHistory();
            uGridPosted.ClearXsdConstraints();
        }

        private void FEMTransfer_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
