﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;
using Accounting.TextMessage;
using Infragistics.Win.UltraWinSchedule.CalendarCombo;
using Accounting.Model;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace Accounting
{
    public partial class FEMTransferDetail : CustormForm
    {
        #region khai báo
        public Guid IdEMTransferDetail;
        private readonly IEMShareHolderService _IEMShareHolderService;
        private readonly IEMTransferService _IEMTransferService;
        private readonly IStockCategoryService _IStockCategoryService;
        private readonly IEMTransferDetailService _IEMTransferDetailService;
        private readonly IInvestorService _IInvestorService;

        //List<EMTransfer> _dsEMTransfer = new List<EMTransfer>();
        EMTransfer _Select = new EMTransfer();
        bool Them = true;
        public static bool isClose = true;
        #endregion

        #region khởi tạo
        public FEMTransferDetail()
        {//Thêm

            #region Khởi tạo giá trị mặc định của Form
            InitializeComponent();

            #endregion

            #region Thiết lập ban đầu cho Form
            //Khai báo các webservices
            _IEMShareHolderService = IoC.Resolve<IEMShareHolderService>();
            _IEMTransferService = IoC.Resolve<IEMTransferService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IStockCategoryService = IoC.Resolve<IStockCategoryService>();
            _IEMTransferDetailService = IoC.Resolve<IEMTransferDetailService>();
            _IInvestorService = IoC.Resolve<IInvestorService>();

            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            //Lấy các giá trị const từ XML
            //<mặc định đã tự khởi tạo>

            //Lấy các giá trị const từ Database
            #region Load config hiển thị dữ liệu từ Database (xử lý giao diện mềm dẻo)

            #endregion

            //khởi tạo và hiển thị giá trị ban đầu cho các control
            InitializeGUI();
            dtTransferDate.DateTime = DateTime.Now;
            this.Text = "Phiếu chuyển nhượng.";

            #endregion
        }
        void LoadDuLieu()
        {
            LoadDuLieu(true);
        }

        private void LoadDuLieu(bool configGrid)
        {
            #region Lấy dữ liệu từ CSDL

            List<EMTransferDetail> list1 = new List<EMTransferDetail>();
            BindingList<EMTransferDetail> bdEMRegistrationDetail = new BindingList<EMTransferDetail>(list1);
            #endregion

            #region hiển thị và xử lý hiển thị
            uGridPosted.DataSource = bdEMRegistrationDetail;
            uGridPosted.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            uGridPosted.DisplayLayout.Override.TemplateAddRowPrompt = "Bấm vào đây để thêm mới....";
            uGridPosted.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.None;
            //Hiện những dòng trống?
            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.ExtendFirstCell;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.Default;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;

            ConfigGridDong(mauGiaoDien);
            ConfigGrid(uGridPosted);
            #endregion
        }



        void loadcbb()
        {
            cbbFromEMShareHolderID.DataSource = _IEMShareHolderService.GetAll();
            cbbToEMShareHolderID.DataSource = _IEMShareHolderService.GetAll();
            cbbToEMShareHolderID1.DataSource = _IEMShareHolderService.GetAll();
            cbbFromEMShareHolderID.DisplayMember = "PersonalTaxCode";
            cbbToEMShareHolderID.DisplayMember = "PersonalTaxCode";
            cbbToEMShareHolderID1.DisplayMember = "PersonalTaxCode";
            Utils.ConfigGrid(cbbFromEMShareHolderID, ConstDatabase.EMShareHolder_TableName);
            Utils.ConfigGrid(cbbToEMShareHolderID, ConstDatabase.EMShareHolder_TableName);
            Utils.ConfigGrid(cbbToEMShareHolderID1, ConstDatabase.EMShareHolder_TableName);
        }
        private void InitializeGUI()
        {
            loadcbb();
            ultraOptionSet1.CheckedIndex = 0;
            LoadDuLieu(true);

        }
        #endregion

        private void ConfigGridDong(Template mauGiaoDien)
        {
            #region Grid động
            uGridPosted.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            uGridPosted.DisplayLayout.Override.TemplateAddRowPrompt = resSystem.MSG_System_07;
            uGridPosted.DisplayLayout.Override.SpecialRowSeparator = SpecialRowSeparator.None;


            //hiển thị 1 band
            uGridPosted.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            //tắt lọc cột
            uGridPosted.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.FilterUIType = FilterUIType.Default;
            //tự thay đổi kích thước cột
            uGridPosted.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //tắt tiêu đề
            uGridPosted.DisplayLayout.CaptionVisible = DefaultableBoolean.False;
            //select cả hàng hay ko?
            uGridPosted.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridPosted.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            //Hiển thị SupportDataErrorInfo
            uGridPosted.DisplayLayout.Override.SupportDataErrorInfo = SupportDataErrorInfo.CellsOnly;

            uGridPosted.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            uGridPosted.DisplayLayout.EmptyRowSettings.Style = EmptyRowStyle.ExtendFirstCell;

            uGridPosted.DisplayLayout.UseFixedHeaders = true;

            uGridPosted.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.All;

            Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];


            //foreach (var item in band.Columns)
            //{
            //    if (item.Key.Equals("StockCategoryView"))
            //    {
            //        List<StockCategory> cbb = _IStockCategoryService.GetAll();
            //        item.Editor = this.CreateCombobox(cbb, ConstDatabase.StockCategory_TableName, "StocKCategoryCode", "StockCategoryName");
            //    }
            //}
            foreach (var item in band.Columns)
            {

                if (item.Key.Equals("StockCategoryView"))
                {
                    //List<EMTransferDetail> listPpd = _IEMTransferDetailService.Query.Where(a => a.EMTransferID == IdEMTransferDetail).ToList();
                    List<EMTransferDetail> listPpd = _IEMTransferDetailService.GetListEMTransferDetailEMTransferID(IdEMTransferDetail);

                    List<StockCategory> listAll = _IStockCategoryService.GetAll();
                    List<StockCategory> listSt = (
                                                    from St in listAll
                                                    where (from abc in listPpd select abc.StockCategoryID).Contains(St.ID)
                                                    select St
                                                  ).ToList();
                    item.Editor = this.CreateCombobox(listSt, ConstDatabase.StockCategory_TableName, "StockCategoryCode", "StockCategoryName");

                }
                else


                    if (item.Key.Equals("QuantityTransfered"))
                    {
                        item.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                        item.MaskInput = " -nn,nnn,nnn,nnn,nnn.nn";
                        item.CellAppearance.TextHAlign = HAlign.Right;
                    }




            }
            #endregion
        }


        #region sự kiện khi thay đổi dữ liệu trên Grid con

        private void uGridPosted_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("StockCategoryID"))
            {
                Guid StId = new Guid(e.Cell.Row.Cells["StockCategoryID"].Value.ToString());
                //EMTransferDetail select = _IEMTransferDetailService.Query.Where(a => a.StockCategoryID == StId).Single();
                EMTransferDetail select = _IEMTransferDetailService.GetEMTransferDetailStockCategoryID(StId);
                e.Cell.Row.Cells["AmountTransfered"].Value = select.AmountTransfered;
                EMTransferDetail temp = e.Cell.Row.ListObject as EMTransferDetail;

            }
        }
        #endregion

        #region ConfigGrid
        void ConfigGrid(Infragistics.Win.UltraWinGrid.UltraGrid utralGrid)
        {//hàm chung
            Utils.ConfigGrid(utralGrid, TextMessage.ConstDatabase.EMTransferDetail_TableName, new List<TemplateColumn>(), 0);
            Infragistics.Win.UltraWinGrid.UltraGridBand band = utralGrid.DisplayLayout.Bands[0];

        }
        #endregion

        #region Button Event
        /// <summary>
        /// Sự kiện nút Lưu lại
        /// </summary>
        /// 
        private void btnSave_Click_1(object sender, EventArgs e)
        {
            #region Check Lỗi
            if (!CheckError()) return;
            #endregion


            //Gui to object
            #region Fill dữ liệu control vào obj
            EMTransfer temp = Them ? new EMTransfer() : _IEMTransferService.Getbykey(_Select.ID);
            ObjandGUI(temp, true);
            #endregion

            #region Thao tác CSDL
            try
            {
                _IEMTransferService.BeginTran();
                if (Them) _IEMTransferService.CreateNew(temp);

                //List<EMTransferDetail> EMTransferDetails = _IEMTransferDetailService.Query.Where(k => k.EMTransferID == temp.ID).ToList();
                List<EMTransferDetail> EMTransferDetails = _IEMTransferDetailService.GetListEMTransferDetailEMTransferID(temp.ID);
                foreach (EMTransferDetail item in EMTransferDetails) _IEMTransferDetailService.Delete(item);
                foreach (EMTransferDetail item in temp.EMTransferDetails) _IEMTransferDetailService.Save(item);

                _IEMTransferService.Update(temp);
                _IEMTransferService.CommitTran();
            }
            catch
            {
                _IEMTransferService.RolbackTran();
            }

            #endregion

            #region xử lý form, kết thúc form

            this.Close();
            isClose = false;

            #endregion
        }


        /// <summary>
        /// Sự kiện nút đóng
        /// </summary>

        private void btnClose_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }
        #endregion

        #region Utils
        /// <summary>
        /// get hoặc set một đối tượng vào giao diện xử lý

        EMTransfer ObjandGUI(EMTransfer input, bool isGet)
        {


            if (isGet)
            {
                if (input.ID == Guid.Empty) input.ID = Guid.NewGuid();  //Thêm mới

                input.TransferCode = txtTransferCode.Text;
                input.TransferDate = dtTransferDate.DateTime;
                input.Description = txtDescription.Text;
                input.FromEMShareHolderName = lblFromEMShareHolderName.Text;
                input.ToEMShareHolderName = lblToEMShareHolderName.Text;

                if (!string.IsNullOrEmpty(txtTransferFee.Text))
                {
                    input.TransferFee = decimal.Parse(txtTransferFee.Text);
                }
                //input.TransferFee = Decimal.Parse( txtTransferFee.Text);

                EMShareHolder temp0 = (EMShareHolder)Utils.getSelectCbbItem(cbbFromEMShareHolderID);
                if (temp0 == null) input.FromEMShareHolderID = null;
                else input.FromEMShareHolderID = temp0.ID;

                EMShareHolder temp1 = (EMShareHolder)Utils.getSelectCbbItem(cbbToEMShareHolderID);
                if (temp1 == null) input.ToEMShareHolderID = null;
                else input.ToEMShareHolderID = temp1.ID;

                if (!string.IsNullOrEmpty(cbbToEMShareHolderID1.Text))
                {
                    input.ToEMShareHolderID = ((EMShareHolder)Utils.getSelectCbbItem(cbbToEMShareHolderID1)).ID;
                }


                ///lưu dữ liệu từ uGrid.....
                EMTransferDetail uControlItem = uGridPosted.Rows[0].ListObject as EMTransferDetail;
                input.Quantity = uControlItem.QuantityTransfered;
                input.Amount = uControlItem.AmountTransfered;

                List<EMTransferDetail> uMaterialGoodsAssembly = ((BindingList<EMTransferDetail>)uGridPosted.DataSource).ToList();
                foreach (EMTransferDetail item in uMaterialGoodsAssembly) item.EMTransferID = input.ID;
                input.EMTransferDetails = uMaterialGoodsAssembly;

            }
            else
            {

                txtTransferCode.Text = input.TransferCode;
                txtDescription.Text = input.Description;
                dtTransferDate.DateTime = input.TransferDate;
                txtTransferFee.Text = input.TransferFee.ToString();
                lblFromEMShareHolderName.Text = input.FromEMShareHolderName;
                lblToEMShareHolderName.Text = input.ToEMShareHolderName;



                foreach (var item in cbbFromEMShareHolderID.Rows)
                {
                    if ((item.ListObject as EMShareHolder).ID == input.FromEMShareHolderID) cbbFromEMShareHolderID.SelectedRow = item;
                }

                foreach (var item in cbbToEMShareHolderID.Rows)
                {
                    if ((item.ListObject as EMShareHolder).ID == input.ToEMShareHolderID) cbbToEMShareHolderID.SelectedRow = item;
                }

                if (input.ToEMShareHolderID != null)
                {
                    foreach (var item in cbbToEMShareHolderID1.Rows)
                    {
                        if ((item.ListObject as EMShareHolder).ID == input.ToEMShareHolderID)
                        {
                            cbbToEMShareHolderID1.SelectedRow = item;
                        }
                    }

                }

                //BindingList<EMTransferDetail> bdMaterialGoods = new BindingList<EMTransferDetail>(_IEMTransferDetailService.Query.Where(p => p.EMTransferID == input.ID).ToList());
                BindingList<EMTransferDetail> bdMaterialGoods = new BindingList<EMTransferDetail>(_IEMTransferDetailService.GetListEMTransferDetailEMTransferID(input.ID));
                uGridPosted.DataSource = bdMaterialGoods;
                ConfigGrid(uGridPosted);

            }
            return input;
        }

        bool CheckError()
        {
            bool kq = true;
            //Check Error Chung

            //Check Error Riêng từng Form
            if (string.IsNullOrEmpty(txtTransferCode.Text) || string.IsNullOrEmpty(dtTransferDate.Text) || cbbFromEMShareHolderID.SelectedRow == null)
            {
                MSG.Error(resSystem.MSG_System_03);
                return false;
            }
            //List<string> hh = _IEMTransferService.Query.Select(a => a.TransferCode).ToList();
            List<string> hh = _IEMTransferService.GetListEMTransferCode();
            foreach (var item in hh)
            {
                if (item.Equals(txtTransferCode.Text))
                {
                    MessageBox.Show("Mã chuyển nhượng" + item + " đã tồn tại trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return kq;
        }
        #endregion


        private void ultraOptionSet1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraOptionSet1.CheckedIndex == 0)
            {
                cbbToEMShareHolderID.Show();
                cbbToEMShareHolderID.Location = new Point(113, 55);
                cbbToEMShareHolderID1.Hide();
            }
            if (ultraOptionSet1.CheckedIndex == 1)
            {
                cbbToEMShareHolderID1.Show();
                cbbToEMShareHolderID.Hide();
                cbbToEMShareHolderID1.Location = new Point(113, 54);
            }
        }

        private void cbbToEMShareHolderID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FInvestorDetail().ShowDialog(this);
            loadcbb();
        }

        private void Ugrid_AfterBeforCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            if (e.Cell.Column.Key.Equals("TotalShare"))
            {//trước khi cell AmountOriginal cập nhật thì kiểm tra nếu giá trị mới rỗng thì mặc định quay trở về giá trị cũ
                double temp = 0;
                string value = e.NewValue.ToString();
                if (string.IsNullOrEmpty(value))    // || double.TryParse(value, out temp)
                {
                    e.Cancel = true;
                }
            }
        }

        public Template mauGiaoDien { get; set; }

        #region //Fin dữ liệu cho các Lable_____________________________________


        private void cbbFromEMShareHolderID_ValueChanged(object sender, EventArgs e)
        {
            object listObject = cbbFromEMShareHolderID.SelectedRow.ListObject;
            if (listObject == null) return;
            EMShareHolder cd = listObject as EMShareHolder;
            if (cd == null) return;


            lblFromEMShareHolderName.Text = cd.ShareHolderName;
            lblFromTotalShare.Text = cd.TotalShare.ToString();
            lblFromTotalShare1.Text = cd.TotalShare.ToString();
        }

        private void cbbToEMShareHolderID_ValueChanged(object sender, EventArgs e)
        {
            object listObject1 = cbbToEMShareHolderID.SelectedRow.ListObject;
            if (listObject1 == null) return;
            EMShareHolder cd1 = listObject1 as EMShareHolder;
            if (cd1 == null) return;

            lblToEMShareHolderName.Text = cd1.ShareHolderName;
            lblToTotalShare.Text = cd1.TotalShare.ToString();
            lblToTotalShare1.Text = cd1.TotalShare.ToString();


        }

        private void cbbToEMShareHolderID1_ValueChanged(object sender, EventArgs e)
        {
            object listObject2 = cbbToEMShareHolderID1.SelectedRow.ListObject;
            if (listObject2 == null) return;
            EMShareHolder cd2 = listObject2 as EMShareHolder;
            if (cd2 == null) return;


            lblToEMShareHolderName.Text = cd2.ShareHolderName;
            lblToTotalShare.Text = cd2.TotalShare.ToString();
            lblToTotalShare1.Text = cd2.TotalShare.ToString();
        }
        #endregion

        private void uGridPosted_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridBand band = uGridPosted.DisplayLayout.Bands[0];
                UltraGrid activeGrid = ((Infragistics.Win.UltraWinGrid.UltraGrid)sender);
                int index = activeGrid.ActiveCell.Row.Index;
                foreach (var item in band.Columns)
                {
                    if (item.Key.Equals("QuantityTransfered"))
                    {
                        object litObject3 = cbbFromEMShareHolderID.SelectedRow.ListObject;
                        if (litObject3 == null) return;
                        EMShareHolder cd3 = litObject3 as EMShareHolder;
                        if (cd3 == null) return;
                        //List<EMShareHolder> cd3 = _IEMShareHolderService.GetAll();

                        List<EMTransferDetail> lit = new List<EMTransferDetail>();
                        Decimal quantityTransfered = Convert.ToDecimal(uGridPosted.Rows[index].Cells["QuantityTransfered"].Value);
                        lblFromTotalShare1.Text = (cd3.TotalShare - quantityTransfered).ToString();
                        lblToTotalShare1.Text = (cd3.TotalShare + quantityTransfered).ToString();
                        uGridPosted.Rows[index].Cells["AmountTransfered"].Value = (quantityTransfered * 10000).ToString();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Bạn phải nhập thông tin 'Cổ đông chuyển' và 'Đối tượng nhận' trước!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
