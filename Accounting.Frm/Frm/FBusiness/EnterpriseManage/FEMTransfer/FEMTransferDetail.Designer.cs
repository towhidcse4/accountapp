﻿namespace Accounting
{
    partial class FEMTransferDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtTransferFee = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.txtTransferCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dtTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblNumberAttach = new Infragistics.Win.Misc.UltraLabel();
            this.txtDescription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.lblReceiver = new Infragistics.Win.Misc.UltraLabel();
            this.lblAccountingObjectID = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbFromEMShareHolderID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblFromEMShareHolderName = new Infragistics.Win.Misc.UltraLabel();
            this.lblFromTotalShare1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.lblFromTotalShare = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbToEMShareHolderID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cbbToEMShareHolderID1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraOptionSet1 = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.lblToEMShareHolderName = new Infragistics.Win.Misc.UltraLabel();
            this.lblToTotalShare1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.lblToTotalShare = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridPosted = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFromEMShareHolderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToEMShareHolderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToEMShareHolderID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            appearance1.TextHAlignAsString = "Left";
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.Controls.Add(this.txtTransferFee);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.txtTransferCode);
            this.ultraGroupBox1.Controls.Add(this.dtTransferDate);
            this.ultraGroupBox1.Controls.Add(this.lblNumberAttach);
            this.ultraGroupBox1.Controls.Add(this.txtDescription);
            this.ultraGroupBox1.Controls.Add(this.lblReceiver);
            this.ultraGroupBox1.Controls.Add(this.lblAccountingObjectID);
            appearance9.FontData.BoldAsString = "True";
            appearance9.FontData.SizeInPoints = 10F;
            this.ultraGroupBox1.HeaderAppearance = appearance9;
            this.ultraGroupBox1.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox1.Location = new System.Drawing.Point(1, 2);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(992, 157);
            this.ultraGroupBox1.TabIndex = 28;
            this.ultraGroupBox1.Text = "Thông tin chung";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtTransferFee
            // 
            appearance2.TextHAlignAsString = "Right";
            this.txtTransferFee.Appearance = appearance2;
            this.txtTransferFee.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtTransferFee.InputMask = "nnn,nnn,nnn,nnn";
            this.txtTransferFee.Location = new System.Drawing.Point(134, 129);
            this.txtTransferFee.Name = "txtTransferFee";
            this.txtTransferFee.PromptChar = ' ';
            this.txtTransferFee.Size = new System.Drawing.Size(140, 20);
            this.txtTransferFee.TabIndex = 4;
            // 
            // ultraLabel1
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance3;
            this.ultraLabel1.Location = new System.Drawing.Point(313, 30);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(48, 19);
            this.ultraLabel1.TabIndex = 36;
            this.ultraLabel1.Text = "Số (*) :";
            // 
            // txtTransferCode
            // 
            appearance4.TextHAlignAsString = "Left";
            this.txtTransferCode.Appearance = appearance4;
            this.txtTransferCode.Location = new System.Drawing.Point(367, 27);
            this.txtTransferCode.Multiline = true;
            this.txtTransferCode.Name = "txtTransferCode";
            this.txtTransferCode.Size = new System.Drawing.Size(140, 22);
            this.txtTransferCode.TabIndex = 2;
            // 
            // dtTransferDate
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.dtTransferDate.Appearance = appearance5;
            this.dtTransferDate.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtTransferDate.FormatProvider = new System.Globalization.CultureInfo("vi-VN");
            this.dtTransferDate.Location = new System.Drawing.Point(134, 28);
            this.dtTransferDate.MaskInput = "";
            this.dtTransferDate.Name = "dtTransferDate";
            this.dtTransferDate.Size = new System.Drawing.Size(140, 21);
            this.dtTransferDate.TabIndex = 1;
            this.dtTransferDate.Value = null;
            // 
            // lblNumberAttach
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberAttach.Appearance = appearance6;
            this.lblNumberAttach.Location = new System.Drawing.Point(6, 132);
            this.lblNumberAttach.Name = "lblNumberAttach";
            this.lblNumberAttach.Size = new System.Drawing.Size(99, 19);
            this.lblNumberAttach.TabIndex = 28;
            this.lblNumberAttach.Text = "Phí giao dịch :";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(134, 60);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(852, 55);
            this.txtDescription.TabIndex = 3;
            // 
            // lblReceiver
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.lblReceiver.Appearance = appearance7;
            this.lblReceiver.Location = new System.Drawing.Point(9, 60);
            this.lblReceiver.Name = "lblReceiver";
            this.lblReceiver.Size = new System.Drawing.Size(98, 19);
            this.lblReceiver.TabIndex = 24;
            this.lblReceiver.Text = "Trích yếu :";
            // 
            // lblAccountingObjectID
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountingObjectID.Appearance = appearance8;
            this.lblAccountingObjectID.Location = new System.Drawing.Point(9, 30);
            this.lblAccountingObjectID.Name = "lblAccountingObjectID";
            this.lblAccountingObjectID.Size = new System.Drawing.Size(98, 19);
            this.lblAccountingObjectID.TabIndex = 0;
            this.lblAccountingObjectID.Text = "Ngày (*) :";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.cbbFromEMShareHolderID);
            this.ultraGroupBox2.Controls.Add(this.lblFromEMShareHolderName);
            this.ultraGroupBox2.Controls.Add(this.lblFromTotalShare1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.lblFromTotalShare);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            appearance30.FontData.BoldAsString = "True";
            appearance30.FontData.SizeInPoints = 10F;
            this.ultraGroupBox2.HeaderAppearance = appearance30;
            this.ultraGroupBox2.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox2.Location = new System.Drawing.Point(1, 165);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(493, 155);
            this.ultraGroupBox2.TabIndex = 29;
            this.ultraGroupBox2.Text = "Cổ đông chuyển";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbFromEMShareHolderID
            // 
            appearance10.TextHAlignAsString = "Left";
            this.cbbFromEMShareHolderID.Appearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbFromEMShareHolderID.DisplayLayout.Appearance = appearance11;
            this.cbbFromEMShareHolderID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbFromEMShareHolderID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFromEMShareHolderID.DisplayLayout.GroupByBox.Appearance = appearance12;
            appearance13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFromEMShareHolderID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance13;
            this.cbbFromEMShareHolderID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance14.BackColor2 = System.Drawing.SystemColors.Control;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbFromEMShareHolderID.DisplayLayout.GroupByBox.PromptAppearance = appearance14;
            this.cbbFromEMShareHolderID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbFromEMShareHolderID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.ActiveCellAppearance = appearance15;
            appearance16.BackColor = System.Drawing.SystemColors.Highlight;
            appearance16.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.ActiveRowAppearance = appearance16;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.CardAreaAppearance = appearance17;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            appearance18.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.CellAppearance = appearance18;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.CellPadding = 0;
            appearance19.BackColor = System.Drawing.SystemColors.Control;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.GroupByRowAppearance = appearance19;
            appearance20.TextHAlignAsString = "Left";
            this.cbbFromEMShareHolderID.DisplayLayout.Override.HeaderAppearance = appearance20;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.RowAppearance = appearance21;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbFromEMShareHolderID.DisplayLayout.Override.TemplateAddRowAppearance = appearance22;
            this.cbbFromEMShareHolderID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbFromEMShareHolderID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbFromEMShareHolderID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbFromEMShareHolderID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbFromEMShareHolderID.Location = new System.Drawing.Point(134, 55);
            this.cbbFromEMShareHolderID.Name = "cbbFromEMShareHolderID";
            this.cbbFromEMShareHolderID.NullText = "<Chọn đối tượng cha>";
            this.cbbFromEMShareHolderID.Size = new System.Drawing.Size(140, 22);
            this.cbbFromEMShareHolderID.TabIndex = 5;
            this.cbbFromEMShareHolderID.ValueChanged += new System.EventHandler(this.cbbFromEMShareHolderID_ValueChanged);
            // 
            // lblFromEMShareHolderName
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.lblFromEMShareHolderName.Appearance = appearance23;
            this.lblFromEMShareHolderName.Location = new System.Drawing.Point(134, 91);
            this.lblFromEMShareHolderName.Name = "lblFromEMShareHolderName";
            this.lblFromEMShareHolderName.Size = new System.Drawing.Size(134, 23);
            this.lblFromEMShareHolderName.TabIndex = 44;
            // 
            // lblFromTotalShare1
            // 
            appearance24.BackColor = System.Drawing.Color.Transparent;
            this.lblFromTotalShare1.Appearance = appearance24;
            this.lblFromTotalShare1.Location = new System.Drawing.Point(367, 126);
            this.lblFromTotalShare1.Name = "lblFromTotalShare1";
            this.lblFromTotalShare1.Size = new System.Drawing.Size(117, 23);
            this.lblFromTotalShare1.TabIndex = 32;
            // 
            // ultraLabel6
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel6.Appearance = appearance25;
            this.ultraLabel6.Location = new System.Drawing.Point(270, 130);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(91, 19);
            this.ultraLabel6.TabIndex = 31;
            this.ultraLabel6.Text = "SL sau chuyển :";
            // 
            // lblFromTotalShare
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            this.lblFromTotalShare.Appearance = appearance26;
            this.lblFromTotalShare.Location = new System.Drawing.Point(134, 126);
            this.lblFromTotalShare.Name = "lblFromTotalShare";
            this.lblFromTotalShare.Size = new System.Drawing.Size(113, 23);
            this.lblFromTotalShare.TabIndex = 30;
            // 
            // ultraLabel3
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel3.Appearance = appearance27;
            this.ultraLabel3.Location = new System.Drawing.Point(0, 130);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(99, 19);
            this.ultraLabel3.TabIndex = 28;
            this.ultraLabel3.Text = "SL trước chuyển :";
            // 
            // ultraLabel4
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance28;
            this.ultraLabel4.Location = new System.Drawing.Point(1, 95);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(98, 19);
            this.ultraLabel4.TabIndex = 24;
            this.ultraLabel4.Text = "Tên :";
            // 
            // ultraLabel5
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance29;
            this.ultraLabel5.Location = new System.Drawing.Point(1, 58);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(98, 19);
            this.ultraLabel5.TabIndex = 0;
            this.ultraLabel5.Text = "Mã (*) :";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.cbbToEMShareHolderID);
            this.ultraGroupBox3.Controls.Add(this.cbbToEMShareHolderID1);
            this.ultraGroupBox3.Controls.Add(this.ultraOptionSet1);
            this.ultraGroupBox3.Controls.Add(this.lblToEMShareHolderName);
            this.ultraGroupBox3.Controls.Add(this.lblToTotalShare1);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox3.Controls.Add(this.lblToTotalShare);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel15);
            appearance53.FontData.BoldAsString = "True";
            appearance53.FontData.SizeInPoints = 10F;
            this.ultraGroupBox3.HeaderAppearance = appearance53;
            this.ultraGroupBox3.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopOnBorder;
            this.ultraGroupBox3.Location = new System.Drawing.Point(500, 165);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(493, 155);
            this.ultraGroupBox3.TabIndex = 30;
            this.ultraGroupBox3.Text = "Cổ đông nhận";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbToEMShareHolderID
            // 
            appearance31.TextHAlignAsString = "Left";
            this.cbbToEMShareHolderID.Appearance = appearance31;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbToEMShareHolderID.DisplayLayout.Appearance = appearance32;
            this.cbbToEMShareHolderID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbToEMShareHolderID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbToEMShareHolderID.DisplayLayout.GroupByBox.Appearance = appearance33;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbToEMShareHolderID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance34;
            this.cbbToEMShareHolderID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance35.BackColor2 = System.Drawing.SystemColors.Control;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbToEMShareHolderID.DisplayLayout.GroupByBox.PromptAppearance = appearance35;
            this.cbbToEMShareHolderID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbToEMShareHolderID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbToEMShareHolderID.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance37.BackColor = System.Drawing.SystemColors.Highlight;
            appearance37.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbToEMShareHolderID.DisplayLayout.Override.ActiveRowAppearance = appearance37;
            this.cbbToEMShareHolderID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbToEMShareHolderID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            this.cbbToEMShareHolderID.DisplayLayout.Override.CardAreaAppearance = appearance38;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            appearance39.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbToEMShareHolderID.DisplayLayout.Override.CellAppearance = appearance39;
            this.cbbToEMShareHolderID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbToEMShareHolderID.DisplayLayout.Override.CellPadding = 0;
            appearance40.BackColor = System.Drawing.SystemColors.Control;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbToEMShareHolderID.DisplayLayout.Override.GroupByRowAppearance = appearance40;
            appearance41.TextHAlignAsString = "Left";
            this.cbbToEMShareHolderID.DisplayLayout.Override.HeaderAppearance = appearance41;
            this.cbbToEMShareHolderID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbToEMShareHolderID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            this.cbbToEMShareHolderID.DisplayLayout.Override.RowAppearance = appearance42;
            this.cbbToEMShareHolderID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbToEMShareHolderID.DisplayLayout.Override.TemplateAddRowAppearance = appearance43;
            this.cbbToEMShareHolderID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbToEMShareHolderID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbToEMShareHolderID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbToEMShareHolderID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbToEMShareHolderID.Location = new System.Drawing.Point(113, 55);
            this.cbbToEMShareHolderID.Name = "cbbToEMShareHolderID";
            this.cbbToEMShareHolderID.NullText = "<Chọn đối tượng cha>";
            this.cbbToEMShareHolderID.Size = new System.Drawing.Size(140, 22);
            this.cbbToEMShareHolderID.TabIndex = 7;
            this.cbbToEMShareHolderID.ValueChanged += new System.EventHandler(this.cbbToEMShareHolderID_ValueChanged);
            // 
            // cbbToEMShareHolderID1
            // 
            appearance44.TextHAlignAsString = "Left";
            this.cbbToEMShareHolderID1.Appearance = appearance44;
            editorButton1.Text = "Thêm";
            this.cbbToEMShareHolderID1.ButtonsRight.Add(editorButton1);
            this.cbbToEMShareHolderID1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbToEMShareHolderID1.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cbbToEMShareHolderID1.Location = new System.Drawing.Point(304, 54);
            this.cbbToEMShareHolderID1.Name = "cbbToEMShareHolderID1";
            this.cbbToEMShareHolderID1.NullText = "<Chọn dữ liệu>";
            this.cbbToEMShareHolderID1.Size = new System.Drawing.Size(183, 22);
            this.cbbToEMShareHolderID1.TabIndex = 8;
            this.cbbToEMShareHolderID1.ValueChanged += new System.EventHandler(this.cbbToEMShareHolderID1_ValueChanged);
            this.cbbToEMShareHolderID1.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbToEMShareHolderID1_EditorButtonClick);
            // 
            // ultraOptionSet1
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.Appearance = appearance45;
            this.ultraOptionSet1.BackColor = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BackColorInternal = System.Drawing.Color.Transparent;
            this.ultraOptionSet1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "1.Cổ đông";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "2. Nhà đầu tư mới";
            this.ultraOptionSet1.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.ultraOptionSet1.Location = new System.Drawing.Point(113, 22);
            this.ultraOptionSet1.Name = "ultraOptionSet1";
            this.ultraOptionSet1.Size = new System.Drawing.Size(161, 18);
            this.ultraOptionSet1.TabIndex = 6;
            this.ultraOptionSet1.ValueChanged += new System.EventHandler(this.ultraOptionSet1_ValueChanged);
            // 
            // lblToEMShareHolderName
            // 
            appearance46.BackColor = System.Drawing.Color.Transparent;
            this.lblToEMShareHolderName.Appearance = appearance46;
            this.lblToEMShareHolderName.Location = new System.Drawing.Point(113, 91);
            this.lblToEMShareHolderName.Name = "lblToEMShareHolderName";
            this.lblToEMShareHolderName.Size = new System.Drawing.Size(140, 23);
            this.lblToEMShareHolderName.TabIndex = 33;
            // 
            // lblToTotalShare1
            // 
            appearance47.BackColor = System.Drawing.Color.Transparent;
            this.lblToTotalShare1.Appearance = appearance47;
            this.lblToTotalShare1.Location = new System.Drawing.Point(354, 126);
            this.lblToTotalShare1.Name = "lblToTotalShare1";
            this.lblToTotalShare1.Size = new System.Drawing.Size(130, 23);
            this.lblToTotalShare1.TabIndex = 32;
            // 
            // ultraLabel11
            // 
            appearance48.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance48;
            this.ultraLabel11.Location = new System.Drawing.Point(259, 130);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(77, 19);
            this.ultraLabel11.TabIndex = 31;
            this.ultraLabel11.Text = "SL sau nhận :";
            // 
            // lblToTotalShare
            // 
            appearance49.BackColor = System.Drawing.Color.Transparent;
            this.lblToTotalShare.Appearance = appearance49;
            this.lblToTotalShare.Location = new System.Drawing.Point(113, 126);
            this.lblToTotalShare.Name = "lblToTotalShare";
            this.lblToTotalShare.Size = new System.Drawing.Size(140, 23);
            this.lblToTotalShare.TabIndex = 30;
            // 
            // ultraLabel13
            // 
            appearance50.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance50;
            this.ultraLabel13.Location = new System.Drawing.Point(0, 130);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(99, 19);
            this.ultraLabel13.TabIndex = 28;
            this.ultraLabel13.Text = "SL trước nhận :";
            // 
            // ultraLabel14
            // 
            appearance51.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance51;
            this.ultraLabel14.Location = new System.Drawing.Point(1, 95);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(98, 19);
            this.ultraLabel14.TabIndex = 24;
            this.ultraLabel14.Text = "Tên :";
            // 
            // ultraLabel15
            // 
            appearance52.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel15.Appearance = appearance52;
            this.ultraLabel15.Location = new System.Drawing.Point(1, 58);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(98, 19);
            this.ultraLabel15.TabIndex = 0;
            this.ultraLabel15.Text = "Mã (*) :";
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uGridPosted);
            this.ultraGroupBox4.Location = new System.Drawing.Point(2, 326);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(991, 176);
            this.ultraGroupBox4.TabIndex = 900;
            this.ultraGroupBox4.Text = "Số cổ phần chuyển nhượng";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGridPosted
            // 
            this.uGridPosted.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.uGridPosted.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridPosted.DisplayLayout.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uGridPosted.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridPosted.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.uGridPosted.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.uGridPosted.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
            this.uGridPosted.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.uGridPosted.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridPosted.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.uGridPosted.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.uGridPosted.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.uGridPosted.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.uGridPosted.Location = new System.Drawing.Point(0, 19);
            this.uGridPosted.Name = "uGridPosted";
            this.uGridPosted.Size = new System.Drawing.Size(985, 153);
            this.uGridPosted.TabIndex = 9;
            this.uGridPosted.Text = "uGrid";
            this.uGridPosted.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPosted_AfterCellUpdate);
            this.uGridPosted.AfterExitEditMode += new System.EventHandler(this.uGridPosted_AfterExitEditMode);
            // 
            // btnSave
            // 
            appearance54.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance54;
            this.btnSave.Location = new System.Drawing.Point(822, 508);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnClose
            // 
            appearance55.Image = global::Accounting.Properties.Resources.ubtnDelete;
            this.btnClose.Appearance = appearance55;
            this.btnClose.Location = new System.Drawing.Point(912, 508);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FEMTransferDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 542);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraGroupBox4);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FEMTransferDetail";
            this.Text = "Phiếu chuyển nhượng.";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbFromEMShareHolderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToEMShareHolderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbToEMShareHolderID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraOptionSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPosted)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTransferCode;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtTransferDate;
        private Infragistics.Win.Misc.UltraLabel lblNumberAttach;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDescription;
        private Infragistics.Win.Misc.UltraLabel lblReceiver;
        private Infragistics.Win.Misc.UltraLabel lblAccountingObjectID;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel lblFromEMShareHolderName;
        private Infragistics.Win.Misc.UltraLabel lblFromTotalShare1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel lblFromTotalShare;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraLabel lblToEMShareHolderName;
        private Infragistics.Win.Misc.UltraLabel lblToTotalShare1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel lblToTotalShare;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet ultraOptionSet1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPosted;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbToEMShareHolderID1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbFromEMShareHolderID;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbToEMShareHolderID;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtTransferFee;
    }
}