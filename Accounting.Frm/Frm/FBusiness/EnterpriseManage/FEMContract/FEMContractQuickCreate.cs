﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;

namespace Accounting
{
    public partial class FEMContractQuickCreate : CustormForm
    {
        private IEMContractService _IEMContractService = Utils.IEMContractService;
        private readonly IGenCodeService _IGenCodeService;
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public FEMContractQuickCreate(DateTime date, int? typeGroup = 85)
        {
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IEMContractService = IoC.Resolve<IEMContractService>();
            InitializeComponent();
            optContractType.CheckedIndex = Utils.SaleTypeGroup.Contains(typeGroup.Value) ? 1 : 0;
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmountOriginal.FormatNumberic(ConstDatabase.Format_TienVND);
            txtExchangeRate.FormatNumberic(ConstDatabase.Format_Rate);
            dteSignedDate.Value = date;
            dteEffectiveDate.Value = date;
            cbbCurrency.DataSource = Utils.ListCurrency;
            cbbCurrency.DisplayMember = "ID";
            cbbCurrency.ValueMember = "ID";
            Utils.ConfigGrid(cbbCurrency, ConstDatabase.Currency_TableName);
            cbbCurrency.Value = "VND";
            lblExchangeRate.Visible = false;
            lblAmountOriginal.Visible = false;
            txtExchangeRate.Visible = false;
            txtAmountOriginal.Visible = false;
            txtExchangeRate.FormatNumberic(ConstDatabase.Format_Rate);
            txtAmountOriginal.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            //txtCode.Text = Utils.TaoMaChungTu(Utils.IGenCodeService.getGenCode(optContractType.CheckedIndex == 0 ? 85 : 86));
            //txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc:", txtCode.Text, dteSignedDate.Text);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Ignore;
            Close();
        }

        EMContract ObjandGUI(EMContract input)
        {
            if (input.ID == Guid.Empty)
                input.ID = Guid.NewGuid();
            input.Code = txtCode.Text;
            input.CurrencyID = (string)cbbCurrency.Value;
            if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
            {
                input.ExchangeRate = decimal.Parse(txtExchangeRate.Value.ToString());
            }
            if (txtAmount.Value != null)
            {
                input.Amount = decimal.Parse(txtAmount.Value.ToString());
                input.AmountOriginal = decimal.Parse(txtAmount.Value.ToString());
            }
            if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
            {
                input.AmountOriginal = decimal.Parse(txtAmountOriginal.Value.ToString());
            }
            input.InvoiceAmount = 0;
            input.ReceiptAmount = 0;
            input.PaymentAmount = 0;
            input.PayableAmount = 0;
            input.IsSecurity = false;
            input.IsClosed = false;
            input.IsWatchForCostPrice = false;
            input.ReceivingAmount = 0;
            input.ContractTotalAmount = input.Amount;
            input.ContractTotalAmountOriginal = input.AmountOriginal;
            input.ContractTotalAmountPayment = 0;
            input.ContractTotalAmountPaymentOriginal = 0;
            input.IsProject = false;
            input.IsBillPaid = false;
            input.PlantPaymentAmount = 0;
            input.ContractState = 2;
            input.AmountBeforCancel = 0;
            input.OpeningPaymentAmount = 0;
            input.OpeningRecreiptAmount = 0;
            input.SignedDate = dteSignedDate.DateTime;
            input.EffectiveDate = dteEffectiveDate.DateTime;
            input.Name = txtName.Text;
            input.TypeID = optContractType.CheckedIndex == 0 ? 850 : 860;
            input.IsActive = true;
            return input;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Checkerror()) return;
            EMContract temp = new EMContract();
            temp = ObjandGUI(temp);
            //var emContract = new EMContract
            //                     {
            //                         ID = Guid.NewGuid(),
            //                         Code = txtCode.Text,
            //                         CurrencyID = (string)cbbCurrency.Value,
            //                         ExchangeRate = (string.IsNullOrEmpty(txtExchangeRate.Value.ToString()) ? 1 : Convert.ToDecimal(txtExchangeRate.Value)),
            //                         Amount = (string.IsNullOrEmpty(txtAmount.Value.ToString()) ? 0 : Convert.ToDecimal(txtAmount.Value)),
            //                         AmountOriginal = (string.IsNullOrEmpty(txtAmountOriginal.Value.ToString()) ? 0 : Convert.ToDecimal(txtAmountOriginal.Value)),
            //                         InvoiceAmount = 0,
            //                         ReceiptAmount = 0,
            //                         PaymentAmount = 0,
            //                         PayableAmount = 0,
            //                         IsSecurity = false,
            //                         IsClosed = false,
            //                         IsWatchForCostPrice = false,
            //                         ReceivingAmount = 0,
            //                         ContractTotalAmount = (string.IsNullOrEmpty(txtAmount.Value.ToString()) ? 0 : Convert.ToDecimal(txtAmount.Value)),
            //                         ContractTotalAmountOriginal = (string.IsNullOrEmpty(txtAmountOriginal.Value.ToString()) ? 0 : Convert.ToDecimal(txtAmountOriginal.Value)),
            //                         ContractTotalAmountPayment = 0,
            //                         ContractTotalAmountPaymentOriginal = 0,
            //                         IsProject = false,
            //                         IsBillPaid = false,
            //                         PlantPaymentAmount = 0,
            //                         ContractState = 2,
            //                         AmountBeforCancel = 0,
            //                         OpeningPaymentAmount = 0,
            //                         OpeningRecreiptAmount = 0,
            //                         SignedDate = dteSignedDate.DateTime,
            //                         EffectiveDate = dteEffectiveDate.DateTime,
            //                         Name = txtName.Text,
            //                         TypeID = optContractType.CheckedIndex == 0 ? 850 : 860,
            //                         IsActive = true
            //                     };
            try
            {
                _IEMContractService.BeginTran();
                _IEMContractService.CreateNew(temp);
                Utils.IGenCodeService.UpdateGenCodeForm(optContractType.CheckedIndex == 0 ? 85 : 86, temp.Code, temp.ID);
                _IEMContractService.CommitTran();
                _IEMContractService.UnbindSession(temp);
                Utils.ClearCacheByType<EMContract>();
                Close();
                Id = temp.ID;               
            }
            catch (Exception)
            {
                _IEMContractService.RolbackTran();
                MSG.Warning(string.Format("Lỗi tạo hợp đồng {0}. \r\n Vui lòng thử lại !",
                                          optContractType.CheckedIndex == 0 ? "mua" : "bán"));
            }
        }

        private void cbbCurrency_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            //if (!cbbCurrency.Value.ToString().Equals("VND"))
            //{
            //    lblAmount.Location = new Point(251, 103);
            //    txtAmount.Location = new Point(344, 103);
            //    lblAmountOriginal.Location = new Point(12, 103);
            //    txtAmountOriginal.Location = new Point(106, 103);
            //    lblExchangeRate.Visible = true;
            //    txtExchangeRate.Visible = true;
            //    lblAmountOriginal.Visible = true;
            //    txtAmountOriginal.Visible = true;
            //}
            //else
            //{
            //    lblAmount.Location = new Point(12, 103);
            //    txtAmount.Location = new Point(106, 103);
            //    lblExchangeRate.Visible = false;
            //    txtExchangeRate.Visible = false;
            //    lblAmountOriginal.Visible = false;
            //    txtAmountOriginal.Visible = false;
            //}
            //var cur = cbbCurrency.SelectedRow.ListObject as Currency;
            //txtExchangeRate.Value = cur.ExchangeRate;
            if (e.Row == null)
            {
                cbbCurrency.Value = "VND";
            }
            if (e.Row != null)
            {
                Currency temp = e.Row.ListObject as Currency;
                if (temp.ID != "VND")
                {
                    lblAmount.Location = new Point(251, 103);
                    txtAmount.Location = new Point(344, 103);
                    lblAmountOriginal.Location = new Point(12, 103);
                    txtAmountOriginal.Location = new Point(106, 103);
                    lblExchangeRate.Visible = true;
                    txtExchangeRate.Visible = true;
                    lblAmountOriginal.Visible = true;
                    txtAmountOriginal.Visible = true;
                    txtExchangeRate.Value = temp.ExchangeRate;
                    txtAmount.Enabled = true;
                    txtExchangeRate.Enabled = true;
                    if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
                    {
                        decimal rateEchangeTemp;
                        decimal orgTemp;
                        if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                        {
                            rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                            orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                            txtAmount.Value = rateEchangeTemp * orgTemp;
                        }
                    }

                }
                if (temp.ID == "VND")
                {
                    lblAmount.Location = new Point(12, 103);
                    txtAmount.Location = new Point(106, 103);
                    lblExchangeRate.Visible = false;
                    txtExchangeRate.Visible = false;
                    lblAmountOriginal.Visible = false;
                    txtAmountOriginal.Visible = false;
                    txtExchangeRate.Value = 1;
                    txtAmount.Enabled = true;
                    txtExchangeRate.Enabled = false;
                    if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
                    {
                        decimal rateEchangeTemp;
                        decimal orgTemp;
                        if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                        {
                            rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                            orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                            txtAmount.Value = rateEchangeTemp * orgTemp;
                            //txtAmountBeforCancel.Value = txtAmount.Value;
                        }
                    }
                }
            }
        }

        private void dteSignedDate_ValueChanged(object sender, EventArgs e)
        {
            txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc:", txtCode.Text, dteSignedDate.Text);
        }

        private void txtCode_ValueChanged(object sender, EventArgs e)
        {
            txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc:", txtCode.Text, dteSignedDate.Text);
        }

        private void txtAmountOriginal_TextChanged(object sender, EventArgs e)
        {
            if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
            {
                decimal rateEchangeTemp;
                decimal orgTemp;
                if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                {
                    rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                    orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                    txtAmount.Value = rateEchangeTemp * orgTemp;
                }
            }
        }

        private void txtExchangeRate_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void optContractType_ValueChanged(object sender, EventArgs e)
        {
            if (optContractType.Text == "Hợp đồng mua")
            {
                txtCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(85));
                //_IGenCodeService.UpdateGenCodeCatalog(85, txtCode.Text);
                txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc:", txtCode.Text, dteSignedDate.Text);
            }
            else
            {
                txtCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(86));
                //_IGenCodeService.UpdateGenCodeCatalog(86, txtCode.Text);
                txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc:", txtCode.Text, dteSignedDate.Text);
            }
        }

        private bool Checkerror()
        {
            bool kq = true;
            List<EMContract> lstemcontract = _IEMContractService.GetAll();
            foreach (var item in lstemcontract)
            {
                if (item.Code == txtCode.Text)
                {
                    if (MSG.Question("Mã hợp đồng này đã tồn tại, bạn có muốn hệ thống tự sinh mã mới không") == DialogResult.Yes)
                    {
                        if (optContractType.Text == "Hợp đồng bán")
                        {
                            txtCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(86));
                            _IGenCodeService.UpdateGenCodeCatalog(86, txtCode.Text);
                        }
                        else
                        {
                            txtCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(85));
                            _IGenCodeService.UpdateGenCodeCatalog(85, txtCode.Text);
                        }
                    }
                    else
                    {
                        txtCode.Focus();
                    }
                    return false;
                }
            }
            if ((txtAmount.Value == null) || (txtAmount.Text.Trim() == ""))
            {
                MSG.Warning("Không được để trống giá trị hợp đồng");
                txtAmount.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtAmount.Value) == 0)
            {
                MSG.Warning("Giá trị hợp đồng phải lớn hơn 0");
                txtAmount.Focus();
                return false;
            }
            DateTime dt1 = DateTime.Parse(dteSignedDate.Value.ToString());
            DateTime dt2 = DateTime.Parse(dteEffectiveDate.Value.ToString());
            if (DateTime.Compare(dt1, dt2) > 0)
            {
                MSG.Warning("Ngày ký hợp đồng không được lớn hơn ngày hạch toán");
                dteSignedDate.Focus();
                return false;
            }
            if (txtCode.Text == "")
            {
                MSG.Warning("Không được để trống số Hợp đồng");
                txtCode.Focus();
                return false;
            }
            return kq;
        }

        private void FEMContractQuickCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            Utils.ListEmContract.Clear();
            Utils.ListEmContract.AddToBindingList(new BindingList<EMContract>(_IEMContractService.GetAll_OrderBy()));

        }
    }
}

