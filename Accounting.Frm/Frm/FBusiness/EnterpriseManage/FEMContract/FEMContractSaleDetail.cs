﻿using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.Core.ServiceImp;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Accounting
{
    public partial class FEMContractSaleDetail : CustormForm
    {
        #region Khai báo
        public readonly IEMContractAttachmentService _IEMContractAttachmentService;
        public readonly IEMContractService _IEmContractService;
        public readonly IAccountingObjectBankAccountService _IAccountingObjectBankAccountService;
        public readonly IAccountingObjectService _IAccountingObjectService;
        public readonly IContractStateService _IContractStateService;
        public readonly IGenCodeService _IGenCodeService;
        public readonly IEMContractDetailPaymentService _IEMContractDetailPaymentService;
        public readonly IEMContractDetailMGService _IEMContractDetailMGService;
        public readonly ISAOrderService _ISAOrderService;
        IEMContractDetailRevenueService _IEMContractDetailRevenueService { get { return IoC.Resolve<IEMContractDetailRevenueService>(); } }
        public readonly ISAOrderDetailService _ISAOrderDetailService;
        public readonly IGeneralLedgerService _IGeneralLedgerService;
        public readonly ISAInvoiceDetailService _ISAInvoiceDetailService;
        public readonly ISAReturnDetailService _ISAReturnDetailService;
        public readonly IPPInvoiceDetailService _IPPInvoiceDetailService;
        private List<EMContractAttachment> listEMContractAttachment = new List<EMContractAttachment>();
        private List<EMContractDetailPayment> listEMContractDetailPayment = new List<EMContractDetailPayment>();
        private List<EMContractDetailMG> listEMContractDetailMG = new List<EMContractDetailMG>();
        private List<EMContractDetailRevenue> listEMContractRevenue = new List<EMContractDetailRevenue>();
        private List<EMContract> listEMContract = new List<EMContract>();
        private List<EMContractSale> listEmContractThucThu = new List<EMContractSale>();
        private List<EMContractSale> listEmContractThucChi = new List<EMContractSale>();
        private List<ContractState> lstContractState = new List<ContractState>();
        private string[] formats = { "dd/MM/yyyy", "MM/dd/yyyy" };
        private EMContract _select;
        private bool them = true;
        private bool _firstItem = true;
        private List<int> typeThucThu = new List<int>() { 100, 101, 320, 321, 322, 323, 324 };
        private List<int> typeThucChi = new List<int>() { 110, 260, 261, 262, 263, 264 };
        public static bool isclose = true;
        public static bool check1 = false;
        public static bool check2 = false;
        private Guid _id;
        bool checkActive = true;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion

        #region Khởi tạo
        public FEMContractSaleDetail()
        {
            InitializeComponent();
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IEmContractService = IoC.Resolve<IEMContractService>();
            _IEMContractAttachmentService = IoC.Resolve<IEMContractAttachmentService>();
            _IEMContractDetailPaymentService = IoC.Resolve<IEMContractDetailPaymentService>();
            _IEMContractDetailMGService = IoC.Resolve<IEMContractDetailMGService>();
            _ISAOrderService = IoC.Resolve<ISAOrderService>();
            _ISAOrderDetailService = IoC.Resolve<ISAOrderDetailService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _ISAInvoiceDetailService = IoC.Resolve<ISAInvoiceDetailService>();
            _ISAReturnDetailService = IoC.Resolve<ISAReturnDetailService>();
            _IPPInvoiceDetailService = IoC.Resolve<IPPInvoiceDetailService>();
            _firstItem = true;
            optContract.CheckedIndex = 0;
            check1 = false;
            check2 = false;
            uGridChungTuThu.DataSource = new BindingList<EMContractSale>(listEmContractThucThu);
            Utils.ConfigGrid(uGridChungTuThu, "Chung Tu Thu", GetColumnThucthu());
            uGridChungTuThu.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridChungTuThu.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGridChungTuChi.DataSource = new BindingList<EMContractSale>(listEmContractThucChi);
            Utils.ConfigGrid(uGridChungTuChi, "Chung Tu Chi", GetColumnsThucChi());
            uGridChungTuChi.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridChungTuChi.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);

            InitializeGUI();
            this.Text = "Thêm mới hợp đồng bán";
            them = true;

            txtAmount.ReadOnly = true;
            txtClosedDate.Enabled = false;
            txtClosedDate.ResetText();
            ultraDateTimeEditor4.Enabled = false;
            ultraDateTimeEditor4.ResetText();
            txtEMContractCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(86));

            if (optContract.CheckedIndex == 0)
            {
                if (txtSignedDate.Text != "")
                {
                    txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc :", txtEMContractCode.Text, FormatDate(txtSignedDate.Text, "dd/MM/yyyy", formats));
                }
            }
            else
            {
                txtName.Text = "";
            }
            btnXoa.Visible = false;
            btnGhidoanhso.Visible = false;
            btnBoghidoanhso.Visible = false;
            btnSua.Visible = false;
            btnSave.Enabled = true;
            //_select = new EMContract();
            cbbOrderID.DataSource = _ISAOrderService.GetAllOrderByNoAndNotInContract(Guid.Empty);
            Utils.ConfigGrid(cbbOrderID, ConstDatabase.SAOrder_TableName);
            UltraGridColumn c = cbbOrderID.DisplayLayout.Bands[0].Columns.Add();
            if (c.Key == "")
                c.Key = "Selected";
            c.Header.Caption = string.Empty;
            c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            c.DataType = typeof(bool);
            c.Header.VisiblePosition = 0;
            cbbOrderID.CheckedListSettings.ListSeparator = "/";
            cbbOrderID.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            cbbOrderID.CheckedListSettings.CheckStateMember = "Selected";
            cbbOrderID.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            cbbOrderID.ValueMember = "ID";
            cbbOrderID.DisplayMember = "No";

        }

        public FEMContractSaleDetail(EMContract temp)
        {
            InitializeComponent();
            _IAccountingObjectBankAccountService = IoC.Resolve<IAccountingObjectBankAccountService>();
            _IContractStateService = IoC.Resolve<IContractStateService>();
            _IAccountingObjectService = IoC.Resolve<IAccountingObjectService>();
            _IGenCodeService = IoC.Resolve<IGenCodeService>();
            _IEmContractService = IoC.Resolve<IEMContractService>();
            _IEMContractAttachmentService = IoC.Resolve<IEMContractAttachmentService>();
            _IEMContractDetailPaymentService = IoC.Resolve<IEMContractDetailPaymentService>();
            _IEMContractDetailMGService = IoC.Resolve<IEMContractDetailMGService>();
            _ISAOrderService = IoC.Resolve<ISAOrderService>();
            _ISAOrderDetailService = IoC.Resolve<ISAOrderDetailService>();
            _IGeneralLedgerService = IoC.Resolve<IGeneralLedgerService>();
            _ISAInvoiceDetailService = IoC.Resolve<ISAInvoiceDetailService>();
            _ISAReturnDetailService = IoC.Resolve<ISAReturnDetailService>();
            _IPPInvoiceDetailService = IoC.Resolve<IPPInvoiceDetailService>();
            this.Text = "Hợp đồng bán";
            check1 = true;
            check2 = true;
            them = false;
            _select = temp;
            cbbOrderID.DataSource = _ISAOrderService.GetOrderByContract(_select.ID);
            Utils.ConfigGrid(cbbOrderID, ConstDatabase.SAOrder_TableName);
            UltraGridColumn c = cbbOrderID.DisplayLayout.Bands[0].Columns.Add();
            if (c.Key == "")
                c.Key = "Selected";
            c.Header.Caption = string.Empty;
            c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            c.DataType = typeof(bool);
            c.Header.VisiblePosition = 0;
            cbbOrderID.CheckedListSettings.ListSeparator = "/";
            cbbOrderID.CheckedListSettings.ItemCheckArea = ItemCheckArea.Item;
            cbbOrderID.CheckedListSettings.CheckStateMember = "Selected";
            cbbOrderID.CheckedListSettings.EditorValueSource = EditorWithComboValueSource.CheckedItems;
            cbbOrderID.ValueMember = "ID";
            cbbOrderID.DisplayMember = "No";
            //if (chkIsWatchForCostPrice.Checked == true)
            //{
            //    lblCostset.Visible = true;
            //    cbbCostSet.Visible = true;
            //}
            //else
            //{
            //    lblCostset.Visible = false;
            //    cbbCostSet.Visible = false;
            //}

            if (chkIsBillPaid.Checked == true)
            {
                ultraDateTimeEditor4.Enabled = true;
            }
            else
                ultraDateTimeEditor4.Enabled = false;

            txtAmount.ReadOnly = true;
            txtEMContractCode.Enabled = false;

            listEMContractAttachment = _IEMContractAttachmentService.GetEMContractAttachmentByContractID(temp.ID);
            listEMContractDetailPayment = _IEMContractDetailPaymentService.getEMContractDetailPaymentbyIDEMContract(temp.ID);
            listEMContractDetailMG = _IEMContractDetailMGService.GetEMContractDetailMGByContractID(temp.ID);
            listEmContractThucThu = _IGeneralLedgerService.GetEMContractSaleDetailByContractID(temp.ID, typeThucThu);
            listEmContractThucChi = _IGeneralLedgerService.GetEMContractSaleThucChiDetailByContractID(temp.ID, typeThucChi);
            //var listSAOrderDetail = _ISAOrderDetailService.GetSAOrderDetailByContractID((Guid)temp.ID);

            uGridChungTuThu.DataSource = new BindingList<EMContractSale>(listEmContractThucThu);
            Utils.ConfigGrid(uGridChungTuThu, "Chung Tu Thu", GetColumnThucthu());
            uGridChungTuThu.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridChungTuThu.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);

            uGridChungTuChi.DataSource = new BindingList<EMContractSale>(listEmContractThucChi);
            Utils.ConfigGrid(uGridChungTuChi, "Chung Tu Chi", GetColumnsThucChi());
            uGridChungTuChi.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridChungTuChi.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);

            InitializeGUI();
            ObjandGUI(temp, false);
        }
        #endregion

        #region override
        private void InitializeGUI()
        {
            cbbAccountingObjectBankAccount.DataSource = new List<AccountingObjectBankAccount>();
            Utils.ConfigGrid(cbbAccountingObjectBankAccount, ConstDatabase.AccountingObjectBankAccount_TableName);
            cbbAccountingObjectBankAccount.DisplayMember = "BankAccount";
            cbbAccountingObjectBankAccount.ValueMember = "ID";
            cbbAccountingObjectBankAccount.DropDownStyle = UltraComboStyle.DropDownList;

            listEMContract = _IEmContractService.GetAllProject();
            cbbDA.DataSource = listEMContract;
            Utils.ConfigGrid(cbbDA, ConstDatabase.FProject_TableName);
            cbbDA.ValueMember = "ID";
            cbbDA.DisplayMember = "Code";

            cbbAccountingObjectID.DataSource = Utils.ListAccountingObject.Where(x => (x.ObjectType == 0 || x.ObjectType == 2) && x.IsActive).ToList();
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            cbbAccountingObjectID.ValueMember = "ID";
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";

            cbbCurrencyID.DataSource = Utils.ListCurrency;
            Utils.ConfigGrid(cbbCurrencyID, ConstDatabase.Currency_TableName);
            cbbCurrencyID.ValueMember = "ID";

            cbbContractEmployeeID.DataSource = Utils.ListAccountingObject.Where(x => x.IsEmployee).ToList().OrderBy(x => x.AccountingObjectCode).ToList();
            Utils.ConfigGrid(cbbContractEmployeeID, ConstDatabase.AccountingObject_TableName);
            cbbContractEmployeeID.ValueMember = "ID";
            cbbContractEmployeeID.DisplayMember = "AccountingObjectCode";
            cbbContractEmployeeID.DisplayLayout.Bands[0].Columns["AccountingObjectCode"].Header.Caption = "Mã nhân viên";
            cbbContractEmployeeID.DisplayLayout.Bands[0].Columns["AccountingObjectName"].Header.Caption = "Tên nhân viên";

            cbbDepartmentID.DataSource = Utils.ListDepartment;
            Utils.ConfigGrid(cbbDepartmentID, ConstDatabase.Department_TableName);
            cbbDepartmentID.ValueMember = "ID";
            cbbDepartmentID.DisplayMember = "DepartmentCode";

            lstContractState = _IContractStateService.GetAll();
            cbbContractState.DataSource = lstContractState;
            Utils.ConfigGrid(cbbContractState, ConstDatabase.ContractState_TableName);
            cbbContractState.ValueMember = "ID";
            cbbContractState.DisplayMember = "ContractStateName";

            cbbAccountingObjectBankAccount.DropDownStyle = UltraComboStyle.DropDownList;

            //Tab dự kiến chi
            uGridChi.DataSource = new BindingList<EMContractDetailPayment>(listEMContractDetailPayment);
            Utils.ConfigGrid(uGridChi, ConstDatabase.EMContractDetailPayment_TableName, new List<TemplateColumn>(), isBusiness: true);
            uGridChi.DisplayLayout.Bands[0].Columns["RecordDate"].Header.VisiblePosition = 0;
            uGridChi.DisplayLayout.Bands[0].Columns["ExpenseItemID"].Header.VisiblePosition = 1;
            uGridChi.DisplayLayout.Bands[0].Columns["DepartmentID"].Header.VisiblePosition = 2;
            uGridChi.DisplayLayout.Bands[0].Columns["Description"].Header.VisiblePosition = 3;
            uGridChi.DisplayLayout.Bands[0].Columns["Methods"].Header.VisiblePosition = 4;
            uGridChi.DisplayLayout.Bands[0].Columns["PercentAmount"].Header.VisiblePosition = 5;
            uGridChi.DisplayLayout.Bands[0].Columns["AmountOriginal"].Header.VisiblePosition = 6;
            uGridChi.DisplayLayout.Bands[0].Columns["AmountOriginal"].Style = ColumnStyle.DoubleNonNegative;


            UltraGridBand band = uGridChi.DisplayLayout.Bands[0];
            uGridChi.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["RecordDate"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridChi.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridChi.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridChi.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridChi.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            summary = band.Summaries.Add("sumAmountOriginal", SummaryType.Sum, band.Columns["AmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band.Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridChi.Rows.SummaryValues["sumAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            var cbbMethods = new UltraComboEditor();
            cbbMethods.Items.Add(0, "Tỷ lệ %");
            cbbMethods.Items.Add(1, "Số tiền");
            foreach (var column in uGridChi.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridChi);
                if (column.Key.Equals("Methods"))
                {
                    column.EditorComponent = cbbMethods;
                    column.Style = ColumnStyle.DropDownList;
                }
                if (column.Key.Equals("RecordDate"))
                {
                    column.MaskInput = "{LOC}dd/mm/yyyy";
                    //item.MaskInput = "##/##/####";
                    column.Format = Constants.DdMMyyyy;
                }
            }
            foreach (var item in uGridChi.Rows)
            {
                if ((int)item.Cells["Methods"].Value == 1)
                {
                    item.Cells["PercentAmount"].Activation = Activation.NoEdit;
                }
            }

            //Tab Hàng hóa dịch vụ
            uGridEMContractMG.DataSource = new BindingList<EMContractDetailMG>(listEMContractDetailMG);
            Utils.ConfigGrid(uGridEMContractMG, ConstDatabase.EMContractDetailMG_TableName, new List<TemplateColumn>(), true);
            UltraGridBand band1 = uGridEMContractMG.DisplayLayout.Bands[0];
            band1.Summaries.Clear();
            band1.Columns["SAOrderCode"].CellActivation = Activation.NoEdit;
            band1.Columns["TotalAmount"].CellClickAction = CellClickAction.Edit;
            band1.Columns["QuantityReceipt"].CellActivation = Activation.NoEdit;
            band1.Columns["SAOrderID"].Hidden = true;
            band1.Columns["PPOrderID"].Hidden = true;
            band1.Columns["PPOrderCode"].Hidden = true;
            band1.Columns["MaterialGoodsCode"].Hidden = true;

            foreach (UltraGridColumn column in uGridEMContractMG.DisplayLayout.Bands[0].Columns)
            {
                this.ConfigEachColumn4Grid(0, column, uGridEMContractMG);
            }

            if (uGridEMContractMG.Rows.Count > 0)
            {
                foreach (UltraGridRow row in uGridEMContractMG.Rows)
                {
                    if ((decimal)row.Cells["QuantityReceipt"].Value == 0 && !them) row.Cells["MaterialGoodsID"].Activation = Activation.NoEdit;
                }
            }

            SummarySettings summary1 = band1.Summaries.Add("Count", SummaryType.Count, band1.Columns["MaterialGoodsID"]);
            summary1.DisplayFormat = "Số dòng = {0:N0}";
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridEMContractMG.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridEMContractMG.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridEMContractMG.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridEMContractMG.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;

            summary1 = band1.Summaries.Add("sumAmount", SummaryType.Sum, band1.Columns["TotalAmount"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary1.DisplayFormat = "{0:N0}";
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["TotalAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumAmount"].Appearance.TextHAlign = HAlign.Right;

            summary1 = band1.Summaries.Add("sumDiscountAmount", SummaryType.Sum, band1.Columns["DiscountAmount"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary1.DisplayFormat = "{0:N0}";
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["DiscountAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumDiscountAmount"].Appearance.TextHAlign = HAlign.Right;

            summary1 = band1.Summaries.Add("sumVATAmount", SummaryType.Sum, band1.Columns["VATAmount"]);
            summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary1.DisplayFormat = "{0:N0}";
            summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["VATAmount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumVATAmount"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumAmountOriginal", SummaryType.Sum, band1.Columns["TotalAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["TotalAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumDiscountAmountOriginal", SummaryType.Sum, band1.Columns["DiscountAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["DiscountAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumDiscountAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            summary = band1.Summaries.Add("sumVATAmountOriginal", SummaryType.Sum, band1.Columns["VATAmountOriginal"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band1.Columns["VATAmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridEMContractMG.Rows.SummaryValues["sumVATAmountOriginal"].Appearance.TextHAlign = HAlign.Right;

            //Tab đính kèm tài liệu
            uGridEMContractAttachment.DataSource = listEMContractAttachment;
            Utils.ConfigGrid(uGridEMContractAttachment, ConstDatabase.EMContractAttachment_TableName);

            txtExchangeRate.FormatNumberic(ConstDatabase.Format_Rate);
            txtAmountOriginal.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmount.FormatNumberic(ConstDatabase.Format_TienVND);
            txtAmountBeforCancel.FormatNumberic(ConstDatabase.Format_TienVND);
        }

        #endregion
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        #region event

        private void optContract_ValueChanged(object sender, EventArgs e)
        {
            if (optContract.CheckedItem.Tag.ToString().Equals("DA"))
            {
                _firstItem = false;
                ultraLabel1.Text = "Mã dự án (*)";
                lblGTHD.Text = "Giá trị DA (*)";
                lblGTHDQD.Text = "Giá trị DA QĐ (*)";
                ultraLabel27.Text = "Tình trạng DA (*)";
                lblDA.Visible = false;
                cbbDA.Visible = false;
                ultraLabel5.Location = new Point(12, 51);
                txtName.Location = new Point(107, 51);
                txtName.Text = "";
                ultraLabel3.Visible = false;
                cbbOrderID.Visible = false;
                ultraLabel4.Location = new Point(12, 104);
                txtSignedDate.Location = new Point(107, 104);
                ultraLabel2.Location = new Point(203, 104);
                txtDueDate.Location = new Point(310, 104);
                ultraLabel6.Location = new Point(203, 132);
                txtDeliverDate.Location = new Point(310, 132);

                ultraTabPageControl3.Tab.Visible = false;
                ultraTabPageControl4.Tab.Text = "&3. Tài liệu đính kèm";
                chkIsBillPaid.Visible = false;
                ultraLabel26.Visible = false;
                ultraDateTimeEditor4.Visible = false;
            }
            else if (optContract.CheckedItem.Tag.ToString().Equals("HD"))
            {
                _firstItem = true;
                ultraLabel1.Text = "Số hợp đồng (*)";
                lblGTHD.Text = "Giá trị HĐ (*)";
                lblGTHDQD.Text = "Giá trị HĐ QĐ (*)";
                ultraLabel27.Text = "Tình trạng HĐ (*)";
                lblDA.Visible = true;
                cbbDA.Visible = true;
                lblDA.Location = new Point(12, 51);
                cbbDA.Location = new Point(107, 51);
                ultraLabel5.Location = new Point(12, 79);
                txtName.Location = new Point(107, 79);
                if (txtSignedDate.Text != "")
                    txtName.Text = txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc :", txtEMContractCode.Text, FormatDate(txtSignedDate.Text, "dd/MM/yyyy", formats));
                else
                    txtName.Text = "";
                ultraLabel3.Visible = true;
                cbbOrderID.Visible = true;
                ultraLabel3.Location = new Point(12, 132);
                cbbOrderID.Location = new Point(107, 132);
                ultraLabel4.Location = new Point(12, 160);
                txtSignedDate.Location = new Point(107, 160);
                ultraLabel6.Location = new Point(203, 160);
                txtDeliverDate.Location = new Point(310, 160);
                ultraLabel2.Location = new Point(203, 188);
                txtDueDate.Location = new Point(310, 188);

                if (!ultraTabPageControl3.Tab.IsInView)
                {
                    ultraTabPageControl3.Tab.Visible = true;
                    ultraTabPageControl4.Tab.Text = "&4. Tài liệu đính kèm";
                }
                chkIsBillPaid.Visible = true;
                ultraLabel26.Visible = true;
                ultraDateTimeEditor4.Visible = true;
            }
        }

        private void cbbDA_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            List<EMContract> listDA = _IEmContractService.GetAllProject();
            //if (cbbDA.Value != null)
            {
                if (!listDA.Exists(x => x.Code == cbbDA.Text) && cbbDA.Text != "")
                {
                    MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
                }
            }
        }

        private void cbbContractState_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FContractStateDetail().ShowDialog(this);
            lstContractState = _IContractStateService.GetAll();
            cbbContractState.DataSource = lstContractState;
            Utils.ConfigGrid(cbbContractState, ConstDatabase.ContractState_TableName);
            cbbContractState.ValueMember = "ID";
            cbbContractState.DisplayMember = "ContractStateName";
            Utils.ClearCacheByType<ContractState>();
            cbbContractState.SelectedRow = cbbContractState.Rows.Last();
        }

        private void cbbContractEmployeeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FAccountingObjectEmployeeDetail().ShowDialog(this);
            cbbContractEmployeeID.DataSource = Utils.ListAccountingObject.Where(x => x.IsEmployee && x.IsActive == true).ToList().OrderBy(x => x.AccountingObjectCode).ToList();
            Utils.ConfigGrid(cbbContractEmployeeID, ConstDatabase.AccountingObject_TableName);
            cbbContractEmployeeID.ValueMember = "ID";
            cbbContractEmployeeID.DisplayMember = "AccountingObjectCode";
            Utils.ClearCacheByType<FAccountingObjectEmployee>();
            cbbContractEmployeeID.DataSource = Utils.ListAccountingObject.Where(x => x.IsEmployee && x.IsActive == true).ToList().OrderBy(x => x.AccountingObjectCode).ToList();

            cbbContractEmployeeID.SelectedRow = cbbContractEmployeeID.Rows.Last();
        }

        private void cbbAccountingObjectID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            new FAccountingObjectCustomersDetail("1").ShowDialog(this);
            cbbAccountingObjectID.DataSource = Utils.ListAccountingObject.Where(x => x.ObjectType == 0 && x.IsActive).ToList();
            Utils.ConfigGrid(cbbAccountingObjectID, ConstDatabase.AccountingObject_TableName);
            cbbAccountingObjectID.ValueMember = "ID";
            cbbAccountingObjectID.DisplayMember = "AccountingObjectCode";
            Utils.ClearCacheByType<AccountingObject>();
            cbbAccountingObjectID.SelectedText = FAccountingObjectCustomersDetail.AccounttingObjectCode;
        }

        private void cbbOrderID_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            //List<SAOrder> listSAOrder = _ISAOrderService.GetAll();
            //if (!listSAOrder.Exists(x => x.No == cbbOrderID.Text.Trim()) && cbbOrderID.Text != "")
            //{
            //    MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
            //}
        }

        private void cbbContractEmployeeID_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            List<AccountingObject> listAccountingObject = Utils.ListAccountingObject.Where(x => x.IsEmployee && x.IsActive == true).ToList().OrderBy(x => x.AccountingObjectCode).ToList();
            if (!listAccountingObject.Exists(x => x.AccountingObjectCode == cbbContractEmployeeID.Text.ToString()) && cbbContractEmployeeID.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
            }
        }

        private void cbbAccountingObjectID_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            List<AccountingObject> listAccountingObject = Utils.ListAccountingObject.Where(x => x.ObjectType == 0 && x.IsActive).ToList();
            if (!listAccountingObject.Exists(x => x.AccountingObjectCode == cbbAccountingObjectID.Text.ToString()) && cbbAccountingObjectID.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
                cbbAccountingObjectID.Focus();
            }
            if (cbbAccountingObjectID.Value == null || cbbAccountingObjectID.Text.Trim() == "")
            {
                txtAccountingObjectName.Text = "";
                txtAccountingObjectTitle.Text = "";
                txtAccountingObjectSignerName.Text = "";
                txtAccountingObjectAddress.Text = "";
                txtAccountingObjectTel.Text = "";
                txtAccountingObjectFax.Text = "";
                cbbAccountingObjectBankAccount.Text = cbbAccountingObjectBankAccount.NullText;
                txtAccountingObjectBankName.Text = "";
            }
        }

        private void cbbContractState_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            List<ContractState> listContractState = _IContractStateService.GetAll();
            if (!listContractState.Exists(x => x.ContractStateName == cbbContractState.Text.ToString()) && cbbContractState.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
            }
        }

        #endregion event

        #region Utils
        private EMContract ObjandGUI(EMContract input, bool isGet)
        {
            if (isGet)//thêm mới
            {
                if (input.ID == Guid.Empty)
                    input.ID = Guid.NewGuid();
                input.Code = txtEMContractCode.Text;
                input.CurrencyID = (string)(cbbCurrencyID.Value);
                if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                {
                    input.ExchangeRate = decimal.Parse(txtExchangeRate.Value.ToString());
                }
                if (txtAmount.Value != null)
                {
                    input.Amount = decimal.Parse(txtAmount.Value.ToString());
                    input.AmountOriginal = decimal.Parse(txtAmount.Value.ToString());
                }
                if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
                {
                    input.AmountOriginal = decimal.Parse(txtAmountOriginal.Value.ToString());
                }
                if ((txtAmountBeforCancel.Value != null) && (txtAmountBeforCancel.Text.Trim() != ""))
                {
                    input.AmountBeforCancel = decimal.Parse(txtAmountBeforCancel.Value.ToString());
                }
                if (txtSignedDate.Value != null)
                {
                    input.SignedDate = DateTime.Parse(txtSignedDate.Value.ToString());
                }
                if (txtDeliverDate.Value != null)
                {
                    input.PostedDate = DateTime.Parse(txtDeliverDate.Value.ToString());
                }
                if (ultraDateTimeEditor4.Value != null)
                    input.InvoiceDate = DateTime.Parse(ultraDateTimeEditor4.Value.ToString());
                Department department = (Department)Utils.getSelectCbbItem(cbbDepartmentID);
                if (department == null) input.DepartmentID = null;
                else input.DepartmentID = department.ID;

                AccountingObject employee = (AccountingObject)Utils.getSelectCbbItem(cbbContractEmployeeID);
                if (employee == null) input.EmployeeID = null;
                else input.EmployeeID = employee.ID;

                AccountingObject accountingObject = (AccountingObject)Utils.getSelectCbbItem(cbbAccountingObjectID);
                if (accountingObject == null) input.AccountingObjectID = null;
                else input.AccountingObjectID = accountingObject.ID;

                if (_firstItem)
                {
                    input.ProjectID = (Guid?)(cbbDA.Value);
                }
                input.IsProject = (optContract.CheckedIndex == 0) ? false : true;
                input.AccountingObjectTitle = txtAccountingObjectTitle.Text;
                input.AccountingObjectName = txtAccountingObjectName.Text;
                input.AccountingObjectSignerName = txtAccountingObjectSignerName.Text;
                input.AccountingObjectTel = txtAccountingObjectTel.Text;
                input.AccountingObjectFax = txtAccountingObjectFax.Text;
                input.AccountingObjectAddress = txtAccountingObjectAddress.Text;
                input.AccountingObjectBankAccountDetailID = (Guid?)(cbbAccountingObjectBankAccount.Value);
                input.AccountingObjectBankName = txtAccountingObjectBankName.Text;
                input.CompanyName = txtCompanyName.Text;

                ContractState contractState = (ContractState)Utils.getSelectCbbItem(cbbContractState);
                if (contractState == null) input.ContractState = 0;
                else input.ContractState = contractState.ID;

                if (txtClosedDate.Value != null)
                {
                    input.ClosedDate = DateTime.Parse(txtClosedDate.Value.ToString());
                }
                input.CompanyTel = txtCompanyTel.Text;
                input.ClosedReason = txtClosedReason.Text;
                if (txtClosedReason.Text.Trim() == "")
                {
                    input.ClosedReason = null;
                }
                input.SignerName = txtSignerName.Text;
                input.Reason = txtReason.Text;
                input.SignerTitle = txtSignerTitle.Text;
                input.IsBillPaid = chkIsBillPaid.Checked;
                input.TypeID = 860;
                input.Name = txtName.Text;
                input.IsActive = true;
                input.RevenueType = false;
                input.DeliverDate = (txtDeliverDate.Value != null) ? (DateTime?)DateTime.Parse(txtDeliverDate.Value.ToString()) : null;
                input.DueDate = (txtDueDate.Value != null) ? (DateTime?)DateTime.Parse(txtDueDate.Value.ToString()) : null;
                input.IsWatchForCostPrice = chkIsWatchForCostPrice.Checked;
                //btnXoa.Visible = false;
                //btnGhidoanhso.Visible = false;
                //btnBoghidoanhso.Visible = false;
                //btnSua.Visible = false;
                List<string> lstID = new List<string>();
                foreach (var item in cbbOrderID.Rows)
                {
                    SAOrder pporder = item.ListObject as SAOrder;
                    if ((bool)item.Cells["Selected"].Value == true)
                        lstID.Add(pporder.ID.ToString() + ";");

                }
                foreach (var item in lstID)
                {
                    input.OrderID += item;
                }
            }
            else
            {
                if (input.RevenueType == true)
                {
                    btnBoghidoanhso.Visible = true;
                    btnGhidoanhso.Visible = false;
                    btnSua.Enabled = false;
                    btnXoa.Enabled = false;
                }
                else
                {
                    btnSua.Enabled = true;
                    btnXoa.Enabled = true;
                    btnBoghidoanhso.Visible = false;
                    btnGhidoanhso.Visible = true;
                }
                btnXoa.Visible = true;
                btnSua.Visible = true;
                optContract.Enabled = false;
                txtEMContractCode.ReadOnly = true;
                txtName.ReadOnly = true;
                cbbCurrencyID.ReadOnly = true;
                txtExchangeRate.ReadOnly = true;
                txtAmountOriginal.ReadOnly = true;
                txtAmount.ReadOnly = true;
                txtAmountBeforCancel.ReadOnly = true;
                txtSignedDate.ReadOnly = true;
                txtDeliverDate.ReadOnly = true;
                cbbDepartmentID.ReadOnly = true;
                cbbContractEmployeeID.Enabled = false;
                cbbAccountingObjectID.Enabled = false;
                cbbDA.ReadOnly = true;
                txtAccountingObjectTitle.ReadOnly = true;
                txtAccountingObjectName.ReadOnly = true;
                txtAccountingObjectSignerName.ReadOnly = true;
                txtAccountingObjectTel.ReadOnly = true;
                txtAccountingObjectFax.ReadOnly = true;
                txtAccountingObjectAddress.ReadOnly = true;
                cbbAccountingObjectBankAccount.ReadOnly = true;
                txtAccountingObjectBankName.ReadOnly = true;
                txtCompanyName.ReadOnly = true;
                cbbContractState.Enabled = false;
                txtClosedDate.ReadOnly = true;
                txtCompanyTel.ReadOnly = true;
                txtSignerName.ReadOnly = true;
                txtReason.ReadOnly = true;
                txtSignerTitle.ReadOnly = true;
                chkIsWatchForCostPrice.Enabled = false;
                //cbbCostSet.Enabled = false;
                chkIsBillPaid.Enabled = false;
                txtDeliverDate.ReadOnly = true;
                txtDueDate.ReadOnly = true;
                ultraDateTimeEditor4.ReadOnly = true;
                btnSave.Enabled = false;

                foreach (var item in uGridChi.DisplayLayout.Bands[0].Columns)
                {
                    item.CellActivation = Activation.NoEdit;
                }

                foreach (var item in uGridEMContractMG.DisplayLayout.Bands[0].Columns)
                {
                    item.CellActivation = Activation.NoEdit;
                }

                btnChungTuChi.Enabled = false;
                btnChungTuThu.Enabled = false;
                btnAdd.Enabled = false;
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                //}
                txtEMContractCode.Text = input.Code;
                cbbCurrencyID.Value = input.CurrencyID;
                if (input.ExchangeRate != null)
                {
                    txtExchangeRate.Value = input.ExchangeRate;
                }
                if (input.AmountOriginal != null)
                {
                    txtAmountOriginal.Value = input.AmountOriginal;
                }
                if (input.Amount != null)
                {
                    txtAmount.Value = input.Amount;
                }
                if (input.AmountBeforCancel != null)
                {
                    txtAmountBeforCancel.Value = input.AmountBeforCancel;
                }
                if (input.SignedDate != null)
                {
                    txtSignedDate.Value = input.SignedDate;
                }
                if (input.EffectiveDate != null)
                {
                    txtDeliverDate.Value = input.PostedDate;
                }
                ultraDateTimeEditor4.Value = input.InvoiceDate;
                cbbDepartmentID.Value = input.DepartmentID;
                cbbContractEmployeeID.Value = input.EmployeeID;
                cbbAccountingObjectID.Value = input.AccountingObjectID;
                cbbDA.Value = input.ProjectID;
                if (input.IsProject == true)
                {
                    optContract.CheckedIndex = 1;
                }
                else
                {
                    optContract.CheckedIndex = 0;
                }
                optContract.Enabled = false;
                txtAccountingObjectTitle.Text = input.AccountingObjectTitle;
                txtAccountingObjectName.Text = input.AccountingObjectName;
                txtAccountingObjectSignerName.Text = input.AccountingObjectSignerName;
                txtAccountingObjectTel.Text = input.AccountingObjectTel;
                txtAccountingObjectFax.Text = input.AccountingObjectFax;
                txtAccountingObjectAddress.Text = input.AccountingObjectAddress;
                cbbAccountingObjectBankAccount.Value = input.AccountingObjectBankAccountDetailID;
                txtAccountingObjectBankName.Text = input.AccountingObjectBankName;
                txtCompanyName.Text = input.CompanyName;
                cbbContractState.Value = input.ContractState;
                if (input.ClosedDate != null)
                {
                    txtClosedDate.Value = input.ClosedDate;
                }
                txtCompanyTel.Text = input.CompanyTel;
                txtSignerName.Text = input.SignerName;
                txtReason.Text = input.Reason;
                txtSignerTitle.Text = input.SignerTitle;
                chkIsWatchForCostPrice.Checked = input.IsWatchForCostPrice;
                //cbbCostSet.Value = input.CostSetID;
                chkIsBillPaid.Checked = input.IsBillPaid;
                chkIsWatchForCostPrice.Checked = input.IsWatchForCostPrice;
                txtName.Text = input.Name;
                txtDeliverDate.Value = input.DeliverDate;
                txtDueDate.Value = input.DueDate;
                if (input.OrderID != "" && input.OrderID != null)
                {
                    string[] ss = input.OrderID.Substring(0, input.OrderID.Length - 1).Split(';');
                    List<Guid> guid = new List<Guid>();
                    foreach (var item in ss)
                    {
                        guid.Add(Guid.Parse(item));
                    }
                    foreach (var item in cbbOrderID.Rows)
                    {
                        foreach (var item2 in guid)
                        {
                            if ((item.ListObject as SAOrder).ID == item2)
                            {
                                item.Cells["Selected"].Value = true;
                            }
                        }
                    }
                }
                //List<Guid> lstSAOrder = _ISAOrderService.GetOrderIDByContractID(input.ID);
                //if (lstSAOrder.Count > 0)
                //{
                //    foreach (var item in cbbOrderID.Rows)
                //    {
                //        SAOrder saorder = item.ListObject as SAOrder;
                //        for (int i = 0; i < lstSAOrder.Count; i++)
                //        {
                //            if (saorder.ID == lstSAOrder[i])
                //                item.Cells["Selected"].Value = true;
                //        }
                //    }
                //}
            }
            return input;
        }

        #endregion
        private void cbbCurrencyID_TextChanged(object sender, EventArgs e)
        {
            txtAmountOriginal.Enabled = true;
            txtAmount.ReadOnly = true;
            if (cbbCurrencyID.Text == "VND")
            {
                txtExchangeRate.Value = 1;
                txtExchangeRate.Enabled = false;
            }
        }

        private void cbbCurrencyID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if (e.Row == null)
            {
                cbbCurrencyID.Value = "VND";
            }
            if (e.Row != null)
            {
                Currency temp = e.Row.ListObject as Currency;
                if (temp.ID != "VND")
                {
                    txtExchangeRate.Value = temp.ExchangeRate;
                    txtAmount.Enabled = true;
                    if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
                    {
                        decimal rateEchangeTemp;
                        decimal orgTemp;
                        if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                        {
                            rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                            orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                            txtAmount.Value = rateEchangeTemp * orgTemp;
                            txtAmountBeforCancel.Value = txtAmountOriginal.Value;
                        }
                    }
                }
                if (temp.ID == "VND")
                {
                    txtExchangeRate.Value = 1;
                    txtAmount.Enabled = false;
                    if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
                    {
                        decimal rateEchangeTemp;
                        decimal orgTemp;
                        if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                        {
                            rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                            orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                            txtAmount.Value = rateEchangeTemp * orgTemp;
                            txtAmountBeforCancel.Value = txtAmountOriginal.Value;
                        }
                    }
                }
                UltraGridBand band1 = uGridEMContractMG.DisplayLayout.Bands[0];
                band1.Columns["UnitPrice"].Hidden = temp.ID == "VND";
                band1.Columns["TotalAmount"].Hidden = temp.ID == "VND";
                band1.Columns["DiscountAmount"].Hidden = temp.ID == "VND";
                band1.Columns["VATAmount"].Hidden = temp.ID == "VND";
                band1.Columns["UnitPriceOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_DonGiaQuyDoi : ConstDatabase.Format_DonGiaNgoaiTe);
                band1.Columns["TotalAmountOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                band1.Columns["DiscountAmountOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                band1.Columns["VATAmountOriginal"].FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                foreach (var row in uGridEMContractMG.Rows)
                {
                    row.Cells["UnitPrice"].Value = (decimal)row.Cells["UnitPriceOriginal"].Value * temp.ExchangeRate;
                    row.Cells["TotalAmount"].Value = (decimal)row.Cells["TotalAmountOriginal"].Value * temp.ExchangeRate;
                    row.Cells["DiscountAmount"].Value = (decimal)row.Cells["DiscountAmountOriginal"].Value * temp.ExchangeRate;
                    row.Cells["VATAmount"].Value = (decimal)row.Cells["VATAmountOriginal"].Value * temp.ExchangeRate;
                }
                txtAmountOriginal.FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
                txtAmountBeforCancel.FormatNumberic(temp.ID == "VND" ? ConstDatabase.Format_TienVND : ConstDatabase.Format_ForeignCurrency);
            }
        }

        private void txtAmountOriginal_TextChanged(object sender, EventArgs e)
        {
            if ((txtAmountOriginal.Value != null) && (txtAmountOriginal.Text.Trim() != ""))
            {
                decimal rateEchangeTemp;
                decimal orgTemp;
                if ((txtExchangeRate.Value != null) && (txtExchangeRate.Text.Trim() != ""))
                {
                    rateEchangeTemp = Convert.ToDecimal(txtExchangeRate.Value);
                    orgTemp = Convert.ToDecimal(txtAmountOriginal.Value);
                    txtAmount.Value = rateEchangeTemp * orgTemp;
                    txtAmountBeforCancel.Value = txtAmountOriginal.Value;
                }

                foreach (var item in uGridChi.Rows)
                {
                    if (item.Cells["PercentAmount"].Activation != Activation.NoEdit)
                    {
                        item.Cells["AmountOriginal"].Value = Decimal.Parse(txtAmountOriginal.Value.ToString()) * Decimal.Parse(item.Cells["PercentAmount"].Value.ToString()) / 100;
                    }
                }
            }
        }

        private void txtExchangeRate_TextChanged(object sender, EventArgs e)
        {
            if (txtExchangeRate.Value != null)
            {
                txtAmount.Value = ((txtExchangeRate.Value as decimal?) ?? 0) * ((txtAmountOriginal.Value as decimal?) ?? 0);
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            //txtAmountBeforCancel.Value = txtAmount.Value;
        }

        private void cbbAccountingObjectID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbAccountingObjectID.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            AccountingObject accountingObject = e.Row.ListObject as AccountingObject;
            txtAccountingObjectName.Text = accountingObject.AccountingObjectName;
            txtAccountingObjectAddress.Text = accountingObject.Address;
            List<int> type = new List<int>() { 0, 2 };
            cbbAccountingObjectBankAccount.DataSource = _IAccountingObjectBankAccountService.GetByAccountingObjectType((Guid)cbbAccountingObjectID.Value, type);
            txtAccountingObjectFax.Text = accountingObject.Fax;
            txtAccountingObjectTel.Text = accountingObject.ContactMobile;
            check2 = true;
        }

        private void cbbContractState_RowSelected(object sender, RowSelectedEventArgs e)
        {
            if ((cbbContractState.Text == "Hủy bỏ") || (cbbContractState.Text == "Thanh lý") || (cbbContractState.Text == "Kết thúc"))
            {
                txtClosedDate.Enabled = true;
            }
            else
            {
                txtClosedDate.Enabled = false;
                txtClosedDate.ResetText();
            }
        }

        private void chkIsWatchForCostPrice_CheckedChanged(object sender, EventArgs e)
        {
            //    if (chkIsWatchForCostPrice.Checked == false)
            //    {
            //        lblCostset.Visible = false;
            //        cbbCostSet.Visible = false;
            //        cbbCostSet.ResetText();
            //    }
            //    else
            //    {
            //        lblCostset.Visible = true;
            //        cbbCostSet.Visible = true;
            //    }
        }

        private void chkIsBillPaid_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsBillPaid.Checked == false)
            {
                ultraDateTimeEditor4.Enabled = false;
                ultraDateTimeEditor4.ResetText();
            }
            else
            {
                ultraDateTimeEditor4.Enabled = true;
            }
        }

        //private void cbbCostSet_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        //{
        //    new FCostSetDetail().ShowDialog(this);
        //    cbbCostSet.DataSource = Utils.ListCostSet;
        //    Utils.ConfigGrid(cbbCostSet, ConstDatabase.CostSet_TableName);
        //    cbbCostSet.ValueMember = "ID";
        //    cbbCostSet.DisplayMember = "CostSetName";
        //    Utils.ClearCacheByType<CostSet>();
        //    cbbCostSet.SelectedText = FCostSetDetail.CostSetCode;
        //}

        private void txtEMContractCode_ValueChanged(object sender, EventArgs e)
        {
            if (_firstItem)
                if (txtSignedDate.Text != "")
                {
                    txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc :", txtEMContractCode.Text, FormatDate(txtSignedDate.Text, "dd/MM/yyyy", formats));
                }
                else
                    txtName.Text = "";
        }

        #region Lưu
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!checkerror()) return;

            EMContract temp = new EMContract();
            //temp = ObjandGUI(temp, true);
            //_select = temp;
            try
            {
                _IEmContractService.BeginTran();
                if (them == true)
                {
                    temp = ObjandGUI(temp, true);
                    _IEmContractService.CreateNew(temp);
                    _IGenCodeService.UpdateGenCodeCatalog(86, txtEMContractCode.Text);

                    if (cbbOrderID.Text != "")
                    {
                        foreach (var item1 in cbbOrderID.CheckedRows)
                        {
                            SAOrder saorder = (SAOrder)item1.ListObject;
                            IList<SAOrderDetail> lstsaorderdeail = _ISAOrderDetailService.GetSAOrderDetailBySAOrderCode(saorder.ID, temp.ID);
                            if (lstsaorderdeail.Count == 0)
                                lstsaorderdeail = _ISAOrderDetailService.GetSAOrderDetailBySAOrderCode(saorder.ID, null);
                            if (lstsaorderdeail.Count > 0)
                            {
                                foreach (var item in lstsaorderdeail)
                                {
                                    item.ContractID = temp.ID;
                                    _ISAOrderDetailService.Update(item);
                                }
                            }
                        }
                    }

                    if (listEMContractAttachment.Count > 0)
                    {
                        foreach (var item in listEMContractAttachment)
                        {
                            item.EMContractID = temp.ID;
                            _IEMContractAttachmentService.CreateNew(item);
                        }
                    }
                    if (listEMContractDetailPayment.Count > 0)
                    {
                        foreach (var item in listEMContractDetailPayment)
                        {
                            item.ID = Guid.NewGuid();
                            item.EMContractID = temp.ID;
                            if (item.AmountOriginal == null)
                            {
                                item.AmountOriginal = 0;
                            }
                            item.Amount = item.AmountOriginal;
                            item.AmountBeforeCancel = item.AmountOriginal;
                            _IEMContractDetailPaymentService.CreateNew(item);
                        }
                    }
                    //update mã hợp đồng vào chứng từ thu ghi sổ
                    if (uGridChungTuThu.Rows.Count > 0)
                    {
                        var listEmContractThucThu2 = uGridChungTuThu.DataSource as BindingList<EMContractSale>;
                        foreach (EMContractSale item in listEmContractThucThu2)
                        {
                            List<GeneralLedger> gl = new List<GeneralLedger>();
                            gl = _IGeneralLedgerService.GetGLByDetailID(item.GLID);

                            foreach (var x in gl)
                            {
                                x.ContractID = temp.ID;
                                _IGeneralLedgerService.Update(x);
                            }
                            int typeID = 0;
                            if (gl.Count > 0) typeID = gl[0].TypeID;
                            if (new int[] { 320, 321, 322, 323, 324, 325 }.Any(x => x == typeID))
                            {
                                SAInvoiceDetail md = _ISAInvoiceDetailService.Getbykey(item.GLID);
                                md.ContractID = temp.ID;
                                _ISAInvoiceDetailService.Update(md);
                            }
                            else if (new int[] { 330, 340 }.Any(x => x == typeID))
                            {
                                SAReturnDetail sar = _ISAReturnDetailService.Getbykey(item.GLID);
                                sar.ContractID = temp.ID;
                                _ISAReturnDetailService.Update(sar);
                            }
                        }
                    }
                    //update mã hợp đồng vào chứng từ chi ghi sổ
                    if (uGridChungTuChi.Rows.Count > 0)
                    {
                        var listEmContractThucChi2 = uGridChungTuChi.DataSource as BindingList<EMContractSale>;
                        foreach (EMContractSale item in listEmContractThucChi2)
                        {
                            List<GeneralLedger> gl = _IGeneralLedgerService.GetGLByDetailID(item.GLID);
                            foreach (var x in gl)
                            {
                                x.ContractID = temp.ID;
                                _IGeneralLedgerService.Update(x);
                            }
                            int typeID = 0;
                            if (gl.Count > 0) typeID = gl[0].TypeID;
                            if (new int[] { 260, 261, 262, 263, 264 }.Any(x => x == typeID))
                            {
                                PPInvoiceDetail ppd = _IPPInvoiceDetailService.Getbykey(item.GLID);
                                ppd.ContractID = temp.ID;
                                _IPPInvoiceDetailService.Update(ppd);
                            }
                        }
                    }
                    _IEMContractDetailMGService.BeginTran();
                    if (uGridEMContractMG.Rows.Count > 0)
                    {
                        var listEMContractDetailMG2 = uGridEMContractMG.DataSource as BindingList<EMContractDetailMG>;
                        foreach (EMContractDetailMG item in listEMContractDetailMG2)
                        {
                            item.ContractID = temp.ID;
                            item.ID = Guid.NewGuid();
                            _IEMContractDetailMGService.CreateNew(item);
                        }
                    }
                    _IEMContractDetailMGService.CommitTran();
                    _select = temp;
                }
                else
                {
                    var listEmContractAttDb = _IEMContractAttachmentService.GetEMContractAttachmentByContractID(temp.ID);
                    var listEmContractDetDb = _IEMContractDetailPaymentService.getEMContractDetailPaymentbyIDEMContract(temp.ID);
                    var listEmContractMaterialGood = _IEMContractDetailMGService.GetEMContractDetailMGByContractID(temp.ID);
                    var listEmContractThucThu1 = _IGeneralLedgerService.GetEMContractSaleDetailByContractID((Guid)temp.ID, typeThucThu);
                    var listEmContractThucChi1 = _IGeneralLedgerService.GetEMContractSaleThucChiDetailByContractID((Guid)temp.ID, typeThucChi);
                    var listSAOrderDetail = _ISAOrderDetailService.GetSAOrderDetailByContractID((Guid)temp.ID);
                    temp = ObjandGUI(_select, true);
                    if (listSAOrderDetail.Count > 0)
                    {
                        foreach (var item in listSAOrderDetail)
                        {
                            SAOrderDetail saorderdeail = _ISAOrderDetailService.Getbykey(item.ID);
                            //saorderdeail.SAOrderID = null;
                            saorderdeail.ContractID = null;
                            _ISAOrderDetailService.Update(saorderdeail);
                        }
                    }

                    if (cbbOrderID.Text != "")
                    {
                        foreach (var item1 in cbbOrderID.CheckedRows)
                        {
                            SAOrder saorder = (SAOrder)item1.ListObject;
                            List<SAOrderDetail> lstsaorderdeail = null;
                            try
                            {
                                lstsaorderdeail = _ISAOrderDetailService.GetSAOrderDetailBySAOrderCode(saorder.ID, temp.ID);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.ToString());
                            }
                            if (lstsaorderdeail.Count == 0)
                                lstsaorderdeail = _ISAOrderDetailService.GetSAOrderDetailBySAOrderCode(saorder.ID, null);
                            if (lstsaorderdeail.Count > 0)
                            {
                                foreach (var item in lstsaorderdeail)
                                {
                                    item.ContractID = temp.ID;
                                    _ISAOrderDetailService.Update(item);
                                }
                            }
                        }
                    }

                    if (listEmContractAttDb.Count > 0)
                    {
                        foreach (var item in listEmContractAttDb)
                        {
                            _IEMContractAttachmentService.Delete(item);
                        }
                    }

                    if (listEMContractAttachment.Count > 0)
                    {
                        foreach (var item in listEMContractAttachment)
                        {
                            item.EMContractID = temp.ID;
                            _IEMContractAttachmentService.CreateNew(item);
                        }
                    }

                    if (listEmContractDetDb.Count > 0)
                    {
                        foreach (var item in listEmContractDetDb)
                        {
                            _IEMContractDetailPaymentService.Delete(item);
                        }
                    }

                    if (listEMContractDetailPayment.Count > 0)
                    {
                        foreach (EMContractDetailPayment item in listEMContractDetailPayment)
                        {
                            if (item.ID == null || item.ID == Guid.Empty)
                                item.ID = Guid.NewGuid();
                            if (item.EMContractID == null || item.EMContractID == Guid.Empty)
                                item.EMContractID = temp.ID;
                            if (item.AmountOriginal == null)
                            {
                                item.AmountOriginal = 0;
                            }
                            item.Amount = item.AmountOriginal;
                            item.AmountBeforeCancel = item.AmountOriginal;
                            _IEMContractDetailPaymentService.CreateNew(item);
                        }
                    }

                    if (listEmContractThucThu1.Count > 0)
                    {
                        foreach (var item in listEmContractThucThu1)
                        {
                            List<GeneralLedger> gl = _IGeneralLedgerService.GetGLByDetailID(item.GLID);
                            foreach (var x in gl)
                            {
                                x.ContractID = null;
                                _IGeneralLedgerService.Update(x);
                            }
                            int typeID = 0;
                            if (gl.Count > 0) typeID = gl[0].TypeID;
                            if (new int[] { 320, 321, 322, 323, 324, 325 }.Any(x => x == typeID))
                            {
                                SAInvoiceDetail md = _ISAInvoiceDetailService.Getbykey(item.GLID);
                                md.ContractID = null;
                                _ISAInvoiceDetailService.Update(md);
                            }
                            else if (new int[] { 330, 340 }.Any(x => x == typeID))
                            {
                                SAReturnDetail sar = _ISAReturnDetailService.Getbykey(item.GLID);
                                sar.ContractID = null;
                                _ISAReturnDetailService.Update(sar);
                            }
                        }
                    }

                    if (uGridChungTuThu.Rows.Count > 0)
                    {
                        var listEmContractThucThu2 = uGridChungTuThu.DataSource as BindingList<EMContractSale>;
                        foreach (EMContractSale item in listEmContractThucThu2)
                        {
                            List<GeneralLedger> gl = _IGeneralLedgerService.GetGLByDetailID(item.GLID);
                            foreach (var x in gl)
                            {
                                x.ContractID = temp.ID;
                                _IGeneralLedgerService.Update(x);
                            }
                            int typeID = 0;
                            if (gl.Count > 0) typeID = gl[0].TypeID;
                            if (new int[] { 320, 321, 322, 323, 324, 325 }.Any(x => x == typeID))
                            {
                                SAInvoiceDetail md = _ISAInvoiceDetailService.Getbykey(item.GLID);
                                md.ContractID = temp.ID;
                                _ISAInvoiceDetailService.Update(md);
                            }
                            else if (new int[] { 330, 340 }.Any(x => x == typeID))
                            {
                                SAReturnDetail sar = _ISAReturnDetailService.Getbykey(item.GLID);
                                sar.ContractID = temp.ID;
                                _ISAReturnDetailService.Update(sar);
                            }
                        }
                    }
                    if (listEmContractThucChi1.Count > 0)
                    {
                        foreach (var item in listEmContractThucChi1)
                        {
                            List<GeneralLedger> gl = _IGeneralLedgerService.GetGLByDetailID(item.GLID);
                            foreach (var x in gl)
                            {
                                x.ContractID = null;
                                _IGeneralLedgerService.Update(x);
                            }
                            int typeID = 0;
                            if (gl.Count > 0) typeID = gl[0].TypeID;
                            if (new int[] { 260, 261, 262, 263, 264 }.Any(x => x == typeID))
                            {
                                PPInvoiceDetail ppd = _IPPInvoiceDetailService.Getbykey(item.GLID);
                                ppd.ContractID = null;
                                _IPPInvoiceDetailService.Update(ppd);
                            }

                        }
                    }

                    if (uGridChungTuChi.Rows.Count > 0)
                    {
                        var listEmContractThucChi2 = uGridChungTuChi.DataSource as BindingList<EMContractSale>;
                        foreach (EMContractSale item in listEmContractThucChi2)
                        {
                            List<GeneralLedger> gl = _IGeneralLedgerService.GetGLByDetailID(item.GLID);
                            foreach (var x in gl)
                            {
                                x.ContractID = temp.ID;
                                _IGeneralLedgerService.Update(x);
                            }
                            int typeID = 0;
                            if (gl.Count > 0) typeID = gl[0].TypeID;
                            if (new int[] { 260, 261, 262, 263, 264 }.Any(x => x == typeID))
                            {
                                PPInvoiceDetail ppd = _IPPInvoiceDetailService.Getbykey(item.GLID);
                                ppd.ContractID = temp.ID;
                                _IPPInvoiceDetailService.Update(ppd);
                            }

                        }
                    }

                    if (listEmContractMaterialGood.Count > 0)
                    {
                        foreach (var item in listEMContractDetailMG)
                        {
                            _IEMContractDetailMGService.Delete(item);
                        }
                    }
                    _IEMContractDetailMGService.BeginTran();
                    if (uGridEMContractMG.Rows.Count > 0)
                    {
                        var listEMContractDetailMG2 = uGridEMContractMG.DataSource as BindingList<EMContractDetailMG>;
                        foreach (EMContractDetailMG item in listEMContractDetailMG2)
                        {
                            if (item.ContractID == null || item.ContractID == Guid.Empty)
                                item.ContractID = temp.ID;
                            if (item.ID == Guid.Empty || item.ID == null)
                                item.ID = Guid.NewGuid();
                            _IEMContractDetailMGService.CreateNew(item);
                        }
                    }
                    _IEMContractDetailMGService.CommitTran();
                    _IEmContractService.Update(temp);
                }
                _IEmContractService.CommitTran();
                MSG.Information("Lưu thành công");
                Utils.ClearCacheByType<EMContract>();
                Utils.ClearCacheByType<EMContractDetailMG>();
                _select = Utils.IEMContractService.Getbykey(_select.ID);
                _id = temp.ID;
                btnGhidoanhso.Enabled = true;
                btnBoghidoanhso.Enabled = true;
                ObjandGUI(_select, false);
                them = false;
            }
            catch (Exception ex)
            {
                MSG.Error("Lưu không thành công!");
                _IEmContractService.RolbackTran();
                _IEMContractDetailMGService.RolbackTran();
            }
            //Reset();
        }

        public virtual void Reset()
        {
            Utils.ClearCache();
            IEMContractService testInvoke = IoC.Resolve<IEMContractService>();
            PropertyInfo propId = _select.GetType().GetProperty("ID", BindingFlags.Public | BindingFlags.Instance);
            if (null != propId && propId.CanRead)
            {
                object temp = propId.GetValue(_select, null);
                if (temp is Guid)
                {
                    var currentId = (Guid)temp;
                    try
                    {
                        _select = testInvoke.Getbykey(currentId);
                        testInvoke.UnbindSession(_select);
                        _select = testInvoke.Getbykey(currentId);
                    }
                    catch (Exception ex)
                    {
                        MSG.Warning(resSystem.MSG_Error_13);
                        //Close();
                    }
                }
            }
        }

        private bool checkerror()
        {
            bool kq = true;
            if ((txtAmount.Value.ToDouble() == (double)0) || (txtAmount.Text == "0") || string.IsNullOrEmpty(txtAmount.Value.ToString()))
            {
                MSG.Warning("Không được để trống giá trị Hợp đồng");
                return false;
            }

            if (((cbbCurrencyID.Text != "VND") && (txtAmountOriginal.Text == "0")) || txtAmountOriginal.Text == "0" || txtAmountOriginal.Value.ToDouble() == (double)0 || string.IsNullOrEmpty(txtAmountOriginal.Value.ToString()))
            {
                MSG.Warning("Không được để trống giá trị Hợp đồng");
                return false;
            }
            if (txtEMContractCode.Text == "")
            {
                MSG.Warning("Không được để trống số Hợp đồng");
                utEMContractSale.SelectedTab = utEMContractSale.Tabs[0];
                txtEMContractCode.Focus();
                return false;
            }
            if ((cbbContractState.Text == "") || (cbbContractState.Value == null))
            {
                MSG.Warning("Không được để trống tình trạng hợp đồng");
                utEMContractSale.SelectedTab = utEMContractSale.Tabs[0];
                cbbContractState.Focus();
                return false;
            }
            if (((cbbContractState.Text == "Hủy bỏ") && (txtClosedDate.Value == null)) || ((cbbContractState.Text == "Thanh lý") && (txtClosedDate.Value == null)) || ((cbbContractState.Text == "Kết thúc") && (txtClosedDate.Value == null)))
            {
                MSG.Warning(string.Format("Ngày {0} hợp đồng không được bỏ trống", cbbContractState.Text));
                utEMContractSale.SelectedTab = utEMContractSale.Tabs[0];
                txtClosedDate.Focus();
                return false;
            }
            List<EMContract> lstemcontract = _IEmContractService.GetAllContractSale();
            foreach (var item in lstemcontract)
            {
                if (item.Code == txtEMContractCode.Text && them)
                {
                    if (MSG.Question("Mã hợp đồng này đã tồn tại, bạn có muốn hệ thống tự sinh mã mới không") == DialogResult.Yes)
                    {
                        txtEMContractCode.Text = Utils.TaoMaChungTu(_IGenCodeService.getGenCode(86));
                        _IGenCodeService.UpdateGenCodeCatalog(86, txtEMContractCode.Text);
                    }
                    else
                    {
                        utEMContractSale.SelectedTab = utEMContractSale.Tabs[0];
                        txtEMContractCode.Focus();
                    }
                    return false;
                }
            }

            if (cbbContractEmployeeID.Text.Trim() != "")
            {
                List<AccountingObject> listAccountingObject = _IAccountingObjectService.GetListAccountingObjectByEmployee(true).Where(x => x.IsActive).ToList();
                if (!listAccountingObject.Exists(x => x.AccountingObjectCode == cbbContractEmployeeID.Text.ToString()))
                {
                    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                    return false;
                }
            }
            if (cbbAccountingObjectID.Text.Trim() == "")
            {
                MSG.Warning("Không được để trống Khách hàng!");
                return false;
            }
            if (cbbAccountingObjectID.Text.Trim() != "")
            {
                List<AccountingObject> listAccountingObject1 = Utils.ListAccountingObject.Where(x => (x.ObjectType == 0 || x.ObjectType == 2) && x.IsActive).ToList();
                if (!listAccountingObject1.Exists(x => x.AccountingObjectCode == cbbAccountingObjectID.Text.ToString()))
                {
                    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                    return false;
                }
            }

            List<ContractState> listContractState = _IContractStateService.GetAll();
            if (!listContractState.Exists(x => x.ContractStateName == cbbContractState.Text.ToString()) && cbbContractState.Text != "")
            {
                MSG.Warning("Dữ liệu không có trong danh mục. Đề nghị điều chỉnh lại");
            }

            List<EMContract> lstEMContract = _IEmContractService.GetAllContractSale().Where(x => x.IsProject == true).ToList();
            if (cbbDA.Text.Trim() != "")
            {
                if (txtEMContractCode.Text != "")
                {
                    if (cbbDA.Text.Trim() == txtEMContractCode.Text)
                    {
                        MSG.Warning("Mã dự án không được trùng với số hợp đồng. Đề nghị điều chỉnh lại");
                        return false;
                    }
                }
                if (!lstEMContract.Exists(x => x.ID == (Guid)cbbDA.Value))
                {
                    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                    return false;
                }
            }

            List<Department> lstDepartment = Utils.ListDepartment.ToList();
            if (cbbDepartmentID.Text != "")
            {
                if (!lstDepartment.Exists(x => x.ID == (Guid)cbbDepartmentID.Value))
                {
                    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                    return false;
                }
            }
            foreach (UltraGridRow r in cbbOrderID.CheckedRows)
            {
                var md = (SAOrder)r.ListObject;
                if ((Guid)cbbAccountingObjectID.Value != md.AccountingObjectID)
                {
                    MSG.Warning("Bạn phải chọn số đơn hàng thuộc cùng mã khách hàng!");
                    return false;
                }
            }

            return kq;
        }
        #endregion

        #region Tab4

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddData frm = new AddData();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog(this);
            if (frm.isclose)
            {
                listEMContractAttachment.Add(frm.input);
                uGridEMContractAttachment.DataSource = listEMContractAttachment.ToArray();
                Utils.ConfigGrid(uGridEMContractAttachment, TextMessage.ConstDatabase.EMContractAttachment_TableName);
                checkuGridEMContractAttachment();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (uGridEMContractAttachment.Selected.Rows.Count > 0)
            {
                EMContractAttachment temp = uGridEMContractAttachment.Selected.Rows[0].ListObject as EMContractAttachment;
                listEMContractAttachment.Remove(temp);
                AddData frm = new AddData(temp);
                frm.ShowDialog(this);
                listEMContractAttachment.Add(frm.input);
                uGridEMContractAttachment.DataSource = listEMContractAttachment.ToArray();
                uGridEMContractAttachment.Update();
                checkuGridEMContractAttachment();
            }
        }

        public void checkuGridEMContractAttachment()
        {
            if (uGridEMContractAttachment.Rows.Count > 0)
            {
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
            }
            else
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (uGridEMContractAttachment.Selected.Rows.Count > 0)
            {
                EMContractAttachment temp = uGridEMContractAttachment.Selected.Rows[0].ListObject as EMContractAttachment;
                if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, "tệp đính kèm")) == DialogResult.Yes)
                {
                    listEMContractAttachment.Remove(temp);
                    uGridEMContractAttachment.DataSource = listEMContractAttachment.ToArray();
                    uGridEMContractAttachment.Update();
                    checkuGridEMContractAttachment();
                }
            }
        }

        #endregion Tab4

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            if (utEMContractSale.Tabs[2].Active)
            {
                if (uGridEMContractMG.ActiveRow != null || uGridEMContractMG.ActiveCell != null)
                {
                    UltraGridRow row = null;
                    if (uGridEMContractMG.ActiveCell != null || uGridEMContractMG.ActiveRow != null)
                    {
                        row = uGridEMContractMG.ActiveCell != null ? uGridEMContractMG.ActiveCell.Row : uGridEMContractMG.ActiveRow;
                    }
                    else
                    {
                        if (uGridEMContractMG.Rows.Count > 0) row = uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1];
                    }
                    if (row != null) row.Delete(false);
                    uGridEMContractMG.Update();
                }
            }
            if (utEMContractSale.Tabs[1].Active)
            {
                if (uGridChi.ActiveRow != null || uGridChi.ActiveCell != null)
                {
                    UltraGridRow row = null;
                    if (uGridChi.ActiveCell != null || uGridChi.ActiveRow != null)
                    {
                        row = uGridChi.ActiveCell != null ? uGridChi.ActiveCell.Row : uGridChi.ActiveRow;
                    }
                    else
                    {
                        if (uGridChi.Rows.Count > 0) row = uGridChi.Rows[uGridChi.Rows.Count - 1];
                    }
                    if (row != null) row.Delete(false);
                    uGridChi.Update();
                }
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            uGridChi.AddNewRow4Grid();
            uGridEMContractMG.AddNewRow4Grid();
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
        }

        #region Tab3 controls event

        private void cbbDA_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbDA.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            EMContract emContract = e.Row.ListObject as EMContract;
            if (!_firstItem)
            {
                txtName.Text = emContract.Name;
            }
        }

        private void cbbDA_EditorButtonClick(object sender, EditorButtonEventArgs e)
        {
            new FEMContractSaleDetail().ShowDialog(this);
            listEMContract = _IEmContractService.GetAllProject();
            cbbDA.DataSource = listEMContract;
            Utils.ConfigGrid(cbbDA, ConstDatabase.FProject_TableName);
            cbbDA.ValueMember = "ID";
            cbbDA.DisplayMember = "Code";
            Utils.ClearCacheByType<EMContract>();
        }

        private void cbbAccountingObjectBankAccount_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbAccountingObjectBankAccount.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
            AccountingObjectBankAccount accountingObjectBankAccoount = e.Row.ListObject as AccountingObjectBankAccount;
            txtAccountingObjectBankName.Text = accountingObjectBankAccoount.BankName;
        }

        #endregion Tab3 controls event

        #region Tab2 controls event

        private void btnChungTuThu_Click(object sender, EventArgs e)
        {
            FVouchers frm = new FVouchers();
            frm._isPayment = false;
            frm.Text = "Chứng từ thu";
            frm.ShowDialog(this);
            if (frm._isClose)
            {
                uGridChungTuThu.DataSource = new BindingList<EMContractSale>(frm.lstEMContractSale);
                Utils.ConfigGrid(uGridChungTuThu, "Thuc Thu", GetColumnThucthu());
                lblFromDate.Text = frm.timeStart.ToString("dd/MM/yyyy");
                lblToDate.Text = frm.timeEnd.ToString("dd/MM/yyyy");
            }
        }

        private void btnChungTuChi_Click(object sender, EventArgs e)
        {
            FVouchers frm = new FVouchers();
            frm._isPayment = true;
            frm.Text = "Chứng từ chi";
            frm.ShowDialog(this);
            if (frm._isClose)
            {
                uGridChungTuChi.DataSource = new BindingList<EMContractSale>(frm.lstEMContractSale);
                Utils.ConfigGrid(uGridChungTuChi, "Thuc Chi", GetColumnsThucChi());
                lblFromDate1.Text = frm.timeStart.ToString("dd/MM/yyyy");
                lblToDate1.Text = frm.timeEnd.ToString("dd/MM/yyyy");
            }
        }

        private void cbbDepartmentID_ItemNotInList(object sender, ValidationErrorEventArgs e)
        {
            List<Department> lstDepartment = Utils.ListDepartment.ToList();
            if ((cbbDepartmentID.Value == null) || (cbbDepartmentID.Value == ""))
            { return; }
            else
            {
                if (!lstDepartment.Exists(x => x.ID == (Guid)cbbDepartmentID.Value) && cbbDepartmentID.Text != "")
                {
                    MSG.Warning("Dữ liệu không có trong danh mục.Đề nghị điều chỉnh lại");
                }
            }
        }

        #endregion Tab2 controls event

        #region template

        private List<TemplateColumn> GetColumnThucthu()
        {
            return new List<TemplateColumn>
           {
               new TemplateColumn
               {
                   ColumnCaption = "Loại chứng từ",
                   ColumnName = "TypeName",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 0,
                   IsReadOnly = true
                },
                 new TemplateColumn
               {
                   ColumnCaption = "Ngày chứng từ",
                   ColumnName = "Date",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 1,
                   IsReadOnly = true
                   },
                    new TemplateColumn
               {
                   ColumnCaption = "Số chứng từ",
                   ColumnName = "No",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 2,
                   IsReadOnly = true
                   },
                    new TemplateColumn
               {
                   ColumnCaption = "Diễn giải",
                   ColumnName = "Reason",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 3,
                   IsReadOnly = true
                   },

                new TemplateColumn
               {
                   ColumnCaption = "Số tiền",
                   ColumnName = "Amount",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 4,
                   IsReadOnly = true
                   },
           };
        }

        private void cbbOrderID_RowSelected(object sender, RowSelectedEventArgs e)
        {
            cbbOrderID.UpdateData();
            if (e.Row == null) return;
            if (e.Row.ListObject == null) return;
        }

        private List<TemplateColumn> GetColumnsThucChi()
        {
            return new List<TemplateColumn>
            {
                new TemplateColumn()
                {
                   ColumnCaption = "Loại chứng từ",
                   ColumnName = "TypeName",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   IsVisibleCbb = false,
                   VisiblePosition = 0,
                   IsReadOnly = true
                },
                   new TemplateColumn
               {
                   ColumnCaption = "Ngày chứng từ",
                   ColumnName = "Date",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   IsVisibleCbb = false,
                   VisiblePosition = 1,
                   IsReadOnly = true
                   },
                   new TemplateColumn
               {
                   ColumnCaption = "Diễn giải",
                   ColumnName = "Reason",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 4,
                   IsReadOnly = true
                   },
                   new TemplateColumn
               {
                   ColumnCaption = "Số chứng từ",
                   ColumnName = "No",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 5,
                   IsReadOnly = true
                   },
                       new TemplateColumn
               {
                   ColumnCaption = "Số tiền",
                   ColumnName = "Amount",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 6,
                   IsReadOnly = true
                   },
            };
        }

        #endregion template

        private string FormatDate(string input, string output, string[] formats)
        {
            var c = CultureInfo.CurrentCulture;
            var s = DateTimeStyles.None;
            var result = default(DateTime);

            if (DateTime.TryParseExact(input, formats, c, s, out result))
                return result.ToString(output);

            throw new FormatException("Unhandled input format: " + input);
        }

        private void txtSignedDate_ValueChanged(object sender, EventArgs e)
        {
            //if (_firstItem)
            //{
            //    DateTime dt;
            //    if (txtSignedDate.Text != "" && DateTime.TryParseExact(txtSignedDate.Text, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
            //    {
            //        txtName.Text = string.Format("Hợp đồng số {0} ký ngày {1} về việc :", txtEMContractCode.Text, FormatDate(txtSignedDate.Text, "dd/MM/yyyy", formats));
            //    }
            //}
            //else
            //    txtName.Text = "";
        }

        private void uGridChi_AfterCellUpdate(object sender, CellEventArgs e)
        {

            if (e.Cell.Column.Key.Equals("Methods"))
            {
                if (e.Cell.Row.Cells["Methods"].Value.ToString() != "0")
                {
                    e.Cell.Row.Cells["PercentAmount"].Value = 0;
                    e.Cell.Row.Cells["PercentAmount"].Activation = Activation.NoEdit;
                }
                else e.Cell.Row.Cells["PercentAmount"].Activation = Activation.AllowEdit;
            }
            if (e.Cell.Column.Key.Equals("PercentAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["PercentAmount"].Value.ToString()) != 0 && txtAmountOriginal.Value.ToString() != "")
                {
                    e.Cell.Row.Cells["AmountOriginal"].Value = Decimal.Parse(txtAmountOriginal.Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["PercentAmount"].Value.ToString()) / 100;
                }
                else
                {
                    e.Cell.Row.Cells["AmountOriginal"].Value = 0;
                }
            }
            if (e.Cell.Column.Key.Equals("DepartmentID"))
            {
                if (Utils.ListDepartment.FirstOrDefault(x => x.ID == (Guid)e.Cell.Row.Cells["DepartmentID"].Value).IsParentNode)
                {
                    Utils.NotificationCell(uGridChi, e.Cell, "Bạn không được chọn chỉ tiêu tổng hợp");
                }
                else Utils.RemoveNotificationCell(uGridChi, e.Cell);
            }
            if (e.Cell.Column.Key.Equals("ExpenseItemID"))
            {
                if (Utils.ListExpenseItem.FirstOrDefault(x => x.ID == (Guid)e.Cell.Row.Cells["ExpenseItemID"].Value).IsParentNode)
                {
                    Utils.NotificationCell(uGridChi, e.Cell, "Bạn không được chọn chỉ tiêu tổng hợp");
                }
                else Utils.RemoveNotificationCell(uGridChi, e.Cell);
            }
        }

        private void cbbOrderID_ValueChanged(object sender, EventArgs e)
        {
            List<EMContractDetailMG> lstEMContractDetailMG = new List<EMContractDetailMG>();
            if (_select != null)
                lstEMContractDetailMG = Utils.ListEMContractDetailMG.Where(x => x.ContractID == _select.ID).ToList();
            List<Guid> lstMaterialGoodsID = new List<Guid>();
            List<MaterialGoods> listAllMaterialGoods = Utils.ListMaterialGoods.ToList();
            foreach (var item in lstEMContractDetailMG)
            {
                lstMaterialGoodsID.Add((Guid)item.MaterialGoodsID);
            }

            uGridEMContractMG.DataSource = new BindingList<EMContractDetailMG>(lstEMContractDetailMG);
            uGridEMContractMG.UpdateData();
            foreach (UltraGridRow r in cbbOrderID.CheckedRows)
            {
                List<SAOrderDetail> lstSAOrderDetail = Utils.ListSAOrderDetail.Where(x => x.SAOrderID == (Guid)r.Cells["ID"].Value).ToList();
                if (lstSAOrderDetail.Count > 0)
                {
                    foreach (var item in lstSAOrderDetail)
                    {
                        MaterialGoods material = listAllMaterialGoods.FirstOrDefault(o => o.ID == (Guid)item.MaterialGoodsID);
                        if (!lstMaterialGoodsID.Contains(material.ID))
                        {
                            uGridEMContractMG.AddNewRow4Grid();
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["SAOrderCode"].Value = r.Cells["No"].Value;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsID"].Activation = Activation.NoEdit;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["UnitPrice"].Value = item.UnitPrice;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["Quantity"].Value = item.Quantity;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["TotalAmount"].Value = item.Amount.FormatNumberic(ConstDatabase.Format_TienVND);
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["DiscountRate"].Value = item.DiscountRate;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["DiscountAmount"].Value = item.DiscountAmount;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["VATRate"].Value = item.VATRate;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["VATAmount"].Value = item.VATAmount.FormatNumberic(ConstDatabase.Format_TienVND);
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsID"].Value = material.ID;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsCode"].Value = material.MaterialGoodsCode;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["MaterialGoodsName"].Value = material.MaterialGoodsName;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["Unit"].Value = material.Unit;
                            uGridEMContractMG.Rows[uGridEMContractMG.Rows.Count - 1].Cells["QuantityReceipt"].Value = item.QuantityReceipt;
                        }
                    }
                }
            }
            if (cbbOrderID.CheckedRows.Count > 0)
            {
                var md = cbbOrderID.CheckedRows[0].ListObject as SAOrder;
                txtDeliverDate.Value = md.DeliveDate;
                txtDueDate.Value = md.DueDate;
            }
            else
            {
                txtDeliverDate.Value = null;
                txtDueDate.Value = null;
            }
            txtAmountOriginal.Value = lstEMContractDetailMG.Count > 0 ? ((decimal)lstEMContractDetailMG.Sum(x => x.AmountOriginal) - (decimal)lstEMContractDetailMG.Sum(x => x.DiscountAmountOriginal) + (decimal)lstEMContractDetailMG.Sum(x => x.VATAmountOriginal)) : 0;
            check1 = true;
        }

        private void uGridEMContractMG_AfterCellUpdate_1(object sender, CellEventArgs e)
        {

            if (e.Cell.Column.Key.Equals("MaterialGoodsID"))
            {
                if (!e.Cell.Row.Cells["MaterialGoodsID"].IsFilterRowCell)
                {
                    var combo = (UltraCombo)e.Cell.ValueListResolved;
                    decimal quantity = 0;
                    if (combo.SelectedRow != null)
                    {
                        var model = (MaterialGoods)combo.SelectedRow.ListObject;
                        if (model != null)
                        {
                            e.Cell.Row.Cells["MaterialGoodsName"].Value = model.MaterialGoodsName;
                            e.Cell.Row.Cells["Unit"].Value = model.Unit;
                            e.Cell.Row.Cells["VATRate"].Value = model.TaxRate;
                            if (_select != null)
                                quantity = _ISAInvoiceDetailService.GetSAInvoiceQuantity(model.ID, _select.ID);
                            e.Cell.Row.Cells["QuantityReceipt"].Value = quantity;
                        }
                    }
                    if (e.Cell.Row.Cells["MaterialGoodsID"].Text == "")
                    {
                        e.Cell.Row.Cells["MaterialGoodsName"].Value = "";
                        e.Cell.Row.Cells["Unit"].Value = "";
                        e.Cell.Row.Cells["QuantityReceipt"].Value = 0;
                    }
                }
            }
            if (e.Cell.Column.Key.Equals("UnitPriceOriginal"))
            {
                decimal ex = 1;
                try
                {
                    ex = Convert.ToDecimal(txtExchangeRate.Text);
                }
                catch (Exception)
                {
                    ex = 1;
                }
                e.Cell.Row.Cells["UnitPrice"].Value = (decimal)e.Cell.Value * ex;
                if (Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["TotalAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                }
            }
            if (e.Cell.Column.Key.Equals("UnitPrice"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["TotalAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                }
            }

            if (e.Cell.Column.Key.Equals("Quantity"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["TotalAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPrice"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                    e.Cell.Row.Cells["TotalAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["UnitPriceOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["Quantity"].Value.ToString()) : 0;
                }
            }

            if (e.Cell.Column.Key.Equals("DiscountRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["DiscountAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["DiscountAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
            }

            if (e.Cell.Column.Key.Equals("VATRate"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmount"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmount"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmountOriginal"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmountOriginal"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
            }

            if (e.Cell.Column.Key.Contains("TotalAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["DiscountAmount"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmount"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmount"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
            }
            if (e.Cell.Column.Key.Contains("TotalAmountOriginal"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0)
                {

                    e.Cell.Row.Cells["DiscountAmountOriginal"].Value = (Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) != 0) ? Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) * Decimal.Parse(e.Cell.Row.Cells["DiscountRate"].Value.ToString()) / 100 : 0;
                }
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmountOriginal"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2) ?
                        (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmountOriginal"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }
            }
            if (e.Cell.Column.Key.Contains("DiscountAmount"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {
                    e.Cell.Row.Cells["VATAmount"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2)
                        ? (Decimal.Parse(e.Cell.Row.Cells["TotalAmount"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmount"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;

                }

            }
            if (e.Cell.Column.Key.Contains("DiscountAmountOriginal"))
            {
                if (Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) != 0)
                {

                    e.Cell.Row.Cells["VATAmountOriginal"].Value = ((decimal)e.Cell.Row.Cells["VATRate"].Value != 0 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -1 && (decimal)e.Cell.Row.Cells["VATRate"].Value != -2)
                       ? (Decimal.Parse(e.Cell.Row.Cells["TotalAmountOriginal"].Value.ToString()) - Decimal.Parse(e.Cell.Row.Cells["DiscountAmountOriginal"].Value.ToString())) * Decimal.Parse(e.Cell.Row.Cells["VATRate"].Value.ToString()) / 100 : 0;
                }

            }
        }

        private void uGridChi_Error(object sender, ErrorEventArgs e)
        {
            UltraGridCell cell = uGridChi.ActiveCell;
            if (uGridChi.ActiveCell == null) return;
            DateTimeEditor dateTimeEditor = cell.EditorResolved as DateTimeEditor;
            if (dateTimeEditor != null && false == dateTimeEditor.IsValid && cell.Column.Key == "RecordDate")
            {
                MSG.Warning("Lỗi định dạng ngày ghi nhận");
            }
            if (cell.Column.Key.Equals("DepartmentID"))
            {
                int count = Utils.ListDepartment.Count(x => x.DepartmentCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");

                }
            }
            if (cell.Column.Key.Equals("ExpenseItemID"))
            {
                int count = Utils.ListExpenseItem.Count(x => x.ExpenseItemCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");
                }
            }
            e.Cancel = true;
        }

        private void uGridEMContractMG_Error(object sender, ErrorEventArgs e)
        {
            UltraGridCell cell = uGridEMContractMG.ActiveCell;
            if (uGridEMContractMG.ActiveCell == null) return;
            if (cell.Column.Key.Equals("MaterialGoodsID"))
            {
                int count = Utils.ListMaterialGoods.Count(x => x.MaterialGoodsCode == cell.Text);
                if (count == 0)
                {
                    MSG.Error("Dữ liệu không có trong danh mục");

                }
            }
            e.Cancel = true;
        }

        private void uGridEMContractMG_SummaryValueChanged(object sender, SummaryValueChangedEventArgs e)
        {
            if (uGridEMContractMG.Rows.Count > 0)
                txtAmountOriginal.Text = (Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumAmountOriginal"].Value.ToString()) - Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumDiscountAmountOriginal"].Value.ToString()) + Decimal.Parse(uGridEMContractMG.Rows.SummaryValues["sumVATAmountOriginal"].Value.ToString())).ToString();
        }

        private void cbbOrderID_AfterCloseUp(object sender, EventArgs e)
        {
            if (check1)
            {
                check1 = false;
                if (cbbOrderID.Text == "")
                {
                    cbbAccountingObjectID.Value = null;
                    txtAccountingObjectName.Text = null;
                    txtAccountingObjectAddress.Text = null;
                    cbbAccountingObjectBankAccount.DataSource = new List<AccountingObjectBankAccount>();
                    txtAccountingObjectFax.Text = null;
                    txtAccountingObjectTel.Text = null;
                    return;
                }
                var model = (SAOrder)cbbOrderID.CheckedRows[0].ListObject;
                if (cbbAccountingObjectID.Text == null || cbbAccountingObjectID.Text == "")
                {
                    cbbAccountingObjectID.Value = model.AccountingObjectID;
                }
                foreach (UltraGridRow r in cbbOrderID.CheckedRows)
                {
                    var md = (SAOrder)r.ListObject;
                    if ((Guid)cbbAccountingObjectID.Value != md.AccountingObjectID)
                    {
                        MSG.Warning("Bạn phải chọn số đơn hàng thuộc cùng mã khách hàng!");
                        return;
                    }
                }
            }

        }

        private void cbbAccountingObjectID_AfterCloseUp(object sender, EventArgs e)
        {
            //add by cuongpv 
            if (cbbAccountingObjectID.Text == "")
            {
                if (_select != null)
                    cbbOrderID.DataSource = _ISAOrderService.GetOrderByContract(_select.ID);
                else
                    cbbOrderID.DataSource = _ISAOrderService.GetAllOrderByNoAndNotInContract(Guid.Empty);
                return;
            }
            //end add by cuongpv
            if (check2)
            {
                check2 = false;
                AccountingObject accountingObject;
                //if (cbbAccountingObjectID.Text == "") return;//comment by cuongpv
                try
                {
                    accountingObject = Utils.ListAccountingObject.FirstOrDefault(x => x.ID == (Guid)cbbAccountingObjectID.Value);
                }
                catch
                {
                    cbbOrderID.DataSource = _ISAOrderService.GetAllOrderByNoAndNotInContract(Guid.Empty);
                    return;
                }
                if (cbbOrderID.Text == "") cbbOrderID.DataSource = _ISAOrderService.GetAllOrderByNoAndNotInContract((Guid)cbbAccountingObjectID.Value);
                else
                {
                    foreach (UltraGridRow r in cbbOrderID.CheckedRows)
                    {
                        var model = (SAOrder)r.ListObject;
                        if (model.AccountingObjectID != accountingObject.ID)
                        {
                            MSG.Warning("Bạn phải chọn số đơn hàng thuộc cùng mã khách hàng!");
                            return;
                        }
                    }
                }

            }
        }
        private void cbbAccountingObjectID_ValueChanged(object sender, EventArgs e)
        {
            //var ID = (Guid?)cbbAccountingObjectID.Value;            
            //cbbOrderID.DataSource = _ISAOrderService.GetAllOrderByNoAndNotInContract(ID??Guid.Empty);
        }

        private void uGridChi_BeforeRowActivate(object sender, RowEventArgs e)
        {
            if (cbbDepartmentID.Value != null)
            {
                e.Row.Cells["DepartmentID"].Value = cbbDepartmentID.Value;
            }
        }

        private void uGridEMContractMG_CellChange(object sender, CellEventArgs e)
        {

        }

        private void btnSua_Click(object sender, EventArgs e)
        {

            if (_select.RevenueType == true)
            {
                MSG.Warning("Hợp đồng đang ghi doanh số không được sửa");
            }
            else
            {
                btnSave.Enabled = true;
                btnXoa.Enabled = false;
                btnSua.Enabled = false;
                btnGhidoanhso.Enabled = false;
                btnBoghidoanhso.Enabled = false;
                btnClose.Enabled = true;
                optContract.Enabled = true;
                txtEMContractCode.ReadOnly = false;
                txtName.ReadOnly = false;
                cbbCurrencyID.ReadOnly = false;
                txtExchangeRate.ReadOnly = false;
                txtAmountOriginal.ReadOnly = false;
                txtAmount.ReadOnly = false;
                txtAmountBeforCancel.ReadOnly = false;
                txtSignedDate.ReadOnly = false;
                txtDeliverDate.ReadOnly = false;
                cbbDepartmentID.ReadOnly = false;
                cbbContractEmployeeID.Enabled = true;
                cbbAccountingObjectID.Enabled = true;
                cbbDA.ReadOnly = false;
                txtAccountingObjectTitle.ReadOnly = false;
                txtAccountingObjectName.ReadOnly = false;
                txtAccountingObjectSignerName.ReadOnly = false;
                txtAccountingObjectTel.ReadOnly = false;
                txtAccountingObjectFax.ReadOnly = false;
                txtAccountingObjectAddress.ReadOnly = false;
                cbbAccountingObjectBankAccount.ReadOnly = false;
                txtAccountingObjectBankName.ReadOnly = false;
                txtCompanyName.ReadOnly = false;
                cbbContractState.Enabled = true;
                txtClosedDate.ReadOnly = false;
                txtCompanyTel.ReadOnly = false;
                txtSignerName.ReadOnly = false;
                txtReason.ReadOnly = false;
                txtSignerTitle.ReadOnly = false;
                chkIsWatchForCostPrice.Enabled = true;
                //cbbCostSet.Enabled = false;
                chkIsBillPaid.Enabled = true;
                txtDeliverDate.ReadOnly = false;
                txtDueDate.ReadOnly = false;
                ultraDateTimeEditor4.ReadOnly = false;
                btnSave.Enabled = true;

                foreach (var item in uGridChi.DisplayLayout.Bands[0].Columns)
                {
                    item.CellActivation = Activation.AllowEdit;
                }

                foreach (var item in uGridEMContractMG.DisplayLayout.Bands[0].Columns)
                {
                    item.CellActivation = Activation.AllowEdit;
                }

                btnChungTuChi.Enabled = true;
                btnChungTuThu.Enabled = true;
                btnAdd.Enabled = true;
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void btnGhidoanhso_Click(object sender, EventArgs e)
        {
            _select.RevenueType = true;
            _IEmContractService.Update(_select);
            List<EMContractDetailMG> itemM = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)_select.ID);
            List<EMContractDetailRevenue> lst = new List<EMContractDetailRevenue>();
            try
            {
                _IEmContractService.BeginTran();
                if (itemM.Count > 0)
                {
                    foreach (var item in itemM)
                    {
                        EMContractDetailRevenue itemR = new EMContractDetailRevenue();
                        itemR.ID = Guid.NewGuid();
                        itemR.ContractID = (Guid)_select.ID;
                        itemR.DepartmentID = _select.DepartmentID;
                        itemR.ContractEmployeeID = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == (Utils.ListSAInvoiceDetail.FirstOrDefault(p => p.ContractID == _select.ID).SAInvoiceID)).EmployeeID;
                        itemR.MaterialGoodsID = item.MaterialGoodsID;
                        itemR.RevenueDate = DateTime.Now;
                        if (item.TotalAmount != 0)
                        {
                            itemR.RevenueAmount = Math.Round(item.TotalAmount - item.DiscountAmount + item.VATAmount, 0);
                        }
                        _IEMContractDetailRevenueService.CreateNew(itemR);
                        lst.Add(itemR);
                    }
                }
                else
                {
                    EMContractDetailRevenue itemR = new EMContractDetailRevenue();
                    itemR.ID = Guid.NewGuid();
                    itemR.ContractID = (Guid)_select.ID;
                    itemR.DepartmentID = _select.DepartmentID;
                    itemR.ContractEmployeeID = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == (Utils.ListSAInvoiceDetail.FirstOrDefault(p => p.ContractID == _select.ID).SAInvoiceID)).EmployeeID;
                    itemR.MaterialGoodsID = null;
                    itemR.RevenueDate = DateTime.Now;
                    itemR.RevenueAmount = _select.Amount;
                    itemR.OrderPriority = 0;
                    _IEMContractDetailRevenueService.CreateNew(itemR);
                    lst.Add(itemR);
                }
                _IEmContractService.CommitTran();
                //uGridGhiDoanhSo.DataSource = lst;
                //Utils.ConfigGrid(uGridGhiDoanhSo, ConstDatabase.EMContractDetailRevenue_TableName);
                //uGridGhiDoanhSo.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                //UltraGridBand band4 = uGridGhiDoanhSo.DisplayLayout.Bands[0];
                //foreach (UltraGridColumn column in band4.Columns)
                //{
                //    this.ConfigEachColumn4Grid(0, column, uGridGhiDoanhSo);
                //}
                //uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].LockColumn();
                //uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["DepartmentID"].LockColumn();
                //uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["ContractEmployeeID"].LockColumn();
                btnBoghidoanhso.Visible = true;
                btnGhidoanhso.Visible = false;
            }
            catch (Exception ex)
            {
                _IEmContractService.RolbackTran();
                lst.Clear();
            }
        }

        private void btnBoghidoanhso_Click(object sender, EventArgs e)
        {
            try
            {
                _IEmContractService.BeginTran();
                List<EMContractDetailRevenue> lstEMContractDetailRevenue = _IEMContractDetailRevenueService.GetByContractID((Guid)_select.ID);
                foreach (var itemR in lstEMContractDetailRevenue)
                {
                    _IEMContractDetailRevenueService.Delete(itemR);
                }

                EMContract itemE = _IEmContractService.Getbykey((Guid)_select.ID);
                itemE.RevenueType = false;
                _IEmContractService.Update(itemE);
                _IEmContractService.CommitTran();
                //uGridGhiDoanhSo.DataSource = new List<EMContractDetailRevenue>();
                btnGhidoanhso.Visible = true;
                btnBoghidoanhso.Visible = false;
                btnXoa.Enabled = true;
            }
            catch (Exception ex)
            {
                _IEmContractService.RolbackTran();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (_select.RevenueType == true)
            {
                MSG.Error("Hợp đồng đang ghi doanh số không được phép xóa ");
                return;
            }
            else
            {
                _IEmContractService.BeginTran();
                List<SAOrderDetail> lst = _ISAOrderDetailService.GetSAOrderDetailByContractID(_select.ID);
                List<GeneralLedger> lst1 = _IGeneralLedgerService.GetByContractID(_select.ID);
                if (lst1.Count == 0)
                {
                    if (_select != null && MSG.Question(string.Format(resSystem.MSG_System_05, "hợp đồng " + _select.Code)) == DialogResult.Yes)
                    {
                        foreach (EMContractAttachment item in listEMContractAttachment)
                        {
                            if (item.EMContractID == _select.ID)
                            {
                                _IEMContractAttachmentService.Delete(item);
                            }
                        }
                        foreach (var x in lst)
                        {
                            x.ContractID = null;
                            _ISAOrderDetailService.Update(x);
                        }
                        _IEmContractService.Delete(_select);
                        _IEmContractService.CommitTran();
                        // LoadCogfig();
                        MSG.Information("Xóa thành công");
                        this.Dispose();
                        this.Close();
                    }
                }
                else
                {
                    MSG.Warning("Xóa " + _select.Code + " thất bại vì đã phát sinh chứng từ liên quan");
                }
            }
        }

        private void FEMContractSaleDetail_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void FEMContractSaleDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
}