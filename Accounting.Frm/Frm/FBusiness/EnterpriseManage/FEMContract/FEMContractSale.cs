﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Facilities.TypedFactory.Internal;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Accounting
{
    public partial class FEMContractSale : CatalogBase
    {
        List<EMContract> lstEMcontractSale = new List<EMContract>();
        public readonly IEMContractService _IEmContractService; 
        IEMContractAttachmentService _IEMContractAttachmentService { get { return IoC.Resolve<IEMContractAttachmentService>(); } }
        IEMContractDetailPaymentService _IEMContractDetailPaymentService { get { return IoC.Resolve<IEMContractDetailPaymentService>(); } }
        IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        ISAInvoiceService _IMCReceiptDetailService { get { return IoC.Resolve<ISAInvoiceService>(); } }
        ISAInvoiceDetailService _ISAInvoiceDetailService { get { return IoC.Resolve<ISAInvoiceDetailService>(); } }
        IEMContractDetailMGService _IEMContractDetailMGService { get { return IoC.Resolve<IEMContractDetailMGService>(); } }
        IEMContractDetailRevenueService _IEMContractDetailRevenueService { get { return IoC.Resolve<IEMContractDetailRevenueService>(); } }
        ISAOrderDetailService _ISAOrderDetailService { get { return IoC.Resolve<ISAOrderDetailService>(); } }
        List<EMContractAttachment> listEMContractAttachment = new List<EMContractAttachment>();
        List<EMContractSale> _lstMCReceipt = new List<EMContractSale>();
        List<EMContractSale> _lstMCPayment = new List<EMContractSale>();
        List<EMContractSale> _lstSaInvoices = new List<EMContractSale>();
        List<EMContract> lsteEmContracts = new List<EMContract>();
        List<EMContractDetailMG> _lstContractDetailMG = new List<EMContractDetailMG>();
        List<EMContractDetailRevenue> _lstContractDetailRevenue = new List<EMContractDetailRevenue>();
        List<int> typeThucThu = new List<int>() { 100, 101, 320, 321, 322, 323, 324 };
        List<int> typeThucChi = new List<int>() { 110, 260, 261, 262, 263, 264 };
        List<int> typeHoaDon = new List<int>() { 320, 321, 322, 323, 324, 325, 330, 340 };
        public FEMContractSale()
        {
            WaitingFrm.StartWaiting();
            _IEmContractService = IoC.Resolve<IEMContractService>();
            InitializeComponent();
            LoadCogfig();
            btnHelp.Visible = false;
            btnExportExcel.Location = btnHelp.GetLocation();
            WaitingFrm.StopWaiting();
        }

        void LoadCogfig()
        {
            Utils.ClearCacheByType<EMContract>();
            lstEMcontractSale = _IEmContractService.GetAllContractSale();
            uGridDS.DataSource = lstEMcontractSale;
            Utils.ConfigGrid(uGridDS, ConstDatabase.FEMContractSale_TableName);
            
            UltraGridBand band = uGridDS.DisplayLayout.Bands[0];
            //uGridDS.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            //foreach (UltraGridColumn column in band.Columns)//comment by cuongpv 20190424
            //{
            //    this.ConfigEachColumn4Grid(0, column, uGridDS);
            //}
            band.Summaries.Clear();
            SummarySettings summary0 = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Code"]);
            summary0.DisplayFormat = "Số dòng = {0:N0}";
            summary0.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            uGridDS.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary0.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            uGridDS.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            uGridDS.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            uGridDS.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
            uGridDS.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS.DisplayLayout.Bands[0].Columns["AmountBeforCancel"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS.DisplayLayout.Bands[0].Columns["ExpectedCosts"].FormatNumberic(ConstDatabase.Format_TienVND);
            uGridDS.DisplayLayout.Bands[0].Columns["AmountBeforCancel"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["DeliverDate"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["ProjectName"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["IsProject"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["IsBillPaid"].Hidden = true;
            uGridDS.DisplayLayout.Bands[0].Columns["ExpectedCosts"].Hidden = true;

            if (lstEMcontractSale.Count > 0)
            {
                //uGridDS.Rows[0].Selected = true;
                UltraGridRow rowSelect = uGridDS.Rows[0];
                EMContract model = (EMContract)rowSelect.ListObject;
                if (model.RevenueType == true)
                {
                    btnWrite.Visible = false;
                    btnBoGhiDoanhSo.Visible = true;
                    btnDelete.Enabled = false;
                }
                else
                {
                    btnWrite.Visible = true;
                    //btnWrite.Location = new Point(btnDelete.Location.X + btnDelete.Width, btnDelete.Location.Y);
                    btnBoGhiDoanhSo.Visible = false;
                    btnDelete.Enabled = true;
                }
                //Grid Dự kiến chi
                //_lstEMContractDetailPayments = /*_IEMContractDetailPaymentService.getEMContractDetailPaymentbyIDEMContract((Guid)(uGridDS.Rows[0].Cells["ID"].Value))*/;
                uGridChi.DataSource = model.EMContractDetailPayments.ToList();
                Utils.ConfigGrid(uGridChi, ConstDatabase.EMContractDetailPayment2_TableName);
                foreach (var column in uGridChi.DisplayLayout.Bands[0].Columns)
                {
                    this.ConfigEachColumn4Grid(0, column, uGridChi);
                }
                uGridChi.DisplayLayout.Bands[0].Columns["RecordDate"].Header.VisiblePosition = 0;
                uGridChi.DisplayLayout.Bands[0].Columns["ExpenseItemID"].Header.VisiblePosition = 1;
                uGridChi.DisplayLayout.Bands[0].Columns["DepartmentID"].Header.VisiblePosition = 2;
                uGridChi.DisplayLayout.Bands[0].Columns["Description"].Header.VisiblePosition = 3;
                uGridChi.DisplayLayout.Bands[0].Columns["Method"].Header.VisiblePosition = 4;
                uGridChi.DisplayLayout.Bands[0].Columns["PercentAmount"].Header.VisiblePosition = 5;
                uGridChi.DisplayLayout.Bands[0].Columns["AmountOriginal"].Header.VisiblePosition = 6;
                uGridChi.DisplayLayout.Bands[0].Columns["ExpenseItemID"].LockColumn();
                uGridChi.DisplayLayout.Bands[0].Columns["DepartmentID"].LockColumn();


                UltraGridBand band2 = uGridChi.DisplayLayout.Bands[0];
                uGridChi.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                foreach (UltraGridColumn column in band2.Columns)//comment by cuongpv
                {
                    this.ConfigEachColumn4Grid(0, column, uGridChi);
                }
                band2.Summaries.Clear();
                SummarySettings summary = band2.Summaries.Add("Count", SummaryType.Count, band2.Columns["RecordDate"]);
                summary.DisplayFormat = "Số dòng = {0:N0}";
                summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                uGridChi.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
                summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
                uGridChi.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
                uGridChi.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
                uGridChi.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;
                Utils.AddSumColumn(uGridChi, "AmountOriginal", false);
                //summary = band2.Summaries.Add("sumAmountOriginal", SummaryType.Sum, band2.Columns["AmountOriginal"]);
                //summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                //summary.DisplayFormat = "{0:N0}";
                //summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
                //band2.Columns["AmountOriginal"].FormatNumberic(ConstDatabase.Format_TienVND);
                //summary.Appearance.TextHAlign = HAlign.Right;

                //Tab thực thu
                _lstMCReceipt = _IGeneralLedgerService.GetEMContractSaleDetailByContractID((Guid)uGridDS.Rows[0].Cells["ID"].Value, typeThucThu);
                uGridThucThu.DataSource = _lstMCReceipt;
                Utils.ConfigGrid(uGridThucThu, "Thuc thu", GetColumnThucthu());
                uGridThucThu.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                UltraGridBand band5 = uGridThucThu.DisplayLayout.Bands[0];
                band5.Columns["TypeName"].CellActivation = Activation.NoEdit;
                band5.Columns["Date"].CellActivation = Activation.NoEdit;
                band5.Columns["No"].CellActivation = Activation.NoEdit;
                band5.Columns["Reason"].CellActivation = Activation.NoEdit;
                band5.Columns["Amount"].CellActivation = Activation.NoEdit;
                band5.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                //Tab thực chi
                _lstMCPayment = _IGeneralLedgerService.GetEMContractSaleThucChiDetailByContractID((Guid)uGridDS.Rows[0].Cells["ID"].Value, typeThucChi);
                uGridThucChi.DataSource = _lstMCPayment;
                Utils.ConfigGrid(uGridThucChi, "Thuc Chi", GetColumnsThucChi());
                uGridThucChi.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                UltraGridBand band6 = uGridThucChi.DisplayLayout.Bands[0];
                band6.Columns["TypeName"].CellActivation = Activation.NoEdit;
                band6.Columns["Date"].CellActivation = Activation.NoEdit;
                band6.Columns["No"].CellActivation = Activation.NoEdit;
                band6.Columns["Reason"].CellActivation = Activation.NoEdit;
                band6.Columns["Amount"].CellActivation = Activation.NoEdit;
                band6.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                //Tab hóa đơn 
                _lstSaInvoices = _IGeneralLedgerService.GetEMContractSaleDetailByContractID((Guid)uGridDS.Rows[0].Cells["ID"].Value, typeHoaDon);
                uGridHoaDon.DataSource = _lstSaInvoices;
                Utils.ConfigGrid(uGridHoaDon, "Hoa don", GetColumnHoaDon());
                uGridHoaDon.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                UltraGridBand band7 = uGridHoaDon.DisplayLayout.Bands[0];
                band7.Columns["InvoiceDate"].CellActivation = Activation.NoEdit;
                band7.Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                band7.Columns["Reason"].CellActivation = Activation.NoEdit;
                band7.Columns["Amount"].CellActivation = Activation.NoEdit;
                band7.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
                //Tab dự hàng hóa, dịch vụ
                _lstContractDetailMG = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)uGridDS.Rows[0].Cells["ID"].Value);
                uGridHHDV.DataSource = _lstContractDetailMG;
                Utils.ConfigGrid(uGridHHDV, ConstDatabase.EMContractDetailMG_TableName);
                //uGridHHDV.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].Hidden = true;
                uGridHHDV.DisplayLayout.Bands[0].Columns["SAOrderID"].Hidden = true;
                uGridHHDV.DisplayLayout.Bands[0].Columns["PPOrderID"].Hidden = true;
                uGridHHDV.DisplayLayout.Bands[0].Columns["PPOrderCode"].Hidden = true;
                uGridHHDV.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].LockColumn();
                UltraGridBand band3 = uGridHHDV.DisplayLayout.Bands[0];
                foreach (UltraGridColumn column in band3.Columns)//comment by cuongpv
                {
                    this.ConfigEachColumn4Grid(0, column, uGridHHDV);
                }
                band3.Summaries.Clear();
                SummarySettings summary1 = band3.Summaries.Add("Count", SummaryType.Count, band3.Columns["MaterialGoodsID"]);
                summary1.DisplayFormat = "Số dòng = {0:N0}";
                summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                uGridHHDV.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;
                summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;

                //Tab ghi doanh số 
                _lstContractDetailRevenue = _IEMContractDetailRevenueService.GetByContractID((Guid)uGridDS.Rows[0].Cells["ID"].Value);
                uGridGhiDoanhSo.DataSource = _lstContractDetailRevenue;
                Utils.ConfigGrid(uGridGhiDoanhSo, ConstDatabase.EMContractDetailRevenue_TableName);
                uGridGhiDoanhSo.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                UltraGridBand band4 = uGridGhiDoanhSo.DisplayLayout.Bands[0];
                foreach (UltraGridColumn column in band4.Columns)//comment by cuongpv
                {
                    this.ConfigEachColumn4Grid(0, column, uGridGhiDoanhSo);
                }
                uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].LockColumn();
                uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["DepartmentID"].LockColumn();
                uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["ContractEmployeeID"].LockColumn();

            }
        }
        #region template

        private List<TemplateColumn> GetColumnThucthu()
        {
            return new List<TemplateColumn>
           {
               new TemplateColumn
               {
                   ColumnCaption = "Loại chứng từ",
                   ColumnName = "TypeName",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 0,
                },
                 new TemplateColumn
               {
                   ColumnCaption = "Ngày chứng từ",
                   ColumnName = "Date",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 1,
                   },
                    new TemplateColumn
               {
                   ColumnCaption = "Số chứng từ",
                   ColumnName = "No",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 2,
                   },
                    new TemplateColumn
               {
                   ColumnCaption = "Diễn giải",
                   ColumnName = "Reason",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 3,
                   },

                new TemplateColumn
               {
                   ColumnCaption = "Số tiền",
                   ColumnName = "Amount",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 4,
                   },
           };
        }

        private List<TemplateColumn> GetColumnsThucChi()
        {
            return new List<TemplateColumn>
            {
                new TemplateColumn()
                {
                   ColumnCaption = "Loại chứng từ",
                   ColumnName = "TypeName",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   IsVisibleCbb = false,
                   VisiblePosition = 0,
                },
                   new TemplateColumn
               {
                   ColumnCaption = "Ngày chứng từ",
                   ColumnName = "Date",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   IsVisibleCbb = false,
                   VisiblePosition = 1,
                   },
                   new TemplateColumn
               {
                   ColumnCaption = "Diễn giải",
                   ColumnName = "Reason",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 4,
                   },
                   new TemplateColumn
               {
                   ColumnCaption = "Số chứng từ",
                   ColumnName = "No",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 5,
                   },
                       new TemplateColumn
               {
                   ColumnCaption = "Số tiền",
                   ColumnName = "Amount",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 6,
                   },
            };
        }

        private List<TemplateColumn> GetColumnHoaDon()
        {
            return new List<TemplateColumn>
            {
                new TemplateColumn()
                {
                    ColumnCaption = "Ngày hóa đơn",
                    ColumnName = "InvoiceDate",
                    ColumnWidth = 100,
                    ColumnMaxWidth = 500,
                    IsVisible = true,
                    VisiblePosition = 0,
                },
                   new TemplateColumn()
                {
                    ColumnCaption = "Số hóa đơn",
                    ColumnName = "InvoiceNo",
                    ColumnWidth = 100,
                    ColumnMaxWidth = 500,
                    IsVisible = true,
                    VisiblePosition = 1,
                },
                   new TemplateColumn
               {
                   ColumnCaption = "Diễn giải",
                   ColumnName = "Reason",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 2,
                   },
                
                new TemplateColumn
               {
                   ColumnCaption = "Số tiền",
                   ColumnName = "Amount",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 3,
                   },

            };
        }
        #endregion
        #region utils

        protected override void AddFunction()
        {
            //new FEMContractSaleDetail().ShowDialog(this);
            //if (FEMContractSaleDetail.isclose) LoadCogfig();
            //HUYPD Edit Show Form
            FEMContractSaleDetail frm = new FEMContractSaleDetail();
            frm.FormClosed += new FormClosedEventHandler(FEMContractSaleDetail_FormClosed);
            frm.ShowDialogHD(this);
        }

        #endregion
        #region button
        private void btnAddContract_Click(object sender, EventArgs e)
        {
            AddFunction();
        }
        

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        protected override void DeleteFunction()
        {
            
            if (uGridDS.Selected.Rows.Count > 0)
            {
                EMContract temp = uGridDS.Selected.Rows[0].ListObject as EMContract;
                if (temp.RevenueType == true)
                { MSG.Error("Hợp đồng đang ghi doanh số không được phép xóa ");
                    return;
                }
                else
                {
                    _IEmContractService.BeginTran();
                    List<SAOrderDetail> lst = _ISAOrderDetailService.GetSAOrderDetailByContractID(temp.ID);
                    List<GeneralLedger> lst1 = _IGeneralLedgerService.GetByContractID(temp.ID);
                    if (lst1.Count == 0)
                    {
                        if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, "hợp đồng " + temp.Code)) == DialogResult.Yes)
                        {
                            foreach (EMContractAttachment item in listEMContractAttachment)
                            {
                                if (item.EMContractID == temp.ID)
                                {
                                    _IEMContractAttachmentService.Delete(item);
                                }
                            }
                            foreach (var x in lst)
                            {
                                x.ContractID = null;
                                _ISAOrderDetailService.Update(x);
                            }
                            _IEmContractService.Delete(temp);
                            _IEmContractService.CommitTran();
                            LoadCogfig();
                        }
                    }
                    else
                    {
                        MSG.Warning("Xóa " + temp.Code + " thất bại vì đã phát sinh chứng từ liên quan");
                    }
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
            
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        protected override void EditFunction()
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                EMContract temp = uGridDS.Selected.Rows[0].ListObject as EMContract;
                //new FEMContractSaleDetail(temp).ShowDialog(this);
                //if (FEMContractSaleDetail.isclose) LoadCogfig();
                //HUYPD Edit Show Form
                FEMContractSaleDetail frm = new FEMContractSaleDetail(temp);
                frm.FormClosed += new FormClosedEventHandler(FEMContractSaleDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
            else
                MSG.Error(resSystem.MSG_System_04);
            
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadCogfig();
            
        }

        private void uGridDS_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                EMContract temp = uGridDS.Selected.Rows[0].ListObject as EMContract;
                //new FEMContractSaleDetail(temp).ShowDialog(this);
                //if (FEMContractSaleDetail.isclose) LoadCogfig();
                //HUYPD Edit Show Form
                FEMContractSaleDetail frm = new FEMContractSaleDetail(temp);
                frm.FormClosed += new FormClosedEventHandler(FEMContractSaleDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
        }
        private void FEMContractSaleDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoadCogfig();
        }
        private void uGridDS_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Utils.ClearCacheByType<EMContract>();
            uGridDS.DataSource = _IEmContractService.GetAllContractSale();
            if (uGridDS.Selected.Rows.Count > 0)
            {
                Utils.ClearCacheByType<EMContract>();
                var ID = uGridDS.ActiveRow.Cells["ID"].Value;
                if (ID.ToString() != null)
                {
                    //Tab dự kiến chi
                    uGridChi.DataSource = _IEMContractDetailPaymentService.getEMContractDetailPaymentbyIDEMContract((Guid)ID);
                    //Tab thực thu
                    uGridThucThu.DataSource = _IGeneralLedgerService.GetEMContractSaleDetailByContractID((Guid)ID, typeThucThu);
                    //Tab thực chi
                    uGridThucChi.DataSource = _IGeneralLedgerService.GetEMContractSaleThucChiDetailByContractID((Guid)ID, typeThucChi);
                    //Tab hóa đơn
                    uGridHoaDon.DataSource = _IGeneralLedgerService.GetEMContractHoaDonByContractID((Guid)ID, typeHoaDon);
                    //Tab hàng hóa, dịch vụ
                    uGridHHDV.DataSource = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)ID);
                    //Tab Ghi doanh số 
                    uGridGhiDoanhSo.DataSource = _IEMContractDetailRevenueService.GetByContractID((Guid)ID);
                    //Config Grid
                    uGridDS.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;                                     
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (Utils.isDemo)
            {
                MSG.Warning("Phiên bản dùng thử không có quyền sử dụng chức năng này");
                return;
            }
            uGridDS.PrintPreview();
        }
        #endregion
        private void uGridDS_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                EMContract em = uGridDS.Selected.Rows[0].ListObject as EMContract;
                if (em == null) return;
                var ID = uGridDS.ActiveRow.Cells["ID"].Value;
                if (ID.ToString() != null)
                {
                    //Tab dự kiến chi
                    //uGridChi.DataSource = _IEMContractDetailPaymentService.getEMContractDetailPaymentbyIDEMContract((Guid)ID);
                    uGridChi.DataSource = em.EMContractDetailPayments.ToList();
                    //Tab thực thu
                    uGridThucThu.DataSource = _IGeneralLedgerService.GetEMContractSaleDetailByContractID((Guid)ID, typeThucThu);
                    //Tab thực chi
                    uGridThucChi.DataSource = _IGeneralLedgerService.GetEMContractSaleThucChiDetailByContractID((Guid)ID, typeThucChi);
                    //Tab hóa đơn
                    uGridHoaDon.DataSource = _IGeneralLedgerService.GetEMContractHoaDonByContractID((Guid)ID, typeHoaDon);
                    //Tab hàng hóa, dịch vụ
                    uGridHHDV.DataSource = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)ID);
                    //Tab Ghi doanh số 
                    uGridGhiDoanhSo.DataSource = _IEMContractDetailRevenueService.GetByContractID((Guid)ID);
                    //Config Grid
                }
            }
            
        }

        private void btnWrite_Click(object sender, EventArgs e)
        { 
            if (uGridDS.Selected.Rows.Count > 0)
            {
                var ID = uGridDS.ActiveRow.Cells["ID"].Value;
                EMContract itemE = _IEmContractService.Getbykey((Guid)ID);
                itemE.RevenueType = true;
                _IEmContractService.Update(itemE);
                List<EMContractDetailMG> itemM = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)ID);
                List<EMContractDetailRevenue> lst = new List<EMContractDetailRevenue>();
                try
                {
                    _IEmContractService.BeginTran();
                    if (itemM.Count > 0)
                    {
                        foreach (var item in itemM)
                        {
                            EMContractDetailRevenue itemR = new EMContractDetailRevenue();
                            itemR.ID = Guid.NewGuid();
                            itemR.ContractID = (Guid)ID;
                            itemR.DepartmentID = itemE.DepartmentID;
                            itemR.ContractEmployeeID = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == (Utils.ListSAInvoiceDetail.FirstOrDefault(p => p.ContractID == itemE.ID).SAInvoiceID)).EmployeeID;
                            itemR.MaterialGoodsID = item.MaterialGoodsID;
                            itemR.RevenueDate = DateTime.Now;
                            if (item.TotalAmount != 0)
                            {
                                itemR.RevenueAmount = Math.Round(item.TotalAmount - item.DiscountAmount + item.VATAmount, 0);
                            }
                            _IEMContractDetailRevenueService.CreateNew(itemR);
                            lst.Add(itemR);
                        }
                    }
                    else
                    {
                        EMContractDetailRevenue itemR = new EMContractDetailRevenue();
                        itemR.ID = Guid.NewGuid();
                        itemR.ContractID = (Guid)ID;
                        itemR.DepartmentID = itemE.DepartmentID;
                        itemR.ContractEmployeeID = Utils.ListSAInvoice.FirstOrDefault(n => n.ID == (Utils.ListSAInvoiceDetail.FirstOrDefault(p => p.ContractID == itemE.ID).SAInvoiceID)).EmployeeID;
                        itemR.MaterialGoodsID = null;
                        itemR.RevenueDate = DateTime.Now;
                        itemR.RevenueAmount = itemE.Amount;
                        itemR.OrderPriority = 0;
                        _IEMContractDetailRevenueService.CreateNew(itemR);
                        lst.Add(itemR);
                    }
                    _IEmContractService.CommitTran();
                    uGridGhiDoanhSo.DataSource = lst;
                    Utils.ConfigGrid(uGridGhiDoanhSo, ConstDatabase.EMContractDetailRevenue_TableName);
                    uGridGhiDoanhSo.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                    UltraGridBand band4 = uGridGhiDoanhSo.DisplayLayout.Bands[0];
                    foreach (UltraGridColumn column in band4.Columns)
                    {
                        this.ConfigEachColumn4Grid(0, column, uGridGhiDoanhSo);
                    }
                    uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].LockColumn();
                    uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["DepartmentID"].LockColumn();
                    uGridGhiDoanhSo.DisplayLayout.Bands[0].Columns["ContractEmployeeID"].LockColumn();
                }
                catch (Exception ex)
                {
                    _IEmContractService.RolbackTran();
                    lst.Clear();
                }
                btnBoGhiDoanhSo.Visible = true;
                btnWrite.Visible = false;
                //btnDelete.Enabled = false;
            }
        }

        private void btnBoGhiDoanhSo_Click(object sender, EventArgs e)
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                var ID = uGridDS.ActiveRow.Cells["ID"].Value;
                try
                {
                    _IEmContractService.BeginTran();
                    List<EMContractDetailRevenue> lstEMContractDetailRevenue = _IEMContractDetailRevenueService.GetByContractID((Guid)ID);
                    foreach (var itemR in lstEMContractDetailRevenue)
                    {
                        _IEMContractDetailRevenueService.Delete(itemR);
                    }

                    EMContract itemE = _IEmContractService.Getbykey((Guid)ID);
                    itemE.RevenueType = false;
                    _IEmContractService.Update(itemE);
                    _IEmContractService.CommitTran();
                    uGridGhiDoanhSo.DataSource = new List<EMContractDetailRevenue>();
                }
                catch (Exception ex)
                {
                    _IEmContractService.RolbackTran();
                }
                btnWrite.Visible = true;
                btnBoGhiDoanhSo.Visible = false;
                btnDelete.Enabled = true;
            }
        }

        private void uGridDS_Click(object sender, EventArgs e)
        {
            if (uGridDS.Selected.Rows.Count > 0)
            {
                var ID = uGridDS.ActiveRow.Cells["ID"].Value;
                if (ID.ToString() != null)
                {
                    EMContract item = _IEmContractService.Getbykey((Guid)ID);
                    if(item.RevenueType == true)
                    {
                        btnWrite.Visible = false;
                        btnBoGhiDoanhSo.Visible = true;
                        //btnDelete.Enabled = false;
                    }
                    else
                    {
                        btnWrite.Visible = true;
                        btnWrite.Location = new Point(btnDelete.Location.X + btnDelete.Width, btnDelete.Location.Y);
                        btnBoGhiDoanhSo.Visible = false;
                        btnDelete.Enabled = true;
                    }
                }
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(uGridDS, "Hợp đồng bán");
        }

        private void FEMContractSale_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            uGridDS.ResetText();
            uGridDS.ResetUpdateMode();
            uGridDS.ResetExitEditModeOnLeave();
            uGridDS.ResetRowUpdateCancelAction();
            uGridDS.DataSource = null;
            uGridDS.Layouts.Clear();
            uGridDS.ResetLayouts();
            uGridDS.ResetDisplayLayout();
            uGridDS.Refresh();
            uGridDS.ClearUndoHistory();
            uGridDS.ClearXsdConstraints();
            
            uGridChi.ResetText();
            uGridChi.ResetUpdateMode();
            uGridChi.ResetExitEditModeOnLeave();
            uGridChi.ResetRowUpdateCancelAction();
            uGridChi.DataSource = null;
            uGridChi.Layouts.Clear();
            uGridChi.ResetLayouts();
            uGridChi.ResetDisplayLayout();
            uGridChi.Refresh();
            uGridChi.ClearUndoHistory();
            uGridChi.ClearXsdConstraints();

            uGridThucThu.ResetText();
            uGridThucThu.ResetUpdateMode();
            uGridThucThu.ResetExitEditModeOnLeave();
            uGridThucThu.ResetRowUpdateCancelAction();
            uGridThucThu.DataSource = null;
            uGridThucThu.Layouts.Clear();
            uGridThucThu.ResetLayouts();
            uGridThucThu.ResetDisplayLayout();
            uGridThucThu.Refresh();
            uGridThucThu.ClearUndoHistory();
            uGridThucThu.ClearXsdConstraints();

            uGridThucChi.ResetText();
            uGridThucChi.ResetUpdateMode();
            uGridThucChi.ResetExitEditModeOnLeave();
            uGridThucChi.ResetRowUpdateCancelAction();
            uGridThucChi.DataSource = null;
            uGridThucChi.Layouts.Clear();
            uGridThucChi.ResetLayouts();
            uGridThucChi.ResetDisplayLayout();
            uGridThucChi.Refresh();
            uGridThucChi.ClearUndoHistory();
            uGridThucChi.ClearXsdConstraints();

            uGridHoaDon.ResetText();
            uGridHoaDon.ResetUpdateMode();
            uGridHoaDon.ResetExitEditModeOnLeave();
            uGridHoaDon.ResetRowUpdateCancelAction();
            uGridHoaDon.DataSource = null;
            uGridHoaDon.Layouts.Clear();
            uGridHoaDon.ResetLayouts();
            uGridHoaDon.ResetDisplayLayout();
            uGridHoaDon.Refresh();
            uGridHoaDon.ClearUndoHistory();
            uGridHoaDon.ClearXsdConstraints();

            uGridHHDV.ResetText();
            uGridHHDV.ResetUpdateMode();
            uGridHHDV.ResetExitEditModeOnLeave();
            uGridHHDV.ResetRowUpdateCancelAction();
            uGridHHDV.DataSource = null;
            uGridHHDV.Layouts.Clear();
            uGridHHDV.ResetLayouts();
            uGridHHDV.ResetDisplayLayout();
            uGridHHDV.Refresh();
            uGridHHDV.ClearUndoHistory();
            uGridHHDV.ClearXsdConstraints();

            uGridGhiDoanhSo.ResetText();
            uGridGhiDoanhSo.ResetUpdateMode();
            uGridGhiDoanhSo.ResetExitEditModeOnLeave();
            uGridGhiDoanhSo.ResetRowUpdateCancelAction();
            uGridGhiDoanhSo.DataSource = null;
            uGridGhiDoanhSo.Layouts.Clear();
            uGridGhiDoanhSo.ResetLayouts();
            uGridGhiDoanhSo.ResetDisplayLayout();
            uGridGhiDoanhSo.Refresh();
            uGridGhiDoanhSo.ClearUndoHistory();
            uGridGhiDoanhSo.ClearXsdConstraints();
        }

        private void FEMContractSale_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
