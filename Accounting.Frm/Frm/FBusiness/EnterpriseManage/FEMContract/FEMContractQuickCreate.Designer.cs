﻿namespace Accounting
{
    partial class FEMContractQuickCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.lblAmount = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtAmountOriginal = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblAmountOriginal = new Infragistics.Win.Misc.UltraLabel();
            this.txtExchangeRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblExchangeRate = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCurrency = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.dteEffectiveDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dteSignedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.optContractType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteSignedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optContractType)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 22);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Loại (*)";
            // 
            // ultraLabel2
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 49);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Số hợp đồng (*)";
            // 
            // ultraLabel3
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(12, 76);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Loại tiền";
            // 
            // lblAmount
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.lblAmount.Appearance = appearance4;
            this.lblAmount.Location = new System.Drawing.Point(12, 103);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(88, 22);
            this.lblAmount.TabIndex = 3;
            this.lblAmount.Text = "Giá trị HĐ (*)";
            // 
            // ultraLabel5
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance5;
            this.ultraLabel5.Location = new System.Drawing.Point(12, 130);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel5.TabIndex = 4;
            this.ultraLabel5.Text = "Ngày ký";
            // 
            // ultraLabel6
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance6;
            this.ultraLabel6.Location = new System.Drawing.Point(12, 157);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(88, 22);
            this.ultraLabel6.TabIndex = 5;
            this.ultraLabel6.Text = "Trích yếu";
            // 
            // ultraLabel7
            // 
            appearance7.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance7;
            this.ultraLabel7.Location = new System.Drawing.Point(251, 130);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(78, 22);
            this.ultraLabel7.TabIndex = 6;
            this.ultraLabel7.Text = "Ngày hiệu lực";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.txtAmountOriginal);
            this.ultraGroupBox1.Controls.Add(this.lblAmountOriginal);
            this.ultraGroupBox1.Controls.Add(this.txtExchangeRate);
            this.ultraGroupBox1.Controls.Add(this.lblExchangeRate);
            this.ultraGroupBox1.Controls.Add(this.cbbCurrency);
            this.ultraGroupBox1.Controls.Add(this.txtName);
            this.ultraGroupBox1.Controls.Add(this.dteEffectiveDate);
            this.ultraGroupBox1.Controls.Add(this.dteSignedDate);
            this.ultraGroupBox1.Controls.Add(this.txtAmount);
            this.ultraGroupBox1.Controls.Add(this.txtCode);
            this.ultraGroupBox1.Controls.Add(this.optContractType);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.lblAmount);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(485, 221);
            this.ultraGroupBox1.TabIndex = 7;
            // 
            // txtAmountOriginal
            // 
            appearance8.TextHAlignAsString = "Right";
            this.txtAmountOriginal.Appearance = appearance8;
            this.txtAmountOriginal.AutoSize = false;
            this.txtAmountOriginal.Location = new System.Drawing.Point(344, 103);
            this.txtAmountOriginal.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAmountOriginal.Name = "txtAmountOriginal";
            this.txtAmountOriginal.NullText = "0";
            this.txtAmountOriginal.PromptChar = ' ';
            this.txtAmountOriginal.Size = new System.Drawing.Size(135, 22);
            this.txtAmountOriginal.TabIndex = 18;
            this.txtAmountOriginal.TextChanged += new System.EventHandler(this.txtAmountOriginal_TextChanged);
            // 
            // lblAmountOriginal
            // 
            appearance9.TextVAlignAsString = "Middle";
            this.lblAmountOriginal.Appearance = appearance9;
            this.lblAmountOriginal.Location = new System.Drawing.Point(251, 103);
            this.lblAmountOriginal.Name = "lblAmountOriginal";
            this.lblAmountOriginal.Size = new System.Drawing.Size(88, 22);
            this.lblAmountOriginal.TabIndex = 17;
            this.lblAmountOriginal.Text = "Nguyên tệ (*)";
            // 
            // txtExchangeRate
            // 
            appearance10.TextHAlignAsString = "Right";
            this.txtExchangeRate.Appearance = appearance10;
            this.txtExchangeRate.AutoSize = false;
            this.txtExchangeRate.Location = new System.Drawing.Point(344, 76);
            this.txtExchangeRate.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.NullText = "0";
            this.txtExchangeRate.PromptChar = ' ';
            this.txtExchangeRate.Size = new System.Drawing.Size(135, 22);
            this.txtExchangeRate.TabIndex = 16;
            this.txtExchangeRate.TextChanged += new System.EventHandler(this.txtExchangeRate_TextChanged);
            // 
            // lblExchangeRate
            // 
            appearance11.TextVAlignAsString = "Middle";
            this.lblExchangeRate.Appearance = appearance11;
            this.lblExchangeRate.Location = new System.Drawing.Point(251, 76);
            this.lblExchangeRate.Name = "lblExchangeRate";
            this.lblExchangeRate.Size = new System.Drawing.Size(88, 22);
            this.lblExchangeRate.TabIndex = 15;
            this.lblExchangeRate.Text = "Tỷ giá (*)";
            // 
            // cbbCurrency
            // 
            this.cbbCurrency.AutoSize = false;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurrency.DisplayLayout.Appearance = appearance12;
            this.cbbCurrency.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurrency.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.cbbCurrency.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrency.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.cbbCurrency.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurrency.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurrency.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurrency.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurrency.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurrency.DisplayLayout.Override.CellAppearance = appearance19;
            this.cbbCurrency.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurrency.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrency.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.cbbCurrency.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.cbbCurrency.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurrency.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurrency.DisplayLayout.Override.RowAppearance = appearance22;
            this.cbbCurrency.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurrency.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.cbbCurrency.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurrency.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurrency.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurrency.Location = new System.Drawing.Point(106, 76);
            this.cbbCurrency.Name = "cbbCurrency";
            this.cbbCurrency.Size = new System.Drawing.Size(135, 22);
            this.cbbCurrency.TabIndex = 14;
            this.cbbCurrency.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbCurrency_RowSelected);
            // 
            // txtName
            // 
            this.txtName.AutoSize = false;
            this.txtName.Location = new System.Drawing.Point(106, 157);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtName.Size = new System.Drawing.Size(373, 50);
            this.txtName.TabIndex = 13;
            // 
            // dteEffectiveDate
            // 
            appearance24.TextVAlignAsString = "Middle";
            this.dteEffectiveDate.Appearance = appearance24;
            this.dteEffectiveDate.AutoSize = false;
            this.dteEffectiveDate.Location = new System.Drawing.Point(344, 130);
            this.dteEffectiveDate.MaskInput = "{LOC}dd/mm/yyyy";
            this.dteEffectiveDate.Name = "dteEffectiveDate";
            this.dteEffectiveDate.Size = new System.Drawing.Size(135, 22);
            this.dteEffectiveDate.TabIndex = 12;
            // 
            // dteSignedDate
            // 
            appearance25.TextVAlignAsString = "Middle";
            this.dteSignedDate.Appearance = appearance25;
            this.dteSignedDate.AutoSize = false;
            this.dteSignedDate.FormatString = "";
            this.dteSignedDate.Location = new System.Drawing.Point(106, 130);
            this.dteSignedDate.MaskInput = "{LOC}dd/mm/yyyy";
            this.dteSignedDate.Name = "dteSignedDate";
            this.dteSignedDate.Size = new System.Drawing.Size(135, 22);
            this.dteSignedDate.TabIndex = 11;
            this.dteSignedDate.ValueChanged += new System.EventHandler(this.dteSignedDate_ValueChanged);
            // 
            // txtAmount
            // 
            appearance26.TextHAlignAsString = "Right";
            this.txtAmount.Appearance = appearance26;
            this.txtAmount.AutoSize = false;
            this.txtAmount.Location = new System.Drawing.Point(106, 103);
            this.txtAmount.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.NullText = "0";
            this.txtAmount.PromptChar = ' ';
            this.txtAmount.Size = new System.Drawing.Size(135, 22);
            this.txtAmount.TabIndex = 10;
            // 
            // txtCode
            // 
            this.txtCode.AutoSize = false;
            this.txtCode.Location = new System.Drawing.Point(106, 49);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(373, 22);
            this.txtCode.TabIndex = 8;
            this.txtCode.ValueChanged += new System.EventHandler(this.txtCode_ValueChanged);
            // 
            // optContractType
            // 
            this.optContractType.BackColor = System.Drawing.Color.Transparent;
            this.optContractType.BackColorInternal = System.Drawing.Color.Transparent;
            this.optContractType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "Default Item";
            valueListItem1.DisplayText = "Hợp đồng mua";
            valueListItem2.DataValue = "ValueListItem1";
            valueListItem2.DisplayText = "Hợp đồng bán";
            this.optContractType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.optContractType.ItemSpacingHorizontal = 40;
            this.optContractType.Location = new System.Drawing.Point(106, 26);
            this.optContractType.Name = "optContractType";
            this.optContractType.Size = new System.Drawing.Size(268, 19);
            this.optContractType.TabIndex = 7;
            this.optContractType.ValueChanged += new System.EventHandler(this.optContractType_ValueChanged);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance27.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance27;
            this.btnClose.Location = new System.Drawing.Point(422, 239);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance28;
            this.btnSave.Location = new System.Drawing.Point(341, 239);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 302;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FEMContractQuickCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 281);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FEMContractQuickCreate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm nhanh Hợp đồng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FEMContractQuickCreate_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteSignedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optContractType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel lblAmount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet optContractType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteEffectiveDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dteSignedDate;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtCode;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrency;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmountOriginal;
        private Infragistics.Win.Misc.UltraLabel lblAmountOriginal;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtExchangeRate;
        private Infragistics.Win.Misc.UltraLabel lblExchangeRate;
    }
}