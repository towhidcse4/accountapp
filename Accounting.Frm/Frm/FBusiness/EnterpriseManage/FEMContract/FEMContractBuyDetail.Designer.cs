﻿namespace Accounting
{
    partial class FEMContractBuyDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtClosedReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbContractState = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtClosedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbPPOrder = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtDueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtDeliverDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAmountOriginal = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtAmount = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.txtExchangeRate = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.lblOrg = new Infragistics.Win.Misc.UltraLabel();
            this.lblRate = new Infragistics.Win.Misc.UltraLabel();
            this.txtSignerTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtEffectiveDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtSignedDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.lblGiaTriQĐ = new Infragistics.Win.Misc.UltraLabel();
            this.cbbCurrencyID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSignerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtEMContractCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.H = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectBankAccount = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtAccountingObjectBankName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtAccountingObjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectSignerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.txtAccountingObjectTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDk = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.btnEdit = new Infragistics.Win.Misc.UltraButton();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel4 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.txtName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridEMContractMG = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraPanel5 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.utEMContractBuy = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContractState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPPOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliverDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrencyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMContractCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectSignerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.ultraPanel4.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEMContractMG)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ultraPanel5.ClientArea.SuspendLayout();
            this.ultraPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utEMContractBuy)).BeginInit();
            this.utEMContractBuy.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel3);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel1);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel4);
            this.ultraTabPageControl1.Controls.Add(this.ultraPanel2);
            this.ultraTabPageControl1.Controls.Add(this.txtName);
            this.ultraTabPageControl1.Controls.Add(this.ultraLabel16);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(823, 596);
            // 
            // ultraGroupBox3
            // 
            appearance1.FontData.BoldAsString = "True";
            appearance1.FontData.SizeInPoints = 11F;
            this.ultraGroupBox3.Appearance = appearance1;
            this.ultraGroupBox3.Controls.Add(this.txtClosedReason);
            this.ultraGroupBox3.Controls.Add(this.cbbContractState);
            this.ultraGroupBox3.Controls.Add(this.txtClosedDate);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox3.Location = new System.Drawing.Point(2, 340);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(818, 60);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.Text = "Tình trạng hợp đồng";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // txtClosedReason
            // 
            this.txtClosedReason.AutoSize = false;
            this.txtClosedReason.Location = new System.Drawing.Point(594, 28);
            this.txtClosedReason.Name = "txtClosedReason";
            this.txtClosedReason.Size = new System.Drawing.Size(212, 22);
            this.txtClosedReason.TabIndex = 42;
            // 
            // cbbContractState
            // 
            this.cbbContractState.AutoSize = false;
            appearance2.Image = global::Accounting.Properties.Resources.plus;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            appearance3.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton1.PressedAppearance = appearance3;
            this.cbbContractState.ButtonsRight.Add(editorButton1);
            this.cbbContractState.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbContractState.Location = new System.Drawing.Point(96, 28);
            this.cbbContractState.Name = "cbbContractState";
            this.cbbContractState.NullText = "<chọn dữ liệu>";
            this.cbbContractState.Size = new System.Drawing.Size(186, 22);
            this.cbbContractState.TabIndex = 43;
            this.cbbContractState.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbContractState_RowSelected);
            this.cbbContractState.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbContractState_EditorButtonClick);
            // 
            // txtClosedDate
            // 
            this.txtClosedDate.AutoSize = false;
            this.txtClosedDate.Location = new System.Drawing.Point(370, 28);
            this.txtClosedDate.MaskInput = "dd/mm/yyyy";
            this.txtClosedDate.Name = "txtClosedDate";
            this.txtClosedDate.Size = new System.Drawing.Size(117, 22);
            this.txtClosedDate.TabIndex = 11;
            this.txtClosedDate.Value = null;
            // 
            // ultraLabel17
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel17.Appearance = appearance4;
            this.ultraLabel17.Location = new System.Drawing.Point(493, 28);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel17.TabIndex = 10;
            this.ultraLabel17.Text = "Lý do kết thúc";
            // 
            // ultraLabel19
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel19.Appearance = appearance5;
            this.ultraLabel19.Location = new System.Drawing.Point(288, 28);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(76, 22);
            this.ultraLabel19.TabIndex = 8;
            this.ultraLabel19.Text = "Ngày kết thúc";
            // 
            // ultraLabel23
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel23.Appearance = appearance6;
            this.ultraLabel23.Location = new System.Drawing.Point(6, 28);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(80, 22);
            this.ultraLabel23.TabIndex = 0;
            this.ultraLabel23.Text = "Tình trạng (*)";
            // 
            // ultraGroupBox1
            // 
            appearance7.FontData.BoldAsString = "True";
            appearance7.FontData.SizeInPoints = 11F;
            appearance7.TextVAlignAsString = "Middle";
            this.ultraGroupBox1.Appearance = appearance7;
            this.ultraGroupBox1.Controls.Add(this.cbbPPOrder);
            this.ultraGroupBox1.Controls.Add(this.txtDueDate);
            this.ultraGroupBox1.Controls.Add(this.txtDeliverDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox1.Controls.Add(this.txtAmountOriginal);
            this.ultraGroupBox1.Controls.Add(this.txtAmount);
            this.ultraGroupBox1.Controls.Add(this.txtExchangeRate);
            this.ultraGroupBox1.Controls.Add(this.lblOrg);
            this.ultraGroupBox1.Controls.Add(this.lblRate);
            this.ultraGroupBox1.Controls.Add(this.txtSignerTitle);
            this.ultraGroupBox1.Controls.Add(this.txtEffectiveDate);
            this.ultraGroupBox1.Controls.Add(this.txtSignedDate);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox1.Controls.Add(this.lblGiaTriQĐ);
            this.ultraGroupBox1.Controls.Add(this.cbbCurrencyID);
            this.ultraGroupBox1.Controls.Add(this.txtSignerName);
            this.ultraGroupBox1.Controls.Add(this.txtEMContractCode);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.H);
            this.ultraGroupBox1.Location = new System.Drawing.Point(2, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(820, 167);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Hợp đồng";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbPPOrder
            // 
            appearance8.TextVAlignAsString = "Middle";
            this.cbbPPOrder.Appearance = appearance8;
            this.cbbPPOrder.AutoSize = false;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbPPOrder.DisplayLayout.Appearance = appearance9;
            this.cbbPPOrder.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbPPOrder.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPPOrder.DisplayLayout.GroupByBox.Appearance = appearance10;
            appearance11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPPOrder.DisplayLayout.GroupByBox.BandLabelAppearance = appearance11;
            this.cbbPPOrder.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbPPOrder.DisplayLayout.GroupByBox.PromptAppearance = appearance12;
            this.cbbPPOrder.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbPPOrder.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbPPOrder.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbPPOrder.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.cbbPPOrder.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbPPOrder.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            this.cbbPPOrder.DisplayLayout.Override.CardAreaAppearance = appearance15;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbPPOrder.DisplayLayout.Override.CellAppearance = appearance16;
            this.cbbPPOrder.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbPPOrder.DisplayLayout.Override.CellPadding = 0;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbPPOrder.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Left";
            this.cbbPPOrder.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.cbbPPOrder.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbPPOrder.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.cbbPPOrder.DisplayLayout.Override.RowAppearance = appearance19;
            this.cbbPPOrder.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbPPOrder.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.cbbPPOrder.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbPPOrder.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbPPOrder.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbPPOrder.Location = new System.Drawing.Point(102, 109);
            this.cbbPPOrder.Name = "cbbPPOrder";
            this.cbbPPOrder.Size = new System.Drawing.Size(304, 22);
            this.cbbPPOrder.TabIndex = 43;
            this.cbbPPOrder.AfterCloseUp += new System.EventHandler(this.cbbPPOrder_AfterCloseUp);
            this.cbbPPOrder.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbPPOrder_RowSelected);
            this.cbbPPOrder.ValueChanged += new System.EventHandler(this.cbbPPOrder_ValueChanged);
            this.cbbPPOrder.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbPPOrder_ItemNotInList);
            // 
            // txtDueDate
            // 
            this.txtDueDate.AutoSize = false;
            this.txtDueDate.FormatString = "";
            this.txtDueDate.Location = new System.Drawing.Point(300, 136);
            this.txtDueDate.MaskInput = "dd/mm/yyyy";
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(106, 22);
            this.txtDueDate.TabIndex = 26;
            this.txtDueDate.Value = null;
            // 
            // txtDeliverDate
            // 
            this.txtDeliverDate.AutoSize = false;
            this.txtDeliverDate.FormatString = "";
            this.txtDeliverDate.Location = new System.Drawing.Point(102, 136);
            this.txtDeliverDate.MaskInput = "dd/mm/yyyy";
            this.txtDeliverDate.Name = "txtDeliverDate";
            this.txtDeliverDate.Size = new System.Drawing.Size(106, 22);
            this.txtDeliverDate.TabIndex = 25;
            this.txtDeliverDate.Value = null;
            // 
            // ultraLabel21
            // 
            appearance21.BackColor = System.Drawing.Color.Transparent;
            appearance21.TextVAlignAsString = "Middle";
            this.ultraLabel21.Appearance = appearance21;
            this.ultraLabel21.Location = new System.Drawing.Point(6, 137);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel21.TabIndex = 24;
            this.ultraLabel21.Text = "Ngày giao hàng";
            // 
            // ultraLabel20
            // 
            appearance22.BackColor = System.Drawing.Color.Transparent;
            appearance22.TextVAlignAsString = "Middle";
            this.ultraLabel20.Appearance = appearance22;
            this.ultraLabel20.Location = new System.Drawing.Point(214, 137);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel20.TabIndex = 23;
            this.ultraLabel20.Text = "Hạn thanh toán";
            // 
            // ultraLabel18
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            appearance23.TextVAlignAsString = "Middle";
            this.ultraLabel18.Appearance = appearance23;
            this.ultraLabel18.Location = new System.Drawing.Point(6, 109);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel18.TabIndex = 22;
            this.ultraLabel18.Text = "Số đơn hàng";
            // 
            // txtAmountOriginal
            // 
            this.txtAmountOriginal.AutoSize = false;
            this.txtAmountOriginal.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmountOriginal.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtAmountOriginal.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtAmountOriginal.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtAmountOriginal.Location = new System.Drawing.Point(475, 81);
            this.txtAmountOriginal.MinValue = "0";
            this.txtAmountOriginal.Name = "txtAmountOriginal";
            this.txtAmountOriginal.Size = new System.Drawing.Size(94, 22);
            this.txtAmountOriginal.TabIndex = 21;
            this.txtAmountOriginal.Text = " ";
            this.txtAmountOriginal.TextChanged += new System.EventHandler(this.txtOrg_TextChanged);
            // 
            // txtAmount
            // 
            appearance24.TextHAlignAsString = "Right";
            this.txtAmount.Appearance = appearance24;
            this.txtAmount.AutoSize = false;
            this.txtAmount.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtAmount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtAmount.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
            this.txtAmount.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtAmount.Location = new System.Drawing.Point(656, 81);
            this.txtAmount.MinValue = "0";
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PromptChar = ' ';
            this.txtAmount.Size = new System.Drawing.Size(150, 22);
            this.txtAmount.TabIndex = 20;
            this.txtAmount.Text = " 0";
            this.txtAmount.Visible = false;
            // 
            // txtExchangeRate
            // 
            this.txtExchangeRate.AutoSize = false;
            this.txtExchangeRate.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.txtExchangeRate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.txtExchangeRate.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this.txtExchangeRate.InputMask = " -n,nnn,nnn,nnn,nnn.nn";
            this.txtExchangeRate.Location = new System.Drawing.Point(258, 81);
            this.txtExchangeRate.MinValue = "0";
            this.txtExchangeRate.Name = "txtExchangeRate";
            this.txtExchangeRate.Size = new System.Drawing.Size(148, 22);
            this.txtExchangeRate.TabIndex = 19;
            this.txtExchangeRate.Text = " ";
            this.txtExchangeRate.Visible = false;
            this.txtExchangeRate.TextChanged += new System.EventHandler(this.txtRateEchange_TextChanged);
            // 
            // lblOrg
            // 
            appearance25.BackColor = System.Drawing.Color.Transparent;
            appearance25.TextVAlignAsString = "Middle";
            this.lblOrg.Appearance = appearance25;
            this.lblOrg.Location = new System.Drawing.Point(412, 81);
            this.lblOrg.Name = "lblOrg";
            this.lblOrg.Size = new System.Drawing.Size(57, 22);
            this.lblOrg.TabIndex = 16;
            this.lblOrg.Text = "Giá trị";
            // 
            // lblRate
            // 
            appearance26.BackColor = System.Drawing.Color.Transparent;
            appearance26.TextVAlignAsString = "Middle";
            this.lblRate.Appearance = appearance26;
            this.lblRate.Location = new System.Drawing.Point(208, 81);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(44, 22);
            this.lblRate.TabIndex = 14;
            this.lblRate.Text = "Tỷ giá";
            this.lblRate.Visible = false;
            // 
            // txtSignerTitle
            // 
            this.txtSignerTitle.AutoSize = false;
            this.txtSignerTitle.Location = new System.Drawing.Point(475, 55);
            this.txtSignerTitle.Name = "txtSignerTitle";
            this.txtSignerTitle.Size = new System.Drawing.Size(331, 22);
            this.txtSignerTitle.TabIndex = 13;
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.AutoSize = false;
            this.txtEffectiveDate.FormatString = "";
            this.txtEffectiveDate.Location = new System.Drawing.Point(707, 29);
            this.txtEffectiveDate.MaskInput = "dd/mm/yyyy";
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(99, 22);
            this.txtEffectiveDate.TabIndex = 12;
            // 
            // txtSignedDate
            // 
            this.txtSignedDate.AutoSize = false;
            this.txtSignedDate.FormatString = "";
            this.txtSignedDate.Location = new System.Drawing.Point(475, 29);
            this.txtSignedDate.MaskInput = "dd/mm/yyyy";
            this.txtSignedDate.Name = "txtSignedDate";
            this.txtSignedDate.Size = new System.Drawing.Size(106, 22);
            this.txtSignedDate.TabIndex = 11;
            // 
            // ultraLabel6
            // 
            appearance27.BackColor = System.Drawing.Color.Transparent;
            appearance27.TextVAlignAsString = "Middle";
            this.ultraLabel6.Appearance = appearance27;
            this.ultraLabel6.Location = new System.Drawing.Point(610, 29);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 22);
            this.ultraLabel6.TabIndex = 10;
            this.ultraLabel6.Text = "Ngày có hiệu lực";
            // 
            // ultraLabel5
            // 
            appearance28.BackColor = System.Drawing.Color.Transparent;
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance28;
            this.ultraLabel5.Location = new System.Drawing.Point(412, 55);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(57, 22);
            this.ultraLabel5.TabIndex = 9;
            this.ultraLabel5.Text = "Chức vụ";
            // 
            // ultraLabel4
            // 
            appearance29.BackColor = System.Drawing.Color.Transparent;
            appearance29.TextVAlignAsString = "Middle";
            this.ultraLabel4.Appearance = appearance29;
            this.ultraLabel4.Location = new System.Drawing.Point(412, 29);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(76, 22);
            this.ultraLabel4.TabIndex = 8;
            this.ultraLabel4.Text = "Ngày ký";
            // 
            // lblGiaTriQĐ
            // 
            appearance30.BackColor = System.Drawing.Color.Transparent;
            appearance30.TextVAlignAsString = "Middle";
            this.lblGiaTriQĐ.Appearance = appearance30;
            this.lblGiaTriQĐ.Location = new System.Drawing.Point(575, 81);
            this.lblGiaTriQĐ.Name = "lblGiaTriQĐ";
            this.lblGiaTriQĐ.Size = new System.Drawing.Size(75, 22);
            this.lblGiaTriQĐ.TabIndex = 7;
            this.lblGiaTriQĐ.Text = "Giá trị QĐ";
            this.lblGiaTriQĐ.Visible = false;
            // 
            // cbbCurrencyID
            // 
            this.cbbCurrencyID.AutoSize = false;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbCurrencyID.DisplayLayout.Appearance = appearance31;
            this.cbbCurrencyID.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbCurrencyID.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.Appearance = appearance32;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.BandLabelAppearance = appearance33;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance34.BackColor2 = System.Drawing.SystemColors.Control;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbCurrencyID.DisplayLayout.GroupByBox.PromptAppearance = appearance34;
            this.cbbCurrencyID.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbCurrencyID.DisplayLayout.MaxRowScrollRegions = 1;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCurrencyID.DisplayLayout.Override.ActiveCellAppearance = appearance35;
            appearance36.BackColor = System.Drawing.SystemColors.Highlight;
            appearance36.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbCurrencyID.DisplayLayout.Override.ActiveRowAppearance = appearance36;
            this.cbbCurrencyID.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbCurrencyID.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCurrencyID.DisplayLayout.Override.CardAreaAppearance = appearance37;
            appearance38.BorderColor = System.Drawing.Color.Silver;
            appearance38.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbCurrencyID.DisplayLayout.Override.CellAppearance = appearance38;
            this.cbbCurrencyID.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbCurrencyID.DisplayLayout.Override.CellPadding = 0;
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbCurrencyID.DisplayLayout.Override.GroupByRowAppearance = appearance39;
            appearance40.TextHAlignAsString = "Left";
            this.cbbCurrencyID.DisplayLayout.Override.HeaderAppearance = appearance40;
            this.cbbCurrencyID.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbCurrencyID.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            this.cbbCurrencyID.DisplayLayout.Override.RowAppearance = appearance41;
            this.cbbCurrencyID.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbCurrencyID.DisplayLayout.Override.TemplateAddRowAppearance = appearance42;
            this.cbbCurrencyID.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbCurrencyID.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbCurrencyID.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbCurrencyID.Location = new System.Drawing.Point(102, 81);
            this.cbbCurrencyID.Name = "cbbCurrencyID";
            this.cbbCurrencyID.Size = new System.Drawing.Size(100, 22);
            this.cbbCurrencyID.TabIndex = 6;
            this.cbbCurrencyID.Text = "VND";
            this.cbbCurrencyID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbMoney_RowSelected);
            this.cbbCurrencyID.TextChanged += new System.EventHandler(this.cbbMoney_TextChanged);
            // 
            // txtSignerName
            // 
            this.txtSignerName.AutoSize = false;
            this.txtSignerName.Location = new System.Drawing.Point(102, 55);
            this.txtSignerName.Name = "txtSignerName";
            this.txtSignerName.Size = new System.Drawing.Size(304, 22);
            this.txtSignerName.TabIndex = 4;
            // 
            // txtEMContractCode
            // 
            this.txtEMContractCode.AutoSize = false;
            this.txtEMContractCode.Location = new System.Drawing.Point(102, 29);
            this.txtEMContractCode.Name = "txtEMContractCode";
            this.txtEMContractCode.Size = new System.Drawing.Size(304, 22);
            this.txtEMContractCode.TabIndex = 3;
            // 
            // ultraLabel3
            // 
            appearance43.BackColor = System.Drawing.Color.Transparent;
            appearance43.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance43;
            this.ultraLabel3.Location = new System.Drawing.Point(6, 55);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Người ký";
            // 
            // ultraLabel2
            // 
            appearance44.BackColor = System.Drawing.Color.Transparent;
            appearance44.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance44;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 81);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(90, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Loại tiền";
            // 
            // H
            // 
            appearance45.BackColor = System.Drawing.Color.Transparent;
            appearance45.TextVAlignAsString = "Middle";
            this.H.Appearance = appearance45;
            this.H.Location = new System.Drawing.Point(6, 29);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(90, 22);
            this.H.TabIndex = 0;
            this.H.Text = "Số hợp đồng (*)";
            // 
            // ultraGroupBox2
            // 
            appearance46.FontData.BoldAsString = "True";
            appearance46.FontData.SizeInPoints = 11F;
            this.ultraGroupBox2.Appearance = appearance46;
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectBankAccount);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectBankName);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectFax);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectTel);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectAddress);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectName);
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectSignerName);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox2.Controls.Add(this.txtAccountingObjectTitle);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 169);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(817, 139);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "Bên bán";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectBankAccount
            // 
            appearance47.TextVAlignAsString = "Middle";
            this.cbbAccountingObjectBankAccount.Appearance = appearance47;
            this.cbbAccountingObjectBankAccount.AutoSize = false;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Appearance = appearance48;
            this.cbbAccountingObjectBankAccount.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cbbAccountingObjectBankAccount.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance49.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance49.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance49.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.Appearance = appearance49;
            appearance50.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BandLabelAppearance = appearance50;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance51.BackColor2 = System.Drawing.SystemColors.Control;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.GroupByBox.PromptAppearance = appearance51;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxColScrollRegions = 1;
            this.cbbAccountingObjectBankAccount.DisplayLayout.MaxRowScrollRegions = 1;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveCellAppearance = appearance52;
            appearance53.BackColor = System.Drawing.SystemColors.Highlight;
            appearance53.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.ActiveRowAppearance = appearance53;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CardAreaAppearance = appearance54;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            appearance55.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellAppearance = appearance55;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.CellPadding = 0;
            appearance56.BackColor = System.Drawing.SystemColors.Control;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.GroupByRowAppearance = appearance56;
            appearance57.TextHAlignAsString = "Left";
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderAppearance = appearance57;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.BorderColor = System.Drawing.Color.Silver;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowAppearance = appearance58;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cbbAccountingObjectBankAccount.DisplayLayout.Override.TemplateAddRowAppearance = appearance59;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cbbAccountingObjectBankAccount.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cbbAccountingObjectBankAccount.Location = new System.Drawing.Point(444, 111);
            this.cbbAccountingObjectBankAccount.Name = "cbbAccountingObjectBankAccount";
            this.cbbAccountingObjectBankAccount.Size = new System.Drawing.Size(125, 22);
            this.cbbAccountingObjectBankAccount.TabIndex = 42;
            this.cbbAccountingObjectBankAccount.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectBankAccount_RowSelected);
            // 
            // txtAccountingObjectBankName
            // 
            this.txtAccountingObjectBankName.AutoSize = false;
            this.txtAccountingObjectBankName.Location = new System.Drawing.Point(610, 110);
            this.txtAccountingObjectBankName.Name = "txtAccountingObjectBankName";
            this.txtAccountingObjectBankName.Size = new System.Drawing.Size(196, 22);
            this.txtAccountingObjectBankName.TabIndex = 41;
            // 
            // ultraLabel15
            // 
            appearance60.BackColor = System.Drawing.Color.Transparent;
            appearance60.TextVAlignAsString = "Middle";
            this.ultraLabel15.Appearance = appearance60;
            this.ultraLabel15.Location = new System.Drawing.Point(575, 110);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(29, 22);
            this.ultraLabel15.TabIndex = 40;
            this.ultraLabel15.Text = "Tại";
            // 
            // ultraLabel10
            // 
            appearance61.BackColor = System.Drawing.Color.Transparent;
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel10.Appearance = appearance61;
            this.ultraLabel10.Location = new System.Drawing.Point(388, 110);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(61, 22);
            this.ultraLabel10.TabIndex = 38;
            this.ultraLabel10.Text = "Tài khoản";
            // 
            // txtAccountingObjectFax
            // 
            this.txtAccountingObjectFax.AutoSize = false;
            this.txtAccountingObjectFax.Location = new System.Drawing.Point(258, 110);
            this.txtAccountingObjectFax.Name = "txtAccountingObjectFax";
            this.txtAccountingObjectFax.Size = new System.Drawing.Size(124, 22);
            this.txtAccountingObjectFax.TabIndex = 37;
            this.txtAccountingObjectFax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFax_KeyPress);
            // 
            // ultraLabel7
            // 
            appearance62.BackColor = System.Drawing.Color.Transparent;
            appearance62.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance62;
            this.ultraLabel7.Location = new System.Drawing.Point(224, 110);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(28, 22);
            this.ultraLabel7.TabIndex = 36;
            this.ultraLabel7.Text = "Fax";
            // 
            // txtAccountingObjectTel
            // 
            this.txtAccountingObjectTel.AutoSize = false;
            this.txtAccountingObjectTel.Location = new System.Drawing.Point(96, 110);
            this.txtAccountingObjectTel.Name = "txtAccountingObjectTel";
            this.txtAccountingObjectTel.Size = new System.Drawing.Size(122, 22);
            this.txtAccountingObjectTel.TabIndex = 35;
            this.txtAccountingObjectTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobile_KeyPress);
            // 
            // txtAccountingObjectAddress
            // 
            this.txtAccountingObjectAddress.AutoSize = false;
            this.txtAccountingObjectAddress.Location = new System.Drawing.Point(96, 83);
            this.txtAccountingObjectAddress.Name = "txtAccountingObjectAddress";
            this.txtAccountingObjectAddress.Size = new System.Drawing.Size(710, 22);
            this.txtAccountingObjectAddress.TabIndex = 34;
            // 
            // txtAccountingObjectName
            // 
            this.txtAccountingObjectName.AutoSize = false;
            this.txtAccountingObjectName.Location = new System.Drawing.Point(412, 29);
            this.txtAccountingObjectName.Name = "txtAccountingObjectName";
            this.txtAccountingObjectName.Size = new System.Drawing.Size(394, 22);
            this.txtAccountingObjectName.TabIndex = 33;
            // 
            // cbbAccountingObjectID
            // 
            this.cbbAccountingObjectID.AutoSize = false;
            appearance63.Image = global::Accounting.Properties.Resources.plus;
            editorButton2.Appearance = appearance63;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            appearance64.Image = global::Accounting.Properties.Resources.plus_press;
            editorButton2.PressedAppearance = appearance64;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton2);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(96, 29);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(247, 22);
            this.cbbAccountingObjectID.TabIndex = 32;
            this.cbbAccountingObjectID.AfterCloseUp += new System.EventHandler(this.cbbAccountingObjectID_AfterCloseUp);
            this.cbbAccountingObjectID.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cbbAccountingObjectID_RowSelected);
            this.cbbAccountingObjectID.ItemNotInList += new Infragistics.Win.UltraWinGrid.ItemNotInListEventHandler(this.cbbAccountingObjectID_ItemNotInList);
            this.cbbAccountingObjectID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.cbbAccountingObjectID_EditorButtonClick);
            // 
            // ultraLabel14
            // 
            appearance65.BackColor = System.Drawing.Color.Transparent;
            appearance65.TextVAlignAsString = "Middle";
            this.ultraLabel14.Appearance = appearance65;
            this.ultraLabel14.Location = new System.Drawing.Point(6, 110);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(84, 22);
            this.ultraLabel14.TabIndex = 14;
            this.ultraLabel14.Text = "Điện thoại";
            // 
            // txtAccountingObjectSignerName
            // 
            this.txtAccountingObjectSignerName.AutoSize = false;
            this.txtAccountingObjectSignerName.Location = new System.Drawing.Point(412, 56);
            this.txtAccountingObjectSignerName.Name = "txtAccountingObjectSignerName";
            this.txtAccountingObjectSignerName.Size = new System.Drawing.Size(394, 22);
            this.txtAccountingObjectSignerName.TabIndex = 13;
            // 
            // ultraLabel8
            // 
            appearance66.BackColor = System.Drawing.Color.Transparent;
            appearance66.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance66;
            this.ultraLabel8.Location = new System.Drawing.Point(349, 56);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(57, 22);
            this.ultraLabel8.TabIndex = 9;
            this.ultraLabel8.Text = "Chức vụ";
            // 
            // ultraLabel9
            // 
            appearance67.BackColor = System.Drawing.Color.Transparent;
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel9.Appearance = appearance67;
            this.ultraLabel9.Location = new System.Drawing.Point(349, 29);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(57, 22);
            this.ultraLabel9.TabIndex = 8;
            this.ultraLabel9.Text = "Tên NCC";
            // 
            // txtAccountingObjectTitle
            // 
            this.txtAccountingObjectTitle.AutoSize = false;
            this.txtAccountingObjectTitle.Location = new System.Drawing.Point(96, 56);
            this.txtAccountingObjectTitle.Name = "txtAccountingObjectTitle";
            this.txtAccountingObjectTitle.Size = new System.Drawing.Size(247, 22);
            this.txtAccountingObjectTitle.TabIndex = 4;
            // 
            // ultraLabel11
            // 
            appearance68.BackColor = System.Drawing.Color.Transparent;
            appearance68.TextVAlignAsString = "Middle";
            this.ultraLabel11.Appearance = appearance68;
            this.ultraLabel11.Location = new System.Drawing.Point(6, 56);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(84, 22);
            this.ultraLabel11.TabIndex = 2;
            this.ultraLabel11.Text = "Đại Diện";
            // 
            // ultraLabel12
            // 
            appearance69.BackColor = System.Drawing.Color.Transparent;
            appearance69.TextVAlignAsString = "Middle";
            this.ultraLabel12.Appearance = appearance69;
            this.ultraLabel12.Location = new System.Drawing.Point(6, 83);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(84, 22);
            this.ultraLabel12.TabIndex = 1;
            this.ultraLabel12.Text = "Địa chỉ";
            // 
            // ultraLabel13
            // 
            appearance70.BackColor = System.Drawing.Color.Transparent;
            appearance70.TextVAlignAsString = "Middle";
            this.ultraLabel13.Appearance = appearance70;
            this.ultraLabel13.Location = new System.Drawing.Point(6, 29);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(84, 22);
            this.ultraLabel13.TabIndex = 0;
            this.ultraLabel13.Text = "Mã NCC (*)";
            // 
            // ultraGroupBox4
            // 
            appearance71.FontData.BoldAsString = "True";
            appearance71.FontData.SizeInPoints = 11F;
            this.ultraGroupBox4.Appearance = appearance71;
            this.ultraGroupBox4.Controls.Add(this.uGridDk);
            this.ultraGroupBox4.Controls.Add(this.ultraGroupBox5);
            this.ultraGroupBox4.Location = new System.Drawing.Point(2, 401);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(818, 195);
            this.ultraGroupBox4.TabIndex = 0;
            this.ultraGroupBox4.Text = "Đính kèm ";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // uGridDk
            // 
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            appearance72.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDk.DisplayLayout.Appearance = appearance72;
            this.uGridDk.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.uGridDk.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDk.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.uGridDk.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance73.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance73.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance73.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDk.DisplayLayout.GroupByBox.Appearance = appearance73;
            appearance74.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDk.DisplayLayout.GroupByBox.BandLabelAppearance = appearance74;
            this.uGridDk.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance75.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance75.BackColor2 = System.Drawing.SystemColors.Control;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDk.DisplayLayout.GroupByBox.PromptAppearance = appearance75;
            this.uGridDk.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDk.DisplayLayout.MaxRowScrollRegions = 1;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            appearance76.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDk.DisplayLayout.Override.ActiveCellAppearance = appearance76;
            appearance77.BackColor = System.Drawing.SystemColors.Highlight;
            appearance77.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDk.DisplayLayout.Override.ActiveRowAppearance = appearance77;
            this.uGridDk.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDk.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDk.DisplayLayout.Override.CardAreaAppearance = appearance78;
            appearance79.BorderColor = System.Drawing.Color.Silver;
            appearance79.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDk.DisplayLayout.Override.CellAppearance = appearance79;
            this.uGridDk.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDk.DisplayLayout.Override.CellPadding = 0;
            appearance80.BackColor = System.Drawing.SystemColors.Control;
            appearance80.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance80.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance80.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDk.DisplayLayout.Override.GroupByRowAppearance = appearance80;
            this.uGridDk.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDk.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            this.uGridDk.DisplayLayout.Override.RowAppearance = appearance81;
            this.uGridDk.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDk.DisplayLayout.Override.TemplateAddRowAppearance = appearance82;
            this.uGridDk.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDk.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDk.DisplayLayout.UseFixedHeaders = true;
            this.uGridDk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDk.Location = new System.Drawing.Point(3, 63);
            this.uGridDk.Name = "uGridDk";
            this.uGridDk.Size = new System.Drawing.Size(812, 129);
            this.uGridDk.TabIndex = 1;
            this.uGridDk.Text = "ultraGrid1";
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.btnDelete);
            this.ultraGroupBox5.Controls.Add(this.btnEdit);
            this.ultraGroupBox5.Controls.Add(this.btnAdd);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox5.Location = new System.Drawing.Point(3, 21);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(812, 42);
            this.ultraGroupBox5.TabIndex = 0;
            this.ultraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnDelete
            // 
            appearance83.Image = global::Accounting.Properties.Resources.xoa1_xoa_du_lieu_ke_toan_khoi_database_;
            this.btnDelete.Appearance = appearance83;
            this.btnDelete.Location = new System.Drawing.Point(168, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            appearance84.Image = global::Accounting.Properties.Resources.ubtnEdit1;
            this.btnEdit.Appearance = appearance84;
            this.btnEdit.Location = new System.Drawing.Point(87, 6);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 30);
            this.btnEdit.TabIndex = 13;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            appearance85.Image = global::Accounting.Properties.Resources.them_moi_1_dulieu_kt_;
            this.btnAdd.Appearance = appearance85;
            this.btnAdd.Location = new System.Drawing.Point(6, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ultraPanel3
            // 
            this.ultraPanel3.Location = new System.Drawing.Point(3, 364);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(824, 35);
            this.ultraPanel3.TabIndex = 44;
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.Location = new System.Drawing.Point(3, 11);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(820, 167);
            this.ultraPanel1.TabIndex = 0;
            // 
            // ultraPanel4
            // 
            this.ultraPanel4.Location = new System.Drawing.Point(3, 448);
            this.ultraPanel4.Name = "ultraPanel4";
            this.ultraPanel4.Size = new System.Drawing.Size(814, 144);
            this.ultraPanel4.TabIndex = 45;
            // 
            // ultraPanel2
            // 
            this.ultraPanel2.Location = new System.Drawing.Point(5, 196);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(814, 112);
            this.ultraPanel2.TabIndex = 1;
            // 
            // txtName
            // 
            this.txtName.AutoSize = false;
            this.txtName.Location = new System.Drawing.Point(99, 308);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtName.Size = new System.Drawing.Size(710, 29);
            this.txtName.TabIndex = 43;
            // 
            // ultraLabel16
            // 
            appearance86.BackColor = System.Drawing.Color.Transparent;
            appearance86.TextVAlignAsString = "Middle";
            this.ultraLabel16.Appearance = appearance86;
            this.ultraLabel16.Location = new System.Drawing.Point(9, 309);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(75, 22);
            this.ultraLabel16.TabIndex = 42;
            this.ultraLabel16.Text = "Trích Yếu :";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridEMContractMG);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(823, 596);
            // 
            // uGridEMContractMG
            // 
            this.uGridEMContractMG.ContextMenuStrip = this.contextMenuStrip1;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEMContractMG.DisplayLayout.Appearance = appearance87;
            this.uGridEMContractMG.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEMContractMG.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance88.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance88.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance88.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.Appearance = appearance88;
            appearance89.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.BandLabelAppearance = appearance89;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance90.BackColor2 = System.Drawing.SystemColors.Control;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEMContractMG.DisplayLayout.GroupByBox.PromptAppearance = appearance90;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEMContractMG.DisplayLayout.Override.ActiveCellAppearance = appearance91;
            appearance92.BackColor = System.Drawing.SystemColors.Highlight;
            appearance92.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEMContractMG.DisplayLayout.Override.ActiveRowAppearance = appearance92;
            this.uGridEMContractMG.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEMContractMG.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractMG.DisplayLayout.Override.CardAreaAppearance = appearance93;
            appearance94.BorderColor = System.Drawing.Color.Silver;
            appearance94.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEMContractMG.DisplayLayout.Override.CellAppearance = appearance94;
            this.uGridEMContractMG.DisplayLayout.Override.CellPadding = 0;
            appearance95.BackColor = System.Drawing.SystemColors.Control;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEMContractMG.DisplayLayout.Override.GroupByRowAppearance = appearance95;
            appearance96.TextHAlignAsString = "Left";
            this.uGridEMContractMG.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.uGridEMContractMG.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.uGridEMContractMG.DisplayLayout.Override.RowAppearance = appearance97;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEMContractMG.DisplayLayout.Override.TemplateAddRowAppearance = appearance98;
            this.uGridEMContractMG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridEMContractMG.Location = new System.Drawing.Point(0, 0);
            this.uGridEMContractMG.Name = "uGridEMContractMG";
            this.uGridEMContractMG.Size = new System.Drawing.Size(823, 596);
            this.uGridEMContractMG.TabIndex = 2;
            this.uGridEMContractMG.Text = "ultraGrid1";
            this.uGridEMContractMG.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEMContractMG_AfterCellUpdate);
            this.uGridEMContractMG.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEMContractMG_CellChange);
            this.uGridEMContractMG.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridEMContractMG_Error);
            this.uGridEMContractMG.SummaryValueChanged += new Infragistics.Win.UltraWinGrid.SummaryValueChangedEventHandler(this.uGridEMContractMG_SummaryValueChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(168, 26);
            // 
            // tsmDelete
            // 
            this.tsmDelete.Name = "tsmDelete";
            this.tsmDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmDelete.Size = new System.Drawing.Size(167, 22);
            this.tsmDelete.Text = "Xóa dòng";
            this.tsmDelete.Click += new System.EventHandler(this.tsmDelete_Click);
            // 
            // ultraPanel5
            // 
            // 
            // ultraPanel5.ClientArea
            // 
            this.ultraPanel5.ClientArea.Controls.Add(this.ultraGroupBox6);
            this.ultraPanel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraPanel5.Location = new System.Drawing.Point(0, 613);
            this.ultraPanel5.Name = "ultraPanel5";
            this.ultraPanel5.Size = new System.Drawing.Size(826, 39);
            this.ultraPanel5.TabIndex = 46;
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.btnClose);
            this.ultraGroupBox6.Controls.Add(this.btnSave);
            this.ultraGroupBox6.Location = new System.Drawing.Point(0, 3);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(828, 36);
            this.ultraGroupBox6.TabIndex = 0;
            this.ultraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // btnClose
            // 
            appearance99.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance99;
            this.btnClose.Location = new System.Drawing.Point(737, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 302;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance100.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance100;
            this.btnSave.Location = new System.Drawing.Point(650, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 301;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // utEMContractBuy
            // 
            this.utEMContractBuy.Controls.Add(this.ultraTabSharedControlsPage1);
            this.utEMContractBuy.Controls.Add(this.ultraTabPageControl1);
            this.utEMContractBuy.Controls.Add(this.ultraTabPageControl2);
            this.utEMContractBuy.Location = new System.Drawing.Point(3, 0);
            this.utEMContractBuy.Name = "utEMContractBuy";
            this.utEMContractBuy.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.utEMContractBuy.Size = new System.Drawing.Size(825, 619);
            this.utEMContractBuy.TabIndex = 22;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "&1. Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "&2. Hàng hóa, dịch vụ";
            this.utEMContractBuy.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.utEMContractBuy.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(823, 596);
            // 
            // FEMContractBuyDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 652);
            this.Controls.Add(this.utEMContractBuy);
            this.Controls.Add(this.ultraPanel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FEMContractBuyDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hợp đồng mua";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FEMContractBuyDetail_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FEMContractBuyDetail_FormClosed);
            this.Load += new System.EventHandler(this.FEMContractBuyDetail_Load);
            this.Resize += new System.EventHandler(this.FEMContractBuyDetail_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbContractState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClosedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbPPOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliverDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCurrencyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMContractCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectBankName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectSignerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountingObjectTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel4.ResumeLayout(false);
            this.ultraPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEMContractMG)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ultraPanel5.ClientArea.ResumeLayout(false);
            this.ultraPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utEMContractBuy)).EndInit();
            this.utEMContractBuy.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraPanel ultraPanel5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDk;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraPanel ultraPanel4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbContractState;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtClosedDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectBankName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectFax;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectName;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectSignerName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtAccountingObjectTitle;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmountOriginal;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtAmount;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit txtExchangeRate;
        private Infragistics.Win.Misc.UltraLabel lblOrg;
        private Infragistics.Win.Misc.UltraLabel lblRate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerTitle;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtEffectiveDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtSignedDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel lblGiaTriQĐ;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbCurrencyID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtSignerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtEMContractCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel H;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.Misc.UltraButton btnEdit;
        private Infragistics.Win.Misc.UltraButton btnDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtClosedReason;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectBankAccount;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl utEMContractBuy;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbPPOrder;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDueDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDeliverDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEMContractMG;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmDelete;
    }
}