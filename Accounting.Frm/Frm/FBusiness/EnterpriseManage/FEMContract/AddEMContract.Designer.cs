﻿namespace Accounting
{
    partial class AddEMContract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraDateTimeEditor3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCombo4 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTextEditor16 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor17 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCombo3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cbbAccountingObjectID = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCombo2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraMaskedEdit3 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraMaskedEdit2 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraMaskedEdit1 = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraCombo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox4);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl1.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(919, 527);
            // 
            // ultraGroupBox4
            // 
            appearance1.FontData.BoldAsString = "True";
            appearance1.FontData.SizeInPoints = 11F;
            this.ultraGroupBox4.Appearance = appearance1;
            this.ultraGroupBox4.Controls.Add(this.ultraDateTimeEditor3);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel29);
            this.ultraGroupBox4.Controls.Add(this.ultraCombo4);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor16);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor17);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel28);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel27);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel26);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor15);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor14);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor13);
            this.ultraGroupBox4.Controls.Add(this.ultraTextEditor12);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel25);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel24);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel23);
            this.ultraGroupBox4.Controls.Add(this.ultraLabel22);
            this.ultraGroupBox4.Location = new System.Drawing.Point(2, 307);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(915, 129);
            this.ultraGroupBox4.TabIndex = 2;
            this.ultraGroupBox4.Text = "Thông tin khác";
            this.ultraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraDateTimeEditor3
            // 
            this.ultraDateTimeEditor3.Location = new System.Drawing.Point(779, 24);
            this.ultraDateTimeEditor3.Name = "ultraDateTimeEditor3";
            this.ultraDateTimeEditor3.Size = new System.Drawing.Size(132, 21);
            this.ultraDateTimeEditor3.TabIndex = 52;
            // 
            // ultraLabel29
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel29.Appearance = appearance2;
            this.ultraLabel29.Location = new System.Drawing.Point(731, 24);
            this.ultraLabel29.Name = "ultraLabel29";
            this.ultraLabel29.Size = new System.Drawing.Size(39, 23);
            this.ultraLabel29.TabIndex = 51;
            this.ultraLabel29.Text = "Ngày :";
            // 
            // ultraCombo4
            // 
            appearance3.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraCombo4.ButtonsRight.Add(editorButton1);
            this.ultraCombo4.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo4.Location = new System.Drawing.Point(552, 23);
            this.ultraCombo4.Name = "ultraCombo4";
            this.ultraCombo4.NullText = "<chọn dữ liệu>";
            this.ultraCombo4.Size = new System.Drawing.Size(173, 22);
            this.ultraCombo4.TabIndex = 50;
            // 
            // ultraTextEditor16
            // 
            this.ultraTextEditor16.Location = new System.Drawing.Point(551, 51);
            this.ultraTextEditor16.Name = "ultraTextEditor16";
            this.ultraTextEditor16.Size = new System.Drawing.Size(358, 21);
            this.ultraTextEditor16.TabIndex = 48;
            // 
            // ultraTextEditor17
            // 
            this.ultraTextEditor17.Location = new System.Drawing.Point(551, 76);
            this.ultraTextEditor17.Multiline = true;
            this.ultraTextEditor17.Name = "ultraTextEditor17";
            this.ultraTextEditor17.Size = new System.Drawing.Size(358, 46);
            this.ultraTextEditor17.TabIndex = 49;
            // 
            // ultraLabel28
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel28.Appearance = appearance4;
            this.ultraLabel28.Location = new System.Drawing.Point(463, 80);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel28.TabIndex = 46;
            this.ultraLabel28.Text = "Diễn giải :";
            // 
            // ultraLabel27
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel27.Appearance = appearance5;
            this.ultraLabel27.Location = new System.Drawing.Point(465, 53);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel27.TabIndex = 45;
            this.ultraLabel27.Text = "Lý do :";
            // 
            // ultraLabel26
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel26.Appearance = appearance6;
            this.ultraLabel26.Location = new System.Drawing.Point(462, 24);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel26.TabIndex = 44;
            this.ultraLabel26.Text = "Tình trạng :";
            // 
            // ultraTextEditor15
            // 
            this.ultraTextEditor15.Location = new System.Drawing.Point(99, 101);
            this.ultraTextEditor15.Name = "ultraTextEditor15";
            this.ultraTextEditor15.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor15.TabIndex = 43;
            // 
            // ultraTextEditor14
            // 
            this.ultraTextEditor14.Location = new System.Drawing.Point(99, 20);
            this.ultraTextEditor14.Name = "ultraTextEditor14";
            this.ultraTextEditor14.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor14.TabIndex = 42;
            // 
            // ultraTextEditor13
            // 
            this.ultraTextEditor13.Location = new System.Drawing.Point(99, 47);
            this.ultraTextEditor13.Name = "ultraTextEditor13";
            this.ultraTextEditor13.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor13.TabIndex = 41;
            // 
            // ultraTextEditor12
            // 
            this.ultraTextEditor12.Location = new System.Drawing.Point(99, 74);
            this.ultraTextEditor12.Name = "ultraTextEditor12";
            this.ultraTextEditor12.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor12.TabIndex = 40;
            // 
            // ultraLabel25
            // 
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel25.Appearance = appearance7;
            this.ultraLabel25.Location = new System.Drawing.Point(6, 48);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel25.TabIndex = 5;
            this.ultraLabel25.Text = "Điện thoại :";
            // 
            // ultraLabel24
            // 
            appearance8.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel24.Appearance = appearance8;
            this.ultraLabel24.Location = new System.Drawing.Point(6, 71);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel24.TabIndex = 4;
            this.ultraLabel24.Text = "Người ký :";
            // 
            // ultraLabel23
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel23.Appearance = appearance9;
            this.ultraLabel23.Location = new System.Drawing.Point(6, 100);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel23.TabIndex = 3;
            this.ultraLabel23.Text = "Chức vụ :";
            // 
            // ultraLabel22
            // 
            appearance10.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel22.Appearance = appearance10;
            this.ultraLabel22.Location = new System.Drawing.Point(6, 24);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel22.TabIndex = 2;
            this.ultraLabel22.Text = "Người liên hệ :";
            // 
            // ultraGroupBox3
            // 
            appearance11.FontData.BoldAsString = "True";
            appearance11.FontData.SizeInPoints = 11F;
            this.ultraGroupBox3.Appearance = appearance11;
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor11);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel21);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor10);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel20);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor9);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel19);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor8);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel18);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor7);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel17);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor6);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel16);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel15);
            this.ultraGroupBox3.Controls.Add(this.ultraCombo3);
            this.ultraGroupBox3.Controls.Add(this.ultraTextEditor5);
            this.ultraGroupBox3.Controls.Add(this.ultraLabel14);
            this.ultraGroupBox3.Location = new System.Drawing.Point(3, 173);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(915, 131);
            this.ultraGroupBox3.TabIndex = 1;
            this.ultraGroupBox3.Text = "Bên mua";
            this.ultraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraTextEditor11
            // 
            this.ultraTextEditor11.Location = new System.Drawing.Point(551, 101);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.Size = new System.Drawing.Size(358, 21);
            this.ultraTextEditor11.TabIndex = 47;
            // 
            // ultraLabel21
            // 
            appearance12.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel21.Appearance = appearance12;
            this.ultraLabel21.Location = new System.Drawing.Point(461, 107);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(81, 23);
            this.ultraLabel21.TabIndex = 46;
            this.ultraLabel21.Text = "Mở tại NH :";
            // 
            // ultraTextEditor10
            // 
            this.ultraTextEditor10.Location = new System.Drawing.Point(552, 74);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.Size = new System.Drawing.Size(358, 21);
            this.ultraTextEditor10.TabIndex = 45;
            // 
            // ultraLabel20
            // 
            appearance13.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel20.Appearance = appearance13;
            this.ultraLabel20.Location = new System.Drawing.Point(461, 78);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(81, 23);
            this.ultraLabel20.TabIndex = 44;
            this.ultraLabel20.Text = "Tài khoản NH :";
            // 
            // ultraTextEditor9
            // 
            this.ultraTextEditor9.Location = new System.Drawing.Point(551, 47);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.Size = new System.Drawing.Size(358, 21);
            this.ultraTextEditor9.TabIndex = 43;
            // 
            // ultraLabel19
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel19.Appearance = appearance14;
            this.ultraLabel19.Location = new System.Drawing.Point(464, 51);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(81, 23);
            this.ultraLabel19.TabIndex = 42;
            this.ultraLabel19.Text = "Chức vụ :";
            // 
            // ultraTextEditor8
            // 
            this.ultraTextEditor8.Location = new System.Drawing.Point(551, 20);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.Size = new System.Drawing.Size(358, 21);
            this.ultraTextEditor8.TabIndex = 41;
            // 
            // ultraLabel18
            // 
            appearance15.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel18.Appearance = appearance15;
            this.ultraLabel18.Location = new System.Drawing.Point(464, 24);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(81, 23);
            this.ultraLabel18.TabIndex = 40;
            this.ultraLabel18.Text = "Đại diện :";
            // 
            // ultraTextEditor7
            // 
            this.ultraTextEditor7.Location = new System.Drawing.Point(98, 107);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor7.TabIndex = 39;
            // 
            // ultraLabel17
            // 
            appearance16.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel17.Appearance = appearance16;
            this.ultraLabel17.Location = new System.Drawing.Point(5, 105);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(72, 23);
            this.ultraLabel17.TabIndex = 38;
            this.ultraLabel17.Text = "Địa chỉ :";
            // 
            // ultraTextEditor6
            // 
            this.ultraTextEditor6.Location = new System.Drawing.Point(98, 80);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor6.TabIndex = 37;
            // 
            // ultraLabel16
            // 
            appearance17.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel16.Appearance = appearance17;
            this.ultraLabel16.Location = new System.Drawing.Point(5, 79);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(72, 23);
            this.ultraLabel16.TabIndex = 36;
            this.ultraLabel16.Text = "Điện thoại :";
            // 
            // ultraLabel15
            // 
            appearance18.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel15.Appearance = appearance18;
            this.ultraLabel15.Location = new System.Drawing.Point(5, 53);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(72, 20);
            this.ultraLabel15.TabIndex = 35;
            this.ultraLabel15.Text = "Tên KH :";
            // 
            // ultraCombo3
            // 
            appearance19.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton2.Appearance = appearance19;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.ultraCombo3.ButtonsRight.Add(editorButton2);
            this.ultraCombo3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.ultraCombo3.Location = new System.Drawing.Point(98, 24);
            this.ultraCombo3.Name = "ultraCombo3";
            this.ultraCombo3.NullText = "<chọn dữ liệu>";
            this.ultraCombo3.Size = new System.Drawing.Size(358, 22);
            this.ultraCombo3.TabIndex = 34;
            // 
            // ultraTextEditor5
            // 
            this.ultraTextEditor5.Location = new System.Drawing.Point(98, 53);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.Size = new System.Drawing.Size(357, 21);
            this.ultraTextEditor5.TabIndex = 2;
            // 
            // ultraLabel14
            // 
            appearance20.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel14.Appearance = appearance20;
            this.ultraLabel14.Location = new System.Drawing.Point(6, 24);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel14.TabIndex = 1;
            this.ultraLabel14.Text = "Mã KH :";
            // 
            // ultraGroupBox2
            // 
            appearance21.FontData.BoldAsString = "True";
            appearance21.FontData.SizeInPoints = 11F;
            this.ultraGroupBox2.Appearance = appearance21;
            this.ultraGroupBox2.Controls.Add(this.cbbAccountingObjectID);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel13);
            this.ultraGroupBox2.Controls.Add(this.ultraCombo2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel12);
            this.ultraGroupBox2.Controls.Add(this.ultraMaskedEdit3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel11);
            this.ultraGroupBox2.Controls.Add(this.ultraMaskedEdit2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel10);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel9);
            this.ultraGroupBox2.Controls.Add(this.ultraTextEditor4);
            this.ultraGroupBox2.Controls.Add(this.ultraMaskedEdit1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel8);
            this.ultraGroupBox2.Controls.Add(this.ultraCombo1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel7);
            this.ultraGroupBox2.Controls.Add(this.ultraTextEditor3);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel6);
            this.ultraGroupBox2.Controls.Add(this.ultraDateTimeEditor2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel5);
            this.ultraGroupBox2.Controls.Add(this.ultraDateTimeEditor1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel4);
            this.ultraGroupBox2.Controls.Add(this.ultraTextEditor2);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.ultraComboEditor1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox2.Controls.Add(this.ultraTextEditor1);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel1);
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(915, 167);
            this.ultraGroupBox2.TabIndex = 0;
            this.ultraGroupBox2.Text = "Hợp đồng";
            this.ultraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // cbbAccountingObjectID
            // 
            appearance22.Image = global::Accounting.Properties.Resources.ubtnAdd4;
            editorButton3.Appearance = appearance22;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.cbbAccountingObjectID.ButtonsRight.Add(editorButton3);
            this.cbbAccountingObjectID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
            this.cbbAccountingObjectID.Location = new System.Drawing.Point(551, 120);
            this.cbbAccountingObjectID.Name = "cbbAccountingObjectID";
            this.cbbAccountingObjectID.NullText = "<chọn dữ liệu>";
            this.cbbAccountingObjectID.Size = new System.Drawing.Size(358, 22);
            this.cbbAccountingObjectID.TabIndex = 33;
            // 
            // ultraLabel13
            // 
            appearance23.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel13.Appearance = appearance23;
            this.ultraLabel13.Location = new System.Drawing.Point(462, 124);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(91, 23);
            this.ultraLabel13.TabIndex = 25;
            this.ultraLabel13.Text = "Người thực hiện :";
            // 
            // ultraCombo2
            // 
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraCombo2.DisplayLayout.Appearance = appearance24;
            this.ultraCombo2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCombo2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraCombo2.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraCombo2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.ultraCombo2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraCombo2.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.ultraCombo2.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraCombo2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraCombo2.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraCombo2.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.ultraCombo2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraCombo2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.ultraCombo2.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraCombo2.DisplayLayout.Override.CellAppearance = appearance31;
            this.ultraCombo2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraCombo2.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraCombo2.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.ultraCombo2.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.ultraCombo2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraCombo2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.ultraCombo2.DisplayLayout.Override.RowAppearance = appearance34;
            this.ultraCombo2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraCombo2.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.ultraCombo2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraCombo2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraCombo2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraCombo2.Location = new System.Drawing.Point(551, 92);
            this.ultraCombo2.Name = "ultraCombo2";
            this.ultraCombo2.Size = new System.Drawing.Size(358, 22);
            this.ultraCombo2.TabIndex = 24;
            // 
            // ultraLabel12
            // 
            appearance36.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel12.Appearance = appearance36;
            this.ultraLabel12.Location = new System.Drawing.Point(462, 96);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(91, 23);
            this.ultraLabel12.TabIndex = 23;
            this.ultraLabel12.Text = "Đơn vị thực hiện :";
            // 
            // ultraMaskedEdit3
            // 
            this.ultraMaskedEdit3.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.ultraMaskedEdit3.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.ultraMaskedEdit3.InputMask = "-nnn,nnn,nnn,nnn.nn";
            this.ultraMaskedEdit3.Location = new System.Drawing.Point(551, 69);
            this.ultraMaskedEdit3.Name = "ultraMaskedEdit3";
            this.ultraMaskedEdit3.Size = new System.Drawing.Size(358, 20);
            this.ultraMaskedEdit3.TabIndex = 22;
            // 
            // ultraLabel11
            // 
            appearance37.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel11.Appearance = appearance37;
            this.ultraLabel11.Location = new System.Drawing.Point(462, 69);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(83, 23);
            this.ultraLabel11.TabIndex = 21;
            this.ultraLabel11.Text = "Giá trị thanh lý :";
            // 
            // ultraMaskedEdit2
            // 
            this.ultraMaskedEdit2.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.ultraMaskedEdit2.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.ultraMaskedEdit2.InputMask = "-nnn,nnn,nnn,nnn.nn";
            this.ultraMaskedEdit2.Location = new System.Drawing.Point(748, 46);
            this.ultraMaskedEdit2.Name = "ultraMaskedEdit2";
            this.ultraMaskedEdit2.Size = new System.Drawing.Size(161, 20);
            this.ultraMaskedEdit2.TabIndex = 20;
            // 
            // ultraLabel10
            // 
            appearance38.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel10.Appearance = appearance38;
            this.ultraLabel10.Location = new System.Drawing.Point(651, 46);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(91, 23);
            this.ultraLabel10.TabIndex = 19;
            this.ultraLabel10.Text = "Giá trị HĐ QĐ :";
            // 
            // ultraLabel9
            // 
            appearance39.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel9.Appearance = appearance39;
            this.ultraLabel9.Location = new System.Drawing.Point(462, 49);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(63, 23);
            this.ultraLabel9.TabIndex = 18;
            this.ultraLabel9.Text = "Tỷ giá :";
            // 
            // ultraTextEditor4
            // 
            this.ultraTextEditor4.Location = new System.Drawing.Point(551, 45);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.Size = new System.Drawing.Size(94, 21);
            this.ultraTextEditor4.TabIndex = 17;
            // 
            // ultraMaskedEdit1
            // 
            this.ultraMaskedEdit1.DisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
            this.ultraMaskedEdit1.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Currency;
            this.ultraMaskedEdit1.InputMask = "-nnn,nnn,nnn,nnn.nn";
            this.ultraMaskedEdit1.Location = new System.Drawing.Point(748, 24);
            this.ultraMaskedEdit1.Name = "ultraMaskedEdit1";
            this.ultraMaskedEdit1.Size = new System.Drawing.Size(161, 20);
            this.ultraMaskedEdit1.TabIndex = 16;
            // 
            // ultraLabel8
            // 
            appearance40.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel8.Appearance = appearance40;
            this.ultraLabel8.Location = new System.Drawing.Point(651, 24);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(91, 23);
            this.ultraLabel8.TabIndex = 14;
            this.ultraLabel8.Text = "Giá trị hợp đồng :";
            // 
            // ultraCombo1
            // 
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraCombo1.DisplayLayout.Appearance = appearance41;
            this.ultraCombo1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCombo1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraCombo1.DisplayLayout.GroupByBox.Appearance = appearance42;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraCombo1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance43;
            this.ultraCombo1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance44.BackColor2 = System.Drawing.SystemColors.Control;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraCombo1.DisplayLayout.GroupByBox.PromptAppearance = appearance44;
            this.ultraCombo1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraCombo1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraCombo1.DisplayLayout.Override.ActiveCellAppearance = appearance45;
            appearance46.BackColor = System.Drawing.SystemColors.Highlight;
            appearance46.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraCombo1.DisplayLayout.Override.ActiveRowAppearance = appearance46;
            this.ultraCombo1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraCombo1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            this.ultraCombo1.DisplayLayout.Override.CardAreaAppearance = appearance47;
            appearance48.BorderColor = System.Drawing.Color.Silver;
            appearance48.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraCombo1.DisplayLayout.Override.CellAppearance = appearance48;
            this.ultraCombo1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraCombo1.DisplayLayout.Override.CellPadding = 0;
            appearance49.BackColor = System.Drawing.SystemColors.Control;
            appearance49.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance49.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraCombo1.DisplayLayout.Override.GroupByRowAppearance = appearance49;
            appearance50.TextHAlignAsString = "Left";
            this.ultraCombo1.DisplayLayout.Override.HeaderAppearance = appearance50;
            this.ultraCombo1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraCombo1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.Color.Silver;
            this.ultraCombo1.DisplayLayout.Override.RowAppearance = appearance51;
            this.ultraCombo1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraCombo1.DisplayLayout.Override.TemplateAddRowAppearance = appearance52;
            this.ultraCombo1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraCombo1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraCombo1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraCombo1.Location = new System.Drawing.Point(550, 22);
            this.ultraCombo1.Name = "ultraCombo1";
            this.ultraCombo1.Size = new System.Drawing.Size(94, 22);
            this.ultraCombo1.TabIndex = 13;
            // 
            // ultraLabel7
            // 
            appearance53.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel7.Appearance = appearance53;
            this.ultraLabel7.Location = new System.Drawing.Point(462, 24);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(63, 23);
            this.ultraLabel7.TabIndex = 12;
            this.ultraLabel7.Text = "Loại tiền :";
            // 
            // ultraTextEditor3
            // 
            this.ultraTextEditor3.Location = new System.Drawing.Point(101, 121);
            this.ultraTextEditor3.Multiline = true;
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.Size = new System.Drawing.Size(355, 43);
            this.ultraTextEditor3.TabIndex = 11;
            // 
            // ultraLabel6
            // 
            appearance54.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel6.Appearance = appearance54;
            this.ultraLabel6.Location = new System.Drawing.Point(5, 127);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel6.TabIndex = 10;
            this.ultraLabel6.Text = "Trích yếu :";
            // 
            // ultraDateTimeEditor2
            // 
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(326, 98);
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(130, 21);
            this.ultraDateTimeEditor2.TabIndex = 9;
            // 
            // ultraLabel5
            // 
            appearance55.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel5.Appearance = appearance55;
            this.ultraLabel5.Location = new System.Drawing.Point(231, 96);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel5.TabIndex = 8;
            this.ultraLabel5.Text = "Ngày hạch toán :";
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(101, 96);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(124, 21);
            this.ultraDateTimeEditor1.TabIndex = 7;
            // 
            // ultraLabel4
            // 
            appearance56.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel4.Appearance = appearance56;
            this.ultraLabel4.Location = new System.Drawing.Point(6, 98);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel4.TabIndex = 6;
            this.ultraLabel4.Text = "Ngày ký :";
            // 
            // ultraTextEditor2
            // 
            this.ultraTextEditor2.Location = new System.Drawing.Point(101, 72);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.Size = new System.Drawing.Size(356, 21);
            this.ultraTextEditor2.TabIndex = 5;
            // 
            // ultraLabel3
            // 
            appearance57.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel3.Appearance = appearance57;
            this.ultraLabel3.Location = new System.Drawing.Point(5, 72);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel3.TabIndex = 4;
            this.ultraLabel3.Text = "Số đơn hàng :";
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.Location = new System.Drawing.Point(101, 49);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(354, 21);
            this.ultraComboEditor1.TabIndex = 3;
            // 
            // ultraLabel2
            // 
            appearance58.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel2.Appearance = appearance58;
            this.ultraLabel2.Location = new System.Drawing.Point(6, 49);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel2.TabIndex = 2;
            this.ultraLabel2.Text = "Thuộc dự án  :";
            // 
            // ultraTextEditor1
            // 
            this.ultraTextEditor1.Location = new System.Drawing.Point(101, 26);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(355, 21);
            this.ultraTextEditor1.TabIndex = 1;
            // 
            // ultraLabel1
            // 
            appearance59.BackColor = System.Drawing.Color.Transparent;
            this.ultraLabel1.Appearance = appearance59;
            this.ultraLabel1.Location = new System.Drawing.Point(6, 27);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(89, 23);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Số hợp đồng :";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(919, 527);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(919, 527);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(919, 527);
            // 
            // ultraGroupBox1
            // 
            appearance60.FontData.BoldAsString = "True";
            appearance60.FontData.SizeInPoints = 11F;
            this.ultraGroupBox1.Appearance = appearance60;
            this.ultraGroupBox1.Controls.Add(this.ultraTabControl1);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(927, 574);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Thêm mới hợp đồng";
            this.ultraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007;
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl4);
            this.ultraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControl1.Location = new System.Drawing.Point(3, 21);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(921, 550);
            this.ultraTabControl1.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Thông tin chung";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Lãi lỗ";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "Hàng hóa dịch vụ";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "Tài liệu đính kèm";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.ultraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(919, 527);
            // 
            // AddEMContract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 574);
            this.Controls.Add(this.ultraGroupBox1);
            this.Name = "AddEMContract";
            this.Text = "AddEMContract";
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAccountingObjectID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit ultraMaskedEdit3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit ultraMaskedEdit2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit ultraMaskedEdit1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cbbAccountingObjectID;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel29;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor15;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor12;
    }
}