﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using Castle.Facilities.TypedFactory.Internal;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Globalization;

namespace Accounting
{
    public partial class FEMContractBuy : CatalogBase
    {
        IEMContractAttachmentService _IEMContractAttachmentService { get { return IoC.Resolve<IEMContractAttachmentService>(); } }
        IEMContractService _IEmContractService { get { return IoC.Resolve<IEMContractService>(); } }
        IAccountingObjectService _IAccountingObjectService { get { return IoC.Resolve<IAccountingObjectService>(); } }
        IContractStateService _IContractStateService { get { return IoC.Resolve<IContractStateService>(); } }
        ICostSetService _ICostSetService { get { return IoC.Resolve<ICostSetService>(); } }
        IMCPaymentService _ImcPaymentService { get { return IoC.Resolve<IMCPaymentService>(); } }
        IPPInvoiceService _IPPInvoiceService { get { return IoC.Resolve<IPPInvoiceService>(); } }
        IMBTellerPaperService _ImbTellerPaperService { get { return IoC.Resolve<IMBTellerPaperService>(); } }
        IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        IEMContractDetailMGService _IEMContractDetailMGService { get { return IoC.Resolve<IEMContractDetailMGService>(); } }
        IPPOrderDetailService _IPPOrderDetailService { get { return IoC.Resolve<IPPOrderDetailService>(); } }
        IPPInvoiceDetailService _IPPInvoiceDetailService { get { return IoC.Resolve<IPPInvoiceDetailService>(); } }
        List<EMContract> lsteEmContracts = new List<EMContract>();
        List<EMContractAttachment> listEMContractAttachment = new List<EMContractAttachment>();
        List<EMContractBuy> lstEmContractDetailPayment = new List<EMContractBuy>();
        List<EMContractBuy> lstInvoice = new List<EMContractBuy>();
        List<EMContractDetailMG> lstDetailMG = new List<EMContractDetailMG>();
        List<int> typeId = new List<int>() { 110, 116, 117, 118, 119, 120, 126, 127, 128, 129, 130, 131, 132, 133, 134, 140, 141, 142, 143, 144, 170, 171, 172, 173, 174, 210, 240, 260, 261, 262, 263, 264, 500, 600 };
        List<int> typeId1 = new List<int>() { 126, 127, 129, 131, 132, 133, 141, 142, 143, 171, 172, 173, 210, 220, 230, 240, 260, 261, 262, 263, 264 };

        private List<AddDatas> add;
        public FEMContractBuy()
        {
            WaitingFrm.StartWaiting();
            listEMContractAttachment = _IEMContractAttachmentService.GetAll();
            InitializeComponent();
            LoadCofig();
            btnHelp.Visible = false;
            btnExportExcel.Location = btnHelp.GetLocation();
            //ugridDS
            WaitingFrm.StopWaiting();
        }

        void LoadCofig()
        {
            lsteEmContracts = _IEmContractService.GetAllContractBuy();
            ugridDS.DataSource = lsteEmContracts;
            Utils.ConfigGrid(ugridDS, ConstDatabase.FEMContractBuy_TableName);
            UltraGridBand band = ugridDS.DisplayLayout.Bands[0];
            //foreach (UltraGridColumn column in band.Columns)//comment by cuongpv
            //{
            //    this.ConfigEachColumn4Grid(0, column, ugridDS);
            //}
            band.Summaries.Clear();
            SummarySettings summary = band.Summaries.Add("Count", SummaryType.Count, band.Columns["Code"]);
            summary.DisplayFormat = "Số dòng = {0:N0}";
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            ugridDS.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False; //ẩn
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            ugridDS.DisplayLayout.Override.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.SummaryCells;
            ugridDS.DisplayLayout.Bands[0].SummaryFooterCaption = "Số dòng dữ liệu: ";
            ugridDS.DisplayLayout.Override.SummaryFooterAppearance.FontData.Bold = DefaultableBoolean.True;

            summary = band.Summaries.Add("sumAmount", SummaryType.Sum, band.Columns["Amount"]);
            summary.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
            summary.DisplayFormat = "{0:N0}";
            summary.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
            band.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            ugridDS.Rows.SummaryValues["sumAmount"].Appearance.TextHAlign = HAlign.Right;
            //if (ugridDS.Rows.Count > 0) ugridDS.Rows[0].Selected = true;
            if (lsteEmContracts.Count > 0)
            {
                EMContract lstEmAllocation = ugridDS.Rows[0].ListObject as EMContract;
                lblEMcontractCode.Text = lstEmAllocation.Code;
                lblDecreption.Text = lstEmAllocation.Name;
                if (lstEmAllocation.SignedDate != null)
                    lblSignedDate.Text = DateTime.ParseExact(lstEmAllocation.SignedDate.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lblAccoutingCode.Text = "";
                List<AccountingObject> ls = _IAccountingObjectService.GetAll();
                foreach (var item in ls)
                {
                    if (item.ID == lstEmAllocation.AccountingObjectID)
                    {
                        lblAccoutingCode.Text = item.AccountingObjectCode;
                    }
                }
                lblContractState.Text = "";
                List<ContractState> lscontractstate = _IContractStateService.GetAll();
                foreach (var item in lscontractstate)
                {
                    if (item.ContractStateCode == lstEmAllocation.ContractState.ToString())
                    {
                        lblContractState.Text = item.ContractStateName;
                    }
                }
                lblAccoutingName.Text = lstEmAllocation.AccountingObjectName;
                if (lstEmAllocation.EffectiveDate != null)
                    lblEffectiveDate.Text = DateTime.ParseExact(lstEmAllocation.EffectiveDate.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lblCosetID.Text = "";
                List<CostSet> lscostset = Utils.ListCostSet.ToList();
                foreach (var item in lscostset)
                {
                    if (item.ID == lstEmAllocation.CostSetID)
                    {
                        lblCosetID.Text = item.CostSetCode;
                    }
                }
                lblAmount.Text = lstEmAllocation.Amount.ToString("##,###,###");

                lstEmContractDetailPayment = _IGeneralLedgerService.GetEmContractBuys((Guid)ugridDS.Rows[0].Cells["ID"].Value, typeId);
                ugridChi.DataSource = lstEmContractDetailPayment;
                Utils.ConfigGrid(ugridChi, "Chung tu Chi", Template());
                ugridChi.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                UltraGridBand band6 = ugridChi.DisplayLayout.Bands[0];
                band6.Columns["TypeName"].CellActivation = Activation.NoEdit;
                band6.Columns["Date"].CellActivation = Activation.NoEdit;
                band6.Columns["No"].CellActivation = Activation.NoEdit;
                band6.Columns["Reason"].CellActivation = Activation.NoEdit;
                band6.Columns["Amount"].CellActivation = Activation.NoEdit;
                band6.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);

                lstInvoice = _IGeneralLedgerService.GetEmContractBuyInvoices((Guid)ugridDS.Rows[0].Cells["ID"].Value, typeId1);
                uGridInvoice.DataSource = lstInvoice;
                Utils.ConfigGrid(uGridInvoice, "Hoa don", GetColumnHoaDon());
                uGridInvoice.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                UltraGridBand band7 = uGridInvoice.DisplayLayout.Bands[0];
                band7.Columns["InvoiceDate"].CellActivation = Activation.NoEdit;
                band7.Columns["InvoiceNo"].CellActivation = Activation.NoEdit;
                band7.Columns["Reason"].CellActivation = Activation.NoEdit;
                band7.Columns["Amount"].CellActivation = Activation.NoEdit;
                band7.Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);

                lstDetailMG = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)ugridDS.Rows[0].Cells["ID"].Value);
                ugridNhan.DataSource = lstDetailMG;
                UltraGridBand band3 = ugridNhan.DisplayLayout.Bands[0];
                foreach (UltraGridColumn column in band3.Columns)
                {
                    this.ConfigEachColumn4Grid(0, column, ugridNhan);
                }
                Utils.ConfigGrid(ugridNhan, ConstDatabase.EMContractDetailBuyMG_TableName);
                ugridNhan.DisplayLayout.Bands[0].Columns["SAOrderID"].Hidden = true;
                ugridNhan.DisplayLayout.Bands[0].Columns["PPOrderID"].Hidden = true;
                ugridNhan.DisplayLayout.Bands[0].Columns["SAOrderCode"].Hidden = true;
                ugridNhan.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].LockColumn();
                band3.Summaries.Clear();
                SummarySettings summary1 = band3.Summaries.Add("Count", SummaryType.Count, band3.Columns["MaterialGoodsID"]);
                summary1.DisplayFormat = "Số dòng = {0:N0}";
                summary1.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                ugridNhan.DisplayLayout.Override.SummaryFooterCaptionVisible = DefaultableBoolean.False;
                summary1.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter;
            }
        }
        #region event
        private void ugridDS_AfterSelectChange(object sender, Infragistics.Win.UltraWinGrid.AfterSelectChangeEventArgs e)
        {
            if (ugridDS.Selected.Rows.Count > 0)
            {
                EMContract lstEmAllocation = ugridDS.Selected.Rows[0].ListObject as EMContract;
                lblEMcontractCode.Text = lstEmAllocation.Code;
                lblDecreption.Text = lstEmAllocation.Name;
                if (lstEmAllocation.SignedDate != null)
                    lblSignedDate.Text = DateTime.ParseExact(lstEmAllocation.SignedDate.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lblAccoutingCode.Text = "";
                List<AccountingObject> ls = _IAccountingObjectService.GetAll();
                foreach (var item in ls)
                {
                    if (item.ID == lstEmAllocation.AccountingObjectID)
                    {
                        lblAccoutingCode.Text = item.AccountingObjectCode;
                    }
                }
                lblContractState.Text = "";
                List<ContractState> lscontractstate = _IContractStateService.GetAll();
                foreach (var item in lscontractstate)
                {
                    if (item.ContractStateCode == lstEmAllocation.ContractState.ToString())
                    {
                        lblContractState.Text = item.ContractStateName;
                    }
                }
                lblAccoutingName.Text = lstEmAllocation.AccountingObjectName;
                if (lstEmAllocation.EffectiveDate != null)
                    lblEffectiveDate.Text = DateTime.ParseExact(lstEmAllocation.EffectiveDate.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lblCosetID.Text = "";
                List<CostSet> lscostset = _ICostSetService.GetAll();
                foreach (var item in lscostset)
                {
                    if (item.ID == lstEmAllocation.CostSetID)
                    {
                        lblCosetID.Text = item.CostSetCode;
                    }
                }
                lblAmount.Text = lstEmAllocation.Amount.ToString("##,###,###");

                var ID = ugridDS.ActiveRow.Cells["ID"].Value;
                if (ID.ToString() != null)
                {
                    //Tab chứng từ chi
                    ugridChi.DataSource = _IGeneralLedgerService.GetEmContractBuys((Guid)ID, typeId);

                    //Tab hóa đơn
                    uGridInvoice.DataSource = _IGeneralLedgerService.GetEmContractBuyInvoices((Guid)ID, typeId1);

                    //Tab hàng hóa, dịch vụ đã nhận
                    ugridNhan.DataSource = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)ID);

                }
            }

        }
        #endregion
        #region template
        private List<TemplateColumn> Template()
        {
            return new List<TemplateColumn>
            {
                    new TemplateColumn()
                    {
                        VisiblePosition = 0,
                        ColumnCaption = "Loại chứng từ",
                        ColumnName = "TypeName",
                        ColumnWidth = 100,
                        IsVisible = true,
                        IsVisibleCbb = false
                    },
                      new TemplateColumn()
                    {
                        VisiblePosition = 1,
                        ColumnCaption = "Ngày chứng từ",
                        ColumnName = "Date",
                        ColumnWidth = 100,
                        IsVisible = true,
                        IsVisibleCbb = false
                    },
                      new TemplateColumn()
                    {
                        VisiblePosition = 2,
                        ColumnCaption = "Số",
                        ColumnName = "No",
                        ColumnWidth = 100,
                        IsVisible = true,
                        IsVisibleCbb = false
                    },
                         new TemplateColumn()
                    {
                        VisiblePosition = 3,
                        ColumnCaption = "Diễn giải",
                        ColumnName = "Reason",
                        ColumnWidth = 100,
                        IsVisible = true,
                        IsVisibleCbb = false
                    },
                        new TemplateColumn()
                    {
                        VisiblePosition = 4,
                        ColumnCaption = "Tổng tiền",
                        ColumnName = "Amount",
                        ColumnWidth = 100,
                        IsVisible = true,
                        IsVisibleCbb = false
                    },
            };
        }

        private List<TemplateColumn> GetColumnHoaDon()
        {
            return new List<TemplateColumn>
            {
                new TemplateColumn()
                {
                    ColumnCaption = "Ngày hóa đơn",
                    ColumnName = "InvoiceDate",
                    ColumnWidth = 100,
                    ColumnMaxWidth = 500,
                    IsVisible = true,
                    VisiblePosition = 0,
                },
                   new TemplateColumn()
                {
                    ColumnCaption = "Số hóa đơn",
                    ColumnName = "InvoiceNo",
                    ColumnWidth = 100,
                    ColumnMaxWidth = 500,
                    IsVisible = true,
                    VisiblePosition = 1,
                },
                   new TemplateColumn
               {
                   ColumnCaption = "Diễn giải",
                   ColumnName = "Reason",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 2,
                   },

                new TemplateColumn
               {
                   ColumnCaption = "Số tiền",
                   ColumnName = "Amount",
                   ColumnWidth = 100,
                   ColumnMaxWidth = 500,
                   IsVisible = true,
                   VisiblePosition = 3,
                   },

            };
        }
        #endregion
        #region utils
        protected override void DeleteFunction()
        {
            _IEmContractService.BeginTran();
            _IEMContractAttachmentService.BeginTran();
            if (ugridDS.Selected.Rows.Count > 0)
            {
                EMContract temp = ugridDS.Selected.Rows[0].ListObject as EMContract;
                List<PPOrderDetail> lst = _IPPOrderDetailService.GetPPOrderDetailbyContractID(temp.ID);
                List<GeneralLedger> lst1 = _IGeneralLedgerService.GetByContractID(temp.ID);
                if (lst1.Count == 0)
                {
                    if (temp != null && MSG.Question(string.Format(resSystem.MSG_System_05, "hợp đồng " + temp.Code)) == DialogResult.Yes)
                    {
                        foreach (EMContractAttachment item in listEMContractAttachment)
                        {
                            if (item.EMContractID == temp.ID)
                            {
                                _IEMContractAttachmentService.Delete(item);
                            }
                        }
                        foreach (var x in lst)
                        {
                            x.ContractID = null;
                            _IPPOrderDetailService.Update(x);
                        }
                        _IEmContractService.Delete(temp);
                        _IEmContractService.CommitTran();
                        _IEMContractAttachmentService.CommitTran();
                        RefreshUgrid();//edit by cuongpv LoadCofig() -> RefreshUgrid()
                    }
                }
                else
                {
                    MSG.Warning("Xóa " + temp.Code + " thất bại vì đã phát sinh chứng từ liên quan");
                }
            }
            else
                MSG.Error(resSystem.MSG_System_06);
        }

        protected override void AddFunction()
        {
            EMContract temp = ugridDS.Selected.Rows.Count <= 0
                ? new EMContract()
                : ugridDS.Selected.Rows[0].ListObject as EMContract;
            //new FEMContractBuyDetail().ShowDialog(this);
            //if (!FEMContractBuyDetail.isclose)
            //    RefreshUgrid();//edit by cuongpv LoadCofig() -> RefreshUgrid()

            //HUYPD Edit Show Form
            FEMContractBuyDetail frm = new FEMContractBuyDetail();
            frm.FormClosed += new FormClosedEventHandler(FEMContractBuyDetail_FormClosed);
            frm.ShowDialogHD(this);
        }
        #endregion

        protected override void EditFunction()
        {
            if (ugridDS.Selected.Rows.Count > 0)
            {
                EMContract temp = ugridDS.Selected.Rows[0].ListObject as EMContract;
                //new FEMContractBuyDetail(temp).ShowDialog(this);
                //if (!FEMContractBuyDetail.isclose) RefreshUgrid();//edit by cuongpv LoadCofig() -> RefreshUgrid()
                //HUYPD Edit Show Form
                FEMContractBuyDetail frm = new FEMContractBuyDetail(temp);
                frm.FormClosed += new FormClosedEventHandler(FEMContractBuyDetail_FormClosed);
                frm.ShowDialogHD(this);
            }
        }

        private void ugridDS_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            EditFunction();
        }
        private void FEMContractBuyDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            RefreshUgrid();
        }
        #region controls event
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void tsmEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void tsmDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void tmsReLoad_Click(object sender, EventArgs e)
        {
            LoadCofig();
        }

        private void ugridDS_MouseDown(object sender, MouseEventArgs e)
        {
            Utils.uGrid_MouseDown(sender, e, cms4Grid);
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            AddFunction();
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            EditFunction();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void btnRefresh_Click_1(object sender, EventArgs e)
        {
            //LoadCofig();
            RefreshUgrid();
        }

        private void RefreshUgrid()
        {
            Utils.ClearCacheByType<EMContract>();
            lsteEmContracts = _IEmContractService.GetAllContractBuy();
            ugridDS.DataSource = lsteEmContracts;
            if (ugridDS.Selected.Rows.Count > 0)
            {
                EMContract lstEmAllocation = ugridDS.Selected.Rows[0].ListObject as EMContract;
                lblEMcontractCode.Text = lstEmAllocation.Code;
                lblDecreption.Text = lstEmAllocation.Name;
                if (lstEmAllocation.SignedDate != null)
                    lblSignedDate.Text = DateTime.ParseExact(lstEmAllocation.SignedDate.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lblAccoutingCode.Text = "";
                List<AccountingObject> ls = _IAccountingObjectService.GetAll();
                foreach (var item in ls)
                {
                    if (item.ID == lstEmAllocation.AccountingObjectID)
                    {
                        lblAccoutingCode.Text = item.AccountingObjectCode;
                    }
                }
                lblContractState.Text = "";
                List<ContractState> lscontractstate = _IContractStateService.GetAll();
                foreach (var item in lscontractstate)
                {
                    if (item.ContractStateCode == lstEmAllocation.ContractState.ToString())
                    {
                        lblContractState.Text = item.ContractStateName;
                    }
                }
                lblAccoutingName.Text = lstEmAllocation.AccountingObjectName;
                if (lstEmAllocation.EffectiveDate != null)
                    lblEffectiveDate.Text = DateTime.ParseExact(lstEmAllocation.EffectiveDate.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lblCosetID.Text = "";
                List<CostSet> lscostset = _ICostSetService.GetAll();
                foreach (var item in lscostset)
                {
                    if (item.ID == lstEmAllocation.CostSetID)
                    {
                        lblCosetID.Text = item.CostSetCode;
                    }
                }
                lblAmount.Text = lstEmAllocation.Amount.ToString("##,###,###");

                var ID = ugridDS.ActiveRow.Cells["ID"].Value;
                if (ID.ToString() != null)
                {
                    //Tab chứng từ chi
                    ugridChi.DataSource = _IGeneralLedgerService.GetEmContractBuys((Guid)ID, typeId);

                    //Tab hóa đơn
                    uGridInvoice.DataSource = _IGeneralLedgerService.GetEmContractBuyInvoices((Guid)ID, typeId1);

                    //Tab hàng hóa, dịch vụ đã nhận
                    ugridNhan.DataSource = _IEMContractDetailMGService.GetEMContractDetailMGByContractID((Guid)ID);

                    UltraGridBand band3 = ugridNhan.DisplayLayout.Bands[0];
                    foreach (UltraGridColumn column in band3.Columns)
                    {
                        this.ConfigEachColumn4Grid(0, column, ugridNhan);
                    }
                    Utils.ConfigGrid(ugridNhan, ConstDatabase.EMContractDetailBuyMG_TableName);
                    ugridNhan.DisplayLayout.Bands[0].Columns["SAOrderID"].Hidden = true;
                    ugridNhan.DisplayLayout.Bands[0].Columns["PPOrderID"].Hidden = true;
                    ugridNhan.DisplayLayout.Bands[0].Columns["SAOrderCode"].Hidden = true;
                    ugridNhan.DisplayLayout.Bands[0].Columns["MaterialGoodsID"].LockColumn();
                    band3.Summaries.Clear();

                }
                //lsteEmContracts = _IEmContractService.GetAllContractBuy();
                ugridDS.DataSource = _IEmContractService.GetAllContractBuy(); ;

                ugridDS.DataBind();
                ugridChi.DataBind();
                uGridInvoice.DataBind();
                ugridNhan.DataBind();
            }
        }

        private void btnPrint_Click_1(object sender, EventArgs e)
        {
            if (Utils.isDemo)
            {
                MSG.Warning("Phiên bản dùng thử không có quyền sử dụng chức năng này");
                return;
            }
            ugridDS.PrintPreview();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {

        }


        #endregion controls event

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Utils.ExportExcel(ugridDS, "Hợp đồng mua");
        }

        private void FEMContractBuy_FormClosing(object sender, FormClosingEventArgs e)
        {
            //clear ugrid o day
            ugridDS.ResetText();
            ugridDS.ResetUpdateMode();
            ugridDS.ResetExitEditModeOnLeave();
            ugridDS.ResetRowUpdateCancelAction();
            ugridDS.DataSource = null;
            ugridDS.Layouts.Clear();
            ugridDS.ResetLayouts();
            ugridDS.ResetDisplayLayout();
            ugridDS.Refresh();
            ugridDS.ClearUndoHistory();
            ugridDS.ClearXsdConstraints();

            ugridChi.ResetText();
            ugridChi.ResetUpdateMode();
            ugridChi.ResetExitEditModeOnLeave();
            ugridChi.ResetRowUpdateCancelAction();
            ugridChi.DataSource = null;
            ugridChi.Layouts.Clear();
            ugridChi.ResetLayouts();
            ugridChi.ResetDisplayLayout();
            ugridChi.Refresh();
            ugridChi.ClearUndoHistory();
            ugridChi.ClearXsdConstraints();

            uGridInvoice.ResetText();
            uGridInvoice.ResetUpdateMode();
            uGridInvoice.ResetExitEditModeOnLeave();
            uGridInvoice.ResetRowUpdateCancelAction();
            uGridInvoice.DataSource = null;
            uGridInvoice.Layouts.Clear();
            uGridInvoice.ResetLayouts();
            uGridInvoice.ResetDisplayLayout();
            uGridInvoice.Refresh();
            uGridInvoice.ClearUndoHistory();
            uGridInvoice.ClearXsdConstraints();

            ugridNhan.ResetText();
            ugridNhan.ResetUpdateMode();
            ugridNhan.ResetExitEditModeOnLeave();
            ugridNhan.ResetRowUpdateCancelAction();
            ugridNhan.DataSource = null;
            ugridNhan.Layouts.Clear();
            ugridNhan.ResetLayouts();
            ugridNhan.ResetDisplayLayout();
            ugridNhan.Refresh();
            ugridNhan.ClearUndoHistory();
            ugridNhan.ClearXsdConstraints();
        }

        private void FEMContractBuy_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {

            }
        }
    }
}
