﻿using Accounting;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using FX.Core;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting
{
    public partial class FVouchers : CustormForm
    {
        IGeneralLedgerService _IGeneralLedgerService { get { return IoC.Resolve<IGeneralLedgerService>(); } }
        public List<EMContractSale> lstVouchers = new List<EMContractSale>();
        public bool _isPayment;
        public bool _isClose;
        public DateTime timeStart;
        public DateTime timeEnd;
        public List<EMContractSale> lstEMContractSale = new List<EMContractSale>();
        List<int> typeThucThu = new List<int>() { 100, 101, 321, 322, 323, 324 };
        List<int> typeThucChi = new List<int>() { 110, 260, 261, 262, 263, 264 };
        public FVouchers()
        {
            InitializeComponent();
            InitializeGUI();
        }

        public void InitializeGUI()
        {
            uGridVouchers.DataSource = new BindingList<EMContractSale>();            
            Utils.ConfigGrid(uGridVouchers, ConstDatabase.CTTHU_CHI);
            uGridVouchers.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            uGridVouchers.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            uGridVouchers.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;
            uGridVouchers.DisplayLayout.Bands[0].Summaries.Clear();
            uGridVouchers.DisplayLayout.Bands[0].Override.CellClickAction = CellClickAction.Default;
            UltraGridBand band = uGridVouchers.DisplayLayout.Bands[0];
            foreach (var x in band.Columns) if (x.Key != "Select") x.CellActivation = Activation.NoEdit;
            uGridVouchers.DisplayLayout.Bands[0].Columns["Amount"].FormatNumberic(ConstDatabase.Format_TienVND);
            UltraGridColumn ugc = band.Columns["Select"];
            ugc.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;
            ugc.Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;
            ugc.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
            ugc.CellActivation = Activation.AllowEdit;
            ugc.Header.Fixed = true;

        }

        #region events control
        private void btnGetData_Click(object sender, EventArgs e)
        {
            if (_isPayment)
            {
                lstVouchers = _IGeneralLedgerService.GetEMContractSaleThucChiDetailByTypeID(typeThucChi, DateTime.Parse(txtFromDate.Value.ToString()), DateTime.Parse(txtToDate.Value.ToString()));
                uGridVouchers.DataSource = lstVouchers;
            }
            else
            {
                lstVouchers = _IGeneralLedgerService.GetEMContractSaleDetailByTypeID(typeThucThu, DateTime.Parse(txtFromDate.Value.ToString()), DateTime.Parse(txtToDate.Value.ToString()));
                uGridVouchers.DataSource = lstVouchers;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            timeStart = DateTime.Parse(txtFromDate.Value.ToString());
            timeEnd = DateTime.Parse(txtToDate.Value.ToString());
            for (int i = 0; i < uGridVouchers.Rows.Count; i++)
            {
                if(uGridVouchers.Rows[i].Cells["Select"].Value == null)
                {
                    uGridVouchers.Rows[i].Cells["Select"].Value = false;
                }
                if (bool.Parse(uGridVouchers.Rows[i].Cells["Select"].Value.ToString()) == true)
                {
                    EMContractSale emcontractSale = uGridVouchers.Rows[i].ListObject as EMContractSale;
                    lstEMContractSale.Add(emcontractSale);
                }
            }
            _isClose = true;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            _isClose = false;
            this.Close();
        }
        #endregion events control

        private void FVouchers_TextChanged(object sender, EventArgs e)
        {
            if(txtFromDate.Value != null && txtToDate.Value != null)
            {
                if(DateTime.Parse(txtFromDate.Value.ToString()) > DateTime.Parse(txtToDate.Value.ToString()))
                {
                    MSG.Warning("Nhập thời gian không hợp lệ");
                }
            }
        }       
    }
}
