﻿using System;
using System.IO;
using System.Windows.Forms;
using FX.Core;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using Accounting.TextMessage;
using System.Diagnostics;

namespace Accounting
{
    public partial class AddData : CustormForm
    {
        public readonly IEMContractAttachmentService _IEMContractAttachmentService;
        public AddDatas lst = new AddDatas();
        public bool isclose = false;
        public bool them;
        public EMContractAttachment input;
  
        public AddData()
        {
            _IEMContractAttachmentService = IoC.Resolve<IEMContractAttachmentService>();
            this.Text = "Lưu tệp";
            lst = new AddDatas();
            InitializeComponent();
            input = new EMContractAttachment();
            txtTextName.Enabled = false;
            them = true;
        }

        public AddData(EMContractAttachment temp)
        {
            _IEMContractAttachmentService = IoC.Resolve<IEMContractAttachmentService>();
            InitializeComponent();
            this.Text = "Sửa tệp";
            txtTextName.Enabled = false;
            them = false;
            txtNamefile.Text = temp.FileName;
            txtTextName.Text = Path.GetFileName(temp.FileLink);
            txtDecription.Text = temp.Description;
            input = temp;
        }

        public void ultraButton1_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "all file|*.*"
                
            };


            var result = openFile.ShowDialog(this);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                //giới hạn dung lượng ảnh cho phép là 50 kb
                double fileSize = (double)new System.IO.FileInfo(openFile.FileName).Length / 8000;
                var fileExtension = new System.IO.FileInfo(openFile.FileName).Extension;
                if((fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".gif" || fileExtension == ".bmp")&& fileSize > 50)               
                {
                    MSG.Error(resSystem.MSG_Catalog_FBankAccountDetail);
                    return;
                }
                //Kích thước ảnh cho phép không lớn hơn 128x128
                try
                {
                    using (FileStream file = new FileStream(openFile.FileName, FileMode.Open, FileAccess.Read))
                    {
                        input.FileAttachment = new byte[file.Length];
                        input.FileSize = Convert.ToInt32(fileSize);
                        input.FileExtension = fileExtension;
                        input.Description = txtDecription.ToString();
                        input.FileLink = openFile.FileName;
                        txtTextName.Text = openFile.SafeFileName.ToString();
                    }
                }
                catch (Exception)
                {
                    MSG.Error(resSystem.MSG_Catalog_FBank);
                    return;
                }

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            isclose = false;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtNamefile.Text == "")
            {
                MSG.Error("Bạn chưa nhập tên tệp, mời nhập lại");
                return;
            }
            if(txtTextName.Text == "")
            {
                MSG.Error("Bạn chưa có file đính kèm, mời chọn file");
                return;
            }
            if (them == true)
            {
                input.ID = Guid.NewGuid();
                input.FileName = txtNamefile.Text;
                input.Description = txtDecription.Text;
                isclose = true;
                this.Close();
            }
            else
            {
                input.FileName = txtNamefile.Text;
                input.Description = txtDecription.Text;
                isclose = true;
                this.Close();
            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            if(File.Exists(input.FileLink))
            {
                try
                {
                    //FileStream fs = File.Open(input.FileLink, FileMode.Open);
                    Process.Start(input.FileLink);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
    }
}
