﻿namespace Accounting
{
    partial class AddData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.txtNamefile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.txtDecription = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.btnClose = new Infragistics.Win.Misc.UltraButton();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamefile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTextName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecription)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(12, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Tên tài liệu";
            // 
            // ultraLabel2
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(12, 73);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel2.TabIndex = 1;
            this.ultraLabel2.Text = "Diễn giải ";
            // 
            // ultraLabel3
            // 
            appearance3.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance3;
            this.ultraLabel3.Location = new System.Drawing.Point(12, 39);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(65, 22);
            this.ultraLabel3.TabIndex = 2;
            this.ultraLabel3.Text = "Tệp tài liệu";
            // 
            // txtNamefile
            // 
            appearance4.TextVAlignAsString = "Middle";
            this.txtNamefile.Appearance = appearance4;
            this.txtNamefile.AutoSize = false;
            this.txtNamefile.Location = new System.Drawing.Point(96, 12);
            this.txtNamefile.Name = "txtNamefile";
            this.txtNamefile.Size = new System.Drawing.Size(218, 22);
            this.txtNamefile.TabIndex = 3;
            // 
            // txtTextName
            // 
            appearance5.TextVAlignAsString = "Middle";
            this.txtTextName.Appearance = appearance5;
            this.txtTextName.AutoSize = false;
            this.txtTextName.Location = new System.Drawing.Point(96, 39);
            this.txtTextName.Name = "txtTextName";
            this.txtTextName.ReadOnly = true;
            this.txtTextName.Size = new System.Drawing.Size(218, 22);
            this.txtTextName.TabIndex = 4;
            // 
            // txtDecription
            // 
            appearance6.TextVAlignAsString = "Middle";
            this.txtDecription.Appearance = appearance6;
            this.txtDecription.Location = new System.Drawing.Point(96, 67);
            this.txtDecription.Multiline = true;
            this.txtDecription.Name = "txtDecription";
            this.txtDecription.Size = new System.Drawing.Size(294, 35);
            this.txtDecription.TabIndex = 5;
            // 
            // ultraButton1
            // 
            appearance7.Image = global::Accounting.Properties.Resources.Avosoft_Warm_Toolbar_Folder_open;
            appearance7.ImageBackground = global::Accounting.Properties.Resources.new_file_icon;
            this.ultraButton1.Appearance = appearance7;
            this.ultraButton1.Location = new System.Drawing.Point(320, 39);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(32, 22);
            this.ultraButton1.TabIndex = 6;
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // ultraButton2
            // 
            appearance8.Image = global::Accounting.Properties.Resources.timkiem1;
            this.ultraButton2.Appearance = appearance8;
            this.ultraButton2.Location = new System.Drawing.Point(358, 39);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(32, 22);
            this.ultraButton2.TabIndex = 7;
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // btnClose
            // 
            appearance9.Image = global::Accounting.Properties.Resources.btnPoweOff;
            this.btnClose.Appearance = appearance9;
            this.btnClose.Location = new System.Drawing.Point(315, 113);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 304;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            appearance10.Image = global::Accounting.Properties.Resources.ubtnSave;
            this.btnSave.Appearance = appearance10;
            this.btnSave.Location = new System.Drawing.Point(231, 113);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 303;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // AddData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 155);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ultraButton2);
            this.Controls.Add(this.ultraButton1);
            this.Controls.Add(this.txtDecription);
            this.Controls.Add(this.txtTextName);
            this.Controls.Add(this.txtNamefile);
            this.Controls.Add(this.ultraLabel3);
            this.Controls.Add(this.ultraLabel2);
            this.Controls.Add(this.ultraLabel1);
            this.Icon = global::Accounting.Properties.Resources.icon;
            this.Name = "AddData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm tệp đính kèm";
            ((System.ComponentModel.ISupportInitialize)(this.txtNamefile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTextName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecription)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtNamefile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtTextName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDecription;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton btnClose;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}